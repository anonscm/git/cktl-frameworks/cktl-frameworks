/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

// EOStructure.java
package org.cocktail.fwkcktlpersonne.common.metier;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.controles.ControleSiret;
import org.cocktail.fwkcktlpersonne.common.controles.ControleSpecifique;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForEntrepriseSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForPartenaireSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForServiceSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForUtilisateurSISpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructureTypesGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.providers.CstructureProvider;
import org.cocktail.fwkcktlpersonne.common.metier.providers.CstructureProviderDefaultImpl;
import org.cocktail.fwkcktlpersonne.server.util.FwkCktlPersonneUtil;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.eof.CktlFetchSpecification;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.qualifiers.ERXTrueQualifier;

/**
 *
 Classe permettant la gestion des objets Structure.<br/>
 * <ul>
 * <li>Règles d'initialisation : {@link EOStructure#awakeFromInsertion(EOEditingContext)}</li>
 * <li>Règles de validation : {@link EOStructure#validateObjectMetier()}</li>
 * </ul>
 * Si des spécificités sont rattachées à un objet EOStructure, d'autres règles doivent être respectées ({@link EOStructure#registerDetectedSpec()}).<br/>
 * <b>Exemple 1 : Creation d'une structure simple</b><br/>
 * <code>
 *  <br/>
 *  	EOStructure structure = EOStructure.creerInstance(myEditingContext);<br/>
 *  	structure.setStrAffichage("Société des Exportation Inter Sidérales");<br/>
 *  	...<br/>
 *   	myEditingContext.saveChanges();<br/>
 *   <br/>
 * </code> <br/>
 * <b>Exemple 2 : Creation d'une structure qui doit être un service de l'établissement </b><br/>
 * <code>
 *  <br/>
 *  	EOStructure structure = EOStructure.creerInstance(myEditingContext, new NSArray(EOStructureForServiceSpec.sharedInstance()));<br/>
 *  	structure.setStrAffichage("UFR d'Etudes Antédéluviennes");<br/>
 *  	...<br/>
 *   	myEditingContext.saveChanges();<br/>
 *   <br/>
 *
 * </code> <br/>
 * Remarques sur les groupes:<br/>
 * Tout nouveau groupe doit avoir un repartTypeGroupe a G. Tout groupe existant doit avoir au moins un repartTypeGroupe.
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

/**
 * Dans cette classe, nous avons ajouté une instance de la classe FwkCktlPersonneGestionErreurs permettant de renvoyer l'erreur générée dans celle-ci
 * et la trier grâce à un double numéro représentant le numéro du fichier et le numéro de la méthode. Ces numéros ont été insrits en dur et sont
 * répertoriés sur un fichier Excel à part.
 * 
 * @author Alain Malaplate
 */

public class EOStructure extends _EOStructure implements IPersonne, IStructure, IStructureTypesGroupe {

	public static final Logger logger = Logger.getLogger(EOStructure.class);

	public static final NSArray<String> IGNORE_CHANGES_IN_KEYS = new NSArray<String>(
			new String[] {
					TO_REPART_STRUCTURES_KEY,
					TO_REPART_STRUCTURES_ELTS_KEY,
					TO_REPART_ASSOCIATIONS_ELTS_KEY, TO_REPART_ASSOCIATIONS_KEY
			});

	public static final String EXCEPTION_NAF = "Le code NAF spécifié n'est pas valide.";
	public static final String TEM_VALIDE_OUI = OUI;
	public static final String TEM_VALIDE_NON = NON;
	// public static final String TEM_VALIDE_OUI = "O";
	// public static final String TEM_VALIDE_NON = "N";
	public static final EOQualifier QUAL_STRUCTURES_VALIDE = new EOKeyValueQualifier(
			EOStructure.TEM_VALIDE_KEY, EOQualifier.QualifierOperatorEqual,
			EOStructure.TEM_VALIDE_OUI);
	public static final EOQualifier QUAL_STRUCTURES_INVALIDE = new EOKeyValueQualifier(
			EOStructure.TEM_VALIDE_KEY, EOQualifier.QualifierOperatorEqual,
			EOStructure.TEM_VALIDE_NON);
	public static final EOQualifier QUAL_TOUTES_STRUCTURES = ERXQ.or(
			QUAL_STRUCTURES_VALIDE, QUAL_STRUCTURES_INVALIDE);

	/**
	 * Structures de type composantes (qualifier basé sur le type_structure). Utilisez plutot {@link EOStructure#QUAL_STRUCTURES_AS_COMPOSANTES}
	 */
	public static final EOQualifier QUAL_STRUCTURES_TYPE_COMPOSANTE = new EOKeyValueQualifier(
			EOStructure.C_TYPE_STRUCTURE_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOTypeStructure.TYPE_STRUCTURE_C);

	/**
	 * Structures de type Etablissements (qualifier basé sur le type_structure).
	 */
	public static final EOQualifier QUAL_STRUCTURES_TYPE_ETABLISSEMENT = new EOKeyValueQualifier(
			EOStructure.C_TYPE_STRUCTURE_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOTypeStructure.TYPE_STRUCTURE_E);

	/**
	 * Structures de type Etablissement secondaires (qualifier basé sur le type_structure).
	 */
	public static final EOQualifier QUAL_STRUCTURES_TYPE_ETABLISSEMENT_SECONDAIRE = new EOKeyValueQualifier(
			EOStructure.C_TYPE_STRUCTURE_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOTypeStructure.TYPE_STRUCTURE_ES);

	/**
	 * Structures de type autres (qualifier basé sur le type_structure).
	 */
	public static final EOQualifier QUAL_STRUCTURES_TYPE_AUTRES = new EOKeyValueQualifier(
			EOStructure.C_TYPE_STRUCTURE_KEY,
			EOQualifier.QualifierOperatorEqual,
			EOTypeStructure.TYPE_STRUCTURE_A);

	/** Structures qui sont des composantes */
	public static final EOQualifier QUAL_STRUCTURES_AS_COMPOSANTES = QUAL_STRUCTURES_TYPE_COMPOSANTE;

	/** Structures qui sont des services. */
	public static final EOQualifier QUAL_STRUCTURES_AS_SERVICES = EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES;

	/** Structures qui sont des laboratoires. */
	public static final EOQualifier QUAL_STRUCTURES_AS_LABORATOIRES = EOStructureForGroupeSpec.QUAL_GROUPES_LABORATOIRES;

	/** Structures qui sont internes a l'établissement. */
	public static final EOQualifier QUAL_STRUCTURES_INTERNES = new EOOrQualifier(
			new NSArray(
					new Object[] {
							QUAL_STRUCTURES_AS_LABORATOIRES,
							QUAL_STRUCTURES_AS_SERVICES,
							QUAL_STRUCTURES_AS_COMPOSANTES
					}));

	/**
	 * Etablissements externes (structures avec un RNE qui ne soient pas racine de l'annuaire ni etab secondaire)
	 */
	public static final EOQualifier QUAL_STRUCTURES_ETABLISSEMENTS_EXTERNES = new EOAndQualifier(
			new NSArray(new Object[] {
					new EONotQualifier(new EOKeyValueQualifier(
							EOStructure.C_RNE_KEY,
							EOQualifier.QualifierOperatorEqual, null)),
					new EONotQualifier(
							QUAL_STRUCTURES_TYPE_ETABLISSEMENT_SECONDAIRE),
					new EONotQualifier(
							EOQualifier.qualifierWithQualifierFormat(
									EOStructure.C_STRUCTURE_KEY + "="
											+ EOStructure.C_STRUCTURE_PERE_KEY,
									null))
			}));

	/** Structures qui sont des entreprises */
	public static final EOQualifier QUAL_STRUCTURES_ENTREPRISES = EOStructureForGroupeSpec.QUAL_GROUPE_ENTREPRISES;

	/** Structures qui sont des forums */
	public static final EOQualifier QUAL_STRUCTURES_FORUMS = EOStructureForGroupeSpec.QUAL_GROUPE_FORUMS;

	/** Qualifier pour recuperer les structures prives (grpAcces = -) */
	public static final EOQualifier QUAL_GROUPE_PRIVE = new EOKeyValueQualifier(
			EOStructure.GRP_ACCES_KEY, EOQualifier.QualifierOperatorEqual,
			EOStructureForGroupeSpec.GRP_ACCES_PRIVE);

	/** Qualifier pour recuperer les structures prives (grpAcces = +) */
	public static final EOQualifier QUAL_GROUPE_PUBLIC = new EOKeyValueQualifier(
			EOStructure.GRP_ACCES_KEY, EOQualifier.QualifierOperatorEqual,
			EOStructureForGroupeSpec.GRP_ACCES_PUBLIC);

	/** Structures qui ne sont affectees a aucun groupe */
	// public static final EOQualifier QUAL_STRUCTURES_SANS_GROUPE = new
	// EOKeyValueQualifier("@count." +
	// EOStructure.TO_REPART_STRUCTURES_KEY, EOQualifier.QualifierOperatorEqual,
	// Integer.valueOf(0));
	// public static final EOQualifier QUAL_STRUCTURES_SANS_GROUPE =
	// EOQualifier.qualifierWithQualifierFormat("@count." +
	// EOStructure.TO_REPART_STRUCTURES_KEY + "=0", null);

	public static final EOSortOrdering SORT_LL_STRUCTURE_ASC = EOSortOrdering
			.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY,
					EOSortOrdering.CompareAscending);

	public static final String SEQ_STRUCTURE_ENTITY_NAME = "Fwkpers_SeqStructure";

	public static final String LIBELLE_FOR_GROUPE_KEY = "libelleForGroupe";

	public static final String IS_ARCHIVE_KEY = "isArchivee";

	private PersonneDelegate personneDelegate = new PersonneDelegate(this);

	private static CstructureProvider cstructureProvider = new CstructureProviderDefaultImpl();

	private final int longueurSiren = 9;

	public EOStructure() {
		super();
	}

	public static void setCstructureProvider(CstructureProvider provider) {
		cstructureProvider = provider;
	}

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		registerDetectedSpec();
		super.validateForInsert();
	}

	/**
	 * Teste si l'objet doit respecter certaines specificites et les affecte à l'objet.<br/>
	 * Par exemple si la structure est un service ( {@link EOStructureForServiceSpec#isSpecificite(AfwkPersRecord)} renvoie true), la specificité
	 * EOStructureForServiceSpec est affecté à l'objet via {@link #registerSpecificite(ISpecificite)}).<br/>
	 * Cette méthode est appelée par les méthodes de validation. <br/>
	 * <br/>
	 * Liste des specificites detectees :
	 * <ul>
	 * <li>{@link EOStructureForFournisseurSpec}</li>
	 * <li>{@link EOStructureForServiceSpec}</li>
	 * <li>{@link EOStructureForGroupeSpec}</li>
	 * <li>{@link EOStructureForEntrepriseSpec}</li>
	 * <li>{@link EOStructureForUtilisateurSISpec}</li>
	 * </ul>
	 * 
	 * @return true si des nouvelles specificites ont été ajoutées.
	 */
	public boolean registerDetectedSpec() {
		boolean res = false;
		if (!hasSpecificite(EOStructureForFournisseurSpec.sharedInstance())
				&& EOStructureForFournisseurSpec.sharedInstance()
						.isSpecificite(this)) {
			registerSpecificite(EOStructureForFournisseurSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOStructureForServiceSpec.sharedInstance())
				&& EOStructureForServiceSpec.sharedInstance().isSpecificite(
						this)) {
			registerSpecificite(EOStructureForServiceSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOStructureForGroupeSpec.sharedInstance())
				&& EOStructureForGroupeSpec.sharedInstance()
						.isSpecificite(this)
				&& !EOStructureForServiceSpec.sharedInstance().isSpecificite(
						this)) {
			registerSpecificite(EOStructureForGroupeSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOStructureForEntrepriseSpec.sharedInstance())
				&& EOStructureForEntrepriseSpec.sharedInstance().isSpecificite(
						this)) {
			registerSpecificite(EOStructureForEntrepriseSpec.sharedInstance());
			res = true;
		}
		if (!hasSpecificite(EOStructureForUtilisateurSISpec.sharedInstance())
				&& EOStructureForUtilisateurSISpec.sharedInstance()
						.isSpecificite(this)) {
			registerSpecificite(EOStructureForUtilisateurSISpec
					.sharedInstance());
			res = true;
		}

		// FIXME Ajouter des spécificités

		return res;
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		registerDetectedSpec();
		super.validateForUpdate();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * <ul>
	 * <li>Remplir le strAffichage si celui-ci est vide.</li>
	 * <li>Verifier si le nom d'affichage est egal au libellé en majuscules sans accent.</li>
	 * <li>Verifier que le type de structure est correct.</li>
	 * <li>Verifier que le grpAcces est valide si saisi.</li>
	 * <li>Verifier que le code NAF est valide si saisi.</li>
	 * <li>Verifier que le code RNE n'est pas affecté a une autre structure si saisi.</li>
	 * <li>Verifier que le code SIRET est valide si saisi.</li>
	 * <li>Verifier que le code SIRET n'est pas affecté a une autre structure si saisi.</li>
	 * <li>Verifier que le code TVA est valide si saisi.</li>
	 * <li>Verifier que les dates ouverture/fermeture sont coherentes si saisies.</li>
	 * <li>Si affecté dans aucun groupe, affecte au groupe par defaut (defini par parametre ANNUAIRE_STRUCTURE_DEFAUT).</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (objectHasChanged()) {
			try {
				trimAllString();
				setDModification(getTimeStamp());
				// Ajout des persIdModification à partir du user courant
				if (!"O".equals(FwkCktlPersonne.paramManager
						.getParam(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE))
						&& hasDirtyAttributes()) {
					fixPersIdCreationEtModification();
					// FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE);
				}
				// fixPersIdCreationEtModification();
				// Verifier que l'utilisateur a bien le droit de modifier la
				// structure
				// s'il s'agit d'un groupe reservé, on ne controle pas, sinon
				// l'utilisateur n'a jamais les droits
				if (!EOStructureForGroupeSpec.isGroupeGereEnAuto(this)) {
					getPersonneDelegate().checkUsers();
				}

				if (isObjectInValidationEditingContext()) {
					CktlLog.trace("Validation de EOStructure " + libelleEtId());

					fixNoms();

					fixGroupeForStructurePere();
					fixSiren();

					checkContraintesObligatoires();
					checkContraintesLongueursMax();

					// verifier que le nom affichage egal llStructure en
					// majuscules sans accents.
					checkNoms();

					// Verifier que le type de structure est correct
					checkTypeStructure();

					// alias
					checkAlias();

					// if
					// (EOTypeStructure.TYPE_STRUCTURE_C.equals(cTypeStructure())
					// ||
					// EOTypeStructure.TYPE_STRUCTURE_CS.equals(cTypeStructure()))
					// {
					if (isInterneEtablissement().booleanValue()) {
						if (cStructurePere() == null) {
							String messErreur = "Une structure de l'établissement doit avoir une structure parent";
							throw new NSValidation.ValidationException(
									messErreur);
						}
					}

					checkGrpacces();
					checkNafValide();
					checkDoublonForRne();
					if (hasKeyChangedFromCommittedSnapshot(SIRET_KEY)) {
						checkSiretValide();
						checkSirenValide();
					}
					//					checkSiretValide();
					//					checkSirenValide();

					checkTVAIntra();
					checkDates();
					checkArborescence();
					checkStrActivite();
					checkRnsr();
					// Si la structure est interne, on ne verifie pas les
					// doublons de SIRET (regle metier à valider...)
					// Rajout du .booleanValue() pour tester le booléen et non
					// l'objet
					if (!isInterneEtablissement().booleanValue()) {
						checkDoublonsForSiret();
						fixGroupeEntreprises();
					}
					
					if (temSectorise() == null || temSectorise().isEmpty()) {
						setTemSectorise("N");
					}

					super.validateObjectMetier();
					// si pas affecte a un groupe, l'affecter a un groupe par
					// defaut (ANNUAIRE_STRUCTURE_DEFAUT").
					personneDelegate.fixGroupeDefaut(persIdModification());

					// si pas fournisseurspec, verifier que pas dans groupe
					// repart FO
					if (!EOStructureForFournisseurSpec.sharedInstance()
							.isSpecificite(this)) {
						if (EOStructureForGroupeSpec.isPersonneInGroupeOfType(
								this, EOTypeGroupe.TGRP_CODE_FO)) {
							String messErreur = "Une structure qui n'est pas fournisseur ne peut être affectée à un groupe de type FO.";
							throw new NSValidation.ValidationException(
									messErreur);
						}
					}

				}
			} catch (NSValidation.ValidationException e) {
				e.printStackTrace();
				NSValidation.ValidationException e1 = new ValidationException(
						libelleEtId() + " : " + e.getMessage());
				throw e1;
			}
		}
	}

	private void checkRnsr() throws NSValidation.ValidationException {
		if (idRnsr() == null) {
			return;
		}
		Pattern pattern =  Pattern.compile("\\p{Digit}{9}\\p{Upper}");
		Matcher matcher = pattern.matcher(idRnsr());
		if (!matcher.matches()) {
			throw new NSValidation.ValidationException("L'identifiant RNSR doit être composé de 9 chiffres et une lettre");
		}
	}

	/**
	 * Verifie la coherence d'un n° de TVA intracommunautaire. Actuellement seulement fait pour le France. Un formulaire existe pour verifier ca :
	 * http://ec.europa.eu/taxation_customs/vies/vieshome.do
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkTVAIntra() throws NSValidation.ValidationException {
		if (!MyStringCtrl.isEmpty(tvaIntracom())) {
			// le code ne doit pas etre < 3 caracteres
			if (tvaIntracom().length() < 3) {
				String messErreur = "Le code de TVA Intracommunautaire ne peut être inférieur à 3 caractères.";
				throw new NSValidation.ValidationException(messErreur);
			}

			String clePays = tvaIntracom().substring(0, 2);

			// verifier n° francais (plus courant)
			if ("FR".equals(clePays)) {
				if (tvaIntracom().length() < 13) {
					String messErreur = "Pour la France le n° de TVA doit être égal à FR+ Clé (2 caractères) +n° Siren (9 caractères).";
					throw new NSValidation.ValidationException(messErreur);
				}

				String fin = tvaIntracom().substring(4);

				if (siren() != null) {
					if (!siren().equals(fin)) {
						String messErreur = "Pour la France le n° de TVA doit être égal à FR+ Clé (2 caractères) +n° Siren (9 caractères).";
						throw new NSValidation.ValidationException(messErreur);
					}
				}
			}

			// Pour les autres pays, non developpé...

		}
	}

	/**
	 * Verifier que les dates ouverture/fermeture sont coherentes
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkDates() throws NSValidation.ValidationException {
		if (dateOuverture() != null && dateFermeture() != null
				&& DateCtrl.isAfterEq(dateOuverture(), dateFermeture())) {
			String messErreur = "La date d'ouverture ne peut être postérieure à la date de fermeture";
			throw new NSValidation.ValidationException(messErreur);
		}
	}

	/**
	 * @see PersonneUtil#fixGroupeDefaut(IPersonne)
	 */
	private void fixGroupeDefaut() throws NSValidation.ValidationException {
		personneDelegate.fixGroupeDefaut(persIdModification());
	}

	/**
	 * L'arborescence des structure n'est valable que dans le cas ou ces structures sont des groupes. Donc si on a une structure pere, on s'assure que
	 * la structure est un groupe. La verification est ensuite effectuee dans {@link EOStructure#checkArborescence() }
	 */
	private void fixGroupeForStructurePere() {
		if (toStructurePere() != null) {
			// Verifier que la structure est un groupe
			if (!toStructurePere().equals(this)
					&& !EOStructureForGroupeSpec.isGroupeOfType(this,
							EOTypeGroupe.TGRP_CODE_G)) {
				EOStructureForGroupeSpec.setStructureAsGroupe(this);
			}
		}
	}

	private void fixGroupeEntreprises() {
		EOStructureForGroupeSpec.sharedInstance().fixGroupeEntreprises(this);
	}

	/**
	 * Corrige les incohérences sur les noms (entre nom affichage et llStructure)
	 */
	private void fixNoms() {
		if (!MyStringCtrl.isEmpty(llStructure())
				&& MyStringCtrl.isEmpty(strAffichage())) {
			setStrAffichage(llStructure());
		}
		if (!MyStringCtrl.isEmpty(llStructure())) {
			if (!llStructure().equals(
					MyStringCtrl.chaineSansAccents(strAffichage())
							.toUpperCase())) {
				setStrAffichage(llStructure());
			}
		}
	}

	private void fixSiren() {
		if (MyStringCtrl.isEmpty(siren()) && !MyStringCtrl.isEmpty(siret())
				&& siret().trim().length() >= longueurSiren) {
			setSiren(siret().substring(0, longueurSiren));
		}
	}

	/**
	 * Verifie si le code NAF existe.
	 * 
	 * @throws NSValidation.ValidationException Si le code NAF n'est pas trouvé.
	 */
	private void checkNafValide() throws NSValidation.ValidationException {
		if (cNaf() != null) {
			setCNaf(cNaf().trim());
			if (cNaf().length() > 0) {
				EONaf naf = EONaf.getNaf(this.editingContext(),
						new NSTimestamp(), cNaf());
				if (naf == null) {
					throw new NSValidation.ValidationException(EXCEPTION_NAF);
				}
			}
		}
	}

	private void checkStrActivite() throws NSValidation.ValidationException {
		if (strActivite() != null) {
			setStrActivite(strActivite().trim());
			if (strActivite().length() > 0) {
				EOActivites res = null;
				try {
					res = EOActivites.fetchRequiredByKeyValue(editingContext(),
							EOActivites.L_ACTIVITE_KEY, strActivite());
				} catch (Exception e) {
				}
				if (res == null) {
					String messErreur = "L'activité spécifiée ("
							+ strActivite()
							+ ") n'est pas dans la liste des activités possibles (table "
							+ EOActivites.ENTITY_TABLE_NAME + ").";
					throw new NSValidation.ValidationException(messErreur);
				}
			}
		}
	}

	/**
	 * Verifie que le type structure est correct.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkTypeStructure() throws NSValidation.ValidationException {
		if (EOTypeStructure.fetchByKeyValue(editingContext(),
				EOTypeStructure.C_TYPE_STRUCTURE_KEY, cTypeStructure()) == null) {
			String messErreur = "Le type de structure est invalide ("
					+ cTypeStructure() + ")";
			throw new NSValidation.ValidationException(messErreur);
		}
	}

	/**
	 * Verifie la coherence des données dans le cas ou une structure pere est definie. L'arborescence des structure n'est valable que dans le cas ou
	 * ces structures sont des groupes. Donc si on a une structure pere, on s'assure que
	 * <ul>
	 * <li>La structure pere est bien un groupe</li>
	 * <li>La structure en cours est bien un groupe</li>
	 * <li>La structure en cours est differente de la structure pere</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkArborescence() throws NSValidation.ValidationException {
		if (toStructurePere() != null) {
			// Verifier que la structure pere est un groupe
			if (!EOStructureForGroupeSpec.isGroupeOfType(toStructurePere(),
					EOTypeGroupe.TGRP_CODE_G)) {
				String messErreur = "La structure père "
						+ toStructurePere().libelleEtId()
						+ " doit être un groupe.";
				throw new NSValidation.ValidationException(messErreur);
			}

			// Verifier que la structure est un groupe
			if (!EOStructureForGroupeSpec.isGroupeOfType(this,
					EOTypeGroupe.TGRP_CODE_G)) {
				String messErreur = "Pour avoir une structure parente, la structure "
						+ libelleEtId() + " doit être un groupe.";
				throw new NSValidation.ValidationException(messErreur);
			}

			// Verifier que structurePere <> structure, sauf cas etablissement
			if (!isEtablissement()) {
				if (toStructurePere().equals(this)) {
					String messErreur = "La structure ne peut être identique à sa structure pere.";
					throw new NSValidation.ValidationException(messErreur);
				}
			}
		}
	}

	public String libelleEtId() {
		return persLibelle() + " (persid=" + persId() + ")";
	}

	/**
	 * Verifier si le grpAcces est valide (+ ou -)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkGrpacces() throws NSValidation.ValidationException {
		if (grpAcces() != null) {
			if ((!EOStructureForGroupeSpec.GRP_ACCES_PUBLIC.equals(grpAcces()) && !EOStructureForGroupeSpec.GRP_ACCES_PRIVE
					.equals(grpAcces()))) {
				String messErreur = "Le champ grpAcces doit être rempli avec + ou -.";
				throw new NSValidation.ValidationException(messErreur);
			}
		}
	}

	/**
	 * Vérifier qu'il n'y a pas d'autres structures avec le même numéro de Rne.
	 */
	private void checkDoublonForRne() throws NSValidation.ValidationException {
		if (toRne() != null) {
			NSArray structures = EOStructure.rechercherStructuresAvecRne(
					editingContext(), toRne().cRne());
			if (structures.count() > 1
					|| (structures.count() == 1 && structures.objectAtIndex(0) != this)) {
				String messErreur = "Une autre structure a déjà ce RNE";
				throw new NSValidation.ValidationException(messErreur);
			}
		}
	}

	/**
	 * Vérifie si le nunero de SIRET est valide.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkSiretValide() throws NSValidation.ValidationException {
		if (siret() != null && temSiretProvisoire() != null
				&& temSiretProvisoire().equals("R")) {
			final int longueurSiret = 14;

			// if (siret() != null) {
			if (!MyStringCtrl.isDigits(siret())) {
				String messErreur = "Le numéro de Siret doit être numérique.";
				throw new NSValidation.ValidationException(messErreur);
			}

			if (siret().length() < longueurSiret) {
				String messErreur = "Le numéro de siret doit comporter 14 caractères";
				throw new NSValidation.ValidationException(messErreur);
			}

			if (siret().length() > longueurSiret) {
				String messErreur = "Le numéro de siret comporte au plus 14 caractères";
				throw new NSValidation.ValidationException(messErreur);
			}

			if (siret().length() == longueurSiret && MyStringCtrl.hasOnlyDigits(siret())
					&& Long.parseLong(siret()) == 0) {
				String messErreur = "Le numéro de siret doit être différent de la simple série de 14 zéros";
				throw new NSValidation.ValidationException(messErreur);
			}

			String verifierSiret = EOGrhumParametres.parametrePourCle(
					editingContext(), "GRHUM_CONTROLE_SIRET");
			if (verifierSiret != null && verifierSiret.startsWith(OUI)) {
				if (isSiretValide(siret()) == false) {
					String messErreur = "Le numéro de Siret est invalide";
					throw new NSValidation.ValidationException(messErreur);
				}
			}

			// verif doublons effectuee dans methode a part
			// // Vérifier qu'il n'y a pas d'autres structures avec le même
			// numéro de Siret
			// NSArray structures =
			// EOStructure.rechercherStructuresAvecSiret(editingContext(),
			// siret());
			// if (structures.count() > 1 || (structures.count() == 1 &&
			// structures.objectAtIndex(0) != this)) {
			// throw new
			// NSValidation.ValidationException("Une autre structure a déjà ce numéro de siret");
			// }

			if (!siret().startsWith(siren())) {
				String messErreur = "Le numéro de siret doit commencer par le n° de Siren.";
				throw new NSValidation.ValidationException(messErreur);
			}
		}
	}

	private void checkSirenValide() throws NSValidation.ValidationException {
		if (siren() != null) {

			if (siren().length() > longueurSiren) {
				String messErreur = "Le numéro de siret comporte au plus 9 caractères";
				throw new NSValidation.ValidationException(messErreur);
			}
			String verifierSiret = EOGrhumParametres.parametrePourCle(
					editingContext(), "GRHUM_CONTROLE_SIRET");
			if (verifierSiret != null && verifierSiret.startsWith(OUI)) {
				if (isSirenValide(siren()) == false) {
					String messErreur = "Le numéro de Siren est invalide";
					throw new NSValidation.ValidationException(messErreur);
				}
			}
		}
	}

	/**
	 * Vérifier qu'il n'y a pas d'autres structures avec le même numéro de Siret.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkDoublonsForSiret()
			throws NSValidation.ValidationException {
		if (siret() != null) {
			// Vérifier qu'il n'y a pas d'autres structures avec le même numéro
			// de Siret
			NSArray structures = EOStructure.rechercherStructuresAvecSiret(
					editingContext(), siret());
			if (structures.count() > 1
					|| (structures.count() == 1 && structures.objectAtIndex(0) != this)) {
				String messErreur = "Une autre structure a déjà ce numéro de Siret";
				throw new NSValidation.ValidationException(messErreur);
			}
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	@Override
	public void validateBeforeTransactionSave()
			throws NSValidation.ValidationException {
		checkArchivage();
		super.validateBeforeTransactionSave();
	}

	/**
	 * @see IPersonne#persLc()
	 */
	public String persLc() {
		return VIDE;
	}

	/**
	 * @see IPersonne#persLibelle()
	 */
	public String persLibelle() {
		return llStructure();
	}

	/**
	 * @see IPersonne#persNomptr()
	 */
	public String persNomptr() {
		return llStructure();
	}

	/**
	 * @see IPersonne#persType()
	 */
	public String persType() {
		return IPersonne.PERS_TYPE_STR;
	}

	/**
	 * @return un nouveau cStructure
	 */
	public String construireCStructure() {
		return cstructureProvider.construireCStructure(editingContext());
	}

	/**
	 * @return un nouveau persId
	 */
	public Integer construirePersId() {
		return AFinder.construirePersId(editingContext());
	}

	/**
	 * Appele lors de l'insertion de l'objet dans l'editingcontext.
	 * 
	 * @see com.webobjects.eocontrol.EOCustomObject#awakeFromInsertion(com.webobjects.eocontrol.EOEditingContext)
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);

		setDCreation(getTimeStamp());
		// Pour les autres bases que Oracle, une exception sera générée car
		// cStructure est une string
		setCStructure(construireCStructure());
		setPersId(construirePersId());
		setTemValide(OUI);
		setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_A);
		EOTypeStructure typeStructure = EOTypeStructure.fetchByKeyValue(ec,
				EOTypeStructure.C_TYPE_STRUCTURE_KEY,
				EOTypeStructure.TYPE_STRUCTURE_A);
		setToTypeStructureRelationship(typeStructure);

	}

	/**
	 * Ajoute le parent comme structure p&egrave;re, duplique l'adresse principale du parent et cr&eacute;e la repartPersonneAdresse principale et les
	 * secr&eacute;tariats en utilisant ceux du parent.
	 */
	public void initAvecParent(EOStructure structurePere) {
		if (structurePere != null) {
			setCStructurePere(structurePere.cStructure());
			setToStructurePereRelationship(structurePere);
		}
	}

	/**
	 * Interdit la suppression pour les structures qui ne sont pas des feuilles
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		registerDetectedSpec();
		if (structuresFils().count() > 0) {
			String messErreur = "On ne peut supprimer que des structures sans fils";
			throw new NSValidation.ValidationException(messErreur);
		}
		super.validateForDelete();
	}

	/**
	 * @return true si la structure est une composante (C => Composante, CS => Composante Statutaire).
	 */
	public boolean isComposante() {
		return EOTypeStructure.TYPE_STRUCTURE_C.equals(cTypeStructure())
				|| EOTypeStructure.TYPE_STRUCTURE_CS.equals(cTypeStructure());
		// return "C".equals(cTypeStructure()) || "E".equals(cTypeStructure())
		// || "ES".equals(cTypeStructure()) ||
		// "CS".equals(cTypeStructure());
	}

	/**
	 * @return true si la structure est un service de l'universite. (grpCode=S). Une structure de type service doit aussi etre un groupe.
	 * @deprecated Utilisez {@link EOStructureForServiceSpec#isSpecificite(AfwkPersRecord)} a la place.
	 */
	@Deprecated
	public boolean isService() {
		return EOStructureForServiceSpec.sharedInstance().isSpecificite(this);
		// return toRepartTypeGroupes( new
		// EOKeyValueQualifier(EOTypeGroupe.TGRP_CODE_KEY,
		// EOQualifier.QualifierOperatorEqual,
		// EOTypeGroupe.TGRP_CODE_S) ).count()>0;
	}

	/**
	 * @return true si structure est un fournisseur (basé sur la relation toFournis()).
	 * @deprecated Utilisez {@link EOStructureForFournisseurSpec#isSpecificite(AfwkPersRecord)} a la place.
	 */
	@Deprecated
	public boolean isFournis() {
		return EOStructureForFournisseurSpec.sharedInstance().isSpecificite(
				this);
		// return toFournis()!=null;
	}

	/**
	 * @return true si la structure est un etablissement ou un etablissement secondaire.
	 */
	public boolean isEtablissement() {
		return (EOTypeStructure.TYPE_STRUCTURE_E.equals(cTypeStructure()) || EOTypeStructure.TYPE_STRUCTURE_ES
				.equals(cTypeStructure()));
	}

	/**
	 * @return La composante dans laquelle se trouve la structure.
	 */
	public EOStructure toComposante() {
		EOStructure composante = null;
		if (isComposante()) {
			composante = this;
		} else if (!isEtablissement()) {
			if (toStructurePere() != null
					&& toStructurePere().toComposante() != null) {
				composante = toStructurePere().toComposante();
			}
		}
		return composante;
	}
	
	/**
	 * @return La composante dans laquelle se trouve la structure. (Ressort l'établissement si non trouvé)
	 */
	public EOStructure toComposanteEtEtablissement() {
		EOStructure composante = null;
		if (isComposante()) {
			composante = this;
		} else if (!isEtablissement()) {
			if (toStructurePere() != null
					&& toStructurePere().toComposanteEtEtablissement() != null) {
				composante = toStructurePere().toComposanteEtEtablissement();
			}
		} else {
			composante = etablissement();
		}
		return composante;
	}

	// méthodes ajoutées
	@Override
	public String toString() {
		super.toString();
		return getNomPrenomAffichage() + getNumero();
	}

	/**
	 * @return Toutes les structures directement "fils" de la structure (sans filtre).
	 */
	public NSArray<EOStructure> structuresFils() {
		return structuresFils((EOQualifier) null);
	}

	/**
	 * @param qual Un qualifier permettant de filtrer les "fils".
	 * @return Les structures directement "fils" de cette structure
	 */
	public NSArray<EOStructure> structuresFils(EOQualifier qual) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOStructure.C_STRUCTURE_KEY + "<>%@", new NSArray(
						new Object[] {
								cStructure()
						})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOStructure.TO_STRUCTURE_PERE_KEY + "=%@", new NSArray(
						new Object[] {
								this
						})));
		if (qual != null) {
			quals.addObject(qual);
		}
		EOFetchSpecification fs = new EOFetchSpecification(
				EOStructure.ENTITY_NAME, new EOAndQualifier(quals), null);
		return editingContext().objectsWithFetchSpecification(fs);
	}

	/**
	 * @param tgrpCode Le type de groupe pour filtrer les structures "fils". Si null, les structures de n'importe quel type sont renvoyees.
	 * @return Les structures directement "fils" de cette structure.
	 */
	public NSArray<EOStructure> structuresFils(String tgrpCode) {
		EOQualifier qual = null;
		if (tgrpCode != null) {
			qual = EOQualifier.qualifierWithQualifierFormat(
					EOStructure.TO_REPART_TYPE_GROUPES_KEY + "."
							+ EORepartTypeGroupe.TGRP_CODE_KEY + "=%@",
					new NSArray(new Object[] {
							tgrpCode
					}));
		}
		return structuresFils(qual);
	}

	/**
	 * @param qual Un qualifier permettant de filtrer les "fils".
	 * @return Toutes les structures "fils" de cette structure (de maniere recursive).
	 */
	public NSArray<EOStructure> toutesStructuresFils(EOQualifier qual) {
		Enumeration fils = structuresFils(qual).objectEnumerator();
		NSMutableArray res = new NSMutableArray();
		while (fils.hasMoreElements()) {
			EOStructure object = (EOStructure) fils.nextElement();
			res.addObject(object);
			res.addObjectsFromArray(object.toutesStructuresFils(qual));
		}
		return res;
	}

	/**
	 * @param tgrpCode Le type de groupe pour filtrer les structures "fils". Si null, les structures de n'importe quel type sont renvoyees.
	 * @return Toutes les structures "fils" de cette structure (de maniere recursive).
	 */
	public NSArray<EOStructure> toutesStructuresFils(String tgrpCode) {
		NSMutableArray tousFils = new NSMutableArray();
		tousFils.addObjectsFromArray(structuresFils(tgrpCode));
		Enumeration e = tousFils.objectEnumerator();
		while (e.hasMoreElements()) {
			tousFils.addObjectsFromArray(((EOStructure) e.nextElement())
					.toutesStructuresFils(tgrpCode));
		}
		return tousFils;
	}

	//
	// public NSArray toutesStructuresFils() {
	// NSMutableArray tousFils = new NSMutableArray();
	//
	// NSMutableArray quals = new NSMutableArray();
	// quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.C_STRUCTURE_KEY
	// + "<>%@", new NSArray(new
	// Object[]{cStructure()})));
	// quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_STRUCTURE_PERE_KEY
	// + "=%@", new NSArray(new
	// Object[]{this})));
	// quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_REPART_TYPE_GROUPES_KEY+"."
	// +
	// EORepartTypeGroupe.TGRP_CODE_KEY + "=%@", new NSArray(new
	// Object[]{EORepartTypeGroupe.TRGP_CODE_G})));
	//
	//
	// EOQualifier qual =
	// EOQualifier.qualifierWithQualifierFormat("cStructure != %@ AND toStructurePere = %@ AND toRepartTypeGroupes.tgrpCode = 'G'",args);
	// EOFetchSpecification fs = new
	// EOFetchSpecification(EOStructure.ENTITY_NAME,qual,null);
	// NSArray fils = editingContext().objectsWithFetchSpecification(fs);
	// Enumeration e = fils.objectEnumerator();
	// while (e.hasMoreElements()) {
	// EOStructure structure = (EOStructure)e.nextElement();
	// tousFils.addObject(structure);
	// tousFils.addObjectsFromArray(structure.toutesStructuresFils());
	// }
	// return tousFils;
	// }

	/**
	 * retourne l'adresse valide principale d'une structure de type 'PRO', null si il n'y en a pas
	 */
	public EORepartPersonneAdresse adresseProfessionnelle() {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EORepartPersonneAdresse.QUAL_RPA_PRINCIPAL);
		quals.addObject(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE);
		quals.addObject(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_PRO);

		NSArray res = toRepartPersonneAdresses(new EOAndQualifier(quals), false);
		if (res.count() > 0) {
			return (EORepartPersonneAdresse) res.objectAtIndex(0);
		}
		return null;

		//
		// NSArray args = new NSArray(persId());
		// EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
		// "rpaPrincipal = '" + OUI + "' AND rpaValide ='" + OUI +
		// "' AND tadrCode = 'PRO' AND persId = %@", args);
		// EOFetchSpecification fs = new
		// EOFetchSpecification("RepartPersonneAdresse", qualifier, null);
		// NSMutableArray prefetches = new NSMutableArray("toAdresse");
		// fs.setPrefetchingRelationshipKeyPaths(prefetches);
		// NSArray reparts = editingContext().objectsWithFetchSpecification(fs);
		// try {
		// return ((EORepartPersonneAdresse)
		// reparts.objectAtIndex(0)).toAdresse();
		// } catch (Exception e) {
		// return null;
		// }
	}

	public int niveauStructure() {
		if (cTypeStructure().equals("E")) {
			return 1;
		} else {
			EOStructure etablissement = rechercherEtablissement(editingContext());
			if (etablissement != null) {
				if (cStructurePere() != null && isComposante()) {
					if (cStructurePere().equals(etablissement.cStructure())) {
						return 2;
					} else {
						return 3;
					}
				} else {
					return 0;
				}
			} else {
				return 0;
			}
		}
	}

	/** Retourne l'&eacute;tablissement associ&eacute; &agrave; cette structure */
	/** Ajout du groupe racine pour stopper l'itération */
	public EOStructure etablissement() {
		if (isEtablissement()) {
			return this;
		} else if (EOStructureForGroupeSpec.isGroupeRacine(this)) {
			return this;
		} else {
			return toStructurePere().etablissement();
		}
	}

	// interface RecordAvecLibelleEtCode
	public String code() {
		return lcStructure();
	}

	public String libelle() {
		return llStructure();
	}

	// METHODES STATIQUES
	public static EOStructure rechercherEtablissement(
			EOEditingContext editingContext) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOStructure.C_TYPE_STRUCTURE_KEY + "=%s", new NSArray(
						new Object[] {
								EOTypeStructure.TYPE_STRUCTURE_E
						}));
		EOFetchSpecification fs = new EOFetchSpecification(
				EOStructure.ENTITY_NAME, qual, null);
		try {
			return (EOStructure) editingContext.objectsWithFetchSpecification(
					fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param editingContext
	 * @return Les structures de type Etablissement ou Etablissement Secondaire
	 */
	public static NSArray<EOStructure> rechercherEtablissements(
			EOEditingContext editingContext) {
		return rechercherEtablissements(editingContext, null, new NSArray(
				SORT_LL_STRUCTURE_ASC));
	}

	/**
	 * @param editingContext
	 * @param qual
	 * @param sortOrderings
	 * @return Les structures de type Etablissement ou Etablissement Secondaire
	 */
	public static NSArray<EOStructure> rechercherEtablissements(
			EOEditingContext editingContext, EOQualifier qual,
			NSArray sortOrderings) {
		EOQualifier qual1 = new EOOrQualifier(new NSArray(new Object[] {
				QUAL_STRUCTURES_TYPE_ETABLISSEMENT,
				QUAL_STRUCTURES_TYPE_ETABLISSEMENT_SECONDAIRE
		}));
		if (qual != null) {
			qual1 = new EOAndQualifier(
					new NSArray(new Object[] {
							qual1, qual
					}));
		}
		return fetchAll(editingContext, qual1, sortOrderings);
	}

	/**
	 * @return la liste des structures des &eacute;tablissements class&eacute;es par ordre alpha croissant (renvoie les services et composantes)
	 */
	public static NSArray<EOStructure> rechercherServicesEtablissements(
			EOEditingContext editingContext) {
		return rechercherServicesEtablissements(editingContext, null);
	}

	/**
	 * @param editingContext edc
	 * @return la liste des structures des &eacute;tablissements class&eacute;es par ordre alpha croissant (renvoie les services)
	 */
	public static NSArray<EOStructure> rechercherServices(EOEditingContext editingContext) {
		return rechercherServices(editingContext, null);
	}
	
	/**
	 * @param editingContext edc
	 * @param qual qualifier supplémentaire à la recherche des services
	 * @return la liste des structures des &eacute;tablissements class&eacute;es par ordre alpha croissant (renvoie les services)
	 */
	public static NSArray<EOStructure> rechercherServices(EOEditingContext editingContext, EOQualifier qual) {
		return fetchAll(editingContext, ERXQ.and(QUAL_STRUCTURES_AS_SERVICES, qual));
	}
	
	/**
	 * @return la liste des structures des &eacute;tablissements class&eacute;es par ordre alpha croissant (renvoie les services et composantes)
	 */
	public static NSArray<EOStructure> rechercherServicesEtablissements(
			EOEditingContext editingContext, String nomLike) {
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		if (!ERXStringUtilities.stringIsNullOrEmpty(nomLike)) {
			quals.addObject(new EOKeyValueQualifier(
					EOStructure.LL_STRUCTURE_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, nomLike));
		}
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOStructure.C_TYPE_STRUCTURE_KEY + "=%s OR "
						+ EOStructure.C_TYPE_STRUCTURE_KEY + "=%s or "
						+ EOStructure.TO_REPART_TYPE_GROUPES_KEY + "."
						+ EORepartTypeGroupe.TGRP_CODE_KEY + "=%s",
				new NSArray(new Object[] {
						EOTypeStructure.TYPE_STRUCTURE_E,
						EOTypeStructure.TYPE_STRUCTURE_ES,
						EOTypeGroupe.TGRP_CODE_S
				})));
		return fetchAll(editingContext, ERXQ.and(quals), new NSArray(
				SORT_LL_STRUCTURE_ASC));
	}

	public static NSArray<EOStructure> rechercherStructuresAvecRne(
			EOEditingContext editingContext, String rne) {
		return fetchAll(editingContext, EOStructure.C_RNE_KEY, rne,
				new NSArray(SORT_LL_STRUCTURE_ASC));
	}

	public static NSArray<EOStructure> rechercherStructuresAvecSiret(
			EOEditingContext editingContext, String siret) {
		return fetchAll(editingContext, EOStructure.SIRET_KEY, siret,
				new NSArray(SORT_LL_STRUCTURE_ASC));
	}

	/**
	 * @param ec
	 * @param cStructure
	 * @return La composante d'une structure (en remontant le long de l'arbre). Si composante non trouvée, retourne null.
	 */
	public static EOStructure getComposante(EOEditingContext ec,
			String cStructure) {
		EOStructure structure = fetchRequiredByKeyValue(ec,
				EOStructure.C_STRUCTURE_KEY, cStructure);
		if (structure.isComposante()) {
			return structure;
		}
		if (structure.toStructurePere() == null
				|| structure.toStructurePere().equals(structure)) {
			return null;
		}
		return getComposante(ec, structure.toStructurePere().cStructure());
	}

	/**
	 * @param editingContext
	 * @param cStructure
	 * @return la structure li&eacute;e &agrave; un code structure ou null.
	 */
	public static EOStructure structurePourCode(
			EOEditingContext editingContext, String cStructure) {
		return fetchByKeyValue(editingContext, EOStructure.C_STRUCTURE_KEY,
				cStructure);
	}

	public static EOStructure structureWithPersId(EOEditingContext ec,
			Integer persId) {
		return fetchFirstByQualifier(ec, ERXQ.equals(PERS_ID_KEY, persId));
	}

	private boolean isSiretValide(String siret) {
		return ControleSiret.isSiretValide(siret);

	}

	private boolean isSirenValide(String siren) {
		return ControleSiret.isSirenValide(siren);

	}

	private static EOQualifier qualForSigleLike(String nom) {
		NSMutableArray array = new NSMutableArray();
		array.addObject(new EOKeyValueQualifier(EOStructure.LC_STRUCTURE_KEY,
				EOQualifier.QualifierOperatorLike, nom));
		EOQualifier qual = new EOOrQualifier(array);
		// LRLogger.verbose("EOStructure.qualForSigleLike() " + qual);
		return qual;
	}

	private static EOQualifier qualForNomLike(String nom) {
		NSMutableArray array = new NSMutableArray();
		array.addObject(new EOKeyValueQualifier(EOStructure.LL_STRUCTURE_KEY,
				EOQualifier.QualifierOperatorLike, nom));
		EOQualifier qual = new EOOrQualifier(array);
		// LRLogger.verbose("EOStructure.qualForNomLike() " + qual);
		return qual;
	}

	private static EOQualifier qualForSiretLike(String siret) {
		NSMutableArray array = new NSMutableArray();
		array.addObject(new EOKeyValueQualifier(EOStructure.SIRET_KEY,
				EOQualifier.QualifierOperatorLike, siret + ETOILE));
		array.addObject(new EOKeyValueQualifier(EOStructure.SIREN_KEY,
				EOQualifier.QualifierOperatorLike, siret + ETOILE));
		EOQualifier qual = new EOOrQualifier(array);
		// LRLogger.verbose("EOStructure.qualForSiretLike() " + qual);
		return qual;
	}

	/**
	 * @param myContext
	 * @param nom Seule une partie du nom peut etre fournie. Les mots sont analysés.
	 * @param siret Optionnel
	 * @param qualifierOptionnel
	 * @return Toutes les structures valides trouvees en fonction du nom et du siret. le résultat est classés par ordre de pertinence.
	 */
	public static NSArray<EOStructure> structuresByNameAndSiret(
			EOEditingContext myContext, String nom, String siret,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom) && MyStringCtrl.isEmpty(siret)) {
			return NSArray.EmptyArray;
		}

		NSMutableArray res = new NSMutableArray();
		if (!MyStringCtrl.isEmpty(siret) && MyStringCtrl.isDigits(siret)) {
			res.addObjectsFromArray(structuresWithSiretEquals(myContext,
					siret.trim(), qualifierOptionnel, fetchLimit));
		}
		String nomsInv = null;
		if (!MyStringCtrl.isEmpty(nom)) {
			nom = MyStringCtrl.chaineSansAccents(nom, "?").trim().toUpperCase();
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
			nomsInv = MyStringCtrl.inverseMots(nom).replaceAll(SPACE, ETOILE);
			nom = nom.replaceAll(SPACE, ETOILE);
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom
					+ ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv + ETOILE, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, ETOILE
					+ nom + ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						ETOILE + nomsInv + ETOILE, qualifierOptionnel,
						fetchLimit));
			}
		}

		AFinder.removeDuplicatesInNSArray(res);
		return res;
	}

	public static NSArray<EOStructure> structuresByNameAndSigleAndSiretAndFouCodeInGroupe(
			EOEditingContext myContext, String nom, String sigle, String siret,
			String fouCode, EOQualifier qualifierOptionnel, int fetchLimit,
			EOStructure groupe) {
		EOQualifier qual = ERXQ.and(qualifierOptionnel,
				EOStructure.QUAL_STRUCTURES_IN_GROUPE(myContext, groupe));
		NSArray res = structuresByNameAndSigleAndSiretAndFouCode(myContext,
				nom, sigle, siret, fouCode, qual, fetchLimit);
		return res;
	}

	public static NSArray<EOStructure> structuresExternesByNameAndSigleAndSiretAndFouCode(
			EOEditingContext myContext, String nom, String sigle, String siret,
			String fouCode, EOQualifier qualifierOptionnel, int fetchLimit) {
		EOQualifier qual = ERXQ.and(qualifierOptionnel,
				EOStructure.QUAL_STRUCTURES_EXTERNES(myContext));
		NSArray res = structuresByNameAndSigleAndSiretAndFouCode(myContext,
				nom, sigle, siret, fouCode, qual, fetchLimit);

		// si aucun resultat, on recherche plus large (structure qui sont dans
		// le groupe par defaut)
		if (res.count() == 0) {
			res = structuresByNameAndSigleAndSiretAndFouCodeInGroupe(myContext,
					nom, sigle, siret, fouCode, qualifierOptionnel, fetchLimit,
					EOStructureForGroupeSpec.getGroupeDefaut(myContext));
		}

		return res;
	}

	public static NSArray<EOStructure> structuresInternesByNameAndSigleAndSiretAndFouCode(
			EOEditingContext myContext, String nom, String sigle, String siret,
			String fouCode, EOQualifier qualifierOptionnel, int fetchLimit) {
		EOQualifier qual = ERXQ.and(qualifierOptionnel,
				EOStructure.QUAL_STRUCTURES_INTERNES);
		return structuresByNameAndSigleAndSiretAndFouCode(myContext, nom,
				sigle, siret, fouCode, qual, fetchLimit);
	}

	public static NSArray<EOStructure> structuresByNameAndSigleAndSiretAndFouCode(
			EOEditingContext myContext, String nom, String sigle, String siret,
			String fouCode, EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom) && MyStringCtrl.isEmpty(siret)
				&& MyStringCtrl.isEmpty(sigle) && MyStringCtrl.isEmpty(fouCode)) {
			return NSArray.EmptyArray;
		}
		// LRLogger.verbose(qualifierOptionnel);

		NSMutableArray res = new NSMutableArray();
		if (!MyStringCtrl.isEmpty(siret) && MyStringCtrl.isDigits(siret)) {
			res.addObjectsFromArray(structuresWithSiretEquals(myContext,
					siret.trim(), qualifierOptionnel, fetchLimit));
		}
		String nomsInv = null;
		if (!MyStringCtrl.isEmpty(fouCode)) {
			res.addObjectsFromArray(EOStructureForFournisseurSpec
					.fetchStructuresForFouCode(myContext, fouCode,
							qualifierOptionnel));
		}

		if (sigle != null) {
			sigle = MyStringCtrl.chaineSansAccents(sigle, "?").trim()
					.toUpperCase();
		}

		if (!MyStringCtrl.isEmpty(nom)) {
			nom = MyStringCtrl.chaineSansAccents(nom, "?").trim().toUpperCase();
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
		}
		
		if (!MyStringCtrl.isEmpty(nom)) {
			res.addObjectsFromArray(structuresWithSigleLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
			res.addObjectsFromArray(structuresWithSigleLike(myContext, nom
					+ ETOILE, qualifierOptionnel, fetchLimit));
		}

		if (!MyStringCtrl.isEmpty(sigle)) {
			res.addObjectsFromArray(structuresWithSigleLike(myContext, sigle,
					qualifierOptionnel, fetchLimit));
			res.addObjectsFromArray(structuresWithSigleLike(myContext, sigle
					+ ETOILE, qualifierOptionnel, fetchLimit));
		}

		if (!MyStringCtrl.isEmpty(nom)) {
			nomsInv = MyStringCtrl.inverseMots(nom).replaceAll(SPACE, ETOILE);
			nom = nom.replaceAll(SPACE, ETOILE);
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom
					+ ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv + ETOILE, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, ETOILE
					+ nom + ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						ETOILE + nomsInv + ETOILE, qualifierOptionnel,
						fetchLimit));
			}
		}

		AFinder.removeDuplicatesInNSArray(res);
		return res;
	}

	/**
	 * @param myContext
	 * @param nom
	 * @param siret
	 * @param qualifierOptionnel
	 * @param fetchLimit
	 * @return La meme chose que {@link EOStructure#structuresByNameAndSiret(EOEditingContext, String, String, EOQualifier, int)} mais en filtrant sur
	 *         les structures internes.
	 */
	public static NSArray<EOStructure> structuresInternesByNameAndSiret(
			EOEditingContext myContext, String nom, String siret,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		EOQualifier qual = (qualifierOptionnel == null ? EOStructure.QUAL_STRUCTURES_INTERNES
				: new EOAndQualifier(new NSArray(new Object[] {
						qualifierOptionnel,
						EOStructure.QUAL_STRUCTURES_INTERNES
				})));
		return structuresByNameAndSiret(myContext, nom, siret, qual, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 * @param siret
	 * @param qualifierOptionnel
	 * @param fetchLimit
	 * @return La meme chose que {@link EOStructure#structuresByNameAndSiret(EOEditingContext, String, String, EOQualifier, int)} mais en filtrant sur
	 *         les structures externes.
	 */
	public static NSArray<EOStructure> structuresExternesByNameAndSiret(
			EOEditingContext myContext, String nom, String siret,
			EOQualifier qualifierOptionnel, int fetchLimit) {

		EOQualifier qual = (qualifierOptionnel == null ? EOStructure
				.QUAL_STRUCTURES_EXTERNES(myContext) : new EOAndQualifier(
				new NSArray(new Object[] {
						qualifierOptionnel,
						EOStructure.QUAL_STRUCTURES_EXTERNES(myContext)
				})));
		return structuresByNameAndSiret(myContext, nom, siret, qual, fetchLimit);
	}

	/**
	 * @param myContext
	 * @param nom
	 * @param qualifierOptionnel
	 * @return Les structures valides
	 */
	protected static NSArray structuresWithNameLike(EOEditingContext myContext,
			String nom, EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom)) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(nom)) {
			quals.addObject(qualForNomLike(nom));
		}

		quals.addObject(QUAL_STRUCTURES_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}

		final EOQualifier qual = new EOAndQualifier(quals);
		// LRLogger.verbose(qual);

		EOFetchSpecification spec = new EOFetchSpecification(
				EOStructure.ENTITY_NAME, qual, new NSArray(
						new Object[] {
								SORT_LL_STRUCTURE_ASC
						}));
		spec.setUsesDistinct(true);
		spec.setFetchLimit(fetchLimit);
		spec.setRefreshesRefetchedObjects(true);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	protected static NSArray structuresWithSigleLike(
			EOEditingContext myContext, String sigle,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(sigle)) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();

		if (!MyStringCtrl.isEmpty(sigle)) {
			quals.addObject(qualForSigleLike(sigle));
		}

		quals.addObject(QUAL_STRUCTURES_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}

		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(
				EOStructure.ENTITY_NAME, qual, new NSArray(
						new Object[] {
								SORT_LL_STRUCTURE_ASC
						}));
		spec.setFetchLimit(fetchLimit);
		spec.setRefreshesRefetchedObjects(true);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	protected static NSArray structuresWithSiretEquals(
			EOEditingContext myContext, String siret,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		NSMutableArray quals = new NSMutableArray();

		if (siret != null && siret.trim().length() > 0) {
			quals.addObject(qualForSiretLike(siret.trim()));
		} else {
			return new NSArray();
		}

		quals.addObject(QUAL_STRUCTURES_VALIDE);
		if (qualifierOptionnel != null) {
			quals.addObject(qualifierOptionnel);
		}

		final EOQualifier qual = new EOAndQualifier(quals);
		EOFetchSpecification spec = new EOFetchSpecification(
				EOStructure.ENTITY_NAME, qual, new NSArray(
						new Object[] {
								SORT_LL_STRUCTURE_ASC
						}));
		spec.setUsesDistinct(true);
		spec.setFetchLimit(fetchLimit);
		spec.setRefreshesRefetchedObjects(true);
		NSArray disp = myContext.objectsWithFetchSpecification(spec);
		return disp;
	}

	/**
	 * @param ec
	 * @param qual
	 * @return Les structures valides de type groupe dependant du qualifier specifie.
	 * @deprecated Utilisez {@link EOStructureForGroupeSpec#rechercherGroupes(EOEditingContext, EOQualifier, int)} a la place.
	 */
	@Deprecated
	public static NSArray rechercherGroupes(EOEditingContext ec,
			EOQualifier qual, int fetchLimit) {
		return EOStructureForGroupeSpec.rechercherGroupes(ec, qual, fetchLimit, true);
	}

	// /**
	// * Ajoute une adresse et l'attache a la structure.
	// *
	// * @param ed
	// * @param structureUlr
	// * @param adresse
	// * @param typeAdresse
	// * @return
	// */
	// public EORepartPersonneAdresse ajouterAdresse(EOEditingContext ed,
	// EOAdresse adresse, EOTypeAdresse typeAdresse) {
	// EORepartPersonneAdresse repartPersonneAdresse =
	// EORepartPersonneAdresse.creerForPersonne(ed, this, typeAdresse, adresse);
	// return repartPersonneAdresse;
	// }

	/**
	 * @return Le fournisseur trouvé s'il existe (d'abord le valide sinon celui en instance, sinon celui annnulé).
	 */
	public IFournis toFournis() {
		if (toFourniss() == null || toFourniss().count() == 0) {
			return null;
		} else {
			NSArray res = EOQualifier.filteredArrayWithQualifier(toFourniss(),
					EOFournis.QUAL_FOU_VALIDE_OUI);
			if (res.count() > 0) {
				return (EOFournis) res.objectAtIndex(0);
			}
			NSArray res2 = EOQualifier.filteredArrayWithQualifier(toFourniss(),
					EOFournis.QUAL_FOU_VALIDE_INSTANCE);
			if (res2.count() > 0) {
				return (EOFournis) res2.objectAtIndex(0);
			}
			NSArray res3 = EOQualifier.filteredArrayWithQualifier(toFourniss(),
					EOFournis.QUAL_FOU_VALIDE_ANNULE);
			if (res3.count() > 0) {
				return (EOFournis) res3.objectAtIndex(0);
			}

			return null;
		}
	}

	//
	// public static EOStructure creerInstance(EOEditingContext ec) {
	// EOStructure object = (EOStructure) AUtils.instanceForEntity(ec,
	// EOStructure.ENTITY_NAME);
	// ec.insertObject(object);
	// object.onCreerInstance();
	// return object;
	// }

	/**
	 * Le nom est convertie en majuscules sans accents.
	 */
	@Override
	public void setLcStructure(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setLcStructure(value);
	}

	/**
	 * Le nom est converti en majuscules sans accents.<br/>
	 * Utilisez plutot {@link #setStrAffichage(String)}
	 */
	@Override
	public void setLlStructure(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setLlStructure(value);
	}

	public String getNumero() {
		return cStructure();
	}

	public Integer getNumeroInt() {
		return Integer.valueOf(cStructure());
	}

	public String getNomPrenomAffichage() {
		return persLibelleAffichage();
	}

	/**
	 * @param ec
	 * @param groupeKey
	 * @return la structure trouvée.
	 * @throws Exception
	 * @deprecated Utilisez {@link EOStructureForGroupeSpec#getStructureForGroupe(EOEditingContext, String)} à la place.
	 */
	@Deprecated
	public static EOStructure getStructureForGroupe(EOEditingContext ec,
			String groupeKey) throws Exception {
		return EOStructureForGroupeSpec.getStructureForGroupe(ec, groupeKey);
	}

	/**
	 * Affecte le libellé long de la structure (appelle {@link EOStructure#setLlStructure(String)}.
	 */
	@Override
	public void setStrAffichage(String value) {
		super.setStrAffichage(value);
		setLlStructure(value);
	}

	@Override
	public void setGrpAlias(String value) {
		if (value == null || !value.equalsIgnoreCase(grpAlias())) {
			// Recuperation de tous les alias secondaires correspondants au
			// grpAlias
			NSArray<EOPersonneAlias> aliases = EOPersonneAlias.fetchAll(
					editingContext(), EOPersonneAlias.ALIAS.eq(grpAlias()));
			super.setGrpAlias(value);
			for (EOPersonneAlias alias : aliases) {
				IPersonne personneAncienAlias = alias.toPersonne();
				// Suppression de l'alias car impossible de modifier la cle
				// primaire
				personneAncienAlias
						.removeFromToPersonneAliasesRelationship(alias);
				editingContext().deleteObject(alias);
				// Creation du nouvel alias avec les donnees de l'ancien si la
				// nouvelle valeur n'est pas nulle
				if (value != null) {
					EOPersonneAlias nouvelAlias = EOPersonneAlias
							.creerInstance(editingContext());
					nouvelAlias.setAlias(value);
					nouvelAlias.setPersId(personneAncienAlias.persId());
					personneAncienAlias
							.addToToPersonneAliasesRelationship(nouvelAlias);
				}
			}
		}
	}

	/**
	 * Verifie la coherence des noms.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkNoms() throws NSValidation.ValidationException {
		if (!llStructure().equals(
				MyStringCtrl.chaineSansAccents(strAffichage()).toUpperCase())) {
			String messErreur = "Le libellé de la structure doit être égal au nom d'affichage (en majuscules, sans accent).";
			throw new NSValidation.ValidationException(messErreur);
		}
	}

	/**
	 * Renvoie le libellé d'affichage de la structure. Si le libellé d'affichage est vide, renvoie llStructure().
	 */
	@Override
	public String strAffichage() {
		return (MyStringCtrl.isEmpty(super.strAffichage()) ? llStructure()
				: super.strAffichage());
	}

	public String persLcAffichage() {
		return null;
	}

	/**
	 * Renvoie strAffichage + lcStructure s'ils sont différents.
	 */
	public String persLibelleAffichage() {
		return strAffichage()
				+ (strAffichage() != null && lcStructure() != null
						&& !strAffichage().toUpperCase().equals(lcStructure()) ? " ("
						+ lcStructure() + ")"
						: "");
	}

	public String persNomptrAffichage() {
		return strAffichage();
	}

	public String getNomCompletAffichage() {
		return persLibelleAffichage();
	}

	public String getNomPrenomRecherche() {
		return persLibelleAffichage();
	}

	/**
	 * Affecte le n° de SIRET. Si le n° de SIREN est vide et que value correspond à un SIRET valide, le SIREN est rempli avec les 9 premiers
	 * caracteres de value..
	 */
	@Override
	public void setSiret(String value) {
		setSiren(value);
		super.setSiret(value);
	}

	@Override
	public void setSiren(String value) {
		if (value != null) {
			if (value.length() > 9) {
				value = value.substring(0, 9);
			}
		}
		super.setSiren(value);
	}

	@Override
	public NSArray toComptes(EOQualifier qualifier) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(super
				.toComptes(qualifier),
				new NSArray(EOCompte.SORT_VLANS_PRIORITE));
	}

	public NSArray<EOIndividu> toSecretariatsAsIndividus() {
		NSMutableArray<EOIndividu> res = new NSMutableArray<EOIndividu>();
		NSArray secrs = toSecretariats();
		if (secrs == null) {
			secrs = NSArray.EmptyArray;
		}
		for (int i = 0; i < secrs.count(); i++) {
			res.addObject(((EOSecretariat) secrs.objectAtIndex(i)).toIndividu());
		}
		return res.immutableClone();
	}

	/**
	 * @param qualifier Qualifier à appliquer sur les objets EORepartAssociation
	 * @param sortOrderings tableau d'EOSortOrdering pour trier le resultat
	 */

	/*
	 * Redefini dans le modele directement
	 * 
	 * public NSArray toRepartAssociationsElts(EOQualifier qualifier, NSArray sortOrderings) { NSArray results; NSArray<EORepartStructure> res1 =
	 * toRepartStructuresElts(); NSMutableArray array = new NSMutableArray(); for (EORepartStructure array_element : res1) {
	 * array.addObjectsFromArray(array_element.toRepartAssociations(qualifier)); } results = array.immutableClone(); if (sortOrderings != null) {
	 * results = (NSArray) EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings); } return results; }
	 * 
	 * public NSArray toRepartAssociationsElts(EOQualifier qualifier) { return toRepartAssociationsElts(qualifier, null); }
	 * 
	 * public NSArray toRepartAssociationsElts() { return toRepartAssociationsElts(null, null); }
	 */

	/**
	 * @param qualifier Qualifier à appliquer sur les objets EORepartAssociation
	 * @param sortOrderings tableau d'EOSortOrdering pour trier le resultat
	 */

	/*
	 * Redefini dans le modele directement
	 * 
	 * public NSArray toRepartAssociations(EOQualifier qualifier, NSArray sortOrderings) { EOQualifier _qualifier =
	 * EORepartAssociation.PERS_ID.eq(persId()).and(qualifier); NSArray results = EORepartAssociation.fetchAll(editingContext(), _qualifier,
	 * sortOrderings); return results; }
	 * 
	 * public NSArray toRepartAssociations(EOQualifier qualifier) { return toRepartAssociations(qualifier, null); }
	 * 
	 * public NSArray toRepartAssociations() { return toRepartAssociations(null, null); }
	 */
	public void setPersonneDelegate(PersonneDelegate personneDelegate) {
		this.personneDelegate = personneDelegate;
	}

	public PersonneDelegate getPersonneDelegate() {
		return personneDelegate;
	}

	/**
	 * @param appUser
	 * @return Les groupes affectés à l'individu en fonction des droits de l'utilisateur.
	 */
	public NSArray getGroupesAffectes(PersonneApplicationUser appUser) {
		return personneDelegate.getGroupesAffectes(appUser, null);
	}

	public NSArray getGroupesAffectes(Integer persIdForUser) {
		return personneDelegate.getGroupesAffectes(persIdForUser, null);
	}

	/**
	 * Le libellé a afficher dans le cas d'un groupe (llStructure + indique si le groupe est privé)
	 */
	public String libelleForGroupe() {
		return llStructure()
				+ (EOStructureForGroupeSpec.GRP_ACCES_PRIVE.equals(grpAcces()) ? " (privé)"
						: "");
	}

	public NSArray getRepartStructuresAffectes(PersonneApplicationUser appUser) {
		return personneDelegate.getRepartStructuresAffectes(appUser, null);
	}

	public NSArray getRepartStructuresAffectes(Integer persIdForUser) {
		return personneDelegate
				.getRepartStructuresAffectes(persIdForUser, null);
	}

	public NSArray getRepartStructuresAffectes(PersonneApplicationUser appUser,
			EOQualifier qualifierForGroupe) {
		return personneDelegate.getRepartStructuresAffectes(appUser,
				qualifierForGroupe);
	}

	public boolean isStructure() {
		return true;
	}

	public boolean isIndividu() {
		return false;
	}

	/**
	 * Structures qui sont externes a l'établissement. (Etablissements externes ou groupes de type entreprise ou structures fournisseurs externes -
	 * cad structure dans le groupe des fournisseurs valides externes )
	 */
	public static final EOQualifier QUAL_STRUCTURES_EXTERNES(EOEditingContext ec) {
		return new EOOrQualifier(
				new NSArray(
						new Object[] {
								QUAL_STRUCTURES_ETABLISSEMENTS_EXTERNES,
								EOStructureForGroupeSpec.QUAL_GROUPE_ENTREPRISES,
								EOStructureForFournisseurSpec.QUAL_STRUCTURES_IN_GROUPE_TYPE_FOURNISSEUR,
								EOStructureForPartenaireSpec.QUAL_STRUCTURES_IN_GROUPE_TYPE_PARTENAIRE

						}));
		// return new EOOrQualifier(new NSArray(new Object[] {
		// QUAL_STRUCTURES_ETABLISSEMENTS_EXTERNES,
		// EOStructureForGroupeSpec.QUAL_GROUPE_ENTREPRISES,
		// EOStructureForFournisseurSpec.QUAL_STRUCTURES_IN_GROUPE_FOURNISSEUR_VALIDE_EXTERNE(ec),
		// EOStructureForFournisseurSpec.QUAL_STRUCTURES_IN_GROUPE_FOURNISSEUR_VALIDE_MORALE(ec)
		//
		// }));

		// return new EOAndQualifier(new NSArray(new Object[] {
		// new EONotQualifier(QUAL_STRUCTURES_INTERNES), new
		// EONotQualifier(QUAL_STRUCTURES_FORUMS)
		// }));

	}

	public static final EOQualifier QUAL_STRUCTURES_IN_GROUPE(
			EOEditingContext ec, EOStructure groupe) {
		return new EOKeyValueQualifier(EOStructure.TO_REPART_STRUCTURES_KEY
				+ "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY,
				EOQualifier.QualifierOperatorEqual, groupe);
	}

	public EORepartAssociation definitUnRole(EOEditingContext ec,
			EOAssociation association, EOStructure groupe,
			Integer persIdUtilisateur, NSTimestamp rasDOuverture,
			NSTimestamp rasDFermeture, String rasCommentaire,
			BigDecimal rasQuotite, Integer rasRang) {
		return personneDelegate.definitUnRole(ec, association, groupe,
				persIdUtilisateur, rasDOuverture, rasDFermeture,
				rasCommentaire, rasQuotite, rasRang);
	}

	public EORepartAssociation definitUnRole(EOEditingContext ec,
			EOAssociation association, EOStructure groupe,
			Integer persIdUtilisateur, NSTimestamp rasDOuverture,
			NSTimestamp rasDFermeture, String rasCommentaire,
			BigDecimal rasQuotite, Integer rasRang,
			boolean autoriserGroupeReserve) {
		return personneDelegate.definitUnRole(ec, association, groupe,
				persIdUtilisateur, rasDOuverture, rasDFermeture,
				rasCommentaire, rasQuotite, rasRang, autoriserGroupeReserve);
	}

	public NSArray definitLesRoles(EOEditingContext ec, NSArray associations,
			EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture,
			String rasCommentaire, BigDecimal rasQuotite, Integer rasRang,
			boolean autoriserGroupeReserve) {
		return personneDelegate.definitLesRoles(ec, associations, groupe,
				persIdUtilisateur, rasDOuverture, rasDFermeture,
				rasCommentaire, rasQuotite, rasRang, autoriserGroupeReserve);
	}

	public NSArray definitLesRoles(EOEditingContext ec, NSArray associations,
			EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture, NSTimestamp rasDFermeture,
			String rasCommentaire, BigDecimal rasQuotite, Integer rasRang) {
		return personneDelegate.definitLesRoles(ec, associations, groupe,
				persIdUtilisateur, rasDOuverture, rasDFermeture,
				rasCommentaire, rasQuotite, rasRang);
	}

	@Override
	public void setToResponsableRelationship(EOIndividu value) {
		super.setToResponsableRelationship(value);
		if (value == null) {
			setGrpResponsable(null);
		} else {
			setGrpResponsable(value.noIndividu());
		}

	}

	@Override
	public void setToNafRelationship(EONaf value) {
		super.setToNafRelationship(value);
		if (value == null) {
			setCNaf(null);
		} else {
			setCNaf(value.cNaf());
		}
	}

	@Override
	public void setToFormesJuridiquesDetailsRelationship(
			EOFormesJuridiquesDetails value) {
		super.setToFormesJuridiquesDetailsRelationship(value);
		if (value == null) {
			setGrpFormeJuridique(null);
		} else {
			setGrpFormeJuridique(value.cFjd());
		}
	}

	public NSArray getRepartAssociationsInGroupe(EOStructure groupe,
			EOQualifier qualifier) {
		return getPersonneDelegate().getRepartAssociationsInGroupe(groupe,
				qualifier);
	}

	public void supprimerAffectationAUnGroupe(EOEditingContext ec,
			Integer persIdUtilisateur, EORepartStructure repartStructure) {
		getPersonneDelegate().supprimerAffectationAUnGroupe(ec,
				persIdUtilisateur, repartStructure);
	}

	public void supprimerAffectationAUnGroupe(EOEditingContext editingContext,
			Integer persIdUtilisateur, EOStructure groupe) {
		getPersonneDelegate().supprimerAffectationAUnGroupe(editingContext,
				persIdUtilisateur, groupe);
	}

	public void supprimerAffectationATousLesGroupes(
			EOEditingContext editingContext, Integer persIdUtilisateur) {
		getPersonneDelegate().supprimerAffectationATousLesGroupes(
				editingContext, persIdUtilisateur);
	}

	/**
	 * Ne pas utiliser cette methode directement, utiliser
	 * {@link PersonneDelegate#supprimerAffectationAUnGroupe(EOEditingContext, Integer, EORepartStructure)} a la place.
	 */
	@Override
	public void removeFromToRepartStructuresEltsRelationship(
			EORepartStructure object) {
		super.removeFromToRepartStructuresEltsRelationship(object);
	}

	public void supprimerLesRoles(EOEditingContext ec,
			EOAssociation association, EOStructure groupe,
			Integer persIdUtilisateur) {
		getPersonneDelegate().supprimerLesRoles(ec, association, groupe,
				persIdUtilisateur);

	}

	public void supprimerUnRole(EOEditingContext ec, EOAssociation association,
			EOStructure groupe, Integer persIdUtilisateur,
			NSTimestamp rasDOuverture) {
		getPersonneDelegate().supprimerUnRole(ec, association, groupe,
				persIdUtilisateur, rasDOuverture);
	}

	/**
	 * @return true si la structure est une structure interne de l'etablissement (cad etablissement, composante ou service).
	 */
	public Boolean isInterneEtablissement() {
		return (isEtablissement()
				|| isComposante()
				|| EOStructureForServiceSpec.sharedInstance().isSpecificite(
						this) || EOStructureForFournisseurSpec
					.isStructureFournisseurInterne(this));
	}

	public EOAdresse getAdressePrincipale() {
		return getPersonneDelegate().getAdressePrincipale(editingContext());
	}

	public String getNomEtId() {
		return libelleEtId();
	}

	public void setPersIdModification(Integer value) {
		super.setPersIdModification(value);
		if (persIdCreation() == null) {
			setPersIdCreation(value);
		}
	}

	/**
	 * Archive la structure courrante en positionnant la structure archive comme son père. La date de fermeture devient la date actuelle d'archivage
	 * si la structure ne possède pas déjà de date de fermeture, ou si la date de fermeture de la structure est postèrieure à la date actuelle. Les
	 * structures filles de cette structure sont aussi fermées La structure archive(l'orphelinat) est paramètrable grace au paramètre
	 * {@link EOGrhumParametres#PARAM_ANNUAIRE_ARCHIVES}. ROD 17/06/2014 : il ne faut pas supprimer les responsables et secrétaires du groupe lors de
	 * l'archivage
	 * 
	 * @throws Exception si le paramètre GRHUM: ANNUAIRE_ARCHIVES n'est pas defini ou si la structure d'archivage correspondante n'est pas trouvée.
	 */
	public void archiver() throws Exception {
		EOStructure orphelinat = EOStructureForGroupeSpec
				.getStructurePourArchivage(this.editingContext());
		logger.info("L'annuaire des archives est positionné sur le groupe avec le C_STRUCTURE = " + orphelinat.cStructure());
		if (orphelinat != null) {
			try {
				logger.info("L'ancien parent est : " + this.toStructurePere().llStructure());
				setToStructurePereRelationship(orphelinat);
				logger.info("On vient de mettre le groupe ARCHIVES comme nouveau parent : " + this.toStructurePere().llStructure());

				logger.info("Le témoin de validité est avant changement : " + this.temValide());
				setTemValide(TEM_VALIDE_NON);
				logger.info("Le témoin de validité est après changement : " + this.temValide());

				NSTimestamp timeStamp = getTimeStamp();

				setDModification(timeStamp);
				logger.info("Modification effectuée de la date de modification");
				fermerStructure(timeStamp);
				logger.info("La méthode de fermeture de la structure vient d'être utilisée");
			} catch (Exception e) {
				throw new NSValidation.ValidationException("L'opération d'archivage ne s'est pas déroulée correctement!");
			}
		} else {
			throw new NSValidation.ValidationException("Le paramètre ANNUAIRE_ARCHIVES est soit non renseigné soit renseigné avec une valeur erronée pour le c_structure du groupe ARCHIVES");
		}
	}

	protected NSTimestamp getTimeStamp() {
		return MyDateCtrl.now();
	}

	/**
	 * Ensemble des traitements à effectuer lors de la fermeture d'une structure. Cette methode est appelée lors de "l'archivage" d'une structure. La
	 * fermeture d'une structure implique la fermeture de ses structures filles de façon reccursive.
	 * 
	 * @param dateFermeture date prise en compte pour la fermeture
	 */
	protected void fermerStructure(NSTimestamp dateFermeture) {
		if (dateFermeture() == null
				|| MyDateCtrl.isAfterEq(dateFermeture(), dateFermeture)) {
			setDateFermeture(dateFermeture);
			setDModification(dateFermeture);
		}
		if (!structuresFils().isEmpty()) { // sinon passer directement à la
											// requete recursive
											// toutesStructuresFils()...
			for (int i = 0; i < structuresFils().count(); i++) {
				EOStructure structureFille = (EOStructure) structuresFils()
						.objectAtIndex(i);
				if (!structureFille.equals(this)) {
					structureFille.fermerStructure(dateFermeture);
				}
			}
		}
	}

	/**
	 * Traitements propres au fait de retirer une structure de l'orphelinat (desArchivage). La structure donnée en argument, sera positionnée comme
	 * père de la structure actuelle. La date d'ouverture devient la date actuelle. La date de fermeture est suprimée. Les structures filles sont
	 * aussi ouvertes récursivement. La structure archive(l'orphelinat) est paramètrable grace au paramètre
	 * {@link EOGrhumParametres#PARAM_ANNUAIRE_ARCHIVES}.
	 * 
	 * @param nouvelleStructurePere : structure donnée qui sera positionnée comme père de la structure actuelle.
	 */
	public void desarchiverVersPere(EOStructure nouvelleStructurePere) {
		if (nouvelleStructurePere != null) {
			setToStructurePereRelationship(nouvelleStructurePere);
			logger.info("La nouvelle structure parent est : " + this.toStructurePere().getNomCompletAffichage());

			// Par defaut reprendre le responsable du groupe pere
			if (nouvelleStructurePere.toResponsable() != null) {
				logger.info("Début de la récupération du responsable du groupe du parent");
				setToResponsableRelationship(nouvelleStructurePere.toResponsable().localInstanceIn(editingContext()));
				logger.info("Fin de la récupération du responsable du groupe du parent et parent : " + this.toResponsable().getNomCompletAffichage());
			}

			NSTimestamp timeStamp = getTimeStamp();
			// Par defaut reprendre les secretariats du groupe pere
			if (nouvelleStructurePere.toSecretariats() != null && nouvelleStructurePere.toSecretariats().count() > 0) {
				logger.info("Début de la récupération des secrétariats du parent");

				for (Object secretariat : nouvelleStructurePere.toSecretariats()) {
					// le Cstructure dans secretatriat fait partie de la PK => donc clonnage des secrétariats
					EOSecretariat newSecretariat = EOSecretariat.creerInstance(editingContext());
					newSecretariat.setToIndividuRelationship(((EOSecretariat) secretariat).toIndividu().localInstanceIn(editingContext()));
					newSecretariat.setDCreation(timeStamp);
					newSecretariat.setDModification(timeStamp);
					newSecretariat.setCStructure(cStructure());
					addToToSecretariatsRelationship(newSecretariat);
				}

				logger.info("Fin de la récupération des secrétariats du parent");
			}

			logger.info("Le témoin de validité est avant changement : " + this.temValide());
			setTemValide(TEM_VALIDE_OUI);
			logger.info("Le témoin de validité est après changement : " + this.temValide());

			setDModification(timeStamp);
			logger.info("Modification effectuée de la date de modification");
			ouvrirStructure(timeStamp);
			logger.info("La méthode d'ouverture de la structure vient d'être utilisée");
		}
	}

	/**
	 * Ensemble des traitements à effectuer lors de l'ouverture d'une structure. Cette methode est appelée lors du "désarchivage" d'une structure.
	 * L'ouverture d'une structure implique l'ouverture de ses structures filles de façon reccursive.
	 * 
	 * @param dateOuverture date prise en compte pour l'ouverture
	 */
	protected void ouvrirStructure(NSTimestamp dateOuverture) {
		setDModification(dateOuverture);
		setDateOuverture(dateOuverture);
		setDateFermeture(null);
		if (!structuresFils().isEmpty()) { // sinon passer directement à la
											// requete recursive
											// toutesStructuresFils()...
			for (int i = 0; i < structuresFils().count(); i++) {
				EOStructure structureFille = (EOStructure) structuresFils()
						.objectAtIndex(i);
				if (!structureFille.equals(this)) {
					structureFille.ouvrirStructure(dateOuverture);
				}
			}
		}
	}

	/**
	 * Renvoie l'état d'archivage de cette structure. Une structure est considérée comme archivée, si elle ou l'un de ses pères est placé dans
	 * l'orphelinat (le groupe archive) ET si sa date de fermeture est antérieure à la date courante.
	 * 
	 * @return Vrai si cette structure se trouve dans l'orphelinat ET qu'elle est fermée. Faux sinon.
	 */
	public boolean isArchivee() {
		return estDansOrphelinat() && isFermee();
	}
	
	/**
	 * Renvoie vrai si cette structure ou l'un de ses pères est fils du groupe correspondant à l'orphelinat.
	 * 
	 * @return Vrai si cette structure se trouve dans l'orphelinat. Faux sinon.
	 */
	public boolean estDansOrphelinat() {
		EOStructure orphelinat;
		try {
			orphelinat = EOStructureForGroupeSpec
					.getStructurePourArchivage(this.editingContext());
		} catch (Exception e) {
			logger.error("Une erreur est survenue lors de la recherche du groupe orphelinat pour la structure " + this, e);
			return false;
		}
		if (toStructurePere() == null) {
			return false;
		}
		return toStructurePere().equals(orphelinat)
				|| (!toStructurePere().equals(this) && toStructurePere()
						.estDansOrphelinat());
	}

	/**
	 * Une structure est fermée si sa date de fermeture est antérieure à la date courante.
	 * 
	 * @return Vrai si cette structure est fermée
	 */
	public boolean isFermee() {
		return dateFermeture() != null
				&& MyDateCtrl.isBeforeEq(dateFermeture(), getTimeStamp());
	}

	private void checkArchivage() {
		if (isArchivee()
				&& (EOStructureForGroupeSpec.isGroupeRacine(this)
						|| EOStructureForGroupeSpec.isGroupeArchive(this) || EOStructureForGroupeSpec
							.isGroupeGereEnAuto(this))) {
			String messErreur = "Une structure 'archive', 'racine' ou d'application ne peut être archivée.";
			throw new NSValidation.ValidationException(messErreur);
		}
	}

	/**
	 * @return la liste des alias de messagerie du groupe
	 */
	public NSArray aliases() {
		NSMutableArray aliases = new NSMutableArray();
		String domain = EOGrhumParametres.parametrePourCle(editingContext(),
				EOGrhumParametres.PARAM_GRHUM_DOMAINE_PRINCIPAL);
		if (!ERXStringUtilities.stringIsNullOrEmpty(grpAlias())) {
			aliases.addObject(grpAlias() + "@" + domain);
		}
		for (Object aliasObj : toPersonneAliases()) {
			EOPersonneAlias alias = (EOPersonneAlias) aliasObj;
			if (!ERXStringUtilities.stringIsNullOrEmpty(alias.alias())) {
				aliases.addObject(alias.alias() + "@" + domain);
			}
		}
		return aliases.immutableClone();
	}

	/**
	 * Vérifie si le groupe donné n'a pas un alias déjà attribué à un autre groupe, ou un compte dont le login est égal à cet alias. La logique est
	 * tirée d'Annuaire.app...
	 * <ul>
	 * <li>les grpAlias doivent être uniques.</li>
	 * <li>le grpAlias de la structure ne doit pas être affecté comme personneAlias de la même structure.</li>
	 * <li>Les personneAlias dans le cas des groupes doivent faire reference à un grpAlias existant.</li>
	 * <li>Les personneAlias dans le cas des individus doivent être uniques (non utilisés dans grpAlias, ni dans login (sauf pour la personne
	 * concernée), ni dans compte_email sur domaine interne).</li>
	 * </ul>
	 * 
	 * @param structure le groupe que l'on veut checker
	 */
	private void checkAlias() {
		if (grpAlias() != null) {
			// Vérification du format de l'alias
			String alias = grpAlias();
			ControleSpecifique.checkAliasFormatCorrect(editingContext(), alias);

			// Vérification de l'unicité de l'alias par rapport à grpAlias
			EOStructure struct = EOStructureForGroupeSpec.getGroupeForGrpAlias(
					editingContext(), alias);
			if (struct != null && !struct.globalID().equals(globalID())) {
				String messErreur = "La structure " + struct.libelleEtId()
						+ " " + "comporte déjà l'alias " + struct.grpAlias();
				throw new NSValidation.ValidationException(messErreur);
			}

			// Vérification de l'unicité de l'alias par rapport aux
			// personneAlias sur la même structure
			NSArray<EOPersonneAlias> aliasDouble = EOPersonneAlias
					.fetchForAlias(editingContext(), alias, null);
			Iterator<EOPersonneAlias> iter = aliasDouble.iterator();
			while (iter.hasNext()) {
				EOPersonneAlias eoPersonneAlias = (EOPersonneAlias) iter.next();
				if (globalID().equals(eoPersonneAlias.toStructure().globalID())) {
					String messErreur = "La structure " + libelleEtId() + " "
							+ "comporte déjà l'alias " + alias
							+ " affecté dans PersonneAlias";
					throw new NSValidation.ValidationException(messErreur);
				}
			}

			// Vérification de l'unicité de l'alias par rapport aux login
			NSArray<EOVlans> vlans = EOVlans
					.fetchAllVLansAPrendreEnCompte(editingContext());
			EOQualifier qualifier = EOCompte.CPT_LOGIN.eq(alias).and(
					EOCompte.TO_VLANS.in(vlans));
			EOCompte compte = EOCompte.fetchFirstByQualifier(editingContext(),
					qualifier);

			if (compte != null) {
				String messErreur = "La structure " + libelleEtId() + " "
						+ "comporte déjà l'alias " + alias
						+ " affecté dans PersonneAlias";
				throw new NSValidation.ValidationException(
						"Un compte avec le login " + compte.cptLogin()
								+ " existe déjà dans le système d'information");
			}

			// Vérification de non collision avec les emails
			EOCompteEmail compteEmail = EOCompteEmail.fetchCompteEmailInterne(
					editingContext(), alias);
			if (compteEmail != null) {
				String messErreur = "Une adresse email "
						+ compteEmail.cemEmail() + "@"
						+ compteEmail.cemDomaine()
						+ " existe déjà pour la personne "
						+ compteEmail.toCompte().toPersonne().libelleEtId();
				throw new NSValidation.ValidationException(messErreur);
			}

		}
	}

	/**
	 * @author alainmalaplate Réalisation d'une recherche
	 * @param myContext
	 * @param nom Seule une partie du nom peut etre fournie. Les mots sont analysés.
	 * @param sigle Optionnel
	 * @param qualifierOptionnel
	 * @return Toutes les structures valides trouvees en fonction du nom et du sigle. le résultat est classés par ordre de pertinence.
	 */
	public static NSArray<EOStructure> structuresByNameAndSigle(
			EOEditingContext myContext, String nom, String sigle,
			EOQualifier qualifierOptionnel, int fetchLimit) {
		if (MyStringCtrl.isEmpty(nom) && MyStringCtrl.isEmpty(sigle)) {
			return NSArray.EmptyArray;
		}

		NSMutableArray res = new NSMutableArray();
		if (sigle != null) {
			sigle = MyStringCtrl.chaineSansAccents(sigle, "?").trim()
					.toUpperCase();
		}

		String nomsInv = null;

		if (!MyStringCtrl.isEmpty(nom)) {
			nom = MyStringCtrl.chaineSansAccents(nom, "?").trim().toUpperCase();
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
		}

		if (!MyStringCtrl.isEmpty(sigle)) {
			res.addObjectsFromArray(structuresWithSigleLike(myContext, sigle,
					qualifierOptionnel, fetchLimit));
			res.addObjectsFromArray(structuresWithSigleLike(myContext, sigle
					+ ETOILE, qualifierOptionnel, fetchLimit));
		}

		if (!MyStringCtrl.isEmpty(nom)) {
			nomsInv = MyStringCtrl.inverseMots(nom).replaceAll(SPACE, ETOILE);
			nom = nom.replaceAll(SPACE, ETOILE);
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom,
					qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, nom
					+ ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						nomsInv + ETOILE, qualifierOptionnel, fetchLimit));
			}
			res.addObjectsFromArray(structuresWithNameLike(myContext, ETOILE
					+ nom + EOStructure.ETOILE, qualifierOptionnel, fetchLimit));
			if (!nomsInv.equals(nom)) {
				res.addObjectsFromArray(structuresWithNameLike(myContext,
						EOStructure.ETOILE + nomsInv + EOStructure.ETOILE,
						qualifierOptionnel, fetchLimit));
			}
		}

		AFinder.removeDuplicatesInNSArray(res);
		return res;
	}

	/**
	 * @return True si l'objet a été modifié (en ignorant les modifications sur les clés de IGNORE_CHANGES_IN_KEYS)
	 */
	public boolean objectHasChanged() {
		// Modification faite pour que seul le fichier Properties de RGRHUM
		// n'impacte cette méthode.
		if ("O".equals(FwkCktlPersonne.paramManager
				.getParam(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE))) {
			return true;
		}
		if (this.hasTemporaryGlobalID()) {
			return true;
		}
		NSArray committedKeys = changesFromCommittedSnapshot().allKeys();
		return !ERXArrayUtilities.arrayContainsArray(IGNORE_CHANGES_IN_KEYS,
				committedKeys);
	}

	public String retourneCreateur() {
		String createur = "";
		if (getCreateur() != null) {
			createur = getCreateur().getNomAndPrenom();
		}
		return createur;
	}

	public String retourneModificateur() {
		String modificateur = "";
		if (getModificateur() != null) {
			modificateur = getModificateur().getNomAndPrenom();
		}
		return modificateur;
	}

	@Deprecated
	// utliser dCreation
	public NSTimestamp retourneDateCreation() {
		return dCreation();
	}

	@Deprecated
	// utliser dModification
	public NSTimestamp retourneDateModification() {
		return dModification();
	}

	public static NSArray<EOStructure> filtrerLesStructuresNonAffichables(
			NSArray<EOStructure> structures, EOEditingContext edc,
			Integer persId) {
		PersonneApplicationUser appUser = new PersonneApplicationUser(edc,
				persId);
		if (!appUser.hasDroitGrhumCreateur()) {

			EOQualifier qualifierDesGroupesAMasquer = EOStructure.TO_REPART_TYPE_GROUPES
					.dot(EORepartTypeGroupe.TGRP_CODE)
					.containsObject("RE")
					.and(EOStructure.TO_REPART_TYPE_GROUPES.atCount().eq(2))
					.or(EOStructure.TO_REPART_TYPE_GROUPES.dot(
							EORepartTypeGroupe.TGRP_CODE).containsObject("FO"))
					.or(EOStructure.TO_REPART_TYPE_GROUPES.dot(
							EORepartTypeGroupe.TGRP_CODE).containsObject("PN"));

			return ERXArrayUtilities.arrayMinusArray(structures,
					ERXQ.filtered(structures, qualifierDesGroupesAMasquer));
		} else {
			return structures;
		}

	}

	public NSTimestamp dateNaissance() {
		return null;
	}

	// METHODE RAJOUTEES

	public static final NSArray<Object> ARRAY_SORT = new NSArray<Object>(
			new Object[] {
					EOSortOrdering.sortOrderingWithKey(LL_STRUCTURE_KEY,
							EOSortOrdering.CompareAscending)
			});

	public static final String TOS_STRUCTURES_FILLES_KEY = "tosStructuresFilles";
	public static final String TOS_SERVICE_PETITS_FILS_WITHOUT_SELF = "tosServicePetitsFilsWithoutSelf";

	public String display() {
		return displayLong();
	}

	/**
	 * display ultra court
	 */
	public String displayUltraCourt() {
		String display = lcStructure();
		return display;
	}

	/**
	 * On tronque le llStructure a 40 caracteres. Si la structure n'est pas un service, on affiche "archive' au lieu de la composante.
	 */
	public String displayLong() {
		String display = "";
		if (llStructure() != null) {
			display += (llStructure().length() > 40 ? llStructure().substring(
					0, 35)
					+ "(...)" : llStructure());
		} else {
			display = this + "";
		}
		// certains services qui n'en sont plus ne sont pas dans V_Service
		// d'ou composante introuvable ...
		EOStructure structureComposante = toComposanteEtEtablissement(); 
				
		display += (toStructurePere() == null ? "" : " ("
				+ (structureComposante != null ? structureComposante
						.lcStructure() : "archive") + ")");
		return display;
	}

	/**
	 * trouver la composante dont dépend un service
	 * 
	 * @param ec
	 * @return
	 */
	public static EOStructure findComposanteForService(EOStructure service) {
		EOStructure composante = null;

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				"toStructure = %@", new NSArray(service));
		NSArray records = FwkCktlPersonneUtil.fetchArray(
				service.editingContext(), EOVService.ENTITY_NAME, qual, null);
		if (records.count() > 0) {
			composante = (EOStructure) ((EOGenericRecord) records.lastObject())
					.valueForKey("toComposante");
		}

		return composante;
	}

	// public String cStructure() {
	// return (String) EOUtilities.primaryKeyForObject(editingContext(),
	// this).valueForKey("cStructure");
	// }

	public static final String TOS_SERVICE_FILS_KEY = "tosServiceFils";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String IS_SERVICE_KEY = "isService";
	public static final String IS_COMPOSANTE_KEY = "isComposante";

	/**
	 * La liste des individus ayant droit sur la structure du fait de la configuration de l'annuaire.
	 * 
	 * @return un <code>NSArray</code> de <code>EOIndividu</code>
	 */
	public NSArray<EOIndividu> getIndividuDroitAnnuaireList() {
		NSArray<EOIndividu> result = new NSArray<EOIndividu>();

		// remonter aux responsables des groupes intermédiaires jusqu'a la
		// racine
		boolean shouldContinue = true;
		EOStructure structure = this;
		while (shouldContinue) {
			if (structure == structure.toStructurePere()) {
				shouldContinue = false;
			}
			if (structure.toResponsable() != null) {
				result = result.arrayByAddingObject(structure.toResponsable());
			}
			structure = structure.toStructurePere();
		}

		return result;
	}

	/**
	 * Indique si la structure est une racine
	 * 
	 * @return
	 */
	public boolean isRacine() {
		boolean isRacine = false;

		if (cStructure().equals(toStructurePere().cStructure())
				&& cTypeStructure().equals("E")) {
			isRacine = true;
		}

		return isRacine;
	}

	/**
	 * Obtenir le qualifier permettant de faire un filtrage sur le libelle
	 * 
	 * @param filtreComposante
	 * @return
	 */
	public static EOQualifier getFiltreQualifier(String filtreComposante) {
		EOQualifier qual = null;

		qual = CktlDataBus.newCondition(LC_STRUCTURE_KEY
				+ " caseInsensitiveLike '*" + filtreComposante + "*' or "
				+ LL_STRUCTURE_KEY + " caseInsensitiveLike '*"
				+ filtreComposante + "*'");

		return qual;
	}

	public boolean isArchive() {

		if ((this.toStructurePere() != null)
				&& (this.toStructurePere().lcStructure() != null)
				&& (this.toStructurePere().lcStructure().contains("ARCHIV"))) {
			return true;
		}
		return false;
	}

	/**
	 * Liste de tous les sous services au sens annuaire (on l'inclue si elle meme est aussi service) A utiliser par exemple pour un affichage a 2
	 * niveaux : - 1 = composantes - 2 = services (le resultat de cette methode)
	 */
	public NSArray<EOStructure> tosServiceFils() {
		NSArray result = new NSArray();
		if (isService()) {
			// les fils directs (sans lui)
			result = NSArrayCtrl
					.flattenArray((NSArray) tosStructuresFilles(CktlDataBus
							.newCondition(C_STRUCTURE_KEY + "<>%@ and "
									+ IS_SERVICE_KEY + "=%@", new NSArray(
									new Object[] {
											this, Boolean.TRUE
									}))));
			// doublons
			result = NSArrayCtrl.removeDuplicate(result);
			// classement alphabetique par lcStructure (c'est ce qui est affiché
			// dans
			// le Browser)
			result = CktlSort.sortedArray(result, LC_STRUCTURE_KEY);
		} else if (isComposante()) {
			result = result.arrayByAddingObject(this);
		}
		//
		return result;
	}

	/**
	 * TODO switcher sur Rechercher tous les sous services de la structure. On arrete la recherche dans l'arbre s'il y a un trou (i.e. une structure
	 * pas service) ou bien si on trouve une composante (on l'incluera elle meme). Ajoute aussi la structure initiale a la liste
	 * 
	 * @param isShowArchive
	 * @return
	 */
	public NSArray<EOStructure> tosSousServiceDeep(boolean showArchive) {
		NSArray<EOStructure> result = new NSArray<EOStructure>();
		boolean shouldNotContinue = !showArchive && isArchive();
		if (!shouldNotContinue) {
			//
			if (isService()) {
				result = result.arrayByAddingObject(this);
			}
			//
			NSArray<EOStructure> servicesFils = tosServiceFils();
			for (int i = 0; i < servicesFils.count(); i++) {
				EOStructure service = servicesFils.objectAtIndex(i);
				if (cStructure() != service.cStructure()) {
					// TODO decommenter la ligne du dessous quand les
					// composantes seront
					// correctement
					// saisies, i.e. il n'y aura qu'un seul et unique niveau de
					// composantes
					// if (service != service.toStructurePere() &&
					// !service.isComposante()) {
					if (service != service.toStructurePere()) {
						result = result.arrayByAddingObjectsFromArray(service
								.tosSousServiceDeep(showArchive));
					}
				}
			}
		}
		//
		return result;
	}

	/**
	 * Trouver la racine de l'annuaire
	 * 
	 * @param ec editing context
	 * @return la racine de l'établissement
	 */
	public static EOStructure findRacineInContext(EOEditingContext ec) {
		EOStructure racine = null;

		EOQualifier qual = CktlDataBus.newCondition(C_STRUCTURE_KEY + "="
				+ TO_STRUCTURE_PERE_KEY + "." + EOStructure.C_STRUCTURE_KEY
				+ " and " + C_TYPE_STRUCTURE_KEY + "='E'");
		NSArray records = ec
				.objectsWithFetchSpecification(new EOFetchSpecification(
						ENTITY_NAME, qual, null));
		if (records.count() > 0) {
			racine = (EOStructure) records.lastObject();
		}
		return racine;
	}

	/**
	 * on tronque le llStructure a 25 caracteres + lcStructure
	 */
	public String displayCourt() {
		String display = "";
		if (llStructure() != null) {
			display += (llStructure().length() > 25 ? llStructure().substring(
					0, 20)
					+ "(...)" : llStructure());
		}
		if (lcStructure() != null) {
			display += " " + lcStructure();
		}
		display += (toStructurePere() == null ? "" : " ("
				+ EOStructure.findComposanteForService((EOStructure) this)
						.lcStructure() + ")");
		return display;
	}

	/**
	 * {@inheritDoc}
	 */
	public IAdresse adresseFacturation() {
		return personneDelegate.adressesFacturation();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean laStructurePossedeUnTypeGroupeDiplome() {
		return !toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE
				.inObjects(EOTypeGroupe.TGRP_CODE_D)).isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean laStructurePossedeUnTypeGroupeUnix() {
		return !toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE
				.inObjects(EOTypeGroupe.TGRP_CODE_U)).isEmpty();
	}
	
	public NSArray<EOStructure> getStructuresFilles() {
		CktlFetchSpecification<EOStructure> fs = new CktlFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, new ERXTrueQualifier(), null);
		fs.setConnectBy(C_STRUCTURE, C_STRUCTURE_PERE);
		fs.setStartWithQualifier(C_STRUCTURE_PERE.eq(cStructure()));
		return fs.fetchObjects(editingContext());
	}
	
	/**
	 * @return qualifier pour le type de groupe Service
	 */
	private static EOQualifier qualifierTypeService() {
		return EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).containsObject(EOTypeGroupe.TGRP_CODE_S);
	}
	
	
}
