/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Renvoie le code ROME 
 *
 */
public class EORome extends _EORome {

	private static final long serialVersionUID = -6465952061359832406L;

	//Qualifier pour récupérer la racine des codes ROME
	public static final EOQualifier QUAL_RACINE = ERXQ.or(
															ERXQ.equals(EORome.ROM_ID_PERE_KEY, EORome.ROM_ID_KEY),
															ERXQ.equals(EORome.ROM_ID_PERE_KEY, null)
													);
	
	public static final EOQualifier QUAL_FILS = EOQualifier.qualifierWithQualifierFormat(EORome.ROM_ID_PERE_KEY + " != " + EORome.ROM_ID_KEY, null);

	/** ROMEs non clos */
	public static final EOQualifier QUAL_ROME_NON_FERMEE = ERXQ.greaterThan(EORome.ROM_D_FERMETURE_KEY, new NSTimestamp());
	/** ROMEs sans date de fermeture */
	public static final EOQualifier QUAL_ROME_FERMETURE_NULL = ERXQ.isNull(EORome.ROM_D_FERMETURE.toString());
	/** ROMEs valides */
	public static final EOQualifier QUAL_ROME_VALIDE = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] { QUAL_ROME_NON_FERMEE, QUAL_ROME_FERMETURE_NULL }));

	public static final EOSortOrdering SORT_LIBELLE = EOSortOrdering.sortOrderingWithKey(EORome.ROM_LIBELLE_KEY, EOSortOrdering.CompareAscending);

	public EORome() {
		super();
	}

	/**
	 * @param edc : editingContext
	 * @return Les objet EORome definies comme racines de l'arbre des codes
	 *         ROME.
	 */
	public static NSArray<EORome> getRacines(EOEditingContext edc) {
		return fetchAll(edc, QUAL_RACINE);
		// fetchAll(edc, QUAL_RACINE, new NSArray(new Object[] {
		// SORT_LIBELLE
		// }));
	}

	/**
	 * @param edc : editingContext
	 * @return Les objets {@link EORome} fils de l'objet en cours.
	 */
	public NSArray<EORome> getFils(EOEditingContext edc) {
		return getFils(edc, null);
	}

	/**
	 * @param edc : editingContext
	 * @param qualifier : qualifier
	 * @return Les objets {@link EORome} fils de l'objet en cours.
	 */
	public NSArray<EORome> getFils(EOEditingContext edc, EOQualifier qualifier) {
		NSArray<EORome> resFinal = NSArray.emptyArray();
		// NSArray<EORome> res = fetchAll(edc, QUAL_RACINE, null);
		NSArray<EORome> res = fetchAll(edc, QUAL_FILS, null);
		if (res.count() > 0) {
			resFinal = (NSArray<EORome>) res.valueForKey(EORome.ROM_ID_KEY);
			if (qualifier != null) {
				resFinal = EOQualifier.filteredArrayWithQualifier(resFinal, qualifier);
			}
		}
		return resFinal;
	}

	/**
	 * @deprecated Utiliser la méthode {@link EORome#toChildren(EOQualifier, NSArray, boolean)} 
	 */
	@Deprecated 
	public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> toChildren(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return toChildren(qualifier, sortOrderings, false);
	}

}
