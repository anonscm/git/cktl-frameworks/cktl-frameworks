/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ISecretariat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOSecretariat extends _EOSecretariat implements ISecretariat {
	public EOSecretariat() {
		super();
	}
	
	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		setDModification(MyDateCtrl.now());
		if (cStructure() == null) {
			throw new NSValidation.ValidationException("Secretariat : vous devez fournir une structure");
		}
		if (toIndividu() == null) {
			throw new NSValidation.ValidationException("Secretariat : vous devez fournir un individu");
		}

		super.validateObjectMetier();
	}

	public void initAvecStructureEtNoIndividu(EOStructure structure, Integer noIndividu) {
		setCStructure(structure.cStructure());
		setNoIndividu(noIndividu);
	}

	/**
	 * retourne la liste des secr&eacute;tariats associ&eacute;s &agrave; une
	 * structure ou tous si la structure est nulle
	 */
	public static NSArray<EOSecretariat> rechercherSecretariatsPourStructure(EOEditingContext editingContext, EOStructure structure) {
		return rechercherSecretariatsPourStructure(editingContext, structure);
	}
	
	/**
	 * retourne la liste des secr&eacute;tariats associ&eacute;s &agrave; un
	 * individu ou tous si la structure est nulle
	 */
	public static NSArray<EOSecretariat> rechercherSecretariatsPourIndividu(
			EOEditingContext editingContext, EOIndividu individu) {
		return rechercherSecretariatsPourIndividu(editingContext, individu);
	}
	
	/**
	 * retourne la liste des secr&eacute;tariats associ&eacute;s &agrave; une
	 * personne ou tous si la structure est nulle
	 */
	public static NSArray<EOSecretariat> rechercherSecretariatsPourPersonne(
			EOEditingContext editingContext, IPersonne personne) {
		EOQualifier qualifier = null;
		if (personne != null) {
			if (personne.isIndividu()) {
				qualifier = ERXQ.is(TO_INDIVIDU_KEY, (EOIndividu) personne);
			} else {
				qualifier = ERXQ.is(C_STRUCTURE_KEY,
						((EOStructure) personne).cStructure());
			}
		}
		
		return EOSecretariat.fetchAll(editingContext, qualifier);
	}

	@Override
	public void setToIndividuRelationship(EOIndividu value) {
		super.setToIndividuRelationship(value);
		if (toIndividu() == null) {
			setNoIndividu(null);
		}
		else {
			setNoIndividu(toIndividu().noIndividu());
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDCreation(MyDateCtrl.now());
	}

}
