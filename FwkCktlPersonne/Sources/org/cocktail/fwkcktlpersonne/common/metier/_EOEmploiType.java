/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEmploiType.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOEmploiType extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOEmploiType.class);

	public static final String ENTITY_NAME = "Fwkpers_EmploiType";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EMPLOI_TYPE_OLD";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> ETY_CODE = new ERXKey<String>("etyCode");
  public static final ERXKey<String> ETY_CODE_MEN = new ERXKey<String>("etyCodeMen");
  public static final ERXKey<String> ETY_DEFINITION = new ERXKey<String>("etyDefinition");
  public static final ERXKey<String> ETY_LIBELLE = new ERXKey<String>("etyLibelle");
  public static final ERXKey<String> ETY_SIGLE_CORPS = new ERXKey<String>("etySigleCorps");
  public static final ERXKey<Integer> FPR_KEY = new ERXKey<Integer>("fprKey");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> TO_FAMILLE_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle>("toFamilleProfessionnelle");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> TOS_REPART_EMPLOI_ACTIVITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite>("tosRepartEmploiActivite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> TOS_REPART_EMPLOI_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>("tosRepartEmploiCompetence");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "etyCode";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETY_CODE_KEY = "etyCode";
	public static final String ETY_CODE_MEN_KEY = "etyCodeMen";
	public static final String ETY_DEFINITION_KEY = "etyDefinition";
	public static final String ETY_LIBELLE_KEY = "etyLibelle";
	public static final String ETY_SIGLE_CORPS_KEY = "etySigleCorps";
	public static final String FPR_KEY_KEY = "fprKey";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETY_CODE_COLKEY = "ETY_CODE";
	public static final String ETY_CODE_MEN_COLKEY = "ETY_CODE_MEN";
	public static final String ETY_DEFINITION_COLKEY = "ETY_DEFINITION";
	public static final String ETY_LIBELLE_COLKEY = "ETY_LIBELLE";
	public static final String ETY_SIGLE_CORPS_COLKEY = "ETY_SIGLE_CORPS";
	public static final String FPR_KEY_COLKEY = "FPR_KEY";



	// Relationships
	public static final String TO_FAMILLE_PROFESSIONNELLE_KEY = "toFamilleProfessionnelle";
	public static final String TOS_REPART_EMPLOI_ACTIVITE_KEY = "tosRepartEmploiActivite";
	public static final String TOS_REPART_EMPLOI_COMPETENCE_KEY = "tosRepartEmploiCompetence";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String etyCode() {
    return (String) storedValueForKey(ETY_CODE_KEY);
  }

  public void setEtyCode(String value) {
    takeStoredValueForKey(value, ETY_CODE_KEY);
  }

  public String etyCodeMen() {
    return (String) storedValueForKey(ETY_CODE_MEN_KEY);
  }

  public void setEtyCodeMen(String value) {
    takeStoredValueForKey(value, ETY_CODE_MEN_KEY);
  }

  public String etyDefinition() {
    return (String) storedValueForKey(ETY_DEFINITION_KEY);
  }

  public void setEtyDefinition(String value) {
    takeStoredValueForKey(value, ETY_DEFINITION_KEY);
  }

  public String etyLibelle() {
    return (String) storedValueForKey(ETY_LIBELLE_KEY);
  }

  public void setEtyLibelle(String value) {
    takeStoredValueForKey(value, ETY_LIBELLE_KEY);
  }

  public String etySigleCorps() {
    return (String) storedValueForKey(ETY_SIGLE_CORPS_KEY);
  }

  public void setEtySigleCorps(String value) {
    takeStoredValueForKey(value, ETY_SIGLE_CORPS_KEY);
  }

  public Integer fprKey() {
    return (Integer) storedValueForKey(FPR_KEY_KEY);
  }

  public void setFprKey(Integer value) {
    takeStoredValueForKey(value, FPR_KEY_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle toFamilleProfessionnelle() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle)storedValueForKey(TO_FAMILLE_PROFESSIONNELLE_KEY);
  }

  public void setToFamilleProfessionnelleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle oldValue = toFamilleProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FAMILLE_PROFESSIONNELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FAMILLE_PROFESSIONNELLE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> tosRepartEmploiActivite() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite>)storedValueForKey(TOS_REPART_EMPLOI_ACTIVITE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> tosRepartEmploiActivite(EOQualifier qualifier) {
    return tosRepartEmploiActivite(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> tosRepartEmploiActivite(EOQualifier qualifier, boolean fetch) {
    return tosRepartEmploiActivite(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> tosRepartEmploiActivite(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite.TO_EMPLOI_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartEmploiActivite();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartEmploiActiviteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_ACTIVITE_KEY);
  }

  public void removeFromTosRepartEmploiActiviteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_ACTIVITE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite createTosRepartEmploiActiviteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartEmploiActivite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_EMPLOI_ACTIVITE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite) eo;
  }

  public void deleteTosRepartEmploiActiviteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_ACTIVITE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartEmploiActiviteRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiActivite> objects = tosRepartEmploiActivite().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartEmploiActiviteRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)storedValueForKey(TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier) {
    return tosRepartEmploiCompetence(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier, boolean fetch) {
    return tosRepartEmploiCompetence(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence.TO_EMPLOI_TYPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartEmploiCompetence();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public void removeFromTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence createTosRepartEmploiCompetenceRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartEmploiCompetence");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_EMPLOI_COMPETENCE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence) eo;
  }

  public void deleteTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartEmploiCompetenceRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> objects = tosRepartEmploiCompetence().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartEmploiCompetenceRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEmploiType avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEmploiType createEOEmploiType(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String etyCode
			) {
    EOEmploiType eo = (EOEmploiType) createAndInsertInstance(editingContext, _EOEmploiType.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEtyCode(etyCode);
    return eo;
  }

  
	  public EOEmploiType localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEmploiType)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploiType creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploiType creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOEmploiType object = (EOEmploiType)createAndInsertInstance(editingContext, _EOEmploiType.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEmploiType localInstanceIn(EOEditingContext editingContext, EOEmploiType eo) {
    EOEmploiType localInstance = (eo == null) ? null : (EOEmploiType)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEmploiType#localInstanceIn a la place.
   */
	public static EOEmploiType localInstanceOf(EOEditingContext editingContext, EOEmploiType eo) {
		return EOEmploiType.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEmploiType fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEmploiType fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEmploiType> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEmploiType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEmploiType)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEmploiType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEmploiType fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEmploiType> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEmploiType eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEmploiType)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEmploiType fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEmploiType eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEmploiType ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEmploiType fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
