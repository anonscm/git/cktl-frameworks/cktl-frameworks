/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

public class EOAssociation extends _EOAssociation {
	public static final EOQualifier QUAL_RACINE = new EOKeyValueQualifier(
			EOAssociation.ASS_RACINE_KEY, EOQualifier.QualifierOperatorEqual,
			"O");
	public static final EOQualifier QUAL_CONTACT = new EOKeyValueQualifier(
			EOAssociation.ASS_CODE_KEY, EOQualifier.QualifierOperatorLike,
			"CONTA*");
	public static final EOQualifier QUAL_FONCTION = new EOKeyValueQualifier(
			EOAssociation.ASS_CODE_KEY, EOQualifier.QualifierOperatorLike,
			"FONCT*");

	public static final EOSortOrdering SORT_LIBELLE = EOSortOrdering
			.sortOrderingWithKey(EOAssociation.ASS_LIBELLE_KEY,
					EOSortOrdering.CompareAscending);
	public static final String ASS_CODE_RESP_JURIDIQUE = "RESJURIDIQUE";
	public static final String ASS_CODE_FINANCIEREPIE = "FINANCIERPIE";
	public static final String ASS_CODE_HEBERGE = "HEBERGE";
	public static final String ASS_CODE_FONCTION = "FONCTION";

	/** Associations de type Local */
	public static final EOQualifier QUAL_ASS_LOCALE = new EOKeyValueQualifier(
			EOAssociation.ASS_LOCALE_KEY, EOQualifier.QualifierOperatorEqual,
			"O");
	/** Associations de la nomenclature officielle */
	public static final EOQualifier QUAL_ASS_INSTITUTIONNELLE = new EOKeyValueQualifier(
			EOAssociation.ASS_LOCALE_KEY, EOQualifier.QualifierOperatorEqual,
			"N");
	/** Associations non close */
	public static final EOQualifier QUAL_ASS_NON_FERMEE = ERXQ.greaterThan(
			EOAssociation.D_FERMETURE_KEY, new NSTimestamp());
	/** Associations sans date de fermeture */
	public static final EOQualifier QUAL_ASS_FERMETURE_NULL = ERXQ
			.isNull(EOAssociation.D_FERMETURE.toString());
	/** Associations valides */
	public static final EOQualifier QUAL_ASS_VALIDE = new EOOrQualifier(
			new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_ASS_NON_FERMEE,
					QUAL_ASS_FERMETURE_NULL
			}));
	/** Associations expirées */
	public static final EOQualifier QUAL_ASS_EXPIREE = new EONotQualifier(
			QUAL_ASS_VALIDE);
	/** Toutes les Associations */
	public static final EOQualifier QUAL_ASS_TOUTES = new EOOrQualifier(
			new NSArray<EOQualifier>(new EOQualifier[] {
					QUAL_ASS_INSTITUTIONNELLE, QUAL_ASS_LOCALE
			}));

	public EOAssociation() {
		super();
	}

	/**
	 * @param edc
	 * @return Les objet EOAssociation definies comme racines de l'arbre des associations.
	 */
	public static NSArray getRacines(EOEditingContext edc) {
		EOQualifier qual = new EOAndQualifier(new NSArray<EOQualifier>(
				new EOQualifier[] {
						QUAL_RACINE, QUAL_FONCTION
				}));
		// return fetchAll(edc, QUAL_RACINE, new NSArray(new Object[] {
		return fetchAll(edc, qual, new NSArray(new Object[] {
				SORT_LIBELLE
		}));
	}

	/**
	 * @param edc
	 * @param qualifier
	 * @return Les objets {@link EOAssociation} fils de l'objet en cours.
	 */
	public NSArray<EOAssociation> getFils(EOEditingContext edc) {
		return getFils(edc, null);
	}

	/**
	 * @param edc
	 * @param qualifier
	 * @return Les objets {@link EOAssociation} fils de l'objet en cours. Attention : Il faut comprendre que toAssociationReseauPeres donne tous les
	 *         couples Pères-Fils avec origine sur le Père sélectionné donc on obtient tous les Fils.
	 */
	public NSArray<EOAssociation> getFils(EOEditingContext edc,
			EOQualifier qualifier) {
		NSArray<EOAssociation> resFinal = NSArray.emptyArray();
		EOSortOrdering rangOrdering = EOSortOrdering.sortOrderingWithKey(
				EOAssociationReseau.ASR_RANG_KEY,
				EOSortOrdering.CompareAscending);
		NSArray<EOAssociationReseau> res = toAssociationReseauPeres(null,
				new NSArray(rangOrdering), false);
		if (res.count() > 0) {
			resFinal = (NSArray<EOAssociation>) res
					.valueForKey(EOAssociationReseau.TO_ASSOCIATION_FILS_KEY);
			if (qualifier != null) {
				resFinal = EOQualifier.filteredArrayWithQualifier(resFinal,
						qualifier);
			}
		}
		return resFinal;
	}

	/**
	 * @param edc
	 * @param qualifier
	 * @return Les objets {@link EOAssociation} pères de l'objet en cours.
	 */
	public NSArray<EOAssociation> getPeres(EOEditingContext edc) {
		return getPeres(edc, null);
	}

	/**
	 * @param edc
	 * @param qualifier
	 * @return Les objets {@link EOAssociation} pères de l'objet en cours. Attention : Il faut comprendre que toAssociationReseauFils donne tous les
	 *         couples Pères-Fils avec origine sur le Fils sélectionné donc on obtient tous les Pères.
	 */
	public NSArray<EOAssociation> getPeres(EOEditingContext edc,
			EOQualifier qualifier) {
		NSArray<EOAssociation> resFinal = NSArray.emptyArray();
		NSArray<EOAssociationReseau> res = toAssociationReseauFils(null, null,
				false);
		if (res.count() > 0) {
			resFinal = (NSArray<EOAssociation>) res
					.valueForKey(EOAssociationReseau.TO_ASSOCIATION_PERE_KEY);
			if (qualifier != null) {
				resFinal = EOQualifier.filteredArrayWithQualifier(resFinal,
						qualifier);
			}
		}
		return resFinal;
	}

	/**
	 * @param edc
	 * @param type le code du type d'association (cf. les constantes de {@link EOTypeAssociation}).
	 * @return Les associations d'un certain type.
	 */
	public static NSArray fetchAssociationsByType(EOEditingContext edc,
			String type) {
		EOQualifier qual = new EOKeyValueQualifier(
				EOAssociation.TO_TYPE_ASSOCIATION_KEY + "."
						+ EOTypeAssociation.TAS_CODE_KEY,
				EOQualifier.QualifierOperatorEqual, type);
		return fetchAll(edc, qual, new NSArray(new Object[] {
				SORT_LIBELLE
		}));
	}

	/**
	 * On ne supprime pas de manière définitive. On clôt seulement les associations de type LOCALE. On renseigne donc la date de modification et la
	 * date de fermeture.
	 * 
	 * @throws Exception
	 */
	public void archiver() throws Exception {
		setDModification(MyDateCtrl.now());
		setDFermeture(MyDateCtrl.now());
		this.cascadeFermeture();
	}

	public boolean isClosed() {
		if (this.dFermeture() != null) {
			if (this.dFermeture().before(new NSTimestamp())
					|| this.dFermeture().equals(new NSTimestamp())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Vérification s'il existe des fonctions fils et si oui fermeture en cascade des fils et des fonctions encore en dessous.
	 */
	public void cascadeFermeture() {
		if (this.getFils(getValidationEditingContext()).count() > 0) {
			NSArray<EOAssociation> tableauFils = this
					.getFils(getValidationEditingContext());
			int nbreDeFils = this.getFils(getValidationEditingContext())
					.count();
			int index = 0;
			while (index < nbreDeFils) {
				if (!tableauFils.objectAtIndex(index).isClosed()) {
					tableauFils.objectAtIndex(index).setDModification(
							new NSTimestamp());
					tableauFils.objectAtIndex(index).setDFermeture(
							new NSTimestamp());
					if (tableauFils.objectAtIndex(index)
							.getFils(getValidationEditingContext()).count() > 0) {
						tableauFils.objectAtIndex(index).cascadeFermeture();
					}
				}
				index = index + 1;
			}
		}
	}

	/**
	 * Vérification s'il existe des fonctions filles qui sont fermées elles aussi et si oui ouverture en cascade des filles et des fonctions encore en
	 * dessous.
	 */
	public void cascadeOuverture() {
		if (this.getFils(getValidationEditingContext()).count() > 0) {
			NSArray<EOAssociation> tableauFils = this
					.getFils(getValidationEditingContext());
			int nbreDeFils = this.getFils(getValidationEditingContext())
					.count();
			int index = 0;
			while (index < nbreDeFils) {
				if (tableauFils.objectAtIndex(index).isClosed()) {
					tableauFils.objectAtIndex(index).setDModification(
							new NSTimestamp());
					tableauFils.objectAtIndex(index).setDFermeture(null);
					if (tableauFils.objectAtIndex(index)
							.getFils(getValidationEditingContext()).count() > 0) {
						tableauFils.objectAtIndex(index).cascadeFermeture();
					}
				}
				index = index + 1;
			}
		}
	}

	@Override
	public void validateObjectMetier() throws ValidationException {
		if (this.isNewObject() || this.isUpdatedObject()) {
			checkDoublonsForCode();
			checkDoublonsForLibelle();
		}
		super.validateObjectMetier();
	}

	/**
	 * Verifier si le code est utilisé en doublon (cohérence avec index unique grhum.UK_ASS_CODE).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	protected void checkDoublonsForCode() throws NSValidation.ValidationException {
		if (assCode() != null) {
			NSArray doublons = fetchAll(this.editingContext(), ERXQ.equals(ASS_CODE_KEY, assCode()), null);
			if (doublons.count() > 1 || (doublons.count() == 1 && doublons.objectAtIndex(0) != this)) {
				EOAssociation doublon = (EOAssociation) doublons.objectAtIndex(0);
				throw new NSValidation.ValidationException("Un rôle ou une fonction existe déjà avec le code " + doublon.assCode() + ".");
			}
		}
	}

	/**
	 * Verifier si le libellé est utilisé en doublon (cohérence avec index unique grhum.UK_ASS_LIBELLE).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	protected void checkDoublonsForLibelle() throws NSValidation.ValidationException {
		if (assCode() != null) {
			NSArray doublons = fetchAll(this.editingContext(), ERXQ.equals(ASS_LIBELLE_KEY, assLibelle()), null);
			if (doublons.count() > 1 || (doublons.count() == 1 && doublons.objectAtIndex(0) != this)) {
				EOAssociation doublon = (EOAssociation) doublons.objectAtIndex(0);
				throw new NSValidation.ValidationException("Un rôle ou une fonction existe déjà avec le libellé " + doublon.assLibelle() + ".");
			}
		}
	}

	/**
	 * Cette enum contient les association en tant que constantes LEs liste est générée à partir de la base avec la requete suivante : select UPPER(
	 * REPLACE( REPLACE( GRHUM.Chaine_Sans_Accents(ASS_CODE), CHR(45), CHR(95) ), CHR(46), CHR(95) ) ) || '("' || ASS_CODE || '"), // ' || ASS_LIBELLE
	 * from grhum.association where ass_locale = 'N' order by ASS_CODE; Note : petit soucis avec deux ass_code ETUDIANT, un des deux à un accent
	 * 
	 * @author jlafourc
	 */
	public static enum Liste {
		ABONNE("ABONNÉ"), // ABONNÉ
		ACCOMPAGNANT("ACCOMPAGNANT"), // ACCOMPAGNANT DE :
		ACMO("ACMO"), // ACMO
		ADJOINT_AU_C("ADJOINT AU C"), // ADJOINT AU CHEF DES SERVICES
										// ADMINISTRATIFS
		ADJOINT_AU_D("ADJOINT AU D"), // ADJOINT AU DIRECTEUR
		ADMIN("ADMIN"), // ADMINISTRATEUR
		ADMINPROV("ADMINPROV"), // ADMINISTRATEUR PROVISOIRE
		ADRESSE("ADRESSE"), // ADRESSE
		ADRESSE_MAIL("ADRESSE MAIL"), // ADRESSE MAIL
		AFFECT("AFFECT"), // AFFECTATION
		AGE_("AGE :"), // AGE :
		AGENT_COMPTA("AGENT COMPTA"), // AGENT COMPTABLE
		AGENT_DE_TRA("AGENT DE TRA"), // AGENT DE TRAITEMENT
		AIDE_A_LA_CO("AIDE À LA CO"), // AIDE À LA CONCEPTION DE PRODUITS
										// PÉDAGOGIQUES
		ANALYSTE("ANALYSTE"), // ANALYSTE
		ASSESSEUR("ASSESSEUR"), // ASSESSEUR
		ASSESSEUR_DU("ASSESSEUR DU"), // ASSESSEUR DU DOYEN
		ASSISTANTE_S("ASSISTANTE S"), // ASSISTANTE SOCIALE
		AUTRE("AUTRE"), // AUTRE
		BUREAUTIQUE("BUREAUTIQUE"), // BUREAUTIQUE
		CATEGORIE("CATÉGORIE"), // CATÉGORIE
		CELLULE("CELLULE"), // CELLULE
		CHARGCOMMUNI("CHARGCOMMUNI"), // CHARGÉ(E) DE COMMUNICATION
		CHARGDOSSIER("CHARGDOSSIER"), // CHARGE DU DOSSIER
		CHARGE_D_UNE("CHARGÉ D'UNE"), // CHARGÉ D'UNE MISSION OU DE
										// RESPONSABILITÉ PARTICULIÈRE PAR LE
										// MINISTRE
		CHARGEE_DU_S("CHARGÉE DU S"), // CHARGÉE DU SERVICE DE PRÉVENTION
										// HYGIÈNE ET SÉCURITÉ
		CHARGMISSUFR("CHARGMISSUFR"), // CHARGÉ(E) DE MISSION (UFR)
		CHARGMISSUNI("CHARGMISSUNI"), // CHARGÉ(E) DE MISSION (DE L'UNIVERSITÉ)
		CHEF_D_ATELI("CHEF D'ATELI"), // CHEF D'ATELIER MECANOGRAPHIQUE
		CHEF_DE_BURE("CHEF DE BURE"), // CHEF DE BUREAU
		CHEF_DE_DIVI("CHEF DE DIVI"), // CHEF DE DIVISION
		CHEF_DE_GARA("CHEF DE GARA"), // CHEF DE GARAGE
		CHEF_DE_PROJ("CHEF DE PROJ"), // CHEF DE PROJET
		CHEF_DE_SECT("CHEF DE SECT"), // CHEF DE SECTION
		CHEF_DEPARTE("CHEF DÉPARTE"), // CHEF DÉPARTEMENT IUT
		CHEF_DES_SER("CHEF DES SER"), // CHEF DES SERVICES ADMINISTRATIFS DE
										// L'UFR
		CHEF_D_EXPLO("CHEF D'EXPLO"), // CHEF D'EXPLOITATION
		CHEF_OPERATE("CHEF OPERATE"), // CHEF OPERATEUR
		CHEF_PROGRAM("CHEF PROGRAM"), // CHEF PROGRAMMEUR
		CHEFSERVCESO("CHEFSERVCESO"), // CHEF DE SERVICE DE CENTRE DE SOINS
		CHEFSERVFICO("CHEFSERVFICO"), // CHEF DE SERVICE FINANCIER FORMATION
										// CONTINUE
		CHEFSERVFIIN("CHEFSERVFIIN"), // CHEF DE SERVICE FINANCIER FORMATION
										// INITIALE
		CHEFSERVFINA("CHEFSERVFINA"), // CHEF DU SERVICE FINANCIER
		CHEFSERVICE("CHEFSERVICE"), // CHEF DE SERVICE
		CHEFSERVPEDA("CHEFSERVPEDA"), // CHEF DU SERVICE PÉDAGOGIE-PLANNING
		CHEFSERVPERS("CHEFSERVPERS"), // CHEF DU SERVICE DES PERSONNELS
										// ENSEIGNANTS / IATOS
		CHEFSERVSCOL("CHEFSERVSCOL"), // CHEF DU SERVICE DE LA SCOLARITÉ
		COMMANDE_DE("COMMANDE DE "), // COMMANDE DE LOGIGIELS
		COMPETENCE_I("COMPÉTENCE I"), // COMPÉTENCE INFORMATIQUE
		CONCEPTION_D("CONCEPTION D"), // CONCEPTION DE DISPOSITIFS DE FORMATION
										// OUVERTS ET À DISTANCE
		CONDUCTEUR_A("CONDUCTEUR A"), // CONDUCTEUR AUTO
		CONSEILLER_D("CONSEILLER D"), // CONSEILLER D'ÉTABLISSEMENT
		CONSEILLER_P("CONSEILLER P"), // CONSEILLER PÉDAGOGIQUE
		CONSEILLER_S("CONSEILLER S"), // CONSEILLER SCIENTIFIQUE
		CONTACT("CONTACT"), // CONTACT
		CONTACTADMIN("CONTACTADMIN"), // CONTACT ADMINISTRATIF
		CONTACTCOMME("CONTACTCOMME"), // CONTACT COMMERCIAL
		CONTACTFINAN("CONTACTFINAN"), // CONTACT FINANCIER
		CONTACTTECHI("CONTACTTECHI"), // CONTACT TECHNIQUE
		COORDEXTERNE("COORDEXTERNE"), // COORDINATEUR EXTERNE
		COORDINAERAS("COORDINAERAS"), // COORDINATEUR DEPARTEMENTAL ERASMUS
		COORDINAINST("COORDINAINST"), // COORDINATEUR INSTITUTIONNEL
		COORDINAIUFM("COORDINAIUFM"), // COORDINATEUR IUFM
		COORDINASCIE("COORDINASCIE"), // COORDINATEUR SCIENTIFIQUE
		COORDINATIOE("COORDINATIOE"), // COORDINATION EXTERNE
		COORDINATIOI("COORDINATIOI"), // COORDINATION INTERNE
		COORDINATION("COORDINATION"), // COORDINATION D'ENSEIGNEMENT
										// TRANSVERSAUX À PLUSIEURS DIPLÔMES
		COORDINTERNE("COORDINTERNE"), // COORDINATEUR INTERNE
		COORDINTPROJ("COORDINTPROJ"), // COORDINATEUR DU PROJET
		COORDONNATEU("COORDONNATEU"), // COORDONNATEUR D'ENSEIGNEMENT
		COORDONNEES("COORDONNÉES"), // COORDONNÉES
		CORESADMREIN("CORESADMREIN"), // CORRESPONDANT ADMINISTRATIF RELATIONS
										// INTERNATIONALES
		CORESANNUADM("CORESANNUADM"), // CORRESPONDANT ANNUAIRE (ADMINISTRATIF)
		CORESANNUINF("CORESANNUINF"), // CORRESPONDANT ANNUAIRE (INFORMATIQUE)
		CORESBURETIQ("CORESBURETIQ"), // CORRESPONDANT BUREAUTIQUE
		CORESENSREIN("CORESENSREIN"), // CORRESPONDANT ENSEIGNANT RELATIONS
										// INTERNATIONALES
		CORESHARPEGE("CORESHARPEGE"), // CORRESPONDANT HARPÈGE
		CORESINFOLIB("CORESINFOLIB"), // CORRESPONDANT INFORMATIQUE ET LIBERTÉ
		CORESINTRANE("CORESINTRANE"), // CORRESPONDANT INTRANET
		CORESJEFYCO("CORESJEFYCO"), // CORRESPONDANT JEFYCO
		CORESPAPOGEE("CORESPAPOGEE"), // CORRESPONDANT APOGÉE
		CORESPAPOTEC("CORESPAPOTEC"), // CORRESPONDANT APOGÉE (TECHNIQUE)
		CORESPEDAGO("CORESPEDAGO"), // CORRESPONDANT PÉDAGOGIQUE
		CORESPREVEHS("CORESPREVEHS"), // CORRESPONDANT PRÉVENTION HYGIÈNE ET
										// SÉCURITÉ
		CORESRESEAU("CORESRESEAU"), // CORRESPONDANT RÉSEAU
		CORESSECURIT("CORESSECURIT"), // CORRESPONDANT SÉCURITÉ
		CORESTECHETA("CORESTECHETA"), // CORRESPONDANT TECHNIQUE D'ETABLISSEMENT
										// POUR RAP
		CORESTECSITE("CORESTECSITE"), // CORRESPONDANT TECHNIQUE DE SITE N°1
										// POUR RAP
		CORESWEB("CORESWEB"), // CORRESPONDANT WEB
		CREATDOSSIER("CREATDOSSIER"), // CREATEUR DU DOSSIER
		CREATION_ET("CRÉATION ET "), // CRÉATION ET MISE EN PLACE D'UNE NOUVELLE
										// FILIÈRE
		DACTYLOCODEU("DACTYLOCODEU"), // DACTYLOCODEUR
		D_COT_ETAB("D_COT_ETAB"), // ETABLISSEMENT DE COTUTELLE
		D_DIR("D_DIR"), // DIRECTION THESE
		D_DIR_COENC("D_DIR_COENC"), // Encadrant de thèse
		D_DIR_THESE("D_DIR_THESE"), // Directeur de thèse
		D_ED_R("D_ED_R"), // Ecole Doctorale de rattachement
		DELEGUE_REGI("DÉLÉGUÉ RÉGI"), // DÉLÉGUÉ RÉGIONAL À LA RECHERCHE ET À LA
										// TECHNOLOGIE
		D_EMPLOYEUR("D_EMPLOYEUR"), // Employeur
		DEPARTEMENT("DÉPARTEMENT"), // DÉPARTEMENT
		DEPOSITAIRE("DEPOSITAIRE"), // DEPOSITAIRE DE SALLE
		DESTALRTFINA("DESTALRTFINA"), // DESTINATAIRE ALERTE FINANCE
		DESTALRTSUIV("DESTALRTSUIV"), // DESTINATAIRE ALERTE SUIVI
		DESTALRTSUVA("DESTALRTSUVA"), // DESTINATAIRE ALERTE SUIVI VALO
		DESTINATAIRE("DESTINATAIRE"), // DESTINATAIRE CONVENTION
		D_ETAB_ACC("D_ETAB_ACC"), // ETABLISSEMENT D'ACCUEIL
		DEVELOPPEUR("DÉVELOPPEUR"), // DÉVELOPPEUR
		D_FINANCEUR("D_FINANCEUR"), // Financeur
		DGS("DGS"), // DIRECTEUR(TRICE) GENERAL(E) DES SERVICES
		DIRADJECOINS("DIRADJECOINS"), // DIRECTEUR ADJOINT D'ÉCOLES OU INSTITUTS
										// INTERNES
		DIRADJETAPES("DIRADJETAPES"), // DIRECTEUR ADJOINT D'ÉTABLISSEMENT
										// PUBLIC D'ENSEIGNEMENT SUPÉRIEUR
										// (FONCTION PRÉVUE PAR STATUT DE
										// L'ÉTABLISSEMENT)
		DIRADJOINT("DIRADJOINT"), // DIRECTEUR(TRICE)-ADJOINT(E)
		DIRADJUFR("DIRADJUFR"), // DIRECTEUR ADJOINT UFR
		DIRAEPAFIGAR("DIRAEPAFIGAR"), // DIRECTEUR DES AUTRES EPA AUTONOMES
										// FIGURANT DANS L'ARRÊTÉ DU 13/09/1990
		DIRAEPANFIGA("DIRAEPANFIGA"), // DIRECTEUR DES AUTRES EPA AUTONOMES NE
										// FIGURANT PAS DANS L'ARRÊTÉ DU
										// 13/09/1990
		DIRAEPARATAR("DIRAEPARATAR"), // DIRECTEUR DES AUTRES EPA RATTACHÉS
										// FIGURANT DANS L'ARRÊTÉ DU 13/09/1990
		DIRAEPARNFIG("DIRAEPARNFIG"), // DIRECTEUR DES AUTRES EPA RATTACHÉS NE
										// FIGURANT PAS DANS L'ARRÊTÉ DU
										// 13/09/1990
		DIRCELINFORM("DIRCELINFORM"), // DIRECTEUR(TRICE) DE LA CELLULE
										// INFORMATIQUE
		DIRCENTENSMA("DIRCENTENSMA"), // DIRECTEUR DE CENTRE D'ENSEIGNEMENT ET
										// DE RECHERCHE DE L'ENSAM
		DIRCIES("DIRCIES"), // DIRECTEUR DE CIES
		DIRDEPARTEME("DIRDEPARTEME"), // DIRECTEUR(TRICE) DE DÉPARTEMENT
		DIRDPTINSA("DIRDPTINSA"), // DIRECTEUR DE DÉPARTEMENT DE L'INSA
		DIRDUDIUDSP("DIRDUDIUDSP"), // DIRECTEUR(TRICE) DE DU OU DIU OU DSP
		DIRECNORMSUP("DIRECNORMSUP"), // DIRECTEUR D'ECOLES NORMALES SUPÉRIEURES
		DIRECOLCENTR("DIRECOLCENTR"), // DIRECTEUR DES ÉCOLES CENTRALES
		DIRECOLEDOCT("DIRECOLEDOCT"), // DIRECTEUR(TRICE) D'ÉCOLE DOCTORALE
		DIRECOLFRETR("DIRECOLFRETR"), // DIRECTEUR DES ÉCOLES FRANÇAISES À
										// L'ÉTRANGER
		DIRECTETUDES("DIRECTETUDES"), // DIRECTEUR(TRICE) DES ÉTUDES
		DIRECTEUR_IE("DIRECTEUR IE"), // DIRECTEUR IEP
		DIRECTEUR_SE("DIRECTEUR SE"), // DIRECTEUR SERVICE COMMUN UNIVERSITÉ
		DIRECTEUR_UF("DIRECTEUR UF"), // DIRECTEUR UFR
		DIRECTEUR_AD("DIRECTEUR-AD"), // DIRECTEUR-ADJOINT
		DIRECTION("DIRECTION"), // DIRECTION
		DIRENI("DIRENI"), // DIRECTEUR D'ENI
		DIRENSI("DIRENSI"), // DIRECTEUR D'ENSI
		DIREPSCPFIGA("DIREPSCPFIGA"), // DIRECTEUR DES AUTRES EPSCP FIGURANT
										// DANS L'ARRÊTÉ DU 13/09/1990
		DIRGIP("DIRGIP"), // DIRECTEUR DE GIP
		DIRGRANDETAB("DIRGRANDETAB"), // DIRECTEUR D'UN GRAND ÉTABLISSEMENT
		DIRINPFIGARR("DIRINPFIGARR"), // DIRECTEUR DES ÉCOLES ET INSTITUTS
										// INTERNES DES UNIVERSITÉS OU INP
										// FIGURANT DANS L'ARRÊTÉ DU 13/09/1990
										// MODIFIÉ
		DIRINPNFIGAR("DIRINPNFIGAR"), // DIRECTEUR DES ÉCOLES ET INSTITUTS
										// INTERNES DES UNIVERSITÉS OU INP NE
										// FIGURANT PAS DANS L'ARRÊTÉ DU
										// 13/09/1990 MODIFIÉ
		DIRINSA("DIRINSA"), // DIRECTEUR D'INSA
		DIRIUFM("DIRIUFM"), // DIRECTEUR D'IUFM
		DIRIUPMIAGE("DIRIUPMIAGE"), // DIRECTEUR(TRICE) DE L'IUP MIAGE
		DIRIUT("DIRIUT"), // DIRECTEUR D'IUT
		DIRLABORATOI("DIRLABORATOI"), // DIRECTEUR(TRICE) DE LABORATOIRE
		DIRMASTERPRO("DIRMASTERPRO"), // DIRECTEUR(TRICE) MASTER PROFESSIONNEL
		DIRMASTERREC("DIRMASTERREC"), // DIRECTEUR(TRICE) MASTER RECHERCHE
		DIRPROGRAMME("DIRPROGRAMME"), // DIRECTEUR DE PROGRAMME
		DIRRECHERCHE("DIRRECHERCHE"), // DIRECTEUR DE LA RECHERCHE (FONCTION
										// PRÉVUE PAR LE STATUT DE
										// L'ÉTABLISSEMENT)
		DIRSCIENTADJ("DIRSCIENTADJ"), // DIRECTEUR SCIENTIFIQUE ADJOINT
		DIRSCIENTIFI("DIRSCIENTIFI"), // DIRECTEUR SCIENTIFIQUE
		DIRUFRSERVIC("DIRUFRSERVIC"), // DIRECTEUR(TRICE) D'UFR OU DE SERVICE
		D_JR_INV("D_JR_INV"), // Invité
		D_JR_MEM("D_JR_MEM"), // Membre
		D_JR_PRES("D_JR_PRES"), // Président
		D_JURY("D_JURY"), // JURY - SOUTENANCE
		D_JURY_RAP("D_JURY_RAP"), // Rapporteur
		D_LAB_THESE("D_LAB_THESE"), // Laboratoire de thèse
		DOCT("DOCT"), // DOCTORANT
		D_TYPE_JURY("D_TYPE_JURY"), // TYPE MEMBRES JURY
		EMERITE("EMERITE"), // EMERITE
		ENSEIGNANTE("ENSEIGNANT(E"), // ENSEIGNANT(E)
		ENSEIGNEMENT("ENSEIGNEMENT"), // ENSEIGNEMENT
		EQUIPERECHER("EQUIPERECHER"), // EQUIPE DE RECHERCHE
		ETABGESTFINA("ETABGESTFINA"), // ETABLISSEMENT GESTIONNAIRE FINANCIER
		ETABINTPRINC("ETABINTPRINC"), // ETABLISSEMENT INTERNE PRINCIPAL
		ETABRATTACHE("ETABRATTACHE"), // ETABLISSEMENT RATTACHEMENT
		ETABRATTPRIN("ETABRATTPRIN"), // ETABLISSEMENT RATTACHEMENT PRINC.
		ETUDIANT("ETUDIANT"), // ETUDIANT
		ETUDIANT_AVEC_ACCENT("ÉTUDIANT"), // ÉTUDIANT
		EXPERT_CONSE("EXPERT CONSE"), // EXPERT CONSEIL
		FINANCEUR("FINANCEUR"), // FINANCEUR
		FINANCIERPIE("FINANCIERPIE"), // FINANCIERE PIE
		FONCTION("FONCTION"), // FONCTION
		FONCTION_ADM("FONCTION ADM"), // FONCTION ADMINISTRATIVE
		FONCTION_DE("FONCTION DE "), // FONCTION DE GROUPE
		FONCTION_RI("FONCTION RI"), // FONCTION RI
		FONCTION_TEC("FONCTION TEC"), // FONCTION TECHNIQUE
		FONCTIONS_A("FONCTIONS (A"), // FONCTIONS (AUTRES QUE D'ENSEIGNEMENT)
										// CULTURELLES OU SCIENTIFIQUES EXERCÉES
										// AUPRÈS DU MAE (PRÉVUES AU 1° DE
										// L'ARRÊTÉ DU 01/07/1996)
		GESTADMFICON("GESTADMFICON"), // GESTIONNAIRE ADMIN. ET FIN. CONVENTION
		GESTADMINFIN("GESTADMINFIN"), // GESTIONNAIRE ADMIN. ET FIN.
		GESTION_DU_P("GESTION DU P"), // GESTION DU PATRIMOINE IMMOBILIER
		GESTIONBDD("GESTIONBDD"), // GESTIONNAIRE DE BASES DE DONNÉES
		GESTIONCROUS("GESTIONCROUS"), // GESTIONNAIRE DU CROUS
		GROUPE_PEDAG("GROUPE PÉDAG"), // GROUPE PÉDAGOGIQUE
		HEBERGE("HEBERGE"), // HEBERGE
		INDEMNITE_DE("INDEMNITE DE"), // INDEMNITE DE CONDUCTEURS
		INFIRMIERE("INFIRMIÈRE"), // INFIRMIÈRE
		INFORMATION_("INFORMATION,"), // INFORMATION, ORIENTATION, ACCUEIL DES
										// ÉTUDIANTS
		INSCRITADMIN("INSCRITADMIN"), // INSCRIT (INSCRIPTION ADMINISTRATIVE)
		INSCRITPEDA("INSCRITPEDA"), // INSCRIT (INSCRIPTION PÉDAGOGIQUE)
		INTERVENANT("INTERVENANT"), // INTERVENANT
		LABORATOIRE("LABORATOIRE "), // LABORATOIRE ASSOCIÉ
		LOCALISE_DAN("LOCALISÉ DAN"), // LOCALISÉ DANS
		MEMBRE("MEMBRE"), // MEMBRE
		MEMBRE_NP("MEMBRE NP"), // MEMBRE NON PERMANENT
		MEMBRE_P("MEMBRE P"), // MEMBRE PERMANENT
		MOASYSTINFOR("MOASYSTINFOR"), // MAITRISE D'OUVRAGE SYSTÈMES
										// INFORMATIQUES
		MOESYSTINFOR("MOESYSTINFOR"), // MAITRISE D'OEUVRE SYSTÈMES
										// INFORMATIQUES
		MONITEUR("MONITEUR"), // MONITEUR
		OEA("OEA"), // OEA
		OPERATEUR("OPERATEUR"), // OPERATEUR
		ORGAPEDASTAG("ORGAPEDASTAG"), // ORGANISATIONS, COORDINATION ET SUIVI
										// PÉDAGOGIQUE DE STAGE
		ORGAPEDATUTO("ORGAPEDATUTO"), // ORGANISATIONS, COORDINATION ET SUIVI
										// PÉDAGOGIQUE DU TUTORAT
		PARTACTFORCO("PARTACTFORCO"), // PARTICIPATION AUX ACTIVITÉS DE
										// FORMATION CONTINUE
		PARTCONTRACT("PARTCONTRACT"), // PARTENAIRE CONTRACTANT
		PARTDEVANIFO("PARTDEVANIFO"), // PARTICIPATION AU DÉVELOPPEMENT ET À
										// L'ANIMATION DE FORMATIONS
										// DÉLOCALISÉES
		PARTENAIRE("PARTENAIRE"), // PARTENAIRE
		PARTENAIRES("PARTENAIRES"), // PARTENAIRES
		PARTICIPANT("PARTICIPANT"), // PARTICIPANT
		PERSONNALITE("PERSONNALITÉ"), // PERSONNALITÉ EXTÉRIEURE
		PERSONNEL_DE("PERSONNEL DE"), // PERSONNEL DE LA BIBLIOTHÈQUE
		PERSONNEL_EN("PERSONNEL EN"), // PERSONNEL EN CHARGE DE LA REPROGRAPHIE
		PERSONNELS_D("PERSONNELS D"), // PERSONNELS DE LA BIBLIOTHÈQUE
		POLECOMPET("POLECOMPET"), // POLE DE COMPETITIVITE
		PORTEURPROJ("PORTEURPROJ"), // PORTEUR DU PROJET
		PORTEURPROJA("PORTEURPROJA"), // PORTEUR DU PROJET ADJOINT
		PRESGRDETAB("PRESGRDETAB"), // PRÉSIDENT D'UN GRAND ÉTABLISSEMENT
		PRESIDENT("PRÉSIDENT"), // PRÉSIDENT
		PRESIDENT_DE("PRÉSIDENT DE"), // PRÉSIDENT DE L'UNIVERSITÉ
		PRESIDENT_DU("PRÉSIDENT DU"), // PRÉSIDENT DU HAUT COMITÉ ÉDUCATION
										// ÉCONOMIE
		PRESINSNAPOL("PRESINSNAPOL"), // PRÉSIDENT D'INSTITUT NATIONAL
										// POLYTECHNIQUE
		PRESTATIONS("PRESTATIONS"), // PRESTATIONS
		PRESTRECH("PRESTRECH"), // PARTENAIRE RECHERCHE
		PRESUNIVERSI("PRESUNIVERSI"), // PRÉSIDENT D'UNIVERSITÉ
		PRIME("PRIME"), // PRIME
		PRIME_COMPTA("PRIME COMPTA"), // PRIME COMPTABLE
		PRIME_DE_TRA("PRIME DE TRA"), // PRIME DE TRAVAUX INFORMATIQUES
		PRIME_INFORM("PRIME INFORM"), // PRIME INFORMATIQUE
		PROGRAMPUPIT("PROGRAMPUPIT"), // PROGRAMMEUR ET PUPITREUR
		PROGRAMSYSEX("PROGRAMSYSEX"), // PROGRAMMEUR DE SYSTEME D'EXPLOITATION
		PUPITREUR("PUPITREUR"), // PUPITREUR
		RAPPODESIGNE("RAPPODESIGNE"), // RAPPORTEUR DESIGNE
		RECHERCHE("RECHERCHE"), // RECHERCHE
		REGISAVANREC("REGISAVANREC"), // REGISSEUR DE D'AVANCES/RECETTES
		REGISRECETTE("REGISRECETTE"), // REGISSEUR DE RECETTES
		REGISSEUR_D_("REGISSEUR D'"), // REGISSEUR D'AVANCES
		RELEVANT_INS("RELEVANT INS"), // RELEVANT INSTANCE
		RENSEIGNEMEN("RENSEIGNEMEN"), // RENSEIGNEMENTS COMPLÉMENTAIRES
		REPARTITEUR("REPARTITEUR"), // RÉPARTITEUR
		REPRESETUDIA("REPRESETUDIA"), // REPRÉSENTANT ÉTUDIANT
		REPRGESTPART("REPRGESTPART"), // REPRÉSENTANT GESTIONNAIRE PARTENARIAT
		REPRJURIPART("REPRJURIPART"), // REPRÉSENTANT JURIDIQUE PARTENARIAT
		REPROGRAPHIE("REPROGRAPHIE"), // REPROGRAPHIE
		REPRPRINPART("REPRPRINPART"), // REPRÉSENTANT PRINCIPAL PARTENARIAT
		REPRSECOPART("REPRSECOPART"), // REPRÉSENTANT SECONDAIRE PARTENARIAT
		RESJURIDIQUE("RESJURIDIQUE"), // RESPONSABLE JURIDIQUE
		RESPACCCOMMU("RESPACCCOMMU"), // RESPONSABLE ACCUEIL - COMMUNICATION
		RESPADMIN("RESPADMIN"), // RESPONSABLE ADMINISTRATIF
		RESPBU("RESPBU"), // RESPONSABLE DE BIBLIOTHÈQUE
		RESPCAFETERI("RESPCAFETERI"), // RESPONSABLE DE LA CAFÉTÉRIA
		RESPCECSMOD("RESPCECSMOD"), // RESPONSABLE DE DISCIPLINE - RESPONSABLE
									// CECSMO, DESCB
		RESPDIPLOME("RESPDIPLOME"), // RESPONSABLE DE DIPLOME
		RESPDIPLOMES("RESPDIPLOMES"), // RESPONSABLE DE DIPLÔMES
		RESPDISCALP2("RESPDISCALP2"), // RESPONSABLE DE DISCIPLINE - CALP2
		RESPDISCILIC("RESPDISCILIC"), // RESPONSABLE DE DISCIPLINE - LICENCE
		RESPDISCIRI("RESPDISCIRI"), // RESPONSABLE DE DISCIPLINE - RELATIONS
									// INTERNATIONALES
		RESPDISCI1_2("RESPDISCI1-2"), // RESPONSABLE DE DISCIPLINE - 1ER CYCLE,
										// 2ÈME CYCLE
		RESPENSEIGNE("RESPENSEIGNE"), // RESPONSABLE D'ENSEIGNEMENT
		RESPINFORMAT("RESPINFORMAT"), // RESPONSABLE INFORMATIQUE
		RESPLICENPRO("RESPLICENPRO"), // RESPONSABLE DE LICENCE PROFESSIONNELLE
		RESPMASTER1("RESPMASTER1"), // RESPONSABLE DE MASTER 1
		RESPMETIER("RESPMETIER"), // RESPONSABLE MÉTIER
		RESPONSABLE("RESPONSABLE"), // RESPONSABLE
		RESPOPTION("RESPOPTION"), // RESPONSABLE OPTION
		RESPPEDAFILI("RESPPEDAFILI"), // RESPONSABLE PÉDAGOGIQUE DE FILIÈRES
		RESPPEDASTAG("RESPPEDASTAG"), // RESPONSABLE PÉDAGOGIQUE DES STAGES
		RESPREINORST("RESPREINORST"), // RESPONSABLE DES RELATIONS
										// INTERNATIONALES, ORGANISATION ET
										// SUIVI DE STAGES À L'ÉTRANGER
		RESPRELINETA("RESPRELINETA"), // RESPONSABLE DES RELATIONS
										// INTERNATIONALES POUR L'ÉTABLISSEMENT
		RESPSCIENTIF("RESPSCIENTIF"), // RESPONSABLE SCIENTIFIQUE
		RESPSERINFOR("RESPSERINFOR"), // RESPONSABLE SERVICE INFORMATION
										// ORIENTATION
		RESPSERVINTE("RESPSERVINTE"), // RESPONSABLE DU SERVICE INTÉRIEUR
		RESPSERVTECH("RESPSERVTECH"), // RESPONSABLE DES SERVICES TECHNIQUES
		RESPSTAGE("RESPSTAGE"), // RESPONSABLE DES STAGES
		RESPSTAPS("RESPSTAPS"), // RESPONSABLE DE LA CELLULE STAPS
		SECGENEADJ("SECGENEADJ"), // SECRÉTAIRE GÉNÉRAL(E) ADJOINT(E)
		SECGENERAL("SECGENERAL"), // SECRÉTAIRE GÉNÉRAL(E)
		SECGENEUNIV("SECGENEUNIV"), // SECRÉTAIRE GÉNÉRAL(E) DE L'UNIVERSITÉ
		SECRETAIRE_A("SECRÉTAIRE A"), // SECRÉTAIRE ADMINISTRATIVE
		SERVICE("SERVICE"), // SERVICE
		SERVICE_DE_D("SERVICE DE D"), // SERVICE DE DOCUMENTATION
		SOUS_GROUPE("SOUS-GROUPE "), // SOUS-GROUPE DE MAIL
		STAG__FC("STAG. FC"), // STAGIAIRE FORMATION CONTINUE
		STRUCTURELLE("STRUCTURELLE"), // STRUCTURELLE
		TELEPHONE("TÉLÉPHONE"), // TÉLÉPHONE
		TUTELLEDE("TUTELLEDE"), // TUTELLE DE
		TUTELLEPRINC("TUTELLEPRINC"), // TUTELLE PRINCIPALE
		TUTEUR_ENSEI("TUTEUR ENSEI"), // TUTEUR ENSEIGNANT DE STAGE
		TUTEUR_ENTRE("TUTEUR ENTRE"), // TUTEUR ENTREPRISE DE STAGE
		TYPE_MEM_SR("TYPE_MEM_SR"), // TYPES MEMBRES STRUCTURE RECHERCHE
		TYP_MEM_UNI("TYP_MEM_UNI"), // TYPES MEMBRES UNITE
		VALIDFINANCI("VALIDFINANCI"), // VALIDATEUR FINANCIER
		VALIDJURIDIQ("VALIDJURIDIQ"), // VALIDATEUR JURIDIQUE
		VALIDSCIENTI("VALIDSCIENTI"), // VALIDATEUR SCIENTIFIQUE
		VALIDVALO("VALIDVALO"), // VALIDATEUR VALO
		VEILLE_TECHN("VEILLE TECHN"), // VEILLE TECHNOLOGIQUE
		VICE_PDT("VICE PDT"), // VICE PDT
		VICE_PDT_CHA("VICE PDT CHA"), // VICE PDT CHARGÉ DE MISSION (ÉLU PAR LE
										// CA)
		VICE_PRESIDE("VICE PRÉSIDE"), // VICE PRÉSIDENT (DE L'UN DES 3 CONSEILS
										// D'UNIVERSITÉ: CA; CS; CEVU)
		VP("VP"), // VICE-PRÉSIDENT(E)
		VPCA("VPCA"), // VICE-PRÉSIDENT DU CONSEIL D'ADMINISTRATION (CA)
		VPCEVU("VPCEVU"), // VICE-PRÉSIDENT DU CONSEIL DE LA VIE UNIVERSITAIRE
							// (CEVU)
		VPCS("VPCS"); // VICE-PRÉSIDENT DU CONSEIL SCIENTIFIQUE (CS)

		private EOAssociation instance = null;

		private static EOEditingContext editingContext = EOSharedEditingContext.defaultSharedEditingContext();

		private String code;

		private Liste(String code) {
			this.code = code;
		}

		public static void setEditingContext(EOEditingContext edc) {
			editingContext = edc;
		}

		/**
		 * Renvoie le code de l'association
		 * 
		 * @return le code
		 */
		public String code() {
			return code;
		}

		/**
		 * Renvoie une instance de l'EO
		 * 
		 * @param edc EditingContext dans lequel il faut mettre l'instance
		 * @return l'instance
		 */
		public EOAssociation getInstance(EOEditingContext edc) {
			if (instance == null) {
				instance = EOAssociation.fetchFirstByQualifier(
						editingContext,
						EOAssociation.ASS_CODE.eq(code()));
			}
			return instance.localInstanceIn(edc);
		}

	}
}
