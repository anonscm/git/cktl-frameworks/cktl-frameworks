/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPassageChevron.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPassageChevron extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPassageChevron.class);

	public static final String ENTITY_NAME = "Fwkpers_PassageChevron";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PASSAGE_CHEVRON";


// Attribute Keys
  public static final ERXKey<String> C_CHEVRON = new ERXKey<String>("cChevron");
  public static final ERXKey<String> C_ECHELON = new ERXKey<String>("cEchelon");
  public static final ERXKey<String> C_GRADE = new ERXKey<String>("cGrade");
  public static final ERXKey<String> C_INDICE_BRUT = new ERXKey<String>("cIndiceBrut");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_FERMETURE = new ERXKey<NSTimestamp>("dFermeture");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_OUVERTURE = new ERXKey<NSTimestamp>("dOuverture");
  public static final ERXKey<Integer> DUREE_CHEVRON_ANNEES = new ERXKey<Integer>("dureeChevronAnnees");
  public static final ERXKey<Integer> DUREE_CHEVRON_MOIS = new ERXKey<Integer>("dureeChevronMois");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrade");

	// Attributes


	public static final String C_CHEVRON_KEY = "cChevron";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_INDICE_BRUT_KEY = "cIndiceBrut";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String DUREE_CHEVRON_ANNEES_KEY = "dureeChevronAnnees";
	public static final String DUREE_CHEVRON_MOIS_KEY = "dureeChevronMois";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_CHEVRON_COLKEY = "C_CHEVRON";
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_INDICE_BRUT_COLKEY = "C_INDICE_BRUT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String DUREE_CHEVRON_ANNEES_COLKEY = "DUREE_CHEVRON_ANNEES";
	public static final String DUREE_CHEVRON_MOIS_COLKEY = "DUREE_CHEVRON_MOIS";



	// Relationships
	public static final String TO_GRADE_KEY = "toGrade";



	// Accessors methods
  public String cChevron() {
    return (String) storedValueForKey(C_CHEVRON_KEY);
  }

  public void setCChevron(String value) {
    takeStoredValueForKey(value, C_CHEVRON_KEY);
  }

  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String cGrade() {
    return (String) storedValueForKey(C_GRADE_KEY);
  }

  public void setCGrade(String value) {
    takeStoredValueForKey(value, C_GRADE_KEY);
  }

  public String cIndiceBrut() {
    return (String) storedValueForKey(C_INDICE_BRUT_KEY);
  }

  public void setCIndiceBrut(String value) {
    takeStoredValueForKey(value, C_INDICE_BRUT_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
  }

  public void setDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_KEY);
  }

  public Integer dureeChevronAnnees() {
    return (Integer) storedValueForKey(DUREE_CHEVRON_ANNEES_KEY);
  }

  public void setDureeChevronAnnees(Integer value) {
    takeStoredValueForKey(value, DUREE_CHEVRON_ANNEES_KEY);
  }

  public Integer dureeChevronMois() {
    return (Integer) storedValueForKey(DUREE_CHEVRON_MOIS_KEY);
  }

  public void setDureeChevronMois(Integer value) {
    takeStoredValueForKey(value, DUREE_CHEVRON_MOIS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade)storedValueForKey(TO_GRADE_KEY);
  }

  public void setToGradeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPassageChevron avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPassageChevron createEOPassageChevron(EOEditingContext editingContext, String cChevron
, String cEchelon
, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp dOuverture
, org.cocktail.fwkcktlpersonne.common.metier.EOGrade toGrade			) {
    EOPassageChevron eo = (EOPassageChevron) createAndInsertInstance(editingContext, _EOPassageChevron.ENTITY_NAME);    
		eo.setCChevron(cChevron);
		eo.setCEchelon(cEchelon);
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setDOuverture(dOuverture);
    eo.setToGradeRelationship(toGrade);
    return eo;
  }

  
	  public EOPassageChevron localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPassageChevron)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPassageChevron creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPassageChevron creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPassageChevron object = (EOPassageChevron)createAndInsertInstance(editingContext, _EOPassageChevron.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPassageChevron localInstanceIn(EOEditingContext editingContext, EOPassageChevron eo) {
    EOPassageChevron localInstance = (eo == null) ? null : (EOPassageChevron)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPassageChevron#localInstanceIn a la place.
   */
	public static EOPassageChevron localInstanceOf(EOEditingContext editingContext, EOPassageChevron eo) {
		return EOPassageChevron.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageChevron>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPassageChevron fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPassageChevron fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPassageChevron> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPassageChevron eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPassageChevron)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPassageChevron fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPassageChevron fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPassageChevron> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPassageChevron eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPassageChevron)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPassageChevron fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPassageChevron eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPassageChevron ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPassageChevron fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
