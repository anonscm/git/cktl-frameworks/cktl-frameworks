/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPhotosSav.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPhotosSav extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPhotosSav.class);

	public static final String ENTITY_NAME = "Fwkpers_PhotosSav";
	public static final String ENTITY_TABLE_NAME = "PHOTO.PHOTOS_SAV";


// Attribute Keys
  public static final ERXKey<String> APPLICATION = new ERXKey<String>("application");
  public static final ERXKey<NSData> DATAS_PHOTO = new ERXKey<NSData>("datasPhoto");
  public static final ERXKey<NSTimestamp> DATE_PRISE = new ERXKey<NSTimestamp>("datePrise");
  public static final ERXKey<String> LOGIN = new ERXKey<String>("login");
  public static final ERXKey<Integer> NO_INDIVIDU = new ERXKey<Integer>("noIndividu");
  // Relationship Keys

	// Attributes


	public static final String APPLICATION_KEY = "application";
	public static final String DATAS_PHOTO_KEY = "datasPhoto";
	public static final String DATE_PRISE_KEY = "datePrise";
	public static final String LOGIN_KEY = "login";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String APPLICATION_COLKEY = "APPLICATION";
	public static final String DATAS_PHOTO_COLKEY = "DATAS_PHOTO";
	public static final String DATE_PRISE_COLKEY = "DATE_PRISE";
	public static final String LOGIN_COLKEY = "LOGIN";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";



	// Relationships



	// Accessors methods
  public String application() {
    return (String) storedValueForKey(APPLICATION_KEY);
  }

  public void setApplication(String value) {
    takeStoredValueForKey(value, APPLICATION_KEY);
  }

  public NSData datasPhoto() {
    return (NSData) storedValueForKey(DATAS_PHOTO_KEY);
  }

  public void setDatasPhoto(NSData value) {
    takeStoredValueForKey(value, DATAS_PHOTO_KEY);
  }

  public NSTimestamp datePrise() {
    return (NSTimestamp) storedValueForKey(DATE_PRISE_KEY);
  }

  public void setDatePrise(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PRISE_KEY);
  }

  public String login() {
    return (String) storedValueForKey(LOGIN_KEY);
  }

  public void setLogin(String value) {
    takeStoredValueForKey(value, LOGIN_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }


/**
 * Créer une instance de EOPhotosSav avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPhotosSav createEOPhotosSav(EOEditingContext editingContext, String application
, NSTimestamp datePrise
, String login
, Integer noIndividu
			) {
    EOPhotosSav eo = (EOPhotosSav) createAndInsertInstance(editingContext, _EOPhotosSav.ENTITY_NAME);    
		eo.setApplication(application);
		eo.setDatePrise(datePrise);
		eo.setLogin(login);
		eo.setNoIndividu(noIndividu);
    return eo;
  }

  
	  public EOPhotosSav localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPhotosSav)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPhotosSav creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPhotosSav creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPhotosSav object = (EOPhotosSav)createAndInsertInstance(editingContext, _EOPhotosSav.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPhotosSav localInstanceIn(EOEditingContext editingContext, EOPhotosSav eo) {
    EOPhotosSav localInstance = (eo == null) ? null : (EOPhotosSav)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPhotosSav#localInstanceIn a la place.
   */
	public static EOPhotosSav localInstanceOf(EOEditingContext editingContext, EOPhotosSav eo) {
		return EOPhotosSav.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPhotosSav fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPhotosSav fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPhotosSav> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPhotosSav eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPhotosSav)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPhotosSav fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPhotosSav fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPhotosSav> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPhotosSav eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPhotosSav)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPhotosSav fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPhotosSav eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPhotosSav ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPhotosSav fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
