/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcolesMilitaires.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOEcolesMilitaires extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOEcolesMilitaires.class);

	public static final String ENTITY_NAME = "Fwkpers_EcolesMilitaires";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ECOLES_MILITAIRES";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<String> EM_CODE = new ERXKey<String>("emCode");
  public static final ERXKey<String> EM_LIBELLE = new ERXKey<String>("emLibelle");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "emOrdre";

	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String EM_CODE_KEY = "emCode";
	public static final String EM_LIBELLE_KEY = "emLibelle";

// Attributs non visibles
	public static final String EM_ORDRE_KEY = "emOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String EM_CODE_COLKEY = "EM_CODE";
	public static final String EM_LIBELLE_COLKEY = "EM_LIBELLE";

	public static final String EM_ORDRE_COLKEY = "EM_ORDRE";


	// Relationships



	// Accessors methods
  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public String emCode() {
    return (String) storedValueForKey(EM_CODE_KEY);
  }

  public void setEmCode(String value) {
    takeStoredValueForKey(value, EM_CODE_KEY);
  }

  public String emLibelle() {
    return (String) storedValueForKey(EM_LIBELLE_KEY);
  }

  public void setEmLibelle(String value) {
    takeStoredValueForKey(value, EM_LIBELLE_KEY);
  }


/**
 * Créer une instance de EOEcolesMilitaires avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEcolesMilitaires createEOEcolesMilitaires(EOEditingContext editingContext			) {
    EOEcolesMilitaires eo = (EOEcolesMilitaires) createAndInsertInstance(editingContext, _EOEcolesMilitaires.ENTITY_NAME);    
    return eo;
  }

  
	  public EOEcolesMilitaires localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcolesMilitaires)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcolesMilitaires creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEcolesMilitaires creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOEcolesMilitaires object = (EOEcolesMilitaires)createAndInsertInstance(editingContext, _EOEcolesMilitaires.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEcolesMilitaires localInstanceIn(EOEditingContext editingContext, EOEcolesMilitaires eo) {
    EOEcolesMilitaires localInstance = (eo == null) ? null : (EOEcolesMilitaires)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEcolesMilitaires#localInstanceIn a la place.
   */
	public static EOEcolesMilitaires localInstanceOf(EOEditingContext editingContext, EOEcolesMilitaires eo) {
		return EOEcolesMilitaires.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEcolesMilitaires>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEcolesMilitaires fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEcolesMilitaires fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEcolesMilitaires> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcolesMilitaires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcolesMilitaires)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcolesMilitaires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcolesMilitaires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEcolesMilitaires> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcolesMilitaires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcolesMilitaires)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEcolesMilitaires fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcolesMilitaires eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcolesMilitaires ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcolesMilitaires fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
