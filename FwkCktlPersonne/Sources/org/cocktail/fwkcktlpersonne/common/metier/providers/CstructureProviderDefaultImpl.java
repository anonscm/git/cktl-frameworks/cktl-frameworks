package org.cocktail.fwkcktlpersonne.common.metier.providers;

import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Implementation par defaut reprenant le comportement de base 
 * qui était implémenté à l'origine dans EOStructure
 * @author jlafourc
 *
 */
public class CstructureProviderDefaultImpl implements CstructureProvider {

	/**
	 * {@inheritDoc}
	 */
	public String construireCStructure(EOEditingContext editingContext) {
		return AFinder.clePrimairePour(
				editingContext, 
				EOStructure.ENTITY_NAME, 
				EOStructure.C_STRUCTURE_KEY, 
				EOStructure.SEQ_STRUCTURE_ENTITY_NAME,
				true)
			.toString();
	}

}
