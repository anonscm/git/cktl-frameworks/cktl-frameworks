package org.cocktail.fwkcktlpersonne.common.metier.providers;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Provider utilisé pour générer le
 * cStructure d'un structure
 * 
 * @author jlafourc
 *
 */
public interface CstructureProvider {
	/**
	 * Construction du cStructure
	 * @return un nouveau cStructure
	 */
	String construireCStructure(EOEditingContext editingContext);
}
