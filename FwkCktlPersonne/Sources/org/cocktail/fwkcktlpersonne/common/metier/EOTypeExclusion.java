/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;



public class EOTypeExclusion extends _EOTypeExclusion {

	private static String TYPE_FONCTIONNAIRE = "F";
	private static String TYPE_CONTRACTUEL = "C";
	
    public EOTypeExclusion() {
        super();
    }
    
    public String lTypeExclusion() {
        return (String)storedValueForKey("lTypeExclusion");
    }

    public void setLTypeExclusion(String value) {
        takeStoredValueForKey(value, "lTypeExclusion");
    }

    public String cTypePopTypeExclusion() {
        return (String)storedValueForKey("cTypePopTypeExclusion");
    }

    public void setCTypePopTypeExclusion(String value) {
        takeStoredValueForKey(value, "cTypePopTypeExclusion");
    }

    public String cTypeExclusion() {
        return (String)storedValueForKey("cTypeExclusion");
    }

    public void setCTypeExclusion(String value) {
        takeStoredValueForKey(value, "cTypeExclusion");
    }
    
    public EOTypeAbsence typeAbsence() {
    		return (EOTypeAbsence)storedValueForKey("typeAbsence");
    }
    
    public void setTypeAbsence(EOTypeAbsence value) {
        takeStoredValueForKey(value, "typeAbsence");
    }
   
    // méthodes ajoutées
    public boolean estPourFonctionnaire() {
    		return cTypePopTypeExclusion() != null && cTypePopTypeExclusion().equals(TYPE_FONCTIONNAIRE);
    }
    public void setEstPourFonctionnaire(boolean aBool) {
		if (aBool) {
			setCTypeExclusion(TYPE_FONCTIONNAIRE);
		} else {
			setCTypeExclusion(TYPE_CONTRACTUEL);
		}
	}
    // Interface RecordAvecLibelleEtCode
    public String code() {
    		return cTypeExclusion();
    }
    public String libelle() {
    		return lTypeExclusion();
    }
    
   
}
