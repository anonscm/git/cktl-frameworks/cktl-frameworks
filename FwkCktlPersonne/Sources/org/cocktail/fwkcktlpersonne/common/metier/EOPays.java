/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPays.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class EOPays extends _EOPays implements IPays {

	private static String CODE_PAYS_DEFAUT = null;
	public static final String CODE_PAYS_FRANCE = IPays.CODE_PAYS_FRANCE;
	public static final EOSortOrdering SORT_LL_PAYS = EOSortOrdering.sortOrderingWithKey(EOPays.LL_PAYS_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LL_PAYS_EN = EOSortOrdering.sortOrderingWithKey(EOPays.LL_PAYS_EN_KEY, EOSortOrdering.CompareAscending);

	private static EOPays paysDefautCache = null;

	public EOPays() {
		super();
	}

	/**
	 * @param ec
	 * @return Le code du pays te que defini dans les parametres de GRHUM (Parametre GRHUM_C_PAYS_DEFAUT)
	 */
	public static String getCodePaysDefaut(EOEditingContext ec) {
		if (CODE_PAYS_DEFAUT == null) {
			CODE_PAYS_DEFAUT = EOGrhumParametres.parametrePourCle(ec, "GRHUM_C_PAYS_DEFAUT");
		}
		return CODE_PAYS_DEFAUT;
	}

	/**
	 * @param ec
	 * @return Le pays par defaut (en fonction du parametre GRHUM_C_PAYS_DEFAUT)
	 */
	public static EOPays getPaysDefaut(EOEditingContext ec) {
		if (paysDefautCache == null || paysDefautCache.editingContext() == null || !paysDefautCache.editingContext().equals(ec)) {
			String c = getCodePaysDefaut(ec);
			if (c == null) {
				paysDefautCache = null;
			}
			paysDefautCache = fetchByKeyValue(ec, EOPays.C_PAYS_KEY, c);
		}
		return paysDefautCache;
	}

	/**
	 * @return true si le pays est celui par defaut.
	 */
	public boolean isPaysDefaut() {
		return (cPays() != null && cPays().equals(getCodePaysDefaut(this.editingContext())));
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isFrance() {
		return CODE_PAYS_FRANCE.equals(cPays());
	}
	
	public String getPaysEtIndicatifTel() {
		return toPaysIndicatif().indicatif().intValue() + "  " + llPays();
	}

	/**
	 * @param editingContext
	 * @param s
	 * @return Les pays dont le code ou le nom correspondent a s
	 */
	public static NSArray fetchPaysByCodeOrLibelle(EOEditingContext editingContext, String s) {
		String filtPays = StringCtrl.chaineSansAccents(s, "?");
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + " =%@", new NSArray(new Object[] {
				filtPays
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.LL_PAYS_KEY + " caseInsensitiveLike '*" + filtPays + "*'", null));
		return EOPays.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
				EOPays.SORT_LL_PAYS
		}));

	}

	public static EOPays fetchPaysbyCode(EOEditingContext editingContext, String codePays) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + "=%@", new NSArray(new Object[] {
				codePays
		}));
		return fetchFirstByQualifier(editingContext, qual);
	}

	public static EOPays fetchPaysbyCodeIso(EOEditingContext editingContext, String codeIsoPays) {
		EOQualifier qualifier = ERXQ.likeInsensitive(EOPays.CODE_ISO_KEY, codeIsoPays);
		return fetchFirstByQualifier(editingContext, qualifier);
	}

	@Deprecated
	public static EOPays fecthPaysbyCode(EOEditingContext editingContext, String codePays) {
		return fetchPaysbyCode(editingContext, codePays);
	}

	public static EOPays fetchPaysByIndicatifTelephonique(EOEditingContext editingContext, Number indicatif) {
		EOQualifier qual = new EOKeyValueQualifier(EOPays.TO_PAYS_INDICATIF_KEY + "." + EOPaysIndicatif.INDICATIF_KEY, EOQualifier.QualifierOperatorEqual, indicatif);
		return fetchFirstByQualifier(editingContext, qual);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOQualifier qualValide = new EOOrQualifier(new NSArray(new Object[] {
				new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null), new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, new NSTimestamp())
		}));

		if (qualifier != null) {
			qualValide = new EOAndQualifier(new NSArray(new Object[] {
					qualValide, qualifier
			}));
		}

		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualValide, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		NSArray eoObjects = (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * @param edc editing context de la recherche
	 * @param filtPays filtre sur les pays
	 * @param allPays liste des pays à filtrer ou pas
	 * @param returnAllIfFilterEmpty si true, lorsque le filtre est vide, tous les pays sont renvoyés
	 * @param returnAllIfResultEmpty si true, tous les pays sont renvoyés si le résultat du filtre ne renvoie rien
	 * @return tableau contenant des EOPays répondant aux critères de filtre
	 */
	public static NSArray getFilteredPays(EOEditingContext edc, String filtPays, NSArray allPays, boolean returnAllIfFilterEmpty, boolean returnAllIfResultEmpty) {
		if (allPays == null) {
			allPays = EOPays.fetchAll(edc, new NSArray(new Object[] {
					EOPays.SORT_LL_PAYS
			}));
		}
			

		if (filtPays == null || filtPays.trim().length() == 0) {
			if (returnAllIfFilterEmpty) {
				return allPays;
			} else {
				return NSArray.EmptyArray;
			}
		}
		filtPays = filtPays.trim();
		NSMutableArray quals = new NSMutableArray();
		filtPays = StringCtrl.chaineSansAccents(filtPays, "?");
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + " =%@", new NSArray(new Object[] {
				filtPays
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.LL_PAYS_KEY + " caseInsensitiveLike '*" + filtPays + "*'", null));
		EOQualifier qual = new EOOrQualifier(quals);
		NSArray res = EOQualifier.filteredArrayWithQualifier(allPays, qual);
		if (res.count() == 0 && returnAllIfResultEmpty) {
			return allPays;
		}
		return res;

	}
	/**
	 * @param edc editing context de la recherche
	 * @param filtPays filtre sur les pays
	 * @param allPays liste des pays à filtrer ou pas
	 * @param returnAllIfFilterEmpty si true, lorsque le filtre est vide, tous les pays sont renvoyés
	 * @param returnAllIfResultEmpty si true, tous les pays sont renvoyés si le résultat du filtre ne renvoie rien
	 * @return tableau contenant des EOPays répondant aux critères de filtre
	 */
	public static NSArray getFilteredPaysActuels(EOEditingContext edc, String filtPays, NSArray allPays, boolean returnAllIfFilterEmpty, boolean returnAllIfResultEmpty) {
		if (allPays == null) {
			allPays = EOPays.fetchAll(edc, new NSArray(new Object[] {
					EOPays.SORT_LL_PAYS
			}));
		}
		
		
		if (filtPays == null || filtPays.trim().length() == 0) {
			if (returnAllIfFilterEmpty) {
				return allPays;
			} else {
				return NSArray.EmptyArray;
			}
		}
		filtPays = filtPays.trim();
		NSMutableArray quals = new NSMutableArray();
//		quals.addObject(new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null));
		filtPays = StringCtrl.chaineSansAccents(filtPays, "?");
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + " =%@", new NSArray(new Object[] {
				filtPays
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPays.LL_PAYS_KEY + " caseInsensitiveLike '*" + filtPays + "*'", null));
		EOQualifier qual = new EOOrQualifier(quals);
		qual = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qual,
				new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null)
		}));
		NSArray res = EOQualifier.filteredArrayWithQualifier(allPays, qual);
		if (res.count() == 0 && returnAllIfResultEmpty) {
			return allPays;
		}
		return res;
		
	}

	/**
	 * Méthode qui retourne un tableau de pays
	 * @param edc editing context de la recherche
	 * @param filtPays filtre sur les pays
	 * @param allPays liste de pays que l'on souhaite filtrer
	 * @return tableau contenant des EOPays répondant aux critères de filtre
	 */
	public static NSArray getFilteredPays(EOEditingContext edc, String filtPays, NSArray allPays) {
		return getFilteredPays(edc, filtPays, allPays, false, false);

	}
	
	/**
	 * Retourne une liste de pays pour une date de filtrage donnée
	 * @param edc Editing Context de la recherche
	 * @param filtPays filtre sur les pays
	 * @param allPays liste des pays
	 * @param dateFiltrage date pour filtrer sur les validités
	 * @return un tableau filtré par date contenant les pays
	 */
	public static NSArray getFilteredPaysByDate(EOEditingContext edc, String filtPays, NSArray allPays, NSTimestamp dateFiltrage) {
		EOQualifier qualifierTemporel;
    	EOQualifier qualDateFin;
    	final EOQualifier QUAL_DATE_FIN_NULLE = new EOKeyValueQualifier(EOPays.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
    	
    	// Tous les pays qui ont une date de fin de validité à nul ne peuvent être retirés
    	qualDateFin = ERXQ.greaterThanOrEqualTo(EOPays.D_FIN_VAL_KEY, dateFiltrage);
		qualifierTemporel = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				QUAL_DATE_FIN_NULLE, qualDateFin
		}));

		return EOPays.fetchAll(edc, qualifierTemporel, new NSArray(EOPays.SORT_LL_PAYS));

	}
	
	public String getCode() {
		return cPays();
	}
}
