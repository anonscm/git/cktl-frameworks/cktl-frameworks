/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;

public class EOGrhumParametresType extends _EOGrhumParametresType {
	
	/**
	 *  @author alainmalaplate
	 */
	private static final long serialVersionUID = 1123456789L;
	// Ensemble des constantes de type des paramètres
	// Au 22/06/11, il y a 21 types différents.
	public static final String valeurNumerique = "VALEUR_NUMERIQUE";
	public static final String codeActivation= "CODE_ACTIVATION";
	public static final String nomDomaine = "DOMAINE";
	public static final String uRL ="URL";
	public static final String html = "HTML";
	public static final String nomClasse = "CLASSNAME";
	public static final String listeCaracteres = "CHARACTERS_LIST";
	public static final String chemin = "PATH";
	public static final String texteLibre = "FREE_TEXT";
	public static final String anneeUniversitaire = "SCHOOL_YEAR";
	public static final String tcpip ="TCPIP";
	public static final String periodesValidite ="VALIDITY_PERIODS";
	public static final String fuseauHoraire = "TIMEZONE";
	public static final String idPersonnel ="PERS_ID";
	public static final String cStructure = "C_STRUCTURE";
	public static final String liste ="LIST";
	public static final String identifiant ="LOGIN";
	public static final String messageElectronique ="EMAIL";
	public static final String dateFormatee ="DATE";
	public static final String clefTable ="ID";
	public static final String serveur ="SERVEUR";

    public EOGrhumParametresType() {
        super();
    }

}
