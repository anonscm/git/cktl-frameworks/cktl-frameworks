/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.text.DecimalFormat;
import java.util.Enumeration;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOEffectifStructure extends _EOEffectifStructure {
	public static final EOSortOrdering SORT_CODE_ANNEE_DESC = EOSortOrdering.sortOrderingWithKey(EOEffectifStructure.CODE_ANNEE_KEY, EOSortOrdering.CompareDescending);

	public EOEffectifStructure() {
		super();
	}

	/**
	 * Peut etre appele a partir des factories. Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		try {
			trimAllString();
			checkContraintesObligatoires();
			checkContraintesLongueursMax();

			//S'assurer que codeAnnee est bien numerique et le mettre au bon format
			try {
				Integer res = Integer.valueOf(codeAnnee());
				setCodeAnnee(new DecimalFormat("00").format(res.intValue()));
			} catch (NumberFormatException e) {
				throw new NSValidation.ValidationException("La valeur saisie pour l'annee n'est pas correcte");
			}

			//Verifier que la meme annee n'est pas saisie en double
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOEffectifStructure.CODE_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, codeAnnee()));
			EOQualifier qual = new EOAndQualifier(quals);

			NSArray res = toStructure().toEffectifStructures(qual);
			if (res.count() > 0) {
				Enumeration en = res.objectEnumerator();
				while (en.hasMoreElements()) {
					EOEffectifStructure object = (EOEffectifStructure) en.nextElement();
					if (!object.globalID().equals(globalID())) {
						throw new NSValidation.ValidationException("Vous ne pouvez pas definir plusieurs fois l'effectif pour l'annee " + codeAnnee());
					}
				}
			}
			super.validateObjectMetier();
		} catch (NSValidation.ValidationException e) {
			System.err.println(this);
			throw e;
		}

	}

}
