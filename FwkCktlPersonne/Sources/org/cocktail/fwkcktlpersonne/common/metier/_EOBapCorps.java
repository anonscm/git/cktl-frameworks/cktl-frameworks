/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBapCorps.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOBapCorps extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOBapCorps.class);

	public static final String ENTITY_NAME = "FwkPers_BapCorps";
	public static final String ENTITY_TABLE_NAME = "GRHUM.BAP_CORPS";


// Attribute Keys
  public static final ERXKey<String> C_BAP = new ERXKey<String>("cBap");
  public static final ERXKey<String> C_BAP_FAMILLE = new ERXKey<String>("cBapFamille");
  public static final ERXKey<String> C_CORPS = new ERXKey<String>("cCorps");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LC_BAP_CORPS = new ERXKey<String>("lcBapCorps");
  public static final ERXKey<String> LL_BAP_CORPS = new ERXKey<String>("llBapCorps");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap> TO_BAP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBap>("toBap");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> TOS_EMPLOI_TYPE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>("tosEmploiType");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TOS_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("tosReferensEmplois");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cBapCorps";

	public static final String C_BAP_KEY = "cBap";
	public static final String C_BAP_FAMILLE_KEY = "cBapFamille";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_BAP_CORPS_KEY = "lcBapCorps";
	public static final String LL_BAP_CORPS_KEY = "llBapCorps";

// Attributs non visibles
	public static final String C_BAP_CORPS_KEY = "cBapCorps";

//Colonnes dans la base de donnees
	public static final String C_BAP_COLKEY = "C_BAP";
	public static final String C_BAP_FAMILLE_COLKEY = "C_BAP_FAMILLE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LC_BAP_CORPS_COLKEY = "LC_BAP_CORPS";
	public static final String LL_BAP_CORPS_COLKEY = "LL_BAP_CORPS";

	public static final String C_BAP_CORPS_COLKEY = "C_BAP_CORPS";


	// Relationships
	public static final String TO_BAP_KEY = "toBap";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TOS_EMPLOI_TYPE_KEY = "tosEmploiType";
	public static final String TOS_REFERENS_EMPLOIS_KEY = "tosReferensEmplois";



	// Accessors methods
  public String cBap() {
    return (String) storedValueForKey(C_BAP_KEY);
  }

  public void setCBap(String value) {
    takeStoredValueForKey(value, C_BAP_KEY);
  }

  public String cBapFamille() {
    return (String) storedValueForKey(C_BAP_FAMILLE_KEY);
  }

  public void setCBapFamille(String value) {
    takeStoredValueForKey(value, C_BAP_FAMILLE_KEY);
  }

  public String cCorps() {
    return (String) storedValueForKey(C_CORPS_KEY);
  }

  public void setCCorps(String value) {
    takeStoredValueForKey(value, C_CORPS_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lcBapCorps() {
    return (String) storedValueForKey(LC_BAP_CORPS_KEY);
  }

  public void setLcBapCorps(String value) {
    takeStoredValueForKey(value, LC_BAP_CORPS_KEY);
  }

  public String llBapCorps() {
    return (String) storedValueForKey(LL_BAP_CORPS_KEY);
  }

  public void setLlBapCorps(String value) {
    takeStoredValueForKey(value, LL_BAP_CORPS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOBap toBap() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOBap)storedValueForKey(TO_BAP_KEY);
  }

  public void setToBapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBap value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BAP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey(TO_CORPS_KEY);
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)storedValueForKey(TOS_EMPLOI_TYPE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType(EOQualifier qualifier) {
    return tosEmploiType(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> results;
      results = tosEmploiType();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
  }

  public void removeFromTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType createTosEmploiTypeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_EmploiType");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_EMPLOI_TYPE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType) eo;
  }

  public void deleteTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosEmploiTypeRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> objects = tosEmploiType().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosEmploiTypeRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)storedValueForKey(TOS_REFERENS_EMPLOIS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois(EOQualifier qualifier) {
    return tosReferensEmplois(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> tosReferensEmplois(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> results;
      results = tosReferensEmplois();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
  }

  public void removeFromTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois createTosReferensEmploisRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_ReferensEmplois");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REFERENS_EMPLOIS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois) eo;
  }

  public void deleteTosReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REFERENS_EMPLOIS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosReferensEmploisRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> objects = tosReferensEmplois().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosReferensEmploisRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOBapCorps avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBapCorps createEOBapCorps(EOEditingContext editingContext			) {
    EOBapCorps eo = (EOBapCorps) createAndInsertInstance(editingContext, _EOBapCorps.ENTITY_NAME);    
    return eo;
  }

  
	  public EOBapCorps localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBapCorps)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBapCorps creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBapCorps creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOBapCorps object = (EOBapCorps)createAndInsertInstance(editingContext, _EOBapCorps.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOBapCorps localInstanceIn(EOEditingContext editingContext, EOBapCorps eo) {
    EOBapCorps localInstance = (eo == null) ? null : (EOBapCorps)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBapCorps#localInstanceIn a la place.
   */
	public static EOBapCorps localInstanceOf(EOEditingContext editingContext, EOBapCorps eo) {
		return EOBapCorps.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBapCorps>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBapCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBapCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBapCorps> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBapCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBapCorps)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBapCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBapCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBapCorps> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBapCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBapCorps)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBapCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBapCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBapCorps ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBapCorps fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
