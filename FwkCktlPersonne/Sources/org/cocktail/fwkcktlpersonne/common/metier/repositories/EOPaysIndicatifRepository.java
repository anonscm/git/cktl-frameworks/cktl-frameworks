package org.cocktail.fwkcktlpersonne.common.metier.repositories;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPaysIndicatif;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

/**
 * 
 * Implémentation EOF.
 * 
 * @author Alexis Tual
 *
 */
public class EOPaysIndicatifRepository implements IPaysIndicatifRepository {

    private IPaysIndicatif defaultPaysIndicatif;
    private EOEditingContext editingContext;
    
    /**
     * @param editingContext l'ec
     */
    public EOPaysIndicatifRepository(EOEditingContext editingContext) {
        this.editingContext = editingContext;
    }
    
    /**
     * {@inheritDoc}
     */
    public IPaysIndicatif defaultPaysIndicatif() {
        if (defaultPaysIndicatif == null) {
            EOQualifier qual = new EOKeyValueQualifier(EOPaysIndicatif.C_PAYS_KEY, EOQualifier.QualifierOperatorEqual, (EOPays.getPaysDefaut(editingContext).cPays()));
            defaultPaysIndicatif =  EOPaysIndicatif.fetchByQualifier(editingContext, qual);
            if (defaultPaysIndicatif == null) {
                throw new IllegalStateException("L'indicatif par défaut n'a pu être trouvé");
            }
        }
        return defaultPaysIndicatif;
    }
    
    
}
