package org.cocktail.fwkcktlpersonne.common.metier.repositories;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPaysIndicatif;

/**
 * 
 * Recuperation des indicatifs de pays
 * 
 * @author Alexis Tual
 *
 */
public interface IPaysIndicatifRepository {

    /**
     * @return l'indicatif du pays par défaut
     */
    IPaysIndicatif defaultPaysIndicatif();
    
}
