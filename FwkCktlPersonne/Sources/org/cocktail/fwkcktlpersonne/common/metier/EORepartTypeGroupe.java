/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORepartTypeGroupe.java
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartTypeGroupe;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org> Affectation d'un groupe
 *         a un type de groupe. Regles
 *         <ul>
 *         <li>Si type de groupe S : l'utilisateur doit etre un GRHUM_CREATEUR</li>
 *         <li>Si type de groupe RE : l'utilisateur doit etre un GRHUM_CREATEUR</li>
 *         </ul>
 */
public class EORepartTypeGroupe extends _EORepartTypeGroupe implements IRepartTypeGroupe {
	public static final EOQualifier QUAL_TYPE_GROUPE_G = new EOKeyValueQualifier(TGRP_CODE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_G);

	public EORepartTypeGroupe() {
		super();
	}
	
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		checkUsers();
		super.validateForInsert();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		checkUsers();
		super.validateForDelete();
	}

	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		setDModification(AUtils.now());
		checkContraintesObligatoires();
		super.validateObjectMetier();
	}

	public void initAvecStructureEtType(EOStructure structure, String type) {
		setCStructure(structure.cStructure());
		setTgrpCode(type);
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDCreation(AUtils.now());
		setDModification(AUtils.now());
	}

	/**
	 * Retourne la liste des types groupes de l'&eacute;tablissement
	 */
	//TODO voir si utilisee
	public static NSArray rechercherTypesGroupesPourEtablissement(EOEditingContext editingContext) {
		EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cStructure = %@", new NSArray(etablissement.cStructure()));
		EOFetchSpecification fs = new EOFetchSpecification("RepartTypeGroupe", qualifier, null);
		NSArray result = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray qualifiers = new NSMutableArray(result.count());
		java.util.Enumeration e = result.objectEnumerator();
		while (e.hasMoreElements()) {
			EORepartTypeGroupe repart = (EORepartTypeGroupe) e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tgrpCode = %@", new NSArray(repart.tgrpCode())));
		}
		qualifier = new EOOrQualifier(qualifiers);
		fs = new EOFetchSpecification("TypeGroupe", qualifier, new NSArray(EOSortOrdering.sortOrderingWithKey("tgrpLibelle", EOSortOrdering.CompareAscending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}

	public void checkUsers() throws NSValidation.ValidationException {
		if (persIdCreation() == null) {
			setPersIdCreation(persIdModification());
		}
		if (hasTemporaryGlobalID()) {
			if (persIdModification() == null) {
				throw new NSValidation.ValidationException("La reference au modificateur (persIdModification) est obligatoire pour l'affectation / suppression d' un type de groupe.");
			}
			checkDroitCreation(persIdCreation());
		} else {
			if (persIdModification() != null) {
				checkDroitModification(persIdModification());
			}
		}
	}

	public void checkDroitModification(Integer persId) throws NSValidation.ValidationException {
		if (tgrpCode() != null && EOTypeGroupe.TYPE_GROUPES_RESERVES_SUPERADMIN.indexOfObject(tgrpCode()) != NSArray.NotFound) {
			PersonneApplicationUser appUser = new PersonneApplicationUser(editingContext(), persId);
			if (!appUser.hasDroitGrhumCreateur()) {
				throw new NSValidation.ValidationException("Le type de groupe " + tgrpCode() + " ne peut etre affecté à un groupe que par un utilisateur Super Administrateur (GRHUM_CREATEUR)");
			}
		}
	}

	public void checkDroitCreation(Integer persId) throws NSValidation.ValidationException {
		checkDroitModification(persId);
	}

}
