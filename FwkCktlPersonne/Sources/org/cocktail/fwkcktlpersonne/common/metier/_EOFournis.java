/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFournis.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOFournis extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOFournis.class);

	public static final String ENTITY_NAME = "Fwkpers_Fournis";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FOURNIS_ULR";


// Attribute Keys
  public static final ERXKey<Integer> CPT_ORDRE = new ERXKey<Integer>("cptOrdre");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> FOU_CODE = new ERXKey<String>("fouCode");
  public static final ERXKey<NSTimestamp> FOU_DATE = new ERXKey<NSTimestamp>("fouDate");
  public static final ERXKey<String> FOU_ETRANGER = new ERXKey<String>("fouEtranger");
  public static final ERXKey<String> FOU_MARCHE = new ERXKey<String>("fouMarche");
  public static final ERXKey<String> FOU_TYPE = new ERXKey<String>("fouType");
  public static final ERXKey<String> FOU_VALIDE = new ERXKey<String>("fouValide");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresse");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toCompte");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDUS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividus");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_RIBS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toRibs");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructures");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> TO_UTILISATEUR = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("toUtilisateur");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis> TO_VALIDE_FOURNIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis>("toValideFournis");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_DATE_KEY = "fouDate";
	public static final String FOU_ETRANGER_KEY = "fouEtranger";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String AGT_ORDRE_KEY = "agtOrdre";
	public static final String ADR_ORDRE_KEY = "adrOrdre";

//Colonnes dans la base de donnees
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_DATE_COLKEY = "FOU_DATE";
	public static final String FOU_ETRANGER_COLKEY = "FOU_ETRANGER";
	public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String AGT_ORDRE_COLKEY = "AGT_ORDRE";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";


	// Relationships
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_COMPTE_KEY = "toCompte";
	public static final String TO_INDIVIDUS_KEY = "toIndividus";
	public static final String TO_RIBS_KEY = "toRibs";
	public static final String TO_STRUCTURES_KEY = "toStructures";
	public static final String TO_UTILISATEUR_KEY = "toUtilisateur";
	public static final String TO_VALIDE_FOURNIS_KEY = "toValideFournis";



	// Accessors methods
  public Integer cptOrdre() {
    return (Integer) storedValueForKey(CPT_ORDRE_KEY);
  }

  public void setCptOrdre(Integer value) {
    takeStoredValueForKey(value, CPT_ORDRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String fouCode() {
    return (String) storedValueForKey(FOU_CODE_KEY);
  }

  public void setFouCode(String value) {
    takeStoredValueForKey(value, FOU_CODE_KEY);
  }

  public NSTimestamp fouDate() {
    return (NSTimestamp) storedValueForKey(FOU_DATE_KEY);
  }

  public void setFouDate(NSTimestamp value) {
    takeStoredValueForKey(value, FOU_DATE_KEY);
  }

  public String fouEtranger() {
    return (String) storedValueForKey(FOU_ETRANGER_KEY);
  }

  public void setFouEtranger(String value) {
    takeStoredValueForKey(value, FOU_ETRANGER_KEY);
  }

  public String fouMarche() {
    return (String) storedValueForKey(FOU_MARCHE_KEY);
  }

  public void setFouMarche(String value) {
    takeStoredValueForKey(value, FOU_MARCHE_KEY);
  }

  public String fouType() {
    return (String) storedValueForKey(FOU_TYPE_KEY);
  }

  public void setFouType(String value) {
    takeStoredValueForKey(value, FOU_TYPE_KEY);
  }

  public String fouValide() {
    return (String) storedValueForKey(FOU_VALIDE_KEY);
  }

  public void setFouValide(String value) {
    takeStoredValueForKey(value, FOU_VALIDE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey(TO_ADRESSE_KEY);
  }

  public void setToAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte)storedValueForKey(TO_COMPTE_KEY);
  }

  public void setToCompteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompte oldValue = toCompte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur toUtilisateur() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(TO_UTILISATEUR_KEY);
  }

  public void setToUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = toUtilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis toValideFournis() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis)storedValueForKey(TO_VALIDE_FOURNIS_KEY);
  }

  public void setToValideFournisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOValideFournis oldValue = toValideFournis();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_VALIDE_FOURNIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_VALIDE_FOURNIS_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)storedValueForKey(TO_INDIVIDUS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier) {
    return toIndividus(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier, boolean fetch) {
    return toIndividus(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.TO_FOURNISS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toIndividus();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
  }

  public void removeFromToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createToIndividusRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_INDIVIDUS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToIndividusRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> objects = toIndividus().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToIndividusRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib> toRibs() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib>)storedValueForKey(TO_RIBS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib> toRibs(EOQualifier qualifier) {
    return toRibs(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib> toRibs(EOQualifier qualifier, boolean fetch) {
    return toRibs(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib> toRibs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORib.TO_FOURNIS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORib.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRibs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORib>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRibsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
  }

  public void removeFromToRibsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORib createToRibsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Rib");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RIBS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORib) eo;
  }

  public void deleteToRibsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RIBS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRibsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORib> objects = toRibs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRibsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey(TO_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier) {
    return toStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier, boolean fetch) {
    return toStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOStructure.TO_FOURNISS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
  }

  public void removeFromToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createToStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> objects = toStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToStructuresRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFournis avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFournis createEOFournis(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String fouCode
, NSTimestamp fouDate
, String fouEtranger
, String fouMarche
, String fouType
, String fouValide
, Integer persId
, org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse			) {
    EOFournis eo = (EOFournis) createAndInsertInstance(editingContext, _EOFournis.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFouCode(fouCode);
		eo.setFouDate(fouDate);
		eo.setFouEtranger(fouEtranger);
		eo.setFouMarche(fouMarche);
		eo.setFouType(fouType);
		eo.setFouValide(fouValide);
		eo.setPersId(persId);
    eo.setToAdresseRelationship(toAdresse);
    return eo;
  }

  
	  public EOFournis localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFournis)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournis creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournis creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOFournis object = (EOFournis)createAndInsertInstance(editingContext, _EOFournis.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFournis localInstanceIn(EOEditingContext editingContext, EOFournis eo) {
    EOFournis localInstance = (eo == null) ? null : (EOFournis)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFournis#localInstanceIn a la place.
   */
	public static EOFournis localInstanceOf(EOEditingContext editingContext, EOFournis eo) {
		return EOFournis.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFournis fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFournis fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFournis> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFournis)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFournis> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFournis eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFournis)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFournis fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFournis eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFournis ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFournis fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
