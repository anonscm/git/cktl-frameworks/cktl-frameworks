/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGroupeDynamique.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGroupeDynamique extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGroupeDynamique.class);

	public static final String ENTITY_NAME = "Fwkpers_GroupeDynamique";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GROUPE_DYNAMIQUE";


// Attribute Keys
  public static final ERXKey<String> GRPD_DESCRIPTION = new ERXKey<String>("grpdDescription");
  public static final ERXKey<String> GRPD_LC = new ERXKey<String>("grpdLc");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> TO_GROUPE_DYNAMIQUE_PERSONNES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne>("toGroupeDynamiquePersonnes");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle> TO_REGLE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORegle>("toRegle");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "grpdId";

	public static final String GRPD_DESCRIPTION_KEY = "grpdDescription";
	public static final String GRPD_LC_KEY = "grpdLc";

// Attributs non visibles
	public static final String GRPD_ID_KEY = "grpdId";
	public static final String R_ID_KEY = "rId";

//Colonnes dans la base de donnees
	public static final String GRPD_DESCRIPTION_COLKEY = "GRPD_DESCRIPTION";
	public static final String GRPD_LC_COLKEY = "GRPD_LC";

	public static final String GRPD_ID_COLKEY = "GRPD_ID";
	public static final String R_ID_COLKEY = "R_ID";


	// Relationships
	public static final String TO_GROUPE_DYNAMIQUE_PERSONNES_KEY = "toGroupeDynamiquePersonnes";
	public static final String TO_REGLE_KEY = "toRegle";



	// Accessors methods
  public String grpdDescription() {
    return (String) storedValueForKey(GRPD_DESCRIPTION_KEY);
  }

  public void setGrpdDescription(String value) {
    takeStoredValueForKey(value, GRPD_DESCRIPTION_KEY);
  }

  public String grpdLc() {
    return (String) storedValueForKey(GRPD_LC_KEY);
  }

  public void setGrpdLc(String value) {
    takeStoredValueForKey(value, GRPD_LC_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORegle toRegle() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORegle)storedValueForKey(TO_REGLE_KEY);
  }

  public void setToRegleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORegle value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORegle oldValue = toRegle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGLE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> toGroupeDynamiquePersonnes() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne>)storedValueForKey(TO_GROUPE_DYNAMIQUE_PERSONNES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> toGroupeDynamiquePersonnes(EOQualifier qualifier) {
    return toGroupeDynamiquePersonnes(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> toGroupeDynamiquePersonnes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> results;
      results = toGroupeDynamiquePersonnes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToGroupeDynamiquePersonnesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUE_PERSONNES_KEY);
  }

  public void removeFromToGroupeDynamiquePersonnesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUE_PERSONNES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne createToGroupeDynamiquePersonnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_GroupeDynamiquePersonne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GROUPE_DYNAMIQUE_PERSONNES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne) eo;
  }

  public void deleteToGroupeDynamiquePersonnesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GROUPE_DYNAMIQUE_PERSONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGroupeDynamiquePersonnesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne> objects = toGroupeDynamiquePersonnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGroupeDynamiquePersonnesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGroupeDynamique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGroupeDynamique createEOGroupeDynamique(EOEditingContext editingContext, String grpdDescription
, String grpdLc
, org.cocktail.fwkcktlpersonne.common.metier.EORegle toRegle			) {
    EOGroupeDynamique eo = (EOGroupeDynamique) createAndInsertInstance(editingContext, _EOGroupeDynamique.ENTITY_NAME);    
		eo.setGrpdDescription(grpdDescription);
		eo.setGrpdLc(grpdLc);
    eo.setToRegleRelationship(toRegle);
    return eo;
  }

  
	  public EOGroupeDynamique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGroupeDynamique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGroupeDynamique creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGroupeDynamique creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGroupeDynamique object = (EOGroupeDynamique)createAndInsertInstance(editingContext, _EOGroupeDynamique.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGroupeDynamique localInstanceIn(EOEditingContext editingContext, EOGroupeDynamique eo) {
    EOGroupeDynamique localInstance = (eo == null) ? null : (EOGroupeDynamique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGroupeDynamique#localInstanceIn a la place.
   */
	public static EOGroupeDynamique localInstanceOf(EOEditingContext editingContext, EOGroupeDynamique eo) {
		return EOGroupeDynamique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGroupeDynamique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGroupeDynamique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGroupeDynamique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGroupeDynamique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGroupeDynamique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGroupeDynamique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGroupeDynamique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGroupeDynamique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGroupeDynamique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGroupeDynamique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGroupeDynamique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGroupeDynamique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGroupeDynamique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGroupeDynamique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
