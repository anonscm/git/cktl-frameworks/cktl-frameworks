/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeDechargeService.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOTypeDechargeService extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOTypeDechargeService.class);

	public static final String ENTITY_NAME = "Fwkpers_TypeDechargeService";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_DECHARGE_SERVICE";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  public static final ERXKey<String> REFERENCE_REGLEMENTAIRE = new ERXKey<String>("referenceReglementaire");
  public static final ERXKey<String> TEM_H_COMP = new ERXKey<String>("temHComp");
  public static final ERXKey<String> TEM_PARAMETRAGE = new ERXKey<String>("temParametrage");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cTypeDecharge";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_LONG_KEY = "libelleLong";
	public static final String REFERENCE_REGLEMENTAIRE_KEY = "referenceReglementaire";
	public static final String TEM_H_COMP_KEY = "temHComp";
	public static final String TEM_PARAMETRAGE_KEY = "temParametrage";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "D_CREATION";
	public static final String DATE_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String DATE_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DATE_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String LIBELLE_COURT_COLKEY = "LC_TYPE_DECHARGE";
	public static final String LIBELLE_LONG_COLKEY = "LL_TYPE_DECHARGE";
	public static final String REFERENCE_REGLEMENTAIRE_COLKEY = "REF_REGLEMENTAIRE";
	public static final String TEM_H_COMP_COLKEY = "TEM_HCOMP";
	public static final String TEM_PARAMETRAGE_COLKEY = "TEM_PARAM";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_TYPE_DECHARGE_COLKEY = "C_TYPE_DECHARGE";


	// Relationships



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    takeStoredValueForKey(value, LIBELLE_COURT_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    takeStoredValueForKey(value, LIBELLE_LONG_KEY);
  }

  public String referenceReglementaire() {
    return (String) storedValueForKey(REFERENCE_REGLEMENTAIRE_KEY);
  }

  public void setReferenceReglementaire(String value) {
    takeStoredValueForKey(value, REFERENCE_REGLEMENTAIRE_KEY);
  }

  public String temHComp() {
    return (String) storedValueForKey(TEM_H_COMP_KEY);
  }

  public void setTemHComp(String value) {
    takeStoredValueForKey(value, TEM_H_COMP_KEY);
  }

  public String temParametrage() {
    return (String) storedValueForKey(TEM_PARAMETRAGE_KEY);
  }

  public void setTemParametrage(String value) {
    takeStoredValueForKey(value, TEM_PARAMETRAGE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }


/**
 * Créer une instance de EOTypeDechargeService avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeDechargeService createEOTypeDechargeService(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, NSTimestamp dateOuverture
, String libelleCourt
, String libelleLong
, String temHComp
, String temValide
			) {
    EOTypeDechargeService eo = (EOTypeDechargeService) createAndInsertInstance(editingContext, _EOTypeDechargeService.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setDateOuverture(dateOuverture);
		eo.setLibelleCourt(libelleCourt);
		eo.setLibelleLong(libelleLong);
		eo.setTemHComp(temHComp);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOTypeDechargeService localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeDechargeService)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeDechargeService creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeDechargeService creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOTypeDechargeService object = (EOTypeDechargeService)createAndInsertInstance(editingContext, _EOTypeDechargeService.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeDechargeService localInstanceIn(EOEditingContext editingContext, EOTypeDechargeService eo) {
    EOTypeDechargeService localInstance = (eo == null) ? null : (EOTypeDechargeService)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeDechargeService#localInstanceIn a la place.
   */
	public static EOTypeDechargeService localInstanceOf(EOEditingContext editingContext, EOTypeDechargeService eo) {
		return EOTypeDechargeService.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeDechargeService>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeDechargeService fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeDechargeService fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeDechargeService> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeDechargeService eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeDechargeService)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeDechargeService fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeDechargeService fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeDechargeService> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeDechargeService eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeDechargeService)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeDechargeService fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeDechargeService eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeDechargeService ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeDechargeService fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
