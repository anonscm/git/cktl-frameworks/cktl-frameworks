/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EORegleOperateur extends _EORegleOperateur {

    public static final String STR_ID_MEMBRE = "membre";
    public static final String STR_ID_ROLE = "role";
    public static final String STR_ID_ROLEDANSGROUPE = "roledansgroupe";
    
    private static EORegleOperateur RegleOperateurMembre;
    private static EORegleOperateur RegleOperateurRole;
    private static EORegleOperateur RegleOperateurRoleDansGroupe;
    
    public EORegleOperateur() {
        super();
    }
    
    public boolean isOperateurRoleDansGroupe() {
        return STR_ID_ROLEDANSGROUPE.equals(roStrId());
    }
    
    public boolean isOperateurMembre() {
        return STR_ID_MEMBRE.equals(roStrId());
    }
    
    public boolean isOperateurRole() {
        return STR_ID_ROLE.equals(roStrId());
    }
    
    public boolean isOperateurSimple() {
        return !isOperateurRoleDansGroupe();
    }
    
    public EOQualifier qualifier(EORegle regle) {
        if (regle.rValue() == null)
            throw new ValidationException("Une valeur doit être renseignée pour la règle " + regle);
        if (isOperateurMembre())
            return ERXQ.equals(EOIndividu.TO_REPART_STRUCTURES_KEY + "." + 
                        EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." +
                        EOStructure.C_STRUCTURE_KEY, regle.rValue());
        else if (isOperateurRole())
            return ERXQ.equals(EOIndividu.TO_REPART_ASSOCIATIONS_KEY + "." + EORepartAssociation.ASS_ID_KEY, Integer.valueOf(regle.rValue()));
        else if (isOperateurRoleDansGroupe()) {
            if (regle.rValue2() == null)
                throw new ValidationException("Une deuxième valeur doit être renseignée pour la règle " + regle);
            return ERXQ.equals(EOIndividu.TO_REPART_ASSOCIATIONS_KEY + "." + EORepartAssociation.ASS_ID_KEY, Integer.valueOf(regle.rValue()))
                   .and(ERXQ.equals(EOIndividu.TO_REPART_ASSOCIATIONS_KEY + "." + EORepartAssociation.C_STRUCTURE_KEY, regle.rValue2()));
        }
        return null;
    }
    
    /**
     * @return la liste des opérateurs dans le cache
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EORegleOperateur> getRegleOperateurs() {
        return EORegleOperateur.fetchAll(EOSharedEditingContext.defaultSharedEditingContext(), ERXS.ascs(RO_LC_KEY));
    }

    /**
     * @return l'opérateur "Membre de"
     */
    public static EORegleOperateur getRegleOperateurMembre() {
        if (RegleOperateurMembre == null)
            RegleOperateurMembre = ERXQ.first(getRegleOperateurs(), ERXQ.equals(EORegleOperateur.RO_STR_ID_KEY, STR_ID_MEMBRE));
        return RegleOperateurMembre;
    }
    
    /**
     * @return l'opérateur "Membre de"
     */
    public static EORegleOperateur getRegleOperateurRole() {
        if (RegleOperateurRole == null)
            RegleOperateurRole = ERXQ.first(getRegleOperateurs(), ERXQ.equals(EORegleOperateur.RO_STR_ID_KEY, STR_ID_ROLE));
        return RegleOperateurRole;
    }
    
    /**
     * @return l'opérateur "A pour role dans le groupe"
     */
    public static EORegleOperateur getRegleOperateurRoleDansGroupe() {
        if (RegleOperateurRoleDansGroupe == null)
            RegleOperateurRoleDansGroupe = ERXQ.first(getRegleOperateurs(), ERXQ.equals(EORegleOperateur.RO_STR_ID_KEY, STR_ID_ROLEDANSGROUPE));
        return RegleOperateurRoleDansGroupe;
    }
}
