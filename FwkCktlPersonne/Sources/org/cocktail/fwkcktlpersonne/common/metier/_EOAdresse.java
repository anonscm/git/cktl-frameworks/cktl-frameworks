/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAdresse.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOAdresse extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOAdresse.class);

	public static final String ENTITY_NAME = "Fwkpers_Adresse";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ADRESSE";


// Attribute Keys
  public static final ERXKey<String> ADR_ADRESSE1 = new ERXKey<String>("adrAdresse1");
  public static final ERXKey<String> ADR_ADRESSE2 = new ERXKey<String>("adrAdresse2");
  public static final ERXKey<String> ADR_BP = new ERXKey<String>("adrBp");
  public static final ERXKey<java.math.BigDecimal> ADR_GPS_LATITUDE = new ERXKey<java.math.BigDecimal>("adrGpsLatitude");
  public static final ERXKey<java.math.BigDecimal> ADR_GPS_LONGITUDE = new ERXKey<java.math.BigDecimal>("adrGpsLongitude");
  public static final ERXKey<String> ADR_LISTE_ROUGE = new ERXKey<String>("adrListeRouge");
  public static final ERXKey<Integer> ADR_ORDRE = new ERXKey<Integer>("adrOrdre");
  public static final ERXKey<String> ADR_URL_PERE = new ERXKey<String>("adrUrlPere");
  public static final ERXKey<String> BIS_TER = new ERXKey<String>("bisTer");
  public static final ERXKey<String> C_IMPLANTATION = new ERXKey<String>("cImplantation");
  public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
  public static final ERXKey<String> CP_ETRANGER = new ERXKey<String>("cpEtranger");
  public static final ERXKey<String> CPLT_LOCALISATION_ETRANGER = new ERXKey<String>("cpltLocalisationEtranger");
  public static final ERXKey<String> C_VOIE = new ERXKey<String>("cVoie");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
  public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIEU_DIT = new ERXKey<String>("lieuDit");
  public static final ERXKey<String> LOCALITE = new ERXKey<String>("localite");
  public static final ERXKey<String> NOM_VOIE = new ERXKey<String>("nomVoie");
  public static final ERXKey<String> NO_VOIE = new ERXKey<String>("noVoie");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> TEM_PAYE_UTIL = new ERXKey<String>("temPayeUtil");
  public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPays");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> TO_REPART_PERSONNE_ADRESSES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>("toRepartPersonneAdresses");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie> TO_TYPE_VOIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie>("toTypeVoie");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "adrOrdre";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADR_GPS_LATITUDE_KEY = "adrGpsLatitude";
	public static final String ADR_GPS_LONGITUDE_KEY = "adrGpsLongitude";
	public static final String ADR_LISTE_ROUGE_KEY = "adrListeRouge";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String ADR_URL_PERE_KEY = "adrUrlPere";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String C_IMPLANTATION_KEY = "cImplantation";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String CPLT_LOCALISATION_ETRANGER_KEY = "cpltLocalisationEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DIT_KEY = "lieuDit";
	public static final String LOCALITE_KEY = "localite";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String ADR_URL_RELATIVE_KEY = "adrUrlRelative";
	public static final String C_PAYS_KEY = "cPays";
	public static final String ADR_URL_TEMPLATE_KEY = "adrUrlTemplate";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String ADR_GPS_LATITUDE_COLKEY = "adr_Gps_latitude";
	public static final String ADR_GPS_LONGITUDE_COLKEY = "adr_Gps_Longitude";
	public static final String ADR_LISTE_ROUGE_COLKEY = "ADR_LISTE_ROUGE";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String ADR_URL_PERE_COLKEY = "ADR_URL_PERE";
	public static final String BIS_TER_COLKEY = "BIS_TER";
	public static final String C_IMPLANTATION_COLKEY = "C_IMPLANTATION";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String CPLT_LOCALISATION_ETRANGER_COLKEY = "CPLT_LOCALISATION_ETRANGER";
	public static final String C_VOIE_COLKEY = "C_VOIE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIEU_DIT_COLKEY = "LIEU_DIT";
	public static final String LOCALITE_COLKEY = "LOCALITE";
	public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
	public static final String NO_VOIE_COLKEY = "NO_VOIE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String ADR_URL_RELATIVE_COLKEY = "ADR_URL_RELATIVE";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String ADR_URL_TEMPLATE_COLKEY = "ADR_URL_TEMPLATE";


	// Relationships
	public static final String TO_PAYS_KEY = "toPays";
	public static final String TO_REPART_PERSONNE_ADRESSES_KEY = "toRepartPersonneAdresses";
	public static final String TO_TYPE_VOIE_KEY = "toTypeVoie";



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrBp() {
    return (String) storedValueForKey(ADR_BP_KEY);
  }

  public void setAdrBp(String value) {
    takeStoredValueForKey(value, ADR_BP_KEY);
  }

  public java.math.BigDecimal adrGpsLatitude() {
    return (java.math.BigDecimal) storedValueForKey(ADR_GPS_LATITUDE_KEY);
  }

  public void setAdrGpsLatitude(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ADR_GPS_LATITUDE_KEY);
  }

  public java.math.BigDecimal adrGpsLongitude() {
    return (java.math.BigDecimal) storedValueForKey(ADR_GPS_LONGITUDE_KEY);
  }

  public void setAdrGpsLongitude(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ADR_GPS_LONGITUDE_KEY);
  }

  public String adrListeRouge() {
    return (String) storedValueForKey(ADR_LISTE_ROUGE_KEY);
  }

  public void setAdrListeRouge(String value) {
    takeStoredValueForKey(value, ADR_LISTE_ROUGE_KEY);
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey(ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Integer value) {
    takeStoredValueForKey(value, ADR_ORDRE_KEY);
  }

  public String adrUrlPere() {
    return (String) storedValueForKey(ADR_URL_PERE_KEY);
  }

  public void setAdrUrlPere(String value) {
    takeStoredValueForKey(value, ADR_URL_PERE_KEY);
  }

  public String bisTer() {
    return (String) storedValueForKey(BIS_TER_KEY);
  }

  public void setBisTer(String value) {
    takeStoredValueForKey(value, BIS_TER_KEY);
  }

  public String cImplantation() {
    return (String) storedValueForKey(C_IMPLANTATION_KEY);
  }

  public void setCImplantation(String value) {
    takeStoredValueForKey(value, C_IMPLANTATION_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public String cpEtranger() {
    return (String) storedValueForKey(CP_ETRANGER_KEY);
  }

  public void setCpEtranger(String value) {
    takeStoredValueForKey(value, CP_ETRANGER_KEY);
  }

  public String cpltLocalisationEtranger() {
    return (String) storedValueForKey(CPLT_LOCALISATION_ETRANGER_KEY);
  }

  public void setCpltLocalisationEtranger(String value) {
    takeStoredValueForKey(value, CPLT_LOCALISATION_ETRANGER_KEY);
  }

  public String cVoie() {
    return (String) storedValueForKey(C_VOIE_KEY);
  }

  public void setCVoie(String value) {
    takeStoredValueForKey(value, C_VOIE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lieuDit() {
    return (String) storedValueForKey(LIEU_DIT_KEY);
  }

  public void setLieuDit(String value) {
    takeStoredValueForKey(value, LIEU_DIT_KEY);
  }

  public String localite() {
    return (String) storedValueForKey(LOCALITE_KEY);
  }

  public void setLocalite(String value) {
    takeStoredValueForKey(value, LOCALITE_KEY);
  }

  public String nomVoie() {
    return (String) storedValueForKey(NOM_VOIE_KEY);
  }

  public void setNomVoie(String value) {
    takeStoredValueForKey(value, NOM_VOIE_KEY);
  }

  public String noVoie() {
    return (String) storedValueForKey(NO_VOIE_KEY);
  }

  public void setNoVoie(String value) {
    takeStoredValueForKey(value, NO_VOIE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String temPayeUtil() {
    return (String) storedValueForKey(TEM_PAYE_UTIL_KEY);
  }

  public void setTemPayeUtil(String value) {
    takeStoredValueForKey(value, TEM_PAYE_UTIL_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_KEY);
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie toTypeVoie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie)storedValueForKey(TO_TYPE_VOIE_KEY);
  }

  public void setToTypeVoieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeVoie oldValue = toTypeVoie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VOIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VOIE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)storedValueForKey(TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier) {
    return toRepartPersonneAdresses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, boolean fetch) {
    return toRepartPersonneAdresses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartPersonneAdresses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public void removeFromToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse createToRepartPersonneAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartPersonneAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_PERSONNE_ADRESSES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse) eo;
  }

  public void deleteToRepartPersonneAdressesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_PERSONNE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartPersonneAdressesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> objects = toRepartPersonneAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartPersonneAdressesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAdresse createEOAdresse(EOEditingContext editingContext, String adrListeRouge
, Integer adrOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, String temPayeUtil
, org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays			) {
    EOAdresse eo = (EOAdresse) createAndInsertInstance(editingContext, _EOAdresse.ENTITY_NAME);    
		eo.setAdrListeRouge(adrListeRouge);
		eo.setAdrOrdre(adrOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemPayeUtil(temPayeUtil);
    eo.setToPaysRelationship(toPays);
    return eo;
  }

  
	  public EOAdresse localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAdresse)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAdresse creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAdresse creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOAdresse object = (EOAdresse)createAndInsertInstance(editingContext, _EOAdresse.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAdresse localInstanceIn(EOEditingContext editingContext, EOAdresse eo) {
    EOAdresse localInstance = (eo == null) ? null : (EOAdresse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAdresse#localInstanceIn a la place.
   */
	public static EOAdresse localInstanceOf(EOEditingContext editingContext, EOAdresse eo) {
		return EOAdresse.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAdresse> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAdresse> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
