/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMotifTempsPartiel.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOMotifTempsPartiel extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOMotifTempsPartiel.class);

	public static final String ENTITY_NAME = "Fwkpers_MotifTempsPartiel";
	public static final String ENTITY_TABLE_NAME = "GRHUM.MOTIF_TEMPS_PARTIEL";


// Attribute Keys
  public static final ERXKey<String> C_MOTIF_TEMPS_PARTIEL = new ERXKey<String>("cMotifTempsPartiel");
  public static final ERXKey<String> C_MOTIF_TEMPS_PARTIEL_ONP = new ERXKey<String>("cMotifTempsPartielOnp");
  public static final ERXKey<String> LC_MOTIF_TEMPS_PARTIEL = new ERXKey<String>("lcMotifTempsPartiel");
  public static final ERXKey<String> TEM_CTRL_DUREE_TPS = new ERXKey<String>("temCtrlDureeTps");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cMotifTempsPartiel";

	public static final String C_MOTIF_TEMPS_PARTIEL_KEY = "cMotifTempsPartiel";
	public static final String C_MOTIF_TEMPS_PARTIEL_ONP_KEY = "cMotifTempsPartielOnp";
	public static final String LC_MOTIF_TEMPS_PARTIEL_KEY = "lcMotifTempsPartiel";
	public static final String TEM_CTRL_DUREE_TPS_KEY = "temCtrlDureeTps";

// Attributs non visibles
	public static final String LL_MOTIF_TEMPS_PARTIEL_KEY = "llMotifTempsPartiel";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

//Colonnes dans la base de donnees
	public static final String C_MOTIF_TEMPS_PARTIEL_COLKEY = "C_MOTIF_TEMPS_PARTIEL";
	public static final String C_MOTIF_TEMPS_PARTIEL_ONP_COLKEY = "C_MOTIF_TEMPS_PARTIEL_ONP";
	public static final String LC_MOTIF_TEMPS_PARTIEL_COLKEY = "LC_MOTIF_TEMPS_PARTIEL";
	public static final String TEM_CTRL_DUREE_TPS_COLKEY = "TEM_CTRL_DUREE_TPS";

	public static final String LL_MOTIF_TEMPS_PARTIEL_COLKEY = "LL_MOTIF_TEMPS_PARTIEL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";


	// Relationships



	// Accessors methods
  public String cMotifTempsPartiel() {
    return (String) storedValueForKey(C_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public void setCMotifTempsPartiel(String value) {
    takeStoredValueForKey(value, C_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public String cMotifTempsPartielOnp() {
    return (String) storedValueForKey(C_MOTIF_TEMPS_PARTIEL_ONP_KEY);
  }

  public void setCMotifTempsPartielOnp(String value) {
    takeStoredValueForKey(value, C_MOTIF_TEMPS_PARTIEL_ONP_KEY);
  }

  public String lcMotifTempsPartiel() {
    return (String) storedValueForKey(LC_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public void setLcMotifTempsPartiel(String value) {
    takeStoredValueForKey(value, LC_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public String temCtrlDureeTps() {
    return (String) storedValueForKey(TEM_CTRL_DUREE_TPS_KEY);
  }

  public void setTemCtrlDureeTps(String value) {
    takeStoredValueForKey(value, TEM_CTRL_DUREE_TPS_KEY);
  }


/**
 * Créer une instance de EOMotifTempsPartiel avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMotifTempsPartiel createEOMotifTempsPartiel(EOEditingContext editingContext, String cMotifTempsPartiel
			) {
    EOMotifTempsPartiel eo = (EOMotifTempsPartiel) createAndInsertInstance(editingContext, _EOMotifTempsPartiel.ENTITY_NAME);    
		eo.setCMotifTempsPartiel(cMotifTempsPartiel);
    return eo;
  }

  
	  public EOMotifTempsPartiel localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMotifTempsPartiel)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMotifTempsPartiel creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMotifTempsPartiel creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOMotifTempsPartiel object = (EOMotifTempsPartiel)createAndInsertInstance(editingContext, _EOMotifTempsPartiel.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMotifTempsPartiel localInstanceIn(EOEditingContext editingContext, EOMotifTempsPartiel eo) {
    EOMotifTempsPartiel localInstance = (eo == null) ? null : (EOMotifTempsPartiel)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMotifTempsPartiel#localInstanceIn a la place.
   */
	public static EOMotifTempsPartiel localInstanceOf(EOEditingContext editingContext, EOMotifTempsPartiel eo) {
		return EOMotifTempsPartiel.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifTempsPartiel>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMotifTempsPartiel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMotifTempsPartiel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOMotifTempsPartiel> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMotifTempsPartiel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMotifTempsPartiel)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMotifTempsPartiel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMotifTempsPartiel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOMotifTempsPartiel> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMotifTempsPartiel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMotifTempsPartiel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMotifTempsPartiel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMotifTempsPartiel eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMotifTempsPartiel ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMotifTempsPartiel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
