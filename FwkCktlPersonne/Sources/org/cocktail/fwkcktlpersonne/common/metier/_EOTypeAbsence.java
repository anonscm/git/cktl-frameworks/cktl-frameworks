/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeAbsence.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOTypeAbsence extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOTypeAbsence.class);

	public static final String ENTITY_NAME = "Fwkpers_TypeAbsence";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_ABSENCE";


// Attribute Keys
  public static final ERXKey<String> CONGE_LEGAL = new ERXKey<String>("congeLegal");
  public static final ERXKey<String> C_TYPE_ABSENCE = new ERXKey<String>("cTypeAbsence");
  public static final ERXKey<String> C_TYPE_ABSENCE_HARPEGE = new ERXKey<String>("cTypeAbsenceHarpege");
  public static final ERXKey<String> C_TYPE_ABSENCE_ONP = new ERXKey<String>("cTypeAbsenceOnp");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> JOURS_CONSECUTIFS = new ERXKey<String>("joursConsecutifs");
  public static final ERXKey<String> LC_TYPE_ABSENCE = new ERXKey<String>("lcTypeAbsence");
  public static final ERXKey<String> LL_TYPE_ABSENCE = new ERXKey<String>("llTypeAbsence");
  public static final ERXKey<String> TEM_CIR = new ERXKey<String>("temCir");
  public static final ERXKey<String> TEM_ENFANT = new ERXKey<String>("temEnfant");
  public static final ERXKey<String> TEM_HCOMP = new ERXKey<String>("temHCOMP");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cTypeAbsence";

	public static final String CONGE_LEGAL_KEY = "congeLegal";
	public static final String C_TYPE_ABSENCE_KEY = "cTypeAbsence";
	public static final String C_TYPE_ABSENCE_HARPEGE_KEY = "cTypeAbsenceHarpege";
	public static final String C_TYPE_ABSENCE_ONP_KEY = "cTypeAbsenceOnp";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String JOURS_CONSECUTIFS_KEY = "joursConsecutifs";
	public static final String LC_TYPE_ABSENCE_KEY = "lcTypeAbsence";
	public static final String LL_TYPE_ABSENCE_KEY = "llTypeAbsence";
	public static final String TEM_CIR_KEY = "temCir";
	public static final String TEM_ENFANT_KEY = "temEnfant";
	public static final String TEM_HCOMP_KEY = "temHCOMP";

// Attributs non visibles
	public static final String TYPE_SENS_IMPUTATION_KEY = "typeSensImputation";
	public static final String TYPE_ABS_NIVEAU_KEY = "typeAbsNiveau";
	public static final String TYPE_ABS_IMPUTABLE_KEY = "typeAbsImputable";
	public static final String TYPE_ABS_JOURS_AUTORISES_KEY = "typeAbsJoursAutorises";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String TYPE_VALIDATION_KEY = "typeValidation";

//Colonnes dans la base de donnees
	public static final String CONGE_LEGAL_COLKEY = "CONGE_LEGAL";
	public static final String C_TYPE_ABSENCE_COLKEY = "C_TYPE_ABSENCE";
	public static final String C_TYPE_ABSENCE_HARPEGE_COLKEY = "C_TYPE_ABSENCE_HARPEGE";
	public static final String C_TYPE_ABSENCE_ONP_COLKEY = "C_TYPE_ABSENCE_ONP";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String JOURS_CONSECUTIFS_COLKEY = "JOURS_CONSECUTIFS";
	public static final String LC_TYPE_ABSENCE_COLKEY = "LC_TYPE_ABSENCE";
	public static final String LL_TYPE_ABSENCE_COLKEY = "LL_TYPE_ABSENCE";
	public static final String TEM_CIR_COLKEY = "TEM_CIR";
	public static final String TEM_ENFANT_COLKEY = "TEM_ENFANT";
	public static final String TEM_HCOMP_COLKEY = "TEM_HCOMP";

	public static final String TYPE_SENS_IMPUTATION_COLKEY = "TYPE_SENS_IMPUTATION";
	public static final String TYPE_ABS_NIVEAU_COLKEY = "TYPE_ABS_NIVEAU";
	public static final String TYPE_ABS_IMPUTABLE_COLKEY = "TYPE_ABS_IMPUTABLE";
	public static final String TYPE_ABS_JOURS_AUTORISES_COLKEY = "TYPE_ABS_JOURS_AUTORISES";
	public static final String OBSERVATIONS_COLKEY = "OBSERVATIONS";
	public static final String TYPE_VALIDATION_COLKEY = "TYPE_VALIDATION";


	// Relationships



	// Accessors methods
  public String congeLegal() {
    return (String) storedValueForKey(CONGE_LEGAL_KEY);
  }

  public void setCongeLegal(String value) {
    takeStoredValueForKey(value, CONGE_LEGAL_KEY);
  }

  public String cTypeAbsence() {
    return (String) storedValueForKey(C_TYPE_ABSENCE_KEY);
  }

  public void setCTypeAbsence(String value) {
    takeStoredValueForKey(value, C_TYPE_ABSENCE_KEY);
  }

  public String cTypeAbsenceHarpege() {
    return (String) storedValueForKey(C_TYPE_ABSENCE_HARPEGE_KEY);
  }

  public void setCTypeAbsenceHarpege(String value) {
    takeStoredValueForKey(value, C_TYPE_ABSENCE_HARPEGE_KEY);
  }

  public String cTypeAbsenceOnp() {
    return (String) storedValueForKey(C_TYPE_ABSENCE_ONP_KEY);
  }

  public void setCTypeAbsenceOnp(String value) {
    takeStoredValueForKey(value, C_TYPE_ABSENCE_ONP_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String joursConsecutifs() {
    return (String) storedValueForKey(JOURS_CONSECUTIFS_KEY);
  }

  public void setJoursConsecutifs(String value) {
    takeStoredValueForKey(value, JOURS_CONSECUTIFS_KEY);
  }

  public String lcTypeAbsence() {
    return (String) storedValueForKey(LC_TYPE_ABSENCE_KEY);
  }

  public void setLcTypeAbsence(String value) {
    takeStoredValueForKey(value, LC_TYPE_ABSENCE_KEY);
  }

  public String llTypeAbsence() {
    return (String) storedValueForKey(LL_TYPE_ABSENCE_KEY);
  }

  public void setLlTypeAbsence(String value) {
    takeStoredValueForKey(value, LL_TYPE_ABSENCE_KEY);
  }

  public String temCir() {
    return (String) storedValueForKey(TEM_CIR_KEY);
  }

  public void setTemCir(String value) {
    takeStoredValueForKey(value, TEM_CIR_KEY);
  }

  public String temEnfant() {
    return (String) storedValueForKey(TEM_ENFANT_KEY);
  }

  public void setTemEnfant(String value) {
    takeStoredValueForKey(value, TEM_ENFANT_KEY);
  }

  public String temHCOMP() {
    return (String) storedValueForKey(TEM_HCOMP_KEY);
  }

  public void setTemHCOMP(String value) {
    takeStoredValueForKey(value, TEM_HCOMP_KEY);
  }


/**
 * Créer une instance de EOTypeAbsence avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeAbsence createEOTypeAbsence(EOEditingContext editingContext, String cTypeAbsence
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcTypeAbsence
, String llTypeAbsence
			) {
    EOTypeAbsence eo = (EOTypeAbsence) createAndInsertInstance(editingContext, _EOTypeAbsence.ENTITY_NAME);    
		eo.setCTypeAbsence(cTypeAbsence);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcTypeAbsence(lcTypeAbsence);
		eo.setLlTypeAbsence(llTypeAbsence);
    return eo;
  }

  
	  public EOTypeAbsence localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeAbsence)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeAbsence creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeAbsence creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOTypeAbsence object = (EOTypeAbsence)createAndInsertInstance(editingContext, _EOTypeAbsence.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeAbsence localInstanceIn(EOEditingContext editingContext, EOTypeAbsence eo) {
    EOTypeAbsence localInstance = (eo == null) ? null : (EOTypeAbsence)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeAbsence#localInstanceIn a la place.
   */
	public static EOTypeAbsence localInstanceOf(EOEditingContext editingContext, EOTypeAbsence eo) {
		return EOTypeAbsence.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeAbsence fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeAbsence fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeAbsence> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeAbsence eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeAbsence)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeAbsence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeAbsence> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeAbsence eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeAbsence)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeAbsence fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeAbsence eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeAbsence ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeAbsence fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
