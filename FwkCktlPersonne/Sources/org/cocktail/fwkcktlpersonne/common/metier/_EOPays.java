/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPays.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPays extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPays.class);

	public static final String ENTITY_NAME = "Fwkpers_Pays";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PAYS";


// Attribute Keys
  public static final ERXKey<String> CODE_ISO = new ERXKey<String>("codeIso");
  public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
  public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
  public static final ERXKey<String> LC_PAYS = new ERXKey<String>("lcPays");
  public static final ERXKey<String> LL_PAYS = new ERXKey<String>("llPays");
  public static final ERXKey<String> LL_PAYS_EN = new ERXKey<String>("llPaysEn");
  public static final ERXKey<String> L_NATIONALITE = new ERXKey<String>("lNationalite");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif> TO_PAYS_INDICATIF = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif>("toPaysIndicatif");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cPays";

	public static final String CODE_ISO_KEY = "codeIso";
	public static final String C_PAYS_KEY = "cPays";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String LC_PAYS_KEY = "lcPays";
	public static final String LL_PAYS_KEY = "llPays";
	public static final String LL_PAYS_EN_KEY = "llPaysEn";
	public static final String L_NATIONALITE_KEY = "lNationalite";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CODE_ISO_COLKEY = "CODE_ISO";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String LC_PAYS_COLKEY = "LC_PAYS";
	public static final String LL_PAYS_COLKEY = "LL_PAYS";
	public static final String LL_PAYS_EN_COLKEY = "LL_PAYS_EN";
	public static final String L_NATIONALITE_COLKEY = "L_NATIONALITE";



	// Relationships
	public static final String TO_PAYS_INDICATIF_KEY = "toPaysIndicatif";



	// Accessors methods
  public String codeIso() {
    return (String) storedValueForKey(CODE_ISO_KEY);
  }

  public void setCodeIso(String value) {
    takeStoredValueForKey(value, CODE_ISO_KEY);
  }

  public String cPays() {
    return (String) storedValueForKey(C_PAYS_KEY);
  }

  public void setCPays(String value) {
    takeStoredValueForKey(value, C_PAYS_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public String lcPays() {
    return (String) storedValueForKey(LC_PAYS_KEY);
  }

  public void setLcPays(String value) {
    takeStoredValueForKey(value, LC_PAYS_KEY);
  }

  public String llPays() {
    return (String) storedValueForKey(LL_PAYS_KEY);
  }

  public void setLlPays(String value) {
    takeStoredValueForKey(value, LL_PAYS_KEY);
  }

  public String llPaysEn() {
    return (String) storedValueForKey(LL_PAYS_EN_KEY);
  }

  public void setLlPaysEn(String value) {
    takeStoredValueForKey(value, LL_PAYS_EN_KEY);
  }

  public String lNationalite() {
    return (String) storedValueForKey(L_NATIONALITE_KEY);
  }

  public void setLNationalite(String value) {
    takeStoredValueForKey(value, L_NATIONALITE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif toPaysIndicatif() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif)storedValueForKey(TO_PAYS_INDICATIF_KEY);
  }

  public void setToPaysIndicatifRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif oldValue = toPaysIndicatif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_INDICATIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_INDICATIF_KEY);
    }
  }
  

/**
 * Créer une instance de EOPays avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPays createEOPays(EOEditingContext editingContext, String cPays
, org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif toPaysIndicatif			) {
    EOPays eo = (EOPays) createAndInsertInstance(editingContext, _EOPays.ENTITY_NAME);    
		eo.setCPays(cPays);
    eo.setToPaysIndicatifRelationship(toPaysIndicatif);
    return eo;
  }

  
	  public EOPays localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPays)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPays creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPays creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPays object = (EOPays)createAndInsertInstance(editingContext, _EOPays.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPays localInstanceIn(EOEditingContext editingContext, EOPays eo) {
    EOPays localInstance = (eo == null) ? null : (EOPays)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPays#localInstanceIn a la place.
   */
	public static EOPays localInstanceOf(EOEditingContext editingContext, EOPays eo) {
		return EOPays.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPays>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPays fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPays fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPays> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPays eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPays)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPays fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPays fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPays> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPays eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPays)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPays fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPays eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPays ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPays fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
