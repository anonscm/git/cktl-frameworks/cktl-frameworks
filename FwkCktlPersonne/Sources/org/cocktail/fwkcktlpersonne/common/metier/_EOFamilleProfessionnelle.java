/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFamilleProfessionnelle.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOFamilleProfessionnelle extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOFamilleProfessionnelle.class);

	public static final String ENTITY_NAME = "Fwkpers_FamilleProfessionnelle";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FAMILLE_PROFESSIONNELLE_OLD";


// Attribute Keys
  public static final ERXKey<Integer> DCO_KEY = new ERXKey<Integer>("dcoKey");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> FPR_LIBELLE = new ERXKey<String>("fprLibelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence> TO_DOMAINE_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence>("toDomaineCompetence");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> TOS_EMPLOI_TYPE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>("tosEmploiType");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fprKey";

	public static final String DCO_KEY_KEY = "dcoKey";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FPR_LIBELLE_KEY = "fprLibelle";

// Attributs non visibles
	public static final String FPR_KEY_KEY = "fprKey";

//Colonnes dans la base de donnees
	public static final String DCO_KEY_COLKEY = "DCO_KEY";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FPR_LIBELLE_COLKEY = "FPR_LIBELLE";

	public static final String FPR_KEY_COLKEY = "FRP_KEY";


	// Relationships
	public static final String TO_DOMAINE_COMPETENCE_KEY = "toDomaineCompetence";
	public static final String TOS_EMPLOI_TYPE_KEY = "tosEmploiType";



	// Accessors methods
  public Integer dcoKey() {
    return (Integer) storedValueForKey(DCO_KEY_KEY);
  }

  public void setDcoKey(Integer value) {
    takeStoredValueForKey(value, DCO_KEY_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String fprLibelle() {
    return (String) storedValueForKey(FPR_LIBELLE_KEY);
  }

  public void setFprLibelle(String value) {
    takeStoredValueForKey(value, FPR_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence toDomaineCompetence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence)storedValueForKey(TO_DOMAINE_COMPETENCE_KEY);
  }

  public void setToDomaineCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODomaineCompetence oldValue = toDomaineCompetence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOMAINE_COMPETENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOMAINE_COMPETENCE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)storedValueForKey(TOS_EMPLOI_TYPE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType(EOQualifier qualifier) {
    return tosEmploiType(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType(EOQualifier qualifier, boolean fetch) {
    return tosEmploiType(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> tosEmploiType(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType.TO_FAMILLE_PROFESSIONNELLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosEmploiType();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
  }

  public void removeFromTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType createTosEmploiTypeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_EmploiType");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_EMPLOI_TYPE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType) eo;
  }

  public void deleteTosEmploiTypeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_EMPLOI_TYPE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosEmploiTypeRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOEmploiType> objects = tosEmploiType().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosEmploiTypeRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFamilleProfessionnelle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFamilleProfessionnelle createEOFamilleProfessionnelle(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOFamilleProfessionnelle eo = (EOFamilleProfessionnelle) createAndInsertInstance(editingContext, _EOFamilleProfessionnelle.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOFamilleProfessionnelle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFamilleProfessionnelle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFamilleProfessionnelle creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFamilleProfessionnelle creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOFamilleProfessionnelle object = (EOFamilleProfessionnelle)createAndInsertInstance(editingContext, _EOFamilleProfessionnelle.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFamilleProfessionnelle localInstanceIn(EOEditingContext editingContext, EOFamilleProfessionnelle eo) {
    EOFamilleProfessionnelle localInstance = (eo == null) ? null : (EOFamilleProfessionnelle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFamilleProfessionnelle#localInstanceIn a la place.
   */
	public static EOFamilleProfessionnelle localInstanceOf(EOEditingContext editingContext, EOFamilleProfessionnelle eo) {
		return EOFamilleProfessionnelle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFamilleProfessionnelle>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFamilleProfessionnelle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFamilleProfessionnelle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFamilleProfessionnelle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFamilleProfessionnelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFamilleProfessionnelle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFamilleProfessionnelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFamilleProfessionnelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFamilleProfessionnelle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFamilleProfessionnelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFamilleProfessionnelle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFamilleProfessionnelle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFamilleProfessionnelle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFamilleProfessionnelle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFamilleProfessionnelle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
