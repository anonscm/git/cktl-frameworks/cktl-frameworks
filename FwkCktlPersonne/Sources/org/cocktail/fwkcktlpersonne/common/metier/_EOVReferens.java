/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVReferens.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOVReferens extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOVReferens.class);

	public static final String ENTITY_NAME = "Fwkpers_VReferens";
	public static final String ENTITY_TABLE_NAME = "GRHUM.V_REFERENS";


// Attribute Keys
  public static final ERXKey<String> KEY = new ERXKey<String>("key");
  public static final ERXKey<String> KEY_PERE = new ERXKey<String>("keyPere");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_SEUL = new ERXKey<String>("libelleSeul");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<String> ORDRE = new ERXKey<String>("ordre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> TO_REFERENS_ACTIVITES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>("toReferensActivites");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences> TO_REFERENS_COMPETENCES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences>("toReferensCompetences");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp> TO_REFERENS_DCP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp>("toReferensDcp");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp> TO_REFERENS_FP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp>("toReferensFp");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> TOS_V_REFERENS_FILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>("tosVReferensFils");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> TO_V_REFERENS_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>("toVReferensPere");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "key";

	public static final String KEY_KEY = "key";
	public static final String KEY_PERE_KEY = "keyPere";
	public static final String LIBELLE_KEY = "libelle";
	public static final String LIBELLE_SEUL_KEY = "libelleSeul";
	public static final String NIVEAU_KEY = "niveau";
	public static final String ORDRE_KEY = "ordre";

// Attributs non visibles
	public static final String NUM_DCP_KEY = "numDcp";
	public static final String CODE_EMPLOI_KEY = "codeEmploi";
	public static final String NUM_FP_KEY = "numFp";

//Colonnes dans la base de donnees
	public static final String KEY_COLKEY = "KEY";
	public static final String KEY_PERE_COLKEY = "KEY_PERE";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String LIBELLE_SEUL_COLKEY = "LIBELLE_SEUL";
	public static final String NIVEAU_COLKEY = "NIVEAU";
	public static final String ORDRE_COLKEY = "ORDRE";

	public static final String NUM_DCP_COLKEY = "NUMDCP";
	public static final String CODE_EMPLOI_COLKEY = "CODEEMPLOI";
	public static final String NUM_FP_COLKEY = "NUMFP";


	// Relationships
	public static final String TO_REFERENS_ACTIVITES_KEY = "toReferensActivites";
	public static final String TO_REFERENS_COMPETENCES_KEY = "toReferensCompetences";
	public static final String TO_REFERENS_DCP_KEY = "toReferensDcp";
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";
	public static final String TO_REFERENS_FP_KEY = "toReferensFp";
	public static final String TOS_V_REFERENS_FILS_KEY = "tosVReferensFils";
	public static final String TO_V_REFERENS_PERE_KEY = "toVReferensPere";



	// Accessors methods
  public String key() {
    return (String) storedValueForKey(KEY_KEY);
  }

  public void setKey(String value) {
    takeStoredValueForKey(value, KEY_KEY);
  }

  public String keyPere() {
    return (String) storedValueForKey(KEY_PERE_KEY);
  }

  public void setKeyPere(String value) {
    takeStoredValueForKey(value, KEY_PERE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public String libelleSeul() {
    return (String) storedValueForKey(LIBELLE_SEUL_KEY);
  }

  public void setLibelleSeul(String value) {
    takeStoredValueForKey(value, LIBELLE_SEUL_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    takeStoredValueForKey(value, NIVEAU_KEY);
  }

  public String ordre() {
    return (String) storedValueForKey(ORDRE_KEY);
  }

  public void setOrdre(String value) {
    takeStoredValueForKey(value, ORDRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites toReferensActivites() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites)storedValueForKey(TO_REFERENS_ACTIVITES_KEY);
  }

  public void setToReferensActivitesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites oldValue = toReferensActivites();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_ACTIVITES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_ACTIVITES_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences toReferensCompetences() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences)storedValueForKey(TO_REFERENS_COMPETENCES_KEY);
  }

  public void setToReferensCompetencesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences oldValue = toReferensCompetences();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_COMPETENCES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_COMPETENCES_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp toReferensDcp() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp)storedValueForKey(TO_REFERENS_DCP_KEY);
  }

  public void setToReferensDcpRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp oldValue = toReferensDcp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_DCP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_DCP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey(TO_REFERENS_EMPLOIS_KEY);
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_EMPLOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_EMPLOIS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp toReferensFp() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp)storedValueForKey(TO_REFERENS_FP_KEY);
  }

  public void setToReferensFpRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensFp oldValue = toReferensFp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_FP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_FP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOVReferens toVReferensPere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVReferens)storedValueForKey(TO_V_REFERENS_PERE_KEY);
  }

  public void setToVReferensPereRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVReferens value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOVReferens oldValue = toVReferensPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_V_REFERENS_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_V_REFERENS_PERE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> tosVReferensFils() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>)storedValueForKey(TOS_V_REFERENS_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> tosVReferensFils(EOQualifier qualifier) {
    return tosVReferensFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> tosVReferensFils(EOQualifier qualifier, boolean fetch) {
    return tosVReferensFils(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> tosVReferensFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOVReferens.TO_V_REFERENS_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOVReferens.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosVReferensFils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosVReferensFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVReferens object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_V_REFERENS_FILS_KEY);
  }

  public void removeFromTosVReferensFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVReferens object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_REFERENS_FILS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOVReferens createTosVReferensFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_VReferens");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_V_REFERENS_FILS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVReferens) eo;
  }

  public void deleteTosVReferensFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVReferens object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_REFERENS_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVReferensFilsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> objects = tosVReferensFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVReferensFilsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOVReferens avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVReferens createEOVReferens(EOEditingContext editingContext, String key
, String keyPere
, Integer niveau
, org.cocktail.fwkcktlpersonne.common.metier.EOVReferens toVReferensPere			) {
    EOVReferens eo = (EOVReferens) createAndInsertInstance(editingContext, _EOVReferens.ENTITY_NAME);    
		eo.setKey(key);
		eo.setKeyPere(keyPere);
		eo.setNiveau(niveau);
    eo.setToVReferensPereRelationship(toVReferensPere);
    return eo;
  }

  
	  public EOVReferens localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVReferens)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVReferens creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVReferens creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOVReferens object = (EOVReferens)createAndInsertInstance(editingContext, _EOVReferens.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVReferens localInstanceIn(EOEditingContext editingContext, EOVReferens eo) {
    EOVReferens localInstance = (eo == null) ? null : (EOVReferens)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVReferens#localInstanceIn a la place.
   */
	public static EOVReferens localInstanceOf(EOEditingContext editingContext, EOVReferens eo) {
		return EOVReferens.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOVReferens>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVReferens fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVReferens fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOVReferens> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVReferens eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVReferens)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVReferens fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVReferens fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOVReferens> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVReferens eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVReferens)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVReferens fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVReferens eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVReferens ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVReferens fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
