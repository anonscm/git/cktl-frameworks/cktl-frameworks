/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPasswordHistory.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPasswordHistory extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPasswordHistory.class);

	public static final String ENTITY_NAME = "Fwkpers_PasswordHistory";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PASSWORD_HISTORY";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_FIN_VALIDITE = new ERXKey<NSTimestamp>("dFinValidite");
  public static final ERXKey<Integer> PWDH_ORDRE = new ERXKey<Integer>("pwdhOrdre");
  public static final ERXKey<String> PWDH_PASSWD = new ERXKey<String>("pwdhPasswd");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> TO_COMPTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>("toCompte");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage> TO_TYPE_CRYPTAGE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage>("toTypeCryptage");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pwdhOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_VALIDITE_KEY = "dFinValidite";
	public static final String PWDH_ORDRE_KEY = "pwdhOrdre";
	public static final String PWDH_PASSWD_KEY = "pwdhPasswd";

// Attributs non visibles
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String TCRY_ORDRE_KEY = "tcryOrdre";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FIN_VALIDITE_COLKEY = "D_FIN_VALIDITE";
	public static final String PWDH_ORDRE_COLKEY = "PWDH_ORDRE";
	public static final String PWDH_PASSWD_COLKEY = "PWDH_PASSWD";

	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String TCRY_ORDRE_COLKEY = "TCRY_ORDRE";


	// Relationships
	public static final String TO_COMPTE_KEY = "toCompte";
	public static final String TO_TYPE_CRYPTAGE_KEY = "toTypeCryptage";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFinValidite() {
    return (NSTimestamp) storedValueForKey(D_FIN_VALIDITE_KEY);
  }

  public void setDFinValidite(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VALIDITE_KEY);
  }

  public Integer pwdhOrdre() {
    return (Integer) storedValueForKey(PWDH_ORDRE_KEY);
  }

  public void setPwdhOrdre(Integer value) {
    takeStoredValueForKey(value, PWDH_ORDRE_KEY);
  }

  public String pwdhPasswd() {
    return (String) storedValueForKey(PWDH_PASSWD_KEY);
  }

  public void setPwdhPasswd(String value) {
    takeStoredValueForKey(value, PWDH_PASSWD_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompte)storedValueForKey(TO_COMPTE_KEY);
  }

  public void setToCompteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompte value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCompte oldValue = toCompte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COMPTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COMPTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage toTypeCryptage() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage)storedValueForKey(TO_TYPE_CRYPTAGE_KEY);
  }

  public void setToTypeCryptageRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage oldValue = toTypeCryptage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CRYPTAGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CRYPTAGE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPasswordHistory avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPasswordHistory createEOPasswordHistory(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dFinValidite
, Integer pwdhOrdre
, String pwdhPasswd
, org.cocktail.fwkcktlpersonne.common.metier.EOCompte toCompte, org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage toTypeCryptage			) {
    EOPasswordHistory eo = (EOPasswordHistory) createAndInsertInstance(editingContext, _EOPasswordHistory.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDFinValidite(dFinValidite);
		eo.setPwdhOrdre(pwdhOrdre);
		eo.setPwdhPasswd(pwdhPasswd);
    eo.setToCompteRelationship(toCompte);
    eo.setToTypeCryptageRelationship(toTypeCryptage);
    return eo;
  }

  
	  public EOPasswordHistory localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPasswordHistory)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPasswordHistory creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPasswordHistory creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPasswordHistory object = (EOPasswordHistory)createAndInsertInstance(editingContext, _EOPasswordHistory.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPasswordHistory localInstanceIn(EOEditingContext editingContext, EOPasswordHistory eo) {
    EOPasswordHistory localInstance = (eo == null) ? null : (EOPasswordHistory)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPasswordHistory#localInstanceIn a la place.
   */
	public static EOPasswordHistory localInstanceOf(EOEditingContext editingContext, EOPasswordHistory eo) {
		return EOPasswordHistory.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPasswordHistory fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPasswordHistory fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPasswordHistory> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPasswordHistory eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPasswordHistory)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPasswordHistory fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPasswordHistory fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPasswordHistory> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPasswordHistory eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPasswordHistory)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPasswordHistory fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPasswordHistory eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPasswordHistory ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPasswordHistory fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
