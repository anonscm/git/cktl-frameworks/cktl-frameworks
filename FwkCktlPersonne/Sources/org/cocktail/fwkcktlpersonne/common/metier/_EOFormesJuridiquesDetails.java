/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFormesJuridiquesDetails.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOFormesJuridiquesDetails extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOFormesJuridiquesDetails.class);

	public static final String ENTITY_NAME = "Fwkpers_FormesJuridiquesDetails";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FORMES_JURIDIQUES_DETAILS";


// Attribute Keys
  public static final ERXKey<String> C_FJD = new ERXKey<String>("cFjd");
  public static final ERXKey<String> CONTROLE_SIRET_FJD = new ERXKey<String>("controleSiretFjd");
  public static final ERXKey<String> LC_FJD = new ERXKey<String>("lcFjd");
  public static final ERXKey<String> LL_FJD = new ERXKey<String>("llFjd");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cFjd";

	public static final String C_FJD_KEY = "cFjd";
	public static final String CONTROLE_SIRET_FJD_KEY = "controleSiretFjd";
	public static final String LC_FJD_KEY = "lcFjd";
	public static final String LL_FJD_KEY = "llFjd";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_FJD_COLKEY = "C_FJD";
	public static final String CONTROLE_SIRET_FJD_COLKEY = "CONTROLE_SIRET_FJD";
	public static final String LC_FJD_COLKEY = "LC_FJD";
	public static final String LL_FJD_COLKEY = "LL_FJD";



	// Relationships



	// Accessors methods
  public String cFjd() {
    return (String) storedValueForKey(C_FJD_KEY);
  }

  public void setCFjd(String value) {
    takeStoredValueForKey(value, C_FJD_KEY);
  }

  public String controleSiretFjd() {
    return (String) storedValueForKey(CONTROLE_SIRET_FJD_KEY);
  }

  public void setControleSiretFjd(String value) {
    takeStoredValueForKey(value, CONTROLE_SIRET_FJD_KEY);
  }

  public String lcFjd() {
    return (String) storedValueForKey(LC_FJD_KEY);
  }

  public void setLcFjd(String value) {
    takeStoredValueForKey(value, LC_FJD_KEY);
  }

  public String llFjd() {
    return (String) storedValueForKey(LL_FJD_KEY);
  }

  public void setLlFjd(String value) {
    takeStoredValueForKey(value, LL_FJD_KEY);
  }


/**
 * Créer une instance de EOFormesJuridiquesDetails avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFormesJuridiquesDetails createEOFormesJuridiquesDetails(EOEditingContext editingContext, String cFjd
			) {
    EOFormesJuridiquesDetails eo = (EOFormesJuridiquesDetails) createAndInsertInstance(editingContext, _EOFormesJuridiquesDetails.ENTITY_NAME);    
		eo.setCFjd(cFjd);
    return eo;
  }

  
	  public EOFormesJuridiquesDetails localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFormesJuridiquesDetails)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFormesJuridiquesDetails creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFormesJuridiquesDetails creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOFormesJuridiquesDetails object = (EOFormesJuridiquesDetails)createAndInsertInstance(editingContext, _EOFormesJuridiquesDetails.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFormesJuridiquesDetails localInstanceIn(EOEditingContext editingContext, EOFormesJuridiquesDetails eo) {
    EOFormesJuridiquesDetails localInstance = (eo == null) ? null : (EOFormesJuridiquesDetails)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFormesJuridiquesDetails#localInstanceIn a la place.
   */
	public static EOFormesJuridiquesDetails localInstanceOf(EOEditingContext editingContext, EOFormesJuridiquesDetails eo) {
		return EOFormesJuridiquesDetails.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOFormesJuridiquesDetails>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFormesJuridiquesDetails fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFormesJuridiquesDetails fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFormesJuridiquesDetails> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFormesJuridiquesDetails eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFormesJuridiquesDetails)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFormesJuridiquesDetails fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFormesJuridiquesDetails fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFormesJuridiquesDetails> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFormesJuridiquesDetails eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFormesJuridiquesDetails)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFormesJuridiquesDetails fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFormesJuridiquesDetails eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFormesJuridiquesDetails ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFormesJuridiquesDetails fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
