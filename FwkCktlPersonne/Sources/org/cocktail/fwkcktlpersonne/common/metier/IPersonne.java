/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Interface qui permet de simuler la table Personne.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

 public interface IPersonne extends EOEnterpriseObject {

	 String PERSID_KEY = "persId";
	 String NUMERO_KEY = "numero";
	 String NUMERO_INT_KEY = "numeroInt";
	 String NOM_PRENOM_AFFICHAGE_KEY = "nomPrenomAffichage";
	 String NOM_PRENOM_RECHERCHE_KEY = "nomPrenomRecherche";
	 String NOM_PRENOM_COMPLET_AFFICHAGE_KEY = "nomCompletAffichage";
	 String DATE_NAISSANCE_KEY = "dateNaissance";
	 String IS_INTERNE_KEY = "isInterneEtablissement";
	 String TO_FOURNIS_KEY = "toFournis";
	 String PERS_LC_KEY = "persLc";
	 String PERS_LIBELLE_KEY = "persLibelle";
	 String IS_STRUCTURE_KEY = "isStructure";
	 String IS_INDIVIDU_KEY = "isIndividu";

	/** Permet de typer une personne en tant que structure */
	 String PERS_TYPE_STR = "STR";

	/** @return Renvoie le libelle de la personne (a implementer) */
	 String persLibelle();

	/**  @return  Renvoie le libelle court de la personne (a implementer) */
	 String persLc();

	/**  @return  Renvoie le nom patronymique de la personne (a implementer) */
	 String persNomptr();

	/**
	 * @return  Renvoie le type de la personne (STR si structure, sinon civilite) (a implementer)
	 */
	 String persType();

	/**  @return Renvoie l'ID de personne (a implementer) */
	 Integer persId();

	/** @return Renvoie le nom de la personne (nom et prénom affichage) (a implementer) */
	 String getNomPrenomAffichage();

	/** @return Renvoie le nom de la personne (nom et prénom affichage) (a implementer) */
	 String getNomPrenomRecherche();

	/**
	 * @return Renvoie le nom de la personne (nom, nom patronymique et prénom affichage) (a implementer)
	 */
	 String getNomCompletAffichage();

	/**
	 * @return le nom (+ prenom ) et le persId
	 */
	 String getNomEtId();

	/**
	 * @return Renvoie le numéro de la personne (a implementer). Logiquement noIndividu ou cStructure
	 */
	 String getNumero();

	/**
	 * @return Renvoie le numéro de la personne en Integer (a implementer). Logiquement noIndividu ou cStructure
	 */
	 Integer getNumeroInt();

	/**
	 * @return Renvoie les RepartPersonneAdresses associes a la personne (a implementer)
	 */
	 NSArray<EORepartPersonneAdresse> toRepartPersonneAdresses();

	/** @return Renvoie les PersonneTelephones associes a la personne (a implementer) */
	 NSArray<EOPersonneTelephone> toPersonneTelephones();

	/** Associe un RepartPersonneAdresse a la personne (a implementer) */
	 void addToToRepartPersonneAdressesRelationship(EORepartPersonneAdresse repartPersonneAdresse);

	/** Associe un PersonneTelephone a la personne (a implementer) */
	 void addToToPersonneTelephonesRelationship(EOPersonneTelephone personneTelephone);

	/** @return Renvoie les fournisseurs associes a la personne (a implementer) */
	 NSArray<EOFournis> toFourniss();

	/**
	 * @return Le fournisseur trouvé s'il existe (d'abord le valide sinon celui en instance).
	 */
  IFournis toFournis();

	/** @return Renvoie le libelle (affichage) de la personne (a implementer) */
	 String persLibelleAffichage();

	/** @return Renvoie le libelle court (affichage) de la personne (a implementer) */
	 String persLcAffichage();

	/** @return Renvoie le nom patronymique (affichage) de la personne (a implementer) */
	 String persNomptrAffichage();

	/** @return Renvoie la date de naissance de la personne (a implementer) */
	 NSTimestamp dateNaissance();

	/**
	 * @return Renvoie un tableau des repartition de la personne dans les différents groupes
	 */
	 NSArray<EORepartStructure> toRepartStructures();

	 NSArray<EORepartStructure> toRepartStructures(EOQualifier qualifier);

	 void addToToRepartStructuresRelationship(EORepartStructure repartStructure);

	 EOEditingContext editingContext();

	 void removeFromToRepartStructuresRelationship(EORepartStructure repart);

	 void removeFromToPersonneTelephonesRelationship(EOPersonneTelephone unPersonneTelephone);

	 IPersonne localInstanceIn(EOEditingContext editingContext);

	/**
	 * Renvoie les comptes associes a la personne classes suivant la priorite du VLAN (a implementer)
	 */
	 NSArray<EOCompte> toComptes(EOQualifier qualifier);

	 NSArray<EOCompte> toComptes(EOQualifier qualifier, boolean fetch);

	 NSArray<EORepartPersonneAdresse> toRepartPersonneAdresses(EOQualifier qual);

	 void removeFromToRepartPersonneAdressesRelationship(EORepartPersonneAdresse repartPersonneAdresse);

	 void addToToComptesRelationship(EOCompte compte);

	 boolean hasTemporaryGlobalID();

	 EOGlobalID globalID();

	 PersonneDelegate getPersonneDelegate();

	 NSArray<EORepartStructure> getRepartStructuresAffectes(PersonneApplicationUser appUser);

	/**
	 * Doit renvoyer les repartStructure affectes a la personne (au sens groupes affectes)
	 */
	 NSArray<EORepartStructure> getRepartStructuresAffectes(PersonneApplicationUser appUser, EOQualifier qualifierForGroupe);

	/**
	 * Doit renvoyer les repartStructure affectes a la personne (au sens groupes affectes)
	 */
	 NSArray<EORepartStructure> getRepartStructuresAffectes(Integer persIdForUser);

	/**
	 * Les roles de la personne au sein d'un groupe.
	 * 
	 * @param groupe Un groupe
	 * @param qualifier optionnel sur les repartAssociations.
	 * @return Les repartAssociations affectees a la personne au sein du groupe
	 */
	 NSArray<EORepartAssociation> getRepartAssociationsInGroupe(EOStructure groupe, EOQualifier qualifier);

	 String libelleEtId();

	 boolean isStructure();

	 boolean isIndividu();

	 Integer persIdCreation();

	 Integer persIdModification();

	 void setPersIdCreation(Integer persId);

	 void setPersIdModification(Integer persId);

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait deja avec
	 * meme groupe/personne/Association. L'objet repartStructure lié est créé automatiquement si necessaire.
	 * 
	 * @see PersonneDelegate#definitUnRole(EOEditingContext, EOAssociation, EOStructure, Integer, NSTimestamp, NSTimestamp, String, BigDecimal, Integer)
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return L'objet EORepartAssociation
	 */
	 EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur,
	    NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang);

	/**
	 * Définit un role pour une personne au sein d'un groupe. Crée un objet repartAssociation ou bien renvoie un objet repartAssociation qui existait deja avec
	 * meme groupe/personne/Association. L'objet repartStructure lié est créé automatiquement si necessaire.
	 * 
	 * @see PersonneDelegate#definitUnRole(EOEditingContext, EOAssociation, EOStructure, Integer, NSTimestamp, NSTimestamp, String, BigDecimal, Integer)
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @param autoriserGroupeReserve autorise la définition d'un role dans un groupe reserve
	 * @return L'objet EORepartAssociation
	 */
	 EORepartAssociation definitUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur,
	    NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve);

	/**
	 * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau d'EORepartAssociation
	 * nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de roles sans interface.
	 * 
	 * @param ec
	 * @param associations
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @param autoriserGroupeReserve
	 * @return NSArray des EORepartAssociation
	 */
	 NSArray<EORepartAssociation> definitLesRoles(EOEditingContext ec, NSArray<EOAssociation> associations, EOStructure groupe, Integer persIdUtilisateur,
	    NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang, boolean autoriserGroupeReserve);

	/**
	 * Définit les roles pour une personne au sein d'un groupe. Elimine les repartAssociations precedemment existantes Retourne un tableau d'EORepartAssociation
	 * nouvelles L'objet repartStructure lié est créé automatiquement si necessaire. Utilisez cette methode plutot dans le cadre d'ajout de roles sans interface.
	 * 
	 * @param ec
	 * @param associations
	 * @param groupe
	 * @param persIdUtilisateur Le Persid de l'utilisateur
	 * @param rasDOuverture facultatif
	 * @param rasDFermeture facultatif
	 * @param rasCommentaire facultatif
	 * @param rasQuotite facultatif
	 * @param rasRang facultatif
	 * @return NSArray des EORepartAssociation
	 */
	 NSArray<EORepartAssociation> definitLesRoles(EOEditingContext ec, NSArray<EOAssociation> associations, EOStructure groupe, Integer persIdUtilisateur,
	    NSTimestamp rasDOuverture, NSTimestamp rasDFermeture, String rasCommentaire, BigDecimal rasQuotite, Integer rasRang);

	/**
	 * Supprime le repartStructure et les relationShip dependantes.
	 * 
	 * @param repartStructure
	 */
	 void supprimerAffectationAUnGroupe(EOEditingContext ec, Integer persIdUtilisateur, EORepartStructure repartStructure);

	 void supprimerAffectationAUnGroupe(EOEditingContext editingContext, Integer persIdUtilisateur, EOStructure groupe);

	 void supprimerAffectationATousLesGroupes(EOEditingContext ec, Integer persIdUtilisateur);

	/**
	 * Supprime un role specifique (repartAssociation) de la personne au sein d'un groupe commencant a une date donnee.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 * @param rasDOuverture
	 */
	 void supprimerUnRole(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur, NSTimestamp rasDOuverture);

	/**
	 * Supprime les roles specifiques (repartAssociations) de la personne au sein d'un groupe.
	 * 
	 * @param ec
	 * @param association
	 * @param groupe
	 * @param persIdUtilisateur
	 */
	 void supprimerLesRoles(EOEditingContext ec, EOAssociation association, EOStructure groupe, Integer persIdUtilisateur);

	 EOAdresse getAdressePrincipale();

	/**
	 * Indique si la personne est interne à l'établissement.
	 * 
	 * @return
	 */
	 Boolean isInterneEtablissement();

	 void addToToPersonneAliasesRelationship(EOPersonneAlias object);

	 void removeFromToPersonneAliasesRelationship(EOPersonneAlias object);

	/**
	 * @return l'EditingContext de validation.
	 */
	 EOEditingContext getValidationEditingContext();

	 void setValidationEditingContext(EOEditingContext validationEditingContext);

	/**
	 * @return nom et prénom du créateur de la fiche
	 */
	 String retourneCreateur();

	/**
	 * @return nom et prénom du modificateur de la fiche
	 */
	 String retourneModificateur();

	/**
	 * @return date de création de la fiche
	 */
	 NSTimestamp retourneDateCreation();

	/**
	 * @return date de modification de la fiche
	 */
	 NSTimestamp retourneDateModification();

	/**
	 * @return l'adresse de facturation
	 */
	IAdresse adresseFacturation();

}
