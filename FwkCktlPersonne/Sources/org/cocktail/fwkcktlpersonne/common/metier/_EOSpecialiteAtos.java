/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSpecialiteAtos.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOSpecialiteAtos extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOSpecialiteAtos.class);

	public static final String ENTITY_NAME = "Fwkpers_SpecialiteAtos";
	public static final String ENTITY_TABLE_NAME = "GRHUM.SPECIALITE_ATOS";


// Attribute Keys
  public static final ERXKey<String> C_SPECIALITE_ATOS = new ERXKey<String>("cSpecialiteAtos");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LC_SPECIALITE_ATOS = new ERXKey<String>("lcSpecialiteAtos");
  public static final ERXKey<String> LL_SPECIALITE_ATOS = new ERXKey<String>("llSpecialiteAtos");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cSpecialiteAtos";

	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_SPECIALITE_ATOS_KEY = "lcSpecialiteAtos";
	public static final String LL_SPECIALITE_ATOS_KEY = "llSpecialiteAtos";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_SPECIALITE_ATOS_COLKEY = "C_SPECIALITE_ATOS";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LC_SPECIALITE_ATOS_COLKEY = "LC_SPECIALITE_ATOS";
	public static final String LL_SPECIALITE_ATOS_COLKEY = "LL_SPECIALITE_ATOS";



	// Relationships



	// Accessors methods
  public String cSpecialiteAtos() {
    return (String) storedValueForKey(C_SPECIALITE_ATOS_KEY);
  }

  public void setCSpecialiteAtos(String value) {
    takeStoredValueForKey(value, C_SPECIALITE_ATOS_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lcSpecialiteAtos() {
    return (String) storedValueForKey(LC_SPECIALITE_ATOS_KEY);
  }

  public void setLcSpecialiteAtos(String value) {
    takeStoredValueForKey(value, LC_SPECIALITE_ATOS_KEY);
  }

  public String llSpecialiteAtos() {
    return (String) storedValueForKey(LL_SPECIALITE_ATOS_KEY);
  }

  public void setLlSpecialiteAtos(String value) {
    takeStoredValueForKey(value, LL_SPECIALITE_ATOS_KEY);
  }


/**
 * Créer une instance de EOSpecialiteAtos avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSpecialiteAtos createEOSpecialiteAtos(EOEditingContext editingContext, String cSpecialiteAtos
, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOSpecialiteAtos eo = (EOSpecialiteAtos) createAndInsertInstance(editingContext, _EOSpecialiteAtos.ENTITY_NAME);    
		eo.setCSpecialiteAtos(cSpecialiteAtos);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOSpecialiteAtos localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSpecialiteAtos)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSpecialiteAtos creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSpecialiteAtos creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOSpecialiteAtos object = (EOSpecialiteAtos)createAndInsertInstance(editingContext, _EOSpecialiteAtos.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSpecialiteAtos localInstanceIn(EOEditingContext editingContext, EOSpecialiteAtos eo) {
    EOSpecialiteAtos localInstance = (eo == null) ? null : (EOSpecialiteAtos)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSpecialiteAtos#localInstanceIn a la place.
   */
	public static EOSpecialiteAtos localInstanceOf(EOEditingContext editingContext, EOSpecialiteAtos eo) {
		return EOSpecialiteAtos.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOSpecialiteAtos>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSpecialiteAtos fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSpecialiteAtos fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSpecialiteAtos> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSpecialiteAtos eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSpecialiteAtos)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSpecialiteAtos fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSpecialiteAtos fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSpecialiteAtos> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSpecialiteAtos eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSpecialiteAtos)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSpecialiteAtos fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSpecialiteAtos eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSpecialiteAtos ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSpecialiteAtos fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
