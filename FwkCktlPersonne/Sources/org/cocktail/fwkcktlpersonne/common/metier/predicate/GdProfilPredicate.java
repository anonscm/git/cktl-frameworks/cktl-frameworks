package org.cocktail.fwkcktlpersonne.common.metier.predicate;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;

import com.google.common.base.Predicate;

/**
 * 
 * Responsabilité de la classe
 * - Construire des com.google.common.base.Predicate<EOGdProfil>
 *
 */
public class GdProfilPredicate {

	
	/**
	 * Predicate pour filtrer les profils selon les ids
	 * @param ids liste ids à filtrer
	 * @return Predicate
	 */
	public Predicate<EOGdProfil> buildFiltreIds(final List<Integer> ids) {
		
		return new Predicate<EOGdProfil>() {
			public boolean apply(EOGdProfil profil) {
				return !ids.contains(Integer.valueOf(profil.primaryKey()));
			}
		};
		
	}
	
}
