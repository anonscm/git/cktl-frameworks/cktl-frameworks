package org.cocktail.fwkcktlpersonne.common.metier.predicate;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.google.common.base.Predicate;

/**
 * 
 * Responsabilité de la classe :
 * - Construire des com.google.common.base.Predicate
 */
public class StructurePredicate {
	
	
	/**
	 * Filtre par nom d'affichage (case insensitive)
	 * @param searchTerm terme recherché
	 * @return Predicate<IStructure>
	 */
	public Predicate<IStructure> buildContainsIgnoreCaseNomAffichage(final String searchTerm) {
		
		return new Predicate<IStructure>() {
			public boolean apply(IStructure structure) {
				return StringUtils.containsIgnoreCase(structure.getNomCompletAffichage(), searchTerm);
			}
		};
		
	}
	
	/**
	 * Filtre par code structure
	 * @param codeStructure codeStructure
	 * @return Predicate<IStructure>
	 */
	public Predicate<IStructure> buildFiltrerCodeStructure(final String codeStructure) {
		return new Predicate<IStructure>() {
			public boolean apply(IStructure structure) {
				return codeStructure.equals(structure.cStructure());
			}
		};
	}
	
	
	/**
	 * Filtre par IStructure.isArchivee
	 * @return Predicate<IStructure>
	 */
	public Predicate<IStructure> buildFiltrerArchive() {
		return new Predicate<IStructure>() {
			public boolean apply(IStructure structure) {
				return Boolean.FALSE.equals(structure.isArchivee());
			}
		};
	}
	
	
}
