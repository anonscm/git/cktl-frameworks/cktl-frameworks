/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOValideFournis.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Date;

import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IValideFournis;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOValideFournis extends _EOValideFournis implements IValideFournis {
	
	public EOValideFournis() {
		super();
	}

	/**
	 * Controles :
	 * <ul>
	 * 	<li>Si le fournisseur associé est à valide, vérifier que valValidation est biens saisi.</li>
	 * 	<li>Vérifie (si valCreation saisi) que l'utilisateur a les droits de création d'un fournisseur ( {@link EOValideFournis#checkDroitCreation()} ).</li>
	 * 	<li>Vérifie (si valValidation saisi) que l'utilisateur  a les droits de validation d'un fournisseur ( {@link EOValideFournis#checkDroitValidation()} ).</li>
	 * </ul>
	 * 
	 * 
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		//si fournisseur fou_valide=O, l'utilisateur de validation est obligatoire
		if  (EOFournis.FOU_VALIDE_OUI.equals(toFournis().fouValide())) {
			if (valValidation() ==null) {
				throw new NSValidation.ValidationException("L'utilisateur de validation est obligatoire.");
			}
		}
		checkDroitCreation();
        checkDroitValidation();
	}
	
	/**
	 * Verifie si l'utilisateur (si saisi) a les droits de création d'un fournisseur.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkDroitCreation() throws NSValidation.ValidationException {
		 if (valCreation() != null) {
	        	try {
					EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.UTL_ORDRE_KEY, valCreation());
					if (utilisateur == null) {
						throw new NSValidation.ValidationException("Aucun utilisateur ne correspond a l'utl_ordre "+valCreation());
					}
					else {
						PersonneApplicationUser appUser = new PersonneApplicationUser(editingContext(), EOFournis.TYAP_STRID_ANNUAIRE, utilisateur);
						appUser.checkUtilisateurValide(AUtils.now());
						if (!appUser.hasDroitCreationEOFournis(toFournis(), AUtils.currentExercice())) {
							throw new Exception("L'utilisateur " + appUser.getNomAndPrenom() + " n'est pas autorise a creer des fournisseurs.");
						}
					}
				} catch (Exception e) {
					throw new NSValidation.ValidationException(e.getMessage());
				}
	        }
	}
		
	
	/**
	 * Verifie si l'utilisateur si saisi a les droits de validation d'un fournisseur.
	 * @throws NSValidation.ValidationException
	 */
	public void checkDroitValidation() throws NSValidation.ValidationException {
        //verifier que si utl_ordre existe  a les droits de valider
        if (valValidation() != null) {
        	try {
        		EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.UTL_ORDRE_KEY, valValidation());
        		if (utilisateur == null) {
        			throw new NSValidation.ValidationException("Aucun utilisateur ne correspond a l'utl_ordre "+valValidation());
        		}
        		else {
        			PersonneApplicationUser appUser = new PersonneApplicationUser(editingContext(), EOFournis.TYAP_STRID_ANNUAIRE, utilisateur);
        			appUser.checkUtilisateurValide(AUtils.now());
        			if (!appUser.hasDroitValidationEOFournis(toFournis(), AUtils.currentExercice())) {
        				throw new NSValidation.ValidationException("L'utilisateur " + appUser.getNomAndPrenom() + " n'est pas autorise a valider des fournisseurs.");
        			}
        		}
        	} catch (Exception e) {
        		throw new NSValidation.ValidationException(e.getMessage());
        	}
        }		
	}
	
	
	/**
	 * Affecte le appUser au valCreation si celui existant ne correspond pas a un utilisateur qui a les droits.
	 * @param appUser L'utilisateur en cours.
	 */
	public void fixCreateur(PersonneApplicationUser appUser) {
		try {
			checkDroitCreation();
		}
		catch (NSValidation.ValidationException e) {
			setValCreation(appUser.getUtilisateur().utlOrdre());
			setValDateCreate(AUtils.now());
		}
	}
	
	/**
	 * Affecte le appUser au valValidation si celui existant ne correspond pas a un utilisateur qui a les droits.
	 * @param appUser L'utilisateur en cours.
	 */	
	public void fixValidateur(PersonneApplicationUser appUser) {
		try {
			checkDroitValidation();
		}
		catch (NSValidation.ValidationException e) {
			setValValidation(appUser.getUtilisateur().utlOrdre());
		}
	}
	

	public EOUtilisateur getCreateurAsUtilisateur() {
		EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.UTL_ORDRE_KEY, valCreation());
		return utilisateur;
	}
	
	public EOUtilisateur getValideurAsUtilisateur() {
		EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.UTL_ORDRE_KEY, valValidation());
		return utilisateur;
	}

	public void setValDateCreate(Date value) {
		super.setValDateCreate(new NSTimestamp(value));
	}

	public void setValDateVal(Date value) {
		super.setValDateVal(new NSTimestamp(value));
	}

	public void setToFournisRelationship(IFournis value) {
		super.setToFournisRelationship((EOFournis) value);
	}

}
