/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;


import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOGrade extends _EOGrade {

	public static final EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_GRADE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	public static String CODE_SANS_GRADE = "9990";
	public static String CODE_GRADE_DOC_ENS = "6904";		// Grade associe aux doctorants enseignants

	String cGradeNNE;

	public static final String[] LISTE_CATEGORIES = new String[]{"A", "B", "C", "D", "E"};

    public EOGrade() {
        super();
    }
    
    public String codeLibelle() {
    	return cGrade() + " - " + llGrade();
    }
    
	public Boolean isLocal() {
		return true;
		
		//return "O".equals(temLocal());
	}

    public static  EOGrade createEOGrade(EOEditingContext editingContext, EOCorps corps, String cGrade
    		, NSTimestamp dCreation
    		, NSTimestamp dModification
    					) {
    		    EOGrade eo = (EOGrade) createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME);    
    				eo.setCGrade(cGrade);
    				eo.setToCorpsRelationship(corps);
    				eo.setDCreation(dCreation);
    				eo.setDModification(dModification);
    		    return eo;
    		  }

    
	/** Recherche le grade TG dans la table LienGradeMenTG et retourne le premier trouv&eacute;, on ne prend pas en compte
	 * les dates d'ouverture et de fermeture */
	public String cGradeNNE() {
		if (cGradeNNE != null) {
			return cGradeNNE;
		}

		EOFetchSpecification fs = new EOFetchSpecification("LienGradeMenTg", EOQualifier.qualifierWithQualifierFormat("cGrade = %@", new NSArray(cGrade())),null);
		NSArray grades = editingContext().objectsWithFetchSpecification(fs);
		if (grades.count() == 0) {
			return null;
		} else {
			return (String) ((EOGenericRecord) grades.objectAtIndex(0)).valueForKey("cGradeTg");
		}
	}
	/** Retourne null */
	public String cGradeAdage() {
		return null;
	}
	// Interface RecordAvecLibelleEtCode
	public String libelle() {
		String libelle = llGrade();
		if (dOuverture() != null) {
			if (dFermeture() == null) {
				libelle += " ouvert à partir du " + DateCtrl.dateToString(dOuverture());
			} else {
				libelle += " ouvert du " + DateCtrl.dateToString(dOuverture()) + " au " + DateCtrl.dateToString(dFermeture());
			}
		} else if (dFermeture() != null) {
			libelle += " fermé à partir du " + DateCtrl.dateToString(dFermeture());
		}
		return libelle;
	}
	public String code() {
		return cGrade();
	}
	public boolean estSansGrade() {
		return cGrade() != null && cGrade().equals(CODE_SANS_GRADE);
	}
	public boolean estDoctorantEns() {
		return cGrade() != null && cGrade().equals(CODE_GRADE_DOC_ENS);
	}
	

/**
 * Retourne le grade pour code donne
 */
	public static EOGrade rechercherGradePourCode(EOEditingContext ec, String code) {
		
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray(code));
			return fetchFirstByQualifier(ec, qualifier);
		} catch (Exception e) {
			return null;
		}
		
//		String stringQualifier = C_GRADE_KEY + " like '" + CODE_SANS_GRADE + "'";
//		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
//		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
//		NSArray results = editingContext.objectsWithFetchSpecification(fs);
//		try {
//			return (EOGrade)results.objectAtIndex(0);
//		} catch (Exception e) {
//			return null;
//		}
	}


	
	
	
	/** Retourne le grade correspondant a sans grade */
	public static EOGrade rechercherGradePourSansGrade(EOEditingContext editingContext) {
		String stringQualifier = C_GRADE_KEY + " like '" + CODE_SANS_GRADE + "'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier, null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return (EOGrade) results.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	
	/** Recherche les grades fermes ou non
	 * @param editingContext
	 * @param aDateFermeture true si selection des grades avec une date de fermeture
	 */
	public static NSArray rechercherGrades(EOEditingContext editingContext, boolean gestionHU) {

		NSMutableArray qualifiers = new NSMutableArray();

		NSMutableArray orQualifiers = new NSMutableArray();

		if (!gestionHU) {
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + "." + EOCorps.TO_TYPE_POPULATION_KEY + "." + EOTypePopulation.TEM_HOSPITALIER_KEY  + "=%@", new NSArray("N")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + "." + EOCorps.TO_TYPE_POPULATION_KEY  + " = nil", null));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
			
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	
	
	/** Recherche les grades fermes ou non
	 * @param editingContext
	 * @param aDateFermeture true si selection des grades avec une date de fermeture
	 */
	public static NSArray rechercherGrades(EOEditingContext editingContext,boolean aDateFermeture, boolean gestionHU) {

		NSMutableArray qualifiers = new NSMutableArray();

		if (aDateFermeture) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + "!= nil", null));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + "= nil", null));
		}
			
		if (!gestionHU) {
//			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY+"."+EOCorps.TO_TYPE_POPULATION_KEY + "." + EOTypePopulation.TEM_HOSPITALIER_KEY  + "=%@", new NSArray(CocktailConstantes.VRAI)));
//		else
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + "." + EOCorps.TO_TYPE_POPULATION_KEY
					+ "." + EOTypePopulation.TEM_HOSPITALIER_KEY  + "=%@", new NSArray("N")));
		}
			
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	
	public static NSArray rechercherGradesDoctorants(EOEditingContext editingContext) {

		NSMutableArray qualifiers = new NSMutableArray();

		NSMutableArray orQualifiers = new NSMutableArray();
		
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray("6902")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray("6903")));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray("6904")));
			
		qualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	
	
	/** Recherche les grades valides lies a un corps a la date de reference
	 * @param editingContext
	 * @param corps peut etre nul
	 * @param dateReference peut etre nulle
	 */
	public static NSArray rechercherGradesPourCorpsValidesADate(EOEditingContext editingContext,EOCorps corps,NSTimestamp dateReference) {
		return rechercherGradesPourCorpsValidesEtPeriode(editingContext, corps, dateReference, dateReference);
	}
	/** Recherche les grades valides lies a un corps pendant la periode passee en parametre
	 * @param editingContext
	 * @param corps peut etre nul
	 * @param dateDebut peutetre nulle
	 * @param dateFin peut etre nulle
	 */
	public static NSArray rechercherGradesPourCorpsValidesEtPeriode(EOEditingContext editingContext, EOCorps corps,
			NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (corps != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + " = %@", new NSArray(corps)));
		}
		if (dateDebut == null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil", null));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dFermeture  = nil OR dFermeture >= %@)", new NSArray(dateDebut)));
			if (dateFin != null) {
				// champDateDebut = nil or champDateDebut <= finPeriode
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dOuverture = nil OR dOuverture <= %@)", new NSArray(dateFin)));
			}
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	public static NSArray rechercherPourCorps(EOEditingContext ec, EOCorps corps) {

		NSMutableArray qualifiers = new NSMutableArray();				
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(new EOSortOrdering(D_FERMETURE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(new EOSortOrdering(LC_GRADE_KEY, EOSortOrdering.CompareAscending));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + "=%@", new NSArray(corps)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), sorts);
	}
	
	/** Recherche les grades avec le code corps passe en parametre
	 * @param editingContext
	 * @param codeCorps code corps
	 * @param avecTri booleen : vrai si il faut trier les grades par ordre croissant
	 */
	public static NSArray rechercherGradesPourCorps(EOEditingContext editingContext, String codeCorps, NSTimestamp dateValidite, boolean avecTri) {
		NSArray sorts = null;
		if (avecTri) {
			sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(C_GRADE_KEY, EOSortOrdering.CompareAscending));
		}
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(C_CORPS_KEY + " = %@", new NSArray(codeCorps)));
		if (dateValidite != null) {
			NSMutableArray args = new NSMutableArray(dateValidite);
			args.addObject(dateValidite);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_OUVERTURE_KEY + " = nil OR " + D_OUVERTURE_KEY + " <= %@", new NSArray(dateValidite)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil OR " + D_FERMETURE_KEY + " > %@", new NSArray(dateValidite)));
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), sorts);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	
	public static NSArray rechercherGradePourQualifier(EOEditingContext ec, EOQualifier qualifier) {		
		return fetchAll(ec, qualifier, null);	
	}


	/** Recherche les grades avec le code corps passe en parametre
	 * @param editingContext
	 * @param codeCorps code corps
	 */
	public static EOGrade rechercherGradeMinimumPourCorps(EOEditingContext editingContext, String codeCorps, NSTimestamp dateValidite) {
		NSArray grades = rechercherGradesPourCorps(editingContext, codeCorps, dateValidite, true);
		try {
			return (EOGrade) grades.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	
	public String llGradeCir() {
		
		if (cGradeNNE() != null) {
			return llGrade();
		}
			
		return null;
		
	}


    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
        if (cGrade().indexOf(toCorps().cCorps()) == -1) {
			throw new NSValidation.ValidationException("Le code grade doit commencer par le code du corps !");
		}  
			
		if (lcGrade().length() >= 20) {
			throw new NSValidation.ValidationException("Le libellé court du grade doit être renseigné et ne doit pas excéder 20 caractères !");
		}  

		if (llGrade().length() >= 200) {
			throw new NSValidation.ValidationException("Le libellé du grade doit être renseigné et ne doit pas excéder 200 caractères !");
		}  

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
}
