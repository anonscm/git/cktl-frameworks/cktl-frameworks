/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCorps.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCorps extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCorps.class);

	public static final String ENTITY_NAME = "Fwkpers_Corps";
	public static final String ENTITY_TABLE_NAME = "GRHUM.CORPS";


// Attribute Keys
  public static final ERXKey<String> C_CATEGORIE = new ERXKey<String>("cCategorie");
  public static final ERXKey<String> C_CORPS = new ERXKey<String>("cCorps");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_FERMETURE_CORPS = new ERXKey<NSTimestamp>("dFermetureCorps");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_OUVERTURE_CORPS = new ERXKey<NSTimestamp>("dOuvertureCorps");
  public static final ERXKey<String> LC_CORPS = new ERXKey<String>("lcCorps");
  public static final ERXKey<String> LL_CORPS = new ERXKey<String>("llCorps");
  public static final ERXKey<Integer> POTENTIEL_BRUT = new ERXKey<Integer>("potentielBrut");
  public static final ERXKey<String> TEM_CFP = new ERXKey<String>("temCfp");
  public static final ERXKey<String> TEM_CRCT = new ERXKey<String>("temCrct");
  public static final ERXKey<String> TEM_DELEGATION = new ERXKey<String>("temDelegation");
  public static final ERXKey<String> TEM_LOCAL = new ERXKey<String>("temLocal");
  public static final ERXKey<String> TEM_SURNOMBRE = new ERXKey<String>("temSurnombre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> TO_GRADES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>("toGrades");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation> TO_TYPE_POPULATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation>("toTypePopulation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cCorps";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_CORPS_KEY = "dFermetureCorps";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_CORPS_KEY = "dOuvertureCorps";
	public static final String LC_CORPS_KEY = "lcCorps";
	public static final String LL_CORPS_KEY = "llCorps";
	public static final String POTENTIEL_BRUT_KEY = "potentielBrut";
	public static final String TEM_CFP_KEY = "temCfp";
	public static final String TEM_CRCT_KEY = "temCrct";
	public static final String TEM_DELEGATION_KEY = "temDelegation";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_SURNOMBRE_KEY = "temSurnombre";

// Attributs non visibles
	public static final String C_TYPE_CORPS_KEY = "cTypeCorps";
	public static final String C_FILIERE_KEY = "cFiliere";
	public static final String TEM_MISDEP_KEY = "temMisdep";
	public static final String MASSE_INDICIAIRE_KEY = "masseIndiciaire";
	public static final String C_BUREAU_GESTION_KEY = "cBureauGestion";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_CORPS_COLKEY = "D_FERMETURE_CORPS";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_CORPS_COLKEY = "D_OUVERTURE_CORPS";
	public static final String LC_CORPS_COLKEY = "LC_CORPS";
	public static final String LL_CORPS_COLKEY = "LL_CORPS";
	public static final String POTENTIEL_BRUT_COLKEY = "POTENTIEL_BRUT";
	public static final String TEM_CFP_COLKEY = "TEM_CFP";
	public static final String TEM_CRCT_COLKEY = "TEM_CRCT";
	public static final String TEM_DELEGATION_COLKEY = "TEM_DELEGATION";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String TEM_SURNOMBRE_COLKEY = "TEM_SURNOMBRE";

	public static final String C_TYPE_CORPS_COLKEY = "C_TYPE_CORPS";
	public static final String C_FILIERE_COLKEY = "C_FILIERE";
	public static final String TEM_MISDEP_COLKEY = "TEM_MISDEP";
	public static final String MASSE_INDICIAIRE_COLKEY = "MASSE_INDICIAIRE";
	public static final String C_BUREAU_GESTION_COLKEY = "C_BUREAU_GESTION";


	// Relationships
	public static final String TO_GRADES_KEY = "toGrades";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cCorps() {
    return (String) storedValueForKey(C_CORPS_KEY);
  }

  public void setCCorps(String value) {
    takeStoredValueForKey(value, C_CORPS_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermetureCorps() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_CORPS_KEY);
  }

  public void setDFermetureCorps(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_CORPS_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuvertureCorps() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_CORPS_KEY);
  }

  public void setDOuvertureCorps(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_CORPS_KEY);
  }

  public String lcCorps() {
    return (String) storedValueForKey(LC_CORPS_KEY);
  }

  public void setLcCorps(String value) {
    takeStoredValueForKey(value, LC_CORPS_KEY);
  }

  public String llCorps() {
    return (String) storedValueForKey(LL_CORPS_KEY);
  }

  public void setLlCorps(String value) {
    takeStoredValueForKey(value, LL_CORPS_KEY);
  }

  public Integer potentielBrut() {
    return (Integer) storedValueForKey(POTENTIEL_BRUT_KEY);
  }

  public void setPotentielBrut(Integer value) {
    takeStoredValueForKey(value, POTENTIEL_BRUT_KEY);
  }

  public String temCfp() {
    return (String) storedValueForKey(TEM_CFP_KEY);
  }

  public void setTemCfp(String value) {
    takeStoredValueForKey(value, TEM_CFP_KEY);
  }

  public String temCrct() {
    return (String) storedValueForKey(TEM_CRCT_KEY);
  }

  public void setTemCrct(String value) {
    takeStoredValueForKey(value, TEM_CRCT_KEY);
  }

  public String temDelegation() {
    return (String) storedValueForKey(TEM_DELEGATION_KEY);
  }

  public void setTemDelegation(String value) {
    takeStoredValueForKey(value, TEM_DELEGATION_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public String temSurnombre() {
    return (String) storedValueForKey(TEM_SURNOMBRE_KEY);
  }

  public void setTemSurnombre(String value) {
    takeStoredValueForKey(value, TEM_SURNOMBRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation toTypePopulation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation)storedValueForKey(TO_TYPE_POPULATION_KEY);
  }

  public void setToTypePopulationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_POPULATION_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> toGrades() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>)storedValueForKey(TO_GRADES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> toGrades(EOQualifier qualifier) {
    return toGrades(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> toGrades(EOQualifier qualifier, boolean fetch) {
    return toGrades(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> toGrades(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOGrade.TO_CORPS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOGrade.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGrades();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGradesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GRADES_KEY);
  }

  public void removeFromToGradesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GRADES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOGrade createToGradesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Grade");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GRADES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGrade) eo;
  }

  public void deleteToGradesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGrade object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GRADES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGradesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> objects = toGrades().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGradesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCorps avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCorps createEOCorps(EOEditingContext editingContext, String cCorps
, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOCorps eo = (EOCorps) createAndInsertInstance(editingContext, _EOCorps.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOCorps localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCorps)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCorps creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCorps creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCorps object = (EOCorps)createAndInsertInstance(editingContext, _EOCorps.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCorps localInstanceIn(EOEditingContext editingContext, EOCorps eo) {
    EOCorps localInstance = (eo == null) ? null : (EOCorps)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCorps#localInstanceIn a la place.
   */
	public static EOCorps localInstanceOf(EOEditingContext editingContext, EOCorps eo) {
		return EOCorps.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCorps> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCorps)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCorps> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCorps)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCorps ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCorps fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
