/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.*;

public class EOIndice extends _EOIndice {

    public EOIndice() {
        super();
    }
   
    /**
	 * 
	 * Retourne la valeur de l'indice majore pour un indice brut et une date donnee.
	 * 
	 * @param editingContext
	 * @param indiceBrut
	 * @param date
	 * @return
	 */
	public static EOIndice indiceMajorePourIndiceBrutEtDate(EOEditingContext editingContext,String indiceBrut,NSTimestamp date) {

    	try {
    		NSMutableArray qualifiers = new NSMutableArray();

    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_BRUT_KEY + "=%@", new NSArray(indiceBrut)));
    		
    		if (date == null)
    			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+"= nil", null));
    		else {
    			NSMutableArray orQualifiers = new NSMutableArray();
    			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_MAJORATION_KEY+"=nil", null));
    			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_MAJORATION_KEY+"<=%@", new NSArray(date)));

    			qualifiers.addObject(new EOOrQualifier(orQualifiers));
    			
    			orQualifiers = new NSMutableArray();
    			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+"=nil", null));
    			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+">=%@", new NSArray(date)));

    			qualifiers.addObject(new EOOrQualifier(orQualifiers));
    		}    		
    		
    		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
    	
    	} catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    /** Retourne les indices associes a l'indice majore valables a la date fournie. Retourne null si date est nulle ou si non trouve */
    public static NSArray indicesPourIndiceMajoreEtDate(EOEditingContext editingContext,Number indiceMajore,NSTimestamp date) {
    	if (date == null)
    		return new NSArray();

    	NSMutableArray args = new NSMutableArray(indiceMajore);
    	args.addObject(date);
    	args.addObject(date);
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(" cIndiceMajore = %@ AND (dMajoration = nil OR dMajoration <= %@) AND (dFermeture = nil OR dFermeture >= %@)", args);
    	
    	return fetchAll(editingContext, qualifier);

    }

    
    
}
