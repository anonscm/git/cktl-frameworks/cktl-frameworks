/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReferensActivites.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOReferensActivites extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOReferensActivites.class);

	public static final String ENTITY_NAME = "Fwkpers_ReferensActivites";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REFERENS_ACTIVITES";


// Attribute Keys
  public static final ERXKey<String> ID_TYPE_GROUPEMENT = new ERXKey<String>("idTypeGroupement");
  public static final ERXKey<String> INTITUL_ACTIVITE = new ERXKey<String>("intitulActivite");
  public static final ERXKey<String> INTITUL_ACTIVITE_CLEAN = new ERXKey<String>("intitulActiviteClean");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois> TO_REFERENS_EMPLOIS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois>("toReferensEmplois");

	// Attributes


	public static final String ID_TYPE_GROUPEMENT_KEY = "idTypeGroupement";
	public static final String INTITUL_ACTIVITE_KEY = "intitulActivite";
	public static final String INTITUL_ACTIVITE_CLEAN_KEY = "intitulActiviteClean";

// Attributs non visibles
	public static final String ORDRE_KEY = "ordre";
	public static final String CODE_EMPLOI_KEY = "codeEmploi";

//Colonnes dans la base de donnees
	public static final String ID_TYPE_GROUPEMENT_COLKEY = "IDTYPEGROUPEMENT";
	public static final String INTITUL_ACTIVITE_COLKEY = "INTITULACTIVITE";
	public static final String INTITUL_ACTIVITE_CLEAN_COLKEY = "INTITULACTIVITE_CLEAN";

	public static final String ORDRE_COLKEY = "ORDRE";
	public static final String CODE_EMPLOI_COLKEY = "CODEEMPLOI";


	// Relationships
	public static final String TO_REFERENS_EMPLOIS_KEY = "toReferensEmplois";



	// Accessors methods
  public String idTypeGroupement() {
    return (String) storedValueForKey(ID_TYPE_GROUPEMENT_KEY);
  }

  public void setIdTypeGroupement(String value) {
    takeStoredValueForKey(value, ID_TYPE_GROUPEMENT_KEY);
  }

  public String intitulActivite() {
    return (String) storedValueForKey(INTITUL_ACTIVITE_KEY);
  }

  public void setIntitulActivite(String value) {
    takeStoredValueForKey(value, INTITUL_ACTIVITE_KEY);
  }

  public String intitulActiviteClean() {
    return (String) storedValueForKey(INTITUL_ACTIVITE_CLEAN_KEY);
  }

  public void setIntitulActiviteClean(String value) {
    takeStoredValueForKey(value, INTITUL_ACTIVITE_CLEAN_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois)storedValueForKey(TO_REFERENS_EMPLOIS_KEY);
  }

  public void setToReferensEmploisRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois oldValue = toReferensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_EMPLOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_EMPLOIS_KEY);
    }
  }
  

/**
 * Créer une instance de EOReferensActivites avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReferensActivites createEOReferensActivites(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOReferensEmplois toReferensEmplois			) {
    EOReferensActivites eo = (EOReferensActivites) createAndInsertInstance(editingContext, _EOReferensActivites.ENTITY_NAME);    
    eo.setToReferensEmploisRelationship(toReferensEmplois);
    return eo;
  }

  
	  public EOReferensActivites localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReferensActivites)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensActivites creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensActivites creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOReferensActivites object = (EOReferensActivites)createAndInsertInstance(editingContext, _EOReferensActivites.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReferensActivites localInstanceIn(EOEditingContext editingContext, EOReferensActivites eo) {
    EOReferensActivites localInstance = (eo == null) ? null : (EOReferensActivites)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReferensActivites#localInstanceIn a la place.
   */
	public static EOReferensActivites localInstanceOf(EOEditingContext editingContext, EOReferensActivites eo) {
		return EOReferensActivites.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReferensActivites fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReferensActivites fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOReferensActivites> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReferensActivites eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReferensActivites)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReferensActivites fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReferensActivites fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOReferensActivites> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReferensActivites eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReferensActivites)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReferensActivites fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReferensActivites eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReferensActivites ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReferensActivites fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
