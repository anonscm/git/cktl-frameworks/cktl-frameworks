

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;


import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;


public class EODepartement extends _EODepartement implements IDepartement
{
	public static final String SANS_DPT = "000";
	
	public static final EOSortOrdering SORT_LL_DEPARTEMENT = EOSortOrdering.sortOrderingWithKey(EODepartement.LL_DEPARTEMENT_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_C_DEPARTEMENT = EOSortOrdering.sortOrderingWithKey(EODepartement.C_DEPARTEMENT_KEY, EOSortOrdering.CompareAscending);

	public static final EOQualifier QUAL_SANS_DPT = EODepartement.C_DEPARTEMENT.eq(EODepartement.SANS_DPT);
	
    public EODepartement() {
        super();
    }

	/**
	 * recherche les departements en fonction du filtre passé (filtrage par code ou libelle)
	 * @param edc
	 * @param filtDept la valeur utilisée comme filtre sur le code ou le libelle
	 * @param allDepts le tableau à filtrer
	 * @return tableau filtré
	 */
	
	public static NSArray getFilteredDepartements(EOEditingContext edc, String filtreDept,NSArray allDepts, boolean returnAllIfFilterEmpty, boolean returnAllIfResultEmpty) {

		if (allDepts == null)
			allDepts = EODepartement.fetchAll(edc, new NSArray(new Object[] {
					EODepartement.SORT_C_DEPARTEMENT
			}));
	
		if (filtreDept == null || filtreDept.trim().length() == 0) {
			if (returnAllIfFilterEmpty) {
				return allDepts;
			} else {
				return NSArray.EmptyArray;
			}
		}
		filtreDept = filtreDept.trim();
		NSMutableArray quals = new NSMutableArray();
		filtreDept = MyStringCtrl.chaineSansAccents(filtreDept, "?");
		EOQualifier qualDpt = ERXQ.or(
				ERXQ.likeInsensitive(EODepartement.C_DEPARTEMENT_KEY, '*' + filtreDept + '*'),
				ERXQ.likeInsensitive(EODepartement.LL_DEPARTEMENT_KEY, '*' + filtreDept + '*'),
				ERXQ.likeInsensitive(EODepartement.LC_DEPARTEMENT_KEY, '*' + filtreDept + '*'));
		NSArray res = EOQualifier.filteredArrayWithQualifier(allDepts, qualDpt);
		return res;
	}

	public static NSArray getFilteredDepartements(EOEditingContext edc, String filtreDept, NSArray allDepts) {
		return getFilteredDepartements(edc, filtreDept, allDepts, false, false);

	}

	/*
	 * retourne le libelle du departement et le code
	 */
	public String getLibelleAndCode() {
		return llDepartement() + " (" + cDepartement() + ")";
	}

    public String getDepartementFrance() {
        if (StringCtrl.isEmpty(cDepartement())) { return null; }
        
        if(cDepartement().startsWith("0")) return cDepartement().substring(1);
        
        return cDepartement();
    }

    /**
     * Renvoie la liste des départements valides triés
     * @param edc : editing context
     * @param sortOrderings : tri
     * @return liste départements valides
     */
    public static NSArray<EODepartement> fetchDepartementsValides(EOEditingContext edc, NSArray<EOSortOrdering> sortOrderings) {
    	EOQualifier qualifier = ERXQ.isNull(EODepartement.D_FIN_VAL_KEY);
    	
    	return fetchAll(edc, qualifier, sortOrderings);
    }
    
    /**
     * Retourne une liste filtrée par date des départements
     * @param edc Editing Context de la recherche
     * @param filtreDept filtre sur les départements
     * @param allDepts liste de départements
     * @param dateFiltrage date servant au filtrage
     * @return tableau contenant la liste des départements filtrés en accord avec la date passée
     */
    public static NSArray getFilteredDepartementsByDate(EOEditingContext edc, String filtreDept, NSArray allDepts, NSTimestamp dateFiltrage) {
    	EOQualifier qualifierTemporel;
    	EOQualifier qualDateFin;
    	EOQualifier qualDateDebut;
		final EOQualifier QUAL_DATE_FIN_NULLE = new EOKeyValueQualifier(EODepartement.D_FIN_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
		final EOQualifier QUAL_DATE_DEBUT_NOT_NULLE = new EOKeyValueQualifier(EODepartement.D_DEB_VAL_KEY, EOQualifier.QualifierOperatorNotEqual, null);
		final EOQualifier QUAL_DATE_DEBUT_NULLE = new EOKeyValueQualifier(EODepartement.D_DEB_VAL_KEY, EOQualifier.QualifierOperatorEqual, null);
    	
    	qualDateFin = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				QUAL_DATE_FIN_NULLE,
				ERXQ.greaterThanOrEqualTo(EODepartement.D_FIN_VAL_KEY, dateFiltrage)
		}));
    	qualDateDebut = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
    			QUAL_DATE_DEBUT_NULLE,
    			ERXQ.lessThanOrEqualTo(EODepartement.D_DEB_VAL_KEY, dateFiltrage)
    	}));
    	
		qualifierTemporel = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				qualDateDebut,
				qualDateFin
		}));
		
		return EODepartement.fetchAll(edc, qualifierTemporel, ERXS.asc(C_DEPARTEMENT_KEY).array());

	}
    
    public String getCode() {
    	return cDepartement();    	
    }
    
    @Override
    public String toString() {
    	return this.getDepartementFrance() + " - " + this.llDepartement();
    }
    
}
