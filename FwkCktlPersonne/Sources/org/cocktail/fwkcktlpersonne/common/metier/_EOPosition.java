/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPosition.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPosition extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPosition.class);

	public static final String ENTITY_NAME = "Fwkpers_Position";
	public static final String ENTITY_TABLE_NAME = "GRHUM.POSITION";


// Attribute Keys
  public static final ERXKey<String> C_POSITION = new ERXKey<String>("cPosition");
  public static final ERXKey<String> C_POSITION_GESUP = new ERXKey<String>("cPositionGesup");
  public static final ERXKey<String> C_POSITION_ONP = new ERXKey<String>("cPositionOnp");
  public static final ERXKey<Integer> DUREE_MAX_PERIOD_POS = new ERXKey<Integer>("dureeMaxPeriodPos");
  public static final ERXKey<String> LC_POSITION = new ERXKey<String>("lcPosition");
  public static final ERXKey<String> LL_POSITION = new ERXKey<String>("llPosition");
  public static final ERXKey<Integer> PRCT_DROIT_AVCT_ECH = new ERXKey<Integer>("prctDroitAvctEch");
  public static final ERXKey<Integer> PRCT_DROIT_AVCT_GRAD = new ERXKey<Integer>("prctDroitAvctGrad");
  public static final ERXKey<String> TEM_ACTIVITE = new ERXKey<String>("temActivite");
  public static final ERXKey<String> TEM_CLM_SUSPEND = new ERXKey<String>("temClmSuspend");
  public static final ERXKey<String> TEM_CTRA_TRAV = new ERXKey<String>("temCtraTrav");
  public static final ERXKey<String> TEM_DETACHEMENT = new ERXKey<String>("temDetachement");
  public static final ERXKey<String> TEM_D_FIN_OBLIG = new ERXKey<String>("temDFinOblig");
  public static final ERXKey<String> TEM_ENFANT = new ERXKey<String>("temEnfant");
  public static final ERXKey<String> TEM_STAGIAIRE = new ERXKey<String>("temStagiaire");
  public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
  public static final ERXKey<String> TEM_VALIDE = new ERXKey<String>("temValide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> TO_MOTIF_POSITION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>("toMotifPosition");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cPosition";

	public static final String C_POSITION_KEY = "cPosition";
	public static final String C_POSITION_GESUP_KEY = "cPositionGesup";
	public static final String C_POSITION_ONP_KEY = "cPositionOnp";
	public static final String DUREE_MAX_PERIOD_POS_KEY = "dureeMaxPeriodPos";
	public static final String LC_POSITION_KEY = "lcPosition";
	public static final String LL_POSITION_KEY = "llPosition";
	public static final String PRCT_DROIT_AVCT_ECH_KEY = "prctDroitAvctEch";
	public static final String PRCT_DROIT_AVCT_GRAD_KEY = "prctDroitAvctGrad";
	public static final String TEM_ACTIVITE_KEY = "temActivite";
	public static final String TEM_CLM_SUSPEND_KEY = "temClmSuspend";
	public static final String TEM_CTRA_TRAV_KEY = "temCtraTrav";
	public static final String TEM_DETACHEMENT_KEY = "temDetachement";
	public static final String TEM_D_FIN_OBLIG_KEY = "temDFinOblig";
	public static final String TEM_ENFANT_KEY = "temEnfant";
	public static final String TEM_STAGIAIRE_KEY = "temStagiaire";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String TEM_RESERVE_PST_KEY = "temReservePst";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_SN_KEY = "temSn";
	public static final String D_CREATION_KEY = "dCreation";

//Colonnes dans la base de donnees
	public static final String C_POSITION_COLKEY = "C_POSITION";
	public static final String C_POSITION_GESUP_COLKEY = "C_POSITION_GESUP";
	public static final String C_POSITION_ONP_COLKEY = "C_POSITION_ONP";
	public static final String DUREE_MAX_PERIOD_POS_COLKEY = "DUREE_MAX_PERIOD_POS";
	public static final String LC_POSITION_COLKEY = "LC_POSITION";
	public static final String LL_POSITION_COLKEY = "LL_POSITION";
	public static final String PRCT_DROIT_AVCT_ECH_COLKEY = "PRCT_DROIT_AVCT_ECH";
	public static final String PRCT_DROIT_AVCT_GRAD_COLKEY = "PRCT_DROIT_AVCT_GRAD";
	public static final String TEM_ACTIVITE_COLKEY = "TEM_ACTIVITE";
	public static final String TEM_CLM_SUSPEND_COLKEY = "TEM_CLM_SUSPEND";
	public static final String TEM_CTRA_TRAV_COLKEY = "TEM_CTRA_TRAV";
	public static final String TEM_DETACHEMENT_COLKEY = "TEM_DETACHEMENT";
	public static final String TEM_D_FIN_OBLIG_COLKEY = "TEM_D_FIN_OBLIG";
	public static final String TEM_ENFANT_COLKEY = "TEM_ENFANT";
	public static final String TEM_STAGIAIRE_COLKEY = "TEM_STAGIAIRE";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String TEM_RESERVE_PST_COLKEY = "TEM_RESERVE_PST";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_SN_COLKEY = "TEM_SN";
	public static final String D_CREATION_COLKEY = "D_CREATION";


	// Relationships
	public static final String TO_MOTIF_POSITION_KEY = "toMotifPosition";



	// Accessors methods
  public String cPosition() {
    return (String) storedValueForKey(C_POSITION_KEY);
  }

  public void setCPosition(String value) {
    takeStoredValueForKey(value, C_POSITION_KEY);
  }

  public String cPositionGesup() {
    return (String) storedValueForKey(C_POSITION_GESUP_KEY);
  }

  public void setCPositionGesup(String value) {
    takeStoredValueForKey(value, C_POSITION_GESUP_KEY);
  }

  public String cPositionOnp() {
    return (String) storedValueForKey(C_POSITION_ONP_KEY);
  }

  public void setCPositionOnp(String value) {
    takeStoredValueForKey(value, C_POSITION_ONP_KEY);
  }

  public Integer dureeMaxPeriodPos() {
    return (Integer) storedValueForKey(DUREE_MAX_PERIOD_POS_KEY);
  }

  public void setDureeMaxPeriodPos(Integer value) {
    takeStoredValueForKey(value, DUREE_MAX_PERIOD_POS_KEY);
  }

  public String lcPosition() {
    return (String) storedValueForKey(LC_POSITION_KEY);
  }

  public void setLcPosition(String value) {
    takeStoredValueForKey(value, LC_POSITION_KEY);
  }

  public String llPosition() {
    return (String) storedValueForKey(LL_POSITION_KEY);
  }

  public void setLlPosition(String value) {
    takeStoredValueForKey(value, LL_POSITION_KEY);
  }

  public Integer prctDroitAvctEch() {
    return (Integer) storedValueForKey(PRCT_DROIT_AVCT_ECH_KEY);
  }

  public void setPrctDroitAvctEch(Integer value) {
    takeStoredValueForKey(value, PRCT_DROIT_AVCT_ECH_KEY);
  }

  public Integer prctDroitAvctGrad() {
    return (Integer) storedValueForKey(PRCT_DROIT_AVCT_GRAD_KEY);
  }

  public void setPrctDroitAvctGrad(Integer value) {
    takeStoredValueForKey(value, PRCT_DROIT_AVCT_GRAD_KEY);
  }

  public String temActivite() {
    return (String) storedValueForKey(TEM_ACTIVITE_KEY);
  }

  public void setTemActivite(String value) {
    takeStoredValueForKey(value, TEM_ACTIVITE_KEY);
  }

  public String temClmSuspend() {
    return (String) storedValueForKey(TEM_CLM_SUSPEND_KEY);
  }

  public void setTemClmSuspend(String value) {
    takeStoredValueForKey(value, TEM_CLM_SUSPEND_KEY);
  }

  public String temCtraTrav() {
    return (String) storedValueForKey(TEM_CTRA_TRAV_KEY);
  }

  public void setTemCtraTrav(String value) {
    takeStoredValueForKey(value, TEM_CTRA_TRAV_KEY);
  }

  public String temDetachement() {
    return (String) storedValueForKey(TEM_DETACHEMENT_KEY);
  }

  public void setTemDetachement(String value) {
    takeStoredValueForKey(value, TEM_DETACHEMENT_KEY);
  }

  public String temDFinOblig() {
    return (String) storedValueForKey(TEM_D_FIN_OBLIG_KEY);
  }

  public void setTemDFinOblig(String value) {
    takeStoredValueForKey(value, TEM_D_FIN_OBLIG_KEY);
  }

  public String temEnfant() {
    return (String) storedValueForKey(TEM_ENFANT_KEY);
  }

  public void setTemEnfant(String value) {
    takeStoredValueForKey(value, TEM_ENFANT_KEY);
  }

  public String temStagiaire() {
    return (String) storedValueForKey(TEM_STAGIAIRE_KEY);
  }

  public void setTemStagiaire(String value) {
    takeStoredValueForKey(value, TEM_STAGIAIRE_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> toMotifPosition() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>)storedValueForKey(TO_MOTIF_POSITION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> toMotifPosition(EOQualifier qualifier) {
    return toMotifPosition(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> toMotifPosition(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> results;
      results = toMotifPosition();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToMotifPositionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_MOTIF_POSITION_KEY);
  }

  public void removeFromToMotifPositionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MOTIF_POSITION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition createToMotifPositionRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_MotifPosition");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_MOTIF_POSITION_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition) eo;
  }

  public void deleteToMotifPositionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MOTIF_POSITION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToMotifPositionRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOMotifPosition> objects = toMotifPosition().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToMotifPositionRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPosition avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPosition createEOPosition(EOEditingContext editingContext, String cPosition
, String temActivite
, String temClmSuspend
, String temCtraTrav
, String temDetachement
, String temDFinOblig
, String temStagiaire
, String temTitulaire
, String temValide
			) {
    EOPosition eo = (EOPosition) createAndInsertInstance(editingContext, _EOPosition.ENTITY_NAME);    
		eo.setCPosition(cPosition);
		eo.setTemActivite(temActivite);
		eo.setTemClmSuspend(temClmSuspend);
		eo.setTemCtraTrav(temCtraTrav);
		eo.setTemDetachement(temDetachement);
		eo.setTemDFinOblig(temDFinOblig);
		eo.setTemStagiaire(temStagiaire);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOPosition localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPosition)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPosition creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPosition creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPosition object = (EOPosition)createAndInsertInstance(editingContext, _EOPosition.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPosition localInstanceIn(EOEditingContext editingContext, EOPosition eo) {
    EOPosition localInstance = (eo == null) ? null : (EOPosition)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPosition#localInstanceIn a la place.
   */
	public static EOPosition localInstanceOf(EOEditingContext editingContext, EOPosition eo) {
		return EOPosition.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPosition>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPosition fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPosition fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPosition> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPosition)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPosition> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPosition)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPosition fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPosition eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPosition ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPosition fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
