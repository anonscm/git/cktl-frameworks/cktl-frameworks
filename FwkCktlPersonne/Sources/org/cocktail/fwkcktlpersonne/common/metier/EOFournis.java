/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IValideFournis;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * Classe permettant la gestion des objets fournisseurs.<br/>
 * <ul>
 * <li>Règles d'initialisation : {@link EOAdresse#awakeFromInsertion(EOEditingContext)}</li>
 * <li>Règles de validation : {@link EOAdresse#validateObjectMetier()}</li>
 * </ul>
 * Des règles doivent également être respectées pour les structures ou les individus fournisseurs. Cf. {@link EOStructureForFournisseurSpec} et
 * {@link EOIndividuForFournisseurSpec}.<br/>
 * Pour l'initialisation de l'objet, privilégiez la méthode {@link EOFournis#initialise(Integer, Integer, EOAdresse, boolean, String, EORib)} qui est
 * sensée vous simplifier la vie.<br/>
 * <b>Exemple : Creation d'un fournisseur</b><br/>
 * <code>
 *   EOFournis fournis = EOFournis.creerInstance(myEditingContext);<br/>
 *   fournis.setToStructure(uneStructure); // ou fournis.setToIndividu(unIndividu); <br/>
 *   fournis.initialise( null, utlOrdreForUser, adresseFacturation, isEtranger, fouType, unRib); <br/>
 *   ...<br/>
 *   myEditingContext.saveChanges();<br/>
 * </code>
 * <p>
 * Le compte pour le fournisseur n'est pas systematiquement cree. Utilisez la methode {@link #associerCompte(ApplicationUser)} ou
 * {@link #associerCompte(Integer, Integer)} si vous voulez creer un compte pour le fournisseur. La politique de création de compte de connexion pour
 * les fournisseurs est définie dans le parametre GRHUM ANNUAIRE_FOU_COMPTE_AUTO.
 * </p>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EOFournis extends _EOFournis implements IFournis {
	public final static Logger logger = Logger.getLogger(EOFournis.class);
	public static final EOSortOrdering SORT_FOU_CODE_DESC = EOSortOrdering.sortOrderingWithKey(EOFournis.FOU_CODE_KEY, EOSortOrdering.CompareDescending);

	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_PERSONNE_KEY = "toPersonne";

	public static final String FOU_TYPE_CLIENT = "C";
	public static final String FOU_TYPE_FOURNISSEUR = "F";

	/** Type client + fournisseur */
	public static final String FOU_TYPE_TIERS = "T";

	public static final String FOU_VALIDE_OUI = "O";
	public static final String FOU_VALIDE_NON = "N";
	public static final String FOU_VALIDE_ANNULE = "A";
	public static final String FOU_ETRANGER_OUI = "O";
	public static final String FOU_ETRANGER_NON = "N";
	public static final String FOU_MARCHE_0 = "0";

	public static final String FOU_TYPE_MAL_RENSEIGNE = "Le fouType est mal renseigne.";
	public static final String EXCEPTION_FOU_CODE = "Le code fournisseur doit comporter 8 caractères (3 lettres + 5 chiffres).";
	public static final String EXCEPTION_PERSONNE = "Un individu ou une structure doit être défini pour le fournisseur.";
	public static final String EXCEPTION_ADRESSE = "Une adresse est obligatoire pour le fournisseur.";
	public static final String EXCEPTION_ADRESSE_FACT = "Une adresse de facturation est obligatoire pour le fournisseur.";
	public static final String EXCEPTION_RIB_MANQUANT = "Un rib est obligatoire pour le fournisseur.";

	private static final Format FORMAT_FOU_CODE_NUM = new DecimalFormat("00000");
	public static final String TYAP_STRID_ANNUAIRE = "ANNUAIRE";
	public static final String FON_ID_CREATION_FOURNISSEUR = "ANFOUCR";
	public static final String FON_ID_VALIDATION_FOURNISSEUR = "ANFOUVAL";

	public static final EOQualifier QUAL_FOU_VALIDE_OUI = EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + "=%@", new NSArray(new Object[] {
			FOU_VALIDE_OUI
	}));
	public static final EOQualifier QUAL_FOU_VALIDE_INSTANCE = EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + "=%@", new NSArray(new Object[] {
			FOU_VALIDE_NON
	}));
	public static final EOQualifier QUAL_FOU_VALIDE_ANNULE = EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_VALIDE_KEY + "=%@", new NSArray(new Object[] {
			FOU_VALIDE_ANNULE
	}));
	public static final EOQualifier QUAL_RIB_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOFournis.TO_RIBS_KEY + "." + EORib.RIB_VALIDE_KEY + "='O'", null);

	public static final String EMAIL_VALIDFOURNIS_PARAMKEY = "EMAIL_VALIDFOURNIS";
	public static final String GRHUM_HOST_MAIL_PARAMKEY = "GRHUM_HOST_MAIL";

	public EOFournis() {
		super();
		setValidatedWhenNested(false);
	}

	public void setToStructure(EOStructure structure) {
		if (structure != null) {
			addToToStructuresRelationship(structure);
			setPersId(structure.persId());
		}
	}

	public void setToIndividu(EOIndividu individu) {
		if (toIndividu() == null) {
			addToToIndividusRelationship(individu);
			setPersId(individu.persId());
		}
	}

	public void setToPersonne(IPersonne personne) {
		if (personne.isIndividu()) {
			setToIndividu((EOIndividu) personne);
		}
		else {
			setToStructure((EOStructure) personne);
		}
	}

	/**
	 * Modifications :
	 * <ul>
	 * <li>Date de modification</li>
	 * </ul>
	 * Contrôles :
	 * <ul>
	 * <li>Vérifier que l'adresse est saisie, qu'elle est affectée a la personne associee et qu'elle est de type FACT</li>
	 * <li>Vérifier que le type de fournisseur est correct</li>
	 * <li>Vérifier que le code fournisseur est au bon format</li>
	 * <li>Vérifier que le compte de connexion est cree (valide ou non) si le fournisseur est valide (suivant parametre)</li>
	 * <li>Vérifier la coherence avec les groupes {@link EOStructureForFournisseurSpec#checkGroupes(EOStructure)} ou
	 * {@link EOIndividuForFournisseurSpec#checkGroupes(EOIndividu)}</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		try {
			setDModification(AUtils.now());
			trimAllString();

			if (isObjectInValidationEditingContext()) {
				checkContraintesObligatoires();
				checkContraintesLongueursMax();

				if (!isStructure() && !isIndividu()) {
					throw new NSValidation.ValidationException(EXCEPTION_PERSONNE);
				}
				checkAdresse();
				checkFouType();
				checkFouCodeValide(fouCode());
				checkDroits();
				//Verifier qu'il y a bien un compte (suivant parametre)
				checkCompte();

				//Verifier les groupes associes en activant les specificites sur l'individu ou la structure associee
				if (isStructure()) {
					if (toStructure().registerDetectedSpec()) {
						toStructure().willChange();
					}
				}
				if (isIndividu()) {
					if (toIndividu().registerDetectedSpec()) {
						toIndividu().willChange();
					}
				}

				//			if (isStructure()) {
				//				EOStructureForFournisseurSpec.checkGroupes(toStructure());
				//			}
				//			if (isIndividu()) {
				//				EOIndividuForFournisseurSpec.checkGroupes(toIndividu());
				//			}
				super.validateObjectMetier();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage() + " (" + toPersonne().getNomEtId() + " / fou_code=" + fouCode() + ")");
		}
	}

	/**
	 * Verifie si le fouCode respecte le bon format. En l'occurence on verifie sa taille totale, si le premier caractere est une lettre, et si les 5
	 * derniers caractères sont numériques.
	 * 
	 * @param fouCode
	 * @throws NSValidation.ValidationException
	 */
	public void checkFouCodeValide(String fouCode) throws NSValidation.ValidationException {
		if (fouCode == null) {
			throw new NSValidation.ValidationException("Le code specifie est null.");
		}

		if (fouCode.trim().length() != 8) {
			throw new NSValidation.ValidationException("Le code fournisseur doit comporter 8 caracteres.");
		}
		if (!MyStringCtrl.isAcceptChar(fouCode.trim().charAt(0), null)) {
			throw new NSValidation.ValidationException("Le code fournisseur doit commencer par une lettre.");
		}
		String fin = fouCode.trim().substring(3, 8);
		for (int i = 0; i < 5; i++) {
			if (!MyStringCtrl.isBasicDigit(fin.charAt(i))) {
				throw new NSValidation.ValidationException("Le code fournisseur doit se terminer par 5 chiffres.");
			}
		}
	}

	private void checkFouType() {
		if (fouType() == null || !(FOU_TYPE_CLIENT.equals(fouType()) || FOU_TYPE_FOURNISSEUR.equals(fouType()) || FOU_TYPE_TIERS.equals(fouType()))) {
			throw new NSValidation.ValidationException(FOU_TYPE_MAL_RENSEIGNE);
		}
	}

	/**
	 * Verifie si une adresse est affectee au fournisseur et si cette adresse est bien de type FACT.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkAdresse() throws NSValidation.ValidationException {
		if (toAdresse() != null) {
			// verifier si l'adresse est FACT
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TO_ADRESSE_KEY + "." + EOAdresse.ADR_ORDRE_KEY + "=%@ and " + EORepartPersonneAdresse.TADR_CODE_KEY + " =%@", new NSArray(new Object[] {
					toAdresse().adrOrdre(), EOTypeAdresse.TADR_CODE_FACT
			}));
			NSArray res = EOQualifier.filteredArrayWithQualifier(toPersonne().toRepartPersonneAdresses(), qual);
			if (res.count() == 0) {
				throw new NSValidation.ValidationException(EXCEPTION_ADRESSE_FACT);
			}
		}
		else {
			throw new NSValidation.ValidationException(EXCEPTION_ADRESSE_FACT);
		}
	}

	/**
	 * Verifie si un compte est bien affecte au fournisseur (selon le parametre ANNUAIRE_FOU_COMPTE_AUTO) et si fou_valide=O. Ne verifie pas si le
	 * compte est valide car le compte peut être desactive pour une raison ou une autre. Une verification du même ordre est effectuee par
	 * {@link EOStructureForFournisseurSpec#onValidateObjectMetier(AfwkPersRecord)} et
	 * {@link EOIndividuForFournisseurSpec#onValidateObjectMetier(AfwkPersRecord)}
	 */
	private void checkCompte() {
		Boolean creerCompteAuto = Boolean.valueOf(EOGrhumParametres.OUI.equals(EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_ANNUAIRE_FOU_COMPTE_AUTO)));
		if (FOU_VALIDE_OUI.equals(fouValide()) && creerCompteAuto.booleanValue()) {
			if (toCompte() == null) {
				throw new NSValidation.ValidationException("Un compte de connexion devrait être affecté au fournisseur  (car le parametre ANNUAIRE_FOU_COMPTE_AUTO est à O).");
			}
			//			if (!EOCompte.CPT_VALIDE_OUI.equals(toCompte().cptValide())) {
			//				throw new NSValidation.ValidationException("Le compte de connexion affecté au fournisseur devrait être valide.");
			//			}
		}
	}

	public boolean toUtilisateurHasChanged() {
		return (changesFromCommittedSnapshot().containsKey(EOFournis.TO_UTILISATEUR_KEY));
	}

	/**
	 * Vérification des droits. On vérifie les droits uniquement si l'utilisateur a changé.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkDroits() throws NSValidation.ValidationException {
		if (toUtilisateur() == null) {
			throw new NSValidation.ValidationException("L'utilisateur est obligatoire.");
		}
		try {
			PersonneApplicationUser appUser = new PersonneApplicationUser(editingContext(), toUtilisateur());
			appUser.checkUtilisateurValide(AUtils.now());
			if (EOFournis.FOU_VALIDE_OUI.equals(fouValide()) || EOFournis.FOU_VALIDE_ANNULE.equals(fouValide())) {
				if (!appUser.hasDroitValidationEOFournis(this, AUtils.currentExercice())) {
					throw new NSValidation.ValidationException("L'utilisateur " + toUtilisateur().getNomAndPrenom() + " n'a pas le droit de valider un fournisseur ou de modifier un fournisseur valide.");
				}
			} else {
				if (!appUser.hasDroitCreationEOFournis(this, AUtils.currentExercice())) {
					throw new NSValidation.ValidationException("L'utilisateur " + toUtilisateur().getNomAndPrenom() + " n'a pas le droit de créer ni de modifier un fournisseur.");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (toUtilisateurHasChanged()) {
				throw new NSValidation.ValidationException(e.getMessage());
			}
		}

	}

	/**
	 * @return L'individu associe ou null.
	 */
	public EOIndividu toIndividu() {
		if (toIndividus() != null && toIndividus().count() > 0) {
			return (EOIndividu) toIndividus().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * @return L'individu associe ou null.
	 */
	public IIndividu toIIndividu() {
		return (IIndividu)this.toIndividu();
	}
	
	/**
	 * @return La structure associee ou null.
	 */
	public EOStructure toStructure() {
		if (toStructures() != null && toStructures().count() > 0) {
			return (EOStructure) toStructures().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * @return TRUE si le founisseur est base sur une structure.
	 */
	public boolean isStructure() {
		return toStructure() != null;
	}

	/**
	 * @return TRUE si le founisseur est base sur un individu.
	 */
	public boolean isIndividu() {
		return toIndividu() != null;
	}

	/**
	 * Appele lors de l'insertion dans l'editingContext. Initiualisation d'un nouvel objet :
	 * <ul>
	 * <li>Date de création</li>
	 * <li>fouDate</li>
	 * <li>fouMarche a O</li>
	 * <li>fouValide a N</li>
	 * <li>fouType a F</li>
	 * </ul>
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDCreation(AUtils.now());
		setFouDate(AUtils.now());
		setFouMarche(EOFournis.FOU_MARCHE_0);
		setFouValide(EOFournis.FOU_VALIDE_NON);
		setFouType(EOFournis.FOU_TYPE_FOURNISSEUR);
		setFouEtranger(NON);
	}

	/**
	 * Appele lors de la mise d'un objet par un cient (JavaClient). A tester...
	 */
	@Override
	public void awakeFromClientUpdate(EOEditingContext arg0) {
		super.awakeFromClientUpdate(arg0);
	}

	/**
	 * Appele une fois l'objet fetche depuis la base.
	 */
	@Override
	public void awakeFromFetch(EOEditingContext arg0) {
		super.awakeFromFetch(arg0);
	}

	/**
	 * @return Le nom (+ prenom) du fournisseur).
	 */
	public String getNomComplet() {
		if (isStructure()) {
			return toStructure().persLibelle();
		}
		else if (isIndividu()) {
			return toIndividu().nomUsuel() + " " + toIndividu().prenom();
		}
		else {
			return "";
		}
	}

	/**
	 * Verifie si l'utilisateur specifie en parametre a le droit de creer un founisseur.
	 * 
	 * @param appUser
	 * @throws Exception
	 */
	public void checkDroitCreation(PersonneApplicationUser appUser) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Verification droit creation fournisseur pour fournis " + this.getNomComplet());
			logger.debug("Verification droit creation fournisseur pour utilisateur " + appUser.getNomAndPrenom(), new RuntimeException("Dummy for Stacktrace"));
		}
		appUser.checkUtilisateurValide(AUtils.now());
		if (!appUser.hasDroitCreationEOFournis(this, AUtils.currentExercice())) {
			throw new Exception("L'utilisateur " + appUser.getNomAndPrenom() + " n'est pas autorise a creer des fournisseurs.");
		}
	}

	/**
	 * @param appUser
	 * @return true si l'utilisateur a le droit de valider les fournisseurs
	 */
	public boolean checkDroitValidation(PersonneApplicationUser appUser) {
		if (logger.isDebugEnabled()) {
			logger.debug("Verification droit validation fournisseur pour fournis " + this.getNomComplet());
			logger.debug("Verification droit validation fournisseur pour utilisateur " + appUser.getNomAndPrenom(), new RuntimeException("Dummy for Stacktrace"));
		}
		try {
			appUser.checkUtilisateurValide(AUtils.now());
		} catch (Exception e) {
			return false;
		}
		return appUser.hasDroitValidationEOFournis(this, AUtils.currentExercice());
	}

	/**
	 * Cree un code fournisseur a partir de son nom.</br> On prend les 3 premières lettres du nom et on ajoute un nombre sur 5 caracteres. Ex.
	 * ABA00001.</br> Si le nom fait moins de 3 caracteres, on complete avec des 0.
	 * 
	 * @return Le code du fournisseur.
	 */
	private String creerFouCode() {
		String nom = null;
		String prefix = null;
		nom = MyStringCtrl.compactString(MyStringCtrl.toBasicString(MyStringCtrl.chaineSansAccents(toPersonne().persLibelle()), null, ' '), -1, "").replaceAll(" ", "");
		nom = nom.trim();
		nom = MyStringCtrl.replaceCharactersNonAlphaNumeric(nom);
		prefix = nom.substring(0, (nom.length() > 3 ? 3 : nom.length())).toUpperCase();

		//Completer avec des zeros si le prefixe fait moins de 3 caracteres
		while (prefix.length() < 3) {
			prefix = prefix + "0";
		}

		int max = 0;
		//ROD 24/11/2011 Correction de la construction du code fournisseur dans le cas où certains codes existant sont sur moins de 8 caracteres.  
		String sql = "select max(to_number(substr(fou_code,4,8)))  as code from grhum.fournis_ulr where fou_code like '" + prefix + "%'";
		@SuppressWarnings("unchecked")
		NSArray<NSDictionary<String, ?>> res = EOUtilities.rawRowsForSQL(editingContext(), "FwkCktlPersonne", sql, new NSArray<String>("code"));
		if (res.count() > 0) {
			if (res.objectAtIndex(0).valueForKey("code") != NSKeyValueCoding.NullValue) {
				try {
					max = ((Number) FORMAT_FOU_CODE_NUM.parseObject(res.objectAtIndex(0).valueForKey("code").toString())).intValue();
				} catch (ParseException e) {
					if (logger.isDebugEnabled()) {
						logger.error("Erreur lors de la creation du code fournisseur " + this.getNomComplet(), e);
					}
					return null;
				}
			}
		}

		//		NSArray codes = EOFournis.fetchAll(this.editingContext(), EOQualifier.qualifierWithQualifierFormat(EOFournis.FOU_CODE_KEY + " like %@", new NSArray(new Object[] {
		//				prefix + "*"
		//		})), new NSArray(new Object[] {
		//				EOFournis.SORT_FOU_CODE_DESC
		//		}));
		//		if (codes.count() > 0) {
		//			try {
		//				max = ((Number) FORMAT_FOU_CODE_NUM.parseObject(((EOFournis) codes.objectAtIndex(0)).fouCode().substring(3))).intValue();
		//			} catch (ParseException e) {
		//				if (logger.isDebugEnabled()) {
		//					logger.error("Erreur lors de la creation du code fournisseur " + this.getNomComplet(), e);
		//				}
		//				return null;
		//			}
		//		}
		max++;
		prefix = prefix + FORMAT_FOU_CODE_NUM.format(new Integer(max));
		return prefix;
	}

	/**
	 * Ce qui est fait dans cette méthode : <br/>
	 * 
	 * @param ec
	 * @param appUser Utilisateur de l'application. Obligatoire. Dans le cas ou une appli doit creer des fournisseur sans que l'utilisateur soit
	 *            connu, specifier un utilisateur valide et connu de l'application, qui sera utilisé par defaut.
	 * @param individu
	 * @param structure
	 * @param adresse
	 * @param isEtranger
	 * @param fouType
	 * @return
	 * @throws Exception
	 */
	private EOFournis initialise(PersonneApplicationUser appUser, EOIndividu individu, EOStructure structure, EOAdresse adresse, boolean isEtranger, String fouType) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Initialisation d'un fournisseur :", new RuntimeException("Dummy for Stacktrace"));
			logger.debug("appUser = " + appUser.getNomAndPrenom());
			logger.debug("appUser = " + appUser.getNomAndPrenom());
			logger.debug("individu = " + (individu == null ? "null" : individu.getNomAndPrenom()));
			logger.debug("structure = " + (structure == null ? "null" : structure.llStructure()));
			logger.debug("adresse = " + adresse);
			logger.debug("isEtranger = " + "" + isEtranger);
			logger.debug("fouType = " + fouType);
			logger.debug("---------------------------------------------------");
			logger.debug("");
		}

		//verifier les droits
		checkDroitCreation(appUser);

		if (individu != null) {
			if (toIndividu() == null) {
				this.addToToIndividusRelationship(individu);
				this.setPersId(individu.persId());
			}
		} else if (structure != null) {
			if (toStructure() == null) {
				this.addToToStructuresRelationship(structure);
				this.setPersId(structure.persId());
			}
		}
		if (fouCode() == null) {
			this.setFouCode(creerFouCode());
		}

		this.setFouType(fouType);
		EOValideFournis valideFournis = null;
		if (this.toValideFournis() == null || NSKeyValueCoding.NullValue.equals(this.toValideFournis())) {
			valideFournis = EOValideFournis.creerInstance(this.editingContext());
			this.setToValideFournisRelationship(valideFournis);
		}
		if (this.toValideFournis().valDateCreate() == null) {
			this.toValideFournis().setValDateCreate(AUtils.now());
		}
		if (this.toValideFournis().valCreation() == null) {
			this.toValideFournis().setValCreation(appUser.getUtilisateur().utlOrdre());
		}

		this.toValideFournis().fixCreateur(appUser);

		if (FOU_VALIDE_OUI.equals(fouValide())) {
			validerFournis(appUser);
		}
		else if (FOU_VALIDE_ANNULE.equals(fouValide())) {
			archiverFournis(appUser);
		}
		else {
			encoursFournis(appUser);
		}

		//		if (verifierDroitValidationFournisseur(appUser)) {
		//			validerFournis(appUser);
		//		} else {
		//			encoursFournis();
		//		}

		//		traiterAdresse(ec, adresse);
		this.setToAdresseRelationship(adresse);
		this.setFouEtranger((isEtranger ? EOFournis.FOU_ETRANGER_OUI : EOFournis.FOU_ETRANGER_NON));
		this.setToUtilisateurRelationship(appUser.getUtilisateur());

		logger.debug("Fournisseur cree " + this);
		return this;
	}

	/**
	 * Initialise un objet EOFournis (on considere que la structure ou l'individu a deja ete affecte).
	 * <ul>
	 * <li>Relation avec individu ou structure</li>
	 * <li>Création d'un fouCode</li>
	 * <li>Initialisation d'un objet {@link EOValideFournis}</li>
	 * <li>Validation automatique du fournisseur si l'utilisateur a les droits</li>
	 * <li>Relation avec le RIB</li>
	 * <li>Relation avec l'adresse</li>
	 * <li>Relation avec l'utilisateur</li>
	 * </ul>
	 * 
	 * @param persIdForUser Le persId de l'utilisateur qui cree le fournisseur. (au moins un des deux parametres persIdForUser ou utlOrdreForUser doit
	 *            etre specifie. utlOrdreForUser est prioritaire)
	 * @param adresse L'adresse de facturation qui sera affectee au fournisseur
	 * @param isEtranger Indique s'il s'agit d'un fournisseur etranger (a des consequences sur le controle du rib).
	 * @param fouType Le type d'objet fournisseur (F: Fournisseur, C: client, T : fournisseur+client). Suivant le type les controles ne seront pas les
	 *            mêmes.
	 * @return l'objet en cours.
	 * @throws Exception
	 */
	public EOFournis initialise(Integer persIdForUser, EOAdresse adresse, boolean isEtranger, String fouType) throws Exception {
		if (persIdForUser == null) {
			throw new Exception("Le parametre persIdForUser est vide.");
		}
		PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, persIdForUser);
		return initialise(appUser, null, null, adresse, isEtranger, fouType);
	}

	/**
	 * Associe un compte au fournisseur. Si aucun compte n'est trouve, un nouveau compte externe est créé.
	 */
	public EOCompte associerCompte(PersonneApplicationUser appUser) throws Exception {
		IPersonne pers = (this.toIndividu() != null ? (IPersonne) this.toIndividu() : (IPersonne) this.toStructure());

		//Creer un compte
		//si compte valide existe deja, on le garde
		EOCompte compte = null;
		if (!NSKeyValueCoding.NullValue.equals(this.toCompte()) && this.toCompte() != null) {
			compte = this.toCompte();
		}
		else {
			//Verifier qu'un compte n'existe pas
			compte = EOCompte.compteForPersId(this.editingContext(), pers.persId());
			if (compte == null) {
				compte = EOCompte.creerInstance(this.editingContext());
				compte.initialiseCompte(this.editingContext(), appUser, pers, EOVlans.VLAN_X, null, null, true, null, null, null);
			}
			this.setToCompteRelationship(compte);
		}

		return compte;
	}

	/**
	 * Associe un compte au fournisseur. Si aucun compte n'est trouve, un nouveau est cree.
	 * 
	 * @param persIdForUser
	 * @return le compte.
	 * @throws Exception
	 */
	public EOCompte associerCompte(Integer persIdForUser) throws Exception {
		if (persIdForUser == null) {
			throw new Exception("persIdForUser est vide.");
		}
		PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, persIdForUser);
		return associerCompte(appUser);
	}

	public static EOQualifier qualForIndivividuEquals(EOIndividu individu) {
		return EOQualifier.qualifierWithQualifierFormat(EOFournis.TO_INDIVIDUS_KEY + "=%@", new NSArray(new Object[] {
				individu
		}));
	}

	/**
	 * @param ec
	 * @param individu
	 * @return Les fournisseurs valides ayant au moins un rib valide.
	 */
	public static final NSArray fournisseursValidesForIndividuWithRibValide(EOEditingContext ec, EOIndividu individu) {
		return fournisseursValidesForIndividuWithQual(ec, individu, QUAL_RIB_VALIDE);
	}

	/**
	 * Recherche des fournisseurs valides pour un individu. Vous avez la possibilité de spécifier un qualifier qui sera pris en compte dans la
	 * recherche avec un EOAndQualifier.
	 * 
	 * @param ec
	 * @param individu
	 * @param qualOptionnel
	 * @return les fournisseurs
	 */
	public static final NSArray fournisseursValidesForIndividuWithQual(EOEditingContext ec, EOIndividu individu, EOQualifier qualOptionnel) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(QUAL_FOU_VALIDE_OUI);
		quals.addObject(qualForIndivividuEquals(individu));
		quals.addObject(qualOptionnel);

		EOQualifier qual = new EOAndQualifier(quals);
		return AFinder.fetchArray(ec, EOFournis.ENTITY_NAME, qual, null, true, true, false, null);
	}

	/**
	 * Valide le fournisseur si l'utilisateur a les droits (fou_valide, insertion dans le bon groupe. S'il est dans le groupe encours, la repartition
	 * est supprimee). Si l'utilisateur n'a pas les droits, une exception est declenchee.<br/>
	 * <ul>
	 * <li>Si type = Fournisseur ou tiers, insertion dans le groupe defini par le parametre ANNUAIRE_FOU_VALIDE_MORALE ou ANNUAIRE_FOU_VALIDE_PHYSIQUE
	 * (selon le cas)</li>
	 * <li>Si type = Client ou tiers, insertion dans le groupe defini par le parametre ANNUAIRE_FOU_VALIDE_EXTERNE</li>
	 * </ul>
	 * 
	 * @param appUser L'utilisateur en cours.
	 * @throws Exception
	 */
	public void validerFournis(PersonneApplicationUser appUser) throws Exception {
		toValideFournis().fixValidateur(appUser);
		if (checkDroitValidation(appUser)) {
			this.toValideFournis().setValValidation(appUser.getUtilisateur().utlOrdre());
			nettoyerRepartStructureForFournisseurEncours();
			nettoyerRepartStructureForFournisseurAnnules();
			//			nettoyerTousRepartStructureFournisseur();
			if (isTypeFournisseur() || isTypeTiers()) {
				if (isStructure()) {
					EOStructure structureGroupe = EOStructureForGroupeSpec.getGroupeFournisseurValideMorale(editingContext());
					EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toStructure(), structureGroupe, appUser.getPersonne().persId());
				}
				else {
					EOStructure structureGroupe = EOStructureForGroupeSpec.getGroupeFournisseurValidePhysique(editingContext());
					EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toIndividu(), structureGroupe, appUser.getPersonne().persId());
				}
			}
			if (isTypeClient() || isTypeTiers()) {
				EOStructure structureGroupe = EOStructureForGroupeSpec.getGroupeFournisseurValideExterne(editingContext());
				EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toPersonne(), structureGroupe, appUser.getPersonne().persId());
			}

			super.setFouValide(EOFournis.FOU_VALIDE_OUI);
			this.toValideFournis().setValDateVal(AUtils.getDateJour());

		}
	}

	/**
	 * Archive le fournisseur et le met dans le groupe des fournisseur annules.
	 * 
	 * @param appUser
	 * @throws Exception
	 */
	public void archiverFournis(PersonneApplicationUser appUser) throws Exception {
		toValideFournis().fixValidateur(appUser);
		if (checkDroitValidation(appUser)) {
			this.toValideFournis().setValValidation(appUser.getUtilisateur().utlOrdre());
			//			nettoyerTousRepartStructureFournisseur();
			nettoyerRepartStructureForFournisseurEncours();
			nettoyerRepartStructureForFournisseurValides();
			//			EORepartStructure.creerInstanceSiNecessaire(editingContext(), toPersonne(), EOStructureForGroupeSpec.getGroupeFournisseurAnnules(editingContext()));
			EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toPersonne(), EOStructureForGroupeSpec.getGroupeFournisseurAnnules(editingContext()), appUser.getPersId());
			super.setFouValide(EOFournis.FOU_VALIDE_ANNULE);
			this.toValideFournis().setValDateVal(AUtils.getDateJour());

		}
	}

	/**
	 * change l'etat du fournisseur a "en cours de validation" (fou_valide, insertion dans le bon groupe).
	 * 
	 * @param appUser
	 * @throws Exception
	 */
	public void encoursFournis(PersonneApplicationUser appUser) throws Exception {
		nettoyerRepartStructureForFournisseurAnnules();
		nettoyerRepartStructureForFournisseurValides();
		//		nettoyerTousRepartStructureFournisseur();
		EOStructure groupe = EOStructureForGroupeSpec.getGroupeFournisseurEnCours(this.editingContext());

		if (isStructure()) {
			//si la structure est deja affectee au groupe on ne l'affecte pas une nouvelle fois
			//EORepartStructure.creerInstanceSiNecessaire(editingContext(), toStructure(), groupe, appUser.getPersonne().persId());
			EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toStructure(), groupe, appUser.getPersonne().persId());
		}
		else {
			//			EORepartStructure.creerInstanceSiNecessaire(editingContext(), toIndividu(), groupe, appUser.getPersonne().persId());
			EOStructureForGroupeSpec.sharedInstance().affecterPersonneDansGroupeForce(editingContext(), toIndividu(), groupe, appUser.getPersonne().persId());

		}
		super.setFouValide(EOFournis.FOU_VALIDE_NON);
	}

	/**
	 * Supprime l'affectation du fournisseur a un groupe specifie.
	 * 
	 * @param groupe
	 */
	private void nettoyerRepartStructureForFournisseur(EOStructure groupe) {
		//EORepartStructure.supprimerRepartPourPersonneEtGroupe(this.editingContext(), this.toPersonne(), groupe);
		EORepartStructure.supprimerForce(this.editingContext(), this.toPersonne(), groupe);

		//this.toPersonne().supprimerAffectationAUnGroupe(this.editingContext(), this.toPersonne().persIdModification(), groupe);
	}

	/**
	 * Supprimer l'appartenance du fournisseur aux groupes des fournisseurs valides.
	 * 
	 * @throws Exception
	 */
	public void nettoyerRepartStructureForFournisseurValides() throws Exception {
		EOStructure groupe = EOStructureForGroupeSpec.getGroupeFournisseurValideMorale(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);

		groupe = EOStructureForGroupeSpec.getGroupeFournisseurValidePhysique(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);

		groupe = EOStructureForGroupeSpec.getGroupeFournisseurValideExterne(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);

		groupe = EOStructureForGroupeSpec.getGroupeFournisseurValideInterne(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);
	}

	//	/**
	//	 * Supprime les appartenances aux groupes de type fournisseur pour le fournisseur.
	//	 * 
	//	 * @throws Exception
	//	 */
	//	public void nettoyerTousRepartStructureFournisseur() throws Exception {
	//		//Recuperer les groupes de type FO
	//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY + "=%s", new NSArray(EOTypeGroupe.TGRP_CODE_FO));
	//		NSArray groupes = EOStructureForGroupeSpec.rechercherGroupes(editingContext(), qual, 2000);
	//		LRLogger.debug("Groupes fournisseurs");
	//		LRLogger.debug(groupes);
	//
	//		Enumeration enumeration = groupes.objectEnumerator();
	//		while (enumeration.hasMoreElements()) {
	//			EOStructure groupe = (EOStructure) enumeration.nextElement();
	//			this.toPersonne().supprimerAffectationAUnGroupe(this.editingContext(), this.toPersonne().persIdModification(), groupe);
	//			//EORepartStructure.supprimerRepartPourPersonneEtGroupe(this.editingContext(), this.toPersonne(), groupe);
	//		}
	//	}

	/**
	 * Supprimer l'appartenance du fournisseur au groupe des fournisseurs en cours de validation.
	 * 
	 * @throws Exception
	 */
	public void nettoyerRepartStructureForFournisseurEncours() throws Exception {
		EOStructure groupe = EOStructureForGroupeSpec.getGroupeFournisseurEnCours(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);
	}

	/**
	 * Supprimer l'appartenance du fournisseur au groupe des fournisseurs annules.
	 * 
	 * @throws Exception
	 */
	public void nettoyerRepartStructureForFournisseurAnnules() throws Exception {
		EOStructure groupe = EOStructureForGroupeSpec.getGroupeFournisseurAnnules(this.editingContext());
		nettoyerRepartStructureForFournisseur(groupe);
	}

	public NSArray<IRib> toRibsValide() {
		NSArray<IRib> res = new NSArray<IRib>(EOQualifier.filteredArrayWithQualifier(toRibs(), EORib.QUAL_RIB_VALIDE));
		return res;
	}

	public NSArray toRibsNonAnnules() {
		return EOQualifier.filteredArrayWithQualifier(toRibs(), new EONotQualifier(EORib.QUAL_RIB_ANNULE));
	}

	public Boolean isEtranger() {
		return Boolean.valueOf(FOU_ETRANGER_OUI.equals(fouEtranger()));
	}

	/**
	 * Si true, le n° SIRET est vid&eacute;
	 * 
	 * @param value
	 */
	public void setIsEtranger(Boolean value) {
		if (value.booleanValue()) {
			setFouEtranger(FOU_ETRANGER_OUI);
			if (toStructure() != null) {
				toStructure().setSiren(null);
				toStructure().setSiret(null);
			}
		}
		else {
			setFouEtranger(FOU_ETRANGER_NON);
		}
	}

	/**
	 * @return toStructure() ou bien toIndiviodu() selon le cas.
	 */
	public IPersonne toPersonne() {
		return (toStructure() != null ? (IPersonne) toStructure() : (IPersonne) toIndividu());
	}

	public boolean isTypeFournisseur() {
		return (FOU_TYPE_FOURNISSEUR.equals(fouType()));
	}

	public boolean isTypeTiers() {
		return (FOU_TYPE_TIERS.equals(fouType()));
	}

	public boolean isTypeClient() {
		return (FOU_TYPE_CLIENT.equals(fouType()));
	}

	public boolean isFouValideEncours() {
		return EOFournis.FOU_VALIDE_NON.equals(fouValide());
	}

	public boolean isFouValideValide() {
		return EOFournis.FOU_VALIDE_OUI.equals(fouValide());
	}

	public boolean isFouValideAnnule() {
		return EOFournis.FOU_VALIDE_ANNULE.equals(fouValide());
	}

	/**
	 * Cette méthode doit être complétée selon le cas par l'appel aux methodes suivantes (avant le saveChanges)
	 * <ul>
	 * <li>{@link EOFournis#encoursFournis(PersonneApplicationUser)},</li>
	 * <li> {@link EOFournis#validerFournis(PersonneApplicationUser)} et</li>
	 * <li>{@link EOFournis#archiverFournis(PersonneApplicationUser)}</li>
	 * </ul>
	 * à la place.
	 */
	@Override
	public void setFouValide(String value) {
		super.setFouValide(value);
	}

	//	/**
	//	 * @return Un message email pret a etre envoye informant les destinataires de l'adresse indiquee dans le parametre de JefyAdmin
	//	 *         EMAIL_VALIDFOURNIS, demandant la validation du fournisseur, emis par l'utilisateur en cours.
	//	 */
	//	public MyCktlMailMessage buildDemandeValidationMail() throws Exception {
	//		//Recuperer l'adresse des destinataires
	//		String exer = new SimpleDateFormat("yyyy").format(new Date());
	//		EOExercice exercice = EOExercice.getExercice(editingContext(), Integer.valueOf(exer));
	//		if (exercice == null) {
	//			throw new NullPointerException("Aucun exercice defini pour " + exer);
	//		}
	//		EOJefyAdminParametre param = EOJefyAdminParametre.getParametre(editingContext(), EMAIL_VALIDFOURNIS_PARAMKEY, exercice);
	//		if (param == null) {
	//			throw new NullPointerException("Parametre JEFY_ADMIN " + EMAIL_VALIDFOURNIS_PARAMKEY + " non defini pour " + exer);
	//		}
	//
	//		String hostmail = EOGrhumParametres.parametrePourCle(editingContext(), GRHUM_HOST_MAIL_PARAMKEY);
	//		if (hostmail == null) {
	//			throw new NullPointerException("Parametre GRHUM " + GRHUM_HOST_MAIL_PARAMKEY + " non defini.");
	//		}
	//
	//		String to = param.parValue();
	//		String from = this.toValideFournis().getCreateurAsUtilisateur().getEmail();
	//		String reply = from;
	//		String cc = from;
	//		String subject = "Demande de validation du fournisseur " + this.getNomComplet();
	//		String body = this.toValideFournis().getCreateurAsUtilisateur().getPrenomAndNom() + " demande la validation du fournisseur suivant\n" + this.getNomComplet() + " (code : " + fouCode() + ").";
	//
	//		MyCktlMailMessage tmp = new MyCktlMailMessage(hostmail);
	//		tmp.initMessage(from, to, subject, body);
	//		tmp.addCC(cc);
	//		tmp.setReplyTo(reply);
	//		return tmp;
	//
	//	}
	//
	//	/**
	//	 * @return Un message email pret a etre envoye informant le createur du fournisseur que celui-ci a ete valide ou annule.
	//	 * @throws Exception
	//	 */
	//	public MyCktlMailMessage buildInformationValidation() throws Exception {
	//		//Recuperer l'adresse des destinataires
	//		String exer = new SimpleDateFormat("yyyy").format(new Date());
	//		EOExercice exercice = EOExercice.getExercice(editingContext(), Integer.valueOf(exer));
	//		if (exercice == null) {
	//			throw new NullPointerException("Aucun exercice defini pour " + exer);
	//		}
	//		EOJefyAdminParametre param = EOJefyAdminParametre.getParametre(editingContext(), EMAIL_VALIDFOURNIS_PARAMKEY, exercice);
	//		if (param == null) {
	//			throw new NullPointerException("Parametre JEFY_ADMIN " + EMAIL_VALIDFOURNIS_PARAMKEY + " non defini pour " + exer);
	//		}
	//
	//		String hostmail = EOGrhumParametres.parametrePourCle(editingContext(), GRHUM_HOST_MAIL_PARAMKEY);
	//		if (hostmail == null) {
	//			throw new NullPointerException("Parametre GRHUM " + GRHUM_HOST_MAIL_PARAMKEY + " non defini.");
	//		}
	//
	//		String to = this.toValideFournis().getCreateurAsUtilisateur().getEmail();
	//		String from = this.toValideFournis().getValideurAsUtilisateur().getEmail();
	//		String reply = from;
	//		//String cc = from;
	//		String subject = null;
	//		String body = null;
	//		if (EOFournis.FOU_VALIDE_OUI.equals(fouValide())) {
	//			subject = "Fournisseur validé : " + this.getNomComplet();
	//			body = this.toValideFournis().getValideurAsUtilisateur().getPrenomAndNom() + " a validé le fournisseur suivant : \n" + this.getNomComplet() + " (code : " + fouCode() + ").";
	//		}
	//		else if (EOFournis.FOU_VALIDE_ANNULE.equals(fouValide())) {
	//			subject = "Fournisseur annulé : " + this.getNomComplet();
	//			body = this.toValideFournis().getValideurAsUtilisateur().getPrenomAndNom() + " a annulé le fournisseur suivant : \n" + this.getNomComplet() + " (code : " + fouCode() + ").";
	//		}
	//		if (subject != null) {
	//			MyCktlMailMessage tmp = new MyCktlMailMessage(hostmail);
	//			tmp.initMessage(from, to, subject, body);
	//			tmp.addCC(param.parValue());
	//			tmp.setReplyTo(reply);
	//			return tmp;
	//		}
	//		return null;
	//	}

	/**
	 * @return Indique si le fournisseur a des ribs valides affectés.
	 */
	public boolean hasRibsValides() {
		return (toRibs(EORib.QUAL_RIB_VALIDE).count() > 0);
	}

	/**
	 * @return nom et code fournisseur
	 */
	public String libelleEtId() {
		return getNomComplet().concat(" (").concat(fouCode()).concat(")");
	}
	
	/** 
	 * {@inheritDoc}
	 */
	public IRib toDernierRib() {
	    return EORib.D_CREATION.asc().sorted(toRibsValide()).lastObject();
	}
	
	/** 
	 * {@inheritDoc}
	 */
	public void addToToRibsRelationship(IRib rib) {
		super.addToToRibsRelationship((EORib) rib);
	}

	/** 
	 * {@inheritDoc}
	 */
	public IRib rechercherRib(String iban) {
		return toRibs(EORib.IBAN.eq(iban)).lastObject();
	}

	/** 
	 * {@inheritDoc}
	 */
	public void setToValideFournisRelationship(IValideFournis valideFournis) {
		super.setToValideFournisRelationship((EOValideFournis) valideFournis);
	}

	/** 
	 * {@inheritDoc}
	 */
	public void ajouterFouType(String fouType) {
		if (EOFournis.FOU_TYPE_CLIENT.equals(fouType) && isTypeFournisseur()) {
			setFouType(EOFournis.FOU_TYPE_TIERS);
		} else if (EOFournis.FOU_TYPE_FOURNISSEUR.equals(fouType) && isTypeClient()) {
			setFouType(EOFournis.FOU_TYPE_TIERS);
		} else if (fouType != null) {
			setFouType(fouType);
		}
	}
	
}
