/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOAdresse.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Gestion des objets Adresse.
 * <ul>
 * <li>Règles d'initialisation : {@link EOAdresse#awakeFromInsertion(EOEditingContext) }</li>
 * <li>Règles de validation : {@link EOAdresse#validateObjectMetier()}</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EOAdresse extends _EOAdresse implements IAdresse {
	public static final String CP_OBLIGATOIRE_DEFAUT = EOGrhumParametres.OUI;

	/** Nom de la séquence Orcale qui permet d'obtenir la clé primaire. */
	private static final String SEQ_ADRESSE_ENTITY_NAME = "Fwkpers_SeqAdresse";

	private static String CP_OBLIGATOIRE = null;

	private String cpCache;
	private String lPaysCache;
	private String adrAdresse3;
	private String adrAdresse4;
	

	public EOAdresse() {
		super();

	}

	/**
	 * Peut etre appele à partir des factories.<br/>
	 * Modifications :
	 * <ul>
	 * <li>Date de modification</li>
	 * </ul>
	 * Contrôles :
	 * <ul>
	 * <li>Code postal obligatoire pour pays par defaut si parametre GRHUM_CP_OBLIGATOIRE est a OUI.</li>
	 * <li>CP etranger non obligatoire.</li>
	 * <li>Si France, Le code postal doit exister dans la table commune. Le controle de cohérence code postal/ville est abandonné le 07/10/2010</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		trimAllString();
		setDModification(MyDateCtrl.now());

		try {
			checkContraintesObligatoires();
			checkContraintesLongueursMax();

			//Verifier le droit de modifier les adresses de type Par/etudiant
			checkUsers();

			//Si on est sur une adresse de Parent ou d'etudiant, on autorise les adresses "vides".
			//on fait sauter cette exception suite discussion pat. et pascal 11/02/10
			//if (!(isAdresseEtudiant() || isAdresseParent())) {

			//Synchroniser le pays et le cache
			if (toPays() == null) {
				if (getLPaysCache() != null) {
					NSArray res = EOPays.fetchPaysByCodeOrLibelle(editingContext(), getLPaysCache());
					if (res.count() == 1) {
						super.setToPaysRelationship((EOPays) res.objectAtIndex(0));
					}
				}
			}

			if (toPays() == null) {
				throw new NSValidation.ValidationException("Le pays est obligatoire.");
			}

			parseCPCache();
			//			if (isCodePostalObligatoire(editingContext())) {
			//				if (toPays().isPaysDefaut() && (codePostal() == null || codePostal().trim().length() == 0)) {
			//					throw new NSValidation.ValidationException("Le code postal est obligatoire : ");
			//				}
			//				if (!toPays().isPaysDefaut() && (cpEtranger() == null || cpEtranger().trim().length() == 0)) {
			//					throw new NSValidation.ValidationException("Le code postal étranger est obligatoire.");
			//				}
			//			}

			
			if (MyStringCtrl.isEmpty(adrAdresse1())) {
				throw new NSValidation.ValidationException("La première ligne de l'adresse est obligatoire.");
			}

			if ((ville() == null || ville().trim().length() == 0)) {
				throw new NSValidation.ValidationException("La ville est obligatoire.");
			}

			if (isCodePostalObligatoire(editingContext())) {
				if (toPays().isPaysDefaut() && (codePostal() == null || codePostal().trim().length() == 0)) {
					throw new NSValidation.ValidationException("Le code postal est obligatoire pour : " + toPays().lcPays());
				}
			}

			//Si on est en France, verifier si le code postal est coherent avec la ville.
			if (EOPays.CODE_PAYS_FRANCE.equals(toPays().cPays())) {
				if (codePostal() != null) {
					EOQualifier qual = new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorEqual, codePostal());
					NSArray res = EOCommune.fetchAll(this.editingContext(), qual, null);
					if (res.count() == 0) {
						throw new NSValidation.ValidationException("Le code postal (" + codePostal() + ") est inconnu.");
					}
					//07/10/2010 : on enleve ce controle
					//					boolean found = false;
					//					for (int i = 0; i < res.count() && !found; i++) {
					//						EOCommune comm = (EOCommune) res.objectAtIndex(i);
					//						if (ville().startsWith(comm.llCom())) {
					//							found = true;
					//						}
					//					}
					//					if (!found) {
					//						throw new NSValidation.ValidationException("La ville (" + ville() + ") ne correspond pas au code postal (" + codePostal() + ") dans la liste des communes.");
					//					}
				}
			}
			super.validateObjectMetier();
		} catch (Exception e) {
			NSValidation.ValidationException e1 = new ValidationException("Erreur lors de la validation de l'adresse '" + toDisplayString() + "' : " + e.getMessage());
			e1.initCause(e);
			throw e1;
		}
	}

	/**
	 * Crée une instance de EOAdresse et l'insere dans l'editingContext.
	 * 
	 * @param ed
	 * @return
	 * @throws Exception
	 */
	public static EOAdresse creerInstance(EOEditingContext ed) {
		EOAdresse object = (EOAdresse) AUtils.instanceForEntity(ed, EOAdresse.ENTITY_NAME);
		ed.insertObject(object);
		return object;
	}

	/**
	 * @return Un nouvel adrOrdre.
	 */
	public static Integer construireAdrOrdre(EOEditingContext ec) {
		return AFinder.clePrimairePour(ec, EOAdresse.ENTITY_NAME, EOAdresse.ADR_ORDRE_KEY, SEQ_ADRESSE_ENTITY_NAME, true);
	}

	/**
	 * Initialise les valeurs par defaut:
	 * <ul>
	 * <li>Nouvel adrOrdre</li>
	 * <li>Date de creation</li>
	 * <li>tempPayeUtil a N</li>
	 * <li>adrListeRouge a N</li>
	 * </ul>
	 */
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setAdrOrdre(construireAdrOrdre(arg0));
		setDCreation(MyDateCtrl.now());
		setTemPayeUtil(NON);
		setAdrListeRouge(NON);
	}

	/**
	 * Affecte codePostal ou cpEtranger suivant le pays.
	 * 
	 * @param codePostal
	 */
	public void setCPCache(String codePostal) {
		cpCache = codePostal;
		if (codePostal == null) {
			setCodePostal(null);
			setCpEtranger(null);
		}

		parseCPCache();
	}

	/**
	 * @return codePostal ou cpEtranger suivant celui saisi.
	 */
	public String getCPCache() {
		if (cpCache == null) {
			cpCache = codePostal();
		}
		if (cpCache == null) {
			cpCache = cpEtranger();
		}
		return cpCache;
	}

	public void setLPaysCache(String lPays) {
		lPaysCache = lPays;
		if (lPaysCache != null) {
			setToPaysRelationship(null);
		}

		//parseCPCache();
	}

	public String getLPaysCache() {
		if (lPaysCache == null && toPays() != null) {
			lPaysCache = toPays().llPays();
		}
		return lPaysCache;
	}

	/**
	 * Analyse CPCache() et rempli le champ CodePostal ou CPEtranger suivant le pays.
	 */
	protected void parseCPCache() {
		//Si pays vide ou pays defaut, on rempli codePostal, sinon on rempli cpEtranger.
		if (isNotEtranger().booleanValue()) {
			setCodePostal(getCPCache());
			setCpEtranger(null);
		}
		else {
			setCodePostal(null);
			setCpEtranger(getCPCache());
		}
	}

	public void setToPaysRelationship(EOPays value) {
		super.setToPaysRelationship(value);
		if (value != null) {
			//			setLPaysCache(value.llPays());
			lPaysCache = value.llPays();
		}
		if (value == null) {
			lPaysCache = null;
		}
		parseCPCache();
	}

	public Boolean isEtranger() {
		return Boolean.valueOf(!(toPays() == null || EOPays.getPaysDefaut(this.editingContext()).cPays().equals(toPays().cPays())));
	}

	public Boolean isNotEtranger() {
		return Boolean.valueOf(!isEtranger().booleanValue());
	}

	//	/**
	//	 * @return true si l'adresse est une adresse de type ETUD (via repartPersonneAdresse). Il faut que l'objet soit déjà associé à un objet
	//	 *         {@link EORepartPersonneAdresse}.
	//	 */
	//	public boolean isAdresseEtudiant() {
	//
	//		EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, this);
	//		NSArray res = EORepartPersonneAdresse.fetchAll(editingContext(), qual, null);
	//
	//		Enumeration enumeration = res.objectEnumerator();
	//		while (enumeration.hasMoreElements()) {
	//			EORepartPersonneAdresse object = (EORepartPersonneAdresse) enumeration.nextElement();
	//			if (EOTypeAdresse.TADR_CODE_ETUD.equals(object.tadrCode())) {
	//				LRLogger.debug("Adresse ETUD detectee");
	//				return true;
	//			}
	//		}
	//		return false;
	//	}
	//
	//	/**
	//	 * @return true si l'adresse est une adresse de type PAR (via repartPersonneAdresse). Il faut que l'objet soit déjà associé à un objet
	//	 *         {@link EORepartPersonneAdresse}.
	//	 */
	//	public boolean isAdresseParent() {
	//		EOQualifier qual = new EOKeyValueQualifier(EORepartPersonneAdresse.TO_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, this);
	//		NSArray res = EORepartPersonneAdresse.fetchAll(editingContext(), qual, null);
	//
	//		Enumeration enumeration = res.objectEnumerator();
	//		while (enumeration.hasMoreElements()) {
	//			EORepartPersonneAdresse object = (EORepartPersonneAdresse) enumeration.nextElement();
	//			if (EOTypeAdresse.TADR_CODE_PAR.equals(object.tadrCode())) {
	//				LRLogger.debug("Adresse PAR detectee");
	//				return true;
	//			}
	//		}
	//		return false;
	//	}

	/**
	 * Utilisez plutot {@link EOAdresse#getCPCache()}.
	 */
	@Override
	public String codePostal() {
		return super.codePostal();
	}

	/**
	 * Utilisez plutot {@link EOAdresse#getCPCache()}.
	 */
	@Override
	public String cpEtranger() {
		return super.cpEtranger();
	}

	/**
	 * Utilisez plutot {@link EOAdresse#setCPCache(String)}.
	 */
	@Override
	public void setCodePostal(String value) {
		super.setCodePostal(value);
	}

	/**
	 * Utilisez plutot {@link EOAdresse#setCPCache(String)}.
	 */
	@Override
	public void setCpEtranger(String value) {
		super.setCpEtranger(value);
	}

	/**
	 * Convertie en Majuscules sans accents.
	 */
	public void setVille(String value) {
		if (value != null) {
			value = MyStringCtrl.chaineSansAccents(value).toUpperCase();
		}
		super.setVille(value);
	}

	/**
	 * Verifie la validite de l'URL specifiee pour le site web.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkAdrUrlPere() throws NSValidation.ValidationException {
		if (MyStringCtrl.isEmpty(adrUrlPere())) {
			if (!MyStringCtrl.isUrlValid(adrUrlPere())) {
				throw new NSValidation.ValidationException("L'URL specifiee (" + adrUrlPere() + ") n'est pas correcte.");
			}
		}
	}

	/**
	 * @param ec
	 * @return true si le code postal est obligatoire. Si GRHUM_CP_OBLIGATOIRE existe dans les params de GRHUM ont renvoie la valeur sinon on renvoie
	 *         la valeur de CP_OBLIGATOIRE_DEFAUT.
	 */
	public static boolean isCodePostalObligatoire(EOEditingContext ec) {
		if (CP_OBLIGATOIRE == null) {
			CP_OBLIGATOIRE = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CP_OBLIGATOIRE);
		}
		if (CP_OBLIGATOIRE == null) {
			CP_OBLIGATOIRE = CP_OBLIGATOIRE_DEFAUT;
		}
		return EOGrhumParametres.OUI.equals(CP_OBLIGATOIRE);
	}

	/**
	 * Verifie si les utilisateurs affectes a persIdCreation et persIdModification ont bien le droits de creer/Modifier la personne.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkUsers() throws NSValidation.ValidationException {
		if (persIdModification() != null && persIdCreation() == null) {
			setPersIdCreation(persIdModification());
		}

		if (getCreateur() == null) {
			throw new NSValidation.ValidationException("EOAdresse : La reference au createur (persIdCreation) est obligatoire.");
		}
		if (getModificateur() == null) {
			throw new NSValidation.ValidationException("EOAdresse : La référence au modificateur (persIdModification) est obligatoire.");
		}
		PersonneApplicationUser appUser = null;

		if (hasTemporaryGlobalID()) {
			appUser = getCreateur();
			if (!getCreateur().hasDroitCreationEOAdresse(this)) {
				throw new NSValidation.ValidationException("Vous n'avez pas le droit de creer cette adresse.");
			}
		}
		else {
			appUser = getModificateur();
			if (!getModificateur().hasDroitModificationEOAdresse(this)) {
				throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier cette adresse.");
			}
		}

		// PYM : La méthode de validation utilisée sur les droits sur les types d'adresse est la même que celle utilisée pour les droits de création/modif d'une adresse
		// le traitement ci-dessous est donc superflu, jusqu'au jour où l'on aura le moyen de faire les vérifications de droit sur les types directement.
//		NSArray<EOTypeAdresse> types = getEOTypesAdresseAffectes();
//		for (int i = 0; i < types.count(); i++) {
//			EOTypeAdresse typeAdresse = (EOTypeAdresse) types.objectAtIndex(i);
//
//			if (!appUser.hasDroitModificationEOAdresseDeType(typeAdresse.tadrCode(), null)) {
//				throw new NSValidation.ValidationException("Vous n'avez pas le droit de creer/modifier des adresses de type '" + typeAdresse.tadrLibelle() + "'.");
//			}
//
//		}

	}

	/**
	 * @return Les EOTypeAdresse affectés à l'adresse (en passant par repartPersonneAdresse).
	 */
	public NSArray<EOTypeAdresse> getEOTypesAdresseAffectes() {
		NSArray<EORepartPersonneAdresse> reparts = toRepartPersonneAdresses();
		NSMutableArray<EOTypeAdresse> types = new NSMutableArray<EOTypeAdresse>();
		for (int i = 0; i < reparts.count(); i++) {
			if (types.indexOfObject(((EORepartPersonneAdresse) reparts.objectAtIndex(i)).toTypeAdresse()) == NSArray.NotFound) {
				types.addObject(((EORepartPersonneAdresse) reparts.objectAtIndex(i)).toTypeAdresse());
			}
		}
		return types.immutableClone();
	}

	public String toDisplayString() {
		return MyStringCtrl.cut(MyStringCtrl.ifEmpty(adrAdresse1(), ""), 15).concat(" - ").concat(MyStringCtrl.ifEmpty(getCPCache(), "").concat(" - ").concat(MyStringCtrl.cut(MyStringCtrl.ifEmpty(ville(), ""), 15)));
	}
	
	@Override
	public String adrAdresse2() {
		 String adr2 = super.adrAdresse2();
		 if (!MyStringCtrl.isEmpty(adr2) && adr2.length() > 100) {
			 adr2 = super.adrAdresse2().substring(0, 99);
			 if (super.adrAdresse2().length() <= 200) {
				 adrAdresse3 = super.adrAdresse2().substring(100);
				 adrAdresse4 = "";
			 } else {
				 adrAdresse3 = super.adrAdresse2().substring(100, 199);
				 adrAdresse4 = super.adrAdresse2().substring(200);
			 }
		 }else {
			 adrAdresse3 = "";
			 adrAdresse4 = "";
		  }
		 
		 return adr2;
	 } 

	 /**
	* Setter du champs adresse2 d'une Adresse
	* On ne fait les compléments que si la partie 3 et 4 respectivement sont non vides
	* S'il n'existe pas de champs 3 et/ou 4, l'adresse2 est renseignée avec la String passée
	* telle qu'elle est.
	*/	
	public void setAdrAdresse2(String value) {
		 if (!MyStringCtrl.isEmpty(value)) {
			 
			 StringBuffer adr = new StringBuffer();
			
			 if (MyStringCtrl.isEmpty(adrAdresse3)) {
				 adr.append(value);
			 } else {
				 adr.append(MyStringCtrl.extendWithChars(value, " ", 100, false));
				 if (MyStringCtrl.isEmpty(adrAdresse4)) {
					adr.append(adrAdresse3);
				} else {
					adr.append(MyStringCtrl.extendWithChars(adrAdresse3," ", 100, false));
					adr.append(adrAdresse4);
				}
			 }
			 
//			adr = new StringBuffer(MyStringCtrl.extendWithChars(value, " ", 100, false));
//			if (!MyStringCtrl.isEmpty(adrAdresse3)){
//				adr = new StringBuffer(MyStringCtrl.extendWithChars(value, " ", 100, false));
//				adr.append(MyStringCtrl.extendWithChars(adrAdresse3," ", 100, false));
//			
//				if (!MyStringCtrl.isEmpty(adrAdresse4)){
//					adr.append(adrAdresse4);
//				}
//			}
			takeStoredValueForKey(adr.toString(), ADR_ADRESSE2_KEY);
		} else {
			takeStoredValueForKey(null, ADR_ADRESSE2_KEY);
		}
	 }
	
	
	public String adrAdresse2ONP() {
		 String adr2 = super.adrAdresse2();
		 
		 return adr2;
	 } 
	
	public void setAdrAdresse2ONP(String value) {
		takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
	}
	
	
	/**
	 * @return the adresse3
	 */
	public String adrAdresse3() {
		return adrAdresse3;
	}

	/**
	 * @param adresse3 the adresse3 to set
	 */
	public void setAdrAdresse3(String adresse3) {
		if (adresse3 == null) {
			this.adrAdresse3 = "";
		} else {
			this.adrAdresse3 = adresse3;
			if (super.adrAdresse2() != null) {
				StringBuffer adr = new StringBuffer(MyStringCtrl.stringCompletion(super.adrAdresse2(), 100," ", "D"));
				adr.append(MyStringCtrl.extendWithChars(adresse3," ", 100, false));
				takeStoredValueForKey(adr.toString(), ADR_ADRESSE2_KEY);
			}	
		}
	}

	/**
	 * @return the adresse4
	 */
	public String adrAdresse4() {
		return adrAdresse4;
	}

	/**
	 * @param adresse4 the adresse4 to set
	 */
	public void setAdrAdresse4(String adresse4) {
		if (adresse4 == null){
			this.adrAdresse4 = "";
		} else {
			this.adrAdresse4 = adresse4;
			if (super.adrAdresse2() != null) {
				StringBuffer adr = new StringBuffer(MyStringCtrl.stringCompletion(super.adrAdresse2(), 200," ", "D"));
				adr.append(adresse4);
				takeStoredValueForKey(adr.toString(), ADR_ADRESSE2_KEY);
			}
		}
	}
	 
	public static void checkAdresseGroupeParent(EOAdresse adresseATester) throws NSValidation.ValidationException {
		String message = " L'adresse du groupe parent n'est pas correctement renseignée. Veuillez la corriger pour créer le sous-groupe.";
		
		if (adresseATester.toPays() == null) {
			throw new NSValidation.ValidationException("Le pays de l'adresse du groupe parent est obligatoire." + message);
		}
		
		if (MyStringCtrl.isEmpty(adresseATester.adrAdresse1())) {
			throw new NSValidation.ValidationException("La première ligne de l'adresse  de l'adresse du groupe parent est obligatoire." + message);
		}

		if ((adresseATester.ville() == null || adresseATester.ville().trim().length() == 0)) {
			throw new NSValidation.ValidationException("La ville de l'adresse du groupe parent est obligatoire.");
		}

		if (isCodePostalObligatoire(adresseATester.editingContext())) {
			if (adresseATester.toPays().isPaysDefaut() && (adresseATester.codePostal() == null || adresseATester.codePostal().trim().length() == 0)) {
				throw new NSValidation.ValidationException("Le code postal de l'adresse du groupe parent est obligatoire pour : " + adresseATester.toPays().lcPays() + message);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
    public void copierAdresseFrom(IAdresse adresseACopier) {
        setAdrAdresse1(adresseACopier.adrAdresse1());
        setAdrAdresse2(adresseACopier.adrAdresse2());
        setAdrBp(adresseACopier.adrBp());
        setAdrGpsLatitude(adresseACopier.adrGpsLatitude());
        setAdrGpsLongitude(adresseACopier.adrGpsLongitude());
        setAdrListeRouge(adresseACopier.adrListeRouge());
        setAdrUrlPere(adresseACopier.adrUrlPere());
        setBisTer(adresseACopier.bisTer());
        setCImplantation(adresseACopier.cImplantation());
        setCodePostal(adresseACopier.codePostal());
        setCpEtranger(adresseACopier.cpEtranger());
        setCVoie(adresseACopier.cVoie());
        setDCreation(new NSTimestamp());
        setDDebVal(adresseACopier.dDebVal());
        setDFinVal(adresseACopier.dFinVal());
        setDModification(new NSTimestamp());
        setLocalite(adresseACopier.localite());
        setNomVoie(adresseACopier.nomVoie());
        setNoVoie(adresseACopier.noVoie());
        setTemPayeUtil(adresseACopier.temPayeUtil());
        setToPaysRelationship((EOPays) adresseACopier.toPays());
        setVille(adresseACopier.ville());
    }
    
	/**
	 * {@inheritDoc}
	 */
    public void copierAdresseTo(IAdresse adresseARenseigner) {
        adresseARenseigner.setAdrAdresse1(adrAdresse1());
        adresseARenseigner.setAdrAdresse2(adrAdresse2());
        adresseARenseigner.setAdrBp(adrBp());
        adresseARenseigner.setAdrGpsLatitude(adrGpsLatitude());
        adresseARenseigner.setAdrGpsLongitude(adrGpsLongitude());
        adresseARenseigner.setAdrListeRouge(adrListeRouge());
        adresseARenseigner.setAdrUrlPere(adrUrlPere());
        adresseARenseigner.setBisTer(bisTer());
        adresseARenseigner.setCImplantation(cImplantation());
        adresseARenseigner.setCodePostal(codePostal());
        adresseARenseigner.setCpEtranger(cpEtranger());
        adresseARenseigner.setCVoie(cVoie());
        adresseARenseigner.setDCreation(new NSTimestamp());
        adresseARenseigner.setDDebVal(dDebVal());
        adresseARenseigner.setDFinVal(dFinVal());
        adresseARenseigner.setDModification(new NSTimestamp());
        adresseARenseigner.setLocalite(localite());
        adresseARenseigner.setNomVoie(nomVoie());
        adresseARenseigner.setNoVoie(noVoie());
        adresseARenseigner.setTemPayeUtil(temPayeUtil());
        adresseARenseigner.setToPaysRelationship(toPays());
        adresseARenseigner.setVille(ville());
    }
    
    /**
     * {@inheritDoc}
     */
	public void setToPaysRelationship(IPays pays) {
		this.setToPaysRelationship((EOPays) pays);
	}
    
	/**
     * {@inheritDoc}
     */
	public String getEmail() {
		EORepartPersonneAdresse repart = toRepartPersonneAdresses(
				EORepartPersonneAdresse.RPA_VALIDE.eq(EORepartPersonneAdresse.RPA_VALIDE_OUI)
					.and(EORepartPersonneAdresse.E_MAIL.isNotNull())).lastObject();
		if (repart!=null) {
			return repart.eMail();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdresseParent() {
		return hasAdresseDeType(EOTypeAdresse.TADR_CODE_PAR);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdresseEtudiant() {
		return hasAdresseDeType(EOTypeAdresse.TADR_CODE_ETUD);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdresseInvalide() {
		return hasAdresseDeType(EOTypeAdresse.TADR_CODE_INVAL);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isAdressePerso() {
		return hasAdresseDeType(EOTypeAdresse.TADR_CODE_PERSO);
	}
	
	private boolean hasAdresseDeType(String codeTypeAdresse) {
		NSArray<EORepartPersonneAdresse> repartPersonneAdresses = 
				toRepartPersonneAdresses(EORepartPersonneAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(codeTypeAdresse));
		return !repartPersonneAdresses.isEmpty();
	}
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public ITypeAdresse typeAdresse() {
		NSArray<EOTypeAdresse> types = (NSArray<EOTypeAdresse>) toRepartPersonneAdresses().valueForKey(EORepartPersonneAdresse.TO_TYPE_ADRESSE_KEY);
		return types.lastObject();
	}

	/**
	 * {@inheritDoc}
	 */
	public String getEmailDeType(String codeType) {
		EORepartPersonneAdresse repart = getRepartDeType(codeType);
		return repart != null ? repart.eMail() : null;
	}


	/**
	 * {@inheritDoc}
	 */
	public void setEmailDeType(String email, String type) {
		EORepartPersonneAdresse repart = getRepartDeType(type);
		if (repart != null) {
			repart.setEMail(email);
		}
	}
	
	private EORepartPersonneAdresse getRepartDeType(String codeType) {
		EORepartPersonneAdresse repart = toRepartPersonneAdresses(
				EORepartPersonneAdresse.RPA_VALIDE.eq(EORepartPersonneAdresse.RPA_VALIDE_OUI)
				.and(EORepartPersonneAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(codeType))
				.and(EORepartPersonneAdresse.E_MAIL.isNotNull())).lastObject();
		return repart;
	}
}
