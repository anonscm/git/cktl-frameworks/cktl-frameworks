/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;


/**
 * Classe abstraite dont doivent heriter les finders.
 * 
 */

public abstract class AFinder {
	public static final String SEQ_PERSONNE_ENTITY_NAME = "Fwkpers_SeqPersonne" ;
	public static final String BASE_DONNEES_KEY = "BASE_DONNEES" ;
	



    
    
    /**
     * sans distinct
     * @param ec
     * @param entityName
     * @param qual
     * @param sortOrderings
     * @param refreshObjects
     * @return
     */
    public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qual, NSArray sortOrderings, boolean refreshObjects ){
        return fetchArray(ec, entityName, qual, sortOrderings, refreshObjects, false, false, null);
    }

	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qual, NSArray sortOrderings, boolean refreshObjects ,boolean usesDistinct, boolean isDeep, NSDictionary hints ){
		EOFetchSpecification spec = new EOFetchSpecification(entityName,qual,null,usesDistinct,isDeep,hints);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec); 
	}    
    
    /**
     * Fetch un tableau d'objets depuis la base de donnees.
     * 
     * @param ec
     * @param entityName
     * @param conditionStr
     * @param params
     * @param sortOrderings Tableau d'objets EOSortOrdering
     * @param refreshObjects Indique si on souhaite que les objets soient rï¿½cupï¿½rï¿½ de la base de donnï¿½es ou seulement depuis l'editingContext.
     * @param usesDistinct Effectuer un distinct sur le resultat (double le temps de requete à peu pret...)
     * @param isDeep Dans le cas où l'entite a des sous_entites
     * @return
     */
    public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final String conditionStr, final NSArray params, final NSArray sortOrderings, boolean refreshObjects, boolean usesDistinct, boolean isDeep, NSDictionary hints ){
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
        final EOFetchSpecification spec = new EOFetchSpecification(entityName,qual,sortOrderings,usesDistinct,isDeep,hints);
        spec.setRefreshesRefetchedObjects(refreshObjects);
        return ec.objectsWithFetchSpecification(spec); 
    }	
	
    public static final void removeDuplicatesInNSArray(final NSMutableArray array) {
        int i=array.count()-1;        
        while (i>=0) {
            final Object obj = array.objectAtIndex(i);
            int found = array.indexOfObject(obj); 
            if (found != NSArray.NotFound && found != i) {
                array.removeObjectAtIndex(i);
            }
            i--;
        }
    }

	
	
	

	/** retourne le generic record pour la globalID pass&eacute;e en param&egrave;tre dans l'editing context */
	public static EOGenericRecord objetForGlobalIDDansEditingContext(EOGlobalID globalID,EOEditingContext editingContext) {
		return (EOGenericRecord)editingContext.faultForGlobalID(globalID,editingContext);
	}
	// UtilitairesGraphiques de recherche
	/** Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @param forceRefresh true si forcer le refresh des objets
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon,boolean forceRefresh) {
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
		if(!nomEntite.equals("PhotosEmployes") && !nomEntite.equals("EmploiType") && !nomEntite.equals("PhotosStructuresGrhum")) {
			fs.setUsesDistinct(sansDoublon);
		}
		fs.setRefreshesRefetchedObjects(forceRefresh);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	
	/** Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon) {
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,sansDoublon,false);
	}
	
	/** Retourne le contenu complet d'une table sans qualification des entit&eacute;s
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntite(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true);
	}
	
	/** Retourne le contenu complet d'une table sans qualification des entit&eacute;s et avec un refresh
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntiteAvecRefresh(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true,true);
	}
	
	/** Recherche toutes les entit&eacute;s dont un champ a une certaine valeur
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext,String nomEntite,String nomLabel,String valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " caseInsensitiveLike %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	
	/** Recherche toutes les entit&eacute;s dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String nomLabel,Object valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(nomLabel + " = %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	
	/** Recherche l'entit&eacute; dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param nomLabel	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objet trouv&eacute;
	 */
	public static EOGenericRecord rechercherObjetAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String nomLabel,String valeur) {
		NSArray resultats = rechercherAvecAttributEtValeurEgale(editingContext,nomEntite,nomLabel,valeur);
		try {
			return (EOGenericRecord)resultats.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	/** Evalue la cl&eacute; primaire pour une entit&eacute; en cherchant dans la table de s&eacute;quence pour une base Oracle
	 * @param editingContext
	 * @param nomEntite
	 * @param nomClePrimaire
	 * @param nomEntiteSequenceOracle null si Oracle g&egrave;re la s&eacute;quence
	 * @return valeur de la cl&eacute; primaire
	 */
	public static Integer clePrimairePour(EOEditingContext editingContext,String nomEntite,String nomClePrimaire,String nomEntiteSequenceOracle,boolean estUnLong) {
			return numeroSequenceOracle(editingContext,nomEntiteSequenceOracle);
	}
	
	/** Recherche le num&eacute;ro de s&eacute;quence dans une table de s&eacute;quence Oracle (doit comporter un attribut nextval)
	 * @param editingContext
	 * @param nomEntiteSequenceOracle
	 * @return valeur trouv&eacute;e ou null
	 */
	public static Integer numeroSequenceOracle(EOEditingContext editingContext,String nomEntiteSequenceOracle) {
		if (nomEntiteSequenceOracle == null || nomEntiteSequenceOracle.equals("")) {
			return null;
		}
		EOFetchSpecification  myFetch = new EOFetchSpecification(nomEntiteSequenceOracle,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			return new Integer(numero.intValue());
		} catch (Exception e) {
			return null;
		}
	}

	/** Creation manuelle d'un persId a partir de la sequence Oracle.
	@param editingContext
	@return num du persID */
	public static Integer construirePersId(EOEditingContext editingContext) {
		return numeroSequenceOracle(editingContext,SEQ_PERSONNE_ENTITY_NAME);
	}
	
	
	// méthodes de recherche pour les EOGenericRecord
	
//
//    /** Recherche les passages chevron pour un grade et un &eacute;chelon et un chevron donn&eacute;s */
//	public static NSArray rechercherPassagesChevronPourGradeEchelonEtChevron(EOEditingContext editingContext,String grade,String echelon,String chevron) {
//		NSMutableArray args = new NSMutableArray(grade);
//		String qualifier = "cGrade =   %@";
//		args.addObject(echelon);
//		qualifier = qualifier + " AND cEchelon = %@";
//		if (chevron != null) {
//			args.addObject(echelon);
//			qualifier = qualifier + " AND cChevron = %@";
//		}
//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier ,args);
//		EOFetchSpecification myFetch = new EOFetchSpecification("PassageChevron",qual,null);
//		return editingContext.objectsWithFetchSpecification(myFetch);
//	}
//	 /** Recherche les passages chevron pour un grade et un &eacute;chelon et un chevron donn&eacute;s 
//	  * Retourne un tableau de passage chevron */
//	public static NSArray rechercherChevronsPourGradeEchelonAvecTri(EOEditingContext editingContext,String grade,String echelon) {
//		NSArray results = rechercherPassagesChevronPourGradeEchelonEtChevron(editingContext,grade,echelon,null);
//		return EOSortOrdering.sortedArrayUsingKeyOrderArray(results,new NSArray(EOSortOrdering.sortOrderingWithKey("cChevron",EOSortOrdering.CompareAscending)));
//	}

	
	
//	/** retourne une date format&eacute;e sous forme de String : le format est jj/mm/aaaa
//	 * @param record	record pour lequel on doit formatter une date
//	 * @param cle cl&eacute; du champ date
//	 * @return date format&eacute;e
//	 */
//	public static String dateFormatee(EOGenericRecord record,String cle) {
//		if (record.valueForKey(cle) == null) {
//			return "";
//		} else {	
//			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
//			return formatter.format(record.valueForKey(cle));
//		}
//	}
//	
//	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
//	 * sera automatiquement compl&eacute;t&eacute;e
//	 * @param record record pour lequel on doit fournir une date (NSTimestamp)
//	 * @param cle cl&eacute; du champ date
//	 * @param uneDate valeur de la date (String)
//	 */
//	public static void setDateFormatee(EOGenericRecord record,String cle,String uneDate) {
//		if (uneDate == null) {
//			record.takeValueForKey(null,cle);
//		}
//		String myDate = DateCtrl.dateCompletion((String)uneDate);
//		if (myDate.equals("")) {
//	        record.takeValueForKey(null,cle);
//	    } else {
//	    		record.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
//	    } 
//	}
//	/** retourne une date format&eacute;e sous forme de String : le format est jj/mm/aaaa
//	 * @param object	objet pour lequel on doit formatter une date (doit impl&eacute;menter le key-value coding)
//	 * @param cle cl&eacute; du champ date
//	 * @return date format&eacute;e
//	 */
//	public static String dateFormatee(NSKeyValueCoding object,String cle) {
//		if (object.valueForKey(cle) == null) {
//			return "";
//		} else {	
//			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
//			return formatter.format(object.valueForKey(cle));
//		}
//	}
//	
//	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
//	 * sera automatiquement compl&eacute;t&eacute;e
//	 * @param object objet pour lequel on doit fournir une date (NSTimestamp)  (doit impl&eacute;menter le key-value coding)
//	 * @param cle cl&eacute; du champ date
//	 * @param uneDate valeur de la date (String)
//	 */
//	public static void setDateFormatee(NSKeyValueCoding object,String cle,String uneDate) {
//		if (uneDate == null) {
//			object.takeValueForKey(null,cle);
//		}
//		String myDate = DateCtrl.dateCompletion((String)uneDate);
//		if (myDate.equals("")) {
//			object.takeValueForKey(null,cle);
//	    } else {
//	    	object.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
//	    } 
//	}
//
//	
	

	
}
