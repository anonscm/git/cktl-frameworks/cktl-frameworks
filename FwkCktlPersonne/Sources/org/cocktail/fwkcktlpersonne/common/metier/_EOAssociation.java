/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAssociation.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOAssociation extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOAssociation.class);

	public static final String ENTITY_NAME = "Fwkpers_Association";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ASSOCIATION";


// Attribute Keys
  public static final ERXKey<String> ASS_CODE = new ERXKey<String>("assCode");
  public static final ERXKey<String> ASS_LIBELLE = new ERXKey<String>("assLibelle");
  public static final ERXKey<String> ASS_LOCALE = new ERXKey<String>("assLocale");
  public static final ERXKey<String> ASS_RACINE = new ERXKey<String>("assRacine");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_FERMETURE = new ERXKey<NSTimestamp>("dFermeture");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_OUVERTURE = new ERXKey<NSTimestamp>("dOuverture");
  public static final ERXKey<Integer> TAS_ID = new ERXKey<Integer>("tasId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> TO_ASSOCIATION_RESEAU_FILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>("toAssociationReseauFils");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> TO_ASSOCIATION_RESEAU_PERES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>("toAssociationReseauPeres");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation> TO_TYPE_ASSOCIATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation>("toTypeAssociation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "assId";

	public static final String ASS_CODE_KEY = "assCode";
	public static final String ASS_LIBELLE_KEY = "assLibelle";
	public static final String ASS_LOCALE_KEY = "assLocale";
	public static final String ASS_RACINE_KEY = "assRacine";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String TAS_ID_KEY = "tasId";

// Attributs non visibles
	public static final String ASS_ID_KEY = "assId";

//Colonnes dans la base de donnees
	public static final String ASS_CODE_COLKEY = "ASS_CODE";
	public static final String ASS_LIBELLE_COLKEY = "ASS_LIBELLE";
	public static final String ASS_LOCALE_COLKEY = "ASS_LOCALE";
	public static final String ASS_RACINE_COLKEY = "ASS_RACINE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String TAS_ID_COLKEY = "TAS_ID";

	public static final String ASS_ID_COLKEY = "ASS_ID";


	// Relationships
	public static final String TO_ASSOCIATION_RESEAU_FILS_KEY = "toAssociationReseauFils";
	public static final String TO_ASSOCIATION_RESEAU_PERES_KEY = "toAssociationReseauPeres";
	public static final String TO_TYPE_ASSOCIATION_KEY = "toTypeAssociation";



	// Accessors methods
  public String assCode() {
    return (String) storedValueForKey(ASS_CODE_KEY);
  }

  public void setAssCode(String value) {
    takeStoredValueForKey(value, ASS_CODE_KEY);
  }

  public String assLibelle() {
    return (String) storedValueForKey(ASS_LIBELLE_KEY);
  }

  public void setAssLibelle(String value) {
    takeStoredValueForKey(value, ASS_LIBELLE_KEY);
  }

  public String assLocale() {
    return (String) storedValueForKey(ASS_LOCALE_KEY);
  }

  public void setAssLocale(String value) {
    takeStoredValueForKey(value, ASS_LOCALE_KEY);
  }

  public String assRacine() {
    return (String) storedValueForKey(ASS_RACINE_KEY);
  }

  public void setAssRacine(String value) {
    takeStoredValueForKey(value, ASS_RACINE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
  }

  public void setDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_KEY);
  }

  public Integer tasId() {
    return (Integer) storedValueForKey(TAS_ID_KEY);
  }

  public void setTasId(Integer value) {
    takeStoredValueForKey(value, TAS_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation toTypeAssociation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation)storedValueForKey(TO_TYPE_ASSOCIATION_KEY);
  }

  public void setToTypeAssociationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation oldValue = toTypeAssociation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ASSOCIATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ASSOCIATION_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauFils() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)storedValueForKey(TO_ASSOCIATION_RESEAU_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauFils(EOQualifier qualifier) {
    return toAssociationReseauFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauFils(EOQualifier qualifier, boolean fetch) {
    return toAssociationReseauFils(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau.TO_ASSOCIATION_FILS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toAssociationReseauFils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToAssociationReseauFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_FILS_KEY);
  }

  public void removeFromToAssociationReseauFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_FILS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau createToAssociationReseauFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_AssociationReseau");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_ASSOCIATION_RESEAU_FILS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau) eo;
  }

  public void deleteToAssociationReseauFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToAssociationReseauFilsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> objects = toAssociationReseauFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToAssociationReseauFilsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauPeres() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)storedValueForKey(TO_ASSOCIATION_RESEAU_PERES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauPeres(EOQualifier qualifier) {
    return toAssociationReseauPeres(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauPeres(EOQualifier qualifier, boolean fetch) {
    return toAssociationReseauPeres(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> toAssociationReseauPeres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau.TO_ASSOCIATION_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toAssociationReseauPeres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToAssociationReseauPeresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_PERES_KEY);
  }

  public void removeFromToAssociationReseauPeresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_PERES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau createToAssociationReseauPeresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_AssociationReseau");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_ASSOCIATION_RESEAU_PERES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau) eo;
  }

  public void deleteToAssociationReseauPeresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_ASSOCIATION_RESEAU_PERES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToAssociationReseauPeresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau> objects = toAssociationReseauPeres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToAssociationReseauPeresRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOAssociation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAssociation createEOAssociation(EOEditingContext editingContext, String assCode
, String assLibelle
			) {
    EOAssociation eo = (EOAssociation) createAndInsertInstance(editingContext, _EOAssociation.ENTITY_NAME);    
		eo.setAssCode(assCode);
		eo.setAssLibelle(assLibelle);
    return eo;
  }

  
	  public EOAssociation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAssociation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAssociation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAssociation creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOAssociation object = (EOAssociation)createAndInsertInstance(editingContext, _EOAssociation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAssociation localInstanceIn(EOEditingContext editingContext, EOAssociation eo) {
    EOAssociation localInstance = (eo == null) ? null : (EOAssociation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAssociation#localInstanceIn a la place.
   */
	public static EOAssociation localInstanceOf(EOEditingContext editingContext, EOAssociation eo) {
		return EOAssociation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOAssociation>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAssociation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAssociation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAssociation> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAssociation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAssociation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAssociation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAssociation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAssociation> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAssociation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAssociation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAssociation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAssociation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAssociation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAssociation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
