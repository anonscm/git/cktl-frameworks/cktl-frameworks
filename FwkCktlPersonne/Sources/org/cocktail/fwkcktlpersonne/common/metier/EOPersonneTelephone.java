/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPersonneTelephone.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * Gestion des telephones. Exemple <br/>
 * <code>
 * 		EOPersonneTelephone personneTelephone = EOPersonneTelephone.creerInstance(edForTelephone);
 * 		personneTelephone.setToTypeTelRelationship(EOTypeTel.fetchByKeyValue(edc(), EOTypeTel.C_TYPE_TEL_KEY, EOTypeTel.C_TYPE_TEL_PRF));
 * 		personneTelephone.setToTypeNoTelRelationship(typeNoTel);
 * 		personneTelephone.setNoTelephone(noTelephone);
 * 		personneTelephone.setIndicatif(indicatif);
 * 		personneTelephone.setPersId(getFournis().toPersonne().persId());
 * 		personneTelephone.addToPersonneRelationShip(getFournis().toPersonne());
 * </code> <li>
 * <ul>
 * </ul></li>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EOPersonneTelephone extends _EOPersonneTelephone implements ITelephone {
	private static final String KEY_GRHUM_PRF_MIN = "GRHUM_PRF_MIN";
	private static final String KEY_GRHUM_PRF_MAX = "GRHUM_PRF_MAX";

	private static final EOQualifier QUAL_PERSONNE_TELEPHONE_PRIVE = EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TO_TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY + " = 'PRV'", null);
	private static final EOQualifier QUAL_PERSONNE_TELEPHONE_PRF = EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TO_TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY + " = '" + EOTypeTel.C_TYPE_TEL_PRF + "'", null);
	
	private static final EOQualifier QUAL_PERSONNE_TELEPHONE_VIDE = EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.NO_TELEPHONE_KEY + " = ''", null);
	//	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_OK = new EOAndQualifier(new NSArray(new Object[] { new EONotQualifier(QUAL_PERSONNE_TELEPHONE_PRIVE), new EONotQualifier(QUAL_PERSONNE_TELEPHONE_VIDE) }));
	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_OK = new EONotQualifier(QUAL_PERSONNE_TELEPHONE_VIDE);
	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_FAX = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOQualifier.QualifierOperatorEqual, EOTypeNoTel.TYPE_NO_TEL_FAX);
	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_NON_FAX = new EONotQualifier(QUAL_PERSONNE_TELEPHONE_FAX);
	
	public static final EOQualifier QUAL_PERSONNE_LISTE_ROUGE = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOQualifier.QualifierOperatorEqual, EOPersonneTelephone.LISTE_ROUGE_O);
	public static final EOQualifier QUAL_PERSONNE_SANS_NUM_LISTE_ROUGE = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY, EOQualifier.QualifierOperatorEqual, EOPersonneTelephone.LISTE_ROUGE_N);
	
	/** Qualifier pour les telephones professionnels non vides. */
	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_PRF_OK = new EOAndQualifier(new NSArray(new Object[] {
			QUAL_PERSONNE_TELEPHONE_PRF, new EONotQualifier(QUAL_PERSONNE_TELEPHONE_VIDE)
	}));

	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_NON_PRIVE = ERXQ.notEquals(EOPersonneTelephone.TO_TYPE_TEL_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY, EOTypeTel.C_TYPE_TEL_PRV);
	public static final EOQualifier QUAL_PERSONNE_TELEPHONE_ETUD = new EOKeyValueQualifier(EOPersonneTelephone.TO_TYPE_TEL_KEY+"."+EOTypeTel.C_TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, EOTypeTel.QUAL_C_TYPE_TEL_ETUD);
	/**
	 * Permet d'avoir le téléphone principal en premier
	 */
	public static final EOSortOrdering SORT_TEL_PRINCIPAL_DESC = EOSortOrdering.sortOrderingWithKey(EOPersonneTelephone.TEL_PRINCIPAL_KEY, EOSortOrdering.CompareDescending);

	public static final String LISTE_ROUGE_O = "O";
	public static final String LISTE_ROUGE_N = "N";

	private static int tailleTelMin = 0;
	private static int tailleTelMax = 0;

	public static final String TELEPHONE_VIDE = "Un n° de téléphone n'a pas été renseigné.";

	/**
	 * N'utilisez pas cette methode, utilisez à la place {@link EOPersonneTelephone#creerInstance(EOEditingContext)}.
	 */
	public EOPersonneTelephone() {
		super();
	}

	/**
	 * Peut etre appele à partir des factories.<br/>
	 * Liste des vérifications :
	 * <ul>
	 * <li>Telephone vide</li>
	 * <li>Suppression de l'indicatif dans le cas d'un tel interne</li>
	 * <li>Presence l'indicatif dans le cas d'un tel non interne</li>
	 * <li>Validité de l'indicatif pays si celui-ci est saisi</li>
	 * <li>Nombre de chiffres dans le n° si indicatif du pays = indicatif du pays par defaut</li>
	 * <li>Verifie si un des telephones est defini comme principal</li>
	 * <li>Verifie si le n° est deja saisi</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		trimAllString();
		setDModification(MyDateCtrl.now());
		if (dCreation() == null) {
			setDCreation(MyDateCtrl.now());
		}
		if (noTelephone() == null || noTelephone().length() == 0) {
			throw new NSValidation.ValidationException(TELEPHONE_VIDE);
		}
		initParams(editingContext());
		fixIndicatif();
		checkIndicatif();
		checkLongueurSuivantIndicatif();
		if (telPrincipal() == null) {
			setTelPrincipal(NON);
		}
		checkContraintesObligatoires();
		checkContraintesLongueursMax();

		//Verifier si un tel principal est defini
		NSArray autresTels = new NSArray();
		if (this.toIndividu() != null) {
			autresTels = this.toIndividu().toPersonneTelephones();
		}
		else if (this.toStructure() != null) {
			autresTels = this.toStructure().toPersonneTelephones();
		}
		boolean isTelPrinc = isTelPrincipal().booleanValue();
		if (!isTelPrinc) {
			if (autresTels != null) {
				for (int i = 0; i < autresTels.count() && !isTelPrinc; i++) {
					if (((EOPersonneTelephone) autresTels.objectAtIndex(i)).isTelPrincipal().booleanValue()) {
						isTelPrinc = true;
					}
				}
			}
		}
		if (!isTelPrinc) {
			throw new NSValidation.ValidationException("Veuillez définir un n° de téléphone comme principal.");
		}
		if(autresTels != null) {
			for (int i = 0; i < autresTels.count(); i++) {
				if (autresTels != null) {
					EOPersonneTelephone pt = (EOPersonneTelephone) autresTels.objectAtIndex(i);
					if (pt.toTypeTel().equals(this.toTypeTel()) && pt.toTypeNoTel().equals(this.toTypeNoTel()) && !pt.globalID().equals(globalID())) {
						if (nettoieNoTelephone(noTelephone()).equals(nettoieNoTelephone(pt.noTelephone()))) {
							throw new NSValidation.ValidationException("Le numéro de " + this.toTypeNoTel().lTypeNoTel() + " " + MyStringCtrl.formatPhoneNumber(noTelephone()) + " existe déja.");
						}
					}
				}
			}
		}
		super.validateObjectMetier();
	}

	//
	//	/**
	//	 * Cree une instance de la classe et l'insere dans l'editing context.
	//	 * @param ed
	//	 * @return
	//	 * @throws Exception
	//	 */
	//	public static EOPersonneTelephone creerInstance(EOEditingContext ed) throws Exception {
	//		EOPersonneTelephone object = (EOPersonneTelephone) AUtils.instanceForEntity(ed, EOPersonneTelephone.ENTITY_NAME);
	//		ed.insertObject(object);
	//		object.onCreerInstance();
	//		return object;
	//	}

	/**
	 * Affecte le n° de telephone en le nettoyant (sauf si le nouveau n° de telephone nettoye = telephone stocke nettoye).
	 */
	public void setNoTelephone(String noTelephone) {
		String cleanedNoTelephone = nettoieNoTelephone(noTelephone);
		//verifier si le n° de telephone est identique au tel nettoyé nettoyer (pour eviter une exception lors de la mise a jour de la cle primaire)
		if (noTelephone != null && noTelephone() != null && cleanedNoTelephone.equals(nettoieNoTelephone(noTelephone()))) {
			return;
		}
		super.setNoTelephone(cleanedNoTelephone);
	}

	/**
	 * @param noTelephone
	 * @return le numero de telephone nettoye
	 */
	public static String nettoieNoTelephone(String noTelephone) {
		if (noTelephone == null) {
			return null;
		}
		noTelephone = AUtils.keepDigits(noTelephone);
		return noTelephone;
	}

	private static void initParams(EOEditingContext ec) {
		if (tailleTelMin == 0) {
			String taille = EOGrhumParametres.parametrePourCle(ec, KEY_GRHUM_PRF_MIN);
			if (taille == null) {
				throw new RuntimeException(KEY_GRHUM_PRF_MIN + " non trouve dans les parametres.");
			}
			tailleTelMin = Integer.valueOf(taille).intValue();
		}
		if (tailleTelMax == 0) {
			String taille = EOGrhumParametres.parametrePourCle(ec, KEY_GRHUM_PRF_MAX);
			if (taille == null) {
				throw new RuntimeException(KEY_GRHUM_PRF_MAX + " non trouve dans les parametres.");
			}
			tailleTelMax = Integer.valueOf(taille).intValue();
		}
	}

	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(AUtils.now());
	}

	public void setIsTelPrincipal(Boolean value) {
		if (value.booleanValue()) {
			setTelPrincipal(OUI);
		}
		else {
			setTelPrincipal(NON);
		}
	}

	/**
	 * @param value Si value = O, les autres téléphones sont passés a telPrincipal N
	 */
	public void setTelPrincipal(String value) {
		//Mettre les autres telephones a N
		if (OUI.equals(value)) {
			NSArray autresTels = NSArray.emptyArray();
			if (this.toPersonne() != null) {
				autresTels = this.toPersonne().toPersonneTelephones();
			}
			for (int i = 0; i < autresTels.count(); i++) {
				EOPersonneTelephone repart = (EOPersonneTelephone) autresTels.objectAtIndex(i);
				if (!NON.equals(repart.telPrincipal())) {
					repart.setTelPrincipal(NON);
					repart.setDModification(AUtils.now());
				}
			}
		}
		super.setTelPrincipal(value);
	}

	public Boolean isTelPrincipal() {
		return Boolean.valueOf(OUI.equals(telPrincipal()));
	}

	/**
	 * @return L'individu associe au telephone.
	 */
	public EOIndividu toIndividu() {
		return (EOIndividu) (toIndividus().count() > 0 ? toIndividus().objectAtIndex(0) : null);
	}

	/**
	 * @return La structure associee au telephone.
	 */
	public EOStructure toStructure() {
		return (EOStructure) (toStructures().count() > 0 ? toStructures().objectAtIndex(0) : null);
	}

	public IPersonne toPersonne() {
		return (toIndividu() != null ? (IPersonne) toIndividu() : (IPersonne) toStructure());
	}

	/**
	 * @return le numero de telephone formatte, precede de l'indicatif.
	 */
	public String getTelephoneFormateAvecIndicatif() {
//		String res = MyStringCtrl.formatPhoneNumber(noTelephone());
//
//		if (res != null && indicatif() != null) {
//			res = "(" + indicatif() + ") " + res;
//		}
//		return res;
		return  getTelephoneFormateSuivantIndicatif(noTelephone());
	}
	
	/**
	 * Mise en forme pour des numéros français ou allemands
	 * Pas de mise en forme si aucun des 2
	 * @param telephone
	 * @return
	 */
	private String getTelephoneFormateSuivantIndicatif(String telephone){
		String res = telephone;
		if (res != null && indicatif() != null){
			res = MyStringCtrl.formatPhoneNumber(noTelephone());
			
			if ( 49 == indicatif()){
				res = MyStringCtrl.formatPhoneNumberGermany(noTelephone());
			}
			
			res = "(+" + indicatif() + ") " + res;
		}
		return res;
	}

	/**
	 * Methode de convenance a appeler pour affecter une personne a l'objet. Si personne est de la classe d'EOIndividu, la methode
	 * {@link #addToToIndividusRelationship(EOIndividu)} est appelee, sinon c'est la methode {@link #addToToStructuresRelationship(EOStructure)}.
	 * 
	 * @param personne objet de la classe EOIndividu ou EOStructure.
	 * @deprecated Utilisez {@link #setToPersonneRelationship(IPersonne)} a la place.
	 */
	public void addToPersonneRelationShip(IPersonne personne) {
		if (personne instanceof EOIndividu) {
			this.addToToIndividusRelationship((EOIndividu) personne);
		}
		else {
			this.addToToStructuresRelationship((EOStructure) personne);
		}
	}

	/**
	 * Methode de convenance a appeler pour supprimer l'association d'une personne a l'objet. Si personne est de la classe d'EOIndividu, la methode
	 * {@link #removeFromToIndividusRelationship(EOIndividu)} est appelee, sinon c'est la methode
	 * {@link #removeFromToStructuresRelationship(EOStructure)}.
	 * 
	 * @param personne objet de la classe EOIndividu ou EOStructure.
	 * @deprecated Utilisez {@link #setToPersonneRelationship(IPersonne)} a la place.
	 */
	public void removeFromToPersonneRelationship(IPersonne personne) {
		if (personne instanceof EOIndividu) {
			this.removeFromToIndividusRelationship((EOIndividu) personne);
		}
		else {
			this.removeFromToStructuresRelationship((EOStructure) personne);
		}
	}

	/**
	 * Utilisez plutot {@link #addToPersonneRelationShip(IPersonne)}
	 */
	public void addToToIndividusRelationship(EOIndividu object) {
		super.addToToIndividusRelationship(object);
		definirUnTelPrincipal();
	}

	/**
	 * Utilisez plutot {@link #addToPersonneRelationShip(IPersonne)}
	 */
	public void addToToStructuresRelationship(EOStructure object) {
		super.addToToStructuresRelationship(object);
		definirUnTelPrincipal();
	}

	public void setToPersonneRelationship(IPersonne personne) {
		if (personne == null) {
			while (toIndividus().count() > 0) {
				removeFromToIndividusRelationship((EOIndividu) toIndividus().objectAtIndex(0));
			}
			while (toStructures().count() > 0) {
				removeFromToStructuresRelationship((EOStructure) toStructures().objectAtIndex(0));
			}
			setPersId(null);
		}
		else {
			if (personne instanceof EOIndividu) {
				this.addToToIndividusRelationship((EOIndividu) personne);
			}
			if (personne instanceof EOStructure) {
				this.addToToStructuresRelationship((EOStructure) personne);
			}
			setPersId(personne.persId());
		}
	}

	@Override
	public void setToTypeNoTelRelationship(EOTypeNoTel value) {
		super.setToTypeNoTelRelationship(value);
		if (value == null) {
			this.setTypeNo(null);
		}
		else {
			this.setTypeNo(value.cTypeNoTel());
		}
	}

	@Override
	public void setToTypeTelRelationship(EOTypeTel value) {
		super.setToTypeTelRelationship(value);
		if (value == null) {
			this.setTypeTel(null);
		}
		else {
			this.setTypeTel(value.cTypeTel());
		}
	}

	/**
	 * Si aucun autre telephone principal, le definir comme principal.
	 */
	protected void definirUnTelPrincipal() {
		NSArray autresTels = new NSArray();
		boolean isOtherTelPrinc = false;
		autresTels = this.toPersonne().toPersonneTelephones();
		for (int i = 0; i < autresTels.count() && !isOtherTelPrinc; i++) {
			if (((EOPersonneTelephone) autresTels.objectAtIndex(i)).isTelPrincipal().booleanValue()) {
				isOtherTelPrinc = true;
			}
		}
		if (!isOtherTelPrinc) {
			setTelPrincipal(OUI);
		}
	}

	/**
	 * @return le noTelephone avec chiffres uniquement.
	 */
	@Override
	public String noTelephone() {
		return nettoieNoTelephone(super.noTelephone());
	}

	/**
	 * Si Type de telephone interne, on supprime l'indicatif;
	 */
	private void fixIndicatif() {
		if (EOTypeTel.C_TYPE_TEL_INT.equals(toTypeTel().cTypeTel())) {
			setIndicatif(null);
		}
	}

	/**
	 * Indicatif obligatoire si telephone non interne, verification de la validité de l'indicatif.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkIndicatif() throws NSValidation.ValidationException {
		if (!EOTypeTel.C_TYPE_TEL_INT.equals(toTypeTel().cTypeTel())) {
			if (indicatif() == null) {
				throw new NSValidation.ValidationException("L'indicatif est obligatoire pour un numero non interne.");
			}
		}

		//verifier l'indicatif si saisi
		if (indicatif() != null) {
			EOKeyValueQualifier qual = new EOKeyValueQualifier(EOPaysIndicatif.INDICATIF_KEY, EOQualifier.QualifierOperatorEqual, indicatif());
			if (EOPaysIndicatif.fetchAll(editingContext(), qual).count() == 0) {
				throw new NSValidation.ValidationException("L'indicatif telephonique " + indicatif() + " est inconnu.");
			}
		}
	}

	/**
	 * Si indicatif existe et = pays defaut, verification des longuiers min et max.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkLongueurSuivantIndicatif() throws NSValidation.ValidationException {
		if (indicatif() != null) {
			if (indicatif().equals(getIndicatifPaysDefaut(this.editingContext()).indicatif())) {
				if ((tailleTelMin != 0 && noTelephone().length() < tailleTelMin) || (tailleTelMax != 0 && noTelephone().length() > tailleTelMax)) {
					throw new NSValidation.ValidationException("Pour l'indicatif " + indicatif() + " le n° doit comporter de " + tailleTelMin + " à " + tailleTelMax + " chiffres (" + noTelephone() + ").");
				}
			}
		}
	}

	/**
	 * @param ec
	 * @return L'indicatif du pays par defaut (indique dans les parametres de GRHUM).
	 * @see EOPays#getPaysDefaut(EOEditingContext)
	 */
	public static EOPaysIndicatif getIndicatifPaysDefaut(EOEditingContext ec) {
		return EOPays.getPaysDefaut(ec).toPaysIndicatif();
	}

	public void supprimer() {
		setToPersonneRelationship(null);
		editingContext().deleteObject(this);
	}

	public Integer persIdCreation() {
		return this.persIdCreation();
	}

	public Integer persIdModifcation() {
		return this.persIdModifcation();
	}

	public void setPersIdModifcation(Integer value) {
		this.setPersIdModifcation(value);
		
	}

	public void setToTypeNoTelRelationship(ITypeNoTel value) {
		this.setToTypeNoTelRelationship((EOTypeNoTel)value);
		
	}

	public ITypeTel toTypeTels() {
		
		return this.toTypeTel();
	}

	public void setToTypeTelsRelationship(ITypeTel value) {
		this.setToTypeTelRelationship((EOTypeTel)value);
		
	}

}
