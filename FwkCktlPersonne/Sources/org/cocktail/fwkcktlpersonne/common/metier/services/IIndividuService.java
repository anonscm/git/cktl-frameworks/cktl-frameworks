package org.cocktail.fwkcktlpersonne.common.metier.services;

import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuCondition;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;


public interface IIndividuService {
	
	/**
	 * Applique les contrôles applicable à un individu. Dès qu'un des contrôles échoue, le contrôle s'arrête et retourne une Exception.
	 * 
	 * @param individu
	 * @return liste des résultats invalides
	 */
	public List<ResultatControle> appliqueControles(IIndividu individu);

	/**
	 * Retourne la liste des contrôles appliqués
	 * @return liste de IIndividuControle
	 */
	public Map<IIndividuCondition,List<IIndividuControle>> getControles();

	/**
	 * Défini la liste des contrôles appliqués
	 * @param Liste de IIndividuControle
	 */
	public void setControles(Map<IIndividuCondition,List<IIndividuControle>> controles);
	
	/**
	 * 
	 * @return la liste des contrôles employés
	 */
	public List<String> getListeControles();

}