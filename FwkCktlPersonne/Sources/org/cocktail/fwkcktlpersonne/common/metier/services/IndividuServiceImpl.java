package org.cocktail.fwkcktlpersonne.common.metier.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuCondition;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;


public class IndividuServiceImpl implements IIndividuService {

	public static final IIndividuCondition CONDITION_TJRS_VRAIE = new IIndividuCondition() {
		
		public boolean isValide(IIndividu individu) {
			return true;
		}
	};
	public static final IIndividuCondition CONDITION_TJRS_FAUSSE = new IIndividuCondition() {
		
		public boolean isValide(IIndividu individu) {
			return false;
		}
	};
	
	private Map<IIndividuCondition,List<IIndividuControle>> controlesConditionnes = new HashMap<IIndividuCondition,List<IIndividuControle>>();
	
	public List<ResultatControle> appliqueControles(IIndividu individu) {

		List<ResultatControle> resultats = new ArrayList<ResultatControle>();
		for (IIndividuCondition condition : controlesConditionnes.keySet()) {
			if (condition.isValide(individu)) {
				for (IIndividuControle controle : controlesConditionnes.get(condition)) {
					ResultatControle resultat = controle(individu, controle);
					if (!resultat.valide()) {
						resultats.add(resultat);
					}
				}
			}
		}

		return resultats;

	}
	
	public List<String> getListeControles() {
		
		List<String> resultats = new ArrayList<String>();
		for (IIndividuCondition condition : controlesConditionnes.keySet()) {
				for (IIndividuControle controle : controlesConditionnes.get(condition)) {
						resultats.add(controle.getClass().getSimpleName());
				}
		}
		
		return resultats;
		
	}
	
	protected ResultatControle controle(IIndividu individu, IIndividuControle controle) {

		if (controle.checkable(individu)) {
			return controle.check(individu);
		}
		return ResultatControle.RESULTAT_OK;
	}

	public Map<IIndividuCondition,List<IIndividuControle>> getControles() {
		return controlesConditionnes;
	}

	public void setControles(Map<IIndividuCondition,List<IIndividuControle>> controles) {
		this.controlesConditionnes = controles;
	}


}