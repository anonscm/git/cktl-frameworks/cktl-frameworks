/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IReferensDcp;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOReferensDcp extends _EOReferensDcp implements IReferensDcp {

    public EOReferensDcp() {
        super();
    }

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
    // methodes rajoutees

    /**
     * Avec les changement de bap, les lettres ne correspondent plus ...
     */
    private String deductedLettreBap() {
    	if (lettreBap().equals(numDcp())) {
    		return lettreBap();
    	} else {
    		if (numDcp().length() == 1) {
    			return lettreBap();
    		} else {
    			return numDcp().substring(0, 1);
    		}
    	}
    }
    
    /**
     * Pour l'affichage
     * @return une string avec la lettre du Bap et l'intitulé du DCP
     */
    public String display() {
    	String display = deductedLettreBap() + " - " + StringCtrl.capitalizeWords(intitulDcp());
    	if (isArchive()) {
    		display = "___NE PLUS UTILISER__" + display;
    	}
    	return display;
    }
 
    // liste des cle primaire des bap desactivees
    /**
     * liste des cle primaire des bap desactivees
     * @return une liste des bap désactivées
     */
    public final static NSArray LIST_NUMDCP_ARCHIVE = new NSArray(new String[] {
    		"A", "B", "C", "D", "E", "F", "G", "H", "I"});
    
    /**
     * Indique si la DCP est une DCP archivee ou non.
     * @return true si le DCP est archivé
     */
    public boolean isArchive() {
    	return LIST_NUMDCP_ARCHIVE.containsObject(numDcp());
    }
    
    /**
     * 
     * @return la lettre associé au Bap
     */
    public String lettreBap() {
    	if (numDcp() == null) {
//    		return "PRRRRRRRRRRRRRRRRRRRROUT";
    		return "Perdu car le numéro de DCP est vide !";
    	}
    	if (numDcp() != null) {
    		if (numDcp().equals("BB")) { return "B"; }
    		if (numDcp().equals("CC")) { return "C"; }
    	}
    	return super.lettreBap();
    }
}
