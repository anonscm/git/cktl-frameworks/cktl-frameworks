/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBac.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOBac extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOBac.class);

	public static final String ENTITY_NAME = "Fwkpers_Bac";
	public static final String ENTITY_TABLE_NAME = "GRHUM.BAC";


// Attribute Keys
  public static final ERXKey<String> BAC_CODE = new ERXKey<String>("bacCode");
  public static final ERXKey<String> BAC_CODE_SISE = new ERXKey<String>("bacCodeSise");
  public static final ERXKey<Long> BAC_DATE_INVALIDITE = new ERXKey<Long>("bacDateInvalidite");
  public static final ERXKey<Long> BAC_DATE_VALIDITE = new ERXKey<Long>("bacDateValidite");
  public static final ERXKey<String> BAC_LIBELLE = new ERXKey<String>("bacLibelle");
  public static final ERXKey<String> BAC_TYPE = new ERXKey<String>("bacType");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bacCode";

	public static final String BAC_CODE_KEY = "bacCode";
	public static final String BAC_CODE_SISE_KEY = "bacCodeSise";
	public static final String BAC_DATE_INVALIDITE_KEY = "bacDateInvalidite";
	public static final String BAC_DATE_VALIDITE_KEY = "bacDateValidite";
	public static final String BAC_LIBELLE_KEY = "bacLibelle";
	public static final String BAC_TYPE_KEY = "bacType";

// Attributs non visibles
	public static final String BAC_CODE_NATIONAL_KEY = "bacCodeNational";
	public static final String BAC_VALIDITE_KEY = "bacValidite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

//Colonnes dans la base de donnees
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String BAC_CODE_SISE_COLKEY = "BAC_CODE_SISE";
	public static final String BAC_DATE_INVALIDITE_COLKEY = "BAC_DATE_INVALIDITE";
	public static final String BAC_DATE_VALIDITE_COLKEY = "BAC_DATE_VALIDITE";
	public static final String BAC_LIBELLE_COLKEY = "BAC_LIBELLE";
	public static final String BAC_TYPE_COLKEY = "BAC_TYPE";

	public static final String BAC_CODE_NATIONAL_COLKEY = "BAC_CODE_NATIONAL";
	public static final String BAC_VALIDITE_COLKEY = "BAC_VALIDITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";


	// Relationships



	// Accessors methods
  public String bacCode() {
    return (String) storedValueForKey(BAC_CODE_KEY);
  }

  public void setBacCode(String value) {
    takeStoredValueForKey(value, BAC_CODE_KEY);
  }

  public String bacCodeSise() {
    return (String) storedValueForKey(BAC_CODE_SISE_KEY);
  }

  public void setBacCodeSise(String value) {
    takeStoredValueForKey(value, BAC_CODE_SISE_KEY);
  }

  public Long bacDateInvalidite() {
    return (Long) storedValueForKey(BAC_DATE_INVALIDITE_KEY);
  }

  public void setBacDateInvalidite(Long value) {
    takeStoredValueForKey(value, BAC_DATE_INVALIDITE_KEY);
  }

  public Long bacDateValidite() {
    return (Long) storedValueForKey(BAC_DATE_VALIDITE_KEY);
  }

  public void setBacDateValidite(Long value) {
    takeStoredValueForKey(value, BAC_DATE_VALIDITE_KEY);
  }

  public String bacLibelle() {
    return (String) storedValueForKey(BAC_LIBELLE_KEY);
  }

  public void setBacLibelle(String value) {
    takeStoredValueForKey(value, BAC_LIBELLE_KEY);
  }

  public String bacType() {
    return (String) storedValueForKey(BAC_TYPE_KEY);
  }

  public void setBacType(String value) {
    takeStoredValueForKey(value, BAC_TYPE_KEY);
  }


/**
 * Créer une instance de EOBac avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBac createEOBac(EOEditingContext editingContext, String bacCode
, String bacType
			) {
    EOBac eo = (EOBac) createAndInsertInstance(editingContext, _EOBac.ENTITY_NAME);    
		eo.setBacCode(bacCode);
		eo.setBacType(bacType);
    return eo;
  }

  
	  public EOBac localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBac)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBac creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBac creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOBac object = (EOBac)createAndInsertInstance(editingContext, _EOBac.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOBac localInstanceIn(EOEditingContext editingContext, EOBac eo) {
    EOBac localInstance = (eo == null) ? null : (EOBac)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBac#localInstanceIn a la place.
   */
	public static EOBac localInstanceOf(EOEditingContext editingContext, EOBac eo) {
		return EOBac.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOBac>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBac fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBac fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOBac> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBac)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOBac> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBac)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBac fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBac eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBac ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBac fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
