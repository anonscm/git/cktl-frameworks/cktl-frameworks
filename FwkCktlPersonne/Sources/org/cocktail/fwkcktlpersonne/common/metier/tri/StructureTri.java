package org.cocktail.fwkcktlpersonne.common.metier.tri;

import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.google.common.collect.Ordering;

/**
 * Responsabilité de la classe
 * - Trier les structures
 */
public class StructureTri {
	
	private Ordering<IStructure> comparateurLibelleAffichageAsc = new Ordering<IStructure>() {
		@Override
		public int compare(IStructure structure1, IStructure structure2) {
			if (structure1.getNomCompletAffichage() != null && structure2.getNomCompletAffichage() != null) {
				return Ordering.natural().compare(structure1.getNomCompletAffichage(), structure2.getNomCompletAffichage());
			} else {
				return 0;
			}
		}
	};
	
	private Ordering<IStructure> comparateurLibelleCourtAsc = new Ordering<IStructure>() {
		@Override
		public int compare(IStructure structure1, IStructure structure2) {
			if (structure1.lcStructure() != null && structure2.lcStructure() != null) {
				return Ordering.natural().compare(structure1.lcStructure(), structure2.lcStructure());
			} else {
				return 0;
			}
		}
	};
	
	private Ordering<IStructure> comparateurLibelleLongAsc = new Ordering<IStructure>() {
		@Override
		public int compare(IStructure structure1, IStructure structure2) {
			if (structure1.llStructure() != null && structure2.llStructure() != null) {
				return Ordering.natural().compare(structure1.llStructure(), structure2.llStructure());
			} else {
				return 0;
			}
		}
	};
	
	private Ordering<IStructure> comparateurLibelleLongPuisParentAsc = new Ordering<IStructure>() {
		@Override
		public int compare(IStructure structure1, IStructure structure2) {
			if (structure1.llStructure() != null && structure2.llStructure() != null) {
				int comp = Ordering.natural().compare(structure1.llStructure(), structure2.llStructure());
				if (comp == 0) {
					return Ordering.natural().compare(structure1.toStructurePere().llStructure(), structure2.toStructurePere().llStructure());
				} else {
					return comp;
				}
			}
			return 0;
		}
	};
	
	/**
	 * liste de structure à trier par libellé d'affichage
	 * @param listeStructure : liste de structure à trier
	 */
	public void trierParLibelleAffichage(List<IStructure> listeStructure) {
		Collections.sort(listeStructure, comparateurLibelleAffichageAsc);
	}
	
	/**
	 * liste de structure à trier par libellé long
	 * @param listeStructure : liste de structure à trier
	 */
	public void trierParLibelleLong(List<IStructure> listeStructure) {
		Collections.sort(listeStructure, comparateurLibelleLongAsc);
	}
	
	/**
	 * liste de structure à trier par libellé court
	 * @param listeStructure : liste de structure à trier
	 */
	public void trierParLibelleCourt(List<IStructure> listeStructure) {
		Collections.sort(listeStructure, comparateurLibelleCourtAsc);
	}
	
	/**
	 * liste de structure à trier par libellé long
	 * puis par libellé des parents
	 * @param listeStructure : liste de structure à trier
	 */
	public void trierParLibelleLongPuisParentAsc(List<IStructure> listeStructure) {
		Collections.sort(listeStructure, comparateurLibelleLongPuisParentAsc);
	}
}
