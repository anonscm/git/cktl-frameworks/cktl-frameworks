package org.cocktail.fwkcktlpersonne.common.metier.tri;

import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.google.common.collect.Ordering;


/**
 * Responsabilité de la classe
 * - Trier les personnes
 */
public class PersonneTri {

	
	
	private Ordering<IPersonne> comparateurLibelleAffichageAsc = new Ordering<IPersonne>() {
		@Override
		public int compare(IPersonne personne1, IPersonne personne2) {
			if (personne1.getNomCompletAffichage() != null && personne2.getNomCompletAffichage() != null) {
				return Ordering.natural().compare(personne1.getNomCompletAffichage(), personne2.getNomCompletAffichage());
			} else {
				return 0;
			}
		}
	};
	
	
	/**
	 * liste de personnes à trier par libellé d'affichage
	 * @param listePersonne : liste de personne à trier
	 */
	public void trierParLibelleAffichage(List<IPersonne> listePersonne) {
		Collections.sort(listePersonne, comparateurLibelleAffichageAsc);
	}
	
	
}
