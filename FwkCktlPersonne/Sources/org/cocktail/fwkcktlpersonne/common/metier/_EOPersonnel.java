/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersonnel.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOPersonnel extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOPersonnel.class);

	public static final String ENTITY_NAME = "Fwkpers_Personnel";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PERSONNEL_ULR";


// Attribute Keys
  public static final ERXKey<String> AFFECTE_DEFENSE = new ERXKey<String>("affecteDefense");
  public static final ERXKey<NSTimestamp> CIR_D_CERTIFICATION = new ERXKey<NSTimestamp>("cirDCertification");
  public static final ERXKey<NSTimestamp> CIR_D_COMPLETUDE = new ERXKey<NSTimestamp>("cirDCompletude");
  public static final ERXKey<NSTimestamp> CIR_D_VERIFICATION = new ERXKey<NSTimestamp>("cirDVerification");
  public static final ERXKey<String> C_LOGE = new ERXKey<String>("cLoge");
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_SITUATION_FAMILLE = new ERXKey<NSTimestamp>("dSituationFamille");
  public static final ERXKey<Integer> ID_MINISTERE = new ERXKey<Integer>("idMinistere");
  public static final ERXKey<Long> NB_ENFANTS = new ERXKey<Long>("nbEnfants");
  public static final ERXKey<Long> NB_ENFANTS_A_CHARGE = new ERXKey<Long>("nbEnfantsACharge");
  public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
  public static final ERXKey<String> NO_EPICEA = new ERXKey<String>("noEpicea");
  public static final ERXKey<Long> NO_INDIVIDU_URGENCE = new ERXKey<Long>("noIndividuUrgence");
  public static final ERXKey<String> NO_MATRICULE = new ERXKey<String>("noMatricule");
  public static final ERXKey<String> NPC = new ERXKey<String>("npc");
  public static final ERXKey<String> NUMEN = new ERXKey<String>("numen");
  public static final ERXKey<String> TEM_BUDGET_ETAT = new ERXKey<String>("temBudgetEtat");
  public static final ERXKey<String> TEM_IMPOSABLE = new ERXKey<String>("temImposable");
  public static final ERXKey<String> TEM_PAIE_SECU = new ERXKey<String>("temPaieSecu");
  public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
  public static final ERXKey<String> TXT_LIBRE = new ERXKey<String>("txtLibre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noDossierPers";

	public static final String AFFECTE_DEFENSE_KEY = "affecteDefense";
	public static final String CIR_D_CERTIFICATION_KEY = "cirDCertification";
	public static final String CIR_D_COMPLETUDE_KEY = "cirDCompletude";
	public static final String CIR_D_VERIFICATION_KEY = "cirDVerification";
	public static final String C_LOGE_KEY = "cLoge";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_SITUATION_FAMILLE_KEY = "dSituationFamille";
	public static final String ID_MINISTERE_KEY = "idMinistere";
	public static final String NB_ENFANTS_KEY = "nbEnfants";
	public static final String NB_ENFANTS_A_CHARGE_KEY = "nbEnfantsACharge";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_EPICEA_KEY = "noEpicea";
	public static final String NO_INDIVIDU_URGENCE_KEY = "noIndividuUrgence";
	public static final String NO_MATRICULE_KEY = "noMatricule";
	public static final String NPC_KEY = "npc";
	public static final String NUMEN_KEY = "numen";
	public static final String TEM_BUDGET_ETAT_KEY = "temBudgetEtat";
	public static final String TEM_IMPOSABLE_KEY = "temImposable";
	public static final String TEM_PAIE_SECU_KEY = "temPaieSecu";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TXT_LIBRE_KEY = "txtLibre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String AFFECTE_DEFENSE_COLKEY = "AFFECTE_DEFENSE";
	public static final String CIR_D_CERTIFICATION_COLKEY = "CIR_D_CERTIFICATION";
	public static final String CIR_D_COMPLETUDE_COLKEY = "CIR_D_COMPLETUDE";
	public static final String CIR_D_VERIFICATION_COLKEY = "CIR_D_VERIFICATION";
	public static final String C_LOGE_COLKEY = "C_LOGE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_SITUATION_FAMILLE_COLKEY = "D_SITUATION_FAMILLE";
	public static final String ID_MINISTERE_COLKEY = "ID_MINISTERE";
	public static final String NB_ENFANTS_COLKEY = "NB_ENFANTS";
	public static final String NB_ENFANTS_A_CHARGE_COLKEY = "NB_ENFANTS_A_CHARGE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_EPICEA_COLKEY = "NO_EPICEA";
	public static final String NO_INDIVIDU_URGENCE_COLKEY = "NO_INDIVIDU_URGENCE";
	public static final String NO_MATRICULE_COLKEY = "NO_MATRICULE";
	public static final String NPC_COLKEY = "NPC";
	public static final String NUMEN_COLKEY = "NUMEN";
	public static final String TEM_BUDGET_ETAT_COLKEY = "tem_Budget_Etat";
	public static final String TEM_IMPOSABLE_COLKEY = "TEM_IMPOSABLE";
	public static final String TEM_PAIE_SECU_COLKEY = "TEM_PAIE_SECU";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TXT_LIBRE_COLKEY = "TXT_LIBRE";



	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";



	// Accessors methods
  public String affecteDefense() {
    return (String) storedValueForKey(AFFECTE_DEFENSE_KEY);
  }

  public void setAffecteDefense(String value) {
    takeStoredValueForKey(value, AFFECTE_DEFENSE_KEY);
  }

  public NSTimestamp cirDCertification() {
    return (NSTimestamp) storedValueForKey(CIR_D_CERTIFICATION_KEY);
  }

  public void setCirDCertification(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_D_CERTIFICATION_KEY);
  }

  public NSTimestamp cirDCompletude() {
    return (NSTimestamp) storedValueForKey(CIR_D_COMPLETUDE_KEY);
  }

  public void setCirDCompletude(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_D_COMPLETUDE_KEY);
  }

  public NSTimestamp cirDVerification() {
    return (NSTimestamp) storedValueForKey(CIR_D_VERIFICATION_KEY);
  }

  public void setCirDVerification(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_D_VERIFICATION_KEY);
  }

  public String cLoge() {
    return (String) storedValueForKey(C_LOGE_KEY);
  }

  public void setCLoge(String value) {
    takeStoredValueForKey(value, C_LOGE_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dSituationFamille() {
    return (NSTimestamp) storedValueForKey(D_SITUATION_FAMILLE_KEY);
  }

  public void setDSituationFamille(NSTimestamp value) {
    takeStoredValueForKey(value, D_SITUATION_FAMILLE_KEY);
  }

  public Integer idMinistere() {
    return (Integer) storedValueForKey(ID_MINISTERE_KEY);
  }

  public void setIdMinistere(Integer value) {
    takeStoredValueForKey(value, ID_MINISTERE_KEY);
  }

  public Long nbEnfants() {
    return (Long) storedValueForKey(NB_ENFANTS_KEY);
  }

  public void setNbEnfants(Long value) {
    takeStoredValueForKey(value, NB_ENFANTS_KEY);
  }

  public Long nbEnfantsACharge() {
    return (Long) storedValueForKey(NB_ENFANTS_A_CHARGE_KEY);
  }

  public void setNbEnfantsACharge(Long value) {
    takeStoredValueForKey(value, NB_ENFANTS_A_CHARGE_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public String noEpicea() {
    return (String) storedValueForKey(NO_EPICEA_KEY);
  }

  public void setNoEpicea(String value) {
    takeStoredValueForKey(value, NO_EPICEA_KEY);
  }

  public Long noIndividuUrgence() {
    return (Long) storedValueForKey(NO_INDIVIDU_URGENCE_KEY);
  }

  public void setNoIndividuUrgence(Long value) {
    takeStoredValueForKey(value, NO_INDIVIDU_URGENCE_KEY);
  }

  public String noMatricule() {
    return (String) storedValueForKey(NO_MATRICULE_KEY);
  }

  public void setNoMatricule(String value) {
    takeStoredValueForKey(value, NO_MATRICULE_KEY);
  }

  public String npc() {
    return (String) storedValueForKey(NPC_KEY);
  }

  public void setNpc(String value) {
    takeStoredValueForKey(value, NPC_KEY);
  }

  public String numen() {
    return (String) storedValueForKey(NUMEN_KEY);
  }

  public void setNumen(String value) {
    takeStoredValueForKey(value, NUMEN_KEY);
  }

  public String temBudgetEtat() {
    return (String) storedValueForKey(TEM_BUDGET_ETAT_KEY);
  }

  public void setTemBudgetEtat(String value) {
    takeStoredValueForKey(value, TEM_BUDGET_ETAT_KEY);
  }

  public String temImposable() {
    return (String) storedValueForKey(TEM_IMPOSABLE_KEY);
  }

  public void setTemImposable(String value) {
    takeStoredValueForKey(value, TEM_IMPOSABLE_KEY);
  }

  public String temPaieSecu() {
    return (String) storedValueForKey(TEM_PAIE_SECU_KEY);
  }

  public void setTemPaieSecu(String value) {
    takeStoredValueForKey(value, TEM_PAIE_SECU_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String txtLibre() {
    return (String) storedValueForKey(TXT_LIBRE_KEY);
  }

  public void setTxtLibre(String value) {
    takeStoredValueForKey(value, TXT_LIBRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  

/**
 * Créer une instance de EOPersonnel avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersonnel createEOPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, String temBudgetEtat
, String temImposable
, String temPaieSecu
, String temTitulaire
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu			) {
    EOPersonnel eo = (EOPersonnel) createAndInsertInstance(editingContext, _EOPersonnel.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemBudgetEtat(temBudgetEtat);
		eo.setTemImposable(temImposable);
		eo.setTemPaieSecu(temPaieSecu);
		eo.setTemTitulaire(temTitulaire);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOPersonnel localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersonnel)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonnel creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonnel creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPersonnel object = (EOPersonnel)createAndInsertInstance(editingContext, _EOPersonnel.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersonnel localInstanceIn(EOEditingContext editingContext, EOPersonnel eo) {
    EOPersonnel localInstance = (eo == null) ? null : (EOPersonnel)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersonnel#localInstanceIn a la place.
   */
	public static EOPersonnel localInstanceOf(EOEditingContext editingContext, EOPersonnel eo) {
		return EOPersonnel.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersonnel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersonnel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersonnel> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonnel)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersonnel> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonnel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersonnel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonnel eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonnel ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersonnel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
