/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORome.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORome extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORome.class);

	public static final String ENTITY_NAME = "Fwkpers_Rome";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ROME";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> ROM_CODE = new ERXKey<String>("romCode");
  public static final ERXKey<NSTimestamp> ROM_D_FERMETURE = new ERXKey<NSTimestamp>("romDFermeture");
  public static final ERXKey<NSTimestamp> ROM_D_OUVERTURE = new ERXKey<NSTimestamp>("romDOuverture");
  public static final ERXKey<Integer> ROM_ID_PERE = new ERXKey<Integer>("romIdPere");
  public static final ERXKey<String> ROM_LIBELLE = new ERXKey<String>("romLibelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome> TO_CHILDREN = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome>("toChildren");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome> TO_FATHER = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORome>("toFather");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "romId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ROM_CODE_KEY = "romCode";
	public static final String ROM_D_FERMETURE_KEY = "romDFermeture";
	public static final String ROM_D_OUVERTURE_KEY = "romDOuverture";
	public static final String ROM_ID_PERE_KEY = "romIdPere";
	public static final String ROM_LIBELLE_KEY = "romLibelle";

// Attributs non visibles
	public static final String ROM_ID_KEY = "romId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ROM_CODE_COLKEY = "ROM_CODE";
	public static final String ROM_D_FERMETURE_COLKEY = "ROM_D_FERMETURE";
	public static final String ROM_D_OUVERTURE_COLKEY = "ROM_D_OUVERTURE";
	public static final String ROM_ID_PERE_COLKEY = "ROM_ID_PERE";
	public static final String ROM_LIBELLE_COLKEY = "ROM_LIBELLE";

	public static final String ROM_ID_COLKEY = "ROM_ID";


	// Relationships
	public static final String TO_CHILDREN_KEY = "toChildren";
	public static final String TO_FATHER_KEY = "toFather";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String romCode() {
    return (String) storedValueForKey(ROM_CODE_KEY);
  }

  public void setRomCode(String value) {
    takeStoredValueForKey(value, ROM_CODE_KEY);
  }

  public NSTimestamp romDFermeture() {
    return (NSTimestamp) storedValueForKey(ROM_D_FERMETURE_KEY);
  }

  public void setRomDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, ROM_D_FERMETURE_KEY);
  }

  public NSTimestamp romDOuverture() {
    return (NSTimestamp) storedValueForKey(ROM_D_OUVERTURE_KEY);
  }

  public void setRomDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, ROM_D_OUVERTURE_KEY);
  }

  public Integer romIdPere() {
    return (Integer) storedValueForKey(ROM_ID_PERE_KEY);
  }

  public void setRomIdPere(Integer value) {
    takeStoredValueForKey(value, ROM_ID_PERE_KEY);
  }

  public String romLibelle() {
    return (String) storedValueForKey(ROM_LIBELLE_KEY);
  }

  public void setRomLibelle(String value) {
    takeStoredValueForKey(value, ROM_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORome toFather() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORome)storedValueForKey(TO_FATHER_KEY);
  }

  public void setToFatherRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORome value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORome oldValue = toFather();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FATHER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FATHER_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> toChildren() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome>)storedValueForKey(TO_CHILDREN_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> toChildren(EOQualifier qualifier) {
    return toChildren(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> toChildren(EOQualifier qualifier, boolean fetch) {
    return toChildren(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> toChildren(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORome.TO_FATHER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORome.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toChildren();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToChildrenRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORome object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_CHILDREN_KEY);
  }

  public void removeFromToChildrenRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORome object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CHILDREN_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORome createToChildrenRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Rome");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_CHILDREN_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORome) eo;
  }

  public void deleteToChildrenRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORome object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CHILDREN_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToChildrenRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORome> objects = toChildren().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToChildrenRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORome avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORome createEORome(EOEditingContext editingContext, NSTimestamp romDFermeture
, NSTimestamp romDOuverture
			) {
    EORome eo = (EORome) createAndInsertInstance(editingContext, _EORome.ENTITY_NAME);    
		eo.setRomDFermeture(romDFermeture);
		eo.setRomDOuverture(romDOuverture);
    return eo;
  }

  
	  public EORome localInstanceIn(EOEditingContext editingContext) {
	  		return (EORome)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORome creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORome creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORome object = (EORome)createAndInsertInstance(editingContext, _EORome.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORome localInstanceIn(EOEditingContext editingContext, EORome eo) {
    EORome localInstance = (eo == null) ? null : (EORome)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORome#localInstanceIn a la place.
   */
	public static EORome localInstanceOf(EOEditingContext editingContext, EORome eo) {
		return EORome.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORome>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORome fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORome fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORome> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORome eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORome)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORome fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORome> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORome eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORome)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORome fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORome eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORome ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORome fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
