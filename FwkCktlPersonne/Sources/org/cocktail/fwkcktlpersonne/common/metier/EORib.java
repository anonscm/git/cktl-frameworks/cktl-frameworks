/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORib.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import java.math.BigInteger;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.controles.ControleBIC;
import org.cocktail.fwkcktlpersonne.common.controles.ControleIban;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * Representation d'un RIB (francais ou etranger).<br/>
 * <br/>
 * <b>Regles metiers :</b>
 * <ul>
 * <li><b>Doublons : </b><br>
 * La création de doublons est possible (rib affecte a deux fournisseurs en meme temps. Par ex. les comptes joints). Ceci dit il faut que
 * l'application implémente un warning avec confirmation. Utilisez pour cela {@link EORib#findDoublonsFr}.<br/>
 * Par contre on bloque la creation d'un rib deja affecte au meme fournisseur. <br/>
 * </li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EORib extends _EORib implements IRib {

	public final static Logger logger = Logger.getLogger(EORib.class);

	public static final String EXCEPTION_NO_COMPTE = "Le n° de compte est obligatoire et doit comporter 11 caracteres.";
	public static final String EXCEPTION_CLE = "La clé est obligatoire et doit comporter 2 caracteres.";
	public static final String EXCEPTION_BANQUE = "La banque est obligatoire.";
	public static final String EXCEPTION_GUICHET = "Le code guichet est obligatoire.";
	public static final String EXCEPTION_FOURNIS = "Le fournisseur est obligatoire.";
	public static final String EXCEPTION_RIB_FORMAT_INVALIDE = "Mauvais format de rib.";
	//public static final String EXCEPTION_IBAN_FORMAT_INVALIDE = "Le code IBAN est invalide.";
	//	public static final String EXCEPTION_IBAN_OBLIGATOIRE = "Le code IBAN est obligatoire.";
	public static final String EXCEPTION_IBAN_OBLIGATOIRE_SEPA = "Le code IBAN est obligatoire pour la saisie d'un rib attaché à une banque de la zone SEPA";
	public static final String EXCEPTION_RIBFR_OBLIGATOIRE = "Le N° de compte et la clé sont obligatoires pour la saisie d'un rib au format FR";
	public static final String EXCEPTION_RIB_OU_IBAN = "Le rib n'est pas complet";
	public static final String EXCEPTION_TITULAIRE = "Le titulaire est obligatoire";
	public static final String EXCEPTION_PAYE = "Ce rib est controlé par la paye, vous ne pouvez pas le modifier";
	public static final String EXCEPTION_BANQUE_OBLIGATOIRE = "La banque est obligatoire";
	
	public static final String CODE_ISO_FRANCE = "FR";

	private static final String WARN_CODEBANQUE_INCOHERENT_BANQUE_KEY = " WARN_CODEBANQUE_INCOHERENT_BANQUE";

	public static final String RIB_VALIDE_OUI = "O";
	public static final String RIB_VALIDE_NON = "N";
	public static final String RIB_VALIDE_ANNULE = "A";
	private static final String TEM_PAYE_UTIL_NON = "N";

	public static EOQualifier QUAL_RIB_VALIDE = new EOKeyValueQualifier(EORib.RIB_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EORib.RIB_VALIDE_OUI);
	public static EOQualifier QUAL_RIB_ANNULE = new EOKeyValueQualifier(EORib.RIB_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EORib.RIB_VALIDE_ANNULE);

	public static EOSortOrdering SORT_DATE_CREATION_DESC = EOSortOrdering.sortOrderingWithKey(EORib.D_CREATION_KEY, EOSortOrdering.CompareDescending);
	public static EOSortOrdering SORT_RIB_VALIDE_DESC = EOSortOrdering.sortOrderingWithKey(EORib.RIB_VALIDE_KEY, EOSortOrdering.CompareDescending);

	/** Cle dans grhumparametres pour recuperer le mod_code par defaut */
	private static final String ANNUAIRE_MODE_PAIEMENT_KEY = "ANNUAIRE_MODE_PAIEMENT";

	private static final String NOCOMPTE_ETRANGER = "99999999999";
	private static final String CLE_ETRANGER = "99";

	private static String modCodeDefaut = null;

	private String domiciliation;

	private ArrayList<String> warnings = null;

	public EORib() {
		super();
	}

	/**
	 * Si le rib existe dans la base de données, on interdit la suppression.
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
		if (!hasTemporaryGlobalID()) {
			throw new NSValidation.ValidationException("Vous ne pouvez pas supprimer un rib deja enregistré dans la base de données. Il faut le rendre ANNULE a la place.");
		}
	}

	/**
	 * Peut etre appele à partir des factories. Verifie la coherence des donnees pour le rib et recherche d'eventuel doublons. <br/>
	 * Règles du 28/09/2011 :
	 * <ul>
	 * <li>Le titulaire est obligatoire</li>
	 * <li>Si le RIB FR est rempli et que l'IBAN est vide, l'IBAN est créé automatiquement</li>
	 * <li>Si l'IBAN est rempli et que le rib FR est vide, le rib FR est créé automatiquement</li>
	 * <li>L'IBAN doit etre saisi</li>
	 * <li>La banque doit etre référencée</li>
	 * <li>Si le RIB FR est rempli et que l'IBAN n'est pas vide, la cohérence entre le RIB FR et l'IBAN est vérifiée</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (logger.isDebugEnabled()) {
			logger.debug("Validation du rib =" + getRibConcatene(), new RuntimeException("Dummy for Stacktrace"));
		}
		trimAllString();
	    setDModification(AUtils.now());
		checkUsers();

		if (!EORib.RIB_VALIDE_ANNULE.equals(ribValide())) {
			//verifier droits de modification / creation
			checkDroitModification();
			if (isRibFRRempli()) {
				if (!isRib9999()) {
					verifierFormatRibFr();
					verifierDoublonsRibFr();
					if (iban() == null) {
						setIban(convertRibFRtoIban());
					}
				}
			}

			//Verifier l'IBAN s'il est rempli
			if (iban() != null && iban().trim().length() > 0) {
				ControleIban.verifierFormatIban(iban());
				verifierDoublonsIban();
				if (iban().startsWith(ControleIban.PREFIX_FR)) {
					//Calculer le RIB FR
					if (!isRibFRRempli()) {
						fillRibFRFromIban();
					}
				}
			}
			checkRibComplet();
			if (isCompteEtIbanEmpty()) {
				throw new NSValidation.ValidationException(EXCEPTION_RIB_OU_IBAN);
			}
			if (toBanque() == null) {
				throw new NSValidation.ValidationException(EXCEPTION_BANQUE_OBLIGATOIRE);
			}
			verifierCoherenceRibFRvsIban();
			super.validateObjectMetier();
		}

	}

	public void fillRibFRFromIban() {
		if (iban() != null && iban().trim().length() > 0 && iban().startsWith(ControleIban.PREFIX_FR)) {
			setCBanque(ControleIban.getCodeBanqueFR(iban()));
			setCGuichet(ControleIban.getCodeGuichetFR(iban()));
			setNoCompte(ControleIban.getNoCompteFR(iban()));
			setCleRib(ControleIban.getCleRibFR(iban()));
		}
	}

	private void verifierCoherenceRibFRvsIban() {
		//		if (iban() != null && toBanque() != null && toBanque().cBanque() != null && toBanque().cGuichet() != null && noCompte() != null && cleRib() != null) {
		if (iban() != null && cBanque() != null && cGuichet() != null && noCompte() != null && cleRib() != null) {
			if (!iban().equals(convertRibFRtoIban())) {
				throw new NSValidation.ValidationException("L'iban saisi n'est pas cohérent avec l'iban calculé à partir du rib ( " + iban() + " / " + convertRibFRtoIban() + ")");
			}
		}

	}

	private void checkDroitModification() {
		if (!hasUtilisateurDroitModification(persIdModification())) {
			throw new NSValidation.ValidationException("Vous n'avez pas les droits de modifier ce rib.");
		}

	}

	private boolean hasUtilisateurDroitModification(Integer persId) {
		PersonneApplicationUser appUser = new PersonneApplicationUser(editingContext(), persId);
		return appUser.hasDroitModificationEORib(this);
	}

	/**
	 * @return true si la partie FR du rib est saisie.
	 */
	public boolean isRibFRRempli() {
		if (toBanque() == null) {
			return false;
		}
		return (!MyStringCtrl.isEmpty(cBanque()) && !MyStringCtrl.isEmpty(cGuichet()) && !MyStringCtrl.isEmpty(noCompte()) && !MyStringCtrl.isEmpty(cleRib()));
		//return (!MyStringCtrl.isEmpty(noCompte()) || !MyStringCtrl.isEmpty(cleRib()));
	}

	/**
	 * @return true si iban et bic sont remplis
	 */
	public boolean isRibIbanRempli() {
		if (toBanque() == null) {
			return false;
		}
		return !(MyStringCtrl.isEmpty(iban()) || MyStringCtrl.isEmpty(toBanque().bic()));
	}

	/**
	 * @return true si le rib est initialise comme Etranger (code banque , guichet, etc. initialise avec des 999).
	 */
	public boolean isRib9999() {
		//		if (toBanque() == null) {
		//			return false;
		//		}
		return (EOBanque.CBANQUE_ETRANGER.equals(cBanque()) && EOBanque.CGUICHET_ETRANGER.equals(cGuichet()) && NOCOMPTE_ETRANGER.equals(noCompte()) && CLE_ETRANGER.equals(cleRib()));
	}
	
	public Boolean isRibValide() {
		return Boolean.valueOf(EORib.RIB_VALIDE_OUI.equals(ribValide()));
	}

	/**
	 * Verifie la coherence du rib dans le cas d'un rib francais.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private final void verifierFormatRibFr() throws NSValidation.ValidationException {
		//LRLogger.debugCaller();
		// verifier les longueurs

		if (noCompte() == null || noCompte().length() == 0) {
			throw new NSValidation.ValidationException(EXCEPTION_NO_COMPTE);
		}
		if (noCompte().length() > 11 || noCompte().length() < 11) {
			throw new NSValidation.ValidationException(EXCEPTION_NO_COMPTE);
		}
		if (cleRib() == null || cleRib().length() == 0) {
			throw new NSValidation.ValidationException(EXCEPTION_CLE);
		}
		if (cleRib().length() > 2 || cleRib().length() < 2) {
			throw new NSValidation.ValidationException(EXCEPTION_CLE);
		}
		if (toBanque() == null) {
			throw new NSValidation.ValidationException(EXCEPTION_BANQUE);
		}
		if (cBanque() == null || (cBanque().length() > 5 || cBanque().length() < 5)) {
			throw new NSValidation.ValidationException(EXCEPTION_BANQUE);
		}
		if (cGuichet() == null || (cGuichet().length() > 5 || cGuichet().length() < 5)) {
			throw new NSValidation.ValidationException(EXCEPTION_BANQUE);
		}

		//S'il s'agit d'un RIB avec des 9999 pas de verif
		if (isRib9999()) {
			//LRLogger.debug("RIB Etranger");
			return;
		}

		String chiffres = "";
		for (int i = 0; i < noCompte().length(); i++) {
			String car = noCompte().substring(i, i + 1);
			if (("A".equals(car)) || ("J".equals(car))) {
				chiffres = chiffres + "1";
			} else if (("B".equals(car)) || ("K".equals(car)) || ("S".equals(car))) {
				chiffres = chiffres + "2";
			} else if (("C".equals(car)) || ("L".equals(car)) || ("T".equals(car))) {
				chiffres = chiffres + "3";
			} else if (("D".equals(car)) || ("M".equals(car)) || ("U".equals(car))) {
				chiffres = chiffres + "4";
			} else if (("E".equals(car)) || ("N".equals(car)) || ("V".equals(car))) {
				chiffres = chiffres + "5";
			} else if (("F".equals(car)) || ("O".equals(car)) || ("W".equals(car))) {
				chiffres = chiffres + "6";
			} else if (("G".equals(car)) || ("P".equals(car)) || ("X".equals(car))) {
				chiffres = chiffres + "7";
			} else if (("H".equals(car)) || ("Q".equals(car)) || ("Y".equals(car))) {
				chiffres = chiffres + "8";
			} else if (("I".equals(car)) || ("R".equals(car)) || ("Z".equals(car))) {
				chiffres = chiffres + "9";
			} else {
				chiffres = chiffres + car;
			}
		}
		String s = cBanque() + cGuichet() + chiffres + cleRib();
		BigInteger bigInt = new BigInteger(s);
		if ((bigInt.mod(new BigInteger("97"))).intValue() != 0) {
			throw new NSValidation.ValidationException(EXCEPTION_RIB_FORMAT_INVALIDE);
		}
		//LRLogger.debug("Controle Rib Ok");
	}

	public String convertRibFRtoIban() {
		//		if (toBanque() == null) {
		//			return null;
		//		}
		if (cBanque() != null && cGuichet() != null && noCompte() != null && cleRib() != null) {
			return ControleIban.convertRibFRtoIban(cBanque(), cGuichet(), this.noCompte(), this.cleRib());
		}
		return null;
	}

	/**
	 * Verifie les doublons FR sur le meme fournisseur.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private final void verifierDoublonsRibFr() throws NSValidation.ValidationException {
		//EOEditingContext ec = this.editingContext();
		if (isRib9999()) {
			//LRLogger.debug("RIB Etranger");
			return;
		}
		//NSArray res;
		try {
			if (cBanque() != null) {
				//verifier doublons sur le meme fournisseur
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORib.C_BANQUE_KEY + "=%@ and " + EORib.C_GUICHET_KEY + "=%@ and " + EORib.NO_COMPTE_KEY + "=%@ and " + EORib.CLE_RIB_KEY + "=%@ and " + EORib.RIB_VALIDE_KEY + "<>%@", new NSArray<Object>(
						new Object[] {
								cBanque(), cGuichet(), noCompte(), cleRib(), EORib.RIB_VALIDE_ANNULE
						}));

				if (toFournis().toRibs(qual).count() > 1) {
					throw new NSValidation.ValidationException("Ce rib est déjà affecté au même fournisseur.");
				}

			}

			// 
			//			res = findDoublonsFr(ec, this);
			//			if (res.count() > 0) {
			//				EORib ribDoublon = (EORib) res.objectAtIndex(0);
			//				throw new NSValidation.ValidationException("Le rib " + this.getRibConcatene() + " existe deja et est affecte au fournisseur " + ribDoublon.toFournis().getNomComplet());
			//			}
		} catch (NSValidation.ValidationException e) {

			throw e;
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new NSValidation.ValidationException(e1.getMessage());
		}
	}

	/**
	 * Verifie les doublons sur le meme fournisseur a partir de l'IBAN.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private final void verifierDoublonsIban() throws NSValidation.ValidationException {
		try {
			if (iban() != null) {
				EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORib.IBAN_KEY + "=%@ and " + EORib.RIB_VALIDE_KEY + "<>%@ ", new NSArray<Object>(new Object[] {
						iban(), EORib.RIB_VALIDE_ANNULE
				}));

				if (toFournis().toRibs(qual).count() > 1) {
					throw new NSValidation.ValidationException("Ce code IBAN est déjà affecté au même fournisseur.");
				}
			}
		} catch (Exception e) {
			throw new NSValidation.ValidationException(e.getMessage());
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (isRibControleParPaye()) {
			throw new NSValidation.ValidationException(EXCEPTION_PAYE);
		}
		super.validateBeforeTransactionSave();
	}

	/**
	 * @return Le rib sous forme affichable d'un seul bloc (code banque + guichet + numero compte + cle)
	 */
	public final String getRibConcatene() {
		if (!(MyStringCtrl.isEmpty(iban()))) {
			return this.iban();
		}

		if (!isRib9999()) {
			if (isRibFRRempli()) {
				if (!(cBanque() == null || cGuichet() == null || MyStringCtrl.isEmpty(noCompte()))) {
					return cBanque().concat(" ").concat(cGuichet()).concat(" ").concat(this.noCompte()).concat(" ").concat(this.cleRib());
				}
			}
		}
		return "RIB incomplet";
	}

	public final String getRibConcateneMasque() {
		if (!(MyStringCtrl.isEmpty(iban()))) {
			return this.ibanMasque();
		}
		if (!isRib9999()) {
			if (this.toBanque() != null) {
				if (!(cBanque() == null || cGuichet() == null || MyStringCtrl.isEmpty(noCompteMasque()) || this.cleRib() == null)) {
					return cBanque().concat(" ").concat(cGuichet()).concat(" ").concat(this.noCompteMasque()).concat(" ").concat(this.cleRib());
				}
			}
		}
		return "RIB incomplet";
	}

	public boolean isRibIncomplet() {
		if (toBanque() == null) {
			return false;
		}
		if (ControleBIC.isInZoneSEPA(toBanque().bic(), editingContext()) && !isRibIbanRempli()) {
			return false;
		}
		if (ControleBIC.isInZoneFR(toBanque().bic(), editingContext()) && !isRibFRRempli()) {
			return false;
		}
		return true;
	}

	public void checkRibComplet() throws NSValidation.ValidationException {
		if (toBanque() == null) {
			throw new NSValidation.ValidationException(EXCEPTION_BANQUE_OBLIGATOIRE);
		}

		if (MyStringCtrl.isEmpty(ribTitco())) {
			throw new NSValidation.ValidationException(EXCEPTION_TITULAIRE);
		}

		if (ControleBIC.isInZoneSEPA(toBanque().bic(), editingContext()) && !isRibIbanRempli()) {
			throw new NSValidation.ValidationException(EXCEPTION_IBAN_OBLIGATOIRE_SEPA);
		}
		if (ControleBIC.isInZoneFR(toBanque().bic(), editingContext()) && !isRibFRRempli()) {
			throw new NSValidation.ValidationException(EXCEPTION_RIBFR_OBLIGATOIRE);
		}
	}

	/**
	 * @return Le rib affichable (domiciliation + code banque + guichet + numero compte + cle (ou IBAN))
	 */
	public final String getRibConcateneEtDomiciliation() {
		String dom = MyStringCtrl.ifEmpty(getDomiciliation(), "");
		return (dom.length() > 0 ? dom.substring(0, (dom.length() > 20 ? 20 : dom.length())) : "").
				concat(" ").concat(getRibConcatene());
	}

	public final String getRibConcateneMasqueEtDomiciliation() {
		String dom = MyStringCtrl.ifEmpty(getDomiciliation(), "");
		return (dom.length() > 0 ? dom.substring(0, (dom.length() > 20 ? 20 : dom.length())) : "").
				concat(" ").concat(getRibConcateneMasque());
	}

	/**
	 * @return true si le rib ne contient aucune donnée consistente (à la fois le noCompte et iban sont vides).
	 */
	public boolean isCompteEtIbanEmpty() {
		return (MyStringCtrl.isEmpty(noCompte()) && MyStringCtrl.isEmpty(iban()));
	}

	/**
	 * Recherche des ribs FR valides a partir des parametres indiques.
	 * 
	 * @param ed
	 * @param ribEtab
	 * @param ribGuich
	 * @param ribCompte
	 * @param ribCle
	 * @return La liste des ribs.
	 * @throws Exception
	 */
	public static NSArray<EORib> findRibsFr(EOEditingContext ed, String ribEtab, String ribGuich, String ribCompte, String ribCle) throws Exception {
		if (ribEtab == null || ribEtab.trim().length() == 0) {
			return NSArray.emptyArray();
			//throw new NSValidation.ValidationException(EOBanque.EXCEPTION_BANQUE);
		}
		if (ribGuich == null || ribGuich.trim().length() == 0) {
			return NSArray.emptyArray();
			//throw new NSValidation.ValidationException(EOBanque.EXCEPTION_GUICHET);
		}
		if (ribCompte == null || ribCompte.trim().length() == 0) {
			return NSArray.emptyArray();
			//throw new NSValidation.ValidationException(EORib.EXCEPTION_NO_COMPTE);
		}
		if (ribCle == null || ribCle.trim().length() == 0) {
			return NSArray.emptyArray();
			//throw new NSValidation.ValidationException(EORib.EXCEPTION_CLE);
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORib.C_BANQUE_KEY + "=%@ and " + EORib.C_GUICHET_KEY + "=%@ and " + EORib.NO_COMPTE_KEY + "=%@ and " + EORib.CLE_RIB_KEY + "=%@ and " + EORib.RIB_VALIDE_KEY + "<>%@", new NSArray<Object>(
				new Object[] {
						ribEtab, ribGuich, ribCompte, ribCle, EORib.RIB_VALIDE_ANNULE
				}));
		@SuppressWarnings("unchecked")
		NSArray<EORib> res = AFinder.fetchArray(ed, EORib.ENTITY_NAME, qual, null, true);
		return res;
	}

	/**
	 * @param ed
	 * @param iban
	 * @return Les ribs valides a partir du code IBAN.
	 * @throws Exception
	 */
	public static NSArray<EORib> findRibsIban(EOEditingContext ed, String iban) throws Exception {
		//		if (iban == null || iban.trim().length() == 0) {
		//			throw new NSValidation.ValidationException(EORib.EXCEPTION_IBAN_OBLIGATOIRE);
		//		}

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORib.IBAN_KEY + "=%@ and " + EORib.RIB_VALIDE_KEY + "<>%@", new NSArray<Object>(new Object[] {
				iban, EORib.RIB_VALIDE_ANNULE
		}));
		@SuppressWarnings("unchecked")
		NSArray<EORib> res = AFinder.fetchArray(ed, EORib.ENTITY_NAME, qual, null, true);
		return res;
	}

	/**
	 * Recherche et renvoie les éventuels doublons de rib au format FR (a partir de la liste des ribs valides).
	 * 
	 * @param ed
	 * @param rib
	 * @return Un tableau ne contenant que les doublons de l'objet rib.
	 * @throws Exception
	 */
	public static NSArray<EORib> findDoublonsFr(EOEditingContext ed, EORib rib) throws Exception {

		if (rib == null) {
			throw new NSValidation.ValidationException("Le rib est obligatoire pour chercher des doublons.");
		}

		if (rib.toBanque() == null) {
			return NSArray.emptyArray();
		}

		if (MyStringCtrl.isEmpty(rib.cBanque()) && MyStringCtrl.isEmpty(rib.cGuichet()) && MyStringCtrl.isEmpty(rib.noCompte()) && MyStringCtrl.isEmpty(rib.cleRib())) {
			return NSArray.emptyArray();
		}

		NSArray<EORib> res = findRibsFr(ed, rib.cBanque(), rib.cGuichet(), rib.noCompte(), rib.cleRib());
		NSMutableArray<EORib> res2 = new NSMutableArray<EORib>();
		for (int i = 0; i < res.count(); i++) {
			if (!rib.equals(res.objectAtIndex(i)) && !rib.toFournis().equals(((EORib) res.objectAtIndex(i)).toFournis())) {
				res2.addObject(res.objectAtIndex(i));
			}
		}

		return res2;
	}

	/**
	 * @param ed
	 * @param rib
	 * @return renvoie les éventuels doublons de rib basé sur le code IBAN (a partir de la liste des ribs valides).
	 * @throws Exception
	 */
	public static NSArray<EORib> findDoublonsIban(EOEditingContext ed, EORib rib) throws Exception {
		if (rib == null) {
			throw new NSValidation.ValidationException("Le rib est obligatoire pour chercher des doublons.");
		}
		if (MyStringCtrl.isEmpty(rib.iban())) {
			return NSArray.emptyArray();
		}
		NSArray<EORib> res = findRibsIban(ed, rib.iban());
		NSMutableArray<EORib> res2 = new NSMutableArray<EORib>();
		for (int i = 0; i < res.count(); i++) {
			if (!rib.equals(res.objectAtIndex(i))) {
				res2.addObject(res.objectAtIndex(i));
			}
		}
		return res2;
	}

	/**
	 * @return Le mod_code par defaut en fonction du parametre {@link #ANNUAIRE_MODE_PAIEMENT_KEY}.
	 */
	private String modCodeDefaut() {
		if (modCodeDefaut == null) {
			modCodeDefaut = EOGrhumParametres.parametrePourCle(this.editingContext(), ANNUAIRE_MODE_PAIEMENT_KEY);
			if (modCodeDefaut == null) {
				modCodeDefaut = "00";
			}
		}
		return modCodeDefaut;
	}

	/**
	 * Insertion des valeurs par defaut pour un nouvel objet.
	 * <ul>
	 * <li>ribValide a O</li>
	 * <li>tempPayeUtil a N</li>
	 * <li>modCode a celui par defaut</li>
	 * <li>dCreation a date du jour</li>
	 * <ul>
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setRibValide(RIB_VALIDE_OUI);
		setTemPayeUtil(TEM_PAYE_UTIL_NON);
		setModCode(modCodeDefaut());
		setDCreation(AUtils.now());
	}

	public void setDomiciliation(String domiciliation) {
		this.domiciliation = (domiciliation == null ? domiciliation : MyStringCtrl.chaineSansAccents(domiciliation.trim()).toUpperCase());
	}

	/**
	 * @return La domiciliation. Si null, la domiciliation est recupere a partir de l'eventuelle banque affectee au rib.
	 */
	public String getDomiciliation() {
		if (domiciliation == null) {
			if (toBanque() != null) {
				domiciliation = toBanque().domiciliation();
			}
		}
		return domiciliation;
	}

	/**
	 * Supprime un rib. Si le rib est deja present dans la base, on fait passer sont etat à A (archive). Sinon on le supprime de l'editingContext.
	 * 
	 * @param persId persId de l'utilisateur
	 */
	public void supprimer(Integer persId) {
		if (this.hasTemporaryGlobalID()) {
			setToFournisRelationship(null);
			editingContext().deleteObject(this);
		}
		else {
			setRibValide(EORib.RIB_VALIDE_ANNULE);
			setPersIdModification(persId);
		}
	}

	/**
	 * @return true si le RIB est conrole par la paye (modifications interdites).
	 */
	public boolean isRibControleParPaye() {
		return OUI.equals(temPayeUtil());
	}

	/**
	 * Converti le titulaire en majuscules sans accents.
	 */
	@Override
	public void setRibTitco(String value) {
		if (value != null) {
			value = MyStringCtrl.capitalize(MyStringCtrl.chaineSansAccents(value));
		}
		super.setRibTitco(value);
	}

	/**
	 * Verifie si les utilisateurs affectes a persIdCreation et persIdModification ont bien le droits de creer/Modifier la personne.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkUsers() throws NSValidation.ValidationException {
		if (persIdModification() != null && persIdCreation() == null) {
			setPersIdCreation(persIdModification());
		}
		if (getCreateur() == null) {
			throw new NSValidation.ValidationException("EORib : La reference au createur (persIdCreation) est obligatoire.");
		}
		if (getModificateur() == null) {
			throw new NSValidation.ValidationException("EORib : La référence au modificateur (persIdModification) est obligatoire.");
		}
		if (hasTemporaryGlobalID()) {
			if (!getCreateur().hasDroitCreationEORib(this)) {
				throw new NSValidation.ValidationException("Vous n'avez pas le droit de creer ce RIB.");
			}
		}
		else {
			if (!getModificateur().hasDroitModificationEORib(this)) {
				throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier ce RIB.");
			}
		}
	}

	@Override
	public void setPersIdModification(Integer value) {
		super.setPersIdModification(value);
		if (persIdCreation() == null) {
			setPersIdCreation(value);
		}
	}

	/**
	 * @return le n° de compte avec seuls le debut et la fin, le reste est composé de XXXX.
	 */
	public String noCompteMasque() {
		if (MyStringCtrl.isEmpty(noCompte())) {
			return MyStringCtrl.emptyString();
		}
		String res = "xxxxxxxxxxx";
		if (noCompte().length() == 11) {
			res = noCompte().substring(0, 1) + "xxxxxxx" + noCompte().substring(noCompte().length() - 3, noCompte().length());
		}
		return res;
	}

	public String ibanMasque() {
		if (MyStringCtrl.isEmpty(iban())) {
			return MyStringCtrl.emptyString();
		}
		String res = "xxxxxxxxxxx";
		if (iban().length() >= 20) {
			res = iban().substring(0, 14) + "xxxxxxx" + iban().substring(iban().length() - 5, iban().length());
		}
		return res;
	}

	/**
	 *
	 */
	@Override
	public void setCBanque(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setCBanque(value);
		updateBanqueIfNotSetted();
		checkWarnings();
	}

	@Override
	public void setToBanqueRelationship(EOBanque value) {
		super.setToBanqueRelationship(value);
		checkWarnings();
	}

	/**
	 * 
	 */
	@Override
	public void setCGuichet(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setCGuichet(value);
		updateBanqueIfNotSetted();
		checkWarnings();
	}

	private void updateBanqueIfNotSetted() {
		if (toBanque() == null && cBanque() != null && cGuichet() != null) {
			EOBanque banque = EOBanque.findBanque(editingContext(), cBanque(), cGuichet());
			if (banque != null) {
				setToBanqueRelationship(banque);
			}
		}
	}

	@Override
	public void setNoCompte(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setNoCompte(value);
	}

	@Override
	public void setIban(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setIban(value);
	}

	@Override
	public void setCleRib(String value) {
		if (value != null) {
			value = value.toUpperCase();
		}
		super.setCleRib(value);
	}

	/**
	 * @deprecated modifier la banque directement
	 */
	@Override
	public String bic() {
		return (toBanque() == null ? null : toBanque().bic());
	}
	
	// Le 11/07/12 remise en activité du setBic qui avait été "deprecated" car sinon on ne renseignait plus le BIC
	// de la table RIBFOUR_ULR. Ensuite, j'ai ôté le commentaire sur le super et j'ai enlevé la ligne (return;)
	@Override
	public void setBic(String value) {
		super.setBic(value);
	}

	public boolean isRibFRIncoherentAvecBanque() {
		if (cBanque() != null && cGuichet() != null) {
			if (toBanque() != null) {
				if (!cBanque().equals(toBanque().cBanque()) || !cGuichet().equals(toBanque().cGuichet())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isRibFR() {
		if(toBanque() != null) {
			ControleBIC bic = new ControleBIC(toBanque().bic());
			return CODE_ISO_FRANCE.equals(bic.getCodePays());
		}
		return false;
	}

	public void checkWarnings() {
		if (warnings == null) {
			warnings = new ArrayList<String>();
		}
		warnings.clear();
		if(isRibFR()) {
			if (isRibFRIncoherentAvecBanque()) {
				warnings.add("Les codes banque / guichet saisis (" + cBanque() + " / " + cGuichet() + ") ne correspondent pas aux codes banque / guichet de la banque sélectionnée (" + toBanque().cBanque() + " / " + toBanque().cGuichet() + "). Le rib est peut être invalide.");
			}
		}

		if (toBanque() != null && toBanque().isClosed()) {
			warnings.add("La banque affectée au rib (" + toBanque().libelle() + ") est fermée, le rib est peut-être obsolète.");
		}

		if (toBanque() != null && toBanque().isLocal()) {
			warnings.add("La banque affectée au rib (" + toBanque().libelle() + ") a été créée dans votre établissement et ne provient pas du fichier de référence de la Banque de France, le rib est peut-être invalide.");
		}

	}

	public ArrayList<String> getWarnings() {
		if (warnings == null) {
			warnings = new ArrayList<String>();
			checkWarnings();
		}
		return warnings;
	}

    public Integer ribOrdre() {
        return Integer.valueOf(primaryKey());
    }
}
