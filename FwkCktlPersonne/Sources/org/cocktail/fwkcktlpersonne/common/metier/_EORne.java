/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORne.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EORne extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EORne.class);

	public static final String ENTITY_NAME = "Fwkpers_Rne";
	public static final String ENTITY_TABLE_NAME = "GRHUM.RNE";


// Attribute Keys
  public static final ERXKey<String> ACAD_CODE = new ERXKey<String>("acadCode");
  public static final ERXKey<String> ADRESSE = new ERXKey<String>("adresse");
  public static final ERXKey<Integer> ADR_ORDRE = new ERXKey<Integer>("adrOrdre");
  public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
  public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
  public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> ETAB_ENQUETE = new ERXKey<String>("etabEnquete");
  public static final ERXKey<String> ETAB_STATUT = new ERXKey<String>("etabStatut");
  public static final ERXKey<String> LC_RNE = new ERXKey<String>("lcRne");
  public static final ERXKey<String> LL_RNE = new ERXKey<String>("llRne");
  public static final ERXKey<String> TETAB_CODE = new ERXKey<String>("tetabCode");
  public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie> TO_ACADEMIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie>("toAcademie");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresse");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRnePere");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNES_FILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRnesFils");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cRne";

	public static final String ACAD_CODE_KEY = "acadCode";
	public static final String ADRESSE_KEY = "adresse";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String C_RNE_KEY = "cRne";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETAB_ENQUETE_KEY = "etabEnquete";
	public static final String ETAB_STATUT_KEY = "etabStatut";
	public static final String LC_RNE_KEY = "lcRne";
	public static final String LL_RNE_KEY = "llRne";
	public static final String TETAB_CODE_KEY = "tetabCode";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String C_RNE_PERE_KEY = "cRnePere";

//Colonnes dans la base de donnees
	public static final String ACAD_CODE_COLKEY = "ACAD_CODE";
	public static final String ADRESSE_COLKEY = "ADRESSE";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETAB_ENQUETE_COLKEY = "ETAB_ENQUETE";
	public static final String ETAB_STATUT_COLKEY = "ETAB_STATUT";
	public static final String LC_RNE_COLKEY = "LC_RNE";
	public static final String LL_RNE_COLKEY = "LL_RNE";
	public static final String TETAB_CODE_COLKEY = "TETAB_CODE";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String C_RNE_PERE_COLKEY = "C_RNE_PERE";


	// Relationships
	public static final String TO_ACADEMIE_KEY = "toAcademie";
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_RNE_PERE_KEY = "toRnePere";
	public static final String TO_RNES_FILS_KEY = "toRnesFils";



	// Accessors methods
  public String acadCode() {
    return (String) storedValueForKey(ACAD_CODE_KEY);
  }

  public void setAcadCode(String value) {
    takeStoredValueForKey(value, ACAD_CODE_KEY);
  }

  public String adresse() {
    return (String) storedValueForKey(ADRESSE_KEY);
  }

  public void setAdresse(String value) {
    takeStoredValueForKey(value, ADRESSE_KEY);
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey(ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Integer value) {
    takeStoredValueForKey(value, ADR_ORDRE_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public String cRne() {
    return (String) storedValueForKey(C_RNE_KEY);
  }

  public void setCRne(String value) {
    takeStoredValueForKey(value, C_RNE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String etabEnquete() {
    return (String) storedValueForKey(ETAB_ENQUETE_KEY);
  }

  public void setEtabEnquete(String value) {
    takeStoredValueForKey(value, ETAB_ENQUETE_KEY);
  }

  public String etabStatut() {
    return (String) storedValueForKey(ETAB_STATUT_KEY);
  }

  public void setEtabStatut(String value) {
    takeStoredValueForKey(value, ETAB_STATUT_KEY);
  }

  public String lcRne() {
    return (String) storedValueForKey(LC_RNE_KEY);
  }

  public void setLcRne(String value) {
    takeStoredValueForKey(value, LC_RNE_KEY);
  }

  public String llRne() {
    return (String) storedValueForKey(LL_RNE_KEY);
  }

  public void setLlRne(String value) {
    takeStoredValueForKey(value, LL_RNE_KEY);
  }

  public String tetabCode() {
    return (String) storedValueForKey(TETAB_CODE_KEY);
  }

  public void setTetabCode(String value) {
    takeStoredValueForKey(value, TETAB_CODE_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAcademie toAcademie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAcademie)storedValueForKey(TO_ACADEMIE_KEY);
  }

  public void setToAcademieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAcademie value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAcademie oldValue = toAcademie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACADEMIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACADEMIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey(TO_ADRESSE_KEY);
  }

  public void setToAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRnePere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_PERE_KEY);
  }

  public void setToRnePereRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRnePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_PERE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> toRnesFils() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne>)storedValueForKey(TO_RNES_FILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> toRnesFils(EOQualifier qualifier) {
    return toRnesFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> toRnesFils(EOQualifier qualifier, boolean fetch) {
    return toRnesFils(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> toRnesFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORne.TO_RNE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRnesFils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRnesFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RNES_FILS_KEY);
  }

  public void removeFromToRnesFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RNES_FILS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORne createToRnesFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Rne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RNES_FILS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne) eo;
  }

  public void deleteToRnesFilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RNES_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRnesFilsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORne> objects = toRnesFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRnesFilsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORne avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORne createEORne(EOEditingContext editingContext, String cRne
, NSTimestamp dCreation
, NSTimestamp dModification
, String etabStatut
			) {
    EORne eo = (EORne) createAndInsertInstance(editingContext, _EORne.ENTITY_NAME);    
		eo.setCRne(cRne);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEtabStatut(etabStatut);
    return eo;
  }

  
	  public EORne localInstanceIn(EOEditingContext editingContext) {
	  		return (EORne)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORne creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORne creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORne object = (EORne)createAndInsertInstance(editingContext, _EORne.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORne localInstanceIn(EOEditingContext editingContext, EORne eo) {
    EORne localInstance = (eo == null) ? null : (EORne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORne#localInstanceIn a la place.
   */
	public static EORne localInstanceOf(EOEditingContext editingContext, EORne eo) {
		return EORne.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORne>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORne> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORne> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
