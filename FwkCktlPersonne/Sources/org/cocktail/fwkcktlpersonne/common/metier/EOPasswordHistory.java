/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

/**
 * Classe permettant de stocker les mots de passe utilisés pour chaque compte,
 * afin d'empêcher l'utilisateur de les ré-utiliser. <br>
 * On stocke (entre autres) le mot de passe crypté, ainsi que le cryptage
 * utilisé.
 * 
 * @author jblandin
 * 
 */
public class EOPasswordHistory extends _EOPasswordHistory {

	public static final String SEQ_PASSWORD_HISTORY_ENTITY_NAME = "Fwkpers_SeqPasswordHistory";

    public EOPasswordHistory() {
        super();
    }
    
	/**
	 * Peut etre appele à partir des factories.<br>
	 * Contrôle :
	 * <ul>
	 * <li>Cohérence de la date de création et la date de fin de validité</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
    public void validateObjectMetier() throws NSValidation.ValidationException {
		try {
			checkContraintesObligatoires();
			checkContraintesLongueursMax();
			super.validateObjectMetier();
		} catch (NSValidation.ValidationException e1) {
			throw e1;

		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage());
		}
    }

	/**
	 * Initialisation
	 * <ul>
	 * <li>Clé primaire</li>
	 * <li>dCreation</li>
	 * </ul>
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setPwdhOrdre(contruirePwdhOrdre());
		setDCreation(MyDateCtrl.now());
	}

	private Integer contruirePwdhOrdre() {
		return AFinder.clePrimairePour(editingContext(), EOPasswordHistory.ENTITY_NAME, EOPasswordHistory.PWDH_ORDRE_KEY, SEQ_PASSWORD_HISTORY_ENTITY_NAME, true);
	}
}
