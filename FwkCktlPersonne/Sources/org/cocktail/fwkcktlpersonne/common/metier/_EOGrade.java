/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrade.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGrade extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGrade.class);

	public static final String ENTITY_NAME = "Fwkpers_Grade";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GRADE";


// Attribute Keys
  public static final ERXKey<String> C_CATEGORIE = new ERXKey<String>("cCategorie");
  public static final ERXKey<String> C_GRADE = new ERXKey<String>("cGrade");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_FERMETURE = new ERXKey<NSTimestamp>("dFermeture");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_OUVERTURE = new ERXKey<NSTimestamp>("dOuverture");
  public static final ERXKey<String> ECHELLE = new ERXKey<String>("echelle");
  public static final ERXKey<Integer> IRM_GRADE = new ERXKey<Integer>("irmGrade");
  public static final ERXKey<String> LC_GRADE = new ERXKey<String>("lcGrade");
  public static final ERXKey<String> LL_GRADE = new ERXKey<String>("llGrade");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie> TO_CATEGORIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCategorie>("toCategorie");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> TO_PASSAGE_ECHELONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon>("toPassageEchelons");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cGrade";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String ECHELLE_KEY = "echelle";
	public static final String IRM_GRADE_KEY = "irmGrade";
	public static final String LC_GRADE_KEY = "lcGrade";
	public static final String LL_GRADE_KEY = "llGrade";

// Attributs non visibles
	public static final String C_CORPS_KEY = "cCorps";
	public static final String TEMOIN_NOTE_KEY = "temoinNote";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String ECHELLE_COLKEY = "ECHELLE";
	public static final String IRM_GRADE_COLKEY = "IRM_GRADE";
	public static final String LC_GRADE_COLKEY = "LC_GRADE";
	public static final String LL_GRADE_COLKEY = "LL_GRADE";

	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String TEMOIN_NOTE_COLKEY = "TEMOIN_NOTE";


	// Relationships
	public static final String TO_CATEGORIE_KEY = "toCategorie";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_PASSAGE_ECHELONS_KEY = "toPassageEchelons";



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cGrade() {
    return (String) storedValueForKey(C_GRADE_KEY);
  }

  public void setCGrade(String value) {
    takeStoredValueForKey(value, C_GRADE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
  }

  public void setDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_KEY);
  }

  public String echelle() {
    return (String) storedValueForKey(ECHELLE_KEY);
  }

  public void setEchelle(String value) {
    takeStoredValueForKey(value, ECHELLE_KEY);
  }

  public Integer irmGrade() {
    return (Integer) storedValueForKey(IRM_GRADE_KEY);
  }

  public void setIrmGrade(Integer value) {
    takeStoredValueForKey(value, IRM_GRADE_KEY);
  }

  public String lcGrade() {
    return (String) storedValueForKey(LC_GRADE_KEY);
  }

  public void setLcGrade(String value) {
    takeStoredValueForKey(value, LC_GRADE_KEY);
  }

  public String llGrade() {
    return (String) storedValueForKey(LL_GRADE_KEY);
  }

  public void setLlGrade(String value) {
    takeStoredValueForKey(value, LL_GRADE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCategorie toCategorie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCategorie)storedValueForKey(TO_CATEGORIE_KEY);
  }

  public void setToCategorieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCategorie value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey(TO_CORPS_KEY);
  }

  public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> toPassageEchelons() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon>)storedValueForKey(TO_PASSAGE_ECHELONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> toPassageEchelons(EOQualifier qualifier) {
    return toPassageEchelons(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> toPassageEchelons(EOQualifier qualifier, boolean fetch) {
    return toPassageEchelons(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> toPassageEchelons(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon.TO_GRADE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPassageEchelons();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPassageEchelonsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PASSAGE_ECHELONS_KEY);
  }

  public void removeFromToPassageEchelonsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PASSAGE_ECHELONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon createToPassageEchelonsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PassageEchelon");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PASSAGE_ECHELONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon) eo;
  }

  public void deleteToPassageEchelonsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PASSAGE_ECHELONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPassageEchelonsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPassageEchelon> objects = toPassageEchelons().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPassageEchelonsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGrade avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGrade createEOGrade(EOEditingContext editingContext, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcGrade
, String llGrade
, org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps			) {
    EOGrade eo = (EOGrade) createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME);    
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcGrade(lcGrade);
		eo.setLlGrade(llGrade);
    eo.setToCorpsRelationship(toCorps);
    return eo;
  }

  
	  public EOGrade localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGrade)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrade creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrade creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGrade object = (EOGrade)createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGrade localInstanceIn(EOEditingContext editingContext, EOGrade eo) {
    EOGrade localInstance = (eo == null) ? null : (EOGrade)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGrade#localInstanceIn a la place.
   */
	public static EOGrade localInstanceOf(EOEditingContext editingContext, EOGrade eo) {
		return EOGrade.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOGrade>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGrade fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGrade fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGrade> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrade eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrade)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGrade> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrade eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrade)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGrade fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrade eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrade ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrade fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
