/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

//13/07/2010 - rajout du motif ONP
//14/01/2011 - ajout d'une méthode pour évaluer le motif "Retraite"
public class EOMotifDepart extends _EOMotifDepart {

	public static final EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LC_MOTIF_DEPART_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	
	public static final String TYPE_MOTIF_DECES = "DC";
	private static final String TYPE_MOTIF_RETRAITE = "RET";
	public static final String TYPE_TOUTE_POPULATION = "T";

	public String toString() {
		return lcMotifDepart();
	}
	
    public EOMotifDepart() {
        super();
    }
    public boolean metFinACarriere() {
    	return temFinCarriere() != null && temFinCarriere().equals(CocktailConstantes.VRAI);
    }
    public boolean estPourNormalien() {
    	return temEns() != null && (temEns().equals(CocktailConstantes.VRAI) || temEns().equals(TYPE_TOUTE_POPULATION));
    }
    public boolean doitFournirLieu() {
		return temRneLieu() != null && temRneLieu().equals(CocktailConstantes.VRAI);
    }
    public boolean estDeces() {
    	return cMotifDepart().equals(TYPE_MOTIF_DECES);
    }
    // 14/01/2011
    public boolean estRetraite() {
    	return cMotifDepart().equals(TYPE_MOTIF_RETRAITE);
    }

    public boolean estDefinitif() {
		return cMotifDepartOnp() != null;
	}

    // interface RecordAvecLibelle
    public String libelle() {
    	return lcMotifDepart();
    }
 // Méthodes statiques
	public static EOMotifDepart rechercherMotifDeces(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_MOTIF_DEPART_KEY + " = %@", new NSArray(TYPE_MOTIF_DECES));		
		return fetchFirstByQualifier(editingContext, qualifier); 
	}
	public static NSArray rechercherMotifsCir(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_MOTIF_DEPART_ONP_KEY + " != nil", null);		
		return fetchAll(editingContext, qualifier, SORT_ARRAY_LIBELLE_ASC); 
	}
	public static NSArray rechercherMotifsMutation(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_MOTIF_DEPART_ONP_KEY + " = nil", null);		
		return fetchAll(editingContext, qualifier, SORT_ARRAY_LIBELLE_ASC); 
	}

   
}
