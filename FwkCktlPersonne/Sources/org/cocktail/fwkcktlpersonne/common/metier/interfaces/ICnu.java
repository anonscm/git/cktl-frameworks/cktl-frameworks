package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Section Cnu
 * 
 * @author Alexis Tual
 *
 */
public interface ICnu {

    /**
     * @return le code de la section
     */
    String cSectionCnu();
    
    /**
     * @return le code de la sous section
     */
    String cSousSectionCnu();
    
    /**
     * @return le libellé de la section
     */
    String llSectionCnu();
    
}
