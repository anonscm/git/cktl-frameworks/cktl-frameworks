package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  permet de faire le lien entre une personne ou une structure et une adresse
 */
public interface IRepartPersonneAdresse {

	/**
	 * @return l'adresse postale
	 */
	IAdresse toAdresse();
	
	/**
	 * @return true si l'adresse est valide
	 */
	boolean estValide();

	/**
	 * @return true si l'adresse est la principale
	 */
	boolean estAdressePrincipale();
}
