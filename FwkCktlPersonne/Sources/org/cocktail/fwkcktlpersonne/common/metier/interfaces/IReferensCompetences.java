package org.cocktail.fwkcktlpersonne.common.metier.interfaces;


public interface IReferensCompetences {

// METHODES RAJOUTEES
    
    /**
     * 
     * @return une String avec un libellé court
     */
    public String display();
    
    /**
     * affichage des LIBELLE_TAILLE_MAX premiers caracteres
     * @return une String avec une taille adaptée
     */
    public String displayCourt();
    
    /**
     * 
     * @return une String pour l'affichage du libellé long
     */
    public String displayLong();
    
    /**
     * 
     * @return true si la taille de l'intitulé est supérieure à la taille max (fixée ici à 120)
     */
    public boolean libelleTailleMaxDepassee();
    
    /**
     * 
     * @return une String pour l'affichage dans les listes déroulantes
     */
    public String popup();
    
    /**
     * 
     * @return une String pour l'affichage dans les "bulles d'aide"
     */
    public String popupWs();
    
    /**
     * Le composé de la clef primaire
     * @return le nom de l'entité correspondant au Code Emploi
     */
    public String id();
	
}
