package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  Un numéro de téléphone
 */
public interface ITelephone {

	/**
	 * @return  n° de téléphone
	 */
	String noTelephone();

	/**
	 * @param value  n° de téléphone
	 */
	void setNoTelephone(String value);
	
	/**
	 * @return indicatif du pays
	 */
	Integer indicatif();

	/**
	 * @param value indicatif du pays
	 */
	void setIndicatif(Integer value);


	/**
	 * @return identité du créateur de l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value  identité du créateur de l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return  identité du modificateur de l'enregistrement
	 */
	Integer persIdModifcation();

	/**
	 * @param value  identité du modificateur de l'enregistrement
	 */
	void setPersIdModifcation(Integer value);

	/**
	 * @return type de numero de telephone
	 */
	ITypeNoTel toTypeNoTel();

	/**
	 * @param value type de numero de telephone
	 */
	void setToTypeNoTelRelationship(ITypeNoTel value);

	/**
	 * @return type de telephone
	 */
	ITypeTel toTypeTels();

	/**
	 * @param value type de telephone
	 */
	void setToTypeTelsRelationship(ITypeTel value);
	
	/**
	 * @return numéro de télphone formaté avec l'indicatif du pays
	 */
	String getTelephoneFormateAvecIndicatif();

}
