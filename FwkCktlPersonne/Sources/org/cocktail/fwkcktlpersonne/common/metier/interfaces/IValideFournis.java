package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import java.util.Date;

public interface IValideFournis {

	Integer valCreation();

	void setValCreation(Integer value);

	Date valDateCreate();

	void setValDateCreate(Date value);

	Date valDateVal();

	void setValDateVal(Date value);

	Integer valValidation();

	void setValValidation(Integer value);

	IFournis toFournis();

	void setToFournisRelationship(IFournis value);

}