package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import java.util.List;

/**
 * définit le contrat d'une structure
 */
public interface IStructure {

	/**
	 * @return Le code de la structure
	 */
	String cStructure();
	
	/**
	 * @return le libellé court de la structure
	 */
	String lcStructure();

	/**
	 * @return le libellé long de la structure
	 */
	String llStructure();

	/**
	 * @return le nom complet d'affichage
	 */
	String getNomCompletAffichage();
	
	
	/**
	 *  @return on tronque le llStructure a 25 caracteres + lcStructure
	 */
	String displayCourt();
	
	/**
	 * @return code RNE de la structure
	 */
	String cRne();

	/**
	 * @return numéro SIRET de la structure
	 */
	String siret();

	/**
	 * @return liste des adresses associées à cette structure
	 */
	List<? extends IRepartPersonneAdresse> toRepartPersonneAdresses();
	
	/**
	 * @return type de la structure
	 */
	ITypeStructure toTypeStructure();
	
	/**
	 * @return capacité d'accueil
	 */
	Integer grpEffectifs();
	
	/**
	 * @return responsable de la structure
	 */
	IIndividu toResponsable();	
	
	/**
	 * @return liste des secrétaires
	 */
	List<? extends ISecretariat> toSecretariats();
	
	/**
	 * @return mots clés attachés à cette structure
	 */
	String grpMotsClefs();
	
	/**
	 * @return structure parent
	 */
	IStructure  toStructurePere();
	
	/**
	 * Renvoie l'état d'archivage de cette structure. Une structure est considérée comme archivée, si elle ou l'un de ses pères est placé dans
	 * l'orphelinat (le groupe archive) ET si sa date de fermeture est antérieure à la date courante.
	 * 
	 * @return Vrai si cette structure se trouve dans l'orphelinat ET qu'elle est fermée. Faux sinon.
	 */
	boolean isArchivee();
	
	/**
	 * @return Vrai si la structure est la racine de l'établissement
	 */
	boolean isRacine();
}