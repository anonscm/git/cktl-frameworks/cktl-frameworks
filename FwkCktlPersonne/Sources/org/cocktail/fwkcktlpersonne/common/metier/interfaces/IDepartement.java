package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Description d'un département
 */
public interface IDepartement {

	/**
	 * @return le code du départment sur deux caractères
	 */
	String getDepartementFrance();

	/**
	 * @return le code du département sur deux ou trois caractères
	 */
	String getCode();

	/**
	 * @return le libelle et le code concaténés
	 */
	String getLibelleAndCode();
	
	/**
	 * @return le libellé
	 */
	String llDepartement();
	
	/**
	 * @return code du département
	 */
	String cDepartement();
}