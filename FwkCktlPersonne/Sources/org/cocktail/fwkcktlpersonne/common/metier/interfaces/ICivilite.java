package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Description de la civilité 
 */
public interface ICivilite {

    String C_CIVILITE_MONSIEUR = "M.";
    String C_CIVILITE_MADAME = "MME";
    String C_CIVILITE_MADEMOISELLE = "MLLE";

    String SEXE_MASCULIN = "M";
    String SEXE_FEMININ = "F";

	/**
	 * @return le code civilité
	 */
	String getCode();
	
	/**
	 * @return le libellé de la civilité
	 */
	String getLibelle();
	
	/**
	 * Retourne le sexe associé à la civilité
	 * @return 1 pour les hommes, 2 pour les femmes
	 */
	String getSexe();
	
	

}