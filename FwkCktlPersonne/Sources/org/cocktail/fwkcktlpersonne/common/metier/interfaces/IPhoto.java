/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPhotosSav;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;


public interface IPhoto extends EOEnterpriseObject {

	public NSData datasPhoto();

	public void setDatasPhoto(NSData value);

	public NSTimestamp datePrise();

	public void setDatePrise(NSTimestamp value);

	public Integer noIndividu();

	public void setNoIndividu(Integer value);

	public static class IPhotoHelper {

		public static final Logger LOG = Logger.getLogger(IPhotoHelper.class);

		public IPhotoHelper() {
			super();
		}
		

		public static void archiver(EOEditingContext editingContext,
				String nomApplication, NSTimestamp dateArchivage, String login,
				IIndividu unIndividu, IPhoto photoData) {
			
			CktlDataBus databus = ((CktlWebApplication) CktlWebApplication
					.application()).dataBus();
			Integer noIndividu = unIndividu.noIndividu();

			try {
				String exp = "ALTER TRIGGER PHOTO.TRG_PHOTOS_EMPLOYES DISABLE";
				databus.executeSQLQuery(exp, NSArray.EmptyArray);
				
				photoData = unIndividu.toPhoto();
				EOPhotosSav archivePhoto = EOPhotosSav.createEOPhotosSav(
						editingContext, nomApplication, dateArchivage, login,
						noIndividu);
				archivePhoto.setDatasPhoto(unIndividu.toPhoto().datasPhoto());
				unIndividu.setPhoto(null);
				unIndividu.setIndPhoto("N");
				editingContext.saveChanges();
				
				exp = "ALTER TRIGGER PHOTO.TRG_PHOTOS_EMPLOYES ENABLE";
				databus.executeSQLQuery(exp, NSArray.EmptyArray);
			} catch (ValidationException e) {
				// Récupération du message d'erreur de validation pour affichage
				String message = e.getMessage();
				LOG.error(message, e);
//				session().addSimpleErrorMessage("Erreur", message);
			} catch (Throwable t) {
				databus.rollbackTransaction();
				LOG.error(
						"PersonneLegacyWorkAroundObserver.willInsertRepartComptes() : Erreur de traitement",
						t);
			}
		}
	}

}
