package org.cocktail.fwkcktlpersonne.common.metier.interfaces;


public interface ITypePopulation {

	 // ajouts

    /**
     * @return true si c'est une population rattachée aux ATOS
     */
	public boolean isAenes();

	/**
	 * 
	 * @return true si c'est une population rattachée aux ITARF
	 */
	public boolean isItrf();

	/**
	 * 
	 * @return true si c'est une population rattachée aux bibliothèques
	 */
	public boolean isBu();
	
}
