package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Cette interface représente le rôle que joue établissement pour une école doctorale.
 * 
 * @author Pascal MACOUIN
 */
public interface IRoleEtabEcoleDoctorale {

	/**
	 * @return Le code du rôle de l'établissement pour une école doctorale
	 */
	String cRoleEtabEcoleDoctorale();

	/**
	 * @param unCode Un code pour le rôle de l'établissement pour une école doctorale
	 */
	void setCRoleEtabEcoleDoctorale(String unCode);

	/**
	 * @return Le libellé du rôle de l'établissement pour une école doctorale
	 */
	String llRoleEtabEcoleDoctorale();

	/**
	 * @param unLibelle Un libellé pour le rôle de l'établissement pour une école doctorale
	 */
	void setLlRoleEtabEcoleDoctorale(String unLibelle);

}