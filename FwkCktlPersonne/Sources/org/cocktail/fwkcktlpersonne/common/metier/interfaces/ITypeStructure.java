package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  type de structure
 */
public interface ITypeStructure {
	/**
	 * @return code du type de structure
	 */
	String cTypeStructure() ;
	
	/**
	 * @return libelle du type de structure
	 */
	String lTypeStructure();
}
