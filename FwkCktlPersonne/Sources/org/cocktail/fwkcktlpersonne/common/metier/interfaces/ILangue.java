package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Définition d'une langue
 */
public interface ILangue {

	/**
	 * @return libellé de la langue (français, anglais, etc..)
	 */
	String llLangue();
}
