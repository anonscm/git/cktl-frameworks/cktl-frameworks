package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Cette inteerface représente un type de numero de téléphone (fixe, portable,  ...)
 * @author isabelle
 *
 */
public interface ITypeNoTel {
	
	// Accessors methods
	/**
	 *   
	 * @return code du type du numero de telephonne
	 */
	String cTypeNoTel();

	/**
	 * 
	 * @param value code du type du numero de telephonne
	 */
	void setCTypeNoTel(String value);
	  
	/**
	 * 
	 * @return libelle du type du numero de telephonne
	 */
	String lTypeNoTel();
	
	/**
	 * 
	 * @param value libelle du type du numero de telephonne
	 */
	void setLTypeNoTel(String value);

}
