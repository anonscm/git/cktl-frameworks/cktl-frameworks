package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * 
 * Représente un rib.
 * 
 * @author Alexis Tual
 *
 */
public interface IRib {

    /**
     * @return l'identifiant du rib
     */
    Integer ribOrdre();
    
}
