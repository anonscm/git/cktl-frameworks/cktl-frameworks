package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IReferensEmplois {

	 // METHODES RAJOUTEES

	/**
	 * 
	 * @return true si ce code MEN est archivé
	 */
	public boolean isArchive();

	/**
     * Affichage du code MEN avec prise en compte de l'archivage
     * @return une String avec un libellé du code MEN
     */
	public String display();

	/**
     * 
     * @return une String pour l'affichage du libellé long
     */
	public String displayLong();

	public NSArray tosReferensActivites();

	public NSArray tosReferensCompetences();
	
}
