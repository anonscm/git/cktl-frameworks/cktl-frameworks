package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * Représente un fournisseur.
 * 
 * @author Alexis Tual
 * 
 */
public interface IFournis {

	/**
	 * @return le rib valide le plus récent
	 */
	IRib toDernierRib();

	/**
	 * @return les ribs
	 */
	List<? extends IRib> toRibs();

	/**
	 * @param rib le rib
	 */
	void addToToRibsRelationship(IRib rib);

	/**
	 * @return la personne correspondante
	 */
	IPersonne toPersonne();

	/**
	 * @param iban l'iban
	 * @return le rib correspondant à l'iban donné
	 */
	IRib rechercherRib(String iban);

	/**
	 * @return L'individu associe ou null.
	 */
	IIndividu toIIndividu();

	/**
	 * @return la validité du fournisseur
	 */
	IValideFournis toValideFournis();

	/**
	 * @param valideFournis la validité du fournisseur
	 */
	void setToValideFournisRelationship(IValideFournis valideFournis);

	/**
	 * Ajouter un type de fournisseur.
	 * <p>
	 * Si on ajoute le type CLIENT et que le fournisseur est déjà FOURNISSEUR alors il deviendra TIERS.
	 * @param fouType un type de fournisseur à ajouter
	 */
	void ajouterFouType(String fouType);

	/**
	 * @return valeur de FOU_VALIDE ( FOU_VALIDE_OUI = "O";FOU_VALIDE_NON = "N";FOU_VALIDE_ANNULE = "A");
	 */
	String fouValide();

	/**
	 * @return <code> true </code> si le fournisseur est étranger
	 */
	Boolean isEtranger();

	/**
	 * @return <code> true </code> si le fournisseur est un client
	 */
	boolean isTypeClient();

	/**
	 * @return <code> true </code> le fournisseur est de type fournisseur
	 */
	boolean isTypeFournisseur();

	/**
	 * @return <code> true </code> le fournisseur est de type
	 */
	boolean isTypeTiers();

	/**
	 * @return adresse du fournisseur
	 */
	IAdresse toAdresse();

	/**
	 * @return <code> true </code> si le fournisseur est valide
	 */
	boolean isFouValideValide();

	/**
	 * @param persIdForUser persid de l'utilisateur qui réalise l'opération
	 * @return compte associé au fournisseur
	 * @throws Exception exception remontée en cas d'erreur
	 */
	EOCompte associerCompte(Integer persIdForUser) throws Exception;

	/**
	 * @return code du fournisseur
	 */
	String fouCode();
	
	/**
	 * @return validation avant update qui doit appeler la validation métier
	 */
	public void validateForUpdate() throws NSValidation.ValidationException;
	
	/**
	 * @return la liste des RIB valides
	 */
	public List<IRib> toRibsValide();
}
