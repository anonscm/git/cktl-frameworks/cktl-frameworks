package org.cocktail.fwkcktlpersonne.common.metier.interfaces;


public interface IFctSilland {

	// methodes rajoutees

	public String display();

	public boolean isEnseignant();

	public boolean isNonEnseignant();

	public void setIsEnseignant(boolean value);

	public void setIsNonEnseignant(boolean value);
	
}
