package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  Description du baccalauréat
 */
public interface IBac {
	
	/**
	 * @return code du Bac
	 */
	String  bacCode();
	/**
	 * @return code sise du Bac
	 */
	String bacCodeSise();
	String bacType();
	
	String bacLibelle();
}
