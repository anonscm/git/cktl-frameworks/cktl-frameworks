package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Cette interface représente une école doctorale.
 * 
 * @author Pascal MACOUIN
 */
public interface IEcoleDoctorale {
	
	/**
	 * @return Le numéro de l'école doctorale
	 */
	Integer noEcoleDoctorale();
	
	/**
	 * @param unNumero Un numéro pour l'école doctorale
	 */
	void setNoEcoleDoctorale(Integer unNumero);

	/**
	 * @return Le libelle de l'école doctorale
	 */
	String llEcoleDoctorale();

	/**
	 * @param unLibelle Un libellé pour l'école doctorale
	 */
	void setLlEcoleDoctorale(String unLibelle);

}