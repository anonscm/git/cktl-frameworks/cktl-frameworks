package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOAcademie;
import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOMention;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public interface IEtudiant {
	
	public static final String NUMERO_ETUDIANT_KEY = "etudNumero";
	public static final String CODE_INE_KEY = "etudCodeIne";
	
	public EOEditingContext editingContext();
	
	/**
	 * @return L'identifiant technique de l'étudiant
	 */
	Long id();
	
	IIndividu toIndividu();

	String etudCodeIne();

	Integer etudNumero();

	Integer candNumero();

	void setCandNumero(Integer value);

	NSTimestamp dCreation();

	void setDCreation(NSTimestamp value);

	public NSTimestamp dModification();

	public void setDModification(NSTimestamp value);

	public Long etudAnbac();

	public void setEtudAnbac(Long value);

	public Long etudAnnee1inscSup();

	public void setEtudAnnee1inscSup(Long value);

	public Long etudAnnee1inscUlr();

	public void setEtudAnnee1inscUlr(Long value);

	public Long etudAnnee1inscUniv();

	public void setEtudAnnee1inscUniv(Long value);

	public void setEtudCodeIne(String value);

	public void setEtudNumero(Integer value);

	public String etudReimmatriculation();

	public void setEtudReimmatriculation(String value);

	public Long etudSitfam();

	public void setEtudSitfam(Long value);

	public Long etudSnAttestation();

	public void setEtudSnAttestation(Long value);

	public Long etudSnCertification();

	public void setEtudSnCertification(Long value);

	public Long etudSportHn();

	public void setEtudSportHn(Long value);

	public String etudVilleBac();

	public void setEtudVilleBac(String value);

	public String mentCode();

	public void setMentCode(String value);

	public String proCode();

	public void setProCode(String value);

	public String proCode2();

	public void setProCode2(String value);

	public String thebCode();

	public void setThebCode(String value);

	public EOAcademie toAcademie();

	public void setToAcademieRelationship(EOAcademie value);

	public EOBac toBac();

	public void setToBacRelationship(EOBac value);

	public EODepartement toDepartementEtabBac();

	public void setToDepartementEtabBacRelationship(EODepartement value);

	public EODepartement toDepartementParent();

	public void setToDepartementParentRelationship(EODepartement value);


	public void setToIndividuRelationship(EOIndividu value);

	public EOMention toMention();

	public void setToMentionRelationship(EOMention value);

	public EOPays toPaysEtabBac();

	public void setToPaysEtabBacRelationship(EOPays value);

	public EORne toRneCodeBac();

	public void setToRneCodeBacRelationship(EORne value);

	public EORne toRneCodeSup();

	public void setToRneCodeSupRelationship(EORne value);
    
    public boolean hasEnfants();

    public void setSituationFamilialeEtudiante(boolean hasEnfants);
    
		
    
}