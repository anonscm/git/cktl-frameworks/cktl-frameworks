/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire;
import org.cocktail.fwkcktlpersonne.common.metier.ICompte;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;

public interface IIndividu {
	public static final String TEM_VALIDE_O = "O";
	public static final String TEM_VALIDE_N = "N";

	public Date getDtNaissance();
	
	public void setDNaissance(Date dNaissance);

	public Date getDtNaturalisation();

	public String indNoInsee();
	
	void setIndNoInsee(String numero);
	
	public String indNoInseeProv();
	
	void setIndNoInseeProv(String numero);
	
	public boolean hasNoInseeProvisoire();
	
	public void setHasNoInseeProvisoire(boolean inseeProvisoire);
	
	public CodeInsee getCodeInsee();

	public CodeInsee getCodeInseeProv();
	
	public Integer getCleInsee();
	
	public void setCleInsee(Integer clef);

	public Integer getCleInseeProv();
	
	public void setIndCleInseeProv(Integer clef);
	
	public IDepartement getDepartement();

	public IPays getPaysNaissance();

	public ICivilite getCivilite();

	public boolean estHomme();

	public boolean isPersonnel();

	public boolean isEtudiant();

	public String nomAffichage();

	public String prenomAffichage();

	public String nomPatronymique();
	
	public void setNomPatronymique(String nom);

	public void setNomPatronymiqueAffichage(String value);
	
	public String nomUsuel();
	
	public void setNomUsuel(String nom);

	public String prenom();
	
	public void setPrenom(String prenom);

	public String prenom2();

	public void setPrenom2(String value);
	
	public Integer noIndividu();

	public Date dNaissance();

	public String villeDeNaissance();
	
	public void setVilleDeNaissance(String ville);

	public Integer persId();

	public IPays toPaysNaissance();

	public IPays toPaysNationalite();

	public ISituationFamiliale toSituationFamiliale();

	public EOSituationMilitaire toSituationMilitaire();
	
	public IAdresse adresseFacturation();
	
	public IAdresse adresseEtudiant();
	
	public IAdresse adresseParent();
	
	public List<IAdresse> toAdresses();
	
	public List<ITelephone> toTelephones();
	
	public EOPersonneTelephone telephone(String typeNoTel, String typeTel);
	
	public IFournis toFournis();

	public IIndividu localInstanceIn(EOEditingContext editingContext);

	public IPhoto toPhoto();
	
	public IPhoto setPhoto(NSData data);

	public void setIndPhoto(String string);

	public String indPhoto();

	public void setValidatedWhenNested(boolean b);
	
	public void setToCiviliteRelationship(ICivilite civilite);

	public boolean isEtranger();
	
	public void setToPaysNationaliteRelationship(IPays pays);
	
	public void setToPaysNaissanceRelationship(IPays pays);
	
	public IDepartement toDepartement();
	
	public void setToDepartementRelationship(IDepartement departement);
	
	public void setToSituationFamilialeRelationship(ISituationFamiliale situation);
	
	public void supprimerAdresse(IAdresse adresse, ITypeAdresse type);
	
	/**
	 * @return qualite de l'individu, correspond à sa fonction
	 */
	String indQualite();

	/**
	 * @return liste des comptes informatiques de l'individu
	 */
	List<? extends ICompte> toComptes();

	/**
	 * @return le nom et prénom
	 */
	String getNomPrenomAffichage();
	
	String priseCptInsee();
	
}