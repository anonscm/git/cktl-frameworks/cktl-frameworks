package org.cocktail.fwkcktlpersonne.common.metier.interfaces;


public interface IReferensDcp {

	   // methodes rajoutees

    /**
     * Pour l'affichage
     * @return une string avec la lettre du Bap et l'intitulé du DCP
     */
    public String display();
 
    /**
     * Indique si la DCP est une DCP archivee ou non.
     * @return true si le DCP est archivé
     */
    public boolean isArchive();
    
    /**
     * 
     * @return la lettre associé au Bap
     */
    public String lettreBap();
	
}
