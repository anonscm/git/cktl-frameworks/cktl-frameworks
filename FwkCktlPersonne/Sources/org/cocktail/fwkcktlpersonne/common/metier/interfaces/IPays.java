package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Description de pays 
 */
public interface IPays {

	String CODE_PAYS_FRANCE = "100";

	/**
	 * @return true si le pays est celui par defaut.
	 */
	boolean isPaysDefaut();

	/**
	 * @return true si le pays est la France
	 */
	boolean isFrance();

	/**
	 * Retourne le code ISO 3 caractères du Pays
	 * @return code ISO
	 */
	String getCode();
	
	/**
	 * Retourne le code ISO 3 caractères du Pays
	 * @return code ISO
	 */
	String cPays();
	
	/**
	 * @return libelle long du pays
	 */
	String llPays();
	
	

	/**
	 * @return libelle court de la nationalite
	 */
	String lNationalite();
}