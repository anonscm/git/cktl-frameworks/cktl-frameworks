package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Académie
 */
public interface IAcademie {
	/**
	 * @return code de l'académie
	 */
	String cAcademie();

	/**
	 * @param value code de l'académie
	 */
	void setCAcademie(String value);
}
