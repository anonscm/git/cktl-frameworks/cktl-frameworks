package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;

public interface IRepartStructure {


	public void checkUsers();

	public void init(IPersonne personne, EOStructure structureGroupe);

	//    
	//	/** Initialise une repart avec un individu et une structure  */
	//	public void init(EOIndividu individu,EOStructure structureGroupe) {
	//		init(individu.persId(), structureGroupe);	
	//		individu.addToToRepartStructuresRelationship(this);
	//	}
	//	
	//	public void init(EOStructure structure, EOStructure structureGroupe) {
	//		init(structure.persId(), structureGroupe);
	//		structure.addToToRepartStructuresRelationship(this);
	//	}

	public void init(Integer persId, EOStructure structureGroupe);

	/**
	 * Affecte la personne au repartStructure.
	 * 
	 * @param personne
	 */
	public void setToPersonneElt(IPersonne personne);

	public void awakeFromInsertion(EOEditingContext arg0);

	public void setToStructureGroupeRelationship(EOStructure value);

	/**
	 * @return La personne pour la partie "element" de repartStructure.
	 */
	public IPersonne toPersonneElt();


	public void removeFromToRepartAssociationsRelationship(EORepartAssociation object);

	public void addToToRepartAssociationsRelationship(EORepartAssociation object);

	public void nettoieRepartAssociationVides();

	public void fixPersIdCreation(Integer persId);

	public void setPersIdModification(Integer value);

	public void cleanPersonneEltCache();

	public String libelleEtId();

	
	// methodes rajoutees après le passage de Feve sous les Frameworks

	/**
	 * L'individu associé
	 * @return l'individu associé à une structure
	 */
	public EOIndividu toIndividu();

}
