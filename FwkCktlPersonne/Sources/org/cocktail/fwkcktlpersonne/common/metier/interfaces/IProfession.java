package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  Profession
 */
public interface IProfession {

	/**
	 * @return code de la profession
	 */
	String proCode();
}
