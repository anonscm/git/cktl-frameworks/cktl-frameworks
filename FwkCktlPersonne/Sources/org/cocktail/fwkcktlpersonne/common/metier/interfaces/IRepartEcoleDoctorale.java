package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Cette interface représente l'association entre un établissement, une école doctorale et le rôle de l'établissment pour cette école doctorale.
 * 
 * @author Pascal MACOUIN
 */
public interface IRepartEcoleDoctorale {

	/**
	 * @return L'établissement
	 */
	IRne toRne();
	
	/**
	 * @param unEtablissement Un établissement
	 */
	void setToRneRelationship(IRne unEtablissement);
	
	/**
	 * @return L'école doctorale
	 */
	IEcoleDoctorale toEcoleDoctorale();

	/**
	 * @param uneEcoleDoctorale Une école doctorale
	 */
	void setToEcoleDoctoraleRelationship(IEcoleDoctorale uneEcoleDoctorale);

	/**
	 * @return Le rôle de l'établissement pour l'école doctorale
	 */
	IRoleEtabEcoleDoctorale toRoleEtabEcoleDoctorale();

	/**
	 * @param unRole Un rôle de l'établissement pour l'école doctorale
	 */
	void setToRoleEtabEcoleDoctoraleRelationship(IRoleEtabEcoleDoctorale unRole);

}