package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Cette inteerface représente un type de téléphone (privé, professionnel, étudiant, ...)
 * 
 * @author Pascal MACOUIN
 */
public interface ITypeTel {

	/**
	 * @return le code du type de téléphone
	 */
	String cTypeTel();

	/**
	 * Affecte le code du type de téléphone.
	 * @param codeType un code type de téléphone
	 */
	void setCTypeTel(String codeType);

	/**
	 * @return le libellé du type de téléphone
	 */
	String lTypeTel();

	/**
	 * Affecte le libellé du type de téléphone.
	 * @param libelle un libellé
	 */
	void setLTypeTel(String libelle);

}