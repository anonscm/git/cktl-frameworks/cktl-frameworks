package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

public interface ITypeContratTravail {

	   /**
     * Méthode test pour savoir si un agent est vacataire
     * @return true si vacataire
     */
    public boolean estVacataire();
    
    /**
     * 
     * @return true si l'agent à un témoin Rémunération Principale non nul ET qui est à O(ui)
     */
    public boolean estRemunerationPrincipale();
	
}
