package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Relation entre une personne secrétaire et une structure
 */
public interface ISecretariat {
	
	/**
	 * @return individu secrétaire
	 */
	IIndividu toIndividu();
	
	/**
	 * @return structure concernée
	 */
	IStructure toStructure();
}
