package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import com.webobjects.foundation.NSArray;

public interface IFormationPersonnel {

	 // ajouts de méthodes dédiées

	/**
	 * @param edc
	 * @param qualifier
	 * @return Les objets {@link EOFormation} fils de l'objet en cours. on enleve
	 *         la racine elle meme
	 */
	public NSArray getFils();

	/**
	 * 
	 * @return
	 */
	public boolean isFeuille();

	
}
