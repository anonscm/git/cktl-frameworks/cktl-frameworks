package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * 
 * Indicatif de pays
 * 
 * @author Alexis Tual
 *
 */
public interface IPaysIndicatif {

    /**
     * @return le numéro indicatif
     */
    Integer indicatif();
    
}
