package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;

public interface IRepartTypeGroupe {

	public void initAvecStructureEtType(EOStructure structure, String type);

	public void awakeFromInsertion(EOEditingContext ec);

	public void checkUsers();

	public void checkDroitModification(Integer persId);

	public void checkDroitCreation(Integer persId);
	
}
