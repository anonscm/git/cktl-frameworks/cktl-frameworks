package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.EOReferensDcp;

public interface IReferensFp {

	 // METHODES RAJOUTEES

    /**
     * @return le libellé en capitale de l'intitulé FP
     */
	public String display();

	/**
	 * Pour eviter les exceptions
	 * er.extensions.eof.ERXDatabaseContextDelegate$ObjectNotAvailableException:
	 * No org.cocktail.feve.eos.modele.grhum.EOReferensFp found with globalID:
	 * <ReferensFp: [numdcp: 'L', numfp: '01'] >
	 *
	 * @return un objet de type EOREferens DCP
	 */
	public EOReferensDcp toReferensDcp();

	/**
	 * Pour eviter les exceptions
	 * er.extensions.eof.ERXDatabaseContextDelegate$ObjectNotAvailableException:
	 * No org.cocktail.feve.eos.modele.grhum.EOReferensFp found with globalID:
	 * <ReferensFp: [numdcp: 'L', numfp: '01'] >
	 * 
	 * @return une String avec l'intitulé de Referens FP
	 */
	public String intitulFp();
	
}
