package org.cocktail.fwkcktlpersonne.common.metier.interfaces;


/**
 * code RNE d'un établissement (maintenant UAI)
 */
public interface IRne {

	 /**
	 * @return  code RNE (UAI) de l'établissement
	 */
	String cRne();
	
	/**
	 * Est-ce que ce RNE correspond à une université ?
	 * @return <code>true</code> si oui
	 */
	boolean isUneUniversite();

	/**
	 * Est-ce un établissement français à l'étranger ?
	 * @return <code>true</code> si oui
	 */
	boolean isUnEtablissementFrancaisALEtranger();
	
	/**
	 * 
	 * @return le département
	 */
	IDepartement getDepartement();

	/**
	 * 
	 * @return le code postal
	 */
	String codePostal();
	
	/**
	 * 
	 * @return la ville
	 */
	String ville();
	
	/**
	 * 
	 * @return le libelle
	 */
	String llRne();
	
	/**
	 * @return L'académie de l'établissement
	 */
	String acadCode();
	
	/**
	 * @return Le type de l'établissement
	 */
	String tetabCode();
}
