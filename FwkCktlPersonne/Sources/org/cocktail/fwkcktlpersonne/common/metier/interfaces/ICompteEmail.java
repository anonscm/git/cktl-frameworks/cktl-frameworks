package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 *  compte email attache à un compte utilisateur
 */
public interface ICompteEmail {
	
	/**
	 * @return prefixe  adresse email
	 */
	String cemEmail();
	
	/**
	 * @return domaine de l'adresse mail
	 */
	String cemDomaine();
}
