package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;

import com.webobjects.foundation.NSTimestamp;

public interface IAdresse {

    // Accessors methods
    String adrAdresse1();

    void setAdrAdresse1(String value);

    String adrAdresse2();

    void setAdrAdresse2(String value);

    String adrBp();

    void setAdrBp(String value);

    java.math.BigDecimal adrGpsLatitude();

    void setAdrGpsLatitude(java.math.BigDecimal value);

    java.math.BigDecimal adrGpsLongitude();

    void setAdrGpsLongitude(java.math.BigDecimal value);

    String adrListeRouge();

    void setAdrListeRouge(String value);

    Integer adrOrdre();

    void setAdrOrdre(Integer value);

    String adrUrlPere();

    void setAdrUrlPere(String value);

    String bisTer();

    void setBisTer(String value);

    String cImplantation();

    void setCImplantation(String value);

    String codePostal();

    void setCodePostal(String value);

    String cpEtranger();

    void setCpEtranger(String value);

    String cVoie();

    void setCVoie(String value);

    NSTimestamp dCreation();

    void setDCreation(NSTimestamp value);

    NSTimestamp dDebVal();

    void setDDebVal(NSTimestamp value);

    NSTimestamp dFinVal();

    void setDFinVal(NSTimestamp value);

    NSTimestamp dModification();

    void setDModification(NSTimestamp value);

    String localite();

    void setLocalite(String value);

    String nomVoie();

    void setNomVoie(String value);

    String noVoie();

    void setNoVoie(String value);

    Integer persIdCreation();

    void setPersIdCreation(Integer value);

    Integer persIdModification();

    void setPersIdModification(Integer value);

    String temPayeUtil();

    void setTemPayeUtil(String value);

    String ville();

    void setVille(String value);

    IPays toPays();

    void setToPaysRelationship(IPays pays);

    /**
     * Copier les informations depuis une adresse dans une autre.
     * <p>
     * Attention lorsque l'adresse source et destination sont des classes concrètes différentes (EOPreAdresse, EOAdresse) ou (EOAdresse, EOPreAdresse).
     * @param adresseACopier Adresse depuis laquelle copier (source de la copie)
     * @see #copierAdresseTo(IAdresse)
     */
    void copierAdresseFrom(IAdresse adresseACopier);
    
    /**
     * Copier les informations de l'adresse dans une autre.
     * <p>
     * Attention lorsque l'adresse source et destination sont des classes concrètes différentes (EOPreAdresse, EOAdresse) ou (EOAdresse, EOPreAdresse).
     * @param adresseARenseigner Adresse vers laquelle copier (destination de la copie)
     * @see #copierAdresseFrom(IAdresse)
     */
    void copierAdresseTo(IAdresse adresseARenseigner);
    
    String getEmail();
    
    String getEmailDeType(String codeType);
    
    void setEmailDeType(String email, String type);
    
    boolean isAdresseParent();
    
    boolean isAdresseEtudiant();
    
    ITypeAdresse typeAdresse();

	boolean isAdresseInvalide();

	boolean isAdressePerso();
	
	public String getCPCache();
            
}