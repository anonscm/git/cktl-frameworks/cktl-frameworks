package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

/**
 * Définition d'un VLAN
 */
public interface IVlans {

	/**
	 * @return code du VLan
	 */
	String cVlan();
}
