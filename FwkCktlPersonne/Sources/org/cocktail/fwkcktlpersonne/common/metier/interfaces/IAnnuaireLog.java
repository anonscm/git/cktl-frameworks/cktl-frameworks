package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

import com.webobjects.foundation.NSTimestamp;

/**
 * Description de AnnuaireLog 
 */
public interface IAnnuaireLog {

	/**
	 * @return La date de création du log
	 */
	public NSTimestamp dCreation();

	/**
	 * @return Le login de la personne à l'origine de l'action qui génère le log
	 */
	public String login();

	/**
	 * @return L'action qui génère le log
	 */
	public String operation();

	/**
	 * @return Le PersId de la personne à l'origine de l'action qui génère le log
	 */
	public Integer persId();

	/**
	 * 
	 * @param value pour setter la date de création
	 */
	public void setDCreation(NSTimestamp value);

	/**
	 * 
	 * @param value pour setter le login
	 */
	public void setLogin(String value);

	/**
	 * 
	 * @param value pour setter l'opération qui décrit l'opération
	 */
	public void setOperation(String value);

	/**
	 * 
	 * @param value pour setter le PersId de la personne qui a fait l'opération
	 */
	public void setPersId(Integer value);
}
