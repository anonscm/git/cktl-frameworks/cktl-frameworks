package org.cocktail.fwkcktlpersonne.common.metier.interfaces;

public interface IStructureTypesGroupe {
	
	public boolean laStructurePossedeUnTypeGroupeDiplome();
	
	public boolean laStructurePossedeUnTypeGroupeUnix();

}
