/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeContratTravail.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOTypeContratTravail extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOTypeContratTravail.class);

	public static final String ENTITY_NAME = "Fwkpers_TypeContratTravail";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_CONTRAT_TRAVAIL";


// Attribute Keys
  public static final ERXKey<String> C_CATEGORIE = new ERXKey<String>("cCategorie");
  public static final ERXKey<String> CODE_SUP_INFO = new ERXKey<String>("codeSupInfo");
  public static final ERXKey<String> C_TYPE_CONTRAT_TRAV = new ERXKey<String>("cTypeContratTrav");
  public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
  public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
  public static final ERXKey<String> DROIT_CONGES_CONTRAT = new ERXKey<String>("droitCongesContrat");
  public static final ERXKey<Integer> DUREE_INIT_CONTRAT = new ERXKey<Integer>("dureeInitContrat");
  public static final ERXKey<Integer> DUREE_MAX_CONTRAT = new ERXKey<Integer>("dureeMaxContrat");
  public static final ERXKey<String> LC_TYPE_CONTRAT_TRAV = new ERXKey<String>("lcTypeContratTrav");
  public static final ERXKey<String> LL_TYPE_CONTRAT_TRAV = new ERXKey<String>("llTypeContratTrav");
  public static final ERXKey<Integer> PERIODE_REF_RENOUV = new ERXKey<Integer>("periodeRefRenouv");
  public static final ERXKey<Double> POTENTIEL_BRUT = new ERXKey<Double>("potentielBrut");
  public static final ERXKey<String> TEM_AH_CU_AO = new ERXKey<String>("temAhCuAo");
  public static final ERXKey<String> TEM_CAV_AUTOR_TITUL = new ERXKey<String>("temCavAutorTitul");
  public static final ERXKey<String> TEM_CDI = new ERXKey<String>("temCdi");
  public static final ERXKey<String> TEM_DELEGATION = new ERXKey<String>("temDelegation");
  public static final ERXKey<String> TEM_ENSEIGNANT = new ERXKey<String>("temEnseignant");
  public static final ERXKey<String> TEM_ENSEIGNEMENT = new ERXKey<String>("temEnseignement");
  public static final ERXKey<String> TEM_EQUIV_GRADE = new ERXKey<String>("temEquivGrade");
  public static final ERXKey<String> TEM_INVITE_ASSOCIE = new ERXKey<String>("temInviteAssocie");
  public static final ERXKey<String> TEM_PARTIEL = new ERXKey<String>("temPartiel");
  public static final ERXKey<String> TEM_REMUNERATION_PRINCIPALE = new ERXKey<String>("temRemunerationPrincipale");
  public static final ERXKey<String> TEM_RENOUV_CONTRAT = new ERXKey<String>("temRenouvContrat");
  public static final ERXKey<String> TEM_SERVICE_PUBLIC = new ERXKey<String>("temServicePublic");
  public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
  public static final ERXKey<String> TEM_VACATAIRE = new ERXKey<String>("temVacataire");
  public static final ERXKey<String> TEM_VISIBLE = new ERXKey<String>("temVisible");
  // Relationship Keys
  public static final ERXKey<EOGenericRecord> TO_GEST_CONTRAT = new ERXKey<EOGenericRecord>("toGestContrat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cTypeContratTrav";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String CODE_SUP_INFO_KEY = "codeSupInfo";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String DROIT_CONGES_CONTRAT_KEY = "droitCongesContrat";
	public static final String DUREE_INIT_CONTRAT_KEY = "dureeInitContrat";
	public static final String DUREE_MAX_CONTRAT_KEY = "dureeMaxContrat";
	public static final String LC_TYPE_CONTRAT_TRAV_KEY = "lcTypeContratTrav";
	public static final String LL_TYPE_CONTRAT_TRAV_KEY = "llTypeContratTrav";
	public static final String PERIODE_REF_RENOUV_KEY = "periodeRefRenouv";
	public static final String POTENTIEL_BRUT_KEY = "potentielBrut";
	public static final String TEM_AH_CU_AO_KEY = "temAhCuAo";
	public static final String TEM_CAV_AUTOR_TITUL_KEY = "temCavAutorTitul";
	public static final String TEM_CDI_KEY = "temCdi";
	public static final String TEM_DELEGATION_KEY = "temDelegation";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_ENSEIGNEMENT_KEY = "temEnseignement";
	public static final String TEM_EQUIV_GRADE_KEY = "temEquivGrade";
	public static final String TEM_INVITE_ASSOCIE_KEY = "temInviteAssocie";
	public static final String TEM_PARTIEL_KEY = "temPartiel";
	public static final String TEM_REMUNERATION_PRINCIPALE_KEY = "temRemunerationPrincipale";
	public static final String TEM_RENOUV_CONTRAT_KEY = "temRenouvContrat";
	public static final String TEM_SERVICE_PUBLIC_KEY = "temServicePublic";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VACATAIRE_KEY = "temVacataire";
	public static final String TEM_VISIBLE_KEY = "temVisible";

// Attributs non visibles
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HORAIRE_HEBDOMADAIRE_KEY = "horaireHebdomadaire";
	public static final String REF_REGLEMENTAIRE_KEY = "refReglementaire";
	public static final String REGIME_URSSAF_KEY = "regimeUrssaf";
	public static final String TEM_CAV_CTRL169_H_KEY = "temCavCtrl169H";
	public static final String TEM_MISSION_KEY = "temMission";
	public static final String TEM_REMUNERATION_ACCESSOIRE_KEY = "temRemunerationAccessoire";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String CODE_SUP_INFO_COLKEY = "C_SUPINFO";
	public static final String C_TYPE_CONTRAT_TRAV_COLKEY = "C_TYPE_CONTRAT_TRAV";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String DROIT_CONGES_CONTRAT_COLKEY = "DROIT_CONGES_CONTRAT";
	public static final String DUREE_INIT_CONTRAT_COLKEY = "DUREE_INIT_CONTRAT";
	public static final String DUREE_MAX_CONTRAT_COLKEY = "DUREE_MAX_CONTRAT";
	public static final String LC_TYPE_CONTRAT_TRAV_COLKEY = "LC_TYPE_CONTRAT_TRAV";
	public static final String LL_TYPE_CONTRAT_TRAV_COLKEY = "LL_TYPE_CONTRAT_TRAV";
	public static final String PERIODE_REF_RENOUV_COLKEY = "PERIODE_REF_RENOUV";
	public static final String POTENTIEL_BRUT_COLKEY = "POTENTIEL_BRUT";
	public static final String TEM_AH_CU_AO_COLKEY = "TEM_AH_CU_AO";
	public static final String TEM_CAV_AUTOR_TITUL_COLKEY = "TEM_CAV_AUTOR_TITUL";
	public static final String TEM_CDI_COLKEY = "TEM_CDI";
	public static final String TEM_DELEGATION_COLKEY = "TEM_DELEGATION";
	public static final String TEM_ENSEIGNANT_COLKEY = "TEM_ENSEIGNANT";
	public static final String TEM_ENSEIGNEMENT_COLKEY = "TEM_ENSEIGNEMENT";
	public static final String TEM_EQUIV_GRADE_COLKEY = "TEM_EQUIV_GRADE";
	public static final String TEM_INVITE_ASSOCIE_COLKEY = "TEM_INVITE_ASSOCIE";
	public static final String TEM_PARTIEL_COLKEY = "TEM_PARTIEL";
	public static final String TEM_REMUNERATION_PRINCIPALE_COLKEY = "TEM_REMUNERATION_PRINCIPALE";
	public static final String TEM_RENOUV_CONTRAT_COLKEY = "TEM_RENOUV_CONTRAT";
	public static final String TEM_SERVICE_PUBLIC_COLKEY = "TEM_SERVICE_PUBLIC";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VACATAIRE_COLKEY = "TEM_VACATAIRE";
	public static final String TEM_VISIBLE_COLKEY = "TEM_VISIBLE";

	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HORAIRE_HEBDOMADAIRE_COLKEY = "HORAIRE_HEBDOMADAIRE";
	public static final String REF_REGLEMENTAIRE_COLKEY = "REF_REGLEMENTAIRE";
	public static final String REGIME_URSSAF_COLKEY = "REGIME_URSSAF";
	public static final String TEM_CAV_CTRL169_H_COLKEY = "TEM_CAV_CTRL_169H";
	public static final String TEM_MISSION_COLKEY = "TEM_MISSION";
	public static final String TEM_REMUNERATION_ACCESSOIRE_COLKEY = "TEM_REMUNERATION_ACCESSOIRE";


	// Relationships
	public static final String TO_GEST_CONTRAT_KEY = "toGestContrat";



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String codeSupInfo() {
    return (String) storedValueForKey(CODE_SUP_INFO_KEY);
  }

  public void setCodeSupInfo(String value) {
    takeStoredValueForKey(value, CODE_SUP_INFO_KEY);
  }

  public String cTypeContratTrav() {
    return (String) storedValueForKey(C_TYPE_CONTRAT_TRAV_KEY);
  }

  public void setCTypeContratTrav(String value) {
    takeStoredValueForKey(value, C_TYPE_CONTRAT_TRAV_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public String droitCongesContrat() {
    return (String) storedValueForKey(DROIT_CONGES_CONTRAT_KEY);
  }

  public void setDroitCongesContrat(String value) {
    takeStoredValueForKey(value, DROIT_CONGES_CONTRAT_KEY);
  }

  public Integer dureeInitContrat() {
    return (Integer) storedValueForKey(DUREE_INIT_CONTRAT_KEY);
  }

  public void setDureeInitContrat(Integer value) {
    takeStoredValueForKey(value, DUREE_INIT_CONTRAT_KEY);
  }

  public Integer dureeMaxContrat() {
    return (Integer) storedValueForKey(DUREE_MAX_CONTRAT_KEY);
  }

  public void setDureeMaxContrat(Integer value) {
    takeStoredValueForKey(value, DUREE_MAX_CONTRAT_KEY);
  }

  public String lcTypeContratTrav() {
    return (String) storedValueForKey(LC_TYPE_CONTRAT_TRAV_KEY);
  }

  public void setLcTypeContratTrav(String value) {
    takeStoredValueForKey(value, LC_TYPE_CONTRAT_TRAV_KEY);
  }

  public String llTypeContratTrav() {
    return (String) storedValueForKey(LL_TYPE_CONTRAT_TRAV_KEY);
  }

  public void setLlTypeContratTrav(String value) {
    takeStoredValueForKey(value, LL_TYPE_CONTRAT_TRAV_KEY);
  }

  public Integer periodeRefRenouv() {
    return (Integer) storedValueForKey(PERIODE_REF_RENOUV_KEY);
  }

  public void setPeriodeRefRenouv(Integer value) {
    takeStoredValueForKey(value, PERIODE_REF_RENOUV_KEY);
  }

  public Double potentielBrut() {
    return (Double) storedValueForKey(POTENTIEL_BRUT_KEY);
  }

  public void setPotentielBrut(Double value) {
    takeStoredValueForKey(value, POTENTIEL_BRUT_KEY);
  }

  public String temAhCuAo() {
    return (String) storedValueForKey(TEM_AH_CU_AO_KEY);
  }

  public void setTemAhCuAo(String value) {
    takeStoredValueForKey(value, TEM_AH_CU_AO_KEY);
  }

  public String temCavAutorTitul() {
    return (String) storedValueForKey(TEM_CAV_AUTOR_TITUL_KEY);
  }

  public void setTemCavAutorTitul(String value) {
    takeStoredValueForKey(value, TEM_CAV_AUTOR_TITUL_KEY);
  }

  public String temCdi() {
    return (String) storedValueForKey(TEM_CDI_KEY);
  }

  public void setTemCdi(String value) {
    takeStoredValueForKey(value, TEM_CDI_KEY);
  }

  public String temDelegation() {
    return (String) storedValueForKey(TEM_DELEGATION_KEY);
  }

  public void setTemDelegation(String value) {
    takeStoredValueForKey(value, TEM_DELEGATION_KEY);
  }

  public String temEnseignant() {
    return (String) storedValueForKey(TEM_ENSEIGNANT_KEY);
  }

  public void setTemEnseignant(String value) {
    takeStoredValueForKey(value, TEM_ENSEIGNANT_KEY);
  }

  public String temEnseignement() {
    return (String) storedValueForKey(TEM_ENSEIGNEMENT_KEY);
  }

  public void setTemEnseignement(String value) {
    takeStoredValueForKey(value, TEM_ENSEIGNEMENT_KEY);
  }

  public String temEquivGrade() {
    return (String) storedValueForKey(TEM_EQUIV_GRADE_KEY);
  }

  public void setTemEquivGrade(String value) {
    takeStoredValueForKey(value, TEM_EQUIV_GRADE_KEY);
  }

  public String temInviteAssocie() {
    return (String) storedValueForKey(TEM_INVITE_ASSOCIE_KEY);
  }

  public void setTemInviteAssocie(String value) {
    takeStoredValueForKey(value, TEM_INVITE_ASSOCIE_KEY);
  }

  public String temPartiel() {
    return (String) storedValueForKey(TEM_PARTIEL_KEY);
  }

  public void setTemPartiel(String value) {
    takeStoredValueForKey(value, TEM_PARTIEL_KEY);
  }

  public String temRemunerationPrincipale() {
    return (String) storedValueForKey(TEM_REMUNERATION_PRINCIPALE_KEY);
  }

  public void setTemRemunerationPrincipale(String value) {
    takeStoredValueForKey(value, TEM_REMUNERATION_PRINCIPALE_KEY);
  }

  public String temRenouvContrat() {
    return (String) storedValueForKey(TEM_RENOUV_CONTRAT_KEY);
  }

  public void setTemRenouvContrat(String value) {
    takeStoredValueForKey(value, TEM_RENOUV_CONTRAT_KEY);
  }

  public String temServicePublic() {
    return (String) storedValueForKey(TEM_SERVICE_PUBLIC_KEY);
  }

  public void setTemServicePublic(String value) {
    takeStoredValueForKey(value, TEM_SERVICE_PUBLIC_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temVacataire() {
    return (String) storedValueForKey(TEM_VACATAIRE_KEY);
  }

  public void setTemVacataire(String value) {
    takeStoredValueForKey(value, TEM_VACATAIRE_KEY);
  }

  public String temVisible() {
    return (String) storedValueForKey(TEM_VISIBLE_KEY);
  }

  public void setTemVisible(String value) {
    takeStoredValueForKey(value, TEM_VISIBLE_KEY);
  }

  public EOGenericRecord toGestContrat() {
    return (EOGenericRecord)storedValueForKey(TO_GEST_CONTRAT_KEY);
  }

  public void setToGestContratRelationship(EOGenericRecord value) {
    if (value == null) {
    	EOGenericRecord oldValue = toGestContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GEST_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GEST_CONTRAT_KEY);
    }
  }
  

/**
 * Créer une instance de EOTypeContratTravail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeContratTravail createEOTypeContratTravail(EOEditingContext editingContext, String codeSupInfo
, String cTypeContratTrav
, NSTimestamp dFinVal
, String droitCongesContrat
, Double potentielBrut
, String temEnseignement
, String temEquivGrade
, String temRemunerationPrincipale
, EOGenericRecord toGestContrat			) {
    EOTypeContratTravail eo = (EOTypeContratTravail) createAndInsertInstance(editingContext, _EOTypeContratTravail.ENTITY_NAME);    
		eo.setCodeSupInfo(codeSupInfo);
		eo.setCTypeContratTrav(cTypeContratTrav);
		eo.setDFinVal(dFinVal);
		eo.setDroitCongesContrat(droitCongesContrat);
		eo.setPotentielBrut(potentielBrut);
		eo.setTemEnseignement(temEnseignement);
		eo.setTemEquivGrade(temEquivGrade);
		eo.setTemRemunerationPrincipale(temRemunerationPrincipale);
    eo.setToGestContratRelationship(toGestContrat);
    return eo;
  }

  
	  public EOTypeContratTravail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeContratTravail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeContratTravail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeContratTravail creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOTypeContratTravail object = (EOTypeContratTravail)createAndInsertInstance(editingContext, _EOTypeContratTravail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeContratTravail localInstanceIn(EOEditingContext editingContext, EOTypeContratTravail eo) {
    EOTypeContratTravail localInstance = (eo == null) ? null : (EOTypeContratTravail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeContratTravail#localInstanceIn a la place.
   */
	public static EOTypeContratTravail localInstanceOf(EOEditingContext editingContext, EOTypeContratTravail eo) {
		return EOTypeContratTravail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeContratTravail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeContratTravail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeContratTravail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeContratTravail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeContratTravail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeContratTravail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeContratTravail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeContratTravail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeContratTravail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeContratTravail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeContratTravail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeContratTravail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeContratTravail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeContratTravail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
