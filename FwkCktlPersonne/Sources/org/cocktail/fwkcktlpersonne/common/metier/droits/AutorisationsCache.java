/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

/**
 * 
 * Represente le cache des autorisations qu'un utilisateur a sur les fonctions et données
 * d'une "application".
 * Il convient d'implémenter une sous classe par "application". Cette sous classe implémentant les méthodes "en dur"
 * de vérification des droits. <br/>
 * Par exemple : 
 * 
 * <pre>
 *      public boolean hasDroitConsultationContrat() {...}
 * </pre>
 * 
 * Les sous classes sont à utiliser avec <code>ApplicationUser</code>.
 * 
 * @see ApplicationUser
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class AutorisationsCache {

    private NSDictionary<String, NSArray<String>> autorisationsOnDonneeCache;    
    private NSDictionary<String, NSArray<String>> autorisationsOnFonctionCache;
    private NSArray<EOGlobalID> perimetresAutorises;    
    private String appStrId;
    
    private static final String FONCTION_PERIMETRE = "cocktail.feature.perimetre";
    
    public AutorisationsCache(String appStrId, Integer persId) {
        this.appStrId = appStrId;
        refreshCache(appStrId, persId);
    }
    
    private void refreshCache(String appStrId, Integer persId) {
        EOEditingContext ec = ERXEC.newEditingContext();
        this.autorisationsOnDonneeCache = buildAutorizationOnDonneeCache(ec, appStrId, persId);
        this.autorisationsOnFonctionCache = buildAutorizationOnFonctionCache(ec, appStrId, persId);
        if (isFonctionActive(FONCTION_PERIMETRE)) {
        	this.setPerimetresAutorises(buildPerimetresAutorise(ec, appStrId, persId));
        }	
        ec.dispose();
        ec = null;
    }
    
    private NSArray<EOGlobalID> buildPerimetresAutorise(EOEditingContext ec, String appStrId2, Integer persId) {
    	NSArray<EOGlobalID> perimetres = new NSMutableArray<EOGlobalID>();
	    for (EOGdPerimetre perimetre : DroitsHelper.droitsPerimetresForPersonne(ec, appStrId2, persId)) {
	        perimetres.add(perimetre.globalID());
        }
        return perimetres;
    }

	@Deprecated
    private NSDictionary<String, NSArray<String>> buildAutorizationOnDonneeCache(
            EOEditingContext ec, String appStrId, Integer persId) {
        NSArray<EOGdProfilDroitDonnee> droitsDonnee = DroitsHelper.droitsDonneeForPersonne(ec, appStrId, persId);
        NSMutableDictionary<String, NSMutableArray<String>> droitsCache = new NSMutableDictionary<String, NSMutableArray<String>>();
        for (EOGdProfilDroitDonnee droitDonnee : droitsDonnee) {
            String key = droitDonnee.toGdDonnee().donIdInterne();
            NSMutableArray<String> droitsForDonnee = droitsCache.objectForKey(key);
            if (droitsForDonnee == null) {
                droitsForDonnee = new NSMutableArray<String>();
                droitsCache.setObjectForKey(droitsForDonnee, key);
            }
            droitsForDonnee.addObject(droitDonnee.toGdTypeDroitDonnee().tddStrId());
        }
        // Bon c'est un peu sale mais on est obligé de supprimer dans une 2ème boucle les entrés interdites
        NSMutableDictionary<String, NSArray<String>> droitsCacheFinal = new NSMutableDictionary<String, NSArray<String>>();
        for (String currentKey : droitsCache.allKeys()) {
            NSArray<String> droits = droitsCache.objectForKey(currentKey);
            if (!droits.containsObject(EOGdTypeDroitDonnee.STR_ID_N))
                droitsCacheFinal.setObjectForKey(droits, currentKey);
        }
        return droitsCacheFinal;
    }
    
    private NSDictionary<String, NSArray<String>> buildAutorizationOnFonctionCache(
            EOEditingContext ec, String appStrId, Integer persId) {
        NSArray<EOGdProfilDroitFonction> droitsFonction = rechercherDroitsFonctionToCache(
				ec, appStrId, persId);
        NSMutableDictionary<String, NSMutableArray<String>> droitsCache = new NSMutableDictionary<String, NSMutableArray<String>>();
        for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
            String key = droitFonction.toGdFonction().fonIdInterne();
            NSMutableArray<String> droitsForFonction = droitsCache.objectForKey(key);
            if (droitsForFonction == null) {
                droitsForFonction = new NSMutableArray<String>();
                droitsCache.setObjectForKey(droitsForFonction, key);
            }
            droitsForFonction.addObject(droitFonction.toGdTypeDroitFonction().tdfStrId());
        }
        // Bon c'est un peu sale mais on est obligé de supprimer dans une 2ème boucle les entrés interdites
        NSMutableDictionary<String, NSArray<String>> droitsCacheFinal = new NSMutableDictionary<String, NSArray<String>>();
        for (String currentKey : droitsCache.allKeys()) {
            NSArray<String> droits = droitsCache.objectForKey(currentKey);
            if (!droits.containsObject(EOGdTypeDroitFonction.STR_ID_N))
                droitsCacheFinal.setObjectForKey(droits, currentKey);
        }
        return droitsCacheFinal;
    }

    protected NSArray<EOGdProfilDroitFonction> rechercherDroitsFonctionToCache(EOEditingContext ec, String appStrId, Integer persId) {
		return DroitsHelper.droitsFonctionForPersonne(ec, appStrId, persId);
	}
    
    public boolean hasDroitCreationOnDonnee(String strId) {
        NSArray<String> droits = autorisationsOnDonneeCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitDonnee.STR_ID_C);
    }
    
    public boolean hasDroitLectureOnDonnee(String strId) {
        NSArray<String> droits = autorisationsOnDonneeCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitDonnee.STR_ID_R);
    }
    
    public boolean hasDroitModificationOnDonnee(String strId) {
        NSArray<String> droits = autorisationsOnDonneeCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitDonnee.STR_ID_U);
    }
    
    public boolean hasDroitSuppressionOnDonnee(String strId) {
        NSArray<String> droits = autorisationsOnDonneeCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitDonnee.STR_ID_D);
    }
    
    public boolean hasDroitUtilisationOnFonction(String strId) {
        NSArray<String> droits = autorisationsOnFonctionCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitFonction.STR_ID_U);
    }
    
    public boolean hasDroitConnaissanceOnFonction(String strId) {
        NSArray<String> droits = autorisationsOnFonctionCache.objectForKey(strId);
        return droits != null && droits.containsObject(EOGdTypeDroitFonction.STR_ID_K);
    }
    
    public String getAppStrId() {
        return appStrId;
    }

	public NSArray<EOGlobalID> getPerimetresAutorises() {
	    return perimetresAutorises;
    }

	public void setPerimetresAutorises(NSArray<EOGlobalID> perimetresAutorises) {
	    this.perimetresAutorises = perimetresAutorises;
    }
    
	
	private boolean isFonctionActive(String param) {
		// Par défaut fonction active
		return ERXProperties.booleanForKeyWithDefault(param, false);
	}
}
