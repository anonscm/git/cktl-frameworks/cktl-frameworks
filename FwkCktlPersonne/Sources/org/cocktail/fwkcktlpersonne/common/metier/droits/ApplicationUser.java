/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.foundation.ERXThreadStorage;

/**
 * Classe représentant l'utilisateur authentifié d'une application.
 * Il convient d'implémenter une classe par application, et de l'instancier à partir de caches d'autorisation.<br/>
 * Par exemple, pour l'application AGrhum, la classe AGrhumApplicationUser serait instanciée de la sorte : 
 *  <pre>
 *      AGrhumApplicationUser appUser = new AGrhumApplicationUser(
 *                                          new FwkPersonneAutorisationsCache(persId), 
 *                                          new FwkGrhAutorisationsCache(persId)); 
 *  </pre>
 *  
 *  Les méthodes de vérification des droits sur une fonction ou une donnée sont implémentés dans les 
 *  AutorisationsCache.<br/>
 *  Il appartient au développeur implémentant la sous classe d'ApplicationUser de fournir
 *  des accesseurs aux caches pour vérifier les droits.<br/>
 *  Par exemple dans AGrhumApplicationUser :
 *  <pre>
 *      public FwkPersonneAutorisationCache getFwkPersonneAutorisationsCache() {...}
 *  </pre>
 *  
 *  Ainsi dans un .wod, on peut y faire référence "en dur" :
 *  <pre>
 *      condition = session.appUser.fwkPersonneAutorisationsCache.hasDroitCreationFournisseur;
 *  </pre>
 *  
 *  Enfin dernier cas, si l'on se trouve dans le contexte d'une méthode métier d'un framework et que l'on veut 
 *  faire appel à la vérification des droits, il faut :
 *  <ul>
 *      <li> Rajouter dans la méthode awake() de Session :
 *           <code>ApplicationUser.setInCurrentThread(appUser);</code>
 *      </li>
 *      <li>
 *           Dans la méthode métier (ou dans une méthode utilitaire) :
 *           <pre>
 *              ApplicationUser appUser = ApplicationUser.getFromCurrentThread();
 *              FwkPersonneAutorisationsCache cache = appUser.getAutorisationsCacheForAppId("PERSONNE");
 *           </pre>
 *      </li>
 *  </ul>
 *  
 * @see AutorisationsCache
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public abstract class ApplicationUser {

    public static final String THREAD_STORAGE_KEY = "GdApplicationUser";
    private NSDictionary<String, AutorisationsCache> cachesForAppIds;
    
    public ApplicationUser(AutorisationsCache ...caches) {
        setCaches(caches);
    }

    private void setCaches(AutorisationsCache[] caches) {
        NSMutableDictionary<String, AutorisationsCache> cachesTmp =
            new NSMutableDictionary<String, AutorisationsCache>();
        for (AutorisationsCache cache : caches) {
            cachesTmp.setObjectForKey(cache, cache.getAppStrId());
        }
        this.cachesForAppIds = cachesTmp.immutableClone();
    }
    
    public AutorisationsCache getAutorisationsCacheForAppId(String appId) {
        return cachesForAppIds.objectForKey(appId);
    }
    
    public static void setForCurrentThread(ApplicationUser appUser) {
        ERXThreadStorage.takeValueForKey(appUser, THREAD_STORAGE_KEY);
    }
    
    public static ApplicationUser getFromCurrentThread() {
        return (ApplicationUser) ERXThreadStorage.valueForKey(THREAD_STORAGE_KEY);
    }
    
}
