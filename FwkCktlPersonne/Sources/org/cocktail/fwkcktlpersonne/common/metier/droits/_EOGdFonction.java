/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdFonction.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdFonction extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdFonction.class);

	public static final String ENTITY_NAME = "GdFonction";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_FONCTION";


// Attribute Keys
  public static final ERXKey<String> FON_CATEGORIE = new ERXKey<String>("fonCategorie");
  public static final ERXKey<String> FON_DESCRIPTION = new ERXKey<String>("fonDescription");
  public static final ERXKey<String> FON_ID_INTERNE = new ERXKey<String>("fonIdInterne");
  public static final ERXKey<String> FON_LC = new ERXKey<String>("fonLc");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> TO_GD_APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("toGdApplication");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fonId";

	public static final String FON_CATEGORIE_KEY = "fonCategorie";
	public static final String FON_DESCRIPTION_KEY = "fonDescription";
	public static final String FON_ID_INTERNE_KEY = "fonIdInterne";
	public static final String FON_LC_KEY = "fonLc";

// Attributs non visibles
	public static final String APP_ID_KEY = "appId";
	public static final String FON_ID_KEY = "fonId";

//Colonnes dans la base de donnees
	public static final String FON_CATEGORIE_COLKEY = "FON_CATEGORIE";
	public static final String FON_DESCRIPTION_COLKEY = "FON_DESCRIPTION";
	public static final String FON_ID_INTERNE_COLKEY = "FON_ID_INTERNE";
	public static final String FON_LC_COLKEY = "FON_LC";

	public static final String APP_ID_COLKEY = "APP_ID";
	public static final String FON_ID_COLKEY = "FON_ID";


	// Relationships
	public static final String TO_GD_APPLICATION_KEY = "toGdApplication";



	// Accessors methods
  public String fonCategorie() {
    return (String) storedValueForKey(FON_CATEGORIE_KEY);
  }

  public void setFonCategorie(String value) {
    takeStoredValueForKey(value, FON_CATEGORIE_KEY);
  }

  public String fonDescription() {
    return (String) storedValueForKey(FON_DESCRIPTION_KEY);
  }

  public void setFonDescription(String value) {
    takeStoredValueForKey(value, FON_DESCRIPTION_KEY);
  }

  public String fonIdInterne() {
    return (String) storedValueForKey(FON_ID_INTERNE_KEY);
  }

  public void setFonIdInterne(String value) {
    takeStoredValueForKey(value, FON_ID_INTERNE_KEY);
  }

  public String fonLc() {
    return (String) storedValueForKey(FON_LC_KEY);
  }

  public void setFonLc(String value) {
    takeStoredValueForKey(value, FON_LC_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication)storedValueForKey(TO_GD_APPLICATION_KEY);
  }

  public void setToGdApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication oldValue = toGdApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_APPLICATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOGdFonction avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdFonction createEOGdFonction(EOEditingContext editingContext, String fonCategorie
, String fonIdInterne
, String fonLc
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication			) {
    EOGdFonction eo = (EOGdFonction) createAndInsertInstance(editingContext, _EOGdFonction.ENTITY_NAME);    
		eo.setFonCategorie(fonCategorie);
		eo.setFonIdInterne(fonIdInterne);
		eo.setFonLc(fonLc);
    eo.setToGdApplicationRelationship(toGdApplication);
    return eo;
  }

  
	  public EOGdFonction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdFonction)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdFonction creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdFonction creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdFonction object = (EOGdFonction)createAndInsertInstance(editingContext, _EOGdFonction.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdFonction localInstanceIn(EOEditingContext editingContext, EOGdFonction eo) {
    EOGdFonction localInstance = (eo == null) ? null : (EOGdFonction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdFonction#localInstanceIn a la place.
   */
	public static EOGdFonction localInstanceOf(EOEditingContext editingContext, EOGdFonction eo) {
		return EOGdFonction.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdFonction> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdFonction> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
