package org.cocktail.fwkcktlpersonne.common.metier.droits.referentiel;

import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;

public class ReferentielApplicationAutorisationCache extends
AutorisationsCache {
	
	public static final String APP_STRING_ID_DROITS = "GESTDROIT";
	public static final String APP_STRING_ID_GESTION_DROITS = "GESTION DES DROITS";

	public ReferentielApplicationAutorisationCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}
	
	/* Debut droits pour Gestion des Droits */
	
	public Boolean hasDroitAccesGestionDroits() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_DROITS);
	}
	

}
