/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdPerimetre.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdPerimetre extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdPerimetre.class);

	public static final String ENTITY_NAME = "GdPerimetre";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_PERIMETRE";


// Attribute Keys
  public static final ERXKey<Integer> APP_ID = new ERXKey<Integer>("appId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERIMETRE_PARENT_ID = new ERXKey<Integer>("perimetreParentId");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> TYPE_DROIT_DONNEE_ID = new ERXKey<Integer>("typeDroitDonneeId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("application");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRE_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetreParent");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRES_ENFANTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetresEnfants");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> PROFILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("profils");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee> TYPE_DROIT_DONNEE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee>("typeDroitDonnee");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String APP_ID_KEY = "appId";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String LIBELLE_KEY = "libelle";
	public static final String PERIMETRE_PARENT_ID_KEY = "perimetreParentId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TYPE_DROIT_DONNEE_ID_KEY = "typeDroitDonneeId";

// Attributs non visibles
	public static final String ID_KEY = "id";

//Colonnes dans la base de donnees
	public static final String APP_ID_COLKEY = "APP_ID";
	public static final String DATE_CREATION_COLKEY = "D_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String PERIMETRE_PARENT_ID_COLKEY = "PERIMETRE_PARENT_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TYPE_DROIT_DONNEE_ID_COLKEY = "TDD_ID";

	public static final String ID_COLKEY = "ID_GD_PERIMETRE";


	// Relationships
	public static final String APPLICATION_KEY = "application";
	public static final String PERIMETRE_PARENT_KEY = "perimetreParent";
	public static final String PERIMETRES_ENFANTS_KEY = "perimetresEnfants";
	public static final String PROFILS_KEY = "profils";
	public static final String TYPE_DROIT_DONNEE_KEY = "typeDroitDonnee";



	// Accessors methods
  public Integer appId() {
    return (Integer) storedValueForKey(APP_ID_KEY);
  }

  public void setAppId(Integer value) {
    takeStoredValueForKey(value, APP_ID_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public Integer perimetreParentId() {
    return (Integer) storedValueForKey(PERIMETRE_PARENT_ID_KEY);
  }

  public void setPerimetreParentId(Integer value) {
    takeStoredValueForKey(value, PERIMETRE_PARENT_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Integer typeDroitDonneeId() {
    return (Integer) storedValueForKey(TYPE_DROIT_DONNEE_ID_KEY);
  }

  public void setTypeDroitDonneeId(Integer value) {
    takeStoredValueForKey(value, TYPE_DROIT_DONNEE_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication application() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication)storedValueForKey(APPLICATION_KEY);
  }

  public void setApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication oldValue = application();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre perimetreParent() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre)storedValueForKey(PERIMETRE_PARENT_KEY);
  }

  public void setPerimetreParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre oldValue = perimetreParent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERIMETRE_PARENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERIMETRE_PARENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee typeDroitDonnee() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee)storedValueForKey(TYPE_DROIT_DONNEE_KEY);
  }

  public void setTypeDroitDonneeRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee oldValue = typeDroitDonnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_DROIT_DONNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_DROIT_DONNEE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetresEnfants() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)storedValueForKey(PERIMETRES_ENFANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetresEnfants(EOQualifier qualifier) {
    return perimetresEnfants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetresEnfants(EOQualifier qualifier, boolean fetch) {
    return perimetresEnfants(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetresEnfants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre.PERIMETRE_PARENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = perimetresEnfants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPerimetresEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERIMETRES_ENFANTS_KEY);
  }

  public void removeFromPerimetresEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIMETRES_ENFANTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre createPerimetresEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdPerimetre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERIMETRES_ENFANTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre) eo;
  }

  public void deletePerimetresEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIMETRES_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPerimetresEnfantsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> objects = perimetresEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePerimetresEnfantsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> profils() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)storedValueForKey(PROFILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> profils(EOQualifier qualifier) {
    return profils(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> profils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> results;
      results = profils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToProfilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROFILS_KEY);
  }

  public void removeFromProfilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROFILS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil createProfilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfil");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROFILS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil) eo;
  }

  public void deleteProfilsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROFILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllProfilsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> objects = profils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteProfilsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGdPerimetre avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdPerimetre createEOGdPerimetre(EOEditingContext editingContext, Integer appId
, NSTimestamp dateCreation
, String libelle
, Integer persIdCreation
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication application			) {
    EOGdPerimetre eo = (EOGdPerimetre) createAndInsertInstance(editingContext, _EOGdPerimetre.ENTITY_NAME);    
		eo.setAppId(appId);
		eo.setDateCreation(dateCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    eo.setApplicationRelationship(application);
    return eo;
  }

  
	  public EOGdPerimetre localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdPerimetre)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdPerimetre creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdPerimetre creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdPerimetre object = (EOGdPerimetre)createAndInsertInstance(editingContext, _EOGdPerimetre.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdPerimetre localInstanceIn(EOEditingContext editingContext, EOGdPerimetre eo) {
    EOGdPerimetre localInstance = (eo == null) ? null : (EOGdPerimetre)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdPerimetre#localInstanceIn a la place.
   */
	public static EOGdPerimetre localInstanceOf(EOEditingContext editingContext, EOGdPerimetre eo) {
		return EOGdPerimetre.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdPerimetre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdPerimetre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdPerimetre> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdPerimetre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdPerimetre)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdPerimetre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdPerimetre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdPerimetre> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdPerimetre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdPerimetre)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdPerimetre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdPerimetre eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdPerimetre ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdPerimetre fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
