/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdDomaine.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdDomaine extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdDomaine.class);

	public static final String ENTITY_NAME = "GdDomaine";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_DOMAINE";


// Attribute Keys
  public static final ERXKey<String> DOM_LC = new ERXKey<String>("domLc");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> TO_GD_APPLICATIONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("toGdApplications");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "domId";

	public static final String DOM_LC_KEY = "domLc";

// Attributs non visibles
	public static final String DOM_ID_KEY = "domId";

//Colonnes dans la base de donnees
	public static final String DOM_LC_COLKEY = "DOM_LC";

	public static final String DOM_ID_COLKEY = "DOM_ID";


	// Relationships
	public static final String TO_GD_APPLICATIONS_KEY = "toGdApplications";



	// Accessors methods
  public String domLc() {
    return (String) storedValueForKey(DOM_LC_KEY);
  }

  public void setDomLc(String value) {
    takeStoredValueForKey(value, DOM_LC_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> toGdApplications() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>)storedValueForKey(TO_GD_APPLICATIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> toGdApplications(EOQualifier qualifier) {
    return toGdApplications(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> toGdApplications(EOQualifier qualifier, boolean fetch) {
    return toGdApplications(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> toGdApplications(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication.TO_GD_DOMAINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdApplications();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdApplicationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_APPLICATIONS_KEY);
  }

  public void removeFromToGdApplicationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_APPLICATIONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication createToGdApplicationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdApplication");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_APPLICATIONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication) eo;
  }

  public void deleteToGdApplicationsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_APPLICATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdApplicationsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> objects = toGdApplications().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdApplicationsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGdDomaine avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdDomaine createEOGdDomaine(EOEditingContext editingContext, String domLc
			) {
    EOGdDomaine eo = (EOGdDomaine) createAndInsertInstance(editingContext, _EOGdDomaine.ENTITY_NAME);    
		eo.setDomLc(domLc);
    return eo;
  }

  
	  public EOGdDomaine localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdDomaine)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDomaine creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDomaine creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdDomaine object = (EOGdDomaine)createAndInsertInstance(editingContext, _EOGdDomaine.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdDomaine localInstanceIn(EOEditingContext editingContext, EOGdDomaine eo) {
    EOGdDomaine localInstance = (eo == null) ? null : (EOGdDomaine)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdDomaine#localInstanceIn a la place.
   */
	public static EOGdDomaine localInstanceOf(EOEditingContext editingContext, EOGdDomaine eo) {
		return EOGdDomaine.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdDomaine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdDomaine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdDomaine> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdDomaine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdDomaine)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdDomaine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdDomaine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdDomaine> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdDomaine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdDomaine)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdDomaine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdDomaine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdDomaine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdDomaine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
