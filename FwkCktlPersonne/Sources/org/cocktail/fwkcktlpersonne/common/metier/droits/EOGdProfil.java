/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier.droits;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Cette classe définit et gère un profil dans la gestion centralisée des droits
 */
public class EOGdProfil extends _EOGdProfil {
	private static final long serialVersionUID = 1L;

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException : exception remontée par l'opération de suppression si ds profils enfants existent
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		//Verifier si profils enfants
		if (toGdProfilsEnfants().count() > 0) {
			throw new NSValidation.ValidationException("Ce profil possède des profils enfants, impossible de le supprimer");
		}
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException : exception remontée lors de la validation d el'objet métier
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		trimAllString();
		setDateModification(MyDateCtrl.now());

		try {
			checkContraintesObligatoires();
			checkContraintesLongueursMax();

			super.validateObjectMetier();
		} catch (Exception e) {
			NSValidation.ValidationException e1 = new ValidationException("Erreur lors de la validation du profil '" + toDisplayString() + "' : "
			        + e.getMessage());
			e1.initCause(e);
			throw e1;
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDateCreation(MyDateCtrl.now());
	}

	/**
	 * @return chaine affichée
	 */
	public String toDisplayString() {
		return prLc();
	}

	/**
	 * Verifie si les utilisateurs affectes a persIdCreation et persIdModification ont bien le droits de creer/Modifier l'entité.
	 * @throws NSValidation.ValidationException : exception remontée si les utilisateurs ne sont pas habilités à créer/modifier lentité
	 */
	public void checkUsers() throws NSValidation.ValidationException {
		try {
			if (persIdModification() != null && persIdCreation() == null) {
				setPersIdCreation(persIdModification());
			}

			if (getCreateur() == null) {
				throw new NSValidation.ValidationException("La reference au createur (persIdCreation) est obligatoire.");
			}
			if (getModificateur() == null) {
				throw new NSValidation.ValidationException("La reference au modificateur (persIdModification) est obligatoire.");
			}
		} catch (Exception e) {
			NSValidation.ValidationException e1 = new NSValidation.ValidationException(this.getClass().getSimpleName() + " : " + toDisplayString() + ":"
			        + e.getLocalizedMessage());
			throw e1;
		}
	}

	/**
	 * @param donnee
	 * @param typeDroit
	 * @param persIdCrea
	 */
	@Deprecated
	public void addDroitOnDonnee(EOGdDonnee donnee, EOGdTypeDroitDonnee typeDroit, Integer persIdCrea) {
		NSArray<EOGdProfilDroitDonnee> droits = allDroitsDonnee(donnee.toGdApplication());
		EOQualifier qual =
		        ERXQ.equals(EOGdProfilDroitDonnee.TO_GD_DONNEE_KEY, donnee).and(
		                ERXQ.equals(EOGdProfilDroitDonnee.TO_GD_TYPE_DROIT_DONNEE_KEY, typeDroit));
		if (ERXQ.first(droits, qual) == null) {
			EOGdProfilDroitDonnee droitDonnee = EOGdProfilDroitDonnee.creerInstance(editingContext());
			droitDonnee.setToGdDonneeRelationship(donnee);
			droitDonnee.setToGdProfilRelationship(this);
			droitDonnee.setToGdTypeDroitDonneeRelationship(typeDroit);
			droitDonnee.setPersIdCreation(persIdCrea);
		}
	}

	/**
	 * @param fonction : fonction sur laquelle on définit un droit
	 * @param typeDroit : type de droit
	 * @param persIdCrea : id de l'utilisateur qui crée le droit sur la fonction
	 */
	public void addDroitOnFonction(EOGdFonction fonction, EOGdTypeDroitFonction typeDroit, Integer persIdCrea) {
		NSArray<EOGdProfilDroitFonction> droits = allDroitsFonction(fonction.toGdApplication());
		EOQualifier qual =
		        ERXQ.equals(EOGdProfilDroitFonction.TO_GD_FONCTION_KEY, fonction).and(
		                ERXQ.equals(EOGdProfilDroitFonction.TO_GD_TYPE_DROIT_FONCTION_KEY, typeDroit));
		if (ERXQ.first(droits, qual) == null) {
			EOGdProfilDroitFonction droitFonction = EOGdProfilDroitFonction.creerInstance(editingContext());
			droitFonction.setToGdFonctionRelationship(fonction);
			droitFonction.setToGdProfilRelationship(this);
			droitFonction.setToGdTypeDroitFonctionRelationship(typeDroit);
			droitFonction.setPersIdCreation(persIdCrea);
		}
	}

	/**
	 * @param application l'application
	 * @return les droits sur les fonctions du profil correspondant à l'<code>application</code>
	 */
	public NSArray<EOGdProfilDroitFonction> allDroitsFonction(EOGdApplication application) {
		NSMutableArray<EOGdProfilDroitFonction> droits = droitFonctions(application, null).mutableClone();
		droits.addObjectsFromArray(droitFonctionsHerites(application, null));
		return ERXS.sorted(droits, droitFonctionsOrderings());
	}

	/**
	 * @param application l'application
	 * @return les périmètres du profil et de ses profils parents correspondant à l'<code>application</code>
	 */
	public NSArray<EOGdPerimetre> allPerimetres(EOGdApplication application) {
		EOQualifier qual = EOGdPerimetre.APPLICATION.eq(application);
		NSMutableArray<EOGdPerimetre> perimetres = perimetres(qual).mutableClone();
		EOGdProfil profilParent = toGdProfilPere();
		while (profilParent != null) {			
			perimetres.addObjectsFromArray(profilParent.perimetres(qual));
			profilParent = profilParent.toGdProfilPere();
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(perimetres);	
	}

	/**
	 * @param application l'application
	 * @param orderings clause d'ordonnancement
	 * @return les droits sur les fonctions du profil correspondant à l'<code>application</code>
	 */
	public NSArray<EOGdProfilDroitFonction> droitFonctions(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOQualifier qual = null;
		if (application != null) {
			qual = qualifierFonctionApplication(application);
		}
		return toGdProfilDroitFonctions(qual, orderings, false);
	}

	/**
	 * @param application l'application pour filtrer les droits
	 * @param orderings clause d'ordonnancement
	 * @return les droits des profils hérités par this et correspondant à <code>applicaiton</code>
	 */
	public NSArray<EOGdProfilDroitFonction> droitFonctionsHerites(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOGdProfil pere = toGdProfilPere();
		NSMutableArray<EOGdProfilDroitFonction> droitsHerites = new NSMutableArray<EOGdProfilDroitFonction>();
		while (pere != null) {
			droitsHerites.addObjectsFromArray(pere.toGdProfilDroitFonctions());
			pere = pere.toGdProfilPere();
		}
		if (application != null) {
			ERXQ.filter(droitsHerites, qualifierFonctionApplication(application));
		}
		if (orderings != null) {
			ERXS.sort(droitsHerites, orderings);
		}
		return droitsHerites;
	}

	/**
	 * @param application l'application
	 * @return le qualifier correspondant à l' <code>application</code>
	 */
	private EOQualifier qualifierFonctionApplication(EOGdApplication application) {
		return ERXQ.equals(
		        ERXQ.keyPath(EOGdProfilDroitFonction.TO_GD_FONCTION_KEY, EOGdFonction.TO_GD_APPLICATION_KEY),
		        application);
	}

	/**
	 * @return les sort orderings
	 */
	private NSArray<EOSortOrdering> droitFonctionsOrderings() {
		return ERXS.ascInsensitives(
		        EOGdProfilDroitFonction.TO_GD_FONCTION_KEY + "." + EOGdFonction.FON_CATEGORIE_KEY,
		        EOGdProfilDroitFonction.TO_GD_FONCTION_KEY + "." + EOGdFonction.FON_LC_KEY);
	}

	/**
	 * @param application l'application
	 * @return les droits sur les périmètres de données correspondant à l' <code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonnee> allDroitsDonnee(EOGdApplication application) {
		NSMutableArray<EOGdProfilDroitDonnee> droits = droitsDonnee(application, null).mutableClone();
		droits.addObjectsFromArray(droitsDonneeHerites(application, null));
		return ERXS.sorted(droits, droitsDonneeOrderings());
	}

	/**
	 * @param application l'application
	 * @param orderings clause d'ordonnancement
	 * @return les droits sur les périmètres de données correspondant à l' <code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonnee> droitsDonnee(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOQualifier qual = null;
		if (application != null) {
			qual = qualifierDonneeApplication(application);
		}
		return toGdProfilDroitDonnees(qual, orderings, false);
	}

	/**
	 * @param application l'application
	 * @param orderings clause d'ordonnancement
	 * @return les droits sur les périmètres de données du profil correspondant à l'<code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonnee> droitsDonneeHerites(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOGdProfil pere = toGdProfilPere();
		NSMutableArray<EOGdProfilDroitDonnee> droitsHerites = new NSMutableArray<EOGdProfilDroitDonnee>();
		while (pere != null) {
			droitsHerites.addObjectsFromArray(pere.toGdProfilDroitDonnees());
			pere = pere.toGdProfilPere();
		}
		if (application != null) {
			ERXQ.filter(droitsHerites, qualifierDonneeApplication(application));
		}
		if (orderings != null) {
			ERXS.sort(droitsHerites, orderings);
		}
		return droitsHerites;
	}

	/**
	 * @param application l'application
	 * @return le qualifier correspondant à l' <code>application</code>
	 */
	@Deprecated
	private EOQualifier qualifierDonneeApplication(EOGdApplication application) {
		return ERXQ.equals(
		        ERXQ.keyPath(EOGdProfilDroitDonnee.TO_GD_DONNEE_KEY, EOGdDonnee.TO_GD_APPLICATION_KEY),
		        application);
	}

	/**
	 * @return les sort orderings sur les droits des données
	 */
	@Deprecated
	private NSArray<EOSortOrdering> droitsDonneeOrderings() {
		return ERXS.ascInsensitives(
		        EOGdProfilDroitDonnee.TO_GD_DONNEE_KEY + "." + EOGdDonnee.DON_CATEGORIE_KEY,
		        EOGdProfilDroitDonnee.TO_GD_DONNEE_KEY + "." + EOGdDonnee.DON_LC_KEY);
	}

	/**
	 * @param application l'application
	 * @return les droits sur les données statiques correspondant à l'<code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonneeSt> allDroitsDonneeSt(EOGdApplication application) {
		NSMutableArray<EOGdProfilDroitDonneeSt> droits = droitsDonneeSt(application, null).mutableClone();
		droits.addObjectsFromArray(droitsDonneeStHerites(application, null));
		return ERXS.sorted(droits, droitsDonneeStOrderings());
	}

	/**
	 * @param application l'application
	 * @param orderings clause d'ordonnancement
	 * @return les droits sur les données statiques correspondant à l'<code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonneeSt> droitsDonneeSt(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOQualifier qual = null;
		if (application != null) {
			qual = qualifierDonneeStApplication(application);
		}
		return toGdProfilDroitDonneeSts(qual, orderings, false);
	}

	/**
	 * @param application l'application
	 * @param orderings clause d'ordonnancement
	 * @return les droits sur les données statiques hérités correspondant à l'<code>application</code>
	 */
	@Deprecated
	public NSArray<EOGdProfilDroitDonneeSt> droitsDonneeStHerites(EOGdApplication application, NSArray<EOSortOrdering> orderings) {
		EOGdProfil pere = toGdProfilPere();
		NSMutableArray<EOGdProfilDroitDonneeSt> droitsHerites = new NSMutableArray<EOGdProfilDroitDonneeSt>();
		while (pere != null) {
			droitsHerites.addObjectsFromArray(pere.toGdProfilDroitDonneeSts());
			pere = pere.toGdProfilPere();
		}
		if (application != null) {
			ERXQ.filter(droitsHerites, qualifierDonneeStApplication(application));
		}
		if (orderings != null) {
			ERXS.sort(droitsHerites, orderings);
		}
		return droitsHerites;
	}

	/**
	 * @param application l'application
	 * @return le qualifier sur les données statiques correspondant à l'<code>application</code>
	 */
	@Deprecated
	private EOQualifier qualifierDonneeStApplication(EOGdApplication application) {
		return ERXQ.equals(
		        ERXQ.keyPath(EOGdProfilDroitDonneeSt.TO_GD_DONNEE_STAT_KEY, EOGdDonneeStat.TO_GD_APPLICATION_KEY),
		        application);
	}

	/**
	 * @return les sort orderings sur les droits des données
	 */
	@Deprecated
	private NSArray<EOSortOrdering> droitsDonneeStOrderings() {
		return ERXS.ascInsensitives(
		        EOGdProfilDroitDonneeSt.TO_GD_DONNEE_STAT_KEY + "." + EOGdDonneeStat.DOS_ENTITE_KEY,
		        EOGdProfilDroitDonneeSt.TO_GD_DONNEE_STAT_KEY + "." + EOGdDonneeStat.DOS_LC_KEY);
	}

	/**
	 * @param editingContext un ec
	 * @return le profil racine ie sans profil père
	 */
	public static EOGdProfil rootProfil(EOEditingContext editingContext) {
		return fetchFirstByQualifier(editingContext, ERXQ.isNull(TO_GD_PROFIL_PERE_KEY));
	}

	/**
	 * associe un périmetre au profil
	 * @param perimetre : périmètre de données
	 */
	public void associerPerimetre(EOGdPerimetre perimetre) {
		this.addToPerimetresRelationship(perimetre);
	}

	/**
	 * enleve un périmetre au profil
	 * @param perimetre : périmètre de données
	 */
	public void enleverPerimetre(EOGdPerimetre perimetre) {
		this.removeFromPerimetresRelationship(perimetre);
	}
	
	/**
	 * Supprime un profil en ecrivant et executant directement les requetes SQ
	 * @param supprimerPerimetres : indique si on supprime les perimetres associes (cas si cocktail.feature.perimetre=true)
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer(Boolean supprimerPerimetres) throws Exception {
		try {
			this.toGdProfilPere().removeFromToGdProfilsEnfantsRelationship(this);
			this.setToGdProfilPereRelationship(null);
			
			if (supprimerPerimetres) {
				this.deleteAllPerimetresRelationships();
			}

			this.delete();

		} catch (Exception e) {
			System.err.println("Erreur supprimer : " + e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Supprime un profil en ecrivant et executant directement les requetes SQ
	 * @param supprimerPerimetres : indique si on supprime les perimetres associes (cas si cocktail.feature.perimetre=true)
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimerEnSQL(Boolean supprimerPerimetres) throws Exception {
		try {
			// DELETE du profil
			String requeteDeleteProfil = "DELETE FROM " + ENTITY_TABLE_NAME + " WHERE " + PR_ID_COLKEY + "=" + primaryKey();
			ERXEOAccessUtilities.evaluateSQLWithEntityNamed(editingContext(), ENTITY_NAME, requeteDeleteProfil);

			refetchObjectFromDB();

		} catch (Exception e) {
			System.err.println("Erreur supprimer : " + e.getMessage());
			throw e;
		}
	}
	
}
