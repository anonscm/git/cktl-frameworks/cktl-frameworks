/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdDonneeStat.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdDonneeStat extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdDonneeStat.class);

	public static final String ENTITY_NAME = "GdDonneeStat";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_DONNEE_STAT";


// Attribute Keys
  public static final ERXKey<String> DOS_CLE = new ERXKey<String>("dosCle");
  public static final ERXKey<String> DOS_DESCRIPTION = new ERXKey<String>("dosDescription");
  public static final ERXKey<String> DOS_ENTITE = new ERXKey<String>("dosEntite");
  public static final ERXKey<String> DOS_ENTITE_LIBELLE_KEY = new ERXKey<String>("dosEntiteLibelleKey");
  public static final ERXKey<String> DOS_LC = new ERXKey<String>("dosLc");
  public static final ERXKey<String> DOS_TYPE_CLE = new ERXKey<String>("dosTypeCle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> TO_GD_APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("toGdApplication");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dosId";

	public static final String DOS_CLE_KEY = "dosCle";
	public static final String DOS_DESCRIPTION_KEY = "dosDescription";
	public static final String DOS_ENTITE_KEY = "dosEntite";
	public static final String DOS_ENTITE_LIBELLE_KEY_KEY = "dosEntiteLibelleKey";
	public static final String DOS_LC_KEY = "dosLc";
	public static final String DOS_TYPE_CLE_KEY = "dosTypeCle";

// Attributs non visibles
	public static final String APP_ID_KEY = "appId";
	public static final String DOS_ID_KEY = "dosId";

//Colonnes dans la base de donnees
	public static final String DOS_CLE_COLKEY = "DOS_CLE";
	public static final String DOS_DESCRIPTION_COLKEY = "DOS_DESCRIPTION";
	public static final String DOS_ENTITE_COLKEY = "DOS_ENTITE";
	public static final String DOS_ENTITE_LIBELLE_KEY_COLKEY = "DOS_ENTITE_LIBELLE_KEY";
	public static final String DOS_LC_COLKEY = "DOS_LC";
	public static final String DOS_TYPE_CLE_COLKEY = "DOS_TYPE_CLE";

	public static final String APP_ID_COLKEY = "APP_ID";
	public static final String DOS_ID_COLKEY = "DOS_ID";


	// Relationships
	public static final String TO_GD_APPLICATION_KEY = "toGdApplication";



	// Accessors methods
  public String dosCle() {
    return (String) storedValueForKey(DOS_CLE_KEY);
  }

  public void setDosCle(String value) {
    takeStoredValueForKey(value, DOS_CLE_KEY);
  }

  public String dosDescription() {
    return (String) storedValueForKey(DOS_DESCRIPTION_KEY);
  }

  public void setDosDescription(String value) {
    takeStoredValueForKey(value, DOS_DESCRIPTION_KEY);
  }

  public String dosEntite() {
    return (String) storedValueForKey(DOS_ENTITE_KEY);
  }

  public void setDosEntite(String value) {
    takeStoredValueForKey(value, DOS_ENTITE_KEY);
  }

  public String dosEntiteLibelleKey() {
    return (String) storedValueForKey(DOS_ENTITE_LIBELLE_KEY_KEY);
  }

  public void setDosEntiteLibelleKey(String value) {
    takeStoredValueForKey(value, DOS_ENTITE_LIBELLE_KEY_KEY);
  }

  public String dosLc() {
    return (String) storedValueForKey(DOS_LC_KEY);
  }

  public void setDosLc(String value) {
    takeStoredValueForKey(value, DOS_LC_KEY);
  }

  public String dosTypeCle() {
    return (String) storedValueForKey(DOS_TYPE_CLE_KEY);
  }

  public void setDosTypeCle(String value) {
    takeStoredValueForKey(value, DOS_TYPE_CLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication)storedValueForKey(TO_GD_APPLICATION_KEY);
  }

  public void setToGdApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication oldValue = toGdApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_APPLICATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOGdDonneeStat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdDonneeStat createEOGdDonneeStat(EOEditingContext editingContext, String dosCle
, String dosDescription
, String dosEntite
, String dosEntiteLibelleKey
, String dosLc
, String dosTypeCle
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication			) {
    EOGdDonneeStat eo = (EOGdDonneeStat) createAndInsertInstance(editingContext, _EOGdDonneeStat.ENTITY_NAME);    
		eo.setDosCle(dosCle);
		eo.setDosDescription(dosDescription);
		eo.setDosEntite(dosEntite);
		eo.setDosEntiteLibelleKey(dosEntiteLibelleKey);
		eo.setDosLc(dosLc);
		eo.setDosTypeCle(dosTypeCle);
    eo.setToGdApplicationRelationship(toGdApplication);
    return eo;
  }

  
	  public EOGdDonneeStat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdDonneeStat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDonneeStat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDonneeStat creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdDonneeStat object = (EOGdDonneeStat)createAndInsertInstance(editingContext, _EOGdDonneeStat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdDonneeStat localInstanceIn(EOEditingContext editingContext, EOGdDonneeStat eo) {
    EOGdDonneeStat localInstance = (eo == null) ? null : (EOGdDonneeStat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdDonneeStat#localInstanceIn a la place.
   */
	public static EOGdDonneeStat localInstanceOf(EOEditingContext editingContext, EOGdDonneeStat eo) {
		return EOGdDonneeStat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdDonneeStat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdDonneeStat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdDonneeStat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdDonneeStat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdDonneeStat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdDonneeStat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdDonneeStat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdDonneeStat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdDonneeStat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdDonneeStat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdDonneeStat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdDonneeStat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdDonneeStat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdDonneeStat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
