/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdProfil.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdProfil extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdProfil.class);

	public static final String ENTITY_NAME = "GdProfil";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_PROFIL";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PR_DESCRIPTION = new ERXKey<String>("prDescription");
  public static final ERXKey<String> PR_LC = new ERXKey<String>("prLc");
  public static final ERXKey<String> PR_REMARQUES = new ERXKey<String>("prRemarques");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetres");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> TO_GD_PROFIL_DROIT_DONNEES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee>("toGdProfilDroitDonnees");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> TO_GD_PROFIL_DROIT_DONNEE_STS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt>("toGdProfilDroitDonneeSts");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> TO_GD_PROFIL_DROIT_FONCTIONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction>("toGdProfilDroitFonctions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> TO_GD_PROFIL_PERE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("toGdProfilPere");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> TO_GD_PROFIL_PERIMETRES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>("toGdProfilPerimetres");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> TO_GD_PROFILS_ENFANTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("toGdProfilsEnfants");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique> TO_GROUPE_DYNAMIQUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique>("toGroupeDynamique");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PR_DESCRIPTION_KEY = "prDescription";
	public static final String PR_LC_KEY = "prLc";
	public static final String PR_REMARQUES_KEY = "prRemarques";

// Attributs non visibles
	public static final String GRPD_ID_KEY = "grpdId";
	public static final String PR_ID_KEY = "prId";
	public static final String PR_PERE_ID_KEY = "prPereId";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PR_DESCRIPTION_COLKEY = "PR_DESCRIPTION";
	public static final String PR_LC_COLKEY = "PR_LC";
	public static final String PR_REMARQUES_COLKEY = "PR_REMARQUES";

	public static final String GRPD_ID_COLKEY = "GRPD_ID";
	public static final String PR_ID_COLKEY = "PR_ID";
	public static final String PR_PERE_ID_COLKEY = "PR_PERE_ID";


	// Relationships
	public static final String PERIMETRES_KEY = "perimetres";
	public static final String TO_GD_PROFIL_DROIT_DONNEES_KEY = "toGdProfilDroitDonnees";
	public static final String TO_GD_PROFIL_DROIT_DONNEE_STS_KEY = "toGdProfilDroitDonneeSts";
	public static final String TO_GD_PROFIL_DROIT_FONCTIONS_KEY = "toGdProfilDroitFonctions";
	public static final String TO_GD_PROFIL_PERE_KEY = "toGdProfilPere";
	public static final String TO_GD_PROFIL_PERIMETRES_KEY = "toGdProfilPerimetres";
	public static final String TO_GD_PROFILS_ENFANTS_KEY = "toGdProfilsEnfants";
	public static final String TO_GROUPE_DYNAMIQUE_KEY = "toGroupeDynamique";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String prDescription() {
    return (String) storedValueForKey(PR_DESCRIPTION_KEY);
  }

  public void setPrDescription(String value) {
    takeStoredValueForKey(value, PR_DESCRIPTION_KEY);
  }

  public String prLc() {
    return (String) storedValueForKey(PR_LC_KEY);
  }

  public void setPrLc(String value) {
    takeStoredValueForKey(value, PR_LC_KEY);
  }

  public String prRemarques() {
    return (String) storedValueForKey(PR_REMARQUES_KEY);
  }

  public void setPrRemarques(String value) {
    takeStoredValueForKey(value, PR_REMARQUES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toGdProfilPere() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil)storedValueForKey(TO_GD_PROFIL_PERE_KEY);
  }

  public void setToGdProfilPereRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil oldValue = toGdProfilPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_PROFIL_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_PROFIL_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique toGroupeDynamique() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique)storedValueForKey(TO_GROUPE_DYNAMIQUE_KEY);
  }

  public void setToGroupeDynamiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique oldValue = toGroupeDynamique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GROUPE_DYNAMIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GROUPE_DYNAMIQUE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetres() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)storedValueForKey(PERIMETRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetres(EOQualifier qualifier) {
    return perimetres(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> perimetres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> results;
      results = perimetres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERIMETRES_KEY);
  }

  public void removeFromPerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIMETRES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre createPerimetresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdPerimetre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERIMETRES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre) eo;
  }

  public void deletePerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERIMETRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPerimetresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> objects = perimetres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePerimetresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> toGdProfilDroitDonnees() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee>)storedValueForKey(TO_GD_PROFIL_DROIT_DONNEES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> toGdProfilDroitDonnees(EOQualifier qualifier) {
    return toGdProfilDroitDonnees(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> toGdProfilDroitDonnees(EOQualifier qualifier, boolean fetch) {
    return toGdProfilDroitDonnees(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> toGdProfilDroitDonnees(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee.TO_GD_PROFIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdProfilDroitDonnees();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdProfilDroitDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEES_KEY);
  }

  public void removeFromToGdProfilDroitDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee createToGdProfilDroitDonneesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfilDroitDonnee");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_PROFIL_DROIT_DONNEES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee) eo;
  }

  public void deleteToGdProfilDroitDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdProfilDroitDonneesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonnee> objects = toGdProfilDroitDonnees().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdProfilDroitDonneesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> toGdProfilDroitDonneeSts() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt>)storedValueForKey(TO_GD_PROFIL_DROIT_DONNEE_STS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> toGdProfilDroitDonneeSts(EOQualifier qualifier) {
    return toGdProfilDroitDonneeSts(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> toGdProfilDroitDonneeSts(EOQualifier qualifier, boolean fetch) {
    return toGdProfilDroitDonneeSts(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> toGdProfilDroitDonneeSts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt.TO_GD_PROFIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdProfilDroitDonneeSts();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdProfilDroitDonneeStsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEE_STS_KEY);
  }

  public void removeFromToGdProfilDroitDonneeStsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEE_STS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt createToGdProfilDroitDonneeStsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfilDroitDonneeSt");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_PROFIL_DROIT_DONNEE_STS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt) eo;
  }

  public void deleteToGdProfilDroitDonneeStsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_DONNEE_STS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdProfilDroitDonneeStsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitDonneeSt> objects = toGdProfilDroitDonneeSts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdProfilDroitDonneeStsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> toGdProfilDroitFonctions() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction>)storedValueForKey(TO_GD_PROFIL_DROIT_FONCTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> toGdProfilDroitFonctions(EOQualifier qualifier) {
    return toGdProfilDroitFonctions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> toGdProfilDroitFonctions(EOQualifier qualifier, boolean fetch) {
    return toGdProfilDroitFonctions(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> toGdProfilDroitFonctions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction.TO_GD_PROFIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdProfilDroitFonctions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdProfilDroitFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_FONCTIONS_KEY);
  }

  public void removeFromToGdProfilDroitFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_FONCTIONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction createToGdProfilDroitFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfilDroitFonction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_PROFIL_DROIT_FONCTIONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction) eo;
  }

  public void deleteToGdProfilDroitFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_DROIT_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdProfilDroitFonctionsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> objects = toGdProfilDroitFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdProfilDroitFonctionsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> toGdProfilPerimetres() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>)storedValueForKey(TO_GD_PROFIL_PERIMETRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> toGdProfilPerimetres(EOQualifier qualifier) {
    return toGdProfilPerimetres(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> toGdProfilPerimetres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> results;
      results = toGdProfilPerimetres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToGdProfilPerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_PERIMETRES_KEY);
  }

  public void removeFromToGdProfilPerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_PERIMETRES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre createToGdProfilPerimetresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfilPerimetre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_PROFIL_PERIMETRES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre) eo;
  }

  public void deleteToGdProfilPerimetresRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFIL_PERIMETRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdProfilPerimetresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> objects = toGdProfilPerimetres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdProfilPerimetresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> toGdProfilsEnfants() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)storedValueForKey(TO_GD_PROFILS_ENFANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> toGdProfilsEnfants(EOQualifier qualifier) {
    return toGdProfilsEnfants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> toGdProfilsEnfants(EOQualifier qualifier, boolean fetch) {
    return toGdProfilsEnfants(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> toGdProfilsEnfants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil.TO_GD_PROFIL_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdProfilsEnfants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdProfilsEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_PROFILS_ENFANTS_KEY);
  }

  public void removeFromToGdProfilsEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFILS_ENFANTS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil createToGdProfilsEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdProfil");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_PROFILS_ENFANTS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil) eo;
  }

  public void deleteToGdProfilsEnfantsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_PROFILS_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdProfilsEnfantsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> objects = toGdProfilsEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdProfilsEnfantsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGdProfil avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdProfil createEOGdProfil(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer persIdCreation
, Integer persIdModification
, String prLc
			) {
    EOGdProfil eo = (EOGdProfil) createAndInsertInstance(editingContext, _EOGdProfil.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setPrLc(prLc);
    return eo;
  }

  
	  public EOGdProfil localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdProfil)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdProfil creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdProfil creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdProfil object = (EOGdProfil)createAndInsertInstance(editingContext, _EOGdProfil.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdProfil localInstanceIn(EOEditingContext editingContext, EOGdProfil eo) {
    EOGdProfil localInstance = (eo == null) ? null : (EOGdProfil)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdProfil#localInstanceIn a la place.
   */
	public static EOGdProfil localInstanceOf(EOEditingContext editingContext, EOGdProfil eo) {
		return EOGdProfil.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdProfil fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdProfil fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdProfil> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdProfil eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdProfil)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdProfil fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdProfil fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdProfil> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdProfil eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdProfil)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdProfil fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdProfil eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdProfil ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdProfil fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
