/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.exception.TooManyResultsException;
import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGroupeDynamiquePersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Point d'entrée pour les opérations de base sur les droits :
 * <ul>
 * <li>Détermination de l'ensemble des profils d'un utilisateur</li>
 * <li>Détermination de l'ensemble des droits d'un utilisateur</li>
 * <li>Détermination des droits d'un utilisateur</li>
 * <li>Détermination des utilisateurs ayant un profil</li>
 * <li>Détermination des utilisateurs ayant un droit</li>
 * </ul>
 * L'implémentation est faite en EOF lorsque possible, en SQL (raw rows) lorsque la rêquête est complexe.
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class DroitsHelper {

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @return les profils d'une personne
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOGdProfil> profilsForPersonne(EOEditingContext ec, Integer persId) {
		String key = EOGdProfil.TO_GROUPE_DYNAMIQUE_KEY + "." +
		        EOGroupeDynamique.TO_GROUPE_DYNAMIQUE_PERSONNES_KEY + "." + EOGroupeDynamiquePersonne.PERS_ID_KEY;
		EOQualifier qual = ERXQ.equals(key, persId);
		return EOGdProfil.fetchAll(ec, qual);
	}
	
	/**
	 * 
	 * @param ec un editing context
	 * @param appStrId id de l'application
	 * @return les profils utilisées par une application
	 */
	public static NSArray<EOGdProfil> profilsForApplication(EOEditingContext ec, String appStrId) {
		NSArray<EOGdProfil> profils = new NSMutableArray<EOGdProfil>();
		NSArray<EOGdProfilDroitFonction> profilFonctions = droitsFonctionForApplication(
				ec, appStrId);
		for (EOGdProfilDroitFonction eoGdProfilDroitFonction : profilFonctions) {
			if (appStrId.equals(eoGdProfilDroitFonction.toGdFonction().toGdApplication().appStrId())) {
				if (!profils.contains(eoGdProfilDroitFonction.toGdProfil())) {
					profils.add(eoGdProfilDroitFonction.toGdProfil());
				}
			}
		}
		return profils;		
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @return les infos sur les droits qu'a une personne sur des données
	 */
	public static NSArray<EOGdProfilDroitDonnee> droitsDonneeForPersonne(EOEditingContext ec, String appStrId, Integer persId) {
		NSArray<EOGdProfil> profils = profilsForPersonne(ec, persId);
		NSMutableArray<EOGdProfilDroitDonnee> droitsDonnee = new NSMutableArray<EOGdProfilDroitDonnee>();
		for (EOGdProfil profil : profils) {
			EOGdApplication app = getGdApplication(ec, appStrId);
			droitsDonnee.addObjectsFromArray(profil.allDroitsDonnee(app));
		}
		return droitsDonnee.immutableClone();
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @return les infos sur les droits qu'a une personne sur des fonctions
	 */
	public static NSArray<EOGdProfilDroitFonction> droitsFonctionForPersonne(EOEditingContext ec, String appStrId, Integer persId) {
		NSArray<EOGdProfil> profils = profilsForPersonne(ec, persId);
		NSMutableArray<EOGdProfilDroitFonction> droitsFonction = new NSMutableArray<EOGdProfilDroitFonction>();
		EOGdApplication app = getGdApplication(ec, appStrId);
		if (app != null) {
			for (EOGdProfil profil : profils) {
				droitsFonction.addObjectsFromArray(profil.allDroitsFonction(app));
			}
		}
		return droitsFonction.immutableClone();
	}
	
	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @return les infos sur les droits qu'a une personne sur des fonctions (sans notion de droits hérités d'un profil pére)
	 */
	public static NSArray<EOGdProfilDroitFonction> droitsFonctionForPersonneProfilPasHerite(EOEditingContext ec, String appStrId, Integer persId) {
		NSArray<EOGdProfil> profils = profilsForPersonne(ec, persId);
		
		EOQualifier qualApp = EOGdProfilDroitFonction.getQualifierProfilDroitFonctionApp(appStrId);
		EOQualifier qualProfil = ERXQ.in(EOGdProfilDroitFonction.TO_GD_PROFIL_KEY , profils);
		NSArray<String> prefetchKeyPaths = EOGdProfilDroitFonction.getKeyPathFonctionEtTypeDroit();
		NSArray<EOGdProfilDroitFonction> profilFonctions = EOGdProfilDroitFonction.fetchAll(ec, ERXQ.and(qualApp, qualProfil), null, false, false, prefetchKeyPaths);
		
		return profilFonctions.immutableClone();
	}
	
	/**
	 * @param ec un editing context
	 * @param appStrId id de l'application
	 * @param profilId id du profil
	 * @return les infos sur les droits d'un profil donné sur des fonctions
	 */
	public static NSArray<EOGdProfilDroitFonction> droitsFonctionForProfil(EOEditingContext ec, String appStrId, Integer profilId) {
		EOGdProfil profil = EOGdProfil.fetchByKeyValue(ec, EOGdProfil.ENTITY_PRIMARY_KEY, profilId);
		
		NSMutableArray<EOGdProfilDroitFonction> droitsFonction = new NSMutableArray<EOGdProfilDroitFonction>();
		EOGdApplication app = getGdApplication(ec, appStrId);
		if (app != null) {
			droitsFonction.addObjectsFromArray(profil.allDroitsFonction(app));
		}
		return droitsFonction.immutableClone();
	}
	
	/**
	 * 
	 * @param ec un editing context
	 * @param appStrId id de l'application
	 * @param profilId id du profil
	 * @return les infos sur les droits d'un profil donné sur des fonction (Pas de notion de profil hérité)
	 */
	public static NSArray<EOGdProfilDroitFonction> droitsFonctionForProfilPasHerite(EOEditingContext ec, String appStrId, Integer profilId) {
		
		EOQualifier qualApp = EOGdProfilDroitFonction.getQualifierProfilDroitFonctionApp(appStrId);
		EOQualifier qualProfil = ERXQ.equals(EOGdProfilDroitFonction.TO_GD_PROFIL_KEY + "." + EOGdProfil.PR_ID_KEY , profilId);
		NSArray<String> prefetchKeyPaths = EOGdProfilDroitFonction.getKeyPathFonctionEtTypeDroit();
		NSArray<EOGdProfilDroitFonction> profilFonctions = EOGdProfilDroitFonction.fetchAll(ec, ERXQ.and(qualApp, qualProfil), null, false, false, prefetchKeyPaths);
		
		return profilFonctions.immutableClone();
	}

	
	
	
	/**
	 * 
	 * @param ec un editing context 
	 * @param appStrId id de l'application
	 * @return les infos sur les droits d'une appli donnée sur des fonctions
	 */
	public static NSArray<EOGdProfilDroitFonction> droitsFonctionForApplication(EOEditingContext ec, String appStrId) {
		EOQualifier qual = EOGdProfilDroitFonction.getQualifierProfilDroitFonctionApp(appStrId);
		NSArray<EOGdProfilDroitFonction> profilFonctions = EOGdProfilDroitFonction.fetchAll(ec, qual);
		return profilFonctions;
	}

	
	

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @return les infos sur les droits qu'a une personne sur des fonctions
	 */
	public static NSArray<EOGdPerimetre> droitsPerimetresForPersonne(EOEditingContext ec, String appStrId, Integer persId) {
		NSArray<EOGdProfil> profils = profilsForPersonne(ec, persId);
		NSMutableArray<EOGdPerimetre> droitsPerimetre = new NSMutableArray<EOGdPerimetre>();
		EOGdApplication app = getGdApplication(ec, appStrId);
		if (app != null) {
			for (EOGdProfil profil : profils) {
				droitsPerimetre.addObjectsFromArray(profil.allPerimetres(app));		
			}
		}
		return droitsPerimetre.immutableClone();
	}

	/**
	 * @param ec
	 * @param appStrId :
	 * @return l'application correspondant à appStrId si elle existe
	 */
	private static EOGdApplication getGdApplication(EOEditingContext ec, String appStrId) {
		EOGdApplication app = null;
		if (appStrId != null)
			app = EOGdApplication.fetchFirstByQualifier(ec, ERXQ.equals(EOGdApplication.APP_STR_ID_KEY, appStrId));
		return app;
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param droit code du type de droit sur une donnee ('C','R','U','D')
	 * @param donnee la donnee
	 * @return true si la personne a le droit <code>droit</code> sur la donnée <code>donnee</code>. Cette méthode
	 *         prend en compte les interdictions.
	 */
	private static boolean personneHasDroitOnDonnee(EOEditingContext ec, Integer persId, String droit, EOGdDonnee donnee) {
		NSArray<EOGdProfilDroitDonnee> droitsDonnee = droitsDonneeForPersonne(ec, donnee.toGdApplication().appStrId(), persId);
		boolean hasDroit = false;
		for (EOGdProfilDroitDonnee droitDonnee : droitsDonnee) {
			if (donnee.equals(droitDonnee.toGdDonnee())) {
				if (droitDonnee.toGdTypeDroitDonnee().isTypeInterdiction()) {
					hasDroit = false;
					break;
				}
				if (droit.equals(droitDonnee.toGdTypeDroitDonnee().tddStrId()))
					hasDroit = true;
			}
		}
		return hasDroit;
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param donnee la donnee
	 * @return true si la personne n'a pas de droit sur la donnée <code>donnee</code> : si elle ne possède pas de droit
	 *         de type CRUD ou si c'est explicitement interdit (droit 'N')
	 */
	public static boolean personneHasNoDroitOnDonnee(EOEditingContext ec, Integer persId, EOGdDonnee donnee) {
		NSArray<EOGdProfilDroitDonnee> droitsDonnee = droitsDonneeForPersonne(ec, donnee.toGdApplication().appStrId(), persId);
		boolean hasNoDroit = true;
		for (EOGdProfilDroitDonnee droitDonnee : droitsDonnee) {
			if (donnee.equals(droitDonnee.toGdDonnee())) {
				if (droitDonnee.toGdTypeDroitDonnee().isTypeInterdiction()) {
					hasNoDroit = true;
					break;
				} else {
					hasNoDroit = false;
				}
			}
		}
		return hasNoDroit;
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param donnee la donnee
	 * @return true si la personne a le droit de lecture sur la donnee <code>donnee</code>
	 */
	public static boolean personneHasDroitLectureOnDonnee(EOEditingContext ec, Integer persId, EOGdDonnee donnee) {
		return personneHasDroitOnDonnee(ec, persId, EOGdTypeDroitDonnee.STR_ID_R, donnee);
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param donnee la donnee
	 * @return true si la personne a le droit de création sur la donnee <code>donnee</code>
	 */
	public static boolean personneHasDroitCreationOnDonnee(EOEditingContext ec, Integer persId, EOGdDonnee donnee) {
		return personneHasDroitOnDonnee(ec, persId, EOGdTypeDroitDonnee.STR_ID_C, donnee);
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param donnee la donnee
	 * @return true si la personne a le droit de suppression sur la donnee <code>donnee</code>
	 */
	public static boolean personneHasDroitSuppressionOnDonnee(EOEditingContext ec, Integer persId, EOGdDonnee donnee) {
		return personneHasDroitOnDonnee(ec, persId, EOGdTypeDroitDonnee.STR_ID_D, donnee);
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param donnee la donnee
	 * @return true si la personne a le droit de maj sur la donnee <code>donnee</code>
	 */
	public static boolean personneHasDroitModificationOnDonnee(EOEditingContext ec, Integer persId, EOGdDonnee donnee) {
		return personneHasDroitOnDonnee(ec, persId, EOGdTypeDroitDonnee.STR_ID_U, donnee);
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param droit code du type de droit sur une fonction : ('U', 'K')
	 * @param fonction la fonction
	 * @return true si la personne a le droit <code>droit</code> sur la donnée <code>donnee</code>. Cette méthode
	 *         prend en compte les interdictions.
	 */
	private static boolean personneHasDroitOnFonction(EOEditingContext ec, Integer persId, String droit, EOGdFonction fonction) {
		NSArray<EOGdProfilDroitFonction> droitsFonction = droitsFonctionForPersonne(ec, fonction.toGdApplication().appStrId(), persId);
		boolean hasDroit = false;
		for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
			if (fonction.equals(droitFonction.toGdFonction())) {
				if (droitFonction.toGdTypeDroitFonction().isTypeInterdiction()) {
					hasDroit = false;
					break;
				}
				if (droit.equals(droitFonction.toGdTypeDroitFonction().tdfStrId()))
					hasDroit = true;
			}
		}
		return hasDroit;
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param fonction la fonction
	 * @return true si la personne n'a pas de droit sur la fonction <code>fonction</code> : si elle ne possède pas de droit
	 *         de type ('U','K') ou si c'est explicitement interdit (droit 'N')
	 */
	public static boolean personneHasNoDroitOnFonction(EOEditingContext ec, Integer persId, EOGdFonction fonction) {
		NSArray<EOGdProfilDroitFonction> droitsFonction = droitsFonctionForPersonne(ec, fonction.toGdApplication().appStrId(), persId);
		boolean hasNoDroit = true;
		for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
			if (fonction.equals(droitFonction.toGdFonction())) {
				if (droitFonction.toGdTypeDroitFonction().isTypeInterdiction()) {
					hasNoDroit = true;
					break;
				} else {
					hasNoDroit = false;
				}
			}
		}
		return hasNoDroit;
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param fonction la fonction
	 * @return true si la personne a le droit de connaître la fonction
	 */
	public static boolean personneHasDroitConnaissanceOnFonction(EOEditingContext ec, Integer persId, EOGdFonction fonction) {
		return personneHasDroitOnFonction(ec, persId, EOGdTypeDroitFonction.STR_ID_K, fonction);
	}

	/**
	 * @param ec un editing context
	 * @param persId persId de la personne
	 * @param fonction la fonction
	 * @return true si la personne a le droit d'utiliser la fonction
	 */
	public static boolean personneHasDroitUtilisationOnFonction(EOEditingContext ec, Integer persId, EOGdFonction fonction) {
		return personneHasDroitOnFonction(ec, persId, EOGdTypeDroitFonction.STR_ID_U, fonction);
	}

	private static final String SQL_PERS_WITH_PROFIL =
	        "SELECT DISTINCT gdp.PERS_ID FROM GRHUM.GROUPE_DYNAMIQUE gd, GRHUM.GRP_DYNA_PERSONNE gdp, GRHUM.GD_PROFIL pf " +
	                "WHERE gdp.grpd_id = gd.grpd_id AND pf.grpd_id = gd.grpd_id AND pf.pr_id = $pr_id";

	/**
	 * @param ec un editing context
	 * @param profil le profil
	 * @return la liste des personnes ayant le profil <code>profil</code>
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<Integer> personnesWithProfil(EOEditingContext ec, EOGdProfil profil) {
		// Etant donné qu'on ne modélise pas GROUPE_DYNAMIQUE ->> PROFILS, on fait du SQL, raw rows
		String sql = SQL_PERS_WITH_PROFIL.replace("$pr_id", profil.primaryKey());
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(ec, "FwkCktlPersonne", sql, null);
		NSArray<BigDecimal> persIds = (NSArray<BigDecimal>) rawRows.valueForKey(EOGroupeDynamiquePersonne.PERS_ID_COLKEY);
		// Visiblement, avec WO, les NUMBER sont renvoyés comme des BigDecimal quand on utilise du rawRows
		NSArray<Integer> persIdsAsInteger = convertNSArrayBigDecimalEnInteger(persIds);
		return persIdsAsInteger;
	}

	private static NSArray<Integer> convertNSArrayBigDecimalEnInteger(NSArray<BigDecimal> persIds) {
		NSArray<Integer> persIdsAsInteger = new NSMutableArray<Integer>();
		for (BigDecimal bd : persIds) {
			persIdsAsInteger.add(bd.intValue());
		}
		return persIdsAsInteger;
	}

	private static final String SQL_DROIT_ON_DONNEE =
	        "SELECT DISTINCT gdp.PERS_ID FROM GRHUM.GD_PROFIL pr, GRHUM.GD_PROFIL pr2, " +
	                "GRHUM.GD_PROFIL_DROIT_DONNEE pdd, GRHUM.GD_TYPE_DROIT_DONNEE tdd, " +
	                "GRHUM.GROUPE_DYNAMIQUE gd, GRHUM.GRP_DYNA_PERSONNE gdp " +
	                "WHERE (" +
	                "gdp.grpd_id = gd.grpd_id and gd.grpd_id = pr.grpd_id and  " +
	                "pdd.pr_id = pr.pr_id and pdd.don_id = $don_id and " +
	                "pdd.tdd_id = tdd.tdd_id and tdd.tdd_str_id = '$tdd_str_id'" +
	                ") OR (" +
	                "pr.pr_pere_id = pr2.pr_id and gdp.grpd_id = gd.grpd_id and " +
	                "gd.grpd_id = pr2.grpd_id and  pdd.pr_id = pr2.pr_id and " +
	                "pdd.don_id = $don_id and pdd.tdd_id = tdd.tdd_id and " +
	                "tdd.tdd_str_id = '$tdd_str_id')";

	@SuppressWarnings("unchecked")
	private static NSArray<Integer> personnesWithInterdictionOnDonnee(EOEditingContext ec, EOGdDonnee donnee) {
		String sql = SQL_DROIT_ON_DONNEE.replace("$don_id", donnee.primaryKey()).replace("$tdd_str_id", EOGdTypeDroitDonnee.STR_ID_N);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(ec, "FwkCktlPersonne", sql, null);
		NSArray<Integer> persIds = (NSArray<Integer>) rawRows.valueForKey(EOGroupeDynamiquePersonne.PERS_ID_COLKEY);
		return persIds;
	}

	/**
	 * @param ec un editing context
	 * @param droit code du type de droit sur une donnee ('C','R','U','D')
	 * @param donnee une donnee
	 * @return la liste des persId des personnes ayant le droit <code>droit</code> sur la donnee <code>donneee</code>
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<Integer> personnesWithDroitOnDonnee(EOEditingContext ec, String droit, EOGdDonnee donnee) {
		// On récupère les persId qui font partie des groupes dynamiques dont le profil ou le parent possède 
		// un droit sur la donnée (faire un test de perfs là dessus)...
		String sql = SQL_DROIT_ON_DONNEE.replace("$don_id", donnee.primaryKey()).replace("$tdd_str_id", droit);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(ec, "FwkCktlPersonne", sql, null);
		NSArray<Integer> persIds = (NSArray<Integer>) rawRows.valueForKey(EOGroupeDynamiquePersonne.PERS_ID_COLKEY);
		NSArray<Integer> persIdsInterdits = personnesWithInterdictionOnDonnee(ec, donnee);
		return ERXArrayUtilities.arrayMinusArray(persIds, persIdsInterdits);
	}

	private static final String SQL_DROIT_ON_FONCTION =
	        "SELECT DISTINCT gdp.PERS_ID FROM GRHUM.GD_PROFIL pr, GRHUM.GD_PROFIL pr2, " +
	                "GRHUM.GD_PROFIL_DROIT_FONCTION pdf, GRHUM.GD_TYPE_DROIT_FONCTION tdf, " +
	                "GRHUM.GROUPE_DYNAMIQUE gd, GRHUM.GRP_DYNA_PERSONNE gdp " +
	                "WHERE (" +
	                "gdp.grpd_id = gd.grpd_id and gd.grpd_id = pr.grpd_id and  " +
	                "pdf.pr_id = pr.pr_id and pdf.fon_id = $fon_id and " +
	                "pdf.tdf_id = tdf.tdf_id and tdf.tdf_str_id = '$tdf_str_id'" +
	                ") OR (" +
	                "pr.pr_pere_id = pr2.pr_id and gdp.grpd_id = gd.grpd_id and " +
	                "gd.grpd_id = pr2.grpd_id and  pdf.pr_id = pr2.pr_id and " +
	                "pdf.fon_id = $fon_id and pdf.tdf_id = tdf.tdf_id and " +
	                "tdf.tdf_str_id = '$tdf_str_id')";

	@SuppressWarnings("unchecked")
	private static NSArray<Integer> personnesWithInterdictionOnFonction(EOEditingContext ec, EOGdFonction fonction) {
		String sql = SQL_DROIT_ON_FONCTION.replace("$fon_id", fonction.primaryKey()).replace("$tdf_str_id", EOGdTypeDroitFonction.STR_ID_N);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(ec, "FwkCktlPersonne", sql, null);
		NSArray<Integer> persIds = (NSArray<Integer>) rawRows.valueForKey(EOGroupeDynamiquePersonne.PERS_ID_COLKEY);
		return persIds;
	}

	/**
	 * @param ec un editing context
	 * @param droit code du type de droit sur une fonction ('U','K')
	 * @param fonction une fonction
	 * @return la liste des persId des personnes ayant le droit <code>droit</code> sur la fonction <code>fonction</code>
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<Integer> personnesWithDroitOnFonction(EOEditingContext ec, String droit, EOGdFonction fonction) {
		// On récupère les persId qui font partie des groupes dynamiques dont le profil ou le parent possède 
		// un droit sur la fonction (faire un test de perfs là dessus)...
		String sql = SQL_DROIT_ON_FONCTION.replace("$fon_id", fonction.primaryKey()).replace("$tdf_str_id", droit);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(ec, "FwkCktlPersonne", sql, null);
		NSArray<Integer> persIds = (NSArray<Integer>) rawRows.valueForKey(EOGroupeDynamiquePersonne.PERS_ID_COLKEY);
		NSArray<Integer> persIdsInterdits = personnesWithInterdictionOnFonction(ec, fonction);
		return ERXArrayUtilities.arrayMinusArray(persIds, persIdsInterdits);
	}

	/**
	 * @param persIds liste de persIds
	 * @return la liste des personnes correspondant aux persIds passés en paramètre. Attention cette méthode peut-être
	 *         inefficiante en terme CPU et mémoire si utilisé avec un nombre d'éléments "importants".
	 * @throws TooManyResultsException
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<? extends IPersonne> personnesForPersIds(EOEditingContext ec, NSArray<Integer> persIds) {
		if (persIds.count() > 20000)
			throw new TooManyResultsException("Vous allez faire péter la mémoire en remontant plus de 20000 personnes !");
		ERXFetchSpecification<EOIndividu> fspecInd = new ERXFetchSpecification<EOIndividu>(
		        EOIndividu.ENTITY_NAME, ERXQ.in(EOIndividu.PERS_ID_KEY, persIds), null);
		ERXFetchSpecification<EOStructure> fspecStr = new ERXFetchSpecification<EOStructure>(
		        EOStructure.ENTITY_NAME, ERXQ.in(EOStructure.PERS_ID_KEY, persIds), null);
		NSArray<? extends IPersonne> personnes = NSArray.EmptyArray;
		NSArray<EOStructure> structures = fspecStr.fetchObjects(ec);
		NSArray<EOIndividu> individus = fspecInd.fetchObjects(ec);
		personnes = ERXArrayUtilities.arrayByAddingObjectsFromArrayWithoutDuplicates(individus, structures);
		return personnes;
	}

}
