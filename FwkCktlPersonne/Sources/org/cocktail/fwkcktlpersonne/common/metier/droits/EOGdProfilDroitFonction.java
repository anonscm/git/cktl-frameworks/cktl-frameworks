/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;
import er.extensions.validation.ERXValidationFactory;

public class EOGdProfilDroitFonction extends _EOGdProfilDroitFonction {

    public EOGdProfilDroitFonction() {
        super();
    }

    @Override
    public void awakeFromInsertion(EOEditingContext ec) {
        setDateCreation(new NSTimestamp());
        super.awakeFromInsertion(ec);
    }

    /**
     * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
     * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
        try {
            checkUsers();
        } catch (Exception e) {
            NSValidation.ValidationException e1 = new NSValidation.ValidationException(this.getClass().getSimpleName() + " : " + toDisplayString() + ":" + e.getLocalizedMessage());
            throw e1;
        }
        super.validateObjectMetier();
    }

    /**
     * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
     * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        setDateModification(new NSTimestamp());
        // On vérifie si le nouveau droit n'existe pas déjà pour ce profil
        EOQualifier droitQual = ERXQ.equals(EOGdProfilDroitFonction.TO_GD_FONCTION_KEY, this.toGdFonction())
                                    .and(ERXQ.equals(EOGdProfilDroitFonction.TO_GD_TYPE_DROIT_FONCTION_KEY, this.toGdTypeDroitFonction()));
        NSArray<EOGdProfilDroitFonction> existingDroits = ERXQ.filtered(toGdProfil().droitFonctionsHerites(null, null), droitQual);
        if (existingDroits.count() > 0)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ExistingDroitHerite");
        existingDroits = ERXQ.filtered(toGdProfil().droitFonctions(null, null), droitQual);
        if (existingDroits.count() > 1)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ExistingDroit");
        super.validateBeforeTransactionSave();
    }

    public String toDisplayString() {
        return toString();
    }

    /**
     * Verifie si les utilisateurs affectes a persIdCreation ont bien le droits de creer l'entité.
     * 
     * @throws NSValidation.ValidationException
     */
    public void checkUsers() throws NSValidation.ValidationException {
        if (getCreateur() == null) {
            throw new NSValidation.ValidationException("La reference au createur (persIdCreation) est obligatoire.");
        }
    }

    public boolean isTypeUtilisation() {
        return toGdTypeDroitFonction() != null && toGdTypeDroitFonction().isTypeUtilisation();
    }
    
    public boolean isTypeConnaissance() {
        return toGdTypeDroitFonction() != null && toGdTypeDroitFonction().isTypeConnaissance();
    }
    
    public boolean isTypeInterdiction() {
        return toGdTypeDroitFonction() != null && toGdTypeDroitFonction().isTypeInterdiction();
    }
    
    /**
     * @param ec un editing context 
     * @param persId le persId de l'utilisateur
     * @return un nouveau droit 
     */
    public static EOGdProfilDroitFonction creer(EOEditingContext ec, Integer persId) {
        EOGdProfilDroitFonction newDroitFonction = EOGdProfilDroitFonction.creerInstance(ec);
        newDroitFonction.setPersIdCreation(persId);
        return newDroitFonction;
    }
    
    public static NSArray<String> getKeyPathFonctionEtTypeDroit() {
		NSArray<String> prefetchKeyPaths = new NSMutableArray<String>();
		prefetchKeyPaths.add(EOGdProfilDroitFonction.TO_GD_FONCTION_KEY);
		prefetchKeyPaths.add(EOGdProfilDroitFonction.TO_GD_TYPE_DROIT_FONCTION_KEY);
		return prefetchKeyPaths;
	}
    
    public static EOQualifier getQualifierProfilDroitFonctionApp(String appStrId) {
		return ERXQ.equals(EOGdProfilDroitFonction.TO_GD_FONCTION_KEY + "." + EOGdFonction.TO_GD_APPLICATION_KEY + "." + EOGdApplication.APP_STR_ID_KEY, appStrId);
	}
    
    
}
