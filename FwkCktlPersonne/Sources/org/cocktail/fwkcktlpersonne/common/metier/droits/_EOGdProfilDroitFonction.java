/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdProfilDroitFonction.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdProfilDroitFonction extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdProfilDroitFonction.class);

	public static final String ENTITY_NAME = "GdProfilDroitFonction";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_PROFIL_DROIT_FONCTION";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> TO_GD_FONCTION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>("toGdFonction");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> TO_GD_PROFIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("toGdProfil");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction> TO_GD_TYPE_DROIT_FONCTION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction>("toGdTypeDroitFonction");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pdfId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";

// Attributs non visibles
	public static final String FON_ID_KEY = "fonId";
	public static final String PDF_ID_KEY = "pdfId";
	public static final String PR_ID_KEY = "prId";
	public static final String TDF_ID_KEY = "tdfId";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";

	public static final String FON_ID_COLKEY = "FON_ID";
	public static final String PDF_ID_COLKEY = "PDF_ID";
	public static final String PR_ID_COLKEY = "PR_ID";
	public static final String TDF_ID_COLKEY = "TDF_ID";


	// Relationships
	public static final String TO_GD_FONCTION_KEY = "toGdFonction";
	public static final String TO_GD_PROFIL_KEY = "toGdProfil";
	public static final String TO_GD_TYPE_DROIT_FONCTION_KEY = "toGdTypeDroitFonction";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction toGdFonction() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction)storedValueForKey(TO_GD_FONCTION_KEY);
  }

  public void setToGdFonctionRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction oldValue = toGdFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_FONCTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toGdProfil() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil)storedValueForKey(TO_GD_PROFIL_KEY);
  }

  public void setToGdProfilRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil oldValue = toGdProfil();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_PROFIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_PROFIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction toGdTypeDroitFonction() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction)storedValueForKey(TO_GD_TYPE_DROIT_FONCTION_KEY);
  }

  public void setToGdTypeDroitFonctionRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction oldValue = toGdTypeDroitFonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_TYPE_DROIT_FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_TYPE_DROIT_FONCTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOGdProfilDroitFonction avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdProfilDroitFonction createEOGdProfilDroitFonction(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer persIdCreation
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction toGdFonction, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toGdProfil, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction toGdTypeDroitFonction			) {
    EOGdProfilDroitFonction eo = (EOGdProfilDroitFonction) createAndInsertInstance(editingContext, _EOGdProfilDroitFonction.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setPersIdCreation(persIdCreation);
    eo.setToGdFonctionRelationship(toGdFonction);
    eo.setToGdProfilRelationship(toGdProfil);
    eo.setToGdTypeDroitFonctionRelationship(toGdTypeDroitFonction);
    return eo;
  }

  
	  public EOGdProfilDroitFonction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdProfilDroitFonction)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdProfilDroitFonction creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdProfilDroitFonction creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdProfilDroitFonction object = (EOGdProfilDroitFonction)createAndInsertInstance(editingContext, _EOGdProfilDroitFonction.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdProfilDroitFonction localInstanceIn(EOEditingContext editingContext, EOGdProfilDroitFonction eo) {
    EOGdProfilDroitFonction localInstance = (eo == null) ? null : (EOGdProfilDroitFonction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdProfilDroitFonction#localInstanceIn a la place.
   */
	public static EOGdProfilDroitFonction localInstanceOf(EOEditingContext editingContext, EOGdProfilDroitFonction eo) {
		return EOGdProfilDroitFonction.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdProfilDroitFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdProfilDroitFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdProfilDroitFonction> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdProfilDroitFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdProfilDroitFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdProfilDroitFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdProfilDroitFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdProfilDroitFonction> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdProfilDroitFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdProfilDroitFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdProfilDroitFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdProfilDroitFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdProfilDroitFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdProfilDroitFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
