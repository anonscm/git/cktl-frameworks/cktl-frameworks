/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdDonnee.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdDonnee extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdDonnee.class);

	public static final String ENTITY_NAME = "GdDonnee";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_DONNEE";


// Attribute Keys
  public static final ERXKey<String> DON_CATEGORIE = new ERXKey<String>("donCategorie");
  public static final ERXKey<String> DON_DESCRIPTION = new ERXKey<String>("donDescription");
  public static final ERXKey<String> DON_ID_INTERNE = new ERXKey<String>("donIdInterne");
  public static final ERXKey<String> DON_LC = new ERXKey<String>("donLc");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> TO_GD_APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("toGdApplication");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "donId";

	public static final String DON_CATEGORIE_KEY = "donCategorie";
	public static final String DON_DESCRIPTION_KEY = "donDescription";
	public static final String DON_ID_INTERNE_KEY = "donIdInterne";
	public static final String DON_LC_KEY = "donLc";

// Attributs non visibles
	public static final String APP_ID_KEY = "appId";
	public static final String DON_ID_KEY = "donId";

//Colonnes dans la base de donnees
	public static final String DON_CATEGORIE_COLKEY = "DON_CATEGORIE";
	public static final String DON_DESCRIPTION_COLKEY = "DON_DESCRIPTION";
	public static final String DON_ID_INTERNE_COLKEY = "DON_ID_INTERNE";
	public static final String DON_LC_COLKEY = "DON_LC";

	public static final String APP_ID_COLKEY = "APP_ID";
	public static final String DON_ID_COLKEY = "DON_ID";


	// Relationships
	public static final String TO_GD_APPLICATION_KEY = "toGdApplication";



	// Accessors methods
  public String donCategorie() {
    return (String) storedValueForKey(DON_CATEGORIE_KEY);
  }

  public void setDonCategorie(String value) {
    takeStoredValueForKey(value, DON_CATEGORIE_KEY);
  }

  public String donDescription() {
    return (String) storedValueForKey(DON_DESCRIPTION_KEY);
  }

  public void setDonDescription(String value) {
    takeStoredValueForKey(value, DON_DESCRIPTION_KEY);
  }

  public String donIdInterne() {
    return (String) storedValueForKey(DON_ID_INTERNE_KEY);
  }

  public void setDonIdInterne(String value) {
    takeStoredValueForKey(value, DON_ID_INTERNE_KEY);
  }

  public String donLc() {
    return (String) storedValueForKey(DON_LC_KEY);
  }

  public void setDonLc(String value) {
    takeStoredValueForKey(value, DON_LC_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication)storedValueForKey(TO_GD_APPLICATION_KEY);
  }

  public void setToGdApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication oldValue = toGdApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_APPLICATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOGdDonnee avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdDonnee createEOGdDonnee(EOEditingContext editingContext, String donCategorie
, String donIdInterne
, String donLc
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication			) {
    EOGdDonnee eo = (EOGdDonnee) createAndInsertInstance(editingContext, _EOGdDonnee.ENTITY_NAME);    
		eo.setDonCategorie(donCategorie);
		eo.setDonIdInterne(donIdInterne);
		eo.setDonLc(donLc);
    eo.setToGdApplicationRelationship(toGdApplication);
    return eo;
  }

  
	  public EOGdDonnee localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdDonnee)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDonnee creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdDonnee creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdDonnee object = (EOGdDonnee)createAndInsertInstance(editingContext, _EOGdDonnee.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdDonnee localInstanceIn(EOEditingContext editingContext, EOGdDonnee eo) {
    EOGdDonnee localInstance = (eo == null) ? null : (EOGdDonnee)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdDonnee#localInstanceIn a la place.
   */
	public static EOGdDonnee localInstanceOf(EOEditingContext editingContext, EOGdDonnee eo) {
		return EOGdDonnee.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdDonnee fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdDonnee fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdDonnee> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdDonnee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdDonnee)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdDonnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdDonnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdDonnee> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdDonnee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdDonnee)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdDonnee fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdDonnee eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdDonnee ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdDonnee fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
