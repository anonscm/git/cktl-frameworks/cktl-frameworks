/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdApplication.java instead.
package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOGdApplication extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOGdApplication.class);

	public static final String ENTITY_NAME = "GdApplication";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GD_APPLICATION";


// Attribute Keys
  public static final ERXKey<String> APP_LC = new ERXKey<String>("appLc");
  public static final ERXKey<String> APP_STR_ID = new ERXKey<String>("appStrId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine> TO_GD_DOMAINE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine>("toGdDomaine");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> TO_GD_DONNEES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee>("toGdDonnees");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> TO_GD_DONNEE_STATS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat>("toGdDonneeStats");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> TO_GD_FONCTIONS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>("toGdFonctions");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "appId";

	public static final String APP_LC_KEY = "appLc";
	public static final String APP_STR_ID_KEY = "appStrId";

// Attributs non visibles
	public static final String APP_ID_KEY = "appId";
	public static final String DOM_ID_KEY = "domId";

//Colonnes dans la base de donnees
	public static final String APP_LC_COLKEY = "APP_LC";
	public static final String APP_STR_ID_COLKEY = "APP_STR_ID";

	public static final String APP_ID_COLKEY = "APP_ID";
	public static final String DOM_ID_COLKEY = "DOM_ID";


	// Relationships
	public static final String TO_GD_DOMAINE_KEY = "toGdDomaine";
	public static final String TO_GD_DONNEES_KEY = "toGdDonnees";
	public static final String TO_GD_DONNEE_STATS_KEY = "toGdDonneeStats";
	public static final String TO_GD_FONCTIONS_KEY = "toGdFonctions";



	// Accessors methods
  public String appLc() {
    return (String) storedValueForKey(APP_LC_KEY);
  }

  public void setAppLc(String value) {
    takeStoredValueForKey(value, APP_LC_KEY);
  }

  public String appStrId() {
    return (String) storedValueForKey(APP_STR_ID_KEY);
  }

  public void setAppStrId(String value) {
    takeStoredValueForKey(value, APP_STR_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine toGdDomaine() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine)storedValueForKey(TO_GD_DOMAINE_KEY);
  }

  public void setToGdDomaineRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine oldValue = toGdDomaine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GD_DOMAINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GD_DOMAINE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> toGdDonnees() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee>)storedValueForKey(TO_GD_DONNEES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> toGdDonnees(EOQualifier qualifier) {
    return toGdDonnees(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> toGdDonnees(EOQualifier qualifier, boolean fetch) {
    return toGdDonnees(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> toGdDonnees(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee.TO_GD_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdDonnees();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_DONNEES_KEY);
  }

  public void removeFromToGdDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_DONNEES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee createToGdDonneesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdDonnee");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_DONNEES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee) eo;
  }

  public void deleteToGdDonneesRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_DONNEES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdDonneesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonnee> objects = toGdDonnees().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdDonneesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> toGdDonneeStats() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat>)storedValueForKey(TO_GD_DONNEE_STATS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> toGdDonneeStats(EOQualifier qualifier) {
    return toGdDonneeStats(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> toGdDonneeStats(EOQualifier qualifier, boolean fetch) {
    return toGdDonneeStats(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> toGdDonneeStats(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat.TO_GD_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdDonneeStats();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdDonneeStatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_DONNEE_STATS_KEY);
  }

  public void removeFromToGdDonneeStatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_DONNEE_STATS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat createToGdDonneeStatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdDonneeStat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_DONNEE_STATS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat) eo;
  }

  public void deleteToGdDonneeStatsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_DONNEE_STATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdDonneeStatsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDonneeStat> objects = toGdDonneeStats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdDonneeStatsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> toGdFonctions() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>)storedValueForKey(TO_GD_FONCTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> toGdFonctions(EOQualifier qualifier) {
    return toGdFonctions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> toGdFonctions(EOQualifier qualifier, boolean fetch) {
    return toGdFonctions(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> toGdFonctions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction.TO_GD_APPLICATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGdFonctions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGdFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GD_FONCTIONS_KEY);
  }

  public void removeFromToGdFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_FONCTIONS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction createToGdFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GdFonction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GD_FONCTIONS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction) eo;
  }

  public void deleteToGdFonctionsRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GD_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGdFonctionsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdFonction> objects = toGdFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGdFonctionsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOGdApplication avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGdApplication createEOGdApplication(EOEditingContext editingContext, String appLc
, String appStrId
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdDomaine toGdDomaine			) {
    EOGdApplication eo = (EOGdApplication) createAndInsertInstance(editingContext, _EOGdApplication.ENTITY_NAME);    
		eo.setAppLc(appLc);
		eo.setAppStrId(appStrId);
    eo.setToGdDomaineRelationship(toGdDomaine);
    return eo;
  }

  
	  public EOGdApplication localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGdApplication)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdApplication creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGdApplication creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOGdApplication object = (EOGdApplication)createAndInsertInstance(editingContext, _EOGdApplication.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGdApplication localInstanceIn(EOEditingContext editingContext, EOGdApplication eo) {
    EOGdApplication localInstance = (eo == null) ? null : (EOGdApplication)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGdApplication#localInstanceIn a la place.
   */
	public static EOGdApplication localInstanceOf(EOEditingContext editingContext, EOGdApplication eo) {
		return EOGdApplication.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGdApplication fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGdApplication fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOGdApplication> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGdApplication)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGdApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGdApplication fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOGdApplication> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdApplication eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGdApplication)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGdApplication fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGdApplication eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGdApplication ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGdApplication fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
