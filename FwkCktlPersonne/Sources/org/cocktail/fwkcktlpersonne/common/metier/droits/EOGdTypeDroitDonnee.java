/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier.droits;

import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class EOGdTypeDroitDonnee extends _EOGdTypeDroitDonnee {
    public static final String STR_ID_N = "N";
    public static final String STR_ID_R = "R";
    public static final String STR_ID_C = "C";
    public static final String STR_ID_U = "U";
    public static final String STR_ID_D = "D";

    private static NSArray<EOGdTypeDroitDonnee> TypesDroit;
    private static EOGdTypeDroitDonnee TypeDroitInterdiction;
    private static EOGdTypeDroitDonnee TypeDroitLecture;
    private static EOGdTypeDroitDonnee TypeDroitCreation;
    private static EOGdTypeDroitDonnee TypeDroitModification;
    private static EOGdTypeDroitDonnee TypeDroitSupression;
    
    
    public EOGdTypeDroitDonnee() {
        super();
    }

    public boolean isTypeCreation() {
        return STR_ID_C.equals(tddStrId());
    }
    
    public boolean isTypeLecture() {
        return STR_ID_R.equals(tddStrId());
    }
    
    public boolean isTypeModification() {
        return STR_ID_U.equals(tddStrId());
    }
    
    public boolean isTypeSuppression() {
        return STR_ID_D.equals(tddStrId());
    }
    
    public boolean isTypeInterdiction() {
        return STR_ID_N.equals(tddStrId());
    }
    
    /**
     * @return toutes les types droit fonction dans le cache
     */
    @SuppressWarnings("unchecked")
    public static NSArray<EOGdTypeDroitDonnee> getTypesDroitDonnee() {
        if (TypesDroit == null) {
            TypesDroit = EOGdTypeDroitDonnee.fetchAll(
                    EOSharedEditingContext.defaultSharedEditingContext(), ERXS.asc(TDD_LL_KEY).array());
        }
        return TypesDroit;
    }
    
    public static EOGdTypeDroitDonnee getTypeDroitInterdiction() {
        if (TypeDroitInterdiction == null) {
            TypeDroitInterdiction = ERXQ.first(getTypesDroitDonnee(), ERXQ.equals(TDD_STR_ID_KEY, STR_ID_N));
        }
        return TypeDroitInterdiction;
    }
    
    public static EOGdTypeDroitDonnee getTypeDroitLecture() {
        if (TypeDroitLecture == null) {
            TypeDroitLecture = ERXQ.first(getTypesDroitDonnee(), ERXQ.equals(TDD_STR_ID_KEY, STR_ID_R));
        }
        return TypeDroitLecture;
    }
    
    public static EOGdTypeDroitDonnee getTypeDroitCreation() {
        if (TypeDroitCreation == null) {
            TypeDroitCreation = ERXQ.first(getTypesDroitDonnee(), ERXQ.equals(TDD_STR_ID_KEY, STR_ID_C));
        }
        return TypeDroitCreation;
    }
    
    public static EOGdTypeDroitDonnee getTypeDroitModification() {
        if (TypeDroitModification == null) {
            TypeDroitModification = ERXQ.first(getTypesDroitDonnee(), ERXQ.equals(TDD_STR_ID_KEY, STR_ID_U));
        }
        return TypeDroitModification;
    }
    
    public static EOGdTypeDroitDonnee getTypeDroitSuppression() {
        if (TypeDroitSupression == null) {
            TypeDroitSupression = ERXQ.first(getTypesDroitDonnee(), ERXQ.equals(TDD_STR_ID_KEY, STR_ID_D));
        }
        return TypeDroitSupression;
    }
    
    
    
}
