/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEtudiant.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOEtudiant extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOEtudiant.class);

	public static final String ENTITY_NAME = "Fwkpers_Etudiant";
	public static final String ENTITY_TABLE_NAME = "GRHUM.ETUDIANT";


// Attribute Keys
  public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Long> ETUD_ANBAC = new ERXKey<Long>("etudAnbac");
  public static final ERXKey<Long> ETUD_ANNEE1INSC_SUP = new ERXKey<Long>("etudAnnee1inscSup");
  public static final ERXKey<Long> ETUD_ANNEE1INSC_ULR = new ERXKey<Long>("etudAnnee1inscUlr");
  public static final ERXKey<Long> ETUD_ANNEE1INSC_UNIV = new ERXKey<Long>("etudAnnee1inscUniv");
  public static final ERXKey<String> ETUD_CODE_INE = new ERXKey<String>("etudCodeIne");
  public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
  public static final ERXKey<String> ETUD_REIMMATRICULATION = new ERXKey<String>("etudReimmatriculation");
  public static final ERXKey<Long> ETUD_SITFAM = new ERXKey<Long>("etudSitfam");
  public static final ERXKey<Long> ETUD_SN_ATTESTATION = new ERXKey<Long>("etudSnAttestation");
  public static final ERXKey<Long> ETUD_SN_CERTIFICATION = new ERXKey<Long>("etudSnCertification");
  public static final ERXKey<Long> ETUD_SPORT_HN = new ERXKey<Long>("etudSportHn");
  public static final ERXKey<String> ETUD_VILLE_BAC = new ERXKey<String>("etudVilleBac");
  public static final ERXKey<String> MENT_CODE = new ERXKey<String>("mentCode");
  public static final ERXKey<String> PRO_CODE = new ERXKey<String>("proCode");
  public static final ERXKey<String> PRO_CODE2 = new ERXKey<String>("proCode2");
  public static final ERXKey<String> THEB_CODE = new ERXKey<String>("thebCode");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie> TO_ACADEMIE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAcademie>("toAcademie");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBac> TO_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBac>("toBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_DEPARTEMENT_ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toDepartementEtabBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_DEPARTEMENT_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toDepartementParent");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMention> TO_MENTION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMention>("toMention");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysEtabBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_CODE_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneCodeBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_CODE_SUP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneCodeSup");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "etudNumero";

	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETUD_ANBAC_KEY = "etudAnbac";
	public static final String ETUD_ANNEE1INSC_SUP_KEY = "etudAnnee1inscSup";
	public static final String ETUD_ANNEE1INSC_ULR_KEY = "etudAnnee1inscUlr";
	public static final String ETUD_ANNEE1INSC_UNIV_KEY = "etudAnnee1inscUniv";
	public static final String ETUD_CODE_INE_KEY = "etudCodeIne";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String ETUD_REIMMATRICULATION_KEY = "etudReimmatriculation";
	public static final String ETUD_SITFAM_KEY = "etudSitfam";
	public static final String ETUD_SN_ATTESTATION_KEY = "etudSnAttestation";
	public static final String ETUD_SN_CERTIFICATION_KEY = "etudSnCertification";
	public static final String ETUD_SPORT_HN_KEY = "etudSportHn";
	public static final String ETUD_VILLE_BAC_KEY = "etudVilleBac";
	public static final String MENT_CODE_KEY = "mentCode";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String PRO_CODE2_KEY = "proCode2";
	public static final String THEB_CODE_KEY = "thebCode";

// Attributs non visibles
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ETAB_CODE_SUP_KEY = "etabCodeSup";
	public static final String C_DEPARTEMENT_PARENT_KEY = "cDepartementParent";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String C_PAYS_ETAB_BAC_KEY = "cPaysEtabBac";
	public static final String ACAD_CODE_BAC_KEY = "acadCodeBac";
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String C_DEPARTEMENT_ETAB_BAC_KEY = "cDepartementEtabBac";

//Colonnes dans la base de donnees
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETUD_ANBAC_COLKEY = "ETUD_ANBAC";
	public static final String ETUD_ANNEE1INSC_SUP_COLKEY = "ETUD_ANNEE_1INSC_SUP";
	public static final String ETUD_ANNEE1INSC_ULR_COLKEY = "ETUD_ANNEE_1INSC_ULR";
	public static final String ETUD_ANNEE1INSC_UNIV_COLKEY = "ETUD_ANNEE_1INSC_UNIV";
	public static final String ETUD_CODE_INE_COLKEY = "ETUD_CODE_INE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String ETUD_REIMMATRICULATION_COLKEY = "ETUD_REIMMATRICULATION";
	public static final String ETUD_SITFAM_COLKEY = "ETUD_SITFAM";
	public static final String ETUD_SN_ATTESTATION_COLKEY = "ETUD_SN_ATTESTATION";
	public static final String ETUD_SN_CERTIFICATION_COLKEY = "ETUD_SN_CERTIFICATION";
	public static final String ETUD_SPORT_HN_COLKEY = "ETUD_SPORT_HN";
	public static final String ETUD_VILLE_BAC_COLKEY = "ETUD_VILLE_BAC";
	public static final String MENT_CODE_COLKEY = "MENT_CODE";
	public static final String PRO_CODE_COLKEY = "PRO_CODE";
	public static final String PRO_CODE2_COLKEY = "PRO_CODE2";
	public static final String THEB_CODE_COLKEY = "THEB_CODE";

	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ETAB_CODE_SUP_COLKEY = "ETAB_CODE_SUP";
	public static final String C_DEPARTEMENT_PARENT_COLKEY = "C_DEPARTEMENT_PARENT";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String C_PAYS_ETAB_BAC_COLKEY = "C_PAYS_ETAB_BAC";
	public static final String ACAD_CODE_BAC_COLKEY = "ACAD_CODE_BAC";
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String C_DEPARTEMENT_ETAB_BAC_COLKEY = "C_DEPARTEMENT_ETAB_BAC";


	// Relationships
	public static final String TO_ACADEMIE_KEY = "toAcademie";
	public static final String TO_BAC_KEY = "toBac";
	public static final String TO_DEPARTEMENT_ETAB_BAC_KEY = "toDepartementEtabBac";
	public static final String TO_DEPARTEMENT_PARENT_KEY = "toDepartementParent";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_MENTION_KEY = "toMention";
	public static final String TO_PAYS_ETAB_BAC_KEY = "toPaysEtabBac";
	public static final String TO_RNE_CODE_BAC_KEY = "toRneCodeBac";
	public static final String TO_RNE_CODE_SUP_KEY = "toRneCodeSup";



	// Accessors methods
  public Integer candNumero() {
    return (Integer) storedValueForKey(CAND_NUMERO_KEY);
  }

  public void setCandNumero(Integer value) {
    takeStoredValueForKey(value, CAND_NUMERO_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Long etudAnbac() {
    return (Long) storedValueForKey(ETUD_ANBAC_KEY);
  }

  public void setEtudAnbac(Long value) {
    takeStoredValueForKey(value, ETUD_ANBAC_KEY);
  }

  public Long etudAnnee1inscSup() {
    return (Long) storedValueForKey(ETUD_ANNEE1INSC_SUP_KEY);
  }

  public void setEtudAnnee1inscSup(Long value) {
    takeStoredValueForKey(value, ETUD_ANNEE1INSC_SUP_KEY);
  }

  public Long etudAnnee1inscUlr() {
    return (Long) storedValueForKey(ETUD_ANNEE1INSC_ULR_KEY);
  }

  public void setEtudAnnee1inscUlr(Long value) {
    takeStoredValueForKey(value, ETUD_ANNEE1INSC_ULR_KEY);
  }

  public Long etudAnnee1inscUniv() {
    return (Long) storedValueForKey(ETUD_ANNEE1INSC_UNIV_KEY);
  }

  public void setEtudAnnee1inscUniv(Long value) {
    takeStoredValueForKey(value, ETUD_ANNEE1INSC_UNIV_KEY);
  }

  public String etudCodeIne() {
    return (String) storedValueForKey(ETUD_CODE_INE_KEY);
  }

  public void setEtudCodeIne(String value) {
    takeStoredValueForKey(value, ETUD_CODE_INE_KEY);
  }

  public Integer etudNumero() {
    return (Integer) storedValueForKey(ETUD_NUMERO_KEY);
  }

  public void setEtudNumero(Integer value) {
    takeStoredValueForKey(value, ETUD_NUMERO_KEY);
  }

  public String etudReimmatriculation() {
    return (String) storedValueForKey(ETUD_REIMMATRICULATION_KEY);
  }

  public void setEtudReimmatriculation(String value) {
    takeStoredValueForKey(value, ETUD_REIMMATRICULATION_KEY);
  }

  public Long etudSitfam() {
    return (Long) storedValueForKey(ETUD_SITFAM_KEY);
  }

  public void setEtudSitfam(Long value) {
    takeStoredValueForKey(value, ETUD_SITFAM_KEY);
  }

  public Long etudSnAttestation() {
    return (Long) storedValueForKey(ETUD_SN_ATTESTATION_KEY);
  }

  public void setEtudSnAttestation(Long value) {
    takeStoredValueForKey(value, ETUD_SN_ATTESTATION_KEY);
  }

  public Long etudSnCertification() {
    return (Long) storedValueForKey(ETUD_SN_CERTIFICATION_KEY);
  }

  public void setEtudSnCertification(Long value) {
    takeStoredValueForKey(value, ETUD_SN_CERTIFICATION_KEY);
  }

  public Long etudSportHn() {
    return (Long) storedValueForKey(ETUD_SPORT_HN_KEY);
  }

  public void setEtudSportHn(Long value) {
    takeStoredValueForKey(value, ETUD_SPORT_HN_KEY);
  }

  public String etudVilleBac() {
    return (String) storedValueForKey(ETUD_VILLE_BAC_KEY);
  }

  public void setEtudVilleBac(String value) {
    takeStoredValueForKey(value, ETUD_VILLE_BAC_KEY);
  }

  public String mentCode() {
    return (String) storedValueForKey(MENT_CODE_KEY);
  }

  public void setMentCode(String value) {
    takeStoredValueForKey(value, MENT_CODE_KEY);
  }

  public String proCode() {
    return (String) storedValueForKey(PRO_CODE_KEY);
  }

  public void setProCode(String value) {
    takeStoredValueForKey(value, PRO_CODE_KEY);
  }

  public String proCode2() {
    return (String) storedValueForKey(PRO_CODE2_KEY);
  }

  public void setProCode2(String value) {
    takeStoredValueForKey(value, PRO_CODE2_KEY);
  }

  public String thebCode() {
    return (String) storedValueForKey(THEB_CODE_KEY);
  }

  public void setThebCode(String value) {
    takeStoredValueForKey(value, THEB_CODE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAcademie toAcademie() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAcademie)storedValueForKey(TO_ACADEMIE_KEY);
  }

  public void setToAcademieRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAcademie value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAcademie oldValue = toAcademie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACADEMIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACADEMIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOBac toBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOBac)storedValueForKey(TO_BAC_KEY);
  }

  public void setToBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBac value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOBac oldValue = toBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartementEtabBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey(TO_DEPARTEMENT_ETAB_BAC_KEY);
  }

  public void setToDepartementEtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toDepartementEtabBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_ETAB_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_ETAB_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartementParent() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey(TO_DEPARTEMENT_PARENT_KEY);
  }

  public void setToDepartementParentRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toDepartementParent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_PARENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_PARENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMention toMention() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMention)storedValueForKey(TO_MENTION_KEY);
  }

  public void setToMentionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMention value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMention oldValue = toMention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysEtabBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_ETAB_BAC_KEY);
  }

  public void setToPaysEtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysEtabBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_ETAB_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_ETAB_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneCodeBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_CODE_BAC_KEY);
  }

  public void setToRneCodeBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneCodeBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_CODE_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_CODE_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneCodeSup() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_CODE_SUP_KEY);
  }

  public void setToRneCodeSupRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneCodeSup();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_CODE_SUP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_CODE_SUP_KEY);
    }
  }
  

/**
 * Créer une instance de EOEtudiant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEtudiant createEOEtudiant(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Long etudAnnee1inscUlr
, Integer etudNumero
, Long etudSitfam
, Long etudSportHn
, String proCode
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu			) {
    EOEtudiant eo = (EOEtudiant) createAndInsertInstance(editingContext, _EOEtudiant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEtudAnnee1inscUlr(etudAnnee1inscUlr);
		eo.setEtudNumero(etudNumero);
		eo.setEtudSitfam(etudSitfam);
		eo.setEtudSportHn(etudSportHn);
		eo.setProCode(proCode);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOEtudiant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEtudiant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEtudiant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEtudiant creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOEtudiant object = (EOEtudiant)createAndInsertInstance(editingContext, _EOEtudiant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEtudiant localInstanceIn(EOEditingContext editingContext, EOEtudiant eo) {
    EOEtudiant localInstance = (eo == null) ? null : (EOEtudiant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEtudiant#localInstanceIn a la place.
   */
	public static EOEtudiant localInstanceOf(EOEditingContext editingContext, EOEtudiant eo) {
		return EOEtudiant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEtudiant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEtudiant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEtudiant> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEtudiant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEtudiant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEtudiant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEtudiant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEtudiant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEtudiant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEtudiant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
