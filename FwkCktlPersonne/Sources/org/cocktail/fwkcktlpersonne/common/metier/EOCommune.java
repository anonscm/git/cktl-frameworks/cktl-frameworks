/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOCommune.java
// 
package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOCommune extends _EOCommune {

	public static final EOSortOrdering SORT_LC_COM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOCommune.LC_COM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LL_COM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOCommune.LL_COM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_C_POSTAL_ASC = EOSortOrdering.sortOrderingWithKey(EOCommune.C_POSTAL_KEY, EOSortOrdering.CompareAscending);

	public EOCommune() {
		super();
	}
	
	/**
	 * recherche les communes pour un code postal
	 * 
	 * @param editingContext
	 * @param codePostal code postal
	 * @return communes trouv&eacute;s
	 */
	public static NSArray rechercherCommunes(EOEditingContext editingContext, String codePostal) {
		NSMutableArray args = new NSMutableArray(codePostal);

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("cPostal = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification("Commune", myQualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	public static NSArray getFilteredCommunes(EOEditingContext editingContext, String filtCommune, EOPays pays) {
		//      LRLogger.debug("filtCommune", filtCommune);
		//      LRLogger.debug("pays defaut", EOPays.getPaysDefaut(edc()));
		//      LRLogger.debug("pays ", getAdresse().toPays());

		NSArray res = NSArray.EmptyArray;
		if (pays == null || pays.cPays().equals(EOPays.getPaysDefaut(editingContext).cPays())) {
			NSMutableArray quals = new NSMutableArray();

			if (filtCommune != null) {
				filtCommune = filtCommune.trim();
			}

			//Si filtre est numerique on considere que c'est un code postal qui est saisi
			if (!MyStringCtrl.isEmpty(filtCommune)) {
				if (MyStringCtrl.isDigits(filtCommune)) {
					res = EOCommune.fetchAll(editingContext, new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorLike, filtCommune + "*"), new NSArray(new Object[] {
							EOCommune.SORT_C_POSTAL_ASC
					}));
				} else {
					filtCommune = filtCommune.replaceAll(" ", "*");
					quals.addObject(new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "" + filtCommune + "*"));
					quals.addObject(new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "" + filtCommune + "*"));
					res = EOCommune.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
							EOCommune.SORT_LC_COM_KEY_ASC, EOCommune.SORT_C_POSTAL_ASC
					}));
					if (res.count() == 0) {
						quals.removeAllObjects();
						quals.addObject(new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtCommune + "*"));
						quals.addObject(new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtCommune + "*"));
						res = EOCommune.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
								EOCommune.SORT_LC_COM_KEY_ASC, EOCommune.SORT_C_POSTAL_ASC
						}));
					}
				}
			}
		}
		return res;
	}
	
	public static NSArray getFilteredCommunesSurLl(EOEditingContext editingContext, String filtCommune, EOPays pays) {
		//      LRLogger.debug("filtCommune", filtCommune);
		//      LRLogger.debug("pays defaut", EOPays.getPaysDefaut(edc()));
		//      LRLogger.debug("pays ", getAdresse().toPays());
		
		NSArray res = NSArray.EmptyArray;
		if (pays == null || pays.cPays().equals(EOPays.getPaysDefaut(editingContext).cPays())) {
			NSMutableArray quals = new NSMutableArray();
			
			if (filtCommune != null) {
				filtCommune = filtCommune.trim();
			}
			
			//Si filtre est numerique on considere que c'est un code postal qui est saisi
			if (!MyStringCtrl.isEmpty(filtCommune)) {
				if (MyStringCtrl.isDigits(filtCommune)) {
					res = EOCommune.fetchAll(editingContext, new EOKeyValueQualifier(EOCommune.C_POSTAL_KEY, EOQualifier.QualifierOperatorLike, filtCommune + "*"), new NSArray(new Object[] {
							EOCommune.SORT_C_POSTAL_ASC
					}));
				}
				else {
					filtCommune = filtCommune.replaceAll(" ", "*");
					quals.addObject(new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "" + filtCommune + "*"));
					quals.addObject(new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "" + filtCommune + "*"));
					res = EOCommune.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
							EOCommune.SORT_LL_COM_KEY_ASC, EOCommune.SORT_C_POSTAL_ASC
					}));
					if (res.count() == 0) {
						quals.removeAllObjects();
						quals.addObject(new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtCommune + "*"));
						quals.addObject(new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + filtCommune + "*"));
						res = EOCommune.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
								EOCommune.SORT_LL_COM_KEY_ASC, EOCommune.SORT_C_POSTAL_ASC
						}));
					}
					
				}
			}
		}
		return res;
	}

	public String codePostalEtLlComm() {
		return cPostal() + " " + llCom();
	}

}
