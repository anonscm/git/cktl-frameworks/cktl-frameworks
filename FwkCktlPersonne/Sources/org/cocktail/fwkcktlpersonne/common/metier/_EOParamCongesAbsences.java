/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOParamCongesAbsences.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOParamCongesAbsences extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOParamCongesAbsences.class);

	public static final String ENTITY_NAME = "Fwkpers_ParamCongesAbsences";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PARAM_CONGES_ABSENCES";


// Attribute Keys
  public static final ERXKey<Integer> ADOPT_DUREE_MIN = new ERXKey<Integer>("adoptDureeMin");
  public static final ERXKey<Integer> ADOPT_DUREE_MULTIPLES = new ERXKey<Integer>("adoptDureeMultiples");
  public static final ERXKey<Integer> ADOPT_DUREE_NBENF31 = new ERXKey<Integer>("adoptDureeNbenf31");
  public static final ERXKey<Integer> ANC_CG_SANS_TRAIT = new ERXKey<Integer>("ancCgSansTrait");
  public static final ERXKey<Integer> CBON_DUREE = new ERXKey<Integer>("cbonDuree");
  public static final ERXKey<Integer> CBON_INTERVALLE = new ERXKey<Integer>("cbonIntervalle");
  public static final ERXKey<Integer> CFP_DUREE_INDEMNITE = new ERXKey<Integer>("cfpDureeIndemnite");
  public static final ERXKey<Integer> CFP_DUREE_MAX = new ERXKey<Integer>("cfpDureeMax");
  public static final ERXKey<Integer> CFP_DUREE_MIN = new ERXKey<Integer>("cfpDureeMin");
  public static final ERXKey<Integer> CGM_ANCIENNETE = new ERXKey<Integer>("cgmAnciennete");
  public static final ERXKey<Integer> CGM_DELAI = new ERXKey<Integer>("cgmDelai");
  public static final ERXKey<Integer> CGM_DEMI_TRAITEMENT = new ERXKey<Integer>("cgmDemiTraitement");
  public static final ERXKey<Integer> CGM_DUREE1 = new ERXKey<Integer>("cgmDuree1");
  public static final ERXKey<Integer> CGM_DUREE2 = new ERXKey<Integer>("cgmDuree2");
  public static final ERXKey<Integer> CGM_PLEIN_TRAITEMENT = new ERXKey<Integer>("cgmPleinTraitement");
  public static final ERXKey<Integer> CLD_DUREE_BLOQUANT = new ERXKey<Integer>("cldDureeBloquant");
  public static final ERXKey<Integer> CLD_DUREE_NON_BLOQUANT = new ERXKey<Integer>("cldDureeNonBloquant");
  public static final ERXKey<Integer> CLM_DEMI_TRAITEMENT = new ERXKey<Integer>("clmDemiTraitement");
  public static final ERXKey<Integer> CLM_DUREE_BLOQUANT = new ERXKey<Integer>("clmDureeBloquant");
  public static final ERXKey<Integer> CLM_DUREE_NON_BLOQUANT = new ERXKey<Integer>("clmDureeNonBloquant");
  public static final ERXKey<Integer> CLM_PERIODE_REFERENCE = new ERXKey<Integer>("clmPeriodeReference");
  public static final ERXKey<Integer> CLM_PLEIN_TRAITEMENT = new ERXKey<Integer>("clmPleinTraitement");
  public static final ERXKey<Integer> CMNT_PERIODE_REFERENCE_C = new ERXKey<Integer>("cmntPeriodeReferenceC");
  public static final ERXKey<Integer> CMNT_PERIODE_REFERENCE_D = new ERXKey<Integer>("cmntPeriodeReferenceD");
  public static final ERXKey<Integer> CMOR_AVERT_PROLONG = new ERXKey<Integer>("cmorAvertProlong");
  public static final ERXKey<Integer> CMOR_DEMI_TRAITEMENT = new ERXKey<Integer>("cmorDemiTraitement");
  public static final ERXKey<Integer> CMOR_DUREE_MAX = new ERXKey<Integer>("cmorDureeMax");
  public static final ERXKey<Integer> CMOR_PERIODE_REFERENCE = new ERXKey<Integer>("cmorPeriodeReference");
  public static final ERXKey<Integer> CMOR_PLEIN_TRAITEMENT = new ERXKey<Integer>("cmorPleinTraitement");
  public static final ERXKey<Integer> CONVPS_DATE_CGF = new ERXKey<Integer>("convpsDateCgf");
  public static final ERXKey<Integer> CONVPS_DUREE_CGF = new ERXKey<Integer>("convpsDureeCgf");
  public static final ERXKey<Integer> CONVPS_DUREE_MAX = new ERXKey<Integer>("convpsDureeMax");
  public static final ERXKey<Integer> CONVPS_DUREE_MINI = new ERXKey<Integer>("convpsDureeMini");
  public static final ERXKey<Integer> CPA_AGE_MAXI = new ERXKey<Integer>("cpaAgeMaxi");
  public static final ERXKey<Integer> CPA_AGE_MAXIMUM = new ERXKey<Integer>("cpaAgeMaximum");
  public static final ERXKey<Integer> CPA_AGE_MINI_DEB = new ERXKey<Integer>("cpaAgeMiniDeb");
  public static final ERXKey<Integer> CPA_ANC_SERVICE = new ERXKey<Integer>("cpaAncService");
  public static final ERXKey<Integer> CPAR_AGE_SUP_ENFANT = new ERXKey<Integer>("cparAgeSupEnfant");
  public static final ERXKey<Integer> CPAR_DUREE_MAX = new ERXKey<Integer>("cparDureeMax");
  public static final ERXKey<Integer> CPAR_DUREE_PERIODE = new ERXKey<Integer>("cparDureePeriode");
  public static final ERXKey<Integer> CRCT_DUREE_MAX = new ERXKey<Integer>("crctDureeMax");
  public static final ERXKey<Integer> CRCT_DUREE_MINI = new ERXKey<Integer>("crctDureeMini");
  public static final ERXKey<Integer> CRCT_DUREE_POSIT = new ERXKey<Integer>("crctDureePosit");
  public static final ERXKey<Integer> CRCT_DUREE_TOTAL = new ERXKey<Integer>("crctDureeTotal");
  public static final ERXKey<Integer> CSF_DUREE_FRACTIONNEMENT_MINI = new ERXKey<Integer>("csfDureeFractionnementMini");
  public static final ERXKey<Integer> CSF_DUREE_MAXI = new ERXKey<Integer>("csfDureeMaxi");
  public static final ERXKey<Integer> CSPEC_AGE_ENFANT = new ERXKey<Integer>("cspecAgeEnfant");
  public static final ERXKey<Integer> CSPEC_DUREE1 = new ERXKey<Integer>("cspecDuree1");
  public static final ERXKey<Integer> CSPEC_DUREE2 = new ERXKey<Integer>("cspecDuree2");
  public static final ERXKey<Integer> CSPEC_DUREE3 = new ERXKey<Integer>("cspecDuree3");
  public static final ERXKey<Integer> CSPEC_DUREE_MAX = new ERXKey<Integer>("cspecDureeMax");
  public static final ERXKey<Integer> CSPEC_RENOUVELLEMENT = new ERXKey<Integer>("cspecRenouvellement");
  public static final ERXKey<Integer> CSPESTA_DUREE5_MAX = new ERXKey<Integer>("cspestaDuree5Max");
  public static final ERXKey<Integer> CSPESTA_DUREE_MAX = new ERXKey<Integer>("cspestaDureeMax");
  public static final ERXKey<Integer> CSPESTA_NB_RENOUVELLEMENT = new ERXKey<Integer>("cspestaNbRenouvellement");
  public static final ERXKey<Integer> DELEG_DUREE_MAX = new ERXKey<Integer>("delegDureeMax");
  public static final ERXKey<Integer> DELEG_HU_NT_DUREE_FONCTION = new ERXKey<Integer>("delegHuNtDureeFonction");
  public static final ERXKey<Integer> DELEG_HU_NT_DUREE_MAX = new ERXKey<Integer>("delegHuNtDureeMax");
  public static final ERXKey<Integer> DELEG_HU_NT_PROLONGATION = new ERXKey<Integer>("delegHuNtProlongation");
  public static final ERXKey<Integer> DELEG_HU_TIT_DUREE_MAX = new ERXKey<Integer>("delegHuTitDureeMax");
  public static final ERXKey<Integer> DELEG_HU_TIT_DUREE_REPRISE = new ERXKey<Integer>("delegHuTitDureeReprise");
  public static final ERXKey<Integer> MAD_DUREE_MAX = new ERXKey<Integer>("madDureeMax");
  public static final ERXKey<Integer> MAINT_FONCT_AGE_MAXI = new ERXKey<Integer>("maintFonctAgeMaxi");
  public static final ERXKey<Integer> MAINT_FONCT_AGE_MINI = new ERXKey<Integer>("maintFonctAgeMini");
  public static final ERXKey<Integer> MAINT_FONCT_DUREE_MAXI = new ERXKey<Integer>("maintFonctDureeMaxi");
  public static final ERXKey<Integer> MAT_DUREE_MIN_POSTNATALE = new ERXKey<Integer>("matDureeMinPostnatale");
  public static final ERXKey<Integer> MAT_DUREE_MIN_PRENATALE = new ERXKey<Integer>("matDureeMinPrenatale");
  public static final ERXKey<Integer> MISDEP_DUREE = new ERXKey<Integer>("misdepDuree");
  public static final ERXKey<Integer> MTTH_DUREE_ACSERV = new ERXKey<Integer>("mtthDureeAcserv");
  public static final ERXKey<Integer> MTTH_DUREE_CLD_CLM = new ERXKey<Integer>("mtthDureeCldClm");
  public static final ERXKey<Integer> PROLONGATION_DUREE_MAXI = new ERXKey<Integer>("prolongationDureeMaxi");
  public static final ERXKey<Integer> RECUL_AGE_DUREE_MAXI = new ERXKey<Integer>("reculAgeDureeMaxi");
  public static final ERXKey<Integer> RECUL_AGE_DUREE_MINI = new ERXKey<Integer>("reculAgeDureeMini");
  public static final ERXKey<Integer> RECUL_AGE_MINI = new ERXKey<Integer>("reculAgeMini");
  public static final ERXKey<Integer> SURNB_DUREE = new ERXKey<Integer>("surnbDuree");
  public static final ERXKey<Integer> SURNB_LIMITE_AGE = new ERXKey<Integer>("surnbLimiteAge");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noParam";

	public static final String ADOPT_DUREE_MIN_KEY = "adoptDureeMin";
	public static final String ADOPT_DUREE_MULTIPLES_KEY = "adoptDureeMultiples";
	public static final String ADOPT_DUREE_NBENF31_KEY = "adoptDureeNbenf31";
	public static final String ANC_CG_SANS_TRAIT_KEY = "ancCgSansTrait";
	public static final String CBON_DUREE_KEY = "cbonDuree";
	public static final String CBON_INTERVALLE_KEY = "cbonIntervalle";
	public static final String CFP_DUREE_INDEMNITE_KEY = "cfpDureeIndemnite";
	public static final String CFP_DUREE_MAX_KEY = "cfpDureeMax";
	public static final String CFP_DUREE_MIN_KEY = "cfpDureeMin";
	public static final String CGM_ANCIENNETE_KEY = "cgmAnciennete";
	public static final String CGM_DELAI_KEY = "cgmDelai";
	public static final String CGM_DEMI_TRAITEMENT_KEY = "cgmDemiTraitement";
	public static final String CGM_DUREE1_KEY = "cgmDuree1";
	public static final String CGM_DUREE2_KEY = "cgmDuree2";
	public static final String CGM_PLEIN_TRAITEMENT_KEY = "cgmPleinTraitement";
	public static final String CLD_DUREE_BLOQUANT_KEY = "cldDureeBloquant";
	public static final String CLD_DUREE_NON_BLOQUANT_KEY = "cldDureeNonBloquant";
	public static final String CLM_DEMI_TRAITEMENT_KEY = "clmDemiTraitement";
	public static final String CLM_DUREE_BLOQUANT_KEY = "clmDureeBloquant";
	public static final String CLM_DUREE_NON_BLOQUANT_KEY = "clmDureeNonBloquant";
	public static final String CLM_PERIODE_REFERENCE_KEY = "clmPeriodeReference";
	public static final String CLM_PLEIN_TRAITEMENT_KEY = "clmPleinTraitement";
	public static final String CMNT_PERIODE_REFERENCE_C_KEY = "cmntPeriodeReferenceC";
	public static final String CMNT_PERIODE_REFERENCE_D_KEY = "cmntPeriodeReferenceD";
	public static final String CMOR_AVERT_PROLONG_KEY = "cmorAvertProlong";
	public static final String CMOR_DEMI_TRAITEMENT_KEY = "cmorDemiTraitement";
	public static final String CMOR_DUREE_MAX_KEY = "cmorDureeMax";
	public static final String CMOR_PERIODE_REFERENCE_KEY = "cmorPeriodeReference";
	public static final String CMOR_PLEIN_TRAITEMENT_KEY = "cmorPleinTraitement";
	public static final String CONVPS_DATE_CGF_KEY = "convpsDateCgf";
	public static final String CONVPS_DUREE_CGF_KEY = "convpsDureeCgf";
	public static final String CONVPS_DUREE_MAX_KEY = "convpsDureeMax";
	public static final String CONVPS_DUREE_MINI_KEY = "convpsDureeMini";
	public static final String CPA_AGE_MAXI_KEY = "cpaAgeMaxi";
	public static final String CPA_AGE_MAXIMUM_KEY = "cpaAgeMaximum";
	public static final String CPA_AGE_MINI_DEB_KEY = "cpaAgeMiniDeb";
	public static final String CPA_ANC_SERVICE_KEY = "cpaAncService";
	public static final String CPAR_AGE_SUP_ENFANT_KEY = "cparAgeSupEnfant";
	public static final String CPAR_DUREE_MAX_KEY = "cparDureeMax";
	public static final String CPAR_DUREE_PERIODE_KEY = "cparDureePeriode";
	public static final String CRCT_DUREE_MAX_KEY = "crctDureeMax";
	public static final String CRCT_DUREE_MINI_KEY = "crctDureeMini";
	public static final String CRCT_DUREE_POSIT_KEY = "crctDureePosit";
	public static final String CRCT_DUREE_TOTAL_KEY = "crctDureeTotal";
	public static final String CSF_DUREE_FRACTIONNEMENT_MINI_KEY = "csfDureeFractionnementMini";
	public static final String CSF_DUREE_MAXI_KEY = "csfDureeMaxi";
	public static final String CSPEC_AGE_ENFANT_KEY = "cspecAgeEnfant";
	public static final String CSPEC_DUREE1_KEY = "cspecDuree1";
	public static final String CSPEC_DUREE2_KEY = "cspecDuree2";
	public static final String CSPEC_DUREE3_KEY = "cspecDuree3";
	public static final String CSPEC_DUREE_MAX_KEY = "cspecDureeMax";
	public static final String CSPEC_RENOUVELLEMENT_KEY = "cspecRenouvellement";
	public static final String CSPESTA_DUREE5_MAX_KEY = "cspestaDuree5Max";
	public static final String CSPESTA_DUREE_MAX_KEY = "cspestaDureeMax";
	public static final String CSPESTA_NB_RENOUVELLEMENT_KEY = "cspestaNbRenouvellement";
	public static final String DELEG_DUREE_MAX_KEY = "delegDureeMax";
	public static final String DELEG_HU_NT_DUREE_FONCTION_KEY = "delegHuNtDureeFonction";
	public static final String DELEG_HU_NT_DUREE_MAX_KEY = "delegHuNtDureeMax";
	public static final String DELEG_HU_NT_PROLONGATION_KEY = "delegHuNtProlongation";
	public static final String DELEG_HU_TIT_DUREE_MAX_KEY = "delegHuTitDureeMax";
	public static final String DELEG_HU_TIT_DUREE_REPRISE_KEY = "delegHuTitDureeReprise";
	public static final String MAD_DUREE_MAX_KEY = "madDureeMax";
	public static final String MAINT_FONCT_AGE_MAXI_KEY = "maintFonctAgeMaxi";
	public static final String MAINT_FONCT_AGE_MINI_KEY = "maintFonctAgeMini";
	public static final String MAINT_FONCT_DUREE_MAXI_KEY = "maintFonctDureeMaxi";
	public static final String MAT_DUREE_MIN_POSTNATALE_KEY = "matDureeMinPostnatale";
	public static final String MAT_DUREE_MIN_PRENATALE_KEY = "matDureeMinPrenatale";
	public static final String MISDEP_DUREE_KEY = "misdepDuree";
	public static final String MTTH_DUREE_ACSERV_KEY = "mtthDureeAcserv";
	public static final String MTTH_DUREE_CLD_CLM_KEY = "mtthDureeCldClm";
	public static final String PROLONGATION_DUREE_MAXI_KEY = "prolongationDureeMaxi";
	public static final String RECUL_AGE_DUREE_MAXI_KEY = "reculAgeDureeMaxi";
	public static final String RECUL_AGE_DUREE_MINI_KEY = "reculAgeDureeMini";
	public static final String RECUL_AGE_MINI_KEY = "reculAgeMini";
	public static final String SURNB_DUREE_KEY = "surnbDuree";
	public static final String SURNB_LIMITE_AGE_KEY = "surnbLimiteAge";

// Attributs non visibles
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String NO_PARAM_KEY = "noParam";

//Colonnes dans la base de donnees
	public static final String ADOPT_DUREE_MIN_COLKEY = "ADOPT_DUREE_MIN";
	public static final String ADOPT_DUREE_MULTIPLES_COLKEY = "ADOPT_DUREE_MULTIPLES";
	public static final String ADOPT_DUREE_NBENF31_COLKEY = "ADOPT_DUREE_NBENF_3_1";
	public static final String ANC_CG_SANS_TRAIT_COLKEY = "ANC_CG_SANS_TRAIT";
	public static final String CBON_DUREE_COLKEY = "CBON_DUREE";
	public static final String CBON_INTERVALLE_COLKEY = "CBON_INTERVALLE";
	public static final String CFP_DUREE_INDEMNITE_COLKEY = "CFP_DUREE_INDEMNITE";
	public static final String CFP_DUREE_MAX_COLKEY = "CFP_DUREE_MAX";
	public static final String CFP_DUREE_MIN_COLKEY = "CFP_DUREE_MIN";
	public static final String CGM_ANCIENNETE_COLKEY = "CGM_ANCIENNETE";
	public static final String CGM_DELAI_COLKEY = "CGM_DELAI";
	public static final String CGM_DEMI_TRAITEMENT_COLKEY = "CGM_DEMI_TRAITEMENT";
	public static final String CGM_DUREE1_COLKEY = "CGM_DUREE_1";
	public static final String CGM_DUREE2_COLKEY = "CGM_DUREE_2";
	public static final String CGM_PLEIN_TRAITEMENT_COLKEY = "CGM_PLEIN_TRAITEMENT";
	public static final String CLD_DUREE_BLOQUANT_COLKEY = "CLD_DUREE_BLOQUANT";
	public static final String CLD_DUREE_NON_BLOQUANT_COLKEY = "CLD_DUREE_NON_BLOQUANT";
	public static final String CLM_DEMI_TRAITEMENT_COLKEY = "CLM_DEMI_TRAITEMENT";
	public static final String CLM_DUREE_BLOQUANT_COLKEY = "CLM_DUREE_BLOQUANT";
	public static final String CLM_DUREE_NON_BLOQUANT_COLKEY = "CLM_DUREE_NON_BLOQUANT";
	public static final String CLM_PERIODE_REFERENCE_COLKEY = "CLM_PERIODE_REFERENCE";
	public static final String CLM_PLEIN_TRAITEMENT_COLKEY = "CLM_PLEIN_TRAITEMENT";
	public static final String CMNT_PERIODE_REFERENCE_C_COLKEY = "CMNT_PERIODE_REFERENCE_C";
	public static final String CMNT_PERIODE_REFERENCE_D_COLKEY = "CMNT_PERIODE_REFERENCE_D";
	public static final String CMOR_AVERT_PROLONG_COLKEY = "CMOR_AVERT_PROLONG";
	public static final String CMOR_DEMI_TRAITEMENT_COLKEY = "CMOR_DEMI_TRAITEMENT";
	public static final String CMOR_DUREE_MAX_COLKEY = "CMOR_DUREE_MAX";
	public static final String CMOR_PERIODE_REFERENCE_COLKEY = "CMOR_PERIODE_REFERENCE";
	public static final String CMOR_PLEIN_TRAITEMENT_COLKEY = "CMOR_PLEIN_TRAITEMENT";
	public static final String CONVPS_DATE_CGF_COLKEY = "CONVPS_DATE_CGF";
	public static final String CONVPS_DUREE_CGF_COLKEY = "CONVPS_DUREE_CGF";
	public static final String CONVPS_DUREE_MAX_COLKEY = "CONVPS_DUREE_MAX";
	public static final String CONVPS_DUREE_MINI_COLKEY = "CONVPS_DUREE_MINI";
	public static final String CPA_AGE_MAXI_COLKEY = "CPA_AGE_MAXI";
	public static final String CPA_AGE_MAXIMUM_COLKEY = "CPA_AGE_MAXIMUM";
	public static final String CPA_AGE_MINI_DEB_COLKEY = "CPA_AGE_MINI_DEB";
	public static final String CPA_ANC_SERVICE_COLKEY = "CPA_ANC_SERVICE";
	public static final String CPAR_AGE_SUP_ENFANT_COLKEY = "CPAR_AGE_SUP_ENFANT";
	public static final String CPAR_DUREE_MAX_COLKEY = "CPAR_DUREE_MAX";
	public static final String CPAR_DUREE_PERIODE_COLKEY = "CPAR_DUREE_PERIODE";
	public static final String CRCT_DUREE_MAX_COLKEY = "CRCT_DUREE_MAX";
	public static final String CRCT_DUREE_MINI_COLKEY = "CRCT_DUREE_MINI";
	public static final String CRCT_DUREE_POSIT_COLKEY = "CRCT_DUREE_POSIT";
	public static final String CRCT_DUREE_TOTAL_COLKEY = "CRCT_DUREE_TOTAL";
	public static final String CSF_DUREE_FRACTIONNEMENT_MINI_COLKEY = "CSF_DUREE_FRACTIONNEMENT_MINI";
	public static final String CSF_DUREE_MAXI_COLKEY = "CSF_DUREE_MAXI";
	public static final String CSPEC_AGE_ENFANT_COLKEY = "CSPEC_AGE_ENFANT";
	public static final String CSPEC_DUREE1_COLKEY = "CSPEC_DUREE1";
	public static final String CSPEC_DUREE2_COLKEY = "CSPEC_DUREE2";
	public static final String CSPEC_DUREE3_COLKEY = "CSPEC_DUREE3";
	public static final String CSPEC_DUREE_MAX_COLKEY = "CSPEC_DUREE_MAX";
	public static final String CSPEC_RENOUVELLEMENT_COLKEY = "CSPEC_RENOUVELLEMENT";
	public static final String CSPESTA_DUREE5_MAX_COLKEY = "CSPESTA_DUREE5_MAX";
	public static final String CSPESTA_DUREE_MAX_COLKEY = "CSPESTA_DUREE_MAX";
	public static final String CSPESTA_NB_RENOUVELLEMENT_COLKEY = "CSPESTA_NB_RENOUVELLEMENT";
	public static final String DELEG_DUREE_MAX_COLKEY = "DELEG_DUREE_MAX";
	public static final String DELEG_HU_NT_DUREE_FONCTION_COLKEY = "DELEG_HU_NT_DUREE_FONCTION";
	public static final String DELEG_HU_NT_DUREE_MAX_COLKEY = "DELEG_HU_NT_DUREE_MAX";
	public static final String DELEG_HU_NT_PROLONGATION_COLKEY = "DELEG_HU_NT_PROLONGATION";
	public static final String DELEG_HU_TIT_DUREE_MAX_COLKEY = "DELEG_HU_TIT_DUREE_MAX";
	public static final String DELEG_HU_TIT_DUREE_REPRISE_COLKEY = "DELEG_HU_TIT_DUREE_REPRISE";
	public static final String MAD_DUREE_MAX_COLKEY = "MAD_DUREE_MAX";
	public static final String MAINT_FONCT_AGE_MAXI_COLKEY = "MAINT_FONCT_AGE_MAXI";
	public static final String MAINT_FONCT_AGE_MINI_COLKEY = "MAINT_FONCT_AGE_MINI";
	public static final String MAINT_FONCT_DUREE_MAXI_COLKEY = "MAINT_FONCT_DUREE_MAXI";
	public static final String MAT_DUREE_MIN_POSTNATALE_COLKEY = "MAT_DUREE_MIN_POSTNATALE";
	public static final String MAT_DUREE_MIN_PRENATALE_COLKEY = "MAT_DUREE_MIN_PRENATALE";
	public static final String MISDEP_DUREE_COLKEY = "MISDEP_DUREE";
	public static final String MTTH_DUREE_ACSERV_COLKEY = "MTTH_DUREE_ACSERV";
	public static final String MTTH_DUREE_CLD_CLM_COLKEY = "MTTH_DUREE_CLD_CLM";
	public static final String PROLONGATION_DUREE_MAXI_COLKEY = "PROLONGATION_DUREE_MAXI";
	public static final String RECUL_AGE_DUREE_MAXI_COLKEY = "RECUL_AGE_DUREE_MAXI";
	public static final String RECUL_AGE_DUREE_MINI_COLKEY = "RECUL_AGE_DUREE_MINI";
	public static final String RECUL_AGE_MINI_COLKEY = "RECUL_AGE_MINI";
	public static final String SURNB_DUREE_COLKEY = "SURNB_DUREE";
	public static final String SURNB_LIMITE_AGE_COLKEY = "SURNB_LIMITE_AGE";

	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String NO_PARAM_COLKEY = "NO_PARAM";


	// Relationships



	// Accessors methods
  public Integer adoptDureeMin() {
    return (Integer) storedValueForKey(ADOPT_DUREE_MIN_KEY);
  }

  public void setAdoptDureeMin(Integer value) {
    takeStoredValueForKey(value, ADOPT_DUREE_MIN_KEY);
  }

  public Integer adoptDureeMultiples() {
    return (Integer) storedValueForKey(ADOPT_DUREE_MULTIPLES_KEY);
  }

  public void setAdoptDureeMultiples(Integer value) {
    takeStoredValueForKey(value, ADOPT_DUREE_MULTIPLES_KEY);
  }

  public Integer adoptDureeNbenf31() {
    return (Integer) storedValueForKey(ADOPT_DUREE_NBENF31_KEY);
  }

  public void setAdoptDureeNbenf31(Integer value) {
    takeStoredValueForKey(value, ADOPT_DUREE_NBENF31_KEY);
  }

  public Integer ancCgSansTrait() {
    return (Integer) storedValueForKey(ANC_CG_SANS_TRAIT_KEY);
  }

  public void setAncCgSansTrait(Integer value) {
    takeStoredValueForKey(value, ANC_CG_SANS_TRAIT_KEY);
  }

  public Integer cbonDuree() {
    return (Integer) storedValueForKey(CBON_DUREE_KEY);
  }

  public void setCbonDuree(Integer value) {
    takeStoredValueForKey(value, CBON_DUREE_KEY);
  }

  public Integer cbonIntervalle() {
    return (Integer) storedValueForKey(CBON_INTERVALLE_KEY);
  }

  public void setCbonIntervalle(Integer value) {
    takeStoredValueForKey(value, CBON_INTERVALLE_KEY);
  }

  public Integer cfpDureeIndemnite() {
    return (Integer) storedValueForKey(CFP_DUREE_INDEMNITE_KEY);
  }

  public void setCfpDureeIndemnite(Integer value) {
    takeStoredValueForKey(value, CFP_DUREE_INDEMNITE_KEY);
  }

  public Integer cfpDureeMax() {
    return (Integer) storedValueForKey(CFP_DUREE_MAX_KEY);
  }

  public void setCfpDureeMax(Integer value) {
    takeStoredValueForKey(value, CFP_DUREE_MAX_KEY);
  }

  public Integer cfpDureeMin() {
    return (Integer) storedValueForKey(CFP_DUREE_MIN_KEY);
  }

  public void setCfpDureeMin(Integer value) {
    takeStoredValueForKey(value, CFP_DUREE_MIN_KEY);
  }

  public Integer cgmAnciennete() {
    return (Integer) storedValueForKey(CGM_ANCIENNETE_KEY);
  }

  public void setCgmAnciennete(Integer value) {
    takeStoredValueForKey(value, CGM_ANCIENNETE_KEY);
  }

  public Integer cgmDelai() {
    return (Integer) storedValueForKey(CGM_DELAI_KEY);
  }

  public void setCgmDelai(Integer value) {
    takeStoredValueForKey(value, CGM_DELAI_KEY);
  }

  public Integer cgmDemiTraitement() {
    return (Integer) storedValueForKey(CGM_DEMI_TRAITEMENT_KEY);
  }

  public void setCgmDemiTraitement(Integer value) {
    takeStoredValueForKey(value, CGM_DEMI_TRAITEMENT_KEY);
  }

  public Integer cgmDuree1() {
    return (Integer) storedValueForKey(CGM_DUREE1_KEY);
  }

  public void setCgmDuree1(Integer value) {
    takeStoredValueForKey(value, CGM_DUREE1_KEY);
  }

  public Integer cgmDuree2() {
    return (Integer) storedValueForKey(CGM_DUREE2_KEY);
  }

  public void setCgmDuree2(Integer value) {
    takeStoredValueForKey(value, CGM_DUREE2_KEY);
  }

  public Integer cgmPleinTraitement() {
    return (Integer) storedValueForKey(CGM_PLEIN_TRAITEMENT_KEY);
  }

  public void setCgmPleinTraitement(Integer value) {
    takeStoredValueForKey(value, CGM_PLEIN_TRAITEMENT_KEY);
  }

  public Integer cldDureeBloquant() {
    return (Integer) storedValueForKey(CLD_DUREE_BLOQUANT_KEY);
  }

  public void setCldDureeBloquant(Integer value) {
    takeStoredValueForKey(value, CLD_DUREE_BLOQUANT_KEY);
  }

  public Integer cldDureeNonBloquant() {
    return (Integer) storedValueForKey(CLD_DUREE_NON_BLOQUANT_KEY);
  }

  public void setCldDureeNonBloquant(Integer value) {
    takeStoredValueForKey(value, CLD_DUREE_NON_BLOQUANT_KEY);
  }

  public Integer clmDemiTraitement() {
    return (Integer) storedValueForKey(CLM_DEMI_TRAITEMENT_KEY);
  }

  public void setClmDemiTraitement(Integer value) {
    takeStoredValueForKey(value, CLM_DEMI_TRAITEMENT_KEY);
  }

  public Integer clmDureeBloquant() {
    return (Integer) storedValueForKey(CLM_DUREE_BLOQUANT_KEY);
  }

  public void setClmDureeBloquant(Integer value) {
    takeStoredValueForKey(value, CLM_DUREE_BLOQUANT_KEY);
  }

  public Integer clmDureeNonBloquant() {
    return (Integer) storedValueForKey(CLM_DUREE_NON_BLOQUANT_KEY);
  }

  public void setClmDureeNonBloquant(Integer value) {
    takeStoredValueForKey(value, CLM_DUREE_NON_BLOQUANT_KEY);
  }

  public Integer clmPeriodeReference() {
    return (Integer) storedValueForKey(CLM_PERIODE_REFERENCE_KEY);
  }

  public void setClmPeriodeReference(Integer value) {
    takeStoredValueForKey(value, CLM_PERIODE_REFERENCE_KEY);
  }

  public Integer clmPleinTraitement() {
    return (Integer) storedValueForKey(CLM_PLEIN_TRAITEMENT_KEY);
  }

  public void setClmPleinTraitement(Integer value) {
    takeStoredValueForKey(value, CLM_PLEIN_TRAITEMENT_KEY);
  }

  public Integer cmntPeriodeReferenceC() {
    return (Integer) storedValueForKey(CMNT_PERIODE_REFERENCE_C_KEY);
  }

  public void setCmntPeriodeReferenceC(Integer value) {
    takeStoredValueForKey(value, CMNT_PERIODE_REFERENCE_C_KEY);
  }

  public Integer cmntPeriodeReferenceD() {
    return (Integer) storedValueForKey(CMNT_PERIODE_REFERENCE_D_KEY);
  }

  public void setCmntPeriodeReferenceD(Integer value) {
    takeStoredValueForKey(value, CMNT_PERIODE_REFERENCE_D_KEY);
  }

  public Integer cmorAvertProlong() {
    return (Integer) storedValueForKey(CMOR_AVERT_PROLONG_KEY);
  }

  public void setCmorAvertProlong(Integer value) {
    takeStoredValueForKey(value, CMOR_AVERT_PROLONG_KEY);
  }

  public Integer cmorDemiTraitement() {
    return (Integer) storedValueForKey(CMOR_DEMI_TRAITEMENT_KEY);
  }

  public void setCmorDemiTraitement(Integer value) {
    takeStoredValueForKey(value, CMOR_DEMI_TRAITEMENT_KEY);
  }

  public Integer cmorDureeMax() {
    return (Integer) storedValueForKey(CMOR_DUREE_MAX_KEY);
  }

  public void setCmorDureeMax(Integer value) {
    takeStoredValueForKey(value, CMOR_DUREE_MAX_KEY);
  }

  public Integer cmorPeriodeReference() {
    return (Integer) storedValueForKey(CMOR_PERIODE_REFERENCE_KEY);
  }

  public void setCmorPeriodeReference(Integer value) {
    takeStoredValueForKey(value, CMOR_PERIODE_REFERENCE_KEY);
  }

  public Integer cmorPleinTraitement() {
    return (Integer) storedValueForKey(CMOR_PLEIN_TRAITEMENT_KEY);
  }

  public void setCmorPleinTraitement(Integer value) {
    takeStoredValueForKey(value, CMOR_PLEIN_TRAITEMENT_KEY);
  }

  public Integer convpsDateCgf() {
    return (Integer) storedValueForKey(CONVPS_DATE_CGF_KEY);
  }

  public void setConvpsDateCgf(Integer value) {
    takeStoredValueForKey(value, CONVPS_DATE_CGF_KEY);
  }

  public Integer convpsDureeCgf() {
    return (Integer) storedValueForKey(CONVPS_DUREE_CGF_KEY);
  }

  public void setConvpsDureeCgf(Integer value) {
    takeStoredValueForKey(value, CONVPS_DUREE_CGF_KEY);
  }

  public Integer convpsDureeMax() {
    return (Integer) storedValueForKey(CONVPS_DUREE_MAX_KEY);
  }

  public void setConvpsDureeMax(Integer value) {
    takeStoredValueForKey(value, CONVPS_DUREE_MAX_KEY);
  }

  public Integer convpsDureeMini() {
    return (Integer) storedValueForKey(CONVPS_DUREE_MINI_KEY);
  }

  public void setConvpsDureeMini(Integer value) {
    takeStoredValueForKey(value, CONVPS_DUREE_MINI_KEY);
  }

  public Integer cpaAgeMaxi() {
    return (Integer) storedValueForKey(CPA_AGE_MAXI_KEY);
  }

  public void setCpaAgeMaxi(Integer value) {
    takeStoredValueForKey(value, CPA_AGE_MAXI_KEY);
  }

  public Integer cpaAgeMaximum() {
    return (Integer) storedValueForKey(CPA_AGE_MAXIMUM_KEY);
  }

  public void setCpaAgeMaximum(Integer value) {
    takeStoredValueForKey(value, CPA_AGE_MAXIMUM_KEY);
  }

  public Integer cpaAgeMiniDeb() {
    return (Integer) storedValueForKey(CPA_AGE_MINI_DEB_KEY);
  }

  public void setCpaAgeMiniDeb(Integer value) {
    takeStoredValueForKey(value, CPA_AGE_MINI_DEB_KEY);
  }

  public Integer cpaAncService() {
    return (Integer) storedValueForKey(CPA_ANC_SERVICE_KEY);
  }

  public void setCpaAncService(Integer value) {
    takeStoredValueForKey(value, CPA_ANC_SERVICE_KEY);
  }

  public Integer cparAgeSupEnfant() {
    return (Integer) storedValueForKey(CPAR_AGE_SUP_ENFANT_KEY);
  }

  public void setCparAgeSupEnfant(Integer value) {
    takeStoredValueForKey(value, CPAR_AGE_SUP_ENFANT_KEY);
  }

  public Integer cparDureeMax() {
    return (Integer) storedValueForKey(CPAR_DUREE_MAX_KEY);
  }

  public void setCparDureeMax(Integer value) {
    takeStoredValueForKey(value, CPAR_DUREE_MAX_KEY);
  }

  public Integer cparDureePeriode() {
    return (Integer) storedValueForKey(CPAR_DUREE_PERIODE_KEY);
  }

  public void setCparDureePeriode(Integer value) {
    takeStoredValueForKey(value, CPAR_DUREE_PERIODE_KEY);
  }

  public Integer crctDureeMax() {
    return (Integer) storedValueForKey(CRCT_DUREE_MAX_KEY);
  }

  public void setCrctDureeMax(Integer value) {
    takeStoredValueForKey(value, CRCT_DUREE_MAX_KEY);
  }

  public Integer crctDureeMini() {
    return (Integer) storedValueForKey(CRCT_DUREE_MINI_KEY);
  }

  public void setCrctDureeMini(Integer value) {
    takeStoredValueForKey(value, CRCT_DUREE_MINI_KEY);
  }

  public Integer crctDureePosit() {
    return (Integer) storedValueForKey(CRCT_DUREE_POSIT_KEY);
  }

  public void setCrctDureePosit(Integer value) {
    takeStoredValueForKey(value, CRCT_DUREE_POSIT_KEY);
  }

  public Integer crctDureeTotal() {
    return (Integer) storedValueForKey(CRCT_DUREE_TOTAL_KEY);
  }

  public void setCrctDureeTotal(Integer value) {
    takeStoredValueForKey(value, CRCT_DUREE_TOTAL_KEY);
  }

  public Integer csfDureeFractionnementMini() {
    return (Integer) storedValueForKey(CSF_DUREE_FRACTIONNEMENT_MINI_KEY);
  }

  public void setCsfDureeFractionnementMini(Integer value) {
    takeStoredValueForKey(value, CSF_DUREE_FRACTIONNEMENT_MINI_KEY);
  }

  public Integer csfDureeMaxi() {
    return (Integer) storedValueForKey(CSF_DUREE_MAXI_KEY);
  }

  public void setCsfDureeMaxi(Integer value) {
    takeStoredValueForKey(value, CSF_DUREE_MAXI_KEY);
  }

  public Integer cspecAgeEnfant() {
    return (Integer) storedValueForKey(CSPEC_AGE_ENFANT_KEY);
  }

  public void setCspecAgeEnfant(Integer value) {
    takeStoredValueForKey(value, CSPEC_AGE_ENFANT_KEY);
  }

  public Integer cspecDuree1() {
    return (Integer) storedValueForKey(CSPEC_DUREE1_KEY);
  }

  public void setCspecDuree1(Integer value) {
    takeStoredValueForKey(value, CSPEC_DUREE1_KEY);
  }

  public Integer cspecDuree2() {
    return (Integer) storedValueForKey(CSPEC_DUREE2_KEY);
  }

  public void setCspecDuree2(Integer value) {
    takeStoredValueForKey(value, CSPEC_DUREE2_KEY);
  }

  public Integer cspecDuree3() {
    return (Integer) storedValueForKey(CSPEC_DUREE3_KEY);
  }

  public void setCspecDuree3(Integer value) {
    takeStoredValueForKey(value, CSPEC_DUREE3_KEY);
  }

  public Integer cspecDureeMax() {
    return (Integer) storedValueForKey(CSPEC_DUREE_MAX_KEY);
  }

  public void setCspecDureeMax(Integer value) {
    takeStoredValueForKey(value, CSPEC_DUREE_MAX_KEY);
  }

  public Integer cspecRenouvellement() {
    return (Integer) storedValueForKey(CSPEC_RENOUVELLEMENT_KEY);
  }

  public void setCspecRenouvellement(Integer value) {
    takeStoredValueForKey(value, CSPEC_RENOUVELLEMENT_KEY);
  }

  public Integer cspestaDuree5Max() {
    return (Integer) storedValueForKey(CSPESTA_DUREE5_MAX_KEY);
  }

  public void setCspestaDuree5Max(Integer value) {
    takeStoredValueForKey(value, CSPESTA_DUREE5_MAX_KEY);
  }

  public Integer cspestaDureeMax() {
    return (Integer) storedValueForKey(CSPESTA_DUREE_MAX_KEY);
  }

  public void setCspestaDureeMax(Integer value) {
    takeStoredValueForKey(value, CSPESTA_DUREE_MAX_KEY);
  }

  public Integer cspestaNbRenouvellement() {
    return (Integer) storedValueForKey(CSPESTA_NB_RENOUVELLEMENT_KEY);
  }

  public void setCspestaNbRenouvellement(Integer value) {
    takeStoredValueForKey(value, CSPESTA_NB_RENOUVELLEMENT_KEY);
  }

  public Integer delegDureeMax() {
    return (Integer) storedValueForKey(DELEG_DUREE_MAX_KEY);
  }

  public void setDelegDureeMax(Integer value) {
    takeStoredValueForKey(value, DELEG_DUREE_MAX_KEY);
  }

  public Integer delegHuNtDureeFonction() {
    return (Integer) storedValueForKey(DELEG_HU_NT_DUREE_FONCTION_KEY);
  }

  public void setDelegHuNtDureeFonction(Integer value) {
    takeStoredValueForKey(value, DELEG_HU_NT_DUREE_FONCTION_KEY);
  }

  public Integer delegHuNtDureeMax() {
    return (Integer) storedValueForKey(DELEG_HU_NT_DUREE_MAX_KEY);
  }

  public void setDelegHuNtDureeMax(Integer value) {
    takeStoredValueForKey(value, DELEG_HU_NT_DUREE_MAX_KEY);
  }

  public Integer delegHuNtProlongation() {
    return (Integer) storedValueForKey(DELEG_HU_NT_PROLONGATION_KEY);
  }

  public void setDelegHuNtProlongation(Integer value) {
    takeStoredValueForKey(value, DELEG_HU_NT_PROLONGATION_KEY);
  }

  public Integer delegHuTitDureeMax() {
    return (Integer) storedValueForKey(DELEG_HU_TIT_DUREE_MAX_KEY);
  }

  public void setDelegHuTitDureeMax(Integer value) {
    takeStoredValueForKey(value, DELEG_HU_TIT_DUREE_MAX_KEY);
  }

  public Integer delegHuTitDureeReprise() {
    return (Integer) storedValueForKey(DELEG_HU_TIT_DUREE_REPRISE_KEY);
  }

  public void setDelegHuTitDureeReprise(Integer value) {
    takeStoredValueForKey(value, DELEG_HU_TIT_DUREE_REPRISE_KEY);
  }

  public Integer madDureeMax() {
    return (Integer) storedValueForKey(MAD_DUREE_MAX_KEY);
  }

  public void setMadDureeMax(Integer value) {
    takeStoredValueForKey(value, MAD_DUREE_MAX_KEY);
  }

  public Integer maintFonctAgeMaxi() {
    return (Integer) storedValueForKey(MAINT_FONCT_AGE_MAXI_KEY);
  }

  public void setMaintFonctAgeMaxi(Integer value) {
    takeStoredValueForKey(value, MAINT_FONCT_AGE_MAXI_KEY);
  }

  public Integer maintFonctAgeMini() {
    return (Integer) storedValueForKey(MAINT_FONCT_AGE_MINI_KEY);
  }

  public void setMaintFonctAgeMini(Integer value) {
    takeStoredValueForKey(value, MAINT_FONCT_AGE_MINI_KEY);
  }

  public Integer maintFonctDureeMaxi() {
    return (Integer) storedValueForKey(MAINT_FONCT_DUREE_MAXI_KEY);
  }

  public void setMaintFonctDureeMaxi(Integer value) {
    takeStoredValueForKey(value, MAINT_FONCT_DUREE_MAXI_KEY);
  }

  public Integer matDureeMinPostnatale() {
    return (Integer) storedValueForKey(MAT_DUREE_MIN_POSTNATALE_KEY);
  }

  public void setMatDureeMinPostnatale(Integer value) {
    takeStoredValueForKey(value, MAT_DUREE_MIN_POSTNATALE_KEY);
  }

  public Integer matDureeMinPrenatale() {
    return (Integer) storedValueForKey(MAT_DUREE_MIN_PRENATALE_KEY);
  }

  public void setMatDureeMinPrenatale(Integer value) {
    takeStoredValueForKey(value, MAT_DUREE_MIN_PRENATALE_KEY);
  }

  public Integer misdepDuree() {
    return (Integer) storedValueForKey(MISDEP_DUREE_KEY);
  }

  public void setMisdepDuree(Integer value) {
    takeStoredValueForKey(value, MISDEP_DUREE_KEY);
  }

  public Integer mtthDureeAcserv() {
    return (Integer) storedValueForKey(MTTH_DUREE_ACSERV_KEY);
  }

  public void setMtthDureeAcserv(Integer value) {
    takeStoredValueForKey(value, MTTH_DUREE_ACSERV_KEY);
  }

  public Integer mtthDureeCldClm() {
    return (Integer) storedValueForKey(MTTH_DUREE_CLD_CLM_KEY);
  }

  public void setMtthDureeCldClm(Integer value) {
    takeStoredValueForKey(value, MTTH_DUREE_CLD_CLM_KEY);
  }

  public Integer prolongationDureeMaxi() {
    return (Integer) storedValueForKey(PROLONGATION_DUREE_MAXI_KEY);
  }

  public void setProlongationDureeMaxi(Integer value) {
    takeStoredValueForKey(value, PROLONGATION_DUREE_MAXI_KEY);
  }

  public Integer reculAgeDureeMaxi() {
    return (Integer) storedValueForKey(RECUL_AGE_DUREE_MAXI_KEY);
  }

  public void setReculAgeDureeMaxi(Integer value) {
    takeStoredValueForKey(value, RECUL_AGE_DUREE_MAXI_KEY);
  }

  public Integer reculAgeDureeMini() {
    return (Integer) storedValueForKey(RECUL_AGE_DUREE_MINI_KEY);
  }

  public void setReculAgeDureeMini(Integer value) {
    takeStoredValueForKey(value, RECUL_AGE_DUREE_MINI_KEY);
  }

  public Integer reculAgeMini() {
    return (Integer) storedValueForKey(RECUL_AGE_MINI_KEY);
  }

  public void setReculAgeMini(Integer value) {
    takeStoredValueForKey(value, RECUL_AGE_MINI_KEY);
  }

  public Integer surnbDuree() {
    return (Integer) storedValueForKey(SURNB_DUREE_KEY);
  }

  public void setSurnbDuree(Integer value) {
    takeStoredValueForKey(value, SURNB_DUREE_KEY);
  }

  public Integer surnbLimiteAge() {
    return (Integer) storedValueForKey(SURNB_LIMITE_AGE_KEY);
  }

  public void setSurnbLimiteAge(Integer value) {
    takeStoredValueForKey(value, SURNB_LIMITE_AGE_KEY);
  }


/**
 * Créer une instance de EOParamCongesAbsences avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOParamCongesAbsences createEOParamCongesAbsences(EOEditingContext editingContext			) {
    EOParamCongesAbsences eo = (EOParamCongesAbsences) createAndInsertInstance(editingContext, _EOParamCongesAbsences.ENTITY_NAME);    
    return eo;
  }

  
	  public EOParamCongesAbsences localInstanceIn(EOEditingContext editingContext) {
	  		return (EOParamCongesAbsences)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOParamCongesAbsences creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOParamCongesAbsences creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOParamCongesAbsences object = (EOParamCongesAbsences)createAndInsertInstance(editingContext, _EOParamCongesAbsences.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOParamCongesAbsences localInstanceIn(EOEditingContext editingContext, EOParamCongesAbsences eo) {
    EOParamCongesAbsences localInstance = (eo == null) ? null : (EOParamCongesAbsences)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOParamCongesAbsences#localInstanceIn a la place.
   */
	public static EOParamCongesAbsences localInstanceOf(EOEditingContext editingContext, EOParamCongesAbsences eo) {
		return EOParamCongesAbsences.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOParamCongesAbsences>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOParamCongesAbsences fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOParamCongesAbsences fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOParamCongesAbsences> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOParamCongesAbsences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOParamCongesAbsences)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOParamCongesAbsences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOParamCongesAbsences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOParamCongesAbsences> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOParamCongesAbsences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOParamCongesAbsences)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOParamCongesAbsences fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOParamCongesAbsences eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOParamCongesAbsences ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOParamCongesAbsences fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
