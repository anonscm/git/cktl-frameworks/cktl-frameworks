package org.cocktail.fwkcktlpersonne.common.metier.manager;

import org.cocktail.fwkcktlpersonne.common.metier.services.IIndividuService;

public interface IFwkPersonneManager {

	/**
	 * Retourne un service pour la gestion des individus
	 * @return
	 */
	IIndividuService getIndividuService();

}