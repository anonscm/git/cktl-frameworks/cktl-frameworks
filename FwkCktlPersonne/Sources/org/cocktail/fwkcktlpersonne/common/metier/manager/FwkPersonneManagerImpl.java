package org.cocktail.fwkcktlpersonne.common.metier.manager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ConditionInseeNonTemporaire;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuCondition;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeAnneeNaissance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeMoisNaissance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeSexe;
import org.cocktail.fwkcktlpersonne.common.metier.services.IIndividuService;
import org.cocktail.fwkcktlpersonne.common.metier.services.IndividuServiceImpl;


public class FwkPersonneManagerImpl implements IFwkPersonneManager {

	private static final String DEFAUT_PROPERTIES = "FwkPersonne.properties";
	private static final String CLEF_INDIVIDU_CONTROLE = "individu.controle";
	

	private final static Logger LOGGER = Logger.getLogger("FwkPersonneConfiguration");
	
	public List<IIndividuControle> controles = new ArrayList<IIndividuControle>();
	public IIndividuService individuService = new IndividuServiceImpl();
	
	private Map<IIndividuCondition,List<IIndividuControle>> conditions = new HashMap<IIndividuCondition,List<IIndividuControle>>();
	
	
	public FwkPersonneManagerImpl() {
		
		this(DEFAUT_PROPERTIES);
	}
	
	public FwkPersonneManagerImpl(String propertiesName) {
		
		List<IIndividuControle> controlesGeneraux = new ArrayList<IIndividuControle>();
		controlesGeneraux.add(new InseeAnneeNaissance());
		controlesGeneraux.add(new InseeMoisNaissance());

		List<IIndividuControle> controlesInsee = new ArrayList<IIndividuControle>();
		controlesInsee.add(new InseeSexe());
		
		conditions.put(IndividuServiceImpl.CONDITION_TJRS_VRAIE, controlesGeneraux);
		conditions.put(new ConditionInseeNonTemporaire(), chargeControlesOptionels(controlesInsee, propertiesName));
		individuService.setControles(conditions);
		
	}

	public IIndividuService getIndividuService() {
		return individuService;
	}

	public List<String> getListeControles() {
		return getIndividuService().getListeControles();
	}
	
	private List<IIndividuControle> chargeControlesOptionels(List<IIndividuControle> controles, String propertyName) {
		PropertiesConfiguration prop = new PropertiesConfiguration();
		
		try {
			InputStream stream = getClass().getClassLoader().getResourceAsStream(propertyName);
			
			if (stream == null) {
				throw new ConfigurationException("Le flux est null");
			}
			prop.load(stream);
		} catch (Exception e) {
			LOGGER.warning("[PERS0001] Le fichier de configuration ne peut-être chargé :" + propertyName);
			LOGGER.log(Level.FINEST, e.getMessage(), e);
		}
		
		for (Object nom : prop.getList(CLEF_INDIVIDU_CONTROLE)) {
			try {
				Class controle = this.getClass().getClassLoader().loadClass((String) nom);
				controles.add((IIndividuControle) controle.newInstance());
			} catch (Exception e) {
				LOGGER.warning("[PERS0002] La classe ne peut être chargée :" + nom);
				LOGGER.log(Level.FINEST, e.getMessage(), e);
			}
		}
		
		return controles;
	}

	
}
