package org.cocktail.fwkcktlpersonne.common.metier.manager;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

/**
 * 
 * Le manager de paramètres allant taper dans la table GRHUM_PARAMETRES.
 * 
 * @author Alexis Tual
 *
 */
public class CktlGrhumParamManager extends CktlParamManager {

    private EOEditingContext ec;
    
    /**
     * @param ec l'ec à utiliser pour la création de param.
     */
    public CktlGrhumParamManager(EOEditingContext ec) {
        this.ec = ec;
    }
    
    /**
     * @param paramKey la clef du paramètre à ajouter
     * @param paramComment le commentaire du paramètre
     * @param defaultValue la valeur par défaut
     * @param type le type
     */
    public void addParam(String paramKey, String paramComment, String defaultValue, String type) {
        getParamList().add(paramKey);
        getParamComments().put(paramKey, paramComment);
        getParamDefault().put(paramKey, defaultValue);
        getParamTypes().put(paramKey, type);
    }
    
    
    @Override
    public void createNewParam(String key, String value, String comment) {
        createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);
    }
    
    @Override
    public void checkAndInitParamsWithDefault() {
        //Recuperer un grhum_createur
        String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
        if (cptLogin != null) {
            EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
            if (cpt != null) {
                ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
            }
        }
        super.checkAndInitParamsWithDefault();
        getApplication().config().refresh();
    }

    @Override
    public void createNewParam(String key, String value, String comment,
            String type) {
        EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
        newParametre.setParamKey(key);
        newParametre.setParamValue(value);
        newParametre.setParamCommentaires(comment);
        newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
        if (ec.hasChanges()) {
            EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
            try {

                // Avant de sauvegarder les données, nous modifions le modèle
                // pour que l'on puisse avoir accès aussi en écriture sur les données
                entityParameter.setReadOnly(false);
                ec.saveChanges();

            } catch (Exception e) {
                log.warn("Erreur lors de l'enregistrement des parametres.");
                e.printStackTrace();
            } finally {
                entityParameter.setReadOnly(true);
            }
        }
    }

    @Override
    public String getParam(String key) {
        String res = getApplication().config().stringForKey(key);
        return res;
    }

    /**
     * @param key la clef du paramètre
     * @return la valeur sous forme d'entier, -1 si paramètre introuvable ou invalide
     */
    public int getIntParam(String key) {
        return getApplication().config().intForKey(key);
    }
    
    /**
     * @param key la clef du paramètre
     * @return true si la valeur est un booléen (O, N, true, false, yes, no, oui, non), false sinon
     * @see CktlConfig#booleanForKey(String)
     */
    public boolean getBoolParam(String key) {
        return getApplication().config().booleanForKey(key);
    }
}
