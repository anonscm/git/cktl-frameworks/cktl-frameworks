package org.cocktail.fwkcktlpersonne.common.metier;

public interface ITypeAdresse {

	String tadrCode();
	String tadrLibelle();
	String tadrLibelleCourt();
}
