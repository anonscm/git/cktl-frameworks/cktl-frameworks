/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompetence.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCompetence extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCompetence.class);

	public static final String ENTITY_NAME = "Fwkpers_Competence";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPETENCE_PROFESSIONNELLE_OLD";


// Attribute Keys
  public static final ERXKey<String> COM_LIBELLE = new ERXKey<String>("comLibelle");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> TOS_REPART_EMPLOI_COMPETENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>("tosRepartEmploiCompetence");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "comKey";

	public static final String COM_LIBELLE_KEY = "comLibelle";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String COM_KEY_KEY = "comKey";

//Colonnes dans la base de donnees
	public static final String COM_LIBELLE_COLKEY = "COM_LIBELLE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "newAttribute";

	public static final String COM_KEY_COLKEY = "COM_KEY";


	// Relationships
	public static final String TOS_REPART_EMPLOI_COMPETENCE_KEY = "tosRepartEmploiCompetence";



	// Accessors methods
  public String comLibelle() {
    return (String) storedValueForKey(COM_LIBELLE_KEY);
  }

  public void setComLibelle(String value) {
    takeStoredValueForKey(value, COM_LIBELLE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)storedValueForKey(TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier) {
    return tosRepartEmploiCompetence(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier, boolean fetch) {
    return tosRepartEmploiCompetence(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> tosRepartEmploiCompetence(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence.TO_COMPETENCE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartEmploiCompetence();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public void removeFromTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence createTosRepartEmploiCompetenceRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_RepartEmploiCompetence");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_EMPLOI_COMPETENCE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence) eo;
  }

  public void deleteTosRepartEmploiCompetenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_EMPLOI_COMPETENCE_KEY);
  }

  public void deleteAllTosRepartEmploiCompetenceRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartEmploiCompetence> objects = tosRepartEmploiCompetence().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartEmploiCompetenceRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCompetence avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCompetence createEOCompetence(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOCompetence eo = (EOCompetence) createAndInsertInstance(editingContext, _EOCompetence.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOCompetence localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompetence)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompetence creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompetence creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCompetence object = (EOCompetence)createAndInsertInstance(editingContext, _EOCompetence.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCompetence localInstanceIn(EOEditingContext editingContext, EOCompetence eo) {
    EOCompetence localInstance = (eo == null) ? null : (EOCompetence)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCompetence#localInstanceIn a la place.
   */
	public static EOCompetence localInstanceOf(EOEditingContext editingContext, EOCompetence eo) {
		return EOCompetence.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompetence>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompetence fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompetence fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCompetence> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompetence eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompetence)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompetence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompetence fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCompetence> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompetence eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompetence)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompetence fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompetence eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompetence ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompetence fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
