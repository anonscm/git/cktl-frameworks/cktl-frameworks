/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeExclusion.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOTypeExclusion extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOTypeExclusion.class);

	public static final String ENTITY_NAME = "Fwkpers_TypeExclusion";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_EXCLUSION";


// Attribute Keys
  public static final ERXKey<String> C_TYPE_EXCLUSION = new ERXKey<String>("cTypeExclusion");
  public static final ERXKey<String> C_TYPE_POP_TYPE_EXCLUSION = new ERXKey<String>("cTypePopTypeExclusion");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> L_TYPE_EXCLUSION = new ERXKey<String>("lTypeExclusion");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> TYPE_ABSENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence>("typeAbsence");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cTypeExclusion";

	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String C_TYPE_POP_TYPE_EXCLUSION_KEY = "cTypePopTypeExclusion";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String L_TYPE_EXCLUSION_KEY = "lTypeExclusion";

// Attributs non visibles
	public static final String C_TYPE_ABSENCE_KEY = "cTypeAbsence";

//Colonnes dans la base de donnees
	public static final String C_TYPE_EXCLUSION_COLKEY = "C_TYPE_EXCLUSION";
	public static final String C_TYPE_POP_TYPE_EXCLUSION_COLKEY = "C_TYPE_POP_TYPE_EXCLUSION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String L_TYPE_EXCLUSION_COLKEY = "newAttribute";

	public static final String C_TYPE_ABSENCE_COLKEY = "C_TYPE_ABSENCE";


	// Relationships
	public static final String TYPE_ABSENCE_KEY = "typeAbsence";



	// Accessors methods
  public String cTypeExclusion() {
    return (String) storedValueForKey(C_TYPE_EXCLUSION_KEY);
  }

  public void setCTypeExclusion(String value) {
    takeStoredValueForKey(value, C_TYPE_EXCLUSION_KEY);
  }

  public String cTypePopTypeExclusion() {
    return (String) storedValueForKey(C_TYPE_POP_TYPE_EXCLUSION_KEY);
  }

  public void setCTypePopTypeExclusion(String value) {
    takeStoredValueForKey(value, C_TYPE_POP_TYPE_EXCLUSION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lTypeExclusion() {
    return (String) storedValueForKey(L_TYPE_EXCLUSION_KEY);
  }

  public void setLTypeExclusion(String value) {
    takeStoredValueForKey(value, L_TYPE_EXCLUSION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence typeAbsence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence)storedValueForKey(TYPE_ABSENCE_KEY);
  }

  public void setTypeAbsenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence oldValue = typeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ABSENCE_KEY);
    }
  }
  

/**
 * Créer une instance de EOTypeExclusion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeExclusion createEOTypeExclusion(EOEditingContext editingContext, String cTypeExclusion
, String cTypePopTypeExclusion
, NSTimestamp dCreation
, NSTimestamp dModification
, String lTypeExclusion
			) {
    EOTypeExclusion eo = (EOTypeExclusion) createAndInsertInstance(editingContext, _EOTypeExclusion.ENTITY_NAME);    
		eo.setCTypeExclusion(cTypeExclusion);
		eo.setCTypePopTypeExclusion(cTypePopTypeExclusion);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLTypeExclusion(lTypeExclusion);
    return eo;
  }

  
	  public EOTypeExclusion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeExclusion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeExclusion creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeExclusion creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOTypeExclusion object = (EOTypeExclusion)createAndInsertInstance(editingContext, _EOTypeExclusion.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeExclusion localInstanceIn(EOEditingContext editingContext, EOTypeExclusion eo) {
    EOTypeExclusion localInstance = (eo == null) ? null : (EOTypeExclusion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeExclusion#localInstanceIn a la place.
   */
	public static EOTypeExclusion localInstanceOf(EOEditingContext editingContext, EOTypeExclusion eo) {
		return EOTypeExclusion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOTypeExclusion>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeExclusion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeExclusion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeExclusion> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeExclusion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeExclusion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeExclusion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeExclusion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeExclusion> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeExclusion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeExclusion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeExclusion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeExclusion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeExclusion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeExclusion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
