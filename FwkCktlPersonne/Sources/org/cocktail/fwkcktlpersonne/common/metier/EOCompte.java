/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Date;
import java.util.Enumeration;
import java.util.Random;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.controles.ControleSpecifique;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlpersonne.server.util.PasswordService;
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Gestion des objets Compte (comptes d'accès au Système d'information).
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EOCompte extends _EOCompte implements ICompte {

	private static final long serialVersionUID = 3687982978620361186L;

	public static final Logger logger = Logger.getLogger(EOCompte.class);

	public static final String TO_COMPTE_SAMBA_KEY = "toCompteSamba";

	public String pwdClair;

	public static final String CPT_VALIDE_OUI = "O";
	public static final String CPT_VALIDE_NON = "N";
	public static final String CPT_CHARTE_OUI = "O";
	public static final String CPT_CHARTE_NON = "N";
	public static final String CPT_LISTE_ROUGE_OUI = "O";
	public static final String CPT_LISTE_ROUGE_NON = "N";
	public static final String CPT_CRYPTE_OUI = "O";
	public static final String CPT_PRINCIPAL_OUI = "O";
	public static final String CPT_PRINCIPAL_NON = "N";

	public static final String PARAM_OUI = "O";
	public static final String PARAM_NON = "N";

	// Parametres password
	public static final String PARAM_PASSWORD_LENGTH_MIN = "PASSWORD_LENGTH_MIN";
	public static final String PARAM_PASSWORD_LENGTH_MAX = "PASSWORD_LENGTH_MAX";
	public static final String PARAM_PASSWORD_AUTHORIZED_CHARS = "PASSWORD_AUTHORIZED_CHARS";

	public static final String PARAM_PASSWORD_VALID_PERIOD_CREATE = "PASSWORD_VALID_PERIOD_CREATE";
	public static final String PARAM_PASSWORD_VALID_PERIOD_MODIFY = "PASSWORD_VALID_PERIOD_MODIFY";
	public static final String PARAM_PASSWORD_VALID_PERIOD_MODIFY_ADMIN = "PASSWORD_VALID_PERIOD_MODIFY_ADMIN";

	public static final String PASSWORD_FOR_VLAN_G = "*";

	private static Integer passwordValidPeriodCreate = null;
	private static Integer passwordValidPeriodModify = null;
	private static Integer passwordValidPeriodModifyAdmin = null;

	public static final EOQualifier QUAL_CPT_VALIDE_OUI = new EOKeyValueQualifier(EOCompte.CPT_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOCompte.CPT_VALIDE_OUI);
	public static final EOQualifier QUAL_CPT_VALIDE_NON = new EOKeyValueQualifier(EOCompte.CPT_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOCompte.CPT_VALIDE_NON);
	public static final EOQualifier QUAL_CPT_VLAN_X = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_X);
	public static final EOQualifier QUAL_CPT_VLAN_P = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_P);
	public static final EOQualifier QUAL_CPT_VLAN_R = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_R);
	public static final EOQualifier QUAL_CPT_VLAN_E = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_E);
	public static final EOQualifier QUAL_CPT_FIN_VALIDITE_KO = new EOKeyValueQualifier(EOCompte.CPT_FIN_VALIDE_KEY, EOQualifier.QualifierOperatorLessThan, new NSTimestamp());

	public static final EOQualifier QUAL_CPT_VLAN_D = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_D);
	public static final EOQualifier QUAL_CPT_VLAN_G = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, EOVlans.VLAN_G);

	/** Qualifier pour obtenir les comptes sur les VLAN P ou R */
	public static final EOQualifier QUAL_CPT_VLAN_P_R = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
			QUAL_CPT_VLAN_P, QUAL_CPT_VLAN_R
	}));

	public static final EOQualifier QUAL_CPT_INTERNE = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
			QUAL_CPT_VLAN_P_R, QUAL_CPT_VLAN_E
	}));

	public static final EOQualifier QUAL_CPT_NON_VALIDE = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
			QUAL_CPT_VALIDE_NON, QUAL_CPT_FIN_VALIDITE_KO
	}));

	public static final EOQualifier QUAL_CPT = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
			QUAL_CPT_VALIDE_OUI, QUAL_CPT_NON_VALIDE
	}));

	public static final EOSortOrdering SORT_VLANS_PRIORITE = EOSortOrdering.sortOrderingWithKey(TO_VLANS_KEY + "." + EOVlans.PRIORITE_KEY, EOSortOrdering.CompareAscending);

	public static final String TYAP_STRID_ANNUAIRE = "ANNUAIRE";

	public static final String EOCompteDidInsertNotification = "EOCompteDidInsertNotification";

	@Inject
	private static CompteService compteService = new CompteService();
	private static PasswordService passwordService = new PasswordService();

	public EOCompte() {
		super();
	}

	private boolean isEditingSelfPassword() {
		// On vérifie que le compte appartient bien à l'utilisateur
		if (cptModificateur() != null) {
			PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, cptModificateur());
			if (logger.isDebugEnabled()) {
				logger.debug("Vérification que le compte appartient bien au dernier utilisateur l'ayant modifié (persId / cptModificateur =" + persId() + " / " + cptModificateur() + ").");
			}
			if (!this.persId().equals(appUser.getPersId()))
				return false;
		}

		// On regarde que les seules keys modifiées sont celles relatives au mot de passe
		NSArray<String> authorizedKeys = new NSArray<String>(new String[] {
				CPT_PASSWD_KEY, CPT_PASSWD_CLAIR_KEY, D_MODIFICATION_KEY, CPT_MODIFICATEUR_KEY, TO_PASSWORD_HISTORIES_KEY, TO_COMPTE_SAMBAS_KEY, CPT_QUESTION_KEY, CPT_REPONSE_KEY
		});
		for (Object key : this.changesFromCommittedSnapshot().allKeys())
			if (!authorizedKeys.containsObject(key))
				return false;
		return true;
	}

	/**
	 * Méthode qui évalue si un changement a eu lieu pour la valeur de l'UID et du GID
	 * 
	 * @return true si le GID ou l'UID a changé
	 */
	private boolean isUidGidChanged() {

		// On regarde que les seules keys modifiées sont celles relatives soit à l'UID soit au GID
		NSArray<String> testedKeys = new NSArray<String>(new String[] {
				CPT_UID_KEY, CPT_GID_KEY
		});
		for (Object key : this.changesFromCommittedSnapshot().allKeys())
			if (testedKeys.containsObject(key))
				return true;
		return false;
	}

	/**
	 * Méthode qui évalue si un changement dans la String du login a eu lieu
	 * 
	 * @return true si le login a changé
	 */
	private boolean isLoginChanged() {
		// On ne regarde que la key relative au login du compte
		NSArray<String> testedKeys = new NSArray<String>(new String[] {
				CPT_LOGIN_KEY
		});
		for (Object key : this.changesFromCommittedSnapshot().allKeys())
			if (testedKeys.containsObject(key))
				return true;
		return false;
	}

	/**
	 * Contrôles :
	 * <ul>
	 * <li>Vérifier que le format du login est correct ({@link ControleSpecifique#checkLoginFormatCorrect(EOEditingContext, String)})</li>
	 * <li>Vérifier qu'il n'y a pas deux comptes pour la meme personne sur le même vlan ( {@link EOVlans})</li>
	 * <li>Vérifier qu'il n'y a pas de compte d'une autre personne avec le même login.</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateObjectMetier() throws NSValidation.ValidationException {
		try {
			trimAllString();
			miseAJourValidite();
			setDModification(new NSTimestamp());
			checkContraintesObligatoires();
			checkContraintesLongueursMax();

			// Un utilisateur doit avoir le droit de changer son propre mot de
			// passe, sans avoir de droit de modification
			if (!isEditingSelfPassword()) {
				// Si on est dans la création de compte
				if (this.editingContext().insertedObjects().contains(this)) {
					checkDroitCreation();
				}
				if (verifierDroitsModification()) {
					checkDroitModification();
				}
			}

			if (verifierDroitsModification()) {
				fixPersIdModification();
			}

			ControleSpecifique.checkLoginFormatCorrect(editingContext(), cptLogin());

			checkDoublonsForVlan();
			checkDoublonsForLogin();

			/**
			 * Si nous sommes sur le VLAN X, les règles sur le login n'ont pas lieu d'être
			 */
			//TODO Il serait peut-être intéressant de factoriser toute la partie sur le login dans une méthode et de rajouter des tests unitaires.
			if (isLoginChanged()) {
				int lMin = compteService.getLoginLengthMin(this.editingContext()).intValue();
				int lMax = compteService.getLoginLengthMax(this.editingContext()).intValue();
				if (cptLogin().length() < lMin || cptLogin().length() > lMax) {
					throw new NSValidation.ValidationException("Le login ("
							+ cptLogin().trim() + ") doit comporter au moins "
							+ lMin + " caracteres et au plus " + lMax
							+ " caracteres.");
				}
			}
			if (toVlans() == null || NSKeyValueCoding.NullValue.equals(toVlans())) {
				throw new NSValidation.ValidationException("Le VLAN est obligatoire.");
			}

			if (isLoginChanged()) {
				//verifier les unicites
				//Verifier que le login est unique
				final EOCompte doublonLogin = EOCompte.fetchByKeyValue(
						this.editingContext(), EOCompte.CPT_LOGIN_KEY,
						cptLogin());
				if (doublonLogin != null
						&& !this.editingContext()
								.globalIDForObject(doublonLogin)
								.equals(this.editingContext()
										.globalIDForObject(this))) {
					throw new NSValidation.ValidationException("Le login "
							+ doublonLogin.cptLogin()
							+ " est deja affecte a un autre compte.");
				}
			}
			//Verifier selon les VLANs que les infos sont bien saisies
			if (isSaisieUnixInfoObligatoire(toVlans())) {
				//if (EOVlans.VLAN_P.equals(toVlans().cVlan()) || EOVlans.VLAN_R.equals(toVlans().cVlan()) || EOVlans.VLAN_E.equals(toVlans().cVlan())) {
				if (cptUid() == null) {
					throw new NSValidation.ValidationException("Le UID doit etre defini pour le VLAN " + toVlans().cVlan());
				}
				if (cptGid() == null) {
					throw new NSValidation.ValidationException("Le GID doit etre defini pour le VLAN " + toVlans().cVlan());
				}
				if (MyStringCtrl.isEmpty(cptHome())) {
					throw new NSValidation.ValidationException("Le HOME doit etre defini pour le VLAN " + toVlans().cVlan());
				}
				if (MyStringCtrl.isEmpty(cptShell())) {
					throw new NSValidation.ValidationException("Le SHELL doit etre defini pour le VLAN " + toVlans().cVlan());
				}

				// Pas de contrôle des valeurs de l'UID ou/et du GID si on n'a pas modifié la valeur existante
				if (isUidGidChanged()) {
					Integer rangeMinUid = Integer.parseInt(EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY));
					Integer rangeMaxUid = Integer.parseInt(EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY));

					if (cptUid() < rangeMinUid || cptUid() > rangeMaxUid) {
						throw new ValidationException("L'UID doit être dans l'intervalle [" + rangeMinUid + "-" + rangeMaxUid + "]");
					}

					Integer rangeMinGid = Integer.parseInt(EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY));
					Integer rangeMaxGid = Integer.parseInt(EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY));

					if (cptGid() < rangeMinGid || cptGid() > rangeMaxGid) {
						throw new ValidationException("Le GID doit être dans l'intervalle [" + rangeMinGid + "-" + rangeMaxGid + "]");
					}
				}

				//Verifier que le uid est unique
				EOQualifier qualUid = EOQualifier.qualifierWithQualifierFormat(EOCompte.CPT_UID_KEY + "=%@ and " + EOCompte.TO_VLANS_KEY + "=%@", new NSArray(new Object[] {
						cptUid(), toVlans()
				}));
				final EOCompte doublonUid = EOCompte.fetchFirstByQualifier(this.editingContext(), qualUid);
				if (doublonUid != null && doublonUid.toVlans().equals(this.toVlans()) && !this.editingContext().globalIDForObject(doublonUid).equals(this.editingContext().globalIDForObject(this))) {
					throw new NSValidation.ValidationException("L'UID " + doublonUid.cptUid() + " est deja affecte (sur le meme VLAN) au compte portant le login " + doublonUid.cptLogin());
				}

			}

			if (this.toIndividu() != null) {
				compteService.determinationCptePrincipalAvecCategorie(
						editingContext(), this.toIndividu().persId());
			}
			if (MyStringCtrl.isEmpty(cptCrypte())) {
				setCptCrypte(CPT_CRYPTE_OUI);
			}

			// Remplir le type de cryptage affecté au vlan si celui-ci non rempli
//			initialiseTypeCryptage();
			setToTypeCryptageRelationship(null);
			initialiseTypeCryptage();

			super.validateObjectMetier();
		} catch (NSValidation.ValidationException e1) {
			e1.printStackTrace();
//			throw e1;
			throw new NSValidation.ValidationException(e1.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage());
		}

	}

	@Override
	public void didInsert() {
		if (editingContext() != null && editingContext().parentObjectStore() instanceof EOObjectStoreCoordinator) {
			NSNotificationCenter.defaultCenter().postNotification(new NSNotification(EOCompte.EOCompteDidInsertNotification, this));
		}
		super.didInsert();
	}

	/**
	 * Permet de renseigner le persidModificateur de la personne associé au compte, si celui-ci n'est pas encore renseigné.
	 */
	private void fixPersIdModification() {
		if (cptModificateur() != null && toPersonne() != null) {
			if (toPersonne().persIdModification() == null) {
				toPersonne().setPersIdModification(cptModificateur());
			}
		}
	}

	/**
	 * Verifie si ce login est utilise pour une autre personne.
	 */
	private void checkDoublonsForLogin() {
		NSArray doublons = new NSArray();
		EOCompte doublon = EOCompte.compteForLogin(editingContext(), cptLogin());

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(CPT_LOGIN_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, cptLogin()));
		quals.addObject(new EOKeyValueQualifier(PERS_ID_KEY, EOQualifier.QualifierOperatorNotEqual, persId()));
		NSArray res = EOCompte.fetchAll(editingContext(), new EOAndQualifier(quals));
		if (res.count() > 0) {
			throw new NSValidation.ValidationException("Une autre personne possède ce login (" + ((EOCompte) res.objectAtIndex(0)).cptLogin() + ").");
		}
	}

	public static EOCompte compteForLogin(EOEditingContext editingContext, String cptLogin) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(CPT_LOGIN_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, cptLogin));
	}

	/**
	 * Verifie si un compte valide sur le même VLAN existe deja.
	 */
	private void checkDoublonsForVlan() {
		NSArray doublons = new NSArray();
		EOQualifier qualExistant = EOQualifier.qualifierWithQualifierFormat(EOCompte.CPT_VALIDE_KEY + "=%@ and " + EOCompte.TO_VLANS_KEY + "=%@ ", new NSArray(new Object[] {
				EOCompte.CPT_VALIDE_OUI, toVlans()
		}));
		if (toIndividu() != null) {
			doublons = toIndividu().toComptes(qualExistant);
		}
		if (toStructure() != null) {
			doublons = toStructure().toComptes(qualExistant);
		}
		Enumeration enumeration = doublons.objectEnumerator();
		while (enumeration.hasMoreElements()) {
			EOCompte object = (EOCompte) enumeration.nextElement();
			if (!editingContext().globalIDForObject(this).equals(editingContext().globalIDForObject(object))) {
				logger.warn("Erreur de cohérence, le compte en cours de validation : \n" + this.toLongString() + 
						" \n est sur le meme VLAN que le compte : \n" + object.toLongString());
				throw new NSValidation.ValidationException("Une personne ne peut avoir deux comptes valides sur le même VLAN " + toVlans().cVlan() + ".");
			}
		}

	}

	/**
	 * Initialisation lors de la création d'un nouveau compte
	 * <ul>
	 * <li>Date de creation</li>
	 * <li>cptCharte a N</li>
	 * <li>cptvalide a O</li>
	 * <li>cptListeRouge a N</li>
	 * <li>cptPrincipal a N</li>
	 * <li>type VPN a NO</li>
	 * </ul>
	 */
	@Override
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
		setDCreation(new NSTimestamp());
		this.setCptCharte(EOCompte.CPT_CHARTE_NON);
		this.setCptValide(EOCompte.CPT_VALIDE_OUI);
		this.setCptListeRouge(EOCompte.CPT_LISTE_ROUGE_NON);
		this.setCptPrincipal(EOCompte.CPT_PRINCIPAL_NON);
		this.setToTypeVpnRelationship(EOTypeVpn.fetchByKeyValue(editingContext(), EOTypeVpn.TVPN_CODE_KEY, EOTypeVpn.TVPN_CODE_NO));
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	@Override
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}

	/**
	 * Si le type de cryptage n'est pas specifie, recuperer la methode de cryptage par defaut pour le VLAN et l'affecter.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	protected void initialiseTypeCryptage() throws NSValidation.ValidationException {
		if (toTypeCryptage() == null) {
			EOTypeCryptage tc = toVlans().toTypeCryptage();
			if (!ERXStringUtilities.stringEqualsString(toVlans().cVlan(), EOVlans.VLAN_G) && tc == null) {
				throw new NSValidation.ValidationException("Methode de cryptage non specifiee pour le VLAN " + toVlans().llVlan());
			}
			setToTypeCryptageRelationship(tc);
		}
	}

	/**
	 * Si le type est spécifié, on recherche la méthode de cryptage, sinon, on recherche la méthode de cryptage par défaut, et on l'affecte
	 * 
	 * @param ec
	 * @param typeCryptage
	 */
	protected void initialiseTypeCryptage(EOEditingContext ec, String typeCryptage) throws NSValidation.ValidationException {
		if (toTypeCryptage() == null) {
			EOTypeCryptage tc;
			if (typeCryptage != null) {
				// Types de cryptages present sur le SI
				tc = EOTypeCryptage.fetchByKeyValue(ec, EOTypeCryptage.TCRY_LIBELLE_KEY, typeCryptage);
				if (tc == null) {
					throw new NSValidation.ValidationException("Le type de cryptage specifie (" + typeCryptage + ") n'est pas defini dans le SI (table "
							+ EOTypeCryptage.ENTITY_TABLE_NAME + ").");
				}
				setToTypeCryptageRelationship(tc);
			} else {
				initialiseTypeCryptage();
			}
		}
	}

	/**
	 * @param ec
	 * @param typeCryptage
	 * @param passwordClair
	 * @return la chaine cryptee
	 */
	private String cryptePassword(EOEditingContext ec, EOTypeCryptage typeCryptage, String passwordClair) throws Exception {
		String passwordCrypte = null;

		String javaMethode = typeCryptage.tcryJavaMethode();

		if (MyStringCtrl.isEmpty(javaMethode)) {
			throw new Exception("Classe Java non defini pour le type de cryptage " + typeCryptage.tcryLibelle());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Cryptage du mot de passe avec la methode " + typeCryptage.tcryLibelle() + "/ " + javaMethode);
		}

		try {
			logger.info("le MdP est : " + passwordClair + " avant cryptage");
			passwordCrypte = CryptoCtrl.cryptPass(javaMethode, passwordClair);
			logger.info("le MdP crypté est : " + passwordCrypte);
		} catch (Exception e) {
			logger.error("erreur lors de l'encryptage", e);
			throw new Exception("Erreur lors de l'encryptage du password", e);
		}

		return passwordCrypte;
	}

	/**
	 * Ajoute le nouveau mot de passe dans l'historique Met à jour les date de fin de validité pour les mots de passe encore valides
	 * 
	 * @param cryptedPassword
	 * @throws Exception
	 */
	protected void updatePasswordHistory(ApplicationUser appUser, String cryptedPassword) throws Exception {
		if (compteService.isHistorisationActivePourVlanParticulier(this.editingContext(), toVlans().cVlan())) {
			// Cohérence de l'historique : on fixe les dates de fin de validité
			// des autres enregistrements à celle d'ajout
			NSArray oldHistories = this.toPasswordHistories();
			for (Object hist : oldHistories) {
				if (((EOPasswordHistory) hist).dFinValidite().after(DateCtrl.now())) {
					((EOPasswordHistory) hist).setDFinValidite(DateCtrl.now());
				}
			}
			// Ajout du mot de passe courrant
			EOPasswordHistory eoPWDH = this.createToPasswordHistoriesRelationship();
			eoPWDH.setPwdhPasswd(this.cptPasswd());
			eoPWDH.setToTypeCryptageRelationship(toTypeCryptage());
			// Durée
			Integer dureeValidite;
			// On fixe la durée de validité suivant si on modifie (admin ou
			// utilisateur) ou si on créé le compte
			if (this.editingContext().insertedObjects().contains(this)) {
				dureeValidite = getPasswordValidPeriodCreate();
			} else if (appUser.getPersId().equals(this.persId())) {
				dureeValidite = getPasswordValidPeriodModify();
			} else {
				dureeValidite = getPasswordValidPeriodModifyAdmin();
			}
			eoPWDH.setDFinValidite(eoPWDH.dCreation().timestampByAddingGregorianUnits(0, 0, dureeValidite, 0, 0, 0));
			System.out.println(eoPWDH.dFinValidite().toString());
		}
	}

	/**
	 * Effectue le changement de mot de passe en le cryptant avec la méthode de cryptage associée à ce compte (VLan).<br>
	 * Met à jour la liste de l'historique des mots de passe, si l'historisation est activée Le changement de mot de passe n'est propagé dans le
	 * compte samba correspondant avec cette méthode, utilisez à la place, {@link EOCompte#changePassword(ApplicationUser, String, boolean, boolean)}.
	 * 
	 * @param appUser L'utilisateur
	 * @param passwordClair Obligatoire : Le mot de passe en clair qui sera crypté et qui remplacera le mot de passe actuel. Il ne doit pas être null,
	 *            et respecter la longueur minimale {@link EOCompte#PASSWORD_LENGTH_MIN}.
	 * @param stockePasswordClair TRUE si le mot de passe doit être stocké en clair (en plus du crypte) dans la table.
	 * @throws Exception : Si passwordClair est null ou longueur inférieure à {@link EOCompte#PASSWORD_LENGTH_MIN}. Si la méthode de cryptage est
	 *             indéterminée {@link EOCompte#toTypeCryptage()}.
	 */
	public void changePassword(ApplicationUser appUser, String passwordClair,
			boolean stockePasswordClair) throws Exception {
		changePassword(appUser, passwordClair, stockePasswordClair, false);
	}

	/**
	 * Effectue le changement de mot de passe en le cryptant avec la méthode de cryptage associée à ce compte (VLan).<br>
	 * Met à jour la liste de l'historique des mots de passe, si l'historisation est activée Si Samba est activé, le changement de mot de passe est
	 * propagé dans le compte samba correspondant suivant les 2 méthodes de cryptage propres à samba.
	 * 
	 * @param appUser L'utilisateur
	 * @param passwordClair Obligatoire : Le mot de passe en clair qui sera crypté et qui remplacera le mot de passe actuel. Il ne doit pas être null,
	 *            et respecter la longueur minimale {@link EOCompte#PASSWORD_LENGTH_MIN}.
	 * @param stockePasswordClair TRUE si le mot de passe doit être stocké en clair (en plus du crypte) dans la table.
	 * @param propagateToSamba TRUE si le mot de passe doit être propagé au compte Samba associé.
	 * @throws Exception : Si passwordClair est null ou longueur inférieure à {@link EOCompte#PASSWORD_LENGTH_MIN}. Si la méthode de cryptage est
	 *             indéterminée {@link EOCompte#toTypeCryptage()}.
	 */
	public void changePassword(ApplicationUser appUser, String passwordClair,
			boolean stockePasswordClair, boolean propagateToSamba) throws Exception {
		// Si on est sur le VLAN groupe, pas de vérification, on stocke juste le mot de passe
		if (ERXStringUtilities.stringEqualsString(toVlans().cVlan(), EOVlans.VLAN_G)) {
			this.setCptPasswd(PASSWORD_FOR_VLAN_G);
			this.setToTypeCryptageRelationship(EOTypeCryptage.fetchFirstByQualifier(editingContext(), EOTypeCryptage.TCRY_LIBELLE.eq(EOTypeCryptage.TCRY_LIBELLE_NOCRYPT)));
			return;
		}
		// Vérifications minimales
		passwordClair = passwordService.checkPassword(passwordClair, toVlans().cVlan(), editingContext(), this);
		pwdClair = passwordClair;
		// Vérification du type de cryptage
		if (toTypeCryptage() == null) { // TODO : Si toTypeCryptage==null && Vlan !=null alors mettre par defaut le type cryptage du Vlan sinon message d'erreur.
			throw new Exception("Impossible de déterminer la methode de cryptage à appliquer.");
		}
		// Cryptage et modification
		this.setCptPasswd(cryptePassword(this.editingContext(),
				toTypeCryptage(), passwordClair));
		// Stockage du mot de passe en clair
		if (stockePasswordClair) {
			this.setCptPasswdClair(passwordClair);
		} else {
			this.setCptPasswdClair(null);
		}
		// Mise à jour de l'historique
		updatePasswordHistory(appUser, this.cptPasswd());
		// Mise à jour du modificateur
		this.setCptModificateur(appUser.getPersId());
		// Propagation vers le compte Samba
		if (propagateToSamba) {
			EOCompteSamba.changePassword(this, passwordClair);
		}
	}

	// Utilitaires
	/**
	 * Renvoi une chaine au hasard, sur 8 caracteres
	 * 
	 * @return
	 * @deprecated Utiliser la fonction de génération de mot de passe en fonction de règles {@link #getRandomPasswordWithRules()}
	 */
	private static String getRandomPassword() {
		String chaine = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789/+-";
		Random x = new Random();
		String password = "";
		for (int i = 0; i < 8; i++) {
			password = password.concat((new Character(chaine.charAt(x.nextInt(chaine.length())))).toString());
		}
		return password;
	}

	/**
	 * Renvoie un nom de login par defaut. C'est la premiere lettre de prenom (+premiere lettre prenom compose) et 7 lettres au maximum du nom
	 * d'utilisateur. Ajoute les numeros "01", "02",..., si le login existe deja dans la base.
	 * 
	 * @deprecated eleguer la creation du login a une fonction pl/sql
	 */
	private static String getNewDefaultLogin(EOEditingContext ec, IPersonne personne) {
		String prenom = MyStringCtrl.chaineSansAccents(personne.persLc()).toUpperCase();
		String nom = MyStringCtrl.chaineSansAccents(personne.persLibelle()).toUpperCase();
		nom = MyStringCtrl.toBasicString(nom, "abcdefghijklmnopqrstuvwxyz1234567890");

		NSArray prenoms = NSArray.componentsSeparatedByString(MyStringCtrl.toBasicString(prenom), "-");
		String defaultLogin = "";
		for (int i = 0; i < prenoms.count(); i++) {
			defaultLogin += ((String) prenoms.objectAtIndex(i)).substring(0, 1);
		}
		defaultLogin += MyStringCtrl.toBasicString(nom);
		if (defaultLogin.length() > 8) {
			defaultLogin = defaultLogin.substring(0, 8);
		}
		return getNewLogin(ec, defaultLogin.toLowerCase());
	}

	/**
	 * Retourne le nouveau login en se basant sur le login propose. Ajoute les numeros "01", "02",..., si le login existe deja dans la base.
	 * 
	 * @deprecated eleguer la creation du login a une fonction pl/sql
	 */
	private static String getNewLogin(EOEditingContext ec, String newLogin) {
		String loginCut;
		int i;
		if (loginExiste(ec, newLogin)) {
			loginCut = newLogin;
			if (loginCut.length() > 6) {
				loginCut = loginCut.substring(0, 6);
			}
			i = 0;
			do {
				i++;
				newLogin = loginCut + MyStringCtrl.get0Int(i, 2);
			} while (loginExiste(ec, newLogin));
		}
		return newLogin;
	}

	private static String getNewDefaultLoginFromBD(EOEditingContext ec, IPersonne personne) throws Exception {
		return SpConsLogin.getNewLoginForPersonne(ec, personne);
	}

	/**
	 * Teste si le login donne existe deja dans la base.
	 */
	private static boolean loginExiste(EOEditingContext ec, String aLogin) {
		EOQualifier myQual = EOQualifier.qualifierWithQualifierFormat(EOCompte.CPT_LOGIN_KEY + " = '" + aLogin + "'", null);
		NSArray objects = EOCompte.fetchAll(ec, myQual, null);
		//		NSArray objects = AFinder.fetchArray(ec, EOCompte.ENTITY_NAME, myQual, null, true);
		return (objects.count() > 0);
	}

	/**
	 * Initialise un compte a partir d'un inidividu ou d'une structure.
	 * 
	 * @param ec
	 * @param appUser Utilisateur qui cree le compte
	 * @param personne Individu ou structure a partir duquel creer un compte.
	 * @param login Facultatif. Si non specifie, un login auto est cree.
	 * @param passwordClair Facultatif. Si non specifie, un password est cree.
	 * @param stockePasswordClair TRUE si le mot de passe doit etre stocke en clair (en plus du crypte) dans la table.
	 * @param typeCryptage Facultatif. Type de cryptage a utiliser pour crypter le mot de passe (doit correspondre a un libelle defini dans la table
	 *            GRHUM.TYPE_CRYPTAGE). Si non specifie, on recherchera le type de cryptage associe au VLAN. Si non associe au VLAN, une exception
	 *            sera declenchee.
	 * @param vLanCode Obligatoire. Doit correspondre a un code de VLAN de la table GRHUM_VLANS.
	 * @param debutValidite
	 * @param finValidite
	 * @throws Exception
	 */
	public void initialiseCompte(EOEditingContext ec, PersonneApplicationUser appUser, IPersonne personne, String vLanCode, String login, String passwordClair, boolean stockePasswordClair, String typeCryptage, NSTimestamp debutValidite, NSTimestamp finValidite) throws Exception {
		if (personne == null) {
			throw new Exception("La personne doit etre specifiee.");
		}
		if (MyStringCtrl.isEmpty(vLanCode)) {
			throw new Exception("Le code du VLAN doit etre specifie.");
		}

		EOVlans vlan = EOVlans.fetchByKeyValue(ec, EOVlans.C_VLAN_KEY, vLanCode);
		if (vlan == null) {
			throw new Exception("Le VLAN " + vLanCode + " n'a pas ete trouve dans la table GRHUM.VLANS.");
		}

		//Verifier qu'un compte n'est pas deja present sur le meme vlan
		Integer persid = personne.persId();
		if (persid == null) {
			throw new Exception("Le PERSID ne doit pas etre null.");
		}

		EOQualifier qualExistant = EOQualifier.qualifierWithQualifierFormat(EOCompte.CPT_VALIDE_KEY + "=%@ and " + EOCompte.TO_VLANS_KEY + "=%@ and " + EOCompte.PERS_ID_KEY + "=%@", new NSArray(new Object[] {
				EOCompte.CPT_VALIDE_OUI, vlan, persid
		}));
		EOCompte compteExistant = EOCompte.fetchFirstByQualifier(ec, qualExistant);
		if (compteExistant != null && !editingContext().globalIDForObject(this).equals(editingContext().globalIDForObject(compteExistant))) {
			throw new Exception("Un compte valide existe deja sur le meme VLAN, vous ne pouvez pas en creer un nouveau.");
		}

		//Verifier les parametres
		if (login == null || login.trim().length() == 0) {
			//construire un login en auto
			//login = getNewDefaultLogin(ec, personne);
			login = getNewDefaultLoginFromBD(ec, personne);

		}

		int lMin = compteService.getLoginLengthMin(this.editingContext()).intValue();
		int lMax = compteService.getLoginLengthMax(this.editingContext()).intValue();

		if (!verifieLongueurMin(login, lMin)) {
			throw new Exception("Le login (" + login.trim() + ") doit comporter au moins " + lMin + " caracteres et au plus " + lMax + " caracteres.");
		}

		if (!verifieLongueurMax(login, lMax)) {
			throw new Exception("Le login (" + login.trim() + ") doit comporter au moins " + lMin + " caracteres et au plus " + lMax + " caracteres.");
		}

		this.setCptCreateur(appUser.iPersonne().persId());
		this.setCptModificateur(appUser.iPersonne().persId());

		if (passwordClair == null || passwordClair.trim().length() == 0) {
			//construire un password en auto
			//			passwordClair = getRandomPasswordWithRules();
			int i = 0;
			while ((passwordClair == null || !passwordService.isPasswordOK(passwordClair, vLanCode, ec, compteExistant)) && i < 50) {
				passwordClair = passwordService.getRandomPasswordWithPrivateRules(ec, vLanCode);
				i++;
			}

			if (i >= 50) {
				throw new Exception("Impossible de générer un mot de passe valide.");
			}
		}
		// Ajout d'une boucle pour obtenir un nouveau mot de passe qui n'est pas dans l'historique
		if (compteService.isHistorisationActivePourVlanParticulier(ec, vLanCode)) {
			while (this.toPasswordHistories().contains(passwordClair)) {
				passwordClair = passwordService.getRandomPasswordWithPrivateRules(ec, vLanCode);
			}
		}

		this.setToVlansRelationship(vlan);
		if (this.toVlans().toTypeVpn() != null && !NSKeyValueCoding.NullValue.equals(this.toVlans().toTypeVpn())) {
			this.setToTypeVpnRelationship(this.toVlans().toTypeVpn());
		}
		this.initialiseTypeCryptage(ec, typeCryptage);
		this.setCptLogin(login);
		this.setCptCrypte(EOCompte.CPT_CRYPTE_OUI);

		this.changePassword(appUser, passwordClair, stockePasswordClair, isPropagerASambaActif());
		//		this.changePassword(appUser, passwordClair, stockePasswordClair);

		this.setCptDebutValide(debutValidite);
		this.setCptFinValide(finValidite);

		personne.addToToComptesRelationship(this);
		this.setPersId(persid);

		//Definir le compte principal. Un seul compte principal par personne.
		EOQualifier qualPrincipal = EOQualifier.qualifierWithQualifierFormat(EOCompte.CPT_VALIDE_KEY + "=%@ and " + EOCompte.PERS_ID_KEY + "=%@ and " + EOCompte.CPT_PRINCIPAL_KEY + "=%@", new NSArray(new Object[] {
				EOCompte.CPT_VALIDE_OUI, persid, EOCompte.CPT_PRINCIPAL_OUI
		}));
		EOCompte comptePrincipal = EOCompte.fetchFirstByQualifier(ec, qualPrincipal);
		if (comptePrincipal == null) {
			this.setCptPrincipal(EOCompte.CPT_PRINCIPAL_OUI);
		} else {
			compteService.determinationCptePrincipalAvecCategorie(ec, this.toIndividu().persId());
		}

		// initialisation des infos unix si besoin
		if (EOCompte.isSaisieUnixInfoObligatoire(vlan)) {
			// Création et utilisation des paramètres Home et Shell pour ne plus avoir ces données en dur.
			//			setCptHome("/Users/" + cptLogin());
			setCptHome(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_HOME) + cptLogin());
			//			setCptShell("/bin/bash");
			setCptShell(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_SHELL));
			//GID par defaut
			Integer maxGID = initialiseGID(ec);
			setCptGid(maxGID); //TODO : Eventuellement sortir dans un parametre le pas d'augmentation pour le GID par defaut.

			// UID par defaut
			Integer maxUID = initialiseUID(ec); // ATTENTION : cette méthode peut renvoyer une Exception de Validation : bien veiller à la placer en dernier traitement.
			setCptUid(maxUID);
		}

	}

	public boolean isPropagerASambaActif() {
		return "YES".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_PROPAGATE_TO_SAMBA_ENABLED));
	}

	/**
	 * @param ec
	 * @param vlanCode
	 * @param persId
	 * @return Le premier compte valide trouve pour le vlan et le persid specifie. Si vlan=null, les comptes sont tries suivant la priorite du vlan.
	 */
	public static EOCompte compteForPersId(EOEditingContext ec, String vlanCode, Number persId) {
		final NSMutableArray quals = new NSMutableArray();
		quals.addObject(QUAL_CPT_VALIDE_OUI);
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompte.PERS_ID_KEY + "=%@", new NSArray(new Object[] {
				persId
		})));
		if (!MyStringCtrl.isEmpty(vlanCode)) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY + "=%@", new NSArray(new Object[] {
					vlanCode
			})));
		}
		NSArray compteExistants = EOCompte.fetchAll(ec, new EOAndQualifier(quals), null);
		// TODO : sortir compteForPersId dans un finder/service pour éviter le static et donc injecter le compte service
		CompteService compteService = new CompteService();
		NSArray<EOCompte> sorted = compteService.classerParPriorite(compteExistants);
		return (EOCompte) (sorted.count() > 0 ? sorted.objectAtIndex(0) : null);

	}

	/**
	 * @param ec
	 * @param persId
	 * @return Le premier compte valide trouve le persid specifie (l'ordre des comptes depend de la priorite du vlan affecte au compte)
	 */
	public static EOCompte compteForPersId(EOEditingContext ec, Number persId) {
		return compteForPersId(ec, null, persId);

	}

	/**
	 * @return Le compte email associe s'il existe (renvoie le premier element des toCompteEmails classes par priorite).
	 */
	public EOCompteEmail toCompteEmail() {
		return (EOCompteEmail) (toCompteEmails().count() > 0 ? EOSortOrdering.sortedArrayUsingKeyOrderArray(toCompteEmails(), new NSArray(new Object[] {
				EOCompteEmail.SORT_PRIORITE
		})).objectAtIndex(0) : null);
	}

	/**
	 * @return Le compte sous forme affichable.
	 */
	public String affichage() {
		return cptLogin() + " (vlan " + toVlans().cVlan() + ")";
	}

	/**
	 * @return L'individu associe ou null.
	 */
	public EOIndividu toIndividu() {
		if (toIndividus().count() > 0) {
			return (EOIndividu) toIndividus().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * @return La structure associee ou null.
	 */
	public EOStructure toStructure() {
		if (toStructures().count() > 0) {
			return (EOStructure) toStructures().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * Verifie si l'utilisateur (si saisi) a les droits de création d'un compte.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkDroitCreation() throws NSValidation.ValidationException {
		if (cptCreateur() != null) {
			//SI VLAN X, pas de controle
			//sinon il faut avoir un droit (dans agent_compte)
			if (!EOVlans.VLAN_X.equals(toVlans().cVlan())) {
				try {
					PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, cptCreateur());
					if (!appUser.hasDroitCompteModification() || !appUser.hasDroitModificationIPersonne(toPersonne())) {
						throw new NSValidation.ValidationException("Vous n'avez pas le droit de créer un compte d'accès sur le vLan " + toVlans().llVlan());
					}
				} catch (Exception e) {
					throw new NSValidation.ValidationException(e.getMessage());
				}
			} else {
				PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, cptCreateur());
				if (!appUser.hasDroitCompteTempoVisualisation() || !appUser.hasDroitModificationIPersonne(toPersonne())) {
					throw new NSValidation.ValidationException("Vous n'avez pas le droit de créer un compte d'accès sur le vLan " + toVlans().llVlan());
				}
			}
		}
	}

	/**
	 * Verifie si l'utilisateur (si saisi) a les droits de modification d'un compte.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkDroitModification() throws NSValidation.ValidationException {
		if (cptModificateur() != null) {
			//SI VLAN X, pas de controle
			//sinon il faut avoir un droit (dans agent_compte)
			if (!EOVlans.VLAN_X.equals(toVlans().cVlan())) {
				try {
					logger.info("Verification des droits de modification pour le compte " + this.toLongString() + " sur un VLAN de type non X et pour la personne " + cptModificateur());
					PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, cptModificateur());
					boolean aPasDroitCompteModification = !appUser.hasDroitCompteModification();
					if (aPasDroitCompteModification) {
						logger.info("La personne " + cptModificateur() + " n'a pas les droits de modification de compte");
					}
					if (aPasDroitCompteModification || !appUser.hasDroitModificationIPersonne(toPersonne())) {
						throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier un compte d'accès sur le vLan " + toVlans().llVlan());
					}
				} catch (Exception e) {
					throw new NSValidation.ValidationException(e.getMessage());
				}
			} else {
				logger.info("Verification des droits de modification pour le compte " + this.toLongString() + " sur un VLAN de type X pour la personne " + cptModificateur());
				PersonneApplicationUser appUser = new PersonneApplicationUser(this.editingContext(), TYAP_STRID_ANNUAIRE, cptModificateur());
				boolean aPasDroitCompteTempoVisualisation = !appUser.hasDroitCompteTempoVisualisation();
				if (aPasDroitCompteTempoVisualisation) {
					logger.info("La personne " + cptModificateur() + " n'a pas les droits de de visualiser les comptes exterieurs");
				}
				if (aPasDroitCompteTempoVisualisation || !appUser.hasDroitModificationIPersonne(toPersonne())) {
					throw new NSValidation.ValidationException("Vous n'avez pas le droit de modifier un compte d'accès sur le vLan " + toVlans().llVlan());
				}
			}
		}
	}

	/**
	 * @return toIndividu() si pas null sinon toStructure().
	 */
	public IPersonne toPersonne() {
		return (toIndividu() != null ? toIndividu() : toStructure());
	}

	/**
	 * @return true si la saisie des GID, UID, etc. est obligatoire (dépend du VLAN).
	 */
	public static boolean isSaisieUnixInfoObligatoire(EOVlans vlan) {
		
		if (!FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.COMPTE_GERE_PAR_GRHUM)) {
			return false;
		}
		
		return EOVlans.VLAN_P.equals(vlan.cVlan()) || EOVlans.VLAN_R.equals(vlan.cVlan()) || EOVlans.VLAN_E.equals(vlan.cVlan());
	}

	/**
	 * @return La période de validité du mot de passe lorsqu'il est créé
	 * @throws Exception
	 */
	public Integer getPasswordValidPeriodCreate() throws Exception {
		if (passwordValidPeriodCreate == null) {
			String val = EOGrhumParametres.parametrePourCle(this.editingContext(), PARAM_PASSWORD_VALID_PERIOD_CREATE);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_VALID_PERIOD_CREATE + " n'est pas correctement renseigné.");
			}
			passwordValidPeriodCreate = Integer.valueOf(val);
		}
		return passwordValidPeriodCreate;
	}

	/**
	 * @return La période de validité du mot de passe lorsqu'il est modifié (par l'utilisateur)
	 * @throws Exception
	 */
	public Integer getPasswordValidPeriodModify() throws Exception {
		if (passwordValidPeriodModify == null) {
			String val = EOGrhumParametres.parametrePourCle(this.editingContext(), PARAM_PASSWORD_VALID_PERIOD_MODIFY);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_VALID_PERIOD_MODIFY + " n'est pas correctement renseigné.");
			}
			passwordValidPeriodModify = Integer.valueOf(val);
		}
		return passwordValidPeriodModify;
	}

	/**
	 * @return La période de validité du mot de passe lorsqu'il est modifié (par l'admin)
	 * @throws Exception
	 */
	public Integer getPasswordValidPeriodModifyAdmin() throws Exception {
		if (passwordValidPeriodModifyAdmin == null) {
			String val = EOGrhumParametres.parametrePourCle(this.editingContext(), PARAM_PASSWORD_VALID_PERIOD_MODIFY_ADMIN);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_VALID_PERIOD_MODIFY_ADMIN + " n'est pas correctement renseigné.");
			}
			passwordValidPeriodModifyAdmin = Integer.valueOf(val);
		}
		return passwordValidPeriodModifyAdmin;
	}

	/**
	 * @return le cptOrdre du compte pour consultation (pour le passage à la synchronisation par exemple...)
	 */
	public Integer _cptOrdreConsult() {
		NSDictionary pKeys = EOUtilities.primaryKeyForObject(editingContext(), this);
		return (pKeys != null) ? (Integer) pKeys.objectForKey(this.ENTITY_PRIMARY_KEY) : null;
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba toCompteSamba() {
		NSArray<EOCompteSamba> res = super.toCompteSambas();
		//		EOCompteSamba res = super.toCompteSambas(); toCompteSambas();

		if (res != null && res.count() > 0) {
			//		if (res != null ) {
			return ERXArrayUtilities.firstObject(res);
			//			return res;
		}
		return null;
	}

	/**
	 * @return Vrai si le temoin de signature de la charte informatique est vrai (cpt_charte)
	 * @see EOCompte#cptCharte()
	 */
	public boolean isCharteSigned() {
		return (!MyStringCtrl.isEmpty(this.cptCharte()) && this.cptCharte().equals(CPT_CHARTE_OUI));
	}

	/**
	 * @return Vrai si le temoin de validite du compte informatique est vrai
	 * @see EOCompte#cptValide()
	 */
	public boolean isCompteValide() {
		return (!MyStringCtrl.isEmpty(this.cptValide()) && this.cptValide().equals("O"));
	}

	/**
	 * @return "O" si le temoin de validité du compte est vrai
	 * @see EOCompte#cptValide()
	 */
	public String isValideAsString() {
		String validite = "N";
		if (cptValide().equals("O")) {
			validite = "O";
			if (cptFinValide() != null && cptFinValide().before(new NSTimestamp())) {
				validite = "N";
			}
		} else {
		}
		return validite;
	}

	/**
	 * @return "O" si le temoin de validité du compte est vrai
	 * @see EOCompte#cptValide()
	 */
	public void miseAJourValidite() {
		if (cptValide().equals("O")) {
			if (cptFinValide() != null && cptFinValide().before(new NSTimestamp())) {
				setCptValide("N");
			}
		} else {
			if (cptFinValide() != null && cptFinValide().after(new NSTimestamp())) {
				setCptValide("O");
			}
		}
	}

	/**
	 * @param value Une date de fin de validité du compte (ou <code>null</code> pour "sans fin de validité")
	 */
	public void setCptFinValide(Date value) {
		super.setCptFinValide(value == null ? null : new NSTimestamp(value));
	}
	
	/**
	 * @return Retourne la valeur de l'UID lors de la création de compte en fonction du range de valeur
	 * @return si celui-ci est défini
	 * @see EOGrhumParametres.GRHUM_UID_MAX
	 * @see EOGrhumParametres.GRHUM_UID_MIN
	 */

	protected Integer initialiseUID(EOEditingContext ec) {
		//		Integer valueUID = 1000;
		//		Integer maxUID = 1000;

		Integer valueUID = Integer.parseInt(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY));
		Integer maxUID = Integer.parseInt(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY));

		EOQualifier qual = compteService.determinationDomaineVlans(ec, this.toVlans().cVlan());
		NSArray<EOCompte> tableUID = EOCompte.fetchAll(ec, qual, ERXSortOrdering.sortOrderingWithKey(CPT_UID, ERXSortOrdering.CompareAscending).array());

		//		NSArray<EOCompte> tableUID = EOCompte.fetchAll(ec, QUAL_CPT_VLAN_P_R, ERXSortOrdering.sortOrderingWithKey(CPT_UID, ERXSortOrdering.CompareAscending).array());
		//		NSArray<EOCompte> tableUID = EOCompte.fetchAll(ec, QUAL_CPT_INTERNE, ERXSortOrdering.sortOrderingWithKey(CPT_UID, ERXSortOrdering.CompareAscending).array());
		NSMutableArray<Integer> uids = (NSMutableArray<Integer>) tableUID.valueForKey(CPT_UID_KEY);
		uids = ERXArrayUtilities.removeNullValues(uids).mutableClone();
		Integer maxUids = (Integer) uids.valueForKey("@" + NSArray.MaximumOperatorName);

		// Refonte de la procédure de recherche des trous qui pouvaient générer des soucis de "Out of Memory" à cause de la taille du tableau d'UID.
		if (maxUids != null) {
			if (maxUids >= valueUID) {
				int i;
				for (i = valueUID; i <= maxUids; i++) {
					if (!uids.contains(i)) {
						break;
					}
				}
				if (i == maxUids) {
					maxUID = maxUids + 1;
				} else {
					maxUID = i;
				}
			}
		}
		valueUID = maxUID;

		if ((EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY) != null)
				&& (EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY) != null)) {
			Integer rangeMaxUID = new Integer(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY));
			Integer rangeMinUID = new Integer(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY));

			// Si la valeur est hors de l'intervalle, ou si l'intervalle n'est pas valide : aucune valeur n'est attribuée, on laisse l'utilisateur remplir le champs.
			if (rangeMaxUID == null || rangeMaxUID < 1
					|| rangeMinUID == null || rangeMinUID < 1
					|| rangeMinUID > rangeMaxUID
					|| rangeMinUID > valueUID || valueUID > rangeMaxUID) {
				throw new ValidationException("La valeur par défaut attribuer à l'UID est hors de la gamme de valeur. L'utilisateur doit rentrer lui-même la valeur entre "
						+ EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY + " " + EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MIN_KEY)
						+ " et " + EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY + " " + EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_UID_MAX_KEY));
			}
		}
		return valueUID;
	}

	/**
	 * Retourne la valeur du GID lors de la création de compte en fonction du range de valeur si celui-ci est défini et de la valeur par défaut
	 * 
	 * @see EOGrhumParametres.GRHUM_GID_MAX
	 * @see EOGrhumParametres.GRHUM_GID_MIN
	 */

	protected Integer initialiseGID(EOEditingContext ec) {
		//		Integer valueGID = 1000;
		//		Integer maxGID = 1000;

		Integer valueGID = Integer.parseInt(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY));
		Integer maxGID = Integer.parseInt(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY));

		EOQualifier qual = compteService.determinationDomaineVlans(ec, this.toVlans().cVlan());

		NSArray<EOCompte> tableGID = EOCompte.fetchAll(ec, qual, ERXSortOrdering.sortOrderingWithKey(CPT_GID, ERXSortOrdering.CompareAscending).array());
		NSMutableArray<Integer> gids = (NSMutableArray<Integer>) tableGID.valueForKey(CPT_GID_KEY);
		gids = ERXArrayUtilities.removeNullValues(gids).mutableClone();
		Integer maxGids = (Integer) gids.valueForKey("@" + NSArray.MaximumOperatorName);

		// Refonte de la procédure de recherche des trous qui pouvaient générer des soucis de "Out of Memory" à cause de la taille du tableau d'UID.
		if (maxGids != null) {
			if (maxGids >= valueGID) {
				int i;
				for (i = valueGID; i <= maxGids; i++) {
					if (!gids.contains(i)) {
						break;
					}
				}
				if (i == maxGids) {
					maxGID = maxGids + 1;
				} else {
					maxGID = i;
				}
			}
		}
		valueGID = maxGID;

		if ((EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY) != null)
				&& (EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY) != null)
				&& (EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_DEFAULT_GID_KEY) != null)) {

			Integer rangeMaxGID = new Integer(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MAX_KEY));
			Integer rangeMinGID = new Integer(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_GID_MIN_KEY));

			// Si la valeur est hors de l'intervalle, ou si l'intervalle n'est pas valide : la valeur par défaut est attribuée
			if (rangeMaxGID == null || rangeMaxGID < 1
					|| rangeMinGID == null || rangeMinGID < 1
					|| rangeMinGID > rangeMaxGID
					|| rangeMinGID > valueGID || valueGID > rangeMaxGID) {
				valueGID = new Integer(EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_DEFAULT_GID_KEY));
			}
		}
		return valueGID;
	}

	/**
	 * Méthode pour ré-initialiser le mot de passe
	 * 
	 * @param ec
	 * @param appUser
	 * @param passwordClair
	 * @param stockePasswordClair
	 */
	public void reinitialisePassword(EOEditingContext ec, PersonneApplicationUser appUser, String passwordClair, boolean stockePasswordClair) {
		try {
//			logger.info("Début de la réinitialisation du MdP");
			this.setCptModificateur(appUser.iPersonne().persId());
			passwordClair = passwordService.getRandomPasswordWithPrivateRules(ec, toVlans().cVlan());
			// Ajout d'une boucle pour obtenir un nouveau mot de passe qui n'est pas dans l'historique
			if (compteService.isHistorisationActivePourVlanParticulier(ec, toVlans().cVlan())) {
//				logger.info("On regarde s'il y a une historisation");

				while (this.toPasswordHistories().contains(passwordClair)) {
					passwordClair = passwordService.getRandomPasswordWithPrivateRules(ec, toVlans().cVlan());
				}
//				logger.info("On a bouclé tant que l'on était sur un MdP déjà utilisé");
			}
			if (isSaisieUnixInfoObligatoire(toVlans())) {
				if (MyStringCtrl.isEmpty(cptHome())
						|| MyStringCtrl.isEmpty(cptShell())) {
					setCptHome(FwkCktlPersonne.paramManager
							.getParam(FwkCktlPersonneParamManager.PARAM_GRHUM_COMPTE_HOME));
					setCptShell(FwkCktlPersonne.paramManager
							.getParam(FwkCktlPersonneParamManager.PARAM_GRHUM_COMPTE_SHELL));
				}
			}
//			logger.info("le MdP est : " + passwordClair);
			
			if (isPropagerASambaActif()) {
				this.changePassword(appUser, passwordClair, stockePasswordClair, true);
			} else {
				this.changePassword(appUser, passwordClair, stockePasswordClair);
			}
//			logger.info("Fin de la ré-initiaisation du MdP");
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new NSValidation.ValidationException(e.getMessage());
		}
	}


	/**
	 * @return True si l'on doit vérifier les droits (va verifier le positionnement du paramètre
	 *         FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE qui au passage porte très mal son nom)
	 */
	protected boolean verifierDroitsModification() {
		return activationObjectHasChanged() && objectHasChanged(EOCompte.CPT_PRINCIPAL_KEY, EOCompte.D_MODIFICATION_KEY);
	}

	protected boolean activationObjectHasChanged() {
		return !"O".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.PERSONNE_OBJECTHASCHANGED_DESACTIVE));
	}

	@SuppressWarnings("unchecked")
	protected boolean objectHasChanged(String... ignoredKeys) {
		return sommeKeyHasChanged(changesFromCommittedSnapshot().allKeys(), ignoredKeys);
	}

	protected boolean sommeKeyHasChanged(NSArray<String> changedKeys, String... ignoredKeys) {
		boolean hasChanged = false;
		if (changedKeys != null && ignoredKeys != null) {
			NSArray<String> changedKeysExceptIgnored = ERXArrayUtilities.arrayMinusArray(changedKeys, new NSArray<String>(ignoredKeys));
			hasChanged = !changedKeysExceptIgnored.isEmpty();
		}
		return hasChanged;
	}

	public void setCompteService(CompteService compteService) {
		this.compteService = compteService;
	}

	/**
	 * Supprimer un email
	 * 
	 * @param unCompteEmail : email à supprimer
	 */
	public void supprimerCompteEmail(EOCompteEmail unCompteEmail) {
		this.setCptDomaine(null);
		this.removeFromToCompteEmailsRelationship(unCompteEmail);
		//On met le cpt_domaine à null, sinon erreur du trigger GRHUM.TRG_COMPTE quand on bascule sur un vlan X
//		this.setCptDomaine(null);
	}

}
