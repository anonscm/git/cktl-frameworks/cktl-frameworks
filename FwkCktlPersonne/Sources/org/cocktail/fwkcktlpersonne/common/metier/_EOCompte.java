/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompte.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCompte extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCompte.class);

	public static final String ENTITY_NAME = "Fwkpers_Compte";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPTE";


// Attribute Keys
  public static final ERXKey<String> CPT_CHARTE = new ERXKey<String>("cptCharte");
  public static final ERXKey<String> CPT_COMMENTAIRE = new ERXKey<String>("cptCommentaire");
  public static final ERXKey<Integer> CPT_CREATEUR = new ERXKey<Integer>("cptCreateur");
  public static final ERXKey<String> CPT_CRYPTE = new ERXKey<String>("cptCrypte");
  public static final ERXKey<NSTimestamp> CPT_DATE_CHARTE = new ERXKey<NSTimestamp>("cptDateCharte");
  public static final ERXKey<NSTimestamp> CPT_DEBUT_VALIDE = new ERXKey<NSTimestamp>("cptDebutValide");
  public static final ERXKey<String> CPT_DOMAINE = new ERXKey<String>("cptDomaine");
  public static final ERXKey<String> CPT_EMAIL = new ERXKey<String>("cptEmail");
  public static final ERXKey<NSTimestamp> CPT_FIN_VALIDE = new ERXKey<NSTimestamp>("cptFinValide");
  public static final ERXKey<Integer> CPT_GID = new ERXKey<Integer>("cptGid");
  public static final ERXKey<String> CPT_HOME = new ERXKey<String>("cptHome");
  public static final ERXKey<String> CPT_LISTE_ROUGE = new ERXKey<String>("cptListeRouge");
  public static final ERXKey<String> CPT_LOGIN = new ERXKey<String>("cptLogin");
  public static final ERXKey<Integer> CPT_MODIFICATEUR = new ERXKey<Integer>("cptModificateur");
  public static final ERXKey<String> CPT_PASSWD = new ERXKey<String>("cptPasswd");
  public static final ERXKey<String> CPT_PASSWD_CLAIR = new ERXKey<String>("cptPasswdClair");
  public static final ERXKey<String> CPT_PRINCIPAL = new ERXKey<String>("cptPrincipal");
  public static final ERXKey<String> CPT_QUESTION = new ERXKey<String>("cptQuestion");
  public static final ERXKey<String> CPT_REPONSE = new ERXKey<String>("cptReponse");
  public static final ERXKey<String> CPT_SHELL = new ERXKey<String>("cptShell");
  public static final ERXKey<String> CPT_TYPE = new ERXKey<String>("cptType");
  public static final ERXKey<Integer> CPT_UID = new ERXKey<Integer>("cptUid");
  public static final ERXKey<String> CPT_VALIDE = new ERXKey<String>("cptValide");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> TO_COMPTE_EMAILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail>("toCompteEmails");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> TO_COMPTE_SAMBAS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba>("toCompteSambas");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDUS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividus");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> TO_PASSWORD_HISTORIES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory>("toPasswordHistories");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage> TO_TYPE_CRYPTAGE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage>("toTypeCryptage");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn> TO_TYPE_VPN = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn>("toTypeVpn");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVlans> TO_VLANS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOVlans>("toVlans");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_COMMENTAIRE_KEY = "cptCommentaire";
	public static final String CPT_CREATEUR_KEY = "cptCreateur";
	public static final String CPT_CRYPTE_KEY = "cptCrypte";
	public static final String CPT_DATE_CHARTE_KEY = "cptDateCharte";
	public static final String CPT_DEBUT_VALIDE_KEY = "cptDebutValide";
	public static final String CPT_DOMAINE_KEY = "cptDomaine";
	public static final String CPT_EMAIL_KEY = "cptEmail";
	public static final String CPT_FIN_VALIDE_KEY = "cptFinValide";
	public static final String CPT_GID_KEY = "cptGid";
	public static final String CPT_HOME_KEY = "cptHome";
	public static final String CPT_LISTE_ROUGE_KEY = "cptListeRouge";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_MODIFICATEUR_KEY = "cptModificateur";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_PASSWD_CLAIR_KEY = "cptPasswdClair";
	public static final String CPT_PRINCIPAL_KEY = "cptPrincipal";
	public static final String CPT_QUESTION_KEY = "cptQuestion";
	public static final String CPT_REPONSE_KEY = "cptReponse";
	public static final String CPT_SHELL_KEY = "cptShell";
	public static final String CPT_TYPE_KEY = "cptType";
	public static final String CPT_UID_KEY = "cptUid";
	public static final String CPT_VALIDE_KEY = "cptValide";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String TCRY_ORDRE_KEY = "tcryOrdre";
	public static final String TVPN_CODE_KEY = "tvpnCode";
	public static final String CPT_VLAN_KEY = "cptVlan";
	public static final String CPT_ORDRE_KEY = "cptOrdre";

//Colonnes dans la base de donnees
	public static final String CPT_CHARTE_COLKEY = "CPT_CHARTE";
	public static final String CPT_COMMENTAIRE_COLKEY = "CPT_COMMENTAIRE";
	public static final String CPT_CREATEUR_COLKEY = "CPT_CREATEUR";
	public static final String CPT_CRYPTE_COLKEY = "CPT_CRYPTE";
	public static final String CPT_DATE_CHARTE_COLKEY = "CPT_DATE_CHARTE";
	public static final String CPT_DEBUT_VALIDE_COLKEY = "CPT_DEBUT_VALIDE";
	public static final String CPT_DOMAINE_COLKEY = "CPT_DOMAINE";
	public static final String CPT_EMAIL_COLKEY = "CPT_EMAIL";
	public static final String CPT_FIN_VALIDE_COLKEY = "CPT_FIN_VALIDE";
	public static final String CPT_GID_COLKEY = "CPT_GID";
	public static final String CPT_HOME_COLKEY = "CPT_HOME";
	public static final String CPT_LISTE_ROUGE_COLKEY = "CPT_LISTE_ROUGE";
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String CPT_MODIFICATEUR_COLKEY = "CPT_MODIFICATEUR";
	public static final String CPT_PASSWD_COLKEY = "CPT_PASSWD";
	public static final String CPT_PASSWD_CLAIR_COLKEY = "CPT_PASSWD_CLAIR";
	public static final String CPT_PRINCIPAL_COLKEY = "CPT_PRINCIPAL";
	public static final String CPT_QUESTION_COLKEY = "CPT_QUESTION";
	public static final String CPT_REPONSE_COLKEY = "CPT_REPONSE";
	public static final String CPT_SHELL_COLKEY = "CPT_SHELL";
	public static final String CPT_TYPE_COLKEY = "CPT_TYPE";
	public static final String CPT_UID_COLKEY = "CPT_UID";
	public static final String CPT_VALIDE_COLKEY = "CPT_VALIDE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String TCRY_ORDRE_COLKEY = "tcry_Ordre";
	public static final String TVPN_CODE_COLKEY = "TVPN_CODE";
	public static final String CPT_VLAN_COLKEY = "CPT_VLAN";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";


	// Relationships
	public static final String TO_COMPTE_EMAILS_KEY = "toCompteEmails";
	public static final String TO_COMPTE_SAMBAS_KEY = "toCompteSambas";
	public static final String TO_INDIVIDUS_KEY = "toIndividus";
	public static final String TO_PASSWORD_HISTORIES_KEY = "toPasswordHistories";
	public static final String TO_STRUCTURES_KEY = "toStructures";
	public static final String TO_TYPE_CRYPTAGE_KEY = "toTypeCryptage";
	public static final String TO_TYPE_VPN_KEY = "toTypeVpn";
	public static final String TO_VLANS_KEY = "toVlans";



	// Accessors methods
  public String cptCharte() {
    return (String) storedValueForKey(CPT_CHARTE_KEY);
  }

  public void setCptCharte(String value) {
    takeStoredValueForKey(value, CPT_CHARTE_KEY);
  }

  public String cptCommentaire() {
    return (String) storedValueForKey(CPT_COMMENTAIRE_KEY);
  }

  public void setCptCommentaire(String value) {
    takeStoredValueForKey(value, CPT_COMMENTAIRE_KEY);
  }

  public Integer cptCreateur() {
    return (Integer) storedValueForKey(CPT_CREATEUR_KEY);
  }

  public void setCptCreateur(Integer value) {
    takeStoredValueForKey(value, CPT_CREATEUR_KEY);
  }

  public String cptCrypte() {
    return (String) storedValueForKey(CPT_CRYPTE_KEY);
  }

  public void setCptCrypte(String value) {
    takeStoredValueForKey(value, CPT_CRYPTE_KEY);
  }

  public NSTimestamp cptDateCharte() {
    return (NSTimestamp) storedValueForKey(CPT_DATE_CHARTE_KEY);
  }

  public void setCptDateCharte(NSTimestamp value) {
    takeStoredValueForKey(value, CPT_DATE_CHARTE_KEY);
  }

  public NSTimestamp cptDebutValide() {
    return (NSTimestamp) storedValueForKey(CPT_DEBUT_VALIDE_KEY);
  }

  public void setCptDebutValide(NSTimestamp value) {
    takeStoredValueForKey(value, CPT_DEBUT_VALIDE_KEY);
  }

  public String cptDomaine() {
    return (String) storedValueForKey(CPT_DOMAINE_KEY);
  }

  public void setCptDomaine(String value) {
    takeStoredValueForKey(value, CPT_DOMAINE_KEY);
  }

  public String cptEmail() {
    return (String) storedValueForKey(CPT_EMAIL_KEY);
  }

  public void setCptEmail(String value) {
    takeStoredValueForKey(value, CPT_EMAIL_KEY);
  }

  public NSTimestamp cptFinValide() {
    return (NSTimestamp) storedValueForKey(CPT_FIN_VALIDE_KEY);
  }

  public void setCptFinValide(NSTimestamp value) {
    takeStoredValueForKey(value, CPT_FIN_VALIDE_KEY);
  }

  public Integer cptGid() {
    return (Integer) storedValueForKey(CPT_GID_KEY);
  }

  public void setCptGid(Integer value) {
    takeStoredValueForKey(value, CPT_GID_KEY);
  }

  public String cptHome() {
    return (String) storedValueForKey(CPT_HOME_KEY);
  }

  public void setCptHome(String value) {
    takeStoredValueForKey(value, CPT_HOME_KEY);
  }

  public String cptListeRouge() {
    return (String) storedValueForKey(CPT_LISTE_ROUGE_KEY);
  }

  public void setCptListeRouge(String value) {
    takeStoredValueForKey(value, CPT_LISTE_ROUGE_KEY);
  }

  public String cptLogin() {
    return (String) storedValueForKey(CPT_LOGIN_KEY);
  }

  public void setCptLogin(String value) {
    takeStoredValueForKey(value, CPT_LOGIN_KEY);
  }

  public Integer cptModificateur() {
    return (Integer) storedValueForKey(CPT_MODIFICATEUR_KEY);
  }

  public void setCptModificateur(Integer value) {
    takeStoredValueForKey(value, CPT_MODIFICATEUR_KEY);
  }

  public String cptPasswd() {
    return (String) storedValueForKey(CPT_PASSWD_KEY);
  }

  public void setCptPasswd(String value) {
    takeStoredValueForKey(value, CPT_PASSWD_KEY);
  }

  public String cptPasswdClair() {
    return (String) storedValueForKey(CPT_PASSWD_CLAIR_KEY);
  }

  public void setCptPasswdClair(String value) {
    takeStoredValueForKey(value, CPT_PASSWD_CLAIR_KEY);
  }

  public String cptPrincipal() {
    return (String) storedValueForKey(CPT_PRINCIPAL_KEY);
  }

  public void setCptPrincipal(String value) {
    takeStoredValueForKey(value, CPT_PRINCIPAL_KEY);
  }

  public String cptQuestion() {
    return (String) storedValueForKey(CPT_QUESTION_KEY);
  }

  public void setCptQuestion(String value) {
    takeStoredValueForKey(value, CPT_QUESTION_KEY);
  }

  public String cptReponse() {
    return (String) storedValueForKey(CPT_REPONSE_KEY);
  }

  public void setCptReponse(String value) {
    takeStoredValueForKey(value, CPT_REPONSE_KEY);
  }

  public String cptShell() {
    return (String) storedValueForKey(CPT_SHELL_KEY);
  }

  public void setCptShell(String value) {
    takeStoredValueForKey(value, CPT_SHELL_KEY);
  }

  public String cptType() {
    return (String) storedValueForKey(CPT_TYPE_KEY);
  }

  public void setCptType(String value) {
    takeStoredValueForKey(value, CPT_TYPE_KEY);
  }

  public Integer cptUid() {
    return (Integer) storedValueForKey(CPT_UID_KEY);
  }

  public void setCptUid(Integer value) {
    takeStoredValueForKey(value, CPT_UID_KEY);
  }

  public String cptValide() {
    return (String) storedValueForKey(CPT_VALIDE_KEY);
  }

  public void setCptValide(String value) {
    takeStoredValueForKey(value, CPT_VALIDE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage toTypeCryptage() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage)storedValueForKey(TO_TYPE_CRYPTAGE_KEY);
  }

  public void setToTypeCryptageRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage oldValue = toTypeCryptage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CRYPTAGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CRYPTAGE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn toTypeVpn() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn)storedValueForKey(TO_TYPE_VPN_KEY);
  }

  public void setToTypeVpnRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn oldValue = toTypeVpn();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VPN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VPN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOVlans toVlans() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOVlans)storedValueForKey(TO_VLANS_KEY);
  }

  public void setToVlansRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOVlans value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOVlans oldValue = toVlans();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_VLANS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_VLANS_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> toCompteEmails() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail>)storedValueForKey(TO_COMPTE_EMAILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> toCompteEmails(EOQualifier qualifier) {
    return toCompteEmails(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> toCompteEmails(EOQualifier qualifier, boolean fetch) {
    return toCompteEmails(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> toCompteEmails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail.TO_COMPTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toCompteEmails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToCompteEmailsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
  }

  public void removeFromToCompteEmailsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail createToCompteEmailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_CompteEmail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTE_EMAILS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail) eo;
  }

  public void deleteToCompteEmailsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_EMAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToCompteEmailsRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail> objects = toCompteEmails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToCompteEmailsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> toCompteSambas() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba>)storedValueForKey(TO_COMPTE_SAMBAS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> toCompteSambas(EOQualifier qualifier) {
    return toCompteSambas(qualifier, null);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> toCompteSambas(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> results;
      results = toCompteSambas();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToCompteSambasRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_COMPTE_SAMBAS_KEY);
  }

  public void removeFromToCompteSambasRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_SAMBAS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba createToCompteSambasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_CompteSamba");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_COMPTE_SAMBAS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba) eo;
  }

  public void deleteToCompteSambasRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_COMPTE_SAMBAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToCompteSambasRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOCompteSamba> objects = toCompteSambas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToCompteSambasRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)storedValueForKey(TO_INDIVIDUS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier) {
    return toIndividus(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier, boolean fetch) {
    return toIndividus(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> toIndividus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.TO_COMPTES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOIndividu.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toIndividus();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
  }

  public void removeFromToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createToIndividusRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_INDIVIDUS_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteToIndividusRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INDIVIDUS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToIndividusRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> objects = toIndividus().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToIndividusRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> toPasswordHistories() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory>)storedValueForKey(TO_PASSWORD_HISTORIES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> toPasswordHistories(EOQualifier qualifier) {
    return toPasswordHistories(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> toPasswordHistories(EOQualifier qualifier, boolean fetch) {
    return toPasswordHistories(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> toPasswordHistories(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory.TO_COMPTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPasswordHistories();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPasswordHistoriesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PASSWORD_HISTORIES_KEY);
  }

  public void removeFromToPasswordHistoriesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PASSWORD_HISTORIES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory createToPasswordHistoriesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_PasswordHistory");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PASSWORD_HISTORIES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory) eo;
  }

  public void deleteToPasswordHistoriesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PASSWORD_HISTORIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPasswordHistoriesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory> objects = toPasswordHistories().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPasswordHistoriesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey(TO_STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier) {
    return toStructures(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier, boolean fetch) {
    return toStructures(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> toStructures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlpersonne.common.metier.EOStructure.TO_COMPTES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlpersonne.common.metier.EOStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toStructures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
  }

  public void removeFromToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createToStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> objects = toStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToStructuresRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCompte avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCompte createEOCompte(EOEditingContext editingContext, String cptCharte
, Integer cptCreateur
, String cptCrypte
, String cptListeRouge
, String cptLogin
, Integer cptModificateur
, String cptPasswd
, String cptPrincipal
, String cptValide
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
, org.cocktail.fwkcktlpersonne.common.metier.EOTypeCryptage toTypeCryptage, org.cocktail.fwkcktlpersonne.common.metier.EOTypeVpn toTypeVpn, org.cocktail.fwkcktlpersonne.common.metier.EOVlans toVlans			) {
    EOCompte eo = (EOCompte) createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);    
		eo.setCptCharte(cptCharte);
		eo.setCptCreateur(cptCreateur);
		eo.setCptCrypte(cptCrypte);
		eo.setCptListeRouge(cptListeRouge);
		eo.setCptLogin(cptLogin);
		eo.setCptModificateur(cptModificateur);
		eo.setCptPasswd(cptPasswd);
		eo.setCptPrincipal(cptPrincipal);
		eo.setCptValide(cptValide);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
    eo.setToTypeCryptageRelationship(toTypeCryptage);
    eo.setToTypeVpnRelationship(toTypeVpn);
    eo.setToVlansRelationship(toVlans);
    return eo;
  }

  
	  public EOCompte localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompte)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompte creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompte creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCompte object = (EOCompte)createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCompte localInstanceIn(EOEditingContext editingContext, EOCompte eo) {
    EOCompte localInstance = (eo == null) ? null : (EOCompte)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCompte#localInstanceIn a la place.
   */
	public static EOCompte localInstanceOf(EOEditingContext editingContext, EOCompte eo) {
		return EOCompte.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCompte>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompte fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompte fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCompte> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCompte> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompte fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompte eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompte ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompte fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
