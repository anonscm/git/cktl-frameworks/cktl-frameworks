/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCommune.java instead.
package org.cocktail.fwkcktlpersonne.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.AfwkPersRecord;


public abstract class _EOCommune extends  AfwkPersRecord {
//	private static Logger logger = Logger.getLogger(_EOCommune.class);

	public static final String ENTITY_NAME = "Fwkpers_Commune";
	public static final String ENTITY_TABLE_NAME = "GRHUM.V_COMMUNE";


// Attribute Keys
  public static final ERXKey<String> C_DEP = new ERXKey<String>("cDep");
  public static final ERXKey<String> C_INSEE = new ERXKey<String>("cInsee");
  public static final ERXKey<String> C_POSTAL = new ERXKey<String>("cPostal");
  public static final ERXKey<String> LC_COM = new ERXKey<String>("lcCom");
  public static final ERXKey<String> LL_COM = new ERXKey<String>("llCom");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_DEPARTEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toDepartement");

	// Attributes


	public static final String C_DEP_KEY = "cDep";
	public static final String C_INSEE_KEY = "cInsee";
	public static final String C_POSTAL_KEY = "cPostal";
	public static final String LC_COM_KEY = "lcCom";
	public static final String LL_COM_KEY = "llCom";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_DEP_COLKEY = "C_DEP";
	public static final String C_INSEE_COLKEY = "C_INSEE";
	public static final String C_POSTAL_COLKEY = "C_POSTAL";
	public static final String LC_COM_COLKEY = "LC_COM";
	public static final String LL_COM_COLKEY = "LL_COM";



	// Relationships
	public static final String TO_DEPARTEMENT_KEY = "toDepartement";



	// Accessors methods
  public String cDep() {
    return (String) storedValueForKey(C_DEP_KEY);
  }

  public void setCDep(String value) {
    takeStoredValueForKey(value, C_DEP_KEY);
  }

  public String cInsee() {
    return (String) storedValueForKey(C_INSEE_KEY);
  }

  public void setCInsee(String value) {
    takeStoredValueForKey(value, C_INSEE_KEY);
  }

  public String cPostal() {
    return (String) storedValueForKey(C_POSTAL_KEY);
  }

  public void setCPostal(String value) {
    takeStoredValueForKey(value, C_POSTAL_KEY);
  }

  public String lcCom() {
    return (String) storedValueForKey(LC_COM_KEY);
  }

  public void setLcCom(String value) {
    takeStoredValueForKey(value, LC_COM_KEY);
  }

  public String llCom() {
    return (String) storedValueForKey(LL_COM_KEY);
  }

  public void setLlCom(String value) {
    takeStoredValueForKey(value, LL_COM_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartement() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey(TO_DEPARTEMENT_KEY);
  }

  public void setToDepartementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toDepartement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOCommune avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCommune createEOCommune(EOEditingContext editingContext, String cDep
, String cInsee
, String cPostal
, String lcCom
, String llCom
, org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartement			) {
    EOCommune eo = (EOCommune) createAndInsertInstance(editingContext, _EOCommune.ENTITY_NAME);    
		eo.setCDep(cDep);
		eo.setCInsee(cInsee);
		eo.setCPostal(cPostal);
		eo.setLcCom(lcCom);
		eo.setLlCom(llCom);
    eo.setToDepartementRelationship(toDepartement);
    return eo;
  }

  
	  public EOCommune localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCommune)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommune creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCommune creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCommune object = (EOCommune)createAndInsertInstance(editingContext, _EOCommune.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCommune localInstanceIn(EOEditingContext editingContext, EOCommune eo) {
    EOCommune localInstance = (eo == null) ? null : (EOCommune)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCommune#localInstanceIn a la place.
   */
	public static EOCommune localInstanceOf(EOEditingContext editingContext, EOCommune eo) {
		return EOCommune.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune> eoObjects = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOCommune>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCommune fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCommune fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCommune> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCommune eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCommune)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCommune fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCommune fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCommune> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCommune eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCommune)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCommune fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCommune eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCommune ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCommune fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
