/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.fwkcktlpersonne.common.metier;

import java.util.Iterator;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

/**
 * Sert à deux choses :
 * <ul>
 * <li>dans le cas d'un groupe (structure), la table sert à stocker les alias qui pointent vers un alias d'un autre groupe (structure). Par exemple le
 * groupe TOTO a comme grpAlias <b>toto</b>, on peut ajouter au groupe TITI l'alias <b>toto</b> dans PersonneAlias. Ceci permet ensuite lors de
 * l'envoi d'un mail à l'adresse toto@univ.fr que le mail soit recu par les membres du groupe TOTO mais aussi par ceux du groupe TITI.</li>
 * <li>Dans le cas d'un individu, la table sert à stocker des identités supplémentaires. Par exemple si l'individu Jean Martin possède une adresse
 * email jean.martin@univ.fr, on peut lui rajouter un alias jean007 ce qui lui permettra de recevoir les mails envoyés à jean007@univ.fr.</li>
 * </ul>
 * Pour les règles de validation, Cf. {@link EOPersonneAlias#validateObjectMetier()}.
 * 
 * @author rprin
 */
public class EOPersonneAlias extends _EOPersonneAlias {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3839798031915977293L;

	public EOPersonneAlias() {
		super();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDCreation(AUtils.now());
	}

	/**
	 * <ul>
	 * <li>Les personneAlias dans le cas des groupes (structures) doivent faire reference à un grpAlias (de structure) existant.</li>
	 * <li>Les personneAlias dans le cas des individus doivent être uniques (non utilisés dans grpAlias, ni dans login, ni dans compte_email sur
	 * domaine interne).</li>
	 * </ul>
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		trimAllString();
		setDModification(new NSTimestamp());

		if (MyStringCtrl.isEmpty(alias())) {
			throw new NSValidation.ValidationException("Un alias est obligatoire.");
		}

		checkContraintesObligatoires();
		checkContraintesLongueursMax();

		checkForStructure();
		checkForIndividu();
		super.validateObjectMetier();
	}

	/**
	 * Verifie que l'alias est bien l'alias principal d'un groupe. l'alias ne doit pas etre un cpt_login existant ni un personnealias associé à un
	 * individu, ni un compteemail interne.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkForStructure() throws NSValidation.ValidationException {
		if (toStructure() != null) {
			// On fait le fetch en base plus en memoire
			// NOTE: il faut voir si cela fonctionne correctement. Dans le cas ou ca ne marche, il faudra aborder la solution de mettre
			// un flag sur cet EO et de le lever dans le cas ou l'on voudrait desactiver la verification sur la structure
			EOQualifier qualifierForStructureWithAlias = EOStructure.GRP_ALIAS.eq(alias());
			ERXFetchSpecification<EOStructure> fetchSpecificationForStructureWithAlias = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifierForStructureWithAlias, null);
			fetchSpecificationForStructureWithAlias.setIncludeEditingContextChanges(true);
			NSArray<EOStructure> resultsForStructureWithAlias = fetchSpecificationForStructureWithAlias.fetchObjects(editingContext());
			// EOStructure res = EOStructure.fetchByKeyValue(editingContext(), EOStructure.GRP_ALIAS_KEY, alias());
			if (resultsForStructureWithAlias == null) {
				throw new NSValidation.ValidationException("L'alias " + alias() + " n'est pas l'alias principal d'un groupe.");
			}

			// Vérification de l'unicité de l'alias par rapport aux login
			NSArray<EOVlans> vlans = EOVlans.fetchAllVLansAPrendreEnCompte(editingContext());
			EOQualifier qualifier = EOCompte.CPT_LOGIN.eq(alias()).and(EOCompte.TO_VLANS.in(vlans));
			EOCompte compte = EOCompte.fetchFirstByQualifier(editingContext(), qualifier);

			if (compte != null) {
				throw new NSValidation.ValidationException("Un compte avec le login " + compte.cptLogin() + " existe déjà dans le système d'information");
			}

			// Vérification de non collision avec les emails
			EOCompteEmail compteEmail = EOCompteEmail.fetchCompteEmailInterne(editingContext(), alias());
			if (compteEmail != null) {
				throw new NSValidation.ValidationException("Une adresse email " + compteEmail.cemEmail() + "@" + compteEmail.cemDomaine() + " existe déjà pour la personne " + compteEmail.toCompte().toPersonne().libelleEtId());
			}

			//Verification que l'alias n'est pas l'alias d'un individu
			NSArray<EOPersonneAlias> aliasDouble = EOPersonneAlias.fetchForAlias(editingContext(), alias(), null);
			Iterator<EOPersonneAlias> iter = aliasDouble.iterator();
			while (iter.hasNext()) {
				EOPersonneAlias eoPersonneAlias = (EOPersonneAlias) iter.next();
				if (eoPersonneAlias.toIndividu() != null) {

					throw new NSValidation.ValidationException(
							"L'alias " + alias() + " est déjà affecté à un individu (" + eoPersonneAlias.toIndividu().libelleEtId() + ")");
				}
			}

		}
	}

	/**
	 * Verifie que l'alias est unique.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkForIndividu() throws NSValidation.ValidationException {
		String alias = alias();
		if (toIndividu() != null) {
			EOStructure structure = EOStructure.fetchFirstByQualifier(editingContext(), new EOKeyValueQualifier(EOStructure.GRP_ALIAS_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, alias));
			if (structure != null) {
				throw new NSValidation.ValidationException("L'alias " + alias + " est l'alias principal du groupe " + structure.libelleEtId());
			}

			//Rechercher sur les autres personnes
			EOPersonneAlias personneAlias = EOPersonneAlias.fetchFirstByQualifier(editingContext(), new EOAndQualifier(
					new NSArray(new Object[] {
							new EOKeyValueQualifier(EOPersonneAlias.PERS_ID_KEY, EOQualifier.QualifierOperatorNotEqual, toPersonne().persId()),
							new EOKeyValueQualifier(EOPersonneAlias.ALIAS_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, alias)
					}
					)));

			if (personneAlias != null) {
				throw new NSValidation.ValidationException("L'alias " + alias + " est déjà affecté à " + personneAlias.toPersonne().libelleEtId());
			}

			//Rechercher sur la personne elle meme
			EOPersonneAlias personneAlias2 = EOPersonneAlias.fetchFirstByQualifier(editingContext(), new EOAndQualifier(
					new NSArray(new Object[] {
							new EOKeyValueQualifier(EOPersonneAlias.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, toPersonne().persId()),
							new EOKeyValueQualifier(EOPersonneAlias.ALIAS_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, alias)
					}
					)));

			if (personneAlias2 != null && !personneAlias2.globalID().equals(this.globalID())) {
				throw new NSValidation.ValidationException("L'alias " + alias + " est déjà affecté à " + personneAlias2.toPersonne().libelleEtId());
			}

			//rechercher sur les login
			NSArray<EOVlans> vlans = EOVlans.fetchAllVLansAPrendreEnCompte(editingContext());
			EOQualifier qualifier = EOCompte.CPT_LOGIN.eq(alias).and(EOCompte.TO_VLANS.in(vlans));
			EOCompte compte = EOCompte.fetchFirstByQualifier(editingContext(), qualifier);

			if (compte != null) {
				throw new NSValidation.ValidationException("Un compte avec le login " + compte.cptLogin() + " existe déjà dans le système d'information");
			}

			// Vérification de non collision avec les emails
			EOCompteEmail compteEmail = EOCompteEmail.fetchCompteEmailInterne(editingContext(), alias);
			if (compteEmail != null) {
				throw new NSValidation.ValidationException("Une adresse email " + compteEmail.cemEmail() + "@" + compteEmail.cemDomaine() + " existe déjà pour la personne " + compteEmail.toCompte().toPersonne().libelleEtId());
			}

		}

	}

	/**
	 * @param ec
	 * @param alias
	 * @param qualifier
	 * @return les personnealias trouvé pour alias.
	 */
	public static NSArray<EOPersonneAlias> fetchForAlias(EOEditingContext ec, String alias, EOQualifier qualifier) {
		EOQualifier qual = new EOKeyValueQualifier(
				EOPersonneAlias.ALIAS_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike,
				alias);
		if (qualifier != null) {
			qual = ERXQ.and(qual, qualifier);
		}
		NSArray<EOPersonneAlias> aliasDouble = EOPersonneAlias.fetchAll(ec, qual);
		return aliasDouble;
	}

	/**
	 * @param ec
	 * @param alias
	 * @return le premier personnealias trouvé pour alias.
	 */
	public static EOPersonneAlias fetchForAlias(EOEditingContext ec, String alias) {
		EOQualifier qual = new EOKeyValueQualifier(
				EOPersonneAlias.ALIAS_KEY,
				EOQualifier.QualifierOperatorCaseInsensitiveLike,
				alias);

		EOPersonneAlias aliasDouble = EOPersonneAlias.fetchFirstByQualifier(ec, qual);
		return aliasDouble;
	}

	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}

	/**
	 * @return toStructure() ou bien toIndiviodu() selon le cas.
	 */
	public IPersonne toPersonne() {
		return (toStructure() != null ? (IPersonne) toStructure() : (IPersonne) toIndividu());
	}

	public EOIndividu toIndividu() {
		return (EOIndividu) ((toIndividus() != null && toIndividus().count() > 0) ? toIndividus().objectAtIndex(0) : null);
	}

	public EOStructure toStructure() {
		return (EOStructure) ((toStructures() != null && toStructures().count() > 0) ? toStructures().objectAtIndex(0) : null);
	}
}
