/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeContratTravail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTypeContratTravail extends _EOTypeContratTravail implements ITypeContratTravail {

    public EOTypeContratTravail() {
        super();
    }

    /**
     * Méthode test pour savoir si un agent est vacataire
     * @return true si vacataire
     */
    public boolean estVacataire() {
        return !estRemunerationPrincipale();
    }
    
    /**
     * 
     * @return true si l'agent à un témoin Rémunération Principale non nul ET qui est à O(ui)
     */
    public boolean estRemunerationPrincipale() {
        return temRemunerationPrincipale() != null && temRemunerationPrincipale().equals("O");
    }

	public boolean estServicePublic() {
		return temServicePublic() != null && temServicePublic().equals("O");
	}

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    @Override
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    @Override
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appel̩e.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     * @throws une exception de validation
     */
    @Override
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * @throws NSValidation.ValidationException levée d'une exception si problème à la validation
     */
    @Override
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();   
    }
    
    
    /**
     * Méthode retourne tous les types de contrats vacataires
     * si le témoin Rémunération Principale n'est pas à O(ui)
     * @return la liste des types de contrats vacataires
     * @deprecated Utilisez typesContratsTravailVacatairesEnseignement 
     */
    
	public static NSArray<EOTypeContratTravail> tousLesTypesContratsTravailVacatairesEnseignant(EOEditingContext ec) {
		
		EOQualifier qualifier =	ERXQ.and(EOTypeContratTravail.C_TYPE_CONTRAT_TRAV.isNotNull(),
						ERXQ.notEquals(EOTypeContratTravail.TEM_REMUNERATION_PRINCIPALE_KEY, "O"),
						ERXQ.equals(EOTypeContratTravail.TEM_ENSEIGNANT_KEY, "O"),						
						EOTypeContratTravail.D_FIN_VAL.isNull());
		
		return fetchAll(ec, qualifier);
	}
	
	 /**
     * Méthode retourne tous les types de contrats vacataires d'enseignement 
     * @return la liste des types de contrats vacataires  
     */
	public static NSArray<EOTypeContratTravail> typesContratsTravailVacatairesEnseignement(EOEditingContext ec) {
		EOQualifier qualifier =	ERXQ.and(
				ERXQ.equals(EOTypeContratTravail.TEM_ENSEIGNEMENT_KEY, "O"),
				ERXQ.equals(EOTypeContratTravail.TEM_VACATAIRE_KEY, "O"),
				EOTypeContratTravail.D_FIN_VAL.isNull());		
		return fetchAll(ec, qualifier);
	}

	 /**
     * Méthode retourne tous les types de contrats vacataires 
     * @return la liste des types de contrats vacataires  
     */
	public static NSArray<EOTypeContratTravail> typesContratsTravailNonVacatairesEnseignement(EOEditingContext ec) {
		EOQualifier qualifier =	ERXQ.and(
				ERXQ.equals(EOTypeContratTravail.TEM_ENSEIGNEMENT_KEY, "O"),
				ERXQ.equals(EOTypeContratTravail.TEM_VACATAIRE_KEY, "N"),
				EOTypeContratTravail.D_FIN_VAL.isNull());		
		return fetchAll(ec, qualifier);
	}


	public boolean estCdi() {
		return temCdi() != null && temCdi().equals("O");
	}

}
