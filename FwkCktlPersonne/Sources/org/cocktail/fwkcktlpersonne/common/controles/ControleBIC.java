/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Utilitaires autour du code BIC (Swift), norme ISO 9362. Extrait Wikipedia :
 * <table>
 * <tbody>
 * <tr>
 * <th colspan="13" bgcolor="#BDBDFF">Composition du BIC</th>
 * </tr>
 * <tr align="center" bgcolor="#DDDDFF">
 * <td colspan="4">Code Banque</td>
 * <td colspan="2">Code Pays</td>
 * <td colspan="2">Code Emplacement</td>
 * <td colspan="3">Code Branche</td>
 * </tr>
 * <tr align="center" bgcolor="white">
 * <td colspan="4">LLLL</td>
 * <td colspan="2">LL</td>
 * <td colspan="2">XX</td>
 * <td colspan="3">XXX</td>
 * </tr>
 * <tr align="center" bgcolor="#BDBDFF">
 * <td colspan="13"><small>L&nbsp;: Lettre - X&nbsp;: Chiffre ou lettre</small></td>
 * </tr>
 * </tbody>
 * </table>
 * <div style="clear:both;"></div>
 * <p>
 * Le BIC est constitué de 8 ou 11 caractères&nbsp;:
 * </p>
 * <ul>
 * <li><b>Code Banque</b>&nbsp;: 4 caractères définissant la banque d'une manière unique</li>
 * <li><b>Code Pays</b>&nbsp;: 2 caractères constituant le code ISO du pays (<a title="ISO 3166" href="/wiki/ISO_3166">ISO 3166</a>)</li>
 * <li><b>Code Emplacement</b>&nbsp;: 2 caractères de localisation (alphabétique ou numérique) pour distinguer les banques d'un même pays (ville,
 * état, provinces)</li>
 * <li><b>Code Branche</b>&nbsp;: 3 caractères optionnels définissant l'agence comme une branche de la banque ('XXX' pour le siège central, 'LYO' pour
 * une agence à Lyon, etc.)</li>
 * </ul>
 * <p>
 * Lorsque le code ne contient que 8 caractères, il s'agit du siège central national.
 * </p>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class ControleBIC {
	private static final String AUTHORIZED_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	private static final String CODE_PAYS_RIB_FR_OBLIGATOIRE_KEY = "org.cocktail.grhum.pays.ribformatfr.obligatoire";
	private static final String CODE_PAYS_ZONE_SEPA_KEY = "org.cocktail.grhum.pays.zonesepa";

	private static NSArray<String> codePaysRibFRObligatoire = null;
	private static NSArray<String> codePaysZoneSepa = null;

	private String bic;
	private String codeBanque;
	private String codePays;
	private String codeEmplacement;
	private String codeBranche;

	public ControleBIC(String bic) {
		setBic(bic);
	}

	private void splitBic(String s) {
		String res = s;

		if (ERXStringUtilities.emptyStringForNull(res).length() >= 4) {
			setCodeBanque(res.substring(0, 4));
			res = res.substring(4);
			if (res.length() >= 2) {
				setCodePays(res.substring(0, 2));
				res = res.substring(2);
				if (res.length() >= 2) {
					setCodeEmplacement(res.substring(0, 2));
					res = res.substring(2);
					setCodeBranche(res);
				}
			}
		}
	}

	/**
	 * Vérifie si le BIC est valide. Une exception est levée si ce n'est pas le cas. Aucune exception si le BIC est vide.
	 */
	public void checkValide(EOEditingContext edc) throws NSValidation.ValidationException {
		if (ERXStringUtilities.emptyStringForNull(getBic()).length() > 0) {
			try {

				checkCodeBanqueValide(getCodeBanque());
				checkCodePaysValide(edc);
				checkCodeEmplacementValide();
				checkCodeBrancheValide();

			} catch (Exception e) {
				NSValidation.ValidationException e2 = new NSValidation.ValidationException("Validation du code BIC " + getBic() + " : " + e.getMessage());
				throw e2;
			}
		}
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
		splitBic(bic);
	}

	public String getCodeBanque() {
		return codeBanque;
	}

	public void setCodeBanque(String codeBanque) {
		this.codeBanque = codeBanque;
	}

	public String getCodePays() {
		return codePays;
	}

	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}

	public String getCodeEmplacement() {
		return codeEmplacement;
	}

	public void setCodeEmplacement(String codeEmplacement) {
		this.codeEmplacement = codeEmplacement;
	}

	public String getCodeBranche() {
		return codeBranche;
	}

	public void setCodeBranche(String codeBranche) {
		this.codeBranche = codeBranche;
	}

	public static void checkCodeBanqueValide(final String codeBanque) {
		String msgPrefix = codeBanque + " : Le code banque (caractères 1 à 4)";
		if (ERXStringUtilities.emptyStringForNull(codeBanque).length() == 0) {
			throw new NSValidation.ValidationException(msgPrefix + " est non spécifié.");
		}

		if (codeBanque.length() != 4 || !isValidLettersOnly(codeBanque)) {
			throw new NSValidation.ValidationException(msgPrefix + " doit comprendre exactement 4 lettres majuscules sans accents.");
		}
	}

	public void checkCodePaysValide(EOEditingContext edc) {
		String msgPrefix = getCodePays() + " : Le code pays (caractères 5 à 6)";
		if (ERXStringUtilities.emptyStringForNull(getCodePays()).length() == 0) {
			throw new NSValidation.ValidationException(msgPrefix + " est non spécifié.");
		}

		if (getCodePays().length() != 2 || !isValidLettersOnly(getCodePays())) {
			throw new NSValidation.ValidationException(msgPrefix + " doit comprendre exactement 2 lettres majuscules sans accents.");
		}

		if (EOPays.fetchPaysbyCodeIso(edc, getCodePays()) == null) {
			throw new NSValidation.ValidationException(msgPrefix + " doit etre un code pays existant.");
		}
	}

	public void checkCodeEmplacementValide() {
		String msgPrefix = getCodeEmplacement() + " : Le code emplacement (caractères 7 à 8)";
		if (ERXStringUtilities.emptyStringForNull(getCodeEmplacement()).length() == 0) {
			throw new NSValidation.ValidationException(msgPrefix + " est non spécifié.");
		}

		if (getCodeEmplacement().length() != 2 || !(isValidLettersOrDigitsOnly(getCodeEmplacement()))) {
			throw new NSValidation.ValidationException(msgPrefix + " doit comprendre exactement 2 chiffres ou lettres majuscules sans accents.");
		}

	}

	public void checkCodeBrancheValide() {
		String msgPrefix = getCodeBranche() + " : Le code emplacement (caractères 9 à 11)";
		if (ERXStringUtilities.emptyStringForNull(getCodeBranche()).length() > 0) {
			if (getCodeBranche().length() != 3 || !isValidLettersOrDigitsOnly(getCodeBranche())) {
				throw new NSValidation.ValidationException(msgPrefix + " doit comprendre exactement 3 chiffres ou lettres majuscules sans accents ou être vide.");
			}
		}
	}

	public static boolean isValidLettersOnly(String s) {
		return (MyStringCtrl.chaineSansAccents(s).equals(s) && ERXStringUtilities.isLettersOnly(s) && s.toUpperCase().equals(s));
	}

	public static boolean isValidLettersOrDigitsOnly(String s) {
		return (s.toUpperCase().equals(s) && StringCtrl.toBasicString(s, AUTHORIZED_CHARS, '$').equals(s));
	}

	/**
	 * @param bic
	 * @return TRUE si le code pays fait partie de la liste des pays acceptant les virements SEPA
	 * @deprecated
	 */
	public static Boolean isInZoneSEPA(String bic) {
		if (bic != null) {
			ControleBIC ctrl = new ControleBIC(bic);
			return codePaysZoneSepa().contains(ctrl.getCodePays());
		}
		return Boolean.FALSE;
	}

	/**
	 * @param bic
	 * @param edc
	 * @return TRUE si le code pays fait partie de la liste des pays acceptant les virements SEPA
	 */
	public static Boolean isInZoneSEPA(String bic, EOEditingContext edc) {
		if (bic != null) {
			ControleBIC ctrl = new ControleBIC(bic);
			return codePaysZoneSepa(edc).contains(ctrl.getCodePays());
		}
		return Boolean.FALSE;
	}

	/**
	 * @param bic
	 * @return TRUE si le code pays fait partie de la liste des pays acceptant les virements FR
	 * @deprecated
	 */
	public static Boolean isInZoneFR(String bic) {
		if (bic != null) {
			ControleBIC ctrl = new ControleBIC(bic);
			return codesPaysObligatoiresPourRibFR().contains(ctrl.getCodePays());
		}
		return Boolean.FALSE;
	}

	/**
	 * @param bic
	 * @param edc
	 * @return TRUE si le code pays fait partie de la liste des pays acceptant les virements FR
	 */
	public static Boolean isInZoneFR(String bic, EOEditingContext edc) {
		if (bic != null) {
			ControleBIC ctrl = new ControleBIC(bic);
			return codesPaysObligatoiresPourRibFR(edc).contains(ctrl.getCodePays());
		}
		return Boolean.FALSE;
	}

	/**
	 * @return les codes pays ou la saisie d'un rib FR est obligatoire.
	 * @deprecated
	 */
	public static NSArray<String> codesPaysObligatoiresPourRibFR() {
		return codesPaysObligatoiresPourRibFR(EOSharedEditingContext.defaultSharedEditingContext());
	}

	/**
	 * @param edc
	 * @return les codes pays ou la saisie d'un rib FR est obligatoire.
	 */
	public static NSArray<String> codesPaysObligatoiresPourRibFR(EOEditingContext edc) {
		if (codePaysRibFRObligatoire == null) {
			String s = EOGrhumParametres.parametrePourCle(edc, CODE_PAYS_RIB_FR_OBLIGATOIRE_KEY);
			NSArray<String> res = NSArray.emptyArray();
			if (!ERXStringUtilities.stringIsNullOrEmpty(s)) {
				s = s.trim().replaceAll(" ", "");
				res = NSArray.componentsSeparatedByString(s, ",");
			}
			codePaysRibFRObligatoire = res;
		}
		return codePaysRibFRObligatoire;
	}

	/**
	 * @return les codes pays de la zone SEPA (ceux où la saisie de l'IBAN est obligatoire).
	 * @deprecated
	 */
	public static NSArray<String> codePaysZoneSepa() {
		return codePaysZoneSepa(EOSharedEditingContext.defaultSharedEditingContext());
	}

	/**
	 * @param edc
	 * @return les codes pays de la zone SEPA (ceux où la saisie de l'IBAN est obligatoire).
	 */
	public static NSArray<String> codePaysZoneSepa(EOEditingContext edc) {
		if (codePaysZoneSepa == null) {
			String s = EOGrhumParametres.parametrePourCle(edc, CODE_PAYS_ZONE_SEPA_KEY);
			NSArray<String> res = NSArray.emptyArray();
			if (!ERXStringUtilities.stringIsNullOrEmpty(s)) {
				s = s.trim().replaceAll(" ", "");
				res = NSArray.componentsSeparatedByString(s, ",");
			}
			codePaysZoneSepa = res;
		}
		return codePaysZoneSepa;
	}
}
