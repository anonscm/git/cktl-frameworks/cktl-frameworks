/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.controles;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

/**
 * Rassemble des controles sur les saisies (validité du format, doublons etc.)
 * 
 * @author rprin
 */
public class ControleSpecifique {

	public static String DEFAULT_LOGIN_PATTERN = "[^[a-zA-Z0-9]]+";
	public static String DEFAULT_LOGIN_PATTERN_NOT_MATCH_MSG = "Le login ne doit pas comporter de caractères autres qu'alphanumériques";

	//	public static String EMAIL_PATTERN = "^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,3}$";

	/**
	 * Pattern pour valider la partie gauche d'une adresse email (avant le @).
	 */
	public static String EMAIL_LEFT_PATTERN = "^[a-z0-9._-]+";

	public static String ALIAS_PATTERN = "^[a-zA-Z0-9._-]+";
	public static String ALIAS_PATTERN_NOT_MATCH_MSG = "L'alias ne doit pas comporter de caractères autres " +
			"qu'alphanumériques ou -, ou _";

	/**
	 * Verifie si le login passé en paramètre a un format correct.
	 * 
	 * @param ec
	 * @param login
	 * @throws ControleSpecifiqueException
	 */
	public static void checkLoginFormatCorrect(EOEditingContext ec, String login) throws ControleSpecifiqueException {
		String s = getLoginPattern(ec);
		try {
			Pattern.compile(s);
			if (!login.matches(s)) {
				String msg = getLoginPatternNotMatchMsg(ec);
				if (msg == null && s.equals(DEFAULT_LOGIN_PATTERN)) {
					msg = DEFAULT_LOGIN_PATTERN_NOT_MATCH_MSG;
				}
				else {
					msg = "Le login ne respecte pas la pattern " + s;
				}

				throw new ControleSpecifiqueException(msg + " (" + login + ")");
			}

		} catch (PatternSyntaxException e) {
			ControleSpecifiqueException excp = new ControleSpecifiqueException("La chaine " + s + " n'est pas une regular expression valide : " + e.getMessage());
			throw excp;
		}

	}

	/**
	 * @param ec
	 * @param login
	 * @return true si la chaine login respecte le format des login.
	 */
	public static boolean isLoginFormatCorrect(EOEditingContext ec, String login) {
		try {
			checkLoginFormatCorrect(ec, login);
			return true;
		} catch (ControleSpecifiqueException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void checkAliasFormatCorrect(EOEditingContext ec, String alias) throws ControleSpecifiqueException {
		String s = ALIAS_PATTERN;
		try {
			Pattern.compile(s);
			if (!alias.matches(s)) {
				throw new ControleSpecifiqueException(ALIAS_PATTERN_NOT_MATCH_MSG + " (" + alias + ")");
			}

		} catch (PatternSyntaxException e) {
			ControleSpecifiqueException excp = new ControleSpecifiqueException("La chaine " + s + " n'est pas une regular expression valide : " + e.getMessage());
			throw excp;
		}

	}

	/**
	 * @param ec
	 * @param alias
	 * @return true si la chaine alias respecte le format des alias.
	 */
	public static boolean isAliasFormatCorrect(EOEditingContext ec, String alias) {
		try {
			checkAliasFormatCorrect(ec, alias);
			return true;
		} catch (ControleSpecifiqueException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void checkEmailFormatCorrect(EOEditingContext ec, String email) throws ControleSpecifiqueException {
		if (!MyStringCtrl.isEmailValid(email)) {
			throw new ControleSpecifiqueException("L'email n'est pas valide (" + email + ")");
		}
	}

	/**
	 * @param ec
	 * @param email
	 * @return true si la chaine alias respecte le format des email.
	 */
	public static boolean isEmailFormatCorrect(EOEditingContext ec, String email) {
		try {
			checkAliasFormatCorrect(ec, email);
			return true;
		} catch (ControleSpecifiqueException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static class ControleSpecifiqueException extends NSValidation.ValidationException {
		private static final long serialVersionUID = -1666734307137512985L;

		public ControleSpecifiqueException(String arg0) {
			super(arg0);
		}

	}

	/**
	 * @param ec
	 * @return la Regular Expression servant à valider un login.
	 */
	public static String getLoginPattern(EOEditingContext ec) {
		return EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_ANNUAIRE_LOGIN_PATTERN, DEFAULT_LOGIN_PATTERN);
	}

	/**
	 * @param ec
	 * @return Le message d'erreur associé à la patern.
	 */
	public static String getLoginPatternNotMatchMsg(EOEditingContext ec) {
		return EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_ANNUAIRE_LOGIN_PATTERN_NOT_MATCH_MSG, null);
	}

}
