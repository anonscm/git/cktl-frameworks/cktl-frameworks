/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.common.controles;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Uptilitaires autour de l'IBAN.<br>
 * Format pour la France :<br>
 * Format IBAN : FRkk BBBB BGGG GGCC CCCC CCCC CKK<br>
 * B = code banque, G = code guichet, C = numéro de compte, K = clé
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class ControleIban {
	public final static Logger logger = Logger.getLogger(ControleIban.class);
	public static final String EXCEPTION_IBAN_FORMAT_INVALIDE = "Le code IBAN est invalide.";
	public static final String PREFIX_FR = "FR";

	public static String convertRibFRtoIban(String codebanque, String codeguichet, String numerocompte, String cle) {
		HashMap<String, String> tabConversion = new HashMap<String, String>();
		tabConversion.put("A", "10");
		tabConversion.put("B", "11");
		tabConversion.put("C", "12");
		tabConversion.put("D", "13");
		tabConversion.put("E", "14");
		tabConversion.put("F", "15");
		tabConversion.put("G", "16");
		tabConversion.put("H", "17");
		tabConversion.put("I", "18");
		tabConversion.put("J", "19");
		tabConversion.put("K", "20");
		tabConversion.put("L", "21");
		tabConversion.put("M", "22");
		tabConversion.put("N", "23");
		tabConversion.put("O", "24");
		tabConversion.put("P", "25");
		tabConversion.put("Q", "26");
		tabConversion.put("R", "27");
		tabConversion.put("S", "28");
		tabConversion.put("T", "29");
		tabConversion.put("U", "30");
		tabConversion.put("V", "31");
		tabConversion.put("W", "32");
		tabConversion.put("X", "33");
		tabConversion.put("Y", "34");
		tabConversion.put("Z", "35");

		String cleFinale = "XX";
		//	String codePays = "FR";
		String bban = codebanque.concat(codeguichet).concat(numerocompte).concat(cle);
		String bbanConvert = bban.toUpperCase();
		// Conversion des lettres du BBAN en chiffre
		Iterator<String> iter = tabConversion.keySet().iterator();
		while (iter.hasNext()) {
			String k = (String) iter.next();
			bbanConvert = bbanConvert.replaceAll(k, tabConversion.get(k));
		}

		// Conversion des lettres du code Pays en chiffre
		String pays1 = tabConversion.get(PREFIX_FR.substring(0, 1));
		String pays2 = tabConversion.get(PREFIX_FR.substring(1, 2));

		String etape = bbanConvert + pays1 + pays2 + "00";
		//int clefCalculee = 98 - ( MOD(TO_NUMBER(etape),97));
		BigDecimal clefCalculee = new BigDecimal(98).subtract(new BigDecimal(etape).remainder(new BigDecimal(97)));
		cleFinale = (clefCalculee.intValue() < 10 ? "0" : "") + clefCalculee.intValue();

		String res = PREFIX_FR.concat(cleFinale).concat(codebanque).concat(codeguichet).concat(numerocompte).concat(cle);
		return res;
	}

	/**
	 * @param iban
	 * @return Le code banque (ancien RIB) déduit à partir de l'IBAN pour les comptes FR
	 */
	public static String getCodeBanqueFR(String iban) {
		if (StringCtrl.isEmpty(iban)) {
			return null;
		}
		iban = iban.trim();
		if (!isCompteFR(iban) || iban.length() < 10) {
			return null;
		}
		return iban.substring(4, 9);
	}

	/**
	 * @param iban
	 * @return Le code guichet (ancien RIB) déduit à partir de l'IBAN pour les comptes FR
	 */
	public static String getCodeGuichetFR(String iban) {
		if (StringCtrl.isEmpty(iban)) {
			return null;
		}
		iban = iban.trim();
		if (!isCompteFR(iban) || iban.length() < 10) {
			return null;
		}
		return iban.substring(9, 14);
	}

	public static String getNoCompteFR(String iban) {
		if (StringCtrl.isEmpty(iban)) {
			return null;
		}
		iban = iban.trim();
		if (!isCompteFR(iban) || iban.length() < 25) {
			return null;
		}
		return iban.substring(14, 25);
	}

	/**
	 * @param iban
	 * @return La clé (ancien RIB) déduit à partir de l'IBAN pour les comptes FR
	 */
	public static String getCleRibFR(String iban) {
		if (StringCtrl.isEmpty(iban)) {
			return null;
		}
		iban = iban.trim();
		if (!isCompteFR(iban) || iban.length() < 27) {
			return null;
		}
		return iban.substring(25, 27);
	}

	/**
	 * @param iban
	 * @return le code IBAN formaté par grappes de 4 caractères
	 */
	public static String formatIban(String iban) {
		if (MyStringCtrl.isEmpty(iban)) {
			return "";
		}
		iban = iban.trim().replaceAll(" ", "");
		iban = iban.toUpperCase();
		String res = "";

		BigDecimal nbgrappes = BigDecimal.valueOf(iban.length()).divide(BigDecimal.valueOf(4));
		int max = nbgrappes.intValue();
		String reste = iban;
		for (int i = 0; i < max; i++) {
			res = res.concat(reste.substring(0, 4)).concat(" ");
			reste = reste.substring((reste.length() > 4 ? 4 : reste.length()));
		}
		res = res.concat(reste);
		return res;
	}

	/**
	 * Vérifie si l'IBAN passé en paramètre est valide.
	 * 
	 * @param iban
	 * @throws NSValidation.ValidationException
	 */
	public static void verifierFormatIban(String iban) throws NSValidation.ValidationException {
		/*
		 * Le code IBAN (International Bank Account Number) est un identifiant international pour les comptes clients des institutions financières. Il
		 * est composé de trois parties : Le code pays : 2 lettres identifiant le pays dans lequel l'institution financière réside. On utilise les
		 * codes spécifiés dans la norme ISO 3166. Une clé de contrôle composée de 2 chiffres. Le numéro de compte (BBAN, Basic Bank Account Number)
		 * composé au maximum de 30 caractères alphanumériques (les 10 chiffres plus les lettres majuscules de "A" à "Z". ex : FR14 2004 1010 0505
		 * 0001 3M02 606
		 */

		final String[] lettres_iban = new String[] {
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
		};
		final String[] nombres_iban = new String[] {
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35"
		};

		if (iban == null || iban.trim().length() == 0) {
			return;
		}

		String ibanTmp = iban.trim().toUpperCase();
		ibanTmp = ibanTmp.replaceAll(" ", "");
		String codePays = ibanTmp.substring(0, 2);
		String clefIban = ibanTmp.substring(2, 4);
		String bban = ibanTmp.substring(4);

		//		String etape1 = bban + codePays + "00";

		// Conversion des lettres du BBAN en chiffre
		//Conversion des lettres du code Pays en chiffre
		for (int i = 0; i < lettres_iban.length; i++) {
			bban = bban.replaceAll(lettres_iban[i], nombres_iban[i]);
			codePays = codePays.replaceAll(lettres_iban[i], nombres_iban[i]);
		}
		String etape2 = bban + codePays + "00";
		BigInteger etape2Int = new BigInteger(etape2);
		BigInteger clefCalculee = new BigInteger("98").add(etape2Int.mod(new BigInteger("97")).negate());
		String clefFinale = (clefCalculee.intValue() < 10 ? clefFinale = "0" + clefCalculee.intValue() : "" + clefCalculee.intValue());

		if (!clefFinale.equals(clefIban)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Controle Iban : IBAN invalide : " + iban);
			}
			throw new NSValidation.ValidationException(EXCEPTION_IBAN_FORMAT_INVALIDE);
		}

	}

	/**
	 * @param iban
	 * @return TRUE si l'iban représente un compte d'une banque francaise. (Commence par FR).
	 */
	public static Boolean isCompteFR(String iban) {
		if (ERXStringUtilities.stringIsNullOrEmpty(iban)) {
			return Boolean.FALSE;
		}
		return iban.startsWith(PREFIX_FR);
	}

}
