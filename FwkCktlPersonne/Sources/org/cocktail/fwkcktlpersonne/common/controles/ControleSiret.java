/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.common.controles;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

/**
 * Utilitaires autour du SIRET et SIREN.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class ControleSiret {
	public final static Logger logger = Logger.getLogger(ControleSiret.class);

	public static final int NB_CHARS_SIREN = 9;
	public static final int NB_CHARS_SIRET = 14;

	/**
	 * Verifie la validite d'un n° de Siret
	 * 
	 * @param siret Le siret a tester
	 * @return true si le siret specifie est valide, false sinon.
	 */
	public static boolean isSiretValide(String siret) {
		try {
			new Long(siret);
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Validation du SIRET " + siret + " : ne contient pas que des chiffres");
			}
			return false; // Ne contient pas que des caractères numériques
		}
		// completion à gauche par des 0 pour avoir un numero SIRET de 14 caractères
		if (siret.length() < 14) {
			siret = MyStringCtrl.extendWithChars(siret, "0", 14, true);
		}
		int somme = 0;
		boolean res = false;
		for (int i = 0; i < siret.length(); i++) {
			int nombre = new Integer(siret.substring(i, i + 1)).intValue();
			// pour les chiffres impairs, on multiplie par 2. Comme on commence à zéro pour le premier caractère, on regarde le modulo 2
			if (i % 2 == 0) {
				try {
					nombre = nombre * 2;
					// si le nombre est supérieur à 9 on retranche 9 
					if (nombre > 9) {
						nombre = nombre - 9;
					}
				} catch (Exception e) {
					return false;
				}
			}
			somme += nombre;
		}
		if (somme % 10 == 0) {
			res = true;
		}
		else {
			if (logger.isDebugEnabled()) {
				logger.debug("Validation du SIRET " + siret + " : INVALIDE");
			}
		}

		return res;

	}

	/**
	 * Verifie la validite d'un n° de SIREN.<br/>
	 * Le numéro SIREN est composé de 8 chiffres, plus un chiffre de contrôle qui permet de vérifier la validité du numéro. La clé de contrôle
	 * utilisée pour vérifier l'exactitude d'un identifiant est une clé « 1-2 », suivant l'algorithme de Luhn. Le principe est le suivant : on
	 * multiplie les chiffres de rang impair à partir de la droite par 1, ceux de rang pair par 2 ; la somme des chiffres obtenus doit être congrue à
	 * zéro modulo 10, c'est-à-dire qu'elle doit être multiple de 10.
	 * 
	 * @param siren
	 * @return true si le siren passé en parametre est valide.
	 */
	public static boolean isSirenValide(String siren) {
		try {
			new Long(siren);
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Validation du SIREN " + siren + " : ne contient pas que des chiffres");
			}
			return false; // Ne contient pas que des caractères numériques
		}
		// completion à gauche par des 0 pour avoir un numero SIREN de 9 caractères
		if (siren.length() < 9) {
			siren = MyStringCtrl.extendWithChars(siren, "0", 9, true);
		}
		int somme = 0;
		int pos = 0;
		boolean res = false;
		for (int i = siren.length() - 1; i >= 0; i--) {
			int nombre = new Integer(siren.substring(i, i + 1)).intValue();

			pos++;
			// pour les chiffres pairs, on multiplie par 2. 
			if (pos % 2 == 0) {
				try {
					nombre = nombre * 2;
					// si le nombre est supérieur à 9 on retranche 9 (par ex. si on a 7 : 7x2=14, logiquement on prend en compte 1+4=5, soit 14-9).
					if (nombre > 9) {
						nombre = nombre - 9;
					}

				} catch (Exception e) {
					return false;
				}
			}
			//pour les chiffres impairs on les multiplie par 1...
			somme += nombre;
		}
		if (somme % 10 == 0) {
			res = true;
		}
		else {
			if (logger.isDebugEnabled()) {
				logger.debug("Validation du SIREN " + siren + " : INVALIDE");
			}
		}

		return res;

	}
	//	public static boolean isSirenValide(String siren) {
	//		try {
	//			new Long(siren);
	//		} catch (Exception e) {
	//			return false; // Ne contient pas que des caractères numériques
	//		}
	//		// completion à gauche par des 0 pour avoir un numero SIRET de 9 caractères
	//		if (siren.length() < 9) {
	//			siren = MyStringCtrl.extendWithChars(siren, "0", 9, true);
	//		}
	//		int somme = 0;
	//		for (int i = 0; i < siren.length(); i++) {
	//			int nombre = new Integer(siren.substring(i, i + 1)).intValue();
	//			// pour les chiffres pairs, on multiplie par 2. Comme on commence à zéro pour le premier caractère, on regarde le modulo 2
	//			if (i % 2 == 1) {
	//				try {
	//					nombre = nombre * 2;
	//					// si le nombre est supérieur à 9 on retranche 9 
	//					if (nombre > 9) {
	//						nombre = nombre - 9;
	//					}
	//				} catch (Exception e) {
	//					return false;
	//				}
	//			}
	//			somme += nombre;
	//		}
	//		if (somme % 10 == 0) {
	//			return true;
	//		}
	//		else {
	//			return false;
	//		}
	//		
	//	}
}
