/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.server.finder;

import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @deprecated utilisez PersonneDelegate
 */

public abstract class PersonneFinder extends AFinder {

	/**
	 * Recherche une personne par son persId. Effectue d'abord une recherche sur les individus. Si pas trouve, recherche sur les structures.
	 * 
	 * @deprecated utilisez PersonneDelegate.fetchPersonneByPersId a la place
	 */
	public static IPersonne fetchPersonneByPersId(EOEditingContext ec, Number persId) {
		if (persId == null) {
			return null;
		}
		EOIndividu ind = EOIndividu.fetchByKeyValue(ec, EOIndividu.PERS_ID_KEY, persId);
		if (ind != null) {
			return ind;
		}
		EOStructure struct = EOStructure.fetchByKeyValue(ec, EOIndividu.PERS_ID_KEY, persId);
		if (struct != null) {
			return struct;
		}

		return null;

	}

	/**
	 * @param ec
	 * @param login
	 * @param pass
	 * @return
	 * @deprecated utilisez PersonneDelegate.fetchPersonneForLoginPass a la place
	 */
	public static IPersonne fetchPersonneForLoginPass(EOEditingContext ec, String login, String pass) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		NSArray res = EOCompte.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
			sort1
		}));
		if (res.count() > 0) {
			return ((EOCompte) res.objectAtIndex(0)).toPersonne();
		}
		return null;
	}

	/**
	 * @param ec
	 * @param login
	 * @return
	 * @deprecated utilisez PersonneDelegate.fetchPersonneForLogin a la place
	 */
	public static IPersonne fetchPersonneForLogin(EOEditingContext ec, String login) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_LOGIN_KEY, EOQualifier.QualifierOperatorEqual, login));
		//    	quals.addObject(new EOKeyValueQualifier(EOCompte.CPT_PASSWD_KEY, EOQualifier.QualifierOperatorEqual, pass));              

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOCompte.CPT_VLAN_KEY, EOSortOrdering.CompareAscending);
		NSArray res = EOCompte.fetchAll(ec, new EOAndQualifier(quals), new NSArray(new Object[] {
			sort1
		}));
		if (res.count() > 0) {
			return ((EOCompte) res.objectAtIndex(0)).toPersonne();
		}
		return null;
	}
	
	/** 
     * execute la requette sql et recupere les valeurs des colonnes demandees dans le tableau 
     */
    public static NSArray rawRowsForSQL(EOEditingContext ec, String eomodelName, String query) {
      NSArray rawRowsForSQL = null;
      try {
        rawRowsForSQL = EOUtilities.rawRowsForSQL(ec, eomodelName, query);
      } catch (Throwable th) {
        th.printStackTrace();
        rawRowsForSQL = new NSArray();
      }
      return rawRowsForSQL;
    }

}
