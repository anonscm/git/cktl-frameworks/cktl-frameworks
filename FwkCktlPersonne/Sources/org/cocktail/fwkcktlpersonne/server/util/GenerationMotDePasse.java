/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.server.util;

import java.io.IOException;
import java.nio.CharBuffer;
import java.security.SecureRandom;
import java.util.Random;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;

import com.webobjects.eocontrol.EOEditingContext;

import edu.vt.middleware.password.PasswordCharacterRule;

public class GenerationMotDePasse {
	
	
	/* ************************ Extraction du code  de la classe PasswordGenerator.class du package edu.vt.middleware.password ***************************************** */
		protected StringBuilder digits;
		protected StringBuilder lowercase;
		protected StringBuilder uppercase;
		protected StringBuilder nonAlphanumeric;
		protected StringBuilder alphabetical;
		protected StringBuilder alphanumeric;
		protected StringBuilder all;
		protected Random random;
		
		private boolean hasLimitedCahracterList = false;

		public GenerationMotDePasse(EOEditingContext ec) throws Exception {
			 this(new SecureRandom());
			 PasswordService passwordUtilities = new PasswordService();
				// On détermine la chaine nonAlphanumeric en supprimant les
				// caractères alpha-numériques de ceux autorisés
//				this.nonAlphanumeric = new StringBuilder(passwordUtilities.getPasswordAuthorizedChars(ec).replaceAll("[a-zA-Z0-9]", ""));
				this.nonAlphanumeric = new StringBuilder(getGrhumParameter(ec, EOCompte.PARAM_PASSWORD_AUTHORIZED_CHARS).replaceAll("[a-zA-Z0-9]", ""));
				String val = getGrhumParameter(ec, EOCompte.PARAM_PASSWORD_AUTHORIZED_CHARS);
				if (!MyStringCtrl.isEmpty(val)) {
					all = new StringBuilder(val);
					hasLimitedCahracterList = true;
				}
		}

		public GenerationMotDePasse(Random r) {
			this.digits = new StringBuilder("0123456789");

			this.lowercase = new StringBuilder("abcdefghijklmnopqrstuvwxyz");

			this.uppercase = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

			this.nonAlphanumeric = new StringBuilder("`~!@#$%^&*()-_=+[{]};:<,>./?");

			this.alphabetical = new StringBuilder(this.lowercase).append(this.uppercase);

			this.alphanumeric = new StringBuilder(this.digits).append(this.alphabetical);

			this.random = r;
			this.all = new StringBuilder(this.alphanumeric).append(this.nonAlphanumeric);
			
		}
		
		public String generatePassword(int length, PasswordCharacterRule rule) {
			if (length <= 0) {
				throw new IllegalArgumentException("length must be greater than 0");
			}
			
			if (hasLimitedCahracterList) {
				this.lowercase = updateSubList(this.lowercase, all);
				this.uppercase = updateSubList(this.uppercase, all);
				this.alphabetical = updateSubList(this.alphabetical, all);
				this.digits = updateSubList(this.digits, all);
				this.nonAlphanumeric = updateSubList(this.nonAlphanumeric, all);
			}
			
			
			CharBuffer buffer = CharBuffer.allocate(length);
			if (rule != null) {
				fillRandomCharacters(this.lowercase, rule.getNumberOfLowercase(), buffer);
				fillRandomCharacters(this.uppercase, rule.getNumberOfUppercase(), buffer);
				fillRandomCharacters(this.alphabetical, rule.getNumberOfAlphabetical(), buffer);
				
				fillRandomCharacters(this.digits, rule.getNumberOfDigits(), buffer);
				fillRandomCharacters(this.nonAlphanumeric, rule.getNumberOfNonAlphanumeric(), buffer);
				
			}
			
			fillRandomCharacters(this.all, length - buffer.position(), buffer);
			buffer.flip();
			randomize(buffer);
			return buffer.toString();
		}

		public String generatePasswordAvecPremiereLettre(int length, int lengthMax, PasswordCharacterRule rule) {
			if (length <= 0) {
				throw new IllegalArgumentException("length must be greater than 0");
			}

			if (hasLimitedCahracterList) {
				this.lowercase = updateSubList(this.lowercase, all);
				this.uppercase = updateSubList(this.uppercase, all);
				this.alphabetical = updateSubList(this.alphabetical, all);
				this.digits = updateSubList(this.digits, all);
				this.nonAlphanumeric = updateSubList(this.nonAlphanumeric, all);
			}
			
			CharBuffer buffer1 = CharBuffer.allocate(length);
			if (rule != null) {
				fillRandomCharacters(this.lowercase, 0, buffer1);
				fillRandomCharacters(this.uppercase, 1, buffer1);
				fillRandomCharacters(this.alphabetical, 1, buffer1);
				
				fillRandomCharacters(this.digits, 0, buffer1);
				fillRandomCharacters(this.nonAlphanumeric, 0, buffer1);
				
//				if (rule.getNumberOfNonAlphanumeric() == 0) {
//					this.all = new StringBuilder(this.alphanumeric);
//				}
			}
			fillRandomCharacters(this.uppercase, length - buffer1.position(), buffer1);
			buffer1.flip();
			randomize(buffer1);
			
			CharBuffer buffer = CharBuffer.allocate(lengthMax);
			if (rule != null) {
				fillRandomCharacters(this.lowercase, rule.getNumberOfLowercase(), buffer);
				fillRandomCharacters(this.uppercase, rule.getNumberOfUppercase(), buffer);
				fillRandomCharacters(this.alphabetical, rule.getNumberOfAlphabetical(), buffer);
				
				fillRandomCharacters(this.digits, rule.getNumberOfDigits(), buffer);
				fillRandomCharacters(this.nonAlphanumeric, rule.getNumberOfNonAlphanumeric(), buffer);
				
			}

			fillRandomCharacters(this.all, lengthMax - buffer.position(), buffer);
			buffer.flip();
			randomize(buffer);
			return buffer1.toString().substring(0, 1) + buffer.toString().substring(0, length - 1);
		}

		protected void fillRandomCharacters(CharSequence source, int count, Appendable target)
		{
			for (int i = 0; i < count; ++i) {
				try {
					target.append(source.charAt(this.random.nextInt(source.length())));
				} catch (IOException e) {
					throw new RuntimeException("Error appending characters.", e);
				}
			}	
		}

		protected void randomize(CharBuffer buffer) {
			for (int i = buffer.position(); i < buffer.limit(); ++i) {
				int n = this.random.nextInt(buffer.length());
				char c = buffer.get(n);
				buffer.put(n, buffer.get(i));
				buffer.put(i, c);
			}
		}

		protected String getGrhumParameter(EOEditingContext ec, String nomParam) {
			return EOGrhumParametres.parametrePourCle(ec, nomParam);
		}
		
		protected StringBuilder updateSubList(StringBuilder sousListe, StringBuilder fullList) {
			int longueur = sousListe.length();
			String listeCaracteres = fullList.toString();
			String listeATester = sousListe.toString();
			
			for (int i = 0; i < longueur; i++) {
				char caractere = listeATester.charAt(i);
				
				if (listeCaracteres.indexOf(caractere) == -1) {
					sousListe.deleteCharAt(i);
				}
			}
			
			return sousListe;
		}
	
}
