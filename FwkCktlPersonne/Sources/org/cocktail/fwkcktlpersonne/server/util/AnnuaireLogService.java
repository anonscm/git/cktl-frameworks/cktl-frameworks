package org.cocktail.fwkcktlpersonne.server.util;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAnnuaireLog;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAnnuaireLog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXThreadStorage;

public class AnnuaireLogService {

	public void creationUnAnnuaireLog(EOEditingContext edc, String message, String nomApplication) {
		Integer persId = (Integer) ERXThreadStorage.valueForKey(PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
//		String login = FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.LOGIN_CURRENT_USER_STORAGE_KEY);
		String login = (String) ERXThreadStorage.valueForKey(PersonneApplicationUser.LOGIN_CURRENT_USER_STORAGE_KEY);
		
		
		if (persId != null && login != null && message != null && nomApplication != null) {
			IAnnuaireLog annuaireLog = EOAnnuaireLog.creerInstance(edc); // createEOAnnuaireLog(edc);

			annuaireLog.setDCreation(new NSTimestamp());

			annuaireLog.setPersId(persId);

			annuaireLog.setLogin(login);

			annuaireLog.setOperation(nomApplication + " : " + message);
		}
		
	}
	
}
