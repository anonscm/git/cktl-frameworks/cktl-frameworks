/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlpersonne.server.util;


import java.io.File;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOPasswordHistory;
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import edu.vt.middleware.dictionary.Dictionary;
import edu.vt.middleware.password.AbstractPasswordRule;
import edu.vt.middleware.password.Password;
import edu.vt.middleware.password.PasswordCharacterRule;
import edu.vt.middleware.password.PasswordChecker;
import edu.vt.middleware.password.PasswordDictionaryRule;
import edu.vt.middleware.password.PasswordException;
import edu.vt.middleware.password.PasswordLengthRule;
import edu.vt.middleware.password.PasswordRule;
import edu.vt.middleware.password.PasswordWhitespaceRule;
import er.extensions.foundation.ERXStringUtilities;

public class PasswordService {

	
	// Zone des variables
	
	public static final String PARAM_OUI = "O";
	public static final String PARAM_NON = "N";
	
	
	private static String passwordAuthorizedChars = null;
	private static String passwordDictionarySearch = null;
	private static String passwordDictionaryLocation = null;
	private String passwordHistory = null;
	
	// Parametres password
	public static final String PARAM_PASSWORD_DICTIONARY_SEARCH = "PASSWORD_DICTIONARY_SEARCH";
	public static final String PARAM_PASSWORD_DICTIONARY_LOCATION = "PASSWORD_DICTIONARY_LOCATION";
	public static final String PARAM_PASSWORD_HISTORY = "PASSWORD_HISTORY";
	
	private static final CompteService SERVICE_EOCOMPTE = new CompteService();
	
	
	// Constructeur pour éviter les méthodes statiques
	private EOEditingContext edc;
	
	public PasswordService() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Constructeur où on passe un Editing Context en paramètre
	 * @param edc Editing Context
	 */
	public PasswordService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * Méthode retournant l'editing context lié à la classe 
	 * @return l'editing Context de la classe
	 */
	public EOEditingContext edc() {
		return edc;
	}
	
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public static PasswordService creerNouvelleInstance(EOEditingContext edc) {
		return new PasswordService(edc);
	}
	
	// Méthodes de la classe de service
	/**
	 * Vérifie que les caractères utilisés dans le mot de passe sont valides
	 * 
	 * @param ec editing context passé en paramètre
	 * @param passwordClair password en clair
	 * @return true si aucun caractère non autorisé n'est présent dans le mot de passe
	 */
	public boolean checkPasswordAuthorizedChar(EOEditingContext ec, String passwordClair)  {
		String authChars;
		try {
			authChars = getPasswordAuthorizedChars(ec);
		} catch (Exception e) {
			return false;
		}
		for (char c : passwordClair.toCharArray()) {
			if (authChars.indexOf(c) == -1) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @param ec editing context passé en paramètre
	 * @return La liste des caractères autorisés
	 * @throws Exception exception levée lorsque la récupération du paramètre s'est mal passée.
	 */
	public String getPasswordAuthorizedChars(EOEditingContext ec) throws Exception {
		if (passwordAuthorizedChars == null) {
			String val = EOGrhumParametres.parametrePourCle(ec, EOCompte.PARAM_PASSWORD_AUTHORIZED_CHARS);
			if (ERXStringUtilities.stringIsNullOrEmpty(val)) {
				throw new Exception("Le parametre " + EOCompte.PARAM_PASSWORD_AUTHORIZED_CHARS + " n'est pas correctement renseigné.");
			}
			passwordAuthorizedChars = val;
		}
		return passwordAuthorizedChars;
	}
	
	
	/**
	 * Vérifie que le mot de passe répond bien aux différentes règles<br>
	 * La configuration des règles se fait via les paramètres suivants dans GRHUM_PARAMETRES :<br>
	 * <ul>
	 * <li>PASSWORD_LENGTH_MIN : Longueur minimum du mot de passe</li>
	 * <li>PASSWORD_LENGTH_MAX : Longueur maximum du mot de passe</li>
	 * <li>PASSWORD_AUTHORIZED_CHARS : Liste des caractères autorisés dans le mot de passe</li>
	 * <li>PASSWORD_RULE_NB_UPPERCASE : Nombre minimum de majuscules dans le mot de passe.(0 si non actif).</li>
	 * <li>PASSWORD_RULE_NB_DIGITS : Nombre minimum de chiffres dans le mot de passe.(0 si non actif).</li>
	 * <li>PASSWORD_RULE_NB_ALPHABETICAL : Nombre minimum de lettres dans le mot de passe.(0 si non actif).</li>
	 * <li>PASSWORD_RULE_NB_LOWERCASE : Nombre minimum de minuscules dans le mot de passe. (0 si non actif).</li>
	 * <li>PASSWORD_RULE_NB_NONALPHANUMERIC : Nombre minimum de caractères non alphanumériques dans le mot de passe. (0 si non actif).</li>
	 * <li>PASSWORD_RULES_MIN : Nombre de règles à respecter pour valider un mot de passe.</li>
	 * <li>PASSWORD_DICTIONARY_SEARCH : Booléen indiquant si la recherche par dictionnaires est activée</li>
	 * <li>PASSWORD_DICTIONARY_LOCATION : Dossier contenant les dictionnaires</li>
	 * <li>PASSWORD_HISTORY : Booléen indiquant si l'historisation des mots de passe est activée</li>
	 * <li>PASSWORD_VALID_PERIOD_CREATE : Durée de la validité du mot de passe lors de la création du compte</li>
	 * <li>PASSWORD_VALID_PERIOD_MODIFY : Durée de la validité du mot de passe lors de la modification l'utilisateur</li>
	 * <li>PASSWORD_VALID_PERIOD_MODIFY_ADMIN : Durée de la validité du mot de passe lors de la modification par un administrateur</li>
	 * </ul>
	 * 
	 * @param passwordClair
	 * @return Le mot de passe
	 * @throws Exception
	 */
	public String checkPassword(String passwordClair, String vlanCpt, EOEditingContext ec, EOCompte compte) throws Exception {
		if (passwordClair == null || passwordClair.trim().length() == 0) {
			throw new NSValidation.ValidationException("Le mot de passe ne doit pas être vide.");
		}
		passwordClair = passwordClair.trim();
		
		//TODO Faire qqch du booléen retourner.
		checkPasswordAuthorizedChar(ec, passwordClair);
		checkPasswordRules(passwordClair, vlanCpt, ec, compte);

		return passwordClair;
	}

	/**
	 * 
	 * @param passwordClair password à examiner
	 * @param vlanCpt Vlan lié au compte
	 * @param ec editing context de travail
	 * @param compte compte auquel est associé le password
	 * @return true si le password répond aux règles
	 */
	public boolean isPasswordOK(String passwordClair, String vlanCpt, EOEditingContext ec, EOCompte compte) {
		
		if (!checkPasswordAuthorizedChar(ec, passwordClair)) {
			return false;
		}
		
		try {
			checkPasswordRules(passwordClair, vlanCpt, ec, compte);
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	
	/**
	 * @return La règle sur les caractères composant le mot de passe
	 * @throws Exception
	 */
	private PasswordCharacterRule getPasswordCharRule(String vlanCpt, EOEditingContext ec) throws Exception {
		PasswordCharacterRule charRule = new PasswordCharacterRule();

		// Nombre minimum de majuscules
		int nbUppercase = SERVICE_EOCOMPTE.getPasswordRuleNbUppercase(ec).intValue();
		if (nbUppercase != -1) {
			charRule.setNumberOfUppercase(nbUppercase);
		}

		// Nombre minimum de minuscules
		int nbLowercase = SERVICE_EOCOMPTE.getPasswordRuleNbLowercase(ec).intValue();
		if (nbLowercase != -1) {
			charRule.setNumberOfLowercase(nbLowercase);
		}

		// Nombre minimum de chiffres
		int nbDigits = SERVICE_EOCOMPTE.getPasswordRuleNbDigits(ec).intValue();
		if (nbDigits != -1) {
			charRule.setNumberOfDigits(nbDigits);
		}

		// Nombre minimum de lettres
		int nbAlphabetical = SERVICE_EOCOMPTE.getPasswordRuleNbAlphabetical(ec).intValue();
		if (nbAlphabetical != -1) {
			charRule.setNumberOfAlphabetical(nbAlphabetical);
		}
		
		// Nombre minimum de caractères non-alphanumériques
		int nbNonAlphanumeric;
		if (vlanCpt.equals("X")) {
			nbNonAlphanumeric = 0;
		} else {
			nbNonAlphanumeric = SERVICE_EOCOMPTE.getPasswordRuleNbNonAlphanumeric(ec).intValue();
		}
		if (nbNonAlphanumeric != -1) {
				charRule.setNumberOfNonAlphanumeric(nbNonAlphanumeric);
		}

		// Nombre minimum de règles à valider
		int nbRulesMin;
		if (!vlanCpt.equals("X")) {
			nbRulesMin = SERVICE_EOCOMPTE.getPasswordRulesMin(ec).intValue();
		} else {
			nbRulesMin = 2;
		}
//		int nbRulesMin = getPasswordRulesMin().intValue();
		charRule.setNumberOfCharacteristics(nbRulesMin);

		return charRule;
	}

	/**
	 * @return Le checker avec des règles sur la création du mot de passe
	 * @throws Exception
	 */
	private PasswordChecker createPasswordChecker(String vlanCpt, EOEditingContext ec, EOCompte compte) throws Exception {
		PasswordChecker ruleList = new PasswordChecker();
		if (!vlanCpt.equals("X")) {
			ruleList.addPasswordRule(getPasswordLengthRule(ec));
//		}
//		ruleList.addPasswordRule(getPasswordCharRule());
//		ruleList.addPasswordRule(new PasswordWhitespaceRule()); // Pas d'espace dans le mot de passe
			ruleList.addPasswordRule(getPasswordCharRule(vlanCpt, ec));
			ruleList.addPasswordRule(new PasswordWhitespaceRule()); // Pas d'espace dans le mot de passe
		}
//		ruleList.addPasswordRule(getPasswordLengthRule());
//		ruleList.addPasswordRule(getPasswordCharRule());
//		ruleList.addPasswordRule(new PasswordWhitespaceRule()); // Pas d'espace dans le mot de passe
		// Si on doit chercher dans les dictionnaires
		if (isPasswordDictionarySearch(ec)) {
			ruleList.addPasswordRule(getPasswordDicRule());
		}
		// Si on doit chercher dans l'historique
		if (compte != null && SERVICE_EOCOMPTE.isHistorisationActivePourVlanParticulier(compte.editingContext(), compte.toVlans().cVlan())) {
			// On supprime les historiques qui n'ont pas été commités lors d'une
			// tentative précédente infructueuse de changer le mot de passe
			for (Object obj : ec.insertedObjects()) {
				if (obj instanceof EOPasswordHistory) {
					compte.deleteToPasswordHistoriesRelationship((EOPasswordHistory) obj);
				}
			}
			ruleList.addPasswordRule(getPasswordHistoryRule(compte));
		}
		return ruleList;
	}
	
	/**
	 * Vérifie la validation des règles et interprète le message d'erreur
	 * 
	 * @param passwordClair
	 * @throws Exception
	 */
	private void checkPasswordRules(String passwordClair, String vlanCpt, EOEditingContext ec, EOCompte compte) throws Exception {
		PasswordChecker checker = createPasswordChecker(vlanCpt, ec, compte);
		Password password = new Password(passwordClair);

		for (PasswordRule rule : checker.getPasswordRules()) {
			if (!rule.verifyPassword(password)) {
				// Règles sur les caractères
				if (rule instanceof PasswordCharacterRule) {
					String msg = "Le mot de passe ne répond pas à " + SERVICE_EOCOMPTE.getPasswordRulesMin(ec) + " des caractéristiques suivantes :\n";

					if (SERVICE_EOCOMPTE.getPasswordRuleNbDigits(ec) > 0) {
						msg += "    * doit contenir au moins " + SERVICE_EOCOMPTE.getPasswordRuleNbDigits(ec) + " chiffre(s)\n";
					}
						
					if (SERVICE_EOCOMPTE.getPasswordRuleNbAlphabetical(ec) > 0) {
						msg += "    * doit contenir au moins " + SERVICE_EOCOMPTE.getPasswordRuleNbAlphabetical(ec) + " lettre(s)\n";
					}
					
					if (SERVICE_EOCOMPTE.getPasswordRuleNbNonAlphanumeric(ec) > 0) {
						msg += "    * doit contenir au moins " + SERVICE_EOCOMPTE.getPasswordRuleNbNonAlphanumeric(ec) + " caractère(s) non-alphanumérique(s)\n";
					}
					
					if (SERVICE_EOCOMPTE.getPasswordRuleNbUppercase(ec) > 0) {
						msg += "    * doit contenir au moins " + SERVICE_EOCOMPTE.getPasswordRuleNbUppercase(ec) + " lettre(s) majuscule(s)\n";
					}
					
					if (SERVICE_EOCOMPTE.getPasswordRuleNbLowercase(ec) > 0) {
						msg += "    * doit contenir au moins " + SERVICE_EOCOMPTE.getPasswordRuleNbLowercase(ec) + " lettre(s) minuscule(s)\n";
					}
					
					throw new NSValidation.ValidationException(msg);
				} else if (rule instanceof PasswordLengthRule) {
				// Règles sur la longueur
				
					if (SERVICE_EOCOMPTE.getPasswordLengthMax(ec).equals(SERVICE_EOCOMPTE.getPasswordLengthMin(ec))) {
						throw new NSValidation.ValidationException("Le mot de passe doit contenir " + SERVICE_EOCOMPTE.getPasswordLengthMax(ec) + " caractères.");
					} else {
						throw new NSValidation.ValidationException("La longueur du mot de passe doit être comprise entre " + SERVICE_EOCOMPTE.getPasswordLengthMin(ec) + " et "
								+ SERVICE_EOCOMPTE.getPasswordLengthMax(ec) + " caractères.");
					}
				} else if (rule instanceof PasswordWhitespaceRule) {
				// Règles des espaces
				
					throw new NSValidation.ValidationException("Le mot de passe ne doit pas contenir d'espace.");

				} else if (rule instanceof PasswordDictionaryRule) {
				// Recherche dans les dictionnaires
				
					throw new NSValidation.ValidationException("Le mot de passe contient un mot d'un dictionnaire : " + ((PasswordDictionaryRule) rule).getMatchingWord());
				} else if (rule instanceof myPasswordHistoryRule) {
				// Recherche dans l'historique
				
					throw new NSValidation.ValidationException(rule.getMessage());
				} else {
					// Erreur non connue
					throw new PasswordException(rule);
				}
			}
		}
	}
	
	/**
	 * Renvoi un mot de passe généré en fonction des règles paramétrées, de longueur PASSWORD_LENGTH_MIN
	 * 
	 * @param ec editing context de la classe de travail
	 * @param vlanCpt le Vlan associé au compte
	 * @return le nouveau mot de passe généré
	 * @throws Exception exception levée lors de la récupération des valeurs des paramètres 
	 */
	public String getRandomPasswordWithPrivateRules(EOEditingContext ec, String vlanCpt) throws Exception {
		GenerationMotDePasse generator = new GenerationMotDePasse(ec);

		Integer lengthMin;
		if (vlanCpt.equals("X")) {
			lengthMin = new Integer("8");
		} else {
			lengthMin = SERVICE_EOCOMPTE.getPasswordLengthMin(ec);
		}

		if (FwkCktlPersonne.paramManager.isCodeActivationActif(FwkCktlPersonneParamManager.COMPTE_PASSWORD_GENERATOR_SWITCH)) {
			int lengthMax = SERVICE_EOCOMPTE.getPasswordLengthMax(ec);
			return generator.generatePasswordAvecPremiereLettre(lengthMin, lengthMax, getPasswordCharRule(vlanCpt, ec));

		} else {
			return generator.generatePassword(lengthMin, getPasswordCharRule(vlanCpt, ec));
		}
		
	}
	
	/**
	 * @return La règle sur la longueur du mot de passe
	 * @throws Exception
	 */
	private PasswordLengthRule getPasswordLengthRule(EOEditingContext ec) throws Exception {
		int pwdLengthMax = SERVICE_EOCOMPTE.getPasswordLengthMax(ec).intValue();
		int pwdLengthMin = SERVICE_EOCOMPTE.getPasswordLengthMin(ec).intValue();
		return new PasswordLengthRule(pwdLengthMin, pwdLengthMax);
	}
	
	
	/**
	 * Règle permettant de vérifier que le mot de passe n'a pas déjà été utilisé On se sert de {@link EOPasswordHistory} pour avoir l'historique
	 */
	private class myPasswordHistoryRule extends AbstractPasswordRule {
		private NSMutableArray history = new NSMutableArray();

		public void addHistory(String cryptedPassword, String typeCryptage) {
			history.addObject(new Object[] {
					cryptedPassword, typeCryptage
			});
		}

		public void addHistory(EOPasswordHistory eoPH) {
			addHistory(eoPH.pwdhPasswd(), eoPH.toTypeCryptage().tcryJavaMethode());
		}

		/**
		 * Récupère les anciens mots de passe ainsi que leurs cryptage
		 * 
		 * @param eoPasswordHistoryList
		 */
		public void addHistory(NSArray eoPasswordHistoryList) {
			if (eoPasswordHistoryList == null) {
				return;
			}
			for (Object eoPH : eoPasswordHistoryList) {
				if (eoPH instanceof EOPasswordHistory) {
					addHistory((EOPasswordHistory) eoPH);
				} else {
					throw new ClassCastException("La liste ne contient pas de EOPasswordHistory");
				}
			}
		}

		/**
		 * {@inheritDoc} <br>
		 * Vérifie que le mot de passe n'a pas déjà été utilisé
		 */
		@Override
		public boolean verifyPassword(Password password) {
			boolean success = false;

			if (password != null) {
				if (history.count() == 0) {
					success = true;
				} else {
					for (Object cryptpassAndTypecrypt : history) {
						String cryptedPassword = (String) ((Object[]) cryptpassAndTypecrypt)[0];
						String typeCrypt = (String) ((Object[]) cryptpassAndTypecrypt)[1];
						if (CryptoCtrl.equalsToCryptedPass(typeCrypt, password.getText(), cryptedPassword)) {
							success = false;
							setMessage("Le mot de passe a déjà été utilisé pour ce compte.");
							break;
						} else {
							success = true;
						}
					}
				}
			} else {
				setMessage("Le mot de passe ne doit pas être null");
			}
			return success;
		}
	}
	
	/**
	 * @return La règle sur la recherche d'une partie du mot de passe dans les dictionnaires
	 * @throws Exception
	 */
	private PasswordDictionaryRule getPasswordDicRule() throws Exception {
		// Récupération des fichiers dictionnaires
		String dicDirectory = getPasswordDictionaryLocation();
		String[] dicFileList = FileCtrl.listDir(dicDirectory, "*", true);
		// Creation d'un dictionnaire
		Dictionary dictionary = new Dictionary();
		// Ignore la casse
		dictionary.ignoreCase();
		// Ajout des fichiers dictionnaires
		for (String dicFileName : dicFileList) {
			if (!FileCtrl.isDirectory(dicFileName)) {
				dictionary.insert(new File(dicFileName));
			}
		}
		// Tri
		dictionary.quickSort();
		// Construction
		dictionary.build();
		// Creation des règles du dictionnaire
		PasswordDictionaryRule dictionaryRule = new PasswordDictionaryRule(dictionary, 4);
		// On verifie les mots dans les deux sens
		dictionaryRule.matchBackwards();

		return dictionaryRule;
	}
	
	/**
	 * @return the passwordDictionaryLocation
	 * @throws Exception exception levée quand le paramètre n'arrive pas à être récupéré.
	 */
	public String getPasswordDictionaryLocation() throws Exception {
		if (passwordDictionaryLocation == null) {
			String val = ((CktlWebApplication) CktlWebApplication.application()).config().stringForKey(PARAM_PASSWORD_DICTIONARY_LOCATION);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_DICTIONARY_LOCATION + " n'est pas correctement renseigné.");
			}
			val = val.trim();
			if (!val.endsWith(FileCtrl.SEPARATOR)) {
				val += FileCtrl.SEPARATOR;
			}

			if (!FileCtrl.existsFile(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_DICTIONARY_LOCATION + " n'est pas correctement renseigné : Le répertoire n'a pas été trouvé.");
			}
			if (FileCtrl.listDir(val).length == 0) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_DICTIONARY_LOCATION + " n'est pas correctement renseigné : Le répertoire est vide.");
			}
			passwordDictionaryLocation = val;
		}
		return passwordDictionaryLocation;
	}
	
	
	/**
	 * @return La règle vérifiant que le mot de passe n'a pas déjà été utilisé
	 * @throws Exception
	 */
	private myPasswordHistoryRule getPasswordHistoryRule(EOCompte compte) throws Exception {
		myPasswordHistoryRule passwordHistoryRule = new myPasswordHistoryRule();
		passwordHistoryRule.addHistory(compte.toPasswordHistories());
		return passwordHistoryRule;
	}
	
	/**
	 * @param ec editing context passé en paramètre
	 * @return the passwordDictionarySearch
	 * @throws Exception exception levée quand le paramètre n'arrive pas à être récupéré.
	 */
	public boolean isPasswordDictionarySearch(EOEditingContext ec) throws Exception {
		if (passwordDictionarySearch == null) {
			String val = EOGrhumParametres.parametrePourCle(ec, PARAM_PASSWORD_DICTIONARY_SEARCH);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_DICTIONARY_SEARCH + " n'est pas correctement renseigné.");
			}
			passwordDictionarySearch = val;
		}
		return PARAM_OUI.equals(passwordDictionarySearch);
	}
	
	/**
	 * @param ec editing context passé en paramètre
	 * @return the passwordHistory
	 * @throws Exception exception levée quand le paramètre n'arrive pas à être récupéré.
	 */
	public boolean isHistorisationGlobaleActive(EOEditingContext ec) throws Exception {
		if (passwordHistory == null) {
			String val = getHistorisationParametre(ec);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + PARAM_PASSWORD_HISTORY + " n'est pas correctement renseigné.");
			}
			passwordHistory = val;
		}
		return PARAM_OUI.equals(passwordHistory);
	}
	
	protected String getHistorisationParametre(EOEditingContext ec) {
		return EOGrhumParametres.parametrePourCle(ec, PARAM_PASSWORD_HISTORY);
	}
	
	/**
	 * 
	 * @param ec editing context associé au traitement
	 * @param compte passage du compte qui a ce mot de passe
	 * @param passwordClair valeur actuelle non crypté contenu dans le passwordClair
	 * @return le nouveau mot de passe ou bien l'ancien contrôlé
	 * @throws Exception exception levée lors de la récupération de paramètres
	 */
	public String checkPasswordIsNotHistorized(EOEditingContext ec, EOCompte compte, String passwordClair) throws Exception {
		
		if (SERVICE_EOCOMPTE.isHistorisationActivePourVlanParticulier(ec, compte.toVlans().cVlan())) {
			// Variables pour stopper les boucles au cas où
			int nbreBoucles = 0;
			final int NBRE_MAX_BOUCLES = 50;
			
			NSArray<EOPasswordHistory> listeHistoriquePwd = compte.toPasswordHistories();
			NSArray<String> listePwd = (NSArray<String>) listeHistoriquePwd.valueForKey(EOPasswordHistory.PWDH_PASSWD_KEY);
			while (listePwd.contains(passwordClair) && nbreBoucles < NBRE_MAX_BOUCLES) {
				passwordClair = getRandomPasswordWithPrivateRules(ec, compte.toVlans().cVlan());
				nbreBoucles++;
			}
		}
		
		return passwordClair;
	}

	
	
	
	
}
