package org.cocktail.fwkcktlpersonne.server.util;

import java.util.Collection;

public class UnicitePwd {

	private static final int MAX_TRY_LIMIT = 5;
	private PwdGenerator pwdGenerator;

	public UnicitePwd(PwdGenerator pwdGenerator) {
		this.pwdGenerator = pwdGenerator;
	}

	public boolean checkUnicite(Collection<String> pwds, String pwd) {
		return !pwds.contains(pwd);
	}

	public String genererPwdUnique(final Collection<String> pwdExistants) {
		return genererPwdUnique(pwdExistants, 0);
	}

	protected String genererPwdUnique(final Collection<String> pwdExistants, int maxTry) {
		if (maxTry > MAX_TRY_LIMIT) {
			// moche, mettre une exception metier non runtime par exemple.
			throw new RuntimeException();
		}

		String newPwd = pwdGenerator.generate();
		if (checkUnicite(pwdExistants, newPwd)) {
			return newPwd;
		}

		return genererPwdUnique(pwdExistants, maxTry + 1);
	}

	public PwdGenerator getPwdGenerator() {
		return pwdGenerator;
	}

	public void setPwdGenerator(PwdGenerator pwdGenerator) {
		this.pwdGenerator = pwdGenerator;
	}

}

