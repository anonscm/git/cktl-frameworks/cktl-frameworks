/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlpersonne.server.util;

import java.lang.reflect.Method;

import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.appserver.WOSession;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @deprecated
 */

public class FwkCktlPersonneUtil {

	/**
	 * @param session Un objet heritant de WOSession.
	 * @return Le persId de l'utilisateur en cours. Fait appel a la methode Integer Session.getApplicationUserPersId(), qui doit être implementee par
	 *         le developpeur qui utilise le framework.
	 * @throws RuntimeException Si la methode n'est pas implementée (ou mal implémentée).
	 * @deprecated
	 */
	public static Integer getApplicationUserPersId(WOSession session) {
		try {
			Method method = session.getClass().getMethod("getApplicationUserPersId", new Class[] {
				PersonneApplicationUser.class
			});
			if (method == null) {
				throw new RuntimeException("La classe session doit implémenter la méthode getApplicationUserPersId() qui doit renvoyer le persId de l'utilisateur en cours.");
			}

			Object ret = method.invoke(session, new Object[] {});
			if (!(ret instanceof Number)) {
				throw new Exception("La methode getApplicationUserId de la classe Session doit renvoyer un Number");
			}
			return Integer.valueOf(((Number) ret).intValue());

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * @param session Un objet heritant de WOSession.
	 * @param ec
	 * @param tyapStrId Le type d'application
	 * @return
	 * @deprecated
	 */
	public static PersonneApplicationUser getApplicationUser(WOSession session, EOEditingContext ec, String tyapStrId) {
		try {
			Integer persid = getApplicationUserPersId(session);
			PersonneApplicationUser appUser = new PersonneApplicationUser(ec, tyapStrId, persid);
			if (appUser == null) {
				throw new Exception("Impossible de recuperer l'utilisateur correspondant au persId = " + persid);
			}
			return appUser;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * @param ed
	 * @param laProcedure
	 * @param lesParametres
	 * @deprecated
	 */
	public static void executeStoredProcedure(EOEditingContext ed, String laProcedure, NSDictionary lesParametres) {
		EOUtilities.executeStoredProcedureNamed(ed, laProcedure, lesParametres);
	}
	
	
	/**
	 * permet de faire un fetch en une seule ligne
	 * 
	 * @param ec
	 * @param entity
	 * @param qual
	 * @param arraySort
	 * @return NSArray des objets issus du fetch
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entity, EOQualifier qual, NSArray arraySort) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(entity, qual, arraySort);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * permet de faire un fetch en une seule ligne
	 * 
	 * @param ec
	 * @param entity
	 * @param qual
	 * @param arraySort
	 * @param refresh
	 * @return NSArray des objets issus du fetch
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entity, EOQualifier qual, NSArray arraySort, boolean refresh) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(entity, qual, arraySort);
		fetchSpec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(fetchSpec);
	}

	/**
	 * permet de sauvegarder un editing context
	 * 
	 * @param ec
	 * @param message
	 *          : le message d'erreur si probleme
	 */
	public static boolean save(EOEditingContext ec, String message)
			throws Exception {
		ec.lock();
		try {
			ec.saveChanges();
			ec.unlock();
			return true;
		} catch (Exception e) {
			ec.revert();
			ec.unlock();
			e.printStackTrace();
			throw e;
		}
	}
}
