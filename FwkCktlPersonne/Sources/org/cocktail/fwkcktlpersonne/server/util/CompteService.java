package org.cocktail.fwkcktlpersonne.server.util;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class CompteService {
	
	public static final Logger logger = Logger.getLogger(CompteService.class);
	
	public static final String PARAM_PASSWORD_LENGTH_MIN = "PASSWORD_LENGTH_MIN";
	public static final String PARAM_PASSWORD_LENGTH_MAX = "PASSWORD_LENGTH_MAX";
	
	public static final String PARAM_PASSWORD_RULE_NB_UPPERCASE = "PASSWORD_RULE_NB_UPPERCASE";
	public static final String PARAM_PASSWORD_RULE_NB_DIGITS = "PASSWORD_RULE_NB_DIGITS";
	public static final String PARAM_PASSWORD_RULE_NB_ALPHABETICAL = "PASSWORD_RULE_NB_ALPHABETICAL";
	public static final String PARAM_PASSWORD_RULE_NB_LOWERCASE = "PASSWORD_RULE_NB_LOWERCASE";
	public static final String PARAM_PASSWORD_RULE_NB_NONALPHANUMERIC = "PASSWORD_RULE_NB_NONALPHANUMERIC";
	public static final String PARAM_PASSWORD_RULES_MIN = "PASSWORD_RULES_MIN";
	
	private PasswordService passwordService = new PasswordService();
	
	private static Integer loginLengthMin = null;
	private static Integer loginLengthMax = null;
	
	private static Integer passwordLengthMax = null;
	private static Integer passwordLengthMin = null;
	
	private static Integer passwordRuleNbUppercase = null;
	private static Integer passwordRuleNbLowercase = null;
	private static Integer passwordRuleNbDigits = null;
	private static Integer passwordRuleNbAlphabetical = null;
	private static Integer passwordRuleNbNonAlphanumeric = null;
	private static Integer passwordRulesMin = null;

	static final HashMap<String, EOQualifier> mapVlan = new HashMap<String, EOQualifier>();
	static final HashMap<String, String> mapHistoryVlan = new HashMap<String, String>();
	
	static {
		mapVlan.put("X", EOCompte.QUAL_CPT_VLAN_X);
		mapVlan.put("E", EOCompte.QUAL_CPT_VLAN_E);
		mapVlan.put("P", EOCompte.QUAL_CPT_VLAN_P);
		mapVlan.put("R", EOCompte.QUAL_CPT_VLAN_R);
		mapVlan.put("D", EOCompte.QUAL_CPT_VLAN_D);
		mapVlan.put("G", EOCompte.QUAL_CPT_VLAN_G);
		
		mapHistoryVlan.put("X", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_X);
		mapHistoryVlan.put("E", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_E);
		mapHistoryVlan.put("P", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_P);
		mapHistoryVlan.put("R", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_R);
		mapHistoryVlan.put("D", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_D);
		mapHistoryVlan.put("G", FwkCktlPersonneParamManager.COMPTE_PASSWORD_HISTORY_VLAN_G);
	}
	
	
	public PasswordService getPasswordService() {
		return passwordService;
	}

	public void setPasswordService(PasswordService passwordService) {
		this.passwordService = passwordService;
	}
	
	public EOQualifier determinationDomaineVlans(EOEditingContext ec, String vlanSource){

		EOVlans vlan = EOVlans.fetchByKeyValue(ec, EOVlans.C_VLAN_KEY, vlanSource);
		String domaine = vlan.domaine();

		EOQualifier qual = new EOKeyValueQualifier(EOCompte.TO_VLANS_KEY + "." + EOVlans.C_VLAN_KEY, EOQualifier.QualifierOperatorEqual, vlanSource);

		if (domaine != null) {
			NSArray<EOVlans> lesVlans = EOVlans.fetchAll(ec);
			for (EOVlans unVlan : lesVlans) {
				if (isAutreVlanAvecMemeDomaine(vlanSource, domaine, unVlan)) {
					
					qual = new EOOrQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
							qual, mapVlan.get(unVlan.cVlan())
					}));
					
				}
			}
		}
		return qual;
	}

	private boolean isAutreVlanAvecMemeDomaine(String vlanSource, String domaine,
			EOVlans unVlan) {
		return !unVlan.cVlan().equals(vlanSource) && (domaine.equals(unVlan.domaine()));
	}
	
	/**
	 * @return la liste des domaines secondaires que l'on peut mettre sur un Vlan P
	 */
	public NSArray<String> getDomainesSecondairesVlanP(EOEditingContext ec) {
		return getDomainesSecondaires(ec, FwkCktlPersonneParamManager.PARAM_GRHUM_DOMAINES_SECONDAIRES);
	}
	
	/**
	 * @return la liste des domaines secondaires que l'on peut mettre sur un Vlan E
	 */
	public NSArray<String> getDomainesSecondairesVlanE(EOEditingContext ec) {
		return getDomainesSecondaires(ec, FwkCktlPersonneParamManager.PARAM_GRHUM_DOMAINES_SECONDAIRES_SCOL);
	}
	
	/**
	 * @return la liste des domaines secondaires que l'on peut mettre sur un Vlan R
	 */
	public NSArray<String> getDomainesSecondairesVlanR(EOEditingContext ec) {
		return getDomainesSecondaires(ec, FwkCktlPersonneParamManager.PARAM_GRHUM_DOMAINES_SECONDAIRES_RECHERCHE);
	}
	
	/**
	 * @return la liste des domaines secondaires que l'on peut mettre sur un Vlan
	 */
	public NSArray<String> getDomainesSecondaires(EOEditingContext ec, String nomsDomaine) {
		NSArray<String> listDomainesSecondaires = null;
		String paramDomainesSecondaires = EOGrhumParametres.parametrePourCle(ec, nomsDomaine);
		if (listDomainesSecondaires == null) {
			if (!MyStringCtrl.isEmpty(paramDomainesSecondaires) && !"univ.fr".equals(paramDomainesSecondaires)) {
				listDomainesSecondaires = new NSArray<String>(paramDomainesSecondaires.split(";"));
			}
		}
		return listDomainesSecondaires;
	}
	
	
	/**
	 * @return the passwordHistory
	 */
	public boolean isHistorisationActivePourVlanParticulier(EOEditingContext ec, String cVlan) throws Exception {
		boolean isHistoryWished = false;
		
		if (passwordService.isHistorisationGlobaleActive(ec)) {
			if (mapHistoryVlan.containsKey(cVlan)) {
				isHistoryWished = isCodeActivationActif(mapHistoryVlan.get(cVlan));
			}
		}
		return isHistoryWished;
	}

	protected boolean isCodeActivationActif(String nomParam) {
		return FwkCktlPersonne.paramManager.isCodeActivationActif(nomParam);
	}
	
	public Integer getLoginLengthMin(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, EOGrhumParametres.PARAM_ANNUAIRE_LOGIN_MIN, loginLengthMin);
	}

	public Integer getLoginLengthMax(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, EOGrhumParametres.PARAM_ANNUAIRE_LOGIN_MAX, loginLengthMax);
	}

	/**
	 * @return the passwordLengthMin
	 */
	public Integer getPasswordLengthMin(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_LENGTH_MIN, passwordLengthMin);
	}

	/**
	 * @return the passwordLengthMax
	 */
	public Integer getPasswordLengthMax(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_LENGTH_MAX, passwordLengthMax);
	}

	/**
	 * @return the passwordRuleNbUppercase
	 */
	public Integer getPasswordRuleNbUppercase(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULE_NB_UPPERCASE, passwordRuleNbUppercase);
	}

	/**
	 * @return the passwordRuleNbLowercase
	 */
	public Integer getPasswordRuleNbLowercase(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULE_NB_LOWERCASE, passwordRuleNbLowercase);
	}

	/**
	 * @return the passwordRuleNbDigits
	 */
	public Integer getPasswordRuleNbDigits(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULE_NB_DIGITS, passwordRuleNbDigits);
	}

	/**
	 * @return the passwordRuleNbAlphabetical
	 */
	public Integer getPasswordRuleNbAlphabetical(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULE_NB_ALPHABETICAL, passwordRuleNbAlphabetical);
	}

	/**
	 * @return the passwordRuleNbNonAlphanumeric
	 */
	public Integer getPasswordRuleNbNonAlphanumeric(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULE_NB_NONALPHANUMERIC, passwordRuleNbNonAlphanumeric);
	}

	/**
	 * @return the passwordRulesMin
	 */
	public Integer getPasswordRulesMin(EOEditingContext ec) throws Exception {
		return retourneUnParamNumerique(ec, PARAM_PASSWORD_RULES_MIN, passwordRulesMin);
	}
	
	public Integer retourneUnParamNumerique(EOEditingContext ec, String nomParam, Integer valueParam)
	throws Exception {
		if (valueParam == null) {
			String val = getGrhumParameter(ec, nomParam);
			if (MyStringCtrl.isEmpty(val)) {
				throw new Exception("Le parametre " + nomParam + " n'est pas correctement renseigné.");
			}
			valueParam = Integer.valueOf(val);
		}
		return valueParam;
	}

	protected String getGrhumParameter(EOEditingContext ec, String nomParam) {
		return EOGrhumParametres.parametrePourCle(ec, nomParam);
	}
	
	public void determinationCptePrincipal(EOEditingContext ec, Number persId) {
	    EOQualifier qual = 
	            ERXQ.and(EOCompte.QUAL_CPT_VALIDE_OUI, EOCompte.PERS_ID.eq(persId.intValue()));
	    NSArray<EOCompte> compteExistants = EOCompte.fetchAll(ec, qual, null);
	    NSArray<EOCompte> sorted = classerParPriorite(compteExistants);

	    for (int i = 0; i < sorted.count(); i++) {
            EOCompte compte = sorted.objectAtIndex(i);
	        if (0 == i) {
	            setCptPrincipal(compte, "O");
	        } else {
	            setCptPrincipal(compte, "N");
	        }
	    }
	    
	}
	
	public void determinationCptePrincipalAvecCategorie(EOEditingContext ec, Number persId) {
		EOQualifier qual = 
			ERXQ.and(EOCompte.QUAL_CPT_VALIDE_OUI, EOCompte.PERS_ID.eq(persId.intValue()));
		NSArray<EOCompte> compteExistants = EOCompte.fetchAll(ec, qual, null);
		NSArray<EOCompte> sorted = classerParPriorite(compteExistants);
		
		for (int i = 0; i < sorted.count(); i++) {
			EOCompte compte = sorted.objectAtIndex(i);
			if (0 == i) {
				setCptPrincipal(compte, "O");
			} else {
				setCptPrincipal(compte, "N");
			}
		}
		
		IPersonne personne = EOIndividu.fetchByKeyValue(ec, EOIndividu.PERS_ID_KEY, persId);
		Integer categorieIndividu = null;
		int numero;
		
		if ( personne != null && personne.isIndividu()) {
			categorieIndividu = ((EOIndividu)personne).categoriePrinc();
			
			if (categorieIndividu != null) {
				for (int i = 0; i < sorted.count(); i++) {
					EOCompte compte = sorted.objectAtIndex(i);
					setCptPrincipal(compte, "N");
				}
			}
			
			if (categorieIndividu != null) {
				numero = categorieIndividu.intValue();
			} else {
				numero = 0;
			}
			tenirCompteCategorieIndividu(numero, personne, ec);
		}
	}
	
	
	private void setCptPrincipal(EOCompte compte, String value) {
	    if (!value.equals(compte.cptPrincipal())) {
	        compte.setCptPrincipal(value);
	    }
	}
	
	public NSArray<EOCompte> classerParPriorite(NSArray<EOCompte> compteExistants) {
	    NSArray<EOCompte> sorted = ERXS.sorted(compteExistants, EOCompte.SORT_VLANS_PRIORITE);
	    return sorted;
	}
	
	private void tenirCompteCategorieIndividu(int numero, IPersonne personne, EOEditingContext editingContext) {
			
			switch (numero) {
			case 1 : miseJourCptPrincipal(new Integer(1), personne, "P", editingContext);
				break;
				
			case 2 : miseJourCptPrincipal(new Integer(2), personne, "E", editingContext);
				break;
				
			case 3 : miseJourCptPrincipal(new Integer(3), personne, "P", editingContext);
				break;
				
			case 4 : miseJourCptPrincipal(new Integer(4), personne, "E", editingContext);
				break;
				
			case 5 : miseJourCptPrincipal(new Integer(5), personne, "P", editingContext);
				break;
				
			case 6 : miseJourCptPrincipal(new Integer(6), personne, "P", editingContext);
				break;
				
			case 7 : miseJourCptPrincipal(new Integer(7), personne, "R", editingContext);
				break;
				
			case 8 : miseJourCptPrincipal(new Integer(8), personne, "X", editingContext);
				break;
				
			case 9 : miseJourCptPrincipal(new Integer(9), personne, "P", editingContext);
				break;
				
			case 10 : miseJourCptPrincipal(new Integer(10), personne, "P", editingContext);
				break;
				
			case 11 : miseJourCptPrincipal(new Integer(11), personne, "P", editingContext);
				break;
				
			case 12 : miseJourCptPrincipal(null, personne, "P", editingContext);
				break;
				
			default : logger.info("La catégorie principale ne correspond pas ou est nulle");
				miseJourCptPrincipal(null, personne, "P", editingContext);
				break;
			}
	}
	
	private void miseJourCptPrincipal(Integer numCategorie, IPersonne personne, String cVlan, EOEditingContext editingContext) {
		 EOQualifier qual = 
	            ERXQ.and(EOCompte.QUAL_CPT_VALIDE_OUI, EOCompte.PERS_ID.eq(personne.persId()));
	    NSArray<EOCompte> compteExistants = EOCompte.fetchAll(editingContext, qual, null);
	    NSArray<EOCompte> sorted = classerParPriorite(compteExistants);
	    
	    for (int i = 0; i < sorted.count(); i++) {
    		EOCompte compte = sorted.objectAtIndex(i);
//    		System.out.println("Compte sur le Vlan : " + compte.toVlans().cVlan());
        	if (0 == i) {
        		setCptPrincipal(compte, "O");
        	} else {
        		setCptPrincipal(compte, "N");
        	}
    	}
	    if (numCategorie != null && numCategorie != new Integer(12) && numCategorie != new Integer(7)) {
	    	for (int i = 0; i < sorted.count(); i++) {
	    		EOCompte compte = sorted.objectAtIndex(i);
	    		
	    		if (sorted.contains(cVlan)) {
					if (compte.toVlans().cVlan().equals(cVlan)) {
						setCptPrincipal(compte, "O");
					} else {
						setCptPrincipal(compte, "N");
					}
				}
	    	}
	    } 
	    if (numCategorie == new Integer(7)) {
	    	if (sorted.contains("R")) {
	    		for (int i = 0; i < sorted.count(); i++) {
	    			EOCompte compte = sorted.objectAtIndex(i);
	    		
	    			if (compte.toVlans().cVlan().equals(cVlan)) {
	    				setCptPrincipal(compte, "O");
	    			} else {
	    				setCptPrincipal(compte, "N");
	    			}
	    		}
	    	} else if (sorted.contains("E")) {
	    		for (int i = 0; i < sorted.count(); i++) {
	    			EOCompte compte = sorted.objectAtIndex(i);
	    		
	    			if (compte.toVlans().cVlan().equals("E")) {
	    				setCptPrincipal(compte, "O");
	    			} else {
	    				setCptPrincipal(compte, "N");
	    			}
	    		}
	    	}
	    } 
	}

}
