package org.cocktail.fwkcktlpersonne.server.util;

public interface PwdGenerator {
	String generate();
}
