package org.cocktail.fwkgspotguiajax.serveur;

import java.util.Iterator;

import com.webobjects.appserver.WOSession;
import com.webobjects.foundation.NSMutableArray;

public class UtilMessages {
	private int messageType;
	public final static int ERROR_MESSAGE = 0;
	public final static int INFO_MESSAGE = 1;
	public final static String ARRAY_MESSAGES_NAME = "ARRAY_MESSAGE_NAME";
	public final static String MESSAGE_FIELD = "message";
	public final static String MESSAGE_TYPE_FIELD = "messageType";
	private String message;
	private boolean isPersistant;

	public UtilMessages(int messageType, String message) {
		super();
		this.messageType = messageType;
		this.message = message;
		this.isPersistant = false;
	}

	public UtilMessages(int messageType, String message, boolean isPersistant) {
		super();
		this.messageType = messageType;
		this.message = message;
		this.isPersistant = isPersistant;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static void creatMessageUtil(WOSession sess, int messageType,
			String message, boolean isPersistant) {
		if (sess.objectForKey(ARRAY_MESSAGES_NAME) == null)
			sess.setObjectForKey(new NSMutableArray(), ARRAY_MESSAGES_NAME);
		NSMutableArray lstMsg = (NSMutableArray) sess
				.objectForKey(ARRAY_MESSAGES_NAME);
		lstMsg.add(new UtilMessages(messageType, message, isPersistant));
	}

	public static void creatMessageUtil(WOSession sess, int messageType,
			String message) {
		creatMessageUtil(sess, messageType, message, false);
	}

	public static void clearMessagesUtil(WOSession sess) {
		if (sess.objectForKey(ARRAY_MESSAGES_NAME) != null) {
			Iterator<UtilMessages> it = ((NSMutableArray) sess
					.objectForKey(ARRAY_MESSAGES_NAME)).iterator();
			NSMutableArray<UtilMessages> msgToDelete = new NSMutableArray<UtilMessages>();
			while (it.hasNext()) {
				UtilMessages mesg = (UtilMessages) it.next();
				if (!mesg.isPersistant)
					msgToDelete.add(mesg);
			}
			((NSMutableArray) sess.objectForKey(ARRAY_MESSAGES_NAME))
					.removeObjectsInArray(msgToDelete);
		}
	}

	public boolean isPersistant() {
		return isPersistant;
	}

	public void setPersistant(boolean isPersistant) {
		this.isPersistant = isPersistant;
	}

}
