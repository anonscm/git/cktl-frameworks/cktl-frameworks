package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.Iterator;

import org.cocktail.fwkgspot.serveur.finder.FinderLocal;
import org.cocktail.fwkgspot.serveur.finder.FinderSalle;
import org.cocktail.fwkgspot.serveur.finder.FinderVetageLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SalleTreeItem {
	/**
	 * Object metier de l'item
	 */
	protected EOGenericRecord embededObject;

	/**
	 * Objet metier parent de l'item
	 */
	protected SalleTreeItem parent;

	protected NSArray<SalleTreeItem> childs;

	public SalleTreeItem(EOGenericRecord eoObject, SalleTreeItem parent) {
		super();
		this.embededObject = eoObject;
		this.parent = parent;
	}

	public NSArray<SalleTreeItem> getChilds() {
		if (this.childs == null)
			setChilds(getChilds(null, false, false, false));
		return childs;
	}

	public NSArray<SalleTreeItem> getChilds(String Libelle, boolean batSearch,
			boolean porteSearch, boolean descSearch) {
		if (this.childs == null)
			setChilds(getChildsInt(Libelle, batSearch, porteSearch, descSearch));
		return childs;
	}

	private NSArray<SalleTreeItem> getChildsInt(String libelleSearch,
			boolean batSearch, boolean porteSearch, boolean etageSearch) {

		if (this.embededObject instanceof EOImplantationGeo) {
			if (libelleSearch != null) {

				EOQualifier qual = getQualifier(batSearch, porteSearch,
						etageSearch, libelleSearch, "", EOLocal.TO_SALLES_KEY
								+ ".");

				return buildSalleTreeItemArray(FinderLocal
						.getLocalsForImplantation(
								(EOImplantationGeo) this.embededObject,
								this.embededObject.editingContext(), qual),
						this);
			} else {
				return buildSalleTreeItemArray(FinderLocal
						.getLocalsForImplantation(
								(EOImplantationGeo) this.embededObject,
								this.embededObject.editingContext()), this);
			}
		}

		if (this.embededObject instanceof EOLocal) {
			if (libelleSearch != null) {

				EOQualifier qual = getQualifier(false, porteSearch,
						etageSearch, libelleSearch, "",
						EOVEtageLocal.TO_SALLES_KEY + ".");

				return buildSalleTreeItemArray(FinderVetageLocal
						.getEtageForLocal((EOLocal) this.embededObject,
								this.embededObject.editingContext(), qual),
						this);
			} else {
				return buildSalleTreeItemArray(FinderVetageLocal
						.getEtageForLocal((EOLocal) this.embededObject,
								this.embededObject.editingContext()), this);
			}
		}

		if (this.embededObject instanceof EOVEtageLocal) {
			if (libelleSearch != null) {

				EOQualifier qual = getQualifier(false, porteSearch,
						etageSearch, libelleSearch, "",
						EOTypeSalle.TO_V_TYPE_SALLE_LOCAL_ETAGES_KEY + "."
								+ EOVTypeSalleLocalEtage.TO_SALLES_KEY + ".");

				return buildSalleTreeItemArray(FinderVetageLocal
						.getTypeSalleForEtageAndLocal(
								((EOVEtageLocal) this.embededObject).toLocal(),
								((EOVEtageLocal) this.embededObject),
								((EOVEtageLocal) this.embededObject)
										.editingContext(), qual), this);
			} else {
				return buildSalleTreeItemArray(FinderVetageLocal
						.getTypeSalleForEtageAndLocal(
								((EOVEtageLocal) this.embededObject).toLocal(),
								((EOVEtageLocal) this.embededObject),
								((EOVEtageLocal) this.embededObject)
										.editingContext()), this);
			}
		}

		if (this.embededObject instanceof EOTypeSalle) {
			if (libelleSearch != null) {

				EOQualifier qual = getQualifier(false, porteSearch,
						etageSearch, libelleSearch, "", "");

				return buildSalleTreeItemArray(FinderSalle
						.getSallesForLocalEtageAndType(
								((EOVEtageLocal) this.parent.embededObject)
										.toLocal(),
								((EOTypeSalle) this.embededObject)
										.editingContext(), qual,
								((EOVEtageLocal) this.parent.embededObject),
								(EOTypeSalle) this.embededObject), this);
			} else {
				return buildSalleTreeItemArray(FinderSalle
						.getSallesForLocalEtageAndType(
								((EOVEtageLocal) this.parent.embededObject)
										.toLocal(),
								((EOTypeSalle) this.embededObject)
										.editingContext(),
								((EOVEtageLocal) this.parent.embededObject),
								(EOTypeSalle) this.embededObject), this);
			}
		}

		return null;
	}

	private EOQualifier getQualifier(boolean batSearch, boolean salleSearch,
			boolean etageSearch, String libelleSearch, String prefixToBat,
			String prefixToSalle) {
		String str = "";
		libelleSearch = libelleSearch.trim().replaceAll(" ", "").replaceAll(
				",", "*");
		NSMutableArray<String> keysQual = new NSMutableArray<String>();
		if (batSearch) {
			str = "(" + prefixToBat + EOLocal.APPELLATION_KEY
					+ " caseInsensitiveLike %s OR " + prefixToBat
					+ EOLocal.ADRESSE_INTERNE_KEY + " caseInsensitiveLike %s )";
			keysQual.addObjectsFromArray(new NSArray<String>(new String[] {
					"*" + libelleSearch + "*", "*" + libelleSearch + "*" }));
		}
		if (salleSearch) {
			if (!"".equals(str))
				str += " OR ";
			str += "(" + prefixToSalle + EOSalles.SAL_PORTE_KEY
					+ " caseInsensitiveLike %s OR " + prefixToSalle
					+ EOSalles.SAL_DESCRIPTIF_KEY + " caseInsensitiveLike %s )";

			keysQual.addObjectsFromArray(new NSArray<String>(new String[] {
					"*" + libelleSearch + "*", "*" + libelleSearch + "*" }));
		}
		if (etageSearch) {
			if (!"".equals(str))
				str += " OR ";
			str += "(" + prefixToSalle + EOSalles.SAL_ETAGE_KEY
					+ " caseInsensitiveLike %s )";

			keysQual.addObjectsFromArray(new NSArray<String>(new String[] { "*"
					+ libelleSearch + "*" }));

		}

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(str,
				keysQual);

		System.out.println(qual);
		return qual;
	}

	public String getLibelle() {
		if (this.embededObject instanceof EOImplantationGeo)
			return ((EOImplantationGeo) this.embededObject).llImplantationGeo();
		if (this.embededObject instanceof EOLocal)
			return ((EOLocal) this.embededObject).appellation();
		if (this.embededObject instanceof EOVEtageLocal)
			return "Etage : " + ((EOVEtageLocal) this.embededObject).salEtage();
		if (this.embededObject instanceof EOTypeSalle)
			return ((EOTypeSalle) this.embededObject).tsalLibelle();
		if (this.embededObject instanceof EOSalles)
			return ((EOSalles) this.embededObject).salPorte();
		return null;
	}

	public String getTitle() {
		if (this.embededObject instanceof EOLocal)
			return ((EOLocal) this.embededObject).adresseInterne();

		if (this.embededObject instanceof EOSalles)
			return ((EOSalles) this.embededObject).salDescriptif();
		return null;
	}

	public EOGenericRecord getEmbededObject() {
		return embededObject;
	}

	public void setEmbededObject(EOGenericRecord eoObject) {
		this.embededObject = eoObject;
	}

	public SalleTreeItem getParent() {
		return parent;
	}

	public void setParent(SalleTreeItem parent) {
		this.parent = parent;
	}

	public static <T extends EOGenericRecord> NSArray<SalleTreeItem> buildSalleTreeItemArray(
			NSArray<T> lst, SalleTreeItem parent) {
		NSMutableArray<SalleTreeItem> retour = new NSMutableArray<SalleTreeItem>();
		Iterator<T> it = lst.iterator();
		while (it.hasNext()) {
			EOGenericRecord eoObj = it.next();
			retour.add(new SalleTreeItem(eoObj, parent));
		}

		return retour;
	}

	public void setChilds(NSArray<SalleTreeItem> childs) {
		this.childs = childs;
	}

}
