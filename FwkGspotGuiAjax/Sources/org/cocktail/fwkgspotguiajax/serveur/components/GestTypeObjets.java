package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestTypeObjets extends BaseGestionRef {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestTypeObjets(WOContext context) {
		super(context);
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		if (getEditedType().toResaFamilleObjet() == null) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, "La famille est obligatoire !");
			return null;
		}
		if ((getEditedType().rtoLibelle() == null)
				|| ("".equals(getEditedType().rtoLibelle()))) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, "Le libellé est obligatoire !");
			return null;
		}
		if ((getEditedType().rtoCommentaire() != null)
				&& (getEditedType().rtoCommentaire().length() > 400))
			getEditedType().setRtoCommentaire(
					getEditedType().rtoCommentaire().substring(0, 400));

		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedType().rtoLibelle());
			setSearchFamille(null);
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Type objet créé ");
			setEditedObj(createNewType(getEditedType().toResaFamilleObjet()));
		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addType() {
		setEditedObj(createNewType(searchFamille()));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'un type d'objets");
		return null;
	}

	private EOResaTypeObjet createNewType(EOResaFamilleObjet famille) {
		EOEditingContext creatEc = new EOEditingContext(ec());
		EOResaTypeObjet type = EOResaTypeObjet.createFwkGspot_ResaTypeObjet(
				creatEc, "", (EOResaFamilleObjet) EOUtilities
						.localInstanceOfObject(creatEc, famille));
		return type;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOResaTypeObjet) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'un type d'objets");
		return null;
	}

	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if (searchFamille() != null) {
			qualStr = EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY + " = %@ ";
			qualArray.add(searchFamille());
		}

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr += EOResaTypeObjet.RTO_COMMENTAIRE_KEY
					+ " caseInsensitiveLike %@ OR "
					+ EOResaTypeObjet.RTO_LIBELLE_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
			qualArray.add(searchLib() + "*");

		}

		return EOResaTypeObjet
				.fetchFwkGspot_ResaTypeObjets(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] {
										new EOSortOrdering(
												EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY
														+ "."
														+ EOResaFamilleObjet.RFO_LIBELLE_KEY,
												EOSortOrdering.CompareCaseInsensitiveAscending),
										new EOSortOrdering(
												EOResaTypeObjet.RTO_LIBELLE_KEY,
												EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOResaTypeObjet getEditedType() {
		return (EOResaTypeObjet) getEditedObj();
	}

	private NSArray<EOResaFamilleObjet> lstFamilles;

	/**
	 * @return the lstFamilles
	 */
	public NSArray<EOResaFamilleObjet> lstFamilles() {
		if (lstFamilles == null)
			lstFamilles = EOResaFamilleObjet
					.fetchAllFwkGspot_ResaFamilleObjets(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOResaFamilleObjet.RFO_LIBELLE_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstFamilles;
	}

	/**
	 * @param lstFamilles
	 *            the lstFamilles to set
	 */
	public void setLstFamilles(NSArray<EOResaFamilleObjet> lstFamilles) {
		this.lstFamilles = lstFamilles;
	}

	private EOResaFamilleObjet fammilleOccur;

	/**
	 * @return the fammilleOccur
	 */
	public EOResaFamilleObjet fammilleOccur() {
		return fammilleOccur;
	}

	/**
	 * @param fammilleOccur
	 *            the fammilleOccur to set
	 */
	public void setFammilleOccur(EOResaFamilleObjet fammilleOccur) {
		this.fammilleOccur = fammilleOccur;
	}

	private EOResaFamilleObjet searchFamille;

	/**
	 * @return the searchFamille
	 */
	public EOResaFamilleObjet searchFamille() {
		return searchFamille;
	}

	/**
	 * @param searchFamille
	 *            the searchFamille to set
	 */
	public void setSearchFamille(EOResaFamilleObjet searchFamille) {
		this.searchFamille = searchFamille;
	}

}