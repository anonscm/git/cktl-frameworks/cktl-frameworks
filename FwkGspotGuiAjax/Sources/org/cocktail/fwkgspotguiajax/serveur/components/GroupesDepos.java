package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXStringUtilities;

public class GroupesDepos extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WODisplayGroup displayGroup;
	private EODepositaireSalles selectedGroupe;
	private NSArray<String> lstEditZones;
	private NSArray<String> lstAjoutZones;

	public GroupesDepos(WOContext context) {
		super(context);

	}

	public WOComponent editMethodeObject() {
		return this;
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			//displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshGroupes();
		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedGroupe((EODepositaireSalles) group.selectedObject());
		}
	}

	public EODepositaireSalles getSelectedGroupe() {
		return selectedGroupe;
	}

	public void setSelectedGroupe(EODepositaireSalles selectedGroupe) {
		this.selectedGroupe = selectedGroupe;
	}

	public NSArray<String> getLstEditZones() {
		if (lstEditZones == null) {
			if (parentIdToRefresh() != null)
				lstEditZones = new NSArray<String>(new String[] {
						auceditGroupeid(), auclstgroupesid(),
						parentIdToRefresh() });
			else
				lstEditZones = new NSArray<String>(new String[] {
						auceditGroupeid(), auclstgroupesid() });
		}
		return lstEditZones;
	}

	public NSArray<String> getLstAjoutZones() {
		if (lstAjoutZones == null) {
			if (parentIdToRefresh() != null)
				lstAjoutZones = new NSArray<String>(new String[] {
						aucaddgroupeid(), auclstgroupesid(),tbvsearchid(),
						parentIdToRefresh() });
			else
				lstAjoutZones = new NSArray<String>(new String[] {
						aucaddgroupeid(), tbvsearchid(),auclstgroupesid() });
		}
		return lstAjoutZones;
	}

	private String auceditGroupeid = ERXStringUtilities
			.safeIdentifierName("auceditGroupeid" + UUID.randomUUID());

	/**
	 * @return the auceditGroupeid
	 */
	public String auceditGroupeid() {
		return auceditGroupeid;
	}

	private String auclstgroupesid = ERXStringUtilities
			.safeIdentifierName("auclstgroupesid" + UUID.randomUUID());

	/**
	 * @return the auclstgroupesid
	 */
	public String auclstgroupesid() {
		return auclstgroupesid;
	}

	public WOActionResults refreshGroupes() {
		displayGroup.setObjectArray(selectedSalle().toDepositaireSalless());
		displayGroup.clearSelection();
		return null;
	}

	public boolean canEditGrps() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	public WOActionResults addDepos() {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddgroupeid());
		return null;
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	public WOActionResults closeAddGrp() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), camaddgroupeid());
		return null;
	}

	private String camaddgroupeid = ERXStringUtilities
			.safeIdentifierName("camaddgroupeid" + UUID.randomUUID());

	/**
	 * @return the camaddgroupeid
	 */
	public String camaddgroupeid() {
		return camaddgroupeid;
	}

	/**
	 * @param camaddgroupeid
	 *            the camaddgroupeid to set
	 */
	public void setCamaddgroupeid(String camaddgroupeid) {
		this.camaddgroupeid = camaddgroupeid;
	}

	public String ajaxGroupeSelectId() {
		return auceditGroupeid() + "_ajaxGroupeSelect";
	}

	private EOStructure selectedDeposGroupe;
	private NSDictionary<String, EOQualifier> userFiltersDictionary;

	/**
	 * @return the selectedDeposGroupe
	 */
	public EOStructure selectedDeposGroupe() {
		return selectedDeposGroupe;
	}

	/**
	 * @param selectedDeposGroupe
	 *            the selectedDeposGroupe to set
	 * @throws Exception
	 */
	public void setSelectedDeposGroupe(EOStructure selectedDeposGroupe)
			throws Exception {

		if (selectedDeposGroupe != null) {

			if (getGroupeDeposSalle().contains(
					EOStructureUlr.fetchRequiredFwkGspot_StructureUlr(ec(),
							EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY,
							selectedDeposGroupe))) {
				throw new Exception(
						"Le groupe "
								+ selectedDeposGroupe.persLibelle()
								+ " est déja dépositaire de la salle "
								+ selectedSalle().salPorte()
								+ ". Annuler la création de l'affectation, sélectionnez le groupe dans la liste et modifiez-le.");
			}

			this.selectedDeposGroupe = selectedDeposGroupe;
		}
	}

	public NSDictionary<String, EOQualifier> userFiltersDictionary() {
		if (userFiltersDictionary == null) {
			userFiltersDictionary = new NSMutableDictionary<String, EOQualifier>();
			userFiltersDictionary.takeValueForKey(
					FinderDepositaires.QUAL_GROUPE_DEPOS,
					"Groupes dépositaires");
		}
		return userFiltersDictionary;
	}

	public EOQualifier qualDepositaire() {
		return FinderDepositaires.QUAL_GROUPE_DEPOS;
	}

	private EOStructureUlr newGroupe;

	/**
	 * @return the newGroupe
	 */
	public EOStructureUlr newGroupe() {
		return newGroupe;
	}

	/**
	 * @param newGroupe
	 *            the newGroupe to set
	 */
	public void setNewGroupe(EOStructureUlr newGroupe) {
		this.newGroupe = newGroupe;
	}

	private String aucaddgroupeid = getComponentId() + "_aucaddgroupeid";

	/**
	 * @return the aucaddgroupeid
	 */
	public String aucaddgroupeid() {
		return aucaddgroupeid;
	}

	/**
	 * @param aucaddgroupeid
	 *            the aucaddgroupeid to set
	 */
	public void setAucaddgroupeid(String aucaddgroupeid) {
		this.aucaddgroupeid = aucaddgroupeid;
	}

	public WOActionResults validAjout() {
		if (newGroupe() != null) {

			// verif si il n'est pas déja dépositaire
			if (!getGroupeDeposSalle().contains(newGroupe())) {
				EODepositaireSalles newDepos = EODepositaireSalles
						.createFwkGspot_DepositaireSalles(ec(),
								new NSTimestamp(), new NSTimestamp(),
								selectedSalle().localInstanceIn(ec()), newGroupe().localInstanceIn(ec()));

			if (isCommitOnValid())
				ec().saveChanges();
				
			}
			//setNewGroupe(null);
			
			refreshGroupes();
		}
		return null;
	}

	public String aucTriggerEditMode = getComponentId()
			+ "_aucTriggerEditModeid";

	/**
	 * @return the aucTriggerEditMode
	 */
	public String aucTriggerEditMode() {
		return aucTriggerEditMode;
	}

	/**
	 * @param aucTriggerEditMode
	 *            the aucTriggerEditMode to set
	 */
	public void setAucTriggerEditMode(String aucTriggerEditMode) {
		this.aucTriggerEditMode = aucTriggerEditMode;
	}

	public String aucTriggerAjoutMode() {
		return getComponentId() + "_aucTriggerAjoutMode";
	}

	public NSArray<EOStructureUlr> getGroupeDeposSalle() {
		return ((NSArray<EOStructureUlr>) displayGroup().allObjects()
				.valueForKey(EODepositaireSalles.TO_STRUCTURE_ULR_KEY));
	}

	public String tbvsearchid() {
		return getComponentId()+"_tbvsearchid";
	}

	

}