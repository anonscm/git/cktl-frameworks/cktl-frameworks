package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.CktlAjaxTableView;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestampFormatter;

public abstract class GspotTbView extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String BINDING_updateContainerID = "updateContainerID";
	public static final String VALUE_WHEN_EMPTY_KEY = "emptyValue";
	private static final String NO_UPLETS_BIND = "noUpletsMessage";
	/**
	 * Bindings pour les colonnes a afficher,
	 */
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
	private NSMutableArray<EOGenericRecord> deletedObjects;
	protected NSArray<CktlAjaxTableViewColumn> colonnes;
	private NSMutableDictionary<Boolean, NSArray<CktlAjaxTableViewColumn>> colsForCanEdit;
	protected EOGenericRecord objectOccur, selectedObject;
	protected static final String OBJ_KEY = "objectOccur";

	public GspotTbView(WOContext context) {
		super(context);
		deletedObjects = new NSMutableArray<EOGenericRecord>();
		colsForCanEdit = new NSMutableDictionary<Boolean, NSArray<CktlAjaxTableViewColumn>>();
	}

	public abstract NSArray<String> DEFAULT_COLONNES_KEYS();

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		// if (colonnes == null) {
		if (colsForCanEdit.objectForKey(getCanEdit()) == null) {
			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				CktlAjaxTableViewColumn col = (CktlAjaxTableViewColumn) _colonnesMap.valueForKey((String) colkeys.objectAtIndex(i));
				// /*
				if (!"false".equals(col.valueForKey("visible"))) {
					// si la colonne est visible
					if ((("TableAction".equals(col.getComponent())) && (getCanEdit())) || (!"TableAction".equals(col.getComponent())))
						// si ce n'est pas une colonne Action ou si on a les droits
						res.addObject((CktlAjaxTableViewColumn) _colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));

				}
				// */
				// res.addObject((CktlAjaxTableViewColumn) _colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));

			}
			colsForCanEdit.setObjectForKey(res.immutableClone(), getCanEdit());
		}
		return colsForCanEdit.objectForKey(getCanEdit());
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS();
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}
		return keys;
	}

	public String getEmptyValue() {
		return " ";
	}

	public String getIdToRefreshEdit() {
		return (String) valueForBinding("idToRefreshEdit");
	}
	
	public String getIdToRefreshMove() {
		return (String) valueForBinding("idToRefreshMove");
	}

	public String getIdToRefreshDelete() {
		return (String) valueForBinding("idToRefreshDelete");
	}

	public String getOnSuccessEdit() {
		return (String) valueForBinding("onSuccessEdit");
	}
	
	public String getOnSuccessMove() {
		return (String) valueForBinding("onSuccessMove");
	}

	public Boolean isEditEnabled() {
		return (Boolean) valueForBinding("isEditEnabled");
	}

	public NSMutableArray<EOGenericRecord> getDeletedObjects() {
		return deletedObjects;
	}

	public void setDeletedObjects(NSMutableArray<EOGenericRecord> deletedObj) {
		this.deletedObjects = deletedObj;
	}
	
	/**
	 * Nom de la methode a invoquer pour l'édition d'un object
	 * 
	 * @return String methodeName
	 */
	public String getEditMethodName() {
		return (String) valueForBinding(TableAction.EDIT_METHODE_NAME_KEY);
	}

	public void setEditMethodName(String lst) {
		setValueForBinding(lst, TableAction.EDIT_METHODE_NAME_KEY);
	}

	/**
	 * WOComponent contenant la methode a invoquer pour l'édition d'un object
	 * 
	 * @return WOComponent methodeObject
	 */
	public WOComponent getEditMethodObject() {
		return (WOComponent) valueForBinding(TableAction.EDIT_METHODE_OBJECT_KEY);
	}

	public void setEditMethodObject(WOComponent act) {
		setValueForBinding(act, TableAction.EDIT_METHODE_OBJECT_KEY);
	}

	public Boolean getCanEdit() {
		return (Boolean) valueForBinding("canEdit");
	}
	
	/**
	 * Nom de la methode a invoquer pour le déplacement d'un object
	 * 
	 * @return String methodeName
	 */
	public String getMoveMethodName() {
		return (String) valueForBinding(TableAction.MOVE_METHODE_NAME_KEY);
	}

	public void setMoveMethodName(String lst) {
		setValueForBinding(lst, TableAction.MOVE_METHODE_NAME_KEY);
	}

	/**
	 * WOComponent contenant la methode a invoquer pour le déplacement d'un object
	 * 
	 * @return WOComponent methodeObject
	 */
	public WOComponent getMoveMethodObject() {
		return (WOComponent) valueForBinding(TableAction.MOVE_METHODE_OBJECT_KEY);
	}

	public void setMoveMethodObject(WOComponent act) {
		setValueForBinding(act, TableAction.MOVE_METHODE_OBJECT_KEY);
	}
	
	public Boolean getCanDelete() {
		if (hasBinding("canDelete"))
			return (Boolean) valueForBinding("canDelete");
		return Boolean.TRUE;
	}
	
	public Boolean getCanMove() {
		if (hasBinding("canMove"))
			return (Boolean) valueForBinding("canMove");
		return Boolean.FALSE;
	}

	public boolean showNavigBar() {
		if (valueForBinding("dg") != null) {
			WODisplayGroup dg = (WODisplayGroup) valueForBinding("dg");
			if (dg.allObjects().count() > dg.displayedObjects().count())
				return true;
		}
		return false;
	}

	public String classLigne() {
		if (isDeletedObject())
			return "deleted";
		if (isForbiddenObject())
			return "forbidden";
		return "";
	}

	public boolean isDeletedObject() {
		return getDeletedObjects().containsObject(objectOccur());
	}

	public boolean displayTable() {
		return ((dg() != null) && (dg().allObjects().count() > 0));

	}

	public WODisplayGroup dg() {
		return (WODisplayGroup) valueForBinding(CktlAjaxTableView.BDG_DISPLAY_GROUP);
	}

	public WOActionResults cancelSave() {
		getDeletedObjects().removeAllObjects();
		return null;
	}

	public abstract WOActionResults commitSave();

	public EOGenericRecord objectOccur() {
		if (hasBinding("objectOccur"))
			objectOccur = (EOGenericRecord) valueForBinding("objectOccur");
		return objectOccur;
	}

	public void setObjectOccur(EOGenericRecord objectOccur) {
		this.objectOccur = objectOccur;
		if ((canSetValueForBinding("objectOccur")))
			setValueForBinding(objectOccur, "objectOccur");
	}

	public EOGenericRecord selectedObject() {
		return selectedObject;
	}

	public void setSelectedObject(EOGenericRecord selectedObject) {
		this.selectedObject = selectedObject;
	}

	public NSArray<EOGenericRecord> forbiddenObject() {
		if (hasBinding("forbiddenObject"))
			return (NSArray<EOGenericRecord>) valueForBinding("forbiddenObject");
		return null;
	}

	public boolean isForbiddenObject() {
		return ((forbiddenObject() != null) && (forbiddenObject().containsObject(objectOccur())));
	}

	public boolean isCommitOnValid() {
		if (hasBinding("commitOnValid"))
			return (Boolean) valueForBinding("commitOnValid");
		return false;
	}

	public void setCommitOnValid(boolean commitOnValid) {
		if (canSetValueForBinding("commitOnValid"))
			setValueForBinding(commitOnValid, "commitOnValid");
	}

	public NSTimestampFormatter dateFormatter() {
		return new NSTimestampFormatter("%d/%m/%Y");
	}

	public String buzzyid() {
		return getComponentId() + "_buzzyid";
	}

	public String aucerreurtbvid() {
		return getComponentId() + "_aucerreurtbvid";
	}

	public String idmessageutiltbv() {
		return getComponentId() + "_idmessageutiltbv";
	}

	@SuppressWarnings("unchecked")
	public static CktlAjaxTableViewColumn getColonneAction() {
		CktlAjaxTableViewColumn colEdit = new CktlAjaxTableViewColumn();
		colEdit.setLibelle("Action");
		colEdit.setComponent("TableAction");
		CktlAjaxTableViewColumnAssociation assEdit = new CktlAjaxTableViewColumnAssociation(OBJ_KEY, "");
		assEdit.setObjectForKey("idToRefreshEdit", "idToRefreshEdit");
		assEdit.setObjectForKey("idToRefreshDelete", "idToRefreshDelete");
		assEdit.setObjectForKey("idToRefreshMove", "idToRefreshMove");
		assEdit.setObjectForKey("getDeletedObjects", TableAction.LST_DELETED_OBJECTS_KEY);
		assEdit.setObjectForKey(TableAction.EDIT_METHODE_NAME_KEY, TableAction.EDIT_METHODE_NAME_KEY);
		assEdit.setObjectForKey(TableAction.EDIT_METHODE_OBJECT_KEY, TableAction.EDIT_METHODE_OBJECT_KEY);
		assEdit.setObjectForKey("onSuccessEdit", "onSuccessEdit");
		assEdit.setObjectForKey("isEditEnabled", "isEditEnabled");
		assEdit.setObjectForKey("canEdit", "canEdit");
		assEdit.setObjectForKey("hyperlinkTarget", "hyperlinkTarget");
		assEdit.setObjectForKey("canDelete", "canDelete");
		assEdit.setObjectForKey("canMove", "canMove");
		assEdit.setObjectForKey(TableAction.MOVE_METHODE_NAME_KEY, TableAction.MOVE_METHODE_NAME_KEY);
		assEdit.setObjectForKey(TableAction.MOVE_METHODE_OBJECT_KEY, TableAction.MOVE_METHODE_OBJECT_KEY);
		assEdit.setObjectForKey("onSuccessMove", "onSuccessMove");
		colEdit.setAssociations(assEdit);
		colEdit.setHeaderCssStyle("width:64px;text-align:center;");
		colEdit.setRowCssStyle("text-align:center;background-image:none !important;");
		return colEdit;
	}

	public boolean haveDeletedObj() {
		return (getCanEdit() && (getDeletedObjects() != null) && (getDeletedObjects().count() > 0));
	}

	public String noUpletsMessage() {
		if (hasBinding(NO_UPLETS_BIND)) {
			return valueForBinding(NO_UPLETS_BIND) + "";
		}
		else {
			return "...";
		}

	}

}
