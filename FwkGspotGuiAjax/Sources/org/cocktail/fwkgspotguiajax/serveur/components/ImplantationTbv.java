package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ImplantationTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EOImplantationGeo.LC_IMPLANTATION_GEO_KEY;
	private static final String LIB_LONG = EOImplantationGeo.LL_IMPLANTATION_GEO_KEY;
	private static final String DEBUT = EOImplantationGeo.D_DEB_VAL_KEY;
	private static final String FIN = EOImplantationGeo.D_FIN_VAL_KEY;
    public ImplantationTbv(WOContext context) {
        super(context);
    }
    static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Lib court");
		col0.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, LIBELLE);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Libelle");
		col1.setOrderKeyPath(LIB_LONG);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIB_LONG, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, LIB_LONG);
		

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Début validitié");
		col2.setOrderKeyPath(DEBUT);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + DEBUT, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		ass2.setDateformat("%d/%m/%Y");
		col2.setAssociations(ass2);	
		col2.setHeaderCssStyle("width:80px;");
		col2.setRowCssStyle("text-align:center;");
		_colonnesMap.takeValueForKey(col2, DEBUT);
		

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Fin validitié");
		col3.setOrderKeyPath(FIN);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + FIN, "emptyValue");
		ass3.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		ass3.setDateformat("%d/%m/%Y"); 
		col3.setAssociations(ass3);		
		col3.setHeaderCssStyle("width:80px;");
		col3.setRowCssStyle("text-align:center;");
		_colonnesMap.takeValueForKey(col3, FIN);
		
		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Action");
		col6.setComponent("TableAction");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY, "");
		ass6.setObjectForKey("idToRefreshEdit", "idToRefreshEdit");
		ass6.setObjectForKey("idToRefreshDelete", "idToRefreshDelete");
		ass6.setObjectForKey("getDeletedObjects",
				TableAction.LST_DELETED_OBJECTS_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_NAME_KEY,
				TableAction.EDIT_METHODE_NAME_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_OBJECT_KEY,
				TableAction.EDIT_METHODE_OBJECT_KEY);
		ass6.setObjectForKey("onSuccessEdit", "onSuccessEdit");
		ass6.setObjectForKey("isEditEnabled", "isEditEnabled");
		ass6.setObjectForKey("canEdit", "canEdit");
		col6.setAssociations(ass6);
		col6.setHeaderCssStyle("width:45px;");
		col6
				.setRowCssStyle("text-align:center;background-image:none !important;");
		//_colonnesMap.takeValueForKey(col6, OBJ_KEY + ".action");
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(
				new String[] { LIBELLE,LIB_LONG,DEBUT,FIN, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			NSMutableArray<EOGenericRecord> deletedObj = new NSMutableArray<EOGenericRecord>();
			for (EOGenericRecord delDetail : getDeletedObjects()) {				
				if (((EOImplantationGeo) delDetail).toRepartBatImpGeos().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Implantation utilisée par un bâtiment, supression impossible");
					
				} else {
					delDetail.editingContext().deleteObject(delDetail);
					deletedObj.addObject(delDetail);
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
			getDeletedObjects().removeObjectsInArray(deletedObj);
		}

		return null;
	}
}