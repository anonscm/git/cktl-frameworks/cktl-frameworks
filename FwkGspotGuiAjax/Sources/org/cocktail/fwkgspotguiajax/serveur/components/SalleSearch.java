package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXStringUtilities;

public class SalleSearch extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String BINDING_ON_SELECT_SALLE = "onSelectSalle";
	private static final String BINDING_CLOSE_ON_SELECT = "closeOnSelect";
	private SallesTreeDelegate treeDelegate;

	private String treeZoneId = ERXStringUtilities.safeIdentifierName("tree"
			+ UUID.randomUUID());
	private boolean treeShowed = false;
	private EOGenericRecord selectedObj;

	public SalleSearch(WOContext context) {
		super(context);
	}

	public SallesTreeDelegate getTreeDelegate() {
		if (hasBinding("treeDelegate"))
			treeDelegate = (SallesTreeDelegate) valueForBinding("treeDelegate");

		if (treeDelegate.getEc() == null)
			treeDelegate.setEc(ec());

		return treeDelegate;
	}

	public void setTreeDelegate(SallesTreeDelegate treeDelegate) {
		if (canSetValueForBinding("treeDelegate"))
			setValueForBinding(treeDelegate, "treeDelegate");

		this.treeDelegate = treeDelegate;
	}

	/**
	 * @return the libelleSearch
	 */
	public String libelleSearch() {
		return getTreeDelegate().getLibelleSearch();
	}

	/**
	 * @param libelleSearch
	 *            the libelleSearch to set
	 */
	public void setLibelleSearch(String libelleSearch) {
		getTreeDelegate().setLibelleSearch(libelleSearch);
	}

	/**
	 * @return the treeZoneId
	 */
	public String treeZoneId() {
		return treeZoneId;
	}

	public WOActionResults showTree() {
		if (selImpl == null) {
			return null;
		}
		getTreeDelegate().setRootTree(new SalleTreeItem(selImpl, null));

		setTreeShowed(true);

		return null;
	}

	private NSArray<EOImplantationGeo> lstImplantations;

	/**
	 * @return the lstImplantations
	 */
	@SuppressWarnings("unchecked")
	public NSArray<EOImplantationGeo> lstImplantations() {
		if (hasBinding("lstImplantations"))
			lstImplantations = (NSArray<EOImplantationGeo>) valueForBinding("lstImplantations");
		if (lstImplantations == null)
			lstImplantations = EOImplantationGeo
					.fetchAllFwkGspot_ImplantationGeos(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOImplantationGeo.LC_IMPLANTATION_GEO_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstImplantations;
	}

	/**
	 * @param lstImplantations
	 *            the lstImplantations to set
	 */
	public void setLstImplantations(NSArray<EOImplantationGeo> lstImplantations) {
		if (canSetValueForBinding("lstImplantations"))
			setValueForBinding(lstImplantations, "lstImplantations");
		this.lstImplantations = lstImplantations;
	}

	private EOImplantationGeo implOccur;

	/**
	 * @return the implOccur
	 */
	public EOImplantationGeo implOccur() {
		return implOccur;
	}

	/**
	 * @param implOccur
	 *            the implOccur to set
	 */
	public void setImplOccur(EOImplantationGeo implOccur) {
		this.implOccur = implOccur;
	}

	private EOImplantationGeo selImpl;

	/**
	 * @return the selImpl
	 */
	public EOImplantationGeo selImpl() {
		return selImpl;
	}

	/**
	 * @param selImpl
	 *            the selImpl to set
	 */
	public void setSelImpl(EOImplantationGeo selImpl) {
		this.selImpl = selImpl;
	}

	/**
	 * @return the treeShowed
	 */
	public boolean treeShowed() {
		return treeShowed;
	}

	/**
	 * @param treeShowed
	 *            the treeShowed to set
	 */
	public void setTreeShowed(boolean treeShowed) {
		this.treeShowed = treeShowed;
	}

	public String idToRefresh() {
		if (hasBinding("idToRefresh"))
			return (String) valueForBinding("idToRefresh");
		return null;
	}

	public void setIdToRefresh(String idToRefresh) {
		if (canSetValueForBinding("idToRefresh"))
			setValueForBinding(idToRefresh, "idToRefresh");
	}

	public WOActionResults selectObj() {
		setSelectedObj(treeDelegate.getTreeItem().getEmbededObject());
		if (hasBinding(BINDING_ON_SELECT_SALLE))
			return (WOActionResults) valueForBinding(BINDING_ON_SELECT_SALLE);
		else
			return null;
	}

	public EOGenericRecord getSelectedObj() {
		if (hasBinding("selectedObj"))
			selectedObj = (EOGenericRecord) valueForBinding("selectedObj");
		return selectedObj;
	}

	public void setSelectedObj(EOGenericRecord selectedObj) {
		this.selectedObj = selectedObj;
		if (canSetValueForBinding("selectedObj"))
			setValueForBinding(selectedObj, "selectedObj");
	}

	public String functionOnClickWoLink() {
		if (hasBinding("functionOnClickWoLink"))
			return (String) valueForBinding("functionOnClickWoLink");
		return null;
	}

	public void setFunctionOnClickWoLink(String functionOnClickWoLink) {
		if (canSetValueForBinding("functionOnClickWoLink"))
			setValueForBinding(functionOnClickWoLink, "functionOnClickWoLink");
	}

	public String closeSearchJs() {
		return "function(close){ new Effect.BlindLeft('" + getDivSearchId()
				+ "');}";
	}

	public String getDivSearchId() {
		if (hasBinding("divSearchId"))
			return (String) valueForBinding("divSearchId");
		return null;
	}

	public String getDivShowSearchId() {
		if (hasBinding("divShowSearchId"))
			return (String) valueForBinding("divShowSearchId");
		return null;
	}

	private boolean visible = true;

	public boolean isVisible() {
		if (hasBinding("visible"))
			visible = (Boolean) valueForBinding("visible");
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		if (canSetValueForBinding("visible"))
			setValueForBinding(visible, "visible");
	}

	public WOActionResults hideSearch() {
		setVisible(false);
		return null;
	}

	public WOActionResults ras() {
		return null;
	}

	private NSArray<EOTypeSalle> lstTypesSalles;

	/**
	 * @return the lstTypesSalles
	 */
	public NSArray<EOTypeSalle> lstTypesSalles() {
		if (lstTypesSalles == null)
			lstTypesSalles = EOTypeSalle.fetchAllFwkGspot_TypeSalles(ec(),
					new NSArray<EOSortOrdering>(new EOSortOrdering(
							EOTypeSalle.TSAL_LIBELLE_KEY,
							EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstTypesSalles;
	}

	/**
	 * @param lstTypesSalles
	 *            the lstTypesSalles to set
	 */
	public void setLstTypesSalles(NSArray<EOTypeSalle> lstTypesSalles) {
		this.lstTypesSalles = lstTypesSalles;
	}

	private EOTypeSalle typeSalleOccur;

	/**
	 * @return the typeSalleOccur
	 */
	public EOTypeSalle typeSalleOccur() {
		return typeSalleOccur;
	}

	/**
	 * @param typeSalleOccur
	 *            the typeSalleOccur to set
	 */
	public void setTypeSalleOccur(EOTypeSalle typeSalleOccur) {
		this.typeSalleOccur = typeSalleOccur;
	}

	private EOTypeSalle selTypeSalle;

	/**
	 * @return the selTypeSalle
	 */
	public EOTypeSalle selTypeSalle() {
		return selTypeSalle;
	}

	/**
	 * @param selTypeSalle
	 *            the selTypeSalle to set
	 */
	public void setSelTypeSalle(EOTypeSalle selTypeSalle) {
		this.selTypeSalle = selTypeSalle;
	}

	/**
	 * @return the closeOnSelect
	 */
	public Boolean closeOnSelect() {
		return (valueForBinding(BINDING_CLOSE_ON_SELECT) != null ? (Boolean) valueForBinding(BINDING_CLOSE_ON_SELECT)
				: Boolean.FALSE);
	}

	/**
	 * @param closeOnSelect
	 *            the closeOnSelect to set
	 */
	public void setCloseOnSelect(Boolean closeOnSelect) {
		setValueForBinding((closeOnSelect!=null?closeOnSelect:Boolean.FALSE), BINDING_CLOSE_ON_SELECT);
	}

	public boolean showCloseOnSelect() {
		return (hasBinding(BINDING_CLOSE_ON_SELECT) && canSetValueForBinding(BINDING_CLOSE_ON_SELECT));
	}

}