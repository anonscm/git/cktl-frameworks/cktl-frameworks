package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOPrises;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class PriseTbView extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String TYPE_KEY = EOPrises.PRI_TYPE_LIB_KEY;
	private static final String CODE_KEY = EOPrises.PRI_CODE_KEY;
	
	
	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Type");
		col0.setOrderKeyPath(TYPE_KEY);		
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + TYPE_KEY, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		col0.setHeaderCssStyle("width:70px;");
		_colonnesMap.takeValueForKey(col0, TYPE_KEY);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Code prise");
		col1.setOrderKeyPath(CODE_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CODE_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		col1.setHeaderCssStyle("width:70px;");
		_colonnesMap.takeValueForKey(col1, CODE_KEY);		
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
	
    public PriseTbView(WOContext context) {
        super(context);
    }
    
    @Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { TYPE_KEY, CODE_KEY,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delPrise : getDeletedObjects()) {
				((EOPrises) delPrise)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EOPrises) delPrise).toSalles(),
								EOPrises.TO_SALLES_KEY);
	
				delPrise.editingContext().deleteObject(delPrise);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}