package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOPrises;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspotguiajax.serveur.components.SalleSearchDetail.DgDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class EquiSearchDetail extends CktlAjaxWOComponent  {
    public EquiSearchDetail(WOContext context) {
        super(context);
    }
    
    public static final String BIND_EC = "ec";
	public static final String BIND_SELECTED_SALLE = "selectedSalle";
	public static final String BIND_SELECTED_EQUIPEMENT = "selectedEquipement";
	public static final String BIND_UPDATE_CONTAINER_ID = "updateContainerID";
    
	public EOEditingContext ec() {
		if (valueForBinding(BIND_EC) != null)
			return (EOEditingContext) valueForBinding(BIND_EC);
		else
			return session().defaultEditingContext();

	}
	
	public void setSelectedSalle(EOSalles selectedSalle) {
		if (canSetValueForBinding(BIND_SELECTED_SALLE)) {
			setValueForBinding(selectedSalle, BIND_SELECTED_SALLE);
		}
		if (hasBinding(BIND_UPDATE_CONTAINER_ID)) {
			AjaxUpdateContainer.updateContainerWithID((String) valueForBinding(BIND_UPDATE_CONTAINER_ID), context());
		}

	}

	public EOSalles getSelectedSalle() {
		return (EOSalles) valueForBinding(BIND_SELECTED_SALLE);
	}
	
	public boolean selectionSalleEnabled() {
		return (hasBinding(BIND_SELECTED_SALLE) && canSetValueForBinding(BIND_SELECTED_SALLE));
	}

	
	public void setSelectedEquipement(EOResaObjet selectedEquipement) {
		if (canSetValueForBinding(BIND_SELECTED_EQUIPEMENT)) {
			setValueForBinding(selectedEquipement, BIND_SELECTED_EQUIPEMENT);
		}
	}

	public EOResaObjet getSelectedEquipement() {
		return (EOResaObjet) valueForBinding(BIND_SELECTED_EQUIPEMENT);
	}
	

	private EOResaFamilleObjet searchFamille;

	/**
	 * @return the searchFamille
	 */
	public EOResaFamilleObjet searchFamille() {
		return searchFamille;
	}

	/**
	 * @param searchFamille
	 *            the searchFamille to set
	 */
	public void setSearchFamille(EOResaFamilleObjet searchFamille) {
		this.searchFamille = searchFamille;
	}

	private EOResaTypeObjet searchType;

	/**
	 * @return the searchType
	 */
	public EOResaTypeObjet searchType() {
		return searchType;
	}

	/**
	 * @param searchType
	 *            the searchType to set
	 */
	public void setSearchType(EOResaTypeObjet searchType) {
		this.searchType = searchType;
	}

	
	private NSArray<EOResaFamilleObjet> lstFamilles;

	/**
	 * @return the lstFamilles
	 */
	public NSArray<EOResaFamilleObjet> lstFamilles() {
		if (lstFamilles == null)
			lstFamilles = EOResaFamilleObjet
					.fetchAllFwkGspot_ResaFamilleObjets(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOResaFamilleObjet.RFO_LIBELLE_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstFamilles;
	}

	/**
	 * @param lstFamilles
	 *            the lstFamilles to set
	 */
	public void setLstFamilles(NSArray<EOResaFamilleObjet> lstFamilles) {
		this.lstFamilles = lstFamilles;
	}

	private EOResaFamilleObjet fammilleOccur;

	/**
	 * @return the fammilleOccur
	 */
	public EOResaFamilleObjet fammilleOccur() {
		return fammilleOccur;
	}

	/**
	 * @param fammilleOccur
	 *            the fammilleOccur to set
	 */
	public void setFammilleOccur(EOResaFamilleObjet fammilleOccur) {
		this.fammilleOccur = fammilleOccur;
	}

	private EOResaFamilleObjet selectedFamille;

	/**
	 * @return the selectedFamille
	 */
	public EOResaFamilleObjet selectedFamille() {
		return selectedFamille;
	}

	/**
	 * @param selectedFamille
	 *            the selectedFamille to set
	 */
	public void setSelectedFamille(EOResaFamilleObjet selectedFamille) {
		this.selectedFamille = selectedFamille;
	}

	public NSArray<EOResaTypeObjet> lstTypeObj() {
		if (selectedFamille() != null)
			return selectedFamille().toResaTypeObjets();
		return null;
	}

	public NSArray<EOResaTypeObjet> lstTypeObjSearch() {
		if (searchFamille() != null)
			return searchFamille().toResaTypeObjets();
		return null;
	}

	private EOResaTypeObjet typeOccur;

	/**
	 * @return the typeOccur
	 */
	public EOResaTypeObjet typeOccur() {
		return typeOccur;
	}

	/**
	 * @param typeOccur
	 *            the typeOccur to set
	 */
	public void setTypeOccur(EOResaTypeObjet typeOccur) {
		this.typeOccur = typeOccur;
	}
	
	private NSArray<String> lstColonnesKeys;

	/**
	 * @return the lstColonnesKeys
	 */
	public NSArray<String> lstColonnesKeys() {
		if (lstColonnesKeys == null)
			lstColonnesKeys = new NSArray<String>(new String[] {
					ObjetTbv.SALLE,
					ObjetTbv.FAMILLE, ObjetTbv.TYPE, ObjetTbv.LIBELLE1, ObjetTbv.LIBELLE2,
					ObjetTbv.CODE_BARRE, ObjetTbv.RESERVABLE });
		return lstColonnesKeys;
	}
	
	private WODisplayGroup displayGroup;

	public WODisplayGroup displayGroup() {

		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setDelegate(new DgDelegate());
			displayGroup.setNumberOfObjectsPerBatch(25);
		}
		
		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			EOResaObjet obj =(EOResaObjet) group.selectedObject();
			setSelectedEquipement(obj);
			if (obj!=null) setSelectedSalle(obj.toSalles()); else setSelectedSalle(null);
		}
	}

	public WOActionResults searchObj() {
		displayGroup().setObjectArray(getLstEquipement());
		displayGroup().clearSelection();
		displayGroup().updateDisplayedObjects();
		displayGroup().setCurrentBatchIndex(1);
		haveSearched = true;
		return null;
	}
	
	public NSArray<EOResaObjet> getLstEquipement() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		EOQualifier qualObj = null;
		if (searchFamille() != null)
			qualObj = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.TO_RESA_TYPE_OBJET_KEY + "."
							+ EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY
							+ " =%@ ", new NSArray<EOResaFamilleObjet>(
							searchFamille()));
		
		if (searchType() != null) {
			EOQualifier qualType = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.TO_RESA_TYPE_OBJET_KEY + " =%@ ",
					new NSArray<EOResaTypeObjet>(searchType()));
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualType }));
			else
				qualObj = qualType;
		}
		
		if (libelleEquipement!=null) {
			EOQualifier qualLibelleEquipement = ERXQ.contains(EOResaObjet.RO_LIBELLE1_KEY, libelleEquipement);
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualLibelleEquipement}));
			else
				qualObj = qualLibelleEquipement;
			
		}
		
		if (codeBarre!=null) {
			EOQualifier qualCodebarre = ERXQ.likeInsensitive(EOResaObjet.RO_CODE_BARRE_KEY,codeBarre);
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualCodebarre }));
			else
				qualObj = qualCodebarre;
		}
		
		if (reservableBool) {
			EOQualifier qualRes = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.RO_RESERVABLE_KEY + " =%@ ",
					new NSArray<String>("O"));
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualRes }));
			else
				qualObj = qualRes;
		}
		return EOResaObjet.fetchFwkGspot_ResaObjets(ec(),qualObj,null);
	}

	
	public String auccriteresid() {
		return getComponentId() + "_auccriteresid";
	}
	public String aucequipementid() {
		return getComponentId() + "_aucequipementid";
	}
	
	public String buzzyid() {
		return getComponentId() + "_buzzyid";
	}
	
	public WOActionResults resetSearch() {
		setCodeBarre(null);
		setFammilleOccur(null);
		setTypeOccur(null);
		setReservableBool(Boolean.FALSE);
		setLibelleEquipement(null);
		return null;
	}
	
	private String libelleEquipement;
	
	
	public String getLibelleEquipement() {
		return libelleEquipement;
	}

	public void setLibelleEquipement(String libelleEquipement) {
		this.libelleEquipement = libelleEquipement;
	}

	private String codeBarre;
	
	public String getCodeBarre() {
		return codeBarre;
	}

	public void setCodeBarre(String codeBarre) {
		this.codeBarre = codeBarre;
	}

	private Boolean haveSearched = Boolean.FALSE;
	private Boolean reservableBool;

	public Boolean getReservableBool() {
		return reservableBool;
	}

	public void setReservableBool(Boolean reservableBool) {
		this.reservableBool = reservableBool;
	}

	public String noUpletsMessage() {
		if (haveSearched) {
			return "Recherche vide";
		}
		else {
			return "Saisissez des critères de recherche et cliquez sur \"Chercher\"";
		}

	}

	
}