package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class TypeObjTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EOResaTypeObjet.RTO_LIBELLE_KEY;
	private static final String COMMENT = EOResaTypeObjet.RTO_COMMENTAIRE_KEY;
	private static final String FAMILLE = EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY+"."+EOResaFamilleObjet.RFO_LIBELLE_KEY;
    public TypeObjTbv(WOContext context) {
        super(context);
    }
    static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Libelle");
		col0.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col0, LIBELLE);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Commentaire");
		col1.setOrderKeyPath(COMMENT);
		col1.setComponent("CommentZoneTbv");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COMMENT, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		
		ass1.setObjectForKey("30", "cols");
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, COMMENT);
		
		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Famille");
		col2.setOrderKeyPath(FAMILLE);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + FAMILLE, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setAssociations(ass2);
		col2.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col2, FAMILLE);

		
		
		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Action");
		col6.setComponent("TableAction");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY, "");
		ass6.setObjectForKey("idToRefreshEdit", "idToRefreshEdit");
		ass6.setObjectForKey("idToRefreshDelete", "idToRefreshDelete");
		ass6.setObjectForKey("getDeletedObjects",
				TableAction.LST_DELETED_OBJECTS_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_NAME_KEY,
				TableAction.EDIT_METHODE_NAME_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_OBJECT_KEY,
				TableAction.EDIT_METHODE_OBJECT_KEY);
		ass6.setObjectForKey("onSuccessEdit", "onSuccessEdit");
		ass6.setObjectForKey("isEditEnabled", "isEditEnabled");
		ass6.setObjectForKey("canEdit", "canEdit");
		col6.setAssociations(ass6);
		col6.setHeaderCssStyle("width:45px;");
		col6
				.setRowCssStyle("text-align:center;background-image:none !important;");
		//_colonnesMap.takeValueForKey(col6, OBJ_KEY + ".action");
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(
				new String[] { FAMILLE, LIBELLE,COMMENT, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			NSMutableArray<EOGenericRecord> deletedObj = new NSMutableArray<EOGenericRecord>();
			for (EOGenericRecord delDetail : getDeletedObjects()) {				
				if (((EOResaTypeObjet) delDetail).toResaObjets().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Type utilisée par des objets, supression impossible");
				} else {
					delDetail.editingContext().deleteObject(delDetail);
					deletedObj.addObject(delDetail);
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
			getDeletedObjects().removeObjectsInArray(deletedObj);
		}

		return null;
	}
}