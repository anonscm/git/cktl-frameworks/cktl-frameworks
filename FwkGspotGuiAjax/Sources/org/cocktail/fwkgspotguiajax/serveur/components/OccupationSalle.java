package org.cocktail.fwkgspotguiajax.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.ajax.AjaxUtils;

public class OccupationSalle extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WODisplayGroup displayGroup;

	public OccupationSalle(WOContext context) {
		super(context);
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshOccups();
		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {

			setSelectedOccup((EODetailPourcentage) group.selectedObject());
		}
	}

	public boolean canEditOccup() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	public String auclstoccupid() {
		if (hasBinding("auclstoccupid"))
			return (String) valueForBinding("auclstoccupid");
		return getComponentId() + "_auclstoccupid2";
	}

	public WOActionResults refreshOccups() {
		displayGroup.setObjectArray(selectedSalle().toDetailPourcentages(
				null,
				new NSArray<EOSortOrdering>(new EOSortOrdering(
						EODetailPourcentage.DET_POURCENTAGE_KEY,
						EOSortOrdering.CompareDescending)), true));
		displayGroup.clearSelection();
		if (canSetValueForBinding(DeposIndiv.NEED_LOCAL_REFRESH)) {
			setValueForBinding(Boolean.TRUE, DeposIndiv.NEED_LOCAL_REFRESH);
		}
		return null;

	}

	public String camaddoccupid() {
		return getComponentId() + "_camaddoccupid";
	}

	public WOActionResults addOccup() {
		setSelectedOccup(null);
		setServiceOccup(null);
		setSelectedTypeOccup(null);
		setPourcentageOccup(null);
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddoccupid());
		return null;
	}

	public WOActionResults editOccup(Object ligne) {

		setSelectedOccup((EODetailPourcentage) ligne);
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddoccupid(),
				"Modification d'une occupation");
		return null;
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	private EODetailPourcentage selectedOccup;

	public EODetailPourcentage getSelectedOccup() {

		return selectedOccup;
	}

	public void setSelectedOccup(EODetailPourcentage selectedOccup) {
		this.selectedOccup = selectedOccup;

	}

	public String auceditoccupid() {
		return getComponentId() + "_auceditoccupid";
	}

	private String editionWinTitle = "Ajout d'une occupation";

	/**
	 * @return the editionWinTitle
	 */
	public String editionWinTitle() {
		return editionWinTitle;
	}

	/**
	 * @param editionWinTitle
	 *            the editionWinTitle to set
	 */
	public void setEditionWinTitle(String editionWinTitle) {
		this.editionWinTitle = editionWinTitle;
	}

	private NSArray<EOTypeOccupation> lstTypesOccupations;

	/**
	 * @return the lstTypesOccupations
	 */
	public NSArray<EOTypeOccupation> lstTypesOccupations() {
		if (lstTypesOccupations == null)
			lstTypesOccupations = EOTypeOccupation
					.fetchAllFwkGspot_TypeOccupations(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering[] { new EOSortOrdering(
											EOTypeOccupation.TOC_LIBELLE_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending) }));
		return lstTypesOccupations;
	}

	/**
	 * @param lstTypesOccupations
	 *            the lstTypesOccupations to set
	 */
	public void setLstTypesOccupations(
			NSArray<EOTypeOccupation> lstTypesOccupations) {
		this.lstTypesOccupations = lstTypesOccupations;
	}

	private EOTypeOccupation typeOccupOccur;

	/**
	 * @return the typeOccupOccur
	 */
	public EOTypeOccupation typeOccupOccur() {
		return typeOccupOccur;
	}

	/**
	 * @param typeOccupOccur
	 *            the typeOccupOccur to set
	 */
	public void setTypeOccupOccur(EOTypeOccupation typeOccupOccur) {
		this.typeOccupOccur = typeOccupOccur;
	}

	public WOActionResults closeEditOccup() {
		setWindowVisible(false);
		ec().revert();
		// AjaxUpdateContainer.updateContainerWithID(auceditoccupid(),
		// context());
		CktlAjaxWindow.close(context(), camaddoccupid());
		return null;
	}

	private EOStructureUlr serviceOccup;

	/**
	 * @return the serviceOccup
	 */
	public EOStructureUlr serviceOccup() {
		if (selectedOccup != null)
			serviceOccup = selectedOccup.toStructureUlr();
		return serviceOccup;
	}

	/**
	 * @param serviceOccup
	 *            the serviceOccup to set
	 */
	public void setServiceOccup(EOStructureUlr serviceOccup) {
		this.serviceOccup = serviceOccup;
	}

	private EOTypeOccupation typeOccupOccup;

	/**
	 * @return the typeOccupOccup
	 */
	public EOTypeOccupation typeOccupOccup() {
		if (selectedOccup != null)
			typeOccupOccup = selectedOccup.toTypeOccupation();
		return typeOccupOccup;
	}

	/**
	 * @param typeOccupOccup
	 *            the typeOccupOccup to set
	 */
	public void setTypeOccupOccup(EOTypeOccupation typeOccupOccup) {
		this.typeOccupOccup = typeOccupOccup;
	}

	private BigDecimal pourcentageOccup;

	/**
	 * @return the pourcentageOccup
	 */
	public BigDecimal pourcentageOccup() {
		if (selectedOccup != null)
			pourcentageOccup = selectedOccup.detPourcentage();
		return pourcentageOccup;
	}

	/**
	 * @param pourcentageOccup
	 *            the pourcentageOccup to set
	 */
	public void setPourcentageOccup(BigDecimal pourcentageOccup) {
		this.pourcentageOccup = pourcentageOccup;
	}

	public WOActionResults validEdition() {

		BigDecimal pct = pourcentageOccup;
		if (selectedOccup == null) {
			// creation
			if (serviceOccup() == null) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"Le service est obligatoire !!");

				return null;
			}

			if (selectedTypeOccup() == null) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"Le type occupation est obligatoire !!");

				return null;
			}

			if (pct == null) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"Le pourcentage est obligatoire !!");

				return null;
			}

			for (EODetailPourcentage detPct : selectedSalle()
					.toDetailPourcentages()) {
				if ((detPct.toStructureUlr().equals(serviceOccup()))
						&& (detPct.toTypeOccupation()
								.equals(selectedTypeOccup()))) {
					UtilMessages.creatMessageUtil(session(),
							UtilMessages.ERROR_MESSAGE,
							"Cette occupation existe déjà !!");

					return null;
				}
			}

			setSelectedOccup(EODetailPourcentage
					.createFwkGspot_DetailPourcentage(ec(), selectedSalle(),
							serviceOccup(), selectedTypeOccup));
		}
		if (pct == null) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE,
					"Le pourcentage est obligatoire !!");

			return null;
		}

		selectedOccup.setDetPourcentage(pct);

		if (isCommitOnValid())
			ec().saveChanges();

		setWindowVisible(false);
		refreshOccups();
		AjaxUpdateContainer.updateContainerWithID(aucTriggerEditMode(),
				context());
		// setIdToValidOnUpdate("updateAfterValid();CAMD.close('"+
		// camaddoccupid() + "_win');");

		CktlAjaxWindow.close(context(), camaddoccupid());
		return null;
	}

	public String aucmessageid() {
		return getComponentId() + "_aucmessageid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	public String aucTriggerEditMode() {
		return getComponentId() + "_aucTriggerEditMode";
	}

	private NSArray<String> lstEditZones;

	public NSArray<String> getLstEditZones() {
		if (lstEditZones == null) {
			if (parentIdToRefresh() != null)
				lstEditZones = new NSArray<String>(
						new String[] { auceditoccupid(), auclstoccupid(),
								parentIdToRefresh() });
			else
				lstEditZones = new NSArray<String>(new String[] {
						auceditoccupid(), auclstoccupid() });
		}
		return lstEditZones;
	}

	public WOComponent editMethodeObject() {
		return this;
	}

	private EODetailPourcentage occupOccur;

	/**
	 * @return the occupOccur
	 */
	public EODetailPourcentage occupOccur() {
		return occupOccur;
	}

	/**
	 * @param occupOccur
	 *            the occupOccur to set
	 */
	public void setOccupOccur(EODetailPourcentage occupOccur) {
		this.occupOccur = occupOccur;
	}

	public String camservicesearchid() {
		return getComponentId() + "_camservicesearchid";
	}

	private boolean isInSearch = false;

	/**
	 * @return the isInSearch
	 */
	public boolean isInSearch() {
		return isInSearch;
	}

	/**
	 * @param isInSearch
	 *            the isInSearch to set
	 */
	public void setInSearch(boolean isInSearch) {
		this.isInSearch = isInSearch;
	}

	/**
	 * @return the getServicesSalle
	 */
	@SuppressWarnings("unchecked")
	public NSArray<EOStructureUlr> getServicesSalle() {
		return (NSArray<EOStructureUlr>) selectedSalle().toDetailPourcentages()
				.valueForKey(EODetailPourcentage.TO_STRUCTURE_ULR_KEY);
	}

	public String tbvsearchid() {
		return getComponentId() + "_tbvsearchid";
	}

	public WOActionResults searchOn() {
		setInSearch(true);
		
		// CktlAjaxWindow.open(context(), camservicesearchid());
		AjaxModalDialog.open(context(), camservicesearchid());
		return null;
	}

	public WOActionResults closeSearch() {
		setInSearch(false);
		// CktlAjaxWindow.close(context(), camservicesearchid());
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(auceditoccupid(), context());
		return null;
	}
	
	public WOActionResults selectService(){
		setServiceOccup(searchServiceOccup());
		return closeSearch();
	}

	public String aucsearchserviceid() {
		return getComponentId() + "_aucsearchserviceid";
	}

	public String aucserviceoccupid() {
		return getComponentId() + "_aucserviceoccupid";
	}

	private EOTypeOccupation selectedTypeOccup;

	/**
	 * @return the selectedTypeOccup
	 */
	public EOTypeOccupation selectedTypeOccup() {
		return selectedTypeOccup;
	}

	/**
	 * @param selectedTypeOccup
	 *            the selectedTypeOccup to set
	 */
	public void setSelectedTypeOccup(EOTypeOccupation selectedTypeOccup) {
		this.selectedTypeOccup = selectedTypeOccup;
	}

	public String aucselservice() {
		return getComponentId() + "_aucselservice";
	}

	public String aucTriggerSearchid() {
		return getComponentId() + "_aucTriggerSearchid";
	}

	private NSArray<String> lstSearchZones = new NSArray<String>(new String[] {
			aucserviceoccupid(), aucselservice() });
	private EOStructureUlr searchServiceOccup;;

	/**
	 * @return the lstSearchZones
	 */
	public NSArray<String> lstSearchZones() {
		return lstSearchZones;
	}

	/**
	 * @param lstSearchZones
	 *            the lstSearchZones to set
	 */
	public void setLstSearchZones(NSArray<String> lstSearchZones) {
		this.lstSearchZones = lstSearchZones;
	}

	public String aucallwinsid() {
		return getComponentId() + "_aucallwinsid";
	}

	public String closeSearchScript() {
		return "function(close){CAMD.close('" + camservicesearchid()
				+ "_win');}";
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		// Chargement à la main des js et css pour la modal box pour éviter des
		// bug sur ouverture d'une modal dans un CktlAjaxWindow
		super.appendToResponse(response, context);
		AjaxUtils.addStylesheetResourceInHead(context, response, "modalbox.css");
	}

	/**
	 * @return the searchServiceOccup
	 */
	public EOStructureUlr searchServiceOccup() {
		return searchServiceOccup;
	}

	/**
	 * @param searchServiceOccup the searchServiceOccup to set
	 */
	public void setSearchServiceOccup(EOStructureUlr searchServiceOccup) {
		this.searchServiceOccup = searchServiceOccup;
	}

}