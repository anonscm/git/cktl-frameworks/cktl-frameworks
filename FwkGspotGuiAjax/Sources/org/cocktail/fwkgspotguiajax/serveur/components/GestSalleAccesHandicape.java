package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class GestSalleAccesHandicape extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WODisplayGroup displayGroup;
	private NSArray<String> lstEditZones;

	public GestSalleAccesHandicape(WOContext context) {
		super(context);
	}

	public String auclstaccesid() {
		return getComponentId() + "_auclstaccesid";
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshAcces();
		return displayGroup;
	}

	public WOActionResults refreshAcces() {
		displayGroup.setObjectArray(selectedSalle()
				.toRepartSalleAccesHandicapes());
		displayGroup.clearSelection();
		return null;
	}

	public boolean canEditAcces() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	public WOActionResults addAcces() {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddaccesid());

		return null;
	}

	public String camaddaccesid() {

		return getComponentId() + "_camaddaccesid";
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	public String auceditaccesid() {
		return getComponentId() + "_auceditaccesid";
	}

	public String aucaddaccesid() {
		return getComponentId() + "_aucaddaccesid";
	}

	private EOTypeAccesHandicape newAcces;

	@SuppressWarnings("unchecked")
	public WOActionResults validAjout() {
		if (newAcces != null) {

			// verif si il n'est pas déja pour la salle
			if (((NSArray<EOTypeAccesHandicape>) selectedSalle()
					.toRepartSalleAccesHandicapes()
					.valueForKey(
							EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY))
					.contains(newAcces)) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"L'accès "+newAcces.libelle()+" est déjà dans la salle !!");
				AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
				return null;

			}
			EORepartSalleTypeAccesHandicape
					.createFwkGspot_RepartSalleAccesHandicape(ec(),
							selectedSalle(), newAcces);

			if (isCommitOnValid()) {
				ec().saveChanges();

			}
			refreshAcces();
			AjaxUpdateContainer.updateContainerWithID( aucTriggerEditMode(), context());
			
		}
		return closeAddAcces();
	}

	public EOTypeAccesHandicape getNewAcces() {
		return newAcces;
	}

	public void setNewAcces(EOTypeAccesHandicape newAcces) {
		this.newAcces = newAcces;
	}

	public NSArray<String> getLstEditZones() {
		if (lstEditZones == null) {
			if (parentIdToRefresh() != null)
				lstEditZones = new NSArray<String>(
						new String[] { auceditaccesid(), auclstaccesid(),
								parentIdToRefresh() });
			else
				lstEditZones = new NSArray<String>(new String[] {
						auceditaccesid(), auclstaccesid() });
		}
		return lstEditZones;
	}

	public String aucTriggerEditMode() {
		return getComponentId() + "_aucTriggerEditModeid";
	}

	public WOActionResults closeAddAcces() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), camaddaccesid());
		return null;
	}

	private NSArray<EOTypeAccesHandicape> lstAcces;

	/**
	 * @return the lstAcces
	 */
	public NSArray<EOTypeAccesHandicape> lstAcces() {
		if (lstAcces == null)
			lstAcces = EOTypeAccesHandicape
					.fetchAllFwkGspot_TypeAccesHandicapes(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering[] { new EOSortOrdering(
											EOTypeAccesHandicape.LIBELLE_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending) }));
		return lstAcces;
	}

	/**
	 * @param lstAcces
	 *            the lstAcces to set
	 */
	public void setLstAcces(NSArray<EOTypeAccesHandicape> lstAcces) {
		this.lstAcces = lstAcces;
	}

	private EOTypeAccesHandicape accesOccur;

	/**
	 * @return the accesOccur
	 */
	public EOTypeAccesHandicape accesOccur() {
		return accesOccur;
	}

	/**
	 * @param accesOccur
	 *            the accesOccur to set
	 */
	public void setAccesOccur(EOTypeAccesHandicape accesOccur) {
		this.accesOccur = accesOccur;
	}
	
	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

}