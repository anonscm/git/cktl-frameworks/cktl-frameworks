package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class Portes extends GspotBaseComponent {
    public Portes(WOContext context) {
        super(context);
    }
    public String aucLstPortesid() {
		if (hasBinding("lstPortesid"))
			return (String) valueForBinding("lstPortesid");
		return getComponentId() + "_aucLstPortesid";
	}
    
    public boolean canEditPortes() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	private WODisplayGroup displayGroup;
	private EOSallePorte editedPorte;

	public EOSallePorte getEditedPorte() {
		return editedPorte;
	}
	public void setEditedPorte(EOSallePorte editedPorte) {
		this.editedPorte = editedPorte;
	}
	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshPortes();
		return displayGroup;
	}

	public WOActionResults refreshPortes() {		
		displayGroup.setObjectArray(selectedSalle()
				.toSallePortes(
						null,
						new NSArray<EOSortOrdering>(new EOSortOrdering(
								EOSallePorte.SAL_PORTE_KEY,
								EOSortOrdering.CompareAscending)), true));
		displayGroup.clearSelection();
		return null;
	}
	
	public String idTbvPortes() {
		return getComponentId() + "_idTbvPortes";
	}
	
	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}
	
	public WOActionResults validAjout() {
		try {
			editedPorte.validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
			return null;
		}
		if (isCommitOnValid()) {
			editedPorte.editingContext().saveChanges();
			if (editedPorte.editingContext().parentObjectStore()
					.equals(session().defaultEditingContext())) {
				session().defaultEditingContext().saveChanges();
			}
		}
		refreshPortes();
		AjaxUpdateContainer.updateContainerWithID(aucLstPortesid(), context());
		if (parentIdToRefresh() != null)
			AjaxUpdateContainer.updateContainerWithID(parentIdToRefresh(),
					context());
		closeAddPortes();

		return null;
	}

	public WOActionResults editPorte(Object porte) {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddportesid(),
				"Modification d'une porte");
		editedPorte = (EOSallePorte) porte;
		return null;
	}

	public WOActionResults closeAddPortes() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), camaddportesid());
		return null;
	}
	
	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}
	
	public String camaddportesid() {
		return getComponentId() + "_camaddporteid";
	}
	
	public String auceditportesid() {
		return getComponentId() + "_auceditportesid";
	}
	public WOActionResults addPorte() {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddportesid());
		EOEditingContext ec = new EOEditingContext(session()
				.defaultEditingContext());
		editedPorte = EOSallePorte.createFwkGspot_SallePorte(ec,
				selectedSalle.localInstanceIn(ec));
		return null;
	}

}