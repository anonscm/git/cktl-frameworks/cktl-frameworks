package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOPrises;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;

public class Prises extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Prises(WOContext context) {
		super(context);
	}

	public String aucLstPrisesid() {
		if (hasBinding("lstPrisesid"))
			return (String) valueForBinding("lstPrisesid");
		return getComponentId() + "_aucLstPrisesid";
	}

	public boolean canEditPrises() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	private WODisplayGroup displayGroup;

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshPrises();
		return displayGroup;
	}

	public WOActionResults refreshPrises() {
		EOQualifier qual = null;
		if ((priTypeSearch() != null) && (!"TOUT".equals(priTypeSearch()))) {
			qual = EOQualifier.qualifierWithQualifierFormat(
					EOPrises.PRI_TYPE_KEY + "=%s", new NSArray<String>(
							priTypeSearch()));
		}
		displayGroup.setObjectArray(selectedSalle()
				.toPriseses(
						qual,
						new NSArray<EOSortOrdering>(new EOSortOrdering(
								EOPrises.PRI_TYPE_KEY,
								EOSortOrdering.CompareAscending)), true));
		displayGroup.clearSelection();
		return null;
	}

	public WOActionResults addPrise() {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddpriseid());
		EOEditingContext ec = new EOEditingContext(session()
				.defaultEditingContext());
		editedPrise = EOPrises.createFwkGspot_Prises(ec,
				selectedSalle.localInstanceIn(ec));
		return null;
	}

	private EOPrises editedPrise;

	public EOPrises getEditedPrise() {
		return editedPrise;
	}

	public void setEditedPrise(EOPrises editedPrise) {
		this.editedPrise = editedPrise;
	}

	private String typeOccur;
	private String priTypeSearch = "TOUT";

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	public String auceditprisesid() {
		return getComponentId() + "_auceditprisesid";
	}

	public String camaddpriseid() {
		return getComponentId() + "_camaddpriseid";
	}

	public NSArray<String> lstTypes() {
		return EOPrises.lstTypePrise.allKeys();
	}

	public NSArray<String> lstTypesSearch() {
		NSMutableArray<String> retour = new NSMutableArray<String>("TOUT");
		retour.addObjectsFromArray(EOPrises.lstTypePrise.allKeys());
		return retour;
	}

	/**
	 * @return the typeOccur
	 */
	public String typeOccur() {
		return typeOccur;
	}

	/**
	 * @param typeOccur
	 *            the typeOccur to set
	 */
	public void setTypeOccur(String typeOccur) {
		this.typeOccur = typeOccur;
	}

	public String libType() {
		if ("TOUT".equals(typeOccur)) {
			return "Tous";
		}
		return EOPrises.lstTypePrise.objectForKey(typeOccur);
	}

	public WOActionResults validAjout() {
		try {
			editedPrise.validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
			return null;
		}
		if (isCommitOnValid()) {
			editedPrise.editingContext().saveChanges();
			if (editedPrise.editingContext().parentObjectStore()
					.equals(session().defaultEditingContext())) {
				session().defaultEditingContext().saveChanges();
			}
		}
		refreshPrises();
		AjaxUpdateContainer.updateContainerWithID(aucLstPrisesid(), context());
		if (parentIdToRefresh() != null)
			AjaxUpdateContainer.updateContainerWithID(parentIdToRefresh(),
					context());
		closeAddPrises();

		return null;
	}

	public WOActionResults editPrise(Object prise) {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddpriseid(),
				"Modification d'une prise");
		editedPrise = (EOPrises) prise;
		return null;
	}

	public WOActionResults closeAddPrises() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), camaddpriseid());
		return null;
	}

	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}

	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	/**
	 * @return the priTypeSearch
	 */
	public String priTypeSearch() {
		if (priTypeSearch == null) {
			priTypeSearch = "TOUT";
		}
		return priTypeSearch;
	}

	/**
	 * @param priTypeSearch
	 *            the priTypeSearch to set
	 */
	public void setPriTypeSearch(String priTypeSearch) {
		this.priTypeSearch = priTypeSearch;
	}

	public String idTbvPrises() {
		return getComponentId() + "_idTbvPrises";
	}
}