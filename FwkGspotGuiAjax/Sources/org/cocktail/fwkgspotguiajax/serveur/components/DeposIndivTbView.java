package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.StructureTableView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.foundation.ERXStringUtilities;

public class DeposIndivTbView extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public EOIndividu unRow;
	private EOIndividu unIndividu, selectedIndividu;
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";
	/**
	 * Bindings pour les colonnes a afficher, {@link StructureTableView#DEFAULT_COLONNES_KEYS}
	 */
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	public static final String COL_NUMERO_KEY = "toPersonneElt" + "." + IPersonne.NUMERO_KEY;
	public static final String COL_NOM_KEY = "toPersonneElt" + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	public static final String COL_SERVICE_KEY = EORepartAssociation.TO_STRUCTURE_KEY + "." + EOStructure.LC_STRUCTURE_KEY;

	static {

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Numéro");
		col1.setOrderKeyPath(COL_NUMERO_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + COL_NUMERO_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setHeaderCssStyle("width:80px;");
		_colonnesMap.takeValueForKey(col1, COL_NUMERO_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Nom");
		col2.setOrderKeyPath(COL_NOM_KEY);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + COL_NOM_KEY, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_NOM_KEY);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Service");
		col3.setOrderKeyPath(COL_SERVICE_KEY);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + COL_SERVICE_KEY, "emptyValue");
		ass3.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col3.setAssociations(ass3);
		_colonnesMap.takeValueForKey(col3, COL_SERVICE_KEY);

		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");

	}

	public DeposIndivTbView(WOContext context) {
		super(context);
	}

	public NSTimestampFormatter dateFormatter() {
		return new NSTimestampFormatter("%d/%m/%Y");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { COL_NUMERO_KEY, COL_NOM_KEY, COL_SERVICE_KEY, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject().editingContext();
			for (EOGenericRecord delAsso : getDeletedObjects()) {
				((EORepartAssociation) delAsso).toPersonneElt().supprimerUnRole(ec, ((EORepartAssociation) delAsso).toAssociation(),
						((EORepartAssociation) delAsso).toStructure(), getUtilisateurPersId(), null);
			}
			if (isCommitOnValid()) {
				ec.saveChanges();
			}
		}

		return cancelSave();
	}

	/**
	 * @return la valeur du binding utilisateurPersId.
	 */
	public Integer getUtilisateurPersId() {
		if (valueForBinding(BINDING_utilisateurPersId) == null) {
			System.err.println("**** Le binding utilisateurPersId n'est pas renseigné pour le composant " + name());
		}
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}

	private String buzzyid = ERXStringUtilities.safeIdentifierName("buzzyid" + UUID.randomUUID());

	/**
	 * @return the buzzyid
	 */
	public String buzzyid() {
		return buzzyid;
	}

}