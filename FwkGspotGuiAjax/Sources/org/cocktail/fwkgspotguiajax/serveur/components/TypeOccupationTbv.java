package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class TypeOccupationTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EOTypeOccupation.TOC_LIBELLE_KEY;
    public TypeOccupationTbv(WOContext context) {
        super(context);
    }
    static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Lib court");
		col0.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, LIBELLE);
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(
				new String[] { LIBELLE, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			NSMutableArray<EOGenericRecord> deletedObj = new NSMutableArray<EOGenericRecord>();
			for (EOGenericRecord delDetail : getDeletedObjects()) {
				if (((EOTypeOccupation) delDetail).toDetailPourcentages().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Type occupation utilisé par des salles, supression impossible");
				} else {
					delDetail.editingContext().deleteObject(delDetail);
					deletedObj.addObject(delDetail);
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}