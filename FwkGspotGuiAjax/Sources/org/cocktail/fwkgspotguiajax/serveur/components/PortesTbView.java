package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class PortesTbView extends GspotTbView {
	private static final long serialVersionUID = 1L;

	private static final String CB_KEY = EOSallePorte.SAL_CB_KEY;
	private static final String CODE_KEY = EOSallePorte.SAL_PORTE_KEY;
	private static final String SALLE_KEY = EOSallePorte.TO_SALLES_KEY+"."+EOSalles.SAL_PORTE_KEY;;
	
	
	static {
		CktlAjaxTableViewColumn colSal = new CktlAjaxTableViewColumn();
		colSal.setLibelle("Salle");
		colSal.setOrderKeyPath(SALLE_KEY);		
		CktlAjaxTableViewColumnAssociation assSal = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + SALLE_KEY, "emptyValue");
		assSal.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colSal.setAssociations(assSal);
		colSal.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(colSal, SALLE_KEY);
		
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Code porte");
		col0.setOrderKeyPath(CODE_KEY);		
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CODE_KEY, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setHeaderCssStyle("width:100px;");
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, CODE_KEY);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Code barre");
		col1.setOrderKeyPath(CB_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CB_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		col1.setHeaderCssStyle("width:100px;");
		_colonnesMap.takeValueForKey(col1, CB_KEY);		
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
    public PortesTbView(WOContext context) {
        super(context);
    }
    @Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { SALLE_KEY, CODE_KEY,CB_KEY,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delTel : getDeletedObjects()) {
				((EOSallePorte) delTel)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EOSallePorte) delTel).toSalles(),
								EOSallePorte.TO_SALLES_KEY);
	
				delTel.editingContext().deleteObject(delTel);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}