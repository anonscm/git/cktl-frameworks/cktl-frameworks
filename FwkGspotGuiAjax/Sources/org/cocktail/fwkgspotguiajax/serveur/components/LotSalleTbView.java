package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class LotSalleTbView extends GspotTbView {
	private static final long serialVersionUID = 1L;

	private static final String COL_LIBELLE_KEY = EOLotSalle.LOT_LIBELLE_KEY;
	private static final String COL_NOM_KEY = ERXQ.keyPath(EOLotSalle.TO_FWKPERS__INDIVIDU_KEY,EOIndividu.NOM_PRENOM_AFFICHAGE_KEY);
	
	static {
		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Libellé lot");
		col1.setOrderKeyPath(COL_LIBELLE_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COL_LIBELLE_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, COL_LIBELLE_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();		
		col2.setOrderKeyPath(COL_NOM_KEY);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COL_NOM_KEY, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setAssociations(ass2);
		col2.setRowCssStyle("text-align:left;padding-left:3px;");
		col2.setLibelle("Créateur lot");
		_colonnesMap.takeValueForKey(col2, "col_"+COL_NOM_KEY);
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
    public LotSalleTbView(WOContext context) {
        super(context);
    }
    @Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { COL_LIBELLE_KEY, "col_"+COL_NOM_KEY,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			boolean haveErr=false;
			for (EOGenericRecord delOccup : getDeletedObjects()) {
				haveErr=false;
				if (((EOLotSalle) delOccup).toRepartLotIndividus().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Il y a des individus associés au lot "+((EOLotSalle) delOccup).lotLibelle()+", supression impossible");
					haveErr=true;
				}
				
				if (((EOLotSalle) delOccup).toRepartLotSalles().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Il y a des salles associées au lot "+((EOLotSalle) delOccup).lotLibelle()+", supression impossible");
					haveErr=true;
				}
				
				if (!haveErr)  {
					delOccup.editingContext().deleteObject(delOccup);
				}else {
					return null;
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
	
	
}