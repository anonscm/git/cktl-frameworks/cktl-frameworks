package org.cocktail.fwkgspotguiajax.serveur.components;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSMutableArray;

public class TableAction extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String LST_DELETED_OBJECTS_KEY="lstDeletedObjects";
	public final static String EDIT_METHODE_NAME_KEY="editMethodName";
	public final static String EDIT_METHODE_OBJECT_KEY="editMethodObject";
	public final static String VALUE_KEY="value";
	public final static String CAN_DELETE_KEY="canDelete";
	public final static String CAN_MOVE_KEY="canMove";
	public final static String MOVE_METHODE_NAME_KEY="moveMethodName";
	public final static String MOVE_METHODE_OBJECT_KEY="moveMethodObject";
	
    public TableAction(WOContext context) {
        super(context);
    }
	
    /**
	 * @return the isDeleted
	 */
	public boolean isDeleted() {
		return ((lstDeletedObjects()!=null) && (lstDeletedObjects().contains(value())));
	}

	public NSMutableArray<Object> lstDeletedObjects(){
		return (NSMutableArray<Object>) valueForBinding(LST_DELETED_OBJECTS_KEY);
	}
	
	public void setLstDeletedObjects(NSMutableArray<Object> lst){
		setValueForBinding(lst, LST_DELETED_OBJECTS_KEY);
	}
	
	public Object value(){		
		return (Object) valueForBinding("value");
	}
	
	public void setValue(Object value){
		setValueForBinding(value, "value");
	}
	
	public WOActionResults restore() {
		lstDeletedObjects().removeObject(value());
		return null;
	}
	
	public WOActionResults edit() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		return  (WOActionResults)invokeMethod(getEditMethodObject(), getEditMethodName(), new Object[]{value()},new Class[]{Object.class});	
	}
	
	public WOActionResults move() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		return  (WOActionResults)invokeMethod(getMoveMethodObject(), getMoveMethodName(), new Object[]{value()},new Class[]{Object.class});	
	}
	
	public WOActionResults delete() {
		lstDeletedObjects().addObject(value());
		return null;
	}
	
	public String getEditMethodName() {
		return (String) valueForBinding(EDIT_METHODE_NAME_KEY);
	}


	public void setEditMethodName(String editMethodName) {
		setValueForBinding(editMethodName,EDIT_METHODE_NAME_KEY);
	}
	
	public String getMoveMethodName() {
		return (String) valueForBinding(MOVE_METHODE_NAME_KEY);
	}


	public void setMoveMethodName(String moveMethodName) {
		setValueForBinding(moveMethodName,MOVE_METHODE_NAME_KEY);
	}

	public String getHyperlinkTarget(){
		return (String) valueForBinding("hyperlinkTarget");
	}
	public void setHyperlinkTarget(String HyperlinkTarget) {
		setValueForBinding(HyperlinkTarget,"hyperlinkTarget");
	}

	public WOComponent getEditMethodObject() {
		return (WOComponent) valueForBinding(EDIT_METHODE_OBJECT_KEY);
	}


	public void setEditMethodObject(WOComponent editMethodObject) {
		setValueForBinding(editMethodObject,EDIT_METHODE_OBJECT_KEY);
	}
	

	public WOComponent getMoveMethodObject() {
		return (WOComponent) valueForBinding(MOVE_METHODE_OBJECT_KEY);
	}


	public void setMoveMethodObject(WOComponent moveMethodObject) {
		setValueForBinding(moveMethodObject,MOVE_METHODE_OBJECT_KEY);
	}

	/**
	 * Invoke une methode "methodename" de l'objet "object" avec les arguments
	 * "args"
	 * 
	 * @param object
	 * @param methodName
	 * @param args
	 * @param argTypes
	 * @return
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static Object invokeMethod(Object object, String methodName,
			Object[] args, Class[] argTypes) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Object result = null;

		Class cls = object.getClass();
		Method method = cls.getMethod(methodName, argTypes);
		result = method.invoke(object, args);

		return result;
	}

	public Boolean canDelete() {
		if (hasBinding(CAN_DELETE_KEY))
			return (java.lang.Boolean) valueForBinding(CAN_DELETE_KEY);
		return Boolean.TRUE;
	}
	
	public Boolean canMove() {
		if (hasBinding(CAN_MOVE_KEY))
			return (java.lang.Boolean) valueForBinding(CAN_MOVE_KEY);
		return Boolean.FALSE;
	}
	

	public void setCanDelete(Boolean canDelete) {
		if (canSetValueForBinding(CAN_DELETE_KEY))
			setValueForBinding(canDelete, CAN_DELETE_KEY);
	}
	
	public void setCanMove(Boolean canMove) {
		if (canSetValueForBinding(CAN_MOVE_KEY))
			setValueForBinding(canMove, CAN_MOVE_KEY);
	}

		public Boolean getUseStdHyperlink(){		
			return !(hasBinding("idToRefreshEdit")&&valueForBinding("idToRefreshEdit")!=null);
		}

		@Override
		public boolean isStateless() {
			
			return true;
		}
	

	
}