package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;

public class CommentZoneTbv extends CktlAjaxWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String VALUE_KEY="value";
            
    public CommentZoneTbv(WOContext context) {
        super(context);
    }
    public Object value(){		
		return (Object) valueForBinding(VALUE_KEY);
	}
	
	public void setValue(Object value){
		setValueForBinding(value, VALUE_KEY);
	}
	
	public Integer nbCommentRows(){
		if (hasBinding("rows")) {
			return Integer.decode(valueForBinding("rows")+"");
		}
		int occur = 0;		
		if (value()!=null) {
			Matcher matcher = Pattern.compile("\n").matcher(value()+"");		    
		    while(matcher.find()) {
		        occur ++;
		    }		    
		}
		if ((occur>0)&&(occur<3)){
			occur = 3;
		}
		 if (occur>10) {
			 
			 occur=10;
		 }
			
		return occur+1;	
	}
	
	
	
}