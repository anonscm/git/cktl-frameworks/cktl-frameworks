package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class SalleAccesHandTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY
			+ "." + EOTypeAccesHandicape.LIBELLE_KEY;

	public SalleAccesHandTbv(WOContext context) {
		super(context);
	}

	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Libelle");
		col0.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, LIBELLE);

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Action");
		col6.setComponent("TableAction");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY, "");
		ass6.setObjectForKey("idToRefreshEdit", "idToRefreshEdit");
		ass6.setObjectForKey("idToRefreshDelete", "idToRefreshDelete");
		ass6.setObjectForKey("getDeletedObjects",
				TableAction.LST_DELETED_OBJECTS_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_NAME_KEY,
				TableAction.EDIT_METHODE_NAME_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_OBJECT_KEY,
				TableAction.EDIT_METHODE_OBJECT_KEY);
		ass6.setObjectForKey("onSuccessEdit", "onSuccessEdit");
		ass6.setObjectForKey("isEditEnabled", "isEditEnabled");
		ass6.setObjectForKey("canEdit", "canEdit");
		col6.setAssociations(ass6);
		col6.setHeaderCssStyle("width:45px;");
		col6
				.setRowCssStyle("text-align:center;background-image:none !important;");
		//_colonnesMap.takeValueForKey(col6, OBJ_KEY + ".action");
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(
				new String[] { LIBELLE, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delDetail : getDeletedObjects()) {
				((EORepartSalleTypeAccesHandicape) delDetail)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EORepartSalleTypeAccesHandicape) delDetail)
										.toFwkGspot_TypeAccesHandicape(),
								EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY);
				((EORepartSalleTypeAccesHandicape) delDetail)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EORepartSalleTypeAccesHandicape) delDetail)
										.toFwkGspot_Salles(),
								EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__SALLES_KEY);
				delDetail.editingContext().deleteObject(delDetail);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}