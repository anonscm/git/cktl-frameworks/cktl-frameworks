package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class ObjetTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String LIBELLE1 = EOResaObjet.RO_LIBELLE1_KEY;
	public static final String LIBELLE2 = EOResaObjet.RO_LIBELLE2_KEY;
	public static final String ACCES = EOResaObjet.RO_ACCES_KEY;
	public static final String CODE_BARRE = EOResaObjet.RO_CODE_BARRE_KEY;
	public static final String RESERVABLE = EOResaObjet.RO_RESERVABLE_KEY;
	public static final String TYPE = EOResaObjet.TO_RESA_TYPE_OBJET_KEY + "."
			+ EOResaTypeObjet.RTO_LIBELLE_KEY;
	public static final String FAMILLE = EOResaObjet.TO_RESA_TYPE_OBJET_KEY
			+ "." + EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY + "."
			+ EOResaFamilleObjet.RFO_LIBELLE_KEY;
	public static final String SALLE = EOResaObjet.TO_SALLES_KEY + "."
			+ EOSalles.SAL_PORTE_KEY;
	

	public ObjetTbv(WOContext context) {
		super(context);
	}

	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Type objet");
		col0.setOrderKeyPath(TYPE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + TYPE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0
				.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col0, TYPE);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Accès");
		col1.setOrderKeyPath(ACCES);
		col1.setComponent("CommentZoneTbv");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + ACCES, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		//ass1.setObjectForKey("1", "rows");
		ass1.setObjectForKey("30", "cols");
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, ACCES);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Libelle");
		col2.setOrderKeyPath(LIBELLE1);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE1, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setAssociations(ass2);
		col2
				.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col2, LIBELLE1);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Commentaire");
		col3.setOrderKeyPath(LIBELLE2);
		col3.setComponent("CommentZoneTbv");
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE2, "emptyValue");
		ass3.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		//ass3.setObjectForKey("1", "rows");
		ass3.setObjectForKey("25", "cols");
		col3.setAssociations(ass3);
		col3.setHeaderCssStyle("width:150px;");
		col3.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col3, LIBELLE2);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Réservable");
		col4.setOrderKeyPath(RESERVABLE);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + RESERVABLE, "emptyValue");
		ass4.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col4.setAssociations(ass4);
		col4.setHeaderCssStyle("width:70px;text-align:center;");
		col4.setRowCssStyle("text-align:center;vertical-align:top;");
		_colonnesMap.takeValueForKey(col4, RESERVABLE);

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Salle");
		col5.setOrderKeyPath(SALLE);
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + SALLE, "emptyValue");
		ass5.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col5.setAssociations(ass5);
		col5
				.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col5, SALLE);

		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");

		CktlAjaxTableViewColumn col7 = new CktlAjaxTableViewColumn();
		col7.setLibelle("Famille");
		col7.setOrderKeyPath(FAMILLE);
		CktlAjaxTableViewColumnAssociation ass7 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + FAMILLE, "emptyValue");
		ass7.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col7.setAssociations(ass7);
		col7
				.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col7, FAMILLE);
		
		CktlAjaxTableViewColumn col8 = new CktlAjaxTableViewColumn();
		col8.setLibelle("Code Barre");
		col8.setOrderKeyPath(CODE_BARRE);
		CktlAjaxTableViewColumnAssociation ass8 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CODE_BARRE, "emptyValue");
		ass8.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		ass8.setObjectForKey("16", "length");
		col8.setAssociations(ass8);
		col8.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col8, CODE_BARRE);
		
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		if (hasBinding("defaultColonnesKeys"))
			return (NSArray<String>) valueForBinding("defaultColonnesKeys");
		return new NSArray<String>(new String[] { TYPE, LIBELLE1, LIBELLE2, 
				CODE_BARRE, RESERVABLE, ACCES, SALLE, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delDetail : getDeletedObjects()) {
				((EOResaObjet) delDetail)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EOResaObjet) delDetail).toSalles(),
								EOResaObjet.TO_SALLES_KEY);
				delDetail.editingContext().deleteObject(delDetail);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}

}