package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class ServiceSearch extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NUMBER_PER_BATCH_KEY = "numberOfObjectsPerBatch";

	public ServiceSearch(WOContext context) {
		super(context);
	}

	private String libSearch;

	/**
	 * @return the libSearch
	 */
	public String libSearch() {
		return libSearch;
	}

	/**
	 * @param libSearch
	 *            the libSearch to set
	 */
	public void setLibSearch(String libSearch) {
		this.libSearch = libSearch;
	}

	public WOActionResults performSearch() {
		EOQualifier fQual = null;
		if ((libSearch != null) && (!"".equals(libSearch.trim()))) {
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("("
					+ EOStructureUlr.LL_STRUCTURE_KEY
					+ " caseInsensitiveLike %s OR "
					+ EOStructureUlr.LC_STRUCTURE_KEY
					+ " caseInsensitiveLike %s )",
					new NSArray<String>(new String[] { "*" + libSearch + "*",
							"*" + libSearch + "*" }));
			fQual = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qual,
							EOStructureUlr.QUAL_GROUPES_SERVICES }));
		} else
			fQual = EOStructureUlr.QUAL_GROUPES_SERVICES;

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOStructureUlr.ENTITY_NAME,
				fQual,
				new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(
								EOStructureUlr.LC_STRUCTURE_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);

		displayGroup.setObjectArray(ec().objectsWithFetchSpecification(
				fetchSpec));
		displayGroup.clearSelection();
		
		return null;
	}
	
	public WOActionResults searchAction(){
		performSearch();
		setSelectedGroupe(null);
		if (hasBinding("updateContainerID")){
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		}

		return null;
	}

	private WODisplayGroup displayGroup;

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setDelegate(new DgDelegate());
			if ((hasBinding(NUMBER_PER_BATCH_KEY))&&(valueForBinding(NUMBER_PER_BATCH_KEY)!=null)){
				displayGroup.setNumberOfObjectsPerBatch((Integer) valueForBinding(NUMBER_PER_BATCH_KEY));
			}
			performSearch();
		}

		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedGroupe((EOStructureUlr) group.selectedObject());
		}
	}

	public EOStructureUlr getSelectedGroupe() {
		return (EOStructureUlr) valueForBinding("selectedGroupe");
	}

	public void setSelectedGroupe(EOStructureUlr selectedGroupe) {
		setValueForBinding(selectedGroupe, "selectedGroupe");
	}

	private NSArray<String> lstMajZones;

	/**
	 * @return the aucListid
	 */
	public String aucListid() {
		if (hasBinding("tbvsearchid"))
			return (String) valueForBinding("tbvsearchid");
		return getComponentId() + "_aucListid";
	}

	public String AucTriggerMaj() {
		return getComponentId() + "_AucTriggerMaj";
	}

	public NSArray<String> getLstMajZones() {
		if (lstMajZones == null) {
			if (updateContainerID() != null)
				lstMajZones = new NSArray<String>(new String[] { aucListid(),
						updateContainerID() });
			else
				lstMajZones = new NSArray<String>(new String[] { aucListid() });
		}
		return lstMajZones;
	}

	public String updateContainerID() {
		return (String) valueForBinding("updateContainerID");
	}

	public void setUpdateContainerID(String updateContainerID) {
		setValueForBinding(updateContainerID, "updateContainerID");
	}

	
}