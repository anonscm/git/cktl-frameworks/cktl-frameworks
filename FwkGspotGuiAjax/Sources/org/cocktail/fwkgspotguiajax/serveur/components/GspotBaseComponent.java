package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkgspot.serveur.GspotUser;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

public class GspotBaseComponent extends CktlAjaxWOComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GspotBaseComponent(WOContext context) {
		super(context);
	}

	public EOEditingContext ec() {
		if (hasBinding("ec"))
			return (EOEditingContext) valueForBinding("ec");
		return null;
	}

	protected EOSalles selectedSalle;

	public EOSalles selectedSalle() {
		if (hasBinding("selectedSalle")) {			
			selectedSalle = (EOSalles) valueForBinding("selectedSalle");
		}
		return selectedSalle;
	}

	public void setSelectedSalle(EOSalles selectedSalle) {
	
		this.selectedSalle = selectedSalle;
		if (canSetValueForBinding("selectedSalle"))
			setValueForBinding(selectedSalle, "selectedSalle");

	}

	public Integer getUtilisateurPersId() {
		if (valueForBinding("utilisateurPersId") == null) {
			System.err
					.println("**** Le binding utilisateurPersId n'est pas renseigné pour le composant "
							+ name());
		}
		return (Integer) valueForBinding("utilisateurPersId");
	}

	public Boolean isReadOnly() {
		if (!hasBinding("isReadOnly")) {
			return Boolean.TRUE;
		}
		return (Boolean) valueForBinding("isReadOnly");
	}

	public void setIsReadOnly(Boolean value) {
		setValueForBinding(value, "isReadOnly");
	}

	public Boolean isEditing() {
		if (hasBinding("isEditing") == false
				|| isReadOnly().booleanValue() == true) {
			return Boolean.FALSE;
		}
		return (Boolean) valueForBinding("isEditing");
	}

	public void setIsEditing(Boolean value) {
		setValueForBinding(value, "isEditing");
	}

	public GspotUser getGspotUser() {
		// on fait un cache dans la session pour pas re-creer un gspotUser a
		// chaque requete
		if (session().objectForKey("gspotUser") == null)
			session().setObjectForKey(
					new GspotUser(ec(), getUtilisateurPersId()), "gspotUser");
		return (GspotUser) session().objectForKey("gspotUser");
	}

	public void setGspotUser(GspotUser appUser) {
		session().setObjectForKey(appUser, "gspotUser");
	}

	protected Boolean needReset;

	public Boolean isNeedReset() {
		if (hasBinding("selectedSalle")) {			
			if ((selectedSalle == null)
					|| ((selectedSalle != null) && (!selectedSalle
							.equals((EOSalles) valueForBinding("selectedSalle")))))
				needReset =Boolean.TRUE;
			selectedSalle = (EOSalles) valueForBinding("selectedSalle");
		}

		if ((needReset!=null)&&(Boolean.TRUE.equals(needReset))) {
			needReset = Boolean.FALSE;

			return Boolean.TRUE;
		}
		
		return (needReset!=null?needReset:Boolean.FALSE);
	}

	public void setNeedReset(Boolean needReset) {

		this.needReset = needReset;
	}

	public String parentIdToRefresh() {
		if (hasBinding("parentIdToRefresh"))
			return (String) valueForBinding("parentIdToRefresh");
		return null;
	}

	public boolean isCommitOnValid() {
		if (hasBinding("commitOnValid"))
			return (Boolean) valueForBinding("commitOnValid");
		return false;
	}

	public void setCommitOnValid(boolean commitOnValid) {
		if (canSetValueForBinding("commitOnValid"))
			setValueForBinding(commitOnValid, "commitOnValid");
	}
	
	public boolean showValidBtb() {
		if (hasBinding("showValidBtb"))
			return (Boolean) valueForBinding("showValidBtb");
		return true;
	}

	public void setShowValidBtb(boolean showValidBtb) {
		if (canSetValueForBinding("showValidBtb"))
			setValueForBinding(showValidBtb, "showValidBtb");
	}
	
}
