package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class GspotSalleTbView extends GspotTbView {
	private static final long serialVersionUID = 6349499744079625692L;

	protected static final String NOM_KP = EOSalles.SAL_PORTE_KEY;
	protected static final String IMPLANTATION_KP = ERXQ.keyPath(EOSalles.TO_LOCAL_KEY, EOLocal.TO_FIRST_IMPLANTATION_GEO,
			EOImplantationGeo.LL_IMPLANTATION_GEO_KEY);
	protected static final String ETAGE_KP = EOSalles.SAL_ETAGE_KEY;
	protected static final String BAT_KP = ERXQ.keyPath(EOSalles.TO_LOCAL_KEY, EOLocal.APPELLATION_KEY);
	protected static final String TYPE_KP = ERXQ.keyPath(EOSalles.TO_TYPE_SALLE_KEY, EOTypeSalle.TSAL_LIBELLE_KEY);
	protected static final String DESC_KP = EOSalles.SAL_DESCRIPTIF_KEY;

	static {
		// Colonne Nom
		CktlAjaxTableViewColumn colNom = new CktlAjaxTableViewColumn();
		colNom.setLibelle("Nom");
		colNom.setOrderKeyPath(NOM_KP);
		CktlAjaxTableViewColumnAssociation assNom = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + NOM_KP, "emptyValue");
		assNom.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colNom.setAssociations(assNom);
		colNom.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.setObjectForKey(colNom, NOM_KP);

		// Colonne Descriptif
		CktlAjaxTableViewColumn colDesc = new CktlAjaxTableViewColumn();
		colDesc.setLibelle("Descriptif");
		colDesc.setComponent("CommentZoneTbv");
		colDesc.setOrderKeyPath(DESC_KP);
		CktlAjaxTableViewColumnAssociation assDesc = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + DESC_KP, "emptyValue");
		assDesc.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		assDesc.setObjectForKey("30", "cols");
		colDesc.setAssociations(assDesc);
		colDesc.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.setObjectForKey(colDesc, DESC_KP);

		// Colonne Type
		CktlAjaxTableViewColumn colType = new CktlAjaxTableViewColumn();
		colType.setLibelle("Type");
		colType.setOrderKeyPath(TYPE_KP);
		CktlAjaxTableViewColumnAssociation assType = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + TYPE_KP, "emptyValue");
		assType.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colType.setAssociations(assType);
		colType.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.setObjectForKey(colType, TYPE_KP);

		// Colonne Implantation
		CktlAjaxTableViewColumn colImp = new CktlAjaxTableViewColumn();
		colImp.setLibelle("Implantation");
		colImp.setOrderKeyPath(IMPLANTATION_KP);
		CktlAjaxTableViewColumnAssociation assImp = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + IMPLANTATION_KP, "emptyValue");
		assImp.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colImp.setAssociations(assImp);
		colImp.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.setObjectForKey(colImp, IMPLANTATION_KP);
		// Colonne Etage
		CktlAjaxTableViewColumn colEtage = new CktlAjaxTableViewColumn();
		colEtage.setLibelle("Etage");
		colEtage.setOrderKeyPath(ETAGE_KP);
		CktlAjaxTableViewColumnAssociation assEtage = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + ETAGE_KP, "emptyValue");
		assEtage.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colEtage.setAssociations(assEtage);
		colEtage.setRowCssStyle("text-align:center;");
		colEtage.setHeaderCssStyle("width:30px;");
		_colonnesMap.setObjectForKey(colEtage, ETAGE_KP);
		// Colonne Bâtiment
		CktlAjaxTableViewColumn colBat = new CktlAjaxTableViewColumn();
		colBat.setLibelle("Bâtiment");
		colBat.setOrderKeyPath(BAT_KP);
		CktlAjaxTableViewColumnAssociation assBat = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + BAT_KP, "emptyValue");
		assBat.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		colBat.setAssociations(assBat);
		colBat.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.setObjectForKey(colBat, BAT_KP);

	}

	public GspotSalleTbView(WOContext context) {
		super(context);
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { NOM_KP, DESC_KP, TYPE_KP, ETAGE_KP, BAT_KP, IMPLANTATION_KP });
	}

	@Override
	public WOActionResults commitSave() {
		// TODO Auto-generated method stub
		return null;
	}
}