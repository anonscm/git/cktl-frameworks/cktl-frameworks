package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class SalleTelephoneTbView extends GspotTbView {
	private static final long serialVersionUID = 1L;

	private static final String TELEPHONE_KEY = EOSalleTelephone.NO_TELEPHONE_KEY;
	private static final String SALLE_KEY = EOSalleTelephone.TO_SALLES_KEY+"."+EOSalles.SAL_PORTE_KEY;
	
	
	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Salle");
		col0.setOrderKeyPath(SALLE_KEY);		
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + SALLE_KEY, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, SALLE_KEY);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Téléphone");
		col1.setOrderKeyPath(TELEPHONE_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + TELEPHONE_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		col1.setHeaderCssStyle("width:200px;");
		_colonnesMap.takeValueForKey(col1, TELEPHONE_KEY);		
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
    public SalleTelephoneTbView(WOContext context) {
        super(context);
    }
    
    @Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { SALLE_KEY, TELEPHONE_KEY,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delTel : getDeletedObjects()) {
				((EOSalleTelephone) delTel)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EOSalleTelephone) delTel).toSalles(),
								EOSalleTelephone.TO_SALLES_KEY);
	
				delTel.editingContext().deleteObject(delTel);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}