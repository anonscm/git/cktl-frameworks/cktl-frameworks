package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

public class SalleLotSalleTbView extends SallesTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static {   
		((CktlAjaxTableViewColumn)_colonnesMap.objectForKey(IMPLANTATION_KP)).setOrderKeyPath(IMPLANTATION_KP);
		((CktlAjaxTableViewColumn)_colonnesMap.objectForKey(ETAGE_KP)).setOrderKeyPath(ETAGE_KP);
		((CktlAjaxTableViewColumn)_colonnesMap.objectForKey(BAT_KP)).setOrderKeyPath(BAT_KP);
        _colonnesMap.setObjectForKey(getColonneAction(), OBJ_KEY + ".action");
    }
    public SalleLotSalleTbView(WOContext context) {
        super(context);
    }
    
    @Override
    public NSArray<String> DEFAULT_COLONNES_KEYS() {
        return new NSArray<String>(new String[] { NOM_KP, IMPLANTATION_KP, ETAGE_KP, BAT_KP,OBJ_KEY + ".action" });
    }

    @Override
    public WOActionResults commitSave() {
    	if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delOccup : getDeletedObjects()) {
				((EORepartLotSalle) delOccup)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EORepartLotSalle) delOccup).toLotSalle(),
								EORepartLotSalle.TO_LOT_SALLE_KEY);
	
				delOccup.editingContext().deleteObject(delOccup);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
    
}