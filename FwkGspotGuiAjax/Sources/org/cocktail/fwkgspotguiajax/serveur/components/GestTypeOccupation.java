package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestTypeOccupation extends BaseGestionRef {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestTypeOccupation(WOContext context) {
		super(context);
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		if ((getEditedType().tocLibelle() == null)
				|| ("".equals(getEditedType().tocLibelle()))) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, "Le libellé est obligatoire !");
			return null;
		}
		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedType().tocLibelle());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Type occupation créé ");
			setEditedObj(EOTypeOccupation
					.createFwkGspot_TypeOccupation(new EOEditingContext(ec())));

		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addType() {
		setEditedObj(EOTypeOccupation
				.createFwkGspot_TypeOccupation(new EOEditingContext(ec())));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'un type occupation");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOTypeOccupation) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'un type occupation");
		return null;
	}

	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = EOTypeOccupation.TOC_LIBELLE_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
		}

		return EOTypeOccupation
				.fetchFwkGspot_TypeOccupations(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOTypeOccupation.TOC_LIBELLE_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOTypeOccupation getEditedType() {
		return (EOTypeOccupation) getEditedObj();
	}
}