package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxTreeModel;

public class SallesTreeDelegate implements AjaxTreeModel.Delegate {

	private EOEditingContext ec;
	private AjaxTreeModel treeModel;
	private SalleTreeItem treeItem;

	private SalleTreeItem rootTree;

	private String libelleSearch;
	private boolean batSearch = true;
	private boolean porteSearch = true;
	private boolean etageSearch = false;
	
	private EOImplantationGeo selImplantation;

	public SallesTreeDelegate(EOEditingContext ec) {
		super();
		this.ec = ec;
	}

	public String getLibelleArbre() {
		return treeItem.getLibelle();
	}

	public NSArray<SalleTreeItem> childrenTreeNodes(Object node) {
		return ((SalleTreeItem) node).getChilds(libelleSearch, isBatSearch(),
				isPorteSearch(), isEtageSearch());
	}

	public boolean isLeaf(Object node) {
		return ((node instanceof SalleTreeItem) && (((SalleTreeItem) node)
				.getEmbededObject() instanceof EOSalles));

	}

	public Object parentTreeNode(Object node) {
		if (node == null) {
			return null;
		}
		return ((SalleTreeItem) node).getParent();
	}

	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}

	public AjaxTreeModel getTreeModel() {
		if (treeModel == null)
			treeModel = new AjaxTreeModel();
		return treeModel;
	}

	public void setTreeModel(AjaxTreeModel treeModel) {
		this.treeModel = treeModel;
	}

	public SalleTreeItem getTreeItem() {
		return treeItem;
	}

	public void setTreeItem(SalleTreeItem treeItem) {
		this.treeItem = treeItem;
	}

	public SalleTreeItem getRootTree() {
		if (this.rootTree==null)
			setRootTree(new SalleTreeItem(this.getSelImplantation(),null));
		return rootTree;
	}

	public void setRootTree(SalleTreeItem rootTree) {
		this.rootTree = rootTree;
	}

	public String getLibelleSearch() {
		return libelleSearch;
	}

	public void setLibelleSearch(String libelleSearch) {
		this.libelleSearch = libelleSearch;
	}

	public boolean isBatSearch() {
		return batSearch;
	}

	public void setBatSearch(Boolean batSearch) {
		if (batSearch == null)
			this.batSearch = false;
		else
			this.batSearch = batSearch;
	}

	public boolean isPorteSearch() {
		return porteSearch;
	}

	public void setPorteSearch(Boolean porteSearch) {
		if (porteSearch == null)
			this.porteSearch = false;
		else
			this.porteSearch = porteSearch;
	}

	public boolean isEtageSearch() {
		return etageSearch;
	}

	public void setEtageSearch(Boolean descSearch) {
		if (descSearch == null)
			this.etageSearch = false;
		else
			this.etageSearch = descSearch;
	}
	
	public boolean isLink(){
		return (treeItem!=null)&& (treeItem.getEmbededObject() instanceof EOSalles); 
	}

	public EOImplantationGeo getSelImplantation() {
		return selImplantation;
	}

	public void setSelImplantation(EOImplantationGeo selImplantation) {
		this.selImplantation = selImplantation;
		if ((this.rootTree!=null)&&(selImplantation!=null)&&(!selImplantation.equals(this.rootTree.getEmbededObject())))
			this.rootTree = null;
	}

}
