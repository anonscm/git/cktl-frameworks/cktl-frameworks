package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class IndividusLotSalleTbView extends GspotTbView {
	private static final long serialVersionUID = 1L;

	private static final String COL_NUMERO_KEY = ERXQ.keyPath(EORepartLotIndividu.TO_FWKPERS__INDIVIDU_KEY,EOIndividu.NUMERO_KEY);
	private static final String COL_NOM_KEY = ERXQ.keyPath(EORepartLotIndividu.TO_FWKPERS__INDIVIDU_KEY,EOIndividu.NOM_PRENOM_AFFICHAGE_KEY);
		
	static {
		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Numéro");
		col1.setOrderKeyPath(COL_NUMERO_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COL_NUMERO_KEY, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setHeaderCssStyle("width:80px;");
		_colonnesMap.takeValueForKey(col1, COL_NUMERO_KEY);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Nom");
		col2.setOrderKeyPath(COL_NOM_KEY);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COL_NOM_KEY, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setRowCssStyle("text-align:left;padding-left:3px;");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, COL_NOM_KEY);
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
    public IndividusLotSalleTbView(WOContext context) {
        super(context);
    }
    
    @Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { COL_NUMERO_KEY, COL_NOM_KEY,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delOccup : getDeletedObjects()) {
				((EORepartLotIndividu) delOccup)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EORepartLotIndividu) delOccup).toLotSalle(),
								EORepartLotIndividu.TO_LOT_SALLE_KEY);
	
				delOccup.editingContext().deleteObject(delOccup);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}