package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class ImplantationFromBatTbv extends CktlAjaxWOComponent {
	public ImplantationFromBatTbv(WOContext context) {
		super(context);
	}

	public Object value() {
		return (Object) valueForBinding("value");
	}

	public void setValue(Object value) {
		setValueForBinding(value, "value");
	}

	public String libImplantation() {
		EORepartBatImpGeo repart = ((NSArray<EORepartBatImpGeo>) value())
				.lastObject();
		if (repart != null)
			return repart.toImplantationGeo().lcImplantationGeo();
		else
			return "";
	}

}