package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

public class ZoneButton extends CktlAjaxWOComponent {
    public ZoneButton(WOContext context) {
        super(context);
    }

	private String zonebuttonid;

	/**
	 * @return the zonebuttonid
	 */
	public String zonebuttonid() {
		if (hasBinding("zonebuttonid"))
			zonebuttonid = (String) valueForBinding("zonebuttonid");
		return zonebuttonid;
	}

	/**
	 * @param zonebuttonid the zonebuttonid to set
	 */
	public void setZonebuttonid(String zonebuttonid) {
		this.zonebuttonid = zonebuttonid;
		if (canSetValueForBinding("zonebuttonid"))
			setValueForBinding(zonebuttonid,"zonebuttonid");
	}

	public boolean isEdited() {
		return (Boolean) valueForBinding("isEdited");
	}

	public void setIsEdited(boolean isEdited) {
		setValueForBinding(isEdited, "isEdited");
	}
	
	public WOActionResults backFromEdit() {
		getEcToRevert().revert();
		setIsEdited(false);
		return null;
	}

	public WOActionResults goToEdit() {
		setIsEdited(true);
		return null;
	}

	public EOEditingContext getEcToRevert() {
		return (EOEditingContext) valueForBinding("ecToRevert");
	}

	
}