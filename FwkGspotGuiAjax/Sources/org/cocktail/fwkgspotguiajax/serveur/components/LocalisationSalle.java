package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkgspot.serveur.finder.FinderLocal;
import org.cocktail.fwkgspot.serveur.finder.FinderVetageLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class LocalisationSalle extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LocalisationSalle(WOContext context) {
		super(context);
	}

	public EOImplantationGeo implantation() {
		if (selectedSalle() != null)
			return (selectedSalle().toLocal().toRepartBatImpGeos() != null ? selectedSalle()
					.toLocal().toRepartBatImpGeos().lastObject()
					.toImplantationGeo()
					: null);
		return null;
	}

	public EOLocal batiment() {
		if (selectedSalle() != null)
			return (selectedSalle().toLocal() != null ? selectedSalle()
					.toLocal() : null);
		return null;
	}

	private NSArray<EOImplantationGeo> lstImplantations;

	/**
	 * @return the lstImplantations
	 */
	public NSArray<EOImplantationGeo> lstImplantations() {
		if (lstImplantations == null)
			lstImplantations = EOImplantationGeo
					.fetchFwkGspot_ImplantationGeos(
							ec(),
							EOImplantationGeo.QUAL_VALID_IMPLANTATION,
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOImplantationGeo.LC_IMPLANTATION_GEO_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstImplantations;
	}

	/**
	 * @param lstImplantations
	 *            the lstImplantations to set
	 */
	public void setLstImplantations(NSArray<EOImplantationGeo> lstImplantations) {
		this.lstImplantations = lstImplantations;
	}

	private EOImplantationGeo implantationOccur;

	/**
	 * @return the implantationOccur
	 */
	public EOImplantationGeo implantationOccur() {
		return implantationOccur;
	}

	/**
	 * @param implantationOccur
	 *            the implantationOccur to set
	 */
	public void setImplantationOccur(EOImplantationGeo implantationOccur) {
		this.implantationOccur = implantationOccur;
	}

	private EOImplantationGeo selectedImplantation;

	/**
	 * @return the selectedImplantation
	 */
	public EOImplantationGeo selectedImplantation() {
		if (selectedImplantation == null) {
			if (selectedSalle() != null && selectedSalle().toLocal() != null) {
				selectedImplantation = selectedSalle().toLocal()
						.toRepartBatImpGeos().lastObject().toImplantationGeo();
			}
		}
		return selectedImplantation;
	}

	/**
	 * @param selectedImplantation
	 *            the selectedImplantation to set
	 */
	public void setSelectedImplantation(EOImplantationGeo selectedImplantation) {
		this.selectedImplantation = selectedImplantation;
	}

	private NSArray<EOLocal> lstBatiments;

	/**
	 * @return the lstBatiments
	 */
	public NSArray<EOLocal> lstBatiments() {
		if (lstBatiments == null) {
			if (selectedImplantation() != null) {
				lstBatiments = FinderLocal.getLocalsForImplantation(selectedImplantation(), ec());
			}
	   }
	   return lstBatiments;
	}

	/**
	 * @param lstBatiments
	 *            the lstBatiments to set
	 */
	public void setLstBatiments(NSArray<EOLocal> lstBatiments) {
		this.lstBatiments = lstBatiments;
	}

	private EOLocal batOccur;

	/**
	 * @return the batOccur
	 */
	public EOLocal batOccur() {
		return batOccur;
	}

	/**
	 * @param batOccur
	 *            the batOccur to set
	 */
	public void setBatOccur(EOLocal batOccur) {
		this.batOccur = batOccur;
	}
	
	public String infosContainerId() {
		return getComponentId() + "_infosContainer";
	}
	
	private String inputEtage;

	/**
	 * @return the inputEtage
	 */
	public String inputEtage() {
		if (inputEtage == null && selectedSalle() != null)
			inputEtage = selectedSalle().salEtage();
		return (inputEtage != null ? inputEtage.trim() : null);
	}

	/**
	 * @param inputEtage
	 *            the inputEtage to set
	 */
	public void setInputEtage(String inputEtage) {
		this.inputEtage = inputEtage != null ? inputEtage.trim() : null;
		selectedSalle().setSalEtage(this.inputEtage);
	}

	private NSArray<String> lstVEtages;

	/**
	 * @return the lstEtages
	 */
	@SuppressWarnings("unchecked")
	public NSArray<String> lstEtages() {
		if (lstVEtages == null) {
			lstVEtages = (NSArray<String>) FinderVetageLocal.getEtageForLocal(selectedSalle().toLocal(), ec()).valueForKey(EOVEtageLocal.SAL_ETAGE_KEY);
		}
		return lstVEtages;
	}


	private String aucTriggerLocSalleId = ERXStringUtilities
			.safeIdentifierName("aucTriggerLocSalleId" + UUID.randomUUID());

	/**
	 * @return the aucTriggerLocSalleId
	 */
	public String aucTriggerLocSalleId() {
		return aucTriggerLocSalleId;
	}

	/**
	 * @param aucTriggerLocSalleId
	 *            the aucTriggerLocSalleId to set
	 */
	public void setAucTriggerLocSalleId(String aucTriggerLocSalleId) {
		this.aucTriggerLocSalleId = aucTriggerLocSalleId;
	}

	private NSArray<String> lstRefreshZones;

	/**
	 * @return the lstRefreshZones
	 */
	public NSArray<String> lstRefreshZones() {
		if (lstRefreshZones == null) {
			if (parentIdToRefresh() != null)
				setLstRefreshZones(new NSArray<String>(new String[] {
						auclocsalleid(), aucmessageid(), parentIdToRefresh() }));
			else
				setLstRefreshZones(new NSArray<String>(new String[] {
						auclocsalleid(), aucmessageid() }));
		}

		return lstRefreshZones;
	}

	/**
	 * @param lstRefreshZones
	 *            the lstRefreshZones to set
	 */
	public void setLstRefreshZones(NSArray<String> lstRefreshZones) {
		this.lstRefreshZones = lstRefreshZones;
	}

	private String auclocsalleid = ERXStringUtilities
			.safeIdentifierName("auclocsalleid" + UUID.randomUUID());

	/**
	 * @return the auclocsalleid
	 */
	public String auclocsalleid() {
		return auclocsalleid;
	}

	/**
	 * @param auclocsalleid
	 *            the auclocsalleid to set
	 */
	public void setAuclocsalleid(String auclocsalleid) {
		this.auclocsalleid = auclocsalleid;
	}

	public WOActionResults validLocal() {

		if (selectedSalle().toLocal() == null) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Le bâtiment est obligatoire !!");

			return null;
		}
		if (selectedSalle().salEtage() == null 
		    || (selectedSalle().salEtage()!=null && "".equals(selectedSalle().salEtage()))) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"L'étage est obligatoire !!");

			return null;
		}
		if (selectedSalle().salPorte() == null 
			|| (selectedSalle().salPorte()!=null && "".equals(selectedSalle().salPorte()))) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Le nom est obligatoire !!");
			return null;
		}
		if (isCommitOnValid())
			ec().saveChanges();
		setIsEditing(false);
		return null;
	}

	public String aucmessageid() {
		if (hasBinding("aucmessageid"))
			return (String) valueForBinding("aucmessageid");
		else
			return getComponentId() + "_aucmessageid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	public WOActionResults changementBatiment() {
		lstVEtages = null;
		selectedSalle().setSalEtage(null);

		return doNothing();
	}

	public WOActionResults changementImplantation() {
		lstBatiments = null;
		lstVEtages = null;
		
		selectedSalle().setSalEtage(null);
		selectedSalle().setToLocalRelationship(null);
		
		return doNothing();
	}

}