package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;

import er.extensions.foundation.ERXStringUtilities;

public class ArbreSalle extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;

	private EOEditingContext ec;

	private SallesTreeDelegate delegate;
	private EOGenericRecord selectedEOObjet;
    public ArbreSalle(WOContext context) {
        super(context);
    }
    private String treeId = ERXStringUtilities.safeIdentifierName("treeid"
			+ UUID.randomUUID());

	/**
	 * @return the treeId
	 */
	public String treeId() {
		return treeId;
	}

	/**
	 * @param treeId
	 *            the treeId to set
	 */
	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}
	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}
	public SallesTreeDelegate delegate() {
		if (hasBinding("delegate"))
			delegate = (SallesTreeDelegate) valueForBinding("delegate");
		
		if (delegate == null) {
			delegate = new SallesTreeDelegate(ec);

		}
		return delegate;

	}

	public void setDelegate(SallesTreeDelegate delegate) {
		
		this.delegate = delegate;
		if (canSetValueForBinding("delegate"))
			setValueForBinding(delegate, "delegate");
	}

	public EOGenericRecord getSelectedEOObjet() {
		if (hasBinding("selectedEOObjet"))
			selectedEOObjet = (EOGenericRecord) valueForBinding("selectedEOObjet");
		return selectedEOObjet;
	}

	public void setSelectedEOObjet(EOGenericRecord selectedItem) {
		this.selectedEOObjet = selectedItem;
		if (canSetValueForBinding("selectedEOObjet"))
			setValueForBinding(selectedEOObjet, "selectedEOObjet");
	}

	public String styleArbre() {
		String style ="overflow:auto;";
		if (hasBinding("style")){
			style+=valueForBinding("style");
		}
		return style;
	}
}