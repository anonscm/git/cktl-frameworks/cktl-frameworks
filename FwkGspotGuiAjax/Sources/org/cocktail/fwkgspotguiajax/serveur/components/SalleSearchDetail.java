package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.finder.FinderLocal;
import org.cocktail.fwkgspot.serveur.finder.FinderVetageLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOPrises;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class SalleSearchDetail extends CktlAjaxWOComponent {
	public static final String BIND_EC = "ec";
	public static final String BIND_SELECTED_SALLE = "selectedSalle";
	public static final String BIND_UPDATE_CONTAINER_ID = "updateContainerID";

	public SalleSearchDetail(WOContext context) {
		super(context);
	}

	private NSArray<EOTypeSalle> lstTypesSalless;
	private EOTypeSalle typeSalleOccur;
	private EOTypeSalle searchedTypeSalle;

	/**
	 * @return the lstTypesSalless
	 */
	public NSArray<EOTypeSalle> lstTypesSalless() {
		if (lstTypesSalless == null)
			lstTypesSalless = EOTypeSalle.fetchAllFwkGspot_TypeSalles(ec(), new NSArray<EOSortOrdering>(new EOSortOrdering[] { new EOSortOrdering(
					EOTypeSalle.TSAL_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }));
		return lstTypesSalless;
	}

	public EOEditingContext ec() {
		if (valueForBinding(BIND_EC) != null)
			return (EOEditingContext) valueForBinding(BIND_EC);
		else
			return session().defaultEditingContext();

	}

	/**
	 * @return the typeSalleOccur
	 */
	public EOTypeSalle typeSalleOccur() {
		return typeSalleOccur;
	}

	/**
	 * @param typeSalleOccur
	 *            the typeSalleOccur to set
	 */
	public void setTypeSalleOccur(EOTypeSalle typeSalleOccur) {
		this.typeSalleOccur = typeSalleOccur;
	}

	/**
	 * @return the searchedTypeSalle
	 */
	public EOTypeSalle searchedTypeSalle() {
		return searchedTypeSalle;
	}

	/**
	 * @param searchedTypeSalle
	 *            the searchedTypeSalle to set
	 */
	public void setSearchedTypeSalle(EOTypeSalle searchedTypeSalle) {
		this.searchedTypeSalle = searchedTypeSalle;
	}

	private WODisplayGroup displayGroup;

	public WOActionResults searchObj() {
		displayGroup.setObjectArray(getLstSalles());
		displayGroup.clearSelection();
		displayGroup.updateDisplayedObjects();
		displayGroup.setCurrentBatchIndex(1);
		haveSearched = true;
		return null;
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOSalles> getLstSalles() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if (searchedTypeSalle() != null) {
			qualStr = ERXQ.keyPath(EOSalles.TO_TYPE_SALLE_KEY) + " = %@ ";
			qualArray.add(searchedTypeSalle());
		}
		if ((searchedNom() != null) && (!"".equals(searchedNom().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.SAL_PORTE_KEY) + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchedNom() + "*");
		}

		if ((searchedDesc() != null) && (!"".equals(searchedDesc().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.SAL_DESCRIPTIF_KEY) + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchedDesc() + "*");
		}

		if (searchedAcces() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_REPART_SALLE_ACCES_HANDICAPES_KEY,
					EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY) + " = %@ ";
			qualArray.add(searchedAcces());
		}

		if (searchedImplantation() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_LOCAL_KEY, EOLocal.TO_REPART_BAT_IMP_GEOS_KEY, EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY) + " = %@ ";
			qualArray.add(searchedImplantation());
		}

		if (searchedLocal() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += EOSalles.TO_LOCAL_KEY + " = %@ ";
			qualArray.add(searchedLocal());
		}

		if ((searchedEtage() != null) && (!"".equals(searchedEtage().salEtage().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.SAL_ETAGE_KEY) + " = %@ ";
			qualArray.add(searchedEtage().salEtage());
		}

		if (searchedOccup() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_REPART_BUREAU_KEY, EORepartBureau.TO_INDIVIDU_KEY) + " = %@ ";
			qualArray.add(searchedOccup());
		}

		if (searchedDepos() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_DETAIL_POURCENTAGES_KEY, EODetailPourcentage.TO_STRUCTURE_ULR_KEY,
					EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY, EOStructure.TO_REPART_STRUCTURES_ELTS_KEY,
					EORepartStructure.TO_REPART_ASSOCIATIONS_KEY, EORepartAssociation.TO_ASSOCIATION_KEY, EOAssociation.ASS_CODE_KEY)
					+ " = %s ";
			qualArray.add(FinderDepositaires.ASS_CODE_DEPOS);
			qualStr += " AND "
					+ ERXQ.keyPath(EOSalles.TO_DETAIL_POURCENTAGES_KEY, EODetailPourcentage.TO_STRUCTURE_ULR_KEY,
							EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY, EOStructure.TO_REPART_STRUCTURES_ELTS_KEY,
							EORepartStructure.TO_REPART_ASSOCIATIONS_KEY, EORepartAssociation.TO_INDIVIDUS_ASSOCIES_KEY) + " = %@ ";
			qualArray.add(searchedDepos());
		}

		if ((searchedPrise() != null) && (!"".equals(searchedPrise().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_PRISESES_KEY, EOPrises.PRI_CODE_KEY) + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchedPrise() + "*");
		}

		if ((searchedPorte() != null) && (!"".equals(searchedPorte().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_SALLE_PORTES_KEY, EOSallePorte.SAL_PORTE_KEY) + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchedPorte() + "*");
		}

		if ((searchedTelephone() != null) && (!"".equals(searchedTelephone().trim()))) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_SALLE_TELEPHONES_KEY, EOSalleTelephone.NO_TELEPHONE_KEY) + " caseInsensitiveLike %@ ";
			qualArray.add("*" + searchedTelephone() + "*");
		}

		if (searchedService() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_DETAIL_POURCENTAGES_KEY, EODetailPourcentage.TO_STRUCTURE_ULR_KEY) + " = %@ ";
			qualArray.add(searchedService());
		}

		if (searchedGroupe() != null) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.TO_DEPOSITAIRE_SALLESS_KEY, EODepositaireSalles.TO_STRUCTURE_ULR_KEY) + " = %@ ";
			qualArray.add(searchedGroupe());
		}

		if (salleReservable()) {
			if (!"".equals(qualStr)) {
				qualStr += " AND ";
			}
			qualStr += ERXQ.keyPath(EOSalles.SAL_RESERVABLE_KEY) + " = %@ ";
			qualArray.add("O");
		}

		System.out.println(qualStr);
		NSMutableArray<EOSortOrdering> orderBy = new NSMutableArray<EOSortOrdering>();
		if (searchedImplantation == null) {
			orderBy.addObject(new EOSortOrdering(ERXQ.keyPath(EOSalles.TO_LOCAL_KEY, EOLocal.TO_REPART_BAT_IMP_GEOS_KEY,
					EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY, EOImplantationGeo.LC_IMPLANTATION_GEO_KEY),
					EOSortOrdering.CompareCaseInsensitiveAscending));
		}
		if (searchedLocal == null) {
			orderBy.addObject(new EOSortOrdering(ERXQ.keyPath(EOSalles.TO_LOCAL_KEY, EOLocal.APPELLATION_KEY),
					EOSortOrdering.CompareCaseInsensitiveAscending));
		}
		if (searchedEtage == null) {
			orderBy.addObject(new EOSortOrdering(EOSalles.SAL_ETAGE_KEY, EOSortOrdering.CompareAscending));
		}
		if (searchedTypeSalle == null) {
			orderBy.addObject(new EOSortOrdering(ERXQ.keyPath(EOSalles.TO_TYPE_SALLE_KEY, EOTypeSalle.TSAL_LIBELLE_KEY),
					EOSortOrdering.CompareCaseInsensitiveAscending));
		}
		orderBy.addObject(new EOSortOrdering(ERXQ.keyPath(EOSalles.SAL_PORTE_KEY), EOSortOrdering.CompareCaseInsensitiveAscending));

		NSArray<EOSalles> retour = EOSalles.fetchDistinctFwkGspot_Salleses(ec(), EOQualifier.qualifierWithQualifierFormat(qualStr, qualArray), null);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(retour, orderBy);
	}

	public WODisplayGroup displayGroup() {

		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setDelegate(new DgDelegate());
			displayGroup.setNumberOfObjectsPerBatch(8);
			// searchObj();
		}

		return displayGroup;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedSalle((EOSalles) group.selectedObject());
		}
	}

	private String searchedNom;
	private String searchedDesc;

	public void setSelectedSalle(EOSalles selectedSalle) {
		if (canSetValueForBinding(BIND_SELECTED_SALLE)) {
			setValueForBinding(selectedSalle, BIND_SELECTED_SALLE);
		}
		if (hasBinding(BIND_UPDATE_CONTAINER_ID)) {
			AjaxUpdateContainer.updateContainerWithID((String) valueForBinding(BIND_UPDATE_CONTAINER_ID), context());
		}

	}

	public EOSalles getSelectedSalle() {
		return (EOSalles) valueForBinding(BIND_SELECTED_SALLE);
	}

	/**
	 * @return the searchedNom
	 */
	public String searchedNom() {
		return searchedNom;
	}

	/**
	 * @param searchedNom
	 *            the searchedNom to set
	 */
	public void setSearchedNom(String searchedNom) {
		this.searchedNom = searchedNom;
	}

	/**
	 * @return the searchedDesc
	 */
	public String searchedDesc() {
		return searchedDesc;
	}

	/**
	 * @param searchedDesc
	 *            the searchedDesc to set
	 */
	public void setSearchedDesc(String searchedDesc) {
		this.searchedDesc = searchedDesc;
	}

	private NSArray<EOTypeAccesHandicape> lstAcces;

	/**
	 * @return the lstAcces
	 */
	public NSArray<EOTypeAccesHandicape> lstAcces() {
		if (lstAcces == null)
			lstAcces = EOTypeAccesHandicape.fetchAllFwkGspot_TypeAccesHandicapes(ec(), new NSArray<EOSortOrdering>(
					new EOSortOrdering[] { new EOSortOrdering(EOTypeAccesHandicape.LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) }));
		return lstAcces;
	}

	private EOTypeAccesHandicape accesOccur;
	private EOTypeAccesHandicape searchedAcces;

	/**
	 * @return the accesOccur
	 */
	public EOTypeAccesHandicape accesOccur() {
		return accesOccur;
	}

	/**
	 * @param accesOccur
	 *            the accesOccur to set
	 */
	public void setAccesOccur(EOTypeAccesHandicape accesOccur) {
		this.accesOccur = accesOccur;
	}

	/**
	 * @return the searchedAcces
	 */
	public EOTypeAccesHandicape searchedAcces() {
		return searchedAcces;
	}

	/**
	 * @param searchedAcces
	 *            the searchedAcces to set
	 */
	public void setSearchedAcces(EOTypeAccesHandicape searchedAcces) {
		this.searchedAcces = searchedAcces;
	}

	private NSArray<EOImplantationGeo> lstImplantations;

	/**
	 * @return the lstImplantations
	 */
	public NSArray<EOImplantationGeo> lstImplantations() {
		if (lstImplantations == null)
			lstImplantations = EOImplantationGeo.fetchFwkGspot_ImplantationGeos(ec(), EOImplantationGeo.QUAL_VALID_IMPLANTATION,
					new NSArray<EOSortOrdering>(new EOSortOrdering(EOImplantationGeo.LC_IMPLANTATION_GEO_KEY,
							EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstImplantations;
	}

	private EOImplantationGeo implantationOccur;
	private EOImplantationGeo searchedImplantation;

	/**
	 * @return the implantationOccur
	 */
	public EOImplantationGeo implantationOccur() {
		return implantationOccur;
	}

	/**
	 * @param implantationOccur
	 *            the implantationOccur to set
	 */
	public void setImplantationOccur(EOImplantationGeo implantationOccur) {
		this.implantationOccur = implantationOccur;
	}

	/**
	 * @return the searchedImplantation
	 */
	public EOImplantationGeo searchedImplantation() {
		return searchedImplantation;
	}

	/**
	 * @param searchedImplantation
	 *            the searchedImplantation to set
	 */
	public void setSearchedImplantation(EOImplantationGeo searchedImplantation) {
		this.searchedImplantation = searchedImplantation;
	}

	public String lstBatimentId() {
		return getComponentId() + "_lstBatimentId";
	}

	public String lstEtagesid() {
		return getComponentId() + "_lstEtagesid";
	}

	private NSArray<EOLocal> lstBatiments;
	private EOLocal batOccur;
	private EOLocal searchedLocal;
	private EOVEtageLocal etageOccur;
	private EOVEtageLocal searchedEtage;

	/**
	 * @return the lstBatiments
	 */
	public NSArray<EOLocal> lstBatiments() {
		if (searchedImplantation == null) {
			return null;
		}
		if ((lstBatiments == null)
				|| ((lstBatiments != null) && (lstBatiments.lastObject().toRepartBatImpGeos() != null)
						&& (lstBatiments.lastObject().toRepartBatImpGeos().lastObject().toImplantationGeo() != null) && (!lstBatiments.lastObject()
						.toRepartBatImpGeos().lastObject().toImplantationGeo().equals(searchedImplantation))))
			lstBatiments = FinderLocal.getLocalsForImplantation((searchedImplantation != null ? searchedImplantation : lstImplantations()
					.objectAtIndex(0)), ec());
		return lstBatiments;
	}

	/**
	 * @return the batOccur
	 */
	public EOLocal batOccur() {
		return batOccur;
	}

	/**
	 * @param batOccur
	 *            the batOccur to set
	 */
	public void setBatOccur(EOLocal batOccur) {
		this.batOccur = batOccur;
	}

	/**
	 * @return the searchedLocal
	 */
	public EOLocal searchedLocal() {
		return searchedLocal;
	}

	/**
	 * @param searchedLocal
	 *            the searchedLocal to set
	 */
	public void setSearchedLocal(EOLocal searchedLocal) {
		this.searchedLocal = searchedLocal;
	}

	public NSArray<EOVEtageLocal> lstEtages() {
		return FinderVetageLocal.getEtageForLocal(searchedLocal, ec());
	}

	/**
	 * @return the etageOccur
	 */
	public EOVEtageLocal etageOccur() {
		return etageOccur;
	}

	/**
	 * @param etageOccur
	 *            the etageOccur to set
	 */
	public void setEtageOccur(EOVEtageLocal etageOccur) {
		this.etageOccur = etageOccur;
	}

	/**
	 * @return the searchedEtage
	 */
	public EOVEtageLocal searchedEtage() {
		return searchedEtage;
	}

	/**
	 * @param searchedEtage
	 *            the searchedEtage to set
	 */
	public void setSearchedEtage(EOVEtageLocal searchedEtage) {
		this.searchedEtage = searchedEtage;
	}

	public String aucsearchedindivid() {
		return getComponentId() + "_aucsearchedindivid";
	}

	public String aucindivsearchid() {
		return getComponentId() + "_aucindivsearchid";
	}

	public WOActionResults searchIndiv() {
		setWindowIndVisible(true);
		AjaxModalDialog.open(context(), camsearchoccupid());
		return null;
	}

	private boolean isWindowIndVisible;

	/**
	 * @return the isWindowIndVisible
	 */
	public boolean isWindowIndVisible() {
		return isWindowIndVisible;
	}

	/**
	 * @param isWindowIndVisible
	 *            the isWindowIndVisible to set
	 */
	public void setWindowIndVisible(boolean isWindowIndVisible) {
		this.isWindowIndVisible = isWindowIndVisible;
	}

	public String camsearchoccupid() {
		return getComponentId() + "_camsearchoccupid";
	}

	public WOActionResults resetSearchIndiv() {
		setSearchedOccup(null);
		return null;
	}

	private EOIndividu selectedSearchPersonne;

	/**
	 * @return the selectedSearchPersonne
	 */
	public EOIndividu selectedSearchPersonne() {
		return selectedSearchPersonne;
	}

	/**
	 * @param selectedSearchPersonne
	 *            the selectedSearchPersonne to set
	 */
	public void setSelectedSearchPersonne(EOIndividu selectedSearchPersonne) {
		this.selectedSearchPersonne = selectedSearchPersonne;
	}

	private String searchTypeIntExt = "interne";
	private EOIndividu searchedOccup;
	private boolean isWindowDeposVisible;
	private EOIndividu selectedSearchDepos;

	/**
	 * @return the searchTypeIntExt
	 */
	public String searchTypeIntExt() {
		return searchTypeIntExt;
	}

	/**
	 * @param searchTypeIntExt
	 *            the searchTypeIntExt to set
	 */
	public void setSearchTypeIntExt(String searchTypeIntExt) {
		this.searchTypeIntExt = searchTypeIntExt;
	}

	public String aucselpersonneid() {
		return getComponentId() + "_aucselpersonneid";
	}

	/**
	 * @return the searchedOccup
	 */
	public EOIndividu searchedOccup() {
		return searchedOccup;
	}

	/**
	 * @param searchedOccup
	 *            the searchedOccup to set
	 */
	public void setSearchedOccup(EOIndividu searchedOccup) {
		this.searchedOccup = searchedOccup;
	}

	public WOActionResults chooseSearchedOccup() {
		setSearchedOccup(selectedSearchPersonne);
		closeSearchInd();
		return null;
	}

	public WOActionResults closeSearchInd() {
		setWindowIndVisible(false);
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(aucsearchedindivid(), context());
		return null;
	}

	public String auclisteid() {
		return getComponentId() + "_auclisteid";
	}

	public String buzzyid() {
		return getComponentId() + "_buzzyid";
	}

	public boolean selectionSalleEnabled() {
		return (hasBinding(BIND_SELECTED_SALLE) && canSetValueForBinding(BIND_SELECTED_SALLE));
	}

	public String aucindivdepossearchid() {
		return getComponentId() + "_aucindivdepossearchid";
	}

	public String camsearchdeposid() {
		return getComponentId() + "_camsearchdeposid";
	}

	/**
	 * @return the isWindowDeposVisible
	 */
	public boolean isWindowDeposVisible() {
		return isWindowDeposVisible;
	}

	/**
	 * @param isWindowDeposVisible
	 *            the isWindowDeposVisible to set
	 */
	public void setWindowDeposVisible(boolean isWindowDeposVisible) {
		this.isWindowDeposVisible = isWindowDeposVisible;
	}

	/**
	 * @return the selectedSearchDepos
	 */
	public EOIndividu selectedSearchDepos() {
		return selectedSearchDepos;
	}

	/**
	 * @param selectedSearchDepos
	 *            the selectedSearchDepos to set
	 */
	public void setSelectedSearchDepos(EOIndividu selectedSearchDepos) {
		this.selectedSearchDepos = selectedSearchDepos;
	}

	public String aucseldeposid() {
		return getComponentId() + "_aucseldeposid";
	}

	public void setSearchedDepos(EOIndividu searchedDepos) {
		this.searchedDepos = searchedDepos;
	}

	public EOIndividu searchedDepos() {
		return searchedDepos;
	}

	private EOIndividu searchedDepos;
	private String searchedPrise;
	private String searchedPorte;
	private String searchedTelephone;

	public WOActionResults chooseSearchedDepos() {
		setSearchedDepos(selectedSearchDepos);
		closeSearchDepos();
		return null;
	}

	public WOActionResults closeSearchDepos() {
		setWindowDeposVisible(false);
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(aucsearcheddeposid(), context());
		return null;
	}

	public String aucsearcheddeposid() {
		return getComponentId() + "_aucsearcheddeposid";
	}

	public WOActionResults searchDepos() {
		setWindowDeposVisible(true);
		AjaxModalDialog.open(context(), camsearchdeposid());
		return null;
	}

	public WOActionResults resetSearchDepos() {
		setSearchedDepos(null);
		return null;
	}

	/**
	 * @return the searchedPrise
	 */
	public String searchedPrise() {
		return searchedPrise;
	}

	/**
	 * @param searchedPrise
	 *            the searchedPrise to set
	 */
	public void setSearchedPrise(String searchedPrise) {
		this.searchedPrise = searchedPrise;
	}

	/**
	 * @return the searchedPorte
	 */
	public String searchedPorte() {
		return searchedPorte;
	}

	/**
	 * @param searchedPorte
	 *            the searchedPorte to set
	 */
	public void setSearchedPorte(String searchedPorte) {
		this.searchedPorte = searchedPorte;
	}

	/**
	 * @return the searchedTelephone
	 */
	public String searchedTelephone() {
		return searchedTelephone;
	}

	/**
	 * @param searchedTelephone
	 *            the searchedTelephone to set
	 */
	public void setSearchedTelephone(String searchedTelephone) {
		this.searchedTelephone = searchedTelephone;
	}

	public String telephoneId() {
		return getComponentId() + "_telephoneId";
	}

	public WOActionResults formatPhoneNumber() {
		setSearchedTelephone(MyStringCtrl.formatPhoneNumber(searchedTelephone));
		return null;
	}

	public String aucsearchserviceid() {
		return getComponentId() + "_aucsearchserviceid";
	}

	public String camservicesearchid() {
		return getComponentId() + "_camservicesearchid";
	}

	private boolean isInSearch = false;
	private EOStructureUlr searchedService;
	private EOStructureUlr inSearchService;
	private EOStructureUlr inSearchGroupe;
	private Boolean isWindowGroupeVisible;
	private EOStructureUlr searchedGroupe;

	/**
	 * @return the isInSearch
	 */
	public boolean isInSearch() {
		return isInSearch;
	}

	/**
	 * @param isInSearch
	 *            the isInSearch to set
	 */
	public void setInSearch(boolean isInSearch) {
		this.isInSearch = isInSearch;
	}

	/**
	 * @return the searchedService
	 */
	public EOStructureUlr searchedService() {
		return searchedService;
	}

	/**
	 * @param searchedService
	 *            the searchedService to set
	 */
	public void setSearchedService(EOStructureUlr searchedService) {
		this.searchedService = searchedService;
	}

	public String aucselservice() {
		return getComponentId() + "_aucselservice";
	}

	public String tbvsearchid() {
		return getComponentId() + "_tbvsearchid";
	}

	/**
	 * @return the inSearchService
	 */
	public EOStructureUlr inSearchService() {
		return inSearchService;
	}

	/**
	 * @param inSearchService
	 *            the inSearchService to set
	 */
	public void setInSearchService(EOStructureUlr inSearchService) {
		this.inSearchService = inSearchService;
	}

	public WOActionResults selectService() {
		setSearchedService(inSearchService);
		return closeSearch();
	}

	public WOActionResults closeSearch() {
		setInSearch(false);
		AjaxModalDialog.close(context());
		AjaxUpdateContainer.updateContainerWithID(aucsearchedserviceid(), context());
		return null;
	}

	public String aucsearchedserviceid() {
		return getComponentId() + "_aucsearchedserviceid";
	}

	public WOActionResults searchService() {
		setInSearch(true);
		AjaxModalDialog.open(context(), camservicesearchid());
		return null;
	}

	public WOActionResults resetSearchService() {
		setSearchedService(null);
		return null;
	}

	public String auceditGroupeid() {
		return getComponentId() + "_auceditGroupeid";
	}

	/**
	 * @return the inSearchGroupe
	 */
	public EOStructureUlr inSearchGroupe() {
		return inSearchGroupe;
	}

	/**
	 * @param inSearchGroupe
	 *            the inSearchGroupe to set
	 */
	public void setInSearchGroupe(EOStructureUlr inSearchGroupe) {
		this.inSearchGroupe = inSearchGroupe;
	}

	public String aucaddgroupeid() {
		return getComponentId() + "_aucaddgroupeid";
	}

	public String tbvsearchdeposid() {
		return getComponentId() + "_tbvsearchdeposid";
	}

	public String camaddgroupeid() {
		return getComponentId() + "_camaddgroupeid";
	}

	public String aucsearchedgroupeid() {
		return getComponentId() + "_aucsearchedgroupeid";
	}

	public WOActionResults selectGroupe() {
		setSearchedGroupe(inSearchGroupe());
		return closeSearchGroupe();
	}

	public WOActionResults closeSearchGroupe() {
		setWindowGroupeVisible(Boolean.FALSE);
		CktlAjaxWindow.close(context(), camaddgroupeid());
		AjaxUpdateContainer.updateContainerWithID(aucsearchedgroupeid(), context());
		return null;
	}

	/**
	 * @return the isWindowGroupeVisible
	 */
	public Boolean isWindowGroupeVisible() {
		return isWindowGroupeVisible;
	}

	/**
	 * @param isWindowGroupeVisible
	 *            the isWindowGroupeVisible to set
	 */
	public void setWindowGroupeVisible(Boolean isWindowGroupeVisible) {
		this.isWindowGroupeVisible = isWindowGroupeVisible;
	}

	/**
	 * @return the searchedGroupe
	 */
	public EOStructureUlr searchedGroupe() {
		return searchedGroupe;
	}

	/**
	 * @param searchedGroupe
	 *            the searchedGroupe to set
	 */
	public void setSearchedGroupe(EOStructureUlr searchedGroupe) {
		this.searchedGroupe = searchedGroupe;
	}

	public WOActionResults searchGroupe() {
		setWindowGroupeVisible(Boolean.TRUE);
		CktlAjaxWindow.open(context(), camaddgroupeid());
		return null;
	}

	public WOActionResults resetSearchGroupe() {
		setSearchedGroupe(null);
		return null;
	}

	public WOActionResults resetSearch() {
		setSearchedAcces(null);
		setSearchedDepos(null);
		setSearchedDesc(null);
		setSearchedEtage(null);
		setSearchedGroupe(null);
		setSearchedImplantation(null);
		setSearchedLocal(null);
		setSearchedNom(null);
		setSearchedOccup(null);
		setSearchedPorte(null);
		setSearchedPrise(null);
		setSearchedTelephone(null);
		setSearchedService(null);
		setSearchedTypeSalle(null);
		return null;
	}

	public String auccriteresid() {
		return getComponentId() + "_auccriteresid";
	}

	private boolean haveSearched = false;
	private boolean salleReservable;

	public String noUpletsMessage() {
		if (haveSearched) {
			return "Recherche vide";
		}
		else {
			return "Saisissez des critères de recherche et cliquez sur \"Chercher\"";
		}

	}

	public boolean salleReservable() {
		return salleReservable;
	}

	public void setSalleReservable(boolean salleReservable) {
		this.salleReservable = salleReservable;
	}

}