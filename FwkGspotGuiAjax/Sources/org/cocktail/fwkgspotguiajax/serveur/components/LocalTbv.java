package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LocalTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String IMPLANTATION = EOLocal.TO_REPART_BAT_IMP_GEOS_KEY;
	private static final String CODE = EOLocal.C_LOCAL_KEY;
	private static final String APPELLATION = EOLocal.APPELLATION_KEY;
	private static final String DEBUT = EOImplantationGeo.D_DEB_VAL_KEY;
	private static final String FIN = EOImplantationGeo.D_FIN_VAL_KEY;

	public LocalTbv(WOContext context) {
		super(context);
	}

	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Implantation");
		col0.setOrderKeyPath(IMPLANTATION + "."
				+ EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY + "."
				+ EOImplantationGeo.LC_IMPLANTATION_GEO_KEY);
		col0.setComponent("ImplantationFromBatTbv");
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + IMPLANTATION, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, IMPLANTATION);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Code");
		col1.setOrderKeyPath(CODE);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CODE, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setHeaderCssStyle("text-align:center;width:50px;");
		col1.setRowCssStyle("text-align:center;");
		_colonnesMap.takeValueForKey(col1, CODE);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Appellation");
		col4.setOrderKeyPath(APPELLATION);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + APPELLATION, "emptyValue");
		ass4.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col4.setAssociations(ass4);
		col4.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col4, APPELLATION);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Début validitié");
		col2.setOrderKeyPath(DEBUT);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + DEBUT, "emptyValue");
		ass2.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		ass2.setDateformat("%d/%m/%Y");
		col2.setAssociations(ass2);
		col2.setHeaderCssStyle("width:80px;");
		col2.setRowCssStyle("text-align:center;");
		_colonnesMap.takeValueForKey(col2, DEBUT);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Fin validitié");
		col3.setOrderKeyPath(FIN);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + FIN, "emptyValue");
		ass3.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		ass3.setDateformat("%d/%m/%Y");
		col3.setAssociations(ass3);
		col3.setHeaderCssStyle("width:80px;");
		col3.setRowCssStyle("text-align:center;");
		_colonnesMap.takeValueForKey(col3, FIN);

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Action");
		col6.setComponent("TableAction");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY, "");
		ass6.setObjectForKey("idToRefreshEdit", "idToRefreshEdit");
		ass6.setObjectForKey("idToRefreshDelete", "idToRefreshDelete");
		ass6.setObjectForKey("getDeletedObjects",
				TableAction.LST_DELETED_OBJECTS_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_NAME_KEY,
				TableAction.EDIT_METHODE_NAME_KEY);
		ass6.setObjectForKey(TableAction.EDIT_METHODE_OBJECT_KEY,
				TableAction.EDIT_METHODE_OBJECT_KEY);
		ass6.setObjectForKey("onSuccessEdit", "onSuccessEdit");
		ass6.setObjectForKey("isEditEnabled", "isEditEnabled");
		ass6.setObjectForKey("canEdit", "canEdit");
		col6.setAssociations(ass6);
		col6.setHeaderCssStyle("width:45px;");
		col6
				.setRowCssStyle("text-align:center;background-image:none !important;");
		//_colonnesMap.takeValueForKey(col6, OBJ_KEY + ".action");
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(new String[] { IMPLANTATION, CODE,
				APPELLATION, DEBUT, FIN, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			NSMutableArray<EOGenericRecord> deletedObj = new NSMutableArray<EOGenericRecord>();
			for (EOGenericRecord delDetail : getDeletedObjects()) {
				if (((EOLocal) delDetail).toSalles().size() > 0) {
					UtilMessages
							.creatMessageUtil(session(),
									UtilMessages.ERROR_MESSAGE,
									"Bâtiment utilisé par des salles, supression impossible");
				} else {

					deletedObj.addObject(delDetail);

					// delete des REPART_BAT_IMP_GEOS
					NSArray<EORepartBatImpGeo> lstRepart = (NSArray<EORepartBatImpGeo>) ((EOLocal) delDetail)
							.toRepartBatImpGeos().clone();
					for (EORepartBatImpGeo repart : lstRepart) {

						if (repart != null) {
							repart
									.removeObjectFromBothSidesOfRelationshipWithKey(
											repart.toLocal(),
											EORepartBatImpGeo.TO_LOCAL_KEY);
							repart
									.removeObjectFromBothSidesOfRelationshipWithKey(
											repart.toImplantationGeo(),
											EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY);
							repart.editingContext().deleteObject(repart);
						}
					}

					EOAdresse adrsBat = ((EOLocal) delDetail)
							.toFwkpers_Adresse();

					((EOLocal) delDetail)
							.removeObjectFromBothSidesOfRelationshipWithKey(
									adrsBat, EOLocal.TO_FWKPERS__ADRESSE_KEY);
					//adrsBat.editingContext().deleteObject(adrsBat);

					delDetail.editingContext().deleteObject(delDetail);
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
			getDeletedObjects().removeObjectsInArray(deletedObj);
		}

		return null;
	}
}