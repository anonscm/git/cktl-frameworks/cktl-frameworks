package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;

public class GestionTypeSalle extends BaseGestionRef {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestionTypeSalle(WOContext context) {
		super(context);
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		try {
			getEditedType().validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			//AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
			System.out.println("erreur");
			return null;
		}
		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedType().tsalLibelle());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Type de salle créé ");
			setEditedObj(EOTypeSalle.createFwkGspot_TypeSalle(new EOEditingContext(
					ec()), new NSTimestamp(), new NSTimestamp()));

		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addType() {
		setEditedObj(EOTypeSalle.createFwkGspot_TypeSalle(new EOEditingContext(
				ec()), new NSTimestamp(), new NSTimestamp()));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'un type de salle");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOTypeSalle) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'un type de salle");
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = org.cocktail.fwkcktlpersonne.common.metier.EOTypeSalle.TSAL_LIBELLE_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
		}

		return EOTypeSalle
				.fetchFwkGspot_TypeSalles(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOTypeSalle.TSAL_LIBELLE_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOTypeSalle getEditedType() {
		return (EOTypeSalle) getEditedObj();
	}
}