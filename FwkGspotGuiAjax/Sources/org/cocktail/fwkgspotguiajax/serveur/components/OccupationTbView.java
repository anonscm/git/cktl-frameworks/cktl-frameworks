package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

public class OccupationTbView extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SERVICE = EODetailPourcentage.TO_STRUCTURE_ULR_KEY+"."+EOStructureUlr.LC_STRUCTURE_KEY;
	private static final String PCT = EODetailPourcentage.DET_POURCENTAGE_KEY;
	private static final String TYPE = EODetailPourcentage.TO_TYPE_OCCUPATION_KEY+"."+EOTypeOccupation.TOC_LIBELLE_KEY;
	
	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Service");
		col0.setOrderKeyPath(SERVICE);		
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + SERVICE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, SERVICE);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("%");
		col1.setOrderKeyPath(PCT);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + PCT, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:center;");
		col1.setHeaderCssStyle("width:30px;text-align:center;");
		_colonnesMap.takeValueForKey(col1, PCT);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Type");
		col2.setOrderKeyPath(TYPE);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + TYPE, "emptyValue");
		ass3.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col2.setAssociations(ass3);
		col2.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col2, TYPE);
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}
    public OccupationTbView(WOContext context) {
        super(context);
    }

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { SERVICE, PCT,TYPE,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delDetail : getDeletedObjects()) {
				((EODetailPourcentage) delDetail)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EODetailPourcentage) delDetail).toSalles(),
								EODetailPourcentage.TO_SALLES_KEY);
				((EODetailPourcentage) delDetail)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EODetailPourcentage) delDetail)
										.toStructureUlr(),
								EODetailPourcentage.TO_STRUCTURE_ULR_KEY);
				((EODetailPourcentage) delDetail)
				.removeObjectFromBothSidesOfRelationshipWithKey(
						((EODetailPourcentage) delDetail)
								.toTypeOccupation(),
						EODetailPourcentage.TO_TYPE_OCCUPATION_KEY);
				delDetail.editingContext().deleteObject(delDetail);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}
}