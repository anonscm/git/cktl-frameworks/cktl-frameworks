package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestAccesHandicap extends BaseGestionRef {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestAccesHandicap(WOContext context) {
		super(context);
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		if ((getEditedType().libelle() == null)
				|| ("".equals(getEditedType().libelle()))) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, "Le libellé est obligatoire !");
			return null;
		}
		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedType().libelle());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Type accès handicapé créé ");
			setEditedObj(EOTypeAccesHandicape
					.createFwkGspot_TypeAccesHandicape(new EOEditingContext(ec()),""));

		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addType() {
		setEditedObj(EOTypeAccesHandicape
				.createFwkGspot_TypeAccesHandicape(new EOEditingContext(ec()),""));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'un type acces handicape");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOTypeAccesHandicape) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'un type acces handicape");
		return null;
	}

	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = EOTypeAccesHandicape.LIBELLE_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
		}

		return EOTypeAccesHandicape
				.fetchFwkGspot_TypeAccesHandicapes(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOTypeAccesHandicape.LIBELLE_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOTypeAccesHandicape getEditedType() {
		return (EOTypeAccesHandicape) getEditedObj();
	}
}