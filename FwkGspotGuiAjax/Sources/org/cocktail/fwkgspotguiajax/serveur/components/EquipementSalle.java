package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUtils;
import er.extensions.foundation.ERXStringUtilities;

public class EquipementSalle extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WODisplayGroup displayGroup;

	public EquipementSalle(WOContext context) {
		super(context);
	}

	public String aucequipementid() {
		if (hasBinding("lstequipementid"))
			return (String) valueForBinding("lstequipementid");
		return getComponentId() + "_aucequipementid";
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			displayGroup.setNumberOfObjectsPerBatch(10);
		}

		if (isNeedReset())
			refreshObjs();
		return displayGroup;
	}

	public WOActionResults refreshObjs() {
		if (selectedSalle()==null) return null;
		EOQualifier qualObj = null;
		if (searchFamille() != null)
			qualObj = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.TO_RESA_TYPE_OBJET_KEY + "."
							+ EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY
							+ " =%@ ", new NSArray<EOResaFamilleObjet>(
							searchFamille()));

		if (searchType() != null) {
			EOQualifier qualType = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.TO_RESA_TYPE_OBJET_KEY + " =%@ ",
					new NSArray<EOResaTypeObjet>(searchType()));
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualType }));
			else
				qualObj = qualType;
		}
		
		if ("O".equals(searchReservable)) {
			EOQualifier qualRes = EOQualifier.qualifierWithQualifierFormat(
					EOResaObjet.RO_RESERVABLE_KEY + " =%@ ",
					new NSArray<String>(searchReservable));
			if (qualObj != null)
				qualObj = new EOAndQualifier(new NSArray<EOQualifier>(
						new EOQualifier[] { qualObj, qualRes }));
			else
				qualObj = qualRes;
		}
		
		
		displayGroup
				.setObjectArray(selectedSalle()
						.toResaObjets(
								qualObj,
								new NSArray<EOSortOrdering>(
										new EOSortOrdering[] { new EOSortOrdering(
												EOResaObjet.TO_RESA_TYPE_OBJET_KEY
														+ "."
														+ EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY
														+ "."
														+ EOResaFamilleObjet.RFO_LIBELLE_KEY,
												EOSortOrdering.CompareCaseInsensitiveAscending) }),
								false));
		displayGroup.clearSelection();
		return null;

	}

	public boolean canEditEquipement() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}
	
	public boolean canMoveEquipement() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}
	
	private NSArray<String> lstColonnesKeys;

	/**
	 * @return the lstColonnesKeys
	 */
	public NSArray<String> lstColonnesKeys() {
		if (lstColonnesKeys == null)
			lstColonnesKeys = new NSArray<String>(new String[] {
					ObjetTbv.FAMILLE, ObjetTbv.TYPE, ObjetTbv.LIBELLE1, ObjetTbv.LIBELLE2,
					ObjetTbv.CODE_BARRE, ObjetTbv.RESERVABLE, ObjetTbv.OBJ_KEY + ".action" });
		return lstColonnesKeys;
	}

	/**
	 * @param lstColonnesKeys
	 *            the lstColonnesKeys to set
	 */
	public void setLstColonnesKeys(NSArray<String> lstColonnesKeys) {
		this.lstColonnesKeys = lstColonnesKeys;
	}

	public String aucaddequipementid() {
		return getComponentId() + "_aucaddequipementid";
	}
	
	public String aucmoveequipementid() {
		return getComponentId() + "_aucmoveequipementid";
	}
	

	public String cawaddequipementid() {
		return getComponentId() + "_cawaddequipementid";
	}
	
	public String cawmoveequipementid() {
		return getComponentId() + "_cawmoveequipementid";
	}
	
	public String cadSearchid() {
		return getComponentId() + "_cadSearchid";
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	private NSArray<EOResaFamilleObjet> lstFamilles;

	/**
	 * @return the lstFamilles
	 */
	public NSArray<EOResaFamilleObjet> lstFamilles() {
		if (lstFamilles == null)
			lstFamilles = EOResaFamilleObjet
					.fetchAllFwkGspot_ResaFamilleObjets(
							ec(),
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOResaFamilleObjet.RFO_LIBELLE_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstFamilles;
	}

	/**
	 * @param lstFamilles
	 *            the lstFamilles to set
	 */
	public void setLstFamilles(NSArray<EOResaFamilleObjet> lstFamilles) {
		this.lstFamilles = lstFamilles;
	}

	private EOResaFamilleObjet fammilleOccur;

	/**
	 * @return the fammilleOccur
	 */
	public EOResaFamilleObjet fammilleOccur() {
		return fammilleOccur;
	}

	/**
	 * @param fammilleOccur
	 *            the fammilleOccur to set
	 */
	public void setFammilleOccur(EOResaFamilleObjet fammilleOccur) {
		this.fammilleOccur = fammilleOccur;
	}

	private EOResaObjet editedObj;

	public EOResaObjet editedObj() {
		return editedObj;
	}

	public void setEditedObj(EOResaObjet editedObj) {
		this.editedObj = editedObj;
	}

	private EOResaFamilleObjet selectedFamille;

	/**
	 * @return the selectedFamille
	 */
	public EOResaFamilleObjet selectedFamille() {
		if ((selectedFamille == null)
				&& (editedObj().toResaTypeObjet() != null))
			selectedFamille = editedObj().toResaTypeObjet()
					.toResaFamilleObjet();

		if ((selectedFamille == null) && (searchFamille() != null))
			selectedFamille = searchFamille();
		return selectedFamille;
	}

	/**
	 * @param selectedFamille
	 *            the selectedFamille to set
	 */
	public void setSelectedFamille(EOResaFamilleObjet selectedFamille) {
		this.selectedFamille = selectedFamille;
	}

	public NSArray<EOResaTypeObjet> lstTypeObj() {
		if (selectedFamille() != null)
			return selectedFamille().toResaTypeObjets();
		return null;
	}

	public NSArray<EOResaTypeObjet> lstTypeObjSearch() {
		if (searchFamille() != null)
			return searchFamille().toResaTypeObjets();
		return null;
	}

	private EOResaTypeObjet typeOccur;

	/**
	 * @return the typeOccur
	 */
	public EOResaTypeObjet typeOccur() {
		return typeOccur;
	}

	/**
	 * @param typeOccur
	 *            the typeOccur to set
	 */
	public void setTypeOccur(EOResaTypeObjet typeOccur) {
		this.typeOccur = typeOccur;
	}

	public WOActionResults validAjout() {
		if (editedObj() != null) {

			if ((editedObj().roLibelle1() == null)
					|| ("".equals(editedObj().roLibelle1()))) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"Le libellé est obligatoire !");
				return null;
			}

			if (editedObj().toResaTypeObjet() == null) {
				UtilMessages.creatMessageUtil(session(),
						UtilMessages.ERROR_MESSAGE,
						"Le type objet est obligatoire !");
				return null;
			}
			if ((editedObj().roLibelle2() != null)
					&& (editedObj().roLibelle2().length() > 4000))
				editedObj().setRoLibelle2(
						editedObj().roLibelle2().substring(0, 4000));

			int mode = -1;
			if (editedObj().editingContext().insertedObjects().size() > 0)
				mode = 0;
			else
				mode = 1;
			if (isCommitOnValid()) {
				editedObj().editingContext().saveChanges();
				if (editedObj().editingContext().parentObjectStore().equals(
						session().defaultEditingContext()))
					session().defaultEditingContext().saveChanges();
			}
			refreshObjs();
			if (mode == 0) {

				UtilMessages.creatMessageUtil(session(),
						UtilMessages.INFO_MESSAGE, "Equipement créé ");
				setEditedObj(creatNewEquipementForSalle());
			} else {
				closeEditObj();
			}

		}
		return null;
	}

	public String deleteTrigId() {
		return getComponentId() + "_deleteTrigId";
	}

	private NSArray<String> lstEditZones;

	public NSArray<String> getLstEditZones() {
		if (lstEditZones == null) {
			lstEditZones = new NSArray<String>(new String[] {
					aucequipementid(), aucaddequipementid(), aucerreurid() });
		}
		return lstEditZones;
	}

	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	public WOActionResults closeEditObj() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), cawaddequipementid());
		return null;
	}

	public WOActionResults addObj() {
		setWindowVisible(true);
		setEditedObj(creatNewEquipementForSalle());
		CktlAjaxWindow.open(context(), cawaddequipementid(),
				"Création d'un objet");
		return null;
	}

	private EOResaObjet creatNewEquipementForSalle() {
		EOEditingContext newEc = new EOEditingContext(ec());
		EOResaObjet obj = EOResaObjet.createFwkGspot_ResaObjet(newEc, "",
				(EOResaTypeObjet) EOUtilities.localInstanceOfObject(newEc,
						searchType()));
		obj.setToSallesRelationship((EOSalles) EOUtilities
				.localInstanceOfObject(obj.editingContext(), selectedSalle()));
		return obj;
	}

	public String auctypeobjid() {
		return getComponentId() + "_auctypeobjid";
	}

	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}

	public WOActionResults editObj(Object obj) {
		setEditedObj((EOResaObjet) obj);
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), cawaddequipementid(),
				"Modification d'un objet");
		return null;
	}
	
	public String pubfiltrefamilleid() {
		return getComponentId() + "_pubfiltrefamilleid";
	}

	public String pubfiltretypeid() {
		return getComponentId() + "_pubfiltretypeid";
	}

	private EOResaFamilleObjet searchFamille;

	/**
	 * @return the searchFamille
	 */
	public EOResaFamilleObjet searchFamille() {
		return searchFamille;
	}

	/**
	 * @param searchFamille
	 *            the searchFamille to set
	 */
	public void setSearchFamille(EOResaFamilleObjet searchFamille) {
		this.searchFamille = searchFamille;
	}

	private EOResaTypeObjet searchType;

	/**
	 * @return the searchType
	 */
	public EOResaTypeObjet searchType() {
		return searchType;
	}

	/**
	 * @param searchType
	 *            the searchType to set
	 */
	public void setSearchType(EOResaTypeObjet searchType) {
		this.searchType = searchType;
	}

	public String searchtrigid() {
		return getComponentId() + "_searchtrigid";

	}

	private NSArray<String> lstSearchZones;
	private String searchReservable;

	public NSArray<String> lstSearchZones() {
		if (lstSearchZones == null) {
			lstSearchZones = new NSArray<String>(new String[] {
					aucequipementid(), auctypesearchid() });
		}
		return lstSearchZones;
	}

	public String auctypesearchid() {
		return getComponentId() + "_auctypesearchid";

	}

	public String aucWindowId() {
		return getComponentId() + "_aucWindowId";

	}

	public String typeNoSelection() {
		if ((lstTypeObj()!=null)&&(lstTypeObj().count()>0)){
			return null;
		} else {
			return "-- Choisissez une famille --";
		}
	}

	/**
	 * @return the searchReservable
	 */
	public String searchReservable() {
		return searchReservable;
	}

	/**
	 * @param searchReservable the searchReservable to set
	 */
	public void setSearchReservable(String searchReservable) {
		this.searchReservable = searchReservable;
	}
	
	
   /**
    * 
    */
	private Boolean searchVisible = Boolean.FALSE;

	/**
	 * @return the searchVisible
	 */
	public boolean searchVisible() {
		return searchVisible;
	}

	/**
	 * @param searchVisible
	 *            the searchVisible to set
	 */
	public void setSearchVisible(boolean searchVisible) {
		this.searchVisible = searchVisible;

	}
	
	public CktlAjaxWOComponent moveMethodeObject() {
		return this;
	}

	public WOActionResults moveObj(Object obj) {
		setEditedObj((EOResaObjet) obj);
		openWinSearch();
		return null;
	}

	public WOActionResults openWinSearch() {
	    CktlAjaxWindow.open(context(), cadSearchid(),"Salle destination pour '"+editedObj.roLibelle1()+"'");
	    setSearchVisible(true);
		return null;
	}
	
	private String divSearchId = ERXStringUtilities
			.safeIdentifierName("searchSalle" + UUID.randomUUID());

	/**
	 * @return the divSearchId
	 */
	public String divSearchId() {
		return divSearchId;
	}

	/**
	 * @param divSearchId
	 *            the divSearchId to set
	 */
	public void setDivSearchId(String divSearchId) {
		this.divSearchId = divSearchId;
	}

	
	private SallesTreeDelegate treeDelegate;   
	public SallesTreeDelegate getTreeDelegate() {

		if ((treeDelegate != null)
				&& (treeDelegate instanceof SallesTreeDelegate)) {

		} else {
			treeDelegate = new SallesTreeDelegate(session()
					.defaultEditingContext());

		}

		return treeDelegate;
	}

	public void setTreeDelegate(SallesTreeDelegate treeDelegate) {
		this.treeDelegate = treeDelegate;
	}
	
	private Boolean closeOnSelect=Boolean.TRUE;
	
	/**
	 * @return the closeOnSelect
	 */
	public Boolean closeOnSelect() {
		return closeOnSelect;
	}

	/**
	 * @param closeOnSelect the closeOnSelect to set
	 */
	public void setCloseOnSelect(Boolean closeOnSelect) {
		this.closeOnSelect = closeOnSelect;
	}
	
	public WOActionResults onSelectSalle() {
		if (closeOnSelect()){
			CktlAjaxWindow.close(context(), cadSearchid());
			setSearchVisible(false);
		}
		if (editedObj()!=null && toSalle!=null){
	     	editedObj().setToSallesRelationship(toSalle);
	      try {
			editedObj().editingContext().saveChanges();
			refreshObjs();
		  } catch (Exception e) {
			e.printStackTrace();
		  }  
	    }
		return null;
	}
	
	private EOSalles toSalle;

	/**
	 * @return the selSalle
	 */
	public EOSalles getToSalle() {
		return toSalle;
	}

	/**
	 * @param selSalle the selSalle to set
	 */
	public void setToSalle(EOSalles selSalle) {
		this.toSalle = selSalle;
	}
	
}