package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.extensions.foundation.ERXStringUtilities;

public class DeposIndiv extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NEED_LOCAL_REFRESH = "needLocalRefresh";
	private WODisplayGroup displayGroup;

	public DeposIndiv(WOContext context) {
		super(context);
	}

	public String auclstindid() {
		return getComponentId() + "_auclstindid";
	}

	public WODisplayGroup displayGroup() {
		if (hasBinding("displayGroup")) {
			displayGroup = (WODisplayGroup) valueForBinding("displayGroup");
		}
		else {
			if (displayGroup == null) {
				displayGroup = new WODisplayGroup();
			}

			if (isNeedReset() || needLocalRefresh()) {
				refreshInds();
			}
		}
		return displayGroup;
	}

	public Boolean needLocalRefresh() {
		return (Boolean) valueForBinding(NEED_LOCAL_REFRESH);
	}

	public WOActionResults refreshInds() {
		if (!hasBinding("displayGroup")) {
			dicoDepositaires.removeObjectForKey(selectedSalle());
			displayGroup.setObjectArray(getDepositaires());
			displayGroup.clearSelection();
		}
		else {
			// pour le refresh de la table details
			setValueForBinding(null, "displayGroup");
		}
		if (canSetValueForBinding(DeposIndiv.NEED_LOCAL_REFRESH)) {
			setValueForBinding(Boolean.FALSE, DeposIndiv.NEED_LOCAL_REFRESH);
		}
		return null;
	}

	private NSMutableDictionary<EOSalles, NSArray<EORepartAssociation>> dicoDepositaires = new NSMutableDictionary<EOSalles, NSArray<EORepartAssociation>>();

	public NSArray<EORepartAssociation> getDepositaires() {
		if (dicoDepositaires.objectForKey(selectedSalle()) == null) {
			dicoDepositaires.setObjectForKey(FinderDepositaires.getDepositaireIndivForSalle(ec(), selectedSalle()), selectedSalle());
		}

		return dicoDepositaires.objectForKey(selectedSalle());
	}

	public boolean canEditDepos() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	public WOActionResults addDepos() {
		setWindowVisible(true);
		displayGroupSearch = null;
		selectedInd = null;
		AjaxModalDialog.open(context(), camadddeposid());
		return null;
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	public String camadddeposid() {
		return getComponentId() + "_camadddeposid";
	}

	public String aucadddeposid() {
		return getComponentId() + "_aucadddeposid";
	}

	private EOIndividu selectedInd;

	private WODisplayGroup displayGroupSearch;

	/**
	 * @return the displayGroupSearch
	 */
	public WODisplayGroup displayGroupSearch() {
		if (displayGroupSearch == null) {
			displayGroupSearch = new WODisplayGroup();
			getIndsForService();
			displayGroupSearch.setDelegate(new DgDelegate());
		}
		return displayGroupSearch;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedInd((EOIndividu) group.selectedObject());
		}
	}

	/**
	 * @param displayGroupSearch
	 *            the displayGroupSearch to set
	 */
	public void setDisplayGroupSearch(WODisplayGroup displayGroupSearch) {
		this.displayGroupSearch = displayGroupSearch;
	}

	public EOIndividu getSelectedInd() {
		return selectedInd;
	}

	public void setSelectedInd(EOIndividu selectedInd) {
		this.selectedInd = selectedInd;
	}

	public String aucaddinddeposid() {
		return getComponentId() + "_aucaddinddeposid";
	}

	private EOStructure selectedService;

	/**
	 * @return the selectedService
	 */
	public EOStructure selectedService() {
		if (hasBinding("selectedService")) {
			selectedService = ((EOStructureUlr) valueForBinding("selectedService")).toFwkpers_Structure();
		}
		if (selectedService == null) {
			selectedService = ((lstService() != null) && (lstService().size() > 0) ? lstService().objectAtIndex(0) : null);
		}
		return selectedService;
	}

	/**
	 * @param selectedService
	 *            the selectedService to set
	 */
	public void setSelectedService(EOStructure selectedService) {
		if (canSetValueForBinding("selectedService")) {
			setValueForBinding(selectedService, "selectedService");
		}
		this.selectedService = selectedService;
	}

	public WOActionResults validAjout() {
		if (FinderDepositaires.isDepositaireForSalle(ec(), getSelectedInd().persId(), selectedSalle())) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE, "Cet individu est déjà dépositaire de la salle !!");
			return null;
		}

		if (getDeposAssoc() == null) {
			deposAssoc = EOAssociation.createEOAssociation(ec(), FinderDepositaires.ASS_CODE_DEPOS, "Dépositaire de salle");
			//deposAssoc.setAssCode(FinderDepositaires.ASS_CODE_DEPOS);
			deposAssoc.setToTypeAssociationRelationship(EOTypeAssociation.fetchRequiredByKeyValue(ec(), EOTypeAssociation.TAS_CODE_ROLE, "ROLE"));
		}

		getSelectedInd().setPersIdModification(getUtilisateurPersId());
		getSelectedInd().definitUnRole(ec(), getDeposAssoc(), selectedService(), getUtilisateurPersId(), null, null, null, null, null);
		if (isCommitOnValid()) {
			ec().saveChanges();
		}

		setWindowVisible(false);
		refreshInds();
		AjaxUpdateContainer.updateContainerWithID(aucTriggerAjoutMode(), context());
		AjaxModalDialog.close(context());
		return null;
	}

	private EOAssociation deposAssoc;

	public EOAssociation getDeposAssoc() {
		if (deposAssoc == null) {
			deposAssoc = EOAssociation.fetchRequiredByKeyValue(ec(), EOAssociation.ASS_CODE_KEY, FinderDepositaires.ASS_CODE_DEPOS);
		}
		return deposAssoc;
	}

	public String aucTriggerAjoutMode() {
		return getComponentId() + "_aucTriggerAjoutMode";
	}

	private NSArray<String> lstAjoutZones;

	public NSArray<String> getLstAjoutZones() {
		if (lstAjoutZones == null) {
			if (parentIdToRefresh() != null) {
				lstAjoutZones = new NSArray<String>(new String[] { aucadddeposid(), auclstindid(), parentIdToRefresh() });
			}
			else {
				lstAjoutZones = new NSArray<String>(new String[] { aucadddeposid(), auclstindid() });
			}
		}
		return lstAjoutZones;
	}

	public WOActionResults closeAddInd() {
		setWindowVisible(false);
		// CktlAjaxWindow.close(context(), camadddeposid());
		AjaxModalDialog.close(context());
		return null;
	}

	public NSArray<EOStructure> lstService() {
		if (hasBinding("selectedService")) {
			return new NSArray<EOStructure>(((EOStructureUlr) valueForBinding("selectedService")).toFwkpers_Structure());
		}
		else {
			return (NSArray<EOStructure>) selectedSalle().toDetailPourcentages().valueForKeyPath(
					EODetailPourcentage.TO_STRUCTURE_ULR_KEY + "." + EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY);
		}
	}

	private EOStructure serviceOccur;

	/**
	 * @return the serviceOccur
	 */
	public EOStructure serviceOccur() {
		return serviceOccur;
	}

	/**
	 * @param serviceOccur
	 *            the serviceOccur to set
	 */
	public void setServiceOccur(EOStructure serviceOccur) {
		this.serviceOccur = serviceOccur;
	}

	public WOActionResults getIndsForService() {
		if (selectedService() == null) {
			return null;
		}

		String qualStr = EOIndividu.TO_REPART_STRUCTURES_KEY + "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "=%@";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>(selectedService());

		if ((searchedName != null) && (!"".equals(searchedName.trim()))) {
			qualStr += " AND (" + EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@ OR " + EOIndividu.NOM_PATRONYMIQUE_KEY
					+ " caseInsensitiveLike %@)";
			qualArray.add("*" + searchedName + "*");
			qualArray.add("*" + searchedName + "*");
		}

		displayGroupSearch().setObjectArray(
				EOIndividu.fetchAll(ec(), EOQualifier.qualifierWithQualifierFormat(qualStr, qualArray), new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareCaseInsensitiveAscending) })));
		displayGroupSearch.clearSelection();

		return null;
	}

	public String auclstindivserviceid() {
		return getComponentId() + "_auclstindivserviceid";
	}

	public String aucmessageid() {
		return getComponentId() + "_aucmessageid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	private EORepartAssociation deposOccur;

	/**
	 * @return the deposOccur
	 */
	public EORepartAssociation deposOccur() {
		return deposOccur;
	}

	/**
	 * @param deposOccur
	 *            the deposOccur to set
	 */
	public void setDeposOccur(EORepartAssociation deposOccur) {
		this.deposOccur = deposOccur;
	}

	public String detailPersonneModalBoxId() {
		return getComponentId() + "_detailPersonneModalBoxId";
	}

	public boolean isDepositaire() {
		for (Object repart : selectedService().toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.dot(EOAssociation.ASS_CODE).eq(FinderDepositaires.ASS_CODE_DEPOS))) {
			if (((EORepartAssociation) repart).toIndividusAssocies().contains(getSelectedInd())) {
				return true;
			}
		}
		return false;
	}

	private String searchedName;

	/**
	 * @return the searchedName
	 */
	public String searchedName() {
		return searchedName;
	}

	/**
	 * @param searchedName
	 *            the searchedName to set
	 */
	public void setSearchedName(String searchedName) {
		this.searchedName = searchedName;
	}

	private String buzzyid = ERXStringUtilities.safeIdentifierName("buzzyid" + UUID.randomUUID());

	/**
	 * @return the buzzyid
	 */
	public String buzzyid() {
		return buzzyid;
	}

	public String aucTriggersearch() {
		return getComponentId() + "_aucTriggersearchid";
	}

	private NSArray<String> lstSearchZones;

	public NSArray<String> lstSearchZones() {
		if (lstSearchZones == null) {
			lstSearchZones = new NSArray<String>(new String[] { auclstindivserviceid(), aucaddinddeposid() });
		}
		return lstSearchZones;
	}
}