package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.AjaxUpdateContainer;

public class SalleTelephone extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SalleTelephone(WOContext context) {
		super(context);
	}

	public String aucLstTelid() {
		if (hasBinding("idTbvTels"))
			return (String) valueForBinding("idTbvTels");
		return getComponentId() + "_aucLstTelid";
	}

	private WODisplayGroup displayGroup;

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			//displayGroup.setNumberOfObjectsPerBatch(5);
			// displayGroup.setDelegate(new DgDelegate());
		}

		if (isNeedReset())
			refreshTels();
		return displayGroup;
	}

	public WOActionResults refreshTels() {
		displayGroup.setObjectArray(selectedSalle().toSalleTelephones());
		displayGroup.clearSelection();
		return null;
	}

	public WOActionResults addTel() {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddtelid());
		EOEditingContext ec = new EOEditingContext(session()
				.defaultEditingContext());
		editedTel = EOSalleTelephone.createFwkGspot_SalleTelephone(ec,
				null, selectedSalle.localInstanceIn(ec));

		return null;
	}

	public String camaddtelid() {
		return getComponentId() + "_camaddtelid";
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}

	private EOSalleTelephone editedTel;

	public void setEditedTel(EOSalleTelephone editedTel) {
		this.editedTel = editedTel;
	}

	public EOSalleTelephone getEditedTel() {
		return editedTel;
	}

	public String idTbvTels() {
		return getComponentId() + "_idTbvTels";
	}

	public boolean canEditTels() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}

	public WOActionResults editTel(Object tel) {
		setWindowVisible(true);
		CktlAjaxWindow.open(context(), camaddtelid(),
				"Modification d'un téléphone");
		editedTel = (EOSalleTelephone) tel;
		return null;
	}

	public String aucedittelid() {
		return getComponentId() + "_aucedittelid";
	}

	public WOActionResults validAjout() {
		try {
			editedTel.validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
			return null;
		}
		if (isCommitOnValid()) {
			editedTel.editingContext().saveChanges();
			if (editedTel.editingContext().parentObjectStore()
					.equals(session().defaultEditingContext())) {
				session().defaultEditingContext().saveChanges();
			}
		}
		refreshTels();
		AjaxUpdateContainer.updateContainerWithID(aucLstTelid(), context());
		if (parentIdToRefresh() != null)
			AjaxUpdateContainer.updateContainerWithID(parentIdToRefresh(),
					context());
		closeAddTels();

		return null;
	}

	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	public WOActionResults closeAddTels() {
		setWindowVisible(false);
		CktlAjaxWindow.close(context(), camaddtelid());
		return null;
	}

	public WOActionResults formatPhoneNumber() {		
		editedTel.setNoTelephone(MyStringCtrl.formatPhoneNumber(editedTel.noTelephone()));		
		return null;
	}

	public String telephoneId() {
		return getComponentId() + "_telephoneId";
	}

}