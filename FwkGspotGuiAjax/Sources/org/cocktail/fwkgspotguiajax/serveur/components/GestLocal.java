package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class GestLocal extends BaseGestionRef {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String BINDING_PERSID = "utilisateurPersId";
    
	public GestLocal(WOContext context) {
		super(context);
	}

	private String searchLib;
	private EOAdresse editAdresse;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		if ((selectedImpEdit() == null) || ("".equals(getEditedBat().cLocal()))) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE,
					"L'implantation est obligatoire !");
			return null;
		}
		
		try {
			getEditedBat().validateObjectMetier();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			return null;
		}

		// validation de l'adresse
		try {
			editAdresse.validateObjectMetier();
		} catch (NSValidation.ValidationException e) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE, e.getMessage());
			return null;
		}

		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0) {
			mode = 0;
			creatNewRepartBatImpGeo();
		} else {
			mode = 1;
			// modification, verfi de la localisation
			boolean find = false;
			for (EORepartBatImpGeo rep : getEditedBat().toRepartBatImpGeos()) {
				if (rep.toImplantationGeo().equals(selectedImpEdit)) {
					find = true;
					break;
				}
			}
			if (!find) {
				// delete de la localisation pour mettre la nouvelle
				EORepartBatImpGeo rep = getEditedBat().toRepartBatImpGeos()
						.lastObject();
				if ((rep != null)
						&& (!selectedImpEdit().equals(rep.toImplantationGeo()))) {
					rep.removeObjectFromBothSidesOfRelationshipWithKey(rep
							.toImplantationGeo(), rep.TO_IMPLANTATION_GEO_KEY);
					rep.removeObjectFromBothSidesOfRelationshipWithKey(rep
							.toLocal(), rep.TO_LOCAL_KEY);
					rep.editingContext().deleteObject(rep);
				}
				creatNewRepartBatImpGeo();
			}

		}

		editAdresse.editingContext().saveChanges();
		getEditedBat().setToFwkpers_AdresseRelationship(
				(EOAdresse) EOUtilities.localInstanceOfObject(getEditedBat()
						.editingContext(), editAdresse)); 

		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedBat().appellation());
			setSelectedImplantation(null);
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Batiment créé ");
			creatNewBat();
		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	private void creatNewRepartBatImpGeo() {
		EORepartBatImpGeo repImp = EORepartBatImpGeo
				.createFwkGspot_RepartBatImpGeo(
						getEditedBat().editingContext(),
						(EOImplantationGeo) EOUtilities.localInstanceOfObject(
								getEditedBat().editingContext(),
								selectedImpEdit()), getEditedBat());
		repImp.setDCreation(new NSTimestamp());
		repImp.setDModification(new NSTimestamp());

	}

	private void creatNewBat() {
		EOEditingContext tmpEc = new EOEditingContext(ec());
		EOEditingContext adrsEc = new EOEditingContext();
		editAdresse = EOAdresse.creerInstance(adrsEc);
		editAdresse.setPersIdModification((Integer)valueForBinding(BINDING_PERSID));
		editAdresse.setToPaysRelationship(EOPays.getPaysDefaut(adrsEc));
		setEditedObj(EOLocal.createFwkGspot_Local(tmpEc, null, null,
				new NSTimestamp(), new NSTimestamp(), new NSTimestamp()));

		//getEditedBat().setToFwkpers_AdresseRelationship(editAdresse);
	}

	public WOActionResults addBat() {

		creatNewBat();
		selectedImpEdit = null;
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(), "Ajout d'un batiment");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOLocal) disc);
		setEdited(Boolean.TRUE);
		editAdresse = null;
		selectedImpEdit = null;
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'un batiment");
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if (selectedImplantation() != null) {
			qualStr = EOLocal.TO_REPART_BAT_IMP_GEOS_KEY + "."
					+ EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY + " = %@ ";
			qualArray.add(selectedImplantation());
		}

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			if (qualStr.length()>0) qualStr +=" and  ";
			qualStr += EOLocal.APPELLATION_KEY + " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
		}

		return EOLocal
				.fetchFwkGspot_Locals(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOLocal.APPELLATION_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOLocal getEditedBat() {
		return (EOLocal) getEditedObj();
	}

	private NSArray<EOImplantationGeo> lstImplantations;

	/**
	 * @return the lstImplantations
	 */
	public NSArray<EOImplantationGeo> lstImplantations() {
		if (lstImplantations == null)
			lstImplantations = EOImplantationGeo
					.fetchFwkGspot_ImplantationGeos(
							ec(),
							EOImplantationGeo.QUAL_VALID_IMPLANTATION,
							new NSArray<EOSortOrdering>(
									new EOSortOrdering(
											EOImplantationGeo.LC_IMPLANTATION_GEO_KEY,
											EOSortOrdering.CompareCaseInsensitiveAscending)));
		return lstImplantations;
	}

	/**
	 * @param lstImplantations
	 *            the lstImplantations to set
	 */
	public void setLstImplantations(NSArray<EOImplantationGeo> lstImplantations) {
		this.lstImplantations = lstImplantations;
	}

	private EOImplantationGeo implantationOccur;

	/**
	 * @return the implantationOccur
	 */
	public EOImplantationGeo implantationOccur() {
		return implantationOccur;
	}

	/**
	 * @param implantationOccur
	 *            the implantationOccur to set
	 */
	public void setImplantationOccur(EOImplantationGeo implantationOccur) {
		this.implantationOccur = implantationOccur;
	}

	private EOImplantationGeo selectedImplantation;
	private NSArray<EOTypeAdresse> lesTypeAdresses;

	/**
	 * @return the selectedImplantation
	 */
	public EOImplantationGeo selectedImplantation() {

		return selectedImplantation;
	}

	/**
	 * @param selectedImplantation
	 *            the selectedImplantation to set
	 */
	public void setSelectedImplantation(EOImplantationGeo selectedImplantation) {
		this.selectedImplantation = selectedImplantation;
	}

	@SuppressWarnings("unchecked")
	public NSArray<EOTypeAdresse> getLesTypeAdresses() {
		if (lesTypeAdresses == null) {
			lesTypeAdresses = EOTypeAdresse.fetchAll(edc(),
					EOTypeAdresse.QUAL_TADR_CODE_PRO);
		}
		return lesTypeAdresses;
	}

	public EOAdresse getEditAdresse() {
		if ((editAdresse == null)
				&& (getEditedBat().toFwkpers_Adresse() != null))
			editAdresse = getEditedBat().toFwkpers_Adresse();
		return editAdresse;
	}

	private EOImplantationGeo selectedImpEdit;

	/**
	 * @return the selectedImpEdit
	 */
	public EOImplantationGeo selectedImpEdit() {
		if ((selectedImpEdit == null)
				&& (getEditedBat().toRepartBatImpGeos().size() > 0))
			selectedImpEdit = getEditedBat().toRepartBatImpGeos().lastObject()
					.toImplantationGeo();
		return selectedImpEdit;
	}

	/**
	 * @param selectedImpEdit
	 *            the selectedImpEdit to set
	 */
	public void setSelectedImpEdit(EOImplantationGeo selectedImpEdit) {
		this.selectedImpEdit = selectedImpEdit;
	}

	public boolean isEditMode() {
		return (getEditedObj().editingContext().insertedObjects().size() <= 0);
	}

	

}