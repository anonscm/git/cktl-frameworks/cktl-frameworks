package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class SalleOccupants extends GspotBaseComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SalleOccupants(WOContext context) {
        super(context);
    }
    public String aucLstOccupsid() {
		if (hasBinding("lstOccupsid")) {
			return (String) valueForBinding("lstOccupsid");
		}
		return getComponentId() + "_aucLstOccupsid";
	}
    
    public boolean canEditOccups() {
		return isEditing() && getGspotUser().canEditSalle(selectedSalle());
	}

	private WODisplayGroup displayGroup;
	private EORepartBureau editedOccups;

	public EORepartBureau getEditedOccups() {
		return editedOccups;
	}
	public void setEditedOccups(EORepartBureau editedOccups) {
		this.editedOccups = editedOccups;
	}
	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// displayGroup.setDelegate(new DgDelegate());
			displayGroup.setNumberOfObjectsPerBatch(5);
		}

		if (isNeedReset())
			refreshOccups();
		return displayGroup;
	}

	public WOActionResults refreshOccups() {		
		displayGroup.setObjectArray(selectedSalle()
				.toRepartBureau(
						null,
						new NSArray<EOSortOrdering>(new EOSortOrdering(
								ERXQ.keyPath(EORepartBureau.TO_INDIVIDU_KEY,EOIndividu.NOM_AFFICHAGE_KEY),
								EOSortOrdering.CompareAscending)), true));
		displayGroup.clearSelection();
		return null;
	}
	
	public String idTbvOccups() {
		return getComponentId() + "_idTbvOccups";
	}
	
	public CktlAjaxWOComponent editMethodeObject() {
		return this;
	}
	
	public WOActionResults validAjout() {
		editedOccups.setToIndividuRelationship((EOIndividu) selectedSearchPersonne());
		try {
			editedOccups.validateForSave();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			AjaxUpdateContainer.updateContainerWithID(aucerreurid(), context());
			return null;
		}
		if (isCommitOnValid()) {
			editedOccups.editingContext().saveChanges();
			if (editedOccups.editingContext().parentObjectStore()
					.equals(session().defaultEditingContext())) {
				session().defaultEditingContext().saveChanges();
			}
		}
		refreshOccups();
		AjaxUpdateContainer.updateContainerWithID(aucLstOccupsid(), context());
		if (parentIdToRefresh() != null)
			AjaxUpdateContainer.updateContainerWithID(parentIdToRefresh(),
					context());
		closeAddOccups();

		return null;
	}

	

	public WOActionResults closeAddOccups() {
		setWindowVisible(false);
		//CktlAjaxWindow.close(context(), camaddOccupsid());
		AjaxModalDialog.close(context());
		return null;
	}
	
	public String aucerreurid() {
		return getComponentId() + "_aucerreurid";
	}

	public String idmessageutil() {
		return getComponentId() + "_idmessageutil";
	}

	private boolean isWindowVisible = false;

	/**
	 * @return the isWindowVisible
	 */
	public boolean isWindowVisible() {
		return isWindowVisible;
	}

	/**
	 * @param isWindowVisible
	 *            the isWindowVisible to set
	 */
	public void setWindowVisible(boolean isWindowVisible) {
		this.isWindowVisible = isWindowVisible;
	}
	
	public String camaddOccupsid() {
		return getComponentId() + "_camaddOccupsid";
	}
	
	public String auceditOccupsid() {
		return getComponentId() + "_auceditOccupsid";
	}
	public WOActionResults addOccups() {
		setWindowVisible(true);
		AjaxModalDialog.open(context(), camaddOccupsid());
		EOEditingContext ec = new EOEditingContext(session()
				.defaultEditingContext());
		editedOccups = EORepartBureau.createFwkGspot_RepartBureau(ec,false,new NSTimestamp(),new NSTimestamp(),null,
				selectedSalle.localInstanceIn(ec));
		return null;
	}
	
	private IPersonne selectedSearchPersonne;

	/**
	 * @return the selectedSearchPersonne
	 */
	public IPersonne selectedSearchPersonne() {
		return selectedSearchPersonne;
	}

	/**
	 * @param selectedSearchPersonne
	 *            the selectedSearchPersonne to set
	 */
	public void setSelectedSearchPersonne(IPersonne selectedSearchPersonne) {
		this.selectedSearchPersonne = selectedSearchPersonne;
	}
	private String searchTypeIntExt = "interne";

	/**
	 * @return the searchTypeIntExt
	 */
	public String searchTypeIntExt() {
		return searchTypeIntExt;
	}

	/**
	 * @param searchTypeIntExt
	 *            the searchTypeIntExt to set
	 */
	public void setSearchTypeIntExt(String searchTypeIntExt) {
		this.searchTypeIntExt = searchTypeIntExt;
	}
	private String searchTypePhysMoral = "individu";

	/**
	 * @return the searchTypePhysMoral
	 */
	public String searchTypePhysMoral() {
		return searchTypePhysMoral;
	}

	/**
	 * @param searchTypePhysMoral
	 *            the searchTypePhysMoral to set
	 */
	public void setSearchTypePhysMoral(String searchTypePhysMoral) {
		this.searchTypePhysMoral = searchTypePhysMoral;
	}
	public String aucselpersonneid() {
		return getComponentId() + "_aucselpersonneid";
	}
	
	public boolean isOccupant() {
		for (Object repart : selectedSalle().toRepartBureau()) {
			if (((EORepartBureau) repart).toIndividu().persId().equals(
					selectedSearchPersonne().persId()))
				return true;
		}
		return false;
	}
	public String buzzyid() {
		return getComponentId() + "_buzzyid";
		
	}

}