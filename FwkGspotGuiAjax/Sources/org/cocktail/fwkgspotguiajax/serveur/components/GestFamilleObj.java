package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestFamilleObj extends BaseGestionRef {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GestFamilleObj(WOContext context) {
        super(context);
    }
    private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		if ((getEditedFam().rfoLibelle() == null)
				|| ("".equals(getEditedFam().rfoLibelle()))) {
			UtilMessages.creatMessageUtil(session(),
					UtilMessages.ERROR_MESSAGE,
					"Le libellé est obligatoire !");
			return null;
		}
		
		if ((getEditedFam().rfoCommentaire()!=null)&&(getEditedFam().rfoCommentaire().length() > 400))
			getEditedFam().setRfoCommentaire(
					getEditedFam().rfoCommentaire().substring(0, 400));
		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedFam().rfoLibelle());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Famille créée ");
			setEditedObj(EOResaFamilleObjet
					.createFwkGspot_ResaFamilleObjet(new EOEditingContext(ec()),""));
		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addFamille() {
		setEditedObj(EOResaFamilleObjet
				.createFwkGspot_ResaFamilleObjet(new EOEditingContext(ec()),""));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'une famille d'objets");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOResaFamilleObjet) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'une famille d'objets");
		return null;
	}

	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = EOResaFamilleObjet.RFO_COMMENTAIRE_KEY
					+ " caseInsensitiveLike %@ OR "
					+ EOResaFamilleObjet.RFO_LIBELLE_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
			qualArray.add(searchLib() + "*");

		}

		return EOResaFamilleObjet
				.fetchFwkGspot_ResaFamilleObjets(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOResaFamilleObjet.RFO_LIBELLE_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOResaFamilleObjet getEditedFam() {
		return (EOResaFamilleObjet) getEditedObj();
	}
}