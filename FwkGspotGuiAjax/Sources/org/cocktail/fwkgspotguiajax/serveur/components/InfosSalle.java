package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.UUID;

import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXStringUtilities;

public class InfosSalle extends GspotBaseComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InfosSalle(WOContext context) {
		super(context);

	}

	public boolean disabledCbx() {
		return !isEditing();
	}

	private String aucinfossalleid = ERXStringUtilities
			.safeIdentifierName("aucinfossalleid" + UUID.randomUUID());

	/**
	 * @return the aucinfossalleid
	 */
	public String aucinfossalleid() {
		return aucinfossalleid;
	}

	private NSArray<String> lstRefreshZones;

	/**
	 * @return the lstRefreshZones
	 */
	public NSArray<String> lstRefreshZones() {
		if (lstRefreshZones == null) {
			if (parentIdToRefresh() != null)
				setLstRefreshZones(new NSArray<String>(new String[] {
						aucinfossalleid(), parentIdToRefresh() }));
			else
				setLstRefreshZones(new NSArray<String>(
						new String[] { aucinfossalleid() }));
		}
		return lstRefreshZones;
	}

	/**
	 * @param lstRefreshZones
	 *            the lstRefreshZones to set
	 */
	public void setLstRefreshZones(NSArray<String> lstRefreshZones) {
		this.lstRefreshZones = lstRefreshZones;
	}

	private String aucTriggerInfoSalleId = ERXStringUtilities
			.safeIdentifierName("aucTriggerInfoSalleId" + UUID.randomUUID());

	/**
	 * @return the aucTriggerInfoSalleId
	 */
	public String aucTriggerInfoSalleId() {
		return aucTriggerInfoSalleId;
	}

	/**
	 * @param aucTriggerInfoSalleId
	 *            the aucTriggerInfoSalleId to set
	 */
	public void setAucTriggerInfoSalleId(String aucTriggerInfoSalleId) {
		this.aucTriggerInfoSalleId = aucTriggerInfoSalleId;
	}

	public WOActionResults validInfosSalle() {
		if (isCommitOnValid())
			ec().saveChanges();
		setIsEditing(false);
		return null;
	}

	private NSArray<EOTypeSalle> lstTypesSalless;

	/**
	 * @return the lstTypesSalless
	 */
	public NSArray<EOTypeSalle> lstTypesSalless() {
		if (lstTypesSalless==null)
			lstTypesSalless=EOTypeSalle.fetchAllFwkGspot_TypeSalles(ec(), new NSArray<EOSortOrdering>(new EOSortOrdering[]{ new EOSortOrdering(
					EOTypeSalle.TSAL_LIBELLE_KEY,
					EOSortOrdering.CompareCaseInsensitiveAscending)}));
		return lstTypesSalless;
	}

	/**
	 * @param lstTypesSalless
	 *            the lstTypesSalless to set
	 */
	public void setLstTypesSalless(NSArray<EOTypeSalle> lstTypesSalless) {
		this.lstTypesSalless = lstTypesSalless;
	}

	private EOTypeSalle typeSalleOccur;

	/**
	 * @return the typeSalleOccur
	 */
	public EOTypeSalle typeSalleOccur() {
		return typeSalleOccur;
	}

	/**
	 * @param typeSalleOccur the typeSalleOccur to set
	 */
	public void setTypeSalleOccur(EOTypeSalle typeSalleOccur) {
		this.typeSalleOccur = typeSalleOccur;
	}

}