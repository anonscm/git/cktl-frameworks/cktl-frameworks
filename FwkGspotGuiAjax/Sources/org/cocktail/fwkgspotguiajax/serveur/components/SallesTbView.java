package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 */
public class SallesTbView extends GspotTbView {
    
    private static final long serialVersionUID = 6349499744079625692L;
    
    /**
     * On part du principe que les objets fetchées par le displayGroup disposent
     * d'une relation toSalles...
     */
    protected static final String TO_SALLES_KEY = "toSalles";
    protected static final String NOM_KP = ERXQ.keyPath(TO_SALLES_KEY, EOSalles.SAL_PORTE_KEY);
    protected static final String IMPLANTATION_KP = ERXQ.keyPath(TO_SALLES_KEY, EOSalles.TO_LOCAL_KEY, EOLocal.TO_FIRST_IMPLANTATION_GEO, EOImplantationGeo.LL_IMPLANTATION_GEO_KEY);
    protected static final String ETAGE_KP = ERXQ.keyPath(TO_SALLES_KEY, EOSalles.SAL_ETAGE_KEY);
    protected static final String BAT_KP = ERXQ.keyPath(TO_SALLES_KEY, EOSalles.TO_LOCAL_KEY, EOLocal.APPELLATION_KEY);
    protected static final String PRINCIPAL_KP = ERXQ.keyPath("bureauPrincipal");
    
    static {
        // Colonne Nom
        CktlAjaxTableViewColumn colNom = new CktlAjaxTableViewColumn();
        colNom.setLibelle("Nom");
        CktlAjaxTableViewColumnAssociation assNom = new CktlAjaxTableViewColumnAssociation(
                OBJ_KEY + "." + NOM_KP, "emptyValue");
        assNom.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
        colNom.setAssociations(assNom);
        colNom.setRowCssStyle("text-align:left;padding-left:3px;");
        _colonnesMap.setObjectForKey(colNom, NOM_KP);
        // Colonne Implantation
        CktlAjaxTableViewColumn colImp = new CktlAjaxTableViewColumn();
        colImp.setLibelle("Implantation");
        CktlAjaxTableViewColumnAssociation assImp = new CktlAjaxTableViewColumnAssociation(
                OBJ_KEY + "." + IMPLANTATION_KP, "emptyValue");
        assImp.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
        colImp.setAssociations(assImp);
        colImp.setRowCssStyle("text-align:left;padding-left:3px;");
        _colonnesMap.setObjectForKey(colImp, IMPLANTATION_KP);
        // Colonne Etage
        CktlAjaxTableViewColumn colEtage = new CktlAjaxTableViewColumn();
        colEtage.setLibelle("Etage");
        CktlAjaxTableViewColumnAssociation assEtage = new CktlAjaxTableViewColumnAssociation(
                OBJ_KEY + "." + ETAGE_KP, "emptyValue");
        assEtage.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
        colEtage.setAssociations(assEtage);
        colEtage.setRowCssStyle("text-align:center;");
        colEtage.setHeaderCssStyle("width:30px;");
        _colonnesMap.setObjectForKey(colEtage, ETAGE_KP);
        // Colonne Bâtiment
        CktlAjaxTableViewColumn colBat = new CktlAjaxTableViewColumn();
        colBat.setLibelle("Bâtiment");
        CktlAjaxTableViewColumnAssociation assBat = new CktlAjaxTableViewColumnAssociation(
                OBJ_KEY + "." + BAT_KP, "emptyValue");
        assBat.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
        colBat.setAssociations(assBat);
        colBat.setRowCssStyle("text-align:left;padding-left:3px;");
        _colonnesMap.setObjectForKey(colBat, BAT_KP);
        // Colonne Bureau Principal
        CktlAjaxTableViewColumn colPrincipal = new CktlAjaxTableViewColumn();
        colPrincipal.setLibelle("Bureau Principal");
        CktlAjaxTableViewColumnAssociation assPrincipal = new CktlAjaxTableViewColumnAssociation(
                OBJ_KEY + "." + PRINCIPAL_KP, "emptyValue");
        assPrincipal.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
        colPrincipal.setAssociations(assPrincipal);
        colPrincipal.setRowCssStyle("text-align:left;padding-left:3px;");
        _colonnesMap.setObjectForKey(colPrincipal, PRINCIPAL_KP);
    }
    
    public SallesTbView(WOContext context) {
        super(context);
    }

    @Override
    public NSArray<String> DEFAULT_COLONNES_KEYS() {
        return new NSArray<String>(new String[] {NOM_KP, IMPLANTATION_KP, ETAGE_KP, BAT_KP, PRINCIPAL_KP});
    }

    @Override
    public WOActionResults commitSave() {
        // TODO Auto-generated method stub
        return null;
    }
}