package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

@SuppressWarnings("unchecked")
public class GroupeDeposTbView extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EODepositaireSalles.TO_STRUCTURE_ULR_KEY
			+ "." + EOStructureUlr.LL_STRUCTURE_KEY;
	private static final String PATH = EODepositaireSalles.TO_STRUCTURE_ULR_KEY
			+ "." + EOStructureUlr.PATH_KEY;

	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Chemin");
		col0.setOrderKeyPath(PATH);
		col0.setObjectForKey("false", "visible");
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + PATH, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, PATH);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Groupe");
		col1.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		//ass1.setObjectForKey("false", "escapeHTML");
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, LIBELLE);
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	public GroupeDeposTbView(WOContext context) {
		super(context);
	}
	
	

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(new String[] { PATH, LIBELLE,
				OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			for (EOGenericRecord delDepos : getDeletedObjects()) {
				((EODepositaireSalles) delDepos)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EODepositaireSalles) delDepos).toSalles(),
								EODepositaireSalles.TO_SALLES_KEY);
				((EODepositaireSalles) delDepos)
						.removeObjectFromBothSidesOfRelationshipWithKey(
								((EODepositaireSalles) delDepos)
										.toStructureUlr(),
								EODepositaireSalles.TO_STRUCTURE_ULR_KEY);
				delDepos.editingContext().deleteObject(delDepos);
			}
			if (isCommitOnValid())
				ec.saveChanges();
		}

		return cancelSave();
	}

	

	@Override
	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		NSMutableArray<CktlAjaxTableViewColumn> cols =  super.getColonnes().mutableClone();
		
		return cols;
	}
}