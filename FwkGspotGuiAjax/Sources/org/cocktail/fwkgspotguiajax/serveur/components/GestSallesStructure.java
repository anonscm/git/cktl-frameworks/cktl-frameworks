package org.cocktail.fwkgspotguiajax.serveur.components;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.finder.FinderSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/**
 * 
 * Composant de listing et d'attribution de salles à une structure.
 * 
 * @binding structure la structure que l'on veut localiser
 * @binding editingContext (optionnel) l'editingContext parent, dans tous les
 *          cas, un nested editing context est crée sur l'ec fourni si dispo, 
 *          sinon sur le default ec.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 * 
 */
public class GestSallesStructure extends WOComponent {

    private static final long serialVersionUID = -5868927415763423199L;
    private static final String BINDING_EC = "editingContext";
    private static final String BINDING_STRUCTURE = "structure";
    private static final String BINDING_isEditing = "isEditing";
    private static final Logger LOG = Logger
            .getLogger(GestSallesStructure.class);

    private WODisplayGroup displayGroup;
    private EOEditingContext edc;
    private EOSalles selectedSalle;
    private NSArray<EOTypeOccupation> typesOccupations;
    private EOTypeOccupation selectedTypeOccupation;
    private EOTypeOccupation currentTypeOccupation;
    private SallesTreeDelegate sallesTreeDelegate;
    private BigDecimal pourcentageOccup;
    private boolean isModalVisible = false;
    private String message;
    private String tbViewContainerId;
    private String salleDialogContainerId;
    private String salleDialogId;

    public GestSallesStructure(WOContext context) {
        super(context);
    }

    @Override
    public void sleep() {
        setMessage(null);
        super.sleep();
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public EOEditingContext edc() {
        if (edc == null) {
            EOEditingContext parentEc = session().defaultEditingContext();
            if (hasBinding(BINDING_EC))
                parentEc = (EOEditingContext) valueForBinding(BINDING_EC);
            edc = ERXEC.newEditingContext(parentEc);
        }
        return edc;
    }

    public WOActionResults ajouter() {
        return null;
    }

    public WOActionResults close() {
        edc().revert();
        return null;
    }

    public WOActionResults supprimer() {
        EODetailPourcentage detail = (EODetailPourcentage) getDisplayGroup()
                .selectedObject();
        try {
            edc().deleteObject(detail);
            edc().saveChanges();
            getDisplayGroup().deleteSelection();
        } catch (NSValidation.ValidationException e) {
            setMessage(e.getMessage());
            LOG.error(e.getMessage(), e);
            edc().revert();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            edc().revert();
            throw new NSForwardException(e);
        }
        return null;
    }

    public WOActionResults ajouterSalle() {
        // hélas !
        EOStructureUlr structureUlr = EOStructureUlr
                .fetchFwkGspot_StructureUlr(edc(), ERXQ.equals(
                        EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY, structure()));
        EODetailPourcentage pct = EODetailPourcentage
                .createFwkGspot_DetailPourcentage(edc(), getSelectedSalle(),
                        structureUlr, selectedTypeOccupation);
        pct.setDetPourcentage(getPourcentageOccup());
        getDisplayGroup().insertObjectAtIndex(pct, 0);
        try {
            edc().saveChanges();
            CktlAjaxWindow.close(context(), getSalleDialogId());
        } catch (NSValidation.ValidationException e) {
            setMessage(e.getMessage());
            LOG.error(e.getMessage(), e);
            edc().revert();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            edc().revert();
            // rebalance !
            throw new NSForwardException(e);
        }
        // Refresh de la tb view
        return null;
    }

    public WODisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new WODisplayGroup();
            if (structure() != null)
                displayGroup.setObjectArray(FinderSalle.getSallesForStructure(
                        edc(), structure().cStructure()));
        }
        return displayGroup;
    }

    public NSArray<EOTypeOccupation> typesOccupations() {
        if (typesOccupations == null)
            typesOccupations = EOTypeOccupation
                    .fetchAllFwkGspot_TypeOccupations(edc(), ERXS
                            .ascInsensitives(EOTypeOccupation.TOC_LIBELLE_KEY));
        return typesOccupations;
    }

    public EOStructure structure() {
        return EOStructure.localInstanceIn(edc(),
                (EOStructure) valueForBinding(BINDING_STRUCTURE));
    }

    public SallesTreeDelegate getTreeDelegate() {
        if (sallesTreeDelegate == null)
            sallesTreeDelegate = new SallesTreeDelegate(edc());
        return sallesTreeDelegate;
    }

    public String getTbViewContainerId() {
        if (tbViewContainerId == null)
            tbViewContainerId = ERXWOContext
                    .safeIdentifierName(context(), true);
        return tbViewContainerId;
    }

    public String getSallesDialogContainerId() {
        if (salleDialogContainerId == null)
            salleDialogContainerId = ERXWOContext.safeIdentifierName(context(),
                    true);
        return salleDialogContainerId;
    }

    public String getSalleDialogId() {
        if (salleDialogId == null)
            salleDialogId = ERXWOContext.safeIdentifierName(context(), true);
        return salleDialogId;
    }

    public boolean noDetailSelected() {
        return getDisplayGroup().selectedObject() == null;
    }

    public EOSalles getSelectedSalle() {
        return selectedSalle;
    }

    public void setSelectedSalle(EOSalles selectedSalle) {
        this.selectedSalle = selectedSalle;
    }

    public EOTypeOccupation getSelectedTypeOccupation() {
        return selectedTypeOccupation;
    }

    public void setSelectedTypeOccupation(
            EOTypeOccupation selectedTypeOccupation) {
        this.selectedTypeOccupation = selectedTypeOccupation;
    }

    public EOTypeOccupation getCurrentTypeOccupation() {
        return currentTypeOccupation;
    }

    public void setCurrentTypeOccupation(EOTypeOccupation currentTypeOccupation) {
        this.currentTypeOccupation = currentTypeOccupation;
    }

    public void setPourcentageOccup(BigDecimal pourcentageOccup) {
        this.pourcentageOccup = pourcentageOccup;
    }

    public BigDecimal getPourcentageOccup() {
        return pourcentageOccup;
    }

    public void setModalVisible(boolean isModalVisible) {
        this.isModalVisible = isModalVisible;
    }

    public boolean isModalVisible() {
        return isModalVisible;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Boolean isEditing() {
		if(hasBinding(BINDING_isEditing)) {
			return (Boolean) valueForBinding(BINDING_isEditing);
		} else {
			return false;
		}
	}

}