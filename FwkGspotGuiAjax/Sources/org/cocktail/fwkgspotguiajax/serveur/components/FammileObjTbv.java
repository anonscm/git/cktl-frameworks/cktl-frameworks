package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FammileObjTbv extends GspotTbView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LIBELLE = EOResaFamilleObjet.RFO_LIBELLE_KEY;
	private static final String COMMENT = EOResaFamilleObjet.RFO_COMMENTAIRE_KEY;
    public FammileObjTbv(WOContext context) {
        super(context);
    }
    static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Libelle");
		col0.setOrderKeyPath(LIBELLE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + LIBELLE, "emptyValue");
		ass0.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;vertical-align:top;");
		_colonnesMap.takeValueForKey(col0, LIBELLE);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Commentaire");
		col1.setOrderKeyPath(COMMENT);
		col1.setComponent("CommentZoneTbv");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + COMMENT, "emptyValue");
		ass1.setValueWhenEmptyDynamique(VALUE_WHEN_EMPTY_KEY);
		//ass1.setObjectForKey("3", "rows");
		ass1.setObjectForKey("30", "cols");
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, COMMENT);
		
		
		_colonnesMap.takeValueForKey(getColonneAction(), OBJ_KEY + ".action");
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {

		return new NSArray<String>(
				new String[] { LIBELLE,COMMENT, OBJ_KEY + ".action" });
	}

	@Override
	public WOActionResults commitSave() {
		if (getDeletedObjects().size() > 0) {
			EOEditingContext ec = getDeletedObjects().lastObject()
					.editingContext();
			NSMutableArray<EOGenericRecord> deletedObj = new NSMutableArray<EOGenericRecord>();
			for (EOGenericRecord delDetail : getDeletedObjects()) {				
				if (((EOResaFamilleObjet) delDetail).toResaTypeObjets().size()>0){
					UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					"Famille utilisée par des types d'objets, supression impossible");
				} else {
					delDetail.editingContext().deleteObject(delDetail);
					deletedObj.addObject(delDetail);
				}
			}
			if (isCommitOnValid())
				ec.saveChanges();
			getDeletedObjects().removeObjectsInArray(deletedObj);
		}

		return null;
	}
}