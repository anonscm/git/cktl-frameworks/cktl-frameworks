package org.cocktail.fwkgspotguiajax.serveur.components;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkgspot.serveur.finder.FinderSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;

/**
 * Gestion de l'attribution des salles aux individus.
 *   
 * @binding individu l'objet de type EOIndividu que l'on veut editer. 
 * @binding editingContext (optionnel) l'editingContext parent, dans tous les
 *          cas, un nested editing context est crée sur l'ec fourni si dispo, 
 *          sinon sur le default ec.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
/**
 * @author ffavreau
 *
 */
/**
 * @author ffavreau
 *
 */
public class GestSallesIndividu extends WOComponent {

    private static final long serialVersionUID = -8972062399408792602L;
    private static final Logger LOG = Logger
            .getLogger(GestSallesIndividu.class);
    private static final String BINDING_EC = "editingContext";
    private static final String BINDING_INDIVIDU = "individu";
    private static final String BINDING_wantReset = "wantReset";
    private static final String BINDING_isEditing = "isEditing";
//    private static final String BINDING_ReadOnly = "readOnly";
    
    
    private WODisplayGroup displayGroup;
    private SallesTreeDelegate sallesTreeDelegate;
    private EOSalles selectedSalle;
    private String tbViewContainerId;
    private String salleDialogContainerId;
    private String salleDialogId;
    private EOEditingContext edc;
    private String message;

    public GestSallesIndividu(WOContext context) {
        super(context);
    }

    @Override
    public void appendToResponse(WOResponse aResponse, WOContext aContext) {
    	if (isWantRefresh()) {
    		displayGroup = null;
    		setWantRefresh(false);
    	}
    	super.appendToResponse(aResponse, aContext);
    }
    
    @Override
    public void sleep() {
        setMessage(null);
        super.sleep();
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }

    public WOActionResults ajouter() {
        return null;
    }

    public WOActionResults close() {
        edc().revert();
        return null;
    }

    public WOActionResults supprimer() {
        EORepartBureau repart = (EORepartBureau) getDisplayGroup()
                .selectedObject();
        try {
        	getDisplayGroup().deleteSelection();
            edc().deleteObject(repart);
            edc().saveChanges();

//            resetDisplayGroup();
        } catch (NSValidation.ValidationException e) {
            setMessage(e.getMessage());
            LOG.error(e.getMessage(), e);
            edc().revert();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            edc().revert();
            throw new NSForwardException(e);
        }
        return null;
    }

    public WOActionResults ajouterSalle() {
        // hélas !
        NSTimestamp now = new NSTimestamp();
        
        // Récupère la liste des bureaux associés à un individu
        //   Si la liste est vide, alors le bureau créé est le bureau principal
        //   S'il y a déjà des élements dans la liste alors le nouveau bureau n'est pas défini comme bureau principal
        NSArray<EORepartBureau> bureaux=FinderSalle.getSallesForIndividu(edc(), individu().noIndividu());
        Boolean estBureauPrincipal=bureaux.count()<1;
        
        EORepartBureau repartBureau = EORepartBureau.createFwkGspot_RepartBureau(
                edc(), estBureauPrincipal, now, now, individu(), getSelectedSalle());
        try {
        	//individu().toRepar
            edc().saveChanges();
            getDisplayGroup().insertObjectAtIndex(repartBureau, 0);
            CktlAjaxWindow.close(context(), getSalleDialogId());
        } catch (NSValidation.ValidationException e) {
            setMessage(e.getMessage());
            LOG.error(e.getMessage(), e);
            edc().revert();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            edc().revert();
            // rebalance !
            throw new NSForwardException(e);
        }
        return null;
    }

    public EOEditingContext edc() {
        if (edc == null) {
            EOEditingContext parentEc = session().defaultEditingContext();
            if (hasBinding(BINDING_EC)) {
				parentEc = (EOEditingContext) valueForBinding(BINDING_EC);
			}
            edc = ERXEC.newEditingContext(parentEc);
        }
        return edc;
    }

    public WODisplayGroup getDisplayGroup() {
        if (displayGroup == null) {
            displayGroup = new WODisplayGroup();
            if (individu() != null) {
				displayGroup.setObjectArray(FinderSalle.getSallesForIndividu(
                		edc(), individu().noIndividu()));
			}
        }
        return displayGroup;
    }

    public Boolean nePeutSupprimerLocalisation() {
    	return noRepartSelected() || !isEditing();
    }
    
    public boolean noRepartSelected() {
        return getDisplayGroup().selectedObject() == null;
    }

    public EOIndividu individu() {
        return EOIndividu.localInstanceIn(edc(),
                (EOIndividu) valueForBinding(BINDING_INDIVIDU));
    }

    public String getTbViewContainerId() {
        if (tbViewContainerId == null) {
			tbViewContainerId = ERXWOContext
                    .safeIdentifierName(context(), true);
		}
        return tbViewContainerId;
    }

    public String getSallesDialogContainerId() {
        if (salleDialogContainerId == null)
            salleDialogContainerId = ERXWOContext.safeIdentifierName(context(),
                    true);
        return salleDialogContainerId;
    }

    public String getSalleDialogId() {
        if (salleDialogId == null) {
			salleDialogId = ERXWOContext.safeIdentifierName(context(), true);
		}
        return salleDialogId;
    }

    public SallesTreeDelegate getTreeDelegate() {
        if (sallesTreeDelegate == null) {
			sallesTreeDelegate = new SallesTreeDelegate(edc());
		}
        return sallesTreeDelegate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EOSalles getSelectedSalle() {
        return selectedSalle;
    }

    public void setSelectedSalle(EOSalles selectedSalle) {
        this.selectedSalle = selectedSalle;
    }

	public boolean isWantRefresh() {
		if (hasBinding(BINDING_wantReset)) {
			return (Boolean) valueForBinding(BINDING_wantReset);
		} else {
			return false;
		}
	}

	public void setWantRefresh(boolean wantRefresh) {
		if (hasBinding(BINDING_wantReset)) {
			setValueForBinding(false, BINDING_wantReset);
		}
	}
	
	public Boolean isEditing() {
		if (hasBinding(BINDING_isEditing)) {
			return (Boolean) valueForBinding(BINDING_isEditing);
		} else {
			return false;
		}
	}

	/**
	 * Modifie le bureau principal de la répartition sélectionnée.
	 *   Active le bureau principal sur la sélection et désactive le bureau principal sur les autres bureaux associés à l'individu.
	 * @return
	 */
	public WOActionResults definirBureauPrincipal() {
        EORepartBureau repartitionSelectionnee = (EORepartBureau) getDisplayGroup()
                .selectedObject();
        if (repartitionSelectionnee==null)
        	return null;

        NSArray<EORepartBureau> bureaux=FinderSalle.getSallesForIndividu(edc(), individu().noIndividu());
        Iterator<EORepartBureau> iRepart=bureaux.iterator();
        while (iRepart.hasNext())
        {
        	EORepartBureau currentRepartition=iRepart.next();
        	currentRepartition.setBurPrincipal(false);
        }
        repartitionSelectionnee.setBurPrincipal(true);
        save();

		return null;
	}

	/**
	 * Methode permettant de savoir s'il est possible ou non de modifié le bureau principale de la ligne sélectionnée dans le display group
	 * @return Vrai s'il n'est pas possible de définir le bureau principal. Faux s'il est possible de modifier le bureau 
	 */
	public Boolean nePeutDefinirBureauPrincipal() {
        EORepartBureau repartitionSelectionnee = (EORepartBureau) getDisplayGroup()
                .selectedObject();
        // On peut definir l'adresse comme adresse principale s'il y a une selection et que l'adresse n'est pas déjà adresse principale
        Boolean peutDefinirBureauPrincipal=(repartitionSelectionnee!=null) && !repartitionSelectionnee.burPrincipal();
        return !peutDefinirBureauPrincipal;
	}

	/**
	 * Sauvegarde l'edit contexte en cours en gérant les exceptions
	 * @return Vrai si la sauvegarde s'est bien passée
	 */
	public Boolean save() {
		Boolean saveOK=false;
        try {
            edc().saveChanges();
            saveOK=true;
        } catch (NSValidation.ValidationException e) {
            setMessage(e.getMessage());
            LOG.error(e.getMessage(), e);
            edc().revert();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            edc().revert();
            throw new NSForwardException(e);
        }
        return saveOK;
	}
	
//	public Boolean isReadOnly() {
//		if(hasBinding(BINDING_ReadOnly)) {
//			return (Boolean) valueForBinding(BINDING_ReadOnly);
//		} else {
//			return Boolean.FALSE;
//		}
//	}

}