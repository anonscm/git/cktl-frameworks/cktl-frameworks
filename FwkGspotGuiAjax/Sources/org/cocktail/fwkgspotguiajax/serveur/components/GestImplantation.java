package org.cocktail.fwkgspotguiajax.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspotguiajax.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestImplantation extends BaseGestionRef {
	public GestImplantation(WOContext context) {
		super(context);
	}

	private String searchLib;

	/**
	 * @return the searchLib
	 */
	public String searchLib() {
		return searchLib;
	}

	/**
	 * @param searchLib
	 *            the searchLib to set
	 */
	public void setSearchLib(String searchLib) {
		this.searchLib = searchLib;
	}

	@Override
	public WOActionResults valid() {
		try {
			getEditedImpl().validateObjectMetier();

		} catch (ValidationException e) {
			UtilMessages.creatMessageUtil(session(), UtilMessages.ERROR_MESSAGE,
					e.getMessage());
			return null;
		}
		
		int mode = -1;
		if (getEditedObj().editingContext().insertedObjects().size() > 0)
			mode = 0;
		else
			mode = 1;
		super.valid();
		if (mode == 0) {
			setSearchLib(getEditedImpl().lcImplantationGeo());
			searchObj();
			UtilMessages.creatMessageUtil(session(), UtilMessages.INFO_MESSAGE,
					"Implantation créée ");
			setEditedObj(EOImplantationGeo
					.getNewEmptyImplantation(new EOEditingContext(ec())));
		} else {
			CktlAjaxWindow.close(context(), caweditdiscid());
		}

		return null;
	}

	public WOActionResults addimplantation() {
		setEditedObj(EOImplantationGeo
				.getNewEmptyImplantation(new EOEditingContext(ec())));
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Ajout d'une implantation");
		return null;
	}

	@Override
	public WOActionResults editObj(Object disc) {
		setEditedObj((EOImplantationGeo) disc);
		setEdited(Boolean.TRUE);
		CktlAjaxWindow.open(context(), caweditdiscid(),
				"Modification d'une implantation");
		return null;
	}

	@Override
	public NSArray getLstObj() {
		String qualStr = "";
		NSMutableArray<Object> qualArray = new NSMutableArray<Object>();

		if ((searchLib() != null) && (!"".equals(searchLib().trim()))) {
			qualStr = EOImplantationGeo.LC_IMPLANTATION_GEO_KEY
					+ " caseInsensitiveLike %@ OR "
					+ EOImplantationGeo.LL_IMPLANTATION_GEO_KEY
					+ " caseInsensitiveLike %@ ";
			qualArray.add(searchLib() + "*");
			qualArray.add(searchLib() + "*");

		}

		return EOImplantationGeo
				.fetchFwkGspot_ImplantationGeos(
						ec(),
						EOQualifier.qualifierWithQualifierFormat(qualStr,
								qualArray),
						new NSArray<EOSortOrdering>(
								new EOSortOrdering[] { new EOSortOrdering(
										EOImplantationGeo.LC_IMPLANTATION_GEO_KEY,
										EOSortOrdering.CompareCaseInsensitiveAscending) }));
	}

	public EOImplantationGeo getEditedImpl() {
		return (EOImplantationGeo) getEditedObj();
	}
}