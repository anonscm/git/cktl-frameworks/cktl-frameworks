--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
REM INSERTING into LITCHI.INTERNAT_PARAMETRES
SET DEFINE OFF;

Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('26','MODE_PRELEVEMENT','SEPA','Mode de prélèvement : SEPA/NATIONAL');


-- Version complètement installé
update LITCHI.DB_VERSION set DBV_INSTALL = sysdate where DBV_ID = 1100;
commit;
