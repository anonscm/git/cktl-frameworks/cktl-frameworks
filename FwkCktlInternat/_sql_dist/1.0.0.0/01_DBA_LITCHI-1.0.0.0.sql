--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
/*
DROP USER LITCHI CASCADE;
DROP PROFILE CI_DB_GE;
*/
SET DEFINE OFF;

whenever sqlerror exit sql.sqlcode ;

CREATE PROFILE CI_DB_GE LIMIT
CPU_PER_CALL 27000 
CONNECT_TIME 1440 
IDLE_TIME 720 
FAILED_LOGIN_ATTEMPTS 8
PASSWORD_LOCK_TIME 3;

CREATE USER "LITCHI"  PROFILE "CI_DB_GE"  IDENTIFIED BY "********" DEFAULT TABLESPACE "SCOL"  TEMPORARY TABLESPACE "TEMP"  ACCOUNT UNLOCK;

GRANT CONNECT TO LITCHI;                                                                                                                                                                                                                        
GRANT RESOURCE TO LITCHI; 
GRANT UNLIMITED TABLESPACE TO LITCHI;  

GRANT CREATE ANY INDEX TO LITCHI;                                                                                                                                                                                                               
GRANT CREATE DATABASE LINK TO LITCHI;                                                                                                                                                                                                           
GRANT CREATE PROCEDURE TO LITCHI;                                                                                                                                                                                                               
GRANT CREATE TABLE TO LITCHI;                                                                                                                                                                                                                   
GRANT CREATE VIEW TO LITCHI;                                                                                                                                                                                                                    

GRANT EXECUTE ANY PROCEDURE TO LITCHI;                                                                                                                                                                                                          

GRANT SELECT ANY TABLE TO LITCHI;
GRANT SELECT ON GRHUM.SALLES_SEQ TO LITCHI;                                                                                                                                                                                                                

GRANT DELETE ON GRHUM.PERSONNE_TELEPHONE TO LITCHI;                                                                                                                                                                                             
GRANT DELETE ON GRHUM.SALLES TO LITCHI;                                                                                                                                                                                                         

GRANT INSERT ON GRHUM.ADRESSE TO LITCHI;                                                                                                                                                                                                        
GRANT INSERT ON GRHUM.LOCAL TO LITCHI;                                                                                                                                                                                                          
GRANT INSERT ON GRHUM.PERSONNE TO LITCHI;                                                                                                                                                                                                       
GRANT INSERT ON GRHUM.PERSONNE_TELEPHONE TO LITCHI;                                                                                                                                                                                             
GRANT INSERT ON GRHUM.REPART_PERSONNE_ADRESSE TO LITCHI;                                                                                                                                                                                        
GRANT INSERT ON GRHUM.SALLES TO LITCHI;                                                                                                                                                                                                         

GRANT REFERENCES ON GRHUM.LOCAL TO LITCHI;                                                                                                                                                                                                      
GRANT REFERENCES ON GRHUM.PERSONNE TO LITCHI;                                                                                                                                                                                                   
GRANT REFERENCES ON GRHUM.RIBFOUR_ULR TO LITCHI;                                                                                                                                                                                                
GRANT REFERENCES ON GRHUM.SALLES TO LITCHI;                                                                                                                                                                                                     
GRANT REFERENCES ON GRHUM.TYPE_SALLE TO LITCHI;                                                                                                                                                                                                 

GRANT UPDATE ON GRHUM.ADRESSE TO LITCHI;                                                                                                                                                                                                        
GRANT UPDATE ON GRHUM.LOCAL TO LITCHI;                                                                                                                                                                                                          
GRANT UPDATE ON GRHUM.PERSONNE TO LITCHI;                                                                                                                                                                                                       
GRANT UPDATE ON GRHUM.PERSONNE_TELEPHONE TO LITCHI;                                                                                                                                                                                             
GRANT UPDATE ON GRHUM.REPART_PERSONNE_ADRESSE TO LITCHI;                                                                                                                                                                                        
GRANT UPDATE ON GRHUM.SALLES TO LITCHI; 

PROMPT Le mot de passe du user LITCHI est à changer
PROMPT alter user LITCHI identified by "unvraimotdepasse";