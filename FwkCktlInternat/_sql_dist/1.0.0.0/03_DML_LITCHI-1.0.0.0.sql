--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
-- Permet d'inserer des utilisateurs administrateurs de l'application
-- Insertion le/les utilisateurs
REM INSERTING into LITCHI.UTILISATEUR
SET DEFINE OFF;
Insert into LITCHI.UTILISATEUR (PERS_ID,ISADMIN) values ('xxxxx',null);
Insert into LITCHI.UTILISATEUR (PERS_ID,ISADMIN) values ('yyyyy',null);

-- Insertion en tant que Admin
REM INSERTING into LITCHI.REPART_UTILISATEUR_GROUPE
SET DEFINE OFF;
Insert into LITCHI.REPART_UTILISATEUR_GROUPE (PERS_ID,ID_GROUPE) values ('xxxxx','1');
Insert into LITCHI.REPART_UTILISATEUR_GROUPE (PERS_ID,ID_GROUPE) values ('yyyyy','1');

--------------------------------------------------------
--Recompiler tous les objets invalides du USER LITCHI
--------------------------------------------------------
SET DEFINE OFF;
EXEC DBMS_UTILITY.COMPILE_SCHEMA ('LITCHI');
EXEC DBMS_UTILITY.COMPILE_SCHEMA ('LITCHI');
EXEC DBMS_UTILITY.COMPILE_SCHEMA ('LITCHI');

--------------------------------------------------------
--  Fichier créé - mardi-octobre-30-2012   
--------------------------------------------------------
REM INSERTING into LITCHI.EDITIONS
SET DEFINE OFF;
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('6','Remise clés','remise_cle',null,'1','0','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('10','Planning occupation des chambres dans une résidence','occupation_general_chambres',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('8','Liste des locataires individus par ordre alphabétique','liste_locataires',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('2','Certificat administratif','certificat_administratif',null,null,null,'LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('4','Certificat de résidence','certificat_residence',null,null,null,'LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('12','Bilan des factures réglées','bilan_reglements',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('11','Liste des règlements','liste_reglements',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('13','Journal de facturation','journal_facturation',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('14','Journal des règlements','journal_reglements',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('15','Liste des factures','liste_factures',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('31','Chambres des résidences','chambres_residences',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('32','Factures des personnes sans compte informatique','factures_courrier_rest',null,'0','1','RESTO');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('21','CAF AB','CAFR_AB',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('22','CAF FI','CAFL_FI',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('23','CAF CDEGHJM','CAFL_CDEGHJM',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('24','CAF AB','CAFR_AB',null,'0',null,'LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('25','CAF FI','CAFL_FI',null,'0',null,'LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('26','CAF CDEGHJM','CAFL_CDEGHJM',null,'0',null,'LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('27','Liste des réservations individus par ordre alphabétique','liste_reservation',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('28','Liste des prélevés par ordre alphabétique','liste_prelevements',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('29','Liste des factures des personnes sans compte informatique','factures_courrier',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('30','Liste des cautions cochées, facturées ou payées','liste_cautions',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('33','Compte client des clients hébergement','compte_client_hebergement',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('34','Compte client des clients restaurant','compte_client_restaurant',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('35','Fichier optimisé pour excel des locataires','occupation_chambres_excel',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('38','Liste des versements CAFs neutralisés au jour de réception','neutralisation_caf',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('39','Liste des factures non payés','liste_dettes',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('16','Etiquettes 105x35','etiquettes_100x35',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('17','Liste client restaurant','liste_client_restaurant',null,'0','1','RESTO');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('18','Liste des tarifs de location','tarifs_location',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('19','Liste des tarifs de restaurant','tarifs_restaurant',null,'0','1','RESTO');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('20','Etat de reglement des personnes','etat_reglement_personne',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('36','Liste des étudiants qui dorment mais ne mangent pas','qui_dort_ne_dine_pas',null,'0','1','RESTO');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('37','Anomalies sur les contrats de restaurants','anomalies_restaurant',null,'0','1','RESTO');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('40','Premières relances','relances1',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('41','Dernières relances','relances2',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('42','Anomalies sur les contrats de location','anomalies_location',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('43','Taux d''occupation','taux_occupation',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('44','Liste des arrêts de locations par jour','arret_location',null,'0','1','LOC');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('45','Liste des règlements rejetés','liste_reglements_rejetes',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('46','Liste des factures qui ne sont pas en prélèvement','liste_factures_non_preleves',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('47','Liste des factures qui sont en prélèvement','liste_factures_prelevement',null,'0','1','COMPTA');

REM INSERTING into LITCHI.DROIT
SET DEFINE OFF;
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('1','IS_ADMIN','Une deuxième façon d''être administrateur','Administrateur');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('39','DROIT_GESTION_TARIF_LOCATION','Gère les tarifs Locations','Gérer tarifs location');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('40','DROIT_GESTION_REGLEMENTS','Gestion des règlements effectués dans l''application','Gestion règlements');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('3','DROIT_RESERVATION_MULTIPLE','Peut faire des réservations multiples','Multiple réservation');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('2','DROIT_SIMPLE_RESERVATION','Peut faire une simple réservatiion','Simple réservation');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('4','DROIT_GERER_RESERVATION','Peut gérer les résas','Gestion des réservations');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('5','DROIT_GESTION_LOCATION','Peut gérer les locations','Gestion des locations');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('6','DROIT_EDITIONS_LOCATION','Peut éditer les rapports de masse','Editions des rapports');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('7','DROIT_PLANNING_OCCUPATION','Peut voir les plannings','Planning occupation');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('8','DROIT_FACTURATION_LOCATION','Facturation des locations','Facturation location');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('9','DROIT_GESTION_ANOMALIES_LOCATION','Gestion des anomalies','Gestion anomalies');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('10','DROIT_GESTION_RESTAURATION','Gestion des clients la restauration','Gestion restauration');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('11','DROIT_FACTURER_RESTAURATION','Peut Facturer Restauration','Facturation restauration');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('12','DROIT_GESTION_TARIF_RESTAURATION','Peut gérer les tarifs restauration','Gestion tarif restauration');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('13','DROIT_GESTION_QUI_DORT_DINE','Peut gérér le QDD','Qui dort dine');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('14','DROIT_GESTION_FACTURE','Droit de gérer facture','Gestion facture');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('17','DROIT_ACTIVER_FIN_CAF','Activation fin CAF','Activation fin CAF');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('15','DROIT_GESTION_CAF','Droit de gérer la CAF','Gestion CAF');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('16','DROIT_INTEGRATION_CAF','Droit intégration CAF','Intégration CAF');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('18','DROIT_MODIFIER_ALLOCATAIRE_CAF','Droit modifier allocataire CAF','Modifier allocataire CAF');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('19','DROIT_CONSULTER_LOCATAIRE','Droit consulter les llocataires','Consulter liste locataires');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('20','DROIT_CONSULTER_RESTAURATION','Droit consulter client Restauration','Consulter client restaurant');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('21','DROIT_CONSULTER_ALLOCATAIRE_CAF','Droit consulter allocataire CAF','Consulter Allocataire CAF');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('22','DROIT_CONSULTER_FACTURE','Droit consulter facture','Consulter les factures');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('23','DROIT_GESTION_ELEMENTS_DEMANDES','Droit de gérer les éléments demandés','Gérer éléménts demandés');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('24','DROIT_GESTION_TYPE_TARIF','Droit de gérer les types de tarifs de  locations','Gérer types tarifs location');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('25','DROIT_GESTION_STATUT_EXTERIEUR','Droit de gérer les statuts extérieurs','Gérer statut des extérieurs');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('26','DROIT_GESTION_OBJET_RESERVABLE','Droit de gérer les objets réservables','Gérer les objets réservables ');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('27','DROIT_GESTION_TYPE_OBJET','Droit de gérer les types d''objets','Gérer les types d''objets');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('28','DROIT_GESTION_SALLES','Droit de gérer les salles','Gérer les salles');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('29','DROIT_GESTION_TYPE_SALLE','Droit de gérer les types de salles','Gérer les types salles');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('30','DROIT_CHOISIR_LOCAUX_UTILISES','Droit de choisir les locaux','Choix des locaux');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('31','DROIT_GESTION_PARAMETRES','Gestion des paramètres','Gestion paramètres applications');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('32','DROIT_GESTION_UTLISATEUR','Gestion des utilisateurs','Gestion des utilisateurs');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('33','DROIT_GESTION_GROUPES_UTILISATEUR','Gestion des groupes utilisateurs','Gestion des groupes utilisateurs');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('34','DROIT_GESTION_EDITION_LOCATION','Gestion des edition de location','Gestion édition location');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('35','DROIT_GESTION_TYPE_PRODUIT','Gestion des types produit','Gestion type Produit');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('36','DROIT_GESTION_TYPE_TVA','Gestion de la TVA','Gestion TVA');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('37','DROIT_ENCAISSER_FACTURE','Encaissement des factures','Encaissement Facture');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('38','DROIT_EDITION_COMPTABLE','Edition de la comptabilité','Editions Comptable');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('41','DROIT_ENCAISSEMENT_PARTIEL','Encaissement partiel des factures autorisées','Encaissement partiel');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('42','DROIT_EDITION_RESTAURATION','Peut faire des éditions du restaurant','Edition restaurant');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('43','DROIT_GESTION_CAUTION','Peut gérer les dépôts de garantie','Gestion caution');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('44','DROIT_GESTION_AVANCES','Peut gérer les avances','Gestion avances');
Insert into LITCHI.DROIT (ID_DROIT,DROIT_KEY,DROIT_COMMENT,DROIT_VALUE) values ('45','DROIT_GESTION_PRELEVEMENT','Peut gérer les prélèvement','Gestion prélèvement');

REM INSERTING into LITCHI.GROUPE
SET DEFINE OFF;
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('1','Administrateurs',null);
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('2','Gestionnaire Location','Ceux qui gère les locations');
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('3','Comptables','Groupe des comptables');
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('4','Gestionnaire restauration','Gestionnaires des restaurants');
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('5','Régisseurs','Gestion locale des résidences');
Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('6','Service Courrier','Distributeurs de courriers');

REM INSERTING into LITCHI.INTERNAT_PARAMETRES
SET DEFINE OFF;
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('7','GESTION_CAF','OUI','Activer la gestion de la CAF OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('8','FACTURATION_RESTAURATION_MENSUELLE','NON','Facturation restauration mensuelle en cours OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('9','FACTURATION_LOCATION_MENSUELLE','NON','Facturation location mensuelle en cours OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('6','LISTE_TYPE_ETUDIANT','ARCHI2,D1,D2,D3,D4,D5,D6,ECH1,EESH1,FF1,FLE1,ING1,ING2,ING3,ING4,ING5,MI1,MS1,MS2,M1,M2,IUT,CPE','Liste des étudiants possibles que renvoit la procédure stockée STATUT_INDIVIDU');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('1','DATE_DEBUT_RECHERCHE_RESERVATION','03/09/2012','Date de début de recherche des réservations');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('2','DATE_FIN_RECHERCHE_RESERVATION','30/06/2013','Date de fin de recherche des réservations');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('3','NOMBRE_JOUR_PREAVIS','30','Nombre de jour avant la date de départ pour le préavis');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('4','NOMBRE_JOUR_FACTURATION_EST_MENSUEL','30','Quand la facturation passe en mensuel, avec bail');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('5','ANNEE_COURANTE','2012','Année universitaire considérée pour statut étudiant');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('10','WEBSERVICE_KEY','La petite bete a papa','Clé codage pour les direct action en tant que webservice');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('11','LISTE_ENCAISSEMENT_AUTORISE','Espece,Cheque,Carte bancaire,Virement,JOD 1,JOD 2','Liste des encaissement possibles dans la page encaissement');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('12','NOMBRE_JOURS_CARENCE','0','Nombre entre deux réservations : dépend de l''établissement du mode de fonctionnement');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('18','DATE_OUVERTURE_RESTAURANT','03/09/2012','Date d''ouverture du restaurant');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('19','DATE_FERMETURE_RESTAURANT','30/06/2013','Date de fermeture du restaurant');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('20','DATE_DEBUT_RECHERCHE_LOCATION','03/09/2012','Date de début de recherche des locations');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('21','DATE_FIN_RECHERCHE_LOCATION','30/06/2013','Date de fin de recherche des locations');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('13','PRE_C3','12345','Code guichet emetteur - 5 caractères');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('14','PRE_C41','1234','Identifiant client - 4 caractères');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('15','PRE_C42','1234567','Code application et divers - 7 caractères');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('16','PRE_C5','1234567 AGENT CPT ETAB','Nom du donneur d''ordre - 24 caractères max');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('17','PRE_C6','123456','Numéro National d''Emetteur - 6 caractères');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('22','QDD_RESERVATION','OUI','Crée un QDD à la réservation. Valeur OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('23','QDD_REMISE_CLES','OUI','Crée un QDD à la remise de clés. Valeur OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('24','QDD_INTERVALLES_EXCLUSION','01/07,31/08','Liste d''intervalles de dates d''exclusions du QDD. Couples de valeurs  DD/MM,DD/MM,DD/MM,DD,MM...');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('25','QDD_TYPE_OBJET_EXCLUSION','Salle d''etude','Liste de type d''objets reservables exclus du QDD séparés par des virgules');

REM INSERTING into LITCHI.REPART_DROIT_GROUPE
SET DEFINE OFF;
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('1','1');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('8','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('9','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('6','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('7','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('4','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('14','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('5','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('37','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('20','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('15','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('18','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('35','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('17','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('16','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('38','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('37','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('36','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('11','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('12','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('10','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('13','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('37','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('20','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('21','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('22','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','5');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('28','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('29','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('30','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('27','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('39','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('24','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('25','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('23','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('45','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('44','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('43','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('41','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('40','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('2','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('3','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('26','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('22','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('20','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('21','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','2');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('22','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('21','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('14','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('40','3');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('14','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('42','4');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','6');

REM INSERTING into LITCHI.TYPE_CALCUL
SET DEFINE OFF;
Insert into LITCHI.TYPE_CALCUL (ID_TYPE_CALCUL,LIB_TYPE_CALCUL) values ('2','Forfait');
Insert into LITCHI.TYPE_CALCUL (ID_TYPE_CALCUL,LIB_TYPE_CALCUL) values ('1','Mensuel');
Insert into LITCHI.TYPE_CALCUL (ID_TYPE_CALCUL,LIB_TYPE_CALCUL) values ('3','Journalier');

REM INSERTING into LITCHI.TYPE_FACTURATION
SET DEFINE OFF;
Insert into LITCHI.TYPE_FACTURATION (ID_TYPE_FACTURATION,LIB_TYPE_FACTURATION) values ('1','Facture separe');
Insert into LITCHI.TYPE_FACTURATION (ID_TYPE_FACTURATION,LIB_TYPE_FACTURATION) values ('2','A la premiere Facture');

REM INSERTING into LITCHI.TYPE_FACTURE
SET DEFINE OFF;
Insert into LITCHI.TYPE_FACTURE (ID_TYPE_FACTURE,LIB_TYPE_FACTURE) values ('1','HEBERGEMENT');
Insert into LITCHI.TYPE_FACTURE (ID_TYPE_FACTURE,LIB_TYPE_FACTURE) values ('2','RESTAURATION');
Insert into LITCHI.TYPE_FACTURE (ID_TYPE_FACTURE,LIB_TYPE_FACTURE) values ('3','AVOIR');
Insert into LITCHI.TYPE_FACTURE (ID_TYPE_FACTURE,LIB_TYPE_FACTURE) values ('4','ARRHES');

REM INSERTING into LITCHI.TYPE_PRODUIT
SET DEFINE OFF;
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('8','DEPOT GARANTIE','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('9','EXCEDENTS','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('14','LOYER','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('5','ARRHES','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('6','LOYER NU','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('7','RESTAURATION','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('10','RISQUES LOCATIFS','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('11','CAF','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('12','CHARGES','xxxx');
Insert into LITCHI.TYPE_PRODUIT (ID_TYPE_PRODUIT,LIB_TYPE_PRODUIT,COMPTE) values ('13','FRAIS DE DOSSIERS','xxxx');

REM INSERTING into LITCHI.TYPE_REGLEMENT
SET DEFINE OFF;
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('7','Paybox');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('1','Espece');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('2','Cheque');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('3','Carte bancaire');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('4','Prelevement');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('5','Virement');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('6','TIP');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('8','JOD 1');
Insert into LITCHI.TYPE_REGLEMENT (ID_TYPE_REGLEMENT,LIB_TYPE_REGLEMENT) values ('9','JOD 2');

REM INSERTING into LITCHI.TYPE_REMBOURSEMENT
SET DEFINE OFF;
Insert into LITCHI.TYPE_REMBOURSEMENT (ID_TYPE_REMBOURSEMENT,LIB_TYPE_REMBOURSEMENT) values ('1','Aucun remboursement');
Insert into LITCHI.TYPE_REMBOURSEMENT (ID_TYPE_REMBOURSEMENT,LIB_TYPE_REMBOURSEMENT) values ('2','Déduction à la première facture');
Insert into LITCHI.TYPE_REMBOURSEMENT (ID_TYPE_REMBOURSEMENT,LIB_TYPE_REMBOURSEMENT) values ('3','Remboursement à la fin des contrats');

-- Version complètement installé
update LITCHI.DB_VERSION set DBV_INSTALL = sysdate where DBV_ID = 1000;
commit;
