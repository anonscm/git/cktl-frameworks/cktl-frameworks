--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
-- PATCH pour passer de Litchi beta 0.8.5.2 à la version 1.0.0.0
-- A n'utiliser que pour une ancienne installation beta de Litchi

SET DEFINE OFF;



Insert into LITCHI.DB_VERSION (DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) values ('1000','1.0.0.0',to_date('04/09/2013','DD/MM/YYYY'),null,'Version officielle de la base de données de Litchi');

Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('46','Liste des factures qui ne sont pas en prélèvement','liste_factures_non_preleves',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('47','Liste des factures qui sont en prélèvement','liste_factures_prelevement',null,'0','1','COMPTA');

Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('22','QDD_RESERVATION','OUI','Crée un QDD à la réservation. Valeur OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('23','QDD_REMISE_CLES','OUI','Crée un QDD à la remise de clés. Valeur OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('24','QDD_INTERVALLES_EXCLUSION','01/07,31/08','Liste d''intervalles de dates d''exclusions du QDD. Couples de valeurs  DD/MM,DD/MM,DD/MM,DD,MM...');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('25','QDD_TYPE_OBJET_EXCLUSION','Salle d''etude','Liste de type d''objets reservables exclus du QDD séparés par des virgules');


-- Version complètement installé
update LITCHI.DB_VERSION set DBV_INSTALL = sysdate where DBV_ID = 1000;
commit;


