--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

-- Sert à recompiler toutes les procédures du user 

SET SERVEROUTPUT ON SIZE 2000;
-- set serveroutput on
-- recompilation des objets de statut invalide
declare
  cmd varchar2(200);
  nb number;
  passe  integer;
begin
  dbms_output.put_line ('==== compilation ===');

passe := 1;
while passe <= 5 loop
  dbms_output.put_line ('==== compilation === passe ' || passe);

  for r in (select object_name,object_type,status from user_objects
             where (object_type = 'FUNCTION' or object_type = 'PROCEDURE' or  object_type = 'TRIGGER' or
                     object_type = 'PACKAGE' or object_type = 'VIEW'
                  or object_type ='JAVA CLASS' or object_type = 'JAVA SOURCE' or object_type = 'MATERIALIZED VIEW')
             and status = 'INVALID' order by 1) loop
    -- dbms_output.put_line ('compilation de : ' || r.object_name || ' (' || r.object_type || ')');
    cmd := 'alter ' || r.object_type || ' "'|| r.object_name || '" compile';
    begin
      execute immediate cmd;
    exception when others then
    null;
    --  dbms_output.put_line ('Erreur : ' || r.object_name || ' (' || r.object_type || ') non compilé');
    end;
  end loop;
  for r in (select object_name,object_type,status from user_objects where   object_type = 'PACKAGE BODY' and status = 'INVALID' order by 1) loop
    -- dbms_output.put_line ('compilation de : ' || r.object_name || ' (' || r.object_type || ')');
    cmd :=  'alter PACKAGE '|| r.object_name || ' compile body';
    begin
      execute immediate cmd;
        exception when others then
         null;
         --  dbms_output.put_line ('Erreur : ' || r.object_name || ' (' || r.object_type || ') non compilé');
    end;
  end loop;
   
  passe := passe + 1;

end loop;

  select count(*) into nb from user_objects
       where (object_type = 'FUNCTION' or object_type = 'PROCEDURE' or object_type = 'TRIGGER' or
              object_type = 'PACKAGE' or object_type = 'PACKAGE BODY' or object_type = 'VIEW'
              or object_type ='JAVA CLASS' or object_type = 'JAVA SOURCE'  or object_type = 'MATERIALIZED VIEW')
              and status = 'INVALID';
  dbms_output.put_line ('');
  dbms_output.put_line ('--------------------------------------------------------');
  dbms_output.put_line ('Les objets suivants n''ont pas pu être compilés : ' ||nb);
  dbms_output.put_line ('--------------------------------------------------------');
  for r in (select object_name,object_type,status from user_objects
             where (object_type = 'FUNCTION' or object_type = 'PROCEDURE' or object_type = 'TRIGGER' or
                     object_type = 'PACKAGE' or object_type = 'PACKAGE BODY' or object_type = 'VIEW'
                   or object_type ='JAVA CLASS' or object_type = 'JAVA SOURCE' or object_type = 'MATERIALIZED VIEW')
             and status = 'INVALID' order by 1) loop
   dbms_output.put_line (r.object_name || ' (' || r.object_type || ')');
  end loop;
end;
/