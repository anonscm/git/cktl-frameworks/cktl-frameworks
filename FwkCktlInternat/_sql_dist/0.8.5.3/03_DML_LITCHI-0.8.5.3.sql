--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
--

Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('46','Liste des factures qui ne sont pas en prélèvement','liste_factures_non_preleves',null,'0','1','COMPTA');
Insert into LITCHI.EDITIONS (ID,LIBELLE,NOM_FICHIER,ID_TYPE_OBJET,EST_COCHE,EN_MASSE,TAG) values ('47','Liste des factures qui sont en prélèvement','liste_factures_prelevement',null,'0','1','COMPTA');

Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('22','QDD_RESERVATION','OUI','Crée un QDD à la réservation. Valeur OUI/NON');
Insert into LITCHI.INTERNAT_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRE) values ('23','QDD_REMISE_CLES','OUI','Crée un QDD à la remise de clés. Valeur OUI/NON');




-- Version complètement installé
update LITCHI.DB_VERSION set DBV_INSTALL = sysdate where DBV_ID = 3;
commit;
