--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

Insert into LITCHI.GROUPE (ID_GROUPE,GROUPE_LIBELLE,GROUPE_COMMENT) values ('6','Service Courrier','Distributeurs de courriers');
Insert into LITCHI.REPART_DROIT_GROUPE (ID_DROIT,ID_GROUPE) values ('19','6');

-- Version complètement installé
update LITCHI.DB_VERSION set DBV_INSTALL = sysdate where DBV_ID = 2;
commit;
