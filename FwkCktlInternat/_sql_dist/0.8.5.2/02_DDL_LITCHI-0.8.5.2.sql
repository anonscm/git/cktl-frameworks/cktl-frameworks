--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--
--------------------------------------------------------
--  DDL for Procedure STATUT_INDIVIDU
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LITCHI"."STATUT_INDIVIDU" (p_persid NUMBER, p_etudnumero OUT NUMBER, p_statut OUT VARCHAR2)
  IS
     cont NUMBER :=0;
BEGIN
  BEGIN
      IF p_persid IS NULL THEN  p_etudnumero := NULL; p_statut := ''; RETURN; END IF;
    	SELECT   etud_numero, etu.fgra_code||etu.idipl_annee_suivie
	    INTO     p_etudnumero, p_statut
	    FROM     scolarite.scol_inscription_etudiant etu, litchi.internat_parametres p
	    WHERE    etu.pers_id = p_persid
	    AND      (etu.idipl_type_inscription = 0  OR  etu.idipl_type_inscription = 4)
	    AND      etu.fann_key = p.param_value
	    AND      p.param_key = 'ANNEE_COURANTE';
	EXCEPTION
    WHEN NO_DATA_FOUND THEN
    cont := 1;
	  WHEN OTHERS THEN
    p_etudnumero :=NULL;
    p_statut := '';
  END;
  IF cont = 0  THEN RETURN; END IF;
  cont := 0;
  BEGIN
      	SELECT  DISTINCT etud_numero, 'Autre étudiant'
        INTO     p_etudnumero, p_statut
	      FROM		 scolarite.scol_inscription_etudiant etu, litchi.internat_parametres p
	      WHERE    etu.pers_id = p_persid
	      AND      (etu.idipl_type_inscription <> 0 AND  etu.idipl_type_inscription <> 4)
	      AND      etu.fann_key = p.param_value
        AND      p.param_key = 'ANNEE_COURANTE';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    cont := 1;
	  WHEN OTHERS THEN
    p_etudnumero :=NULL;
    p_statut := '';
  END;
  IF cont = 0  THEN RETURN; END IF;
  cont := 0;
  BEGIN
     	SELECT   NULL, 'Personnel établissement'
  	  INTO     p_etudnumero, p_statut
      FROM     grhum.v_personnel_actuel vpa, grhum.individu_ulr iu
	    WHERE    vpa.no_dossier_pers = iu.no_individu
	    AND      p_persid = iu.pers_id;
 	EXCEPTION
    WHEN NO_DATA_FOUND THEN
    cont := 1;
	  WHEN OTHERS THEN
    p_etudnumero :=NULL;
    p_statut := '';
  END;
  IF cont = 0  THEN RETURN; END IF;
  cont := 0;
  BEGIN
	     SELECT   NULL, ts.lib_type_statut
	     INTO     p_etudnumero, p_statut
       FROM     litchi.personne_statut ps, litchi.type_statut ts
	     WHERE    ps.pers_id = p_persid
	     AND      ps.id_type_statut = ts.id_type_statut;
 EXCEPTION
    WHEN NO_DATA_FOUND THEN
    cont := 1;
	  WHEN OTHERS THEN
    p_etudnumero :=NULL;
    p_statut := '';
  END;
  IF cont = 0  THEN RETURN; END IF;
  p_etudnumero :=NULL;
  p_statut := 'AUTRE';
END STATUT_INDIVIDU;

/
--------------------------------------------------------
--  DDL for Procedure FACTURER_LOCATION_MENSUELLE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LITCHI"."FACTURER_LOCATION_MENSUELLE" (p_persid_createur NUMBER, p_date_du_mois DATE, p_date_echeance DATE, p_bArrhes NUMBER, p_bCAF NUMBER, p_bAvances NUMBER) AS

-- Sélectionner touts les details hébergement qui ont un type de calcul mensuel, qui n'ont pas pas été facturé (le mois complet, pas encore etc.)
-- Et qu'il y a un tarif valide en face
-- Attention ne sait pas gérer deux tarifs mensuels accrochés au même type de tarif si il y a recouvrement
-- Deux factures seront créées !
-- Pour facturer mensuellement en avance p_date_du_mois.

CURSOR cHebergements (cp_date_du_mois DATE ) IS
   SELECT h.id_hebergement,h.date_reservation, h.pers_id_payeur, dh.id_detail_hebergement, dh.pers_id_concerner,
          dh.id_type_tarif, ta.id_tarif, ta.date_fin, tt.libelle_type_tarif, ta.prix, tt.type_tva_loc, tt.id_type_produit,
          NVL2(dh.date_fin_facture, dh.date_fin_facture+1, dh.date_debut) as date_debut_facturation,
          LEAST(dh.date_fin, LAST_DAY(cp_date_du_mois)) as date_fin_facturation,
          ore.lib_objet, dh.date_debut, dh.pers_id_concerner
   FROM litchi.hebergement h, litchi.detail_hebergement dh, litchi.type_tarif tt, litchi.tarif ta, litchi.objet_reservable ore

-- hébergement non réservé
   WHERE h.id_hebergement = dh.id_hebergement
   AND dh.date_remise_cle is NOT NULL
   AND dh.date_debut <= LAST_DAY(cp_date_du_mois)

-- Les dates de facturation en face
   AND (dh.date_fin_facture IS NULL OR (dh.date_fin_facture < LAST_DAY(cp_date_du_mois) AND dh.date_fin_facture < dh.date_fin))

-- Le tarif en face avec une date de validité au jour de facturation
-- Trouver le bon tarif à la date de début de facturation !
-- Pas de changement de tarif si changement de tarif
   AND dh.id_type_calcul = 1
   AND tt.code_type_tarif = dh.id_type_tarif
   AND tt.code_type_tarif = ta.type_tarif
   AND ta.id_type_calcul = dh.id_type_calcul
   AND ((ta.date_debut IS NULL OR ta.date_debut <= NVL(dh.date_fin_facture, dh.date_debut)) AND (ta.date_fin IS NULL OR ta.date_fin >= NVL(dh.date_fin_facture, dh.date_debut)))
   AND tt.type_tarif_pere is NULL
   -- objet reservable
   AND ore.id_objet_hebergement = dh.id_objet_hebergement
   ORDER BY h.id_hebergement;

CURSOR cTarifsFils ( cp_id_tarif_pere NUMBER, cp_date_du_mois DATE) IS
   SELECT  ta.id_tarif, tt.libelle_type_tarif, ta.prix, tt.code_type_tarif, tt.type_tva_loc, tt.id_type_produit
   FROM litchi.tarif ta, litchi.type_tarif tt
   WHERE tt.type_tarif_pere IS NOT NULL AND tt.type_tarif_pere = cp_id_tarif_pere
   AND tt.code_type_tarif = ta.type_tarif
   AND ta.id_type_calcul = 1
   AND ((ta.date_debut IS NULL OR ta.date_debut <= cp_date_du_mois) AND (ta.date_fin IS NULL OR ta.date_fin >= cp_date_du_mois))
   ORDER by ta.id_tarif;

CURSOR cElements (cp_id_detail_hebergement  NUMBER) IS
   SELECT   rdhea.id_repart_det_heb_ea, ea.lib_element_associe, ea.montant, ea.type_tva_ea, ea.id_type_produit
		  FROM		litchi.repart_det_heb_ele_ass rdhea, litchi.element_associe ea, litchi.detail_hebergement dh
		  WHERE   dh.id_detail_hebergement = cp_id_detail_hebergement
      AND     rdhea.id_detail_hebergement = dh.id_detail_hebergement
      AND     rdhea.id_element_associe = ea.code_element_associe
-- A la premiere facture
      AND     ea.id_type_facturation = 2
		  AND     rdhea.id_ligne_facture IS NULL
		  ORDER BY rdhea.id_repart_det_heb_ea;

-- variables curseur principal
   c_id_hebergement NUMBER;
   c_date_reservation DATE;
   c_pers_id_payeur NUMBER;
   c_id_detail_hebergement NUMBER;
   c_persid_concerner NUMBER;
   c_id_type_tarif NUMBER;
   c_id_tarif NUMBER;
   c_date_fin_tarif DATE;
   c_id_type_tva NUMBER;
   c_id_taux_tva NUMBER;
   c_taux_tva    NUMBER;
   c_id_type_produit NUMBER;
   c_date_debut_facturation DATE;
   c_date_fin_facturation DATE;
   c_lib_objet_reservable VARCHAR2(250);
   c_lib_ligne_fact    VARCHAR2(250);
   c_prix_ht            NUMBER;
   c_dh_date_debut      DATE;

-- variables de travail
   w_date_debut DATE;
   w_date_fin   DATE;
   w_date_temp  DATE;
   w_id_facture NUMBER;
   w_id_hebergement NUMBER;

-- Facture
   w_mnt_facture_ht    NUMBER;
   w_mnt_facture_ttc   NUMBER;
   w_mnt_a_payer_ht    NUMBER;
   w_mnt_a_payer_ttc   NUMBER;
   w_pers_id_payeur    NUMBER;

-- ligne de facture
   w_unite             VARCHAR2(250);
   w_lib_ligne_fact    VARCHAR2(250);
   w_mnt_ht            NUMBER;
   w_mnt_ttc           NUMBER;
   w_qte               NUMBER;
   w_prix_ht           NUMBER;
   w_taux_tva          NUMBER;
   w_id_rdhea          NUMBER;
   w_id_type_tarif     NUMBER;
   w_id_tarif          NUMBER;
   w_id_type_produit   NUMBER;
   w_id_type_tva       NUMBER;
   w_mois_complet      NUMBER;
   w_pers_id_loc       NUMBER;

-- variable temporaire
   temp_n1       NUMBER;
   temp_d1       DATE;
   temp_n2       NUMBER;

-- statistique
   w_satut_heberge   VARCHAR2(250);
   w_num_etudiant    NUMBER;
   w_id_concerner    NUMBER;

BEGIN
  IF p_bCAF IS NOT NULL AND p_bCAF = 1 THEN
                PRE_AFFECTATION_CAF ();
  END IF;

 OPEN cHebergements (p_date_du_mois) ;

 w_id_hebergement := -1;
 w_id_concerner := -1;
 FETCH cHebergements INTO  c_id_hebergement, c_date_reservation, c_pers_id_payeur, c_id_detail_hebergement, c_persid_concerner, c_id_type_tarif, c_id_tarif,  c_date_fin_tarif, c_lib_ligne_fact, c_prix_ht, c_id_type_tva, c_id_type_produit, c_date_debut_facturation, c_date_fin_facturation, c_lib_objet_reservable, c_dh_date_debut, w_pers_id_loc;
 WHILE cHebergements%FOUND LOOP
 -- Liquider la dernière facture : calculer les Arrhes, CAF, AVOIRS et AVANCES, mettre montant à payer
   IF (w_id_hebergement != c_id_hebergement) THEN
      IF (w_id_hebergement >0 ) THEN
              w_mnt_a_payer_ht := w_mnt_facture_ht;
              w_mnt_a_payer_ttc := w_mnt_facture_ttc;
         UPDATE litchi.entete_facture
         SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
         WHERE id_facture = w_id_facture;
		 
          -- Calcul du montant de TVA
         IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
           UPDATE litchi.entete_facture
           SET   mnt_facture_tva = w_mnt_facture_ttc - w_mnt_facture_ht
           WHERE id_facture = w_id_facture;
         END IF;    
		 
            IF p_bArrhes IS NOT NULL AND p_bArrhes = 1 THEN
                PROC_AFFECTATION_ARRHES (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
            END IF;

             IF  p_bCAF IS NOT NULL AND p_bCAF = 1 AND w_mnt_a_payer_ttc > 0 THEN
                PROC_AFFECTATION_CAF (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
          
             IF p_bAvances IS NOT NULL AND p_bAvances = 1 AND w_mnt_a_payer_ttc > 0  THEN
                PROC_AFFECTATION_AVANCES (w_pers_id_payeur, w_id_facture, 1, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;

      END IF;

    -- Insert première entête facture
     SELECT litchi.entete_facture_seq.NEXTVAL  INTO  w_id_facture  FROM DUAL;
     INSERT INTO  litchi.entete_facture (id_facture, id_type_facture, lib_facture, persid_payeur, persid_createur, date_creation,date_echeance)
			      VALUES                      (w_id_facture, 1, 'Facture hébergement', c_pers_id_payeur, p_persid_createur, sysdate, p_date_echeance);

     LITCHI.REMPLIR_ENTETE_FACTURE (w_id_facture, c_pers_id_payeur, 1);

     w_mnt_facture_ht  :=0;
     w_mnt_facture_ttc :=0;
     w_mnt_a_payer_ht  :=0;
     w_mnt_a_payer_ttc :=0;
     
     w_pers_id_payeur := c_pers_id_payeur;
     w_id_hebergement := c_id_hebergement;
   END IF;

    IF c_persid_concerner IS NOT NULL THEN
      IF c_persid_concerner != w_id_concerner THEN
               litchi.statut_individu(c_persid_concerner, w_num_etudiant, w_satut_heberge);
               w_id_concerner := c_persid_concerner;
      END IF;
    ELSE
       w_num_etudiant := NULL;
       w_id_concerner := NULL;
       w_satut_heberge := NULL;
    END IF;

    -- Recherche dépôt de garantie frais de dossiers etc. et insertion de ce qui a à payer à la première facture

    OPEN cElements (c_id_detail_hebergement);
    FETCH cElements INTO  w_id_rdhea, w_lib_ligne_fact, w_prix_ht , w_id_type_tva, w_id_type_produit;
    WHILE cElements%FOUND LOOP
       IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, p_date_du_mois, w_taux_tva);
       ELSE
       w_taux_tva := NULL;
       END IF;
       IF w_taux_tva IS NULL THEN
        w_mnt_ht := w_prix_ht;
        w_mnt_ttc := w_mnt_ht;
       ELSE
        w_mnt_ht := w_prix_ht ;
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
       END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;
       w_lib_ligne_fact :=  w_lib_ligne_fact ||' en date du : ' || TO_CHAR(c_date_reservation, 'DD/MM/YYYY');

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
      prix_unitaire_ht, mnt_ht, mnt_ttc, type_tva_lf, id_type_produit, status_heberge, id_repart_det_heb_ea, taux_tva,
      mois, annee, date_debut_facture, date_fin_facture)
		 VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
              w_prix_ht,  w_mnt_ht, w_mnt_ttc, w_id_type_tva, w_id_type_produit, w_satut_heberge, w_id_rdhea, w_taux_tva,
              TO_NUMBER(TO_CHAR(c_date_debut_facturation,'MM')), TO_NUMBER(TO_CHAR(c_date_debut_facturation,'YYYY')),
              c_date_debut_facturation, c_date_debut_facturation );

    FETCH cElements INTO  w_id_rdhea, w_lib_ligne_fact, w_prix_ht , w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE cElements;

    -- découpage en mois
    w_date_debut := c_date_debut_facturation;

<<loop_date>>
   -- On veut gérer le cas problématique du départ le premier du mois !
   -- Cas de l'arrivée en fin de mois
   -- Cas de facturation sur moins d'un mois calendaire
      IF (w_date_debut = LAST_DAY(w_date_debut)) THEN
   -- Arrivé en fin mois
       IF c_dh_date_debut = w_date_debut THEN 
		      w_date_temp := w_date_debut + 1; 
       ELSE
		      w_date_debut := w_date_debut + 1;
          w_date_temp := LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));
		  -- Si la fin est au premier on décrémente ;) cling cling ...
		  -- Si pas un mois calendaire 
        IF (w_date_temp-w_date_debut)<1 OR 
           (w_date_temp <> LAST_DAY(w_date_temp) AND LAST_DAY(c_dh_date_debut) = LAST_DAY(w_date_debut-1)) THEN 
				     w_date_debut := w_date_debut-1; 
			  END IF;
		   END IF;
      ELSE
          w_date_temp :=  LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));
      END IF;

    -- chercher un nouveau tarif !
    IF c_date_fin_tarif IS NOT NULL AND w_date_debut > c_date_fin_tarif  THEN
        BEGIN
            SELECT   ta.id_tarif, ta.date_fin, ta.prix
            INTO    c_id_tarif, c_date_fin_tarif, c_prix_ht
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = 1
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut));
      EXCEPTION
      WHEN OTHERS THEN
     -- garder l'ancien ....
          NULL;
      END;
    END IF;


    IF (TO_NUMBER(TO_CHAR(w_date_debut,'DD')) = 1) AND (LAST_DAY(w_date_debut) = w_date_temp) THEN
      w_mois_complet := 1;
      w_qte := NULL;
      w_unite := NULL;
      w_prix_ht := c_prix_ht;
      w_mnt_ht := c_prix_ht;
      w_lib_ligne_fact := 'Loyer du mois : ' || TO_CHAR(w_date_debut,'MONTH YYYY') || ' pour ' ||  c_lib_ligne_fact ||' au '|| c_lib_objet_reservable ;
    ELSE
      w_mois_complet := 0;
      w_qte := w_date_temp  - w_date_debut;
      w_unite := 'NUIT';
 --     Règle du trentième
      w_prix_ht := c_prix_ht / 30.0;
      w_mnt_ht := w_qte *  w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
      w_lib_ligne_fact := c_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY') ||' au '|| c_lib_objet_reservable;
    END IF;
    
     -- Afficher le nom du locataire si different de celui du payeur 
     IF (w_pers_id_loc IS NOT NULL) AND (w_pers_id_loc <> w_pers_id_payeur) THEN 
          w_lib_ligne_fact := w_lib_ligne_fact ||'. Locataire ' || litchi.nom_prenom_personne(w_pers_id_loc); 
     END IF;
     
     -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;
       
     -- Insert lignes facture principal

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
        VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, NULL, w_mnt_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_detail_hebergement, c_id_type_tarif, c_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

     -- Chercher les fils
    OPEN cTarifsFils ( c_id_type_tarif , w_date_debut);
    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif , w_id_type_tva, w_id_type_produit;
    WHILE cTarifsFils%FOUND LOOP

    IF w_mois_complet = 1 THEN
      w_qte := NULL;
      w_unite := NULL;
      w_lib_ligne_fact := w_lib_ligne_fact || ' de ' || TO_CHAR(w_date_debut,'MONTH YYYY')||' au '|| c_lib_objet_reservable;
      w_mnt_ht := w_prix_ht;
    ELSE
       --     Règle du trentième
      w_prix_ht := w_prix_ht / 30.0;
      w_mnt_ht := w_qte * w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
      w_unite := 'NUIT';
      w_lib_ligne_fact := w_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY')||' au '|| c_lib_objet_reservable;
    END IF;

-- chercher tva
    IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, w_date_debut, w_taux_tva);
    ELSE
       w_taux_tva := NULL;
    END IF;
    IF w_taux_tva IS NULL THEN
      w_mnt_ttc := w_mnt_ht;
    ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
    END IF;
    w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
    w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;


     INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, NULL, w_mnt_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, w_id_type_tva, w_id_type_produit, c_id_detail_hebergement, w_id_type_tarif, w_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif, w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE  cTarifsFils;

    w_date_debut := w_date_temp;
    IF (w_date_temp<c_date_fin_facturation) THEN  GOTO loop_date;  END IF;

    -- Nouvel hébergement
    FETCH cHebergements INTO  c_id_hebergement, c_date_reservation, c_pers_id_payeur, c_id_detail_hebergement, c_persid_concerner, c_id_type_tarif,  c_id_tarif,  c_date_fin_tarif, c_lib_ligne_fact, c_prix_ht, c_id_type_tva, c_id_type_produit, c_date_debut_facturation, c_date_fin_facturation, c_lib_objet_reservable,c_dh_date_debut, w_pers_id_loc;
 END LOOP;

 -- Liquider la dernière facture : calculer les Arrhes, CAF, AVOIRS et AVANCES, mettre montant à payer
    IF (w_id_hebergement >0 ) THEN
             w_mnt_a_payer_ht := w_mnt_facture_ht;
             w_mnt_a_payer_ttc := w_mnt_facture_ttc;

             UPDATE litchi.entete_facture
             SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
             WHERE id_facture = w_id_facture;
			   
             -- Calcul du montant de TVA
             IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
                UPDATE litchi.entete_facture
                SET   mnt_facture_tva = w_mnt_facture_ttc - w_mnt_facture_ht
                WHERE id_facture = w_id_facture;
             END IF;
         
             IF p_bArrhes IS NOT NULL AND p_bArrhes = 1 THEN
                PROC_AFFECTATION_ARRHES (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
            END IF;

             IF  p_bCAF IS NOT NULL AND p_bCAF = 1 AND w_mnt_a_payer_ttc > 0 THEN
                PROC_AFFECTATION_CAF (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
          
             IF p_bAvances IS NOT NULL AND p_bAvances = 1 AND w_mnt_a_payer_ttc > 0  THEN
                PROC_AFFECTATION_AVANCES (w_pers_id_payeur, w_id_facture, 1, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
    END IF;

 CLOSE cHebergements;

-- EXCEPTION WHEN OTHERS THEN RETURN;
END FACTURER_LOCATION_MENSUELLE;

/
--------------------------------------------------------
--  DDL for Procedure FACTURER_RESTO_MENSUELLE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LITCHI"."FACTURER_RESTO_MENSUELLE" (p_persid_createur NUMBER, p_date_du_mois DATE, p_date_echeance DATE, p_bAvances NUMBER)AS

CURSOR cRestauration (cp_date_du_mois DATE ) IS
  SELECT  r.pers_id, r.id_restauration , f.id_formule, f.lib_formule, tf.id_tarif_formule, tf.date_fin, tf.prix_mensuel, tf.prix_par_jour, f.type_tva_rest, f.id_type_produit,
          NVL2(r.date_fin_facture, r.date_fin_facture + 1, r.date_debut) as date_debut_facturation,
          LEAST(r.date_fin, LAST_DAY(cp_date_du_mois)) as date_fin_facturation
   FROM litchi.restauration r, litchi.formule f, litchi.tarifs_formule tf, litchi.detail_hebergement dh
   WHERE r.id_formule = f.id_formule
   AND  f.id_formule = tf.id_formule

-- Le tarif en face avec une date de validité au jour de facturation
-- Trouver le bon tarif à la date de début de facturation !
-- Il faut appliquer le bon tarif après chaque mois
-- Chaque changement de tarif se fait en début de mois en guise de simplification
-- Pas de changement de tarif si changement de tarif dans la période
   AND (tf.date_debut IS NULL OR tf.date_debut <= NVL(r.date_fin_facture, r.date_debut)) AND (tf.date_fin IS NULL OR tf.date_fin >= NVL(r.date_fin_facture, r.date_debut))
   AND (tf.prix_mensuel > 0 OR tf.prix_par_jour > 0)

-- On ne facture pas ceux qui ne sont pas en resa QDD !
   AND dh.id_restauration (+)  = r.id_restauration
   AND ( dh.date_remise_cle IS NOT NULL OR dh.id_detail_hebergement IS NULL)

-- Les dates de facturation en face
   AND (r.date_fin_facture IS NULL OR (r.date_fin_facture < LAST_DAY(cp_date_du_mois) AND r.date_fin_facture < r.date_fin))
   AND r.date_debut < LAST_DAY(cp_date_du_mois)
   AND r.date_debut <= r.date_fin
    
   ORDER BY r.pers_id, r.date_fin;

 -- variables curseur principal
   c_pers_id         NUMBER;
   c_id_restauration NUMBER;
   c_id_formule      NUMBER;
   c_lib_formule     VARCHAR2(250);
   c_id_tarif        NUMBER;
   c_taux_tva        NUMBER;
   c_date_fin_tarif  DATE;
   c_prix_mensuel    NUMBER;
   c_prix_par_jour   NUMBER;
   c_id_type_tva     NUMBER;
   c_id_taux_tva     NUMBER;
   c_id_type_produit        NUMBER;
   c_date_debut_facturation DATE;
   c_date_fin_facturation   DATE;


-- variables de travail
   w_date_debut DATE;
   w_date_fin   DATE;
   w_date_temp  DATE;
   w_id_facture NUMBER;
   w_pers_id    NUMBER;

-- Facture
   w_mnt_facture_ht    NUMBER;
   w_mnt_facture_ttc   NUMBER;
   w_mnt_a_payer_ht    NUMBER;
   w_mnt_a_payer_ttc   NUMBER;

-- ligne de facture
   w_unite             VARCHAR2(250);
   w_lib_ligne_fact    VARCHAR2(250);
   w_mnt_ht            NUMBER;
   w_mnt_ttc           NUMBER;
   w_qte               NUMBER;
   w_prix_ht           NUMBER;
   w_taux_tva          NUMBER;
   w_mois_complet      NUMBER;

   -- statistique
   w_satut_heberge   VARCHAR2(250);
   w_num_etudiant    NUMBER;

BEGIN

  w_pers_id :=-1;

  OPEN  cRestauration (p_date_du_mois);
  FETCH cRestauration INTO c_pers_id, c_id_restauration, c_id_formule, c_lib_formule, c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour, c_id_type_tva, c_id_type_produit, c_date_debut_facturation,c_date_fin_facturation;
  WHILE cRestauration%FOUND LOOP

   IF (w_pers_id != c_pers_id) THEN
   -- Liquider la dernière facture : calculer les  AVOIRS et AVANCES, mettre montant à payer
      IF (w_pers_id >0 ) THEN
              w_mnt_a_payer_ht := w_mnt_facture_ht;
              w_mnt_a_payer_ttc := w_mnt_facture_ttc;
              UPDATE litchi.entete_facture
              SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
              WHERE id_facture = w_id_facture;
			    
              -- Calcul du montant de TVA
              IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
                  UPDATE litchi.entete_facture
                  SET   mnt_facture_tva = w_mnt_facture_ttc - w_mnt_facture_ht
                  WHERE id_facture = w_id_facture;
              END IF;
         
              IF p_bAvances IS NOT NULL AND  p_bAvances = 1 THEN
                PROC_AFFECTATION_AVANCES (w_pers_id, w_id_facture, 2, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
              END IF;
     END IF;

    -- Insert première entête facture
    SELECT litchi.entete_facture_seq.NEXTVAL  INTO  w_id_facture  FROM DUAL;
    INSERT INTO  litchi.entete_facture (id_facture, id_type_facture, lib_facture, persid_payeur, persid_createur, date_creation,date_echeance)
			      VALUES                      (w_id_facture, 2, 'Facture restaurants', c_pers_id, p_persid_createur, sysdate, p_date_echeance);

    LITCHI.REMPLIR_ENTETE_FACTURE (w_id_facture, c_pers_id, 1 );

     w_mnt_facture_ht  :=0;
     w_mnt_facture_ttc :=0;
     w_mnt_a_payer_ht  :=0;
     w_mnt_a_payer_ttc :=0;

     w_pers_id := c_pers_id;
     litchi.statut_individu(w_pers_id, w_num_etudiant, w_satut_heberge);
   END IF;

   w_date_debut := c_date_debut_facturation;

<<loop_date>>
    w_date_temp :=  LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));

    -- chercher un nouveau tarif !
    IF c_date_fin_tarif IS NOT NULL AND w_date_debut > c_date_fin_tarif  THEN
      BEGIN
        SELECT  tf.id_tarif_formule, tf.date_fin, tf.prix_mensuel, tf.prix_par_jour
         INTO c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour
         FROM litchi.tarifs_formule tf
         WHERE tf.id_formule = c_id_formule
         AND (tf.date_debut IS NULL OR tf.date_debut <= w_date_debut) AND (tf.date_fin IS NULL OR tf.date_fin >= w_date_debut)
         AND (tf.prix_mensuel > 0 OR tf.prix_par_jour > 0);
      EXCEPTION
      WHEN OTHERS THEN
     -- garder l'ancien ....
          NULL;
      END;
    END IF;

    IF (TO_NUMBER(TO_CHAR(w_date_debut,'DD')) = 1) AND (LAST_DAY(w_date_debut) = w_date_temp) THEN
      w_mois_complet := 1;
      w_lib_ligne_fact := 'Forfait ' || c_lib_formule || ' du mois : ' ||  TO_CHAR(w_date_debut,'MONTH YYYY');
    ELSE
      w_mois_complet := 0;
      w_lib_ligne_fact := 'Forfait ' || c_lib_formule || ' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY');
    END IF;

 -- Forfait prix fixe !
    IF c_prix_mensuel IS NOT NULL AND c_prix_mensuel > 0 THEN
      w_qte := NULL;
      w_unite := NULL;
      w_prix_ht := c_prix_mensuel;
      w_mnt_ht := w_prix_ht;
    END IF;

    IF c_prix_par_jour IS NOT NULL AND c_prix_par_jour > 0 THEN
-- On compte les jours
      SELECT COUNT (*)
      INTO w_qte
      FROM LITCHI.formules_calendrier fc
      WHERE fc.id_tarif_formule = c_id_tarif
      AND fc.date_jour >= w_date_debut
      AND fc.date_jour <= w_date_temp;

      w_unite := 'JOUR';
      w_prix_ht := c_prix_par_jour;
      w_mnt_ht := w_qte * w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
    END IF;

   -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

    -- Insert lignes facture principal

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_restauration, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_restauration, NULL, NULL,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    w_date_debut := w_date_temp + 1;
    IF (trunc(w_date_temp) < trunc(c_date_fin_facturation)) THEN  GOTO loop_date;  END IF;

  -- Nouveau contrat
      FETCH cRestauration INTO c_pers_id, c_id_restauration, c_id_formule, c_lib_formule, c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour, c_id_type_tva, c_id_type_produit, c_date_debut_facturation,c_date_fin_facturation;
   END LOOP;
 -- Liquider la dernière facture : calculer les  AVOIRS et AVANCES, mettre montant à payer
  IF (w_pers_id >0 ) THEN
             w_mnt_a_payer_ht := w_mnt_facture_ht;
             w_mnt_a_payer_ttc := w_mnt_facture_ttc;

             UPDATE litchi.entete_facture
             SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
             WHERE id_facture = w_id_facture;
			   
             -- Calcul du montant de TVA
             IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
               UPDATE litchi.entete_facture
               SET   mnt_facture_tva =  w_mnt_facture_ttc - w_mnt_facture_ht
               WHERE id_facture = w_id_facture;
             END IF;
         
             IF p_bAvances IS NOT NULL AND  p_bAvances = 1 THEN
                PROC_AFFECTATION_AVANCES (w_pers_id, w_id_facture, 2, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
  END IF;
 CLOSE  cRestauration;
-- EXCEPTION WHEN OTHERS THEN NULL;
END FACTURER_RESTO_MENSUELLE;

/
--------------------------------------------------------
--  DDL for Procedure FACTURER_UNE_LOC
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LITCHI"."FACTURER_UNE_LOC" (p_id_hebergement NUMBER, p_persid_createur NUMBER, p_date_fin_facturation DATE, p_date_echeance DATE, p_bArrhes NUMBER, p_bCAF NUMBER, p_bAvances NUMBER, p_idFacture OUT NUMBER )  AS

-- Sélectionner touts les details hébergement qui ont un type de calcul mensuel, qui n'ont pas pas été facturé (le mois complet, pas encore etc.)
-- Et qu'il y a un tarif valide en face
-- Attention ne sait pas gérer deux tarifs accrochés au même type de tarif si il y a recouvrement
-- Deux factures seront créées !
-- Pour facturer mensuellement en avance p_date_du_mois.

CURSOR cHebergements (cp_id_hebergement NUMBER, cp_date_fin_facturation DATE) IS
   SELECT h.id_hebergement, h.date_reservation, h.pers_id_payeur, dh.id_detail_hebergement, dh.pers_id_concerner,
          dh.id_type_tarif, dh.id_type_calcul,
          NVL2(dh.date_fin_facture, dh.date_fin_facture, dh.date_debut) as date_debut_facturation,
          LEAST(dh.date_fin, cp_date_fin_facturation) as date_fin_facturation,
          ore.lib_objet, dh.date_debut, dh.pers_id_concerner
   FROM litchi.hebergement h, litchi.detail_hebergement dh, litchi.objet_reservable ore

-- hébergement non réservé
   WHERE  h.id_hebergement = cp_id_hebergement
   AND h.id_hebergement = dh.id_hebergement
   AND dh.date_remise_cle is NOT NULL
   AND dh.date_debut <= cp_date_fin_facturation

-- Les dates de facturation en face
   AND (dh.date_fin_facture IS NULL OR (dh.date_fin_facture < cp_date_fin_facturation AND dh.date_fin_facture < dh.date_fin))

   -- objet reservable
   AND ore.id_objet_hebergement = dh.id_objet_hebergement
   ORDER BY h.id_hebergement;

CURSOR cTarifsFils ( cp_id_tarif_pere NUMBER, cp_id_type_calcul NUMBER, cp_date_du_mois DATE) IS
   SELECT  ta.id_tarif, tt.libelle_type_tarif, ta.prix, tt.code_type_tarif, tt.type_tva_loc, tt.id_type_produit
   FROM litchi.tarif ta, litchi.type_tarif tt
   WHERE tt.type_tarif_pere IS NOT NULL AND tt.type_tarif_pere = cp_id_tarif_pere
   AND tt.code_type_tarif = ta.type_tarif
   AND ta.id_type_calcul = cp_id_type_calcul
   AND ((ta.date_debut IS NULL OR ta.date_debut <= cp_date_du_mois) AND (ta.date_fin IS NULL OR ta.date_fin >= cp_date_du_mois))
   ORDER by ta.id_tarif;

 CURSOR cTarifsFilsForfait ( cp_id_tarif_pere NUMBER, cp_id_type_calcul NUMBER, cp_date_du_mois DATE, cp_de_x_jour NUMBER, cp_a_y_jour NUMBER ) IS
   SELECT  ta.id_tarif, tt.libelle_type_tarif, ta.prix, tt.code_type_tarif, tt.type_tva_loc, tt.id_type_produit
   FROM litchi.tarif ta, litchi.type_tarif tt
   WHERE tt.type_tarif_pere IS NOT NULL AND tt.type_tarif_pere = cp_id_tarif_pere
   AND tt.code_type_tarif = ta.type_tarif
   AND ta.id_type_calcul = cp_id_type_calcul
   AND ta.de_x_jour = cp_de_x_jour
   AND ta.a_y_jour = cp_a_y_jour
   AND ((ta.date_debut IS NULL OR ta.date_debut <= cp_date_du_mois) AND (ta.date_fin IS NULL OR ta.date_fin >= cp_date_du_mois))
 ORDER by ta.id_tarif;

CURSOR cElements (cp_id_detail_hebergement  NUMBER) IS
   SELECT   rdhea.id_repart_det_heb_ea, ea.lib_element_associe, ea.montant, ea.type_tva_ea, ea.id_type_produit
		  FROM		litchi.repart_det_heb_ele_ass rdhea, litchi.element_associe ea, litchi.detail_hebergement dh
		  WHERE   dh.id_detail_hebergement = cp_id_detail_hebergement
      AND     rdhea.id_detail_hebergement = dh.id_detail_hebergement
      AND     rdhea.id_element_associe = ea.code_element_associe
-- A la premiere facture
      AND     ea.id_type_facturation = 2
		  AND     rdhea.id_ligne_facture IS NULL
		  ORDER BY rdhea.id_repart_det_heb_ea;

-- variables curseur principal
   c_id_hebergement NUMBER;
   c_date_reservation DATE;
   c_pers_id_payeur NUMBER;
   c_id_detail_hebergement NUMBER;
   c_persid_concerner NUMBER;
   c_id_type_tarif NUMBER;
   c_id_type_calcul NUMBER;
   c_id_tarif NUMBER;
   c_date_fin_tarif DATE;
   c_id_type_tva NUMBER;
   c_id_taux_tva NUMBER;
   c_taux_tva    NUMBER;
   c_id_type_produit NUMBER;
   c_date_debut_facturation DATE;
   c_date_fin_facturation DATE;
   c_lib_objet_reservable VARCHAR2(250);
   c_lib_ligne_fact    VARCHAR2(250);
   c_prix_ht            NUMBER;
   c_dh_date_debut      DATE;

-- variables de travail
   w_date_debut DATE;
   w_date_fin   DATE;
   w_date_temp  DATE;
   w_id_facture NUMBER;
   w_id_hebergement NUMBER;

-- Facture
   w_mnt_facture_ht    NUMBER;
   w_mnt_facture_ttc   NUMBER;
   w_mnt_a_payer_ht    NUMBER;
   w_mnt_a_payer_ttc   NUMBER;
   w_pers_id_payeur    NUMBER;

-- ligne de facture
   w_unite             VARCHAR2(250);
   w_lib_ligne_fact    VARCHAR2(250);
   w_mnt_ht            NUMBER;
   w_mnt_ttc           NUMBER;
   w_qte               NUMBER;
   w_prix_ht           NUMBER;
   w_taux_tva          NUMBER;
   w_id_rdhea          NUMBER;
   w_id_type_tarif     NUMBER;
   w_id_tarif          NUMBER;
   w_id_type_produit   NUMBER;
   w_id_type_tva       NUMBER;
   w_mois_complet      NUMBER;
   w_pers_id_loc       NUMBER;

-- Forfait
   w_de_x_jour        NUMBER;
   w_a_y_jour         NUMBER;
   w_type_forfait     NUMBER;
   w_periode_loc      NUMBER;

-- variable temporaire
   temp_n1       NUMBER;
   temp_d1       DATE;
   temp_n2       NUMBER;

-- statistique
   w_satut_heberge   VARCHAR2(250);
   w_num_etudiant    NUMBER;
   w_id_concerner    NUMBER;

BEGIN

  IF p_bCAF IS NOT NULL AND p_bCAF = 1 THEN
                PRE_AFFECTATION_CAF ();
  END IF;

 OPEN cHebergements (p_id_hebergement, p_date_fin_facturation) ;

 p_idFacture := NULL;
 w_id_hebergement := -1;
 w_id_concerner := -1;
 FETCH cHebergements INTO  c_id_hebergement, c_date_reservation, c_pers_id_payeur, c_id_detail_hebergement, c_persid_concerner, c_id_type_tarif, c_id_type_calcul, c_date_debut_facturation, c_date_fin_facturation, c_lib_objet_reservable, c_dh_date_debut, w_pers_id_loc;
 WHILE cHebergements%FOUND LOOP

 -- Tester si on trouve un tarif en face
  SELECT COUNT(*) INTO c_id_tarif
  FROM litchi.type_tarif tt, litchi.tarif ta
  -- Le tarif en face avec une date de validité au jour de facturation
  -- Trouver le bon tarif à la date de début de facturation !
  -- Pas de changement de tarif si changement de tarif
  WHERE  tt.code_type_tarif = c_id_type_tarif
  AND   tt.code_type_tarif = ta.type_tarif
  AND ta.id_type_calcul = c_id_type_calcul
  AND ((ta.date_debut IS NULL OR ta.date_debut <= c_date_debut_facturation) AND (ta.date_fin IS NULL OR ta.date_fin >= c_date_debut_facturation))
  AND tt.type_tarif_pere is NULL;

  IF c_id_tarif = 0 THEN GOTO au_suivant; END IF;

  IF c_id_type_calcul = 1 OR c_id_type_calcul = 3 THEN
   SELECT ta.id_tarif, ta.date_fin, tt.libelle_type_tarif, ta.prix, tt.type_tva_loc, tt.id_type_produit
   INTO   c_id_tarif,  c_date_fin_tarif, c_lib_ligne_fact, c_prix_ht, c_id_type_tva, c_id_type_produit
   FROM litchi.type_tarif tt, litchi.tarif ta
  -- Le tarif en face avec une date de validité au jour de facturation
  -- Trouver le bon tarif à la date de début de facturation !
  -- Pas de changement de tarif si changement de tarif
  WHERE  tt.code_type_tarif = c_id_type_tarif
  AND   tt.code_type_tarif = ta.type_tarif
  AND ta.id_type_calcul = c_id_type_calcul
  AND ((ta.date_debut IS NULL OR ta.date_debut <= c_date_debut_facturation) AND (ta.date_fin IS NULL OR ta.date_fin >= c_date_debut_facturation))
  AND tt.type_tarif_pere is NULL;
  END IF;

-- Le forfait mérite un traitement spécial

  IF c_id_type_calcul = 2 THEN

-- identifier type 1 ou type 2
-- Type 1 possède que des de_x_jours = a_y_jours au moins 2
-- Type 2 = période de tarifs fixes

    SELECT COUNT (*)
    INTO   w_type_forfait
    FROM litchi.type_tarif tt, litchi.tarif ta
    -- Le tarif en face avec une date de validité au jour de facturation
    -- Trouver le bon tarif à la date de début de facturation !
    -- Pas de changement de tarif si changement de tarif
    WHERE  tt.code_type_tarif = c_id_type_tarif
    AND   tt.code_type_tarif = ta.type_tarif
    AND ta.id_type_calcul = c_id_type_calcul
    AND ((ta.date_debut IS NULL OR ta.date_debut <= c_date_debut_facturation) AND (ta.date_fin IS NULL OR ta.date_fin >= c_date_debut_facturation))
    AND tt.type_tarif_pere is NULL
    AND ta.de_x_jour = ta.a_y_jour;

    IF w_type_forfait > 1 THEN w_type_forfait :=1; ELSE  w_type_forfait :=2; END IF;

-- Ramène les types tva et produits

    SELECT MAX (ta.prix)
    INTO   w_prix_ht
    FROM litchi.type_tarif tt, litchi.tarif ta
    -- Le tarif en face avec une date de validité au jour de facturation
    -- Trouver le bon tarif à la date de début de facturation !
    -- Pas de changement de tarif si changement de tarif
    WHERE  tt.code_type_tarif = c_id_type_tarif
    AND   tt.code_type_tarif = ta.type_tarif
    AND ta.id_type_calcul = c_id_type_calcul
    AND ((ta.date_debut IS NULL OR ta.date_debut <= c_date_debut_facturation) AND (ta.date_fin IS NULL OR ta.date_fin >= c_date_debut_facturation))
    AND tt.type_tarif_pere is NULL;

    SELECT ta.id_tarif, ta.date_fin, tt.libelle_type_tarif, ta.prix, tt.type_tva_loc, tt.id_type_produit
    INTO   c_id_tarif,  c_date_fin_tarif, c_lib_ligne_fact, c_prix_ht, c_id_type_tva, c_id_type_produit
    FROM litchi.type_tarif tt, litchi.tarif ta
    -- Le tarif en face avec une date de validité au jour de facturation
    -- Trouver le bon tarif à la date de début de facturation !
    -- Pas de changement de tarif si changement de tarif
    WHERE  tt.code_type_tarif = c_id_type_tarif
    AND   tt.code_type_tarif = ta.type_tarif
    AND ta.id_type_calcul = c_id_type_calcul
    AND ((ta.date_debut IS NULL OR ta.date_debut <= c_date_debut_facturation) AND (ta.date_fin IS NULL OR ta.date_fin >= c_date_debut_facturation))
    AND tt.type_tarif_pere is NULL
    AND ta.prix = w_prix_ht;
  END IF;

 -- Liquider la dernière facture : calculer les Arrhes, CAF, AVOIRS et AVANCES, mettre montant à payer
   IF (w_id_hebergement != c_id_hebergement) THEN
      IF (w_id_hebergement >0 ) THEN
              w_mnt_a_payer_ht := w_mnt_facture_ht;
              w_mnt_a_payer_ttc := w_mnt_facture_ttc;
         UPDATE litchi.entete_facture
         SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
         WHERE id_facture = w_id_facture;
		   
         -- Calcul du montant de TVA
         IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
           UPDATE litchi.entete_facture
           SET   mnt_facture_tva =  w_mnt_facture_ttc - w_mnt_facture_ht
           WHERE id_facture = w_id_facture;
         END IF;
         
         IF p_bArrhes IS NOT NULL AND p_bArrhes = 1 THEN
                PROC_AFFECTATION_ARRHES (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
         END IF;

         IF  p_bCAF IS NOT NULL AND p_bCAF = 1 AND w_mnt_a_payer_ttc > 0 THEN
                PROC_AFFECTATION_CAF (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
         END IF;
          
         IF p_bAvances IS NOT NULL AND p_bAvances = 1 AND w_mnt_a_payer_ttc > 0  THEN
                PROC_AFFECTATION_AVANCES (w_pers_id_payeur, w_id_facture, 1, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
         END IF;
      END IF;

    -- Insert première entête facture
     SELECT litchi.entete_facture_seq.NEXTVAL  INTO  w_id_facture  FROM DUAL;
     p_idFacture := w_id_Facture;
     INSERT INTO  litchi.entete_facture (id_facture, id_type_facture, lib_facture, persid_payeur, persid_createur, date_creation,date_echeance)
			      VALUES                      (w_id_facture, 1, 'Facture hébergement', c_pers_id_payeur, p_persid_createur, sysdate, p_date_echeance);

     LITCHI.REMPLIR_ENTETE_FACTURE (w_id_facture, c_pers_id_payeur, 1 );

     w_mnt_facture_ht  :=0;
     w_mnt_facture_ttc :=0;
     w_mnt_a_payer_ht  :=0;
     w_mnt_a_payer_ttc :=0;
     
     w_pers_id_payeur  :=c_pers_id_payeur;
     w_id_hebergement := c_id_hebergement;
   END IF;

    IF c_persid_concerner IS NOT NULL THEN
      IF c_persid_concerner != w_id_concerner THEN
               litchi.statut_individu(c_persid_concerner, w_num_etudiant, w_satut_heberge);
               w_id_concerner := c_persid_concerner;
      END IF;
    ELSE
       w_num_etudiant := NULL;
       w_id_concerner := NULL;
       w_satut_heberge := NULL;
    END IF;

    -- Recherche dépôt de garantie frais de dossiers etc. et insertion de ce qui a à payer à la première facture

    OPEN cElements (c_id_detail_hebergement);
    FETCH cElements INTO  w_id_rdhea, w_lib_ligne_fact, w_prix_ht , w_id_type_tva, w_id_type_produit;
    WHILE cElements%FOUND LOOP
       IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, p_date_fin_facturation, w_taux_tva);
       ELSE
       w_taux_tva := NULL;
       END IF;
       IF w_taux_tva IS NULL THEN
        w_mnt_ht := w_prix_ht;
        w_mnt_ttc := w_mnt_ht;
       ELSE
        w_mnt_ht := w_prix_ht ;
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
       END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;
       w_lib_ligne_fact :=  w_lib_ligne_fact ||' en date du : ' || TO_CHAR(c_date_reservation, 'DD/MM/YYYY');

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
      prix_unitaire_ht, mnt_ht, mnt_ttc, type_tva_lf, id_type_produit, status_heberge, id_repart_det_heb_ea, taux_tva,
      mois, annee, date_debut_facture, date_fin_facture)
		 VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
              w_prix_ht,  w_mnt_ht, w_mnt_ttc, w_id_type_tva, w_id_type_produit, w_satut_heberge, w_id_rdhea, w_taux_tva,
              TO_NUMBER(TO_CHAR(c_date_debut_facturation,'MM')), TO_NUMBER(TO_CHAR(c_date_debut_facturation,'YYYY')),
              c_date_debut_facturation, c_date_debut_facturation );

    FETCH cElements INTO  w_id_rdhea, w_lib_ligne_fact, w_prix_ht , w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE cElements;


   IF c_id_type_calcul = 1 THEN
        -- découpage en mois
       w_date_debut := c_date_debut_facturation;

<<loop_mensuel>>
      -- On veut gérer le cas problématique du départ le premier du mois !
      -- Cas de l'arrivée en fin de mois
      -- Cas de facturation sur moins d'un mois calendaire
      IF (w_date_debut = LAST_DAY(w_date_debut)) THEN
	    -- Arrivé en fin mois
       IF c_dh_date_debut = w_date_debut THEN 
		      w_date_temp := w_date_debut + 1; 
		   ELSE
		      w_date_debut := w_date_debut + 1;
          w_date_temp := LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));
		  -- Si la fin est au premier on décrémente ;) cling cling ...
		  -- Si pas un mois calendaire 
        IF (w_date_temp-w_date_debut)<1 OR 
           (w_date_temp <> LAST_DAY(w_date_temp) AND LAST_DAY(c_dh_date_debut) = LAST_DAY(w_date_debut-1)) THEN 
            w_date_debut := w_date_debut-1; 
			  END IF;
		   END IF;
      ELSE
          w_date_temp :=  LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));
      END IF;

      -- chercher un nouveau tarif !
      IF c_date_fin_tarif IS NOT NULL AND w_date_debut > c_date_fin_tarif  THEN
         BEGIN
            SELECT   ta.id_tarif, ta.date_fin, ta.prix
            INTO    c_id_tarif, c_date_fin_tarif, c_prix_ht
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = c_id_type_calcul
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut));
   --      EXCEPTION WHEN OTHERS THEN  NULL;
         END;
        -- garder l'ancien ....
      END IF;

     IF (TO_NUMBER(TO_CHAR(w_date_debut,'DD')) = 1) AND (LAST_DAY(w_date_debut) = w_date_temp) THEN
       w_mois_complet := 1;
       w_qte := NULL;
       w_unite := NULL;
       w_prix_ht := c_prix_ht;
       w_mnt_ht := c_prix_ht;
       w_lib_ligne_fact := 'Loyer du mois : ' || TO_CHAR(w_date_debut,'MONTH YYYY') || ' pour ' ||  c_lib_ligne_fact ||' au '|| c_lib_objet_reservable ;
   ELSE
     w_mois_complet := 0;
     w_qte := w_date_temp  - w_date_debut ;
     w_unite := 'NUIT';
     -- Règle du trentième
     w_prix_ht := c_prix_ht / 30.0;
     w_mnt_ht :=  w_qte * w_prix_ht;
     w_mnt_ht := round (w_mnt_ht , 2);
     w_lib_ligne_fact := c_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY') ||' au '|| c_lib_objet_reservable;
   END IF;
   
    -- Afficher le nom du locataire si different de celui du payeur 
     IF (w_pers_id_loc IS NOT NULL) AND (w_pers_id_loc <> w_pers_id_payeur) THEN 
          w_lib_ligne_fact := w_lib_ligne_fact ||'. Locataire ' || litchi.nom_prenom_personne(w_pers_id_loc); 
     END IF;
     
     -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

     -- Insert lignes facture principal

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
        VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, NULL, w_mnt_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_detail_hebergement, c_id_type_tarif, c_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

     -- Chercher les fils
    OPEN cTarifsFils ( c_id_type_tarif , c_id_type_calcul, w_date_debut);
    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif , w_id_type_tva, w_id_type_produit;
    WHILE cTarifsFils%FOUND LOOP

    IF w_mois_complet = 1 THEN
      w_qte := NULL;
      w_unite := NULL;
      w_lib_ligne_fact := w_lib_ligne_fact || ' de ' || TO_CHAR(w_date_debut,'MONTH YYYY')||' au '|| c_lib_objet_reservable;
      w_mnt_ht := w_prix_ht;
    ELSE
       --     Règle du trentième
      w_prix_ht := w_prix_ht / 30.0;
      w_mnt_ht :=  w_qte * w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
      w_unite := 'NUIT';
      w_lib_ligne_fact := w_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY')||' au '|| c_lib_objet_reservable;
    END IF;

-- chercher tva
    IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, w_date_debut, w_taux_tva);
    ELSE
       w_taux_tva := NULL;
    END IF;
    IF w_taux_tva IS NULL THEN
      w_mnt_ttc := w_mnt_ht;
    ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
    END IF;
    w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
    w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;


     INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, NULL, w_mnt_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, w_id_type_tva, w_id_type_produit, c_id_detail_hebergement, w_id_type_tarif, w_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif, w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE  cTarifsFils;

    w_date_debut := w_date_temp;
    IF (w_date_temp<c_date_fin_facturation) THEN  GOTO loop_mensuel;  END IF;
 END IF;

-- Forfait chiant et compliqué
--    Exemple de forfait type 1
--    1 nuit = 48
--    1 semaine = 230
--    15 nuits = 460
--    30 nuits ou 1 mois  = 800 ¿
--    On va chercher le plus grand bloc que l'on peut casser dans la durée
--    On découpe suivant la fin des tarifs et des forfaits
--    La recherche du meilleur prix se fait à chaque instant.

-- Deuxième type : type 2
-- 1 à 7 nuit = 200
-- 8 à 15 nuits = 300

  IF c_id_type_calcul = 2 THEN
          -- découpage en zone de forfait
          -- la recherche du tarif est dynamique donc les changements sont automatiques.
       w_date_debut := c_date_debut_facturation;
<<loop_forfait>>
        w_date_temp := c_date_fin_facturation;
 -- On calcule le nombre de nuit
        w_periode_loc := w_date_temp - w_date_debut;
 -- On va chercher le tarif le plus grand plus petit au nombre de nuit
    IF w_type_forfait =1 THEN
       BEGIN
            SELECT   MAX(ta.a_y_jour)
            INTO   w_a_y_jour
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = c_id_type_calcul
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut))
            AND ta.a_y_jour <= w_periode_loc;

            SELECT   ta.id_tarif, ta.date_fin, ta.prix, ta.de_x_jour
            INTO    c_id_tarif, c_date_fin_tarif, c_prix_ht, w_de_x_jour
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = c_id_type_calcul
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut))
            AND ta.a_y_jour = w_a_y_jour;
     --        EXCEPTION  WHEN OTHERS THEN   NULL;
     -- garder l'ancien ....

      END;
         w_unite := TO_CHAR (w_a_y_jour) || 'NUIT';
         IF w_a_y_jour=0 THEN w_a_y_jour :=1; END IF;
         w_qte := TRUNC (w_periode_loc / w_a_y_jour);
         w_periode_loc := MOD (w_periode_loc, w_a_y_jour);
         w_date_temp := c_date_fin_facturation - w_periode_loc;
     END IF;

    IF w_type_forfait = 2 THEN
       BEGIN
            SELECT   ta.id_tarif, ta.date_fin, ta.prix, ta.de_x_jour, ta.a_y_jour
            INTO    c_id_tarif, c_date_fin_tarif, c_prix_ht, w_de_x_jour, w_a_y_jour
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = c_id_type_calcul
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut))
            AND ta.a_y_jour >= w_periode_loc
            AND ta.de_x_jour <= w_periode_loc;
     --        EXCEPTION  WHEN OTHERS THEN   NULL;
     -- garder l'ancien ....

      END;
         w_unite := TO_CHAR (w_de_x_jour) || ' NUIT ' || TO_CHAR (w_a_y_jour) || 'NUIT';
         w_qte := 1;
     END IF;


     -- Règle du forfait
     w_prix_ht := c_prix_ht;
     w_mnt_ht :=  w_qte * w_prix_ht;
     w_mnt_ht := round (w_mnt_ht , 2);
     w_lib_ligne_fact := c_lib_ligne_fact ||' forfait du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY') ||' au '|| c_lib_objet_reservable;

     -- Afficher le nom du locataire si different de celui du payeur 
     IF (w_pers_id_loc IS NOT NULL) AND (w_pers_id_loc <> w_pers_id_payeur) THEN 
          w_lib_ligne_fact := w_lib_ligne_fact ||'. Locataire ' || litchi.nom_prenom_personne(w_pers_id_loc); 
     END IF;
     
     -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

     -- Insert lignes facture principal

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
        VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_detail_hebergement, c_id_type_tarif, c_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

     -- Chercher les fils
    OPEN cTarifsFilsForfait ( c_id_type_tarif , c_id_type_calcul, w_date_debut, w_de_x_jour, w_a_y_jour);
    FETCH cTarifsFilsForfait INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif , w_id_type_tva, w_id_type_produit;
    WHILE cTarifsFilsForfait%FOUND LOOP

      IF w_type_forfait =1 THEN
          w_prix_ht := c_prix_ht;
          w_mnt_ht :=  w_qte * w_prix_ht;
          w_mnt_ht := round (w_mnt_ht , 2);
      END IF;
 -- chercher le tarif au dessus
      IF w_type_forfait = 2 THEN
           w_prix_ht := c_prix_ht;
          w_mnt_ht := w_prix_ht;
      END IF;

      w_lib_ligne_fact := w_lib_ligne_fact ||' forfait du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY')||' au '|| c_lib_objet_reservable;

-- chercher tva
    IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, w_date_debut, w_taux_tva);
    ELSE
       w_taux_tva := NULL;
    END IF;
    IF w_taux_tva IS NULL THEN
      w_mnt_ttc := w_mnt_ht;
    ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
    END IF;
    w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
    w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;


     INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, w_id_type_tva, w_id_type_produit, c_id_detail_hebergement, w_id_type_tarif, w_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    FETCH cTarifsFilsForfait INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif, w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE  cTarifsFilsForfait;

    w_date_debut := w_date_temp;
    IF (w_date_temp<c_date_fin_facturation) THEN  GOTO loop_forfait;  END IF;
  END IF;

-- Journalier  le plus facile à calculer
-- On découpe suivant la fin des tarifs
 IF c_id_type_calcul = 3 THEN
         -- découpage en zone de changement de tarif s'il y a
       w_date_debut := c_date_debut_facturation;

<<loop_journalier>>
      -- chercher un nouveau tarif !
      IF c_date_fin_tarif IS NOT NULL AND w_date_debut > c_date_fin_tarif  THEN
         BEGIN
            SELECT   ta.id_tarif, ta.date_fin, ta.prix
            INTO    c_id_tarif, c_date_fin_tarif, c_prix_ht
            FROM litchi.tarif ta, litchi.type_tarif tt
            WHERE tt.type_tarif_pere IS NULL
            AND tt.code_type_tarif = ta.type_tarif
            AND tt.code_type_tarif = c_id_type_tarif
            AND ta.id_type_calcul = c_id_type_calcul
            AND ((ta.date_debut IS NULL OR ta.date_debut <= w_date_debut) AND (ta.date_fin IS NULL OR ta.date_fin >= w_date_debut));
  --        EXCEPTION WHEN OTHERS THEN  NULL;
        END;
  -- garder l'ancien ....
     END IF;

     IF c_date_fin_tarif IS NOT NULL THEN
            w_date_temp :=  LEAST(c_date_fin_facturation, c_date_fin_tarif+1);
     ELSE
            w_date_temp := c_date_fin_facturation;
     END IF;

     w_qte := w_date_temp  - w_date_debut;
     w_unite := 'NUIT';
     w_prix_ht := c_prix_ht;
     w_mnt_ht :=  w_qte * w_prix_ht;
     w_mnt_ht := round (w_mnt_ht , 2);
     w_lib_ligne_fact := c_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY') ||' au '|| c_lib_objet_reservable;

     -- Afficher le nom du locataire si different de celui du payeur 
     IF (w_pers_id_loc IS NOT NULL) AND (w_pers_id_loc <> w_pers_id_payeur) THEN 
          w_lib_ligne_fact := w_lib_ligne_fact ||'. Locataire ' || litchi.nom_prenom_personne(w_pers_id_loc); 
     END IF;
     
     -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

     -- Insert lignes facture principal

    INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
        VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_detail_hebergement, c_id_type_tarif, c_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

     -- Chercher les fils
    OPEN cTarifsFils ( c_id_type_tarif, c_id_type_calcul, w_date_debut);
    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif , w_id_type_tva, w_id_type_produit;
    WHILE cTarifsFils%FOUND LOOP
      w_mnt_ht :=  w_qte * w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
      w_unite := 'NUIT';
      w_lib_ligne_fact := w_lib_ligne_fact ||' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY')||' au '|| c_lib_objet_reservable;

-- chercher tva
    IF w_id_type_tva IS NOT NULL THEN
          calcul_tva (w_id_type_tva, w_date_debut, w_taux_tva);
    ELSE
       w_taux_tva := NULL;
    END IF;
    IF w_taux_tva IS NULL THEN
      w_mnt_ttc := w_mnt_ht;
    ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
    END IF;
    w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
    w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

    INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_detail_hebergement, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, w_id_type_tva, w_id_type_produit, c_id_detail_hebergement, w_id_type_tarif, w_id_tarif,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    FETCH cTarifsFils INTO  w_id_tarif, w_lib_ligne_fact, w_prix_ht,  w_id_type_tarif, w_id_type_tva, w_id_type_produit;
    END LOOP;
    CLOSE  cTarifsFils;

    w_date_debut := w_date_temp;
    IF (w_date_temp<c_date_fin_facturation) THEN  GOTO loop_journalier;  END IF;
  END IF;

    -- Nouvel hébergement
<<au_suivant>>
    FETCH cHebergements INTO  c_id_hebergement, c_date_reservation, c_pers_id_payeur, c_id_detail_hebergement, c_persid_concerner, c_id_type_tarif, c_id_type_calcul, c_date_debut_facturation, c_date_fin_facturation, c_lib_objet_reservable, c_dh_date_debut, w_pers_id_loc;
 END LOOP;

 -- Liquider la dernière facture : calculer les Arrhes, CAF, AVOIRS et AVANCES, mettre montant à payer
    IF (w_id_hebergement >0 ) THEN
             w_mnt_a_payer_ht := w_mnt_facture_ht;
             w_mnt_a_payer_ttc := w_mnt_facture_ttc;

             UPDATE litchi.entete_facture
             SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
             WHERE id_facture = w_id_facture;
  
             -- Calcul du montant de TVA
             IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
                  UPDATE litchi.entete_facture
                  SET   mnt_facture_tva = w_mnt_facture_ttc - w_mnt_facture_ht
                  WHERE id_facture = w_id_facture;
             END IF;
         
             IF p_bArrhes IS NOT NULL AND p_bArrhes = 1 THEN
                PROC_AFFECTATION_ARRHES (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;

             IF  p_bCAF IS NOT NULL AND p_bCAF = 1 AND w_mnt_a_payer_ttc > 0 THEN
                PROC_AFFECTATION_CAF (w_pers_id_payeur, w_id_facture, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
          
             IF p_bAvances IS NOT NULL AND p_bAvances = 1 AND w_mnt_a_payer_ttc > 0  THEN
                PROC_AFFECTATION_AVANCES (w_pers_id_payeur, w_id_facture, 1, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
    END IF;

 CLOSE cHebergements;

-- EXCEPTION WHEN OTHERS THEN RETURN;
END FACTURER_UNE_LOC;

/
--------------------------------------------------------
--  DDL for Procedure FACTURER_UN_RESTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LITCHI"."FACTURER_UN_RESTO" (p_pers_id NUMBER, p_persid_createur NUMBER, p_date_fin_facturation DATE, p_date_echeance DATE, p_bAvances NUMBER, p_idFacture OUT NUMBER) AS
CURSOR cRestauration (cp_pers_id NUMBER, cp_date_fin_facturation DATE ) IS
  SELECT  r.pers_id, r.id_restauration , f.id_formule, f.lib_formule, tf.id_tarif_formule, tf.date_fin, tf.prix_mensuel, tf.prix_par_jour, f.type_tva_rest, f.id_type_produit,
          NVL2(r.date_fin_facture, r.date_fin_facture+1, r.date_debut) as date_debut_facturation,
          LEAST(r.date_fin,  cp_date_fin_facturation) as date_fin_facturation
   FROM litchi.restauration r, litchi.formule f, litchi.tarifs_formule tf, litchi.detail_hebergement dh
   WHERE r.id_formule = f.id_formule
   AND  f.id_formule = tf.id_formule
   AND  r.pers_id = cp_pers_id
-- Le tarif en face avec une date de validité au jour de facturation
-- Trouver le bon tarif à la date de début de facturation !
-- Il faut appliquer le bon tarif après chaque mois
-- Chaque changement de tarif se fait en début de mois en guise de simplification
-- Pas de changement de tarif si changement de tarif dans la période
   AND (tf.date_debut IS NULL OR tf.date_debut <= NVL(r.date_fin_facture, r.date_debut)) AND (tf.date_fin IS NULL OR tf.date_fin >= NVL(r.date_fin_facture, r.date_debut))
   AND (tf.prix_mensuel > 0 OR tf.prix_par_jour > 0)

-- On ne facture pas ceux qui ne sont pas en resa QDD !
   AND dh.id_restauration (+)  = r.id_restauration
   AND ( dh.date_remise_cle IS NOT NULL OR dh.id_detail_hebergement IS NULL)

-- Les dates de facturation en face
   AND (r.date_fin_facture IS NULL OR (r.date_fin_facture <  cp_date_fin_facturation AND r.date_fin_facture < r.date_fin))
   AND r.date_debut < cp_date_fin_facturation
   AND r.date_debut <= r.date_fin
   
   ORDER BY r.date_fin;

 -- variables curseur principal
   c_pers_id         NUMBER;
   c_id_restauration NUMBER;
   c_id_formule      NUMBER;
   c_lib_formule     VARCHAR2(250);
   c_id_tarif        NUMBER;
   c_taux_tva        NUMBER;
   c_date_fin_tarif  DATE;
   c_prix_mensuel    NUMBER;
   c_prix_par_jour   NUMBER;
   c_id_type_tva     NUMBER;
   c_id_taux_tva     NUMBER;
   c_id_type_produit        NUMBER;
   c_date_debut_facturation DATE;
   c_date_fin_facturation   DATE;


-- variables de travail
   w_date_debut DATE;
   w_date_fin   DATE;
   w_date_temp  DATE;
   w_id_facture NUMBER;
   w_pers_id    NUMBER;

-- Facture
   w_mnt_facture_ht    NUMBER;
   w_mnt_facture_ttc   NUMBER;
   w_mnt_a_payer_ht    NUMBER;
   w_mnt_a_payer_ttc   NUMBER;

-- ligne de facture
   w_unite             VARCHAR2(250);
   w_lib_ligne_fact    VARCHAR2(250);
   w_mnt_ht            NUMBER;
   w_mnt_ttc           NUMBER;
   w_qte               NUMBER;
   w_prix_ht           NUMBER;
   w_taux_tva          NUMBER;
   w_mois_complet      NUMBER;

   -- statistique
   w_satut_heberge   VARCHAR2(250);
   w_num_etudiant    NUMBER;

BEGIN
  p_idFacture := NULL;
  w_pers_id :=-1;

  OPEN  cRestauration ( p_pers_id, p_date_fin_facturation);
  FETCH cRestauration INTO c_pers_id, c_id_restauration, c_id_formule, c_lib_formule, c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour, c_id_type_tva, c_id_type_produit, c_date_debut_facturation,c_date_fin_facturation;
  WHILE cRestauration%FOUND LOOP

   IF (w_pers_id != c_pers_id) THEN
   -- Liquider la dernière facture : calculer les  AVOIRS et AVANCES, mettre montant à payer
      IF (w_pers_id >0 ) THEN
              w_mnt_a_payer_ht := w_mnt_facture_ht;
              w_mnt_a_payer_ttc := w_mnt_facture_ttc;
              UPDATE litchi.entete_facture
              SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
              WHERE id_facture = w_id_facture;
			    
              -- Calcul du montant de TVA
              IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
                  UPDATE litchi.entete_facture
                  SET   mnt_facture_tva =  w_mnt_facture_ttc - w_mnt_facture_ht
                  WHERE id_facture = w_id_facture;
              END IF;
			  
              IF p_bAvances IS NOT NULL AND  p_bAvances = 1 THEN
                PROC_AFFECTATION_AVANCES (w_pers_id, w_id_facture, 2, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
              END IF;
     END IF;

    -- Insert première entête facture
    SELECT litchi.entete_facture_seq.NEXTVAL  INTO  w_id_facture  FROM DUAL;
    p_idFacture := w_id_Facture;
    INSERT INTO  litchi.entete_facture (id_facture, id_type_facture, lib_facture, persid_payeur, persid_createur, date_creation,date_echeance)
			      VALUES                      (w_id_facture, 2, 'Facture restaurants', c_pers_id, p_persid_createur, sysdate, p_date_echeance);

    LITCHI.REMPLIR_ENTETE_FACTURE (w_id_facture, c_pers_id, 1 );

     w_mnt_facture_ht  :=0;
     w_mnt_facture_ttc :=0;
     w_mnt_a_payer_ht  :=0;
     w_mnt_a_payer_ttc :=0;

     w_pers_id := c_pers_id;
     litchi.statut_individu(w_pers_id, w_num_etudiant, w_satut_heberge);
   END IF;

   w_date_debut := c_date_debut_facturation;

<<loop_date>>
    w_date_temp :=  LEAST(c_date_fin_facturation, LAST_DAY(w_date_debut));

    -- chercher un nouveau tarif !
    IF c_date_fin_tarif IS NOT NULL AND w_date_debut > c_date_fin_tarif  THEN
      BEGIN
         SELECT  tf.id_tarif_formule, tf.date_fin, tf.prix_mensuel, tf.prix_par_jour
         INTO c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour
         FROM litchi.tarifs_formule tf
         WHERE tf.id_formule = c_id_formule
         AND (tf.date_debut IS NULL OR tf.date_debut <= w_date_debut) AND (tf.date_fin IS NULL OR tf.date_fin >= w_date_debut)
         AND (tf.prix_mensuel > 0 OR tf.prix_par_jour > 0);
      EXCEPTION
      WHEN OTHERS THEN
     -- garder l'ancien ....
          NULL;
      END;
    END IF;

 -- Forfait prix fixe !
    IF c_prix_mensuel IS NOT NULL AND c_prix_mensuel > 0 THEN
-- C'est un forfait mensuel
      w_date_temp := LAST_DAY(w_date_debut);
      w_qte := NULL;
      w_unite := NULL;
      w_prix_ht := c_prix_mensuel;
      w_mnt_ht := w_prix_ht;
    END IF;

    IF c_prix_par_jour IS NOT NULL AND c_prix_par_jour > 0 THEN
-- On compte les jours
      SELECT COUNT (*)
      INTO w_qte
      FROM LITCHI.formules_calendrier fc
      WHERE fc.id_tarif_formule = c_id_tarif
      AND fc.date_jour >= w_date_debut
      AND fc.date_jour <= w_date_temp;

      w_unite := 'JOUR';
      w_prix_ht := c_prix_par_jour;
      w_mnt_ht := w_qte * w_prix_ht;
      w_mnt_ht := round (w_mnt_ht , 2);
    END IF;

   -- Chercher tva
     IF c_id_type_tva IS NOT NULL THEN
          calcul_tva (c_id_type_tva, w_date_debut, w_taux_tva);
     ELSE
       w_taux_tva := NULL;
     END IF;
     IF w_taux_tva IS NULL THEN
        w_mnt_ttc := w_mnt_ht;
     ELSE
        w_mnt_ttc := w_mnt_ht * ( 1 + w_taux_tva/100.0);
        w_mnt_ttc := round (w_mnt_ttc , 2 );
     END IF;
       w_mnt_facture_ht  := w_mnt_facture_ht + w_mnt_ht;
       w_mnt_facture_ttc := w_mnt_facture_ttc + w_mnt_ttc;

-- Ligne de libellé

    IF (TO_NUMBER(TO_CHAR(w_date_debut,'DD')) = 1) AND (LAST_DAY(w_date_debut) = w_date_temp) THEN
      w_mois_complet := 1;
      w_lib_ligne_fact := 'Forfait ' || c_lib_formule || ' du mois : ' ||  TO_CHAR(w_date_debut,'MONTH YYYY');
    ELSE
      w_mois_complet := 0;
      w_lib_ligne_fact := 'Forfait ' || c_lib_formule || ' du '|| TO_CHAR(w_date_debut,'DD/MM/YYYY') ||' au '|| TO_CHAR(w_date_temp,'DD/MM/YYYY');
    END IF;

    -- Insert lignes facture principal

      INSERT INTO litchi.ligne_facture (id_ligne_facture, lib_ligne_facture, id_facture,
       unite, quantite, prix_unitaire_ht, mnt_ht, mnt_ttc, taux_tva, type_tva_lf, id_type_produit, id_restauration, id_type_tarif, id_tarif,
       status_heberge, mois, annee, date_debut_facture, date_fin_facture )
       VALUES  (litchi.ligne_facture_seq.NEXTVAL, w_lib_ligne_fact, w_id_facture,
                 w_unite, w_qte, w_prix_ht, w_mnt_ht, w_mnt_ttc, w_taux_tva, c_id_type_tva, c_id_type_produit, c_id_restauration, NULL, NULL,
                 w_satut_heberge, TO_NUMBER(TO_CHAR(w_date_debut,'MM')), TO_NUMBER(TO_CHAR(w_date_debut,'YYYY')),w_date_debut,w_date_temp);

    w_date_debut := w_date_temp + 1;
    IF (trunc(w_date_temp) < trunc(c_date_fin_facturation)) THEN  GOTO loop_date;  END IF;

  -- Nouveau contrat
      FETCH cRestauration INTO c_pers_id, c_id_restauration, c_id_formule, c_lib_formule, c_id_tarif, c_date_fin_tarif, c_prix_mensuel, c_prix_par_jour, c_id_type_tva, c_id_type_produit, c_date_debut_facturation,c_date_fin_facturation;
   END LOOP;
 -- Liquider la dernière facture : calculer les  AVOIRS et AVANCES, mettre montant à payer
  IF (w_pers_id >0 ) THEN
             w_mnt_a_payer_ht := w_mnt_facture_ht;
             w_mnt_a_payer_ttc := w_mnt_facture_ttc;

             UPDATE litchi.entete_facture
             SET   mnt_facture_ht = w_mnt_facture_ht, mnt_facture_ttc = w_mnt_facture_ttc, mnt_a_payer_ht = w_mnt_a_payer_ht, mnt_a_payer_ttc = w_mnt_a_payer_ttc
             WHERE id_facture = w_id_facture;
			  
         -- Calcul du montant de TVA
         IF w_mnt_facture_ttc > w_mnt_facture_ht THEN
           UPDATE litchi.entete_facture
           SET   mnt_facture_tva = w_mnt_facture_ttc - w_mnt_facture_ht
           WHERE id_facture = w_id_facture;
         END IF;
         
             IF p_bAvances IS NOT NULL AND  p_bAvances = 1 THEN
                PROC_AFFECTATION_AVANCES (w_pers_id, w_id_facture, 2, w_mnt_a_payer_ht, w_mnt_a_payer_ttc);
             END IF;
  END IF;
 CLOSE  cRestauration;
-- EXCEPTION WHEN OTHERS THEN NULL;
END FACTURER_UN_RESTO;

/


SET DEFINE OFF;
Insert into LITCHI.DB_VERSION (DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) values ('2','0.8.5.2',to_date('01/03/2013','DD/MM/YYYY'),null,'Calcul du montant de la TVA, Dépersonnalisation STATUT_INDIVIDU');
