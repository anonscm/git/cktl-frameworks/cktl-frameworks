package org.cocktail.fwkcktlinternat.serveur.facturation;

import static org.junit.Assert.*;

import org.junit.Test;

public class FacturationSepaServiceParFactureTest {

	@Test
	public void testGetRequeteChgtEnteteFacture() {
		AFacturationSepaService facturationSepaService = new FacturationSepaServiceParFacture(null, Long.valueOf(2045));
		String req = facturationSepaService.getRequeteChgtEnteteFacture(Long.valueOf(1));
		assertTrue(req.contains("2045"));
	}

}
