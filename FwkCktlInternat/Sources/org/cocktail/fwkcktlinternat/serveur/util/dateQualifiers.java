package org.cocktail.fwkcktlinternat.serveur.util;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
/**
 * 
 * @author rlouissidney
 *
 * Calcul de qualifier pour les dates de recherches
 */
public class dateQualifiers {
	
	public dateQualifiers() {
		
	}
	
	public  final String INCLUSION_KEY = "Inclus";
	public  final String RECOUVREMENT_KEY = "Recouvre"; 
	public  final String FIN_KEY = "Fin"; 
	public  final String DEBUT_KEY = "Debut";
	public  final String EXCLUSION_KEY = "Exclus";
	
	public NSArray<String> getLesModesrecherches =
			new NSArray<String> (INCLUSION_KEY, RECOUVREMENT_KEY, FIN_KEY, DEBUT_KEY, EXCLUSION_KEY);
	
	public EOQualifier getDateQualfichiers (String dateDebutKey, String dateFinKey, NSTimestamp dateDebutRecherche, NSTimestamp dateFinRecherche, String modeRecherche  ) {
		
		if (dateDebutKey == null || dateFinKey == null || modeRecherche == null
				                 || (dateDebutRecherche==null && dateFinRecherche==null)) return null;
		
		EOQualifier dateQual = null;
		
		if (modeRecherche.equals(INCLUSION_KEY))  {
			// Il faut que les dates recherchÃ©s soient comprises exclusivement
			if (dateDebutRecherche!=null && dateFinRecherche==null)
				dateQual = ERXQ.greaterThanOrEqualTo(dateDebutKey,dateDebutRecherche);
			if (dateDebutRecherche==null && dateFinRecherche!=null)
				dateQual = ERXQ.lessThanOrEqualTo(dateFinKey, dateFinRecherche);
		    if (dateDebutRecherche!=null && dateFinRecherche!=null)
			   dateQual = ERXQ.greaterThanOrEqualTo(dateDebutKey,dateDebutRecherche).and(ERXQ.lessThanOrEqualTo(dateFinKey, dateFinRecherche));
		    return dateQual;
		}
		
		if (modeRecherche.equals(RECOUVREMENT_KEY))  {
		    // Il faut que les dates recherchÃ©s recouvrent		
		    if (dateDebutRecherche!=null && dateFinRecherche==null)
					dateQual = ERXQ.greaterThanOrEqualTo(dateDebutKey,dateDebutRecherche);
			if (dateDebutRecherche==null && dateFinRecherche!=null)
					dateQual = ERXQ.lessThanOrEqualTo(dateFinKey, dateFinRecherche);
	   	    if (dateDebutRecherche!=null && dateFinRecherche!=null)
				   dateQual = ERXQ.not(ERXQ.greaterThan(dateDebutKey,dateFinRecherche).or(ERXQ.lessThan(dateFinKey, dateDebutRecherche)));
				   return dateQual;
		}
			 
    	if (modeRecherche.equals(FIN_KEY))  {
		    // Il faut que les dates recherchÃ©s soient comprises
	        if (dateDebutRecherche!=null && dateFinRecherche==null)
					dateQual = ERXQ.greaterThanOrEqualTo(dateFinKey,dateDebutRecherche);
			if (dateDebutRecherche==null && dateFinRecherche!=null)
					dateQual = ERXQ.lessThanOrEqualTo(dateFinKey, dateFinRecherche);
			if (dateDebutRecherche!=null && dateFinRecherche!=null)
				   dateQual = ERXQ.greaterThanOrEqualTo(dateFinKey,dateDebutRecherche).and(ERXQ.lessThanOrEqualTo(dateFinKey, dateFinRecherche));
				   return dateQual;
		}
			
    	if (modeRecherche.equals(DEBUT_KEY))  {
		    // Il faut que les dates recherchÃ©s soient comprises	
			if (dateDebutRecherche!=null && dateFinRecherche==null)
					dateQual = ERXQ.greaterThanOrEqualTo(dateDebutKey,dateDebutRecherche);
	  	    if (dateDebutRecherche==null && dateFinRecherche!=null)
					dateQual = ERXQ.lessThanOrEqualTo(dateDebutKey, dateFinRecherche);
			if (dateDebutRecherche!=null && dateFinRecherche!=null)
				   dateQual = ERXQ.greaterThanOrEqualTo(dateDebutKey,dateDebutRecherche).and(ERXQ.lessThanOrEqualTo(dateDebutKey, dateFinRecherche));
				   return dateQual;
		 }
    	
    	 if (modeRecherche.equals(EXCLUSION_KEY))  {
			// Il faut que les dates recherchÃ©s soient comprises	
			if (dateDebutRecherche!=null && dateFinRecherche==null)
				dateQual = ERXQ.greaterThan(dateDebutKey,dateFinRecherche);
			if (dateDebutRecherche==null && dateFinRecherche!=null)
				dateQual = ERXQ.lessThan(dateFinKey, dateFinRecherche);
			if (dateDebutRecherche!=null && dateFinRecherche!=null)
			   dateQual = ERXQ.greaterThan(dateDebutKey,dateFinRecherche).or(ERXQ.lessThan(dateFinKey, dateDebutRecherche));
			   return dateQual;
	    }
		return dateQual;
	}
}
