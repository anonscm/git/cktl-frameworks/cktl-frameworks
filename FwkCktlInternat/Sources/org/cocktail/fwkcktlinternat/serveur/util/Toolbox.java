/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.joda.time.DateTime;

import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class Toolbox {
	
	static public  String  FEMININ_KEY = "MME/MLLE";
	static public  String  MASCULIN_KEY = "M."; 
	static public  String  NO_SEXE = "";
	static public int ANNEE_EN_COURS=new DateTime().getYear();
	static public long MILLISECS_PER_DAY = 86400000L;

	static public String individuSexe(EOEditingContext edc, Integer persId) {
		
		String lSexe = NO_SEXE;
		EOIndividu eoIndividu = EOIndividu.fetchByKeyValue(edc, EOIndividu.PERS_ID_KEY, persId);
		if (eoIndividu != null) {
			if (eoIndividu.toCivilite() != null) {
				if (EOCivilite.C_CIVILITE_MONSIEUR.equalsIgnoreCase(eoIndividu.toCivilite().cCivilite())) {
					lSexe = MASCULIN_KEY;
				} else {
					lSexe = FEMININ_KEY;
				}
			}
		}
		return lSexe;
	}
	
	static public String DateDuJour(NSTimestamp ts) {
		if (ts==null) return "";
		StringBuffer format = new StringBuffer("%d/%m/%Y");
		return new NSTimestampFormatter(format.toString()).format(ts);
	}
	
	public static NSTimestamp parseNSTimestamp(String dateString) {
		NSTimestamp result;
		NSTimestampFormatter formatter;
		
		if (dateString==null) return null;
		if (dateString.length()==0) return null;
		try {
			 formatter = new  NSTimestampFormatter("%d/%m/%Y");
			 result = (NSTimestamp) formatter.parseObject(dateString);
			 return result;
		} catch (Exception e) { return null; }
	}
	
	static public long nombreJoursEntreDeuxDates(NSTimestamp debut, NSTimestamp fin ) {
		
		if (debut==null || fin==null) return 0;
		
		GregorianCalendar calDebut = new GregorianCalendar ();
		calDebut.setTime(debut);
		GregorianCalendar calFin = new GregorianCalendar ();
		calFin.setTime(fin);
		long endL   =  calFin.getTimeInMillis() +  calFin.getTimeZone().getOffset(  calFin.getTimeInMillis() );
	    long startL = calDebut.getTimeInMillis() + calDebut.getTimeZone().getOffset( calDebut.getTimeInMillis() );
	     
	    return (endL - startL) / MILLISECS_PER_DAY;
	    
	}
	
	static public NSTimestamp opJoursDate (NSTimestamp date, int nbJour) {
		
		return  date.timestampByAddingGregorianUnits(0, 0, nbJour , 0, 0, 0);	
		
	}
	
    static public NSTimestamp premierJourMois (NSTimestamp date) {
    	 Calendar c = Calendar.getInstance();
         c.setTime(date);
         // on se place au premier jour du mois en cours
         c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
         return  new NSTimestamp (c.getTime());
	}
     
    static public NSTimestamp dernierJourMois (NSTimestamp date) {
 		
    	Calendar c = Calendar.getInstance();
        c.setTime(date);
        // on se place au premier jour du mois en cours
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return  new NSTimestamp (c.getTime());	
 	}
	
    public static String MD5(String md5) {
       try {
             java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
             byte[] array = md.digest(md5.getBytes());
             StringBuffer sb = new StringBuffer();
             for (int i = 0; i < array.length; ++i) {
                        sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
             }
             return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        	e.printStackTrace();
        }
        return null;
    }
    
    public static WOResponse dataFilePage(String fileName, NSData data) {
    	
    	if (data==null) return null;
    	if (fileName==null) return null;
   	
    	WOResponse nextPage = new WOResponse();
	       
        // Pour IE
        nextPage.disableClientCaching();
        nextPage.removeHeadersForKey("Cache-Control");
        nextPage.removeHeadersForKey("pragma");
        
        nextPage.setHeader( "content-type; name=\"" + fileName + "\"", "Content-Type" ); 
        nextPage.setHeader( "inline; filename=\"" + fileName + "\"", "Content-Disposition"); 
        nextPage.setHeader(Integer.toString(data.length()), "content-length");
        
        nextPage.setContent(data);
        return nextPage;
        
    }
    
}
