/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.util;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlinternat.serveur.finder.FinderPersonne;
import org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOLocal;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeReglement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.reporting.common.location.finder.ReportLocationFinder;
import org.cocktail.reporting.server.exception.CktlLocationException;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public class ImprimanteEdition {

	private static final Logger LOG = Logger.getLogger(ImprimanteEdition.class);
	private static final boolean IS_JAVA_5 = System.getProperty("java.version").startsWith("1.5.");

	/**
	 * Imprimer les fichier de location Retourne le lien ou on ira le chercher download
	 */
	public static String impressionLocation(String localReportsDir, String nomFichier, String extFichier, Connection conn, EODetailHebergement unDetailHebergement, EOEditingContext edc) {

		long start = System.currentTimeMillis();
		
		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();

		EOIndividu ind = null;
		if (unDetailHebergement.persIdConcerner() != null) {
			ind = (EOIndividu) PersonneDelegate.fetchPersonneByPersId(edc, unDetailHebergement.persIdConcerner());
		}

		String shortFileNameOut = System.currentTimeMillis() + "_" + nomFichier + "." + extFichier;

		if (ind != null) {
			shortFileNameOut = ind.prenomAffichage().trim().toLowerCase() + "_" + ind.nomAffichage().trim().toLowerCase() + "_" + shortFileNameOut;
		}

		String fileNameOut = getLocalTempDirectoryPath() + shortFileNameOut;

		try {
            URL fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);

		String fileNameIn =  fileNameInURL.getFile();
		

			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			parameters.put("FANN_KEY", FinderPersonne.internat_fann_key(edc));
			parameters.put("ID_DETAIL_HEBERGEMENT", ERXEOControlUtilities.primaryKeyStringForObject(unDetailHebergement));

			String ImageDir = recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			if (extFichier.equalsIgnoreCase("xls") || extFichier.equalsIgnoreCase("rtf"))
				parameters.put("IS_IGNORE_PAGINATION", Boolean.TRUE);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF          
			if (extFichier.equalsIgnoreCase("pdf")) {
				JasperExportManager.exportReportToPdfFile(jrPrint, fileNameOut);
			}

			// - Création du rapport au format rtf      
			if (extFichier.equalsIgnoreCase("rtf")) {
				JRRtfExporter exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.exportReport();
			}

			// - Création du rapport au format xls
			if (extFichier.equalsIgnoreCase("xls")) {
				Map dateFormats = new HashMap();
				dateFormats.put("EEE, MMM d, yyyy", "ddd, mmm d, yyyy");
				JRXlsExporter exporter = new JRXlsExporter();

				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.FORMAT_PATTERNS_MAP, dateFormats);
				exporter.exportReport();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		LOG.info("Fichier " + fileNameOut + " : temps de génération " + (System.currentTimeMillis() - start));
		return shortFileNameOut;
	}

	/**
	 * Imprimer les fichier de location en masse Retourne le lien ou on ira le chercher download
	 */

	static public String impressionLocationEnMasse(String localReportsDir,String nomFichier, String extFichier, Connection conn, EOLocal unLocal, NSTimestamp date_debut, NSTimestamp date_fin, EOTypeObjet unTypeObjet, EOTypeStatut unStatut, Boolean isPagination, EOEditingContext edc) {
		long start = System.currentTimeMillis();
		
		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();
		
		String shortFileNameOut = System.currentTimeMillis() + "_" + nomFichier + "." + extFichier;

		String fileNameOut = getLocalTempDirectoryPath() + shortFileNameOut;
		
		JRSwapFileVirtualizer virtualizer = createVirtualizer();

		try {
            URL fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);
 
		String fileNameIn =  fileNameInURL.getFile();


			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			if (virtualizer != null)
				parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

			parameters.put("FANN_KEY", FinderPersonne.internat_fann_key(edc));

			if (unLocal != null)
				parameters.put("C_LOCAL", ERXEOControlUtilities.primaryKeyStringForObject(unLocal));

			if (unTypeObjet != null)
				parameters.put("CODE_TYPE_OBJET", ERXEOControlUtilities.primaryKeyStringForObject(unTypeObjet));

			if (unStatut != null)
				parameters.put("ID_TYPE_STATUT", ERXEOControlUtilities.primaryKeyStringForObject(unStatut));

			parameters.put("DATE_DEBUT", Toolbox.DateDuJour(date_debut));
			parameters.put("DATE_FIN", Toolbox.DateDuJour(date_fin));
			
			
			String ImageDir = recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			if (isPagination != null && !isPagination)
				parameters.put("IS_IGNORE_PAGINATION", Boolean.TRUE);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF          
			if (extFichier.equalsIgnoreCase("pdf")) {
				JasperExportManager.exportReportToPdfFile(jrPrint, fileNameOut);
			}

			// - Création du rapport au format rtf      
			if (extFichier.equalsIgnoreCase("rtf")) {
				JRRtfExporter exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.exportReport();
			}

			// - Création du rapport au format xls
			if (extFichier.equalsIgnoreCase("xls")) {
				Map dateFormats = new HashMap();
				dateFormats.put("EEE, MMM d, yyyy", "ddd, mmm d, yyyy");
				JRXlsExporter exporter = new JRXlsExporter();

				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.FORMAT_PATTERNS_MAP, dateFormats);
				exporter.exportReport();
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (virtualizer != null)
				virtualizer.cleanup();
		}

		LOG.info("Fichier " + fileNameOut + " : temps de génération " + (System.currentTimeMillis() - start));
		return shortFileNameOut;
	}

	/**
	 * Imprimer les fichiers de comptabilité Retourne le lien ou on ira le chercher download
	 */

	static public String impressionComptable(String localReportsDir,String nomFichier, String extFichier, Connection conn, NSTimestamp date_debut, NSTimestamp date_fin, EOTypeFacture leTypeFacture, EOTypeReglement leTypeReglement, Boolean isPagination, EOEditingContext edc) {

		long start = System.currentTimeMillis();
		
		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();

		String shortFileNameOut = System.currentTimeMillis() + "_" + nomFichier + "." + extFichier;

		String fileNameOut = getLocalTempDirectoryPath() + shortFileNameOut;
	
        URL fileNameInURL = null;
        
        JRSwapFileVirtualizer virtualizer = createVirtualizer();


		try {
			fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);

        
		String fileNameIn =  fileNameInURL.getFile();
		
		
			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			if (virtualizer != null)
				parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

			parameters.put("FANN_KEY", FinderPersonne.internat_fann_key(edc));
			if (date_debut != null)
				parameters.put("DATE_DEBUT", Toolbox.DateDuJour(date_debut));
			if (date_fin != null)
				parameters.put("DATE_FIN", Toolbox.DateDuJour(date_fin));

			List lesTypesFactures = new ArrayList();
			if (leTypeFacture == null) {
				NSArray<EOTypeFacture> eoTFactures = EOTypeFacture.fetchAll(edc);
				for (int i = 0; i < eoTFactures.size(); i++) {
					lesTypesFactures.add(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(eoTFactures.get(i))));
				}
			}
			else {
				lesTypesFactures.add(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(leTypeFacture)));
			}
			parameters.put("LES_TYPES_FACTURES", lesTypesFactures);

			List lesTypesReglements = new ArrayList();
			if (leTypeReglement == null) {
				NSArray<EOTypeReglement> eoTReglements = EOTypeReglement.fetchAll(edc);
				for (int i = 0; i < eoTReglements.size(); i++) {
					lesTypesReglements.add(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(eoTReglements.get(i))));
				}
			}
			else {
				lesTypesReglements.add(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(leTypeReglement)));
			}
			parameters.put("LES_TYPES_REGLEMENTS", lesTypesReglements);

			
			String ImageDir =  recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			if (isPagination != null && !isPagination)
				parameters.put("IS_IGNORE_PAGINATION", Boolean.TRUE);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF          
			if (extFichier.equalsIgnoreCase("pdf")) {
				JasperExportManager.exportReportToPdfFile(jrPrint, fileNameOut);
			}

			// - Création du rapport au format rtf      
			if (extFichier.equalsIgnoreCase("rtf")) {
				JRRtfExporter exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.exportReport();
			}

			// - Création du rapport au format xls
			if (extFichier.equalsIgnoreCase("xls")) {
				Map dateFormats = new HashMap();
				dateFormats.put("EEE, MMM d, yyyy", "ddd, mmm d, yyyy");
				JRXlsExporter exporter = new JRXlsExporter();

				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.FORMAT_PATTERNS_MAP, dateFormats);
				exporter.exportReport();
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (virtualizer != null)
				virtualizer.cleanup();
		}

		LOG.info("Fichier " + fileNameOut + " : temps de génération " + (System.currentTimeMillis() - start));
		return shortFileNameOut;
	}


	
	private static String recupDirFileName(String nomFichier, String fileNameIn) {
		return fileNameIn.substring(0, fileNameIn.lastIndexOf(nomFichier));
	}

	
	
	/**
	 * Imprimer les fichier de restauration en masse Retourne le lien ou on ira le chercher download
	 */

	static public String impressionRestaurationEnMasse(String localReportsDir,String nomFichier, String extFichier, Connection conn, NSTimestamp date_debut, NSTimestamp date_fin, EOTypeStatut unStatut, Boolean isPagination, EOEditingContext edc) {

		long start = System.currentTimeMillis();
		
		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();
		
		String shortFileNameOut = System.currentTimeMillis() + "_" + nomFichier + "." + extFichier;

		String fileNameOut = getLocalTempDirectoryPath() + shortFileNameOut;

		JRSwapFileVirtualizer virtualizer = createVirtualizer();
		
		try {
            URL fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);

		String fileNameIn =  fileNameInURL.getFile();


			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			if (virtualizer != null)
				parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

			parameters.put("FANN_KEY", FinderPersonne.internat_fann_key(edc));

			if (unStatut != null)
				parameters.put("ID_TYPE_STATUT", ERXEOControlUtilities.primaryKeyStringForObject(unStatut));

			parameters.put("DATE_DEBUT", Toolbox.DateDuJour(date_debut));
			parameters.put("DATE_FIN", Toolbox.DateDuJour(date_fin));
			

			String ImageDir = recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			if (isPagination != null && !isPagination)
				parameters.put("IS_IGNORE_PAGINATION", Boolean.TRUE);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF          
			if (extFichier.equalsIgnoreCase("pdf")) {
				JasperExportManager.exportReportToPdfFile(jrPrint, fileNameOut);
			}

			// - Création du rapport au format rtf      
			if (extFichier.equalsIgnoreCase("rtf")) {
				JRRtfExporter exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.exportReport();
			}

			// - Création du rapport au format xls
			if (extFichier.equalsIgnoreCase("xls")) {
				Map dateFormats = new HashMap();
				dateFormats.put("EEE, MMM d, yyyy", "ddd, mmm d, yyyy");
				JRXlsExporter exporter = new JRXlsExporter();

				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jrPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameOut);
				exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.FORMAT_PATTERNS_MAP, dateFormats);
				exporter.exportReport();
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (virtualizer != null)
				virtualizer.cleanup();
		}

		LOG.info("Fichier " + fileNameOut + " : temps de génération " + (System.currentTimeMillis() - start));
		return shortFileNameOut;
	}

	/**
	 * Imprime une facture en PDF
	 */
	static public NSData printFacturePDF(String localReportsDir, String idFacture, Connection conn, EOEditingContext edc) {

		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();
		String nomFichier = "facture";
		NSData data = null;

		if (idFacture == null)
			return null;
		idFacture = idFacture.trim();
		if (idFacture.length() == 0)
			return null;

		try {
            URL fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);

		String fileNameIn =  fileNameInURL.getFile();


			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			parameters.put("ID_FACTURE", idFacture);

			String ImageDir = recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF         
			data = new NSData(JasperExportManager.exportReportToPdf(jrPrint));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	static public NSData printMandatSepaPDF(String localReportsDir, List<String> mandatRums, Connection conn, EOEditingContext edc) {

		ReportLocationFinder reportLocationFinder = new ReportLocationFinder();

		String nomFichier = "sepa_mandat_prelevement";
		
		NSData data = null;

		if (mandatRums == null)
			return null;
		if (mandatRums.size() == 0)
			return null;


		try {
            URL fileNameInURL = reportLocationFinder.searchJasperLocation(localReportsDir, nomFichier);

		String fileNameIn = fileNameInURL.getFile();


			// - Paramètres à envoyer au rapport
			// On ne fait passer que des String

			Map parameters = new HashMap();

			String mandatRumsCritere = "";
			for (String mandatRum : mandatRums) {
				if (mandatRumsCritere.length() > 0) {
					mandatRumsCritere += ",";
				}
				mandatRumsCritere += "'" + mandatRum + "'";
			}

			parameters.put("RUM_SEPA_SDD_MANDAT", mandatRumsCritere);

			String ImageDir = recupDirFileName(nomFichier, fileNameIn);
			
			parameters.put("ImageDir", ImageDir);
			parameters.put("SUBREPORT_DIR", ImageDir);

			JasperPrint jrPrint = null;

			if (conn == null) {
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, new JREmptyDataSource());
			} else {
				// - Execution du rapport, en mémoire pour gagner du temps
				jrPrint = JasperFillManager.fillReport(fileNameIn, parameters, conn);
			}

			// - Création du rapport au format PDF         
			data = new NSData(JasperExportManager.exportReportToPdf(jrPrint));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Creation d'un virtualizer Peut être désactivé
	 * 
	 * @return
	 */
	static private JRSwapFileVirtualizer createVirtualizer() {
		WOApplication app = WOApplication.application();
		CktlConfig config = (CktlConfig) app.valueForKey("config");
		String useStringVirtualizer = config.stringForKey("USE_VIRTUALIZER");

		if (useStringVirtualizer == null) {
			useStringVirtualizer = "NO";
		} else {
			useStringVirtualizer = useStringVirtualizer.trim();
		}

		Boolean useVirtualiser = !(IS_JAVA_5 || useStringVirtualizer.equalsIgnoreCase("NO"));

		JRSwapFileVirtualizer virtualizer = null;

		if (useVirtualiser) {
			// creating the virtualizer
			JRSwapFile swapFile = new JRSwapFile(getLocalTempDirectoryPath(), 8192, 8192);
			virtualizer = new JRSwapFileVirtualizer(65536, swapFile, true);
			LOG.debug("Creation virtualizer pour impression de masse");
		}

		if (virtualizer == null)
			LOG.debug("Pas de virtualizer pour impression de masse");

		return virtualizer;
	}

	/**
	 * Chemin des rapports REP_BASE_JASPER_PATH REP_BASE_JASPER_PATH_LOCAL REP_BASE_XLS_PATH REP_BASE_XLS_PATH_LOCAL
	 */

	/**
	 * Retourne le chemin absolu du repertoire contenant les rapports de l'appli.
	 */
	static public String getAppResourcesDir() {
		WOApplication app = WOApplication.application();
		CktlConfig config = (CktlConfig) app.valueForKey("config");
		String resourcesDir = config.stringForKey("REP_BASE_JASPER_PATH_LOCAL");

		if (resourcesDir != null && resourcesDir.trim().length() > 0) {
			resourcesDir = resourcesDir.trim();
			if (!resourcesDir.endsWith(File.separator)) {
				resourcesDir = resourcesDir + File.separator;
			}
		} else {
			resourcesDir = app.path() + File.separator +
					"Contents" + File.separator +
					"Resources" + File.separator +
					"Reports" + File.separator;
		}

		LOG.debug("Repertoire des ressources de l'application : " + resourcesDir);
		return resourcesDir;
	}
	
	/**
	 * Recuperation du chemin complet du repertoire temporaire du systeme courant.
	 */
	static public String getLocalTempDirectoryPath() {
		String temporaryDir = System.getProperty("java.io.tmpdir");
		if (!temporaryDir.endsWith(File.separator)) {
			temporaryDir = temporaryDir + File.separator;
		}
		LOG.debug("Repertoire temporaire de ce systeme : " + temporaryDir);
		return temporaryDir;
	}

}
