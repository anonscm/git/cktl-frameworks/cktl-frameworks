package org.cocktail.fwkcktlinternat.serveur.facturation;

import java.util.List;
import java.util.concurrent.Callable;

import org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

public class FacturationParLot implements Callable<Void> {

    private static final int PAGE_SIZE = 200;
    
    
    private EOEditingContext editingContext;
    private int pageCourante;
    
    public FacturationParLot() {
        this.editingContext = ERXEC.newEditingContext();
        this.pageCourante = 0;
    }
  
    public Void call() throws Exception {
        // algo rapide :
        // ERX*Batching*Display*
        
        //0- initialiser pageSize
        //1- faire un count du nb de resultat a traiter : cela permet d'avoir le nb de page.
        //2- paginer : charger le lot correspond a la page courante et avec le pageSize
        //2.1---- chercher les factures qui ont un numero de facture (num_facture) 
        //          + qui sont en mode de prelevement (indication_prelevement) et  
        //          + pas d'indication d'abandon (indication_abandon a null) + montant a payer > 0
        //          + non rattache a un echeancier sepa
        //2.2---- chercher dans prelevement sepa les enregistrements liés a la facture (id et type) (fait dans la meme requete que le pt 2.1)
        //2.3---- generer l'echeancier + l'echeance et le persister
        // ca fait cb de jointures : entete-facture <-> prelevements_sepa <-> sepa_sdd_echeancier 
        //3- traitement sur le lot en cours
        //4- on termine la Tx et on purge les donnees chargees (liberation memoire = invaldieAllObject dans EDC ?)
      
        return null;
    }
    
    private List<EOEnteteFacture> chargerLotFactures() {
       return null;
    }

    
    
}
