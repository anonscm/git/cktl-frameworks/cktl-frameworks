package org.cocktail.fwkcktlinternat.serveur.facturation;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.factories.EOEcritureFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;
import org.cocktail.fwkcktlcompta.server.services.SepaSddEcheancierVisaService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Création des écritures de prise en charge d'un echeancier SEPA.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at asso-cocktail.fr>
 */
public class EcheancierEcritureService {
	private EOJefyAdminExercice exercice;
	private EOComptabilite comptabilite;
	private EOJefyAdminUtilisateur utilisateur;
	private EOTypeJournal typeJournal;
	private EOTypeOperation typeOperation;
	private NSTimestamp dateCreation;
	private EOPlanComptableExer compteTiersCreditEcheancier;
	private EOPlanComptableExer compteAttenteDebitEcheancier;
	private EOGestion gestionForCreditEcheancier;

	public EcheancierEcritureService(EOEditingContext editingContext, EOJefyAdminExercice exercice, EOComptabilite comptabilite, EOJefyAdminUtilisateur utilisateur, NSTimestamp dateCreation,
			EOPlanComptableExer compteTiersCreditEcheancier, EOPlanComptableExer compteAttenteDebitEcheancier, EOGestion gestionForCreditEcheancier) {
		this.exercice = exercice;
		this.comptabilite = comptabilite;
		this.utilisateur = utilisateur;
		this.dateCreation = dateCreation;
		this.compteTiersCreditEcheancier = compteTiersCreditEcheancier;
		this.compteAttenteDebitEcheancier = compteAttenteDebitEcheancier;
		this.gestionForCreditEcheancier = gestionForCreditEcheancier;
		this.typeJournal = EOTypeJournal.fetchByKeyValue(editingContext, EOTypeJournal.TJO_LIBELLE_KEY, EOTypeJournal.typeJournalRecouvrement);
		this.typeOperation = EOTypeOperation.fetchByKeyValue(editingContext, EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TOP_LIBELLE_RECOUVREMENT);

	}

	public NSDictionary<String, Object> creerEcriturePriseEnChargeForEcheancier(EOEditingContext editingContext, ISepaSddEcheancier sepaSddEcheancier) throws Exception {
		try {
		    if (!sepaSddEcheancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
		        return new NSDictionary<String, Object>();
		    }
		    
			EOJefyAdminExercice exerciceLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, exercice);
			EOComptabilite comptabiliteLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, comptabilite);
			EOJefyAdminUtilisateur utilisateurLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, utilisateur);
			EOPlanComptableExer compteTiersCreditEcheancierLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, compteTiersCreditEcheancier);
			EOPlanComptableExer compteAttenteDebitEcheancierLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, compteAttenteDebitEcheancier);
			EOGestion gestionForCreditEcheancierLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, gestionForCreditEcheancier);
			EOTypeJournal typeJournalLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, this.typeJournal);
			EOTypeOperation typeOperationLoc = ERXEOControlUtilities.localInstanceOfObject(editingContext, this.typeOperation);

			EOSepaSddEcheancier echeancier = ERXEOControlUtilities.localInstanceOfObject(editingContext, (EOSepaSddEcheancier) sepaSddEcheancier);
			SepaSddEcheancierVisaService service = new SepaSddEcheancierVisaService(editingContext, comptabiliteLoc, exerciceLoc, utilisateurLoc, dateCreation, null, typeJournalLoc, typeOperationLoc);
			NSDictionary<String, Object> res = service.creerEcriturePriseEnchargeEcheancierSansEmargement(echeancier, compteTiersCreditEcheancierLoc, compteAttenteDebitEcheancierLoc, gestionForCreditEcheancierLoc);

			if (res.valueForKey(SepaSddEcheancierVisaService.ERREURS_KEY) != null) {
				throw new Exception((String) res.valueForKey(SepaSddEcheancierVisaService.ERREURS_KEY));
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public NSDictionary<String, Object> numeroterEcriture(EOEditingContext edc, ISepaSddEcheancier sepaSddEcheancier, NSArray<EOEcriture> ecritures) {
	    if (!sepaSddEcheancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
            return new NSDictionary<String, Object>();
        }
	    
		NSDictionary<String, Object> resultat = new NSDictionary<String, Object>();
		EOEcritureFactory.getSharedInstance().numeroterEcrituresViaProcedureStockee(edc, ecritures);
		return resultat;
	}

}
