package org.cocktail.fwkcktlinternat.serveur.facturation;

import org.cocktail.fwkcktlwebapp.server.CktlConfig;


public class FacturationSepaServiceEnMasse extends AFacturationSepaService {

    private static final String SQL_BODY = new StringBuffer()
        .append("  from litchi.entete_facture f ")
        .append("  join litchi.prelevements_sepa ps on (f.persid_payeur = ps.pers_id and f.id_type_facture = ps.id_type_facture) ")
        .append("  join maracuja.sepa_sdd_mandat m on ps.id_sepa_sdd_mandat = m.id_sepa_sdd_mandat ")
        .append(" where f.num_facture is not null ")
        .append("   and f.id_type_facture = %d ")
        .append("   and f.indication_prelevement = 1 ")
        .append("   and (f.indication_abandon is null or f.indication_abandon = 0) ")
        .append("   and f.mnt_a_payer_ttc - (NVL((SELECT  SUM(LR.MONTANT_TTC)  FROM  LITCHI.LIGNES_REGLEMENT LR WHERE LR.ID_FACTURE = F.ID_FACTURE),0)) > 0 ")
        .append("   and m.d_mandat_signature is not null ")
        .append("   and m.tyet_id = 1 ")
        .append("   and not exists ( ")
        .append("      select 1 from maracuja.sepa_sdd_echeancier e join maracuja.sepa_sdd_origine o on e.id_sepa_sdd_origine = o.id_sepa_sdd_origine ")
        .append("      join maracuja.sepa_sdd_echeance ee on ee.id_sepa_sdd_echeancier = e.id_sepa_sdd_echeancier ")
        .append("       where e.id_sepa_sdd_mandat = m.id_sepa_sdd_mandat ")
        .append("         and o.origine_id = f.id_facture ")
        .append("         and ee.etat not in ('REJETE', 'ANNULE') ")
        .append("   ) ")
        .toString();
    
    private static final String GROUPBY = " group by f.id_facture, f.date_echeance, f.persid_payeur, f.mnt_a_payer_ttc ";
    private static final String COUNT  = "select count(distinct f.id_facture) nb_res " + SQL_BODY;
    private static final String SELECT = "select f.id_facture as idFacture, f.date_echeance as dateEcheance, f.persid_payeur as persIdPayeur, f.mnt_a_payer_ttc as montantAPayer, max(m.id_sepa_sdd_mandat) as idMandat, " +
    		"(NVL((SELECT  SUM(LR.MONTANT_TTC)  FROM  LITCHI.LIGNES_REGLEMENT LR WHERE LR.ID_FACTURE = F.ID_FACTURE),0)) AS dejaPaye" + SQL_BODY + GROUPBY;
    
    public FacturationSepaServiceEnMasse(CktlConfig config) {
        super(config);
    }
    
	public String getRequeteCountEnteteFacture(Long idTypeFacture) {
		return String.format(COUNT, idTypeFacture);
	}
    
    public String getRequeteChgtEnteteFacture(Long idTypeFacture) {      
		return String.format(SELECT, idTypeFacture);
	}
	
}
