package org.cocktail.fwkcktlinternat.serveur.facturation;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumPersonne;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ctrl.ISepaSddOrigineCtrl;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.IGenerateurEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.generateurs.SepaSddGenerateurEcheancierConstant;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.repositories.SepaSddEcheancierRepository;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOReglements;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.joda.time.LocalDate;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class AFacturationSepaService {

	private static final int TYPE_REGLEMENT_LITCHI_PRELEVEMENT = 4;
	public static final String VAR_ID_MANDAT = "IDMANDAT";
	public static final String VAR_ID_FACTURE = "IDFACTURE";
	public static final String VAR_MT_A_PAYER = "MONTANTAPAYER";
	public static final String VAR_PERSID_PAYEUR = "PERSIDPAYEUR";
	public static final String VAR_DATE_ECHEANCE = "DATEECHEANCE";
	public static final String VAR_DEJA_PAYE = "DEJAPAYE";

	private static final String LITCHI_MODEL_ID = "FwkCktlInternat";
	private static final int ECHEANCE_UNIQUE = 1;

	private CktlConfig config;

	public AFacturationSepaService(CktlConfig config) {
		this.config = config;
	}

	public abstract String getRequeteChgtEnteteFacture(Long idTypeFacture);

	public abstract String getRequeteCountEnteteFacture(Long idTypeFacture);

	public Integer nbJoursMaxAvantBasculeAuMoisSuivant() {
		return Integer.valueOf((String) getConfig().get(IFwkCktlComptaParam.SEPASDDMANDAT_NB_JOURS_MOIS_SUIVANT));
	}

	public Integer numeroJourDePrelevement() {
		return Integer.valueOf((String) getConfig().get(IFwkCktlComptaParam.SEPASDDMANDAT_NUMERO_JOUR));
	}

	public LocalDate calculeDatePremiereEcheanceAvecDateDuJour() {
		return SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDateDuJour(
				nbJoursMaxAvantBasculeAuMoisSuivant(), numeroJourDePrelevement());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlinternat.serveur.facturation.IFacturationSepaService#countFactureParPrelevementSepaATraiter(com.webobjects.eocontrol.
	 * EOEditingContext, java.lang.Long)
	 */
	public Long countFactureParPrelevementSepaATraiter(EOEditingContext editingContext, Long idTypeFacture) {
		@SuppressWarnings("unchecked")
		String requeteCompteEntetefacture = getRequeteCountEnteteFacture(idTypeFacture);
		NSArray<NSDictionary<String, Object>> rawRows = EOUtilities.rawRowsForSQL(editingContext, LITCHI_MODEL_ID, requeteCompteEntetefacture, null);
		Double res = (Double) rawRows.objectAtIndex(0).allValues().lastObject();
		return res.longValue();
	}

	public NSArray<NSDictionary<String, Object>> chargerLesFacturesParPrelevementSepa(EOEditingContext edc, Long idTypeFacture) {
		String requeteChgtEntetefacture = getRequeteChgtEnteteFacture(idTypeFacture);
		NSArray<NSDictionary<String, Object>> lotEnteteFacture = EOUtilities.rawRowsForSQL(edc, LITCHI_MODEL_ID, requeteChgtEntetefacture, null);

		return lotEnteteFacture;
	}

	public ISepaSddEcheancier creerEcheancier(EOEditingContext editingContext,
			BigDecimal montantAPayer, LocalDate datePremiereEcheance, Integer persIdCreation,
			IGrhumPersonne modificateur, ISepaSddOrigineType origineType,
			Integer idFacture, Integer idMandat) {

		IGenerateurEcheancier generateurEcheancier = new SepaSddGenerateurEcheancierConstant(
				montantAPayer, ECHEANCE_UNIQUE, datePremiereEcheance, persIdCreation, modificateur);

		ISepaSddEcheancier echeancier = SepaSddEcheancierRepository.creer(editingContext);
		echeancier = new SepaSddEcheancierHelper.BuilderEcheancier(echeancier, persIdCreation, modificateur)
				.origine(origineType, idFacture)
				.mandatId(idMandat)
				.generateurEcheancier(generateurEcheancier)
				.genererEcheances()
				.build();
		return echeancier;
	}
	
	 public ISepaSddEcheance creerEcheance(EOEditingContext edc, ISepaSddEcheancier echeancier, LocalDate dateEcheance, BigDecimal montant, Integer persIdCreation,
				IGrhumPersonne modificateur) {
	        SepaSddGenerateurEcheancierConstant generateurEcheances = new SepaSddGenerateurEcheancierConstant(null, null, null, persIdCreation, modificateur);
	        ISepaSddEcheance echeance = generateurEcheances.creerEcheance(edc, echeancier, dateEcheance, montant, modificateur);
	        return echeance;
	 }
	
	public EOReglements effectuerReglement(EOEditingContext editingContext,
			BigDecimal montantAPayer, LocalDate datePremiereEcheance, Integer persIdCreation,
			IGrhumPersonne modificateur, Integer idFacture, Integer idMandat, Integer persIdPayeur) {
		
		NSTimestamp datePremiereEcheanceTS = new NSTimestamp(datePremiereEcheance.toDateMidnight().getMillis());
		EOEnteteFacture enteteFacture = EOEnteteFacture.fetchByKeyValue(editingContext, EOEnteteFacture.ID_FACTURE_KEY, idFacture);
		EOSepaSddMandat leMandat = EOSepaSddMandat.fetchByKeyValue(editingContext, EOSepaSddMandat.ID_SEPA_SDD_MANDAT_KEY, idMandat);
		
		EOReglements unReglement = EOReglements.createEOReglements(editingContext);
		unReglement.setPersidPayeur(persIdPayeur);
		// On formate la somme
		unReglement.setMontantTtc(montantAPayer);
		unReglement.setRefReglement("");
		unReglement.setDateReglement(datePremiereEcheanceTS);
		unReglement.setPersidPercepteur(modificateur.persId());
		unReglement.setCommentaire("");
		unReglement.setBanqOrdre(leMandat.toDebiteurRib().banqOrdre()); 
		unReglement.setIdTypeReglement(new Long(TYPE_REGLEMENT_LITCHI_PRELEVEMENT));

		// Création de la ligne de règlement
		EOLignesReglement uneLigneDeReglement = EOLignesReglement
								.createEOLignesReglement(editingContext);
		uneLigneDeReglement.setMontantTtc(montantAPayer);
		uneLigneDeReglement.setDateReglementLigne(datePremiereEcheanceTS);
		uneLigneDeReglement.setEnteteFactureRelationship(enteteFacture);
		uneLigneDeReglement.setReglementsRelationship(unReglement);

		// Mise à jour de l'entete de facture
		enteteFacture.setDatePrelevement(datePremiereEcheanceTS);
		
		return unReglement;
	}
	
	public ArrayList<ISepaSddEcheance> rejeterEcheancePreleveeEtAnnulerEcheanceEnAttentePourReglementInvalide(EOEditingContext edc, EOReglements reglementInvalide) throws Exception {
		
		NSArray<EOSepaSddEcheancier> echeanciers = new NSArray<EOSepaSddEcheancier>();
		NSMutableArray<ISepaSddEcheance> echeancesARejeter = new NSMutableArray<ISepaSddEcheance>();
		NSMutableArray<ISepaSddEcheance> echeancesAAnnuler = new NSMutableArray<ISepaSddEcheance>();
		
		
		for (EOLignesReglement ligne : reglementInvalide.lignesReglements()) {
			
	    	
	    	ISepaSddOrigineType origineType = getTypeOrigineFromLigneReglement(edc, ligne);
	    	ISepaSddOrigine factureOrigine = ISepaSddOrigineCtrl.getSharedInstance().fetchOrigine(edc, origineType, ligne.enteteFacture().idFacture().intValue());
	    	
			echeanciers = factureOrigine.toSepaSddEcheanciers();
			if (echeanciers.count() == 1) {
				ISepaSddEcheancier echeancier = echeanciers.get(0);
				if (echeancier.echeances().size() > 0) {
					
					for (ISepaSddEcheance echeance : echeancier.echeances()) {
						if (echeance.montantAPayer().compareTo(ligne.montantTtc()) == 0) {
							switch (echeance.etatAsEnum()) {
							case PRELEVE:
								echeancesARejeter.add(echeancier.echeances().get(0));
								break;
							
							case ATTENTE:
								echeancesAAnnuler.add(echeancier.echeances().get(0));
								break;
		
							default:
								break;
							}
						}
					}
				}
			} 

			SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesARejete(echeancesARejeter);
			SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesAAnnule(echeancesAAnnuler);
			edc.saveChanges();
		}
		NSMutableArray<ISepaSddEcheance> echeancesTraitees = echeancesARejeter;
		for (ISepaSddEcheance iSepaSddEcheance : echeancesAAnnuler) {
			echeancesARejeter.add(iSepaSddEcheance);
		}
		return echeancesTraitees.arrayList();
		

	}
	
	private ISepaSddOrigineType getTypeOrigineFromLigneReglement(EOEditingContext edc, EOLignesReglement ligne) {
		if (ligne.enteteFacture().typeFacture().libTypeFacture().equals(EOTypeFacture.HEBERGEMENT_KEY)) {
			return SepaSddOrigineHelper.getSharedInstance().fetchOrigineTypeValide(
	                edc, ISepaSddOrigineType.LITCHI_HEBERGEMENT);
		} else if (ligne.enteteFacture().typeFacture().libTypeFacture().equals(EOTypeFacture.RESTAURATION_KEY)) {
			return SepaSddOrigineHelper.getSharedInstance().fetchOrigineTypeValide(
	                edc, ISepaSddOrigineType.LITCHI_RESTAURATION);
		}
		return null;
	}


	public CktlConfig getConfig() {
		return config;
	}

	public void setConfig(CktlConfig config) {
		this.config = config;
	}

}