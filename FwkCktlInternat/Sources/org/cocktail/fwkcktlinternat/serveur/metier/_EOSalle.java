/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOSalle.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOSalle extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOSalle.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Salle";
	public static final String ENTITY_TABLE_NAME = "GRHUM.SALLES";


//Attribute Keys
	public static final ERXKey<NSTimestamp> D_ANNULATION = new ERXKey<NSTimestamp>("dAnnulation");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<String> SAL_ACCES = new ERXKey<String>("salAcces");
	public static final ERXKey<String> SAL_BADGEUR = new ERXKey<String>("salBadgeur");
	public static final ERXKey<Double> SAL_CAPACITE = new ERXKey<Double>("salCapacite");
	public static final ERXKey<Double> SAL_CAPA_EXAM = new ERXKey<Double>("salCapaExam");
	public static final ERXKey<String> SAL_DESCRIPTIF = new ERXKey<String>("salDescriptif");
	public static final ERXKey<String> SAL_ECRAN = new ERXKey<String>("salEcran");
	public static final ERXKey<String> SAL_ETAGE = new ERXKey<String>("salEtage");
	public static final ERXKey<String> SAL_IDENT_ACCES = new ERXKey<String>("salIdentAcces");
	public static final ERXKey<String> SAL_INSONORISEE = new ERXKey<String>("salInsonorisee");
	public static final ERXKey<String> SAL_NATURE = new ERXKey<String>("salNature");
	public static final ERXKey<Double> SAL_NB_ARMOIRES = new ERXKey<Double>("salNbArmoires");
	public static final ERXKey<Double> SAL_NB_BUREAUX = new ERXKey<Double>("salNbBureaux");
	public static final ERXKey<Double> SAL_NB_CHAISES = new ERXKey<Double>("salNbChaises");
	public static final ERXKey<Double> SAL_NB_FENETRES = new ERXKey<Double>("salNbFenetres");
	public static final ERXKey<Double> SAL_NB_PLACES_EXAM_LIB = new ERXKey<Double>("salNbPlacesExamLib");
	public static final ERXKey<Double> SAL_NB_TABLES = new ERXKey<Double>("salNbTables");
	public static final ERXKey<String> SAL_NO_POSTE = new ERXKey<String>("salNoPoste");
	public static final ERXKey<Double> SAL_NUM_DEPART = new ERXKey<Double>("salNumDepart");
	public static final ERXKey<String> SAL_OBSCUR = new ERXKey<String>("salObscur");
	public static final ERXKey<Long> SAL_PAS_NUM = new ERXKey<Long>("salPasNum");
	public static final ERXKey<String> SAL_PORTE = new ERXKey<String>("salPorte");
	public static final ERXKey<java.math.BigDecimal> SAL_POUR_ADMINISTRATION = new ERXKey<java.math.BigDecimal>("salPourAdministration");
	public static final ERXKey<java.math.BigDecimal> SAL_POUR_DOCUMENTATION = new ERXKey<java.math.BigDecimal>("salPourDocumentation");
	public static final ERXKey<java.math.BigDecimal> SAL_POUR_ENSEIGNEMENT = new ERXKey<java.math.BigDecimal>("salPourEnseignement");
	public static final ERXKey<java.math.BigDecimal> SAL_POUR_RECHERCHE = new ERXKey<java.math.BigDecimal>("salPourRecherche");
	public static final ERXKey<java.math.BigDecimal> SAL_POUR_TECHNIQUE = new ERXKey<java.math.BigDecimal>("salPourTechnique");
	public static final ERXKey<String> SAL_RESERVABLE = new ERXKey<String>("salReservable");
	public static final ERXKey<String> SAL_RETRO = new ERXKey<String>("salRetro");
	public static final ERXKey<Double> SAL_SALLE_PROCHE_TEL = new ERXKey<Double>("salSalleProcheTel");
	public static final ERXKey<java.math.BigDecimal> SAL_SUPERFICIE = new ERXKey<java.math.BigDecimal>("salSuperficie");
	public static final ERXKey<String> SAL_TABLEAU = new ERXKey<String>("salTableau");
	public static final ERXKey<String> SAL_TABLEAU_BLANC = new ERXKey<String>("salTableauBlanc");
	public static final ERXKey<String> SAL_TABLEAU_PAPIER = new ERXKey<String>("salTableauPapier");
	public static final ERXKey<String> SAL_TELEVISION = new ERXKey<String>("salTelevision");
	public static final ERXKey<String> SAL_TEM_EN_SERVICE = new ERXKey<String>("salTemEnService");
	public static final ERXKey<String> SAL_TEM_HANDICAP = new ERXKey<String>("salTemHandicap");
	public static final ERXKey<Double> TSAL_NUMERO = new ERXKey<Double>("tsalNumero");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> LOCAL = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal>("local");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> OBJET_RESERVABLES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>("objetReservables");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle> TYPE_SALLE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle>("typeSalle");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "salNumero";

	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String SAL_ACCES_KEY = "salAcces";
	public static final String SAL_BADGEUR_KEY = "salBadgeur";
	public static final String SAL_CAPACITE_KEY = "salCapacite";
	public static final String SAL_CAPA_EXAM_KEY = "salCapaExam";
	public static final String SAL_DESCRIPTIF_KEY = "salDescriptif";
	public static final String SAL_ECRAN_KEY = "salEcran";
	public static final String SAL_ETAGE_KEY = "salEtage";
	public static final String SAL_IDENT_ACCES_KEY = "salIdentAcces";
	public static final String SAL_INSONORISEE_KEY = "salInsonorisee";
	public static final String SAL_NATURE_KEY = "salNature";
	public static final String SAL_NB_ARMOIRES_KEY = "salNbArmoires";
	public static final String SAL_NB_BUREAUX_KEY = "salNbBureaux";
	public static final String SAL_NB_CHAISES_KEY = "salNbChaises";
	public static final String SAL_NB_FENETRES_KEY = "salNbFenetres";
	public static final String SAL_NB_PLACES_EXAM_LIB_KEY = "salNbPlacesExamLib";
	public static final String SAL_NB_TABLES_KEY = "salNbTables";
	public static final String SAL_NO_POSTE_KEY = "salNoPoste";
	public static final String SAL_NUM_DEPART_KEY = "salNumDepart";
	public static final String SAL_OBSCUR_KEY = "salObscur";
	public static final String SAL_PAS_NUM_KEY = "salPasNum";
	public static final String SAL_PORTE_KEY = "salPorte";
	public static final String SAL_POUR_ADMINISTRATION_KEY = "salPourAdministration";
	public static final String SAL_POUR_DOCUMENTATION_KEY = "salPourDocumentation";
	public static final String SAL_POUR_ENSEIGNEMENT_KEY = "salPourEnseignement";
	public static final String SAL_POUR_RECHERCHE_KEY = "salPourRecherche";
	public static final String SAL_POUR_TECHNIQUE_KEY = "salPourTechnique";
	public static final String SAL_RESERVABLE_KEY = "salReservable";
	public static final String SAL_RETRO_KEY = "salRetro";
	public static final String SAL_SALLE_PROCHE_TEL_KEY = "salSalleProcheTel";
	public static final String SAL_SUPERFICIE_KEY = "salSuperficie";
	public static final String SAL_TABLEAU_KEY = "salTableau";
	public static final String SAL_TABLEAU_BLANC_KEY = "salTableauBlanc";
	public static final String SAL_TABLEAU_PAPIER_KEY = "salTableauPapier";
	public static final String SAL_TELEVISION_KEY = "salTelevision";
	public static final String SAL_TEM_EN_SERVICE_KEY = "salTemEnService";
	public static final String SAL_TEM_HANDICAP_KEY = "salTemHandicap";
	public static final String TSAL_NUMERO_KEY = "tsalNumero";

//Attributs non visibles
	public static final String C_LOCAL_KEY = "cLocal";
	public static final String SAL_NUMERO_KEY = "salNumero";

//Colonnes dans la base de donnees
	public static final String D_ANNULATION_COLKEY = "D_ANNULATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String SAL_ACCES_COLKEY = "SAL_ACCES";
	public static final String SAL_BADGEUR_COLKEY = "SAL_BADGEUR";
	public static final String SAL_CAPACITE_COLKEY = "SAL_CAPACITE";
	public static final String SAL_CAPA_EXAM_COLKEY = "SAL_CAPA_EXAM";
	public static final String SAL_DESCRIPTIF_COLKEY = "SAL_DESCRIPTIF";
	public static final String SAL_ECRAN_COLKEY = "SAL_ECRAN";
	public static final String SAL_ETAGE_COLKEY = "SAL_ETAGE";
	public static final String SAL_IDENT_ACCES_COLKEY = "SAL_IDENT_ACCES";
	public static final String SAL_INSONORISEE_COLKEY = "SAL_INSONORISEE";
	public static final String SAL_NATURE_COLKEY = "SAL_NATURE";
	public static final String SAL_NB_ARMOIRES_COLKEY = "SAL_NB_ARMOIRES";
	public static final String SAL_NB_BUREAUX_COLKEY = "SAL_NB_BUREAUX";
	public static final String SAL_NB_CHAISES_COLKEY = "SAL_NB_CHAISES";
	public static final String SAL_NB_FENETRES_COLKEY = "SAL_NB_FENETRES";
	public static final String SAL_NB_PLACES_EXAM_LIB_COLKEY = "SAL_NB_PLACES_EXAM_LIB";
	public static final String SAL_NB_TABLES_COLKEY = "SAL_NB_TABLES";
	public static final String SAL_NO_POSTE_COLKEY = "SAL_NO_POSTE";
	public static final String SAL_NUM_DEPART_COLKEY = "SAL_NUM_DEPART";
	public static final String SAL_OBSCUR_COLKEY = "SAL_OBSCUR";
	public static final String SAL_PAS_NUM_COLKEY = "SAL_PAS_NUM";
	public static final String SAL_PORTE_COLKEY = "SAL_PORTE";
	public static final String SAL_POUR_ADMINISTRATION_COLKEY = "SAL_POUR_ADMINISTRATION";
	public static final String SAL_POUR_DOCUMENTATION_COLKEY = "SAL_POUR_DOCUMENTATION";
	public static final String SAL_POUR_ENSEIGNEMENT_COLKEY = "SAL_POUR_ENSEIGNEMENT";
	public static final String SAL_POUR_RECHERCHE_COLKEY = "SAL_POUR_RECHERCHE";
	public static final String SAL_POUR_TECHNIQUE_COLKEY = "SAL_POUR_TECHNIQUE";
	public static final String SAL_RESERVABLE_COLKEY = "SAL_RESERVABLE";
	public static final String SAL_RETRO_COLKEY = "SAL_RETRO";
	public static final String SAL_SALLE_PROCHE_TEL_COLKEY = "SAL_SALLE_PROCHE_TEL";
	public static final String SAL_SUPERFICIE_COLKEY = "SAL_SUPERFICIE";
	public static final String SAL_TABLEAU_COLKEY = "SAL_TABLEAU";
	public static final String SAL_TABLEAU_BLANC_COLKEY = "SAL_TABLEAU_BLANC";
	public static final String SAL_TABLEAU_PAPIER_COLKEY = "SAL_TABLEAU_PAPIER";
	public static final String SAL_TELEVISION_COLKEY = "SAL_TELEVISION";
	public static final String SAL_TEM_EN_SERVICE_COLKEY = "SAL_TEM_EN_SERVICE";
	public static final String SAL_TEM_HANDICAP_COLKEY = "SAL_TEM_HANDICAP";
	public static final String TSAL_NUMERO_COLKEY = "TSAL_NUMERO";

	public static final String C_LOCAL_COLKEY = "C_LOCAL";
	public static final String SAL_NUMERO_COLKEY = "SAL_NUMERO";


	// Relationships
	public static final String LOCAL_KEY = "local";
	public static final String OBJET_RESERVABLES_KEY = "objetReservables";
	public static final String TYPE_SALLE_KEY = "typeSalle";



	// Accessors methods
	public NSTimestamp dAnnulation() {
	 return (NSTimestamp) storedValueForKey(D_ANNULATION_KEY);
	}

	public void setDAnnulation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_ANNULATION_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public String salAcces() {
	 return (String) storedValueForKey(SAL_ACCES_KEY);
	}

	public void setSalAcces(String value) {
	 takeStoredValueForKey(value, SAL_ACCES_KEY);
	}

	public String salBadgeur() {
	 return (String) storedValueForKey(SAL_BADGEUR_KEY);
	}

	public void setSalBadgeur(String value) {
	 takeStoredValueForKey(value, SAL_BADGEUR_KEY);
	}

	public Double salCapacite() {
	 return (Double) storedValueForKey(SAL_CAPACITE_KEY);
	}

	public void setSalCapacite(Double value) {
	 takeStoredValueForKey(value, SAL_CAPACITE_KEY);
	}

	public Double salCapaExam() {
	 return (Double) storedValueForKey(SAL_CAPA_EXAM_KEY);
	}

	public void setSalCapaExam(Double value) {
	 takeStoredValueForKey(value, SAL_CAPA_EXAM_KEY);
	}

	public String salDescriptif() {
	 return (String) storedValueForKey(SAL_DESCRIPTIF_KEY);
	}

	public void setSalDescriptif(String value) {
	 takeStoredValueForKey(value, SAL_DESCRIPTIF_KEY);
	}

	public String salEcran() {
	 return (String) storedValueForKey(SAL_ECRAN_KEY);
	}

	public void setSalEcran(String value) {
	 takeStoredValueForKey(value, SAL_ECRAN_KEY);
	}

	public String salEtage() {
	 return (String) storedValueForKey(SAL_ETAGE_KEY);
	}

	public void setSalEtage(String value) {
	 takeStoredValueForKey(value, SAL_ETAGE_KEY);
	}

	public String salIdentAcces() {
	 return (String) storedValueForKey(SAL_IDENT_ACCES_KEY);
	}

	public void setSalIdentAcces(String value) {
	 takeStoredValueForKey(value, SAL_IDENT_ACCES_KEY);
	}

	public String salInsonorisee() {
	 return (String) storedValueForKey(SAL_INSONORISEE_KEY);
	}

	public void setSalInsonorisee(String value) {
	 takeStoredValueForKey(value, SAL_INSONORISEE_KEY);
	}

	public String salNature() {
	 return (String) storedValueForKey(SAL_NATURE_KEY);
	}

	public void setSalNature(String value) {
	 takeStoredValueForKey(value, SAL_NATURE_KEY);
	}

	public Double salNbArmoires() {
	 return (Double) storedValueForKey(SAL_NB_ARMOIRES_KEY);
	}

	public void setSalNbArmoires(Double value) {
	 takeStoredValueForKey(value, SAL_NB_ARMOIRES_KEY);
	}

	public Double salNbBureaux() {
	 return (Double) storedValueForKey(SAL_NB_BUREAUX_KEY);
	}

	public void setSalNbBureaux(Double value) {
	 takeStoredValueForKey(value, SAL_NB_BUREAUX_KEY);
	}

	public Double salNbChaises() {
	 return (Double) storedValueForKey(SAL_NB_CHAISES_KEY);
	}

	public void setSalNbChaises(Double value) {
	 takeStoredValueForKey(value, SAL_NB_CHAISES_KEY);
	}

	public Double salNbFenetres() {
	 return (Double) storedValueForKey(SAL_NB_FENETRES_KEY);
	}

	public void setSalNbFenetres(Double value) {
	 takeStoredValueForKey(value, SAL_NB_FENETRES_KEY);
	}

	public Double salNbPlacesExamLib() {
	 return (Double) storedValueForKey(SAL_NB_PLACES_EXAM_LIB_KEY);
	}

	public void setSalNbPlacesExamLib(Double value) {
	 takeStoredValueForKey(value, SAL_NB_PLACES_EXAM_LIB_KEY);
	}

	public Double salNbTables() {
	 return (Double) storedValueForKey(SAL_NB_TABLES_KEY);
	}

	public void setSalNbTables(Double value) {
	 takeStoredValueForKey(value, SAL_NB_TABLES_KEY);
	}

	public String salNoPoste() {
	 return (String) storedValueForKey(SAL_NO_POSTE_KEY);
	}

	public void setSalNoPoste(String value) {
	 takeStoredValueForKey(value, SAL_NO_POSTE_KEY);
	}

	public Double salNumDepart() {
	 return (Double) storedValueForKey(SAL_NUM_DEPART_KEY);
	}

	public void setSalNumDepart(Double value) {
	 takeStoredValueForKey(value, SAL_NUM_DEPART_KEY);
	}

	public String salObscur() {
	 return (String) storedValueForKey(SAL_OBSCUR_KEY);
	}

	public void setSalObscur(String value) {
	 takeStoredValueForKey(value, SAL_OBSCUR_KEY);
	}

	public Long salPasNum() {
	 return (Long) storedValueForKey(SAL_PAS_NUM_KEY);
	}

	public void setSalPasNum(Long value) {
	 takeStoredValueForKey(value, SAL_PAS_NUM_KEY);
	}

	public String salPorte() {
	 return (String) storedValueForKey(SAL_PORTE_KEY);
	}

	public void setSalPorte(String value) {
	 takeStoredValueForKey(value, SAL_PORTE_KEY);
	}

	public java.math.BigDecimal salPourAdministration() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_POUR_ADMINISTRATION_KEY);
	}

	public void setSalPourAdministration(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_POUR_ADMINISTRATION_KEY);
	}

	public java.math.BigDecimal salPourDocumentation() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_POUR_DOCUMENTATION_KEY);
	}

	public void setSalPourDocumentation(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_POUR_DOCUMENTATION_KEY);
	}

	public java.math.BigDecimal salPourEnseignement() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_POUR_ENSEIGNEMENT_KEY);
	}

	public void setSalPourEnseignement(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_POUR_ENSEIGNEMENT_KEY);
	}

	public java.math.BigDecimal salPourRecherche() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_POUR_RECHERCHE_KEY);
	}

	public void setSalPourRecherche(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_POUR_RECHERCHE_KEY);
	}

	public java.math.BigDecimal salPourTechnique() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_POUR_TECHNIQUE_KEY);
	}

	public void setSalPourTechnique(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_POUR_TECHNIQUE_KEY);
	}

	public String salReservable() {
	 return (String) storedValueForKey(SAL_RESERVABLE_KEY);
	}

	public void setSalReservable(String value) {
	 takeStoredValueForKey(value, SAL_RESERVABLE_KEY);
	}

	public String salRetro() {
	 return (String) storedValueForKey(SAL_RETRO_KEY);
	}

	public void setSalRetro(String value) {
	 takeStoredValueForKey(value, SAL_RETRO_KEY);
	}

	public Double salSalleProcheTel() {
	 return (Double) storedValueForKey(SAL_SALLE_PROCHE_TEL_KEY);
	}

	public void setSalSalleProcheTel(Double value) {
	 takeStoredValueForKey(value, SAL_SALLE_PROCHE_TEL_KEY);
	}

	public java.math.BigDecimal salSuperficie() {
	 return (java.math.BigDecimal) storedValueForKey(SAL_SUPERFICIE_KEY);
	}

	public void setSalSuperficie(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SAL_SUPERFICIE_KEY);
	}

	public String salTableau() {
	 return (String) storedValueForKey(SAL_TABLEAU_KEY);
	}

	public void setSalTableau(String value) {
	 takeStoredValueForKey(value, SAL_TABLEAU_KEY);
	}

	public String salTableauBlanc() {
	 return (String) storedValueForKey(SAL_TABLEAU_BLANC_KEY);
	}

	public void setSalTableauBlanc(String value) {
	 takeStoredValueForKey(value, SAL_TABLEAU_BLANC_KEY);
	}

	public String salTableauPapier() {
	 return (String) storedValueForKey(SAL_TABLEAU_PAPIER_KEY);
	}

	public void setSalTableauPapier(String value) {
	 takeStoredValueForKey(value, SAL_TABLEAU_PAPIER_KEY);
	}

	public String salTelevision() {
	 return (String) storedValueForKey(SAL_TELEVISION_KEY);
	}

	public void setSalTelevision(String value) {
	 takeStoredValueForKey(value, SAL_TELEVISION_KEY);
	}

	public String salTemEnService() {
	 return (String) storedValueForKey(SAL_TEM_EN_SERVICE_KEY);
	}

	public void setSalTemEnService(String value) {
	 takeStoredValueForKey(value, SAL_TEM_EN_SERVICE_KEY);
	}

	public String salTemHandicap() {
	 return (String) storedValueForKey(SAL_TEM_HANDICAP_KEY);
	}

	public void setSalTemHandicap(String value) {
	 takeStoredValueForKey(value, SAL_TEM_HANDICAP_KEY);
	}

	public Double tsalNumero() {
	 return (Double) storedValueForKey(TSAL_NUMERO_KEY);
	}

	public void setTsalNumero(Double value) {
	 takeStoredValueForKey(value, TSAL_NUMERO_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOLocal local() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLocal)storedValueForKey(LOCAL_KEY);
	}

	public void setLocalRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLocal value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOLocal oldValue = local();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOCAL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LOCAL_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle typeSalle() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle)storedValueForKey(TYPE_SALLE_KEY);
	}

	public void setTypeSalleRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle oldValue = typeSalle();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SALLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SALLE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)storedValueForKey(OBJET_RESERVABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier) {
	 return objetReservables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier, boolean fetch) {
	 return objetReservables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.SALLE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = objetReservables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
	}
	
	public void removeFromObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable createObjetReservablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, OBJET_RESERVABLES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable) eo;
	}
	
	public void deleteObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllObjetReservablesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objects = objetReservables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteObjetReservablesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOSalle avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOSalle createEOSalle(EOEditingContext editingContext						, NSTimestamp dCreation
							, NSTimestamp dModification
																			, String salEtage
																																	, String salPorte
																																					, org.cocktail.fwkcktlinternat.serveur.metier.EOLocal local							) {
	 EOSalle eo = (EOSalle) EOUtilities.createAndInsertInstance(editingContext, _EOSalle.ENTITY_NAME);	 
									eo.setDCreation(dCreation);
									eo.setDModification(dModification);
																					eo.setSalEtage(salEtage);
																																			eo.setSalPorte(salPorte);
																																						 eo.setLocalRelationship(local);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSalle creerInstance(EOEditingContext editingContext) {
		EOSalle object = (EOSalle)EOUtilities.createAndInsertInstance(editingContext, _EOSalle.ENTITY_NAME);
  		return object;
		}

	

  public EOSalle localInstanceIn(EOEditingContext editingContext) {
    EOSalle localInstance = (EOSalle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSalle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSalle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOSalle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSalle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSalle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSalle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSalle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOSalle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSalle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSalle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSalle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSalle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSalle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSalle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
