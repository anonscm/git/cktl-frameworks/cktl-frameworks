/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTypeTarif.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOTypeTarif extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOTypeTarif.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_TypeTarif";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TYPE_TARIF";


//Attribute Keys
	public static final ERXKey<Long> ID_TYPE_PRODUIT = new ERXKey<Long>("idTypeProduit");
	public static final ERXKey<Boolean> INDIC_CAF = new ERXKey<Boolean>("indicCaf");
	public static final ERXKey<String> LIBELLE_TYPE_TARIF = new ERXKey<String>("libelleTypeTarif");
	public static final ERXKey<Long> TYPE_TARIF_PERE = new ERXKey<Long>("typeTarifPere");
	public static final ERXKey<Long> TYPE_TVA_LOC = new ERXKey<Long>("typeTvaLoc");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> LE_TYPE_TARIF_PERE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("leTypeTarifPere");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva> LE_TYPE_TVA = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva>("leTypeTva");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFactures");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> TYPE_OBJETS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>("typeObjets");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> TYPE_PRODUIT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit>("typeProduit");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> TYPE_TARIF_FILS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("typeTarifFils");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "codeTypeTarif";

	public static final String ID_TYPE_PRODUIT_KEY = "idTypeProduit";
	public static final String INDIC_CAF_KEY = "indicCaf";
	public static final String LIBELLE_TYPE_TARIF_KEY = "libelleTypeTarif";
	public static final String TYPE_TARIF_PERE_KEY = "typeTarifPere";
	public static final String TYPE_TVA_LOC_KEY = "typeTvaLoc";

//Attributs non visibles
	public static final String CODE_TYPE_TARIF_KEY = "codeTypeTarif";

//Colonnes dans la base de donnees
	public static final String ID_TYPE_PRODUIT_COLKEY = "ID_TYPE_PRODUIT";
	public static final String INDIC_CAF_COLKEY = "INDIC_CAF";
	public static final String LIBELLE_TYPE_TARIF_COLKEY = "LIBELLE_TYPE_TARIF";
	public static final String TYPE_TARIF_PERE_COLKEY = "TYPE_TARIF_PERE";
	public static final String TYPE_TVA_LOC_COLKEY = "TYPE_TVA_LOC";

	public static final String CODE_TYPE_TARIF_COLKEY = "CODE_TYPE_TARIF";


	// Relationships
	public static final String DETAIL_HEBERGEMENTS_KEY = "detailHebergements";
	public static final String LE_TYPE_TARIF_PERE_KEY = "leTypeTarifPere";
	public static final String LE_TYPE_TVA_KEY = "leTypeTva";
	public static final String LIGNE_FACTURES_KEY = "ligneFactures";
	public static final String TYPE_OBJETS_KEY = "typeObjets";
	public static final String TYPE_PRODUIT_KEY = "typeProduit";
	public static final String TYPE_TARIF_FILS_KEY = "typeTarifFils";



	// Accessors methods
	public Long idTypeProduit() {
	 return (Long) storedValueForKey(ID_TYPE_PRODUIT_KEY);
	}

	public void setIdTypeProduit(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_PRODUIT_KEY);
	}

	public Boolean indicCaf() {
	 return (Boolean) storedValueForKey(INDIC_CAF_KEY);
	}

	public void setIndicCaf(Boolean value) {
	 takeStoredValueForKey(value, INDIC_CAF_KEY);
	}

	public String libelleTypeTarif() {
	 return (String) storedValueForKey(LIBELLE_TYPE_TARIF_KEY);
	}

	public void setLibelleTypeTarif(String value) {
	 takeStoredValueForKey(value, LIBELLE_TYPE_TARIF_KEY);
	}

	public Long typeTarifPere() {
	 return (Long) storedValueForKey(TYPE_TARIF_PERE_KEY);
	}

	public void setTypeTarifPere(Long value) {
	 takeStoredValueForKey(value, TYPE_TARIF_PERE_KEY);
	}

	public Long typeTvaLoc() {
	 return (Long) storedValueForKey(TYPE_TVA_LOC_KEY);
	}

	public void setTypeTvaLoc(Long value) {
	 takeStoredValueForKey(value, TYPE_TVA_LOC_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif leTypeTarifPere() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif)storedValueForKey(LE_TYPE_TARIF_PERE_KEY);
	}

	public void setLeTypeTarifPereRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif oldValue = leTypeTarifPere();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LE_TYPE_TARIF_PERE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LE_TYPE_TARIF_PERE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva leTypeTva() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva)storedValueForKey(LE_TYPE_TVA_KEY);
	}

	public void setLeTypeTvaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva oldValue = leTypeTva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LE_TYPE_TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LE_TYPE_TVA_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit typeProduit() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit)storedValueForKey(TYPE_PRODUIT_KEY);
	}

	public void setTypeProduitRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit oldValue = typeProduit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PRODUIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PRODUIT_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)storedValueForKey(DETAIL_HEBERGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier) {
	 return detailHebergements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, boolean fetch) {
	 return detailHebergements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.TYPE_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = detailHebergements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public void removeFromDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement createDetailHebergementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DETAIL_HEBERGEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement) eo;
	}
	
	public void deleteDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDetailHebergementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> objects = detailHebergements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDetailHebergementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)storedValueForKey(LIGNE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier) {
	 return ligneFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, boolean fetch) {
	 return ligneFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.TYPE_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ligneFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public void removeFromLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture createLigneFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture) eo;
	}
	
	public void deleteLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLigneFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> objects = ligneFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLigneFacturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> typeObjets() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>)storedValueForKey(TYPE_OBJETS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> typeObjets(EOQualifier qualifier) {
	 return typeObjets(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> typeObjets(EOQualifier qualifier, boolean fetch) {
	 return typeObjets(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> typeObjets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet.TYPE_TARIF_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = typeObjets();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToTypeObjetsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TYPE_OBJETS_KEY);
	}
	
	public void removeFromTypeObjetsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_OBJETS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet createTypeObjetsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TYPE_OBJETS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet) eo;
	}
	
	public void deleteTypeObjetsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_OBJETS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllTypeObjetsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> objects = typeObjets().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteTypeObjetsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifFils() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)storedValueForKey(TYPE_TARIF_FILS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifFils(EOQualifier qualifier) {
	 return typeTarifFils(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifFils(EOQualifier qualifier, boolean fetch) {
	 return typeTarifFils(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.LE_TYPE_TARIF_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = typeTarifFils();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToTypeTarifFilsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TYPE_TARIF_FILS_KEY);
	}
	
	public void removeFromTypeTarifFilsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_TARIF_FILS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif createTypeTarifFilsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TYPE_TARIF_FILS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif) eo;
	}
	
	public void deleteTypeTarifFilsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_TARIF_FILS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllTypeTarifFilsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> objects = typeTarifFils().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteTypeTarifFilsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTypeTarif avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeTarif createEOTypeTarif(EOEditingContext editingContext																					) {
	 EOTypeTarif eo = (EOTypeTarif) EOUtilities.createAndInsertInstance(editingContext, _EOTypeTarif.ENTITY_NAME);	 
																			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeTarif creerInstance(EOEditingContext editingContext) {
		EOTypeTarif object = (EOTypeTarif)EOUtilities.createAndInsertInstance(editingContext, _EOTypeTarif.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeTarif localInstanceIn(EOEditingContext editingContext) {
    EOTypeTarif localInstance = (EOTypeTarif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeTarif fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeTarif fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeTarif> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeTarif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeTarif)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeTarif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeTarif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeTarif> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeTarif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeTarif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeTarif fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeTarif eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeTarif ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeTarif fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
