/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOPersonne.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOPersonne extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOPersonne.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Personne";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PERSONNE";


//Attribute Keys
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
	public static final ERXKey<String> PERS_LC = new ERXKey<String>("persLc");
	public static final ERXKey<String> PERS_LIBELLE = new ERXKey<String>("persLibelle");
	public static final ERXKey<String> PERS_NOMPTR = new ERXKey<String>("persNomptr");
	public static final ERXKey<Integer> PERS_ORDRE = new ERXKey<Integer>("persOrdre");
	public static final ERXKey<String> PERS_TYPE = new ERXKey<String>("persType");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFactures");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> ETRE_ALLOCATAIRES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire>("etreAllocataires");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> FOURNIS_ULRS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>("fournisUlrs");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> HEBERGEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>("hebergements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> PRELEVEMENTSES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>("prelevementses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> PRELEVEMENTS_SEPA = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa>("prelevementsSepa");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> REGLEMENTSES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements>("reglementses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATIONS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restaurations");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "persId";

	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_NOMPTR_KEY = "persNomptr";
	public static final String PERS_ORDRE_KEY = "persOrdre";
	public static final String PERS_TYPE_KEY = "persType";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_NOMPTR_COLKEY = "PERS_NOMPTR";
	public static final String PERS_ORDRE_COLKEY = "PERS_ORDRE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";



	// Relationships
	public static final String DETAIL_HEBERGEMENTS_KEY = "detailHebergements";
	public static final String ENTETE_FACTURES_KEY = "enteteFactures";
	public static final String ETRE_ALLOCATAIRES_KEY = "etreAllocataires";
	public static final String FOURNIS_ULRS_KEY = "fournisUlrs";
	public static final String HEBERGEMENTS_KEY = "hebergements";
	public static final String PRELEVEMENTSES_KEY = "prelevementses";
	public static final String PRELEVEMENTS_SEPA_KEY = "prelevementsSepa";
	public static final String REGLEMENTSES_KEY = "reglementses";
	public static final String RESTAURATIONS_KEY = "restaurations";



	// Accessors methods
	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public String persLc() {
	 return (String) storedValueForKey(PERS_LC_KEY);
	}

	public void setPersLc(String value) {
	 takeStoredValueForKey(value, PERS_LC_KEY);
	}

	public String persLibelle() {
	 return (String) storedValueForKey(PERS_LIBELLE_KEY);
	}

	public void setPersLibelle(String value) {
	 takeStoredValueForKey(value, PERS_LIBELLE_KEY);
	}

	public String persNomptr() {
	 return (String) storedValueForKey(PERS_NOMPTR_KEY);
	}

	public void setPersNomptr(String value) {
	 takeStoredValueForKey(value, PERS_NOMPTR_KEY);
	}

	public Integer persOrdre() {
	 return (Integer) storedValueForKey(PERS_ORDRE_KEY);
	}

	public void setPersOrdre(Integer value) {
	 takeStoredValueForKey(value, PERS_ORDRE_KEY);
	}

	public String persType() {
	 return (String) storedValueForKey(PERS_TYPE_KEY);
	}

	public void setPersType(String value) {
	 takeStoredValueForKey(value, PERS_TYPE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)storedValueForKey(DETAIL_HEBERGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier) {
	 return detailHebergements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, boolean fetch) {
	 return detailHebergements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = detailHebergements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public void removeFromDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement createDetailHebergementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DETAIL_HEBERGEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement) eo;
	}
	
	public void deleteDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDetailHebergementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> objects = detailHebergements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDetailHebergementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFactures() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)storedValueForKey(ENTETE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFactures(EOQualifier qualifier) {
	 return enteteFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFactures(EOQualifier qualifier, boolean fetch) {
	 return enteteFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = enteteFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEnteteFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_KEY);
	}
	
	public void removeFromEnteteFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture createEnteteFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENTETE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture) eo;
	}
	
	public void deleteEnteteFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEnteteFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> objects = enteteFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEnteteFacturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> etreAllocataires() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire>)storedValueForKey(ETRE_ALLOCATAIRES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> etreAllocataires(EOQualifier qualifier) {
	 return etreAllocataires(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> etreAllocataires(EOQualifier qualifier, boolean fetch) {
	 return etreAllocataires(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> etreAllocataires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = etreAllocataires();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEtreAllocatairesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ETRE_ALLOCATAIRES_KEY);
	}
	
	public void removeFromEtreAllocatairesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ETRE_ALLOCATAIRES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire createEtreAllocatairesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ETRE_ALLOCATAIRES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire) eo;
	}
	
	public void deleteEtreAllocatairesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ETRE_ALLOCATAIRES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEtreAllocatairesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> objects = etreAllocataires().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEtreAllocatairesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fournisUlrs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>)storedValueForKey(FOURNIS_ULRS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fournisUlrs(EOQualifier qualifier) {
	 return fournisUlrs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fournisUlrs(EOQualifier qualifier, boolean fetch) {
	 return fournisUlrs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fournisUlrs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = fournisUlrs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFournisUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
	}
	
	public void removeFromFournisUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr createFournisUlrsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FOURNIS_ULRS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr) eo;
	}
	
	public void deleteFournisUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FOURNIS_ULRS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFournisUlrsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> objects = fournisUlrs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFournisUlrsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> hebergements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>)storedValueForKey(HEBERGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> hebergements(EOQualifier qualifier) {
	 return hebergements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> hebergements(EOQualifier qualifier, boolean fetch) {
	 return hebergements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> hebergements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = hebergements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, HEBERGEMENTS_KEY);
	}
	
	public void removeFromHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, HEBERGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement createHebergementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, HEBERGEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement) eo;
	}
	
	public void deleteHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, HEBERGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllHebergementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> objects = hebergements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteHebergementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)storedValueForKey(PRELEVEMENTSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier) {
	 return prelevementses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier, boolean fetch) {
	 return prelevementses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = prelevementses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
	}
	
	public void removeFromPrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements createPrelevementsesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRELEVEMENTSES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements) eo;
	}
	
	public void deletePrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPrelevementsesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> objects = prelevementses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePrelevementsesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> prelevementsSepa() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa>)storedValueForKey(PRELEVEMENTS_SEPA_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> prelevementsSepa(EOQualifier qualifier) {
	 return prelevementsSepa(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> prelevementsSepa(EOQualifier qualifier, boolean fetch) {
	 return prelevementsSepa(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> prelevementsSepa(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa.TO_PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = prelevementsSepa();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPrelevementsSepaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_SEPA_KEY);
	}
	
	public void removeFromPrelevementsSepaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_SEPA_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa createPrelevementsSepaRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRELEVEMENTS_SEPA_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa) eo;
	}
	
	public void deletePrelevementsSepaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_SEPA_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPrelevementsSepaRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevementsSepa> objects = prelevementsSepa().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePrelevementsSepaRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> reglementses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements>)storedValueForKey(REGLEMENTSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> reglementses(EOQualifier qualifier) {
	 return reglementses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> reglementses(EOQualifier qualifier, boolean fetch) {
	 return reglementses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> reglementses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOReglements.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = reglementses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToReglementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REGLEMENTSES_KEY);
	}
	
	public void removeFromReglementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REGLEMENTSES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOReglements createReglementsesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REGLEMENTSES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOReglements) eo;
	}
	
	public void deleteReglementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REGLEMENTSES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllReglementsesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> objects = reglementses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteReglementsesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)storedValueForKey(RESTAURATIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier) {
	 return restaurations(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, boolean fetch) {
	 return restaurations(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = restaurations();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public void removeFromRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration createRestaurationsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RESTAURATIONS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration) eo;
	}
	
	public void deleteRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRestaurationsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> objects = restaurations().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRestaurationsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOPersonne avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOPersonne createEOPersonne(EOEditingContext editingContext				, Integer persId
																		) {
	 EOPersonne eo = (EOPersonne) EOUtilities.createAndInsertInstance(editingContext, _EOPersonne.ENTITY_NAME);	 
							eo.setPersId(persId);
															 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonne creerInstance(EOEditingContext editingContext) {
		EOPersonne object = (EOPersonne)EOUtilities.createAndInsertInstance(editingContext, _EOPersonne.ENTITY_NAME);
  		return object;
		}

	

  public EOPersonne localInstanceIn(EOEditingContext editingContext) {
    EOPersonne localInstance = (EOPersonne)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersonne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersonne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersonne> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersonne> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersonne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersonne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
