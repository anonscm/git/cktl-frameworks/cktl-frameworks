/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOLigneFacture.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOLigneFacture extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOLigneFacture.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_LigneFacture";
	public static final String ENTITY_TABLE_NAME = "LITCHI.LIGNE_FACTURE";


//Attribute Keys
	public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
	public static final ERXKey<NSTimestamp> DATE_DEBUT_FACTURE = new ERXKey<NSTimestamp>("dateDebutFacture");
	public static final ERXKey<NSTimestamp> DATE_FIN_FACTURE = new ERXKey<NSTimestamp>("dateFinFacture");
	public static final ERXKey<Long> ID_DETAIL_HEBERGEMENT = new ERXKey<Long>("idDetailHebergement");
	public static final ERXKey<Long> ID_FACTURE = new ERXKey<Long>("idFacture");
	public static final ERXKey<Long> ID_REPART_DET_HEB_EA = new ERXKey<Long>("idRepartDetHebEa");
	public static final ERXKey<Long> ID_RESTAURATION = new ERXKey<Long>("idRestauration");
	public static final ERXKey<Long> ID_TARIF = new ERXKey<Long>("idTarif");
	public static final ERXKey<Long> ID_TYPE_PRODUIT = new ERXKey<Long>("idTypeProduit");
	public static final ERXKey<Long> ID_TYPE_TARIF = new ERXKey<Long>("idTypeTarif");
	public static final ERXKey<String> LIB_LIGNE_FACTURE = new ERXKey<String>("libLigneFacture");
	public static final ERXKey<java.math.BigDecimal> MNT_HT = new ERXKey<java.math.BigDecimal>("mntHt");
	public static final ERXKey<java.math.BigDecimal> MNT_TTC = new ERXKey<java.math.BigDecimal>("mntTtc");
	public static final ERXKey<Integer> MOIS = new ERXKey<Integer>("mois");
	public static final ERXKey<java.math.BigDecimal> PRIX_UNITAIRE_HT = new ERXKey<java.math.BigDecimal>("prixUnitaireHt");
	public static final ERXKey<java.math.BigDecimal> QUANTITE = new ERXKey<java.math.BigDecimal>("quantite");
	public static final ERXKey<String> STATUT_HEBERGE = new ERXKey<String>("statutHeberge");
	public static final ERXKey<java.math.BigDecimal> TAUX_TVA = new ERXKey<java.math.BigDecimal>("tauxTva");
	public static final ERXKey<Long> TYPE_TVA_LF = new ERXKey<Long>("typeTvaLf");
	public static final ERXKey<String> UNITE = new ERXKey<String>("unite");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergement");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFacture");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>("repartDetailHebergementElementAssocie");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATION = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restauration");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATIONS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restaurations");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTarif> TARIF = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTarif>("tarif");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> TYPE_PRODUIT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit>("typeProduit");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> TYPE_TARIF = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("typeTarif");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva> TYPE_TVA = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva>("typeTva");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idLigneFacture";

	public static final String ANNEE_KEY = "annee";
	public static final String DATE_DEBUT_FACTURE_KEY = "dateDebutFacture";
	public static final String DATE_FIN_FACTURE_KEY = "dateFinFacture";
	public static final String ID_DETAIL_HEBERGEMENT_KEY = "idDetailHebergement";
	public static final String ID_FACTURE_KEY = "idFacture";
	public static final String ID_REPART_DET_HEB_EA_KEY = "idRepartDetHebEa";
	public static final String ID_RESTAURATION_KEY = "idRestauration";
	public static final String ID_TARIF_KEY = "idTarif";
	public static final String ID_TYPE_PRODUIT_KEY = "idTypeProduit";
	public static final String ID_TYPE_TARIF_KEY = "idTypeTarif";
	public static final String LIB_LIGNE_FACTURE_KEY = "libLigneFacture";
	public static final String MNT_HT_KEY = "mntHt";
	public static final String MNT_TTC_KEY = "mntTtc";
	public static final String MOIS_KEY = "mois";
	public static final String PRIX_UNITAIRE_HT_KEY = "prixUnitaireHt";
	public static final String QUANTITE_KEY = "quantite";
	public static final String STATUT_HEBERGE_KEY = "statutHeberge";
	public static final String TAUX_TVA_KEY = "tauxTva";
	public static final String TYPE_TVA_LF_KEY = "typeTvaLf";
	public static final String UNITE_KEY = "unite";

//Attributs non visibles
	public static final String ID_LIGNE_FACTURE_KEY = "idLigneFacture";

//Colonnes dans la base de donnees
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String DATE_DEBUT_FACTURE_COLKEY = "DATE_DEBUT_FACTURE";
	public static final String DATE_FIN_FACTURE_COLKEY = "DATE_FIN_FACTURE";
	public static final String ID_DETAIL_HEBERGEMENT_COLKEY = "ID_DETAIL_HEBERGEMENT";
	public static final String ID_FACTURE_COLKEY = "ID_FACTURE";
	public static final String ID_REPART_DET_HEB_EA_COLKEY = "ID_REPART_DET_HEB_EA";
	public static final String ID_RESTAURATION_COLKEY = "ID_RESTAURATION";
	public static final String ID_TARIF_COLKEY = "ID_TARIF";
	public static final String ID_TYPE_PRODUIT_COLKEY = "ID_TYPE_PRODUIT";
	public static final String ID_TYPE_TARIF_COLKEY = "ID_TYPE_TARIF";
	public static final String LIB_LIGNE_FACTURE_COLKEY = "LIB_LIGNE_FACTURE";
	public static final String MNT_HT_COLKEY = "MNT_HT";
	public static final String MNT_TTC_COLKEY = "MNT_TTC";
	public static final String MOIS_COLKEY = "MOIS";
	public static final String PRIX_UNITAIRE_HT_COLKEY = "PRIX_UNITAIRE_HT";
	public static final String QUANTITE_COLKEY = "QUANTITE";
	public static final String STATUT_HEBERGE_COLKEY = "STATUS_HEBERGE";
	public static final String TAUX_TVA_COLKEY = "TAUX_TVA";
	public static final String TYPE_TVA_LF_COLKEY = "TYPE_TVA_LF";
	public static final String UNITE_COLKEY = "UNITE";

	public static final String ID_LIGNE_FACTURE_COLKEY = "ID_LIGNE_FACTURE";


	// Relationships
	public static final String DETAIL_HEBERGEMENT_KEY = "detailHebergement";
	public static final String DETAIL_HEBERGEMENTS_KEY = "detailHebergements";
	public static final String ENTETE_FACTURE_KEY = "enteteFacture";
	public static final String REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY = "repartDetailHebergementElementAssocie";
	public static final String RESTAURATION_KEY = "restauration";
	public static final String RESTAURATIONS_KEY = "restaurations";
	public static final String TARIF_KEY = "tarif";
	public static final String TYPE_PRODUIT_KEY = "typeProduit";
	public static final String TYPE_TARIF_KEY = "typeTarif";
	public static final String TYPE_TVA_KEY = "typeTva";



	// Accessors methods
	public Integer annee() {
	 return (Integer) storedValueForKey(ANNEE_KEY);
	}

	public void setAnnee(Integer value) {
	 takeStoredValueForKey(value, ANNEE_KEY);
	}

	public NSTimestamp dateDebutFacture() {
	 return (NSTimestamp) storedValueForKey(DATE_DEBUT_FACTURE_KEY);
	}

	public void setDateDebutFacture(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_DEBUT_FACTURE_KEY);
	}

	public NSTimestamp dateFinFacture() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_FACTURE_KEY);
	}

	public void setDateFinFacture(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_FACTURE_KEY);
	}

	public Long idDetailHebergement() {
	 return (Long) storedValueForKey(ID_DETAIL_HEBERGEMENT_KEY);
	}

	public void setIdDetailHebergement(Long value) {
	 takeStoredValueForKey(value, ID_DETAIL_HEBERGEMENT_KEY);
	}

	public Long idFacture() {
	 return (Long) storedValueForKey(ID_FACTURE_KEY);
	}

	public void setIdFacture(Long value) {
	 takeStoredValueForKey(value, ID_FACTURE_KEY);
	}

	public Long idRepartDetHebEa() {
	 return (Long) storedValueForKey(ID_REPART_DET_HEB_EA_KEY);
	}

	public void setIdRepartDetHebEa(Long value) {
	 takeStoredValueForKey(value, ID_REPART_DET_HEB_EA_KEY);
	}

	public Long idRestauration() {
	 return (Long) storedValueForKey(ID_RESTAURATION_KEY);
	}

	public void setIdRestauration(Long value) {
	 takeStoredValueForKey(value, ID_RESTAURATION_KEY);
	}

	public Long idTarif() {
	 return (Long) storedValueForKey(ID_TARIF_KEY);
	}

	public void setIdTarif(Long value) {
	 takeStoredValueForKey(value, ID_TARIF_KEY);
	}

	public Long idTypeProduit() {
	 return (Long) storedValueForKey(ID_TYPE_PRODUIT_KEY);
	}

	public void setIdTypeProduit(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_PRODUIT_KEY);
	}

	public Long idTypeTarif() {
	 return (Long) storedValueForKey(ID_TYPE_TARIF_KEY);
	}

	public void setIdTypeTarif(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_TARIF_KEY);
	}

	public String libLigneFacture() {
	 return (String) storedValueForKey(LIB_LIGNE_FACTURE_KEY);
	}

	public void setLibLigneFacture(String value) {
	 takeStoredValueForKey(value, LIB_LIGNE_FACTURE_KEY);
	}

	public java.math.BigDecimal mntHt() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_HT_KEY);
	}

	public void setMntHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_HT_KEY);
	}

	public java.math.BigDecimal mntTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_TTC_KEY);
	}

	public void setMntTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_TTC_KEY);
	}

	public Integer mois() {
	 return (Integer) storedValueForKey(MOIS_KEY);
	}

	public void setMois(Integer value) {
	 takeStoredValueForKey(value, MOIS_KEY);
	}

	public java.math.BigDecimal prixUnitaireHt() {
	 return (java.math.BigDecimal) storedValueForKey(PRIX_UNITAIRE_HT_KEY);
	}

	public void setPrixUnitaireHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, PRIX_UNITAIRE_HT_KEY);
	}

	public java.math.BigDecimal quantite() {
	 return (java.math.BigDecimal) storedValueForKey(QUANTITE_KEY);
	}

	public void setQuantite(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, QUANTITE_KEY);
	}

	public String statutHeberge() {
	 return (String) storedValueForKey(STATUT_HEBERGE_KEY);
	}

	public void setStatutHeberge(String value) {
	 takeStoredValueForKey(value, STATUT_HEBERGE_KEY);
	}

	public java.math.BigDecimal tauxTva() {
	 return (java.math.BigDecimal) storedValueForKey(TAUX_TVA_KEY);
	}

	public void setTauxTva(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, TAUX_TVA_KEY);
	}

	public Long typeTvaLf() {
	 return (Long) storedValueForKey(TYPE_TVA_LF_KEY);
	}

	public void setTypeTvaLf(Long value) {
	 takeStoredValueForKey(value, TYPE_TVA_LF_KEY);
	}

	public String unite() {
	 return (String) storedValueForKey(UNITE_KEY);
	}

	public void setUnite(String value) {
	 takeStoredValueForKey(value, UNITE_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement detailHebergement() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement)storedValueForKey(DETAIL_HEBERGEMENT_KEY);
	}

	public void setDetailHebergementRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement oldValue = detailHebergement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DETAIL_HEBERGEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DETAIL_HEBERGEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture enteteFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture)storedValueForKey(ENTETE_FACTURE_KEY);
	}

	public void setEnteteFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture oldValue = enteteFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENTETE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENTETE_FACTURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie repartDetailHebergementElementAssocie() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie)storedValueForKey(REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	}

	public void setRepartDetailHebergementElementAssocieRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie oldValue = repartDetailHebergementElementAssocie();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration restauration() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration)storedValueForKey(RESTAURATION_KEY);
	}

	public void setRestaurationRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EORestauration oldValue = restauration();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RESTAURATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, RESTAURATION_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTarif tarif() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTarif)storedValueForKey(TARIF_KEY);
	}

	public void setTarifRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTarif value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTarif oldValue = tarif();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TARIF_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TARIF_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit typeProduit() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit)storedValueForKey(TYPE_PRODUIT_KEY);
	}

	public void setTypeProduitRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit oldValue = typeProduit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PRODUIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PRODUIT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif typeTarif() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif)storedValueForKey(TYPE_TARIF_KEY);
	}

	public void setTypeTarifRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif oldValue = typeTarif();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TARIF_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TARIF_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva typeTva() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva)storedValueForKey(TYPE_TVA_KEY);
	}

	public void setTypeTvaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva oldValue = typeTva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TVA_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)storedValueForKey(DETAIL_HEBERGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier) {
	 return detailHebergements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, boolean fetch) {
	 return detailHebergements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.LIGNE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = detailHebergements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public void removeFromDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement createDetailHebergementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DETAIL_HEBERGEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement) eo;
	}
	
	public void deleteDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDetailHebergementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> objects = detailHebergements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDetailHebergementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)storedValueForKey(RESTAURATIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier) {
	 return restaurations(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, boolean fetch) {
	 return restaurations(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.LIGNE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = restaurations();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public void removeFromRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration createRestaurationsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RESTAURATIONS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration) eo;
	}
	
	public void deleteRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRestaurationsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> objects = restaurations().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRestaurationsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOLigneFacture avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOLigneFacture createEOLigneFacture(EOEditingContext editingContext																																																													) {
	 EOLigneFacture eo = (EOLigneFacture) EOUtilities.createAndInsertInstance(editingContext, _EOLigneFacture.ENTITY_NAME);	 
																																																											 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLigneFacture creerInstance(EOEditingContext editingContext) {
		EOLigneFacture object = (EOLigneFacture)EOUtilities.createAndInsertInstance(editingContext, _EOLigneFacture.ENTITY_NAME);
  		return object;
		}

	

  public EOLigneFacture localInstanceIn(EOEditingContext editingContext) {
    EOLigneFacture localInstance = (EOLigneFacture)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLigneFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLigneFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOLigneFacture> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLigneFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLigneFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLigneFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLigneFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOLigneFacture> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLigneFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLigneFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLigneFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLigneFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLigneFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLigneFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
