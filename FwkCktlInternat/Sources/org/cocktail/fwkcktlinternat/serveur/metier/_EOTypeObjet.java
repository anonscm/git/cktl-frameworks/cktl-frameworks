/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTypeObjet.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOTypeObjet extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOTypeObjet.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_TypeObjet";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TYPE_OBJET";


//Attribute Keys
	public static final ERXKey<String> LIB_TYPE_OBJET = new ERXKey<String>("libTypeObjet");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> EDITIONSES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions>("editionses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> ELEMENT_ASSOCIES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>("elementAssocies");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> OBJET_RESERVABLES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>("objetReservables");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> TYPE_TARIF = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("typeTarif");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "codeTypeObjet";

	public static final String LIB_TYPE_OBJET_KEY = "libTypeObjet";

//Attributs non visibles
	public static final String CODE_TYPE_OBJET_KEY = "codeTypeObjet";
	public static final String CODE_TYPE_TARIF_KEY = "codeTypeTarif";

//Colonnes dans la base de donnees
	public static final String LIB_TYPE_OBJET_COLKEY = "LIB_TYPE_OBJET";

	public static final String CODE_TYPE_OBJET_COLKEY = "CODE_TYPE_OBJET";
	public static final String CODE_TYPE_TARIF_COLKEY = "CODE_TYPE_TARIF";


	// Relationships
	public static final String EDITIONSES_KEY = "editionses";
	public static final String ELEMENT_ASSOCIES_KEY = "elementAssocies";
	public static final String OBJET_RESERVABLES_KEY = "objetReservables";
	public static final String TYPE_TARIF_KEY = "typeTarif";



	// Accessors methods
	public String libTypeObjet() {
	 return (String) storedValueForKey(LIB_TYPE_OBJET_KEY);
	}

	public void setLibTypeObjet(String value) {
	 takeStoredValueForKey(value, LIB_TYPE_OBJET_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif typeTarif() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif)storedValueForKey(TYPE_TARIF_KEY);
	}

	public void setTypeTarifRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif oldValue = typeTarif();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TARIF_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TARIF_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> editionses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions>)storedValueForKey(EDITIONSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> editionses(EOQualifier qualifier) {
	 return editionses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> editionses(EOQualifier qualifier, boolean fetch) {
	 return editionses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> editionses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOEditions.TYPE_OBJET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOEditions.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = editionses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEditionsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEditions object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, EDITIONSES_KEY);
	}
	
	public void removeFromEditionsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEditions object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, EDITIONSES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOEditions createEditionsesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOEditions.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, EDITIONSES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEditions) eo;
	}
	
	public void deleteEditionsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEditions object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, EDITIONSES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEditionsesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> objects = editionses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEditionsesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)storedValueForKey(ELEMENT_ASSOCIES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier) {
	 return elementAssocies(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier, boolean fetch) {
	 return elementAssocies(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.TYPE_OBJET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = elementAssocies();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
	}
	
	public void removeFromElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie createElementAssociesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ELEMENT_ASSOCIES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie) eo;
	}
	
	public void deleteElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllElementAssociesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> objects = elementAssocies().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteElementAssociesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)storedValueForKey(OBJET_RESERVABLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier) {
	 return objetReservables(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier, boolean fetch) {
	 return objetReservables(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objetReservables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.TYPE_OBJET_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = objetReservables();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
	}
	
	public void removeFromObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable createObjetReservablesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, OBJET_RESERVABLES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable) eo;
	}
	
	public void deleteObjetReservablesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, OBJET_RESERVABLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllObjetReservablesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> objects = objetReservables().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteObjetReservablesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTypeObjet avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeObjet createEOTypeObjet(EOEditingContext editingContext									) {
	 EOTypeObjet eo = (EOTypeObjet) EOUtilities.createAndInsertInstance(editingContext, _EOTypeObjet.ENTITY_NAME);	 
							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeObjet creerInstance(EOEditingContext editingContext) {
		EOTypeObjet object = (EOTypeObjet)EOUtilities.createAndInsertInstance(editingContext, _EOTypeObjet.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeObjet localInstanceIn(EOEditingContext editingContext) {
    EOTypeObjet localInstance = (EOTypeObjet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeObjet fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeObjet fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeObjet> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeObjet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeObjet)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeObjet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeObjet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeObjet> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeObjet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeObjet)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeObjet fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeObjet eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeObjet ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeObjet fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
