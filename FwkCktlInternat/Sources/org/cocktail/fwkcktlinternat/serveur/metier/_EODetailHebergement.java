/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EODetailHebergement.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EODetailHebergement extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EODetailHebergement.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_DetailHebergement";
	public static final String ENTITY_TABLE_NAME = "LITCHI.DETAIL_HEBERGEMENT";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<NSTimestamp> DATE_FIN_FACTURE = new ERXKey<NSTimestamp>("dateFinFacture");
	public static final ERXKey<NSTimestamp> DATE_FIN_RECHERCHE = new ERXKey<NSTimestamp>("dateFinRecherche");
	public static final ERXKey<NSTimestamp> DATE_PREAVIS = new ERXKey<NSTimestamp>("datePreavis");
	public static final ERXKey<NSTimestamp> DATE_REMISE_CLE = new ERXKey<NSTimestamp>("dateRemiseCle");
	public static final ERXKey<NSTimestamp> DATE_RETOUR_CLES = new ERXKey<NSTimestamp>("dateRetourCles");
	public static final ERXKey<Long> DERNIERE_FACTURE_LIGNE = new ERXKey<Long>("derniereFactureLigne");
	public static final ERXKey<Long> ID_BAIL = new ERXKey<Long>("idBail");
	public static final ERXKey<Long> ID_HEBERGEMENT = new ERXKey<Long>("idHebergement");
	public static final ERXKey<Long> ID_OBJET_HEBERGEMENT = new ERXKey<Long>("idObjetHebergement");
	public static final ERXKey<Long> ID_RESTAURATION = new ERXKey<Long>("idRestauration");
	public static final ERXKey<Long> ID_TYPE_CALCUL = new ERXKey<Long>("idTypeCalcul");
	public static final ERXKey<Long> ID_TYPE_TARIF = new ERXKey<Long>("idTypeTarif");
	public static final ERXKey<String> MOTIF = new ERXKey<String>("motif");
	public static final ERXKey<Integer> PERS_ID_CONCERNER = new ERXKey<Integer>("persIdConcerner");
	public static final ERXKey<Integer> PERS_ID_CONJOINT = new ERXKey<Integer>("persIdConjoint");
	public static final ERXKey<Integer> PERS_ID_CREATEUR = new ERXKey<Integer>("persIdCreateur");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOBail> BAIL = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOBail>("bail");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> HEBERGEMENT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>("hebergement");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFacture");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFactures");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable> OBJET_RESERVABLE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable>("objetReservable");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>("repartDetailHebergementElementAssocies");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATION = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restauration");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATIONS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restaurations");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul> TYPE_CALCUL = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul>("typeCalcul");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> TYPE_TARIF = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("typeTarif");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idDetailHebergement";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_FIN_FACTURE_KEY = "dateFinFacture";
	public static final String DATE_FIN_RECHERCHE_KEY = "dateFinRecherche";
	public static final String DATE_PREAVIS_KEY = "datePreavis";
	public static final String DATE_REMISE_CLE_KEY = "dateRemiseCle";
	public static final String DATE_RETOUR_CLES_KEY = "dateRetourCles";
	public static final String DERNIERE_FACTURE_LIGNE_KEY = "derniereFactureLigne";
	public static final String ID_BAIL_KEY = "idBail";
	public static final String ID_HEBERGEMENT_KEY = "idHebergement";
	public static final String ID_OBJET_HEBERGEMENT_KEY = "idObjetHebergement";
	public static final String ID_RESTAURATION_KEY = "idRestauration";
	public static final String ID_TYPE_CALCUL_KEY = "idTypeCalcul";
	public static final String ID_TYPE_TARIF_KEY = "idTypeTarif";
	public static final String MOTIF_KEY = "motif";
	public static final String PERS_ID_CONCERNER_KEY = "persIdConcerner";
	public static final String PERS_ID_CONJOINT_KEY = "persIdConjoint";
	public static final String PERS_ID_CREATEUR_KEY = "persIdCreateur";

//Attributs non visibles
	public static final String ID_DETAIL_HEBERGEMENT_KEY = "idDetailHebergement";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_DEBUT_COLKEY = "DATE_DEBUT";
	public static final String DATE_FIN_COLKEY = "DATE_FIN";
	public static final String DATE_FIN_FACTURE_COLKEY = "DATE_FIN_FACTURE";
	public static final String DATE_FIN_RECHERCHE_COLKEY = "$attribute.columnName";
	public static final String DATE_PREAVIS_COLKEY = "DATE_PREAVIS";
	public static final String DATE_REMISE_CLE_COLKEY = "DATE_REMISE_CLE";
	public static final String DATE_RETOUR_CLES_COLKEY = "DATE_RETOUR_CLES";
	public static final String DERNIERE_FACTURE_LIGNE_COLKEY = "DERNIERE_FACTURE_LIGNE";
	public static final String ID_BAIL_COLKEY = "ID_BAIL";
	public static final String ID_HEBERGEMENT_COLKEY = "ID_HEBERGEMENT";
	public static final String ID_OBJET_HEBERGEMENT_COLKEY = "ID_OBJET_HEBERGEMENT";
	public static final String ID_RESTAURATION_COLKEY = "ID_RESTAURATION";
	public static final String ID_TYPE_CALCUL_COLKEY = "ID_TYPE_CALCUL";
	public static final String ID_TYPE_TARIF_COLKEY = "ID_TYPE_TARIF";
	public static final String MOTIF_COLKEY = "MOTIF";
	public static final String PERS_ID_CONCERNER_COLKEY = "PERS_ID_CONCERNER";
	public static final String PERS_ID_CONJOINT_COLKEY = "PERS_ID_CONJOINT";
	public static final String PERS_ID_CREATEUR_COLKEY = "PERS_ID_CEATEUR";

	public static final String ID_DETAIL_HEBERGEMENT_COLKEY = "ID_DETAIL_HEBERGEMENT";


	// Relationships
	public static final String BAIL_KEY = "bail";
	public static final String HEBERGEMENT_KEY = "hebergement";
	public static final String LIGNE_FACTURE_KEY = "ligneFacture";
	public static final String LIGNE_FACTURES_KEY = "ligneFactures";
	public static final String OBJET_RESERVABLE_KEY = "objetReservable";
	public static final String PERSONNE_KEY = "personne";
	public static final String REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY = "repartDetailHebergementElementAssocies";
	public static final String RESTAURATION_KEY = "restauration";
	public static final String RESTAURATIONS_KEY = "restaurations";
	public static final String TYPE_CALCUL_KEY = "typeCalcul";
	public static final String TYPE_TARIF_KEY = "typeTarif";



	// Accessors methods
	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public NSTimestamp dateDebut() {
	 return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
	}

	public void setDateDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_DEBUT_KEY);
	}

	public NSTimestamp dateFin() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
	}

	public void setDateFin(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_KEY);
	}

	public NSTimestamp dateFinFacture() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_FACTURE_KEY);
	}

	public void setDateFinFacture(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_FACTURE_KEY);
	}

	public NSTimestamp dateFinRecherche() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_RECHERCHE_KEY);
	}

	public void setDateFinRecherche(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_RECHERCHE_KEY);
	}

	public NSTimestamp datePreavis() {
	 return (NSTimestamp) storedValueForKey(DATE_PREAVIS_KEY);
	}

	public void setDatePreavis(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_PREAVIS_KEY);
	}

	public NSTimestamp dateRemiseCle() {
	 return (NSTimestamp) storedValueForKey(DATE_REMISE_CLE_KEY);
	}

	public void setDateRemiseCle(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_REMISE_CLE_KEY);
	}

	public NSTimestamp dateRetourCles() {
	 return (NSTimestamp) storedValueForKey(DATE_RETOUR_CLES_KEY);
	}

	public void setDateRetourCles(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_RETOUR_CLES_KEY);
	}

	public Long derniereFactureLigne() {
	 return (Long) storedValueForKey(DERNIERE_FACTURE_LIGNE_KEY);
	}

	public void setDerniereFactureLigne(Long value) {
	 takeStoredValueForKey(value, DERNIERE_FACTURE_LIGNE_KEY);
	}

	public Long idBail() {
	 return (Long) storedValueForKey(ID_BAIL_KEY);
	}

	public void setIdBail(Long value) {
	 takeStoredValueForKey(value, ID_BAIL_KEY);
	}

	public Long idHebergement() {
	 return (Long) storedValueForKey(ID_HEBERGEMENT_KEY);
	}

	public void setIdHebergement(Long value) {
	 takeStoredValueForKey(value, ID_HEBERGEMENT_KEY);
	}

	public Long idObjetHebergement() {
	 return (Long) storedValueForKey(ID_OBJET_HEBERGEMENT_KEY);
	}

	public void setIdObjetHebergement(Long value) {
	 takeStoredValueForKey(value, ID_OBJET_HEBERGEMENT_KEY);
	}

	public Long idRestauration() {
	 return (Long) storedValueForKey(ID_RESTAURATION_KEY);
	}

	public void setIdRestauration(Long value) {
	 takeStoredValueForKey(value, ID_RESTAURATION_KEY);
	}

	public Long idTypeCalcul() {
	 return (Long) storedValueForKey(ID_TYPE_CALCUL_KEY);
	}

	public void setIdTypeCalcul(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_CALCUL_KEY);
	}

	public Long idTypeTarif() {
	 return (Long) storedValueForKey(ID_TYPE_TARIF_KEY);
	}

	public void setIdTypeTarif(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_TARIF_KEY);
	}

	public String motif() {
	 return (String) storedValueForKey(MOTIF_KEY);
	}

	public void setMotif(String value) {
	 takeStoredValueForKey(value, MOTIF_KEY);
	}

	public Integer persIdConcerner() {
	 return (Integer) storedValueForKey(PERS_ID_CONCERNER_KEY);
	}

	public void setPersIdConcerner(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CONCERNER_KEY);
	}

	public Integer persIdConjoint() {
	 return (Integer) storedValueForKey(PERS_ID_CONJOINT_KEY);
	}

	public void setPersIdConjoint(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CONJOINT_KEY);
	}

	public Integer persIdCreateur() {
	 return (Integer) storedValueForKey(PERS_ID_CREATEUR_KEY);
	}

	public void setPersIdCreateur(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CREATEUR_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOBail bail() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOBail)storedValueForKey(BAIL_KEY);
	}

	public void setBailRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOBail value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOBail oldValue = bail();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BAIL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, BAIL_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement hebergement() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement)storedValueForKey(HEBERGEMENT_KEY);
	}

	public void setHebergementRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement oldValue = hebergement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, HEBERGEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, HEBERGEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture ligneFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture)storedValueForKey(LIGNE_FACTURE_KEY);
	}

	public void setLigneFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture oldValue = ligneFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIGNE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LIGNE_FACTURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable objetReservable() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable)storedValueForKey(OBJET_RESERVABLE_KEY);
	}

	public void setObjetReservableRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOObjetReservable oldValue = objetReservable();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, OBJET_RESERVABLE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, OBJET_RESERVABLE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration restauration() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration)storedValueForKey(RESTAURATION_KEY);
	}

	public void setRestaurationRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EORestauration oldValue = restauration();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RESTAURATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, RESTAURATION_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul typeCalcul() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul)storedValueForKey(TYPE_CALCUL_KEY);
	}

	public void setTypeCalculRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul oldValue = typeCalcul();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CALCUL_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CALCUL_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif typeTarif() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif)storedValueForKey(TYPE_TARIF_KEY);
	}

	public void setTypeTarifRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif oldValue = typeTarif();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TARIF_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TARIF_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)storedValueForKey(LIGNE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier) {
	 return ligneFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, boolean fetch) {
	 return ligneFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.DETAIL_HEBERGEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ligneFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public void removeFromLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture createLigneFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture) eo;
	}
	
	public void deleteLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLigneFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> objects = ligneFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLigneFacturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)storedValueForKey(REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier) {
	 return repartDetailHebergementElementAssocies(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier, boolean fetch) {
	 return repartDetailHebergementElementAssocies(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.DETAIL_HEBERGEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = repartDetailHebergementElementAssocies();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}
	
	public void removeFromRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie createRepartDetailHebergementElementAssociesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie) eo;
	}
	
	public void deleteRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRepartDetailHebergementElementAssociesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> objects = repartDetailHebergementElementAssocies().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRepartDetailHebergementElementAssociesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)storedValueForKey(RESTAURATIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier) {
	 return restaurations(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, boolean fetch) {
	 return restaurations(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.DETAIL_HEBERGEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = restaurations();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public void removeFromRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration createRestaurationsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RESTAURATIONS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration) eo;
	}
	
	public void deleteRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRestaurationsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> objects = restaurations().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRestaurationsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EODetailHebergement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EODetailHebergement createEODetailHebergement(EOEditingContext editingContext																																																											) {
	 EODetailHebergement eo = (EODetailHebergement) EOUtilities.createAndInsertInstance(editingContext, _EODetailHebergement.ENTITY_NAME);	 
																																																									 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODetailHebergement creerInstance(EOEditingContext editingContext) {
		EODetailHebergement object = (EODetailHebergement)EOUtilities.createAndInsertInstance(editingContext, _EODetailHebergement.ENTITY_NAME);
  		return object;
		}

	

  public EODetailHebergement localInstanceIn(EOEditingContext editingContext) {
    EODetailHebergement localInstance = (EODetailHebergement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODetailHebergement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODetailHebergement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EODetailHebergement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODetailHebergement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODetailHebergement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODetailHebergement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODetailHebergement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EODetailHebergement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODetailHebergement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODetailHebergement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODetailHebergement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODetailHebergement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODetailHebergement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODetailHebergement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
