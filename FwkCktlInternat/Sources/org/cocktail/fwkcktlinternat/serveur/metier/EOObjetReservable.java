/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOObjetReservable extends _EOObjetReservable {
	private static Logger log = Logger.getLogger(EOObjetReservable.class);

	public static NSArray<EOObjetReservable> getObjetReservablesLibre(
			EOEditingContext editingContext) {
		EOObjetReservable.fetchAll(editingContext);
		EOFetchSpecification efs = new EOFetchSpecification();
		efs.setEntityName(EOObjetReservable.class.getSimpleName());
		String query = "SELECT t0.CODE_TYPE_OBJET, t0.ID_OBJET_HEBERGEMENT, t0.LIB_OBJET, t0.NB_PLACE, t0.RESERVABLE, t0.SAL_NUMERO FROM OBJET_RESERVABLE t0" +
				" minus " +
				"SELECT t0.CODE_TYPE_OBJET, t0.ID_OBJET_HEBERGEMENT, t0.LIB_OBJET, t0.NB_PLACE, t0.RESERVABLE, t0.SAL_NUMERO FROM OBJET_RESERVABLE t0 inner join DETAIL_HEBERGEMENT t1 on t1.ID_OBJET_HEBERGEMENT = t0.ID_OBJET_HEBERGEMENT";
		NSMutableDictionary nmd = new NSMutableDictionary();
		nmd.setObjectForKey(query,
				EODatabaseContext.CustomQueryExpressionHintKey);
		efs.setHints(nmd.immutableClone());
		NSArray<EOObjetReservable> listeObjetDisponibles = editingContext
				.objectsWithFetchSpecification(efs);

		return listeObjetDisponibles;
	}
}
