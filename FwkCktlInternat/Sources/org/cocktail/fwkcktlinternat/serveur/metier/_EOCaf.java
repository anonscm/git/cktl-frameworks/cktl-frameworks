/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOCaf.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOCaf extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOCaf.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Caf";
	public static final String ENTITY_TABLE_NAME = "LITCHI.CAF";


//Attribute Keys
	public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
	public static final ERXKey<Integer> MOIS = new ERXKey<Integer>("mois");
	public static final ERXKey<NSTimestamp> MOIS_RECEPTION = new ERXKey<NSTimestamp>("moisReception");
	public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
	public static final ERXKey<java.math.BigDecimal> MONTANT_AFFECTE = new ERXKey<java.math.BigDecimal>("montantAffecte");
	public static final ERXKey<java.math.BigDecimal> MONTANT_NEUTRALISE = new ERXKey<java.math.BigDecimal>("montantNeutralise");
	public static final ERXKey<String> NUMERO_ALLOCATAIRE = new ERXKey<String>("numeroAllocataire");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> AFFECTATION_CAFS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>("affectationCafs");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire> ETRE_ALLOCATAIRE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire>("etreAllocataire");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idCaf";

	public static final String ANNEE_KEY = "annee";
	public static final String MOIS_KEY = "mois";
	public static final String MOIS_RECEPTION_KEY = "moisReception";
	public static final String MONTANT_KEY = "montant";
	public static final String MONTANT_AFFECTE_KEY = "montantAffecte";
	public static final String MONTANT_NEUTRALISE_KEY = "montantNeutralise";
	public static final String NUMERO_ALLOCATAIRE_KEY = "numeroAllocataire";
	public static final String PERS_ID_KEY = "persId";

//Attributs non visibles
	public static final String ID_CAF_KEY = "idCaf";

//Colonnes dans la base de donnees
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String MOIS_COLKEY = "MOIS";
	public static final String MOIS_RECEPTION_COLKEY = "MOIS_RECEPTION";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String MONTANT_AFFECTE_COLKEY = "MONTANT_AFFECTE";
	public static final String MONTANT_NEUTRALISE_COLKEY = "MONTANT_NEUTRALISE";
	public static final String NUMERO_ALLOCATAIRE_COLKEY = "NUMERO_ALLOCATAIRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String ID_CAF_COLKEY = "ID_CAF";


	// Relationships
	public static final String AFFECTATION_CAFS_KEY = "affectationCafs";
	public static final String ETRE_ALLOCATAIRE_KEY = "etreAllocataire";



	// Accessors methods
	public Integer annee() {
	 return (Integer) storedValueForKey(ANNEE_KEY);
	}

	public void setAnnee(Integer value) {
	 takeStoredValueForKey(value, ANNEE_KEY);
	}

	public Integer mois() {
	 return (Integer) storedValueForKey(MOIS_KEY);
	}

	public void setMois(Integer value) {
	 takeStoredValueForKey(value, MOIS_KEY);
	}

	public NSTimestamp moisReception() {
	 return (NSTimestamp) storedValueForKey(MOIS_RECEPTION_KEY);
	}

	public void setMoisReception(NSTimestamp value) {
	 takeStoredValueForKey(value, MOIS_RECEPTION_KEY);
	}

	public java.math.BigDecimal montant() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
	}

	public void setMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_KEY);
	}

	public java.math.BigDecimal montantAffecte() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_AFFECTE_KEY);
	}

	public void setMontantAffecte(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_AFFECTE_KEY);
	}

	public java.math.BigDecimal montantNeutralise() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_NEUTRALISE_KEY);
	}

	public void setMontantNeutralise(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_NEUTRALISE_KEY);
	}

	public String numeroAllocataire() {
	 return (String) storedValueForKey(NUMERO_ALLOCATAIRE_KEY);
	}

	public void setNumeroAllocataire(String value) {
	 takeStoredValueForKey(value, NUMERO_ALLOCATAIRE_KEY);
	}

	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire etreAllocataire() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire)storedValueForKey(ETRE_ALLOCATAIRE_KEY);
	}

	public void setEtreAllocataireRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire oldValue = etreAllocataire();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ETRE_ALLOCATAIRE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ETRE_ALLOCATAIRE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)storedValueForKey(AFFECTATION_CAFS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier) {
	 return affectationCafs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier, boolean fetch) {
	 return affectationCafs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.CAF_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationCafs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
	}
	
	public void removeFromAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf createAffectationCafsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_CAFS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf) eo;
	}
	
	public void deleteAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationCafsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> objects = affectationCafs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationCafsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOCaf avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOCaf createEOCaf(EOEditingContext editingContext																							) {
	 EOCaf eo = (EOCaf) EOUtilities.createAndInsertInstance(editingContext, _EOCaf.ENTITY_NAME);	 
																					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCaf creerInstance(EOEditingContext editingContext) {
		EOCaf object = (EOCaf)EOUtilities.createAndInsertInstance(editingContext, _EOCaf.ENTITY_NAME);
  		return object;
		}

	

  public EOCaf localInstanceIn(EOEditingContext editingContext) {
    EOCaf localInstance = (EOCaf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOCaf>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCaf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCaf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCaf> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCaf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCaf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCaf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCaf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCaf> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCaf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCaf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCaf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCaf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCaf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCaf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
