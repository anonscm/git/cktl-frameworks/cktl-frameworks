/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOElementAssocie.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOElementAssocie extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOElementAssocie.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_ElementAssocie";
	public static final String ENTITY_TABLE_NAME = "LITCHI.ELEMENT_ASSOCIE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<Long> ID_TYPE_FACTURATION = new ERXKey<Long>("idTypeFacturation");
	public static final ERXKey<Long> ID_TYPE_OBJET = new ERXKey<Long>("idTypeObjet");
	public static final ERXKey<Long> ID_TYPE_PRODUIT = new ERXKey<Long>("idTypeProduit");
	public static final ERXKey<Long> ID_TYPE_REMBOURSEMENT = new ERXKey<Long>("idTypeRemboursement");
	public static final ERXKey<String> LIB_ELEMENT_ASSOCIE = new ERXKey<String>("libElementAssocie");
	public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
	public static final ERXKey<Long> TYPE_TVA_EA = new ERXKey<Long>("typeTvaEa");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>("repartDetailHebergementElementAssocies");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation> TYPE_FACTURATION = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation>("typeFacturation");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> TYPE_OBJET = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>("typeObjet");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> TYPE_PRODUIT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit>("typeProduit");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement> TYPE_REMBOURSEMENT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement>("typeRemboursement");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva> TYPE_TVA = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva>("typeTva");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "codeElementAssocie";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String ID_TYPE_FACTURATION_KEY = "idTypeFacturation";
	public static final String ID_TYPE_OBJET_KEY = "idTypeObjet";
	public static final String ID_TYPE_PRODUIT_KEY = "idTypeProduit";
	public static final String ID_TYPE_REMBOURSEMENT_KEY = "idTypeRemboursement";
	public static final String LIB_ELEMENT_ASSOCIE_KEY = "libElementAssocie";
	public static final String MONTANT_KEY = "montant";
	public static final String TYPE_TVA_EA_KEY = "typeTvaEa";

//Attributs non visibles
	public static final String CODE_ELEMENT_ASSOCIE_KEY = "codeElementAssocie";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "DATE_DEBUT";
	public static final String DATE_FIN_COLKEY = "DATE_FIN";
	public static final String ID_TYPE_FACTURATION_COLKEY = "ID_TYPE_FACTURATION";
	public static final String ID_TYPE_OBJET_COLKEY = "ID_TYPE_OBJET";
	public static final String ID_TYPE_PRODUIT_COLKEY = "ID_TYPE_PRODUIT";
	public static final String ID_TYPE_REMBOURSEMENT_COLKEY = "ID_TYPE_REMBOURSEMENT";
	public static final String LIB_ELEMENT_ASSOCIE_COLKEY = "LIB_ELEMENT_ASSOCIE";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String TYPE_TVA_EA_COLKEY = "TYPE_TVA_EA";

	public static final String CODE_ELEMENT_ASSOCIE_COLKEY = "CODE_ELEMENT_ASSOCIE";


	// Relationships
	public static final String REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY = "repartDetailHebergementElementAssocies";
	public static final String TYPE_FACTURATION_KEY = "typeFacturation";
	public static final String TYPE_OBJET_KEY = "typeObjet";
	public static final String TYPE_PRODUIT_KEY = "typeProduit";
	public static final String TYPE_REMBOURSEMENT_KEY = "typeRemboursement";
	public static final String TYPE_TVA_KEY = "typeTva";



	// Accessors methods
	public NSTimestamp dateDebut() {
	 return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
	}

	public void setDateDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_DEBUT_KEY);
	}

	public NSTimestamp dateFin() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
	}

	public void setDateFin(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_KEY);
	}

	public Long idTypeFacturation() {
	 return (Long) storedValueForKey(ID_TYPE_FACTURATION_KEY);
	}

	public void setIdTypeFacturation(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_FACTURATION_KEY);
	}

	public Long idTypeObjet() {
	 return (Long) storedValueForKey(ID_TYPE_OBJET_KEY);
	}

	public void setIdTypeObjet(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_OBJET_KEY);
	}

	public Long idTypeProduit() {
	 return (Long) storedValueForKey(ID_TYPE_PRODUIT_KEY);
	}

	public void setIdTypeProduit(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_PRODUIT_KEY);
	}

	public Long idTypeRemboursement() {
	 return (Long) storedValueForKey(ID_TYPE_REMBOURSEMENT_KEY);
	}

	public void setIdTypeRemboursement(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_REMBOURSEMENT_KEY);
	}

	public String libElementAssocie() {
	 return (String) storedValueForKey(LIB_ELEMENT_ASSOCIE_KEY);
	}

	public void setLibElementAssocie(String value) {
	 takeStoredValueForKey(value, LIB_ELEMENT_ASSOCIE_KEY);
	}

	public java.math.BigDecimal montant() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
	}

	public void setMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_KEY);
	}

	public Long typeTvaEa() {
	 return (Long) storedValueForKey(TYPE_TVA_EA_KEY);
	}

	public void setTypeTvaEa(Long value) {
	 takeStoredValueForKey(value, TYPE_TVA_EA_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation typeFacturation() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation)storedValueForKey(TYPE_FACTURATION_KEY);
	}

	public void setTypeFacturationRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacturation oldValue = typeFacturation();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_FACTURATION_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_FACTURATION_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet typeObjet() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet)storedValueForKey(TYPE_OBJET_KEY);
	}

	public void setTypeObjetRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet oldValue = typeObjet();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_OBJET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_OBJET_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit typeProduit() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit)storedValueForKey(TYPE_PRODUIT_KEY);
	}

	public void setTypeProduitRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit oldValue = typeProduit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PRODUIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PRODUIT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement typeRemboursement() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement)storedValueForKey(TYPE_REMBOURSEMENT_KEY);
	}

	public void setTypeRemboursementRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeRemboursement oldValue = typeRemboursement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_REMBOURSEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_REMBOURSEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva typeTva() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva)storedValueForKey(TYPE_TVA_KEY);
	}

	public void setTypeTvaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva oldValue = typeTva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TVA_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)storedValueForKey(REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier) {
	 return repartDetailHebergementElementAssocies(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier, boolean fetch) {
	 return repartDetailHebergementElementAssocies(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> repartDetailHebergementElementAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.ELEMENT_ASSOCIE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = repartDetailHebergementElementAssocies();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}
	
	public void removeFromRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie createRepartDetailHebergementElementAssociesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie) eo;
	}
	
	public void deleteRepartDetailHebergementElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRepartDetailHebergementElementAssociesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> objects = repartDetailHebergementElementAssocies().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRepartDetailHebergementElementAssociesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOElementAssocie avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOElementAssocie createEOElementAssocie(EOEditingContext editingContext																																	) {
	 EOElementAssocie eo = (EOElementAssocie) EOUtilities.createAndInsertInstance(editingContext, _EOElementAssocie.ENTITY_NAME);	 
																															 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOElementAssocie creerInstance(EOEditingContext editingContext) {
		EOElementAssocie object = (EOElementAssocie)EOUtilities.createAndInsertInstance(editingContext, _EOElementAssocie.ENTITY_NAME);
  		return object;
		}

	

  public EOElementAssocie localInstanceIn(EOEditingContext editingContext) {
    EOElementAssocie localInstance = (EOElementAssocie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOElementAssocie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOElementAssocie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOElementAssocie> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOElementAssocie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOElementAssocie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOElementAssocie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOElementAssocie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOElementAssocie> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOElementAssocie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOElementAssocie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOElementAssocie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOElementAssocie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOElementAssocie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOElementAssocie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
