/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOAffectationArrhes.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOAffectationArrhes extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOAffectationArrhes.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_AffectationArrhes";
	public static final String ENTITY_TABLE_NAME = "LITCHI.AFFECTATION_ARRHES";


//Attribute Keys
	public static final ERXKey<Long> ID_FACTURE = new ERXKey<Long>("idFacture");
	public static final ERXKey<Long> ID_RDHEA = new ERXKey<Long>("idRdhea");
	public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFacture");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>("repartDetailHebergementElementAssocie");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idAffectationArrhes";

	public static final String ID_FACTURE_KEY = "idFacture";
	public static final String ID_RDHEA_KEY = "idRdhea";
	public static final String MONTANT_KEY = "montant";

//Attributs non visibles
	public static final String ID_AFFECTATION_ARRHES_KEY = "idAffectationArrhes";

//Colonnes dans la base de donnees
	public static final String ID_FACTURE_COLKEY = "ID_FACTURE";
	public static final String ID_RDHEA_COLKEY = "ID_RDHEA";
	public static final String MONTANT_COLKEY = "MONTANT";

	public static final String ID_AFFECTATION_ARRHES_COLKEY = "ID_AFFECTATION_ARRHES";


	// Relationships
	public static final String ENTETE_FACTURE_KEY = "enteteFacture";
	public static final String REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY = "repartDetailHebergementElementAssocie";



	// Accessors methods
	public Long idFacture() {
	 return (Long) storedValueForKey(ID_FACTURE_KEY);
	}

	public void setIdFacture(Long value) {
	 takeStoredValueForKey(value, ID_FACTURE_KEY);
	}

	public Long idRdhea() {
	 return (Long) storedValueForKey(ID_RDHEA_KEY);
	}

	public void setIdRdhea(Long value) {
	 takeStoredValueForKey(value, ID_RDHEA_KEY);
	}

	public java.math.BigDecimal montant() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
	}

	public void setMontant(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture enteteFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture)storedValueForKey(ENTETE_FACTURE_KEY);
	}

	public void setEnteteFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture oldValue = enteteFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENTETE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENTETE_FACTURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie repartDetailHebergementElementAssocie() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie)storedValueForKey(REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	}

	public void setRepartDetailHebergementElementAssocieRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie oldValue = repartDetailHebergementElementAssocie();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY);
	 }
	}


	/**
	* Créer une instance de EOAffectationArrhes avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOAffectationArrhes createEOAffectationArrhes(EOEditingContext editingContext															) {
	 EOAffectationArrhes eo = (EOAffectationArrhes) EOUtilities.createAndInsertInstance(editingContext, _EOAffectationArrhes.ENTITY_NAME);	 
													 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAffectationArrhes creerInstance(EOEditingContext editingContext) {
		EOAffectationArrhes object = (EOAffectationArrhes)EOUtilities.createAndInsertInstance(editingContext, _EOAffectationArrhes.ENTITY_NAME);
  		return object;
		}

	

  public EOAffectationArrhes localInstanceIn(EOEditingContext editingContext) {
    EOAffectationArrhes localInstance = (EOAffectationArrhes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAffectationArrhes fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAffectationArrhes fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOAffectationArrhes> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAffectationArrhes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAffectationArrhes)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAffectationArrhes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAffectationArrhes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOAffectationArrhes> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAffectationArrhes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAffectationArrhes)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAffectationArrhes fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAffectationArrhes eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAffectationArrhes ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAffectationArrhes fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
