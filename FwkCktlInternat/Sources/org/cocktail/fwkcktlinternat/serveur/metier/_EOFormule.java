/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOFormule.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOFormule extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOFormule.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Formule";
	public static final String ENTITY_TABLE_NAME = "LITCHI.FORMULE";


//Attribute Keys
	public static final ERXKey<String> CODE_TARIF = new ERXKey<String>("codeTarif");
	public static final ERXKey<String> DESCRIPTION = new ERXKey<String>("description");
	public static final ERXKey<Long> ID_TYPE_PRODUIT = new ERXKey<Long>("idTypeProduit");
	public static final ERXKey<String> LIB_FORMULE = new ERXKey<String>("libFormule");
	public static final ERXKey<Long> TYPE_TVA_REST = new ERXKey<Long>("typeTvaRest");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> QUI_DORT_DINES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine>("quiDortDines");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> RESTAURATIONS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>("restaurations");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> TARIFS_FORMULES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule>("tarifsFormules");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> TYPE_PRODUIT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit>("typeProduit");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva> TYPE_TVA = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva>("typeTva");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idFormule";

	public static final String CODE_TARIF_KEY = "codeTarif";
	public static final String DESCRIPTION_KEY = "description";
	public static final String ID_TYPE_PRODUIT_KEY = "idTypeProduit";
	public static final String LIB_FORMULE_KEY = "libFormule";
	public static final String TYPE_TVA_REST_KEY = "typeTvaRest";

//Attributs non visibles
	public static final String ID_FORMULE_KEY = "idFormule";

//Colonnes dans la base de donnees
	public static final String CODE_TARIF_COLKEY = "CODE_TARIF";
	public static final String DESCRIPTION_COLKEY = "DESCRIPTION";
	public static final String ID_TYPE_PRODUIT_COLKEY = "ID_TYPE_PRODUIT";
	public static final String LIB_FORMULE_COLKEY = "LIB_FORMULE";
	public static final String TYPE_TVA_REST_COLKEY = "TYPE_TVA_REST";

	public static final String ID_FORMULE_COLKEY = "ID_FORMULE";


	// Relationships
	public static final String QUI_DORT_DINES_KEY = "quiDortDines";
	public static final String RESTAURATIONS_KEY = "restaurations";
	public static final String TARIFS_FORMULES_KEY = "tarifsFormules";
	public static final String TYPE_PRODUIT_KEY = "typeProduit";
	public static final String TYPE_TVA_KEY = "typeTva";



	// Accessors methods
	public String codeTarif() {
	 return (String) storedValueForKey(CODE_TARIF_KEY);
	}

	public void setCodeTarif(String value) {
	 takeStoredValueForKey(value, CODE_TARIF_KEY);
	}

	public String description() {
	 return (String) storedValueForKey(DESCRIPTION_KEY);
	}

	public void setDescription(String value) {
	 takeStoredValueForKey(value, DESCRIPTION_KEY);
	}

	public Long idTypeProduit() {
	 return (Long) storedValueForKey(ID_TYPE_PRODUIT_KEY);
	}

	public void setIdTypeProduit(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_PRODUIT_KEY);
	}

	public String libFormule() {
	 return (String) storedValueForKey(LIB_FORMULE_KEY);
	}

	public void setLibFormule(String value) {
	 takeStoredValueForKey(value, LIB_FORMULE_KEY);
	}

	public Long typeTvaRest() {
	 return (Long) storedValueForKey(TYPE_TVA_REST_KEY);
	}

	public void setTypeTvaRest(Long value) {
	 takeStoredValueForKey(value, TYPE_TVA_REST_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit typeProduit() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit)storedValueForKey(TYPE_PRODUIT_KEY);
	}

	public void setTypeProduitRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit oldValue = typeProduit();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PRODUIT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PRODUIT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva typeTva() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva)storedValueForKey(TYPE_TVA_KEY);
	}

	public void setTypeTvaRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva oldValue = typeTva();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TVA_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TVA_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> quiDortDines() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine>)storedValueForKey(QUI_DORT_DINES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> quiDortDines(EOQualifier qualifier) {
	 return quiDortDines(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> quiDortDines(EOQualifier qualifier, boolean fetch) {
	 return quiDortDines(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> quiDortDines(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine.FORMULE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = quiDortDines();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToQuiDortDinesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, QUI_DORT_DINES_KEY);
	}
	
	public void removeFromQuiDortDinesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, QUI_DORT_DINES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine createQuiDortDinesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, QUI_DORT_DINES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine) eo;
	}
	
	public void deleteQuiDortDinesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, QUI_DORT_DINES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllQuiDortDinesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine> objects = quiDortDines().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteQuiDortDinesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)storedValueForKey(RESTAURATIONS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier) {
	 return restaurations(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, boolean fetch) {
	 return restaurations(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> restaurations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.FORMULE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = restaurations();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public void removeFromRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORestauration createRestaurationsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RESTAURATIONS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORestauration) eo;
	}
	
	public void deleteRestaurationsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORestauration object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RESTAURATIONS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRestaurationsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORestauration> objects = restaurations().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRestaurationsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> tarifsFormules() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule>)storedValueForKey(TARIFS_FORMULES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> tarifsFormules(EOQualifier qualifier) {
	 return tarifsFormules(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> tarifsFormules(EOQualifier qualifier, boolean fetch) {
	 return tarifsFormules(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> tarifsFormules(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule.FORMULE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = tarifsFormules();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToTarifsFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TARIFS_FORMULES_KEY);
	}
	
	public void removeFromTarifsFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TARIFS_FORMULES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule createTarifsFormulesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TARIFS_FORMULES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule) eo;
	}
	
	public void deleteTarifsFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TARIFS_FORMULES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllTarifsFormulesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> objects = tarifsFormules().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteTarifsFormulesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOFormule avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOFormule createEOFormule(EOEditingContext editingContext																			) {
	 EOFormule eo = (EOFormule) EOUtilities.createAndInsertInstance(editingContext, _EOFormule.ENTITY_NAME);	 
																	 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFormule creerInstance(EOEditingContext editingContext) {
		EOFormule object = (EOFormule)EOUtilities.createAndInsertInstance(editingContext, _EOFormule.ENTITY_NAME);
  		return object;
		}

	

  public EOFormule localInstanceIn(EOEditingContext editingContext) {
    EOFormule localInstance = (EOFormule)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFormule fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFormule fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFormule> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFormule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFormule)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFormule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFormule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFormule> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFormule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFormule)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFormule fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFormule eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFormule ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFormule fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
