/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EODroit.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EODroit extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EODroit.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Droit";
	public static final String ENTITY_TABLE_NAME = "LITCHI.DROIT";


//Attribute Keys
	public static final ERXKey<String> DROIT_COMMENT = new ERXKey<String>("droitComment");
	public static final ERXKey<String> DROIT_KEY = new ERXKey<String>("droitKey");
	public static final ERXKey<String> DROIT_VALUE = new ERXKey<String>("droitValue");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> GROUPES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe>("groupes");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> UTILISATEURS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur>("utilisateurs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idDroit";

	public static final String DROIT_COMMENT_KEY = "droitComment";
	public static final String DROIT_KEY_KEY = "droitKey";
	public static final String DROIT_VALUE_KEY = "droitValue";

//Attributs non visibles
	public static final String ID_DROIT_KEY = "idDroit";

//Colonnes dans la base de donnees
	public static final String DROIT_COMMENT_COLKEY = "DROIT_COMMENT";
	public static final String DROIT_KEY_COLKEY = "DROIT_KEY";
	public static final String DROIT_VALUE_COLKEY = "DROIT_VALUE";

	public static final String ID_DROIT_COLKEY = "ID_DROIT";


	// Relationships
	public static final String GROUPES_KEY = "groupes";
	public static final String UTILISATEURS_KEY = "utilisateurs";



	// Accessors methods
	public String droitComment() {
	 return (String) storedValueForKey(DROIT_COMMENT_KEY);
	}

	public void setDroitComment(String value) {
	 takeStoredValueForKey(value, DROIT_COMMENT_KEY);
	}

	public String droitKey() {
	 return (String) storedValueForKey(DROIT_KEY_KEY);
	}

	public void setDroitKey(String value) {
	 takeStoredValueForKey(value, DROIT_KEY_KEY);
	}

	public String droitValue() {
	 return (String) storedValueForKey(DROIT_VALUE_KEY);
	}

	public void setDroitValue(String value) {
	 takeStoredValueForKey(value, DROIT_VALUE_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> groupes() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe>)storedValueForKey(GROUPES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> groupes(EOQualifier qualifier) {
	 return groupes(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> groupes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> results;
			   results = groupes();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToGroupesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, GROUPES_KEY);
	}
	
	public void removeFromGroupesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, GROUPES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe createGroupesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, GROUPES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe) eo;
	}
	
	public void deleteGroupesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, GROUPES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllGroupesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOGroupe> objects = groupes().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteGroupesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> utilisateurs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur>)storedValueForKey(UTILISATEURS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> utilisateurs(EOQualifier qualifier) {
	 return utilisateurs(qualifier, null);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> utilisateurs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> results;
			   results = utilisateurs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 return results;
	}
	
	public void addToUtilisateursRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEURS_KEY);
	}
	
	public void removeFromUtilisateursRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEURS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur createUtilisateursRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEURS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur) eo;
	}
	
	public void deleteUtilisateursRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEURS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllUtilisateursRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOUtilisateur> objects = utilisateurs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteUtilisateursRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EODroit avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EODroit createEODroit(EOEditingContext editingContext						, String droitKey
							, String droitValue
								) {
	 EODroit eo = (EODroit) EOUtilities.createAndInsertInstance(editingContext, _EODroit.ENTITY_NAME);	 
									eo.setDroitKey(droitKey);
									eo.setDroitValue(droitValue);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODroit creerInstance(EOEditingContext editingContext) {
		EODroit object = (EODroit)EOUtilities.createAndInsertInstance(editingContext, _EODroit.ENTITY_NAME);
  		return object;
		}

	

  public EODroit localInstanceIn(EOEditingContext editingContext) {
    EODroit localInstance = (EODroit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODroit>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODroit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODroit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EODroit> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODroit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EODroit> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODroit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODroit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODroit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODroit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODroit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
