/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOLignesReglement.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOLignesReglement extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOLignesReglement.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_LignesReglement";
	public static final String ENTITY_TABLE_NAME = "LITCHI.LIGNES_REGLEMENT";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_NEUTRALISE = new ERXKey<NSTimestamp>("dateNeutralise");
	public static final ERXKey<NSTimestamp> DATE_REGLEMENT_LIGNE = new ERXKey<NSTimestamp>("dateReglementLigne");
	public static final ERXKey<Long> ID_FACTURE = new ERXKey<Long>("idFacture");
	public static final ERXKey<Long> ID_REGLEMENT = new ERXKey<Long>("idReglement");
	public static final ERXKey<java.math.BigDecimal> MONTANT_NEUTRALISE = new ERXKey<java.math.BigDecimal>("montantNeutralise");
	public static final ERXKey<java.math.BigDecimal> MONTANT_TTC = new ERXKey<java.math.BigDecimal>("montantTtc");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> AFFECTATION_AVANCESES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>("affectationAvanceses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFacture");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements> REGLEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOReglements>("reglements");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idLigneReglement";

	public static final String DATE_NEUTRALISE_KEY = "dateNeutralise";
	public static final String DATE_REGLEMENT_LIGNE_KEY = "dateReglementLigne";
	public static final String ID_FACTURE_KEY = "idFacture";
	public static final String ID_REGLEMENT_KEY = "idReglement";
	public static final String MONTANT_NEUTRALISE_KEY = "montantNeutralise";
	public static final String MONTANT_TTC_KEY = "montantTtc";

//Attributs non visibles
	public static final String ID_LIGNE_REGLEMENT_KEY = "idLigneReglement";

//Colonnes dans la base de donnees
	public static final String DATE_NEUTRALISE_COLKEY = "DATE_NEUTRALISE";
	public static final String DATE_REGLEMENT_LIGNE_COLKEY = "DATE_REGLEMENT_LIGNE";
	public static final String ID_FACTURE_COLKEY = "ID_FACTURE";
	public static final String ID_REGLEMENT_COLKEY = "ID_REGLEMENT";
	public static final String MONTANT_NEUTRALISE_COLKEY = "MONTANT_NEUTRALISE";
	public static final String MONTANT_TTC_COLKEY = "MONTANT_TTC";

	public static final String ID_LIGNE_REGLEMENT_COLKEY = "ID_LIGNE_REGLEMENT";


	// Relationships
	public static final String AFFECTATION_AVANCESES_KEY = "affectationAvanceses";
	public static final String ENTETE_FACTURE_KEY = "enteteFacture";
	public static final String REGLEMENTS_KEY = "reglements";



	// Accessors methods
	public NSTimestamp dateNeutralise() {
	 return (NSTimestamp) storedValueForKey(DATE_NEUTRALISE_KEY);
	}

	public void setDateNeutralise(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_NEUTRALISE_KEY);
	}

	public NSTimestamp dateReglementLigne() {
	 return (NSTimestamp) storedValueForKey(DATE_REGLEMENT_LIGNE_KEY);
	}

	public void setDateReglementLigne(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_REGLEMENT_LIGNE_KEY);
	}

	public Long idFacture() {
	 return (Long) storedValueForKey(ID_FACTURE_KEY);
	}

	public void setIdFacture(Long value) {
	 takeStoredValueForKey(value, ID_FACTURE_KEY);
	}

	public Long idReglement() {
	 return (Long) storedValueForKey(ID_REGLEMENT_KEY);
	}

	public void setIdReglement(Long value) {
	 takeStoredValueForKey(value, ID_REGLEMENT_KEY);
	}

	public java.math.BigDecimal montantNeutralise() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_NEUTRALISE_KEY);
	}

	public void setMontantNeutralise(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_NEUTRALISE_KEY);
	}

	public java.math.BigDecimal montantTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_TTC_KEY);
	}

	public void setMontantTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_TTC_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture enteteFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture)storedValueForKey(ENTETE_FACTURE_KEY);
	}

	public void setEnteteFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture oldValue = enteteFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENTETE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENTETE_FACTURE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOReglements reglements() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOReglements)storedValueForKey(REGLEMENTS_KEY);
	}

	public void setReglementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOReglements value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOReglements oldValue = reglements();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REGLEMENTS_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, REGLEMENTS_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)storedValueForKey(AFFECTATION_AVANCESES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier) {
	 return affectationAvanceses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier, boolean fetch) {
	 return affectationAvanceses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.LIGNES_REGLEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationAvanceses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
	}
	
	public void removeFromAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances createAffectationAvancesesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_AVANCESES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances) eo;
	}
	
	public void deleteAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationAvancesesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> objects = affectationAvanceses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationAvancesesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOLignesReglement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOLignesReglement createEOLignesReglement(EOEditingContext editingContext																					) {
	 EOLignesReglement eo = (EOLignesReglement) EOUtilities.createAndInsertInstance(editingContext, _EOLignesReglement.ENTITY_NAME);	 
																			 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLignesReglement creerInstance(EOEditingContext editingContext) {
		EOLignesReglement object = (EOLignesReglement)EOUtilities.createAndInsertInstance(editingContext, _EOLignesReglement.ENTITY_NAME);
  		return object;
		}

	

  public EOLignesReglement localInstanceIn(EOEditingContext editingContext) {
    EOLignesReglement localInstance = (EOLignesReglement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLignesReglement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLignesReglement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOLignesReglement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLignesReglement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLignesReglement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLignesReglement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLignesReglement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOLignesReglement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLignesReglement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLignesReglement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLignesReglement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLignesReglement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLignesReglement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLignesReglement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
