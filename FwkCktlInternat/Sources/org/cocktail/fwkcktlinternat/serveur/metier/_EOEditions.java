/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEditions.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOEditions extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOEditions.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Editions";
	public static final String ENTITY_TABLE_NAME = "LITCHI.EDITIONS";


//Attribute Keys
	public static final ERXKey<Boolean> EN_MASSE = new ERXKey<Boolean>("enMasse");
	public static final ERXKey<Boolean> EST_COCHE = new ERXKey<Boolean>("estCoche");
	public static final ERXKey<Long> ID_TYPE_OBJET = new ERXKey<Long>("idTypeObjet");
	public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
	public static final ERXKey<String> NOM_FICHIER = new ERXKey<String>("nomFichier");
	public static final ERXKey<String> TAG = new ERXKey<String>("tag");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet> TYPE_OBJET = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet>("typeObjet");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String EN_MASSE_KEY = "enMasse";
	public static final String EST_COCHE_KEY = "estCoche";
	public static final String ID_TYPE_OBJET_KEY = "idTypeObjet";
	public static final String LIBELLE_KEY = "libelle";
	public static final String NOM_FICHIER_KEY = "nomFichier";
	public static final String TAG_KEY = "tag";

//Attributs non visibles
	public static final String ID_KEY = "id";

//Colonnes dans la base de donnees
	public static final String EN_MASSE_COLKEY = "EN_MASSE";
	public static final String EST_COCHE_COLKEY = "EST_COCHE";
	public static final String ID_TYPE_OBJET_COLKEY = "ID_TYPE_OBJET";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String NOM_FICHIER_COLKEY = "NOM_FICHIER";
	public static final String TAG_COLKEY = "TAG";

	public static final String ID_COLKEY = "ID";


	// Relationships
	public static final String TYPE_OBJET_KEY = "typeObjet";



	// Accessors methods
	public Boolean enMasse() {
	 return (Boolean) storedValueForKey(EN_MASSE_KEY);
	}

	public void setEnMasse(Boolean value) {
	 takeStoredValueForKey(value, EN_MASSE_KEY);
	}

	public Boolean estCoche() {
	 return (Boolean) storedValueForKey(EST_COCHE_KEY);
	}

	public void setEstCoche(Boolean value) {
	 takeStoredValueForKey(value, EST_COCHE_KEY);
	}

	public Long idTypeObjet() {
	 return (Long) storedValueForKey(ID_TYPE_OBJET_KEY);
	}

	public void setIdTypeObjet(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_OBJET_KEY);
	}

	public String libelle() {
	 return (String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(String value) {
	 takeStoredValueForKey(value, LIBELLE_KEY);
	}

	public String nomFichier() {
	 return (String) storedValueForKey(NOM_FICHIER_KEY);
	}

	public void setNomFichier(String value) {
	 takeStoredValueForKey(value, NOM_FICHIER_KEY);
	}

	public String tag() {
	 return (String) storedValueForKey(TAG_KEY);
	}

	public void setTag(String value) {
	 takeStoredValueForKey(value, TAG_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet typeObjet() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet)storedValueForKey(TYPE_OBJET_KEY);
	}

	public void setTypeObjetRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet oldValue = typeObjet();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_OBJET_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_OBJET_KEY);
	 }
	}


	/**
	* Créer une instance de EOEditions avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEditions createEOEditions(EOEditingContext editingContext																			) {
	 EOEditions eo = (EOEditions) EOUtilities.createAndInsertInstance(editingContext, _EOEditions.ENTITY_NAME);	 
																	 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEditions creerInstance(EOEditingContext editingContext) {
		EOEditions object = (EOEditions)EOUtilities.createAndInsertInstance(editingContext, _EOEditions.ENTITY_NAME);
  		return object;
		}

	

  public EOEditions localInstanceIn(EOEditingContext editingContext) {
    EOEditions localInstance = (EOEditions)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEditions>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEditions fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEditions fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEditions> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEditions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEditions)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEditions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEditions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEditions> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEditions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEditions)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEditions fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEditions eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEditions ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEditions fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
