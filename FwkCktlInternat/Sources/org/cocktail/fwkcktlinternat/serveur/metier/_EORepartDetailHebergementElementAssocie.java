/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EORepartDetailHebergementElementAssocie.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EORepartDetailHebergementElementAssocie extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EORepartDetailHebergementElementAssocie.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_RepartDetailHebergementElementAssocie";
	public static final String ENTITY_TABLE_NAME = "LITCHI.REPART_DET_HEB_ELE_ASS";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_REMBOURSEMENT = new ERXKey<NSTimestamp>("dateRemboursement");
	public static final ERXKey<Long> ID_DETAIL_HEBERGEMENT = new ERXKey<Long>("idDetailHebergement");
	public static final ERXKey<Long> ID_LIGNE_FACTURE = new ERXKey<Long>("idLigneFacture");
	public static final ERXKey<java.math.BigDecimal> MONTANT_REMBOURSE = new ERXKey<java.math.BigDecimal>("montantRembourse");
	public static final ERXKey<String> REMARQUE = new ERXKey<String>("remarque");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> AFFECTATION_ARRHESES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>("affectationArrheses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENT = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergement");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> ELEMENT_ASSOCIE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>("elementAssocie");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFacture");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURES_ASSOCIES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFacturesAssocies");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idRepartDetHebEa";

	public static final String DATE_REMBOURSEMENT_KEY = "dateRemboursement";
	public static final String ID_DETAIL_HEBERGEMENT_KEY = "idDetailHebergement";
	public static final String ID_LIGNE_FACTURE_KEY = "idLigneFacture";
	public static final String MONTANT_REMBOURSE_KEY = "montantRembourse";
	public static final String REMARQUE_KEY = "remarque";

//Attributs non visibles
	public static final String CODE_ELEMENT_ASSOCIE_KEY = "codeElementAssocie";
	public static final String ID_REPART_DET_HEB_EA_KEY = "idRepartDetHebEa";

//Colonnes dans la base de donnees
	public static final String DATE_REMBOURSEMENT_COLKEY = "DATE_REMBOURSEMENT";
	public static final String ID_DETAIL_HEBERGEMENT_COLKEY = "ID_DETAIL_HEBERGEMENT";
	public static final String ID_LIGNE_FACTURE_COLKEY = "ID_LIGNE_FACTURE";
	public static final String MONTANT_REMBOURSE_COLKEY = "MONTANT_REMBOURSE";
	public static final String REMARQUE_COLKEY = "REMARQUE";

	public static final String CODE_ELEMENT_ASSOCIE_COLKEY = "ID_ELEMENT_ASSOCIE";
	public static final String ID_REPART_DET_HEB_EA_COLKEY = "ID_REPART_DET_HEB_EA";


	// Relationships
	public static final String AFFECTATION_ARRHESES_KEY = "affectationArrheses";
	public static final String DETAIL_HEBERGEMENT_KEY = "detailHebergement";
	public static final String ELEMENT_ASSOCIE_KEY = "elementAssocie";
	public static final String LIGNE_FACTURE_KEY = "ligneFacture";
	public static final String LIGNE_FACTURES_ASSOCIES_KEY = "ligneFacturesAssocies";



	// Accessors methods
	public NSTimestamp dateRemboursement() {
	 return (NSTimestamp) storedValueForKey(DATE_REMBOURSEMENT_KEY);
	}

	public void setDateRemboursement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_REMBOURSEMENT_KEY);
	}

	public Long idDetailHebergement() {
	 return (Long) storedValueForKey(ID_DETAIL_HEBERGEMENT_KEY);
	}

	public void setIdDetailHebergement(Long value) {
	 takeStoredValueForKey(value, ID_DETAIL_HEBERGEMENT_KEY);
	}

	public Long idLigneFacture() {
	 return (Long) storedValueForKey(ID_LIGNE_FACTURE_KEY);
	}

	public void setIdLigneFacture(Long value) {
	 takeStoredValueForKey(value, ID_LIGNE_FACTURE_KEY);
	}

	public java.math.BigDecimal montantRembourse() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_REMBOURSE_KEY);
	}

	public void setMontantRembourse(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_REMBOURSE_KEY);
	}

	public String remarque() {
	 return (String) storedValueForKey(REMARQUE_KEY);
	}

	public void setRemarque(String value) {
	 takeStoredValueForKey(value, REMARQUE_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement detailHebergement() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement)storedValueForKey(DETAIL_HEBERGEMENT_KEY);
	}

	public void setDetailHebergementRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement oldValue = detailHebergement();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DETAIL_HEBERGEMENT_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, DETAIL_HEBERGEMENT_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie elementAssocie() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie)storedValueForKey(ELEMENT_ASSOCIE_KEY);
	}

	public void setElementAssocieRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie oldValue = elementAssocie();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ELEMENT_ASSOCIE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ELEMENT_ASSOCIE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture ligneFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture)storedValueForKey(LIGNE_FACTURE_KEY);
	}

	public void setLigneFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture oldValue = ligneFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIGNE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, LIGNE_FACTURE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)storedValueForKey(AFFECTATION_ARRHESES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier) {
	 return affectationArrheses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier, boolean fetch) {
	 return affectationArrheses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationArrheses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
	}
	
	public void removeFromAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes createAffectationArrhesesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_ARRHESES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes) eo;
	}
	
	public void deleteAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationArrhesesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> objects = affectationArrheses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationArrhesesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFacturesAssocies() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)storedValueForKey(LIGNE_FACTURES_ASSOCIES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFacturesAssocies(EOQualifier qualifier) {
	 return ligneFacturesAssocies(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFacturesAssocies(EOQualifier qualifier, boolean fetch) {
	 return ligneFacturesAssocies(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFacturesAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.REPART_DETAIL_HEBERGEMENT_ELEMENT_ASSOCIE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ligneFacturesAssocies();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLigneFacturesAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_ASSOCIES_KEY);
	}
	
	public void removeFromLigneFacturesAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_ASSOCIES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture createLigneFacturesAssociesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNE_FACTURES_ASSOCIES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture) eo;
	}
	
	public void deleteLigneFacturesAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_ASSOCIES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLigneFacturesAssociesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> objects = ligneFacturesAssocies().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLigneFacturesAssociesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EORepartDetailHebergementElementAssocie avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EORepartDetailHebergementElementAssocie createEORepartDetailHebergementElementAssocie(EOEditingContext editingContext														, org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie elementAssocie							) {
	 EORepartDetailHebergementElementAssocie eo = (EORepartDetailHebergementElementAssocie) EOUtilities.createAndInsertInstance(editingContext, _EORepartDetailHebergementElementAssocie.ENTITY_NAME);	 
																 eo.setElementAssocieRelationship(elementAssocie);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartDetailHebergementElementAssocie creerInstance(EOEditingContext editingContext) {
		EORepartDetailHebergementElementAssocie object = (EORepartDetailHebergementElementAssocie)EOUtilities.createAndInsertInstance(editingContext, _EORepartDetailHebergementElementAssocie.ENTITY_NAME);
  		return object;
		}

	

  public EORepartDetailHebergementElementAssocie localInstanceIn(EOEditingContext editingContext) {
    EORepartDetailHebergementElementAssocie localInstance = (EORepartDetailHebergementElementAssocie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartDetailHebergementElementAssocie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartDetailHebergementElementAssocie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORepartDetailHebergementElementAssocie> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartDetailHebergementElementAssocie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartDetailHebergementElementAssocie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartDetailHebergementElementAssocie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartDetailHebergementElementAssocie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORepartDetailHebergementElementAssocie> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartDetailHebergementElementAssocie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartDetailHebergementElementAssocie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartDetailHebergementElementAssocie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartDetailHebergementElementAssocie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartDetailHebergementElementAssocie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartDetailHebergementElementAssocie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
