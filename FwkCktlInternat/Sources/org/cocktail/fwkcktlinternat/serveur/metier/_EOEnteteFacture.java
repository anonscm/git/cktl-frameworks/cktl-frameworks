/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOEnteteFacture.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOEnteteFacture extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOEnteteFacture.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_EnteteFacture";
	public static final String ENTITY_TABLE_NAME = "LITCHI.ENTETE_FACTURE";


//Attribute Keys
	public static final ERXKey<String> ADRESSE1 = new ERXKey<String>("adresse1");
	public static final ERXKey<String> ADRESSE2 = new ERXKey<String>("adresse2");
	public static final ERXKey<String> CIVILITE = new ERXKey<String>("civilite");
	public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
	public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
	public static final ERXKey<NSTimestamp> DATE_ECHEANCE = new ERXKey<NSTimestamp>("dateEcheance");
	public static final ERXKey<NSTimestamp> DATE_MODIFICATION_NOTE = new ERXKey<NSTimestamp>("dateModificationNote");
	public static final ERXKey<NSTimestamp> DATE_PRELEVEMENT = new ERXKey<NSTimestamp>("datePrelevement");
	public static final ERXKey<NSTimestamp> DATE_REMBOURSEMENT = new ERXKey<NSTimestamp>("dateRemboursement");
	public static final ERXKey<NSTimestamp> DATE_VALIDATION = new ERXKey<NSTimestamp>("dateValidation");
	public static final ERXKey<Long> ID_FACTURE = new ERXKey<Long>("idFacture");
	public static final ERXKey<Long> ID_FACTURE_ORIGINE = new ERXKey<Long>("idFactureOrigine");
	public static final ERXKey<Long> ID_TYPE_FACTURE = new ERXKey<Long>("idTypeFacture");
	public static final ERXKey<Boolean> INDICATION_ABANDON = new ERXKey<Boolean>("indicationAbandon");
	public static final ERXKey<Boolean> INDICATION_PRELEVEMENT = new ERXKey<Boolean>("indicationPrelevement");
	public static final ERXKey<String> LIB_FACTURE = new ERXKey<String>("libFacture");
	public static final ERXKey<java.math.BigDecimal> MNT_A_PAYER_HT = new ERXKey<java.math.BigDecimal>("mntAPayerHt");
	public static final ERXKey<java.math.BigDecimal> MNT_A_PAYER_TTC = new ERXKey<java.math.BigDecimal>("mntAPayerTtc");
	public static final ERXKey<java.math.BigDecimal> MNT_A_PAYER_TVA = new ERXKey<java.math.BigDecimal>("mntAPayerTva");
	public static final ERXKey<java.math.BigDecimal> MNT_FACTURE_HT = new ERXKey<java.math.BigDecimal>("mntFactureHt");
	public static final ERXKey<java.math.BigDecimal> MNT_FACTURE_TTC = new ERXKey<java.math.BigDecimal>("mntFactureTtc");
	public static final ERXKey<java.math.BigDecimal> MNT_FACTURE_TVA = new ERXKey<java.math.BigDecimal>("mntFactureTva");
	public static final ERXKey<String> NOM_PERSONNE = new ERXKey<String>("nomPersonne");
	public static final ERXKey<String> NOTE = new ERXKey<String>("note");
	public static final ERXKey<String> NOTE_PUBLIC = new ERXKey<String>("notePublic");
	public static final ERXKey<String> NUM_COMMANDE = new ERXKey<String>("numCommande");
	public static final ERXKey<String> NUM_FACTURE = new ERXKey<String>("numFacture");
	public static final ERXKey<String> PAYS = new ERXKey<String>("pays");
	public static final ERXKey<Integer> PERSID_CREATEUR = new ERXKey<Integer>("persidCreateur");
	public static final ERXKey<Integer> PERSID_MODIFICATION_NOTE = new ERXKey<Integer>("persidModificationNote");
	public static final ERXKey<Integer> PERSID_PAYEUR = new ERXKey<Integer>("persidPayeur");
	public static final ERXKey<Integer> PERSID_VALIDATEUR = new ERXKey<Integer>("persidValidateur");
	public static final ERXKey<String> PRENOM_PERSONNE = new ERXKey<String>("prenomPersonne");
	public static final ERXKey<Integer> REGLEMENT_COUNT = new ERXKey<Integer>("reglementCount");
	public static final ERXKey<java.math.BigDecimal> SOMME_PAYE = new ERXKey<java.math.BigDecimal>("sommePaye");
	public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> AFFECTATION_ARRHESES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>("affectationArrheses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> AFFECTATION_AVANCESES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>("affectationAvanceses");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> AFFECTATION_CAFS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>("affectationCafs");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURE_ORIGNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFactureOrigne");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> ENTETE_FACTURES_ORIGNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>("enteteFacturesOrigne");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFactures");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> LIGNES_REGLEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement>("lignesReglements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> RELANCES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORelance>("relances");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture> TYPE_FACTURE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture>("typeFacture");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idFacture";

	public static final String ADRESSE1_KEY = "adresse1";
	public static final String ADRESSE2_KEY = "adresse2";
	public static final String CIVILITE_KEY = "civilite";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_ECHEANCE_KEY = "dateEcheance";
	public static final String DATE_MODIFICATION_NOTE_KEY = "dateModificationNote";
	public static final String DATE_PRELEVEMENT_KEY = "datePrelevement";
	public static final String DATE_REMBOURSEMENT_KEY = "dateRemboursement";
	public static final String DATE_VALIDATION_KEY = "dateValidation";
	public static final String ID_FACTURE_KEY = "idFacture";
	public static final String ID_FACTURE_ORIGINE_KEY = "idFactureOrigine";
	public static final String ID_TYPE_FACTURE_KEY = "idTypeFacture";
	public static final String INDICATION_ABANDON_KEY = "indicationAbandon";
	public static final String INDICATION_PRELEVEMENT_KEY = "indicationPrelevement";
	public static final String LIB_FACTURE_KEY = "libFacture";
	public static final String MNT_A_PAYER_HT_KEY = "mntAPayerHt";
	public static final String MNT_A_PAYER_TTC_KEY = "mntAPayerTtc";
	public static final String MNT_A_PAYER_TVA_KEY = "mntAPayerTva";
	public static final String MNT_FACTURE_HT_KEY = "mntFactureHt";
	public static final String MNT_FACTURE_TTC_KEY = "mntFactureTtc";
	public static final String MNT_FACTURE_TVA_KEY = "mntFactureTva";
	public static final String NOM_PERSONNE_KEY = "nomPersonne";
	public static final String NOTE_KEY = "note";
	public static final String NOTE_PUBLIC_KEY = "notePublic";
	public static final String NUM_COMMANDE_KEY = "numCommande";
	public static final String NUM_FACTURE_KEY = "numFacture";
	public static final String PAYS_KEY = "pays";
	public static final String PERSID_CREATEUR_KEY = "persidCreateur";
	public static final String PERSID_MODIFICATION_NOTE_KEY = "persidModificationNote";
	public static final String PERSID_PAYEUR_KEY = "persidPayeur";
	public static final String PERSID_VALIDATEUR_KEY = "persidValidateur";
	public static final String PRENOM_PERSONNE_KEY = "prenomPersonne";
	public static final String REGLEMENT_COUNT_KEY = "reglementCount";
	public static final String SOMME_PAYE_KEY = "sommePaye";
	public static final String VILLE_KEY = "ville";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADRESSE1_COLKEY = "ADRESSE_1";
	public static final String ADRESSE2_COLKEY = "ADRESSE_2";
	public static final String CIVILITE_COLKEY = "CIVILITE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_ECHEANCE_COLKEY = "DATE_ECHEANCE";
	public static final String DATE_MODIFICATION_NOTE_COLKEY = "DATE_MODIFICATION_NOTE";
	public static final String DATE_PRELEVEMENT_COLKEY = "DATE_PRELEVEMENT";
	public static final String DATE_REMBOURSEMENT_COLKEY = "DATE_REMBOURSEMENT";
	public static final String DATE_VALIDATION_COLKEY = "DATE_VALIDATION";
	public static final String ID_FACTURE_COLKEY = "ID_FACTURE";
	public static final String ID_FACTURE_ORIGINE_COLKEY = "ID_FACTURE_ORIGINE";
	public static final String ID_TYPE_FACTURE_COLKEY = "ID_TYPE_FACTURE";
	public static final String INDICATION_ABANDON_COLKEY = "INDICATION_ABANDON";
	public static final String INDICATION_PRELEVEMENT_COLKEY = "INDICATION_PRELEVEMENT";
	public static final String LIB_FACTURE_COLKEY = "LIB_FACTURE";
	public static final String MNT_A_PAYER_HT_COLKEY = "MNT_A_PAYER_HT";
	public static final String MNT_A_PAYER_TTC_COLKEY = "MNT_A_PAYER_TTC";
	public static final String MNT_A_PAYER_TVA_COLKEY = "MNT_A_PAYER_TVA";
	public static final String MNT_FACTURE_HT_COLKEY = "MNT_FACTURE_HT";
	public static final String MNT_FACTURE_TTC_COLKEY = "MNT_FACTURE_TTC";
	public static final String MNT_FACTURE_TVA_COLKEY = "MNT_FACTURE_TVA";
	public static final String NOM_PERSONNE_COLKEY = "NOM_PERSONNE";
	public static final String NOTE_COLKEY = "NOTE";
	public static final String NOTE_PUBLIC_COLKEY = "NOTE_PUBLIC";
	public static final String NUM_COMMANDE_COLKEY = "NUM_COMMANDE";
	public static final String NUM_FACTURE_COLKEY = "NUM_FACTURE";
	public static final String PAYS_COLKEY = "PAYS";
	public static final String PERSID_CREATEUR_COLKEY = "PERSID_CREATEUR";
	public static final String PERSID_MODIFICATION_NOTE_COLKEY = "PERSID_MODIFICATION_NOTE";
	public static final String PERSID_PAYEUR_COLKEY = "PERSID_PAYEUR";
	public static final String PERSID_VALIDATEUR_COLKEY = "PERSID_VALIDATEUR";
	public static final String PRENOM_PERSONNE_COLKEY = "PRENOM_PERSONNE";
	public static final String REGLEMENT_COUNT_COLKEY = "$attribute.columnName";
	public static final String SOMME_PAYE_COLKEY = "$attribute.columnName";
	public static final String VILLE_COLKEY = "VILLE";



	// Relationships
	public static final String AFFECTATION_ARRHESES_KEY = "affectationArrheses";
	public static final String AFFECTATION_AVANCESES_KEY = "affectationAvanceses";
	public static final String AFFECTATION_CAFS_KEY = "affectationCafs";
	public static final String ENTETE_FACTURE_ORIGNE_KEY = "enteteFactureOrigne";
	public static final String ENTETE_FACTURES_ORIGNE_KEY = "enteteFacturesOrigne";
	public static final String LIGNE_FACTURES_KEY = "ligneFactures";
	public static final String LIGNES_REGLEMENTS_KEY = "lignesReglements";
	public static final String PERSONNE_KEY = "personne";
	public static final String RELANCES_KEY = "relances";
	public static final String TYPE_FACTURE_KEY = "typeFacture";



	// Accessors methods
	public String adresse1() {
	 return (String) storedValueForKey(ADRESSE1_KEY);
	}

	public void setAdresse1(String value) {
	 takeStoredValueForKey(value, ADRESSE1_KEY);
	}

	public String adresse2() {
	 return (String) storedValueForKey(ADRESSE2_KEY);
	}

	public void setAdresse2(String value) {
	 takeStoredValueForKey(value, ADRESSE2_KEY);
	}

	public String civilite() {
	 return (String) storedValueForKey(CIVILITE_KEY);
	}

	public void setCivilite(String value) {
	 takeStoredValueForKey(value, CIVILITE_KEY);
	}

	public String codePostal() {
	 return (String) storedValueForKey(CODE_POSTAL_KEY);
	}

	public void setCodePostal(String value) {
	 takeStoredValueForKey(value, CODE_POSTAL_KEY);
	}

	public NSTimestamp dateCreation() {
	 return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_CREATION_KEY);
	}

	public NSTimestamp dateEcheance() {
	 return (NSTimestamp) storedValueForKey(DATE_ECHEANCE_KEY);
	}

	public void setDateEcheance(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_ECHEANCE_KEY);
	}

	public NSTimestamp dateModificationNote() {
	 return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_NOTE_KEY);
	}

	public void setDateModificationNote(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_MODIFICATION_NOTE_KEY);
	}

	public NSTimestamp datePrelevement() {
	 return (NSTimestamp) storedValueForKey(DATE_PRELEVEMENT_KEY);
	}

	public void setDatePrelevement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_PRELEVEMENT_KEY);
	}

	public NSTimestamp dateRemboursement() {
	 return (NSTimestamp) storedValueForKey(DATE_REMBOURSEMENT_KEY);
	}

	public void setDateRemboursement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_REMBOURSEMENT_KEY);
	}

	public NSTimestamp dateValidation() {
	 return (NSTimestamp) storedValueForKey(DATE_VALIDATION_KEY);
	}

	public void setDateValidation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_VALIDATION_KEY);
	}

	public Long idFacture() {
	 return (Long) storedValueForKey(ID_FACTURE_KEY);
	}

	public void setIdFacture(Long value) {
	 takeStoredValueForKey(value, ID_FACTURE_KEY);
	}

	public Long idFactureOrigine() {
	 return (Long) storedValueForKey(ID_FACTURE_ORIGINE_KEY);
	}

	public void setIdFactureOrigine(Long value) {
	 takeStoredValueForKey(value, ID_FACTURE_ORIGINE_KEY);
	}

	public Long idTypeFacture() {
	 return (Long) storedValueForKey(ID_TYPE_FACTURE_KEY);
	}

	public void setIdTypeFacture(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_FACTURE_KEY);
	}

	public Boolean indicationAbandon() {
	 return (Boolean) storedValueForKey(INDICATION_ABANDON_KEY);
	}

	public void setIndicationAbandon(Boolean value) {
	 takeStoredValueForKey(value, INDICATION_ABANDON_KEY);
	}

	public Boolean indicationPrelevement() {
	 return (Boolean) storedValueForKey(INDICATION_PRELEVEMENT_KEY);
	}

	public void setIndicationPrelevement(Boolean value) {
	 takeStoredValueForKey(value, INDICATION_PRELEVEMENT_KEY);
	}

	public String libFacture() {
	 return (String) storedValueForKey(LIB_FACTURE_KEY);
	}

	public void setLibFacture(String value) {
	 takeStoredValueForKey(value, LIB_FACTURE_KEY);
	}

	public java.math.BigDecimal mntAPayerHt() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_HT_KEY);
	}

	public void setMntAPayerHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_A_PAYER_HT_KEY);
	}

	public java.math.BigDecimal mntAPayerTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_TTC_KEY);
	}

	public void setMntAPayerTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_A_PAYER_TTC_KEY);
	}

	public java.math.BigDecimal mntAPayerTva() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_A_PAYER_TVA_KEY);
	}

	public void setMntAPayerTva(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_A_PAYER_TVA_KEY);
	}

	public java.math.BigDecimal mntFactureHt() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_HT_KEY);
	}

	public void setMntFactureHt(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_FACTURE_HT_KEY);
	}

	public java.math.BigDecimal mntFactureTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_TTC_KEY);
	}

	public void setMntFactureTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_FACTURE_TTC_KEY);
	}

	public java.math.BigDecimal mntFactureTva() {
	 return (java.math.BigDecimal) storedValueForKey(MNT_FACTURE_TVA_KEY);
	}

	public void setMntFactureTva(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MNT_FACTURE_TVA_KEY);
	}

	public String nomPersonne() {
	 return (String) storedValueForKey(NOM_PERSONNE_KEY);
	}

	public void setNomPersonne(String value) {
	 takeStoredValueForKey(value, NOM_PERSONNE_KEY);
	}

	public String note() {
	 return (String) storedValueForKey(NOTE_KEY);
	}

	public void setNote(String value) {
	 takeStoredValueForKey(value, NOTE_KEY);
	}

	public String notePublic() {
	 return (String) storedValueForKey(NOTE_PUBLIC_KEY);
	}

	public void setNotePublic(String value) {
	 takeStoredValueForKey(value, NOTE_PUBLIC_KEY);
	}

	public String numCommande() {
	 return (String) storedValueForKey(NUM_COMMANDE_KEY);
	}

	public void setNumCommande(String value) {
	 takeStoredValueForKey(value, NUM_COMMANDE_KEY);
	}

	public String numFacture() {
	 return (String) storedValueForKey(NUM_FACTURE_KEY);
	}

	public void setNumFacture(String value) {
	 takeStoredValueForKey(value, NUM_FACTURE_KEY);
	}

	public String pays() {
	 return (String) storedValueForKey(PAYS_KEY);
	}

	public void setPays(String value) {
	 takeStoredValueForKey(value, PAYS_KEY);
	}

	public Integer persidCreateur() {
	 return (Integer) storedValueForKey(PERSID_CREATEUR_KEY);
	}

	public void setPersidCreateur(Integer value) {
	 takeStoredValueForKey(value, PERSID_CREATEUR_KEY);
	}

	public Integer persidModificationNote() {
	 return (Integer) storedValueForKey(PERSID_MODIFICATION_NOTE_KEY);
	}

	public void setPersidModificationNote(Integer value) {
	 takeStoredValueForKey(value, PERSID_MODIFICATION_NOTE_KEY);
	}

	public Integer persidPayeur() {
	 return (Integer) storedValueForKey(PERSID_PAYEUR_KEY);
	}

	public void setPersidPayeur(Integer value) {
	 takeStoredValueForKey(value, PERSID_PAYEUR_KEY);
	}

	public Integer persidValidateur() {
	 return (Integer) storedValueForKey(PERSID_VALIDATEUR_KEY);
	}

	public void setPersidValidateur(Integer value) {
	 takeStoredValueForKey(value, PERSID_VALIDATEUR_KEY);
	}

	public String prenomPersonne() {
	 return (String) storedValueForKey(PRENOM_PERSONNE_KEY);
	}

	public void setPrenomPersonne(String value) {
	 takeStoredValueForKey(value, PRENOM_PERSONNE_KEY);
	}

	public Integer reglementCount() {
	 return (Integer) storedValueForKey(REGLEMENT_COUNT_KEY);
	}

	public void setReglementCount(Integer value) {
	 takeStoredValueForKey(value, REGLEMENT_COUNT_KEY);
	}

	public java.math.BigDecimal sommePaye() {
	 return (java.math.BigDecimal) storedValueForKey(SOMME_PAYE_KEY);
	}

	public void setSommePaye(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, SOMME_PAYE_KEY);
	}

	public String ville() {
	 return (String) storedValueForKey(VILLE_KEY);
	}

	public void setVille(String value) {
	 takeStoredValueForKey(value, VILLE_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture enteteFactureOrigne() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture)storedValueForKey(ENTETE_FACTURE_ORIGNE_KEY);
	}

	public void setEnteteFactureOrigneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture oldValue = enteteFactureOrigne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENTETE_FACTURE_ORIGNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, ENTETE_FACTURE_ORIGNE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture typeFacture() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture)storedValueForKey(TYPE_FACTURE_KEY);
	}

	public void setTypeFactureRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture oldValue = typeFacture();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_FACTURE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_FACTURE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)storedValueForKey(AFFECTATION_ARRHESES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier) {
	 return affectationArrheses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier, boolean fetch) {
	 return affectationArrheses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> affectationArrheses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationArrheses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
	}
	
	public void removeFromAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes createAffectationArrhesesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_ARRHESES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes) eo;
	}
	
	public void deleteAffectationArrhesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_ARRHESES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationArrhesesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationArrhes> objects = affectationArrheses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationArrhesesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)storedValueForKey(AFFECTATION_AVANCESES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier) {
	 return affectationAvanceses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier, boolean fetch) {
	 return affectationAvanceses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> affectationAvanceses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationAvanceses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
	}
	
	public void removeFromAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances createAffectationAvancesesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_AVANCESES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances) eo;
	}
	
	public void deleteAffectationAvancesesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_AVANCESES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationAvancesesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationAvances> objects = affectationAvanceses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationAvancesesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)storedValueForKey(AFFECTATION_CAFS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier) {
	 return affectationCafs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier, boolean fetch) {
	 return affectationCafs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> affectationCafs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = affectationCafs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
	}
	
	public void removeFromAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf createAffectationCafsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATION_CAFS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf) eo;
	}
	
	public void deleteAffectationCafsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATION_CAFS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllAffectationCafsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOAffectationCaf> objects = affectationCafs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteAffectationCafsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFacturesOrigne() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)storedValueForKey(ENTETE_FACTURES_ORIGNE_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFacturesOrigne(EOQualifier qualifier) {
	 return enteteFacturesOrigne(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFacturesOrigne(EOQualifier qualifier, boolean fetch) {
	 return enteteFacturesOrigne(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> enteteFacturesOrigne(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.ENTETE_FACTURE_ORIGNE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = enteteFacturesOrigne();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToEnteteFacturesOrigneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_ORIGNE_KEY);
	}
	
	public void removeFromEnteteFacturesOrigneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_ORIGNE_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture createEnteteFacturesOrigneRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ENTETE_FACTURES_ORIGNE_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture) eo;
	}
	
	public void deleteEnteteFacturesOrigneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ENTETE_FACTURES_ORIGNE_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllEnteteFacturesOrigneRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> objects = enteteFacturesOrigne().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteEnteteFacturesOrigneRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)storedValueForKey(LIGNE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier) {
	 return ligneFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, boolean fetch) {
	 return ligneFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ligneFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public void removeFromLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture createLigneFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture) eo;
	}
	
	public void deleteLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLigneFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> objects = ligneFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLigneFacturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> lignesReglements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement>)storedValueForKey(LIGNES_REGLEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> lignesReglements(EOQualifier qualifier) {
	 return lignesReglements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> lignesReglements(EOQualifier qualifier, boolean fetch) {
	 return lignesReglements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> lignesReglements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = lignesReglements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLignesReglementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNES_REGLEMENTS_KEY);
	}
	
	public void removeFromLignesReglementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNES_REGLEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement createLignesReglementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNES_REGLEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement) eo;
	}
	
	public void deleteLignesReglementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNES_REGLEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLignesReglementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLignesReglement> objects = lignesReglements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLignesReglementsRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> relances() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance>)storedValueForKey(RELANCES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> relances(EOQualifier qualifier) {
	 return relances(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> relances(EOQualifier qualifier, boolean fetch) {
	 return relances(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> relances(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORelance.ENTETE_FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORelance.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = relances();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORelance>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRelancesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORelance object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RELANCES_KEY);
	}
	
	public void removeFromRelancesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORelance object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RELANCES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORelance createRelancesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORelance.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RELANCES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORelance) eo;
	}
	
	public void deleteRelancesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORelance object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RELANCES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRelancesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORelance> objects = relances().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRelancesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOEnteteFacture avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOEnteteFacture createEOEnteteFacture(EOEditingContext editingContext																								, Long idFacture
																																																																) {
	 EOEnteteFacture eo = (EOEnteteFacture) EOUtilities.createAndInsertInstance(editingContext, _EOEnteteFacture.ENTITY_NAME);	 
																											eo.setIdFacture(idFacture);
																																																													 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEnteteFacture creerInstance(EOEditingContext editingContext) {
		EOEnteteFacture object = (EOEnteteFacture)EOUtilities.createAndInsertInstance(editingContext, _EOEnteteFacture.ENTITY_NAME);
  		return object;
		}

	

  public EOEnteteFacture localInstanceIn(EOEditingContext editingContext) {
    EOEnteteFacture localInstance = (EOEnteteFacture)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEnteteFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEnteteFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOEnteteFacture> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEnteteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEnteteFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEnteteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEnteteFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOEnteteFacture> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEnteteFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEnteteFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEnteteFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEnteteFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEnteteFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEnteteFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
