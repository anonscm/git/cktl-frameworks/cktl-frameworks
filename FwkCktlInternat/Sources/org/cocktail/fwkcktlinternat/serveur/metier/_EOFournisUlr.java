/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOFournisUlr.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOFournisUlr extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOFournisUlr.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_FournisUlr";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FOURNIS_ULR";


//Attribute Keys
	public static final ERXKey<Long> ADR_ORDRE = new ERXKey<Long>("adrOrdre");
	public static final ERXKey<String> FOU_CODE = new ERXKey<String>("fouCode");
	public static final ERXKey<java.math.BigDecimal> FOU_ORDRE = new ERXKey<java.math.BigDecimal>("fouOrdre");
	public static final ERXKey<String> FOU_TYPE = new ERXKey<String>("fouType");
	public static final ERXKey<String> FOU_VALIDE = new ERXKey<String>("fouValide");
	public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne>("personne");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> RIBFOUR_ULRS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr>("ribfourUlrs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String PERS_ID_KEY = "persId";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String PERS_ID_COLKEY = "PERS_ID";



	// Relationships
	public static final String PERSONNE_KEY = "personne";
	public static final String RIBFOUR_ULRS_KEY = "ribfourUlrs";



	// Accessors methods
	public Long adrOrdre() {
	 return (Long) storedValueForKey(ADR_ORDRE_KEY);
	}

	public void setAdrOrdre(Long value) {
	 takeStoredValueForKey(value, ADR_ORDRE_KEY);
	}

	public String fouCode() {
	 return (String) storedValueForKey(FOU_CODE_KEY);
	}

	public void setFouCode(String value) {
	 takeStoredValueForKey(value, FOU_CODE_KEY);
	}

	public java.math.BigDecimal fouOrdre() {
	 return (java.math.BigDecimal) storedValueForKey(FOU_ORDRE_KEY);
	}

	public void setFouOrdre(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, FOU_ORDRE_KEY);
	}

	public String fouType() {
	 return (String) storedValueForKey(FOU_TYPE_KEY);
	}

	public void setFouType(String value) {
	 takeStoredValueForKey(value, FOU_TYPE_KEY);
	}

	public String fouValide() {
	 return (String) storedValueForKey(FOU_VALIDE_KEY);
	}

	public void setFouValide(String value) {
	 takeStoredValueForKey(value, FOU_VALIDE_KEY);
	}

	public Integer persId() {
	 return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> ribfourUlrs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr>)storedValueForKey(RIBFOUR_ULRS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> ribfourUlrs(EOQualifier qualifier) {
	 return ribfourUlrs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> ribfourUlrs(EOQualifier qualifier, boolean fetch) {
	 return ribfourUlrs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> ribfourUlrs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr.FOURNIS_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ribfourUlrs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToRibfourUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, RIBFOUR_ULRS_KEY);
	}
	
	public void removeFromRibfourUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RIBFOUR_ULRS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr createRibfourUlrsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, RIBFOUR_ULRS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr) eo;
	}
	
	public void deleteRibfourUlrsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, RIBFOUR_ULRS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllRibfourUlrsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> objects = ribfourUlrs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteRibfourUlrsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOFournisUlr avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOFournisUlr createEOFournisUlr(EOEditingContext editingContext								, java.math.BigDecimal fouOrdre
																) {
	 EOFournisUlr eo = (EOFournisUlr) EOUtilities.createAndInsertInstance(editingContext, _EOFournisUlr.ENTITY_NAME);	 
											eo.setFouOrdre(fouOrdre);
													 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFournisUlr creerInstance(EOEditingContext editingContext) {
		EOFournisUlr object = (EOFournisUlr)EOUtilities.createAndInsertInstance(editingContext, _EOFournisUlr.ENTITY_NAME);
  		return object;
		}

	

  public EOFournisUlr localInstanceIn(EOEditingContext editingContext) {
    EOFournisUlr localInstance = (EOFournisUlr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFournisUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFournisUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOFournisUlr> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFournisUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFournisUlr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFournisUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFournisUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOFournisUlr> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFournisUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFournisUlr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFournisUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFournisUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFournisUlr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFournisUlr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
