/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOHebergement.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOHebergement extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOHebergement.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Hebergement";
	public static final String ENTITY_TABLE_NAME = "LITCHI.HEBERGEMENT";


//Attribute Keys
	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_RESERVATION = new ERXKey<NSTimestamp>("dateReservation");
	public static final ERXKey<Long> ID_HEBERGEMENT = new ERXKey<Long>("idHebergement");
	public static final ERXKey<Integer> PERS_ID_PAYEUR = new ERXKey<Integer>("persIdPayeur");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> DETAIL_HEBERGEMENTS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>("detailHebergements");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne> PERSONNE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne>("personne");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idHebergement";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_RESERVATION_KEY = "dateReservation";
	public static final String ID_HEBERGEMENT_KEY = "idHebergement";
	public static final String PERS_ID_PAYEUR_KEY = "persIdPayeur";

//Attributs non visibles

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_RESERVATION_COLKEY = "DATE_RESERVATION";
	public static final String ID_HEBERGEMENT_COLKEY = "ID_HEBERGEMENT";
	public static final String PERS_ID_PAYEUR_COLKEY = "PERS_ID_PAYEUR";



	// Relationships
	public static final String DETAIL_HEBERGEMENTS_KEY = "detailHebergements";
	public static final String PERSONNE_KEY = "personne";



	// Accessors methods
	public String commentaire() {
	 return (String) storedValueForKey(COMMENTAIRE_KEY);
	}

	public void setCommentaire(String value) {
	 takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}

	public NSTimestamp dateReservation() {
	 return (NSTimestamp) storedValueForKey(DATE_RESERVATION_KEY);
	}

	public void setDateReservation(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_RESERVATION_KEY);
	}

	public Long idHebergement() {
	 return (Long) storedValueForKey(ID_HEBERGEMENT_KEY);
	}

	public void setIdHebergement(Long value) {
	 takeStoredValueForKey(value, ID_HEBERGEMENT_KEY);
	}

	public Integer persIdPayeur() {
	 return (Integer) storedValueForKey(PERS_ID_PAYEUR_KEY);
	}

	public void setPersIdPayeur(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_PAYEUR_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne personne() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
	}

	public void setPersonneRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOPersonne oldValue = personne();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)storedValueForKey(DETAIL_HEBERGEMENTS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier) {
	 return detailHebergements(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, boolean fetch) {
	 return detailHebergements(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> detailHebergements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.HEBERGEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = detailHebergements();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public void removeFromDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement createDetailHebergementsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, DETAIL_HEBERGEMENTS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement) eo;
	}
	
	public void deleteDetailHebergementsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_HEBERGEMENTS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllDetailHebergementsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement> objects = detailHebergements().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteDetailHebergementsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOHebergement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOHebergement createEOHebergement(EOEditingContext editingContext								, Long idHebergement
												) {
	 EOHebergement eo = (EOHebergement) EOUtilities.createAndInsertInstance(editingContext, _EOHebergement.ENTITY_NAME);	 
											eo.setIdHebergement(idHebergement);
									 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOHebergement creerInstance(EOEditingContext editingContext) {
		EOHebergement object = (EOHebergement)EOUtilities.createAndInsertInstance(editingContext, _EOHebergement.ENTITY_NAME);
  		return object;
		}

	

  public EOHebergement localInstanceIn(EOEditingContext editingContext) {
    EOHebergement localInstance = (EOHebergement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOHebergement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOHebergement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOHebergement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOHebergement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOHebergement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOHebergement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOHebergement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOHebergement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOHebergement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOHebergement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOHebergement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOHebergement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOHebergement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOHebergement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
