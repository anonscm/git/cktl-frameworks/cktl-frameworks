/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTrashReglement.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOTrashReglement extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOTrashReglement.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_TrashReglement";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TRASH_REGLEMENT";


//Attribute Keys
	public static final ERXKey<String> AUTRE_BANQUE = new ERXKey<String>("autreBanque");
	public static final ERXKey<Integer> BANQ_ORDRE = new ERXKey<Integer>("banqOrdre");
	public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
	public static final ERXKey<NSTimestamp> DATE_EFFACEMENT = new ERXKey<NSTimestamp>("dateEffacement");
	public static final ERXKey<NSTimestamp> DATE_REGLEMENT = new ERXKey<NSTimestamp>("dateReglement");
	public static final ERXKey<Long> ID_TYPE_REGLEMENT = new ERXKey<Long>("idTypeReglement");
	public static final ERXKey<Boolean> INDIC_TRANSACTION = new ERXKey<Boolean>("indicTransaction");
	public static final ERXKey<java.math.BigDecimal> MONTANT_TTC = new ERXKey<java.math.BigDecimal>("montantTtc");
	public static final ERXKey<String> NOM_TIREUR = new ERXKey<String>("nomTireur");
	public static final ERXKey<String> NUMERO_CHEQUE = new ERXKey<String>("numeroCheque");
	public static final ERXKey<Integer> PERSID_EFFACEUR = new ERXKey<Integer>("persidEffaceur");
	public static final ERXKey<Integer> PERSID_PAYEUR = new ERXKey<Integer>("persidPayeur");
	public static final ERXKey<Integer> PERSID_PERCEPTEUR = new ERXKey<Integer>("persidPercepteur");
	public static final ERXKey<String> PRENOM_TIREUR = new ERXKey<String>("prenomTireur");
	public static final ERXKey<String> REF_REGLEMENT = new ERXKey<String>("refReglement");
// Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idReglement";

	public static final String AUTRE_BANQUE_KEY = "autreBanque";
	public static final String BANQ_ORDRE_KEY = "banqOrdre";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_EFFACEMENT_KEY = "dateEffacement";
	public static final String DATE_REGLEMENT_KEY = "dateReglement";
	public static final String ID_TYPE_REGLEMENT_KEY = "idTypeReglement";
	public static final String INDIC_TRANSACTION_KEY = "indicTransaction";
	public static final String MONTANT_TTC_KEY = "montantTtc";
	public static final String NOM_TIREUR_KEY = "nomTireur";
	public static final String NUMERO_CHEQUE_KEY = "numeroCheque";
	public static final String PERSID_EFFACEUR_KEY = "persidEffaceur";
	public static final String PERSID_PAYEUR_KEY = "persidPayeur";
	public static final String PERSID_PERCEPTEUR_KEY = "persidPercepteur";
	public static final String PRENOM_TIREUR_KEY = "prenomTireur";
	public static final String REF_REGLEMENT_KEY = "refReglement";

//Attributs non visibles
	public static final String ID_REGLEMENT_KEY = "idReglement";

//Colonnes dans la base de donnees
	public static final String AUTRE_BANQUE_COLKEY = "AUTRE_BANQUE";
	public static final String BANQ_ORDRE_COLKEY = "BANQ_ORDRE";
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_EFFACEMENT_COLKEY = "DATE_EFFACEMENT";
	public static final String DATE_REGLEMENT_COLKEY = "DATE_REGLEMENT";
	public static final String ID_TYPE_REGLEMENT_COLKEY = "ID_TYPE_REGLEMENT";
	public static final String INDIC_TRANSACTION_COLKEY = "INDIC_TRANSACTION";
	public static final String MONTANT_TTC_COLKEY = "MONTANT_TTC";
	public static final String NOM_TIREUR_COLKEY = "NOM_TIREUR";
	public static final String NUMERO_CHEQUE_COLKEY = "NUMERO_CHEQUE";
	public static final String PERSID_EFFACEUR_COLKEY = "PERSID_EFFACEUR";
	public static final String PERSID_PAYEUR_COLKEY = "PERSID_PAYEUR";
	public static final String PERSID_PERCEPTEUR_COLKEY = "PERSID_PERCEPTEUR";
	public static final String PRENOM_TIREUR_COLKEY = "PRENOM_TIREUR";
	public static final String REF_REGLEMENT_COLKEY = "REF_REGLEMENT";

	public static final String ID_REGLEMENT_COLKEY = "ID_REGLEMENT";


	// Relationships



	// Accessors methods
	public String autreBanque() {
	 return (String) storedValueForKey(AUTRE_BANQUE_KEY);
	}

	public void setAutreBanque(String value) {
	 takeStoredValueForKey(value, AUTRE_BANQUE_KEY);
	}

	public Integer banqOrdre() {
	 return (Integer) storedValueForKey(BANQ_ORDRE_KEY);
	}

	public void setBanqOrdre(Integer value) {
	 takeStoredValueForKey(value, BANQ_ORDRE_KEY);
	}

	public String commentaire() {
	 return (String) storedValueForKey(COMMENTAIRE_KEY);
	}

	public void setCommentaire(String value) {
	 takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}

	public NSTimestamp dateEffacement() {
	 return (NSTimestamp) storedValueForKey(DATE_EFFACEMENT_KEY);
	}

	public void setDateEffacement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_EFFACEMENT_KEY);
	}

	public NSTimestamp dateReglement() {
	 return (NSTimestamp) storedValueForKey(DATE_REGLEMENT_KEY);
	}

	public void setDateReglement(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_REGLEMENT_KEY);
	}

	public Long idTypeReglement() {
	 return (Long) storedValueForKey(ID_TYPE_REGLEMENT_KEY);
	}

	public void setIdTypeReglement(Long value) {
	 takeStoredValueForKey(value, ID_TYPE_REGLEMENT_KEY);
	}

	public Boolean indicTransaction() {
	 return (Boolean) storedValueForKey(INDIC_TRANSACTION_KEY);
	}

	public void setIndicTransaction(Boolean value) {
	 takeStoredValueForKey(value, INDIC_TRANSACTION_KEY);
	}

	public java.math.BigDecimal montantTtc() {
	 return (java.math.BigDecimal) storedValueForKey(MONTANT_TTC_KEY);
	}

	public void setMontantTtc(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, MONTANT_TTC_KEY);
	}

	public String nomTireur() {
	 return (String) storedValueForKey(NOM_TIREUR_KEY);
	}

	public void setNomTireur(String value) {
	 takeStoredValueForKey(value, NOM_TIREUR_KEY);
	}

	public String numeroCheque() {
	 return (String) storedValueForKey(NUMERO_CHEQUE_KEY);
	}

	public void setNumeroCheque(String value) {
	 takeStoredValueForKey(value, NUMERO_CHEQUE_KEY);
	}

	public Integer persidEffaceur() {
	 return (Integer) storedValueForKey(PERSID_EFFACEUR_KEY);
	}

	public void setPersidEffaceur(Integer value) {
	 takeStoredValueForKey(value, PERSID_EFFACEUR_KEY);
	}

	public Integer persidPayeur() {
	 return (Integer) storedValueForKey(PERSID_PAYEUR_KEY);
	}

	public void setPersidPayeur(Integer value) {
	 takeStoredValueForKey(value, PERSID_PAYEUR_KEY);
	}

	public Integer persidPercepteur() {
	 return (Integer) storedValueForKey(PERSID_PERCEPTEUR_KEY);
	}

	public void setPersidPercepteur(Integer value) {
	 takeStoredValueForKey(value, PERSID_PERCEPTEUR_KEY);
	}

	public String prenomTireur() {
	 return (String) storedValueForKey(PRENOM_TIREUR_KEY);
	}

	public void setPrenomTireur(String value) {
	 takeStoredValueForKey(value, PRENOM_TIREUR_KEY);
	}

	public String refReglement() {
	 return (String) storedValueForKey(REF_REGLEMENT_KEY);
	}

	public void setRefReglement(String value) {
	 takeStoredValueForKey(value, REF_REGLEMENT_KEY);
	}


	/**
	* Créer une instance de EOTrashReglement avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTrashReglement createEOTrashReglement(EOEditingContext editingContext																																			) {
	 EOTrashReglement eo = (EOTrashReglement) EOUtilities.createAndInsertInstance(editingContext, _EOTrashReglement.ENTITY_NAME);	 
																																	 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTrashReglement creerInstance(EOEditingContext editingContext) {
		EOTrashReglement object = (EOTrashReglement)EOUtilities.createAndInsertInstance(editingContext, _EOTrashReglement.ENTITY_NAME);
  		return object;
		}

	

  public EOTrashReglement localInstanceIn(EOEditingContext editingContext) {
    EOTrashReglement localInstance = (EOTrashReglement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTrashReglement>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTrashReglement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTrashReglement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTrashReglement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTrashReglement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTrashReglement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTrashReglement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTrashReglement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTrashReglement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTrashReglement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTrashReglement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTrashReglement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTrashReglement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTrashReglement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTrashReglement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
