/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOLocal.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOLocal extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOLocal.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_Local";
	public static final String ENTITY_TABLE_NAME = "GRHUM.LOCAL";


//Attribute Keys
	public static final ERXKey<String> ADRESSE_INTERNE = new ERXKey<String>("adresseInterne");
	public static final ERXKey<String> APPELLATION = new ERXKey<String>("appellation");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
	public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<Long> ID_ADRESSE_ADMIN = new ERXKey<Long>("idAdresseAdmin");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> SALLES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle>("salles");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cLocal";

	public static final String ADRESSE_INTERNE_KEY = "adresseInterne";
	public static final String APPELLATION_KEY = "appellation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_ADRESSE_ADMIN_KEY = "idAdresseAdmin";

//Attributs non visibles
	public static final String C_LOCAL_KEY = "cLocal";

//Colonnes dans la base de donnees
	public static final String ADRESSE_INTERNE_COLKEY = "ADRESSE_INTERNE";
	public static final String APPELLATION_COLKEY = "APPELLATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_ADRESSE_ADMIN_COLKEY = "ID_ADRESSE_ADMIN";

	public static final String C_LOCAL_COLKEY = "C_LOCAL";


	// Relationships
	public static final String SALLES_KEY = "salles";



	// Accessors methods
	public String adresseInterne() {
	 return (String) storedValueForKey(ADRESSE_INTERNE_KEY);
	}

	public void setAdresseInterne(String value) {
	 takeStoredValueForKey(value, ADRESSE_INTERNE_KEY);
	}

	public String appellation() {
	 return (String) storedValueForKey(APPELLATION_KEY);
	}

	public void setAppellation(String value) {
	 takeStoredValueForKey(value, APPELLATION_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dDebVal() {
	 return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
	}

	public void setDDebVal(NSTimestamp value) {
	 takeStoredValueForKey(value, D_DEB_VAL_KEY);
	}

	public NSTimestamp dFinVal() {
	 return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
	}

	public void setDFinVal(NSTimestamp value) {
	 takeStoredValueForKey(value, D_FIN_VAL_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public Long idAdresseAdmin() {
	 return (Long) storedValueForKey(ID_ADRESSE_ADMIN_KEY);
	}

	public void setIdAdresseAdmin(Long value) {
	 takeStoredValueForKey(value, ID_ADRESSE_ADMIN_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> salles() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle>)storedValueForKey(SALLES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> salles(EOQualifier qualifier) {
	 return salles(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> salles(EOQualifier qualifier, boolean fetch) {
	 return salles(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> salles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOSalle.LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOSalle.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = salles();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToSallesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOSalle object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, SALLES_KEY);
	}
	
	public void removeFromSallesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOSalle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, SALLES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOSalle createSallesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOSalle.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, SALLES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOSalle) eo;
	}
	
	public void deleteSallesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOSalle object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, SALLES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllSallesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOSalle> objects = salles().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteSallesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOLocal avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOLocal createEOLocal(EOEditingContext editingContext				, String adresseInterne
									, NSTimestamp dCreation
							, NSTimestamp dDebVal
									, NSTimestamp dModification
							, Long idAdresseAdmin
								) {
	 EOLocal eo = (EOLocal) EOUtilities.createAndInsertInstance(editingContext, _EOLocal.ENTITY_NAME);	 
							eo.setAdresseInterne(adresseInterne);
											eo.setDCreation(dCreation);
									eo.setDDebVal(dDebVal);
											eo.setDModification(dModification);
									eo.setIdAdresseAdmin(idAdresseAdmin);
					 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLocal creerInstance(EOEditingContext editingContext) {
		EOLocal object = (EOLocal)EOUtilities.createAndInsertInstance(editingContext, _EOLocal.ENTITY_NAME);
  		return object;
		}

	

  public EOLocal localInstanceIn(EOEditingContext editingContext) {
    EOLocal localInstance = (EOLocal)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLocal fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLocal fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOLocal> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLocal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLocal)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLocal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLocal fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOLocal> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLocal eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLocal)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLocal fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLocal eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLocal ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLocal fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
		@SuppressWarnings("unchecked")
	public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLocal> objectsForLocauxAvecObjetsReservables(EOEditingContext ec) {

		EOFetchSpecification spec = EOFetchSpecification.fetchSpecificationNamed("LocauxAvecObjetsReservables", EOLocal.ENTITY_NAME);
			return ec.objectsWithFetchSpecification(spec);
	}
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
