/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EORibfourUlr.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EORibfourUlr extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EORibfourUlr.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_RibfourUlr";
	public static final String ENTITY_TABLE_NAME = "GRHUM.RIBFOUR_ULR";


//Attribute Keys
	public static final ERXKey<Integer> BANQ_ORDRE = new ERXKey<Integer>("banqOrdre");
	public static final ERXKey<String> BIC = new ERXKey<String>("bic");
	public static final ERXKey<String> C_BANQUE = new ERXKey<String>("cBanque");
	public static final ERXKey<String> C_GUICHET = new ERXKey<String>("cGuichet");
	public static final ERXKey<String> CLE_RIB = new ERXKey<String>("cleRib");
	public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
	public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
	public static final ERXKey<java.math.BigDecimal> FOU_ORDRE = new ERXKey<java.math.BigDecimal>("fouOrdre");
	public static final ERXKey<String> IBAN = new ERXKey<String>("iban");
	public static final ERXKey<String> MOD_CODE = new ERXKey<String>("modCode");
	public static final ERXKey<String> NO_COMPTE = new ERXKey<String>("noCompte");
	public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
	public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
	public static final ERXKey<String> RIB_TITCO = new ERXKey<String>("ribTitco");
	public static final ERXKey<String> RIB_VALIDE = new ERXKey<String>("ribValide");
	public static final ERXKey<String> TEM_PAYE_UTIL = new ERXKey<String>("temPayeUtil");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr> FOURNIS_ULR = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr>("fournisUlr");
	public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBanque> FWKPERS__BANQUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBanque>("fwkpers_Banque");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> PRELEVEMENTSES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>("prelevementses");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ribOrdre";

	public static final String BANQ_ORDRE_KEY = "banqOrdre";
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String CLE_RIB_KEY = "cleRib";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String RIB_TITCO_KEY = "ribTitco";
	public static final String RIB_VALIDE_KEY = "ribValide";
	public static final String TEM_PAYE_UTIL_KEY = "temPayeUtil";

//Attributs non visibles
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String BANQ_ORDRE_COLKEY = "BANQ_ORDRE";
	public static final String BIC_COLKEY = "BIC";
	public static final String C_BANQUE_COLKEY = "C_BANQUE";
	public static final String C_GUICHET_COLKEY = "C_GUICHET";
	public static final String CLE_RIB_COLKEY = "CLE_RIB";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String NO_COMPTE_COLKEY = "NO_COMPTE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String RIB_TITCO_COLKEY = "RIB_TITCO";
	public static final String RIB_VALIDE_COLKEY = "RIB_VALIDE";
	public static final String TEM_PAYE_UTIL_COLKEY = "TEM_PAYE_UTIL";

	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";


	// Relationships
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String FWKPERS__BANQUE_KEY = "fwkpers_Banque";
	public static final String PRELEVEMENTSES_KEY = "prelevementses";



	// Accessors methods
	public Integer banqOrdre() {
	 return (Integer) storedValueForKey(BANQ_ORDRE_KEY);
	}

	public void setBanqOrdre(Integer value) {
	 takeStoredValueForKey(value, BANQ_ORDRE_KEY);
	}

	public String bic() {
	 return (String) storedValueForKey(BIC_KEY);
	}

	public void setBic(String value) {
	 takeStoredValueForKey(value, BIC_KEY);
	}

	public String cBanque() {
	 return (String) storedValueForKey(C_BANQUE_KEY);
	}

	public void setCBanque(String value) {
	 takeStoredValueForKey(value, C_BANQUE_KEY);
	}

	public String cGuichet() {
	 return (String) storedValueForKey(C_GUICHET_KEY);
	}

	public void setCGuichet(String value) {
	 takeStoredValueForKey(value, C_GUICHET_KEY);
	}

	public String cleRib() {
	 return (String) storedValueForKey(CLE_RIB_KEY);
	}

	public void setCleRib(String value) {
	 takeStoredValueForKey(value, CLE_RIB_KEY);
	}

	public NSTimestamp dCreation() {
	 return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
	 takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
	 return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
	 takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public java.math.BigDecimal fouOrdre() {
	 return (java.math.BigDecimal) storedValueForKey(FOU_ORDRE_KEY);
	}

	public void setFouOrdre(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, FOU_ORDRE_KEY);
	}

	public String iban() {
	 return (String) storedValueForKey(IBAN_KEY);
	}

	public void setIban(String value) {
	 takeStoredValueForKey(value, IBAN_KEY);
	}

	public String modCode() {
	 return (String) storedValueForKey(MOD_CODE_KEY);
	}

	public void setModCode(String value) {
	 takeStoredValueForKey(value, MOD_CODE_KEY);
	}

	public String noCompte() {
	 return (String) storedValueForKey(NO_COMPTE_KEY);
	}

	public void setNoCompte(String value) {
	 takeStoredValueForKey(value, NO_COMPTE_KEY);
	}

	public Integer persIdCreation() {
	 return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public Integer persIdModification() {
	 return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(Integer value) {
	 takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}

	public String ribTitco() {
	 return (String) storedValueForKey(RIB_TITCO_KEY);
	}

	public void setRibTitco(String value) {
	 takeStoredValueForKey(value, RIB_TITCO_KEY);
	}

	public String ribValide() {
	 return (String) storedValueForKey(RIB_VALIDE_KEY);
	}

	public void setRibValide(String value) {
	 takeStoredValueForKey(value, RIB_VALIDE_KEY);
	}

	public String temPayeUtil() {
	 return (String) storedValueForKey(TEM_PAYE_UTIL_KEY);
	}

	public void setTemPayeUtil(String value) {
	 takeStoredValueForKey(value, TEM_PAYE_UTIL_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr fournisUlr() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
	}

	public void setFournisUlrRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOFournisUlr oldValue = fournisUlr();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
	 }
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EOBanque fwkpers_Banque() {
	 return (org.cocktail.fwkcktlpersonne.common.metier.EOBanque)storedValueForKey(FWKPERS__BANQUE_KEY);
	}

	public void setFwkpers_BanqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBanque value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlpersonne.common.metier.EOBanque oldValue = fwkpers_Banque();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FWKPERS__BANQUE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FWKPERS__BANQUE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)storedValueForKey(PRELEVEMENTSES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier) {
	 return prelevementses(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier, boolean fetch) {
	 return prelevementses(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> prelevementses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.RIBFOUR_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = prelevementses();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToPrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
	}
	
	public void removeFromPrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements createPrelevementsesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, PRELEVEMENTSES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements) eo;
	}
	
	public void deletePrelevementsesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTSES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllPrelevementsesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOPrelevements> objects = prelevementses().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deletePrelevementsesRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EORibfourUlr avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EORibfourUlr createEORibfourUlr(EOEditingContext editingContext																																									) {
	 EORibfourUlr eo = (EORibfourUlr) EOUtilities.createAndInsertInstance(editingContext, _EORibfourUlr.ENTITY_NAME);	 
																																							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORibfourUlr creerInstance(EOEditingContext editingContext) {
		EORibfourUlr object = (EORibfourUlr)EOUtilities.createAndInsertInstance(editingContext, _EORibfourUlr.ENTITY_NAME);
  		return object;
		}

	

  public EORibfourUlr localInstanceIn(EOEditingContext editingContext) {
    EORibfourUlr localInstance = (EORibfourUlr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EORibfourUlr>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORibfourUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORibfourUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORibfourUlr> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORibfourUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORibfourUlr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORibfourUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORibfourUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORibfourUlr> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORibfourUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORibfourUlr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORibfourUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORibfourUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORibfourUlr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORibfourUlr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
