/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTypeProduit.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOTypeProduit extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOTypeProduit.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_TypeProduit";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TYPE_PRODUIT";


//Attribute Keys
	public static final ERXKey<String> COMPTE = new ERXKey<String>("compte");
	public static final ERXKey<String> LIB_TYPE_PRODUIT = new ERXKey<String>("libTypeProduit");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> ELEMENT_ASSOCIES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>("elementAssocies");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> FORMULES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>("formules");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> LIGNE_FACTURES = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>("ligneFactures");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> TYPE_TARIFS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>("typeTarifs");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idTypeProduit";

	public static final String COMPTE_KEY = "compte";
	public static final String LIB_TYPE_PRODUIT_KEY = "libTypeProduit";

//Attributs non visibles
	public static final String ID_TYPE_PRODUIT_KEY = "idTypeProduit";

//Colonnes dans la base de donnees
	public static final String COMPTE_COLKEY = "COMPTE";
	public static final String LIB_TYPE_PRODUIT_COLKEY = "LIB_TYPE_PRODUIT";

	public static final String ID_TYPE_PRODUIT_COLKEY = "ID_TYPE_PRODUIT";


	// Relationships
	public static final String ELEMENT_ASSOCIES_KEY = "elementAssocies";
	public static final String FORMULES_KEY = "formules";
	public static final String LIGNE_FACTURES_KEY = "ligneFactures";
	public static final String TYPE_TARIFS_KEY = "typeTarifs";



	// Accessors methods
	public String compte() {
	 return (String) storedValueForKey(COMPTE_KEY);
	}

	public void setCompte(String value) {
	 takeStoredValueForKey(value, COMPTE_KEY);
	}

	public String libTypeProduit() {
	 return (String) storedValueForKey(LIB_TYPE_PRODUIT_KEY);
	}

	public void setLibTypeProduit(String value) {
	 takeStoredValueForKey(value, LIB_TYPE_PRODUIT_KEY);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)storedValueForKey(ELEMENT_ASSOCIES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier) {
	 return elementAssocies(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier, boolean fetch) {
	 return elementAssocies(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> elementAssocies(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.TYPE_PRODUIT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = elementAssocies();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
	}
	
	public void removeFromElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie createElementAssociesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, ELEMENT_ASSOCIES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie) eo;
	}
	
	public void deleteElementAssociesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENT_ASSOCIES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllElementAssociesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie> objects = elementAssocies().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteElementAssociesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> formules() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>)storedValueForKey(FORMULES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> formules(EOQualifier qualifier) {
	 return formules(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> formules(EOQualifier qualifier, boolean fetch) {
	 return formules(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> formules(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule.TYPE_PRODUIT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOFormule.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = formules();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FORMULES_KEY);
	}
	
	public void removeFromFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FORMULES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOFormule createFormulesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FORMULES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOFormule) eo;
	}
	
	public void deleteFormulesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FORMULES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFormulesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> objects = formules().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFormulesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)storedValueForKey(LIGNE_FACTURES_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier) {
	 return ligneFactures(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, boolean fetch) {
	 return ligneFactures(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> ligneFactures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.TYPE_PRODUIT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = ligneFactures();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public void removeFromLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture createLigneFacturesRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, LIGNE_FACTURES_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture) eo;
	}
	
	public void deleteLigneFacturesRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, LIGNE_FACTURES_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllLigneFacturesRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture> objects = ligneFactures().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteLigneFacturesRelationship(objects.nextElement());
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifs() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)storedValueForKey(TYPE_TARIFS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifs(EOQualifier qualifier) {
	 return typeTarifs(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifs(EOQualifier qualifier, boolean fetch) {
	 return typeTarifs(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> typeTarifs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.TYPE_PRODUIT_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = typeTarifs();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToTypeTarifsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, TYPE_TARIFS_KEY);
	}
	
	public void removeFromTypeTarifsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_TARIFS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif createTypeTarifsRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, TYPE_TARIFS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif) eo;
	}
	
	public void deleteTypeTarifsRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_TARIFS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllTypeTarifsRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif> objects = typeTarifs().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteTypeTarifsRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTypeProduit avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTypeProduit createEOTypeProduit(EOEditingContext editingContext									) {
	 EOTypeProduit eo = (EOTypeProduit) EOUtilities.createAndInsertInstance(editingContext, _EOTypeProduit.ENTITY_NAME);	 
							 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeProduit creerInstance(EOEditingContext editingContext) {
		EOTypeProduit object = (EOTypeProduit)EOUtilities.createAndInsertInstance(editingContext, _EOTypeProduit.ENTITY_NAME);
  		return object;
		}

	

  public EOTypeProduit localInstanceIn(EOEditingContext editingContext) {
    EOTypeProduit localInstance = (EOTypeProduit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTypeProduit>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeProduit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeProduit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeProduit> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeProduit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeProduit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeProduit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeProduit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeProduit> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeProduit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeProduit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeProduit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeProduit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeProduit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeProduit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
