/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// DO NOT EDIT.  Make changes to EOTarifsFormule.java instead.
package org.cocktail.fwkcktlinternat.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXEntityClassDescription;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public abstract class _EOTarifsFormule extends  AfwkInternatRecord {

	 private static Logger LOG = Logger.getLogger(_EOTarifsFormule.class);
	 
	public static final String ENTITY_NAME = "FwkInternat_TarifsFormule";
	public static final String ENTITY_TABLE_NAME = "LITCHI.TARIFS_FORMULE";


//Attribute Keys
	public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
	public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
	public static final ERXKey<Long> ID_FORMULE = new ERXKey<Long>("idFormule");
	public static final ERXKey<Integer> NB_JOURS = new ERXKey<Integer>("nbJours");
	public static final ERXKey<java.math.BigDecimal> PRIX_MENSUEL = new ERXKey<java.math.BigDecimal>("prixMensuel");
	public static final ERXKey<java.math.BigDecimal> PRIX_PAR_JOUR = new ERXKey<java.math.BigDecimal>("prixParJour");
// Relationship Keys
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule> FORMULE = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormule>("formule");
	public static final ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> FORMULES_CALENDRIERS = new ERXKey<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier>("formulesCalendriers");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idTarifFormule";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String ID_FORMULE_KEY = "idFormule";
	public static final String NB_JOURS_KEY = "nbJours";
	public static final String PRIX_MENSUEL_KEY = "prixMensuel";
	public static final String PRIX_PAR_JOUR_KEY = "prixParJour";

//Attributs non visibles
	public static final String ID_TARIF_FORMULE_KEY = "idTarifFormule";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "DATE_DEBUT";
	public static final String DATE_FIN_COLKEY = "DATE_FIN";
	public static final String ID_FORMULE_COLKEY = "ID_FORMULE";
	public static final String NB_JOURS_COLKEY = "NB_JOURS";
	public static final String PRIX_MENSUEL_COLKEY = "PRIX_MENSUEL";
	public static final String PRIX_PAR_JOUR_COLKEY = "PRIX_PAR_JOUR";

	public static final String ID_TARIF_FORMULE_COLKEY = "ID_TARIF_FORMULE";


	// Relationships
	public static final String FORMULE_KEY = "formule";
	public static final String FORMULES_CALENDRIERS_KEY = "formulesCalendriers";



	// Accessors methods
	public NSTimestamp dateDebut() {
	 return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
	}

	public void setDateDebut(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_DEBUT_KEY);
	}

	public NSTimestamp dateFin() {
	 return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
	}

	public void setDateFin(NSTimestamp value) {
	 takeStoredValueForKey(value, DATE_FIN_KEY);
	}

	public Long idFormule() {
	 return (Long) storedValueForKey(ID_FORMULE_KEY);
	}

	public void setIdFormule(Long value) {
	 takeStoredValueForKey(value, ID_FORMULE_KEY);
	}

	public Integer nbJours() {
	 return (Integer) storedValueForKey(NB_JOURS_KEY);
	}

	public void setNbJours(Integer value) {
	 takeStoredValueForKey(value, NB_JOURS_KEY);
	}

	public java.math.BigDecimal prixMensuel() {
	 return (java.math.BigDecimal) storedValueForKey(PRIX_MENSUEL_KEY);
	}

	public void setPrixMensuel(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, PRIX_MENSUEL_KEY);
	}

	public java.math.BigDecimal prixParJour() {
	 return (java.math.BigDecimal) storedValueForKey(PRIX_PAR_JOUR_KEY);
	}

	public void setPrixParJour(java.math.BigDecimal value) {
	 takeStoredValueForKey(value, PRIX_PAR_JOUR_KEY);
	}

	public org.cocktail.fwkcktlinternat.serveur.metier.EOFormule formule() {
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOFormule)storedValueForKey(FORMULE_KEY);
	}

	public void setFormuleRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormule value) {
	 if (value == null) {
	 	org.cocktail.fwkcktlinternat.serveur.metier.EOFormule oldValue = formule();
	 	if (oldValue != null) {
	 		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FORMULE_KEY);
	   }
	 } else {
	 	addObjectToBothSidesOfRelationshipWithKey(value, FORMULE_KEY);
	 }
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> formulesCalendriers() {
	 return (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier>)storedValueForKey(FORMULES_CALENDRIERS_KEY);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> formulesCalendriers(EOQualifier qualifier) {
	 return formulesCalendriers(qualifier, null, false);
	}

	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> formulesCalendriers(EOQualifier qualifier, boolean fetch) {
	 return formulesCalendriers(qualifier, null, fetch);
	}

	@SuppressWarnings("unchecked")
	public NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> formulesCalendriers(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
		 NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> results;
				 if (fetch) {
		   EOQualifier fullQualifier;
				   EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier.TARIFS_FORMULE_KEY, EOQualifier.QualifierOperatorEqual, this);
			 	
	   if (qualifier == null) {
	     fullQualifier = inverseQualifier;
	   }
	   else {
	     NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
	     qualifiers.addObject(qualifier);
	     qualifiers.addObject(inverseQualifier);
	     fullQualifier = new EOAndQualifier(qualifiers);
	   }
	
		   results = org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier.fetchAll(editingContext(), fullQualifier, sortOrderings);
		 }
	 else {
		   results = formulesCalendriers();
	   if (qualifier != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
	   }
	   if (sortOrderings != null) {
	     results = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
	   }
		 }
		 return results;
	}
	
	public void addToFormulesCalendriersRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier object) {
	 addObjectToBothSidesOfRelationshipWithKey(object, FORMULES_CALENDRIERS_KEY);
	}
	
	public void removeFromFormulesCalendriersRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FORMULES_CALENDRIERS_KEY);
	}
	
	public org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier createFormulesCalendriersRelationship() {
	 EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName(org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier.ENTITY_NAME);
	 EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	 editingContext().insertObject(eo);
	 addObjectToBothSidesOfRelationshipWithKey(eo, FORMULES_CALENDRIERS_KEY);
	 return (org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier) eo;
	}
	
	public void deleteFormulesCalendriersRelationship(org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier object) {
	 removeObjectFromBothSidesOfRelationshipWithKey(object, FORMULES_CALENDRIERS_KEY);
		 editingContext().deleteObject(object);
		}
	
	public void deleteAllFormulesCalendriersRelationships() {
	 Enumeration<org.cocktail.fwkcktlinternat.serveur.metier.EOFormulesCalendrier> objects = formulesCalendriers().immutableClone().objectEnumerator();
	 while (objects.hasMoreElements()) {
	   deleteFormulesCalendriersRelationship(objects.nextElement());
	 }
	}


	/**
	* Créer une instance de EOTarifsFormule avec les champs et relations obligatoires et l'insere dans l'editingContext.
	*/
	public static  EOTarifsFormule createEOTarifsFormule(EOEditingContext editingContext																			) {
	 EOTarifsFormule eo = (EOTarifsFormule) EOUtilities.createAndInsertInstance(editingContext, _EOTarifsFormule.ENTITY_NAME);	 
																	 return eo;
	}
	



	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. 
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTarifsFormule creerInstance(EOEditingContext editingContext) {
		EOTarifsFormule object = (EOTarifsFormule)EOUtilities.createAndInsertInstance(editingContext, _EOTarifsFormule.ENTITY_NAME);
  		return object;
		}

	

  public EOTarifsFormule localInstanceIn(EOEditingContext editingContext) {
    EOTarifsFormule localInstance = (EOTarifsFormule)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }



	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
	     return fetchAll(editingContext, qualifier, sortOrderings, distinct, 0);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int limit) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, limit);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		    EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct, limit);
		    @SuppressWarnings("unchecked")
		    NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule> eoObjects = (NSArray<org.cocktail.fwkcktlinternat.serveur.metier.EOTarifsFormule>)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {	  
		  return fetchSpecification(qualifier, sortOrderings, distinct, 0);
	  }
	  
	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @param limit
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, int limit) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  if (limit>0) fetchSpec.setFetchLimit(limit);
		  return fetchSpec;
	  }
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTarifsFormule fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTarifsFormule fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTarifsFormule> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTarifsFormule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTarifsFormule)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTarifsFormule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTarifsFormule fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTarifsFormule> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTarifsFormule eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTarifsFormule)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTarifsFormule fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTarifsFormule eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTarifsFormule ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTarifsFormule fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	  

	public int maxLengthForAttribute(String attributeName) {
		EOClassDescription cd = classDescription();
		if (cd instanceof ERXEntityClassDescription) {
			ERXEntityClassDescription ecd = (ERXEntityClassDescription) cd;
			EOAttribute attribute = ecd.entity().attributeNamed(attributeName);
			return attribute.width();
		}
		return -1;
	}
}
