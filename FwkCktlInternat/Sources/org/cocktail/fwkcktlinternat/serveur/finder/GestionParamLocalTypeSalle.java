/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktlinternat.serveur.metier.EOLocal;
import org.cocktail.fwkcktlinternat.serveur.metier.EOParamLocal;
import org.cocktail.fwkcktlinternat.serveur.metier.EOParamLocalTypeSalle;
import org.cocktail.fwkcktlinternat.serveur.metier.EOSalle;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeSalle;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class GestionParamLocalTypeSalle {
     /**
      * 
      *  
      */
	public static NSArray<String> getEtages(EOEditingContext myContext, EOLocal leLocal){
		return getEtages(myContext, new NSArray<EOLocal>(leLocal));
	}
	
	public static NSArray<String> getEtages(EOEditingContext myContext, NSArray<EOLocal> lesLocaux){
		NSMutableArray<String> etages = new NSMutableArray<String>();
		if (lesLocaux==null) return etages;
		if (lesLocaux.size()==0) return etages;  
		String autresLocaux = new String();
		for (int i=1; i<lesLocaux.size();i++)
		autresLocaux += " OR GRHUM.SALLES.C_LOCAL='"
			         + ERXEOControlUtilities.primaryKeyStringForObject(lesLocaux.get(i))
			         + "'";
		/*
		 *  Recherche les etages d'un local
		 */
	    String sql ="SELECT DISTINCT SAL_ETAGE FROM GRHUM.SALLES  WHERE GRHUM.SALLES.C_LOCAL='"
	    	  +ERXEOControlUtilities.primaryKeyStringForObject(lesLocaux.get(0))+"' " +autresLocaux +" ORDER BY SAL_ETAGE ASC";
	    NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	    if((rows != null) && (rows.count()>0)) {
	             Enumeration e = rows.objectEnumerator();
	             while(e.hasMoreElements()){
		               NSDictionary dic = (NSDictionary) e.nextElement();
		               etages.add((String) dic.objectForKey("SAL_ETAGE"));
	             }
	    }
	    return etages;
	}
		
	 /**
       * 
       * @param edc
       * @return
       */
	  /* Les locaux */
	  public static NSArray<EOLocal> lesLocaux (EOEditingContext edc){
		    NSMutableArray<EOLocal> locaux = new NSMutableArray<EOLocal>();
	    	NSArray<EOParamLocal> PLocaux = EOParamLocal.fetchAll(edc);
	    	for (int i=0;i<PLocaux.size();i++) locaux.add(PLocaux.get(i).local());
	    	return locaux;
	    }
	  
	  /* Les salles */
	  public static NSArray<EOSalle> lesSallesDuLocal(EOEditingContext edc, EOLocal leLocal) {
			 NSArray <EOTypeSalle> selection = GestionParamLocalTypeSalle.lesTypesSallesLocal(edc, leLocal);  
		     EOQualifier qual = null;
			 for (int j=0; j<selection.size(); j++) {
				   qual = ERXQ.or(qual, ERXQ.equals(EOSalle.TYPE_SALLE_KEY,selection.get(j)));
			 } 
			 qual = ERXQ.and(qual, ERXQ.equals(EOSalle.LOCAL_KEY,leLocal), ERXQ.isNotNull(EOSalle.SAL_RESERVABLE_KEY), ERXQ.isNull(EOSalle.D_ANNULATION_KEY));
			 NSArray <EOSalle> desSalles =  EOSalle.fetchAll(edc, qual, ERXS.asc(EOSalle.SAL_PORTE_KEY).array());
			 
		  return desSalles;
	  }
	  
	  public static NSArray<EOSalle> lesSallesDesLocaux(EOEditingContext edc) {
		  NSMutableArray<EOSalle> lesSalles = null;
		  NSArray<EOLocal> locaux = lesLocaux(edc);	  
		  for (int i=0; i<locaux.size();i++) {
			 NSArray <EOSalle> desSalles =  lesSallesDuLocal(edc, locaux.get(i));
			 if (!desSalles.isEmpty()) {
				if (lesSalles==null) lesSalles = new  NSMutableArray<EOSalle>();
			    lesSalles.addObjectsFromArray(desSalles);
			 }
		 }	 		  
		 return lesSalles;
	  }
	  
	  private static void deleteLocal(EOEditingContext edc,EOLocal unLocal) {   
	   deleteFromLocalTypeSalle(edc,unLocal);
	   String pkl = ERXEOControlUtilities.primaryKeyStringForObject(unLocal);
   	   String sql = "DELETE FROM LITCHI.PARAM_LOCAL WHERE C_LOCAL='"+pkl+"'";
   	   try { ERXEOAccessUtilities.evaluateSQLWithEntityNamed(edc, EOParamLocal.ENTITY_NAME, sql); 
         } catch (Exception e) {e.printStackTrace();};
	  }
	  
	  private static void addLocal(EOEditingContext edc, EOLocal unLocal) {
		  String pkl = ERXEOControlUtilities.primaryKeyStringForObject(unLocal);
		  String sql = "INSERT INTO LITCHI.PARAM_LOCAL (C_lOCAL) VALUES ('"+pkl+"')";   		
		  try { ERXEOAccessUtilities.evaluateSQLWithEntityNamed(edc, EOParamLocal.ENTITY_NAME, sql); 
		    		   } catch (Exception e) {e.printStackTrace();}; 
	 }
     
	  public static void setSelectionLocal(EOEditingContext edc, NSArray<EOLocal> selectionLocaux) {
	   /* Ajoute ce qui doit être ajoute et supprime ce qui doit l'être */ 
	   NSMutableArray<EOLocal> locaux = new NSMutableArray<EOLocal>();
   	   NSArray<EOParamLocal> PLocaux = EOParamLocal.fetchAll(edc);
   	   for (int i=0;i<PLocaux.size();i++) locaux.add(PLocaux.get(i).local());
   	   for (int i=0;i<selectionLocaux.size();i++) {
		   if (!locaux.containsObject(selectionLocaux.get(i))) {
			   addLocal(edc, selectionLocaux.get(i));
		   }
	   }
	   for (int i=0;i<locaux.size();i++) {
		   if (!selectionLocaux.containsObject(locaux.get(i))) {
			   deleteLocal(edc,PLocaux.get(i).local());
		   }
	   }    	  
	  }
	  
     /* Les types de salles */
	  
	  public static NSArray<EOTypeSalle> lesTypesSallesLocal (EOEditingContext edc, EOLocal unLocal){
	    	NSMutableArray<EOTypeSalle> TypeSallelocal = new NSMutableArray<EOTypeSalle>();
	    	EOQualifier qual = ERXQ.equals(EOParamLocalTypeSalle.LOCAL_KEY, unLocal);
	    	NSArray<EOParamLocalTypeSalle> PLTS =  EOParamLocalTypeSalle.fetchAll(edc, qual, null);
	    	for (int i=0;i<PLTS.size();i++) TypeSallelocal.add(PLTS.get(i).typeSalle());
	    	return TypeSallelocal;
	    }
	   
	  private static void deleteFromLocalTypeSalle(EOEditingContext edc,EOLocal unLocal) {
		  String pkl = ERXEOControlUtilities.primaryKeyStringForObject(unLocal);
    	  String sql = "DELETE FROM LITCHI.PARAM_LOCAL_TYPE_SALLE WHERE C_LOCAL='"+pkl+"'";
    	  try { ERXEOAccessUtilities.evaluateSQLWithEntityNamed(edc, EOParamLocalTypeSalle.ENTITY_NAME, sql); 
          } catch (Exception e) {e.printStackTrace();};
	    }
	    
	  private static void deleteTypeSalle(EOEditingContext edc, EOLocal unLocal, EOTypeSalle unTypeSalle) {
	    	String pkl = ERXEOControlUtilities.primaryKeyStringForObject(unLocal);
	        String pkts = ERXEOControlUtilities.primaryKeyStringForObject(unTypeSalle);
	    	String sql = "DELETE FROM LITCHI.PARAM_LOCAL_TYPE_SALLE WHERE C_LOCAL='"+pkl+"' AND TSAL_NUMERO="+pkts;
	    	try { ERXEOAccessUtilities.evaluateSQLWithEntityNamed(edc, EOParamLocalTypeSalle.ENTITY_NAME, sql); 
	        } catch (Exception e) {e.printStackTrace();};	          
	    }
	  
	  private static void addTypeSalle(EOEditingContext edc, EOLocal unLocal, EOTypeSalle unTypeSalle) {	
	    String pkl = ERXEOControlUtilities.primaryKeyStringForObject(unLocal);
	    String pkts = ERXEOControlUtilities.primaryKeyStringForObject(unTypeSalle);
	    String sql = "INSERT INTO LITCHI.PARAM_LOCAL_TYPE_SALLE (C_lOCAL,TSAL_NUMERO) VALUES ('"+pkl+"',"+pkts+")";
	    try { ERXEOAccessUtilities.evaluateSQLWithEntityNamed(edc, EOParamLocalTypeSalle.ENTITY_NAME, sql); 
	    		   } catch (Exception e) {e.printStackTrace();};
	    }
	  
	  public static void setSelectionLocalTypeSalle(EOEditingContext edc, EOLocal unLocal, NSArray<EOTypeSalle> selectionTypeSalle) {
		   /* Ajoute ce qui doit être ajoute et supprime ce qui doit l'être */ 
		    NSMutableArray<EOTypeSalle> TypeSallelocal = new NSMutableArray<EOTypeSalle>();
	    	EOQualifier qual = ERXQ.equals(EOParamLocalTypeSalle.LOCAL_KEY, unLocal);
	    	NSArray<EOParamLocalTypeSalle> PLTS =  EOParamLocalTypeSalle.fetchAll(edc, qual, null);
	    	for (int i=0;i<PLTS.size();i++) TypeSallelocal.add(PLTS.get(i).typeSalle());
	   	    for (int i=0;i<selectionTypeSalle.size();i++) {
			   if (!TypeSallelocal.containsObject(selectionTypeSalle.get(i))) {
				   addTypeSalle(edc, unLocal, selectionTypeSalle.get(i));
			   }
		    }
		    for (int i=0;i<TypeSallelocal.size();i++) {
			   if (!selectionTypeSalle.containsObject(TypeSallelocal.get(i))) {
				   deleteTypeSalle(edc,unLocal,PLTS.get(i).typeSalle());
			   }
		    }    	  
		  }  
}