package org.cocktail.fwkcktlinternat.serveur.finder;

import java.util.List;

import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class FinderTypeFacture {

	public static List<EOTypeFacture> typesFacturesDisponiblesPourPrelevementSEPA(EOEditingContext edc) {
    	EOQualifier typeFactureRestauration = ERXQ.equals(EOTypeFacture.LIB_TYPE_FACTURE.key(), EOTypeFacture.RESTAURATION_KEY);
    	EOQualifier typeFactureHebergement = ERXQ.equals(EOTypeFacture.LIB_TYPE_FACTURE.key(), EOTypeFacture.HEBERGEMENT_KEY);
    	
    	return EOTypeFacture.fetchAll(edc, ERXQ.or(typeFactureRestauration, typeFactureHebergement));
	}
	
}
