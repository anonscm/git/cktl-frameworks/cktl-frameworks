/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlinternat.serveur.metier.EOElementAssocie;
import org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeFacture;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

public class FinderFacturation {
	
	public static void effacerBrouillon (EOEditingContext myContext, EOTypeFacture leTypeFacture ) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(leTypeFacture)),"001idTypeFacture");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_effacerBrouillon", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void effacerUnBrouillon (EOEditingContext myContext, EOEnteteFacture laFacture) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(laFacture)),"001idFacture");
	
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_effacerUnBrouillon", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}

	public static void facturerArrhes (EOEditingContext myContext, EOElementAssocie ea, Integer persId, NSTimestamp dateEch ) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(ea)),"001codeElementAssocie");
		leDic.takeValueForKey(persId,"002persidCreateur");
		leDic.takeValueForKey(dateEch,"003dateEcheance");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_facturerArrhes", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void facturerLocationMensuelle (EOEditingContext myContext, Integer persIdCreateur,  NSTimestamp dateDuMois, NSTimestamp dateEch, Integer bArrhes, Integer bCAF, Integer bAvances ) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(persIdCreateur,"001persidCreateur");
		leDic.takeValueForKey(dateDuMois,"002dateDuMois");
		leDic.takeValueForKey(dateEch,"003dateEcheance");
		if (bArrhes==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"004bArrhes"); else leDic.takeValueForKey(bArrhes,"004bArrhes");
		if (bCAF==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"005bCAF"); else leDic.takeValueForKey(bCAF,"005bCAF");
		if (bAvances==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"006bAvances"); else leDic.takeValueForKey(bAvances,"006bAvances");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_facturerLocationMensuelle", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void facturerRestoMensuelle (EOEditingContext myContext, Integer persIdCreateur,  NSTimestamp dateDuMois, NSTimestamp dateEch,  Integer bAvances )  {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(persIdCreateur,"001persidCreateur");
		leDic.takeValueForKey(dateDuMois,"002dateDuMois");
		leDic.takeValueForKey(dateEch,"003dateEcheance");
		if (bAvances==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"004bAvances"); else leDic.takeValueForKey(bAvances,"004bAvances");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_facturerRestoMensuelle", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static Long facturerUneLocation (EOEditingContext myContext, EOHebergement lHebergement, Integer persIdCreateur,  NSTimestamp dateFinFacturation, NSTimestamp dateEch, Integer bArrhes, Integer bCAF, Integer bAvances )  {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		Long idFacture = null;
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(lHebergement)),"001idHebergement");
		leDic.takeValueForKey(persIdCreateur,"002persidCreateur");
		leDic.takeValueForKey(dateFinFacturation,"003dateFinFacturation");
		leDic.takeValueForKey(dateEch,"004dateEcheance");
		if (bArrhes==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"005bArrhes"); else leDic.takeValueForKey(bArrhes,"005bArrhes");
		if (bCAF==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"006bCAF"); else leDic.takeValueForKey(bCAF,"006bCAF");
		if (bAvances==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"007bAvances"); else leDic.takeValueForKey(bAvances,"007bAvances");
		leDic.takeValueForKey(new Long(0),"008idFacture");
		
		NSDictionary<String,Object> res=null;
		try {
		     res = EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_facturerUneLoc", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }   
		if (res!=null)	 
	         idFacture = ( res.valueForKey("008idFacture") == NSKeyValueCoding.NullValue ) ? null : (Long)( res.valueForKey("008idFacture")); 
		
		return  idFacture;
	}
	
	public static Long facturerUnResto  (EOEditingContext myContext, Integer persId, Integer persIdCreateur,  NSTimestamp dateFinFacturation, NSTimestamp dateEch, Integer bAvances )  {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		Long idFacture = null;
		
	    leDic.takeValueForKey(persId,"001persId");
		leDic.takeValueForKey(persIdCreateur,"002persidCreateur");
		leDic.takeValueForKey(dateFinFacturation,"003dateFinFacturation");
		leDic.takeValueForKey(dateEch,"004dateEcheance");
		if (bAvances==null) leDic.takeValueForKey(NSKeyValueCoding.NullValue,"005bAvances"); else leDic.takeValueForKey(bAvances,"005bAvances");
		leDic.takeValueForKey(new Long(0),"006idFacture");
		
		NSDictionary<String,Object> res=null;
		try {
		     res = EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_facturerUnResto", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); } 
		
		if (res!=null)	 
	         idFacture = ( res.valueForKey("006idFacture") == NSKeyValueCoding.NullValue ) ? null : (Long)( res.valueForKey("006idFacture")); 
		
		return idFacture;
		
	}
	
	public static void validerFacturerArrhes (EOEditingContext myContext, EOElementAssocie ea, Integer persIdValidateur ) {
        NSMutableDictionary leDic = new NSMutableDictionary ();
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(ea)),"001codeElementAssocie");
		leDic.takeValueForKey(persIdValidateur,"002persidValidateur");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_validationFacturationArrhes", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void validerFacturation (EOEditingContext myContext, EOTypeFacture leTypeFacture, Integer persIdValidateur ) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(leTypeFacture)),"001idTypeFacture");
		leDic.takeValueForKey(persIdValidateur,"002persidValidateur");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_validerFacturation", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void validerUneFacture (EOEditingContext myContext, Long idFacture, Integer persIdValidateur ) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(idFacture,"001idFacture");
		leDic.takeValueForKey(persIdValidateur,"002persidValidateur");		

		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_validerUneFacture", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void effacerReglement (EOEditingContext myContext, Long idReglement, Integer persIdEffaceur) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		leDic.takeValueForKey(idReglement,"001idReglement");
		leDic.takeValueForKey(persIdEffaceur,"002persidEffaceur");		

		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_effacerReglement", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	public static void preAffectaterLaCAF (EOEditingContext myContext) {
		NSMutableDictionary leDic = new NSMutableDictionary ();	
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_preAffectationCAF", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
	
	/**
	 * A PAYER
	 * ACQUITTE
	 * RETARD
	 * PARTIEL
	 * AVOIR
	 * BROUILLON
	 * ABANDONNE = abandonne version élégante de l'annulation ?
	 * TRESORIE  = paye mais non utilisable ...
	 * REMBOURSE = ce qui a été TRESORIE et remboursé ou un avoir remboursé !
	 * */
	static NSTimestamp dateDuJour = new NSTimestamp();
	
	public static String A_PAYER_KEY   =  "A PAYER";
	public static String ACQUITTE_KEY  =  "ACQUITTE";
	public static String RETARD_KEY    =  "RETARD";
	public static String PARTIEL_KEY   =  "PARTIEL";
	public static String AVOIR_KEY     =  "AVOIR";
	public static String BROUILLON_KEY =  "BROUILLON";
	public static String ABANDONNE_KEY =  "ABANDONNE";
	public static String TRESORIE_KEY  =  "TRESORIE";
	public static String REMBOURSE_KEY  =  "REMBOURSE";
	
	public static  NSArray <String> listeStatutFacture() {
		NSMutableArray <String> list = new NSMutableArray <String>(); 
		
		list.add(A_PAYER_KEY);
		list.add(RETARD_KEY);
		list.add(PARTIEL_KEY);
		list.add(ACQUITTE_KEY);
		// list.add(AVOIR_KEY); -- c'est d'abord un type de facture
		list.add(TRESORIE_KEY);
		list.add(REMBOURSE_KEY);
		list.add(ABANDONNE_KEY);
		list.add(BROUILLON_KEY);
		return list.immutableClone();
	}
	
	public static  String statutFacture(EOEnteteFacture uneFacture) {
		if (uneFacture==null) return null;
		if (uneFacture.dateRemboursement()!=null) return REMBOURSE_KEY;
		if (uneFacture.indicationAbandon()!=null && uneFacture.indicationAbandon()) {
			if (uneFacture.reglementCount()>0) return TRESORIE_KEY;  
			return ABANDONNE_KEY;
		}
		if (uneFacture.idTypeFacture()==3) return AVOIR_KEY;
		if (uneFacture.numFacture()==null) return BROUILLON_KEY;
		
		BigDecimal sommePaye = calculerSommePayeFacture(uneFacture);
		if (sommePaye!=null) {
		         if (sommePaye.equals(uneFacture.mntAPayerTtc())) return ACQUITTE_KEY;
		         else return PARTIEL_KEY + " ("+ sommePaye +")";
		}
		
		if (sommePaye==null && uneFacture.mntAPayerTtc().compareTo(new BigDecimal(0))==0) return ACQUITTE_KEY;
		
		if (uneFacture.dateEcheance().before(dateDuJour)) return RETARD_KEY;
		return A_PAYER_KEY;
	}
	
    public static  String statutFacture(EOLigneFacture uneLigneFacture) {	
		return statutFacture(uneLigneFacture.enteteFacture());
	}
    public static  String statutPaiementFacture(EOEnteteFacture uneFacture) {	
    	if (uneFacture==null) return null;
		if (uneFacture.numFacture()==null) return "BROUILLON";
		if (uneFacture.dateRemboursement()!=null) return "REMBOURSE LE " + Toolbox.DateDuJour(uneFacture.dateRemboursement());
		String s = null; 
		BigDecimal sommePaye = calculerSommePayeFacture(uneFacture);
	
		if (sommePaye!=null) {
		     if (sommePaye.equals(uneFacture.mntAPayerTtc())) {
		        	 s = "PAYE";
		         }
		         else {
		        	  s = "PAYE PARTIELLEMENT ("+ sommePaye +")";
		         }
		    for (int i = 0; i<uneFacture.lignesReglements().size(); i++) {
		       s += " LE  " + Toolbox.DateDuJour(uneFacture.lignesReglements().get(i).dateReglementLigne());
		       s += " PAR " + uneFacture.lignesReglements().get(i).reglements().typeReglement().libTypeReglement();
		    }
		} else {
			if (uneFacture.indicationAbandon()!=null && uneFacture.indicationAbandon()) s = "AUCUN PAIEMENT";
			else {
				if (uneFacture.mntAPayerTtc().compareTo(new BigDecimal(0))==0) return "PAYE";
				if (uneFacture.dateEcheance().before(dateDuJour)) return "EN ATTENTE DE PAIEMENT";
			}
		}
		return s;
	}
    public static  String statutPaiementFacture(EOLigneFacture uneLigneFacture) {	
		return statutPaiementFacture(uneLigneFacture.enteteFacture());
	}
    
    public static boolean isFactureAPayer (EOEnteteFacture uneFacture) {
    	if (uneFacture==null) return false;
    	if (uneFacture.indicationAbandon()!=null && uneFacture.indicationAbandon()) return false;
    	if (uneFacture.idTypeFacture()==3) return false;
		if (uneFacture.numFacture()==null) return false;
		
		BigDecimal sommePaye = calculerSommePayeFacture(uneFacture);
		if (sommePaye!=null) {
		         if (sommePaye.equals(uneFacture.mntAPayerTtc())) return false;
		         else return true;
		} else if (uneFacture.mntAPayerTtc().compareTo(new BigDecimal(0))==0) return false;
    	return true;
    }
	
	public static BigDecimal calculerSommePayeFacture(EOEnteteFacture uneFacture) {
		if (uneFacture==null) return null;
		/*
		if (uneFacture.indicationAbandon()!=null && uneFacture.indicationAbandon()) return null;
    	if (uneFacture.idTypeFacture()==3) return null;
		if (uneFacture.numFacture()==null) return null;
		
		if (uneFacture.lignesReglements()!=null && uneFacture.lignesReglements().size()>0) {
			   BigDecimal montant = new BigDecimal(0);
			   for (int i=0; i<uneFacture.lignesReglements().size(); i++)
				    montant = montant.add(uneFacture.lignesReglements().get(i).montantTtc());
			   return montant;
		} else return null;
		*/

		if (uneFacture.reglementCount()==0) return null;
		return uneFacture.sommePaye();
	}
    /**
     * 
     * @param myContext
     * @param unTypeFacture
     * @return
     */
	public static int getCountBrouillon(EOEditingContext myContext, EOTypeFacture unTypeFacture){	
		
                Integer counter = 0;
                
			    String sql ="SELECT COUNT(*) AS RESULTAT FROM LITCHI.ENTETE_FACTURE  WHERE LITCHI.ENTETE_FACTURE.NUM_FACTURE IS NULL";
			    if (unTypeFacture!=null)
			    	sql += " AND LITCHI.ENTETE_FACTURE.ID_TYPE_FACTURE = " + ERXEOControlUtilities.primaryKeyStringForObject(unTypeFacture);
			    
			    NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
			    if((rows != null) && (rows.count()>0)) {
			             Enumeration e = rows.objectEnumerator();
			             while(e.hasMoreElements()){
				               NSDictionary dic = (NSDictionary) e.nextElement();
				               counter = Integer.valueOf((String) dic.objectForKey("RESULTAT"));
			             }
			    }
			    return counter;
   }
	
   public static Double getAPLFacture(EOEditingContext myContext, EOEnteteFacture uneFacture) {  
	   String sql= null;
	    if (uneFacture!=null) 
	    	sql = "SELECT SUM(AC.MONTANT) AS MONTANT_TOTAL FROM LITCHI.AFFECTATION_CAF AC WHERE AC.ID_FACTURE = ";
	    	sql += ERXEOControlUtilities.primaryKeyStringForObject(uneFacture);
	    
	        NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	       if((rows != null) && (rows.count()>0)) {
	             Enumeration e = rows.objectEnumerator();
	             while(e.hasMoreElements()){
		               NSDictionary dic = (NSDictionary) e.nextElement();
		               if (dic.objectForKey("MONTANT_TOTAL")==NSKeyValueCoding.NullValue) return null;
		               return (Double) dic.objectForKey("MONTANT_TOTAL");
	             }
	    }  
	 return null;   
   }
   
   public static Double getArrhesFacture(EOEditingContext myContext, EOEnteteFacture uneFacture) {  
	    String sql= null;
	    if (uneFacture!=null) 
	    	sql = "SELECT SUM(AR.MONTANT) AS MONTANT_TOTAL FROM LITCHI.AFFECTATION_ARRHES AR WHERE AR.ID_FACTURE = ";
	    	sql += ERXEOControlUtilities.primaryKeyStringForObject(uneFacture);
	    
	        NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	       if((rows != null) && (rows.count()>0)) {
	             Enumeration e = rows.objectEnumerator();
	             while(e.hasMoreElements()){
		               NSDictionary dic = (NSDictionary) e.nextElement();
		               if (dic.objectForKey("MONTANT_TOTAL")==NSKeyValueCoding.NullValue) return null;
		               return (Double) dic.objectForKey("MONTANT_TOTAL");
	             }
	    }  
        return null;   
   }
   
   public static Double getAvancesFacture(EOEditingContext myContext, EOEnteteFacture uneFacture) {
	    String sql= null;
	    if (uneFacture!=null) 
	    	sql = "SELECT SUM(AV.MONTANT) AS MONTANT_TOTAL FROM LITCHI.AFFECTATION_AVANCES AV WHERE AV.ID_FACTURE = ";
	    	sql += ERXEOControlUtilities.primaryKeyStringForObject(uneFacture);
	    
	        NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	       if((rows != null) && (rows.count()>0)) {
	             Enumeration e = rows.objectEnumerator();
	             while(e.hasMoreElements()){
		               NSDictionary dic = (NSDictionary) e.nextElement();
		               if (dic.objectForKey("MONTANT_TOTAL")==NSKeyValueCoding.NullValue) return null;
		               return (Double) dic.objectForKey("MONTANT_TOTAL");
	             }
	    } 
	  return null;   
   }
}
