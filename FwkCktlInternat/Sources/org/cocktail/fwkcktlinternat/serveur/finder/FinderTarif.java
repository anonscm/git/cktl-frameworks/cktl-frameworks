package org.cocktail.fwkcktlinternat.serveur.finder;

/**
 * 
 * @author rlouissidney
 *
 */

import java.math.BigDecimal;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTarif;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTva;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeCalcul;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTarif;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeTva;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class FinderTarif {
	 /**
     * 
     * @param myContext
     * @param tarif
     * @param calcul
     * @param dateCalcul
     * @return
     */
     
    public String  getPrixTTC(EOEditingContext myContext, EOTypeTarif tarif, EOTypeCalcul calcul, NSTimestamp dateCalcul) {
    	
    	try {
    	if (myContext==null || tarif==null || calcul==null || dateCalcul==null) return null;
    	/* 1=mensuel 2=forfait 3=journalier */
    	long idCalcul = Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(calcul));
    	NSArray<EOTarif> tarifs = getTarifCalculDate(myContext, tarif, calcul, dateCalcul);
        if (tarifs.size()==0) return null;
    	if (tarifs.size()>1 && idCalcul!=2) return tarifs.size()+" tarifs trouvés !";
        BigDecimal montant = null;
        BigDecimal charges = null;
        
    	if ( idCalcul==1 || idCalcul==3) {
    		montant = tarifs.get(0).prix();
    		if (montant==null) return "Prix non trouvé sur tarif !";
    		
    		if (tarif.leTypeTva()!=null) {
    		    BigDecimal valeurTva = getMultTva(myContext, tarif, dateCalcul);
    		    if (valeurTva!=null) {
    			    if (valeurTva.compareTo(BigDecimal.ZERO)>=0) { 
    		                	montant = montant.multiply(valeurTva);
    			    } else return valeurTva.abs()+" valeurs de TVA trouvés"; 
    		    } else return "Valeur de TVA non trouvé";
    	   }
    		
    	} else {
    		return "Forfait, " +tarifs.size()+ " plage(s) de tarifs";
    	}
    	
    	if (tarif.leTypeTarifPere()==null) {
    		// cherchons les fils
    		for (int i=0;i<tarif.typeTarifFils().size();i++) {
    			NSArray<EOTarif> lesTarifsFils = getTarifCalculDate(myContext, tarif.typeTarifFils().get(i), calcul, dateCalcul);
    			
    			if (lesTarifsFils.size()>1) return lesTarifsFils.size()+" tarifs fils trouvés !";
    			if (lesTarifsFils.size()==1) {
    				if (charges==null) charges = BigDecimal.ZERO;
    				if (lesTarifsFils.get(0).prix()==null) return ("Prix tarif fils vide");
    				charges = charges.add(lesTarifsFils.get(0).prix());
    				// chercher TVA
    				if (tarif.typeTarifFils().get(i).leTypeTva()!=null) {
    				   BigDecimal valeurTva = getMultTva(myContext, tarif.typeTarifFils().get(i), dateCalcul);
    				   if (valeurTva!=null) {
    				     if (valeurTva.compareTo(BigDecimal.ZERO)>=0) { 
		                   charges = charges.multiply(valeurTva);
			             } else return valeurTva.abs()+" valeurs de TVA trouvés sur un tarif fils"; 
		               } else return "Valeur de TVA non trouvé sur un tarif fils";				
    			    }
    		   }
    	  }  
    	}
    	
    	if (montant==null) return null;   	
    	BigDecimal montantTotal = montant;
    	if (charges!=null) montantTotal=montant.add(charges);
    	String result = montantTotal.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
    	if (charges!=null) {
    		result += " (montant="+montant.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString()
    			   +  " Charges="+charges.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString()+")";
    	}
    	return result;
    	
    	} catch (Exception e) {
			return null;
		}
    }
   /**
    * 
    * @param myContext
    * @param tarif
    * @param dateCalcul
    * @return la valeur de la tva plus un
    */
     private BigDecimal getMultTva(EOEditingContext myContext, EOTypeTarif tarif, NSTimestamp dateCalcul) {
    	if (tarif==null) return null;
    	EOTypeTva typeTva = tarif.leTypeTva();
        if (typeTva == null) return null; 
         
        EOQualifier tvaQual = ERXQ.equals(EOTva.TYPE_TVA_KEY, typeTva);
     	EOQualifier dateDebutQual = ERXQ.or(ERXQ.isNull(EOTva.DATE_DEBUT_KEY),ERXQ.lessThanOrEqualTo(EOTva.DATE_DEBUT_KEY, dateCalcul));
     	EOQualifier dateFinQual = ERXQ.or(ERXQ.isNull(EOTva.DATE_FIN_KEY),ERXQ.greaterThanOrEqualTo(EOTva.DATE_FIN_KEY, dateCalcul));
     	EOQualifier qual = ERXQ.and(tvaQual, dateDebutQual, dateFinQual);
         
     	NSArray<EOTva> lesTvas = EOTva.fetchAll(myContext, qual);
     	
     	if (lesTvas!=null && lesTvas.size()==1 && lesTvas.get(0).taux()!=null) {
     		BigDecimal cent = new BigDecimal(100);
     	    return cent.add(lesTvas.get(0).taux()).divide(cent); 
     	}
     	
     	if (lesTvas!=null && lesTvas.size()>1) return new BigDecimal(-lesTvas.size()); 
     	
    	return null;
     }
     
    /**
     * 
     * @param myContext
     * @param tarif
     * @param calcul
     * @param dateCalcul
     * @return une liste de tarif
     */
     
    private NSArray<EOTarif>  getTarifCalculDate(EOEditingContext myContext, EOTypeTarif tarif, EOTypeCalcul calcul, NSTimestamp dateCalcul) {
    	if (myContext==null || tarif==null || calcul==null || dateCalcul==null) return new NSArray<EOTarif>();  
    	EOQualifier tarifCalculQual = ERXQ.and(ERXQ.equals(EOTarif.TYPE_TARIF_KEY, tarif), ERXQ.equals(EOTarif.TYPE_CALCUL_KEY, calcul));
    	EOQualifier dateDebutQual = ERXQ.or(ERXQ.isNull(EOTarif.DATE_DEBUT_KEY),ERXQ.lessThanOrEqualTo(EOTarif.DATE_DEBUT_KEY, dateCalcul));
    	EOQualifier dateFinQual = ERXQ.or(ERXQ.isNull(EOTarif.DATE_FIN_KEY),ERXQ.greaterThanOrEqualTo(EOTarif.DATE_FIN_KEY, dateCalcul));
    	EOQualifier qual = ERXQ.and(tarifCalculQual, dateDebutQual, dateFinQual);
    	
    	NSArray<EOTarif> lesTarifs = EOTarif.fetchAll(myContext, qual);
    	
    	if (lesTarifs==null)  return new NSArray<EOTarif>();
    	
    	return lesTarifs;
    }
}
