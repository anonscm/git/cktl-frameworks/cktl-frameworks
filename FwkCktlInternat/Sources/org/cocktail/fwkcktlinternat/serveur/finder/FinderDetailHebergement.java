/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import java.math.BigDecimal;

import org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOEnteteFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EOHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOLigneFacture;
import org.cocktail.fwkcktlinternat.serveur.metier.EORepartDetailHebergementElementAssocie;
import org.cocktail.fwkcktlinternat.serveur.metier.EORestauration;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class FinderDetailHebergement {
	
	/**
	 *  
	 * @param edc
	 * @param unDetailHebergement
	 * @param debut
	 * @param fin
	 * @return
	 */
	
	 /**
	  * 
	  * Retourne si il y a le même objet en location/reservation durant la période 
	  * Si retour == null la recherche n'est pas possible
	  * Si le retour est vide c'est ok sinon il retourne les dates maximum. 
	  * Location = remise cle != null && retour cle==null Reservation   remise cle == null && retour cle==null
	  * Introduction du nombre de jour de carence entre deux locations.
	  * 
	  */
	static public NSArray<NSTimestamp> unDetailHebergementControleDate (EOEditingContext edc, EODetailHebergement unDetailHebergement ,NSTimestamp debut, NSTimestamp fin, Integer nbJourCarence) {
		
		if (unDetailHebergement==null) return null;
		if (unDetailHebergement.dateRetourCles()!=null) return null;
		if (fin==null || debut==null) return null;
		if (fin.before(debut)) return null;
		if  (nbJourCarence == null) nbJourCarence = new Integer(0);
		
		NSTimestamp dfpn = Toolbox.opJoursDate(fin,nbJourCarence);
		NSTimestamp ddmn = Toolbox.opJoursDate(debut,-nbJourCarence);
		
		// Petit hack pour ne pas être embêté de l'autre côté
		if (unDetailHebergement.dateDebut().equals(debut))  ddmn = debut;
		if (unDetailHebergement.dateFin().equals(fin))  dfpn = fin;
		
		NSMutableArray<NSTimestamp> datesPossible = new NSMutableArray<NSTimestamp>();
		// Locker la base ?
		EOQualifier objQual = ERXQ.equals(EODetailHebergement.OBJET_RESERVABLE_KEY, unDetailHebergement.objetReservable());
		
		EOQualifier dateQual = ERXQ.lessThan(EODetailHebergement.DATE_DEBUT_KEY,dfpn).and(ERXQ.greaterThan(EODetailHebergement.DATE_FIN_RECHERCHE_KEY,ddmn));
		
		EOQualifier qual = ERXQ.and(objQual).and(dateQual);
		
		
		// NSArray<EOSortOrdering> sort = ERXS.asc(DetailHebergement.DATE_DEBUT_KEY).array(); //ne marche pas ici ?
		NSArray<EODetailHebergement> listeDHNT = EODetailHebergement.fetchAll(edc, qual, null );
		 
		
		if (listeDHNT.size()>1) { // Il y a au moins deux objets
		   // tri par date
	       NSMutableArray<EODetailHebergement> listeDH = new NSMutableArray<EODetailHebergement> (listeDHNT.get(0));
		   for (int i=1;i<listeDHNT.size();i++) {
			   int j;
			   for (j=0;j<listeDH.size();j++) if (listeDHNT.get(i).dateDebut().before(listeDH.get(j).dateDebut())) break; 
				   listeDH.add(j,listeDHNT.get(i));
		   }
	        
		   int index = listeDH.indexOf(unDetailHebergement);
		   if (index==0) datesPossible.add(Toolbox.opJoursDate(listeDH.get(1).dateDebut(),-nbJourCarence)); else 
			   if (index==(listeDH.size()-1)) datesPossible.add(Toolbox.opJoursDate(listeDH.get(index-1).dateFinRecherche(),+nbJourCarence)); else {
				   datesPossible.add(Toolbox.opJoursDate(listeDH.get(index-1).dateFinRecherche(),+nbJourCarence));
        	   datesPossible.add(Toolbox.opJoursDate(listeDH.get(index+1).dateDebut(),-nbJourCarence));
           }
		}
		return datesPossible;
	}

	/**
	 *  Retourne si il y a une personne du même nom, en reservation / location durant la période
	 *
	 * Retourne null si un problème
	 * Retourne vide si Ok
	 * Retourne une liste d'hebergement
	 * Fin Location = remise cle != null && retour cle !=null
     *
	 */
	static public NSArray<EODetailHebergement> controlePersonneEnLocation (EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin, EOIndividu individu) {
    	if (dateDebut == null || dateFin == null) return null;
    	if (dateFin.before(dateDebut)) return null;
    	if (individu==null) return null;
    	
    	EOQualifier dateQual = ERXQ.lessThanOrEqualTo(EODetailHebergement.DATE_DEBUT_KEY,dateFin).and(ERXQ.greaterThanOrEqualTo(EODetailHebergement.DATE_FIN_RECHERCHE_KEY,dateDebut));
		EOQualifier individuQual = ERXQ.equals(EODetailHebergement.PERS_ID_CONCERNER_KEY, individu.persId());
		EOQualifier locQual = ERXQ.isNull(EODetailHebergement.DATE_RETOUR_CLES_KEY).or(ERXQ.isNull(EODetailHebergement.DATE_REMISE_CLE_KEY));
		EOQualifier qual = ERXQ.and(dateQual).and(individuQual).and(locQual);
	
		return EODetailHebergement.fetchAll(edc, qual, null); 
	}
    
   /**
    *  
    * @param edc
    * @param unDetailHebergement
    * @return
    */
	/**
	 * Retourne le dernierlogement sans retour de clé ... 
	 */
    static public EODetailHebergement dernierDHSansRetourCle (EOEditingContext edc, EODetailHebergement unDetailHebergement) {
    	
    	EOQualifier objQual = ERXQ.equals(EODetailHebergement.OBJET_RESERVABLE_KEY, unDetailHebergement.objetReservable());	
    	EOQualifier locQual = ERXQ.isNull(EODetailHebergement.DATE_RETOUR_CLES_KEY).and(ERXQ.isNotNull(EODetailHebergement.DATE_REMISE_CLE_KEY));
		EOQualifier dateQual = ERXQ.lessThan(EODetailHebergement.DATE_FIN_KEY,unDetailHebergement.dateDebut());
		EOQualifier qual = ERXQ.and(objQual).and(locQual).and(dateQual);
		NSArray<EOSortOrdering> sort = ERXS.desc(EODetailHebergement.DATE_DEBUT_KEY).array();
		NSArray<EODetailHebergement> resultat = EODetailHebergement.fetchAll(edc, qual, sort); 
		
		if (resultat!=null && resultat.size()>0) {
			return resultat.get(0);		
		}
		
    	return null; 
    }
    /**
     * 
     * 
     * @param edc
     * @param unDetailHebergement
     * @return
     */
    public static Integer deleteDetailHebergement(EOEditingContext edc, EODetailHebergement unDetailHebergement) {
        
    	EOHebergement unHebergement = unDetailHebergement.hebergement();
    	
		//On abandonne les factures et on met à 0 ceux qui n'ont pas de règlements
		NSArray<EORepartDetailHebergementElementAssocie> lesRDHEA = unDetailHebergement.repartDetailHebergementElementAssocies();
		if (lesRDHEA!=null && lesRDHEA.size()>0) {
			for (int i=0; i<lesRDHEA.size();i++) { 
				EOLigneFacture laLigneFacture = lesRDHEA.get(i).ligneFacture();
				if (laLigneFacture!=null) {	
				   EOEnteteFacture laFacture = laLigneFacture.enteteFacture();
				   if (laFacture != null) {
					   laFacture.setIndicationAbandon(Boolean.TRUE);
					   if ((laFacture.lignesReglements()==null) || (laFacture.lignesReglements()!=null && laFacture.lignesReglements().size()==0)) {
						   laFacture.setMntAPayerHt  (new BigDecimal(0)); 
						   laFacture.setMntAPayerTtc (new BigDecimal(0));
						   laFacture.setMntFactureHt (new BigDecimal(0));
						   laFacture.setMntFactureTtc(new BigDecimal(0));
					   }
				   }
			    }
			}
		    try {edc.saveChanges(); } catch (Exception e){ edc.revert(); return 6; };
	    }
		
		// Le grand ménage
		unDetailHebergement.deleteAllRepartDetailHebergementElementAssociesRelationships();
		try {edc.saveChanges(); } catch (Exception e){ edc.revert(); return 5; };
		
		NSArray<EORestauration>  rest = unDetailHebergement.restaurations();
	    if (rest!=null && rest.size()>0) for (int i=0; i<rest.size();i++) rest.get(i).setIdDetailHebergement(null);
	    try {edc.saveChanges(); } catch (Exception e){ edc.revert(); return 4; };
		
	    unDetailHebergement.deleteAllRestaurationsRelationships();
		try {edc.saveChanges(); } catch (Exception e){ edc.revert(); return 3; };
		
		unHebergement.deleteDetailHebergementsRelationship(unDetailHebergement);
		try { edc.saveChanges(); } catch (Exception e){ edc.revert(); return 2; } 
		
		if (unHebergement.detailHebergements().size()==0){
		    edc.deleteObject(unHebergement);
		    try { edc.saveChanges(); } catch (Exception e){ edc.revert(); return 1; } 
		    unHebergement = null;
		}
    	return 0;
    }
    
    public static void updateAdresse (EOEditingContext myContext, EODetailHebergement unDetailHebergement) {
		NSMutableDictionary leDic = new NSMutableDictionary ();
		
		if (unDetailHebergement==null) return;
		
		leDic.takeValueForKey(Long.valueOf(ERXEOControlUtilities.primaryKeyStringForObject(unDetailHebergement)),"001idDetailHebergement");
		
		try {
		     EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_updateAdresse", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
	}
    
    
}
