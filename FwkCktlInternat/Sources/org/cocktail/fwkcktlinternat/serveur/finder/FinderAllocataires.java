/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import org.cocktail.fwkcktlinternat.serveur.metier.EOEtreAllocataire;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class FinderAllocataires {
   
	public static EOEtreAllocataire trouveAllocataire( EOEditingContext edc, String numero){
     	return EOEtreAllocataire.fetchByKeyValue(edc,EOEtreAllocataire.NUMERO_ALLOCATAIRE_KEY, numero);	
	}
	
	public static NSArray <EOEtreAllocataire> trouveAllocataire( EOEditingContext edc, Integer persId ){
     	return EOEtreAllocataire.fetchAll(edc, ERXQ.equals(EOEtreAllocataire.PERS_ID_KEY, persId));	
	}
	
	public static boolean effacerAllocataire (EOEditingContext edc, String numero) {
		
		EOEtreAllocataire alloc = trouveAllocataire(edc, numero);
		if (alloc==null) return false;
		try { 
		  edc.deleteObject(alloc);
		  edc.saveChanges();	
		} catch (Exception e) {
			edc.revert();
			CktlLog.log("Erreur suppression allocataire "+ numero + "  "+ e);
			return false;
		}
		return true;
	}
	
	public static boolean updateAllocataire (EOEditingContext edc, Integer persId, String numero){
		
		EOEtreAllocataire alloc = trouveAllocataire(edc, numero);
		if (alloc==null){
		     alloc = EOEtreAllocataire.createEOEtreAllocataire(edc, numero);
		     alloc.setPersId(persId);
		     CktlLog.log("Creation nouvel allocataire " + FinderPersonne.personneCiviliteNomPrenom(edc, persId)+ " numero " + numero );
		} else {
			if (alloc.persId().intValue()!=persId.intValue()) alloc.setPersId(persId); 
			else {
			     	// CktlLog.log("Allocataire existe déjà avec le même persId : "+FinderPersonne.personneCiviliteNomPrenom(edc, persId)+" "+numero);
			     	return true;		 
			}
		}
		try { 
			  edc.saveChanges();	
			} catch (Exception e) {
				edc.revert();
				CktlLog.log("Erreur modification allocataire "+ FinderPersonne.personneCiviliteNomPrenom(edc, persId)+" numero " +numero+" "+ e);
				return false;
			   }
		return true;
	}
	
	public static String numerosAllocataire(EOEditingContext edc, Integer persId){
		String nums = null;
		NSArray<EOEtreAllocataire> Alloc = FinderAllocataires.trouveAllocataire(edc, persId);
		if (Alloc!=null && Alloc.size()>0) {
    		for (int i=0; i<Alloc.size();i++)
    	         if (nums==null) nums = Alloc.get(i).numeroAllocataire();
    	         else nums += " - " + Alloc.get(i).numeroAllocataire();
		}
		return nums;
	}
}
