/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlinternat.serveur.metier.EOPersonneStatut;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXQ;

public class FinderPersonne  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3739476541451508985L;
	
	public  FinderPersonne(){
		super();
	}
	/*
	 * 
	 * La liste des types d'étudiants dans la base cocktail
	 * 
	 */
    public static NSArray<String> listeTypeEtudiant(EOEditingContext myContext){
		
		String liste = FinderParametres.getParamValue(myContext, FinderParametres.LISTE_TYPE_ETUDIANT_KEY);
		if (liste==null) return null;
		NSMutableArray<String> listeTab =new NSMutableArray<String>();
		
		String [] st = liste.split(",");
		for(int i =0; i < st.length ; i++) listeTab.add(st[i].trim());
				
		return listeTab.immutableClone();
	}
    
	/*
	 * 
	 * Procedure stockée qui retourne le status de la personne physique
	 *
	 */
	public static NSDictionary getIndividuStatus(EOEditingContext myContext, Integer persId) {
		
		NSMutableDictionary leDic = new NSMutableDictionary ();
	
		leDic.takeValueForKey(persId,"001persId");
		leDic.takeValueForKey(new Integer(0),"002etudNumero");
		leDic.takeValueForKey(new String(),"003statut");
		
		NSDictionary<String,Object> res=null;
		try {
		      res = EOUtilities.executeStoredProcedureNamed(myContext, "FwkInternat_statutIndividu", leDic.immutableClone());
		} catch (Exception e) { e.printStackTrace(); }    
		
		return res;
		
	}
	
	public static String getIndividuStringStatut(EOEditingContext myContext, Integer persId) {
		
		NSDictionary myDic = getIndividuStatus(myContext,persId);
		String statusInd = new String(); 
		
		if (myDic!=null)	 
	    	  statusInd = ( myDic.valueForKey("003statut") == NSKeyValueCoding.NullValue ) ? null : (String)(myDic.valueForKey("003statut"));
		return statusInd;
	}
	
    public static String getIndividuEtudiantNumero(EOEditingContext myContext, Integer persId) {
		
		NSDictionary myDic = getIndividuStatus(myContext,persId);
		Integer etudNum = null; 
		
		if (myDic!=null)	 
		         etudNum = ( myDic.valueForKey("002etudNumero") == NSKeyValueCoding.NullValue ) ? null : (Integer)( myDic.valueForKey("002etudNumero")); 
		
		if (etudNum==null) return "";
		
		return String.valueOf(etudNum);
	}
    
    public static String getPersonneStringStatut(EOEditingContext myContext, Integer persId) {
    	String statusPersonne = new String();
    	if (persId == null) return statusPersonne;
    	
    	IPersonne personne = PersonneDelegate.fetchPersonneByPersId(myContext, persId);
    	if (personne==null) return statusPersonne;
    	
		if (personne.isStructure()) {
  	          EOPersonneStatut internatStatus = EOPersonneStatut.fetchByKeyValue(myContext, EOPersonneStatut.PERSI_ID_KEY, persId);
             if (internatStatus!=null 
                  && internatStatus.typeStatut()!=null 
            	  && internatStatus.typeStatut().libTypeStatut()!=null) {
            	  return internatStatus.typeStatut().libTypeStatut(); 
                } else {
                	  return  "Structure";
             }
        }	
		
    	return getIndividuStatutEtudiantNumero(myContext,persId);    	
    }
    
    public static String getIndividuStatutEtudiantNumero(EOEditingContext myContext, Integer persId) {
   	    NSDictionary leDic = getIndividuStatus(myContext,persId);
   	    String statusIndividu = null; Integer etudNum = null;
	    if (leDic!=null) {
	                statusIndividu = ( leDic.valueForKey("003statut") == NSKeyValueCoding.NullValue ) ? null : (String)(leDic.valueForKey("003statut"));
	                etudNum = ( leDic.valueForKey("002etudNumero") == NSKeyValueCoding.NullValue ) ? null : (Integer)( leDic.valueForKey("002etudNumero")); 
	    }
	    if (statusIndividu==null) statusIndividu = new String(); 
	    if (etudNum !=null) statusIndividu += " n° " + String.valueOf(etudNum);
	    
	    return statusIndividu;
    }
    
    public static String personneCiviliteNomPrenom(EOEditingContext edc, Integer persId) {
		String nom="";
		IPersonne personne = PersonneDelegate.fetchPersonneByPersId(edc, persId);
		if (personne==null) return nom;
		if (personne.isStructure()) return personne.getNomPrenomAffichage();
		
		EOIndividu eoIndividu = (EOIndividu) personne;
		if (eoIndividu != null) {
			if (eoIndividu.toCivilite() != null) {
				nom = eoIndividu.toCivilite().cCivilite()+" ";
			}			
			nom +=  eoIndividu.nomAffichage().toUpperCase() + " " +  
			          eoIndividu.prenomAffichage().substring(0, 1).toUpperCase() + eoIndividu.prenomAffichage().substring(1).toLowerCase();
		}
		return nom;
	}
	
	/*
	 *  Recherche l'annee en cours pour les étudiants
	 */
   /*
	public static String fann_key (EOEditingContext myContext){
		  String sql ="SELECT PARAM_VALUE FROM GARNUCHE.GARNUCHE_PARAMETRES WHERE PARAM_KEY='GARNUCHE_ANNEE_REFERENCE'";
       
           NSArray rows = EOUtilities.rawRowsForSQL(myContext, "Internat", sql, null);
           if((rows == null) || (rows.count() < 1)) return null;
            Enumeration e = rows.objectEnumerator();
			String annee_scolaire = null;
			while(e.hasMoreElements()){
				NSDictionary dic = (NSDictionary) e.nextElement();
			    annee_scolaire = (String)dic.objectForKey("PARAM_VALUE");
			}		
			return annee_scolaire;
    }*/
    
	/*
	 * 
	 * internat fann_key
	 * 
	 */
	
	public static String internat_fann_key (EOEditingContext myContext){
	
		return FinderParametres.getParamValue(myContext, FinderParametres.ANNEE_COURANTE_KEY);
		
	} 
	
	/*
	 *  Retourne l'individu avec le numéro étudiant dans un tableau
	 */
	public static NSArray <EOIndividu> studentByNumber(EOEditingContext myContext, String studentNumber) {	
		EOIndividu ind = EOIndividu.individuWithEtudNumeroEquals(myContext, studentNumber);
		if (ind==null) return new NSArray<EOIndividu> ();
		return new NSArray<EOIndividu> (ind);
	}
	
	/*
	 *  Retourne le persid du numéro étudiant
	 */
	public static Integer studentPersid(EOEditingContext myContext, String studentNumber) {	
		 
		NSArray <EOIndividu> lesInd = studentByNumber(myContext, studentNumber);
		if (lesInd!=null && lesInd.size()>0) return new Integer (lesInd.get(0).persId());
		return null;
		
	}
	
	/*
	 * Rechercher l'individu associé à un étudiant
	 * 
	 */
	public static EOEtudiant getEtudiantFromIndividu(EOEditingContext myContext, EOIndividu Individu){
	    
		if (Individu==null) return null;
		
		EOEtudiant Etudiant = null;	
		
	    try {
	         // Cherchons le !	
	    	 Etudiant = EOEtudiant.fetchByKeyValue(myContext, EOEtudiant.NO_INDIVIDU_KEY, Individu.noIndividu());
	    	 
	    } catch (Exception e){ return null; };
	     
	    return Etudiant; 
	}
	
	/*
	 *  On le crée à un seul endroit afin d'éviter les erreurs .. 
	 */
	
	public static String CreateStatut (String Annee, String abreviation) {
		
		return Annee +"-"+ abreviation;
		
	}
	
	/*
	 *  Recherche l'annee et le diplome de l'étudiant
	 */
	
	public static String getEtudiantInscriptionFromEtudiant (EOEditingContext myContext, EOEtudiant UnEtudiant){
		if (UnEtudiant!=null){
		  String sql ="SELECT IDIPL_ANNEE_SUIVIE,FDIP_ABREVIATION FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE IDIPL_TYPE_INSCRIPTION <> 3 AND FANN_KEY="
	         +internat_fann_key(myContext)+ " AND ETUD_NUMERO="+UnEtudiant.etudNumero();
       
           NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
           if((rows == null) || (rows.count() < 1)) return null;
            Enumeration e = rows.objectEnumerator();
			String args = new String();
			while(e.hasMoreElements()){
				NSDictionary dic = (NSDictionary) e.nextElement();
				Long l = (Long)dic.objectForKey("IDIPL_ANNEE_SUIVIE");
				args +=CreateStatut(l.toString(),(String)dic.objectForKey("FDIP_ABREVIATION"));
			}
			return args;
		}
		return null;
	}
	
	public static String getEtudiantAnneeFromEtudiant (EOEditingContext myContext, EOEtudiant UnEtudiant){
		if (UnEtudiant!=null){
		  String sql ="SELECT IDIPL_ANNEE_SUIVIE FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE IDIPL_TYPE_INSCRIPTION <> 3 AND FANN_KEY="
	         +internat_fann_key(myContext)+ " AND ETUD_NUMERO="+UnEtudiant.etudNumero();
       
           NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
           if((rows == null) || (rows.count() < 1)) return null;
            Enumeration e = rows.objectEnumerator();
			String args = new String();
			while(e.hasMoreElements()){
				NSDictionary dic = (NSDictionary) e.nextElement();
				args +=(Long)dic.objectForKey("IDIPL_ANNEE_SUIVIE");
			}
			return args;
		}
		return null;
	}
	
	public static NSArray<String> getAnneesDisponible(EOEditingContext myContext){
		NSMutableArray<String> Annee = new NSMutableArray<String>();
		/*
		 *  Recherche l'annee et le diplone suivi de l'étudiant
		 */
	    String sql ="SELECT DISTINCT IDIPL_ANNEE_SUIVIE FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE IDIPL_TYPE_INSCRIPTION <> 3 AND FANN_KEY="
	    	  +internat_fann_key(myContext)+" ORDER BY IDIPL_ANNEE_SUIVIE ASC";
	   NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	   if((rows == null) || (rows.count() < 1)) return null;
	   Enumeration e = rows.objectEnumerator();
	   while(e.hasMoreElements()){
		  NSDictionary dic = (NSDictionary) e.nextElement();
		  Long l = (Long)dic.objectForKey("IDIPL_ANNEE_SUIVIE");
		  Annee.add(l.toString());
	    }
	   return Annee;
	}
	
	public static NSArray<String> getAbreviationsDisponible(EOEditingContext myContext){
		NSMutableArray<String> Abreviation = new NSMutableArray<String>();
		   String sql ="SELECT DISTINCT FDIP_ABREVIATION FROM SCOLARITE.SCOL_INSCRIPTION_ETUDIANT WHERE IDIPL_TYPE_INSCRIPTION <> 3 AND FANN_KEY="
			   +internat_fann_key(myContext)+" ORDER BY FDIP_ABREVIATION  ASC";
		   NSArray rows = EOUtilities.rawRowsForSQL(myContext, "FwkCktlInternat", sql, null);
	       if((rows == null) || (rows.count() < 1)) return null;
	       Enumeration e = rows.objectEnumerator();
	       while(e.hasMoreElements()){
		       NSDictionary dic = (NSDictionary) e.nextElement();
			   Abreviation.add((String)dic.objectForKey("FDIP_ABREVIATION"));
	       }	
		return Abreviation;
	}
	
	
	public static NSArray<String> getTypesInscription(EOEditingContext myContext){
		NSMutableArray<String> lesTypesIndividu = new NSMutableArray<String>();
		NSArray<String> Abreviations = getAbreviationsDisponible(myContext);
		NSArray<String> Annees = getAnneesDisponible(myContext);
	
         /*
          *  On groupe
          */
	 
		for (int i=0;i<Annees.size();i++) for (int j=0; j<Abreviations.size();j++) 
		  lesTypesIndividu.add (FinderPersonne.CreateStatut(Annees.get(i),Abreviations.get(j)));
		return lesTypesIndividu;
	}
		
	public static String getEtudiantInscriptionFromIndividu (EOEditingContext myContext, EOIndividu Individu){
		return getEtudiantInscriptionFromEtudiant (myContext, getEtudiantFromIndividu(myContext, Individu));  
	}
	public static String getEtudiantAnneeFromIndividu (EOEditingContext myContext, EOIndividu Individu){
		return getEtudiantAnneeFromEtudiant (myContext, getEtudiantFromIndividu(myContext, Individu));  
	}
	public static String getEtudiantInscriptionFromPersid(EOEditingContext myContext, Integer persId) {
		return getEtudiantInscriptionFromIndividu (myContext, EOIndividu.fetchByKeyValue(myContext, EOIndividu.PERS_ID_KEY, persId));
	}
	
	public static String getEtudiantAnneeFromPersid(EOEditingContext myContext, Integer persId) {
		return getEtudiantAnneeFromIndividu (myContext, EOIndividu.fetchByKeyValue(myContext, EOIndividu.PERS_ID_KEY, persId));
	}
}
