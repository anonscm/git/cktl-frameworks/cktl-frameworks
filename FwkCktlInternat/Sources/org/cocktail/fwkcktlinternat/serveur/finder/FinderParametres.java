/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import org.cocktail.fwkcktlinternat.serveur.metier.EOInternatParametre;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/*
 * Classe de gestion des paramètres de l'application
 */

public class FinderParametres {

	// Constante
	public static String OUI_KEY = "OUI";
	public static String NON_KEY = "NON";

	// Date de debut de recherche
	public static String DATE_DEBUT_RECHERCHE_RESERVATION_KEY = "DATE_DEBUT_RECHERCHE_RESERVATION";
	public static String DATE_DEBUT_RECHERCHE_RESERVATION_DEFAULT = "03/09/2012";
	public static String DATE_DEBUT_RECHERCHE_RESERVATION_COMMENT = "Date de début de recherche des reservations par default";

	// Date de fin de recherche
	public static String DATE_FIN_RECHERCHE_RESERVATION_KEY = "DATE_FIN_RECHERCHE_RESERVATION";
	public static String DATE_FIN_RECHERCHE_RESERVATION_DEFAULT = "30/06/2013";
	public static String DATE_FIN_RECHERCHE_RESERVATION_COMMENT = "Date de fin de recherche des reservations par default";

	// Nombre de jours de carence
	public static String NOMBRE_JOURS_CARENCE_KEY = "NOMBRE_JOURS_CARENCE";
	public static String NOMBRE_JOURS_CARENCE_DEFAULT = "0";
	public static String NOMBRE_JOURS_CARENCE_COMMENT = "Nombre de jours entre deux réservations, dépend du type de service";

	// Durée de preavis
	public static String NOMBRE_JOUR_PREAVIS_KEY = "NOMBRE_JOUR_PREAVIS";
	public static String NOMBRE_JOUR_PREAVIS_DEFAULT = "30";
	public static String NOMBRE_JOUR_PREAVIS_COMMENT = "Nombre de jour avant la date de départ pour le préavis";

	// Durée pour passer en mensuel
	public static String NOMBRE_JOUR_FACTURATION_EST_MENSUEL_KEY = "NOMBRE_JOUR_FACTURATION_EST_MENSUEL";
	public static String NOMBRE_JOUR_FACTURATION_EST_MENSUEL_DEFAULT = "30";
	public static String NOMBRE_JOUR_FACTURATION_EST_MENSUEL_COMMENT = "Quand la facturation passe en mensuel, avec bail";

	// Notre FANN_KEY ANNEE_COURANTE
	public static String ANNEE_COURANTE_KEY = "ANNEE_COURANTE";
	public static String ANNEE_COURANTE_DEFAULT = "2011";
	public static String ANNEE_COURANTE_COMMENT = "Annee de travail de Internat";

	public static String LISTE_TYPE_ETUDIANT_KEY = "LISTE_TYPE_ETUDIANT";
	public static String LISTE_TYPE_ETUDIANT_DEFAULT = "ARCHI2,D1,D2,D3,D4,D5,D6,ECH1,EESH1,FF1,FLE1,ING1,ING2,ING3,ING4,ING5,MI1,MS1,MS2,M1,M2";
	public static String LISTE_TYPE_ETUDIANT_COMMENT = "Liste des étudiants possibles que renvoit la procédure stockée STATUT_INDIVIDU";

	public static String GESTION_CAF_KEY = "GESTION_CAF";
	public static String GESTION_CAF_DEFAULT = "OUI";
	public static String GESTION_CAF_COMMENT = "Activer la gestion de la CAF OUI/NON";

	public static String FACTURATION_LOCATION_MENSUELLE_KEY = "FACTURATION_LOCATION_MENSUELLE";
	public static String FACTURATION_LOCATION_MENSUELLE_DEFAULT = "NON";
	public static String FACTURATION_LOCATION_MENSUELLE_COMMENT = "Facturation location mensuelle en cours OUI/NON";

	public static String FACTURATION_RESTAURATION_MENSUELLE_KEY = "FACTURATION_RESTAURATION_MENSUELLE";
	public static String FACTURATION_RESTAURATION_MENSUELLE_DEFAULT = "NON";
	public static String FACTURATION_RESTAURATION_MENSUELLE_COMMENT = "Facturation restauration mensuelle en cours OUI/NON";

	public static String WEBSERVICE_KEY_KEY = "WEBSERVICE_KEY";
	public static String WEBSERVICE_KEY_DEFAULT = "La petite bete a papa";
	public static String WEBSERVICE_KEY_COMMENT = "Cle pour les direct action en tant que webservice";

	public static String LISTE_ENCAISSEMENT_AUTORISE_KEY = "LISTE_ENCAISSEMENT_AUTORISE";
	public static String LISTE_ENCAISSEMENT_AUTORISE_DEFAULT = "Espece,Cheque,Carte bancaire,Virement,JOD 1,JOD 2";
	public static String LISTE_ENCAISSEMENT_AUTORISE_COMMENT = "Liste des encaissement possibles dans la page encaissement";

	/* Pour les prélèvements */
	public static String PRE_C3_KEY = "PRE_C3";
	public static String PRE_C3_DEFAULT = "12345";
	public static String PRE_C3_COMMENT = "Code guichet emetteur - 5 caractères";

	public static String PRE_C41_KEY = "PRE_C41";
	public static String PRE_C41_DEFAULT = "1234";
	public static String PRE_C41_COMMENT = "Identifiant client - 4 caractères";

	public static String PRE_C42_KEY = "PRE_C42";
	public static String PRE_C42_DEFAULT = "1234567";
	public static String PRE_C42_COMMENT = "Code application et divers - 7 caractères";

	public static String PRE_C5_KEY = "PRE_C5";
	public static String PRE_C5_DEFAULT = "123456789012345678901234";
	public static String PRE_C5_COMMENT = "Nom du donneur d'ordre - 24 caractères max";

	public static String PRE_C6_KEY = "PRE_C6";
	public static String PRE_C6_DEFAULT = "123456";
	public static String PRE_C6_COMMENT = "Numéro National d'Emetteur - 6 caracteres";

	// Date de debut d'ouverture restaurant
	public static String DATE_OUVERTURE_RESTAURANT_KEY = "DATE_OUVERTURE_RESTAURANT";
	public static String DATE_OUVERTURE_RESTAURANT_DEFAULT = "01/09/2012";
	public static String DATE_OUVERTURE_RESTAURANT_COMMENT = "Date d'ouverture du restaurant";

	// Date de fin d'ouverture du restaurant
	public static String DATE_FERMETURE_RESTAURANT_KEY = "DATE_FERMETURE_RESTAURANT";
	public static String DATE_FERMETURE_RESTAURANT_DEFAULT = "30/06/2013";
	public static String DATE_FERMETURE_RESTAURANT_KEY_COMMENT = "Date de fermeture du restaurant";

	// Date de début de recherche des locations
	public static String DATE_DEBUT_RECHERCHE_LOCATION_KEY = "DATE_DEBUT_RECHERCHE_LOCATION";
	public static String DATE_DEBUT_RECHERCHE_LOCATION_DEFAULT = "03/09/2012";
	public static String DATE_DEBUT_RECHERCHE_LOCATION_COMMENT = "Date de début de recherche des locations";

	// Date de fin recherche des locations
	public static String DATE_FIN_RECHERCHE_LOCATION_KEY = "DATE_FIN_RECHERCHE_LOCATION";
	public static String DATE_FIN_RECHERCHE_LOCATION_DEFAULT = "30/06/2013";
	public static String DATE_FIN_RECHERCHE_LOCATION_COMMENT = "Date de fin de recherche des locations";

	// Qui dort dine à la réservation
	public static String QDD_RESERVATION_KEY = "QDD_RESERVATION";
	public static String QDD_RESERVATION_DEFAULT = "OUI";
	public static String QDD_RESERVATION_COMMENT = "Crée un QDD à la réservation. Valeur OUI/NON";

	// Qui dort dine à la remise de clés
	public static String QDD_REMISE_CLES_KEY = "QDD_REMISE_CLES";
	public static String QDD_REMISE_CLES_DEFAULT = "OUI";
	public static String QDD_REMISE_CLES_COMMENT = "Crée un QDD à la remise de clés. Valeur OUI/NON";

	// Qui dort dine, dates d'exclusions
	public static String QDD_INTERVALLES_EXCLUSION_KEY = "QDD_INTERVALLES_EXCLUSION";
	public static String QDD_INTERVALLES_EXCLUSION_DEFAULT = "01/07,31/08";
	public static String QDD_INTERVALLES_EXCLUSION_COMMENT = "Liste d'intervalles de dates d'exclusions du QDD. Couples de valeurs  DD/MM,DD/MM,DD/MM,DD,MM...";

	// Type objet exclusion
	public static String QDD_TYPE_OBJET_EXCLUSION_KEY = "QDD_TYPE_OBJET_EXCLUSION";
	public static String QDD_TYPE_OBJET_EXCLUSION_DEFAULT = "Salle d'etude";
	public static String QDD_TYPE_OBJET_EXCLUSION_COMMENT = "Liste de type d''objets reservables exclus du QDD séparés par des virgules";

	// Type objet exclusion
	public static String MODE_PRELEVEMENT_KEY = "MODE_PRELEVEMENT";
	public static String MODE_PRELEVEMENT_DEFAULT = "SEPA";
	public static String MODE_PRELEVEMENT_COMMENT = "Mode de prélèvement : SEPA/NATIONAL";

	public FinderParametres() {
		// TODO Auto-generated constructor stub
	}

	private static String getDefault(String key) {
		if (key.toUpperCase().equals(DATE_DEBUT_RECHERCHE_RESERVATION_KEY))
			return DATE_DEBUT_RECHERCHE_RESERVATION_DEFAULT;
		if (key.toUpperCase().equals(DATE_FIN_RECHERCHE_RESERVATION_KEY))
			return DATE_FIN_RECHERCHE_RESERVATION_DEFAULT;
		if (key.toUpperCase().equals(NOMBRE_JOUR_PREAVIS_KEY))
			return NOMBRE_JOUR_PREAVIS_DEFAULT;
		if (key.toUpperCase().equals(NOMBRE_JOUR_FACTURATION_EST_MENSUEL_KEY))
			return NOMBRE_JOUR_FACTURATION_EST_MENSUEL_DEFAULT;
		if (key.toUpperCase().equals(ANNEE_COURANTE_KEY))
			return ANNEE_COURANTE_DEFAULT;
		if (key.toUpperCase().equals(LISTE_TYPE_ETUDIANT_KEY))
			return LISTE_TYPE_ETUDIANT_DEFAULT;
		if (key.toUpperCase().equals(FACTURATION_LOCATION_MENSUELLE_KEY))
			return FACTURATION_LOCATION_MENSUELLE_DEFAULT;
		if (key.toUpperCase().equals(FACTURATION_RESTAURATION_MENSUELLE_KEY))
			return FACTURATION_RESTAURATION_MENSUELLE_DEFAULT;
		if (key.toUpperCase().equals(GESTION_CAF_KEY))
			return GESTION_CAF_DEFAULT;
		if (key.toUpperCase().equals(WEBSERVICE_KEY_KEY))
			return WEBSERVICE_KEY_DEFAULT;
		if (key.toUpperCase().equals(LISTE_ENCAISSEMENT_AUTORISE_KEY))
			return LISTE_ENCAISSEMENT_AUTORISE_DEFAULT;
		if (key.toUpperCase().equals(NOMBRE_JOURS_CARENCE_KEY))
			return NOMBRE_JOURS_CARENCE_DEFAULT;
		if (key.toUpperCase().equals(PRE_C3_KEY))
			return PRE_C3_DEFAULT;
		if (key.toUpperCase().equals(PRE_C41_KEY))
			return PRE_C41_DEFAULT;
		if (key.toUpperCase().equals(PRE_C42_KEY))
			return PRE_C42_DEFAULT;
		if (key.toUpperCase().equals(PRE_C5_KEY))
			return PRE_C5_DEFAULT;
		if (key.toUpperCase().equals(PRE_C6_KEY))
			return PRE_C6_DEFAULT;
		if (key.toUpperCase().equals(DATE_OUVERTURE_RESTAURANT_KEY))
			return DATE_OUVERTURE_RESTAURANT_DEFAULT;
		if (key.toUpperCase().equals(DATE_FERMETURE_RESTAURANT_KEY))
			return DATE_FERMETURE_RESTAURANT_DEFAULT;
		if (key.toUpperCase().equals(DATE_DEBUT_RECHERCHE_LOCATION_KEY))
			return DATE_DEBUT_RECHERCHE_LOCATION_DEFAULT;
		if (key.toUpperCase().equals(DATE_FIN_RECHERCHE_LOCATION_KEY))
			return DATE_FIN_RECHERCHE_LOCATION_DEFAULT;
		if (key.toUpperCase().equals(QDD_RESERVATION_KEY))
			return QDD_RESERVATION_DEFAULT;
		if (key.toUpperCase().equals(QDD_REMISE_CLES_KEY))
			return QDD_REMISE_CLES_DEFAULT;
		if (key.toUpperCase().equals(QDD_INTERVALLES_EXCLUSION_KEY))
			return QDD_INTERVALLES_EXCLUSION_DEFAULT;
		if (key.toUpperCase().equals(QDD_TYPE_OBJET_EXCLUSION_KEY))
			return QDD_TYPE_OBJET_EXCLUSION_DEFAULT;
		if (key.toUpperCase().equals(MODE_PRELEVEMENT_KEY))
			return MODE_PRELEVEMENT_DEFAULT;

		return null;

	}

	public static String getParamValue(EOEditingContext edc, String key) {
		NSArray<EOInternatParametre> param = null;

		if (key == null)
			return null;
		if (key.length() == 0)
			return null;

		EOQualifier qual = ERXQ.equals(EOInternatParametre.PARAM_KEY_KEY, key);

		try {
			param = EOInternatParametre.fetchAll(edc, qual, null);
		} catch (Exception e) {
			e.printStackTrace();
			return getDefault(key);
		}
		if (param == null)
			return getDefault(key);
		if (param.size() == 0)
			return getDefault(key);

		return param.get(0).paramValue();
	}

	public static boolean setParamValue(EOEditingContext edc, String key, String value) {
		NSArray<EOInternatParametre> param = null;

		if (key == null)
			return false;
		if (key.length() == 0)
			return false;

		EOQualifier qual = ERXQ.equals(EOInternatParametre.PARAM_KEY_KEY, key);

		try {
			param = EOInternatParametre.fetchAll(edc, qual, null);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		EOInternatParametre p = null;

		if ((param == null) || (param != null && param.size() == 0)) {
			p = EOInternatParametre.createEOInternatParametre(edc);
			p.setParamKey(key);
			p.setParamCommentaire("Argument créé par défaut");
		} else
			p = param.get(0);

		p.setParamValue(value);

		try {
			edc.saveChanges();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean enTrainDeFacturerLocation(EOEditingContext edc) {
		return getParamValue(edc, FACTURATION_LOCATION_MENSUELLE_KEY).equals(OUI_KEY);
	}

	public static boolean gereLaCaf(EOEditingContext edc) {
		return getParamValue(edc, GESTION_CAF_KEY).equals(OUI_KEY);
	}

	public static boolean enTrainDeFacturerRestauration(EOEditingContext edc) {
		return getParamValue(edc, FACTURATION_RESTAURATION_MENSUELLE_KEY).equals(OUI_KEY);
	}

	public static void setEnTrainDeFacturerRestauration(EOEditingContext edc) {
		setParamValue(edc, FACTURATION_RESTAURATION_MENSUELLE_KEY, OUI_KEY);
	}

	public static void resetEnTrainDeFacturerRestauration(EOEditingContext edc) {
		setParamValue(edc, FACTURATION_RESTAURATION_MENSUELLE_KEY, NON_KEY);
	}

	public static void setEnTrainDeFacturerLocation(EOEditingContext edc) {
		setParamValue(edc, FACTURATION_LOCATION_MENSUELLE_KEY, OUI_KEY);
	}

	public static void resetEnTrainDeFacturerLocation(EOEditingContext edc) {
		setParamValue(edc, FACTURATION_LOCATION_MENSUELLE_KEY, NON_KEY);
	}

	public static NSTimestamp getDateDebutRechercheReservation(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_DEBUT_RECHERCHE_RESERVATION_KEY));
	}

	public static NSTimestamp getDateFinRechercheReservation(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_FIN_RECHERCHE_RESERVATION_KEY));
	}

	public static NSTimestamp getDateDebutRechercheLocation(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_DEBUT_RECHERCHE_LOCATION_KEY));
	}

	public static NSTimestamp getDateFinRechercheLocation(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_FIN_RECHERCHE_LOCATION_KEY));
	}

	public static NSTimestamp getDateOuvertureRestaurant(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_OUVERTURE_RESTAURANT_KEY));
	}

	public static NSTimestamp getDateFermetureRestaurant(EOEditingContext edc) {
		return Toolbox.parseNSTimestamp(getParamValue(edc, FinderParametres.DATE_FERMETURE_RESTAURANT_KEY));
	}

	public static boolean isQddRemiseCles(EOEditingContext edc) {
		return getParamValue(edc, QDD_REMISE_CLES_KEY).equals(OUI_KEY);
	}

	public static boolean isQddReservation(EOEditingContext edc) {
		return getParamValue(edc, QDD_RESERVATION_KEY).equals(OUI_KEY);
	}

	public static boolean isModePrelevementSepa(EOEditingContext edc) {
		return getParamValue(edc, MODE_PRELEVEMENT_KEY).equals("SEPA");
	}

	public static boolean isModePrelevementNational(EOEditingContext edc) {
		return getParamValue(edc, MODE_PRELEVEMENT_KEY).equals("NATIONAL");
	}

}
