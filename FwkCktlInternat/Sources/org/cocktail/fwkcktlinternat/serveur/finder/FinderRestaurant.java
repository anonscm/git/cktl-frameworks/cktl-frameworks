/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlinternat.serveur.finder;

import java.util.Calendar;
import java.util.Scanner;

import org.cocktail.fwkcktlinternat.serveur.metier.EODetailHebergement;
import org.cocktail.fwkcktlinternat.serveur.metier.EOFormule;
import org.cocktail.fwkcktlinternat.serveur.metier.EOQuiDortDine;
import org.cocktail.fwkcktlinternat.serveur.metier.EORestauration;
import org.cocktail.fwkcktlinternat.serveur.metier.EOTypeObjet;
import org.cocktail.fwkcktlinternat.serveur.util.Toolbox;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class FinderRestaurant {
	
	/**
	 * @param edc
	 * @param leDetailHebergement
	 * @return
	 */
	
	public static EORestauration donneRestaurationQDD(EOEditingContext edc, EODetailHebergement leDetailHebergement) {
		
		// Qui dort dine
		
		// Pas de QDD sur les objets sans salle 
		 if (leDetailHebergement.objetReservable().salle()==null) return null;
	    
		// Si pas de nom ...  	 
		 if (leDetailHebergement.persIdConcerner()==null) return null;
		 
		// test si le type d'objet à louer mérite un QDD
		 
		 if (typeObjetEstExclus(edc, leDetailHebergement.objetReservable().typeObjet())) return null;
		 
		// test si on n'est pas dans une zone de fermeture du restaurant (vacances)
		 
		 if (dateEstDansZoneExclusion(edc,leDetailHebergement.dateDebut())) return null; 
		 
		   /*
		    * Si on est en location et qui dort dine : on crée un contrat de restauration
		    * 
		    * On cherche l'ancienne formule à 1 jours près
		    * 
		    */
	      
	     Integer anneeCourante = Integer.valueOf(FinderParametres.getParamValue(edc, FinderParametres.ANNEE_COURANTE_KEY)); 
	     EOQualifier qual = ERXQ.equals(EOQuiDortDine.FANN_KEY_KEY, anneeCourante).or(ERXQ.isNull(EOQuiDortDine.FANN_KEY_KEY));
	     NSArray <EOQuiDortDine> laTableQDD = EOQuiDortDine.fetchAll(edc, qual, null);
	     EOFormule laFormule = null;
	     if (laTableQDD !=null && laTableQDD.size()>0) { 
	    	// Chercher le statut 
	    	String statusEleve = FinderPersonne.getIndividuStringStatut(edc, leDetailHebergement.persIdConcerner());
		    if (statusEleve !=null) statusEleve = statusEleve.trim();
		    if (statusEleve!=null)
		       for (int i=0;i<laTableQDD.size();i++) 
		    	if (statusEleve.equalsIgnoreCase(laTableQDD.get(i).typeEtudiant())) {
		    		laFormule = laTableQDD.get(i).formule();
		    		break;
		        }
		    // Eleve en QDD ?
		    if (laFormule!=null){
		      // Chercher si l'élève n'aurait pas un contrat en cours durant cette période 
		      // Il n'aurait pas un QDD mais il mange quand même, tout est alors ok
		      EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, leDetailHebergement.persIdConcerner());	
		      EOQualifier  dateQual = ERXQ.lessThan(EORestauration.DATE_DEBUT_KEY, leDetailHebergement.dateFin())
		    		            .and (ERXQ.greaterThan(EORestauration.DATE_FIN_KEY, leDetailHebergement.dateDebut()));
	    	  qual = ERXQ.and(indQual).and(dateQual);
	    	  NSArray<EOSortOrdering> sort = ERXS.desc(EORestauration.DATE_DEBUT_KEY).array();
	    	  NSArray<EORestauration> derRest = EORestauration.fetchAll(edc, qual, sort);
		      if (derRest!=null && derRest.size()>0) {
		    	      laFormule = null; // Pas besion de QDD	
		      } else {
		       // Chercher l'ancienne formule de l'élève si changement de chambre entre j-1 et j+1 en principe
		       NSTimestamp dateDebutTransition = Toolbox.opJoursDate(leDetailHebergement.dateDebut(),-2);
		       NSTimestamp dateFinTransition = Toolbox.opJoursDate(leDetailHebergement.dateDebut(),2);
		       dateQual = ERXQ.greaterThan(EORestauration.DATE_FIN_KEY,dateDebutTransition).and(
		    		                 ERXQ.lessThan(EORestauration.DATE_FIN_KEY,dateFinTransition));
		       qual = ERXQ.and(indQual).and(dateQual);
		       sort = ERXS.desc(EORestauration.DATE_DEBUT_KEY).array();
		       derRest = EORestauration.fetchAll(edc, qual, sort);
		       if (derRest!=null && derRest.size()>0) laFormule = derRest.get(0).formule();  
		      }
		    }	 
	     }
	     if (laFormule!=null) { 
	    	 EORestauration rest = EORestauration.createEORestauration(edc, null);
	    	 rest.setFormuleRelationship(laFormule);
	    	 return rest;
	     }
		return null;
	}
	
	private static boolean dateEstDansZoneExclusion(EOEditingContext edc, NSTimestamp dateDebut) {
		
		if (dateDebut==null) return true;
		
		String intervalleExclusion=FinderParametres.getParamValue(edc,FinderParametres.QDD_INTERVALLES_EXCLUSION_KEY);
		if (intervalleExclusion!=null && intervalleExclusion.trim().length()>0) {	    
			try {
				 Calendar c = Calendar.getInstance();
                 c.setTime(dateDebut);
		         String anneeDateDebut = Integer.toString(c.get(Calendar.YEAR));
		    
				 Scanner s = new Scanner(intervalleExclusion).useDelimiter("\\s*,\\s*");
		        
				 while (s.hasNext()) {
			      // extraction des dates
				  String sdd = s.next();
				  String sdf = s.next();
				 
				  NSTimestamp dd = Toolbox.parseNSTimestamp(sdd+"/"+anneeDateDebut);
				  NSTimestamp df = Toolbox.parseNSTimestamp(sdf+"/"+anneeDateDebut);
				  if (df.before(dd)) df = Toolbox.opJoursDate(df, 365);
				  
				  // test 
				  if (dateDebut.compare(dd)>=0 && dateDebut.compare(df)<=0) return true;
				 }
				 
			} catch (Exception e) {
				
			} 
		}
		return false;
	}
	
    private static boolean typeObjetEstExclus(EOEditingContext edc, EOTypeObjet typeObjet) {
		
		if (typeObjet==null) return true;
		
		String typeObjetExclusion=FinderParametres.getParamValue(edc,FinderParametres.QDD_TYPE_OBJET_EXCLUSION_KEY);
		if (typeObjetExclusion!=null && typeObjetExclusion.trim().length()>0) {	    
			try {
				 String libelleTypeObjet = StringCtrl.chaineSansAccents(typeObjet.libTypeObjet().trim());
				 Scanner s = new Scanner(typeObjetExclusion).useDelimiter("\\s*,\\s*");
				 while (s.hasNext()) {
			      // extraction des libelles
				  String leLibelle = StringCtrl.chaineSansAccents(s.next().trim());
				  // test 
				  if (leLibelle.equalsIgnoreCase(libelleTypeObjet)) return true;
				 }
			} catch (Exception e) {
				
			} 
		}
		return false;
	}
	
	/**
	 * 
	 * @param edc
	 * @param unDetailHebergement
	 * @return
	 */
	public static EORestauration cherchePremiereRestaurationQDD(EOEditingContext edc, EODetailHebergement unDetailHebergement) {
		/**
		 * Cherche la première formule de restauration
		 */		
		if (unDetailHebergement==null) return null;
		if (unDetailHebergement.persIdConcerner()==null) return null;
		
		EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, unDetailHebergement.persIdConcerner());
        EOQualifier hebQual = ERXQ.equals(EORestauration.DETAIL_HEBERGEMENT_KEY,unDetailHebergement);
	    NSArray<EORestauration> preRest = EORestauration.fetchAll(edc,ERXQ.and(indQual).and(hebQual), null);
		
	    if (preRest!=null && preRest.size()>0) {
	       Integer index = 0; 
	       for (int i=1;i<preRest.size();i++)
	           if (preRest.get(index).dateDebut().after(preRest.get(i).dateDebut())) 
	    	   index = i;
	      return preRest.get(index);
	   }
	    return null;
	}
	
	
	/**
	 * @param edc
	 * @param leDetailHebergement
	 * @return
	 */
	public static EORestauration chercheDerniereRestaurationQDD(EOEditingContext edc, EODetailHebergement unDetailHebergement) {
		/**
		 * Cherche la dernière formule de restauration
		 */		
		if (unDetailHebergement==null) return null;
		if (unDetailHebergement.persIdConcerner()==null) return null;
		
		EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, unDetailHebergement.persIdConcerner());
        EOQualifier hebQual = ERXQ.equals(EORestauration.DETAIL_HEBERGEMENT_KEY,unDetailHebergement);
	    NSArray<EORestauration> derRest = EORestauration.fetchAll(edc,ERXQ.and(indQual).and(hebQual), null);
		
	    if (derRest!=null && derRest.size()>0) {
	       Integer index = 0; 
	       for (int i=1;i<derRest.size();i++)
	           if (derRest.get(index).dateFin().before(derRest.get(i).dateFin())) 
	    	   index = i;
	      return derRest.get(index);
	   }
		
	    return null;
	}
	
 /**
  * 	
  * @param edc
  * @param unDetailHebergement
  * @param dateDebut
  */
	// Non utilisé parce que le QDD est très restrictif : on ne travaille pas sur les résas.
   public static void effacerDebutRestaurationQDD (EOEditingContext edc, EODetailHebergement unDetailHebergement, NSTimestamp dateDebut) {
        
		if (unDetailHebergement==null || dateDebut==null) return;
		if (unDetailHebergement.persIdConcerner()==null) return;
		
		EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, unDetailHebergement.persIdConcerner());
        EOQualifier hebQual = ERXQ.equals(EORestauration.DETAIL_HEBERGEMENT_KEY,unDetailHebergement);
        EOQualifier dateQual = ERXQ.lessThanOrEqualTo(EORestauration.DATE_FIN_KEY, dateDebut); 
        EOQualifier dateFactQual = ERXQ.isNull(EORestauration.DATE_FIN_FACTURE_KEY);
        EOQualifier qual = ERXQ.and(indQual, hebQual, dateQual, dateFactQual);
        
	    NSArray<EORestauration> derRest = EORestauration.fetchAll(edc,qual, null);
	    
	    if (derRest.count()==0) return;
	    for (int i=0;i<derRest.size();i++) edc.deleteObject(derRest.get(i));
	    
	    try {
			edc.saveChanges();
		} catch (Exception e) {
			edc.revert();
	        e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param edc
	 * @param unDetailHebergement
	 * @param dateFin
	 */
	public static void effacerFinRestaurationQDD (EOEditingContext edc, EODetailHebergement unDetailHebergement, NSTimestamp dateFin) {
        
		if (unDetailHebergement==null || dateFin==null) return;
		if (unDetailHebergement.persIdConcerner()==null) return;
		
		EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, unDetailHebergement.persIdConcerner());
        EOQualifier hebQual = ERXQ.equals(EORestauration.DETAIL_HEBERGEMENT_KEY,unDetailHebergement);
        EOQualifier dateQual = ERXQ.greaterThanOrEqualTo(EORestauration.DATE_DEBUT_KEY, dateFin); 
        EOQualifier dateFactQual = ERXQ.isNull(EORestauration.DATE_FIN_FACTURE_KEY);
        EOQualifier qual = ERXQ.and(indQual, hebQual, dateQual, dateFactQual);
        
	    NSArray<EORestauration> derRest = EORestauration.fetchAll(edc,qual, null);
	    
	    if (derRest.count()==0) return;
	    for (int i=0;i<derRest.size();i++) edc.deleteObject(derRest.get(i));
	    
	    try {
			edc.saveChanges();
		} catch (Exception e) {
			edc.revert();
	        e.printStackTrace();
		}
	}
	
	/**
	 * @param edc
	 * @param uneRestauration
	 * @param modifier
	 * @return
	 */
	
	public static NSArray<EORestauration> chercherRecouvrement(EOEditingContext edc, EORestauration uneRestauration, boolean modifier) {
	  EOQualifier indQual = ERXQ.equals(EORestauration.PERS_ID_KEY, uneRestauration.persId());
	  EOQualifier dateQual = ERXQ.and(ERXQ.greaterThanOrEqualTo(EORestauration.DATE_FIN_KEY, uneRestauration.dateDebut())).and
	                               (ERXQ.lessThanOrEqualTo(EORestauration.DATE_DEBUT_KEY, uneRestauration.dateFin()));
	  EOQualifier qual = ERXQ.and(indQual).and(dateQual);
	  if (modifier) 
		  qual = ERXQ.and(qual).and(ERXQ.notEquals(EORestauration.ID_RESTAURATION_KEY, uneRestauration.idRestauration()));

	  NSArray<EORestauration> lesRestos = EORestauration.fetchAll(edc, qual, null);
	
	  return lesRestos;
	
	}	

}
