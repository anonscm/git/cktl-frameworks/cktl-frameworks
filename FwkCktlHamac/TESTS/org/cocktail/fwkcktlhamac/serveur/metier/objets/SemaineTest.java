/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.junit.Test;

import com.webobjects.foundation.NSTimeZone;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class SemaineTest {

	// XXX se référer aux initialisations des timezone de la classe Application
	static {
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("CEST");
		java.util.TimeZone.setDefault(tz);
		NSTimeZone.setDefault(tz);
	}

	@Test
	public void testGetCodeForDate() {
		assertEquals("2012_26", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("25/06/2012")));
		assertEquals("2012_27", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("02/07/2012")));
		assertEquals("2011_52", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("01/01/2012")));
		assertEquals("2012_01", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("02/01/2012")));
		assertEquals("2012_52", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("24/12/2012")));
		assertEquals("2013_01", Semaine.getCodeForDate(DateCtrlHamac.stringToDate("31/12/2012")));
	}

	@Test
	public void testGetDateForCode() {
		assertEquals(DateCtrlHamac.stringToDate("25/06/2012"), Semaine.getDateForCode("2012_26"));
		assertEquals(DateCtrlHamac.stringToDate("24/12/2012"), Semaine.getDateForCode("2012_52"));
		assertEquals(DateCtrlHamac.stringToDate("31/12/2012"), Semaine.getDateForCode("2013_01"));
		assertEquals(DateCtrlHamac.stringToDate("02/01/2012"), Semaine.getDateForCode("2012_01"));
		assertEquals(DateCtrlHamac.stringToDate("26/12/2011"), Semaine.getDateForCode("2011_52"));
	}

}
