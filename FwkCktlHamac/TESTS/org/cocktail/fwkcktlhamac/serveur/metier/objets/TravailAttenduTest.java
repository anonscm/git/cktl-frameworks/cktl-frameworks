package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.junit.Test;

import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;

/**
 * Classe de test de {@link TravailAttendu}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class TravailAttenduTest {

	// XXX se référer aux initialisations des timezone de la classe Application
	static {
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone("CEST");
		java.util.TimeZone.setDefault(tz);
		NSTimeZone.setDefault(tz);
	}

	@Test
	public void traitementStructureIdentiqueTest() {

	}

	@Test
	public void diviseTableauxTest() {

		TravailAttendu t1 = new TravailAttendu(
				DateCtrl.stringToDate("01/01/2008"),
				DateCtrl.stringToDate("31/12/2012"),
				"25",
				Integer.valueOf(50));

		TravailAttendu t2 = new TravailAttendu(
				DateCtrl.stringToDate("01/01/2009"),
				DateCtrl.stringToDate("31/12/2010"),
				"25",
				Integer.valueOf(50));

		TravailAttendu t3 = new TravailAttendu(
				DateCtrl.stringToDate("01/01/2012"),
				null,
				"25",
				Integer.valueOf(30));

		NSMutableArray<TravailAttendu> array = new NSMutableArray<TravailAttendu>(new
				TravailAttendu[] { t1, t2, t3 });

	}
}
