/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.stat;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 * 
 */
public class CongesPayesContainerTest {

	private final static float VALEUR_INDICE = (float) 55.5635;

	@Test
	public void calculerSalaireBrutJournalierTest() {

		assertEquals(
				new Float(15.434306),
				CongesPayesContainer.calculerSalaireBrutJournalier(
						Integer.valueOf(100), null, new Float(VALEUR_INDICE)));

	}
}
