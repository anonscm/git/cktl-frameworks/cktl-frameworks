/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class FormatterCtrlTest {

	@Test
	public void testEnJourArrondi2ChiffresApresVirgule() {

		assertEquals((float) 1, FormatterCtrl.enJourArrondi2ChiffresApresVirgule(420, 420), 0);
		assertEquals((float) 1.1, FormatterCtrl.enJourArrondi2ChiffresApresVirgule(462, 420), 0);
		assertEquals((float) 1.11, FormatterCtrl.enJourArrondi2ChiffresApresVirgule(467, 420), 0);

	}

	@Test
	public void testEnJourArrondiEntierInferieur() {

		assertEquals(0, FormatterCtrl.enJourArrondiEntierInferieur(419, 420));
		assertEquals(1, FormatterCtrl.enJourArrondiEntierInferieur(420, 420));
		assertEquals(1, FormatterCtrl.enJourArrondiEntierInferieur(462, 420));
		assertEquals(1, FormatterCtrl.enJourArrondiEntierInferieur(467, 420));

	}

}
