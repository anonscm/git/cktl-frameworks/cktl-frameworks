package org.cocktail.fwkcktlhamac.serveur.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageModeCalcul;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageModeCalculCompare;
import org.junit.Test;

import com.webobjects.foundation.NSTimeZone;

public class PlageModeCalculServiceTest {

	static {
		NSTimeZone tz =  NSTimeZone.timeZoneWithName("UTC", true);
		NSTimeZone.setDefaultTimeZone(tz);
		NSTimeZone.setDefault(tz);
		java.util.TimeZone.setDefault(tz);
	}
	
	@Test
	public void testgetPlageModeCalculPourSortedPeriodes() {


		List<PlageModeCalculService.SimpleDebutFin> array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();
		List<PlageModeCalcul> arrayResultat = null;
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("01/03/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/03/2012"),
						DateCtrlHamac.stringToDate("02/10/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("03/10/2012"),
						DateCtrlHamac.stringToDate("10/11/2012"),
						false));
	
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("20/11/2012"),
						DateCtrlHamac.stringToDate("20/10/2013"),
						false));
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("30/11/2013"),
						DateCtrlHamac.stringToDate("20/10/2014"),
						true));
		
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);

		assertEquals(
				3,
				arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au 10/11/2012", arrayResultat.get(0).toString());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 20/11/2012 au 20/10/2013", arrayResultat.get(1).toString());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 30/11/2013 au 20/10/2014", arrayResultat.get(2).toString());
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("30/06/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/07/2012"),
						DateCtrlHamac.stringToDate("31/12/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2013"),
						DateCtrlHamac.stringToDate("30/06/2013"),
						false));
	
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);

		assertEquals(
				1,
				arrayResultat.size());

		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au 30/06/2013", arrayResultat.get(0).toString());
		
	
		
		
		/**
		 * 
		 */
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("30/06/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/07/2012"),
						DateCtrlHamac.stringToDate("31/12/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2013"),
						DateCtrlHamac.stringToDate("30/12/2013"),
						false));
	
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);
		assertEquals(2,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au 31/12/2012", arrayResultat.get(0).toString());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/02/2013 au 30/12/2013", arrayResultat.get(1).toString());
		
		
		/**
		 * 
		 */
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("30/06/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("20/07/2012"),
						DateCtrlHamac.stringToDate("31/12/2012"),
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 5, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au 31/12/2012", arrayResultat.get(0).toString());
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("30/06/2012"),
						true));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/07/2012"),
						DateCtrlHamac.stringToDate("31/12/2013"),
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au 31/12/2013", arrayResultat.get(0).toString());
		
		
				
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/07/2012"),
						null,
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/07/2012 au ", arrayResultat.get(0).toString());
		
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("30/06/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/07/2012"),
						null,
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/01/2012 au ", arrayResultat.get(0).toString());
		
		
		
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2014"),
						DateCtrlHamac.stringToDate("30/04/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/06/2014"),
						DateCtrlHamac.stringToDate("28/06/2014"),
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 6, array);
		
		assertEquals(2,	arrayResultat.size());
		
		assertEquals(false, arrayResultat.get(0).isModeCalculTitulaire());
		assertEquals(DateCtrlHamac.stringToDate("01/02/2014"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("30/04/2014"), arrayResultat.get(0).getDateFin());
		assertEquals(false, arrayResultat.get(1).isModeCalculTitulaire());
		assertEquals(DateCtrlHamac.stringToDate("02/06/2014"), arrayResultat.get(1).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("28/06/2014"), arrayResultat.get(1).getDateFin());

		
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2014"),
						null,
						true));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/06/2014"),
						DateCtrlHamac.stringToDate("28/06/2014"),
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 6, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		assertEquals(DateCtrlHamac.stringToDate("01/02/2014"), arrayResultat.get(0).getDateDebut());
		assertEquals(null, arrayResultat.get(0).getDateFin());

		
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2014"),
						DateCtrlHamac.stringToDate("02/05/2014"),
						true));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/02/2014"),
						DateCtrlHamac.stringToDate("28/06/2014"),
						false));
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 6, array);
		
		assertEquals(2,	arrayResultat.size());
		
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		assertEquals(DateCtrlHamac.stringToDate("01/02/2014"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("02/05/2014"), arrayResultat.get(0).getDateFin());
		
		assertEquals(false, arrayResultat.get(1).isModeCalculTitulaire());
		assertEquals(DateCtrlHamac.stringToDate("02/05/2014"), arrayResultat.get(1).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("28/06/2014"), arrayResultat.get(1).getDateFin());
		
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/06/2013"),
						DateCtrlHamac.stringToDate("01/06/2015"),
						false));
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2014"),
						DateCtrlHamac.stringToDate("01/01/2015"),
						true));

		

		
			
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(9, 6, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/06/2013 au 01/06/2015", arrayResultat.get(0).toString());
		/**
		 * 
		 */
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/10/2013"),
						DateCtrlHamac.stringToDate("01/05/2014"),
						false));
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/05/2014"),
						DateCtrlHamac.stringToDate("01/01/2015"),
						false));

		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(6, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/10/2013 au 01/01/2015", arrayResultat.get(0).toString());
			
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2014"),
						DateCtrlHamac.stringToDate("01/03/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/03/2014"),
						DateCtrlHamac.stringToDate("01/04/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/04/2014"),
						DateCtrlHamac.stringToDate("01/05/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/05/2014"),
						DateCtrlHamac.stringToDate("01/06/2014"),
						false));
		
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(8, 6, array);
		
		assertEquals(1,	arrayResultat.size());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_CONTRACTUEL+" du 01/02/2014 au 01/06/2014", arrayResultat.get(0).toString());
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2014"),
						DateCtrlHamac.stringToDate("01/03/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/03/2014"),
						DateCtrlHamac.stringToDate("01/04/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/04/2014"),
						DateCtrlHamac.stringToDate("01/05/2014"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("02/05/2014"),
						DateCtrlHamac.stringToDate("01/06/2014"),
						false));
		
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(3, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/02/2014 au 01/06/2014", arrayResultat.get(0).toString());
		
		
		
		array = new ArrayList<PlageModeCalculService.SimpleDebutFin>();

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/02/2011"),
						DateCtrlHamac.stringToDate("31/12/2011"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("31/10/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/11/2012"),
						DateCtrlHamac.stringToDate("31/12/2012"),
						false));
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2013"),
						DateCtrlHamac.stringToDate("31/12/2013"),
						false));

		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/01/2014"),
						DateCtrlHamac.stringToDate("31/08/2014"),
						false));
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/09/2014"),
						DateCtrlHamac.stringToDate("31/08/2015"),
						false));
		
		array.add(
				new PlageModeCalculService.SimpleDebutFin(
						DateCtrlHamac.stringToDate("01/11/2014"),
						DateCtrlHamac.stringToDate("31/08/2015"),
						false));
		
		
		arrayResultat = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(10, 1, array);
		
		assertEquals(1,	arrayResultat.size());
		assertEquals(PlageModeCalcul.STR_MODE_CALCUL_TITULAIRE + " du 01/02/2011 au 31/08/2015", arrayResultat.get(0).toString());
		
		
	}
	
	@Test
	public void testfusionnerPlagesVoisines() {
		
		List<PlageModeCalcul> listePlage = new ArrayList<PlageModeCalcul>(); 
		
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2013"), DateCtrlHamac.stringToDate("01/10/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/10/2013"), DateCtrlHamac.stringToDate("31/12/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2014"), DateCtrlHamac.stringToDate("30/06/2014"), true));
		
		
		List<PlageModeCalcul> arrayResultat = PlageModeCalculService.fusionnerPlagesVoisines(listePlage);
		
		assertEquals(1,	arrayResultat.size());
		assertEquals(DateCtrlHamac.stringToDate("01/01/2013"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("30/06/2014"), arrayResultat.get(0).getDateFin());
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		
		
		
		listePlage = new ArrayList<PlageModeCalcul>(); 
		
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2013"), DateCtrlHamac.stringToDate("01/10/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("02/10/2013"), DateCtrlHamac.stringToDate("31/12/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2014"), DateCtrlHamac.stringToDate("30/06/2014"), true));
		
		
		arrayResultat = PlageModeCalculService.fusionnerPlagesVoisines(listePlage);
		
		assertEquals(1,	arrayResultat.size());
		assertEquals(DateCtrlHamac.stringToDate("01/01/2013"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("30/06/2014"), arrayResultat.get(0).getDateFin());
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		
		
		
		
		
		
		listePlage = new ArrayList<PlageModeCalcul>(); 
		
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2013"), DateCtrlHamac.stringToDate("01/10/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/10/2013"), DateCtrlHamac.stringToDate("31/12/2013"), false));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2014"), DateCtrlHamac.stringToDate("30/06/2014"), true));
		
		
		arrayResultat = PlageModeCalculService.fusionnerPlagesVoisines(listePlage);
		
		assertEquals(3,	arrayResultat.size());
		assertEquals(DateCtrlHamac.stringToDate("01/01/2013"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("01/10/2013"), arrayResultat.get(0).getDateFin());
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		
		assertEquals(DateCtrlHamac.stringToDate("01/10/2013"), arrayResultat.get(1).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("31/12/2013"), arrayResultat.get(1).getDateFin());
		assertEquals(false, arrayResultat.get(1).isModeCalculTitulaire());
		
		assertEquals(DateCtrlHamac.stringToDate("01/01/2014"), arrayResultat.get(2).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("30/06/2014"), arrayResultat.get(2).getDateFin());
		assertEquals(true, arrayResultat.get(2).isModeCalculTitulaire());
		

		listePlage = new ArrayList<PlageModeCalcul>(); 
		
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2013"), DateCtrlHamac.stringToDate("01/10/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/10/2013"), DateCtrlHamac.stringToDate("31/12/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2014"), DateCtrlHamac.stringToDate("30/06/2014"), false));
		
		
		arrayResultat = PlageModeCalculService.fusionnerPlagesVoisines(listePlage);

		Collections.sort(arrayResultat, new PlageModeCalculCompare());
		
		assertEquals(2,	arrayResultat.size());
		assertEquals(DateCtrlHamac.stringToDate("01/01/2013"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("31/12/2013"), arrayResultat.get(0).getDateFin());
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		
		assertEquals(DateCtrlHamac.stringToDate("01/01/2014"), arrayResultat.get(1).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("30/06/2014"), arrayResultat.get(1).getDateFin());
		assertEquals(false, arrayResultat.get(1).isModeCalculTitulaire());
		
		listePlage = new ArrayList<PlageModeCalcul>(); 
		
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2013"), DateCtrlHamac.stringToDate("01/10/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/10/2013"), DateCtrlHamac.stringToDate("31/12/2013"), true));
		listePlage.add(new PlageModeCalcul(DateCtrlHamac.stringToDate("01/01/2014"), null, false));
		
		
		arrayResultat = PlageModeCalculService.fusionnerPlagesVoisines(listePlage);

		Collections.sort(arrayResultat, new PlageModeCalculCompare());
		
		assertEquals(2,	arrayResultat.size());
		assertEquals(DateCtrlHamac.stringToDate("01/01/2013"), arrayResultat.get(0).getDateDebut());
		assertEquals(DateCtrlHamac.stringToDate("31/12/2013"), arrayResultat.get(0).getDateFin());
		assertEquals(true, arrayResultat.get(0).isModeCalculTitulaire());
		
		assertEquals(DateCtrlHamac.stringToDate("01/01/2014"), arrayResultat.get(1).getDateDebut());
		assertEquals(null, arrayResultat.get(1).getDateFin());
		assertEquals(false, arrayResultat.get(1).isModeCalculTitulaire());
		
		
	}
	
	
}
