/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class TimeCtrlTest {

	/**
	 * Test method for
	 * {@link org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl#formatter(java.lang.String)}
	 * .
	 */
	@Test
	public void testFormatter() {
		assertEquals("08:00", TimeCtrl.formatter("8", true));
		assertEquals("23:59", TimeCtrl.formatter("80", true));
		assertEquals("80:00", TimeCtrl.formatter("80", false));
		assertEquals("08:00", TimeCtrl.formatter("800", true));
		assertEquals("08:15", TimeCtrl.formatter("815", true));
		assertEquals("23:59", TimeCtrl.formatter("8000", true));
		assertEquals("80:00", TimeCtrl.formatter("8000", false));
		assertEquals("23:59", TimeCtrl.formatter("80000", true));
		assertEquals("800:00", TimeCtrl.formatter("80000", false));
		assertEquals("20:00", TimeCtrl.formatter("aze20:00", true));
		assertEquals("20:00", TimeCtrl.formatter("2qs0d:s00", true));
		assertEquals("20:00", TimeCtrl.formatter("2qs0d:s:0dfsfdsqdfsq0", true));
		assertEquals("08:15", TimeCtrl.formatter("8,15", true));
		assertEquals("00:00", TimeCtrl.formatter(null, true));
	}

}
