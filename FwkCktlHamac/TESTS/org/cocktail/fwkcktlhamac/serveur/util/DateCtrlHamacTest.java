/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class DateCtrlHamacTest {

	// XXX tmp en heure d'été
	static {
		NSTimeZone tz =  NSTimeZone.timeZoneWithName("UTC", true);
		NSTimeZone.setDefaultTimeZone(tz);
		NSTimeZone.setDefault(tz);
		java.util.TimeZone.setDefault(tz);
	}

	@Test
	public void testNbJourMoisTrentieme() {

		assertEquals(1, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/01/2012")));

		assertEquals(30, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/02/2012")));

		assertEquals(30, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/03/2012")));

		assertEquals(20, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("20/01/2012")));

		assertEquals(0, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("01/02/2012"),
				DateCtrlHamac.stringToDate("15/02/2012")));

		// fins de mois

		assertEquals(21, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/01/2012"),
				DateCtrlHamac.stringToDate("10/01/2012"),
				DateCtrlHamac.stringToDate("31/01/2012")));

		assertEquals(30, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/02/2013"),
				DateCtrlHamac.stringToDate("01/02/2013"),
				DateCtrlHamac.stringToDate("28/02/2013")));

		assertEquals(16, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/02/2013"),
				DateCtrlHamac.stringToDate("15/02/2013"),
				DateCtrlHamac.stringToDate("28/02/2013")));

		assertEquals(3, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/02/2013"),
				DateCtrlHamac.stringToDate("28/02/2013"),
				DateCtrlHamac.stringToDate("28/02/2013")));

		assertEquals(4, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/02/2013"),
				DateCtrlHamac.stringToDate("27/02/2013"),
				DateCtrlHamac.stringToDate("28/02/2013")));

		assertEquals(3, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/03/2013"),
				DateCtrlHamac.stringToDate("28/03/2013"),
				DateCtrlHamac.stringToDate("31/03/2013")));

		assertEquals(2, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/03/2013"),
				DateCtrlHamac.stringToDate("29/03/2013"),
				DateCtrlHamac.stringToDate("31/03/2013")));

		assertEquals(1, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/03/2013"),
				DateCtrlHamac.stringToDate("30/03/2013"),
				DateCtrlHamac.stringToDate("31/03/2013")));

		assertEquals(1, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/03/2013"),
				DateCtrlHamac.stringToDate("31/03/2013"),
				DateCtrlHamac.stringToDate("31/03/2013")));

		// milieux
		assertEquals(12, DateCtrlHamac.nbJourMoisTrentieme(
				DateCtrlHamac.stringToDate("01/12/2013"),
				DateCtrlHamac.stringToDate("09/12/2013"),
				DateCtrlHamac.stringToDate("20/12/2013")));

	}

	@Test
	public void testGetTotalJours360() {
		
		assertEquals(
				30,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/01/2012"),
						DateCtrlHamac.stringToDate("31/01/2012")));
		
		assertEquals(
				30,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/04/2012"),
						DateCtrlHamac.stringToDate("30/04/2012")));

		assertEquals(
				30,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/02/2012"),
						DateCtrlHamac.stringToDate("29/02/2012")));
		
		assertEquals(
				30,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/02/2013"),
						DateCtrlHamac.stringToDate("28/02/2013")));
		
		
		assertEquals(
				180,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/09/2012"),
						DateCtrlHamac.stringToDate("28/02/2013")));

		assertEquals(
				360,
				DateCtrlHamac.getTotalJours360(
						DateCtrlHamac.stringToDate("01/09/2012"),
						DateCtrlHamac.stringToDate("31/08/2013")));
		
	}

	private final static String DATE_HEURE_FORMAT = "%d/%m/%Y %H:%M:%S";

	@Test
	public void testGetNbDemiJourneeEntre() {

		assertEquals(1,
				DateCtrlHamac.getNbDemiJourneeEntre(
						DateCtrlHamac.stringToDate("12/06/2013 12:00:00", DATE_HEURE_FORMAT),
						DateCtrlHamac.stringToDate("12/06/2013 12:00:00", DATE_HEURE_FORMAT)));

		assertEquals(2,
				DateCtrlHamac.getNbDemiJourneeEntre(
						DateCtrlHamac.stringToDate("12/06/2013 00:00:00", DATE_HEURE_FORMAT),
						DateCtrlHamac.stringToDate("12/06/2013 12:00:00", DATE_HEURE_FORMAT)));

		assertEquals(3,
				DateCtrlHamac.getNbDemiJourneeEntre(
						DateCtrlHamac.stringToDate("12/06/2013 12:00:00", DATE_HEURE_FORMAT),
						DateCtrlHamac.stringToDate("13/06/2013 12:00:00", DATE_HEURE_FORMAT)));

		assertEquals(4,
				DateCtrlHamac.getNbDemiJourneeEntre(
						DateCtrlHamac.stringToDate("12/06/2013 00:00:00", DATE_HEURE_FORMAT),
						DateCtrlHamac.stringToDate("13/06/2013 12:00:00", DATE_HEURE_FORMAT)));

	}

	@Test
	public void testBeforeAfter() {

		NSTimestamp dateDeb1 = DateCtrlHamac.stringToDate("16/09/2013 08:30:00", DATE_HEURE_FORMAT);
		NSTimestamp dateFin1 = DateCtrlHamac.stringToDate("16/09/2013 09:30:00", DATE_HEURE_FORMAT);
		NSTimestamp dateDeb2 = DateCtrlHamac.stringToDate("16/09/2013 10:00:00", DATE_HEURE_FORMAT);
		NSTimestamp dateFin2 = DateCtrlHamac.stringToDate("16/09/2013 10:30:00", DATE_HEURE_FORMAT);

		assertEquals(true,
				DateCtrlHamac.isBeforeEq(dateDeb1, dateDeb2));
		assertEquals(false,
				DateCtrlHamac.isAfterEq(dateFin1, dateFin2));

	}
	
	@Test
	public void testIsDatesCompriseEnNSemaines() {
		
		NSTimestamp date1 = DateCtrlHamac.stringToDate("01/01/2013");
		NSTimestamp date2 = DateCtrlHamac.stringToDate("02/01/2013");
		
		assertEquals(true, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 1));
		
		
		date1 = DateCtrlHamac.stringToDate("01/01/2013");
		date2 = DateCtrlHamac.stringToDate("08/01/2013");
		
		assertEquals(true, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 1));
		
		date1 = DateCtrlHamac.stringToDate("01/01/2013");
		date2 = DateCtrlHamac.stringToDate("10/01/2013");
		
		assertEquals(false, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 1));
		
		date1 = DateCtrlHamac.stringToDate("01/01/2013");
		date2 = DateCtrlHamac.stringToDate("10/01/2013");
		
		assertEquals(true, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 2));
		
		date1 = DateCtrlHamac.stringToDate("31/12/2013");
		date2 = DateCtrlHamac.stringToDate("03/01/2014");
		
		assertEquals(true, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 1));
		
		date1 = DateCtrlHamac.stringToDate("31/12/2013");
		date2 = DateCtrlHamac.stringToDate("03/01/2013");
		
		assertEquals(false, DateCtrlHamac.isDatesCompriseEnNSemaines(date1, date2, 1));
		
	}
	
	public void testIsAfterNull() {
		NSTimestamp date1 = DateCtrlHamac.stringToDate("01/01/2014");
		NSTimestamp date2 = DateCtrlHamac.stringToDate("02/01/2013");
		
		assertEquals(true, DateCtrlHamac.isAfterNull(date1, date2));
		
		date1 = null;
		date2 = DateCtrlHamac.stringToDate("02/01/2013");
		
		assertEquals(true, DateCtrlHamac.isAfterNull(date1, date2));
		
		date1 = null;
		date2 = null;
		
		assertEquals(false, DateCtrlHamac.isAfterNull(date1, date2));
		
		date1 = DateCtrlHamac.stringToDate("02/01/2013");
		date2 = null;
		
		assertEquals(false, DateCtrlHamac.isAfterNull(date1, date2));
		
	}
	
	
}
