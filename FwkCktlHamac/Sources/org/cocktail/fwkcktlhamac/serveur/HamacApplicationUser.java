/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.fwkcktlhamac.serveur;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlgrh.common.metier.InfoCarriereContrat;
import org.cocktail.fwkcktlhamac.serveur.metier.A_EOSource;
import org.cocktail.fwkcktlhamac.serveur.metier.A_FwkCktlHamacRecord;
import org.cocktail.fwkcktlhamac.serveur.metier.EODroit;
import org.cocktail.fwkcktlhamac.serveur.metier.EOMessageInterne;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPersonnePrivilege;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPreference;
import org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Acreditation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.ListePersonne;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageModeCalcul;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.HamacMinuteToJourFormatter;
import org.cocktail.fwkcktlhamac.serveur.util.PlageModeCalculService;
import org.cocktail.fwkcktlhamac.serveur.util.SimpleDebutFinCompare;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Repré̩sente un utilisateur de l'application Hamac. Seules les specificites
 * exclusives à l'application sont decrites dans cette classe
 */
public class HamacApplicationUser
		extends ApplicationUser {

	private Boolean _isSuperAdmin;
	private Boolean _isSuperFonctionnel;

	private NSArray<Acreditation> _acreditationArray;
	private NSArray<EOStructure> _eoStructureAcrediteArray;
	private NSArray<Integer> _persIdCibleAcrediteArray;

	private NSMutableArray<EOStructure> _eoStructureAcceuilArray;
	private NSArray<EOPlanning> _eoPlanningVisibleArray;
	private NSArray<EOPlanning> _eoPlanningDelegationArray;
	
	private IPersonne personne;
	
	/** */
	private Boolean isMailRecipisse = Boolean.FALSE;
	private Boolean isMailRecipisseDelegation = Boolean.FALSE;
	private Map<String, List<Integer>> cStructureNonReceptionMailSaufPersIdMap = new HashMap<String, List<Integer>>();
	private Boolean isExporterMotifServeurPlanning = Boolean.FALSE;
	private Integer dureeJourneeAffichage = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR_AFFICHAGE);
	private NSMutableArray<ListePersonne> listePersonneArray = new NSMutableArray<ListePersonne>();
	private String multiPlanningRadio = EOPreference.MULTI_PLANNING_RADIO_TYPE_SERVICE;

	private HamacMinuteToJourFormatter hamacMinuteToJourFormatter = null;

	// 10 mois
	private static int seuilMoisChgtModeCalculContractuel =
			HamacCktlConfig.intForKey(EOParametre.SEUIL_MOIS_CHGT_MODE_CALCUL_CONTRACTUEL);
	
	private static int seuilSemainesContratsConcomittantsContractuel =
			HamacCktlConfig.intForKey(EOParametre.SEUIL_SEMAINES_CONTRATS_CONCOMITANTS_CONTRACTUEL);

	private boolean isPlageModeCalculArrayCalculee = false;
	private List<PlageModeCalcul> _plageModeCalculArray;

	private Boolean _isAuMoinsQuelqueChoseAValider;
	
	public HamacApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
		postConstructeur();
	}

	public HamacApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
		postConstructeur();
	}

	/**
	 * Les apis suivantes necessitent l'identification de l'application via les
	 * tables de Jefy_admin
	 * 
	 * @link 
	 *       https://sites.google.com/a/cocktail.org/developpeurs/wo#TOC-D-l-guer-la
	 *       -gestion-des-droits-d-un
	 * 
	 */
	public HamacApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		super(ec, tyapStrId, persId);
		postConstructeur();
	}

	public HamacApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
		postConstructeur();
	}

	/**
	 * 
	 */
	private void postConstructeur() {
		// changer les preferences
		EOPreference.loadPreferences(this);
	}

	/**
	 * @return
	 */
	public boolean isSuperAdmin() {
		if (_isSuperAdmin == null) {
			_isSuperAdmin = Boolean.FALSE;

			String cStructureAdmin = HamacCktlConfig.stringForKey(EOParametre.C_STRUCTURE_ADMIN_GLOBAL);

			EOStructure eoStructureAdminGlobal = EOStructure.structurePourCode(getEditingContext(), cStructureAdmin);
			int i = 0;
			while (i < eoStructureAdminGlobal.toRepartStructuresElts().count() && _isSuperAdmin.booleanValue() == false) {
				IPersonne membre = eoStructureAdminGlobal.toRepartStructuresElts().objectAtIndex(i).toPersonneElt();
				if (membre.persId().intValue() == getPersId().intValue()) {
					_isSuperAdmin = Boolean.TRUE;
				}
				i++;
			}

		}
		return _isSuperAdmin.booleanValue();
	}

	/**
	 * @return
	 */
	public boolean isSuperFonctionnel() {
		if (_isSuperFonctionnel == null) {
			_isSuperFonctionnel = Boolean.FALSE;

			String cStructureRhGlobal = HamacCktlConfig.stringForKey(EOParametre.C_STRUCTURE_RH_GLOBAL);

			EOStructure eoStructureRhGlobal = EOStructure.structurePourCode(getEditingContext(), cStructureRhGlobal);
			int i = 0;
			while (i < eoStructureRhGlobal.toRepartStructuresElts().count() && _isSuperFonctionnel.booleanValue() == false) {
				IPersonne membre = eoStructureRhGlobal.toRepartStructuresElts().objectAtIndex(i).toPersonneElt();
				if (membre.persId().intValue() == getPersId().intValue()) {
					_isSuperFonctionnel = Boolean.TRUE;
				}
				i++;
			}

		}
		return _isSuperFonctionnel.booleanValue();
	}

	/**
	 * TODO
	 * 
	 * Liste des acréditations de l'utilisateur
	 * 
	 * @return
	 */
	public NSArray<Acreditation> getAcreditationArray() {
		if (_acreditationArray == null) {

			_acreditationArray = new NSArray<Acreditation>();

			// d'apres les droits
			_acreditationArray = EODroit.getAcreditationArrayPourTitulaire(getEditingContext(), getPersId());

		}
		return _acreditationArray;
	}
	
	
	

	/**
	 * La liste des structures dont l'agent est responsable via Annuaire, parmi la
	 * liste des services autorisés pendant la période courante
	 * 
	 * @return
	 */
	public List<EOStructure> getEoStructureResponsableAnnuaireArray() {
		List<EOStructure> array = null;

		EOPeriode eoPeriodeCourante = EOPeriode.getCurrentPeriode(getEditingContext());

		if (eoPeriodeCourante != null) {

			NSArray<EOStructure> eoStructureArray = (NSArray<EOStructure>) eoPeriodeCourante.tosServiceAutorise().valueForKey(EOServiceAutorise.TO_STRUCTURE_KEY);
			array = EOQualifier.filteredArrayWithQualifier(
					eoStructureArray, ERXQ.equals(EOStructure.TO_RESPONSABLE_KEY, EOIndividu.individuWithPersId(getEditingContext(), getPersId())));

		}

		return array;
	}
	
	
	public boolean getEoStructureResponsableAnnuaireArrayNonVide() {
		boolean listeStructureNonVide = false;
		if (getEoStructureResponsableAnnuaireArray() != null) {
			listeStructureNonVide = getEoStructureResponsableAnnuaireArray().size() > 0;
		}
		return listeStructureNonVide;
	}
	

	/**
	 * Page d'accueil, la liste des services disponibles pour l'affichage du
	 * planning de service
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getEoStructureAcceuilArray() {

		if (_eoStructureAcceuilArray == null) {

			long tDebut = System.currentTimeMillis();

			_eoStructureAcceuilArray = new NSMutableArray<EOStructure>();

			if (isSuperAdmin() ||
					isSuperFonctionnel()) {
				// super admin ou super fonctionnel , c'est toutes les structures
				// autorisées
				NSArray<EOPeriode> eoPeriodeArray = EOPeriode.findSortedPeriodeArray(
						getEditingContext(), null, null);

				for (EOPeriode eoPeriode : eoPeriodeArray) {

					for (EOServiceAutorise eoServiceAutorise : eoPeriode.tosServiceAutorise()) {

						EOStructure eoStructure = eoServiceAutorise.toStructure();

						if (!_eoStructureAcceuilArray.containsObject(eoStructure)) {
							_eoStructureAcceuilArray.addObject(eoServiceAutorise.toStructure());
						}
					}
				}

			} else {

				// sinon, on prend selon dans les plannings dispos
				
				for (EOPlanning eoPlanning : getEoPlanningAcceuilArray()) {

					NSArray<A_EOSource> eoSourceArray = eoPlanning.getEoSourceArray();

					for (A_EOSource eoSource : eoSourceArray) {
						
						if (!_eoStructureAcceuilArray.containsObject(eoSource.toStructureAffectation()) && getEoStructureAcrediteArray().contains(eoSource.toStructureAffectation())) {
							_eoStructureAcceuilArray.addObject(eoSource.toStructureAffectation());
						}

					}

				}

			}

			// classement alpha
			_eoStructureAcceuilArray = new NSMutableArray<EOStructure>(
					CktlSort.sortedArray(_eoStructureAcceuilArray, EOStructure.LC_STRUCTURE_KEY));

			CktlLog.log("HamacApplicationUser.getEoStructureAcceuilArray() : " + (System.currentTimeMillis() - tDebut) + "ms");

		}

		return _eoStructureAcceuilArray;
	}

	/**
	 * Page d'accueil, la liste des plannings disponibles pour l'affichage du
	 * planning de service
	 * 
	 * @return
	 */
	public NSArray<EOPlanning> getEoPlanningAcceuilArray() {

		if (_eoPlanningVisibleArray == null) {

			if (isSuperAdmin() || isSuperFonctionnel()) {

				// super admin ou super fonctionnel , c'est tous les plannings
				_eoPlanningVisibleArray = new NSMutableArray<EOPlanning>(
						EOPlanning.fetchAll(
								getEditingContext(),
								ERXQ.equals(EOPlanning.NATURE, EOPlanning.REEL)));

			} else {

				_eoPlanningVisibleArray = new NSMutableArray<EOPlanning>();
				_eoStructureAcrediteArray = new NSMutableArray<EOStructure>();
				_persIdCibleAcrediteArray = new NSMutableArray<Integer>();
				
				// sinon, on prend ce qu'il y a dans les acreditations

				NSArray<Acreditation> acreditionArray = getAcreditationArray();

				for (Acreditation acreditation : acreditionArray) {

					IPersonne cible = acreditation.getPersonneCible();

					NSArray<EOPlanning> eoPlanningArray = null;

					if (cible.isIndividu()) {
						// le planning de l'individu
						eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
								getEditingContext(), acreditation.getPersonneCible().persId(), EOPlanning.REEL,
								A_FwkCktlHamacRecord.NON, null, acreditation.getEoPeriode());
						
						_persIdCibleAcrediteArray.add(acreditation.getPersonneCible().persId());
						
						
						
					} else if (cible.isStructure()) {

						// les plannings associés au service
						EOStructure eoStructure = (EOStructure) cible;
						eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
								getEditingContext(), null, EOPlanning.REEL,
								A_FwkCktlHamacRecord.NON, eoStructure.cStructure(), acreditation.getEoPeriode());
						
						_eoStructureAcrediteArray.add(eoStructure);
						
						// héritage éventuel
						if (acreditation.isHeritage()) {
							// les plannings des sous-services
							NSArray<EOStructure> eoSousStructureArray = EOPeriode.getEOStructureAutoriseeArrayPourComposante(
									acreditation.getEoPeriode(), eoStructure, true, true);

							for (EOStructure eoSousStructure : eoSousStructureArray) {

								NSArray<EOPlanning> eoSousPlanningArray = EOPlanning.findSortedEoPlanningArray(
										getEditingContext(), null, EOPlanning.REEL,
										A_FwkCktlHamacRecord.NON, eoSousStructure.cStructure(), acreditation.getEoPeriode());
								
								eoPlanningArray = eoPlanningArray.arrayByAddingObjectsFromArray(eoSousPlanningArray);

								_eoStructureAcrediteArray.add(eoSousStructure);
								
							}

						}
					
					}
					
					determinerVisibiliteOnglets(acreditation, eoPlanningArray);
					
					
					if (!NSArrayCtrl.isEmpty(eoPlanningArray)) {
						_eoPlanningVisibleArray = _eoPlanningVisibleArray.arrayByAddingObjectsFromArray(eoPlanningArray);
					}
				}

				_eoStructureAcrediteArray = NSArrayCtrl.removeDuplicate(_eoStructureAcrediteArray);
				
			}

		}

		return _eoPlanningVisibleArray;

	}

	private void determinerVisibiliteOnglets(Acreditation acreditation,
			NSArray<EOPlanning> eoPlanningArray) {
		
		for (EOPlanning eoPlanning : eoPlanningArray) {
			eoPlanning.setOngletsVisibles(false);
		}
		
		for (EOPlanning eoPlanning : eoPlanningArray) {
			if (!acreditation.getEoTypeNiveauDroit().isCodeObservateur() || isProprietaireOuDelegue(eoPlanning)) {
				eoPlanning.setOngletsVisibles(true);
			}
		}
	}
	
	/**
	 * Liste des plannings surlesquels l'utilisateur connecte à le droit de
	 * délégation
	 * 
	 * @return
	 */
	public NSArray<EOPlanning> getEoPlanningDelegationArray() {

		if (_eoPlanningDelegationArray == null) {

			_eoPlanningDelegationArray = new NSMutableArray<EOPlanning>();
			
			// les acréditations de type délégation
			NSArray<Acreditation> acreditionDelegationArray = EOQualifier.filteredArrayWithQualifier(
					getAcreditationArray(),
					ERXQ.equals(
							Acreditation.EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY,
							EOTypeNiveauDroit.CODE_DELEGUE));

			for (Acreditation acreditation : acreditionDelegationArray) {

				IPersonne cible = acreditation.getPersonneCible();

				NSArray<EOPlanning> eoPlanningArray = null;

				if (cible.isIndividu()) {
					// le planning de l'individu
					eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
								getEditingContext(), acreditation.getPersonneCible().persId(), EOPlanning.REEL,
								A_FwkCktlHamacRecord.NON, null, acreditation.getEoPeriode());
				} else if (cible.isStructure()) {
					// les plannings associés au service
					EOStructure eoStructure = (EOStructure) cible;
					eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
								getEditingContext(), null, EOPlanning.REEL,
								A_FwkCktlHamacRecord.NON, eoStructure.cStructure(), acreditation.getEoPeriode());
									
					// héritage éventuel
					if (acreditation.isHeritage()) {
						// les plannings des sous-services
						NSArray<EOStructure> eoSousStructureArray = EOPeriode.getEOStructureAutoriseeArrayPourComposante(
								acreditation.getEoPeriode(), eoStructure, true, true);

						for (EOStructure eoSousStructure : eoSousStructureArray) {

							NSArray<EOPlanning> eoSousPlanningArray = EOPlanning.findSortedEoPlanningArray(
									getEditingContext(), null, EOPlanning.REEL,
									A_FwkCktlHamacRecord.NON, eoSousStructure.cStructure(), acreditation.getEoPeriode());

							eoPlanningArray = eoPlanningArray.arrayByAddingObjectsFromArray(eoSousPlanningArray);
							
						}

					}
				}

				if (!NSArrayCtrl.isEmpty(eoPlanningArray)) {
					_eoPlanningDelegationArray = _eoPlanningDelegationArray.arrayByAddingObjectsFromArray(eoPlanningArray);

				}
			}

		}

		return _eoPlanningDelegationArray;

	}

	/**
	 * Teste si l'utilisateur est proprietaire ou delegue (pre filtre pour
	 * certaines actions)
	 * 
	 * @return
	 */
	public boolean isProprietaireOuDelegue(EOPlanning eoPlanning) {
		boolean isProprietaireOuDelegue = false;

		// droit sur son propre planning
		if (eoPlanning.persId().intValue() == getPersId().intValue()) {
			isProprietaireOuDelegue = true;
		}

		// droit sur les agents pour qui il y a pouvoir de délégation
		if (!isProprietaireOuDelegue) {
			isProprietaireOuDelegue = isPlanningSousDelegation(eoPlanning);
		}

		return isProprietaireOuDelegue;
	}

	/**
	 * test sur la période et le pers_id, on ignore la nature
	 * 
	 * @param eoPlanning
	 * @return
	 */
	private boolean isPlanningSousDelegation(EOPlanning eoPlanning) {

		boolean isPlanningSousDelegation = false;

		EOQualifier qual = ERXQ.and(
				ERXQ.equals(EOPlanning.TO_PERIODE_KEY, eoPlanning.toPeriode()),
				ERXQ.equals(EOPlanning.PERS_ID_KEY, eoPlanning.persId()));
		NSArray<EOPlanning> array = EOQualifier.filteredArrayWithQualifier(
				getEoPlanningDelegationArray(), qual);

		if (array.count() > 0) {
			isPlanningSousDelegation = true;
		}

		return isPlanningSousDelegation;
	}

	// XXX a renommer modifier

	/**
	 * Indique si la personne connectée possède le droit de modification sur
	 * l'association horaires semaines
	 * 
	 * @param eoPlanning
	 * @return
	 */
	public boolean isAutoriseAModifierAssociationHoraireSemaine(EOPlanning eoPlanning) {
		boolean isAutorise = false;

		// droit sur son propre planning
		if (eoPlanning.persId().intValue() == getPersId().intValue()) {
			isAutorise = true;
		}

		if (!isAutorise) {
			isAutorise = isPeriodeModifiableParAdmin(eoPlanning);
		}
		
		// délégué
		if (!isAutorise) {
			isAutorise = isPlanningSousDelegation(eoPlanning);
		}

		return isAutorise;
	}

	
	/**
	 * Si l'utilisation est superAdmin, et si la date du jour n'a pas encore atteint la date de clôture de la période
	 * alors l'utilisateur peut effectuer diverses modifications sur le planning
	 * @return
	 */
	public boolean isPeriodeModifiableParAdmin(EOPlanning planning) {
		
		boolean periodeModifiableParAdmin = false;
		
		if (isSuperAdmin() || isSuperFonctionnel()) {
			NSTimestamp dateCloturePeriode = HamacCktlConfig.dateForKey(EOParametre.DATE_CLOTURE_PERIODE, planning.toPeriode(), planning.toPeriode().perDFin());
			if (dateCloturePeriode != null && DateCtrlHamac.isBeforeEq(DateCtrl.now(), dateCloturePeriode)) {
				periodeModifiableParAdmin = true;
			}
		}
		
		return periodeModifiableParAdmin;
	}
	
	//

	private NSArray<EOPersonnePrivilege> eoPersonnePrivilegeArray;

	private NSArray<EOPersonnePrivilege> getEoPersonnePrivilegeArray() {
		if (eoPersonnePrivilegeArray == null) {
			EOQualifier qual = ERXQ.equals(
					EOPersonnePrivilege.PERS_ID_BENEFICIAIRE_KEY, getPersId());

			eoPersonnePrivilegeArray = EOPersonnePrivilege.fetchAll(getEditingContext(), qual);
		}
		return eoPersonnePrivilegeArray;
	}

	public boolean isDepDroitConge(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_DEP_DROIT_CONGE_KEY);
	}

	public boolean isDepSemHautes(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_DEP_SEM_HAUTES_KEY);
	}

	public boolean isHorsNormes(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_HORS_NORMES_KEY);
	}

	public boolean isPasseDroit(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_PASSE_DROIT_KEY);
	}

	public boolean isTpa(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_TPA_KEY);
	}
	
	public boolean isPauseMeridienneLibre(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_PAUSE_MERIDIENNE_LIBRE_KEY);
	}
	
	public boolean isAucuneBonification(EOPeriode eoPeriode) {
		return isPrivilege(eoPeriode, EOPersonnePrivilege.TEM_AUCUNE_BONIFICATION_KEY);
	}
	

	private boolean isPrivilege(EOPeriode eoPeriode, String attribut) {
		boolean isPrivilege = false;

		EOQualifier qualPeriode = ERXQ.equals(
				EOPersonnePrivilege.TO_PERIODE_KEY, eoPeriode);

		NSArray<EOPersonnePrivilege> array = EOQualifier.filteredArrayWithQualifier(
				getEoPersonnePrivilegeArray(), qualPeriode);

		if (array.count() > 0) {
			isPrivilege = array.objectAtIndex(0).is(attribut);
		}

		return isPrivilege;
	}

	// informations relatives au preferences

	public void setIsMailRecipisse(boolean value) {
		isMailRecipisse = Boolean.valueOf(value);
	}

	public void setIsMailRecipisseDelegation(boolean value) {
		isMailRecipisseDelegation = Boolean.valueOf(value);
	}

	public void setIsNonReceptionMailPourStructure(boolean value, String cStructure, List<Integer> listePersId) {
		if (value) {
			if (!isNonReceptionMailPourStructure(cStructure)) {
				cStructureNonReceptionMailSaufPersIdMap.put(cStructure, listePersId);
			}
		} else {
			if (isNonReceptionMailPourStructure(cStructure)) {
				cStructureNonReceptionMailSaufPersIdMap.remove(cStructure);
			}
		}
	}

	public void setIsExporterMotifServeurPlanning(boolean value) {
		isExporterMotifServeurPlanning = Boolean.valueOf(value);
	}

	public void setDureeJourneeAffichage(Integer value) {
		dureeJourneeAffichage = value;
	}

	public void resetAllNonReceptionMailPourStructure() {
		cStructureNonReceptionMailSaufPersIdMap = new HashMap<String, List<Integer>>();
	}

	/**
	 * Indique si l'utilisateur souhaite recevoir les mails de recipisse lors
	 * d'une validation par un tiers d'un planning / occupation concernnat un
	 * agent dont il a la responsabilité.
	 */
	public boolean isMailRecipisse() {
		return isMailRecipisse.booleanValue();
	}

	/**
	 * Indique si l'utilisateur souhaite recevoir les mails de recipisse lors
	 * d'une validation par un tiers d'un planning / occupation pour une demande
	 * realisee par delegation (par lui pour un autre)
	 */
	public boolean isMailRecipisseDelegation() {
		return isMailRecipisseDelegation.booleanValue();
	}

	/**
	 * Indique si l'utilisateur souhaite que les motifs de ses absences soit
	 * spécifiés dans les exports du serveur de planning
	 */
	public boolean isExporterMotifServeurPlanning() {
		return isExporterMotifServeurPlanning.booleanValue();
	}

	/**
	 * Indique la base de convertion jour<->minutes pour l'affichage
	 */
	public int getDureeJourneeAffichage() {
		return dureeJourneeAffichage.intValue();
	}

	/**
	 * Indique si l'utilisateur ne souhaite pas recevoir les messages de
	 * validation emmanant du service
	 * 
	 * @param cStructure
	 */
	public boolean isNonReceptionMailPourStructure(String cStructure) {
		return cStructureNonReceptionMailSaufPersIdMap.containsKey(cStructure);
	}
	
	
	/**
	 * Indique si l'utilisateur ne souhaite pas recevoir les messages de
	 * validation emmanant du service
	 * 
	 * @param cStructure
	 */
	public boolean isNonReceptionMailPourStructureEtIndividu(String cStructure, Integer persId) {
		boolean retour = false;
		if (cStructureNonReceptionMailSaufPersIdMap.containsKey(cStructure)) {
			retour = true;
			List<Integer> listPersIdReceptionMail = cStructureNonReceptionMailSaufPersIdMap.get(cStructure);
			if (listPersIdReceptionMail.contains(persId)) {
				retour = false;
			}
		}
		return retour;
	}
	
	
	/**
	 * Retourne les individus pour lesquels on souhaite tout de même recevoir les mails de validation
	 * 
	 * @param cStructure
	 */
	public List<Integer> getIsNonReceptionMailPourStructureSaufPersId(String cStructure) {
		return cStructureNonReceptionMailSaufPersIdMap.get(cStructure);
	}
	
	

	/**
	 * @param cStructure
	 */
	public boolean isNonReceptionMailPourToutesStructuresEtIndividu(NSArray<EOStructure> eoStructureArray, Integer persId) {
		boolean isNonReceptionMail = true;

		for (EOStructure eoStructure : eoStructureArray) {
			isNonReceptionMail =
					isNonReceptionMail && isNonReceptionMailPourStructureEtIndividu(eoStructure.cStructure(), persId);
		}

		return isNonReceptionMail;
	}

	/**
	 * Date de fin d'application du mode de calcul contractuel. Si
	 * <code>null</code>, alors ce dernier doit être tout le temps appliqué.
	 * Sinon, le mode titulaire démarre à cette date
	 * 
	 * @return
	 */
	public final List<PlageModeCalcul> getPlageModeCalculArray() {

		if (isPlageModeCalculArrayCalculee == false) {
			
			EOIndividu eoIndividu = EOIndividu.individuWithPersId(getEditingContext(), getPersId());

			NSTimestamp dateDebutRecupCarrCont = EOPeriode.getDateDebutPremierePeriode().timestampByAddingGregorianUnits(0, -seuilMoisChgtModeCalculContractuel, 0, 0, 0, 0);
			
			NSArray<InfoCarriereContrat> eoCarriereContratArray = InfoCarriereContrat.carrieresContratsPourPeriode(getEditingContext(), eoIndividu, dateDebutRecupCarrCont, null, false);

			List<PlageModeCalculService.SimpleDebutFin> periodeArray = new ArrayList<PlageModeCalculService.SimpleDebutFin>();
			
			for (InfoCarriereContrat info : eoCarriereContratArray) {

				// carriere ou contrat
				if (!info.estHeberge() && !info.estVacation()) {

					periodeArray.add(
							new PlageModeCalculService.SimpleDebutFin(
									info.dateDebut(),
									info.dateFin(),
									info.estCarriere()));
				}

			}
						
			if (periodeArray.size() == 0) {
				throw new IllegalArgumentException("Aucune carrière ou contrat pour cet agent : " + eoIndividu.getNomPrenomAffichage());
			} else {
				periodeArray = PlageModeCalculService.filtrerDoublons(periodeArray);
				Collections.sort(periodeArray, new SimpleDebutFinCompare());
				_plageModeCalculArray = PlageModeCalculService.getPlageModeCalculPourSortedPeriodes(seuilMoisChgtModeCalculContractuel, seuilSemainesContratsConcomittantsContractuel, periodeArray);
			}

			isPlageModeCalculArrayCalculee = true;
		}

		return _plageModeCalculArray;
	}

	private NSArray<EOMessageInterne> _eoMessageInterneNonLuArray;

	/**
	 * @return
	 */
	public NSArray<EOMessageInterne> getEoMessageInterneNonLuArray() {
		if (_eoMessageInterneNonLuArray == null) {
			_eoMessageInterneNonLuArray = EOMessageInterne.getMessageALirePourPersId(
					getEditingContext(), getPersId());
		}
		return _eoMessageInterneNonLuArray;
	}

	public void clearMessageCache() {
		_eoMessageInterneNonLuArray = null;
	}

	/**
	 * Permet de savoir très rapidement si la personne connectée peut valider
	 * quelque chose. Permet de savoir si la page d'accueil par défaut est
	 * l'acceuil ou son planning
	 */
	public final boolean isAuMoinsQuelqueChoseAValider() {

		if (_isAuMoinsQuelqueChoseAValider == null) {

			boolean isAuMoinsQuelqueChoseAValider = false;

			// admin ou super fonctionnel
			if (isSuperAdmin() || isSuperFonctionnel()) {
				isAuMoinsQuelqueChoseAValider = true;
			}

			// responsable d'un des services autorisés
			if (!isAuMoinsQuelqueChoseAValider) {
				
				EOQualifier qual = ERXQ.and(ERXQ.equals(
						Acreditation.EO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY, getPersId()),
						Acreditation.QUAL_DROIT_VALIDATION);
				
				if (EOQualifier.filteredArrayWithQualifier(getAcreditationArray(), qual).count() > 0) {
					isAuMoinsQuelqueChoseAValider = true;
				}
				
			}

			_isAuMoinsQuelqueChoseAValider = Boolean.valueOf(isAuMoinsQuelqueChoseAValider);

		}

		return _isAuMoinsQuelqueChoseAValider.booleanValue();
	}

	
	/**
	 * 
	 * @return
	 */
	public final HamacMinuteToJourFormatter getHamacMinuteToJourFormatter() {
		if (hamacMinuteToJourFormatter == null) {
			hamacMinuteToJourFormatter = FormatterCtrl.getNewMinuteToJourFormatter(this);
		}
		return hamacMinuteToJourFormatter;
	}

	public final void resetHamacMinuteToJourFormatter() {
		hamacMinuteToJourFormatter = null;
	}

	public final NSMutableArray<ListePersonne> getListePersonneArray() {
		return listePersonneArray;
	}

	public final void setListePersonneArray(NSMutableArray<ListePersonne> listePersonneArray) {
		this.listePersonneArray = listePersonneArray;
	}

	public final String getMultiPlanningRadio() {
		return multiPlanningRadio;
	}

	public final void setMultiPlanningRadio(String multiPlanningRadio) {
		this.multiPlanningRadio = multiPlanningRadio;
	}

	public NSArray<EOStructure> getEoStructureAcrediteArray() {
		return _eoStructureAcrediteArray;
	}

	public NSArray<Integer> getPersIdCibleAcrediteArray() {
		return _persIdCibleAcrediteArray;
	}

	public String getNomPrenomAffichage() {
		return getIPersonne().getNomPrenomAffichage();
	}
	
	private IPersonne getIPersonne() {
		if (personne == null) {
			personne = EOIndividu.individuWithPersId(getEditingContext(), getPersId());
		}
		return personne;
	}
	
	
}
