/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOCetVidage
		extends _EOCetVidage
		implements I_Operation {

	public EOCetVidage() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		// ne pas autoriser le vidage ancien régime si vidé en pérenne
		if ((cviType().equals(TYPE_CET_VIDAGE_INDEMNISATION_ANCIEN_REGIME) ||
				cviType().equals(TYPE_CET_VIDAGE_TRANSFERT_RAFP_ANCIEN_REGIME)) && (
						toPlanning().getVidage(TYPE_CET_VIDAGE_INDEMNISATION_REGIME_PERENNE) != null ||
						toPlanning().getVidage(TYPE_CET_VIDAGE_TRANSFERT_RAFP_REGIME_PERENNE) != null)) {
			throw new NSValidation.ValidationException("Vous ne pouvez pas vider l'ancien régime " +
					"si le régime pérenne l'a déjà été (le vidage du pérenne vide aussi l'ancien)");
		}

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	// vidage du CET suite au départ de l'agent ou non décision sur l'ancien
	// régime
	public final static String TYPE_CET_VIDAGE_INDEMNISATION_ANCIEN_REGIME = "INDEM_ANCIEN";
	public final static String TYPE_CET_VIDAGE_TRANSFERT_RAFP_ANCIEN_REGIME = "RAFP_ANCIEN";
	public final static String TYPE_CET_VIDAGE_INDEMNISATION_REGIME_PERENNE = "INDEM_PERENNE";
	public final static String TYPE_CET_VIDAGE_TRANSFERT_RAFP_REGIME_PERENNE = "RAFP_PERENNE";

	public static EOCetVidage creer(EOEditingContext ec, EOPlanning eoPlanning, String cviType) {
		EOCetVidage vidage = createEOCetVidage(
				ec, cviType, DateCtrlHamac.now(), DateCtrlHamac.now(), eoPlanning);
		return vidage;
	}

	private OperationDelegate operationDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	public boolean isVidageAncien() {
		return cviType().equals(TYPE_CET_VIDAGE_INDEMNISATION_ANCIEN_REGIME) ||
				cviType().equals(TYPE_CET_VIDAGE_TRANSFERT_RAFP_ANCIEN_REGIME);
	}

	@Override
	public String toString() {
		String toString = "";

		String strTypeRegime = "régime pérènne";
		if (isVidageAncien()) {
			strTypeRegime = "ancien régime";
		}

		String strNatureVidage = "indemnisation";
		if (cviType().equals(TYPE_CET_VIDAGE_TRANSFERT_RAFP_REGIME_PERENNE) ||
				cviType().equals(TYPE_CET_VIDAGE_TRANSFERT_RAFP_ANCIEN_REGIME)) {
			strNatureVidage = "transfert RAFP";
		}

		toString = "Vidage manuel du CET \"" + strTypeRegime + "\" restant au " +
				DateCtrlHamac.dateToString(getOperationDelegate().dDeb()) + " en " + strNatureVidage;

		return toString;
	}
}
