/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanning.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOPlanning extends A_ToManyPersonneTypageComposante  {
	public static final String ENTITY_NAME = "Planning";
	public static final String ENTITY_TABLE_NAME = "HAMAC.PLANNING";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "plaId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NATURE_KEY = "nature";
	public static final String PERS_ID_KEY = "persId";
	public static final String TEM_PERSONNE_COMPOSANTE_KEY = "temPersonneComposante";

// Attributs non visibles
	public static final String PER_ID_KEY = "perId";
	public static final String PLA_ID_KEY = "plaId";
	public static final String STA_ID_KEY = "staId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NATURE_COLKEY = "NATURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TEM_PERSONNE_COMPOSANTE_COLKEY = "TEM_COMPOSANTE";

	public static final String PER_ID_COLKEY = "PER_ID";
	public static final String PLA_ID_COLKEY = "PLA_ID";
	public static final String STA_ID_COLKEY = "STA_ID";


	// Relationships
	public static final String TO_PERIODE_KEY = "toPeriode";
	public static final String TOS_ASSOCIATION_HORAIRE_KEY = "tosAssociationHoraire";
	public static final String TOS_CET_VIDAGE_KEY = "tosCetVidage";
	public static final String TOS_DELEGATION_HISTORIQUE_KEY = "tosDelegationHistorique";
	public static final String TOS_HORAIRE_KEY = "tosHoraire";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_OCCUPATION_PLANNING_CALCUL_KEY = "tosOccupationPlanningCalcul";
	public static final String TOS_SOLDE_KEY = "tosSolde";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";
	public static final String TOS_VISA_KEY = "tosVisa";
	public static final String TO_TYPE_STATUT_KEY = "toTypeStatut";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String nature() {
    return (String) storedValueForKey(NATURE_KEY);
  }

  public void setNature(String value) {
    takeStoredValueForKey(value, NATURE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String temPersonneComposante() {
    return (String) storedValueForKey(TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public void setTemPersonneComposante(String value) {
    takeStoredValueForKey(value, TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode)storedValueForKey(TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERIODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut toTypeStatut() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)storedValueForKey(TO_TYPE_STATUT_KEY);
  }

  public void setToTypeStatutRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut oldValue = toTypeStatut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_STATUT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire() {
    return (NSArray)storedValueForKey(TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier) {
    return tosAssociationHoraire(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier, boolean fetch) {
    return tosAssociationHoraire(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosAssociationHoraire();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public void removeFromTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire createTosAssociationHoraireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AssociationHoraire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_ASSOCIATION_HORAIRE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire) eo;
  }

  public void deleteTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public void deleteAllTosAssociationHoraireRelationships() {
    Enumeration objects = tosAssociationHoraire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosAssociationHoraireRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage> tosCetVidage() {
    return (NSArray)storedValueForKey(TOS_CET_VIDAGE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage> tosCetVidage(EOQualifier qualifier) {
    return tosCetVidage(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage> tosCetVidage(EOQualifier qualifier, boolean fetch) {
    return tosCetVidage(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage> tosCetVidage(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosCetVidage();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosCetVidageRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_CET_VIDAGE_KEY);
  }

  public void removeFromTosCetVidageRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_CET_VIDAGE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage createTosCetVidageRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CetVidage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_CET_VIDAGE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage) eo;
  }

  public void deleteTosCetVidageRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_CET_VIDAGE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosCetVidageRelationships() {
    Enumeration objects = tosCetVidage().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosCetVidageRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique() {
    return (NSArray)storedValueForKey(TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier) {
    return tosDelegationHistorique(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier, boolean fetch) {
    return tosDelegationHistorique(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosDelegationHistorique();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public void removeFromTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique createTosDelegationHistoriqueRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DelegationHistorique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_DELEGATION_HISTORIQUE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique) eo;
  }

  public void deleteTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosDelegationHistoriqueRelationships() {
    Enumeration objects = tosDelegationHistorique().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosDelegationHistoriqueRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire> tosHoraire() {
    return (NSArray)storedValueForKey(TOS_HORAIRE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire> tosHoraire(EOQualifier qualifier) {
    return tosHoraire(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire> tosHoraire(EOQualifier qualifier, boolean fetch) {
    return tosHoraire(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire> tosHoraire(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosHoraire();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_HORAIRE_KEY);
  }

  public void removeFromTosHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_HORAIRE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire createTosHoraireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Horaire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_HORAIRE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire) eo;
  }

  public void deleteTosHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_HORAIRE_KEY);
  }

  public void deleteAllTosHoraireRelationships() {
    Enumeration objects = tosHoraire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosHoraireRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul() {
    return (NSArray)storedValueForKey(TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier) {
    return tosOccupationPlanningCalcul(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier, boolean fetch) {
    return tosOccupationPlanningCalcul(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosOccupationPlanningCalcul();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public void removeFromTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul createTosOccupationPlanningCalculRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OccupationPlanningCalcul");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul) eo;
  }

  public void deleteTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public void deleteAllTosOccupationPlanningCalculRelationships() {
    Enumeration objects = tosOccupationPlanningCalcul().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosOccupationPlanningCalculRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSolde> tosSolde() {
    return (NSArray)storedValueForKey(TOS_SOLDE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSolde> tosSolde(EOQualifier qualifier) {
    return tosSolde(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSolde> tosSolde(EOQualifier qualifier, boolean fetch) {
    return tosSolde(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSolde> tosSolde(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOSolde.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosSolde();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_SOLDE_KEY);
  }

  public void removeFromTosSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOSolde createTosSoldeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Solde");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_SOLDE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSolde) eo;
  }

  public void deleteTosSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_KEY);
  }

  public void deleteAllTosSoldeRelationships() {
    Enumeration objects = tosSolde().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosSoldeRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOSolde)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa() {
    return (NSArray)storedValueForKey(TOS_VISA_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier) {
    return tosVisa(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier, boolean fetch) {
    return tosVisa(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa.TO_PLANNING_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOVisa.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosVisa();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
  }

  public void removeFromTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOVisa createTosVisaRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Visa");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_VISA_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOVisa) eo;
  }

  public void deleteTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
  }

  public void deleteAllTosVisaRelationships() {
    Enumeration objects = tosVisa().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVisaRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOVisa)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPlanning avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPlanning createEOPlanning(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String nature
, Integer persId
, String temPersonneComposante
, org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode			) {
    EOPlanning eo = (EOPlanning) createAndInsertInstance(editingContext, _EOPlanning.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNature(nature);
		eo.setPersId(persId);
		eo.setTemPersonneComposante(temPersonneComposante);
    eo.setToPeriodeRelationship(toPeriode);
    return eo;
  }

  
	  public EOPlanning localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlanning)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanning creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanning creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPlanning object = (EOPlanning)createAndInsertInstance(editingContext, _EOPlanning.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPlanning localInstanceIn(EOEditingContext editingContext, EOPlanning eo) {
    EOPlanning localInstance = (eo == null) ? null : (EOPlanning)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPlanning#localInstanceIn a la place.
   */
	public static EOPlanning localInstanceOf(EOEditingContext editingContext, EOPlanning eo) {
		return EOPlanning.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOPlanning> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPlanning fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPlanning fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanning eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanning)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanning fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanning fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanning eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanning)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPlanning fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanning eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanning ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanning fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
