package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import java.math.BigDecimal;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class CongeLegalDelegate
		extends /* A_OccupationTypeDelegate */CongeAnnuelDelegate {

	/**
	 * le premier palier pour le declenchement de reduction de la duree travaillee
	 */
	private final static int N_JOUR_PALIER_1_REGLE_90_180 = 90;
	/**
	 * le second palier pour le declenchement de reduction de la duree travaillee
	 */
	private final static int N_JOUR_PALIER_2_REGLE_90_180 = 180;

	public CongeLegalDelegate(A_Absence absence) {
		super(absence);
	}

	@Override
	public void validationSpecifique() throws ValidationException {
		// ...
	}

	/**
	 * A partir d'un certain nombre de conges legaux, le total travaille par
	 * l'agent sur l'annee est reduit.
	 * 
	 * - si total jours légaux > 90j : reduction de 10 jours a 7h00 (pour un 100%)
	 * 
	 * - si total jours légaux > 180j : reduction de 20 jours a 7h00 (pour un
	 * 100%)
	 * 
	 * @param planning
	 *          : le planning concerné
	 * @param cStructure
	 *          : le service concerné
	 * 
	 * @return le decompte en minutes.
	 */
	public final static int getMinutesDecompteLegal90180(
			Planning planning, String cStructure) {
		int minutes = 0;

		// date de fin d'application de cette règle
		NSTimestamp finApplication = HamacCktlConfig.dateForKey(EOParametre.DATE_FIN_APPLICATION_MALUS_90_180);

		// ne pas aller plus loin si celle ci est antérieure au planning
		if (DateCtrlHamac.isBefore(finApplication, planning.getEoPlanning().toPeriode().perDDebut())) {
			return minutes;
		}

		NSArray<Jour> jourArray = planning.getJourArray();

		// ne prendre que les jours concernés
		jourArray = EOQualifier.filteredArrayWithQualifier(
				jourArray, ERXQ.lessThanOrEqualTo(Jour.DATE_KEY, finApplication));

		int nbJourCongesLegaux = (((BigDecimal) jourArray.valueForKey("@sum." + Jour.NB_DEMI_JOURNEE_CONGE_LEGAL_KEY)).intValue()) / 2;

		if (nbJourCongesLegaux > N_JOUR_PALIER_1_REGLE_90_180) {
			int dureeJour = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, planning.getEoPlanning().toPeriode());
			minutes = 10 * dureeJour;
			if (nbJourCongesLegaux > N_JOUR_PALIER_2_REGLE_90_180) {
				minutes += 10 * dureeJour;
			}
		}

		// application de la quotité
		if (minutes > 0) {

			NSArray<TravailAttendu> attenduArray = planning.getTravailAttenduArray();

			// ne garder que pour la structure si précisée
			if (!StringCtrl.isEmpty(cStructure)) {
				attenduArray = planning.getTravailAttenduArray(cStructure);
			}

			double ratio = 0.0;

			// pour chaque "période" de planning
			for (int i = 0; i < attenduArray.count(); i++) {
				TravailAttendu attendu = attenduArray.objectAtIndex(i);

				// recadrer les dates de début et fin à la période
				EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

				NSTimestamp debut = attendu.getDateDebutSeuilleeDansPeriode(eoPeriode);
				NSTimestamp fin = attendu.getDateFinPlafonneeDansPeriode(eoPeriode);

				int nJours = DateCtrlHamac.getTotalJours360(debut, fin);

				ratio += (((double) nJours) / 360.0) * (attendu.getQuotite().doubleValue() / 100.0);
			}

			minutes = (int) ((double) minutes * ratio);

		}

		return minutes;
	}

	/**
	 * A partir d'un certain nombre d'heures de congé maladie, le total travaille
	 * par l'agent sur l'annee est reduit.
	 * 
	 * @param planning
	 *          : le planning concerné
	 * @param cStructure
	 *          : le service concerné
	 * 
	 * @return le decompte en minutes.
	 */
	public final static int getMinutesDecompteCongeMaladieReductionRTTSurBaseHeure(
			Planning planning, String cStructure) {
		int minutes = 0;

		// date de fin d'application de cette règle
		NSTimestamp debutApplication = HamacCktlConfig.dateForKey(EOParametre.DATE_DEBUT_APPLICATION_MALUS_RTT);

		// ne pas aller plus loin si celle ci est antérieure au planning
		if (DateCtrlHamac.isAfter(debutApplication, planning.getEoPlanning().toPeriode().perDFin())) {
			return minutes;
		}

		NSArray<Jour> jourArray = planning.getJourArray();

		// ne prendre que les jours concernés
		jourArray = EOQualifier.filteredArrayWithQualifier(
				jourArray, ERXQ.greaterThanOrEqualTo(Jour.DATE_KEY, debutApplication));

		int cumulMaladieMinutes = ((BigDecimal) jourArray.valueForKey("@sum." + Jour.DUREE_CONGE_MALADIE)).intValue();

		// nombre de jours
		// 365 jours – 52 samedis – 52 dimanches – 8 jours fériés (établis sur une
		// moyenne) – 25 congés payés = 228 jours
		int nbJourReference = planning.nbJourOuvre() - 25;

		// calcul du quotient de réduction
		float f_quotient = ((float) nbJourReference / (float) HamacCktlConfig.intForKey(EOParametre.NB_JOUR_MALUS_RTT_PAR_AN, planning.getEoPlanning().toPeriode()));

		// 11 à 11,49 = 11; 11,5 à 11,99 = 12
		int quotient = (int) f_quotient;
		if (f_quotient - (float) quotient >= (float) 0.5) {
			quotient += 1;
		}

		int dureeJour = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR_MALUS_RTT, planning.getEoPlanning().toPeriode());
		int seuilMinutes = quotient * dureeJour;

		// la réduction n'intervient que si on dépasse une première fois le quotient
		if (cumulMaladieMinutes >= seuilMinutes) {

			int jours = ((int) (((float) (cumulMaladieMinutes / dureeJour) / (float) quotient)));

			// System.out.println(
			// "nbJourReference=" + nbJourReference + " jours=" + jours +
			// " (quotient=" + quotient + ") " +
			// " (f_quotient=" + f_quotient + ") ");

			// application de la quotité
			minutes = jours * dureeJour;

			NSArray<TravailAttendu> attenduArray = planning.getTravailAttenduArray();

			// ne garder que pour la structure si précisée
			if (!StringCtrl.isEmpty(cStructure)) {
				attenduArray = planning.getTravailAttenduArray(cStructure);
			}

			double ratio = 0.0;

			// pour chaque "période" de planning
			for (int i = 0; i < attenduArray.count(); i++) {
				TravailAttendu attendu = attenduArray.objectAtIndex(i);

				// recadrer les dates de début et fin à la période
				EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

				NSTimestamp debut = attendu.getDateDebutSeuilleeDansPeriode(eoPeriode);
				NSTimestamp fin = attendu.getDateFinPlafonneeDansPeriode(eoPeriode);

				int nJours = DateCtrlHamac.getTotalJours360(debut, fin);

				ratio += (((double) nJours) / 360.0) * (attendu.getQuotite().doubleValue() / 100.0);
			}

			minutes = (int) ((double) minutes * ratio);

			// plafonner la valeur au nombre total de RTT
			int plafond = (int) ((double)
						(HamacCktlConfig.intForKey(EOParametre.NB_JOUR_MALUS_RTT_PAR_AN) *
								dureeJour) * ratio);
			if (minutes > plafond) {
				minutes = plafond;
			}

		}

		return minutes;
	}

	/**
	 * @deprecated
	 * @see #getMinutesDecompteCongeMaladieReductionRTTSurBaseHeure(Planning,
	 *      String)
	 * 
	 *      A partir d'un certain nombre de conges maladie, le total travaille par
	 *      l'agent sur l'annee est reduit.
	 * 
	 * @param planning
	 *          : le planning concerné
	 * @param cStructure
	 *          : le service concerné
	 * 
	 * @return le decompte en minutes.
	 */
	public final static int getMinutesDecompteCongeMaladieReductionRTTSurBaseDemiJournee(
			Planning planning, String cStructure) {

		int minutes = 0;

		// date de fin d'application de cette règle
		NSTimestamp debutApplication = HamacCktlConfig.dateForKey(EOParametre.DATE_DEBUT_APPLICATION_MALUS_RTT);

		// ne pas aller plus loin si celle ci est antérieure au planning
		if (DateCtrlHamac.isAfter(debutApplication, planning.getEoPlanning().toPeriode().perDFin())) {
			return minutes;
		}

		NSArray<Jour> jourArray = planning.getJourArray();

		// ne prendre que les jours concernés
		jourArray = EOQualifier.filteredArrayWithQualifier(
				jourArray, ERXQ.greaterThanOrEqualTo(Jour.DATE_KEY, debutApplication));

		int nbJourCongesMaladie = (((BigDecimal) jourArray.valueForKey("@sum." + Jour.NB_DEMI_JOURNEE_CONGE_MALADIE_KEY)).intValue()) / 2;

		// calcul du quotient de réduction
		float f_quotient = ((float) planning.nbJourOuvrable() / (float) HamacCktlConfig.intForKey(EOParametre.NB_JOUR_MALUS_RTT_PAR_AN, planning.getEoPlanning().toPeriode()));

		// 11 à 11,49 = 11; 11,5 à 11,99 = 12
		int quotient = (int) f_quotient;
		if (f_quotient - (float) quotient >= (float) 0.5) {
			quotient += 1;
		}

		// la réduction n'intervient que si on dépasse une première fois le quotient
		if (nbJourCongesMaladie >= quotient) {

			int jours = ((int) (((float) nbJourCongesMaladie / (float) quotient) + (float) 1));

			// System.out.println("jours=" + jours + " (quotient=" + quotient + ") " +
			// " (f_quotient=" + f_quotient + ") ");

			// application de la quotité
			if (jours > 0) {

				int dureeJour = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, planning.getEoPlanning().toPeriode());

				minutes = jours * dureeJour;

				NSArray<TravailAttendu> attenduArray = planning.getTravailAttenduArray();

				// ne garder que pour la structure si précisée
				if (!StringCtrl.isEmpty(cStructure)) {
					attenduArray = planning.getTravailAttenduArray(cStructure);
				}

				double ratio = 0.0;

				// pour chaque "période" de planning
				for (int i = 0; i < attenduArray.count(); i++) {
					TravailAttendu attendu = attenduArray.objectAtIndex(i);

					// recadrer les dates de début et fin à la période
					EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

					NSTimestamp debut = attendu.getDateDebutSeuilleeDansPeriode(eoPeriode);
					NSTimestamp fin = attendu.getDateFinPlafonneeDansPeriode(eoPeriode);

					int nJours = DateCtrlHamac.getTotalJours360(debut, fin);

					ratio += (((double) nJours) / 360.0) * (attendu.getQuotite().doubleValue() / 100.0);
				}

				minutes = (int) ((double) minutes * ratio);

				// plafonner la valeur au nombre total de RTT
				int plafond = (int) ((double)
						(HamacCktlConfig.intForKey(EOParametre.NB_JOUR_MALUS_RTT_PAR_AN) *
								dureeJour) * ratio);
				if (minutes > plafond) {
					minutes = plafond;
				}

			}

		}

		return minutes;

	}
}
