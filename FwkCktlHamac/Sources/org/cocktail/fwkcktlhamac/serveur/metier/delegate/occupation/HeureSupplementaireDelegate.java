/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class HeureSupplementaireDelegate
		extends A_OccupationTypePresenceDelegate {

	private final static String ERR_MOTIF_OBLIGATOIRE =
			"La saisie d'un motif est obligatoire pour une heure supplémentaire";
	private final static String ERR_DUREE_MINIMUM =
			"La durée minimum d'une heure supplémentaire est de ";
	private final static String ERR_NB_MAXIMUM = 
			"Vous avez déjà atteint le maximum d’heures supplémentaires pour le mois, merci de contacter votre responsable.";

	

	// TODO paramétrable seuils
	private final static int MINUTES_05H00 = 5 * 60;
	private final static int MINUTES_07H00 = 7 * 60;
	private final static int MINUTES_19H00 = 19 * 60;
	private final static int MINUTES_22H00 = 22 * 60;
	private final static int MINUTES_23H59 = 23 * 60 + 59;
	
	
	
	
	/**
	 * @param absence
	 */
	public HeureSupplementaireDelegate(A_Absence absence) {
		super(absence);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.
	 * A_OccupationTypeDelegate#validationMetier()
	 */
	@Override
	public void validationSpecifique() throws ValidationException {

		// occupation de type présence
		super.validationSpecifique();

		// le motif est obligatoire
		if (StringCtrl.isEmpty(getAbsence().motif())) {
			throw new ValidationException(ERR_MOTIF_OBLIGATOIRE);
		}
		
		// durée minimum
		int duree =  DateCtrlHamac.nbMinutesEntre(getAbsence().dateDebut(), getAbsence().dateFin());
		
		EOPeriode periodeAbsence = EOPeriode.getPeriode(getAbsence().editingContext(), getAbsence().dateDebut());
		
		if (duree < getHsupDureeMini(periodeAbsence)) {
			throw new ValidationException(ERR_DUREE_MINIMUM + TimeCtrl.stringForMinutes(getHsupDureeMini(periodeAbsence)) + " (" + TimeCtrl.stringForMinutes(duree) + ")");
		}

		// si valeur négative, alors on ne bloque pas la saisie des heures supplémentaires par mois
		if (getNbMinutesMax(periodeAbsence) >= 0) {
		
			// nb max 
			int nbMinutesSupp = getNombreMinutesSuppDansMois();
			
			if (nbMinutesSupp > getNbMinutesMax(periodeAbsence)) {
				throw new ValidationException(ERR_NB_MAXIMUM);
			}
		
		}
		
		// TODO controle de non chevauchement

	}

	/**
	 * 
	 * @return Le nombre de minutes totales suplémentaires dans le mois où la demande d'heures supp a été faite
	 */
	private int getNombreMinutesSuppDansMois() {
		
		NSTimestamp dateDebutMois = DateCtrlHamac.dateToDebutMois(getAbsence().dateDebut());
		NSTimestamp dateFinMois = DateCtrlHamac.dateToFinMois(getAbsence().dateDebut());
		
		NSArray<Jour> jours = jours(dateDebutMois, dateFinMois);
		
		int nbMinutesSuppDansMois = 0;
		
		// La demande d'heures supp en cours
		nbMinutesSuppDansMois += DateCtrlHamac.nbMinutesEntre(getAbsence().dateDebut(), getAbsence().dateFin());
		
		// les demandes d'heures supp déjà émises
		for (Jour jour : jours) {
			
			NSMutableArray<A_Absence> absenceArray = jour.getAbsenceArray();
			
			if (!NSArrayCtrl.isEmpty(absenceArray)) {
				
				for (A_Absence absence : absenceArray) {
					
					if (absence.isHeureSupplementaire()) {
						
						nbMinutesSuppDansMois += DateCtrlHamac.nbMinutesEntre(absence.dateDebut(), absence.dateFin());
						
					}
					
				}
				
			}
			
		}
		
		return nbMinutesSuppDansMois;
	}
	
	
	/**
	 * Pour tooltip
	 * 
	 * @param chaineTip
	 *          TODO
	 * @param minutesHorsHoraires
	 * @param mLocalDebut
	 * @param mLocalFin
	 * @param coef
	 * 
	 * @return
	 */
	private final static String getChaineTip(
			String chaineTip, int minutesHorsHoraires, int mLocalDebut, int mLocalFin, float coef) {
		String str = "";

		str += minutesHorsHoraires + " minutes dans la plage " + TimeCtrl.stringForMinutes(mLocalDebut) + "-" + TimeCtrl.stringForMinutes(mLocalFin);
		if (coef != (float) 1) {
			str += " bonifié à " + coef;
		}

		if (chaineTip.length() > 0) {
			str = ", " + str;
		}

		return str;
	}

	private String chaineTip;

	/**a
	 * TODO faire de la gestion multi-jour
	 * 
	 * Calculer la valeur comptabilisee d'une periode d'heures supplementaires. On
	 * avance progressivement dans le temps en appliquant les bonifications :
	 * 
	 * - 00h00 -> 04h59 : 1,5
	 * 
	 * - 05h00 -> 06h59 : 1,1
	 * 
	 * - 07h00 -> 19h00 : 1
	 * 
	 * - 19h01 -> 22h00 : 1,1
	 * 
	 * - 22h01 -> 23h59 : 1,5
	 * 
	 * @return
	 */
	public final int getDureeBonifiee(int mDebut, int mFin) {
		int minutes = 0;

		chaineTip = "";

		// TODO faire du multi jour
		Jour jour = null;
		NSArray<Jour> jourArray = jours(getAbsence().dateDebut(), getAbsence().dateDebut());
		EOQualifier qual = ERXQ.equals(
				Jour.DATE_KEY, TimeCtrl.remonterAMinuit(getAbsence().dateDebut()));
		jourArray = EOQualifier.filteredArrayWithQualifier(jourArray, qual);

		if (jourArray.count() > 0) {
			jour = jourArray.objectAtIndex(0);
		}

		// 6 plages
		// 1 : 00h00 -> 4h59
		// 2 : 05h00 -> 6h59
		// 3 : 07h00 -> 12h30
		// 4 : 12h31 -> 19h30
		// 5 : 19h01 -> 22h00
		// 6 : 22h01 -> 23h59

		// variables locales a chaque intervalle
		int mLocalDebut, mLocalFin = 0;

		// indique s'il faut continuer d'avancer dans la journee pour faire les
		// calculs
		boolean shouldContinue = true;

		// coefficient a appliquer en bonification
		float coef = (float) 0;

		// determiner si on est dans le cas 10 1/2 journée travaillées un samedi
		boolean isTravail10DemiJournees = false;
		if (jour.isSamedi()) {
			isTravail10DemiJournees = isTravail10DemiJourneesPourSamedi(jour);
		}

		// 1 plage 00h00 -> 4h59
		if (mDebut < MINUTES_05H00) {
			//
			mLocalDebut = mDebut;
			if (mFin <= MINUTES_05H00) {
				shouldContinue = false;
				mLocalFin = mFin;
			} else {
				mLocalFin = MINUTES_05H00;
			}
			// determiner le coefficient le plus avantageux entre
			// celui 22h00-05h00 et celui du samedi matin 10 1/2 journee et celui des
			// dimanche / JF
			coef = getCoefHsup22h00_05h00(jour);
			if (jour.isSamedi() && isTravail10DemiJournees && getCoefHsupSamMat5j(jour) > coef) {
				coef = getCoefHsupSamMat5j(jour);
			} else if (jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);

		}

		// plage 05h00 -> 6h59
		if (shouldContinue && mDebut < MINUTES_07H00) {
			//
			if (mDebut <= MINUTES_05H00) {
				mLocalDebut = MINUTES_05H00;
			} else {
				mLocalDebut = mDebut;
			}
			if (mFin <= MINUTES_07H00) {
				shouldContinue = false;
				mLocalFin = mFin;
			} else {
				mLocalFin = MINUTES_07H00;
			}
			// determiner le coefficient le plus avantageux entre
			// celui 05h00-07h00 et celui du samedi matin 10 1/2 journee et celui des
			// dimanche / JF
			coef = getCoefHsupDebordement(jour);
			if (jour.isSamedi() && isTravail10DemiJournees && getCoefHsupSamMat5j(jour) > coef) {
				coef = getCoefHsupSamMat5j(jour);
			} else if (jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}
			// //
			// minutes += (int) (((float) (jour.getMinutesHorsHoraires(mLocalDebut,
			// mLocalFin))) * coef);

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);
		}

		int minutes_12h30 = getMinutes12H30(jour);
		// plage 07h00 -> 12h30
		if (shouldContinue && mDebut <= minutes_12h30) {
			if (mDebut <= MINUTES_07H00) {
				mLocalDebut = MINUTES_07H00;
			} else {
				mLocalDebut = mDebut;
			}
			if (mFin <= minutes_12h30) {
				shouldContinue = false;
				mLocalFin = mFin;
			} else {
				mLocalFin = minutes_12h30;
			}
			// determiner le coefficient le plus avantageux entre
			// celui 07h00-19h00 et celui du samedi matin 10 1/2 journee et celui des
			// dimanche / JF
			coef = (float) 1;
			if (jour.isSamedi() && isTravail10DemiJournees && getCoefHsupSamMat5j(jour) > coef) {
				coef = getCoefHsupSamMat5j(jour);
			} else if (jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}
			// minutes += (int) ((float) (jour.getMinutesHorsHoraires(mLocalDebut,
			// mLocalFin)) * coef);

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);

			// // détail si chevauchement
			// if (DateCtrlHamac.dateToString(jour.getDate()).equals("28/05/2013")) {
			// if (jour.getPlageTravailAm() != null) {
			// for (PlageOccupation po :
			// jour.getPlageTravailAm().getPlageOccupationArray()) {
			// if
			// (DateCtrlHamac.dateToString(po.getJour().getDate()).equals("28/05/2013"))
			// {
			// System.out.println("am : " + po);
			// for (A_Segment segment : po.getSegmentArrayADebiter()) {
			// System.out.println("\tsegment : " + segment);
			// }
			// }
			// }
			// }
			// if (jour.getPlageTravailPm() != null) {
			// for (PlageOccupation po :
			// jour.getPlageTravailPm().getPlageOccupationArray()) {
			// if
			// (DateCtrlHamac.dateToString(po.getJour().getDate()).equals("28/05/2013"))
			// {
			// System.out.println("pm : " + po);
			// for (A_Segment segment : po.getSegmentArrayADebiter()) {
			// System.out.println("\tsegment : " + segment);
			// }
			// }
			// }
			// }
			// }
		}

		// plage 12h31 -> 19h30
		if (shouldContinue && mDebut <= MINUTES_19H00) {
			if (mDebut <= minutes_12h30) {
				mLocalDebut = minutes_12h30;
			} else {
				mLocalDebut = mDebut;
			}
			if (mFin <= MINUTES_19H00) {
				shouldContinue = false;
				mLocalFin = mFin;
			} else {
				mLocalFin = MINUTES_19H00;
			}
			// determiner le coefficient le plus avantageux entre
			// celui 07h00-19h00 et celui des dimanche / JF
			coef = (float) 1;
			if (jour.isSamedi() || jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}
			// minutes += (int) ((float) (jour.getMinutesHorsHoraires(mLocalDebut,
			// mLocalFin)) * coef);

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);
		}

		// plage 19h01 -> 22h00
		if (shouldContinue && mDebut <= MINUTES_22H00) {
			//
			if (mDebut <= MINUTES_19H00) {
				mLocalDebut = MINUTES_19H00;
			} else {
				mLocalDebut = mDebut;
			}
			if (mFin <= MINUTES_22H00) {
				shouldContinue = false;
				mLocalFin = mFin;
			} else {
				mLocalFin = MINUTES_22H00;
			}
			// determiner le coefficient le plus avantageux entre
			// celui 19h00-22h00 et celui des dimanche / JF
			coef = getCoefHsupDebordement(jour);
			if (jour.isSamedi() || jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}
			//
			//
			// minutes += (int) ((float) (jour.getMinutesHorsHoraires(mLocalDebut,
			// mLocalFin)) * coef);

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);
		}

		// plage 22h01 -> 23h59
		if (shouldContinue) {
			//
			if (mDebut <= MINUTES_22H00) {
				mLocalDebut = MINUTES_22H00;
			} else {
				mLocalDebut = mDebut;
			}
			// on rajoute le bout de 1 minutes si la saisie est 23h59
			if (mFin == MINUTES_23H59) {
				mLocalFin = MINUTES_23H59 + 1;
			} else {
				mLocalFin = mFin;
			}

			// determiner le coefficient le plus avantageux entre
			// celui 19h00-22h00 et celui des dimanche / JF
			coef = getCoefHsup22h00_05h00(jour);
			if (jour.isSamedi() || jour.isDimanche() || jour.isFerie()) {
				coef = getCoefHsupSamApremDimJf(jour);
			}
			// minutes += (int) ((float) (jour.getMinutesHorsHoraires(mLocalDebut,
			// mLocalFin)) * coef);

			//
			int minutesHorsHoraires = jour.getMinutesHorsHoraires(mLocalDebut, mLocalFin);
			minutes += (int) ((float) (minutesHorsHoraires) * coef);

			// tip
			chaineTip += getChaineTip(chaineTip, minutesHorsHoraires, mLocalDebut, mLocalFin, coef);
		}

		return minutes;
	}

	/**
	 * Indique si pour un samedi, toutes les journees de travail de la semaine
	 * (lundi au vendredi) sont travaillée sur leur 2 demi journées.
	 * 
	 * TODO faire le contrôle sur {@link Jour#isTravailleAM()} et
	 * {@link Jour#isTravaillePM()}
	 * 
	 * @return
	 */
	private final boolean isTravail10DemiJourneesPourSamedi(Jour jour) {
		boolean result = true;

		NSArray<Jour> jourArray = jours(DateCtrlHamac.reculerToLundi(jour.getDate()), jour.getDate());
		// EOQualifier qual = ERXQ.between(
		// Jour.DATE_KEY,
		// getAbsence().dateDebut().timestampByAddingGregorianUnits(0, 0, -5, 0, 0,
		// 0),
		// getAbsence().dateDebut(),
		// true);
		// jourArray = EOQualifier.filteredArrayWithQualifier(jourArray, qual);

		for (int i = 0; i < jourArray.count(); i++) {
			Jour unJour = jourArray.objectAtIndex(i);
			if (unJour.isLundi() || unJour.isMardi() || unJour.isMercredi() || unJour.isJeudi() || unJour.isVendredi()) {
				if (unJour.isChome() || unJour.isCongeAM() || unJour.isCongePM() || unJour.isFerie() || unJour.isCongeLegalAM() ||
						unJour.isCongeLegalPM() || unJour.isFerme()) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	private float getCoefHsupDebordement(Jour jour) {
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_DEBORDEMENT, jour.getPlanning().getEoPlanning().toPeriode());
	}
	
	private float getCoefHsupSamMat5j(Jour jour) {
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_SAM_MAT_5J, jour.getPlanning().getEoPlanning().toPeriode());
	}

	private float getCoefHsupSamApremDimJf(Jour jour) {
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_SAM_APREM_DIM_JF, jour.getPlanning().getEoPlanning().toPeriode());
	}
	
	private float getCoefHsup22h00_05h00(Jour jour){
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_22H00_05H00, jour.getPlanning().getEoPlanning().toPeriode());
	}
	
	private int getHsupDureeMini(EOPeriode periode) {
		return HamacCktlConfig.intForKey(EOParametre.HSUP_DUREE_MINI, periode);
	}
	
	private int getMinutes12H30(Jour jour) {
		return HamacCktlConfig.intForKey(EOParametre.HSUP_SEUIL_MATIN_APRES_MIDI, jour.getPlanning().getEoPlanning().toPeriode());
	}
	private int getHsupNbMax(EOPeriode periode) {
		return HamacCktlConfig.intForKey(EOParametre.HSUP_NB_MAX, periode);
	}
	
	private int getNbMinutesMax(EOPeriode periode){
		return  getHsupNbMax(periode) * 60;
	}
	
	public final String getChaineTip() {
		return chaineTip;
	}

}
