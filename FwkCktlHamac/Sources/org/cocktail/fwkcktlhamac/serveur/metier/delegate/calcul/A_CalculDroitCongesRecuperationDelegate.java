/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.Recuperation;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_CalculDroitCongesRecuperationDelegate
		extends A_PlanningCalculDroitCongesHeuresDuesDelegate {

	private NSMutableDictionary<String, NSArray<I_Solde>> _soldeRecuperationArrayDico;
	private NSMutableDictionary<String, Integer> _droitCongeDico;

	/**
	 * @param planning
	 */
	public A_CalculDroitCongesRecuperationDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);

		for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

			// pas de récupérations pour un droit à congé en dur
			if (isDroitCongeEnDur(eoStructure.cStructure()) == false) {
				constuireSoldes(eoStructure.cStructure());
			}

		}
	}

	/**
	 * La valeur hebdomadaire "seuil" à partir de laquelle le dépassement génère
	 * du solde de récupération
	 */
	private Integer minutesSeuilRecup100 = null;

	// XXX tmp pour test : saisie directe par l'interface

	private static Integer minutesSeuilRecup100EnDur = null;

	public int getSeuilRecup100() {
		if (minutesSeuilRecup100 == null) {
			if (minutesSeuilRecup100EnDur != null) {
				minutesSeuilRecup100 = minutesSeuilRecup100EnDur;
			} else {
				minutesSeuilRecup100 = HamacCktlConfig.intForKey(
						EOParametre.SEUIL_RECUPERATION, getPlanning().getEoPlanning().toPeriode());
			}
		}
		return minutesSeuilRecup100.intValue();
	}

	public void setSeuilRecup100(int value) {
		if (value != 0) {
			minutesSeuilRecup100EnDur = Integer.valueOf(value);
			minutesSeuilRecup100 = value;
		}
	}

	// XXX fin tmp test

	/**
	 * Constuire la liste des soldes de récupération d'après les horaires associés
	 * au planning et le seuil hebdomadaire.
	 */
	protected abstract void constuireSoldes(String cStructure);

	/**
	 * 
	 * @return
	 */
	protected final NSMutableDictionary<String, NSArray<I_Solde>> getSoldeRecuperationArrayDico() {
		if (_soldeRecuperationArrayDico == null) {
			_soldeRecuperationArrayDico = new NSMutableDictionary<String, NSArray<I_Solde>>();
		}
		return _soldeRecuperationArrayDico;
	}

	/**
	 * 
	 * @return
	 */
	protected final NSMutableDictionary<String, Integer> getDroitCongeDico() {
		if (_droitCongeDico == null) {
			_droitCongeDico = new NSMutableDictionary<String, Integer>();
		}
		return _droitCongeDico;
	}

	// GESTION ARTT

	/**
	 * La liste des soldes de récupération pour une structure (dans le cas d'une
	 * gestion ARTT)
	 */
	public final NSArray<I_Solde> getSoldeRecuperationArray(String cStructure) {
		NSArray<I_Solde> array = null;

		// la liste des récupération peut être à null dans l'hypothèse ou le DC
		// est en dur, on met donc un tableau avec 0 elements dans ce cas
		array = getSoldeRecuperationArrayDico().objectForKey(cStructure);
		if (array == null) {
			array = new NSArray<I_Solde>();
		}

		return array;

	}

	/**
	 * Tous les soldes de récupération
	 */
	public final NSArray<I_Solde> getSoldeRecuperationArray() {
		NSArray<I_Solde> array = new NSArray<I_Solde>();

		for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {
			NSArray<I_Solde> arrayStructure = getSoldeRecuperationArray(eoStructure.cStructure());
			if (!NSArrayCtrl.isEmpty(arrayStructure)) {
				array = array.arrayByAddingObjectsFromArray(arrayStructure);
			}
		}

		return array;
	}

	/**
	 * Uniquement les récupérations avec solde positif
	 * 
	 * @return
	 */
	public final NSArray<I_Solde> getSoldeRecuperationPositiveArray(String cStructure) {
		NSArray<I_Solde> array = getSoldeRecuperationArray(cStructure);

		array = EOQualifier.filteredArrayWithQualifier(array, Recuperation.QUAL_POSITIVE);

		return array;
	}

	/**
	 * Uniquement les récupérations avec solde positif
	 * 
	 * @return
	 */
	public final NSArray<I_Solde> getSoldeRecuperationPositiveArray() {
		NSArray<I_Solde> array = getSoldeRecuperationArray();

		array = EOQualifier.filteredArrayWithQualifier(array, Recuperation.QUAL_POSITIVE);

		return array;
	}

	/**
	 * Uniquement les récupérations avec solde negatif
	 * 
	 * @Deprecated
	 * @return
	 */
	@Deprecated
	public final NSArray<I_Solde> getSoldeRecuperationNegativeArray(String cStructure) {
		NSArray<I_Solde> array = getSoldeRecuperationArray(cStructure);

		array = EOQualifier.filteredArrayWithQualifier(array, Recuperation.QUAL_NEGATIVE);

		return array;
	}

	/**
	 * Uniquement les récupérations avec solde negatif
	 * 
	 * @return
	 */
	@Deprecated
	public final NSArray<I_Solde> getSoldeRecuperationNegativeArray() {
		NSArray<I_Solde> array = getSoldeRecuperationArray();

		array = EOQualifier.filteredArrayWithQualifier(array, Recuperation.QUAL_NEGATIVE);

		return array;
	}

	@Override
	public int getDroitCongeDepuisCalculMinutes(String cStructure) {
		int congeMinutes = 0;

		if (!StringCtrl.isEmpty(cStructure)) {

			congeMinutes = getDroitCongeDico().objectForKey(cStructure).intValue();

		} else {

			for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {
				congeMinutes += getDroitCongeDepuisCalculMinutes(eoStructure.cStructure());
			}

		}

		return congeMinutes;
	}

	@Override
	public int getRecuperationMinutes(String cStructure) {
		int minutes = 0;

		if (!StringCtrl.isEmpty(cStructure)) {

			for (I_Solde recuperation : getSoldeRecuperationArray(cStructure)) {
				minutes += ((Recuperation) recuperation).getValeur().intValue();
				//
				// CktlLog.log(((Recuperation) recuperation).libelle() + " " +
				// TimeCtrl.stringForMinutes(minutes));
			}

		} else {

			for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {
				minutes += getRecuperationMinutes(eoStructure.cStructure());
			}

		}

		return minutes;
	}

	@Override
	public int getRecuperationRestantMinutes(String cStructure) {
		int minutes = 0;

		if (!StringCtrl.isEmpty(cStructure)) {

			for (I_Solde recuperation : getSoldeRecuperationArray(cStructure)) {
				minutes += recuperation.getSoldeDelegate().restant().intValue();
			}

		} else {

			for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {
				minutes += getRecuperationRestantMinutes(eoStructure.cStructure());
			}

		}

		return minutes;
	}

}
