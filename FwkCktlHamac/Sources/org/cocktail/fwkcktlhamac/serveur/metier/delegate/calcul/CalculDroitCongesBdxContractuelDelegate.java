package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Mois;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CalculDroitCongesBdxContractuelDelegate extends
		CalculDroitCongesBdxDemiJourneeDelegate {

	public CalculDroitCongesBdxContractuelDelegate(Planning planning,
			NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
	}
	
	private int getDureeJournee() {
		return HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR_CONTRACTUEL, getPlanning().getEoPlanning().toPeriode());
	}
	
	private float getNbJourCongeMensuel() {
		return HamacCktlConfig.floatForKey(EOParametre.NB_JOUR_CONGE_MENSUEL_CONTRACTUEL, getPlanning().getEoPlanning().toPeriode());
	}

	@Override
	public String affichageFenetreDetail() {
		return "Heures associées - heures dues + ATT pour les contractuels : " + getNbJourCongeMensuel() + "j./mois sur base " + TimeCtrl.stringForMinutes(getDureeJournee()) + "/j.";
	}
		
	@Override
	public boolean isCalculContractuel() {
		return true;
	}

	@Override
	public int getDuMinutes(String cStructure) {
		int duMinutes = 0;

		int dureeJournee = getDureeJournee();
		float nbJourCongeMensuel = getNbJourCongeMensuel();
		
		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			NSArray<EOStructure> eoStuctureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStuctureArray) {
				duMinutes += getDuMinutes(eoStructure.cStructure());
			}

		} else {

			float f_minutes = (float) 0;

			NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray(cStructure);
			
			for (int i = 0; i < attenduArray.count(); i++) {
				TravailAttendu attendu = attenduArray.objectAtIndex(i);

				
				NSArray<Mois> moisAttenduArray = attendu.getMois(getPlanning());

				for (Mois mois : moisAttenduArray) {
				
					int nbJourTravailPourMois = getNbJourDansMois(attendu, getPlanning(), mois, cStructure, getDateDebutApplication(), getDateFinApplication());
				
					float quotite = attendu.getQuotite().floatValue() / (float) 100;				
					
					NSTimestamp dateDebutMoisTravail = mois.getPremierJour();
					NSTimestamp dateFinMoisTravail = mois.getDernierJour();
					
					if (DateCtrlHamac.isAfter(attendu.getDateDebut(), mois.getPremierJour())) {
						dateDebutMoisTravail = attendu.getDateDebut();
					}
					if (attendu.getDateFin() != null && DateCtrlHamac.isBefore(attendu.getDateFin(), mois.getDernierJour())) {
						dateFinMoisTravail = attendu.getDateFin();
					}
					
					int nbJourOuvreTravail = mois.nbJourOuvre(dateDebutMoisTravail, dateFinMoisTravail);
					
					if (nbJourTravailPourMois >= DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) {
						// pour un mois complet
						float minutesDues100 = dureeJournee * quotite * nbJourOuvreTravail;
						minutesDues100 -= (float) nbJourCongeMensuel * dureeJournee * quotite;
						f_minutes += minutesDues100;
					} else {
						
						if (nbJourTravailPourMois > 0) {
						
							// mois incomplet, regle du trentieme
							int nbJourAttendu = mois.nbJourAttendu(attendu);
							float ratio = (float) nbJourAttendu / (float) DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30;
							float minutesDues100 = dureeJournee * quotite * nbJourOuvreTravail;
							
							float nbJourCongeMensuelAuTrentieme = (float) nbJourCongeMensuel * ratio;
							
							minutesDues100 -= (float) nbJourCongeMensuelAuTrentieme * dureeJournee * quotite;
							f_minutes += minutesDues100;
							
						}
					}
					
					
					
				}
				
			}

			duMinutes = (int) f_minutes;

		}

		return duMinutes;
	}

}
