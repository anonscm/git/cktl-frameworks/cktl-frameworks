/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère les modes de calculs multiples pour 1 planning (droit contractuel puis
 * titulaire)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CalculDroitCongeDelegate
		extends A_PlanningCalculDroitCongeDelegate {

	private NSMutableArray<A_PlanningCalculDroitCongeDelegate> calculDelegateArray;

	/**
	 * 
	 */
	public CalculDroitCongeDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
		calculDelegateArray = new NSMutableArray<A_PlanningCalculDroitCongeDelegate>();
	}

	@Override
	public String affichageFenetreDetail() {
		// jamais appelé
		return null;
	}

	public final NSMutableArray<A_PlanningCalculDroitCongeDelegate> getCalculDelegateArray() {
		return calculDelegateArray;
	}

	public final void addCalculDelegateArray(A_PlanningCalculDroitCongeDelegate delegate) {
		getCalculDelegateArray().addObject(delegate);
	}

	@Override
	public int getDroitCongeDepuisCalculMinutes(String cStructure) {
		int minutes = 0;
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			minutes += delegate.getDroitCongeDepuisCalculMinutes(cStructure);
		}
		return minutes;
	}

	@Override
	public int getDuMinutes(String cStructure) {
		int minutes = 0;
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			minutes += delegate.getDuMinutes(cStructure);
		}
		return minutes;
	}

	@Override
	public int getDuMinutes(String cStructure, NSTimestamp dateButoirInclue) {
		int minutes = 0;
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			minutes += delegate.getDuMinutes(cStructure, dateButoirInclue);
		}
		return minutes;
	}

	@Override
	public int getRecuperationMinutes(String cStructure) {
		int minutes = 0;
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			minutes += delegate.getRecuperationMinutes(cStructure);
		}
		return minutes;
	}

	@Override
	public int getRecuperationRestantMinutes(String cStructure) {
		int minutes = 0;
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			minutes += delegate.getRecuperationRestantMinutes(cStructure);
		}
		return minutes;
	}

	/**
	 * La liste des récupérations positives si l'un des mode de calcul contient
	 * des récupérations
	 * 
	 * @return
	 */
	public final NSArray<I_Solde> getSoldeRecuperationPositiveArray() {
		return getSoldeRecuperationPositiveArray(null);
	}

	/**
	 * La liste des récupérations positives si l'un des mode de calcul contient
	 * des récupérations
	 * 
	 * @return
	 */
	public final NSArray<I_Solde> getSoldeRecuperationPositiveArray(String cStructure) {
		NSArray<I_Solde> array = new NSArray<I_Solde>();
		for (A_PlanningCalculDroitCongeDelegate delegate : getCalculDelegateArray()) {
			if (delegate instanceof A_CalculDroitCongesRecuperationDelegate) {

				if (!StringCtrl.isEmpty(cStructure)) {

					array = array.arrayByAddingObjectsFromArray(
							((A_CalculDroitCongesRecuperationDelegate) delegate).getSoldeRecuperationPositiveArray(cStructure));

				} else {

					NSArray<EOStructure> eoStructureArray = getPlanning().getEoStructureAttenduArray();

					for (EOStructure eoStructure : eoStructureArray) {

						array = array.arrayByAddingObjectsFromArray(
								((A_CalculDroitCongesRecuperationDelegate) delegate).getSoldeRecuperationPositiveArray(eoStructure.cStructure()));

					}

				}

			}
		}
		return array;
	}

	@Override
	public boolean isCalculContractuel() {
		// ne doit jamais être appelée directement !
		throw new IllegalAccessError(
				"Cette méthode ne peut pas être appelée directement (" + getClass().getName() + ".isCalculContractuel())!");
	}
}
