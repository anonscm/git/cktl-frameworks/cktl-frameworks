/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Classe de gestion des périodes d'astreinte
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class AstreinteDelegate
		extends A_OccupationTypePresenceDelegate {

	/**
	 * @param absence
	 */
	public AstreinteDelegate(A_Absence absence) {
		super(absence);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.
	 * A_OccupationTypeDelegate #validationSpecifique()
	 */
	@Override
	public void validationSpecifique() throws ValidationException {
		// occupation de type présence
		super.validationSpecifique();

		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.
	 * A_OccupationTypePresenceDelegate#getDureeBonifiee(int, int)
	 */
	@Override
	public int getDureeBonifiee(int mDebut, int mFin) {
		// TODO Auto-generated method stub
		return 0;
	}

}
