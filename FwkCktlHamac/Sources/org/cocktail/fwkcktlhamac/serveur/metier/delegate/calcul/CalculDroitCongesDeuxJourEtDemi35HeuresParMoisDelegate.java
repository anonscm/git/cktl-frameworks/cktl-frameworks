/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Mois;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Calcul du droit à congé selon la règle 2,5 jours (17h30) de congé pour un
 * mois de travail mais pour un rythme de travail hebdomadaire de 35 Heures
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CalculDroitCongesDeuxJourEtDemi35HeuresParMoisDelegate
		extends CalculDroitCongesHeuresAssocieesHeuresDuesDelegate {

	private int getDureeJournee() {
		return HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR_CONTRACTUEL, getPlanning().getEoPlanning().toPeriode());
	}
	
	private float getNbJourCongeMensuel() {
		return HamacCktlConfig.floatForKey(EOParametre.NB_JOUR_CONGE_MENSUEL_CONTRACTUEL, getPlanning().getEoPlanning().toPeriode());
	}
	// cache
	private NSMutableDictionary<String, Integer> _duMinutesDico = new NSMutableDictionary<String, Integer>();

	//

	/**
	 * @param planning
	 */
	public CalculDroitCongesDeuxJourEtDemi35HeuresParMoisDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
	}

	@Override
	public String affichageFenetreDetail() {
		return "Calcul contractuel : " + getNbJourCongeMensuel() + "j./mois sur base " + TimeCtrl.stringForMinutes(getDureeJournee()) + "/j.";
	}

	
	@Override
	public int getDroitCongeDepuisCalculMinutes(String cStructure) {
		int droitCongeMinutes = 0;
		
		int dureeJournee = getDureeJournee();
		float nbJourCongeMensuel = getNbJourCongeMensuel();
		
		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			NSArray<EOStructure> eoStuctureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStuctureArray) {
				droitCongeMinutes += getDuMinutes(eoStructure.cStructure());
			}

		} else {

			
			float droitCongeJours = 0;
			
			NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray(cStructure);

			for (TravailAttendu travailAttendu : attenduArray) {

				NSArray<Mois> moisAttenduArray = travailAttendu.getMois(getPlanning());

				for (Mois mois : moisAttenduArray) {

					if (DateCtrlHamac.isBefore(getDateFinApplication(), mois.getPremierJour()) 	|| DateCtrlHamac.isAfter(getDateDebutApplication(), mois.getDernierJour())) {
						continue;
					}
					
					
					int nbJourTravailPourMois = getNbJourDansMois(travailAttendu, getPlanning(), mois, cStructure, getDateDebutApplication(), getDateFinApplication());

					if (nbJourTravailPourMois > 0) {

						
						NSTimestamp premierJourAComptabiliser = mois.getPremierJour();
						
						if (DateCtrlHamac.isAfter(getDateDebutApplication(), premierJourAComptabiliser)) {
							premierJourAComptabiliser = getDateDebutApplication();
						}
						
						NSTimestamp dernierJourAComptabiliser = mois.getDernierJour();
						
						if (DateCtrlHamac.isBefore(getDateFinApplication(), dernierJourAComptabiliser)) {
							dernierJourAComptabiliser = getDateFinApplication();
						}
						
						int minutesComptabilisees = getPlanning().dureeComptabiliseeMinutes(cStructure, premierJourAComptabiliser, dernierJourAComptabiliser);
						
						float quotite = travailAttendu.getQuotite().floatValue() / (float) 100;

						
						int nbJourOuvre = mois.nbJourOuvre(mois.getPremierJour(), mois.getDernierJour());						
						
						float nombreMinutesDuesDansMois = (nbJourOuvre * (float) dureeJournee * quotite);
						
						if (nbJourTravailPourMois >= DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) {
							// pour un mois complet
							droitCongeJours += ((float) (minutesComptabilisees / nombreMinutesDuesDansMois) * (float) nbJourCongeMensuel);
							
						} else {
							
							// mois incomplet, regle du trentieme
							int nbJourAttendu = mois.nbJourAttendu(travailAttendu);
							droitCongeJours += ((float) ((float) nbJourAttendu / DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) * (float) nbJourCongeMensuel * quotite);
							
						}
						
					}

				}

			}

			droitCongeMinutes = (int) (droitCongeJours * (float) dureeJournee);
			
		}
		

		return droitCongeMinutes;
	}
	
	
	/**
	 * Les heures sont calculées par rapport aux jours ouvrés des mois, selon la
	 * formule : du = (nb_ouvrés - nbJourCongeMensuel) x quotité
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.
	 *      A_PlanningCalculDroitCongeDelegate#getDuMinutes(java.lang.String)
	 */
	@Override
	public int getDuMinutes(String cStructure) {

		int i_minutes = 0;

		int dureeJournee = getDureeJournee();
		float nbJourCongeMensuel = getNbJourCongeMensuel();
		
		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			NSArray<EOStructure> eoStuctureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStuctureArray) {
				i_minutes += getDuMinutes(eoStructure.cStructure());
			}

		} else {

			Integer minutesDues = _duMinutesDico.objectForKey(cStructure);

			if (minutesDues != null) {

				i_minutes = minutesDues.intValue();

			} else {

				float jourDu = (float) 0;

				NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray(cStructure);

				for (TravailAttendu travailAttendu : attenduArray) {

					NSArray<Mois> moisAttenduArray = travailAttendu.getMois(getPlanning());

					for (Mois mois : moisAttenduArray) {

						if (DateCtrlHamac.isBefore(getDateFinApplication(), mois.getPremierJour()) 	|| DateCtrlHamac.isAfter(getDateDebutApplication(), mois.getDernierJour())) {
							continue;
						}
						
						int nbJourTravailPourMois = getNbJourDansMois(travailAttendu, getPlanning(), mois, cStructure, getDateDebutApplication(), getDateFinApplication());

						if (nbJourTravailPourMois > 0) {

							float quotite = travailAttendu.getQuotite().floatValue() / (float) 100;

							// pour un mois complet
							int nbJourOuvre = mois.nbJourOuvre(getDateDebutApplication(), getDateFinApplication());
							float duMoisComplet = (float) nbJourOuvre - nbJourCongeMensuel;

							if (nbJourTravailPourMois >= DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) {

								// mois complet
								duMoisComplet = duMoisComplet * quotite;

							} else {

								// mois incomplet, regle du trentieme
								int nbJourAttendu = mois.nbJourAttendu(travailAttendu);
								float ratio = (float) nbJourAttendu / (float) DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30;
								float nbJourCongeMensuelAuTrentieme = (float) nbJourCongeMensuel * ratio;
								float duMoisIncomplet = nbJourOuvre - nbJourCongeMensuelAuTrentieme;
								duMoisComplet = duMoisIncomplet * quotite;

							}

							jourDu += duMoisComplet;

							mois.setMinutesContractuelDue(cStructure, (int) (duMoisComplet * (float) dureeJournee));

							// en trentieme
							if (nbJourTravailPourMois >= DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) {
								mois.setTrentiemeContractuel(cStructure, Integer.valueOf(DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30));
							} else {
								// cumul si besoin
								int nPrev = mois.getTrentiemeContractuel(cStructure);
								if (nPrev > 0) {
									nbJourTravailPourMois += nPrev;
								}
								mois.setTrentiemeContractuel(cStructure, Integer.valueOf(nbJourTravailPourMois));
							}

						}

					}

				}

				i_minutes = (int) (jourDu * (float) dureeJournee);
				_duMinutesDico.setObjectForKey(Integer.valueOf(i_minutes), cStructure);
			}

		}

		return i_minutes;
	}


	@Override
	public boolean isCalculContractuel() {
		return true;
	}
}
