package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlanningFactory;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXQ;

/**
 * Profil des classes de gestion des occupations selon leur type. La classe
 * delegate est définie dans la table {@link EOOccupationType#ENTITY_TABLE_NAME}
 * , attribut {@link EOOccupationType#otypClasseDelegate()}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_OccupationTypeDelegate {

	private A_Absence absence;

	protected final static NSArray<I_Solde> SOLDE_EMPTY_ARRAY = new NSArray<I_Solde>();

	private final static String ERR_VALIDATION_DEBUT_APRES_FIN =
			"La date de début doit être antérieure à la date de fin.";
	private final static String NB_JOUR_KEY = "nbJour";
	private final static String NB_JOUR_MAX_KEY = "nbJourMax";
	private final static String NB_KEY = "nb";
	private final static String NB_MAX_KEY = "nbMax";
	private final static String CONTRACTUEL_VALUE = "contractuel";
	private final static String TITULAIRE_VALUE = "titulaire";
	private final static String STATUT_KEY = "statut";
	private final static String SUFFIX_ERR_VALIDATION_DEPASSEMENT = ". Votre saisie actuelle couvre %" + NB_JOUR_KEY + "% jours";
	private final static String SUFFIX_ERR_VALIDATION_DEPASSEMENT_NOMBRE = ". Avec celui là, vous en serez à un nombre de %" + NB_KEY + "%";
	private final static String ERR_VALIDATION_JOUR_MAX_DEPASSEMENT =
			"La durée d'un congé de ce type ne peut-être supérieure à %" + NB_JOUR_MAX_KEY + "% jours" + SUFFIX_ERR_VALIDATION_DEPASSEMENT;
	private final static String ERR_VALIDATION_JOUR_OUVRABLE_MAX_DEPASSEMENT =
			"La durée d'un congé de ce type ne peut-être supérieure à %" + NB_JOUR_MAX_KEY + "% jours ouvrables" + SUFFIX_ERR_VALIDATION_DEPASSEMENT;
	private final static String ERR_VALIDATION_NOMBRE_POUR_CONTRACTUEL_DEPASSEMENT =
			"Le nombre de congés de ce type ne peut excéder %" + NB_MAX_KEY + "% par période (%" + STATUT_KEY + "%)" + SUFFIX_ERR_VALIDATION_DEPASSEMENT_NOMBRE;
	
	private final static String ERR_VALIDATION_NOMBRE_ANNEE_CIVILE_DEPASSEMENT =
			"Le nombre de congés de ce type ne peut excéder %" + NB_MAX_KEY + "% par année civile " + SUFFIX_ERR_VALIDATION_DEPASSEMENT_NOMBRE;


	public A_OccupationTypeDelegate(A_Absence absence) {
		super();
		this.absence = absence;
	}

	/**
	 * Effectue la validation commune a toutes les occupations :
	 * 
	 * - champs obligatoires saisis
	 * 
	 * - cohérence de date de début et de fin - ...
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void validationCommune() throws NSValidation.ValidationException {

		// verifier que la date de fin est apres la date de debut
		if (DateCtrl.isAfter(getAbsence().dateDebut(), getAbsence().dateFin())) {
			throw new ValidationException(ERR_VALIDATION_DEBUT_APRES_FIN);
		}

		// vérifier que le planning est ouvert pour ces dates (pas de controle pour
		// les fermetures)
		if (!getAbsence().isFermeture()) {
			if (!isAbsenceInclueDansPlanning()) {
				throw new ValidationException("L'occupation ne couvre aucun jour ouvert");
			}
		}

		// la structure d'application n'est autorisée que pour les occupations de
		// type présence
		if (getAbsence().isPresence() == false &&
				getAbsence().toStructureApplication() != null) {
			throw new NSValidation.ValidationException(
					"Une occupation de type absence ne peut pas avoir un service d'application (" +
							getAbsence().toStructureApplication().lcStructure() + ")");
		}

		// absence à la minute
		if (getAbsence().isPresence() == true) {

			// la structure d'application est obligatoire pour les occupations de type
			// présence
			if (getAbsence().toStructureApplication() == null) {
				throw new NSValidation.ValidationException(
						"Une occupation de type présence doit avoir un service d'application");
			}

			// la date de debut et la date de fin doivent etre le meme jour
			if (DateCtrlHamac.isAfterEq(getAbsence().dateDebut(), getAbsence().dateFin()) ||
					DateCtrlHamac.isBeforeEq(getAbsence().dateFin(), getAbsence().dateDebut())) {
				throw new NSValidation.ValidationException(
						"Le début et la fin n'appartiennent pas à la même journée");
			}

			// l'heure de début et de fin doivent être différentes
			GregorianCalendar gcDebut = new GregorianCalendar();
			gcDebut.setTime(getAbsence().dateDebut());
			GregorianCalendar gcFin = new GregorianCalendar();
			gcFin.setTime(getAbsence().dateFin());

			if (gcDebut.get(GregorianCalendar.HOUR_OF_DAY) == gcFin.get(GregorianCalendar.HOUR_OF_DAY) &&
					gcDebut.get(GregorianCalendar.MINUTE) == gcFin.get(GregorianCalendar.MINUTE)) {
				throw new NSValidation.ValidationException(
						"L'heure de début et de fin sont identiques");
			}

		}

		// non chevauchement avec d'autres occupations
		NSArray<Jour> jourArray = jours(getAbsence().dateDebut(), getAbsence().dateFin());
		for (Jour jour : jourArray) {

			if (jour.isDisponible(getAbsence()) == false) {
				throw new NSValidation.ValidationException(
						"La journée du " + DateCtrlHamac.dateToString(jour.getDate()) + " n'est pas disponible");
			}

		}

		// controles liés au type de l'occupation
		if (getEoOccupation() != null) {

			EOTypeOccupation eoTypeOccupation = getEoOccupation().toTypeOccupation();

			// nombre de jours max
			if (eoTypeOccupation.nbJourMax() != null) {

				float nbJour = (float) DateCtrlHamac.getNbDemiJourneeEntre(getAbsence().dateDebut(), getAbsence().dateFin()) / (float) 2;
				float max = eoTypeOccupation.nbJourMax().floatValue();

				if (nbJour > max) {
					Hashtable<String, String> dico = new Hashtable<String, String>();
					dico.put(NB_JOUR_MAX_KEY, Float.toString(max));
					dico.put(NB_JOUR_KEY, Float.toString(nbJour));
					throw new NSValidation.ValidationException(
							StringCtrl.replaceWithDico(ERR_VALIDATION_JOUR_MAX_DEPASSEMENT, dico));
				}
			}

			// nombre de jours ouvrable max
			if (eoTypeOccupation.nbJourOuvrableMax() != null) {

				int nbJourOuvrable = ((Number) jourArray.valueForKey("@sum." + Jour.NB_JOUR_OUVRABLE_KEY)).intValue();
				int max = eoTypeOccupation.nbJourOuvrableMax().intValue();

				if (nbJourOuvrable > max) {
					Hashtable<String, String> dico = new Hashtable<String, String>();
					dico.put(NB_JOUR_MAX_KEY, Integer.toString(max));
					dico.put(NB_JOUR_KEY, Integer.toString(nbJourOuvrable));
					throw new NSValidation.ValidationException(
							StringCtrl.replaceWithDico(ERR_VALIDATION_JOUR_OUVRABLE_MAX_DEPASSEMENT, dico));
				}
			}

			// nombre maximum autorisé
			if ((eoTypeOccupation.nbMaxParPeriodeContractuel() != null || eoTypeOccupation.nbMaxParPeriodeTitulaire() != null) && getEoOccupation() != null) {

				EOEditingContext ec = eoTypeOccupation.editingContext();
				EOIndividu eoIndividu = EOIndividu.individuWithPersId(ec, getAbsence().persId());
				if (eoIndividu != null) {

					
					boolean isContractuel = GrhumFactory.isContractuel(ec, eoIndividu, getAbsence().dateDebut(), getAbsence().dateFin());

					// nombre d'occupation
					NSMutableArray<A_Absence> absenceArray = new NSMutableArray<A_Absence>();
					for (EOPlanning eoPlanning : getAbsence().getEoPlanningArrayPourOccupation()) {
						// absenceArray.addObjectsFromArray(eoPlanning.getOccupationArray(new
						// NSArray<TravailAttendu>()));
						absenceArray.addObjectsFromArray(PlanningFactory.getAbsenceArraySortedChargementSansLegalArray(eoPlanning.getPlanning()));
					}

					// de ce type
					EOQualifier qual = ERXQ.equals(
							EOOccupation.TO_TYPE_OCCUPATION_KEY, eoTypeOccupation);
					// oter celle qui sont refusées / supprimées ...
					qual = ERXQ.and(ERXQ.isTrue(A_Absence.IS_VISIBLE_KEY), qual);
					absenceArray = new NSMutableArray<A_Absence>(EOQualifier.filteredArrayWithQualifier(
							absenceArray, qual));

					// contient celle en cours de saisie
					int count = absenceArray.count();

					int max = 0;
					String statut = "";
					if (isContractuel) {
						statut = CONTRACTUEL_VALUE;
						max = eoTypeOccupation.nbMaxParPeriodeContractuel().intValue();
					} else {
						statut = TITULAIRE_VALUE;
						max = eoTypeOccupation.nbMaxParPeriodeTitulaire().intValue();
					}

					// nombre max d'occupation de ce type pour les contractuels
					if (count > max) {
						Hashtable<String, String> dico = new Hashtable<String, String>();
						dico.put(NB_MAX_KEY, Integer.toString(max));
						dico.put(NB_KEY, Integer.toString(count));
						dico.put(STATUT_KEY, statut);
						throw new NSValidation.ValidationException(
									StringCtrl.replaceWithDico(ERR_VALIDATION_NOMBRE_POUR_CONTRACTUEL_DEPASSEMENT, dico));
					}

				}

			}
			
			if (eoTypeOccupation.nbMaxParAnneeCivile() != null) {
				
				NSTimestamp dDebut = DateCtrlHamac.dateToDebutAnneeCivile(getAbsence().dateDebut());
				NSTimestamp dFin = DateCtrlHamac.dateToFinAnneeCivile(getAbsence().dateDebut());
				
				// nombre d'occupation
				NSMutableArray<A_Absence> absenceArray = new NSMutableArray<A_Absence>();
				NSArray<EOPlanning> listePlanning = getAbsence().getEoPlanningArrayPourOccupationAnneeCivile(dDebut, dFin);
				for (EOPlanning eoPlanning : listePlanning) {
					NSArray<A_Absence> arrayAbsence = PlanningFactory.getAbsenceArraySortedChargementSansLegalArray(eoPlanning.getPlanning());
					absenceArray.addAll(EOQualifier.filteredArrayWithQualifier(arrayAbsence, A_Absence.getQualifierAbsencesIncluesDansPeriode(dDebut, dFin))); 
				}
				
				// de ce type
				EOQualifier qual = ERXQ.equals(
						EOOccupation.TO_TYPE_OCCUPATION_KEY, eoTypeOccupation);
				// oter celle qui sont refusées / supprimées ...
				qual = ERXQ.and(ERXQ.isTrue(A_Absence.IS_VISIBLE_KEY), qual);
				absenceArray = new NSMutableArray<A_Absence>(EOQualifier.filteredArrayWithQualifier(
						absenceArray, qual));
				
				int nbTotalJourSurAnneeCivile = 0;
				
				for (A_Absence absence : absenceArray) {
					Float nbJour = absence.getNbJourneeSurHoraire();
					nbTotalJourSurAnneeCivile += nbJour;
				}
				
				nbTotalJourSurAnneeCivile += ((Number) jourArray.valueForKey("@sum." + Jour.NB_JOUR_OUVRABLE_KEY)).intValue();
				

				int max = eoTypeOccupation.nbMaxParAnneeCivile();
				
				// nombre max d'occupation de ce type par année civile
				if (nbTotalJourSurAnneeCivile > max) {
					Hashtable<String, String> dico = new Hashtable<String, String>();
					dico.put(NB_MAX_KEY, Integer.toString(max));
					dico.put(NB_KEY, Integer.toString(nbTotalJourSurAnneeCivile));
					throw new NSValidation.ValidationException(
								StringCtrl.replaceWithDico(ERR_VALIDATION_NOMBRE_ANNEE_CIVILE_DEPASSEMENT, dico));
				}
				
			}

		}

	}

	/**
	 * Validation globale de l'absence avant enregistrement dans la base de
	 * données
	 */
	public void validationGenerale() throws NSValidation.ValidationException {
		validationCommune();
		validationSpecifique();
	}

	/**
	 * Effectue la validation metier sur l'absence selon son type : - durée
	 * maximum non dépassée - durée minimum - ...
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public abstract void validationSpecifique() throws NSValidation.ValidationException;

	/**
	 * La absence gérée par ce délégué
	 * 
	 * @return
	 */
	public final A_Absence getAbsence() {
		return absence;
	}

	/**
	 * Méthode interne utilitaire
	 * 
	 * @return
	 */
	private final EOOccupation getEoOccupation() {
		EOOccupation eoOccupation = null;

		if (getAbsence().isAbsenceLegale() == false) {
			eoOccupation = (EOOccupation) absence;
		}

		return eoOccupation;
	}

	/**
	 * Methode interne utilitaire
	 * 
	 * Ne conserver au sein d'une liste de soldes que ceux ayant validité la date
	 * indiquée
	 * 
	 * @param array
	 * @param date
	 * @return
	 */
	public static NSArray<I_Solde> filtrerDisponiblesPourDate(NSArray<I_Solde> array, NSTimestamp date) {
		EOQualifier qual = ERXQ.and(
				ERXQ.lessThanOrEqualTo(I_Solde.D_DEB_KEY, date),
				ERXQ.or(
						ERXQ.greaterThanOrEqualTo(I_Solde.D_FIN_KEY, date),
						ERXQ.isNull(I_Solde.D_FIN_KEY)));

		NSArray<I_Solde> filtredArray = new NSArray<I_Solde>(
				EOQualifier.filteredArrayWithQualifier(array, qual));

		return filtredArray;
	}

	private static NSArray<String> _delegateClassArray;

	/**
	 * La liste des classes concretes java de gestion des types de congé
	 * 
	 * @return
	 */
	public static NSArray<String> getDelegateClassList() {

		if (_delegateClassArray == null) {

			_delegateClassArray = new NSArray<String>();

			try {
				Class[] classArray = getClasses(A_OccupationTypeDelegate.class.getPackage().getName());

				for (Class classe : classArray) {

					// XXX trouver un autre moyen que par le nom de classe
					String className = classe.getName();

					if (!StringCtrl.containsIgnoreCase(className, ".A_")) {
						_delegateClassArray = _delegateClassArray.arrayByAddingObject(className);
					}

				}

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		}

		return _delegateClassArray;
	}

	private static Class[] getClasses(String pckgname) throws ClassNotFoundException {
		ArrayList<Class> classes = new ArrayList<Class>();
		// Get a File object for the package
		File directory = null;
		try {
			ClassLoader cld = Thread.currentThread().getContextClassLoader();
			if (cld == null) {
				throw new ClassNotFoundException("Can't get class loader.");
			}
			String path = pckgname.replace('.', '/');
			URL resource = cld.getResource(path);
			if (resource == null) {
				throw new ClassNotFoundException("No resource for " + path);
			}
			directory = new File(resource.getFile());
		} catch (NullPointerException x) {
			throw new ClassNotFoundException(pckgname + " (" + directory + ") does not appear to be a valid package");
		}
		if (directory.exists()) {
			// Get the list of the files contained in the package
			String[] files = directory.list();
			for (int i = 0; i < files.length; i++) {
				// we are only interested in .class files
				if (files[i].endsWith(".class")) {
					// removes the .class extension
					classes.add(Class.forName(pckgname + '.' + files[i].substring(0, files[i].length() - 6)));
				}
			}
		} else {
			throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
		}
		Class[] classesA = new Class[classes.size()];
		classes.toArray(classesA);
		return classesA;
	}

	/**
	 * Donne la liste des objets {@link Jour} contenus entre 2 dates. Les dates
	 * sont ramenées à minuit, le matin pour englober l'ensemble des jours
	 * couverts.
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	protected final NSArray<Jour> jours(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<Jour> jourArray = new NSArray<Jour>();

		for (EOPlanning eoPlanning : getAbsence().getEoPlanningArrayPourOccupation()) {
			jourArray = jourArray.arrayByAddingObjectsFromArray(eoPlanning.getPlanning().getJours(dateDebut, dateFin));
		}

		return jourArray;
	}

	/**
	 * Indique si l'absence contient au moins 1 jours du planning
	 * 
	 * @return
	 */
	private boolean isAbsenceInclueDansPlanning() {
		boolean isAbsenceInclueDansPlanning = false;

		NSArray<Jour> jourArray = jours(getAbsence().dateDebut(), getAbsence().dateFin());

		EOQualifier qual = ERXQ.isFalse(Jour.IS_HORS_AFFECTATION_KEY);
		// on autorise les jours fériés et chomés pour les présnces
		if (!getAbsence().isPresence()) {
			qual = ERXQ.and(
					ERXQ.isFalse(Jour.IS_CHOME_KEY),
					ERXQ.isFalse(Jour.IS_FERIE_KEY));
		}

		jourArray = EOQualifier.filteredArrayWithQualifier(jourArray, qual);

		if (jourArray.count() > 0) {
			isAbsenceInclueDansPlanning = true;
		}

		return isAbsenceInclueDansPlanning;
	}
}
