/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Profil des classes de calcul du droit a congé généré par une planning
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_PlanningCalculDroitCongeDelegate {

	private Planning planning;

	/**
	 * 
	 * @param planning
	 */
	public A_PlanningCalculDroitCongeDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super();
		this.planning = planning;
		this.dateDebutApplication = dateDebutApplication;
		this.dateFinApplication = dateFinApplication;
	}

	/**
	 * 
	 * @return
	 */
	protected final Planning getPlanning() {
		return planning;
	}

	/**
	 * Pour affichage dans la fenetre des détails
	 * 
	 * @return
	 */
	public abstract String affichageFenetreDetail();

	/**
	 * Pour affichage, durée d'application
	 * 
	 * @return
	 */
	public String periodeApplication() {
		String str = "";

		if (getDateDebutApplication() == null && getDateFinApplication() == null) {
			str = "toute la période";
		} else if (getDateDebutApplication() != null && getDateFinApplication() != null) {
			str = "du " + DateCtrlHamac.dateToString(getDateDebutApplication()) + " au " + DateCtrlHamac.dateToString(getDateFinApplication());
		} else if (getDateDebutApplication() != null) {
			str = "à partir du " + DateCtrlHamac.dateToString(getDateDebutApplication());
		} else if (getDateFinApplication() != null) {
			str = "jusqu'au " + DateCtrlHamac.dateToString(getDateFinApplication());
		}

		return str;
	}

	// DROIT A CONGE NATIF

	/**
	 * Le droit à congé issu d'un calcul (à la différence d'un droit à congé en
	 * dur)
	 * 
	 * @param cStructure
	 * @return
	 */
	public abstract int getDroitCongeDepuisCalculMinutes(String cStructure);

	/**
	 * @param cStructure
	 * @return
	 */
	public int getDroitCongeMinutes(String cStructure) {
		int congeMinutes = 0;

		if (StringCtrl.isEmpty(cStructure)) {

			for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {

				congeMinutes += getDroitCongeMinutes(eoStructure.cStructure());

			}

		} else {

			// est-il en dur ?
			if (isDroitCongeEnDur(cStructure)) {
				congeMinutes = getEoSoldeCongeNatif(cStructure).init().intValue();
			} else {
				// en mode automatique : se baser sur la classe
				congeMinutes = getDroitCongeDepuisCalculMinutes(cStructure);
			}

		}

		return congeMinutes;
	}

	/**
	 * 
	 * @return
	 */
	public final int getDroitCongeMinutesStructureCourante() {
		int congeMinutes = 0;

		congeMinutes = getDroitCongeMinutes(getPlanning().getCStructureCourante());

		return congeMinutes;
	}

	// SOLDE CONGES NATIF

	/**
	 * @param cStructure
	 * @return
	 */
	private final EOSolde getEoSoldeCongeNatif(String cStructure) {
		EOSolde eoSolde = null;

		eoSolde = getPlanning().getEoPlanning().getSoldeCongeNatif(
				cStructure);

		return eoSolde;
	}

	/**
	 * @param cStructure
	 * @return
	 */
	protected final boolean isDroitCongeEnDur(String cStructure) {
		boolean isEnDur = false;

		// on extrait l'objet EOSolde pour ne pas avoir des
		// java.lang.NullPointerException quand cette méthode est appelée alors que
		// le planning n'a jamais été chargé
		EOSolde eoSoldeCongeNatif = getEoSoldeCongeNatif(cStructure);

		if (eoSoldeCongeNatif != null && eoSoldeCongeNatif.init() != null) {
			isEnDur = true;
		}

		return isEnDur;
	}

	/**
	 * XXX repasser en privé (appel depuis rapport comparaison planning)
	 * 
	 * @param cStructure
	 * @return
	 */
	public int getDroitCongeRestantMinutes(String cStructure) {
		int minutes = 0;

		if (!StringCtrl.isEmpty(cStructure)) {

			EOSolde eoSolde = getEoSoldeCongeNatif(cStructure);

			if (eoSolde != null) {
				minutes = eoSolde.restant().intValue();
			}

		} else {

			NSArray<EOStructure> eoStructureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStructureArray) {

				minutes += getDroitCongeRestantMinutes(eoStructure.cStructure());

			}

		}

		return minutes;
	}

	/**
	 * 
	 * @return
	 */
	public final int getDroitCongeRestantMinutesStructureCourante() {
		return getDroitCongeRestantMinutes(getPlanning().getCStructureCourante());
	}

	// RELIQUAT_NATIF

	/**
	 * Le reliquat initial pour un service :
	 * 
	 * - cherche si une valeur a été mise manuellement
	 * 
	 * - sinon prendre la valeur du reliquat du planning n-1 sur la même structure
	 */
	public Integer getReliquatInitialMinutes(String cStructure) {

		Integer i_reliquatMinutes = null;

		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			int reliquatMinutes = 0;

			NSArray<EOStructure> eoStructureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStructureArray) {
				Integer rel = getReliquatInitialMinutes(eoStructure.cStructure());
				if (rel != null) {
					reliquatMinutes += rel.intValue();
				}
			}

			i_reliquatMinutes = Integer.valueOf(reliquatMinutes);

		} else {

			i_reliquatMinutes = getPlanning().getEoPlanning().getReliquatInitial(cStructure);

		}

		return i_reliquatMinutes;
	}

	/**
	 * 
	 * @return
	 */
	public final Integer getReliquatInitialMinutesStructureCourante() {
		return getReliquatInitialMinutes(getPlanning().getCStructureCourante());
	}

	/**
	 * Reliquat restant
	 * 
	 * @param cStructure
	 * @return
	 */
	public Integer getReliquatRestantMinutes(String cStructure) {
		Integer i_reliquatMinutes = null;

		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			int reliquatMinutes = 0;

			NSArray<EOStructure> eoStructureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStructureArray) {
				Integer rel = getReliquatRestantMinutes(eoStructure.cStructure());
				if (rel != null) {
					reliquatMinutes += rel.intValue();
				}
			}

			i_reliquatMinutes = Integer.valueOf(reliquatMinutes);

		} else {

			i_reliquatMinutes = getPlanning().getEoPlanning().getReliquatRestant(cStructure);

		}

		return i_reliquatMinutes;
	}

	/**
	 * 
	 * @return
	 */
	public final Integer getReliquatRestantMinutesStructureCourante() {
		return getReliquatRestantMinutes(getPlanning().getCStructureCourante());
	}

	// HEURES DUES

	/**
	 * 
	 */
	public abstract int getDuMinutes(String cStructure);

	/**
	 * 
	 * @return
	 */
	public final int getDuMinutesStructureCourante() {
		return getDuMinutes(getPlanning().getCStructureCourante());
	}

	/**
	 * Les heures dues pour entre une date début et une date butoir (tableaux de
	 * bord)
	 */
	public abstract int getDuMinutes(String cStructure, NSTimestamp dateButoirInclue);

	// RECUPERATIONS

	/**
	 * 
	 */
	public abstract int getRecuperationMinutes(String cStructure);

	/**
	 * @return
	 */
	public abstract int getRecuperationRestantMinutes(String cStructure);

	/**
	 * 
	 * @return
	 */
	public final int getRecuperationMinutesStructureCourante() {
		return getRecuperationMinutes(getPlanning().getCStructureCourante());
	}

	/**
	 * 
	 * @return
	 */
	public final int getRecuperationRestantMinutesStructureCourante() {
		return getRecuperationRestantMinutes(getPlanning().getCStructureCourante());
	}

	// BALANCE

	private final int getBalanceMinutes(String cStructure) {
		int minutes = 0;

		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {
			NSArray<EOStructure> eoStructureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStructureArray) {
				minutes += getBalanceMinutes(eoStructure.cStructure());
			}

		} else {
			minutes = getPlanning().getEoPlanning().getSoldeBalanceNatif(cStructure).restant().intValue();
			if (HamacCktlConfig.booleanForKey(EOParametre.REPORT_HSUP_ACTIF, getPlanning().getEoPlanning().toPeriode())) {
				minutes += getPlanning().getEoPlanning().getSoldeReliquatHsup(cStructure).restant().intValue();
			}
		}

		return minutes;
	}

	public final int getBalanceMinutesStructureCourante() {
		return getBalanceMinutes(getPlanning().getCStructureCourante());
	}

	// CALCULS MULTIPLES

	private NSTimestamp dateDebutApplication, dateFinApplication;

	public final NSTimestamp getDateDebutApplication() {
		return dateDebutApplication;
	}

	public final NSTimestamp getDateFinApplication() {
		return dateFinApplication;
	}

	/**
	 * Le mode de calcul est-il celui des contractuels
	 * 
	 * @return
	 */
	public abstract boolean isCalculContractuel();

}
