/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Mois;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.Recuperation;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * La méthode de calcul "ARTT" à la bordelaise. Tout ce qui est inférieur à la
 * durée hebdo attendue devient de la récupération négative. La récupération est
 * possible sur toutes les demi-journées
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CalculDroitCongesBdxDemiJourneeDelegate
		extends A_CalculDroitCongesRecuperationDelegate {

	/**
	 * @param planning
	 */
	public CalculDroitCongesBdxDemiJourneeDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
	}

	@Override
	public String affichageFenetreDetail() {
		return "Heures associées - heures dues + ATT : récupération hebdomadaire si dépassement de " + TimeCtrl.stringForMinutes(getSeuilRecup100()) + "/semaine";
	}

	/**
	 * Constuire la liste des soldes de récupération d'après les horaires associés
	 * au planning et le seuil hebdomadaire.
	 */
	@Override
	protected void constuireSoldes(String cStructure) {

		// TODO gérér date début et fin

		NSArray<I_Solde> array = new NSArray<I_Solde>();

		int congeGlobal = 0;
		int recuperationGlobale = 0;

		int dureeTotaleSemaine = 0;
		float recuperationSemaine = (float) 0;
		float congeSemaine = (float) 0;
		int nbDemiJourneeAssociee = 0;
		float seuilPourSemaine = (float) 0;

		for (Mois mois : getPlanning().getMoisArray()) {

			for (Semaine semaine : mois.getSemaines()) {

				// XXX voir pour ignorer les semaines plus proprement
				// XXX A VIRER
				if (semaine.dureeComptabiliseeMinutes(cStructure, getDateDebutApplication(), getDateFinApplication()) < 0) {
					continue;
				}

				if (semaine.isSemaineEnDebutDeFile()) {
					dureeTotaleSemaine = 0;
					recuperationSemaine = (float) 0;
					congeSemaine = (float) 0;
					nbDemiJourneeAssociee = 0;
					seuilPourSemaine = (float) 0;
				}

				NSMutableArray<Horaire> horaireArray = semaine.getHoraires();

				// XXX pour l'instant, on ne gère qu'1 seul horaire !
				if (horaireArray.count() > 0) {

					Horaire horaire = horaireArray.objectAtIndex(0);

					int nbDemiJournee = horaire.getNbDemiJournee();
					float seuilDemiJournee = ((float) getSeuilRecup100()) / (float) nbDemiJournee;

					for (Jour jour : semaine.getJours(getDateDebutApplication(), getDateFinApplication())) {

						float minutesAm = (float) jour.dureeComptabiliseeMinutesAm(cStructure);

						if (minutesAm > seuilDemiJournee) {
							congeSemaine += seuilDemiJournee;
							recuperationSemaine += (minutesAm - seuilDemiJournee);
						} else {
							congeSemaine += minutesAm;
						}

						float minutesPm = (float) jour.dureeComptabiliseeMinutesPm(cStructure);

						if (minutesPm > seuilDemiJournee) {
							congeSemaine += seuilDemiJournee;
							recuperationSemaine += (minutesPm - seuilDemiJournee);
						} else {
							congeSemaine += minutesPm;
						}

						dureeTotaleSemaine += minutesAm + minutesPm;

					}

					// augmenter le droit à congé au seuil de récupération en siphonant
					// les récup
					nbDemiJourneeAssociee += semaine.nbDemiJourneeAssociee(cStructure, getDateDebutApplication(), getDateFinApplication());
					seuilPourSemaine += ((float) nbDemiJourneeAssociee) * seuilDemiJournee;

					// ajout à la liste des recuperation
					if (semaine.isSemaineEnBoutDeFile()) {

						if (congeSemaine < seuilPourSemaine) {
							if (congeSemaine + recuperationSemaine <= seuilPourSemaine) {
								congeSemaine += recuperationSemaine;
								recuperationSemaine = 0;
							} else {
								recuperationSemaine = congeSemaine + recuperationSemaine - seuilPourSemaine;
								congeSemaine = seuilPourSemaine;
							}
						}

						if (recuperationSemaine > (float) 0) {

							Recuperation recup = new Recuperation(
									semaine, (int) recuperationSemaine, getPlanning().getEoStructureForCStructure(cStructure));
							array = array.arrayByAddingObject(recup);

							// accroissement de la récupération
							recuperationGlobale += recuperationSemaine;
						}

						congeGlobal += (int) congeSemaine;

						// CktlLog.log("\n\t" + TimeCtrl.stringForMinutes((int) congeGlobal)
						// + " (+" + TimeCtrl.stringForMinutes((int) congeSemaine) + ")");

					}

					// CktlLog.log(semaine + " constuireSoldes() conges=" +
					// TimeCtrl.stringForMinutes(congeGlobal) +
					// " congeSemaine=" +
					// TimeCtrl.stringForMinutes((int) congeSemaine) +
					// " recuperationGlobale=" +
					// TimeCtrl.stringForMinutes(recuperationGlobale) +
					// " recuperationSemaine=" +
					// TimeCtrl.stringForMinutes((int) recuperationSemaine) +
					// " dureeTotaleSemaine=" +
					// TimeCtrl.stringForMinutes(dureeTotaleSemaine));
				}

			}

		}

		// oter les heures dues pour le droit a conge
		congeGlobal = congeGlobal - getDuMinutes(cStructure);

		getSoldeRecuperationArrayDico().setObjectForKey(array, cStructure);
		getDroitCongeDico().setObjectForKey(Integer.valueOf(congeGlobal), cStructure);

		// System.out.println(getClass().getName() + " construireSoldes() " +
		// TimeCtrl.stringForMinutes(congeGlobal));

	}

	@Override
	public boolean isCalculContractuel() {
		return false;
	}
}
