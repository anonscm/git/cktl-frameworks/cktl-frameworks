package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class CongeAnnuelDelegate
		extends A_OccupationTypeDelegate {

	/**
	 * 
	 * @param absence
	 */
	public CongeAnnuelDelegate(A_Absence absence) {
		super(absence);
	}

	@Override
	public void validationSpecifique() throws ValidationException {
		// ...
	}

}
