/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation;

import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;

import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Classe de gestion des occupation "présence" (heures supp, astreintes ...)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_OccupationTypePresenceDelegate
		extends A_OccupationTypeDelegate {

	/**
	 * @param absence
	 */
	public A_OccupationTypePresenceDelegate(A_Absence absence) {
		super(absence);
	}

	@Override
	public void validationSpecifique() throws ValidationException {

		// la structure d'application est obligatoire
		if (((EOOccupation) getAbsence()).toStructureApplication() == null) {

			throw new NSValidation.ValidationException(
					"La structure d'application doit être précisée pour une occupation de type présence");

		}

	}

	/**
	 * La durée bonifiée associée à la présence
	 * 
	 * @param mDebut
	 * @param mFin
	 * @return
	 */
	public abstract int getDureeBonifiee(int mDebut, int mFin);

}
