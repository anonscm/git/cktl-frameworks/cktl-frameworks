/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Mois;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_PlanningCalculDroitCongesHeuresDuesDelegate
		extends A_PlanningCalculDroitCongeDelegate {

	public A_PlanningCalculDroitCongesHeuresDuesDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
	}

	@Override
	public int getDuMinutes(String cStructure) {

		int duMinutes = 0;

		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			NSArray<EOStructure> eoStuctureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStuctureArray) {
				duMinutes += getDuMinutes(eoStructure.cStructure());
			}

		} else {

			float f_minutes = (float) 0;

			NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray(cStructure);

			for (int i = 0; i < attenduArray.count(); i++) {
				TravailAttendu attendu = attenduArray.objectAtIndex(i);
				int minutesDues100 = HamacCktlConfig.intForKey(EOParametre.MINUTES_DUES, getPlanning().getEoPlanning().toPeriode());
				f_minutes += attendu.valeurPonderee(minutesDues100, getPlanning(), cStructure, getDateDebutApplication(), getDateFinApplication());
			}

			duMinutes = (int) f_minutes;

		}

		return duMinutes;
	}

	@Override
	public int getDuMinutes(String cStructure, NSTimestamp dateButoirInclue) {
		int duMinutes = 0;

		// cas d'un appel avec structure vide ...
		if (StringCtrl.isEmpty(cStructure)) {

			NSArray<EOStructure> eoStuctureArray = getPlanning().getEoStructureAttenduArray();

			for (EOStructure eoStructure : eoStuctureArray) {
				duMinutes += getDuMinutes(eoStructure.cStructure(), dateButoirInclue);
			}

		} else {

			float f_minutes = (float) 0;

			NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray(cStructure);

			for (int i = 0; i < attenduArray.count(); i++) {
				TravailAttendu attendu = attenduArray.objectAtIndex(i);
				int minutesDues100 = HamacCktlConfig.intForKey(EOParametre.MINUTES_DUES, getPlanning().getEoPlanning().toPeriode());
				// plafonnder la date butoir à la date de fin d'application s'il y a
				NSTimestamp dateButoirIncluePlafonnee = dateButoirInclue;
				if (getDateFinApplication() != null && dateButoirInclue != null && DateCtrl.isAfter(dateButoirInclue, getDateFinApplication())) {
					dateButoirIncluePlafonnee = getDateFinApplication();
				}
				f_minutes += attendu.valeurPonderee(minutesDues100, getPlanning(), cStructure, getDateDebutApplication(), dateButoirIncluePlafonnee);
			}

			duMinutes = (int) f_minutes;

		}

		return duMinutes;
	}
	
	/**
	 * Donne le nombre de jours dans le mois à compatibiliser sur la structure
	 * pour un planning
	 * 
	 * @param planning
	 * @param mois
	 * @param cStructure
	 * @param dateFinApplication
	 * @return
	 */
	public final static int getNbJourDansMois(
			TravailAttendu attendu, Planning planning, Mois mois, String cStructure, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		int nb = 0;

		// EOQualifier qualMois = ERXQ.not(
		// ERXQ.and(
		// ERXQ.lessThan(TravailAttendu.DATE_FIN_KEY, mois.getPremierJour()),
		// ERXQ.greaterThan(TravailAttendu.DATE_DEBUT_KEY, mois.getDernierJour())));
		//
		// // NSArray<TravailAttendu> attenduArray =
		// EOQualifier.filteredArrayWithQualifier(
		// planning.getTravailAttenduArray(cStructure), qualMois);

		EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

		// for (TravailAttendu attendu : attenduArray) {

		// la date de début est seuillé par le début de l'application du droit contractuel
		NSTimestamp dateDebut = attendu.getDateDebutSeuilleeDansPeriode(eoPeriode);
		if (dateDebutApplication != null &&
				DateCtrlHamac.isAfter(dateDebutApplication, dateDebut)) {
			dateDebut = dateDebutApplication;
		}

		// la date de fin est plafonnée par la fin de l'application du droits
		// contractuel
		NSTimestamp dateFin = attendu.getDateFinPlafonneeDansPeriode(eoPeriode);
		if (dateFinApplication != null &&
					DateCtrlHamac.isBefore(dateFinApplication, dateFin)) {
			dateFin = dateFinApplication;
		}

		nb += DateCtrlHamac.nbJourMoisTrentieme(mois.getPremierJour(), dateDebut, dateFin);

		// }

		// plafonner à 30 jours
		if (nb > DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30) {
			nb = DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30;
		}

		return nb;
	}
	
	

}
