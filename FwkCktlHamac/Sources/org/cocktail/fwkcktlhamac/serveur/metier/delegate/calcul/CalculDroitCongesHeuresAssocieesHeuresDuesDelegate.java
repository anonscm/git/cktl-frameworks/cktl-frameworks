/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;

import com.webobjects.foundation.NSTimestamp;

/**
 * La méthode de calcul "historique" de Hamac, à savoir : droit à congés =
 * heures associées - heures dues
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CalculDroitCongesHeuresAssocieesHeuresDuesDelegate
		extends A_PlanningCalculDroitCongesHeuresDuesDelegate {

	/**
	 * @param planning
	 */
	public CalculDroitCongesHeuresAssocieesHeuresDuesDelegate(
			Planning planning, NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication) {
		super(planning, dateDebutApplication, dateFinApplication);
	}

	@Override
	public String affichageFenetreDetail() {
		return "Heures associées - heures dues";
	}

	@Override
	public int getDroitCongeDepuisCalculMinutes(String cStructure) {
		int droitCongeMinutes = 0;

		int minutesComptabilisees = getPlanning().dureeComptabiliseeMinutes(cStructure, getDateDebutApplication(), getDateFinApplication());
		int minitesDues = getDuMinutes(cStructure);

		droitCongeMinutes = minutesComptabilisees - minitesDues;

		return droitCongeMinutes;
	}

	@Override
	public int getRecuperationMinutes(String cStructure) {
		return 0;
	}

	@Override
	public int getRecuperationRestantMinutes(String cStructure) {
		return 0;
	}

	@Override
	public boolean isCalculContractuel() {
		return false;
	}

}
