/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAssociationHoraire.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOAssociationHoraire extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "AssociationHoraire";
	public static final String ENTITY_TABLE_NAME = "HAMAC.ASSOCIATION_HORAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "assId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String SEMAINE_CODE_KEY = "semaineCode";

// Attributs non visibles
	public static final String ASS_ID_KEY = "assId";
	public static final String HOR_ID_KEY = "horId";
	public static final String PLA_ID_KEY = "plaId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String SEMAINE_CODE_COLKEY = "SEMAINE_CODE";

	public static final String ASS_ID_COLKEY = "ASS_ID";
	public static final String HOR_ID_COLKEY = "HOR_ID";
	public static final String PLA_ID_COLKEY = "PLA_ID";


	// Relationships
	public static final String TO_HORAIRE_KEY = "toHoraire";
	public static final String TO_PLANNING_KEY = "toPlanning";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String semaineCode() {
    return (String) storedValueForKey(SEMAINE_CODE_KEY);
  }

  public void setSemaineCode(String value) {
    takeStoredValueForKey(value, SEMAINE_CODE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire toHoraire() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire)storedValueForKey(TO_HORAIRE_KEY);
  }

  public void setToHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire oldValue = toHoraire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_HORAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_HORAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)storedValueForKey(TO_PLANNING_KEY);
  }

  public void setToPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning oldValue = toPlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLANNING_KEY);
    }
  }
  

/**
 * Créer une instance de EOAssociationHoraire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAssociationHoraire createEOAssociationHoraire(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String semaineCode
, org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire toHoraire, org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning			) {
    EOAssociationHoraire eo = (EOAssociationHoraire) createAndInsertInstance(editingContext, _EOAssociationHoraire.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSemaineCode(semaineCode);
    eo.setToHoraireRelationship(toHoraire);
    eo.setToPlanningRelationship(toPlanning);
    return eo;
  }

  
	  public EOAssociationHoraire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAssociationHoraire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAssociationHoraire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAssociationHoraire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAssociationHoraire object = (EOAssociationHoraire)createAndInsertInstance(editingContext, _EOAssociationHoraire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAssociationHoraire localInstanceIn(EOEditingContext editingContext, EOAssociationHoraire eo) {
    EOAssociationHoraire localInstance = (eo == null) ? null : (EOAssociationHoraire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAssociationHoraire#localInstanceIn a la place.
   */
	public static EOAssociationHoraire localInstanceOf(EOEditingContext editingContext, EOAssociationHoraire eo) {
		return EOAssociationHoraire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOAssociationHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAssociationHoraire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAssociationHoraire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAssociationHoraire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAssociationHoraire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAssociationHoraire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAssociationHoraire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAssociationHoraire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAssociationHoraire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAssociationHoraire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAssociationHoraire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAssociationHoraire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAssociationHoraire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
