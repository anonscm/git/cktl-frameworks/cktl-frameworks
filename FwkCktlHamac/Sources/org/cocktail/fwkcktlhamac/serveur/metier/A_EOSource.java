/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier;

import org.apache.commons.lang.IllegalClassException;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.ibm.icu.util.GregorianCalendar;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * Pattern d'un objet source
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_EOSource
		extends A_ToManyPersonne {

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (dDebut() == null) {
			throw new NSValidation.ValidationException("La date de début en peut être vide");
		}

		if (dFin() != null && DateCtrlHamac.isAfter(dDebut(), dFin())) {
			throw new NSValidation.ValidationException("La date de début doit être avant la date de fin");
		}

		if (quotite() == null) {
			throw new NSValidation.ValidationException("La quotité ne peut être vide");
		}

		if (quotite().intValue() < 1 || quotite().intValue() > 100) {
			throw new NSValidation.ValidationException("La quotité doit être entre 1% et 100%");
		}

		if (toStructureAffectation() == null && cStructure() == null) {
			throw new NSValidation.ValidationException("Le service d'affectation doit être renseigné");
		}

		super.validateObjectMetier();
	}

	private final static String STR_QUAL_SOURCE_PERIODE =
			"not(" + EOPlanningSource.D_DEBUT_KEY + ">%@ or " + EOPlanningSource.D_FIN_KEY + "<%@) or " +
					"(" + EOPlanningSource.D_DEBUT_KEY + "=nil and " + EOPlanningSource.D_FIN_KEY + "=nil) or " +
					"(" + EOPlanningSource.D_DEBUT_KEY + "<=%@ and " + EOPlanningSource.D_FIN_KEY + "=nil) or " +
					"(" + EOPlanningSource.D_DEBUT_KEY + "=nil and " + EOPlanningSource.D_FIN_KEY + ">=%@)";

	private final static String STR_QUAL_SOURCE_APRES_DATE_PRISE_EN_COMPTE =
			EOPlanningSource.D_DEBUT_KEY + " >=%@ or " +
					"(" + EOPlanningSource.D_DEBUT_KEY + "<=%@ and " + EOPlanningSource.D_FIN_KEY + "=nil) or " +
					"(" + EOPlanningSource.D_DEBUT_KEY + "<=%@ and " + EOPlanningSource.D_FIN_KEY + ">=%@)";

	private final static String STR_QUAL_SOURCE_PERS_ID = EOPlanningSource.PERS_ID_KEY + "=%@";

	private final static String STR_QUAL_SOURCE_STRUCTURE_AFFECTATION = EOPlanningSource.C_STRUCTURE_KEY + "=%@";

	private final static NSArray<EOSortOrdering> SORT_D_DEBUT_ET_STRUCTURE = new NSArray<EOSortOrdering>(
			new EOSortOrdering[] {
					new EOSortOrdering(EOPlanningSource.D_DEBUT_KEY, EOSortOrdering.CompareAscending),
					new EOSortOrdering(EOPlanningSource.TO_STRUCTURE_AFFECTATION_KEY + "." + EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending) });

	// deprecated ou pas ?
	@Deprecated
	public final static String TO_INDIVIDU_PROPRIETAIRE_KEY = "toIndividuProprietaire";

	public abstract EOStructure toStructureAffectation();

	public abstract String cStructure();

	public abstract NSTimestamp dDebut();

	public abstract NSTimestamp dFin();

	public abstract Integer persId();

	public abstract Integer quotite();

	/**
	 * Le qualifier pour obtenir la liste des sources associées à une periode et
	 * une structure d'affectation (filtrage par rapport aux dates de la période
	 * et au C_STRUCTURE)
	 * 
	 * @param eoPlanning
	 *          TODO
	 * @param eoPlanning
	 * 
	 * @return
	 */
	protected static EOQualifier getQualifierSourceFor(
			EOPeriode eoPeriode, NSArray<EOStructure> eoStructureArray, NSArray<Integer> persIdArray) {
		EOQualifier qual = null;

		String strQual = "";
		NSArray<Object> args = new NSArray<Object>();

		if (eoPeriode != null) {
			strQual = STR_QUAL_SOURCE_PERIODE;
			args = new NSArray<Object>(
					new NSTimestamp[] {
							eoPeriode.perDFin(),
							eoPeriode.perDDebut(),
							eoPeriode.perDFin(),
							eoPeriode.perDDebut() });
		}

		if (eoStructureArray != null && eoStructureArray.count() > 0) {
			if (strQual.length() > 0) {
				strQual = "(" + strQual + ") and ";
			}
			strQual += "(";
			for (int i = 0; i < eoStructureArray.count(); i++) {
				EOStructure eoStructure = eoStructureArray.objectAtIndex(i);
				strQual += STR_QUAL_SOURCE_STRUCTURE_AFFECTATION;
				args = args.arrayByAddingObject(eoStructure.cStructure());
				if (i < eoStructureArray.count() - 1) {
					strQual += " or ";
				}
			}
			strQual += ")";
		}
		
		if (persIdArray != null && persIdArray.count() > 0) {
			if (strQual.length() > 0) {
				strQual = "(" + strQual + ") and ";
			}
			strQual += "(";
			for (int i = 0; i < persIdArray.size(); i++) {
				Integer persId = persIdArray.get(i);
				strQual += STR_QUAL_SOURCE_PERS_ID;
				args = args.arrayByAddingObject(persId);
				if (i < persIdArray.size() - 1) {
					strQual += " or ";
				}
			}
			strQual += ")";
		}

		qual = CktlDataBus.newCondition(strQual, args);

		return qual;
	}

	/**
	 * Ne conserver des sources qu'ayant lieu sur une période
	 * 
	 * @param array
	 * @param eoPeriode
	 * @return
	 */
	public final static NSArray<A_EOSource> filtrerSourceArraySurPeriode(
			NSArray<A_EOSource> array, EOPeriode eoPeriode) {
		String strQual = STR_QUAL_SOURCE_PERIODE;
		NSArray<Object> args = new NSArray<Object>(
				new NSTimestamp[] {
						eoPeriode.perDFin(),
						eoPeriode.perDDebut(),
						eoPeriode.perDFin(),
						eoPeriode.perDDebut() });

		EOQualifier qual = CktlDataBus.newCondition(strQual, args);

		return EOQualifier.filteredArrayWithQualifier(array, qual);
	}

	/**
	 * Pour affichage dans le composant TestSaisieAffectaion
	 */
	@Override
	public String toString() {
		String str = "";

		if (toPersonne() != null) {
			str += toPersonne().getNomCompletAffichage();
		} else {
			str += "** erreur toPersonne() **";
		}

		str += " sur ";
		str += toStructureAffectation().lcStructure();

		if (dFin() != null) {
			str += " du ";
			str += DateCtrlHamac.dateToString(dDebut());
			str += " au ";
			str += DateCtrlHamac.dateToString(dFin());
		} else {
			str += " à partir du ";
			str += DateCtrlHamac.dateToString(dDebut());
		}

		str += " @";
		str += quotite().intValue();
		str += "%";

		return str;
	}

	/**
	 * Liste de tous les enregistrements de la source sur une periode et un
	 * service
	 * 
	 * @param clazz
	 * @param editingContext
	 * @param eoPeriode
	 * @param eoStructureArray
	 * @param persId
	 * @param dateDebutPriseEnCompte
	 *          mettre null si pas de restriction
	 * @return
	 */
	public final static NSArray<A_EOSource> getSourceArrayFor(
			Class<?> clazz,
			EOEditingContext editingContext,
			EOPeriode eoPeriode,
			NSArray<EOStructure> eoStructureArray,
			NSArray<Integer> persIdArray,
			NSTimestamp dateDebutPriseEnCompte) {

		// test si la classe est autorisée
		if (clazz != EOPlanningSource.class && clazz != EOVConnecteurAffectation.class && clazz != EOAffectationsMangueHamac.class) {
			throw new IllegalClassException(
					"Cette classe n'est pas reconnu comme données source : " + clazz.getName());
		}

		EOQualifier qual = getQualifierSourceFor(eoPeriode, eoStructureArray, persIdArray);
		if (dateDebutPriseEnCompte != null) {
			EOQualifier qualDatePriseEnCompte = CktlDataBus.newCondition(
					STR_QUAL_SOURCE_APRES_DATE_PRISE_EN_COMPTE, new NSArray<NSTimestamp>(new NSTimestamp[] {
							dateDebutPriseEnCompte, dateDebutPriseEnCompte, dateDebutPriseEnCompte, dateDebutPriseEnCompte }));
			qual = ERXQ.and(
					qual, qualDatePriseEnCompte);
		}

		NSArray<A_EOSource> array = new NSArray<A_EOSource>();
		
		if (clazz == EOPlanningSource.class) {

			NSArray<EOPlanningSource> eoPlanningSourceArray = EOPlanningSource.fetchAll(
					editingContext, qual, SORT_D_DEBUT_ET_STRUCTURE);

			for (EOPlanningSource eoPlanningSource : eoPlanningSourceArray) {
				array = array.arrayByAddingObject(eoPlanningSource);
			}

		} else if (clazz == EOVConnecteurAffectation.class) {

			NSArray<EOVConnecteurAffectation> eoConnecteurSourceArray = EOVConnecteurAffectation.fetchAll(
					editingContext, qual, SORT_D_DEBUT_ET_STRUCTURE);

			for (EOVConnecteurAffectation eoPlanningSource : eoConnecteurSourceArray) {
				array = array.arrayByAddingObject(eoPlanningSource);
			}

		} else if (clazz == EOAffectationsMangueHamac.class) {
			NSArray<EOAffectationsMangueHamac> eoConnecteurSourceArray = EOAffectationsMangueHamac.fetchAll(
					editingContext, qual, SORT_D_DEBUT_ET_STRUCTURE);
			
			for (EOAffectationsMangueHamac eoPlanningSource : eoConnecteurSourceArray) {
				array = array.arrayByAddingObject(eoPlanningSource);
			}
			
		}

		return array;
	}

	/**
	 * Méthode de comparaison, distincte de {@link #equals(Object)} car elle ne
	 * compare que les attributs
	 * 
	 * @param eoSource
	 * @return
	 */
	public final boolean isIdentique(A_EOSource eoSource) {
		boolean identique = false;

		if (persId().intValue() == eoSource.persId().intValue() &&
				quotite().doubleValue() == eoSource.quotite().doubleValue() &&
				toStructureAffectation().cStructure().equals(eoSource.toStructureAffectation().cStructure()) &&
				isDateIdentique(dDebut(), eoSource.dDebut()) &&
				isDateIdentique(dFin(), eoSource.dFin())) {
			identique = true;
		}

		return identique;
	}

	/**
	 * Méthode de comparaison sur les objets {@link TravailAttendu} générés et sur
	 * une {@link EOPeriode}
	 * 
	 * @param eoSource
	 * @return
	 */
	public final boolean isIdentiqueTravailAttenduSurPeriode(
			A_EOSource eoSource, EOPeriode eoPeriode) {
		boolean identique = false;

		if (persId().intValue() == eoSource.persId().intValue() &&
				quotite().doubleValue() == eoSource.quotite().doubleValue() &&
				toStructureAffectation().cStructure().equals(eoSource.toStructureAffectation().cStructure())) {
			TravailAttendu taThis = new TravailAttendu(this);
			TravailAttendu taAutre = new TravailAttendu(eoSource);

			NSArray<TravailAttendu> taArray = new NSArray<TravailAttendu>(new TravailAttendu[] { taThis, taAutre });

			identique = true;
			NSTimestamp date = eoPeriode.perDDebut();

			while (identique && DateCtrlHamac.isBeforeEq(date, eoPeriode.perDFin())) {
				int count = TravailAttendu.filtrerTravailAttenduPourDate(taArray, date).count();
				// ce doit être les mêmes (2 : attendu au deux ou 0 : attendu sur aucun)
				if (count != 0 &&
						count != 2) {
					identique = false;
				}
				date = date.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
			}

		}

		return identique;
	}

	/**
	 * Méthode interne pour comparer 2 dates, en gérant les <code>null</code>
	 * 
	 * @return
	 */
	private boolean isDateIdentique(NSTimestamp date1, NSTimestamp date2) {
		boolean isIdentique = false;

		if (date1 == null && date2 == null) {

			isIdentique = true;

		} else {
			if ((date1 == null && date2 != null) || (date1 != null && date2 == null)) {

				isIdentique = false;

			} else {

				GregorianCalendar gc1 = new GregorianCalendar();
				gc1.setTime(date1);
				GregorianCalendar gc2 = new GregorianCalendar();
				gc2.setTime(date2);

				if (gc1.get(GregorianCalendar.YEAR) == gc2.get(GregorianCalendar.YEAR) &&
						gc1.get(GregorianCalendar.DAY_OF_YEAR) == gc2.get(GregorianCalendar.DAY_OF_YEAR)) {
					isIdentique = true;
				}
			}
		}

		return isIdentique;
	}

	/**
	 * Faire un refault s'il y a un pb
	 */
	@Override
	public IPersonne toPersonne() {
		IPersonne toPersonne = super.toPersonne();

		if (toPersonne == null) {
			ERXEOControlUtilities.refaultObject(this);
			toPersonne = super.toPersonne();
		}

		return toPersonne;
	}

}
