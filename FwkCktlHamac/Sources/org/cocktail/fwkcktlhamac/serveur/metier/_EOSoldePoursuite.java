/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSoldePoursuite.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOSoldePoursuite extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "SoldePoursuite";
	public static final String ENTITY_TABLE_NAME = "HAMAC.SOLDE_POURSUITE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "spoId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String SPO_POURCENTAGE_KEY = "spoPourcentage";
	public static final String SPO_VALEUR_PONDEREE_KEY = "spoValeurPonderee";

// Attributs non visibles
	public static final String SOL_ID_DST_KEY = "solIdDst";
	public static final String SOL_ID_SRC_KEY = "solIdSrc";
	public static final String SPO_ID_KEY = "spoId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String SPO_POURCENTAGE_COLKEY = "SPO_POURCENTAGE";
	public static final String SPO_VALEUR_PONDEREE_COLKEY = "SPO_VALEUR_PONDEREE";

	public static final String SOL_ID_DST_COLKEY = "SOL_ID_DST";
	public static final String SOL_ID_SRC_COLKEY = "SOL_ID_SRC";
	public static final String SPO_ID_COLKEY = "SPO_ID";


	// Relationships
	public static final String TO_SOLDE_DST_KEY = "toSoldeDst";
	public static final String TO_SOLDE_SRC_KEY = "toSoldeSrc";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer spoPourcentage() {
    return (Integer) storedValueForKey(SPO_POURCENTAGE_KEY);
  }

  public void setSpoPourcentage(Integer value) {
    takeStoredValueForKey(value, SPO_POURCENTAGE_KEY);
  }

  public Integer spoValeurPonderee() {
    return (Integer) storedValueForKey(SPO_VALEUR_PONDEREE_KEY);
  }

  public void setSpoValeurPonderee(Integer value) {
    takeStoredValueForKey(value, SPO_VALEUR_PONDEREE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOSolde toSoldeDst() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSolde)storedValueForKey(TO_SOLDE_DST_KEY);
  }

  public void setToSoldeDstRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOSolde oldValue = toSoldeDst();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SOLDE_DST_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SOLDE_DST_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOSolde toSoldeSrc() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSolde)storedValueForKey(TO_SOLDE_SRC_KEY);
  }

  public void setToSoldeSrcRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSolde value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOSolde oldValue = toSoldeSrc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SOLDE_SRC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SOLDE_SRC_KEY);
    }
  }
  

/**
 * Créer une instance de EOSoldePoursuite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSoldePoursuite createEOSoldePoursuite(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer spoPourcentage
, Integer spoValeurPonderee
, org.cocktail.fwkcktlhamac.serveur.metier.EOSolde toSoldeDst, org.cocktail.fwkcktlhamac.serveur.metier.EOSolde toSoldeSrc			) {
    EOSoldePoursuite eo = (EOSoldePoursuite) createAndInsertInstance(editingContext, _EOSoldePoursuite.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSpoPourcentage(spoPourcentage);
		eo.setSpoValeurPonderee(spoValeurPonderee);
    eo.setToSoldeDstRelationship(toSoldeDst);
    eo.setToSoldeSrcRelationship(toSoldeSrc);
    return eo;
  }

  
	  public EOSoldePoursuite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSoldePoursuite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSoldePoursuite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSoldePoursuite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSoldePoursuite object = (EOSoldePoursuite)createAndInsertInstance(editingContext, _EOSoldePoursuite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSoldePoursuite localInstanceIn(EOEditingContext editingContext, EOSoldePoursuite eo) {
    EOSoldePoursuite localInstance = (eo == null) ? null : (EOSoldePoursuite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSoldePoursuite#localInstanceIn a la place.
   */
	public static EOSoldePoursuite localInstanceOf(EOEditingContext editingContext, EOSoldePoursuite eo) {
		return EOSoldePoursuite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOSoldePoursuite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSoldePoursuite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSoldePoursuite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSoldePoursuite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSoldePoursuite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSoldePoursuite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSoldePoursuite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSoldePoursuite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSoldePoursuite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSoldePoursuite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSoldePoursuite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSoldePoursuite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSoldePoursuite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
