/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.DemandeDelegate;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOTypeStatut
		extends _EOTypeStatut {

	public EOTypeStatut() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String STATUT_CODE_NON_VALIDE = "NON_VALIDE";
	public final static String STATUT_CODE_VISE = "VISE";
	public final static String STATUT_CODE_VALIDE = "VALIDE";
	public final static String STATUT_CODE_REFUSE = "REFUSE";
	public final static String STATUT_CODE_SUPPRIME = "SUPPRIME";
	public final static String STATUT_CODE_EN_COURS_DE_VALIDATION = "EN_COURS_DE_VALIDATION";
	public final static String STATUT_CODE_EN_COURS_DE_SUPPRESSION = "EN_COURS_DE_SUPPRESSION";
	public final static String STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE = "EN_COURS_DE_SUPPRESSION_VISE";
	public final static String STATUT_CODE_EN_COURS_DE_MODIFICATION = "EN_COURS_DE_MODIFICATION";
	public final static String STATUT_CODE_EN_COURS_DE_MODIFICATION_VISE = "EN_COURS_DE_MODIFICATION_VISE";
	public final static String STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE = "EN_COURS_DE_VALIDATION_PREVISIONNELLE";
	public final static String STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE = "EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE";

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutValide() {
		boolean isStatutValide = false;

		if (staCode().equals(STATUT_CODE_VALIDE)) {
			isStatutValide = true;
		}

		return isStatutValide;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutNonValide() {
		boolean isStatutNonValide = false;

		if (staCode().equals(STATUT_CODE_NON_VALIDE)) {
			isStatutNonValide = true;
		}

		return isStatutNonValide;
	}

	/**
	 * Indique le planning peut faire l'objet d'une demande de validation
	 * 
	 * @return
	 */
	public final boolean isEtatPlanningAutorisantValidation() {
		boolean isStatutNonValide = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_MODIFICATION) ||
				staCode().equals(STATUT_CODE_NON_VALIDE)) {
			isStatutNonValide = true;
		}

		return isStatutNonValide;
	}

	/**
	 * Indique le planning peut faire l'objet d'une modification d'association
	 * horaire semaine
	 * 
	 * @return
	 */
	public final boolean isEtatPlanningAutorisantModification() {
		boolean isStatutNonValide = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_MODIFICATION) ||
				staCode().equals(STATUT_CODE_NON_VALIDE)) {
			isStatutNonValide = true;
		}

		return isStatutNonValide;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeModification() {
		boolean isStatutEnCoursDeValidation = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_MODIFICATION)) {
			isStatutEnCoursDeValidation = true;
		}

		return isStatutEnCoursDeValidation;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeValidation() {
		boolean isStatutEnCoursDeValidation = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_VALIDATION)) {
			isStatutEnCoursDeValidation = true;
		}

		return isStatutEnCoursDeValidation;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeValidationPrevisionnelle() {
		boolean isStatutEnCoursDeValidationPrevisionnelle = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE) ||
				staCode().equals(STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE)) {
			isStatutEnCoursDeValidationPrevisionnelle = true;
		}

		return isStatutEnCoursDeValidationPrevisionnelle;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutVise() {
		boolean isStatutVise = false;

		if (staCode().equals(STATUT_CODE_VISE) ||
				staCode().equals(STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE)) {
			isStatutVise = true;
		}

		return isStatutVise;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutRefuse() {
		boolean isStatutRefuse = false;

		if (staCode().equals(STATUT_CODE_REFUSE)) {
			isStatutRefuse = true;
		}

		return isStatutRefuse;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutSupprime() {
		boolean isStatutSupprime = false;

		if (staCode().equals(STATUT_CODE_SUPPRIME)) {
			isStatutSupprime = true;
		}

		return isStatutSupprime;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeSuppressionVise() {
		boolean isStatutEnCoursDeSuppressionVise = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE)) {
			isStatutEnCoursDeSuppressionVise = true;
		}

		return isStatutEnCoursDeSuppressionVise;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeSuppression() {
		boolean isStatutEnCoursDeSuppression = false;

		if (staCode().equals(STATUT_CODE_EN_COURS_DE_SUPPRESSION)) {
			isStatutEnCoursDeSuppression = true;
		}

		return isStatutEnCoursDeSuppression;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isStatutEnCoursDeSuppressionOuIsStatutEnCoursDeSuppressionVise() {
		boolean isStatutEnCoursDeSuppressionOuIsStatutEnCoursDeSuppressionVise = false;

		if (isStatutEnCoursDeSuppression() ||
				isStatutEnCoursDeSuppressionVise()) {
			isStatutEnCoursDeSuppressionOuIsStatutEnCoursDeSuppressionVise = true;
		}

		return isStatutEnCoursDeSuppressionOuIsStatutEnCoursDeSuppressionVise;
	}

	/**
	 * Indique si l'état est considéré comme validé
	 */
	public final boolean isEtatValidation() {
		boolean isEtatValidation = false;

		if (isStatutValide()) {
			isEtatValidation = true;
		}

		return isEtatValidation;
	}

	/**
	 * Indique si l'état autorise le planning a apparaitre dans le centre de
	 * validation
	 */
	public final boolean isEtatPourCentreDeValidation() {
		boolean isEtatPourCentreDeValidation = false;

		if (isStatutRefuse() == false &&
				isStatutSupprime() == false &&
				isStatutValide() == false) {
			isEtatPourCentreDeValidation = true;
		}

		return isEtatPourCentreDeValidation;
	}

	/**
	 * @param ec
	 * @param code
	 * @return
	 */
	private static EOTypeStatut getEoTypeStatutForCode(EOEditingContext ec, String code) {
		EOTypeStatut eoTypeStatut = null;

		eoTypeStatut = EOTypeStatut.fetchByKeyValue(
				ec, EOTypeStatut.STA_CODE_KEY, code);

		return eoTypeStatut;
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutEnCoursDeValidation(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_EN_COURS_DE_VALIDATION);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutEnCoursDeValidationPrevisionnelle(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutEnCoursDeModification(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_EN_COURS_DE_MODIFICATION);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutNonValide(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_NON_VALIDE);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutSupprime(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_SUPPRIME);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutValide(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_VALIDE);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeStatut getEoTypeStatutEnCoursDeSuppression(
			EOEditingContext ec) {
		return getEoTypeStatutForCode(ec, STATUT_CODE_EN_COURS_DE_SUPPRESSION);
	}

	private final static NSMutableDictionary<String, NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>> DICO_TRANSITION_ETAT_OCCUPATION =
			new NSMutableDictionary<String, NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>>();

	static {

		NSMutableDictionary<String, NSArray<String>> VISA_OK = new NSMutableDictionary<String, NSArray<String>>();

		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VISE),
				EOTypeStatut.STATUT_CODE_VISE);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);

		NSMutableDictionary<String, NSArray<String>> VISA_NOK = new NSMutableDictionary<String, NSArray<String>>();

		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_REFUSE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_REFUSE),
				EOTypeStatut.STATUT_CODE_VISE);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);

		NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>> VISA = new NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>();
		VISA.setObjectForKey(VISA_OK, Boolean.TRUE);
		VISA.setObjectForKey(VISA_NOK, Boolean.FALSE);

		NSMutableDictionary<String, NSArray<String>> VALIDATION_OK = new NSMutableDictionary<String, NSArray<String>>();

		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_VISE);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_SUPPRIME),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_SUPPRIME),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);

		NSMutableDictionary<String, NSArray<String>> VALIDATION_NOK = new NSMutableDictionary<String, NSArray<String>>();

		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_REFUSE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_REFUSE),
				EOTypeStatut.STATUT_CODE_VISE);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_SUPPRESSION_VISE);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(new String[] { EOTypeStatut.STATUT_CODE_VALIDE, STATUT_CODE_EN_COURS_DE_MODIFICATION }),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION);

		NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>> VALIDATION =
				new NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>();
		VALIDATION.setObjectForKey(VALIDATION_OK, Boolean.TRUE);
		VALIDATION.setObjectForKey(VALIDATION_NOK, Boolean.FALSE);

		DICO_TRANSITION_ETAT_OCCUPATION.setObjectForKey(VISA, EOTypeVisa.CODE_VISA);
		DICO_TRANSITION_ETAT_OCCUPATION.setObjectForKey(VALIDATION, EOTypeVisa.CODE_VALIDATION);

	}

	private final static NSMutableDictionary<String, NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>> DICO_TRANSITION_ETAT_PLANNING =
			new NSMutableDictionary<String, NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>>();

	static {

		NSMutableDictionary<String, NSArray<String>> VISA_OK = new NSMutableDictionary<String, NSArray<String>>();

		// XXX Ajouter un statut
		// STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VISE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VISA_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VISE),
				EOTypeStatut.STATUT_CODE_VISE);

		NSMutableDictionary<String, NSArray<String>> VISA_NOK = new NSMutableDictionary<String, NSArray<String>>();

		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_NON_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_NON_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VISA_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION),
				EOTypeStatut.STATUT_CODE_VISE);

		NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>> VISA = new NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>();
		VISA.setObjectForKey(VISA_OK, Boolean.TRUE);
		VISA.setObjectForKey(VISA_NOK, Boolean.FALSE);

		NSMutableDictionary<String, NSArray<String>> VALIDATION_OK = new NSMutableDictionary<String, NSArray<String>>();

		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_VISE);
		VALIDATION_OK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE);

		NSMutableDictionary<String, NSArray<String>> VALIDATION_NOK = new NSMutableDictionary<String, NSArray<String>>();

		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_NON_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_NON_VALIDE),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION_PREVISIONNELLE_VISE);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION),
				EOTypeStatut.STATUT_CODE_EN_COURS_DE_VALIDATION);
		VALIDATION_NOK.setObjectForKey(
				new NSArray<String>(EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION),
				EOTypeStatut.STATUT_CODE_VISE);
		NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>> VALIDATION =
				new NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>();
		VALIDATION.setObjectForKey(VALIDATION_OK, Boolean.TRUE);
		VALIDATION.setObjectForKey(VALIDATION_NOK, Boolean.FALSE);

		DICO_TRANSITION_ETAT_PLANNING.setObjectForKey(VISA, EOTypeVisa.CODE_VISA);
		DICO_TRANSITION_ETAT_PLANNING.setObjectForKey(VALIDATION, EOTypeVisa.CODE_VALIDATION);

	}

	/**
	 * Le(s) statut(s) attendu(s) consécutif(s) à un visa. Si plusieurs sont
	 * proposés, alors charge à l'utilisateur d'en selectionner un.
	 * 
	 * @param demandeDelegate
	 *          TODO
	 * @param eoTypeVisa
	 * @param eoStructureViseur
	 *          TODO
	 * @param decision
	 * 
	 * @return
	 */
	public NSMutableArray<EOTypeStatut> getEoTypeStatutArrayPourVisa(
			DemandeDelegate demandeDelegate, EOTypeVisa eoTypeVisa, EOStructure eoStructureViseur, String decision) {
		NSMutableArray<EOTypeStatut> eoTypeStatutArray = new NSMutableArray<EOTypeStatut>();

		Boolean bIsAccepte = Boolean.FALSE;
		if (decision.equals(OUI)) {
			bIsAccepte = Boolean.TRUE;
		}

		// l'acceptation implique que tous les visas du meme niveau pour tous les
		// autres services soit aussi acceptés pour atteindre les états suivants
		boolean isStatutSuivantAccessible = true;
		if (bIsAccepte == Boolean.TRUE) {

			NSArray<EOStructure> eoStructureAFaireValiderArray = demandeDelegate.getEoStructureAFaireValiderArray();

			if (eoStructureAFaireValiderArray.count() > 1) {

				for (EOStructure eoStructure : eoStructureAFaireValiderArray) {

					if (eoStructure != eoStructureViseur) {
						EOVisa eoVisa = demandeDelegate.getVisaPourStructureEtTypeVisa(eoStructure, eoTypeVisa);
						if (eoVisa == null ||
								!eoVisa.isAccepte()) {
							isStatutSuivantAccessible = false;
						}
					}

				}

			}
		} else {
			// le refus est éliminatoire et donne donc accès au statut suivant de fait
		}

		if (isStatutSuivantAccessible) {

			// sélectionner le bon dictionnaire état transaction
			NSMutableDictionary<String, NSMutableDictionary<Boolean, NSMutableDictionary<String, NSArray<String>>>> dico = DICO_TRANSITION_ETAT_PLANNING;
			if (demandeDelegate.getEoOccupation() != null) {
				dico = DICO_TRANSITION_ETAT_OCCUPATION;
			}

			NSArray<String> codeStatutArray = dico.objectForKey(eoTypeVisa.vtypCode()).objectForKey(bIsAccepte).objectForKey(staCode());

			for (String codeStatut : codeStatutArray) {
				eoTypeStatutArray.addObject(getEoTypeStatutForCode(editingContext(), codeStatut));
			}

		} else {
			// on reste sur l'état initial
			eoTypeStatutArray.addObject(this);
		}

		return eoTypeStatutArray;
	}
}
