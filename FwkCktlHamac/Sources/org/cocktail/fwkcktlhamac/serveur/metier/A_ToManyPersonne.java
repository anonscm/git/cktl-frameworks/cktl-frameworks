/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.foundation.NSArray;

/**
 * Classe ayant un attribut PERS_ID pointant potentiellement sur un individu ou
 * une structure
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_ToManyPersonne
		extends A_FwkCktlHamacRecord {

	public final static String TO_PERSONNE_KEY = "toPersonne";

	private static boolean ACTIVER_CACHE = true;

	private IPersonne personneCache;

	/**
	 * La to-many vers l'individu s'il s'agit d'un individu
	 * 
	 * @return
	 */
	public abstract NSArray<EOIndividu> tosIndividu();

	/**
	 * La to-many vers la structure s'il s'agit d'une structure
	 * 
	 * @return
	 */
	public abstract NSArray<EOStructure> tosStructure();

	/**
	 * 
	 */
	public abstract void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object);

	/**
	 * 
	 */
	public abstract void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object);

	/**
	 * 
	 */
	public abstract void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object);

	/**
	 * 
	 */
	public abstract void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object);

	/**
	 * 
	 */
	public abstract Integer persId();

	/**
   * 
   */
	public abstract void setPersId(Integer value);

	/**
	 * @return La personne de l'objet.
	 */
	public IPersonne toPersonne() {
		if (ACTIVER_CACHE) {
			if (personneCache != null) {
				return personneCache;
			}
		}

		if (tosStructure() != null && tosStructure().count() > 0) {
			personneCache = tosStructure().objectAtIndex(0);
		} else if (tosIndividu() != null && tosIndividu().count() > 0) {
			personneCache = tosIndividu().objectAtIndex(0);
		}
		return personneCache;
	}

	/**
	 * Affecte la personne.
	 * 
	 * @param personne
	 */
	public void setToPersonne(IPersonne personne) {
		// Nettoyer
		cleanPersonneProprietaireCache();
		while (toPersonne() != null) {
			if (toPersonne() instanceof EOIndividu) {
				removeFromTosIndividuRelationship((EOIndividu) toPersonne());
			}
			if (toPersonne() instanceof EOStructure) {
				removeFromTosStructureRelationship((EOStructure) toPersonne());
			}
		}

		IPersonne _personne = personne;
		if (_personne == null) {
			setPersId(null);
		} else {
			if (!this.editingContext().equals(personne.editingContext())) {
				_personne = personne.localInstanceIn(this.editingContext());
			}
			setPersId(_personne.persId());

		}

		if (ACTIVER_CACHE) {
			personneCache = _personne;
		}
	}

	/**
	 * 
	 */
	public void cleanPersonneProprietaireCache() {
		personneCache = null;
	}

}
