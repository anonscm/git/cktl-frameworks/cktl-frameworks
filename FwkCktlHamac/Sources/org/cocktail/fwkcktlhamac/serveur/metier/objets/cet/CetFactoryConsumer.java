/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetCacheAnnuel;
import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Consommateur de la classe {@link CetFactory}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CetFactoryConsumer
		implements I_CetAffichage {

	private boolean shouldIgnorePeriodeOuverture;
	private NSTimestamp dateReference;
	private NSTimestamp dateDebutDemandeCet;
	private NSTimestamp dateFinDemandeCet;
	private EOPlanning eoPlanning;
	private boolean isAppVerifierStatutDemandeEpargneCet;
	private boolean isAppAutoriserDemandeEpargneCetCddNonCdi;
	private boolean isCetOptionAncienSystemeAuDela20Jours;
	private boolean isContractuel;
	private boolean isContractuelCDI;
	private int dureeJourMinute;
	private int seuilReliquatJourPourEpargneCet;
	private int soldeEnMinutes;
	private int plafondAnnuelEpargneCet;
	private int reliquatInitialEnMinutes;
	private float quotiteMoyenneJoursNonNuls;

	public CetFactoryConsumer(
			boolean shouldIgnorePeriodeOuverture, EOPlanning eoPlanning) {
		super();
		this.shouldIgnorePeriodeOuverture = shouldIgnorePeriodeOuverture;
		this.eoPlanning = eoPlanning;
		init();
	}

	private void init() {

		dateReference = DateCtrlHamac.getDateJour();

		// période par défaut 01/11 -> 31/12
		NSTimestamp date1erJan = DateCtrlHamac.date1erJanAnneeUniv(eoPlanning.toPeriode().perDFin());
		NSTimestamp date1erNov = date1erJan.timestampByAddingGregorianUnits(0, -2, 1, 0, 0, 0);
		NSTimestamp date31Dec = date1erJan.timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);

		dateDebutDemandeCet = HamacCktlConfig.dateForKey(
				EOParametre.DATE_DEBUT_DEMANDE_CET, eoPlanning.toPeriode(), date1erNov);

		dateFinDemandeCet = HamacCktlConfig.dateForKey(
				EOParametre.DATE_FIN_DEMANDE_CET, eoPlanning.toPeriode(), date31Dec);

		reliquatInitialEnMinutes = eoPlanning.getPlanning().getCalculDroitCongeDelegate().getReliquatInitialMinutes(null);

		quotiteMoyenneJoursNonNuls = eoPlanning.getPlanning().getQuotiteMoyenneJoursNonNuls();

		isAppVerifierStatutDemandeEpargneCet = HamacCktlConfig.booleanForKey(
				EOParametre.VERIFIER_STATUT_DEMANDE_EPARGNE_CET, false);

		isAppAutoriserDemandeEpargneCetCddNonCdi = HamacCktlConfig.booleanForKey(
				EOParametre.AUTORISER_DEMANDE_EPARGNE_CET_CDD_NON_CDI, false);

		isCetOptionAncienSystemeAuDela20Jours = HamacCktlConfig.booleanForKey(
				EOParametre.CET_OPTION_ANCIEN_SYSTEME_AU_DELA_20_JOURS, true);

		dureeJourMinute = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, getEoPlanning().toPeriode());

		seuilReliquatJourPourEpargneCet = HamacCktlConfig.intForKey(
				EOParametre.SEUIL_RELIQUAT_EPARGNE_CET, eoPlanning.toPeriode(), 0);

		plafondAnnuelEpargneCet = HamacCktlConfig.intForKey(
				EOParametre.PLAFOND_EPARGNE_CET, eoPlanning.toPeriode(), 25);

		soldeEnMinutes = eoPlanning.getCet().getSoldeTotal().intValue();

		isContractuel = GrhumFactory.isContractuel(eoPlanning);

		isContractuelCDI = GrhumFactory.isContractuelCDI(eoPlanning);

	}

	// pour accès direct par l'extérieur

	public final int getDureeJourMinute() {
		return dureeJourMinute;
	}

	public final int getSoldeEnMinutes() {
		return soldeEnMinutes;
	}

	public final int getReliquatInitialEnMinutes() {
		return reliquatInitialEnMinutes;
	}

	public final boolean isCetOptionAncienSystemeAuDela20Jours() {
		return isCetOptionAncienSystemeAuDela20Jours;
	}

	public final boolean isAppVerifierStatutDemandeEpargneCet() {
		return isAppVerifierStatutDemandeEpargneCet;
	}

	public final EOPlanning getEoPlanning() {
		return eoPlanning;
	}

	//

	/**
	 * Indique si la periode de saisie CET est ouverte
	 * 
	 * @return
	 */
	public boolean isPeriodeDemandeEpargneCet() {
		boolean isPeriodeDemandeEpargneCet = false;

		isPeriodeDemandeEpargneCet = CetFactory.isPeriodeDemandeEpargneCet(dateReference, dateDebutDemandeCet, dateFinDemandeCet);

		return isPeriodeDemandeEpargneCet;
	}

	/**
	 * Indique si l'épargne qui va être demandée conduit l'utilisateur a devoir
	 * faire une option (oui s'il dépasse le seuil de 20 jours en CET)
	 */
	public boolean isOptionPossible(int epargneEnJours) {
		boolean isOptionPossible = false;

		isOptionPossible = CetFactory.isOptionPossible(soldeEnMinutes, epargneEnJours, dureeJourMinute);

		return isOptionPossible;
	}

	/**
	 * La valeur minimum que l'agent doit mettre en CET en considérant que son
	 * épargne sera de epargneEnMinutes, et qu'il y aurait un transfert de CET de
	 * l'ancien vers le nouveau système.
	 * 
	 * L'agent doit conserver au minimum 20 jours en CET (épargne inclue)
	 */
	public int maintienObligatoireCetPourDemandeEpargneCetEnMinutes(int epargneEnJours) {
		int maintienObligatoire = 0;

		maintienObligatoire = CetFactory.maintienObligatoireCetPourDemandeEpargneCetEnMinutes(
				minutesRestantesInitialesRegimePerenne(), epargneEnJours, dureeJourMinute);

		return maintienObligatoire;
	}

	/**
	 * La valeur maximum de congés d'une épargne CET qui peuvent être maintenus
	 * dans le CET de l'agent, en considérant que son épargne sera
	 * epargneEnMinutes, en supplément d'un maintien fixe obligatoire de 20j. déjà
	 * appliqué.
	 * 
	 * A partir de 20 jours épargnés, la progression maximum est de 10 jours. Pour
	 * moins de 20, la progression maximum est de 20 jours, maximisé par un total
	 * épargné en CET de 30 jours.
	 * 
	 * @param epargneEnJours
	 *          : la valeur de l'épargne demandée
	 * @return
	 */
	public int maintienCetMaximumAuDela20JEnMinutes(int epargneEnJours) {
		int maintienCetMax = 0;

		maintienCetMax = CetFactory.maintienCetMaximumAuDela20JEnMinutes(
				minutesRestantesInitialesRegimePerenne(), epargneEnJours, dureeJourMinute);

		return maintienCetMax;
	}

	/**
	 * La valeur du dépassement de CET de l'agent s'il fait une épargne de
	 * epargneEnMinutes sur les 20 jours de maintien obligatoire.
	 * 
	 * @param epargneEnMinutes
	 * @return
	 */
	public int depassementSeuil20JoursPourEpargneCetEnJourEntier(
			int epargneEnMinutes) {
		int depassement = 0;

		depassement = CetFactory.depassementSeuil20JoursPourEpargneCetEnJourEntier(
				minutesRestantesInitialesRegimePerenne(), epargneEnMinutes, dureeJourMinute);

		return depassement;
	}

	/**
	 * Indique si le statut de l'agent lui permet de faire une demande d'épargne
	 * CET
	 * 
	 * @return
	 */
	public boolean isStatutAutorise() {
		boolean isStatutAutorise = false;

		isStatutAutorise = CetFactory.isStatutAutorise(
				isAppVerifierStatutDemandeEpargneCet,
				isAppAutoriserDemandeEpargneCetCddNonCdi,
				isContractuel,
				isContractuelCDI);

		return isStatutAutorise;
	}

	/**
	 * Indique si l'agent peut faire une demande d'épargne CET : - une demande
	 * d'épargne n'est pas encore faite - la période de saisie est ouverte - son
	 * statut est autorisé - il possède au moins 1 jour a 7h00 de reliquat initial
	 * 
	 * @param shouldIgnorePeriodeOuverture
	 *          TODO
	 * @return
	 */
	public boolean isEpargneFaisable() {
		boolean isEpargneFaisable = true;

		isEpargneFaisable = CetFactory.isEpargneFaisable(
				shouldIgnorePeriodeOuverture,
				dateReference,
				dateDebutDemandeCet,
				dateFinDemandeCet,
				eoPlanning,
				isAppVerifierStatutDemandeEpargneCet,
				isAppAutoriserDemandeEpargneCetCddNonCdi,
				isContractuel,
				isContractuelCDI,
				dureeJourMinute,
				seuilReliquatJourPourEpargneCet);

		return isEpargneFaisable;
	}

	/**
	 * Indique si l'agent peut décider parmi ses options - la période de saisie
	 * est ouverte - autorisé - il possède au moins 20 jours de solde avec ou sans
	 * l'épargne
	 * 
	 * @return
	 */
	public boolean isOptionFaisable(int epargneEnMinutes) {
		boolean isOptionFaisable = true;

		isOptionFaisable = CetFactory.isOptionFaisable(
				shouldIgnorePeriodeOuverture,
				dateReference,
				dateDebutDemandeCet,
				dateFinDemandeCet,
				isAppVerifierStatutDemandeEpargneCet,
				isAppAutoriserDemandeEpargneCetCddNonCdi,
				isContractuel, isContractuelCDI,
				minutesRestantesInitialesRegimePerenne(),
				epargneEnMinutes,
				dureeJourMinute);

		return isOptionFaisable;
	}

	/**
	 * Indique si aucune demande d'épargne CET n'a été faite (ni demande, ni
	 * décision)
	 */
	public boolean isEpargneCetDemandeNonFaite() {
		boolean isEpargneCetDemandeNonFaite = true;

		isEpargneCetDemandeNonFaite = CetFactory.isEpargneCetDemandeNonFaite(eoPlanning);

		return isEpargneCetDemandeNonFaite;
	}

	/**
	 * Indique si l'agent peut faire une demande de passage de son CET en régime
	 * pérénne : - demande de passage en CET pérenne non faite (peut être fait une
	 * seule et unique fois par agent) - période ouverte - possède un solde non
	 * vide en CET au 31/12/2008
	 */
	public boolean isDemandePassageRegimePerenneFaisable() {
		boolean isDemandePassageRegimePerenneFaisable = true;

		isDemandePassageRegimePerenneFaisable = CetFactory.isDemandePassageRegimePerenneFaisable(
				shouldIgnorePeriodeOuverture, dateReference, dateDebutDemandeCet, dateFinDemandeCet, eoPlanning);

		return isDemandePassageRegimePerenneFaisable;
	}

	/**
	 * Indique si l'agent possède encore du CET sur l'ancien régime du 31/12/2008
	 */
	public boolean isCET31122008Restant() {
		boolean isCET31122008Restant = true;

		isCET31122008Restant = CetFactory.isCET31122008Restant(eoPlanning);

		return isCET31122008Restant;
	}

	/**
	 * Indique si l'agent est autorisé à exercer son droit d'option sur l'ancien
	 * régime : uniquement si l'agent n'a jamais exercé de droit d'option sur son
	 * ancien régime
	 * 
	 * @return
	 */
	public boolean isDroitOptionAncienRegimeApresRenoncementPossible() {
		boolean isDroitOptionAncienRegimeApresRenoncementPossible = true;

		isDroitOptionAncienRegimeApresRenoncementPossible = CetFactory.isDroitOptionAncienRegimeApresRenoncementPossible(eoPlanning);

		return isDroitOptionAncienRegimeApresRenoncementPossible;
	}

	/**
	 * Donne le total de minutes restantes sur le CET ancien régime
	 */
	public int minutesRestantesAncienRegime() {
		int minutesRestantesAncienRegime = 0;

		minutesRestantesAncienRegime = CetFactory.minutesRestantesAncienRegime(eoPlanning);

		return minutesRestantesAncienRegime;
	}

	/**
	 * Donne le total de minutes restantes sur le CET perenne
	 */
	public int minutesRestantesRegimePerenne() {
		int minutesRestantesRegimePerenne = 0;

		minutesRestantesRegimePerenne = CetFactory.minutesRestantesRegimePerenne(eoPlanning, eoPlanning.toPeriode().perDDebut());

		return minutesRestantesRegimePerenne;
	}

	/**
	 * Donne le total de minutes restantes sur le CET perenne en fin de période
	 */
	public int minutesRestantesRegimePerenneFinPeriode() {
		int minutesRestantesRegimePerenne = 0;

		minutesRestantesRegimePerenne = CetFactory.minutesRestantesRegimePerenne(eoPlanning, eoPlanning.toPeriode().perDFin());

		return minutesRestantesRegimePerenne;
	}

	/**
	 * Donne le total de minutes restantes en début de période
	 */
	public int minutesRestantesInitialesRegimePerenne() {
		int minutesRestantesInitialesRegimePerenne = 0;

		minutesRestantesInitialesRegimePerenne = minutesRestantesRegimePerenne();

		return minutesRestantesInitialesRegimePerenne;
	}

	/**
	 * La valeur maximale, en jours, que l'agent peut demander en blocage CET.
	 * 
	 * La règle est (1 jour = xx:xx) : - ne pas dépasser le plafond maximum de 25
	 * j., déduit du droit à congés normal pour un agent de l'éduction national de
	 * 45 j., auquel on ôte les 20 j. d'épargne minimum pour accèder (paramètre de
	 * l'application PLAFOND_EPARGNE_CET_<ANNEE>)
	 * 
	 * Possible uniquement si lors du planning N-1, l'agent a pris au moins 20 j.
	 * sur ses congés annuels. Mais on ne bloque pas sur cette contrainte (cas des
	 * changement de service ou mutation ... ce sera un message affiché au
	 * demandeur et à l'administrateur qui prendra la décision qui convient) Ces
	 * 20 j. peuvent être paramétrés avec
	 * SEUIL_CONGES_CONSOMMES_JOUR_7H00_NM1_POUR_EPARGNE_CET_<ANNEE>
	 * 
	 * Valeurs à maximiser par le reliquats de congés (on n'épargne pas plus que
	 * ce que l'on a en reliquats) Valeurs à ponderer à la (aux) quotité(s)
	 * d'affectation au service
	 */
	public int reliquatPourBlocageCetMaxEnJours() {
		int reliquatPourBlocageCetMax = 0;

		reliquatPourBlocageCetMax = CetFactory.reliquatPourBlocageCetMaxEnJours(
				reliquatInitialEnMinutes,
				quotiteMoyenneJoursNonNuls,
				plafondAnnuelEpargneCet,
				dureeJourMinute);

		return reliquatPourBlocageCetMax;
	}

	/**
	 * Determiner si la personne connectée est autorisée a faire une demande de
	 * transfert en RAFP
	 */
	public boolean isPasAutoriseARafp() {
		boolean isPasAutoriseARafp = true;

		isPasAutoriseARafp = CetFactory.isPasAutoriseARafp(
				isAppVerifierStatutDemandeEpargneCet, isContractuel);

		return isPasAutoriseARafp;
	}

	// boolean pour interface GUI

	/**
	 * Indique si l'agent a exercicé son droit d'option sur l'ancien régime
	 */
	public boolean isExerciceDroitOptionAncienRegimeFait() {
		boolean isFait = false;

		isFait = CetFactory.isExerciceDroitOptionAncienRegimeFait(eoPlanning);

		return isFait;
	}

	/**
	 * Indique si l'agent a exercicé son droit d'option sur le régime pérènne
	 */
	public boolean isExerciceDroitOptionFait() {
		boolean isFait = false;

		isFait = CetFactory.isExerciceDroitOptionFait(eoPlanning);

		return isFait;
	}

	/**
	 * L'enregistrement {@link EOCetTransactionAnnuelle} s'il existe
	 * 
	 * @return
	 */
	public EOCetTransactionAnnuelle getEOCetTransactionAnnuelle() {
		EOCetTransactionAnnuelle transaction = null;

		transaction = CetFactory.getEOCetTransactionAnnuelle(eoPlanning, false);

		return transaction;
	}

	// simulation

	/**
	 * Donne le nombre de minutes de congés restantes après l'épargne demandée.
	 * TODO c'est pas très très propre la création puis suppression derriere ...
	 * 
	 * @param epargneEnMinutes
	 * @return
	 */
	public int congesRestantsApresEpargneEnMinutes(
			int epargneEnMinutes) {
		int minutesCongesRestantsApresEpargne = 0;

		minutesCongesRestantsApresEpargne = CetFactory.congesRestantsApresEpargneEnMinutes(
				epargneEnMinutes, eoPlanning);

		return minutesCongesRestantsApresEpargne;
	}

	// raz

	/**
	 * Effectue la suppression de l'ensemble des demandes CET liées au planning.
	 */
	public void doSupprimeDemande(EOEditingContext ec) {
		CetFactory.doSupprimeDemande(eoPlanning, ec);
	}

	/**
	 * Effectue la suppression des décisions liées au droit d'option
	 */
	public void doSupprimeDecisionDroitOption() {
		CetFactory.doSupprimeDecisionDroitOption(eoPlanning);
	}

	/**
	 * Effectue la suppression de l'ensemble des décisions CET liées au planning.
	 */
	public void doSupprimeDecision() {
		CetFactory.doSupprimeDecision(eoPlanning);
	}

	// demandes

	/**
	 * Enregistre une demande sur l'ancien régime CET d'un agent.
	 */
	public void doDemandeAncienRegime(
			int minutesTransfertRegimePerenne,
			int joursIndemnisation,
			int joursTransfertRafp,
			int minutesMaintienCet) {

		CetFactory.doDemandeAncienRegime(
				eoPlanning,
				minutesTransfertRegimePerenne,
				joursIndemnisation,
				joursTransfertRafp,
				minutesMaintienCet,
				dureeJourMinute);

	}

	/**
	 * Enregistre une demande sur le régime pérénne d'un agent. Cette demande doit
	 * mentionner : - la valeur de l'épargne (0 si pas d'épargne) - le contenu de
	 * l'exercice du droit d'option (dont 20j doivent être placés en maintien)
	 */
	public void doDemandeRegimePerenne(
			int joursEpargne,
			int joursMaintienCet,
			int minutesMaintienCetForce,
			int joursIndemnisation,
			int joursTransfertRafp) {

		CetFactory.doDemandeRegimePerenne(
				eoPlanning,
				joursEpargne,
				joursMaintienCet,
				minutesMaintienCetForce,
				joursIndemnisation,
				joursTransfertRafp,
				dureeJourMinute);

	}

	// les decisions

	/**
	 * Passage de l'état de demande vers l'état en attente de saisie d'une autre
	 * épargne par le gestionnaire.
	 */
	public boolean doSaisieDecisionAutreEpargne(int minutesDecisionEpargne) throws Exception {
		boolean isSaisieOK = false;

		isSaisieOK = CetFactory.doSaisieDecisionAutreEpargne(
				eoPlanning, reliquatInitialEnMinutes, minutesDecisionEpargne, dureeJourMinute, quotiteMoyenneJoursNonNuls, plafondAnnuelEpargneCet);

		return isSaisieOK;
	}

	/**
	 * Passage de l'état de épargne saisie vers droit d'option saisi par le
	 * gestionnaire.
	 * 
	 * @return <code>true</code> si pas d'erreur de validation.
	 */
	public boolean doSaisieDecisionAutreDroitOption(
			int minutesMaintienCet,
			int minutesIndemnisation,
			int minutesTransfertRafp) throws Exception {
		boolean isSaisieOK = false;

		isSaisieOK = CetFactory.doSaisieDecisionAutreDroitOption(
				eoPlanning, soldeEnMinutes, minutesMaintienCet, minutesIndemnisation, minutesTransfertRafp, dureeJourMinute);

		return isSaisieOK;
	}

	/**
	 * Prise de décision sur l'acceptation ou non à l'identique de la demande.
	 */
	public void setIsAdminDemandeAccepteeALIdentique(boolean isAcceptee) {
		CetFactory.setIsAdminDemandeAccepteeALIdentique(eoPlanning, isAcceptee);
	}

	// etat de validation

	/**
	 * Retourne l'etat de la demande CET
	 */
	public int etatDemande() {
		int etatDemande;

		etatDemande = CetFactory.etatDemande(eoPlanning);

		return etatDemande;
	}

	/**
	 * Indique peut passer à l'état donné en parametre
	 */
	public boolean isTransitionEtatAutorisee(int etatSuivant) {
		boolean isTransitionEtatAutorisee = false;

		isTransitionEtatAutorisee = CetFactory.isTransitionEtatAutorisee(eoPlanning, etatSuivant);

		return isTransitionEtatAutorisee;
	}

	/**
	 * Indique si la demande et la decision CET sont exactement les mêmes en terme
	 * de valeur
	 */
	public boolean isAdminDemandeAccepteeALIdentique() {
		boolean isAdminDemandeAccepteeALIdentique = false;

		isAdminDemandeAccepteeALIdentique = CetFactory.isAdminDemandeAccepteeALIdentique(eoPlanning);

		return isAdminDemandeAccepteeALIdentique;
	}

	/**
	 * Indique si {@link #isAdminDemandeAccepteeALIdentique()} ou
	 * {@link #isAdminDemandeAccepteeDecisionDifferente()}
	 */
	public boolean isAdminDemandeAcceptee() {
		boolean isAdminDemandeAcceptee = false;

		isAdminDemandeAcceptee = CetFactory.isAdminDemandeAcceptee(eoPlanning);

		return isAdminDemandeAcceptee;
	}

	// valeurs calculées

	/**
	 * le solde du CET de la demande après transfert, épargne et exercice du droit
	 * d'option
	 * 
	 * @return
	 */
	public int soldeApresDemandeTransfertEpargneEtExerciceDroitDOptionEnMinutes() {
		int solde = 0;

		solde = CetFactory.soldeApresDemandeTransfertEpargneEtExerciceDroitDOptionEnMinutes(
				minutesRestantesInitialesRegimePerenne(), eoPlanning);

		return solde;
	}

	/**
	 * le solde du CET de la decision après transfert, épargne et exercice du
	 * droit d'option
	 * 
	 * @return
	 */
	public int soldeApresDecisionTransfertEpargneEtExerciceDroitDOptionEnMinutes() {
		int solde = 0;

		solde = CetFactory.soldeApresDecisionTransfertEpargneEtExerciceDroitDOptionEnMinutes(
				minutesRestantesInitialesRegimePerenne(), eoPlanning);

		return solde;
	}

	/**
	 * Indique si le compte CET de cet agent n'est pas encore crédité
	 * 
	 * @return
	 */
	public boolean isCetExistant() {
		boolean isCetExistant = false;

		if (eoPlanning.getCet().getSoldeArray().count() > 0) {
			isCetExistant = true;
		}

		return isCetExistant;
	}

	/**
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetAffichage
	 */

	public Integer soldeAncienRegime() {
		return minutesRestantesAncienRegime();
	}

	public Integer soldeRegimePerenneDebutPeriode() {
		return minutesRestantesInitialesRegimePerenne();
	}

	public Integer soldeRegimePerenneFinPeriode() {
		return minutesRestantesRegimePerenneFinPeriode();
	}

	public Integer soldeAncienRegimeInitial() {
		return minutesRestantesAncienRegime();
	}

	public Integer soldeAncienRegimeFinal() {

		Integer soldeAncienRegimeFinal = null;

		if (getEOCetTransactionAnnuelle() != null) {
			soldeAncienRegimeFinal = getEOCetTransactionAnnuelle().cetDemMaintAncien();
		}

		return soldeAncienRegimeFinal;
	}

	public Integer reliquatInitial() {
		return getReliquatInitialEnMinutes();
	}

	public Integer soldeRegimePerenneInitial() {
		return minutesRestantesInitialesRegimePerenne();
	}

	public Integer soldeRegimePerenneFinal() {
		Integer soldeRegimePerenneFinPeriode = null;

		if (getEOCetTransactionAnnuelle() != null) {
			if (getEOCetTransactionAnnuelle().isAdminDemandeAcceptee()) {
				soldeRegimePerenneFinPeriode = soldeApresDecisionTransfertEpargneEtExerciceDroitDOptionEnMinutes();
			} else {
				soldeRegimePerenneFinPeriode = soldeApresDemandeTransfertEpargneEtExerciceDroitDOptionEnMinutes();
			}
		}

		return soldeRegimePerenneFinPeriode;
	}

	public EOPeriode toPeriode() {
		return eoPlanning.toPeriode();
	}

	/**
	 * Enregistrer dans la base de données les valeurs clés
	 */
	public void memoriser() {

		EOEditingContext ec = eoPlanning.editingContext();

		EOCetCacheAnnuel object = EOCetCacheAnnuel.getCetCacheAnnuel(ec, toPeriode(), eoPlanning.persId());

		if (object == null) {
			// création
			object = EOCetCacheAnnuel.createEOCetCacheAnnuel(
					ec, DateCtrlHamac.now(), DateCtrlHamac.now(), eoPlanning.persId(), toPeriode());
		}

		object.setSoldeAncien(soldeAncienRegime());
		object.setSoldePerenneDebutPeriode(soldeRegimePerenneDebutPeriode());
		object.setSoldePerenneFinPeriode(soldeRegimePerenneFinPeriode());
		object.setSoldeAncienInit(soldeAncienRegimeInitial());
		object.setSoldeAncienFinal(soldeAncienRegimeFinal());
		object.setReliquatInit(reliquatInitial());
		object.setSoldePerenneInit(soldeRegimePerenneInitial());
		object.setSoldePerenneFinal(soldeRegimePerenneFinal());

	}

	public NSTimestamp getDateFinDemandeCet() {
		return dateFinDemandeCet;
	}
}
