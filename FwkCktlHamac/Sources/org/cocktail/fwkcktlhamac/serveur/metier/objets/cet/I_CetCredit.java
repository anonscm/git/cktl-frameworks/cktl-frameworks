/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import com.webobjects.foundation.NSTimestamp;

/**
 * Motif d'une ligne de crédit CET
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_CetCredit {

	public final static String DATE_VALEUR_KEY = "dateValeur";
	public final static String CREDIT_KEY = "credit";
	public final static String DEBIT_KEY = "debit";
	public final static String IS_ANCIEN_REGIME_KEY = "isAncienRegime";

	public NSTimestamp dateValeur();

	public Integer credit();
	
	public Integer debit();

	public boolean isAncienRegime();

}
