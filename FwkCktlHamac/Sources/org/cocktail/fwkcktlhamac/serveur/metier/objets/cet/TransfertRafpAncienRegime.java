/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class TransfertRafpAncienRegime
		extends A_CetTransactionAnnuelleDebit {

	/**
	 * @param eoCetTransactionAnnuelle
	 */
	public TransfertRafpAncienRegime(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	@Override
	public String libelle() {
		return "Transfert CET (ancien régime) vers RAFP (au " + DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur()) + ")";
	}

}
