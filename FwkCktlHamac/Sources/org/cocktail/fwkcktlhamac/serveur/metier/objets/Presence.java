package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.common.CktlSort;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Structure interne pour gérer les vues rapides permettant de savoir si
 * l'agent est présence, absent, en congé ...
 * 
 */
public class Presence {

	private NSMutableDictionary<Jour, PresenceJour> tableauJoursDePresence;
	
	/**
	 * Noeud
	 * 
	 */
	public class PresenceJour
			extends ERXCustomObject {

		public final static String JOUR_KEY = "jour";
		public final static String AM_KEY = "am";
		public final static String PM_KEY = "pm";

		public Jour jour;
		public String am, pm;
		public Integer amDebut, amFin, pmDebut, pmFin, pauseDebut, pauseFin;

		public PresenceJour(Jour jour) {
			super();
			this.jour = jour;
			this.am = jour.getPresenceHoraireDemiJourneeAm();
			this.pm = jour.getPresenceHoraireDemiJourneePm();

			if (jour.getPlageTravailAm() != null) {
				amDebut = Integer.valueOf(jour.getPlageTravailAm().getDebutMinutesDansJour());
				amFin = Integer.valueOf(jour.getPlageTravailAm().getFinMinutesDansJour());
			}

			if (jour.getPlageTravailPm() != null) {
				pmDebut = Integer.valueOf(jour.getPlageTravailPm().getDebutMinutesDansJour());
				pmFin = Integer.valueOf(jour.getPlageTravailPm().getFinMinutesDansJour());
			}

			if (jour.getPlagePauseArray().count() > 0) {
				pauseDebut = Integer.valueOf(jour.getPlagePauseArray().lastObject().getDebutMinutesDansJour());
				pauseFin = Integer.valueOf(jour.getPlagePauseArray().lastObject().getFinMinutesDansJour());
			}

		}

	}

	public Presence(EOPlanning planning) {
		super();
		constuireDico(planning);
	}

	private void constuireDico(EOPlanning eoPlanning) {

		// le planning rapide est suffisant
		Planning planning = null;
		if (eoPlanning.isPlanningCompletCharge()) {
			planning = eoPlanning.getPlanning();
		} else {
			planning = new Planning(eoPlanning, true);
			planning.chargementRapide();
		}

		tableauJoursDePresence = initialiseTableauJoursDePresence(planning);

	}

	private NSMutableDictionary<Jour, PresenceJour> initialiseTableauJoursDePresence(Planning planning) {
		NSMutableDictionary<Jour, PresenceJour> tableau = new NSMutableDictionary<Jour, PresenceJour>();

		for (Mois mois : planning.getMoisArray()) {
			for (Semaine semaine : mois.getSemaines()) {
				for (Jour jour : semaine.getJours()) {
					PresenceJour pj = new PresenceJour(jour);
					tableau.setObjectForKey(pj, jour);
				}
			}
		}
		return tableau;
		
	}

	public NSArray<String> getPresenceArray() {
		return getPresenceArray(null, null);
	}

	public NSArray<String> getPresenceArray(NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		NSArray<Jour> jours = recupererJoursEntreDeuxDates(dateDebut, dateFin);
		NSMutableArray<String> presenceArray = construireTableauPresence(jours);
		
		return presenceArray.immutableClone();
	}

	private NSMutableArray<String> construireTableauPresence(NSArray<Jour> jours) {
		NSMutableArray<String> presenceArray = new NSMutableArray<String>();
		for (Jour key : jours) {
			presenceArray.addObject(tableauJoursDePresence.objectForKey(key).am + tableauJoursDePresence.objectForKey(key).pm);
		}
		return presenceArray;
	}

	private NSArray<Jour> recupererJoursEntreDeuxDates(
			NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		// clés classées chronologiquement
		NSArray<Jour> jours = tableauJoursDePresence.allKeys();
		
		// filtrer
		EOQualifier qual = null;

		if (dateDebut != null && dateFin != null) {

			// positionner les heures à minuit
			NSTimestamp dDeb = DateCtrlHamac.remonterToMinuit(dateDebut);
			NSTimestamp dFin = DateCtrlHamac.remonterToMinuit(dateFin);

			qual = ERXQ.between(
					Jour.DATE_KEY, dDeb, dFin, true);
		}

		jours = EOQualifier.filteredArrayWithQualifier(jours, qual);
		
		// classer
		jours = CktlSort.sortedArray(jours, Jour.DATE_KEY);
		
		return jours;
	}
}
