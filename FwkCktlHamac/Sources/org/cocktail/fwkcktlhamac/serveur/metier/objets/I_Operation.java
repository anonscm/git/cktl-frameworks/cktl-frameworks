/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

/**
 * Motif des objets pouvant entrainer un débit ou un crédit (absence, solde
 * négatif ...). Ce type d'objet est l'aggrégation d'une ou plusieurs
 * {@link Transaction}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_Operation {

	public final static String OPERATION_DELEGATE_KEY = "operationDelegate";

	/**
	 * la date de début / commencement de l'opération (uniquement pour un pseudo
	 * affichage chronologique)
	 */
	public final static String D_DEB = OPERATION_DELEGATE_KEY + "." + OperationDelegate.D_DEB_KEY;

	/** */
	public OperationDelegate getOperationDelegate();

}
