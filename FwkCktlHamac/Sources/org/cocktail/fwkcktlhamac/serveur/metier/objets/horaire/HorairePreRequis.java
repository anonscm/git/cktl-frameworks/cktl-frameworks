/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Pré-requis de durée pour les horaires, par service, par semaine
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HorairePreRequis
		extends ERXCustomObject {

	public final static String NUMERO_KEY = "numero";

	private NSMutableArray<HorairePreRequisPourService> parServiceArray;
	private NSMutableArray<String> semaineCodeArray;
	private int numero;

	/**
	 * 
	 */
	public HorairePreRequis(
			NSMutableArray<HorairePreRequisPourService> parServiceArray, int numero) {
		super();
		this.parServiceArray = parServiceArray;
		this.numero = numero;
		semaineCodeArray = new NSMutableArray<String>();
	}

	/**
	 * Générer une clé à partir de pre requis pour un service
	 * 
	 * @return
	 */
	public static String generateKey(NSArray<HorairePreRequisPourService> array) {
		String key = "";

		// classement par C_Structure
		array = CktlSort.sortedArray(array, HorairePreRequisPourService.C_STRUCTURE_KEY);

		for (HorairePreRequisPourService parService : array) {
			key += parService.getCStructure() + "_" + parService.getDureeHebdoMini() + "_" + parService.getDureeHebdoMaxi() + "-";
		}

		return key;
	}

	public final NSMutableArray<HorairePreRequisPourService> getParServiceArray() {
		return parServiceArray;
	}

	public final NSMutableArray<String> getSemaineCodeArray() {
		return semaineCodeArray;
	}

	/**
	 * Obtenir l'objet lié à un service
	 * 
	 * @param cStructure
	 * @return
	 */
	public final HorairePreRequisPourService getParService(String cStructure) {
		HorairePreRequisPourService result = null;

		int i = 0;
		while (result == null && i < getParServiceArray().count()) {
			if (getParServiceArray().objectAtIndex(i).getCStructure().equals(cStructure)) {
				result = getParServiceArray().objectAtIndex(i);
			} else {
				i++;
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "";

		for (HorairePreRequisPourService hprps : getParServiceArray()) {
			toString += "[" + hprps + "]";
			if (getParServiceArray().indexOfIdenticalObject(hprps) < getParServiceArray().count() - 1) {
				toString += ", ";
			}
		}

		toString += " ==> ";

		toString += getSemaineCodeArrayPourAffichage();

		return toString;
	}

	/**
	 * Constuire la liste des prerequis horaires pour un planning
	 * 
	 * @param planning
	 */
	public final static NSMutableDictionary<String, HorairePreRequis> constuireDicoPourPlanning(Planning planning) {

		NSMutableDictionary<String, HorairePreRequis> dico = new NSMutableDictionary<String, HorairePreRequis>();

		NSTimestamp dateDebutSemaine = null;

		for (Semaine semaine : planning.getSemaineArray()) {

			boolean isATraiter = false;

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(semaine.getPremierJour());

			if (semaine.isComplete() || (
					gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY && semaine.getSemaineSuivante() == null) ||
					semaine.getSemainePrecedente() == null ||
					semaine.getSemainePrecedente().getCode().equals(semaine.getCode())) {
				isATraiter = true;
			}

			NSTimestamp dateFinSemaine = semaine.getDernierJour();
			if (semaine.getSemainePrecedente() == null ||
					semaine.isComplete() || (
							gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY && semaine.getSemaineSuivante() == null) ||
					(semaine.getSemaineSuivante() != null && semaine.getCode().equals(semaine.getSemaineSuivante().getCode()))) {
				dateDebutSemaine = semaine.getPremierJour();
			}

			if (isATraiter) {

				NSArray<Jour> jourArray = planning.getJours(dateDebutSemaine, dateFinSemaine);

				// oter le dimanche
				jourArray = EOQualifier.filteredArrayWithQualifier(
						jourArray, ERXQ.isFalse(Jour.IS_DIMANCHE_KEY));

				// quotite moyenne (ignorer celles à 0)
				NSMutableDictionary<String, Integer> dicoNbJourAvecQuotite = new NSMutableDictionary<String, Integer>();
				NSMutableDictionary<String, Float> dicoCumulQuotite = new NSMutableDictionary<String, Float>();

				// init
				for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {
					dicoNbJourAvecQuotite.setObjectForKey(Integer.valueOf(0), eoStructure.cStructure());
					dicoCumulQuotite.setObjectForKey(new Float(0), eoStructure.cStructure());
				}

				// calcul du cumul
				for (Jour jour : jourArray) {

					for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

						String cStructure = eoStructure.cStructure();

						if (jour.quotite(cStructure) > (float) 0) {

							// cumul
							float cumul = jour.quotite(cStructure) + dicoCumulQuotite.objectForKey(cStructure).floatValue();
							dicoCumulQuotite.setObjectForKey(new Float(cumul), cStructure);

							// comptabiliser
							int count = dicoNbJourAvecQuotite.objectForKey(cStructure).intValue() + 1;
							dicoNbJourAvecQuotite.setObjectForKey(Integer.valueOf(count), cStructure);

						}

					}

				}

				NSMutableArray<HorairePreRequisPourService> preRequisPourServiceArray =
						new NSMutableArray<HorairePreRequisPourService>();

				// bornes par service
				NSMutableDictionary<String, NSMutableDictionary<String, Integer>> dicoSemaine =
						new NSMutableDictionary<String, NSMutableDictionary<String, Integer>>();

				for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

					String cStructure = eoStructure.cStructure();

					int count = dicoNbJourAvecQuotite.objectForKey(cStructure).intValue();

					float quotiteMoyenne = dicoCumulQuotite.objectForKey(cStructure).floatValue() / (float) count;

					if (quotiteMoyenne > (float) 0) {

						// prise en compte des TPA
						boolean isTpa = false;
						if (planning.getPlanningHamacApplicationUser().isTpa(planning.getEoPlanning().toPeriode())) {
							isTpa = true;
						}

						// pas de seuil pour les semaines incompletes ou pour les TPA
						int dureeHoraireHebdoMini = 0;
						if (!isTpa && count == 6) {
							dureeHoraireHebdoMini = (int) ((float) Horaire.DUREE_HORAIRE_HEBDO_MINI * quotiteMoyenne / (float) 100);
						}

						// plafond
						boolean isHorsNormes = planning.getPlanningHamacApplicationUser().isHorsNormes(
								planning.getEoPlanning().toPeriode());

						int dureeHoraireHebdoBaseMaxi = Horaire.DUREE_HORAIRE_HEBDO_MAXI;
						if (isHorsNormes) {
							dureeHoraireHebdoBaseMaxi = Horaire.DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES;
						}

						// pas de quotite pour le plafond pour les TPA
						int dureeHoraireHebdoMaxi = 0;
						if (isTpa) {
							dureeHoraireHebdoMaxi = (int) dureeHoraireHebdoBaseMaxi;
						} else {
							dureeHoraireHebdoMaxi = (int) ((float) dureeHoraireHebdoBaseMaxi * quotiteMoyenne / (float) 100);
						}

						NSMutableDictionary<String, Integer> dicoService = new NSMutableDictionary<String, Integer>();
						dicoService.setObjectForKey(Integer.valueOf((int) dureeHoraireHebdoMini), "min");
						dicoService.setObjectForKey(Integer.valueOf((int) dureeHoraireHebdoMaxi), "max");

						dicoSemaine.setObjectForKey(dicoService, cStructure);

						HorairePreRequisPourService parService = new
								HorairePreRequisPourService(
										eoStructure, dureeHoraireHebdoMini, dureeHoraireHebdoMaxi);
						preRequisPourServiceArray.addObject(parService);

					}

				}

				String cle = HorairePreRequis.generateKey(preRequisPourServiceArray);

				// XXX voir pourquoi c'est vide des fois
				if (!StringCtrl.isEmpty(cle)) {

					HorairePreRequis hpr = dico.objectForKey(cle);
					if (hpr == null) {
						// déterminer le numéro
						int numero = dico.allKeys().count() + 1;

						hpr = new HorairePreRequis(preRequisPourServiceArray, numero);
					}
					hpr.getSemaineCodeArray().addObject(semaine.getCode());
					dico.setObjectForKey(hpr, cle);

				}

			}

		}

		return dico;

	}

	/**
	 * Retrouve le {@link HorairePreRequis} d'un semaine a partir de son
	 * {@link Semaine#getCode()}
	 * 
	 * @param code
	 * @return
	 */
	public static HorairePreRequis findHorairePreRequisForSemaineCodeInDico(
			String code, NSMutableDictionary<String, HorairePreRequis> dico) {
		HorairePreRequis result = null;

		int i = 0;

		NSArray<String> keys = dico.allKeys();

		while (result == null && i < keys.count()) {
			HorairePreRequis hpr = dico.objectForKey(keys.objectAtIndex(i));
			if (hpr.getSemaineCodeArray().containsObject(code)) {
				result = hpr;
			} else {
				i++;
			}
		}

		return result;
	}

	/**
	 * La liste des semaines pour affichage
	 * 
	 * @return
	 */
	public String getSemaineCodeArrayPourAffichage() {
		String str = "Semaines concernées : ";

		// XXX passer par des objets Semaine ?
		for (String code : getSemaineCodeArray()) {

			// seule la semaine 35 peut être sur 2 années
			String codeAffichage = code.substring(5);

			if (codeAffichage.equals("35")) {
				codeAffichage += " (" + code.subSequence(0, 4) + ")";
			}

			str += codeAffichage;
			if (getSemaineCodeArray().indexOfIdenticalObject(code) < getSemaineCodeArray().count() - 1) {
				str += ", ";
			}
		}

		return str;
	}

	/**
	 * Déterminer si le planning remplis le pré-requis
	 * 
	 * @param planning
	 * @return
	 */
	public boolean isPreRequisSatisfait(Planning planning) {
		boolean isOk = false;

		int i = 0;

		while (i < planning.getHoraireArray().count() && !isOk) {

			Horaire horaire = planning.getHoraireArray().objectAtIndex(i);

			boolean isOkLocal = true;
			int j = 0;

			while (j < getParServiceArray().count() && isOkLocal) {

				HorairePreRequisPourService hprps = getParServiceArray().objectAtIndex(j);

				try {
					horaire.checkRepondAuPreRequisPourService(hprps);
				} catch (Exception e) {
					isOkLocal = false;
				}

				j++;

			}

			if (isOkLocal) {
				isOk = true;
			}

			i++;

		}

		return isOk;
	}

	public final int getNumero() {
		return numero;
	}
}
