/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * Segment associé une {@link PlageOccupation} (avec controles des horaires ...)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class PlageOccupationSegment
		extends A_Segment {

	private PlageOccupation plageOccupation;

	public PlageOccupationSegment(int debut, int fin, PlageOccupation plageOccupation) {
		super(debut, fin);
		this.plageOccupation = plageOccupation;

		// si c'est une absence, alors on controle que le segment y est bien inclus
		// dans la plage de travail prévu
		if (getPlageOccupation().getAbsence().isPresence() == false) {

			if (getDebut() < getPlageOccupation().getPlageTravail().getDebutMinutesDansJour() ||
					getDebut() > getPlageOccupation().getPlageTravail().getFinMinutesDansJour()) {
				throw new IllegalStateException("Le segment n'est pas strictement compris dans la plage de travail (début : " +
						TimeCtrl.stringForMinutes(getDebut()) + " PlageTravail : " + getPlageOccupation().getPlageTravail());
			}

			if (getFin() < getPlageOccupation().getPlageTravail().getDebutMinutesDansJour() ||
					getFin() > getPlageOccupation().getPlageTravail().getFinMinutesDansJour()) {
				throw new IllegalStateException("Le segment n'est pas strictement compris dans la plage de travail (fin :" +
						TimeCtrl.stringForMinutes(getFin()) + " PlageTravail : " + getPlageOccupation().getPlageTravail());
			}

		}

	}

	public Jour getJour() {
		return getPlageOccupation().getJour();
	}

	public final PlageOccupation getPlageOccupation() {
		return plageOccupation;
	}

}
