/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_ObjetPlanning
		extends ERXCustomObject {

	public final static String PREMIER_JOUR_KEY = "premierJour";
	public final static String DERNIER_JOUR_KEY = "dernierJour";
	public final static String NB_JOUR_OUVRABLE_KEY = "nbJourOuvrable";
	public final static String QUOTITE_KEY = "quotite";

	private NSTimestamp premierJour;
	private NSTimestamp dernierJour;
	private String libelle;
	private String libelleCourt;

	private Boolean _isCourant;

	protected A_ObjetPlanning(NSTimestamp unPremierJour) {
		super();
		premierJour = unPremierJour;
	}

	public final NSTimestamp getPremierJour() {
		return premierJour;
	}

	public final NSTimestamp getDernierJour() {
		return dernierJour;
	}

	protected final void setDernierJour(NSTimestamp dernierJour) {
		this.dernierJour = dernierJour;
	}

	public final String getLibelle() {
		return libelle;
	}

	protected final void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public final String getLibelleCourt() {
		return libelleCourt;
	}

	protected final void setlibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}

	/**
	 * Duréée associée en minutes sur un service
	 * 
	 * @param cStructure
	 * @return
	 */
	public abstract int dureeAssocieeMinutes(String cStructure);

	/**
	 * Duréée comptabilisée en minutes sur un service (tenant compte répartition
	 * de la pause en cas d'horaires multi-services)
	 * 
	 * @param cStructure
	 * @return
	 */
	public abstract int dureeComptabiliseeMinutes(String cStructure);

	/**
	 * Duréée comptabilisée en minutes sur un service (tenant compte répartition
	 * de la pause en cas d'horaires multi-services). Ne prendre en compte que les
	 * jours dans la période passée en paramètre.
	 * 
	 * @param cStructure
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public abstract int dureeComptabiliseeMinutes(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin);

	/**
	 * Quotite attendue sur un service
	 * 
	 * @return
	 */
	public abstract float quotite(String cStructure);

	/**
	 * La durée de travail effectué depuis le début de l'année
	 * 
	 * @return
	 */
	public abstract int dureeRealiseeMinutes(String cStructure);

	// TYPE AFFICHAGE

	public boolean isTypeAffichageJour() {
		return getPlanning().getTypeAffichage().equals(Planning.TYPE_AFFICHAGE_JOUR);
	}

	public boolean isTypeAffichageHeure() {
		return getPlanning().getTypeAffichage().equals(Planning.TYPE_AFFICHAGE_HEURE);
	}

	// DUREE ASSOCIEE

	/**
	 * Minutes associées independemment du service d'affectation (somme de toutes
	 * les quotités des objets contenus dans le table
	 * {@link #getTravailAttendus()})
	 */
	public final int dureeAssocieeMinutes() {
		return dureeAssocieeMinutes(null);
	}

	/**
	 * Minutes associées en minuts sur {@link #getCStructureCourante()}
	 */
	public final int dureeAssocieeMinutesStructureCourante() {
		return dureeAssocieeMinutes(getPlanning().getCStructureCourante());
	}

	// DUREE COMPTABILISEE

	/**
	 * Minutes comptabilisees independemment du service d'affectation (somme de
	 * toutes les quotités des objets contenus dans le table
	 * {@link #getTravailAttendus()})
	 */
	public final int dureeComptabiliseeMinutes() {
		return dureeComptabiliseeMinutes(null);
	}

	/**
	 * Minutes associées en minuts sur {@link #getCStructureCourante()}
	 */
	public final int dureeComptabiliseeMinutesStructureCourante() {
		return dureeComptabiliseeMinutes(getPlanning().getCStructureCourante());
	}

	// QUOTITE

	/**
	 * La quotité totale (somme de toutes les quotités des objets contenus dans le
	 * table {@link #getTravailAttendus()})
	 * 
	 * @return
	 */
	public final float quotite() {
		return quotite(null);
	}

	// DUREE REALISEE

	/**
	 * Minutes realisée independemment du service d'affectation (somme de toutes
	 * les quotités des objets contenus dans le table
	 * {@link #getTravailAttendus()})
	 */
	public final int dureeRealiseeMinutes() {
		return dureeRealiseeMinutes(null);
	}

	/**
	 * Minutes realisées en minuts sur {@link #getCStructureCourante()}
	 */
	public final int dureeRealiseeMinutesStructureCourante() {
		return dureeRealiseeMinutes(getPlanning().getCStructureCourante());
	}

	/**
	 * Raz des objets en cache. A surcharger si nécéssaire
	 */
	public void clearCache() {
		_isCourant = null;
	}

	/**
	 * Nombre de jours ouvrés pour le service
	 * 
	 * @return
	 */
	public abstract int nbJourOuvre(String cStructure);

	/**
	 * Nombre de jours ouvrés indépedemmant de quelconque service
	 * 
	 * @return
	 */
	public abstract int nbJourOuvre();

	/**
	 * Nombre de jours ouvrés indépedemmant de quelconque service. Ne prendre en
	 * compte que les jours dans la période passée en paramètre.
	 * 
	 * @return
	 */
	public abstract int nbJourOuvre(NSTimestamp dateDebut, NSTimestamp dateFin);

	/**
	 * Nombre de jours ouvrables indépedemmant de quelconque service
	 * 
	 * @return
	 */
	public abstract int nbJourOuvrable();

	/**
	 * Nombre de jours attendus pour un service (pour gestion des calculs en
	 * trentieme)
	 * 
	 * @return
	 */
	@Deprecated
	public abstract int nbJourAttendu(String cStructure);

	/**
	 * Nombre de jours attendus pour un TravailAttendu (pour gestion des calculs
	 * en trentieme)
	 * 
	 * @return
	 */
	public abstract int nbJourAttendu(TravailAttendu travailAttendu);

	// bindings des interfaces

	public abstract Planning getPlanning();

	/**
	 * Déterminer si l'objet est inclus la date d'aujourd'hui
	 */
	public boolean isCourant() {
		if (_isCourant == null) {
			NSTimestamp nowMinuit = DateCtrlHamac.remonterToMinuit(DateCtrlHamac.now());
			if (DateCtrlHamac.isBeforeEq(getPremierJour(), nowMinuit) &&
					DateCtrlHamac.isAfterEq(getDernierJour(), nowMinuit)) {
				_isCourant = Boolean.TRUE;
			} else {
				_isCourant = Boolean.FALSE;
			}
		}
		return _isCourant.booleanValue();
	}

	// autres

	/**
	 * pour empecher les exceptions si tentative d'enregistrement en base de
	 * données
	 * 
	 * @see com.webobjects.eocontrol.EOCustomObject#classDescription()
	 */
	@Override
	public EOClassDescription classDescription() {
		return null;
	}
}
