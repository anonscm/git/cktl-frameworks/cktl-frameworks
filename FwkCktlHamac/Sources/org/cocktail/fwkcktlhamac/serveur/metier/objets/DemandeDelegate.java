/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.List;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.A_FwkCktlHamacRecord;
import org.cocktail.fwkcktlhamac.serveur.metier.EODroit;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * La classe concrète pour la gestion des demandes de validation
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class DemandeDelegate {

	public final static String DEMANDEUR_KEY = "demandeur";

	private I_Demande demande;

	public DemandeDelegate(I_Demande demande) {
		super();
		this.demande = demande;
	}

	/**
	 * la demande associée
	 */
	public I_Demande getDemande() {
		return demande;
	}

	/**
	 * @return
	 */
	public IPersonne getDemandeur() {
		IPersonne demandeur = null;

		demandeur = getDemande().toPersonne();

		return demandeur;
	}

	/**
	 * @return
	 */
	public String libelle() {
		String libelle = "";

		if (getEoOccupation() != null) {

			if (getEoOccupation().toTypeStatut().isStatutSupprime() ||
					getEoOccupation().toTypeStatut().isStatutEnCoursDeSuppression() ||
					getEoOccupation().toTypeStatut().isStatutEnCoursDeSuppressionVise()) {
				libelle += "suppression ";
			}

			libelle += "occupation";
		} else {
			libelle += "planning";
		}

		libelle += " ";
		libelle += getDemande().toString();

		return libelle;
	}

	/**
	 * @return
	 */
	public EOOccupation getEoOccupation() {
		EOOccupation eoOccupation = null;

		if (getDemande() instanceof EOOccupation) {
			eoOccupation = (EOOccupation) getDemande();
		}

		return eoOccupation;
	}

	/**
	 * @return
	 */
	public EOPlanning getEoPlanning() {
		EOPlanning eoPlanning = null;

		if (getDemande() instanceof EOPlanning) {
			eoPlanning = (EOPlanning) getDemande();
		}

		return eoPlanning;
	}

	/**
	 * Les emails ne sont pas envoyé pour le planning de test / prévisionnel
	 * 
	 * @return
	 */
	private final boolean isEmailAEnvoyer() {
		boolean isEmailAEnvoyer = true;

		if (getEoOccupation() != null) {
			if (getEoOccupation().nature().equals(EOPlanning.TEST) ||
					getEoOccupation().toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
				isEmailAEnvoyer = false;
			}
		} else if (getEoPlanning() != null) {
			if (getEoPlanning().isTest()) {
				isEmailAEnvoyer = false;
			}
		}

		return isEmailAEnvoyer;
	}

	/**
	 * @return
	 */
	public boolean isDemandeParDelegation() {
		boolean isDemandeParDelegation = false;

		if (getDemande().tosDelegationHistorique().count() > 0) {
			isDemandeParDelegation = true;
		}

		return isDemandeParDelegation;
	}

	/**
	 * L'enregistrement <code>IPersonne</code> de la personne deleguee qui a
	 * effectue la demande
	 * 
	 * @return
	 */
	public IPersonne personneDelegue() {
		IPersonne personneDelegue = null;

		if (isDemandeParDelegation()) {
			personneDelegue = getDemande().tosDelegationHistorique().lastObject().toPersonne();
		}

		return personneDelegue;
	}

	private NSArray<EOStructure> _eoStructureAFaireValiderArray;

	/**
	 * filtrer ici la liste des demandes selon la structure (par exemple, ne
	 * prendre que le service impacté par une absence en cas de multiservice)
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getEoStructureAFaireValiderArray() {
		if (_eoStructureAFaireValiderArray == null) {
			if (getEoPlanning() != null) {
				// pour un planning : tous les services
				_eoStructureAFaireValiderArray = getEoPlanning().getEoStructureAttenduArray(null, true);
			} else {
				// occupation : selon le positionnement
				_eoStructureAFaireValiderArray = getEoOccupation().getEoStructureAFaireValiderArrayPourDemande();
			}

		}
		return _eoStructureAFaireValiderArray;
	}

	// XXX tmp pour filtrage sur les services

	private String _eoStructureArrayAFaireValiderChaine;

	public String getEoStructureArrayAFaireValiderChaine() {
		if (_eoStructureArrayAFaireValiderChaine == null) {
			_eoStructureArrayAFaireValiderChaine = ";";
			for (EOStructure eoStructure : getEoStructureAFaireValiderArray()) {
				_eoStructureArrayAFaireValiderChaine += eoStructure.cStructure();
				_eoStructureArrayAFaireValiderChaine += ";";
			}
		}
		return _eoStructureArrayAFaireValiderChaine;
	}

	private NSArray<Acreditation> _acreditationArray;

	/**
	 * @return
	 */
	public NSArray<Acreditation> getAcreditationArray() {
		if (_acreditationArray == null) {

			EOEditingContext ec = getDemande().editingContext();
			Integer persIdCible = getDemande().persId();

			_acreditationArray = EODroit.getAcreditationArrayPourCible(
					ec, persIdCible, getEoStructureAFaireValiderArray());

		}
		return _acreditationArray;
	}

	private NSArray<EOPeriode> _eoPeriodeArray;

	/**
	 * @return
	 */
	public NSArray<EOPeriode> getEoPeriodeArray() {
		if (_eoPeriodeArray == null) {

			if (getEoOccupation() != null) {

				// occupation
				_eoPeriodeArray = EOPeriode.findSortedPeriodeArray(
						getEoOccupation().editingContext(), getEoOccupation().dateDebut(), getEoOccupation().dateFin());

			} else {

				// planning
				_eoPeriodeArray = new NSArray<EOPeriode>(getEoPlanning().toPeriode());

			}

		}
		return _eoPeriodeArray;
	}

	/**
	 * Raccourci
	 * 
	 * @param session
	 * @param eoVisa
	 * @throws NSValidation.ValidationException
	 */
	public void envoyerMail(CktlWebSession session, EOVisa eoVisa)
			throws NSValidation.ValidationException {

		if (isEmailAEnvoyer()) {
			envoyerMail(
					(CktlWebApplication) session.application(),
					session.context(),
					session.mailBus(),
					session.dataBus(),
					session.defaultEditingContext(),
					eoVisa);
		}

	}

	/**
	 * notification
	 * 
	 * @param session
	 * @param eoVisa
	 */
	public void envoyerMail(
			CktlWebApplication app,
			WOContext context,
			CktlMailBus mailBus,
			CktlDataBus dataBus,
			EOEditingContext ec,
			EOVisa eoVisa)
			throws NSValidation.ValidationException {

		NSArray<NSMutableArray<IPersonne>> responsablesArray = findPersonneNotifVisaValidPourEnvoiEmail(ec);

		NSMutableArray<IPersonne> personneNotificationArray = responsablesArray.objectAtIndex(0);
		NSMutableArray<IPersonne> personneVisaArray = responsablesArray.objectAtIndex(1);
		NSMutableArray<IPersonne> personneValidationArray = responsablesArray.objectAtIndex(2);

		// mettre un message d'erreur s'il n'y a aucun valideur
		if (personneValidationArray.count() == 0) {
			throw new NSValidation.ValidationException(
					"Attention, aucun responsable \"valideur\" n'a été detecté pour envoyer un email. " +
							"Veuillez en informer verbalement votre responsable pour régulariser la situation.");
		}

		MailCenter mc = new MailCenter(app, context, dataBus, mailBus, ec, this, eoVisa);

		if (eoVisa != null) {
			mc.sendMailAlerteTraitee(
					personneValidationArray, personneVisaArray, personneNotificationArray);
		} else {

			mc.sendMailsNouvelleAlerte(
					personneValidationArray, personneVisaArray, personneNotificationArray, getDemandeur());
		}

	}

	/**
	 * Extraction de la liste des personnes liées à la demande afin d'envoyer les
	 * email. Tant que la liste des valideurs est vide, on va remonter dans la
	 * hiérarchie jusqu'à ce qu'on trouve quelqu'un
	 * 
	 * - 1er element : liste des notifications
	 * 
	 * - 2eme element : liste des viseurs
	 * 
	 * - 3eme element : liste des valideurs
	 * 
	 * @param ec
	 */
	public final NSArray<NSMutableArray<IPersonne>> findPersonneNotifVisaValidPourEnvoiEmail(EOEditingContext ec) {

		boolean isTrouve = false;

		NSMutableArray<IPersonne> personneValidationArray = null;
		NSArray<NSMutableArray<IPersonne>> responsablesArray = null;
		EOTypeSequenceValidation eoTypeSequenceValidation = getDemande().toTypeSequenceValidation();

		while (!isTrouve) {
			responsablesArray = findPersonneNotifVisaValidPourEnvoiEmail(ec, eoTypeSequenceValidation);
			personneValidationArray = responsablesArray.objectAtIndex(2);
			if (personneValidationArray.count() > 0) {
				isTrouve = true;
			} else {
				if (!eoTypeSequenceValidation.tsvCode().equals(eoTypeSequenceValidation.getEoTypeSequenceValidationSuperieur().tsvCode())) {
					eoTypeSequenceValidation = eoTypeSequenceValidation.getEoTypeSequenceValidationSuperieur();
				} else {
					// on trouvera pas plus loin, on arrete les frais
					isTrouve = true;
				}
			}
		}

		return responsablesArray;

	}

	/**
	 * Extraction de la liste des personnes liées à la demande afin d'envoyer les
	 * email. Refactoré pour gérer plusieurs niveau de séquence de validation
	 * 
	 * - 1er element : liste des notifications
	 * 
	 * - 2eme element : liste des viseurs
	 * 
	 * - 3eme element : liste des valideurs
	 * 
	 * @param ec
	 */
	private final NSArray<NSMutableArray<IPersonne>> findPersonneNotifVisaValidPourEnvoiEmail(
			EOEditingContext ec, EOTypeSequenceValidation eoTypeSequenceValidation) {

		// XXX tmp pour l'envoi des mails
		NSMutableArray<IPersonne> personneVisaArray = new NSMutableArray<IPersonne>();
		NSMutableArray<IPersonne> personneNotificationArray = new NSMutableArray<IPersonne>();
		NSMutableArray<IPersonne> personneValidationArray = new NSMutableArray<IPersonne>();

		for (EOSequenceValidation eoSequenceValidation : eoTypeSequenceValidation.tosSequenceValidation()) {

			if (eoSequenceValidation.temEnvoiMail().equals(A_FwkCktlHamacRecord.OUI)) {

				NSArray<Acreditation> acreditationPourSequence = EOQualifier.filteredArrayWithQualifier(
						getAcreditationArray(), ERXQ.equals(Acreditation.EO_TYPE_NIVEAU_DROIT_KEY, eoSequenceValidation.toTypeNiveauDroit()));

				for (Acreditation acreditation : acreditationPourSequence) {

					// ne pas envoyer le mail à ceux qui n'en veulent pas ...
					HamacApplicationUser destUi = new HamacApplicationUser(
							ec, acreditation.getEoIndividu().persId());

					if (!destUi.isNonReceptionMailPourToutesStructuresEtIndividu(getEoStructureAFaireValiderArray(), getDemandeur().persId())) {

						// XXX tmp faire générique
						if (eoSequenceValidation.toTypePrivilege().tprCode().equals(EOTypePrivilege.CODE_VALIDATION)) {
							if (!personneValidationArray.containsObject(acreditation.getEoIndividu())) {
								personneValidationArray.addObject(acreditation.getEoIndividu());
							}
						} else if (eoSequenceValidation.toTypePrivilege().tprCode().equals(EOTypePrivilege.CODE_VISA)) {
							if (!personneVisaArray.containsObject(acreditation.getEoIndividu())) {
								personneVisaArray.addObject(acreditation.getEoIndividu());
							}
						} else if (eoSequenceValidation.toTypePrivilege().tprCode().equals(EOTypePrivilege.CODE_NOTIFICATION)) {
							if (!personneNotificationArray.containsObject(acreditation.getEoIndividu())) {
								personneNotificationArray.addObject(acreditation.getEoIndividu());
							}
						}

					}

				}

			}

		}

		NSMutableArray<NSMutableArray<IPersonne>> result = new NSMutableArray<NSMutableArray<IPersonne>>();

		result.add(personneNotificationArray);
		result.add(personneVisaArray);
		result.add(personneValidationArray);

		return result.immutableClone();

	}

	/**
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @param persIdViseur
	 * @param decision
	 */
	public void doViser(EOStructure eoStructure, EOTypeVisa eoTypeVisa, Number persIdViseur, String decision) {

		//
		setDecisionPourStructureEtTypeVisa(eoStructure, eoTypeVisa, persIdViseur, decision);

	}

	/**
	 * TODO Indique si l'action peut être effectuée
	 * 
	 * @param eoStructureApplication
	 * @param eoTypeVisa
	 * @param persIdViseur
	 * @param decisionON
	 * @param eoTypeStatutSuivant
	 * @return
	 * @throws NSValidation.ValidationException
	 */
	public boolean isVisaAutorise(
			EOStructure eoStructureApplication,
			EOTypeVisa eoTypeVisa,
			Integer persIdViseur,
			String decisionON,
			EOTypeStatut eoTypeStatutSuivant)
			throws NSValidation.ValidationException {

		// tester droit
		if (!isAutoriseAVoirDemande(persIdViseur)) {

			EOIndividu eoIndividuViseur = EOIndividu.individuWithPersId(eoStructureApplication.editingContext(), persIdViseur);

			String errMessage = eoIndividuViseur.getNomCompletAffichage() + ", " +
					"vous n'avez pas de droits suffisants pour effectuer l'operation suivante : " +
					(decisionON.equals(A_FwkCktlHamacRecord.OUI) ? "accepter" : "refuser") + " " +
					(eoTypeVisa.isNiveauVisa() ? "le visa" : "la validation") + " sur le planning de " +
					demande.toPersonne().getNomCompletAffichage() + " (service d'application : " +
					eoStructureApplication.lcStructure() + ")";

			throw new NSValidation.ValidationException(errMessage);
		}

		// tester si toujours en attente de validation
		if (!isEnAttenteDeValidation()) {

			String errMessage = "L'opération ne peut pas aboutir car la demande n'est pas (ou plus) en attente de validation";

			throw new NSValidation.ValidationException(errMessage);

		}

		// tester si l'opération n'a pas déjà été réalisée
		EOVisa eoVisaExistant = getVisaPourStructureEtTypeVisa(eoStructureApplication, eoTypeVisa);
		if (eoVisaExistant != null &&
				eoVisaExistant.isDefinitif()) {

			String errMessage = "L'opération ne peut pas aboutir car cette décision a déjà été prise, " +
					"soit par vous même ou soit par un autre responsable";

			throw new NSValidation.ValidationException(errMessage);

		}

		// tester si le statut suivant est possible
		boolean isTransitionPossible = false;
		NSArray<EOTypeStatut> eoTypeStatutPossibleArray = getEoTypeStatutPossibleArray(eoTypeVisa, eoStructureApplication, decisionON);
		if (eoTypeStatutPossibleArray.count() > 0) {
			NSArray<String> codeStatutPossibleArray = (NSArray<String>) eoTypeStatutPossibleArray.valueForKey(EOTypeStatut.STA_CODE_KEY);
			if (codeStatutPossibleArray.containsObject(eoTypeStatutSuivant.staCode())) {
				isTransitionPossible = true;
			}
		}

		// XXX traiter le cas multi validation
		if (!isTransitionPossible) {

			String errMessage = "L'opération ne peut pas aboutir car le statut suivant de la demande" +
					" est interdit (ne peut être \"" + eoTypeStatutSuivant.staLibelle() + "\" suite à une opération de type \"" + eoTypeVisa.vtypLibelle() + "\")";

			throw new NSValidation.ValidationException(errMessage);

		}

		return true;
	}

	/**
	 * Indique si la demande est toujours en attente de validation
	 * 
	 * @return
	 */
	private boolean isEnAttenteDeValidation() {
		boolean isEnAttenteDeValidation = false;

		NSArray<I_Demande> array = new NSArray<I_Demande>(getDemande());
		array = EOQualifier.filteredArrayWithQualifier(array, I_Demande.QUAL_EN_ATTENTE_VALIDATION);

		if (array.count() > 0) {

			isEnAttenteDeValidation = true;

			// XXX a mettre dans le qualifier
			// masquer les occupations en cours de validation prévisionnelle
			if (getEoOccupation() != null &&
					getEoOccupation().toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
				isEnAttenteDeValidation = false;
			}

		}

		return isEnAttenteDeValidation;
	}

	/**
	 * RAZ de la décision
	 * 
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @return
	 */
	public boolean razVisa(EOStructure eoStructure, EOTypeVisa eoTypeVisa) {

		EOEditingContext ec = eoStructure.editingContext();

		// EOVisa eoVisa = EOVisa.getVisaPourStructureEtTypeVisa(
		// ec, eoStructure, eoTypeVisa, getDemande());

		EOVisa eoVisa = getEoVisa(eoStructure, eoTypeVisa);

		if (eoVisa != null) {
			ec.deleteObject(eoVisa);
			sauvegarder(ec);
			return true;
		}

		return false;
	}

	/**
	 * Application définitive du visa
	 * 
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @param eoTypeStatutSuivant
	 *          TODO
	 * @return
	 */
	public EOVisa appliquerVisa(
			EOStructure eoStructure, EOTypeVisa eoTypeVisa, EOTypeStatut eoTypeStatutSuivant) {

		EOEditingContext ec = eoStructure.editingContext();

		EOVisa eoVisa = getEoVisa(eoStructure, eoTypeVisa);

		if (eoVisa != null) {

			// XXX vérifier que le visa de toutes les autres structures à bien été
			// appliqué !
			eoVisa.setTemDefinitif(A_FwkCktlHamacRecord.OUI);

			//
			getDemande().setToTypeStatutRelationship(eoTypeStatutSuivant);

			sauvegarder(ec);

			return eoVisa;
		}

		return null;
	}

	/**
	 * @param eoStructureViseur
	 *          TODO
	 * @param eoVisa
	 */
	public NSArray<EOTypeStatut> getEoTypeStatutPossibleArray(EOTypeVisa eoTypeVisa, EOStructure eoStructureViseur, String decision) {

		NSArray<EOTypeStatut> eoTypeStatutArray = getDemande().toTypeStatut().getEoTypeStatutArrayPourVisa(
				this, eoTypeVisa, eoStructureViseur, decision);
		//
		// for (EOTypeStatut eoTypeStatut : eoTypeStatutArray) {
		// System.out.print(eoTypeStatut.staLibelle() + "\t");
		// }
		// System.out.print("\n");

		return eoTypeStatutArray;

	}

	/**
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @return
	 */
	public EOVisa getVisaPourStructureEtTypeVisa(EOStructure eoStructure, EOTypeVisa eoTypeVisa) {
		EOVisa eoVisa = null;

		// eoVisa = EOVisa.getVisaPourStructureEtTypeVisa(
		// eoStructure.editingContext(), eoStructure, eoTypeVisa, getDemande());

		eoVisa = getEoVisa(eoStructure, eoTypeVisa);

		return eoVisa;
	}

	/**
	 * Obtenir une entrée en cache, l'instancier à défaut
	 * 
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @return
	 */
	private EOVisa getEoVisa(EOStructure eoStructure, EOTypeVisa eoTypeVisa) {

		EOVisa eoVisa = null;

		EOQualifier qual = ERXQ.and(
				ERXQ.equals(EOVisa.TO_TYPE_VISA_KEY, eoTypeVisa),
				ERXQ.equals(EOVisa.TO_STRUCTURE_AYANT_VISE_KEY, eoStructure),
				ERXQ.isNull(EOVisa.D_HISTORISATION_KEY));

		NSArray<EOVisa> eoVisaArray = null;

		if (getEoOccupation() != null) {
			eoVisaArray = getEoOccupation().tosVisa(qual);
		} else {
			eoVisaArray = getEoPlanning().tosVisa(qual);
		}

		if (!NSArrayCtrl.isEmpty(eoVisaArray)) {
			eoVisa = eoVisaArray.lastObject();
		}

		return eoVisa;
	}

	/**
	 * 
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @return
	 */
	public String getDecisionPourStructureEtTypeVisa(EOStructure eoStructure, EOTypeVisa eoTypeVisa) {
		String decision = null;

		EOVisa eoVisa = getVisaPourStructureEtTypeVisa(eoStructure, eoTypeVisa);

		if (eoVisa != null) {
			decision = eoVisa.visVisaAccepte();
		}

		return decision;
	}

	/**
	 * Enregistrer la ligne de visa.
	 * 
	 * 
	 * @param eoStructure
	 * @param eoTypeVisa
	 * @param persIdViseur
	 * @param decision
	 * @return
	 */
	private void setDecisionPourStructureEtTypeVisa(
			EOStructure eoStructure, EOTypeVisa eoTypeVisa, Number persIdViseur, String decision) {

		EOEditingContext ec = eoStructure.editingContext();

		EOVisa eoVisaExistant = getVisaPourStructureEtTypeVisa(eoStructure, eoTypeVisa);

		if (eoVisaExistant == null) {
			EOVisa eoVisa = EOVisa.createEOVisa(
					ec, Integer.valueOf(persIdViseur.intValue()), eoStructure, eoTypeVisa);

			eoVisa.setToOccupationRelationship(getEoOccupation());
			eoVisa.setToPlanningRelationship(getEoPlanning());
			eoVisaExistant = eoVisa;
		}

		eoVisaExistant.setVisVisaAccepte(decision);
		sauvegarder(ec);

		// forcer les to-many vers le viseur à se rafraichir
		ec.invalidateObjectsWithGlobalIDs(new
				NSArray<EOGlobalID>(eoVisaExistant.globalID()));
	}

	/**
	 * XXX a factoriser
	 * 
	 * @param ec
	 */
	private void sauvegarder(EOEditingContext ec) {
		try {
			ec.lock();
			ec.saveChanges();
		} catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		} finally {
			ec.unlock();
		}
	}

	/**
	 * Ben le nom parle de lui meme
	 * 
	 * @return
	 */
	public boolean isConnectedUserPersIdAutoriseAVoirDemande(HamacApplicationUser user) {
		return isAutoriseAVoirDemande(user.getPersId(), user.isSuperAdmin());
	}

	/**
	 * Ben le nom parle de lui meme
	 * 
	 * @return
	 */
	public boolean isAutoriseAVoirDemande(Integer persIdToCheck, boolean... isSuperAdmin) {

		boolean isAutorise = false;

		// accès interdit à ses propres demandes
		if (persIdToCheck.intValue() == getDemande().persId().intValue()) {
			// sauf si administrateur et que les paramètres l'y autorise
			if (isSuperAdmin.length > 0 &&
					isSuperAdmin[0] == true &&
					HamacCktlConfig.booleanForKey(EOParametre.ADMIN_VOIR_DEMANDES_PERSONNELLES)) {
			} else {
				return false;
			}
		}

		// cas particulier pour l'admin, pour gagner du temps d'accès, l'accès est
		// automatiquement autorisé
		if (isSuperAdmin.length > 0 &&
				isSuperAdmin[0] == true) {
			isAutorise = true;
		}

		if (isAutorise == false) {

			// long tDebut2 = System.currentTimeMillis();

			// les acreditation pour cette personne
			EOQualifier qual = ERXQ.equals(Acreditation.EO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY, persIdToCheck);
			NSArray<Acreditation> acreditationArray = EOQualifier.filteredArrayWithQualifier(getAcreditationArray(), qual);

			int i = 0;
			while (!isAutorise && i < acreditationArray.count()) {

				EOTypeNiveauDroit eoTypeNiveauDroit = acreditationArray.objectAtIndex(i).getEoTypeNiveauDroit();

				NSArray<EOSequenceValidation> eoSequenceValidationArray = getDemande().toTypeSequenceValidation().tosSequenceValidation();

				int j = 0;

				while (!isAutorise && j < eoSequenceValidationArray.count()) {

					EOSequenceValidation eoSequenceValidation = eoSequenceValidationArray.objectAtIndex(j);

					if (eoTypeNiveauDroit.tndCode().equals(eoSequenceValidation.toTypeNiveauDroit().tndCode())) {
						isAutorise = true;
					}

					j++;
				}

				i++;
			}

			// CktlLog.log("isAutoriseAVoirDemande[" + (System.currentTimeMillis() -
			// tDebut2) + "ms] " + demande.getDemandeDelegate().libelle());

		}

		return isAutorise;
	}

	/**
	 * "Je crois que c'est clair" © Serge July
	 * 
	 * @param user
	 *          le user qui veut viser
	 * @param eoTypeVisa
	 *          niveau de visa (visa ou validation)
	 * @param eoStructureRepresentant
	 *          !!! XXX a prendre en compte !!! au nom de quel service le user
	 *          vise
	 * @return
	 */
	public boolean isConnectedUserPersIdAutoriseAViserDemande(
			HamacApplicationUser user, EOTypeVisa eoTypeVisa, EOStructure eoStructureRepresentant) {
		boolean isAutorise = false;

		// accès interdit à ses propres demandes
		if (user.getPersId().intValue() == getDemande().persId().intValue()) {
			// sauf si administrateur et que les paramètres l'y autorise
			if (user.isSuperAdmin() &&
					HamacCktlConfig.booleanForKey(EOParametre.ADMIN_VOIR_DEMANDES_PERSONNELLES)) {

			} else {
				return false;
			}
		}

		// cas particulier pour l'admin, pour gagner du temps de calcul, l'accès est
		// automatiquement autorisé
		if (user.isSuperAdmin()) {
			isAutorise = true;
		}
		
		if (isAutorise == false) {
			
			// les acreditation pour cette personne
			EOQualifier qual = ERXQ.equals(Acreditation.EO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY, user.getPersId());
			NSArray<Acreditation> acreditationArray = EOQualifier.filteredArrayWithQualifier(getAcreditationArray(), qual);

			int i = 0;
			while (!isAutorise && i < acreditationArray.count()) {

				Acreditation acreditation = acreditationArray.objectAtIndex(i);

				EOTypeNiveauDroit eoTypeNiveauDroit = acreditation.getEoTypeNiveauDroit();

				NSArray<EOSequenceValidation> eoSequenceValidationArray = getDemande().toTypeSequenceValidation().tosSequenceValidation();

				int j = 0;

				while (!isAutorise && j < eoSequenceValidationArray.count()) {

					EOSequenceValidation eoSequenceValidation = eoSequenceValidationArray.objectAtIndex(j);

					if (eoTypeNiveauDroit.tndCode().equals(eoSequenceValidation.toTypeNiveauDroit().tndCode()) &&
							eoTypeVisa.getEoTypePrivilege().tprCode().equals(eoSequenceValidation.toTypePrivilege().tprCode()) &&
							(acreditation.getEoStructurePlanning() == null || acreditation.getEoStructurePlanning() == eoStructureRepresentant)) {
						isAutorise = true;
					}

					j++;
				}

				i++;
			}

		}
		
		// cas particulier pour le fonctionnel RH pour les congés RH sous réserve -> si visa non donné, validation non autorisée
		if (user.isSuperFonctionnel()) {
			if (eoTypeVisa.isNiveauValidation() && this.getDemande() instanceof EOOccupation && ((EOOccupation) this.getDemande()).toTypeOccupation().isRHSousReserve()) {
				List<EOVisa> visas = this.getDemande().tosVisa();
				boolean isVisaAccepteEtDefinitif = false;
				for (EOVisa eoVisa : visas) {
					if (eoVisa.toTypeVisa().isNiveauVisa()) {
						 isVisaAccepteEtDefinitif = eoVisa.isAccepte() && eoVisa.isDefinitif();
					}
				}
				if (!isVisaAccepteEtDefinitif) {
					isAutorise = false;
				}
			}
		}

		return isAutorise;
	}
	
	/**
	 * Indique si la date d'effet est passée ou non
	 * 
	 * @return
	 */
	public final boolean isCommenceeOuPassee() {
		boolean isCommenceeOuPassee = false;

		if (DateCtrl.isBeforeEq(getDemande().dateEffective(), DateCtrlHamac.now())) {
			isCommenceeOuPassee = true;
		}

		return isCommenceeOuPassee;
	}

	/**
	 * TODO mettre en paramètre
	 * 
	 * Indique si la date d'effet est passée depuis 2 semaines
	 * 
	 * @return
	 */
	public final boolean isSuperEnRetard() {
		boolean isSuperEnRetard = false;

		if (DateCtrl.isBeforeEq(getDemande().dateEffective(), DateCtrlHamac.now().timestampByAddingGregorianUnits(0, 0, -14, 0, 0, 0))) {
			isSuperEnRetard = true;
		}

		return isSuperEnRetard;
	}

	/**
	 * Liste des demandes en attente selon un profil sur une période
	 * 
	 * Retourne 2 tableaux : la liste des demandes et la liste des structures
	 * associées. 1er element : liste des demandes, 2eme element : liste des
	 * structures
	 * 
	 * @param ec
	 * @param eoPeriode
	 * @param user
	 *          : mettre <code>null</code> pour avoir toutes les demandes
	 * @return
	 */
	public final static NSArray<Object> getDemandeEtStructureArray(
			EOEditingContext ec, EOPeriode eoPeriode, HamacApplicationUser user) {

		// long tDebut = System.currentTimeMillis();

		NSMutableArray<I_Demande> demandeArray = new NSMutableArray<I_Demande>();

		NSMutableArray<EOStructure> eoStructureArray = new NSMutableArray<EOStructure>();

		NSArray<EOOccupation> eoOccupationArray = EOOccupation.fetchAll(
					ec, I_Demande.QUAL_EN_ATTENTE_VALIDATION);

		for (EOOccupation eoOccupation : eoOccupationArray) {

			// XXX a mettre dans le qualifier
			// masquer les occupations en cours de validation prévisionnelle
			if (!eoOccupation.toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
				demandeArray.addObject(eoOccupation);
			}

		}

		// celles liées à des plannings
		NSArray<EOPlanning> eoPlanningArray = EOPlanning.fetchAll(
					ec, I_Demande.QUAL_EN_ATTENTE_VALIDATION);

		for (EOPlanning eoPlanning : eoPlanningArray) {
			demandeArray.addObject(eoPlanning);
		}

		// filtrer sur la période
		if (eoPeriode != null) {

			NSMutableArray<I_Demande> demandeFiltreArray = new NSMutableArray<I_Demande>();

			for (I_Demande demande : demandeArray) {

				if (demande.getDemandeDelegate().getEoPeriodeArray().containsObject(eoPeriode)) {
					demandeFiltreArray.addObject(demande);
				}

			}

			demandeArray = new NSMutableArray<I_Demande>(demandeFiltreArray);

		}

		// filtrer sur les droits de la personne connectée
		NSMutableArray<I_Demande> demandeFiltreArray = new NSMutableArray<I_Demande>();

		for (I_Demande demande : demandeArray) {

			if (user == null ||
					demande.getDemandeDelegate().isConnectedUserPersIdAutoriseAVoirDemande(user)) {
				demandeFiltreArray.addObject(demande);

				// alimenter la liste des structures pour les filtres
				for (EOStructure eoStructure : demande.getDemandeDelegate().getEoStructureAFaireValiderArray()) {
					if (!eoStructureArray.containsObject(eoStructure)) {
						eoStructureArray.addObject(eoStructure);
					}
				}

			}

		}

		eoStructureArray = new NSMutableArray<EOStructure>(EOSortOrdering.sortedArrayUsingKeyOrderArray(
					eoStructureArray, CktlSort.newSort(EOStructure.LC_STRUCTURE_KEY)));

		demandeArray = new NSMutableArray<I_Demande>(demandeFiltreArray);

		// CktlLog.log("recherche demandes : " + (System.currentTimeMillis() -
		// tDebut) + "ms");

		NSArray<Object> array = new NSArray<Object>(new Object[] {
				demandeArray, eoStructureArray });
		return array;
	}

	/**
	 * Dictionnaire demande par responsable et niveau
	 * 
	 * @param ec
	 * @param demandeArray
	 * @return
	 */
	public final static NSMutableDictionary<IPersonne, NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>>> getDico(
			EOEditingContext ec, NSArray<I_Demande> demandeArray) {

		// responsable -> demande / niveau de droit
		NSMutableDictionary<IPersonne, NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>>> dicoGlobal =
				new NSMutableDictionary<IPersonne, NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>>>();

		//
		NSArray<EOTypeVisa> eoTypeVisaArray = EOTypeVisa.fetchAll(ec);

		for (I_Demande demande : demandeArray) {

			//
			DemandeDelegate dlg = demande.getDemandeDelegate();

			if (dlg.isSuperEnRetard()) {

				NSArray<NSMutableArray<IPersonne>> responsablesArray = dlg.findPersonneNotifVisaValidPourEnvoiEmail(ec);

				NSMutableArray<IPersonne> personneVisaArray = responsablesArray.objectAtIndex(1);
				NSMutableArray<IPersonne> personneValidationArray = responsablesArray.objectAtIndex(2);

				NSMutableArray<IPersonne> responsableArray = new NSMutableArray<IPersonne>(personneVisaArray.arrayByAddingObjectsFromArray(personneValidationArray));

				for (IPersonne responsable : responsableArray) {

					NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>> dicoDemande = dicoGlobal.get(responsable);

					HamacApplicationUser user = new HamacApplicationUser(ec, responsable.persId());

					if (dicoDemande == null) {
						dicoDemande = new NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>>();
					}

					NSArray<EOStructure> eoStructureAFaireValiderArray = dlg.getEoStructureAFaireValiderArray();

					for (EOStructure eoStructureAFaireValider : eoStructureAFaireValiderArray) {

						NSMutableDictionary<EOStructure, EOTypeVisa> dicoStructure = dicoDemande.objectForKey(eoStructureAFaireValider);
						if (dicoStructure == null) {
							dicoStructure = new NSMutableDictionary<EOStructure, EOTypeVisa>();
						}

						for (EOTypeVisa eoTypeVisa : eoTypeVisaArray) {

							if (dlg.isConnectedUserPersIdAutoriseAViserDemande(user, eoTypeVisa, eoStructureAFaireValider)) {
								EOVisa eoVisa = dlg.getVisaPourStructureEtTypeVisa(eoStructureAFaireValider, eoTypeVisa);

								if (eoVisa == null ||
										!eoVisa.isDefinitif()) {
									dicoStructure.setObjectForKey(eoTypeVisa, eoStructureAFaireValider);
								}

							}

						}

						if (dicoStructure.size() > 0) {
							dicoDemande.setObjectForKey(dicoStructure, demande);
							dicoGlobal.setObjectForKey(dicoDemande, responsable);
						}

					}

				}

			}

		}

		return dicoGlobal;

	}
}
