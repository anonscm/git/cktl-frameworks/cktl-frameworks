/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Définition simple d'un droit (un individu pour un niveau de droit, et limité
 * à un service optionnellement)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Acreditation
		extends ERXCustomObject {

	public final static String EO_TYPE_NIVEAU_DROIT_KEY = "eoTypeNiveauDroit";
	public final static String EO_INDIVIDU_KEY = "eoIndividu";
	public final static String EO_STRUCTURE_PLANNING = "eoStructurePlanning";
	// XXX a prendre en compte dans la vérification des droits !!
	public final static String EO_PERIODE_KEY = "eoPeriode";

	
	public final static EOQualifier QUAL_DROIT_VALIDATION = ERXQ.or(
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_VISEUR),
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_VALIDEUR),
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_CHEF_DE_SERVICE),
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_FCT_LOCAL),
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_FCT_GLOBAL),
			ERXQ.equals(EO_TYPE_NIVEAU_DROIT_KEY + "." + EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_ADM));
	
	private EOIndividu eoIndividu;
	private EOTypeNiveauDroit eoTypeNiveauDroit;
	private IPersonne personneCible;
	private EOStructure eoStructurePlanning;
	private EOPeriode eoPeriode;
	private boolean isHeritage;

	/**
	 * @param eoIndividu
	 * @param eoTypeNiveauDroit
	 * @param personneCible
	 * @param eoStructurePlanning
	 *          facultatif
	 * @param eoPeriode
	 *          TODO
	 * @param isHeritage
	 *          TODO
	 */
	public Acreditation(
			EOIndividu eoIndividu,
			EOTypeNiveauDroit eoTypeNiveauDroit,
			IPersonne personneCible,
			EOStructure eoStructurePlanning,
			EOPeriode eoPeriode,
			boolean isHeritage) {
		super();
		this.eoIndividu = eoIndividu;
		this.eoTypeNiveauDroit = eoTypeNiveauDroit;
		this.personneCible = personneCible;
		this.eoStructurePlanning = eoStructurePlanning;
		this.eoPeriode = eoPeriode;
		this.isHeritage = isHeritage;
	}

	/**
	 * Individu titulaire de l'acréditation
	 * 
	 * @return
	 */
	public final EOIndividu getEoIndividu() {
		return eoIndividu;
	}

	/**
	 * @return
	 */
	public final EOTypeNiveauDroit getEoTypeNiveauDroit() {
		return eoTypeNiveauDroit;
	}

	/**
	 * @return
	 */
	public final IPersonne getPersonneCible() {
		return personneCible;
	}

	/**
	 * @return
	 */
	public final EOStructure getEoStructurePlanning() {
		return eoStructurePlanning;
	}

	/**
	 * @return
	 */
	public final EOPeriode getEoPeriode() {
		return eoPeriode;
	}

	@Override
	public String toString() {
		String toString = "";

		toString += getEoIndividu().getNomPrenomAffichage();
		toString += " ";
		toString += getEoTypeNiveauDroit().tndLibelle();

		if (getPersonneCible() != null) {
			toString += " sur ";
			toString += getPersonneCible().getNomPrenomAffichage();
		}

		if (getEoStructurePlanning() != null) {
			toString += " pour le planning concernant le service " + getEoStructurePlanning().lcStructure();
		} else {
			toString += " indépendamment du service du planning";
		}

		if (getEoPeriode() != null) {
			toString += " sur la période " + getEoPeriode().perLibelle();
		}

		return toString;
	}

	public final boolean isHeritage() {
		return isHeritage;
	}
	
	public final boolean estMemeCible(Acreditation acreditation) {
		if (this.getEoIndividu().equals(acreditation.getEoIndividu())) {
			if (this.getPersonneCible().equals(acreditation.getPersonneCible())) {
				return true;
			}
		}
		return false;
	}
	
}
