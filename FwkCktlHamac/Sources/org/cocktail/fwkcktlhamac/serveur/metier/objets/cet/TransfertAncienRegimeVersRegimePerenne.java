/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeDelegate;

import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class TransfertAncienRegimeVersRegimePerenne
		extends A_CetTransactionAnnuelleDebit
		implements I_Solde {

	/**
	 * @param eoCetTransactionAnnuelle
	 */
	public TransfertAncienRegimeVersRegimePerenne(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	private SoldeDelegate _soldeDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#getSoldeDelegate()
	 */
	public SoldeDelegate getSoldeDelegate() {
		if (_soldeDelegate == null) {
			_soldeDelegate = new SoldeDelegate(this);
		}
		return _soldeDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#clearSoldeDelegate
	 * ()
	 */
	public void clearCache() {
		_soldeDelegate = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#setRestant(java
	 * .lang.Integer)
	 */
	public void setRestant(Integer restant) {
		// TODO Auto-generated method stub

	}

	@Override
	public String libelle() {
		return "Transfert CET \"ancien régime\" vers CET \"régime pérenne\"";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCongeNatif()
	 */
	public boolean isCongeNatif() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isReliquatNatif()
	 */
	public boolean isReliquatNatif() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isBalance()
	 */
	public boolean isBalance() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isRegularisation()
	 */
	public boolean isRegularisation() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCongeLegal()
	 */
	public boolean isCongeLegal() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCongeRh()
	 */
	public boolean isCongeRh() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isDechargeSyndicale
	 * ()
	 */
	public boolean isDechargeSyndicale() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isJrti()
	 */
	public boolean isJrti() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCet()
	 */
	public boolean isCet() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isReliquatManuel()
	 */
	public boolean isReliquatManuel() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCongeManuel()
	 */
	public boolean isCongeManuel() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isIndemCet20081136
	 * ()
	 */
	public boolean isIndemCet20081136() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isRecuperation()
	 */
	public boolean isRecuperation() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#
	 * isHeureSupplementaireManuel()
	 */
	public boolean isHeureSupplementaireManuel() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#
	 * isHeureSupplementaireNonValide()
	 */
	public boolean isHeureSupplementaireNonValide() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#dDeb()
	 */
	public NSTimestamp dDeb() {
		NSTimestamp dDeb = null;

		// XXX date mise à J-1 pour la faire débiter en premier
		dDeb = getEoCetTransactionAnnuelle().dateValeur().timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);

		return dDeb;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#dFin()
	 */
	public NSTimestamp dFin() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#cStructure()
	 */
	public String cStructure() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isReliquatHsup() {
		// TODO Auto-generated method stub
		return false;
	}

}
