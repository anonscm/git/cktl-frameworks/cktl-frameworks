/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

/**
 * La classe de gestion des segment nécéssaire à la répartition du temps de
 * pause pour les horaires multiservices, lorsque celle ci est tout ou partie
 * recouverte par une absence, et que la pause est toujours accordée
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class ServiceSegment extends A_Segment {

	private String cStructure;
	private Jour jour;

	/**
	 * @param debut
	 * @param fin
	 */
	public ServiceSegment(int debut, int fin, String cStructure, Jour jour) {
		super(debut, fin);
		this.cStructure = cStructure;
		this.jour = jour;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Segment#getJour()
	 */
	@Override
	public Jour getJour() {
		return jour;
	}

	public final String getCStructure() {
		return cStructure;
	}

}
