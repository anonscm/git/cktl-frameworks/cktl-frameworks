/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPersonnePrivilege;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeCalcul;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVacanceScolaire;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.A_PlanningCalculDroitCongeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.CalculDroitCongeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.CongeLegalDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.Epargne;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.BonificationHoraireDecale;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.BonificationHoraireSamediApresMidi;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.BonificationHoraireSamediMatin;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HoraireJournalier;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePreRequis;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.A_MalusPourCongeLegal;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.DroitCongeInitial;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.MalusPourCongeLegalRegle90180;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.MalusPourCongeLegalRegleRTT;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.SaisieManuelleSoldeInitial;
import org.cocktail.fwkcktlhamac.serveur.stat.CongesPayesContainer;
import org.cocktail.fwkcktlhamac.serveur.stat.HeuresSupplementairesContainer;
import org.cocktail.fwkcktlhamac.serveur.stat.ValorisationCETContainer;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Un planning
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Planning
		extends A_ObjetPlanning {

	// le eoPlanning
	private EOPlanning eoPlanning;

	// le eoPlanning des associations horaires
	private EOPlanning eoPlanningAssoHoraire;

	// // la classe de gestion du droit a congés
	// private A_PlanningCalculDroitCongeDelegate
	// _planningCalculDroitCongeDelegate;
	// XXX tmp dev multi calcul
	private CalculDroitCongeDelegate _calculDroitCongeDelegate;

	// XXX tmp gestion du changement de mode de calcul par l'interface
	private Map<EOPeriode, EOTypeCalcul> mapPeriodeTypeCalcul = new HashMap<EOPeriode, EOTypeCalcul>();

	// cache

	//
	private NSMutableArray<Mois> _moisArray;
	//
	private NSArray<Semaine> _semaineArray;
	//
	private NSArray<Jour> _jourArray;
	//
	private NSMutableArray<Horaire> _horaireArray;
	//
	private NSArray<TravailAttendu> _travailAttenduArray;
	//
	private NSMutableArray<EOStructure> _eoStructureAttenduArray;
	//
	private NSArray<A_Absence> _absenceArraySorted;
	//
	private NSArray<A_Absence> _absenceArrayFicheRoseEtBalance;
	//
	private EOTypeOccupation _eoTypeOccupationCongeAnnuel;
	//
	private NSMutableDictionary<String, HorairePreRequis> _dicoHorairePreRequis;

	// fiche rose

	private NSMutableArray<I_LigneFicheRose> ligneFicheRoseArray;

	/**
	 * The constructeur d'un objet complet
	 * 
	 * @param eoPlanning
	 */
	public Planning(EOPlanning eoPlanning) {
		super(eoPlanning.toPeriode().perDDebut());

		this.eoPlanning = eoPlanning;

		retrieveEoPlanningAssoHoraire();

		clearCache();
	}

	/**
	 * XXX tmp dev planning de service
	 * 
	 * @param eoPlanning
	 */
	public Planning(EOPlanning eoPlanning, boolean temp) {
		super(eoPlanning.toPeriode().perDDebut());
		this.eoPlanning = eoPlanning;
		retrieveEoPlanningAssoHoraire();
	}

	/**
	 * A utiliser pour la création d'un planning "anonyme" : colonne du tableau de
	 * planning de service par exemple
	 */
	public Planning(EOPeriode eoPeriode) {
		super(eoPeriode.perDDebut());

		_moisArray = PlanningFactory.construireCalendrier(
				eoPeriode.perDDebut(),
				eoPeriode.perDFin(),
				eoPeriode.getDateJourFerieArray(),
				null,
				this,
				null);
	}

	/**
	 * Recherche le eoPlanning des associations horaires
	 * 
	 * XXX à n'autoriser que pour les 100% pour l'instant ...
	 * 
	 * XXX ignorer les horaires locaux du service hérité
	 */
	private void retrieveEoPlanningAssoHoraire() {

		// eoPlanningAssoHoraire =
		// EOHeritageAssociationHoraire.retrieveProprietaireHeritageAssociationHoraire(this);

		if (eoPlanningAssoHoraire == null) {
			eoPlanningAssoHoraire = getEoPlanning();
		}

	}

	/**
	 * Chargement complet du planning.
	 * 
	 * Doit être appelé systématiquement après {@link #Planning(EOPlanning)}
	 */
	public final void charger() {

		// CktlLog.log("Demande charger() (id=" + hashCode() + ")");

		// déterminer s'il faut charger le planning précédent
		if (isPlanningPrecedentACharger()) {
			// doResetCache();
			// recharger
			EOPlanning eoPlanning = getEoPlanning().getEoPlanningPrecedent();
			if (eoPlanning != null) {
				eoPlanning.getPlanning();
			}
		}

		setLibelle(getEoPlanning().toString());

		long tDebut = System.currentTimeMillis();
		chargerPlanning();
		CktlLog.log("TOTAL chargerPlanning() [" + getEoPlanning().nature() + "] " + getLibelle() + " : " + (System.currentTimeMillis() - tDebut) + "ms");

		// TODO mettre en valeur par défaut les préférences de l'utilisateur
		setTypeAffichage(HamacCktlConfig.stringForKey(EOParametre.UNITE));

	}

	/**
	 * Chargement rapide du planning (pas de calcul, etc ...)
	 */
	public final void chargementRapide() {

		setLibelle(getEoPlanning().toPersonne().getNomPrenomAffichage() + " " + getEoPlanning().toPeriode().perLibelle());

		long tDebut = System.currentTimeMillis();
		prechargerPlanning();
		CktlLog.log("TOTAL chargementRapide() " + getLibelle() + " : " + (System.currentTimeMillis() - tDebut) + "ms");

		// TODO mettre en valeur par défaut les préférences de l'utilisateur
		setTypeAffichage(HamacCktlConfig.stringForKey(EOParametre.UNITE));

	}

	/**
	 * Indique si le planning précédent doit être chargé :
	 * 
	 * - reliquat automatique mais pas de solde à congé sur N-1
	 * 
	 * - ...
	 * 
	 * @return
	 */
	private boolean isPlanningPrecedentACharger() {
		boolean isPlanningPrecedentACharger = false;

		// uniquement s'il y a un planning précédent
		if (getEoPlanning().getEoPlanningPrecedent() != null) {

			// il faut que chaque solde natif existe et qu'il y ait du restant
			for (int i = 0; i < getEoStructureAttenduArray().count() && !isPlanningPrecedentACharger; i++) {

				EOStructure eoStructureAttendu = getEoStructureAttenduArray().objectAtIndex(i);

				EOSolde eoSolde = getEoPlanning().getEoSoldeCongeNatifDernier(
						eoStructureAttendu.cStructure());
				if (eoSolde == null ||
						eoSolde.restant() == null) {
					isPlanningPrecedentACharger = true;
				}

			}

			// toutes les occupations doivent avoir une valeur de durée calculée
			if (isPlanningPrecedentACharger == false) {
				// TODO
			}

		}

		return isPlanningPrecedentACharger;
	}

	/**
	 * Association des horaires au planning et aux sous objets liés
	 */
	private void associerHoraires() {

		// liste des horaires
		NSMutableArray<EOHoraire> eoHoraireArray = new NSMutableArray<EOHoraire>();

		// ceux du planning + planinng pere
		eoHoraireArray.addObjectsFromArray(getEoPlanningAssoHoraire().tosHoraire());

		// // a virer ?
		// {
		// for (EOPlanning pere :
		// getEoPlanningAssoHoraire().getEoPlanningPereArray()) {
		// eoHoraireArray.addObjectsFromArray(pere.tosHoraire());
		// }
		// }

		// ceux associés (a virer ?)
		for (EOAssociationHoraire asso : getEoPlanningAssoHoraire().tosAssociationHoraire()) {
			EOHoraire hor = asso.toHoraire();
			if (eoHoraireArray.contains(hor) == false) {
				eoHoraireArray.add(hor);
			}
		}

		// TODO tmp pour test : tous les horaires (décommenter en haut après test)
		{
			eoHoraireArray.addObjectsFromArray(getEoPlanningAssoHoraire().tosHoraire());
			EOQualifier qual = ERXQ.equals(EOHoraire.PERS_ID_KEY,
					getEoPlanning().persId());
			eoHoraireArray.addObjectsFromArray(
					EOHoraire.fetchAll(getEoPlanning().editingContext(), qual));
		}

		// dégager les doublons (à virer ?)
		eoHoraireArray = new NSMutableArray<EOHoraire>(NSArrayCtrl.removeDuplicate(eoHoraireArray));

		//
		for (EOHoraire eoHoraire : eoHoraireArray) {
			Horaire horaire = new Horaire(eoHoraire, getEoPlanning().toPeriode());
			addHoraire(horaire);
		}

		// affectation des horaires aux semaines et aux jours
		for (Mois moisItem : getMoisArray()) {

			for (Semaine semaineItem : moisItem.getSemaines()) {
				semaineItem.associerHoraire(getEoPlanningAssoHoraire());
			}

		}

	}

	/**
	 * Construire le planning mois / jours / semaine, charger les occauptions et
	 * affectation des horaires
	 */
	private void prechargerPlanning() {

		// String prefix = "";// getProprietaire().proId() + " ";

		//
		// long tDebut = System.currentTimeMillis();
		EOPeriode eoPeriode = getEoPlanning().toPeriode();
		_moisArray = PlanningFactory.construireCalendrier(
				eoPeriode.perDDebut(),
				eoPeriode.perDFin(),
				eoPeriode.getDateJourFerieArray(),
				getTravailAttenduArray(),
				this,
				EOVacanceScolaire.fetchAll(getEoPlanning().editingContext()));
		// CktlLog.log(prefix + "construireCalendrier() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		// le chargement des occupations est préalable s'il faut modifier des
		// horaires a postériori (horaires forcés par exemple)
		// tDebut = System.currentTimeMillis();
		chargerOccupations();
		// CktlLog.log(prefix + "chargerOccupations() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		//
		// tDebut = System.currentTimeMillis();
		associerHoraires();
		// CktlLog.log(prefix + "associerHoraires() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

	}

	/**
	 * Construire le planning mois / jours / semaine et affectation des horaires
	 */
	private void chargerPlanning() {

		String prefix = "";

		// long tDebut = System.currentTimeMillis();
		prechargerPlanning();
		// CktlLog.log(prefix + "prechargerPlanning() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		// remettre à zéro des valeurs restantes des soldes
		getEoPlanning().reinitialiserSoldes();

		ligneFicheRoseArray = new NSMutableArray<I_LigneFicheRose>();

		// tDebut = System.currentTimeMillis();
		traitementAvantDebiterOccupations();
		// CktlLog.log(prefix + "traitementAvantDebiterOccupations() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		// tDebut = System.currentTimeMillis();
		appliquerCreditDebitOccupation();
		// CktlLog.log(prefix + "appliquerCreditDebitAbsences() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		// tDebut = System.currentTimeMillis();
		traitementApresDebiterOccupations();
		// CktlLog.log(prefix + "traitementApresDebiterOccupations() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		memoriserDebitsOccupationCet();

	}

	/**
	 * @return
	 */
	private EOTypeOccupation getEoTypeOccupationCongeAnnuel() {
		if (_eoTypeOccupationCongeAnnuel == null) {
			_eoTypeOccupationCongeAnnuel = EOTypeOccupation.fetchByKeyValue(
					getEoPlanning().editingContext(), EOTypeOccupation.OTYP_CODE_KEY, EOTypeOccupation.CODE_CONGE_ANNUEL);
		}
		return _eoTypeOccupationCongeAnnuel;
	}

	/**
	 * TODO
	 * 
	 * Les débits à effectuer sur les autres comptes avant débit des occupations
	 * 
	 * - reliquats négatifs
	 * 
	 * - malus liés aux cumul des congés légaux
	 * 
	 * - opérations CET
	 * 
	 * - ...
	 */
	private final void traitementAvantDebiterOccupations() {

		// droits à congé
		traitementAvantDebiterOccupationsDroitConge();

		// régularisation de solde
		traitementAvantDebiterOccupationsRegularisationSolde();

		// reliquats
		traitementAvantDebiterOccupationsReliquats();

		// décompte légal
		traitementAvantDebiterOccupationsDecompteLegal();

		// jrti
		traitementAvantDebiterOccupationsJrti();

		// cet
		traitementAvantDebiterOccupationsCet();

	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsCet() {
		getEoPlanning().getCet();

		// il n'y en au plus une seule !
		Epargne epargne = null;
		EOCetTransactionAnnuelle transaction = getEoPlanning().getEoCetTransactionAnnuelle();
		if (transaction != null) {
			epargne = transaction.getEpargneDecidee();
			// à défaut, on prend la demande pour débiter les reliquats
			if (epargne == null) {
				epargne = transaction.getEpargneDemandee();
			}
		}

		if (epargne != null) {

			// répartition de l'épargne sur plusieurs service
			// par rapport à la quotité du premier jour uniquement

			NSDictionary<String, Float> dico = getDicoQuotiteStructurePremierJour();

			int epargneTotaleRestante = epargne.getValeur();

			// il faut commencer par celui qui a la moins de reliquat
			NSArray<EOStructure> eoStructureArray = classementSelonReliquatInitial(getEoStructureAttenduArray());

			// et c'est parti
			for (int i = 0; i < eoStructureArray.count(); i++) {

				String cStructure = eoStructureArray.objectAtIndex(i).cStructure();

				int epargnePonderee = (int) ((float) epargne.getValeur() * dico.objectForKey(cStructure).floatValue());

				if (epargnePonderee > 0) {

					// reliquat restant
					int reliquatRestant = getCalculDroitCongeDelegate().getReliquatInitialMinutes(cStructure);

					if (epargnePonderee > reliquatRestant) {
						epargnePonderee = reliquatRestant;
					}

					// si c'est la derniere structure, alors il faut prendre le restant
					// total de l'épargne
					if (i == eoStructureArray.count() - 1) {
						epargnePonderee = epargneTotaleRestante;
					}

					// épargne sur reliquats puis congés (normalement que reliquats ...)
					NSArray<I_Solde> soldeArray = getSoldeArray(
								getEoTypeOccupationCongeAnnuel(),
								null,
								cStructure,
								getEoPlanning().toPeriode().perDDebut());

					new DebitRoulantEpargneCet(
								soldeArray, epargne, epargnePonderee);

					epargneTotaleRestante -= epargnePonderee;

					// pour affichage fiche rose
					LigneFicheRose lfr = new LigneFicheRose("Epargne CET " + eoStructureArray.objectAtIndex(i).lcStructure(), epargne.toString());
					lfr.setCStructureCourante(cStructure);
					// int restantIntermediaire =
					// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
					// cStructure).getRestantRel() - epargnePonderee;
					// lfr.setRestantRel(restantIntermediaire);
					lfr.setRestantRelFrom(ligneFicheRoseArray, epargnePonderee);
					lfr.setDebitRel(epargnePonderee);
					ligneFicheRoseArray.addObject(lfr);

				}

			}

		}
	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsJrti() {
		for (EOStructure eoStructure : getEoStructureAttenduArray()) {
			EOSolde eoSoldeJrti = getEoPlanning().getSoldeJrti(eoStructure.cStructure());

			if (eoSoldeJrti != null) {

				int soldeRestant = 0;
				if (eoSoldeJrti.restant() != null) {
					soldeRestant = eoSoldeJrti.restant().intValue();
				}

				if (soldeRestant < 0) {
					// JRTI prélevé sur reliquats puis congés

					NSArray<I_Solde> soldeArray = getSoldeArray(
							getEoTypeOccupationCongeAnnuel(),
							null,
							eoStructure.cStructure(),
							getEoPlanning().toPeriode().perDDebut());

					new DebitRoulantJrti(
							soldeArray, eoSoldeJrti, -soldeRestant);

					// pour affichage fiche rose
					LigneFicheRose lfr = new LigneFicheRose(
							eoSoldeJrti + " sur " + eoStructure.lcStructure(), "Total : " + TimeCtrl.stringForMinutes(-soldeRestant));
					lfr.setCStructureCourante(eoStructure.cStructure());
					// XXX a compléter
					ligneFicheRoseArray.addObject(lfr);
				}

			}

		}
	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsDroitConge() {

		for (EOStructure eoStructure : getEoStructureAttenduArray()) {

			//
			EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());

			int droitConges = getCalculDroitCongeDelegate().getDroitCongeMinutes(eoStructure.cStructure());

			if (droitConges > 0) {
				DroitCongeInitial dci = new DroitCongeInitial(droitConges);
				// crédit
				new Transaction(eoSoldeConge, dci, -droitConges);

			} else if (droitConges < 0) {
				DroitCongeInitial dci = new DroitCongeInitial(droitConges);
				// débit sur congés annuels
				new DebitRoulantDroitCongeInitialNegatif(
						new NSArray<I_Solde>(eoSoldeConge),
						dci,
						-droitConges);
			}

			// pour affichage fiche rose
			LigneFicheRose lfr = new LigneFicheRose(eoSoldeConge.toTypeSolde().stypLibelle() + " sur " + eoStructure.lcStructure(), "Crédit de " + TimeCtrl.stringForMinutes(droitConges));
			lfr.setCStructureCourante(eoStructure.cStructure());
			lfr.setRestantCng(droitConges);
			ligneFicheRoseArray.addObject(lfr);

		}

	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsRegularisationSolde() {

		for (EOStructure eoStructure : getEoStructureAttenduArray()) {
			NSArray<EOSolde> eoSoldeRegulArray = getEoPlanning().getSoldeRegularisationArray(
					eoStructure.cStructure());

			// peut être réutilisé
			EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());

			for (int i = 0; i < eoSoldeRegulArray.count(); i++) {

				EOSolde eoSoldeRegul = eoSoldeRegulArray.objectAtIndex(i);

				int soldeRestant = 0;
				if (eoSoldeRegul.restant() != null) {
					soldeRestant = eoSoldeRegul.restant().intValue();
				}

				if (soldeRestant > 0) {
					// crédit sur congés annuels
					new Transaction(eoSoldeConge, eoSoldeRegul, -soldeRestant);

				} else if (soldeRestant < 0) {
					// débit sur congés annuels
					new DebitRoulantRegularisationNegative(
							new NSArray<I_Solde>(eoSoldeConge),
							eoSoldeRegul,
							-soldeRestant);
				}

				// pour affichage fiche rose
				LigneFicheRose lfr = new LigneFicheRose(
						eoSoldeRegul.toTypeSolde().stypLibelle() + " sur " + eoStructure.lcStructure(), eoSoldeRegul.solMotif());
				lfr.setCStructureCourante(eoStructure.cStructure());
				// int restantIntermediaire =
				// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
				// eoStructure.cStructure()).getRestantCng() + soldeRestant;
				// lfr.setRestantCng(restantIntermediaire);
				lfr.setRestantCngFrom(ligneFicheRoseArray, -soldeRestant);
				lfr.setDebitCng(-soldeRestant);
				ligneFicheRoseArray.addObject(lfr);

			}

		}

	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsDecompteLegal() {
		for (EOStructure eoStructure : getEoStructureAttenduArray()) {

			// decompte légal règle 90-180j
			int minutesDecompteLegal = CongeLegalDelegate.getMinutesDecompteLegal90180(this, eoStructure.cStructure());

			if (minutesDecompteLegal > 0) {
				MalusPourCongeLegalRegle90180 malusPourCongeLegal90180 = new MalusPourCongeLegalRegle90180(minutesDecompteLegal);

				// le décompte se fait sur les congés
				EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());

				new DebitRoulantCongeLegal(
						new NSArray<I_Solde>(eoSoldeConge),
						malusPourCongeLegal90180,
						minutesDecompteLegal);

				// pour affichage fiche rose
				LigneFicheRose lfr = new LigneFicheRose(
						"Décompte congés légaux sur " + eoStructure.lcStructure(), malusPourCongeLegal90180.toString());
				lfr.setCStructureCourante(eoStructure.cStructure());
				// int restantIntermediaire =
				// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
				// eoStructure.cStructure()).getRestantCng() - minutesDecompteLegal;
				// lfr.setRestantCng(restantIntermediaire);
				lfr.setRestantCngFrom(ligneFicheRoseArray, minutesDecompteLegal);
				lfr.setDebitCng(minutesDecompteLegal);
				ligneFicheRoseArray.addObject(lfr);
			}

			// decompte légal règle réduction RTT
			minutesDecompteLegal = CongeLegalDelegate.getMinutesDecompteCongeMaladieReductionRTTSurBaseHeure(this, eoStructure.cStructure());

			if (minutesDecompteLegal > 0) {
				MalusPourCongeLegalRegleRTT malusPourCongeLegalRTT = new MalusPourCongeLegalRegleRTT(minutesDecompteLegal, getEoPlanning().toPeriode());

				// le décompte se fait sur les congés
				EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());

				new DebitRoulantCongeLegal(
						new NSArray<I_Solde>(eoSoldeConge),
						malusPourCongeLegalRTT,
						minutesDecompteLegal);

				// pour affichage fiche rose
				LigneFicheRose lfr = new LigneFicheRose(
						"Décompte congés légaux sur " + eoStructure.lcStructure(), malusPourCongeLegalRTT.toString());
				lfr.setCStructureCourante(eoStructure.cStructure());
				// int restantIntermediaire =
				// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
				// eoStructure.cStructure()).getRestantCng() - minutesDecompteLegal;
				// lfr.setRestantCng(restantIntermediaire);
				lfr.setRestantCngFrom(ligneFicheRoseArray, minutesDecompteLegal);
				lfr.setDebitCng(minutesDecompteLegal);
				ligneFicheRoseArray.addObject(lfr);
			}

		}

	}

	/**
	 * 
	 */
	private final void traitementAvantDebiterOccupationsReliquats() {
		for (EOStructure eoStructure : getEoStructureAttenduArray()) {

			traitementAvantDebiterOccupationsReliquatSoldeNatif(eoStructure);
		
			if (HamacCktlConfig.booleanForKey(EOParametre.REPORT_HSUP_ACTIF, getEoPlanning().toPeriode())) {
			
				traitementAvantDebiterOccupationsReliquatHsup(eoStructure);
			
			}

		}
	}

	
	private void traitementAvantDebiterOccupationsReliquatHsup(EOStructure eoStructure) {
		
		EOSolde eoSoldeReliquatBalancePrev = getEoPlanning().getEoSoldeBalanceDernier(eoStructure.cStructure());
		
		EOSolde eoSoldeReliquatHsup = getEoPlanning().getSoldeReliquatHsup(eoStructure.cStructure());
		
		if (eoSoldeReliquatBalancePrev != null) {
			
			int minutes = eoSoldeReliquatBalancePrev.restant();
			
			if (minutes > 0) {
				
				// crédit
				new Transaction(eoSoldeReliquatHsup, eoSoldeReliquatBalancePrev, -minutes);

				// pour affichage fiche rose
				LigneFicheRose lfr = new LigneFicheRose(
						"Reliquat sur " + eoStructure.lcStructure(), "Heures supplémentaires restantes sur l'année N-1");
				lfr.setCStructureCourante(eoStructure.cStructure());
				lfr.setRestantRel(minutes);
				ligneFicheRoseArray.addObject(lfr);
				
			} 
			
		}
		
	}
	
	
	/**
	 * 
	 * @param eoStructure
	 */
	private void traitementAvantDebiterOccupationsReliquatSoldeNatif(EOStructure eoStructure) {
		
		// trouver un enregistrement
		EOSolde eoSoldeReliquat = getEoPlanning().getSoldeReliquatNatif(eoStructure.cStructure());

		eoSoldeReliquat.setRestant(Integer.valueOf(0));

		// d'abord voir s'il y a une valeur manuelle avec période standard
		if (eoSoldeReliquat.init() != null) {

			int minutes = eoSoldeReliquat.init().intValue();

			if (minutes != 0) {

				SaisieManuelleSoldeInitial saisieManuelle = new SaisieManuelleSoldeInitial(eoSoldeReliquat, minutes);

				if (minutes > 0) {

					// crédit
					new Transaction(eoSoldeReliquat, saisieManuelle, -minutes);

					// pour affichage fiche rose
					LigneFicheRose lfr = new LigneFicheRose(
							"Reliquat sur " + eoStructure.lcStructure(), saisieManuelle.toString());
					lfr.setCStructureCourante(eoStructure.cStructure());
					lfr.setRestantRel(minutes);
					ligneFicheRoseArray.addObject(lfr);

				} else {

					// débit roulant droit à congé
					EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());
					new DebitRoulantReliquatNegatif(new NSArray<I_Solde>(eoSoldeConge), saisieManuelle, -minutes);

					// pour affichage fiche rose
					LigneFicheRose lfr = new LigneFicheRose(
							"Reliquat négatif sur " + eoStructure.lcStructure(), saisieManuelle.toString());
					lfr.setCStructureCourante(eoStructure.cStructure());
					// int restantIntermediaire =
					// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
					// eoStructure.cStructure()).getRestantCng() + minutes;
					// lfr.setRestantCng(restantIntermediaire);
					lfr.setRestantCngFrom(ligneFicheRoseArray, -minutes);
					lfr.setDebitCng(-minutes);
					ligneFicheRoseArray.addObject(lfr);
				}
			}

		} else {
			// dernier cas (le plus courant) c'est le restant de l'année N-1 sur
			// la même structure
			EOSolde eoSoldeCongePrev = getEoPlanning().getEoSoldeCongeNatifDernier(eoStructure.cStructure());
			if (eoSoldeCongePrev != null) {
				int minutes = eoSoldeCongePrev.restant();

				if (minutes != 0) {

					if (minutes > 0) {

						// crédit
						new Transaction(eoSoldeReliquat, eoSoldeCongePrev, -minutes);

						// pour affichage fiche rose
						LigneFicheRose lfr = new LigneFicheRose(
								"Reliquat sur " + eoStructure.lcStructure(), "Congés restants sur l'année N-1");
						lfr.setCStructureCourante(eoStructure.cStructure());
						lfr.setRestantRel(minutes);
						ligneFicheRoseArray.addObject(lfr);

					} else {

						// débit roulant droit à congé
						EOSolde eoSoldeConge = getEoPlanning().getSoldeCongeNatif(eoStructure.cStructure());
						new DebitRoulantReliquatNegatif(new NSArray<I_Solde>(eoSoldeConge), eoSoldeCongePrev, -minutes);

						// pour affichage fiche rose
						LigneFicheRose lfr = new LigneFicheRose(
								"Reliquat négatif sur " + eoStructure.lcStructure(), "Déficit sur congé de l'année N-1");
						lfr.setCStructureCourante(eoStructure.cStructure());
						// int restantIntermediaire =
						// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
						// eoStructure.cStructure()).getRestantCng() + minutes;
						// lfr.setRestantCng(restantIntermediaire);
						lfr.setRestantCngFrom(ligneFicheRoseArray, -minutes);
						lfr.setDebitCng(-minutes);
						ligneFicheRoseArray.addObject(lfr);

					}

				}

			}

		}
		
		
	}
	
	
	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de régularisation négative
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantRegularisationNegative
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeRegulNegative
		 * @param valeur
		 */
		public DebitRoulantRegularisationNegative(
				NSArray<I_Solde> soldeArray, EOSolde eoSoldeRegulNegative, int valeur) {
			super(soldeArray, eoSoldeRegulNegative, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de droit à congé négatif
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantDroitCongeInitialNegatif
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeRegulNegative
		 * @param valeur
		 */
		public DebitRoulantDroitCongeInitialNegatif(
				NSArray<I_Solde> soldeArray, DroitCongeInitial dciNegatif, int valeur) {
			super(soldeArray, dciNegatif, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de balance négative
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantBalanceNegative
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeBalance
		 * @param valeur
		 */
		public DebitRoulantBalanceNegative(
				NSArray<I_Solde> soldeArray, EOSolde eoSoldeBalance, int valeur) {
			super(soldeArray, eoSoldeBalance, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de régularisation négative
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantReliquatNegatif
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeReliquatNegatif
		 * @param valeur
		 */
		public DebitRoulantReliquatNegatif(
				NSArray<I_Solde> soldeArray, I_Operation eoSoldeReliquatNegatif, int valeur) {
			super(soldeArray, eoSoldeReliquatNegatif, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de JRTI
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantJrti
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeJrti
		 * @param valeur
		 */
		public DebitRoulantJrti(
				NSArray<I_Solde> soldeArray, EOSolde eoSoldeJrti, int valeur) {
			super(soldeArray, eoSoldeJrti, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants pour l'épargne CEt
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantEpargneCet
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeJrti
		 * @param valeur
		 */
		public DebitRoulantEpargneCet(
				NSArray<I_Solde> soldeArray, Epargne epargne, int valeur) {
			super(soldeArray, epargne, valeur);
		}

	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes en cas
	 * de décompte légal
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantCongeLegal
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param eoSoldeReliquatNegatif
		 * @param valeur
		 */
		public DebitRoulantCongeLegal(
				NSArray<I_Solde> soldeArray, A_MalusPourCongeLegal malusPourCongeLegal, int valeur) {
			super(soldeArray, malusPourCongeLegal, valeur);
		}

	}

	/**
	 * Les débits à effectuer sur les autres comptes après débit des occupations :
	 * 
	 * - balance hsup ccomp négative
	 * 
	 * - ...
	 */
	private void traitementApresDebiterOccupations() {

		// balance heure supp congés compensateurs
		for (EOStructure eoStructure : getEoStructureAttenduArray()) {
			EOSolde eoSoldeBalance = getEoPlanning().getSoldeBalanceNatif(eoStructure.cStructure());
			
			int soldeRestant = 0;
			if (eoSoldeBalance.restant() != null) {
				soldeRestant = eoSoldeBalance.restant().intValue();
			}
			
			if (soldeRestant < 0) {
				// les soldes concernés en cas de balance négative sont les mêmes que
				// pour un congé annuel
				NSArray<I_Solde> soldeArray = getSoldeArray(
						getEoTypeOccupationCongeAnnuel(),
						null,
						eoStructure.cStructure(),
						getEoPlanning().toPeriode().perDFin());

				new DebitRoulantBalanceNegative(
						soldeArray,
						eoSoldeBalance,
						-soldeRestant);

				// pour affichage fiche rose
				LigneFicheRose lfr = new LigneFicheRose(
						"Bilan HSUP/CCOMP négatif sur " + eoStructure.lcStructure(), "Bilan négatif");
				lfr.setCStructureCourante(eoStructure.cStructure());
				// int restantIntermediaire =
				// LigneFicheRose.getLastLigneFicheRoseForStructure(ligneFicheRoseArray,
				// eoStructure.cStructure()).getRestantCng() + soldeRestant;
				// lfr.setRestantCng(restantIntermediaire);
				lfr.setRestantCngFrom(ligneFicheRoseArray, -soldeRestant);
				lfr.setDebitCng(-soldeRestant);
				ligneFicheRoseArray.addObject(lfr);

			}

		}

	}

	/**
	 * Sauvegarder en base de données tous les débits d'occupations liées aux CET
	 * 
	 */
	private void memoriserDebitsOccupationCet() {

		NSArray<A_Absence> absenceCetArray = EOQualifier.filteredArrayWithQualifier(
				getAbsenceArraySortedChargement(), EOOccupation.QUAL_CET);

		for (A_Absence absenceCet : absenceCetArray) {

			// ne tenir compte que celle liés à ce planning
			EOOccupation eoOccupationCet = (EOOccupation) absenceCet;

			if (eoOccupationCet.getEoPlanningArrayPourOccupation().containsObject(getEoPlanning())) {
				eoOccupationCet.setCoutMinute(getEoPlanning());
			}

		}
	}

	/**
	 * La liste de toutes les absences gérables par l'application : occupations
	 * hamac + absences légales.
	 * 
	 * Elles sont classées pour que soit correctement fait le chargement (par
	 * priorité et date)
	 * 
	 * @return
	 */
	private final NSArray<A_Absence> getAbsenceArraySortedChargement() {
		if (_absenceArraySorted == null) {
			_absenceArraySorted = PlanningFactory.getAbsenceArraySortedChargementArray(this);
		}
		return _absenceArraySorted;
	}

	//
	// /**
	// * La liste des absences affichées pour la fiche rose (i.e.
	// chronologiquement,
	// * sans la priorité)
	// *
	// * @return
	// */
	// private NSArray<A_Absence> getAbsenceArrayFicheRoseEtBalance() {
	// if (_absenceArrayFicheRoseEtBalance == null) {
	//
	// _absenceArrayFicheRoseEtBalance = new
	// NSArray<A_Absence>(getAbsenceArraySortedChargement());
	//
	// // reclasser par date de début, pour faire un classement correct
	// _absenceArrayFicheRoseEtBalance =
	// CktlSort.sortedArray(_absenceArrayFicheRoseEtBalance,
	// A_Absence.DATE_DEBUT_KEY);
	//
	// }
	// return _absenceArrayFicheRoseEtBalance;
	// }

	/**
	 * La liste des absences affichées pour la fiche rose (i.e. chronologiquement,
	 * sans la priorité)
	 * 
	 * @return
	 */
	public NSArray<I_LigneFicheRose> getLigneFicheRoseArrayHorsCCompEtHorsHSup() {
		NSArray<I_LigneFicheRose> array = getLigneFicheRoseArray();
		EOQualifier qual = ERXQ.and(
				ERXQ.isFalse(I_LigneFicheRose.IS_ABSENCE_ASSIMILEE_CCOMM_POUR_BALANCE_KEY),
				ERXQ.isFalse(I_LigneFicheRose.IS_HEURE_SUPPLEMENTAIRE_KEY));
		array = EOQualifier.filteredArrayWithQualifier(array, qual);
		return array;
	}

	/**
	 * La liste {@link #getLigneFicheRoseArray()} pour le type assimulés aux
	 * congés compensateurs
	 * 
	 * @return
	 */
	public NSArray<I_LigneFicheRose> getLigneFicheRoseArrayCComp() {
		NSArray<I_LigneFicheRose> array = getLigneFicheRoseArray();
		EOQualifier qual = ERXQ.isTrue(I_LigneFicheRose.IS_ABSENCE_ASSIMILEE_CCOMM_POUR_BALANCE_KEY);
		array = EOQualifier.filteredArrayWithQualifier(array, qual);
		return array;
	}

	/**
	 * La liste {@link #getLigneFicheRoseArray()} pour le type unique heure
	 * supplémentaire
	 * 
	 * @return
	 */
	public NSArray<I_LigneFicheRose> getLigneFicheRoseArrayHSup() {
		NSArray<I_LigneFicheRose> array = getLigneFicheRoseArray();
		EOQualifier qual = ERXQ.isTrue(I_LigneFicheRose.IS_HEURE_SUPPLEMENTAIRE_KEY);
		array = EOQualifier.filteredArrayWithQualifier(array, qual);
		return array;
	}

	/**
	 * Récupérer les occupations de la base de données et les affecter au
	 * calendrier
	 */
	private void chargerOccupations() {

		NSArray<A_Absence> absenceArray = getAbsenceArraySortedChargement();

		// affectation des absences aux jours
		for (A_Absence absence : absenceArray) {
			NSArray<Jour> jours = getJours(absence.dateDebut(), absence.dateFin());
			for (Jour jour : jours) {
				jour.addAbsence(absence);
			}
		}
	}

	/**
	 * 
	 */
	private final void appliquerCreditDebitOccupation() {

		NSArray<A_Absence> absenceArray = getAbsenceArraySortedChargement();

		// calcul des debit et création des plages d'occupation
		for (A_Absence absence : absenceArray) {

			// raz des plages
			absence.resetPlageOccupation();

		}

		//
		boolean isMultiservice = isMultiService();
		// parcours des jours 1 par 1 : préparation des segments + traitement pause
		for (Jour jour : getJourArray()) {

			NSArray<A_Absence> absencePourJourArray = jour.getAbsenceArray();

			if (absencePourJourArray.count() > 0) {

				for (A_Absence absence : absencePourJourArray) {

					NSArray<Jour> jours = getJours(absence.dateDebut(), absence.dateFin());

					if (jours.containsObject(jour)) {
						jour.preparerSegments(absence);
					}

				}

			}

			if (isMultiservice) {
				jour.preTraitementCreditDebitPauseMultiService();
			}

		}

		
		if (!getPlanningHamacApplicationUser().isAucuneBonification(getEoPlanning().toPeriode())) {		
			appliquerBonificationsHoraire();
		}

		// liste des poursuites
		NSArray<EOSoldePoursuite> poursuiteArray = EOSoldePoursuite.getSoldePoursuiteArrayForPlanning(getEoPlanning());

		// parcours des jours 1 par 1 : application des débits
		for (Jour jour : getJourArray()) {

			// poursuite de solde
			EOSoldePoursuite.traitementPoursuiteSoldePourJour(this, poursuiteArray, jour);

			// absences sur la journée
			NSArray<A_Absence> absencePourJourArray = jour.getAbsenceArray();

			if (absencePourJourArray.count() > 0) {

				for (A_Absence absence : absencePourJourArray) {

					if (!ligneFicheRoseArray.containsObject(absence)) {
						ligneFicheRoseArray.addObject(absence);
					}

					NSArray<Jour> jours = getJours(absence.dateDebut(), absence.dateFin());

					if (jours.containsObject(jour)) {

						if (absence.getPlageOccupationArray().count() > 0 ) {
							jour.appliquerCreditDebit(absence);
						}

					}

				}

			}

		}

	}

	private void appliquerBonificationsHoraire() {
		for (Jour jour : getJourArray()) {

			int bonus = jour.getBonusHoraire();

			if (bonus > 0) {

				if (jour.isSamedi() == false) {

					HoraireJournalier hj = jour.getPlageTravailArray().objectAtIndex(0).getHoraireJournalier();

					if (hj.isMultiService()) {
						// multi-service / proratisation
						String cStructureA = jour.getPlageTravailAm().getCStructure();
						int bonusA = hj.getMinutesMultiServiceProrata(cStructureA, bonus);
						EOSolde dciA = getEoPlanning().getSoldeCongeNatif(cStructureA);
						BonificationHoraireDecale bonificationA = new BonificationHoraireDecale(jour.getDate(), bonusA);
						new Transaction(dciA, bonificationA, -bonusA);

						String cStructureB = jour.getPlageTravailPm().getCStructure();
						int bonusB = hj.getMinutesMultiServiceProrata(cStructureB, bonus);
						EOSolde dciB = getEoPlanning().getSoldeCongeNatif(cStructureB);
						BonificationHoraireDecale bonificationB = new BonificationHoraireDecale(jour.getDate(), bonusB);
						new Transaction(dciB, bonificationB, -bonusB);

					} else {

						String cStructureACrediter = jour.getPlageTravailArray().objectAtIndex(0).getCStructure();
						EOSolde dciStructureACrediter = getEoPlanning().getSoldeCongeNatif(cStructureACrediter);
						BonificationHoraireDecale bonification = new BonificationHoraireDecale(jour.getDate(), bonus);
						new Transaction(dciStructureACrediter, bonification, -bonus);

					}

				} else {

					// samedi
					Semaine semaine = jour.getSemaine();

					if (semaine.isSamediMatinABonifier()) {

						bonus = jour.getBonusHoraireSamediMatin();

						if (bonus > 0) {

							// determiner le service surlequel est travaillé le samedi matin
							String cStructureACrediter = jour.getPlageTravailAm().getCStructure();
							EOSolde dciStructureACrediter = getEoPlanning().getSoldeCongeNatif(cStructureACrediter);
							BonificationHoraireSamediMatin btsm = new BonificationHoraireSamediMatin(semaine.getNumeroSemaine(), bonus);
							new Transaction(dciStructureACrediter, btsm, -bonus);

						}

					}

					if (semaine.isSamediApresMidiABonifier()) {

						bonus = jour.getBonusHoraireSamediApresMidi();

						if (bonus > 0) {

							// determiner le service surlequel est travaillé le samedi apres
							// midi
							String cStructureACrediter = jour.getPlageTravailPm().getCStructure();
							EOSolde dciStructureACrediter = getEoPlanning().getSoldeCongeNatif(cStructureACrediter);
							BonificationHoraireSamediApresMidi btsam = new BonificationHoraireSamediApresMidi(semaine.getNumeroSemaine(), bonus);
							new Transaction(dciStructureACrediter, btsam, -bonus);
						}

					}

				}

			}

		}

	}

	/**
	 * Donne la liste des objets {@link TravailAttendu} attendus pour le planning
	 * en cours
	 * 
	 * FIXME attention, traiter les périodes incompletes (i.e. on peut avoir
	 * plusieurs entrées par structure !), auquel cas il faut faire appel à la
	 * méthode {@link #getEoStructureAttenduArray()}
	 * 
	 * @return
	 */
	public NSArray<TravailAttendu> getTravailAttenduArray() {
		if (_travailAttenduArray == null) {
			_travailAttenduArray = getEoPlanning().getTravailAttenduArray();
		}
		return _travailAttenduArray;
	}

	/**
	 * La liste des {@link TravailAttendu} pour la structure passée en paramètre
	 * 
	 * @param cStructure
	 * @return
	 */
	public NSArray<TravailAttendu> getTravailAttenduArray(String cStructure) {
		NSArray<TravailAttendu> array = null;
		EOQualifier qual = ERXQ.equals(TravailAttendu.C_STRUCTURE_KEY, cStructure);
		array = EOQualifier.filteredArrayWithQualifier(getTravailAttenduArray(), qual);
		return array;
	}

	/**
	 * La liste des {@link EOStructure} dont du travail est attendu sur ce
	 * planning
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getEoStructureAttenduArray() {

		if (_eoStructureAttenduArray == null) {
			_eoStructureAttenduArray = getEoPlanning().getEoStructureAttenduArray(getTravailAttenduArray(), false);

		}
		return _eoStructureAttenduArray;
	}

	/**
	 * Indique s'il y a au moins 1 changement de service dans la période pour ce
	 * planning
	 * 
	 * @return
	 */
	public boolean isMultiService() {
		boolean isMultiService = false;

		if (getEoStructureAttenduArray().count() > 1) {
			isMultiService = true;
		}

		return isMultiService;
	}

	/**
	 * Méthode interne pour éviter d'appeler la base de données. Ne doit être
	 * appelé pour les structures concernés par ce planning
	 * 
	 * @param cStructure
	 * @return
	 */
	public EOStructure getEoStructureForCStructure(String cStructure) {
		EOStructure eoStructure = null;

		NSArray<EOStructure> array = EOQualifier.filteredArrayWithQualifier(
				getEoStructureAttenduArray(), ERXQ.equals(EOStructure.C_STRUCTURE_KEY, cStructure));

		if (array.count() > 0) {
			eoStructure = array.objectAtIndex(0);
		} else {
			throw new IllegalAccessError(
					"La structure pour C_STRUCTURE='" + cStructure + "' n'est pas disponible sur le planning " + toString());
		}

		return eoStructure;
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<Jour> getJourArray() {
		if (_jourArray == null) {
			_jourArray = (NSArray<Jour>) NSArrayCtrl.flattenArray(
					(NSArray) getSemaineArray().valueForKey(Semaine.JOURS_KEY));
		}
		return _jourArray;
	}

	public final NSMutableArray<Mois> getMoisArray() {
		return _moisArray;
	}

	/**
	 * Raccourci vers la liste des semaines
	 * 
	 * @return
	 */
	public final NSArray<Semaine> getSemaineArray() {
		if (_semaineArray == null) {
			_semaineArray = (NSArray<Semaine>) NSArrayCtrl.flattenArray((NSArray) getMoisArray().valueForKey(Mois.SEMAINES_KEY));
		}
		return _semaineArray;
	}

	/**
	 * Liste des pré-requis horaires (durée par service, borne min / max par
	 * semaine)
	 */
	public NSMutableDictionary<String, HorairePreRequis> getDicoHorairePreRequis() {

		if (_dicoHorairePreRequis == null) {
			_dicoHorairePreRequis = HorairePreRequis.constuireDicoPourPlanning(this);
		}

		return _dicoHorairePreRequis;

	}

	public final EOPlanning getEoPlanning() {
		return eoPlanning;
	}

	public final EOPlanning getEoPlanningAssoHoraire() {
		return eoPlanningAssoHoraire;
	}

	private final void addHoraire(Horaire element) {
		getHoraireArray().add(element);
	}

	public final NSMutableArray<Horaire> getHoraireArray() {
		if (_horaireArray == null) {
			_horaireArray = new NSMutableArray<Horaire>();
		}
		return _horaireArray;
	}

	public final EOTypeCalcul getEoTypeCalcul() {
		
		if (!mapPeriodeTypeCalcul.containsKey(getEoPlanning().toPeriode())) {
			
			//
			String codeCalculDefaut = HamacCktlConfig.stringForKey(EOParametre.MODE_CALCUL_DEFAUT, getEoPlanning().toPeriode());

			EOTypeCalcul eoTypeCalcul = EOTypeCalcul.fetchByKeyValue(
					getEoPlanning().editingContext(), EOTypeCalcul.CTYP_CODE_KEY, codeCalculDefaut);
			
			mapPeriodeTypeCalcul.put(getEoPlanning().toPeriode(), eoTypeCalcul);
			
		}
		return mapPeriodeTypeCalcul.get(getEoPlanning().toPeriode());
	}

	private boolean isCalculDroitCongeDelegateInstancie() {
		return _calculDroitCongeDelegate != null;
	}

	/**
	 * @return
	 */
	public final CalculDroitCongeDelegate getCalculDroitCongeDelegate() {
		if (_calculDroitCongeDelegate == null) {

			_calculDroitCongeDelegate = new CalculDroitCongeDelegate(this, null, null);

			List<PlageModeCalcul> listePlageModeCalcul = getPlanningHamacApplicationUser().getPlageModeCalculArray();

			NSTimestamp dateDebutPeriode = getEoPlanning().toPeriode().perDDebut();
			NSTimestamp dateFinPeriode = getEoPlanning().toPeriode().perDFin();
			
			for (PlageModeCalcul plageModeCalcul : listePlageModeCalcul) {
				
				if (DateCtrl.isAfterEq(plageModeCalcul.getDateDebut(), dateDebutPeriode) && (plageModeCalcul != null && plageModeCalcul.getDateFin() != null && DateCtrl.isBeforeEq(plageModeCalcul.getDateFin(), dateFinPeriode))) {
					// plage comprise entièrement dans la période concernée
					ajouterCalculDroitCongeDelegateSelonModeCalcul(plageModeCalcul.getDateDebut(), plageModeCalcul.getDateFin(), plageModeCalcul.isModeCalculTitulaire());
					
				} else if (DateCtrl.isAfterEq(plageModeCalcul.getDateDebut(), dateDebutPeriode) 
						&& DateCtrl.isBefore(plageModeCalcul.getDateDebut(), dateFinPeriode) 
						&& (plageModeCalcul.getDateFin() == null || DateCtrl.isAfter(plageModeCalcul.getDateFin(), dateFinPeriode))) {
					// plage démarrant durant la période et se terminant après la fin de la période
					ajouterCalculDroitCongeDelegateSelonModeCalcul(plageModeCalcul.getDateDebut(), dateFinPeriode, plageModeCalcul.isModeCalculTitulaire());
					
				} else if (plageModeCalcul.getDateFin() != null 
						&& DateCtrl.isBeforeEq(plageModeCalcul.getDateFin(), dateFinPeriode) 
						&& DateCtrl.isAfter(plageModeCalcul.getDateFin(), dateDebutPeriode) 
						&& DateCtrl.isBefore(plageModeCalcul.getDateDebut(), dateDebutPeriode)) {
					// plage finissant durant la période et démarrant avant le début de la période
					ajouterCalculDroitCongeDelegateSelonModeCalcul(dateDebutPeriode, plageModeCalcul.getDateFin(), plageModeCalcul.isModeCalculTitulaire());
					
				} else if (DateCtrl.isBefore(plageModeCalcul.getDateDebut(), dateDebutPeriode) && (plageModeCalcul.getDateFin() == null || DateCtrl.isAfter(plageModeCalcul.getDateFin(), dateFinPeriode))) {
					// plage commençant avant le début de la période et se terminant après la fin de la période
					ajouterCalculDroitCongeDelegateSelonModeCalcul(dateDebutPeriode, dateFinPeriode, plageModeCalcul.isModeCalculTitulaire());
					
				}
				
			}
			
		}

		return _calculDroitCongeDelegate;
	}

	/**
	 * 
	 * @param dateDebutApplication
	 * @param dateFinApplication
	 * @param isModeCalculTitulaire
	 */
	private void ajouterCalculDroitCongeDelegateSelonModeCalcul(NSTimestamp dateDebutApplication, NSTimestamp dateFinApplication, boolean isModeCalculTitulaire) {
			
		A_PlanningCalculDroitCongeDelegate delegate = getEoTypeCalcul().getPlanningCalculDroitCongeDelegate(this, dateDebutApplication, dateFinApplication, isModeCalculTitulaire);
		_calculDroitCongeDelegate.addCalculDelegateArray(delegate);
		
	}
	
	// pour les interfaces, le binding d'une structure pour les calculs par
	// service

	private String cStructureCourante;

	/**
	 * seul celui du réel est pris en compte
	 * 
	 * @return
	 */
	public final String getCStructureCourante() {
		if (getEoPlanning().isReel()) {
			return cStructureCourante;
		} else {
			return getEoPlanning().getEoPlanningReel().getPlanning().getCStructureCourante();
		}
	}

	/**
	 * seul celui du réel est pris en compte
	 * 
	 * @param cStructureCourante
	 */
	public final void setCStructureCourante(String cStructureCourante) {
		if (getEoPlanning().isReel()) {
			this.cStructureCourante = cStructureCourante;
			// appliquer aux absences pour affichage dans la fiche rose
			for (I_LigneFicheRose lfr : getLigneFicheRoseArray()) {
				lfr.setCStructureCourante(cStructureCourante);
			}
		} else {
			getEoPlanning().getEoPlanningReel().getPlanning().setCStructureCourante(cStructureCourante);
		}
	}

	// type d'affichage
	public final static String TYPE_AFFICHAGE_HEURE = "H";
	public String typeAffichageHeure = TYPE_AFFICHAGE_HEURE; // pour pouvoir
																														// binder dans les
																														// interfaces
	public final static String TYPE_AFFICHAGE_JOUR = "J";
	public String typeAffichageJour = TYPE_AFFICHAGE_JOUR; // pour pouvoir binder
																													// dans les interfaces
	public NSArray<String> tmpAffichageJourArray = new NSArray<String>(new String[] {
			TYPE_AFFICHAGE_HEURE, TYPE_AFFICHAGE_JOUR });
	private String typeAffichage;

	/**
	 * Type d'affichage : seul celui du réel est pris en compte
	 * 
	 * @return
	 */
	public final String getTypeAffichage() {
		if (getEoPlanning().isReel()) {
			return typeAffichage;
		} else {
			return getEoPlanning().getEoPlanningReel().getPlanning().getTypeAffichage();
		}
	}

	/**
	 * Type d'affichage : seul celui du réel est pris en compte
	 */
	public final void setTypeAffichage(String typeAffichage) {
		if (getEoPlanning().isReel()) {
			this.typeAffichage = typeAffichage;
		} else {
			getEoPlanning().getEoPlanningReel().getPlanning().setTypeAffichage(typeAffichage);
		}
	}

	@Override
	public int dureeAssocieeMinutes(String cStructure) {
		int dureeAssocieeMinutes = 0;

		for (Mois moisItem : getMoisArray()) {
			dureeAssocieeMinutes += moisItem.dureeAssocieeMinutes(cStructure);
		}

		return dureeAssocieeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure) {
		int dureeComptabiliseeMinutes = 0;

		for (Mois moisItem : getMoisArray()) {
			dureeComptabiliseeMinutes += moisItem.dureeComptabiliseeMinutes(cStructure);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int dureeComptabiliseeMinutes = 0;

		for (Mois moisItem : getMoisArray()) {
			dureeComptabiliseeMinutes += moisItem.dureeComptabiliseeMinutes(cStructure, dateDebut, dateFin);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public float quotite(String cStructure) {
		float quotite = (float) 0;

		for (Mois moisItem : getMoisArray()) {
			quotite += moisItem.quotite(cStructure);
		}

		quotite = quotite / (float) getMoisArray().count();

		return quotite;
	}

	@Override
	public int dureeRealiseeMinutes(String cStructure) {
		int dureeRealiseeMinutes = 0;

		for (Mois moisItem : getMoisArray()) {
			dureeRealiseeMinutes += moisItem.dureeRealiseeMinutes(cStructure);
		}

		return dureeRealiseeMinutes;
	}

	@Override
	public Planning getPlanning() {
		return this;
	}

	/**
	 * Donne la liste des objets {@link Jour} contenus entre 2 dates. Les dates
	 * sont ramenées à minuit, le matin pour englober l'ensemble des jours
	 * couverts.
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public final NSArray<Jour> getJours(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<Jour> jours = new NSArray<Jour>();

		// s'assurer qu'on est bien dans le planning
		NSTimestamp debutMinuit = TimeCtrl.remonterAMinuit(dateDebut);
		NSTimestamp finJPlus1Minuit = TimeCtrl.remonterAMinuit(dateFin.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0));

		NSTimestamp finPeriodeJPlus1Minuit = TimeCtrl.remonterAMinuit(
				getEoPlanning().toPeriode().perDFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0));

		if (DateCtrlHamac.isBeforeEq(dateDebut, dateFin) &&
				DateCtrlHamac.isBeforeEq(debutMinuit, finPeriodeJPlus1Minuit) &&
				DateCtrlHamac.isAfterEq(dateFin, getEoPlanning().toPeriode().perDDebut())) {

			EOQualifier qual = ERXQ.and(
					ERXQ.greaterThanOrEqualTo(Jour.DATE_KEY, debutMinuit),
					ERXQ.lessThan(Jour.DATE_KEY, finJPlus1Minuit));

			jours = EOQualifier.filteredArrayWithQualifier(getJourArray(), qual);

		}

		return jours;
	}

	//

	/**
	 * Raz des objets en cache.
	 */
	public void clearCache() {

		// System.out.println("Planning.clearCache() " +
		// getEoPlanning().toString());

		// nettoyage du cache des soldes et des opérations
		NSArray<I_Solde> soldeANettoyerArray = getSoldeArrayForClearCache();
		SoldeFactory.clearCache(soldeANettoyerArray);
		
		// les variables locales
		_jourArray = null;
		_semaineArray = null;
		_moisArray = null;
		_horaireArray = null;
		_travailAttenduArray = null;
		_eoStructureAttenduArray = null;
		_absenceArraySorted = null;
		_absenceArrayFicheRoseEtBalance = null;
		// _planningCalculDroitCongeDelegate = null;
		_calculDroitCongeDelegate = null;
		_allSoldeArray = null;
		_soldeArrayDico = null;
		_operationArrayDico = null;
		_eoTypeOccupationCongeAnnuel = null;
		_dicoHorairePreRequis = null;

	}

	private NSMutableArray<I_Solde> _allSoldeArray;

	/**
	 * Stockage des soldes :
	 * 
	 * - clé = C_STRUCTURE ou {@link NSKeyValueCoding#NullValue} si on prends tous
	 * les services
	 * 
	 * - valeur = les soldes concernés
	 */
	private NSMutableDictionary<Object, NSArray<I_Solde>> _soldeArrayDico;

	/**
	 * A N'UTILISER QUE POUR AFFICHAGE
	 */
	public NSArray<I_Solde> getSoldeArrayStructureCourante() {
		NSArray<I_Solde> soldeArray = null;

		if (_soldeArrayDico == null) {
			_soldeArrayDico = new NSMutableDictionary<Object, NSArray<I_Solde>>();
		}

		Object key = null;
		if (!StringCtrl.isEmpty(getCStructureCourante())) {
			key = getCStructureCourante();
		} else {
			key = NSKeyValueCoding.NullValue;
		}

		soldeArray = _soldeArrayDico.objectForKey(key);

		if (soldeArray == null) {
			if (key == NSKeyValueCoding.NullValue) {
				// tous

				// soldes EO
				soldeArray = getAllSoldeArray();

			} else {
				// ceux de la structure
				EOQualifier qual = ERXQ.or(
						ERXQ.equals(I_Solde.C_STRUCTURE, getCStructureCourante()),
						ERXQ.isNull(I_Solde.C_STRUCTURE));

				// soldes EO
				soldeArray = EOQualifier.filteredArrayWithQualifier(getAllSoldeArray(), qual);

			}

			// mémoriser
			_soldeArrayDico.setObjectForKey(soldeArray, key);

		}

		return soldeArray;
	}

	/**
	 * Stockage des operations :
	 * 
	 * - clé = C_STRUCTURE ou {@link NSKeyValueCoding#NullValue} si on prends tous
	 * les services
	 * 
	 * - valeur = les opérations concernées
	 */
	private NSMutableDictionary<Object, NSArray<I_Operation>> _operationArrayDico;

	/**
	 * A N'UTILISER QUE POUR AFFICHAGE
	 */
	public NSArray<I_Operation> getOperationArrayStructureCourante() {
		NSArray<I_Operation> operationArray = null;

		if (_operationArrayDico == null) {
			_operationArrayDico = new NSMutableDictionary<Object, NSArray<I_Operation>>();
		}

		Object key = null;
		if (!StringCtrl.isEmpty(getCStructureCourante())) {
			key = getCStructureCourante();
		} else {
			key = NSKeyValueCoding.NullValue;
		}

		operationArray = _operationArrayDico.objectForKey(key);

		if (operationArray == null) {

			NSArray<I_Solde> soldeArray = getSoldeArrayStructureCourante();

			operationArray = (NSArray<I_Operation>) soldeArray.valueForKeyPath(I_Solde.OPERATION_ARRAY_KEY);

			operationArray = NSArrayCtrl.flattenArray(operationArray);

			operationArray = NSArrayCtrl.removeDuplicate(operationArray);

			// classement chronologique
			operationArray = CktlSort.sortedArray(operationArray, I_Operation.D_DEB);

			// mémoriser
			_operationArrayDico.setObjectForKey(operationArray, key);

			// for (I_Operation op : operationArray) {
			// System.out.println("getOperationArrayStructureCourante() operation=" +
			// op.hashCode() + " => " + op);
			// }

		}

		return operationArray;
	}

	/**
	 * La liste des soldes liés au planning
	 * 
	 * A N'UTILISER QUE POUR AFFICHAGE
	 */
	private NSMutableArray<I_Solde> getAllSoldeArray() {

		if (_allSoldeArray == null) {
			_allSoldeArray = new NSMutableArray<I_Solde>();

			// soldes EO
			_allSoldeArray.addObjectsFromArray(getEoPlanning().tosSolde());

			// soldes CET
			_allSoldeArray.addObjectsFromArray(getEoPlanning().getCet().getSoldeArray());

			// soldes récupérations
			// ne conserver que les récupération positives
			_allSoldeArray.addObjectsFromArray(getCalculDroitCongeDelegate().getSoldeRecuperationPositiveArray());

		}

		return _allSoldeArray;
	}

	/**
	 * La liste des soldes qui doivent être ré-initialisés par la méthode
	 * {@link #clearCache()}. On prend soin de ne pas lancer les calculs inutiles!
	 * 
	 * @return
	 */
	private NSArray<I_Solde> getSoldeArrayForClearCache() {
		NSMutableArray<I_Solde> array = new NSMutableArray<I_Solde>();

		// soldes EO
		array.addObjectsFromArray(getEoPlanning().tosSolde());

		// soldes CET (si instancié)
		if (getEoPlanning().isCetInstancie()) {
			array.addObjectsFromArray(getEoPlanning().getCet().getSoldeArray());
		}

		// soldes récupérations
		// ne conserver que les récupération positives
		// pas de calculs inutiles
		if (isCalculDroitCongeDelegateInstancie()) {
			array.addObjectsFromArray(getCalculDroitCongeDelegate().getSoldeRecuperationPositiveArray());
		}

		return array.immutableClone();
	}

	@Override
	public int nbJourOuvre(String cStructure) {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourOuvre(cStructure);

		}

		return n;
	}

	@Override
	public int nbJourAttendu(String cStructure) {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourAttendu(cStructure);

		}

		return n;
	}

	@Override
	public int nbJourAttendu(TravailAttendu travailAttendu) {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourAttendu(travailAttendu);

		}

		return n;
	}

	@Override
	public int nbJourOuvre() {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourOuvre();

		}

		return n;
	}

	@Override
	public int nbJourOuvre(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourOuvre(dateDebut, dateFin);

		}

		return n;
	}

	@Override
	public int nbJourOuvrable() {
		int n = 0;

		for (Mois mois : getMoisArray()) {

			n += mois.nbJourOuvrable();

		}

		return n;
	}

	//

	/**
	 * La liste des soldes disponibles
	 * 
	 * @param eoTypeOccupationForce
	 *          : le type d'occupation (mettre alors absence à null)
	 * @param absence
	 *          : l'absence. Si non null, alors le type d'occupation
	 *          eoTypeOccupationForce est ignoré
	 * @param cStructure
	 * @param date
	 * @return
	 */
	public NSArray<I_Solde> getSoldeArray(
			EOTypeOccupation eoTypeOccupationForce, A_Absence absence, String cStructure, NSTimestamp date) {

		if (absence != null && absence.isAbsenceLegale()) {

			// cas particulier des congés légaux
			return new NSArray<I_Solde>(getEoPlanning().getSoldeCongeLegal());
		}

		// type d'occupation a prendre en comtpe
		EOTypeOccupation eoTypeOccupation = eoTypeOccupationForce;
		if (absence != null) {
			eoTypeOccupation = ((EOOccupation) absence).toTypeOccupation();
		}

		// etat de validation
		boolean isEtatValidation = true;
		if (absence != null && absence.toTypeStatut().isEtatValidation() == false) {
			isEtatValidation = false;
		}

		NSArray<I_Solde> soldeArray = new NSArray<I_Solde>();

		NSArray<EOTypeSolde> eoTypeSoldeArray = eoTypeOccupation.getEoTypeSoldeSortedArray(this, isEtatValidation);

		// ajouter les soldes récupérations AT (qui sont en mémoire) si l'AT fait
		// partie de la première liste
		EOQualifier qualRecup = ERXQ.equals(EOTypeSolde.STYP_CODE_KEY, EOTypeSolde.RECUPERATION);
		NSArray<EOTypeSolde> arrayEoTypeSoldeRecuperation = EOQualifier.filteredArrayWithQualifier(eoTypeSoldeArray, qualRecup);
		if (arrayEoTypeSoldeRecuperation.count() > 0) {
			// que les récup positives
			NSArray<I_Solde> soldeRecuperationArray = getCalculDroitCongeDelegate().getSoldeRecuperationPositiveArray(cStructure);
			// période de validité
			soldeRecuperationArray = filtrerSoldesDisponiblesPourDate(soldeRecuperationArray, date);
			// ajout à la liste
			soldeArray = soldeArray.arrayByAddingObjectsFromArray(soldeRecuperationArray);
		}

		// idem pour le CET
		EOQualifier qualCet = ERXQ.equals(EOTypeSolde.STYP_CODE_KEY, EOTypeSolde.CET);
		NSArray<EOTypeSolde> arrayEoTypeSoldeCet = EOQualifier.filteredArrayWithQualifier(eoTypeSoldeArray, qualCet);
		if (arrayEoTypeSoldeCet.count() > 0) {
			// soldes CET
			NSArray<I_Solde> soldeCetArray = getEoPlanning().getCet().getSoldeArray();
			// période de validité
			soldeCetArray = filtrerSoldesDisponiblesPourDate(soldeCetArray, date);
			// ajout à la liste
			soldeArray = soldeArray.arrayByAddingObjectsFromArray(soldeCetArray);
		}

		// liste des autres soldes (qui sont en base)
		NSArray<EOQualifier> qualArray = new NSArray<EOQualifier>();
		for (EOTypeSolde eoTypeSolde : eoTypeSoldeArray) {
			EOQualifier qualTypeSolde = ERXQ.equals(EOSolde.TO_TYPE_SOLDE_KEY, eoTypeSolde);
			qualArray = qualArray.arrayByAddingObject(qualTypeSolde);
		}
		EOQualifier qual = new EOOrQualifier(qualArray);

		// les soldes nécéssitant une structure
		NSArray<EOSolde> eoSoldeArrayDispo = getEoPlanning().getEoSoldeArrayForDate(cStructure, date);
		// et ceux sans
		eoSoldeArrayDispo = eoSoldeArrayDispo.arrayByAddingObjectsFromArray(getEoPlanning().getEoSoldeArrayForDate(null, date));

		eoSoldeArrayDispo = EOQualifier.filteredArrayWithQualifier(eoSoldeArrayDispo, qual);

		// tranformation NSArray<EOSolde> => NSArray<I_Solde>
		for (EOSolde eoSolde : eoSoldeArrayDispo) {
			soldeArray = soldeArray.arrayByAddingObject(eoSolde);
		}

		// reclassement selon l'ordre définit par eoTypeSoldeArray
		NSMutableArray<I_Solde> soldeSortedArray = new NSMutableArray<I_Solde>();

		for (EOTypeSolde eoTypeSolde : eoTypeSoldeArray) {

			NSArray<I_Solde> soldePourTypeArray = EOQualifier.filteredArrayWithQualifier(
					soldeArray, eoTypeSolde.getQualifierPourISolde());

			if (soldePourTypeArray.count() > 0) {
				soldeSortedArray.addObjectsFromArray(soldePourTypeArray);
			}

		}

		return soldeSortedArray.immutableClone();
	}

	/**
	 * Methode interne utilitaire
	 * 
	 * Ne conserver au sein d'une liste de soldes que ceux ayant validité la date
	 * indiquée
	 * 
	 * @param array
	 * @param date
	 * @return
	 */
	private static NSArray<I_Solde> filtrerSoldesDisponiblesPourDate(NSArray<I_Solde> array, NSTimestamp date) {
		EOQualifier qual = ERXQ.and(
				ERXQ.lessThanOrEqualTo(I_Solde.D_DEB_KEY, date),
				ERXQ.or(
						ERXQ.greaterThanOrEqualTo(I_Solde.D_FIN_KEY, date),
						ERXQ.isNull(I_Solde.D_FIN_KEY)));

		NSArray<I_Solde> filtredArray = new NSArray<I_Solde>(
				EOQualifier.filteredArrayWithQualifier(array, qual));

		return filtredArray;
	}

	/**
	 * Raccourci
	 * 
	 * @return
	 */
	public HamacApplicationUser getPlanningHamacApplicationUser() {
		return getEoPlanning().getPlanningHamacApplicationUser();
	}

	/**
	 * Cette méthode donne la quotité moyenne constatée pour un agent sur son
	 * planning unique, c'est à dire un planning avec une unique quotité par jour,
	 * résultant de la somme des quotités journalières de tous ses plannings (si
	 * multi-affectation).
	 * 
	 * Il est possible de choisir s'il faut prendre en compte ou non les jours de
	 * non affectation (i.e. jours avec une quotité de 0).
	 * 
	 * Cette méthode sert entre autres pour déterminer le plafond maximum d'une
	 * épargne CET.
	 * 
	 * Retourne un float entre 0 et 1
	 * 
	 * @return
	 */
	public float getQuotiteMoyenneJoursNonNuls() {
		float quotiteMoyenne = (float) 0;

		int joursNonNuls = 0;
		float quotiteCumul = (float) 0;

		for (Mois mois : getMoisArray()) {
			for (Semaine semaine : mois.getSemaines()) {
				for (Jour jour : semaine.getJours()) {
					float quotiteJour = jour.quotite(null) / (float) 100;
					if (quotiteJour > (float) 0) {
						quotiteCumul += quotiteJour;
						joursNonNuls++;
					}
				}
			}
		}

		quotiteMoyenne = quotiteCumul / (float) joursNonNuls;

		return quotiteMoyenne;
	}

	/**
	 * Connaitre la quotité de travail sur chaque service le premier jour du
	 * planning. Cette méthode permet de connaitre la répartition du débit sur les
	 * reliquats en cas d'épargne de CET. On repositionne le tout entre 0 et 100%.
	 * 
	 * @return
	 */
	private NSDictionary<String, Float> getDicoQuotiteStructurePremierJour() {
		NSMutableDictionary<String, Float> dico = new NSMutableDictionary<String, Float>();

		float quotiteTotal = (float) 0;

		//
		for (EOStructure eoStructure : getEoStructureAttenduArray()) {
			float quotite = getMoisArray().objectAtIndex(0).getSemaines().objectAtIndex(0).getJours().objectAtIndex(0).quotite(eoStructure.cStructure()) / (float) 100;
			quotiteTotal += quotite;
			dico.setObjectForKey(new Float(quotite), eoStructure.cStructure());
		}

		// repasser sur base 100%
		for (String key : dico.allKeys()) {
			dico.setObjectForKey(dico.objectForKey(key).floatValue() / quotiteTotal, key);
		}

		return dico.immutableClone();
	}

	/**
	 * Classe interne pour le classement de
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 * 
	 */
	public class StructureReliquatInit
			extends ERXCustomObject {

		public final static String RELIQUAT_INIT_KEY = "reliquatInit";
		public final static String EO_STRUCTURE_KEY = "eoStructure";

		public EOStructure eoStructure;
		public int reliquatInit;

		public StructureReliquatInit(EOStructure eoStructure, int reliquatInit) {
			this.eoStructure = eoStructure;
			this.reliquatInit = reliquatInit;
		}

	}

	/**
	 * Effectuer le classement croissant d'un tableau de structure selon leur
	 * nombre de reliquat restants
	 * 
	 * @param eoStructureArrayAClasser
	 * @param delegate
	 * @return
	 */
	private NSArray<EOStructure> classementSelonReliquatInitial(
			NSArray<EOStructure> eoStructureArrayAClasser) {

		NSArray<EOStructure> array = null;

		NSMutableArray<StructureReliquatInit> srArray = new NSMutableArray<StructureReliquatInit>();
		for (EOStructure eoStructure : eoStructureArrayAClasser) {
			srArray.addObject(new StructureReliquatInit(
					eoStructure, getCalculDroitCongeDelegate().getReliquatInitialMinutes(eoStructure.cStructure())));
		}

		// classement
		srArray = (NSMutableArray<StructureReliquatInit>) CktlSort.sortedArray(srArray, StructureReliquatInit.RELIQUAT_INIT_KEY);

		// ne conserver que les structures
		array = NSArrayCtrl.flattenArray((NSArray) srArray.valueForKey(StructureReliquatInit.EO_STRUCTURE_KEY));

		return array;

	}

	/**
	 * Le nombre maximum de minutes associables (tiens compte du témoin de
	 * dépassement à droit congé).
	 * 
	 * @param cStructure
	 * @return 0 si pas de limite
	 */
	public final int getDureeAssocieeMinuteMaximum(String cStructure) {
		int minutes = 0;

		if (cStructure != null) {
			int plafondConges = getDroitCongeMinuteMaximum(cStructure);

			if (plafondConges != -1) {
				minutes = getCalculDroitCongeDelegate().getDuMinutes(cStructure) + plafondConges;
			}
		} else {

			for (EOStructure eoStructure : getEoStructureAttenduArray()) {
				minutes += getDureeAssocieeMinuteMaximum(eoStructure.cStructure());
			}

		}

		return minutes;
	}

	/**
	 * 
	 * @return
	 */
	public final int getDureeAssocieeMinuteMaximumStructureCourante() {
		return getDureeAssocieeMinuteMaximum(getCStructureCourante());
	}

	/**
	 * Le nombre maximum de minutes congés générable (tiens compte du témoin de
	 * dépassement à droit congé).
	 * 
	 * @param cStructure
	 * @return 0 si pas de limite
	 */
	public final int getDroitCongeMinuteMaximum(String cStructure) {

		int plafondConges = 0;

		if (cStructure != null) {

			plafondConges = HamacCktlConfig.intForKey(EOParametre.MINUTES_CONGES, getEoPlanning().toPeriode());

			// plafond droit à congé
			if (EOPersonnePrivilege.is(getEoPlanning(), EOPersonnePrivilege.TEM_DEP_DROIT_CONGE_KEY)) {
				plafondConges = 0;
			}

			if (plafondConges > 0) {

				// pondérer le plafond
				NSArray<TravailAttendu> attenduArray = getTravailAttenduArray(cStructure);

				float f_plafondConges = (float) 0;

				for (int i = 0; i < attenduArray.count(); i++) {
					TravailAttendu attendu = attenduArray.objectAtIndex(i);
					f_plafondConges += attendu.valeurPonderee(
							plafondConges, this, cStructure, getEoPlanning().toPeriode().perDDebut());
				}

				plafondConges = (int) f_plafondConges;
			}

		} else {

			for (EOStructure eoStructure : getEoStructureAttenduArray()) {
				plafondConges += getDroitCongeMinuteMaximum(eoStructure.cStructure());
			}

		}

		return plafondConges;
	}

	// stats

	private CongesPayesContainer congesPayesContainer;
	private ValorisationCETContainer valorisationCETContainer;
	private HeuresSupplementairesContainer heuresSupplementairesContainer;

	public final CongesPayesContainer getCongesPayesContainer() {
		if (congesPayesContainer == null) {
			congesPayesContainer = new CongesPayesContainer(getEoPlanning());
		}
		return congesPayesContainer;
	}

	public final ValorisationCETContainer getValorisationCETContainer() {
		if (valorisationCETContainer == null) {
			valorisationCETContainer = new ValorisationCETContainer(getEoPlanning());
		}
		return valorisationCETContainer;
	}

	public final HeuresSupplementairesContainer getHeuresSupplementairesContainer() {
		if (heuresSupplementairesContainer == null) {
			heuresSupplementairesContainer = new HeuresSupplementairesContainer(getEoPlanning());
		}
		return heuresSupplementairesContainer;
	}

	// fiche rose

	public final NSMutableArray<I_LigneFicheRose> getLigneFicheRoseArray() {
		return ligneFicheRoseArray;
	}
}
