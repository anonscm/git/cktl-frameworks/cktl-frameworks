package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.Comparator;

/**
 * Classe implémentant le Comparator pour trier des listes de @see#PlageModeCalcul
 * 
 *
 */
public class PlageModeCalculCompare implements Comparator<PlageModeCalcul> {


	/**
	 * @param o1 objet1
	 * @param o2 objet 2
	 * @return 1 ou -1 
	 */
	public int compare(PlageModeCalcul o1, PlageModeCalcul o2) {
		if (o1.getDateDebut().before(o2.getDateDebut())) {
			return -1;
		} else {
			return 1;
		}
		
	}
	
	
	
}
