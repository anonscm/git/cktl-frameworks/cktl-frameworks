/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.foundation.NSArray;

/**
 * Ensemble d'utilitaires liés à la classe {@link I_Solde}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class SoldeFactory {

	/**
	 * Effacer tous les caches liés à un solde
	 * 
	 * @param solde
	 */
	private final static void clearCache(I_Solde solde) {

		NSArray<I_Operation> operationArray = solde.getSoldeDelegate().getOperationArray();

		for (I_Operation operation : operationArray) {
			operation.getOperationDelegate().clearCache();
		}

		solde.clearCache();
	}

	/**
	 * Effacer tous les caches liés à une liste de solde
	 * 
	 * @param soldeArray
	 */
	public final static void clearCache(NSArray<I_Solde> soldeArray) {

		if (!NSArrayCtrl.isEmpty(soldeArray)) {
			for (I_Solde solde : soldeArray) {
				clearCache(solde);
			}
		}

	}

}
