/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * Classe de gestion des saisies manuelles de soldes initiaux (ex : reliquat
 * manuel, régularisation de solde ...)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class SaisieManuelleSoldeInitial
		implements I_Operation {

	private OperationDelegate operationDelegate;
	private I_Solde soldeInitialise;
	private int minutes;

	public SaisieManuelleSoldeInitial(I_Solde soldeInitialise, int minutes) {
		super();
		this.soldeInitialise = soldeInitialise;
		this.minutes = minutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "";

		str = "Saisie manuelle valeur initiale de " + TimeCtrl.stringForMinutes(minutes)/*
																																										 * +
																																										 * " sur "
																																										 * +
																																										 * getSoldeInitialise
																																										 * (
																																										 * )
																																										 */;

		return str;
	}

}
