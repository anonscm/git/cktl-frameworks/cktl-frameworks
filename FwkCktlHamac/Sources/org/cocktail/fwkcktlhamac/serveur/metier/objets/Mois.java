/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FixedGregorianCalendar;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class Mois
		extends A_ObjetPlanning {

	public final static String SEMAINES_KEY = "semaines";

	private NSArray<Semaine> semaines;
	private Planning planning;

	// cache
	private NSMutableDictionary<String, Integer> _minutesContractuelDuesDico = new NSMutableDictionary<String, Integer>();
	private NSMutableDictionary<String, Integer> _trentiemeContractuelDico = new NSMutableDictionary<String, Integer>();

	public Mois(NSTimestamp premierJour, Planning aPlanning) {
		super(premierJour);
		semaines = new NSArray<Semaine>();
		planning = aPlanning;
		FixedGregorianCalendar gc = new FixedGregorianCalendar();
		gc.setTime(getPremierJour());
		int month = gc.get(FixedGregorianCalendar.MONTH) + 1;
		String strLibelle = month + "/" + gc.get(FixedGregorianCalendar.YEAR);
		// mettre un "0" pour les nombres à 1 chiffre
		if (month < 10) {
			strLibelle = "0" + strLibelle;
		}
		setLibelle(strLibelle);
		setlibelleCourt(DateCtrlHamac.dateToString(getPremierJour(), "%b"));
		setDernierJour(getPremierJour().timestampByAddingGregorianUnits(0, 1, -1, 0, 0, 0));
	}

	public void addSemaine(Semaine element) {
		semaines = semaines.arrayByAddingObject(element);
	}

	public final Planning getPlanning() {
		return planning;
	}

	public final NSArray<Semaine> getSemaines() {
		return semaines;
	}

	@Override
	public int dureeAssocieeMinutes(String cStructure) {
		int dureeAssocieeMinutes = 0;

		for (Semaine semaineItem : semaines) {
			dureeAssocieeMinutes += semaineItem.dureeAssocieeMinutes(cStructure);
		}

		return dureeAssocieeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure) {
		int dureeComptabiliseeMinutes = 0;

		for (Semaine semaineItem : semaines) {
			dureeComptabiliseeMinutes += semaineItem.dureeComptabiliseeMinutes(cStructure);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int dureeComptabiliseeMinutes = 0;

		for (Semaine semaineItem : semaines) {
			dureeComptabiliseeMinutes += semaineItem.dureeComptabiliseeMinutes(cStructure, dateDebut, dateFin);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public float quotite(String cStructure) {
		float quotite = 0;

		for (Semaine semaineItem : semaines) {
			quotite += semaineItem.quotite(cStructure);
		}

		quotite = quotite / (float) semaines.count();

		return quotite;
	}

	@Override
	public int dureeRealiseeMinutes(String cStructure) {
		int dureeRealiseeMinutes = 0;

		for (Semaine semaineItem : semaines) {
			dureeRealiseeMinutes += semaineItem.dureeRealiseeMinutes(cStructure);
		}

		return dureeRealiseeMinutes;
	}

	@Override
	public int nbJourOuvre(String cStructure) {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourOuvre(cStructure);

		}

		return n;
	}

	/**
	 * Donne le nombre de jours attendus pour le mois, afin d'effectuer le calcul
	 * au trentième si nécéssaire
	 * 
	 * @return
	 */
	@Override
	public int nbJourAttendu(String cStructure) {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourAttendu(cStructure);

		}

		return n;
	}

	@Override
	public int nbJourAttendu(TravailAttendu travailAttendu) {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourAttendu(travailAttendu);

		}

		return n;
	}

	@Override
	public int nbJourOuvre() {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourOuvre();

		}

		return n;
	}

	@Override
	public int nbJourOuvre(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourOuvre(dateDebut, dateFin);

		}

		return n;
	}

	@Override
	public int nbJourOuvrable() {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.nbJourOuvrable();

		}

		return n;
	}

	/**
	 * Nombre de jours (planning de service)
	 */
	public int nbJour() {
		int n = 0;

		for (Semaine semaine : getSemaines()) {

			n += semaine.getJours().count();

		}

		return n;
	}

	// affichage des heures dues par mois, pour les contractuels

	public final int getMinutesDuesContractuel(String cStructure) {
		int i_minutesDues = 0;

		if (StringCtrl.isEmpty(cStructure)) {

			for (EOStructure eoStructure : getPlanning().getEoStructureAttenduArray()) {
				i_minutesDues += getMinutesDuesContractuel(eoStructure.cStructure());
			}

		} else {

			Integer minutesDues = _minutesContractuelDuesDico.objectForKey(cStructure);

			if (minutesDues != null) {
				i_minutesDues = minutesDues.intValue();
			}

		}

		return i_minutesDues;
	}

	public final void setMinutesContractuelDue(String cStructure, int value) {
		_minutesContractuelDuesDico.setObjectForKey(Integer.valueOf(value), cStructure);
	}

	public final int getTrentiemeContractuel(String cStructure) {
		Integer i_trentieme = 0;

		if (!StringCtrl.isEmpty(cStructure)) {

			Integer trentieme = _trentiemeContractuelDico.objectForKey(cStructure);

			if (trentieme != null) {
				i_trentieme = trentieme.intValue();
			}

		}

		return i_trentieme;
	}

	public final void setTrentiemeContractuel(String cStructure, int value) {
		_trentiemeContractuelDico.setObjectForKey(Integer.valueOf(value), cStructure);
	}

	// planning de service

	private int getIndexMoisDansPlanning() {
		return getPlanning().getMoisArray().indexOfObject(this);
	}

	public Mois getMoisPrecedent() {
		Mois moisPrecedent = null;

		if (getIndexMoisDansPlanning() != 0) {
			moisPrecedent = getPlanning().getMoisArray().objectAtIndex(getIndexMoisDansPlanning() - 1);
		}

		return moisPrecedent;
	}

	public Mois getMoisSuivant() {
		Mois moisPrecedent = null;

		if (getIndexMoisDansPlanning() < getPlanning().getMoisArray().count() - 1) {
			moisPrecedent = getPlanning().getMoisArray().objectAtIndex(getIndexMoisDansPlanning() + 1);
		}

		return moisPrecedent;
	}

}
