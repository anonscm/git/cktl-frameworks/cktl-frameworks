/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.metier.A_EOSource;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.calcul.CalculDroitCongesDeuxJourEtDemi35HeuresParMoisDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Objet descriptif du travail attendu pour une duree : - 1 date de début - 1
 * date de fin - 1 structure - 1 quotité S'obtient a partir de la source des
 * plannings
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class TravailAttendu
		extends ERXCustomObject {

	public final static String DATE_DEBUT_KEY = "dateDebut";
	public final static String DATE_FIN_KEY = "dateFin";
	public final static String C_STRUCTURE_KEY = "cStructure";
	public final static String QUOTITE_KEY = "quotite";
	public final static String IS_COURANT_OU_FUTUR_KEY = "isCourantOuFutur";

	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private String cStructure;
	private Number quotite;
	private Boolean isCourantOuFutur;

	public TravailAttendu(
			A_EOSource eoSource) {
		this(eoSource.dDebut(), eoSource.dFin(), eoSource.cStructure(), eoSource.quotite());
	}

	public TravailAttendu(
			NSTimestamp dateDebut, NSTimestamp dateFin, String cStructure, Number quotite) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.cStructure = cStructure;
		this.quotite = quotite;
	}

	public final String getCStructure() {
		return cStructure;
	}

	public final Number getQuotite() {
		return quotite;
	}

	public String toString() {
		String toString = "";

		if (getDateDebut() != null) {
			toString += DateCtrlHamac.dateToString(getDateDebut());
		} else {
			toString += "null";
		}

		toString += "-";

		if (getDateFin() != null) {
			toString += DateCtrlHamac.dateToString(getDateFin());
		} else {
			toString += "null";
		}

		toString += " " + getCStructure() + " @ " + getQuotite() + "%";

		return toString;
	}

	/**
	 * 
	 * @return
	 */
	public final NSTimestamp getDateDebut() {
		return dateDebut;
	}

	/**
	 * 
	 * @return
	 */
	public final NSTimestamp getDateFin() {
		return dateFin;
	}

	/**
	 * La date de début seuillée à la période si elle dépasse
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public final NSTimestamp getDateDebutSeuilleeDansPeriode(
			EOPeriode eoPeriode) {
		NSTimestamp debut = getDateDebut();

		if (debut == null || DateCtrlHamac.isBefore(debut, eoPeriode.perDDebut())) {
			debut = eoPeriode.perDDebut();
		}

		return debut;
	}

	/**
	 * La date de fin plafonnée à la période si elle dépasse
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public final NSTimestamp getDateFinPlafonneeDansPeriode(
			EOPeriode eoPeriode) {
		NSTimestamp fin = getDateFin();

		if (fin == null || DateCtrlHamac.isAfter(fin, eoPeriode.perDFin())) {
			fin = eoPeriode.perDFin();
		}

		return fin;
	}

	/**
	 * Calcule la valeur pondérée par la quotité lié à la période du planning.
	 * Utilisé pour le calcul des heures dues.
	 * 
	 * @param minutes100
	 * @param planning
	 * @param cStructure
	 * @param dateDebutAppplication
	 * @return
	 */
	public final float valeurPonderee(
			int minutes100, Planning planning, String cStructure, NSTimestamp dateDebutAppplication) {
		return valeurPonderee(minutes100, planning, cStructure, dateDebutAppplication, getDateFinPlafonneeDansPeriode(planning.getEoPlanning().toPeriode()));
	}

	/**
	 * Calcule la valeur pondérée par la quotité lié à la période du planning.
	 * Utilisé pour le calcul des heures dues, en déterminant la date de fin (pour
	 * tableaux de bord)
	 * 
	 * @param minutes100
	 * @param planning
	 * @param cStructure
	 * @param dateDebutAppplication
	 * @param dateFin
	 * @return
	 */
	public final float valeurPonderee(
			int minutes100, Planning planning, String cStructure, NSTimestamp dateDebutAppplication, NSTimestamp dateFin) {

		float minutesPonderees = (float) 0;

		if (getCStructure().equals(cStructure)) {

			EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

			// on seuille la date de début par la date début d'application
			NSTimestamp dateDebut = getDateDebutSeuilleeDansPeriode(eoPeriode);
			if (dateDebutAppplication != null &&
					DateCtrlHamac.isAfter(dateDebutAppplication, dateDebut)) {
				dateDebut = dateDebutAppplication;
			}

			// on plafonne la date de fin
			if (dateFin == null ||
					(getDateFin() != null && DateCtrlHamac.isAfter(dateFin, getDateFin()))) {
				dateFin = getDateFin();
			}

			NSTimestamp dateDebutMoisEnCours = DateCtrlHamac.dateToDebutMois(dateDebut);
			NSTimestamp dateFinMoisEnCours = DateCtrlHamac.dateToFinMois(dateDebut);

			int nombreJoursPeriode = 0;

			while (DateCtrlHamac.isBeforeEq(dateDebutMoisEnCours, dateFin)) {

				boolean isIncludingDebutMois = true;
				boolean isIncludingFinMois = true;

				// recadrage du debut et de la fin sur la periode
				if (DateCtrlHamac.isBefore(dateDebutMoisEnCours, dateDebut)) {
					dateDebutMoisEnCours = dateDebut;
					isIncludingDebutMois = false;
				}
				if (DateCtrlHamac.isAfter(dateFinMoisEnCours, dateFin)) {
					dateFinMoisEnCours = dateFin;
					isIncludingFinMois = false;
				}

				// si le mois est complet, on compte 30 jours
				if (isIncludingDebutMois && isIncludingFinMois) {
					nombreJoursPeriode += DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30;
				} else {
					int totalJoursMois = (int) ((dateFinMoisEnCours.getTime() - dateDebutMoisEnCours.getTime()) / DateCtrlHamac.NB_MS_PAR_JOUR + 1);
					// on va regarder si avec les periodes voisines on arrive pas a faire
					// un mois complet
					if (!isIncludingDebutMois && isMoisCompletAvecTravailAttentduPrecedentEtMemeQuotite(planning, cStructure)) {
						// periode precedente ?
						// on nivelle comme s'il s'agissait d'un mois a 30 jours
						GregorianCalendar gcMois = new GregorianCalendar();
						gcMois.setTime(dateFinMoisEnCours);
						int joursDuMois = gcMois.get(GregorianCalendar.DAY_OF_MONTH);
						nombreJoursPeriode += totalJoursMois - (joursDuMois - DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30);

					} else {
						// periode suivante ?
						// ajout de la valeur du 1 a J
						nombreJoursPeriode += totalJoursMois;
					}
				}

				// avance au mois suivant
				dateDebutMoisEnCours = DateCtrlHamac.dateToDebutMois(dateDebutMoisEnCours).timestampByAddingGregorianUnits(0, 1, 0, 0, 0, 0);
				dateFinMoisEnCours = dateDebutMoisEnCours.timestampByAddingGregorianUnits(0, 1, -1, 0, 0, 0);
			}

			// prorata sur une annee de 360 jours
			minutesPonderees = (new Float((float) (nombreJoursPeriode * minutes100) / DateCtrlHamac.NB_JOURS_PAR_AN_COMPLET_360)).floatValue();
			// prorata sur la quotite d'affectation
			minutesPonderees = (getQuotite().intValue() * minutesPonderees) / 100;

		}

		return minutesPonderees;
	}

	/**
	 * Indique si l'enregistrement {@link TravailAttendu} actuel ainsi que le
	 * {@link TravailAttendu} precedent forme un mois complet sur leur mois
	 * charniere
	 * 
	 * @param planning
	 * @param cStructure
	 * @return
	 */
	private boolean isMoisCompletAvecTravailAttentduPrecedentEtMemeQuotite(Planning planning, String cStructure) {
		boolean result = false;
		TravailAttendu prevTravailAttendu = getPrevTravailAttendu(planning, cStructure);

		if (prevTravailAttendu != null &&
				prevTravailAttendu.getQuotite().intValue() == getQuotite().intValue()) {

			EOPeriode eoPeriode = planning.getEoPlanning().toPeriode();

			if (DateCtrlHamac.isSameDay(
					prevTravailAttendu.getDateFinPlafonneeDansPeriode(eoPeriode).timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0),
					getDateDebutSeuilleeDansPeriode(eoPeriode))) {

				NSTimestamp dateDebutMois = DateCtrlHamac.dateToDebutMois(getDateDebutSeuilleeDansPeriode(eoPeriode));
				result = DateCtrlHamac.isBeforeEq(prevTravailAttendu.getDateDebutSeuilleeDansPeriode(eoPeriode), dateDebutMois);

			}
		}
		return result;
	}

	/**
	 * Le travail attendu précedent chronologiquement
	 * 
	 * @param planning
	 * @param cStructure
	 * @return
	 */
	private TravailAttendu getPrevTravailAttendu(Planning planning, String cStructure) {
		TravailAttendu prevTravailAttendu = null;

		NSArray<TravailAttendu> travailAttenduArray = planning.getTravailAttenduArray(cStructure);
		int indexCourant = travailAttenduArray.indexOfIdenticalObject(this);
		if (indexCourant > 0) {
			prevTravailAttendu = travailAttenduArray.objectAtIndex(indexCourant - 1);
		}

		return prevTravailAttendu;
	}

	/**
	 * TODO fusionner les {@link TravailAttendu} contigus (car peut créer des
	 * erreurs de calcul dans
	 * {@link CalculDroitCongesDeuxJourEtDemi35HeuresParMoisDelegate})
	 * 
	 * La liste des objets {@link TravailAttendu} correspondant a la source en
	 * correlation avec le planning passé en parametre
	 * 
	 * @param eoPlanning
	 * @return
	 */
	public final static NSArray<TravailAttendu> getTravailAttenduSortedArrayForPlanning(
			EOPlanning eoPlanning) {
		NSArray<TravailAttendu> attenduArray = new NSArray<TravailAttendu>();

		NSArray<A_EOSource> eoSourceArray = eoPlanning.getEoSourceArray();

		// pour chaque résultat du jour, on crée un noeud
		for (A_EOSource eoSource : eoSourceArray) {
			TravailAttendu attendu = new TravailAttendu(eoSource);

			attenduArray = attenduArray.arrayByAddingObject(attendu);
		}

		// traitement de chevauchement
		attenduArray = traitementStructureIdentique(attenduArray);

		// lissage
		attenduArray = lisserQuotiteEtServiceIdentique(attenduArray);

		return attenduArray;
	}

	/**
	 * Retraiter un tableau de {@link TravailAttendu} contenant des chevauchement
	 * pour un cStructure identique en tableau de {@link TravailAttendu} sans
	 * chevauchement.
	 * 
	 * ex : 01/01/2010-null@50%DSI ; 01/01/2011-null@50%DSI =>
	 * 01/01/2010-31/12/2010@100%@DSI ; 01/01/2011-null@50%@DSI
	 * 
	 * @return
	 */
	private final static NSArray<TravailAttendu> traitementStructureIdentique(
			NSArray<TravailAttendu> attenduArray) {

		NSMutableArray<TravailAttendu> arrayGlobal = new NSMutableArray<TravailAttendu>();

		NSArray<String> cStructureArray = (NSArray<String>) attenduArray.valueForKey(C_STRUCTURE_KEY);
		cStructureArray = NSArrayCtrl.removeDuplicate(cStructureArray);

		for (String cStructure : cStructureArray) {

			NSMutableArray<TravailAttendu> array = new NSMutableArray<TravailAttendu>(
					EOQualifier.filteredArrayWithQualifier(attenduArray, ERXQ.equals(C_STRUCTURE_KEY, cStructure)));

			while (isTableauSansChevauchement(array) == false) {
				boolean hasChangement = false;

				NSArray<TravailAttendu> arrayAVirer = new NSArray<TravailAttendu>();
				NSArray<TravailAttendu> arrayAAjouter = new NSArray<TravailAttendu>();

				for (int i = 0; i < array.count() && !hasChangement; i++) {
					for (int j = i + 1; j < array.count() && !hasChangement; j++) {
						if (i != j) {
							TravailAttendu ta = array.objectAtIndex(i);
							TravailAttendu tb = array.objectAtIndex(j);
							arrayAAjouter = TravailAttendu.diviseTableaux(ta, tb);
							if (arrayAAjouter.count() > 0) {
								arrayAVirer = arrayAVirer.arrayByAddingObject(ta);
								arrayAVirer = arrayAVirer.arrayByAddingObject(tb);
								hasChangement = true;
							}
						}
					}
				}

				if (hasChangement) {
					array.removeObjectsInArray(arrayAVirer);
					array.addObjectsFromArray(arrayAAjouter);
				}

			}

			arrayGlobal.addObjectsFromArray(array);

		}

		return arrayGlobal.immutableClone();
	}

	/**
	 * Indique si rien ne se chevauche dans le tableau
	 * 
	 * @param attenduArray
	 * @return
	 */
	private final static boolean isTableauSansChevauchement(NSArray<TravailAttendu> attenduArray) {
		boolean isSansChevauchement = true;

		for (int i = 0; i < attenduArray.count() && isSansChevauchement; i++) {
			for (int j = i + 1; j < attenduArray.count() && isSansChevauchement; j++) {
				if (i != j) {
					if (isChevauche(attenduArray.objectAtIndex(i), attenduArray.objectAtIndex(j))) {
						isSansChevauchement = false;
					}
				}
			}

		}

		return isSansChevauchement;
	}

	private final static boolean isChevauche(
			TravailAttendu t1, TravailAttendu t2) {
		boolean isChevauche = false;

		if (!(t1.getDateFin() != null && DateCtrlHamac.isBefore(t1.getDateFin(), t2.getDateDebut())) &&
				!(t2.getDateFin() != null && DateCtrlHamac.isBefore(t2.getDateFin(), t1.getDateDebut()))) {
			isChevauche = true;
		}

		return isChevauche;
	}

	private final static NSArray<TravailAttendu> diviseTableaux(
			TravailAttendu travailAttendu1, TravailAttendu travailAttendu2) {
		NSMutableArray<TravailAttendu> array = new NSMutableArray<TravailAttendu>();

		if (isChevauche(travailAttendu1, travailAttendu2)) {

			// mettre dans l'ordre
			TravailAttendu t1 = travailAttendu1;
			TravailAttendu t2 = travailAttendu2;
			if (DateCtrlHamac.isAfter(travailAttendu1.getDateDebut(), travailAttendu2.getDateDebut())) {
				t1 = travailAttendu2;
				t2 = travailAttendu1;
			}

			if (!DateCtrlHamac.isSameDay(t1.getDateDebut(), t2.getDateDebut())) {
				array.addObject(new TravailAttendu(
						t1.getDateDebut(),
						t2.getDateDebut().timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0),
						t1.getCStructure(),
						t1.getQuotite()));
			}

			int somme = t1.getQuotite().intValue() + t2.getQuotite().intValue();

			if (t1.getDateFin() == null &&
					t2.getDateFin() == null) {

				array.addObject(new TravailAttendu(
						t2.getDateDebut(), null, t2.getCStructure(), Integer.valueOf(somme)));

			} else if (t1.getDateFin() == null &&
					t2.getDateFin() != null) {

				array.addObject(new TravailAttendu(
						t1.getDateDebut(), t2.getDateFin(), t2.getCStructure(), Integer.valueOf(somme)));
				array.addObject(new TravailAttendu(
						t2.getDateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0),
						null, t1.getCStructure(), t1.getQuotite()));

			} else if (t1.getDateFin() != null &&
					t2.getDateFin() == null) {

				array.addObject(new TravailAttendu(
						t2.getDateDebut(), t1.getDateFin(), t1.getCStructure(), Integer.valueOf(somme)));
				array.addObject(new TravailAttendu(
						t1.getDateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0),
						null, t1.getCStructure(), t2.getQuotite()));

			} else {

				// les 2 dates de fin sont connues

				if (DateCtrlHamac.isSameDay(t1.getDateFin(), t2.getDateFin())) {
					array.addObject(new TravailAttendu(
							t2.getDateDebut(), t1.getDateFin(), t1.getCStructure(), Integer.valueOf(somme)));
				} else if (DateCtrlHamac.isAfter(t2.getDateFin(), t1.getDateFin())) {
					array.addObject(new TravailAttendu(
							t2.getDateDebut(), t1.getDateFin(), t1.getCStructure(), Integer.valueOf(somme)));
					array.addObject(new TravailAttendu(
							t1.getDateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0),
							t2.getDateFin(), t1.getCStructure(), t2.getQuotite()));
				} else if (DateCtrlHamac.isAfter(t1.getDateFin(), t2.getDateFin())) {
					array.addObject(new TravailAttendu(
							t2.getDateDebut(), t2.getDateFin(), t1.getCStructure(), Integer.valueOf(somme)));
					array.addObject(new TravailAttendu(
							t2.getDateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0),
							t1.getDateFin(), t1.getCStructure(), t1.getQuotite()));
				}
			}

		}

		// System.out.println("array=" + array);

		return array;
	}

	/**
	 * Filtrer
	 * 
	 * @return
	 */
	public final static NSArray<TravailAttendu> filtrerTravailAttenduPourDate(
			NSArray<TravailAttendu> attenduArray, NSTimestamp date) {

		NSArray<TravailAttendu> attenduJourArray = new NSArray<TravailAttendu>();

		if (attenduArray != null) {
			EOQualifier qualAttenduJour =
					ERXQ.and(
							ERXQ.or(
									ERXQ.lessThanOrEqualTo(TravailAttendu.DATE_DEBUT_KEY, date),
									ERXQ.isNull(TravailAttendu.DATE_DEBUT_KEY)),
							ERXQ.or(
									ERXQ.greaterThanOrEqualTo(TravailAttendu.DATE_FIN_KEY, date),
									ERXQ.isNull(TravailAttendu.DATE_FIN_KEY)));

			attenduJourArray = EOQualifier.filteredArrayWithQualifier(
					attenduArray, qualAttenduJour);

		}

		return attenduJourArray;
	}

	/**
	 * Donne la liste des {@link Mois} associé au {@link TravailAttendu} courant
	 * pour le {@link Planning}
	 * 
	 * @param planning
	 * @return
	 */
	public NSArray<Mois> getMois(Planning planning) {
		NSArray<Mois> array = new NSArray<Mois>();

		EOQualifier qual =
				ERXQ.not(
						ERXQ.or(
								ERXQ.lessThan(Mois.DERNIER_JOUR_KEY, getDateDebut()),
								ERXQ.greaterThan(Mois.PREMIER_JOUR_KEY, getDateFin())));

		if (getDateFin() == null) {
			qual = ERXQ.or(
					qual,
					ERXQ.lessThan(Mois.DERNIER_JOUR_KEY, getDateDebut()));
		}

		array = EOQualifier.filteredArrayWithQualifier(
				planning.getMoisArray(), qual);

		// Mois.

		return array;
	}

	public final boolean isCourantOuFutur() {
		if (isCourantOuFutur == null) {
			isCourantOuFutur = Boolean.FALSE;
			if ((DateCtrlHamac.isAfterEq(DateCtrlHamac.now(), getDateDebut()) && (getDateFin() == null || DateCtrlHamac.isBeforeEq(DateCtrlHamac.now(), getDateFin()))) // courant 
					|| (DateCtrlHamac.isAfterEq(getDateDebut(), DateCtrlHamac.now()) &&  (getDateFin() == null || DateCtrlHamac.isAfterEq(DateCtrlHamac.now(), getDateFin()))) ) { // futur

				isCourantOuFutur = Boolean.TRUE;
			}
		}
		return isCourantOuFutur.booleanValue();
	}

	/**
	 * Effectuer un "lissage" d'un tableau de {@link TravailAttendu}, pour un même
	 * agent, même service, même quotité. Tout ce qui se suit est fusionné. Il ne
	 * doit exister aucun chevauchement (donc le tableau en entrée sera forcément
	 * passé par {@link #traitementStructureIdentique(NSArray)})
	 * 
	 * ex : 01/01/2010-31/12/2010@50%DSI ; 01/01/2011-null@50%DSI =>
	 * 01/01/2010-null@50%@DSI
	 * 
	 * @param array
	 * @return
	 */
	private final static NSArray<TravailAttendu> lisserQuotiteEtServiceIdentique(NSArray<TravailAttendu> attenduArray) {

		NSMutableArray<TravailAttendu> arrayGlobal = new NSMutableArray<TravailAttendu>();

		NSArray<String> cStructureArray = (NSArray<String>) attenduArray.valueForKey(C_STRUCTURE_KEY);
		cStructureArray = NSArrayCtrl.removeDuplicate(cStructureArray);

		for (String cStructure : cStructureArray) {

			NSMutableArray<TravailAttendu> arrayCStructure = new NSMutableArray<TravailAttendu>(
					EOQualifier.filteredArrayWithQualifier(attenduArray, ERXQ.equals(C_STRUCTURE_KEY, cStructure)));

			NSArray<Number> quotiteArray = (NSArray<Number>) arrayCStructure.valueForKey(QUOTITE_KEY);
			quotiteArray = NSArrayCtrl.removeDuplicate(quotiteArray);

			for (Number quotite : quotiteArray) {

				NSMutableArray<TravailAttendu> array = new NSMutableArray<TravailAttendu>(
						EOQualifier.filteredArrayWithQualifier(arrayCStructure, ERXQ.equals(QUOTITE_KEY, quotite)));

				// on remet dans l'ordre chronologique (exigence de la méthode
				// isConsecutifMemeQuotiteMemeStructure)
				array = new NSMutableArray<TravailAttendu>(CktlSort.sortedArray(array, DATE_DEBUT_KEY));

				NSMutableArray<TravailAttendu> arrayAVirer = new NSMutableArray<TravailAttendu>();
				TravailAttendu taNew = null;

				boolean isOver = false;

				NSMutableArray<TravailAttendu> arrayLocal = new NSMutableArray<TravailAttendu>(array);

				while (!isOver) {

					if (arrayLocal.count() == 1) {
						isOver = true;
					} else {

						for (int i = 0; i < arrayLocal.count() && !isOver && taNew == null; i++) {
							TravailAttendu ta = arrayLocal.objectAtIndex(i);
							for (int j = i + 1; j < arrayLocal.count() && !isOver && taNew == null; j++) {
								TravailAttendu tb = arrayLocal.objectAtIndex(j);
								if (ta.isConsecutifMemeQuotiteMemeStructure(tb)) {
									arrayAVirer.addObject(ta);
									arrayAVirer.addObject(tb);
									taNew = new TravailAttendu(ta.getDateDebut(), tb.getDateFin(), cStructure, quotite);
								}
								if (j == arrayLocal.count() - 1) {
									isOver = true;
								}
							}
						}

						if (taNew != null) {
							arrayLocal.removeObjectsInArray(arrayAVirer);
							// mettre au début
							arrayLocal.insertObjectAtIndex(taNew, 0);
							// raz pour iteration suivante
							taNew = null;
						}

					}

				}

				arrayGlobal.addObjectsFromArray(arrayLocal);

			}

		}

		return arrayGlobal;
	}

	/**
	 * Indique si 2 {@link TravailAttendu} se suivent (même quotité et service).
	 * <code>this</code> doit précéder l'objet en paramètre
	 * 
	 * @param travailAttendu
	 * @return
	 */
	private final boolean isConsecutifMemeQuotiteMemeStructure(TravailAttendu travailAttenduAfter) {
		boolean isConsecutif = false;

		if (getQuotite().floatValue() == travailAttenduAfter.getQuotite().floatValue() &&
				getCStructure().equals(travailAttenduAfter.getCStructure()) &&
				DateCtrlHamac.isSameDay(getDateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0), travailAttenduAfter.getDateDebut())) {
			isConsecutif = true;
		}

		return isConsecutif;
	}
}
