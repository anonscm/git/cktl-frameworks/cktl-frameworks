/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;

import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_CetTransactionAnnuelleDebit
		extends A_CetTransactionAnnuelle
		implements I_Operation {

	/**
	 * @param eoCetTransactionAnnuelle
	 * @param valeur
	 */
	public A_CetTransactionAnnuelleDebit(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	public NSTimestamp dateValeur() {
		return getEoCetTransactionAnnuelle().dateValeur();
	}

	public int minutes() {
		return getValeur();
	}

	private OperationDelegate _operationDelegate;

	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, this);
		}
		return _operationDelegate;
	}

}
