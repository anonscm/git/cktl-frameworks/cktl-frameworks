/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe de gestion d'un débit cascadé sur plusieurs lignes de crédit
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_DebitRoulant {

	// XXX tmp gestion de la liste des transactions pour ce débit roulant
	private NSMutableArray<Transaction> transactionArray = new NSMutableArray<Transaction>();

	private NSArray<I_Solde> soldeArray;
	private I_Operation operation;
	private int minutesDebutDebit = -1;
	private int minutesFinDebit = -1;
	private int aDebiter = -1;

	/**
	 * 
	 */
	private A_DebitRoulant(
			NSArray<I_Solde> soldeArray,
			I_Operation operation) {
		super();
		this.soldeArray = soldeArray;
		this.operation = operation;
	}

	/**
	 * Débit roulant lié à une plage
	 * 
	 * @param soldeArray
	 *          la liste des soldes disponibles par ordre de priorité
	 * @param operation
	 *          objet débiteur
	 * @param minutesDebutDebit
	 *          minute de début
	 * @param minutesFinDebit
	 *          minute de fin
	 */
	public A_DebitRoulant(
			NSArray<I_Solde> soldeArray,
			I_Operation operation,
			int minutesDebutDebit,
			int minutesFinDebit) {
		this(soldeArray, operation);
		this.minutesDebutDebit = minutesDebutDebit;
		this.minutesFinDebit = minutesFinDebit;

		traitementPostConstructeur();

	}

	/**
	 * Débit roulant sans plage (par exemple reliquat négatif)
	 * 
	 * @param soldeArray
	 *          la liste des soldes disponibles par ordre de priorité
	 * @param operation
	 *          objet débiteur
	 * @param aDebiter
	 *          total à débiter
	 */
	public A_DebitRoulant(
			NSArray<I_Solde> soldeArray,
			I_Operation operation,
			int aDebiter) {
		this(soldeArray, operation);
		this.aDebiter = aDebiter;

		traitementPostConstructeur();

	}

	/**
	 * 
	 */
	private void traitementPostConstructeur() {
		appliquerEtRepartirDebitsSurSoldes();
	}

	/**
	 * 
	 */
	private final boolean isDebitAnonyme() {
		boolean isDebitAnonyme = false;

		if (getADebiter() != -1) {
			isDebitAnonyme = true;
		}

		return isDebitAnonyme;
	}

	/**
	 * Mémoriser sur quelle plage horaire s'est fait le débit
	 * 
	 * @param solde
	 * @param minutesDebutDansJour
	 * @param minutesFinDansJour
	 * @throws Exception
	 */
	private final void debiterSolde(
			I_Solde solde, int minutesDebutDansJour, int minutesFinDansJour) throws IllegalAccessException {

		if (isDebitAnonyme()) {
			throw new IllegalAccessException("impossible de débiter de l'anomyme sur du non anonyme !");
		}

		Transaction transaction = new Transaction(
				solde, getOperation(), minutesDebutDansJour, minutesFinDansJour);

		transactionArray.addObject(transaction);
	}

	/**
	 * Mémoriser la valeur a débiter
	 * 
	 * @param solde
	 * @param minutesDebutDansJour
	 * @param minutesFinDansJour
	 * @throws Exception
	 */
	private final void debiterSolde(
			I_Solde solde, int aDebiter) throws IllegalAccessException {

		if (isDebitAnonyme() == false) {
			throw new IllegalAccessException("impossible de débiter du non anomyme sur de l'anonyme !");
		}

		Transaction transaction = new Transaction(
				solde, getOperation(), aDebiter);

		transactionArray.addObject(transaction);
	}

	/** Les soldes concernés */
	public final NSArray<I_Solde> getSoldeArray() {
		return soldeArray;
	}

	/** L'opération "mère" */
	public final I_Operation getOperation() {
		return operation;
	}

	/** La première minute à débiter */
	public final int getMinutesDebutDebit() {
		return minutesDebutDebit;
	}

	/** La dernière minute à débiter */
	public final int getMinutesFinDebit() {
		return minutesFinDebit;
	}

	/** le total à débiter en cas de débit anonyme */
	private final int getADebiter() {
		return aDebiter;
	}

	/**
	 * Effectuer les débits.
	 */
	private final void appliquerEtRepartirDebitsSurSoldes() {

		try {
			int seuilDebit = getMinutesDebutDebit();
			int minuteFinDebit = getMinutesFinDebit();

			if (isDebitAnonyme()) {
				seuilDebit = 0;
				minuteFinDebit = getADebiter();
			}

			int restantADebiter = minuteFinDebit - seuilDebit;

			NSArray<I_Solde> soldeArray = getSoldeArray();

			int i = 0;

			while (restantADebiter > 0 && i < soldeArray.count()) {

				I_Solde solde = soldeArray.objectAtIndex(i);

				// CktlLog.log("restantADebiter : " +
				// TimeCtrl.stringForMinutes(restantADebiter) + " restant : " +
				// solde.getSoldeDelegate().restant());

				int restant = solde.getSoldeDelegate().restant();

				if (restantADebiter > restant) {

					// tant que ce n'est pas le dernier solde disponible, on
					// cascade les débits sur les suivants
					boolean isDebitNegatifAutorise = false;
					if (i == soldeArray.count() - 1) {
						isDebitNegatifAutorise = true;
					}

					if (isDebitNegatifAutorise) {

						if (isDebitAnonyme()) {
							debiterSolde(solde, minuteFinDebit - seuilDebit);
						} else {
							debiterSolde(solde, seuilDebit, minuteFinDebit);
						}
						restantADebiter = 0;

					} else {

						int soldeRestant = solde.getSoldeDelegate().restant();

						// on passe à l'itération suivante si le solde est vide
						if (soldeRestant > 0) {
							restantADebiter -= soldeRestant;
							if (isDebitAnonyme()) {
								debiterSolde(solde, soldeRestant);
							} else {
								debiterSolde(solde, seuilDebit, seuilDebit + soldeRestant);
							}
							seuilDebit += soldeRestant;
						}

					}

				} else {
					if (isDebitAnonyme()) {
						debiterSolde(solde, minuteFinDebit - seuilDebit);
					} else {
						debiterSolde(solde, seuilDebit, minuteFinDebit);
					}
					restantADebiter = 0;
				}

				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "";

		toString += this.getClass().getName();
		// oter le package
		if (StringCtrl.containsIgnoreCase(toString, ".")) {
			toString = toString.substring(toString.lastIndexOf(".") + 1, toString.length());
		}

		toString += " ";

		if (isDebitAnonyme()) {
			toString += "Debit anonyme de " + TimeCtrl.stringForMinutes(getADebiter());
		} else {
			toString += "Debit sur operation de " + TimeCtrl.stringForMinutes(getMinutesDebutDebit()) + " a " + TimeCtrl.stringForMinutes(getMinutesFinDebit());
		}

		return toString;
	}

}
