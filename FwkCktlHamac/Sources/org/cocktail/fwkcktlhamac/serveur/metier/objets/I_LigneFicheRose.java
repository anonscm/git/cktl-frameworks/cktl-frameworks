/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import com.webobjects.foundation.NSTimestamp;

/**
 * Une ligne au sein de la fiche rose
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public interface I_LigneFicheRose {

	public final static String IS_HEURE_SUPPLEMENTAIRE_KEY = "isHeureSupplementaire";
	public final static String IS_ABSENCE_ASSIMILEE_CCOMM_POUR_BALANCE_KEY = "isAbsenceAssimileeCCompPourBalance";
	public final static int TAILLE_TYPE_LIBELLE_COURT = 30;
	public final static int TAILLE_MOTIF_COURT = 40;

	public NSTimestamp dateDebut();

	public NSTimestamp dateFin();

	public Float getNbJourneeSurHoraire();

	public String libelleType();

	public String libelleTypeCourt();

	public String motif();

	public String motifCourt();

	public Integer getDebitTotal();

	public Integer getDebitConge();

	public Integer getDebitReliquat();

	public void setCStructureCourante(String cStructureCourante);

	public String getCStructureCourante();

	public Integer getRestantCng();

	public Integer getRestantRel();

	public boolean isHeureSupplementaire();

	public boolean isAbsenceAssimileeCCompPourBalance();
}
