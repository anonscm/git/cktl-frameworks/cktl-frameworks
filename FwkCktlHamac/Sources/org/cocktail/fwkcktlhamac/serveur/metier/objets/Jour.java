/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.metier.EOAbsenceLegale;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HoraireJournalier;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlagePause;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlageTravail;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FixedGregorianCalendar;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe représentant un jour
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Jour
		extends A_ObjetPlanning
		implements I_ConstsJour, NSKeyValueCoding {

	public final static String DATE_KEY = "date";

	public final static String IS_CONGE_LEGAL_JOURNEE_COMPLETE_KEY = "isCongeLegalJourneeComplete";
	public final static String NB_DEMI_JOURNEE_CONGE_LEGAL_KEY = "nbDemiJourneeCongeLegal";
	public final static String NB_DEMI_JOURNEE_CONGE_MALADIE_KEY = "nbDemiJourneeCongeMaladie";
	public final static String DUREE_CONGE_MALADIE = "dureeCongeMaladie";
	public final static String IS_ACTIF_KEY = "isActif";
	public final static String IS_MERCREDI_KEY = "isMercredi";
	public final static String IS_JEUDI_KEY = "isJeudi";
	public final static String IS_SAMEDI_KEY = "isSamedi";
	public final static String IS_DIMANCHE_KEY = "isDimanche";
	public final static String IS_HORS_AFFECTATION_KEY = "isHorsAffectation";
	public final static String IS_CHOME_KEY = "isChome";
	public final static String IS_FERIE_KEY = "isFerie";
	public final static String IS_JOUR_TRAVAILLE_IMPOT = "isJourTravailleImpot";

	private NSTimestamp date;
	private Semaine semaine;
	private int codeStatut;
	private NSMutableArray<HorairePlageTravail> plageTravailArray;
	private NSMutableArray<HorairePlagePause> plagePauseArray;
	private NSMutableArray<TravailAttendu> travailAttendus;
	private NSMutableDictionary<String, Integer> dicoDureeAssociee;
	private NSMutableDictionary<String, Integer> dicoDureeComptabilisee;
	private NSMutableDictionary<String, Integer> dicoDureeComptabiliseeAm;
	private NSMutableDictionary<String, Integer> dicoDureeComptabiliseePm;
	private NSMutableDictionary<String, Float> dicoQuotite;

	private NSMutableArray<A_Absence> absenceArray;

	private String code;

	private Integer _dayOfWeek;
	public final static int DUREE_JOURNEE_EN_MINUTES = 24 * 60;

	public Jour(NSTimestamp aDate, Semaine aSemaine) {
		super(aDate);
		date = aDate;
		semaine = aSemaine;

		setDernierJour(date);

		FixedGregorianCalendar gc = new FixedGregorianCalendar();
		gc.setTime(date);

		// constitution du libelle : <premiereLettreduJour><numero>
		String libelle = "";
		libelle += DateCtrlHamac.dateToString(getDate(), "%A").substring(0, 1).toUpperCase();
		libelle += Integer.toString(gc.get(FixedGregorianCalendar.DAY_OF_MONTH));
		setLibelle(libelle);
	}

	/**
	 * 
	 * @param element
	 */
	public void addPlageTravail(HorairePlageTravail element) {
		getPlageTravailArray().add(element);
	}

	/**
	 * 
	 * @param element
	 */
	public void addPlagePause(HorairePlagePause element) {
		getPlagePauseArray().add(element);
	}

	/**
	 * 
	 * @param element
	 */
	public void addTravailAttendu(TravailAttendu element) {
		getTravailAttendus().add(element);
	}

	/**
	 * 
	 * 
	 * @param absence
	 */
	public void addAbsence(A_Absence absence) {
		getAbsenceArray().add(absence);

		// TODO remplissage du statut
		if (absence.isAbsenceDemiJournee()) {

			GregorianCalendar debutGc = new GregorianCalendar();
			GregorianCalendar finGc = new GregorianCalendar();
			GregorianCalendar todayGc = new GregorianCalendar();

			debutGc.setTime(absence.dateDebut());
			finGc.setTime(absence.dateFin());
			todayGc.setTime(getDate());

			// les etats selon si conges legal ou pas
			int etatCongeJournee = Jour.STATUS_CONGE;
			int etatCongeAM = Jour.STATUS_CONGE_AM;
			int etatCongePM = Jour.STATUS_CONGE_PM;

			// absence légale
			if (absence.isAbsenceLegale()) {
				etatCongeJournee = Jour.STATUS_CONGE_LEGAL;
				etatCongeAM = Jour.STATUS_CONGE_LEGAL_AM;
				etatCongePM = Jour.STATUS_CONGE_LEGAL_PM;

				// congé maladie ?
				EOAbsenceLegale eoAbsenceLegale = (EOAbsenceLegale) absence;
				if (eoAbsenceLegale.toOccupationTypeLegal().isCongeMaladie(getPlanning().getEoPlanning().toPeriode())) {
					etatCongeJournee = Jour.STATUT_CONGE_MALADIE;
					etatCongeAM = Jour.STATUT_CONGE_MALADIE_AM;
					etatCongePM = Jour.STATUT_CONGE_MALADIE_PM;
				}

			}

			// etat de validation / suppression
			if (!absence.isAbsenceLegale()) {
				EOOccupation eoOccupation = (EOOccupation) absence;
				if (eoOccupation.toTypeStatut().isStatutEnCoursDeValidation() ||
						eoOccupation.toTypeStatut().isStatutVise() ||
						eoOccupation.toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
					addStatut(Jour.STATUS_EN_COURS_DE_VALIDATION);
				} else if (eoOccupation.toTypeStatut().isStatutEnCoursDeSuppression() ||
							eoOccupation.toTypeStatut().isStatutEnCoursDeSuppressionVise()) {
					addStatut(Jour.STATUS_EN_COURS_DE_SUPPRESSION);
				}
			}

			boolean isCongeJournee = false;

			// le jour est-il inclus intégralement dans l'absence
			if ((DateCtrlHamac.isBeforeEq(absence.dateDebut(), getDate()) && DateCtrlHamac.isAfter(absence.dateFin(), getDate())) ||
					DateCtrlHamac.isSameDay(getDate(), absence.dateDebut()) && DateCtrlHamac.isSameDay(getDate(), absence.dateFin()) &&
					debutGc.get(GregorianCalendar.AM_PM) == GregorianCalendar.AM && finGc.get(GregorianCalendar.AM_PM) == GregorianCalendar.PM) {
				isCongeJournee = true;
			}

			if (isCongeJournee) {
				// commence avant et termine après
				addStatut(etatCongeJournee);
				addStatut(etatCongeAM);
				addStatut(etatCongePM);
			} else if (DateCtrlHamac.isSameDay(getDate(), absence.dateFin()) &&
					finGc.get(GregorianCalendar.AM_PM) == GregorianCalendar.AM) {
				// commence avant
				addStatut(etatCongeAM);
			} else if (DateCtrlHamac.isSameDay(getDate(), absence.dateDebut()) &&
					debutGc.get(GregorianCalendar.AM_PM) == GregorianCalendar.PM) {
				// termine après
				addStatut(etatCongePM);
			}

			// fermeture
			if (isCongeJournee &&
					absence.isFermeture()) {
				addStatut(Jour.STATUS_FERMETURE);
			}

		} else {

			// TODO les autres types de congés

			if (absence.isAbsenceMinute() &&
					!absence.isPresence()) {

				// // absence qui recouvre tout l'horaire de la matinée
				// if (getPlageTravailAm() != null &&
				// absence.isDebuteLeMatin() &&
				// absence.dateDebut().getTime() ==
				// getPlageTravailAm().getDebutMinutesDansJour() &&
				// absence.isTermineLeMatin() &&
				// absence.dateFin().getTime() ==
				// getPlageTravailAm().getFinMinutesDansJour()) {
				// addStatut(Jour.STATUS_CONGE_AM);
				// } else if (getPlageTravailPm() != null &&
				// absence.isDebuteLApresMidi() &&
				// absence.dateDebut().getTime() ==
				// getPlageTravailPm().getDebutMinutesDansJour() &&
				// absence.isTermineLApresMidi() &&
				// absence.dateFin().getTime() ==
				// getPlageTravailPm().getFinMinutesDansJour()) {
				// addStatut(Jour.STATUS_CONGE_PM);
				// }

				// FIXME a terminer

				if (absence.isDebuteLeMatin() &&
						absence.isTermineLeMatin()) {
					addStatut(Jour.STATUS_CONGES_COMP_AM);
				} else if (absence.isDebuteLApresMidi() &&
						absence.isTermineLApresMidi()) {
					addStatut(Jour.STATUS_CONGES_COMP_PM);
				} else {
					addStatut(Jour.STATUS_CONGES_COMP_AM);
					addStatut(Jour.STATUS_CONGES_COMP_PM);
				}

			}

		}

	}

	//

	public void preparerSegments(A_Absence absence) {

		if (absence.isPresence()) {

			// présence

			PlageOccupation plageOcc = new PlageOccupation(this, absence);
			plageOcc.preparerSegments();
			absence.addPlageOccupation(plageOcc);

		} else {

			// absence

			// création de plages d'occupation uniquement pour celles qui chevauche
			// tout ou partie
			for (HorairePlageTravail plageHoraire : getPlageTravailArray()) {

				if (plageHoraire.chevauche(absence, this)) {

					PlageOccupation plageOcc = new PlageOccupation(this, absence, plageHoraire);
					plageOcc.preparerSegments();
					absence.addPlageOccupation(plageOcc);

				}

			}

		}

	}

	/**
	 * 
	 * @param absence
	 */
	public void appliquerCreditDebit(A_Absence absence) {

		if (absence.isPresence()) {

			// présence (il n'y a qu'un seul enregistrement)
			NSArray<PlageOccupation> plageOccArray = EOQualifier.filteredArrayWithQualifier(
					absence.getPlageOccupationArray(),
					ERXQ.and(
							ERXQ.equals(PlageOccupation.JOUR_KEY, this)));
			if (plageOccArray.count() == 0) {
				throw new IllegalAccessError("La PlageOccupation pour la présence " + absence + " n'a pas été trouvée pour le jour " + this);
			}

			PlageOccupation plageOcc = plageOccArray.objectAtIndex(0);
			plageOcc.appliquerCreditDebit();

		} else {

			// absence
			NSArray<PlageOccupation> plageOccArray = EOQualifier.filteredArrayWithQualifier(
					absence.getPlageOccupationArray(),
					ERXQ.and(
							ERXQ.equals(PlageOccupation.JOUR_KEY, this)));

			for (PlageOccupation plageOcc : plageOccArray) {
				plageOcc.appliquerCreditDebit();
			}

		}

	}

	private boolean isPreTraitementCreditPauseMultiServiceEffectue = false;

	public final boolean getIsPreTraitementCreditPauseMultiServiceEffectue() {
		return isPreTraitementCreditPauseMultiServiceEffectue;
	}

	public final void setIsPreTraitmentCreditPauseMultiServiceEffectue(boolean isPreTraitementCreditPauseMultiServiceEffectue) {
		this.isPreTraitementCreditPauseMultiServiceEffectue = isPreTraitementCreditPauseMultiServiceEffectue;
	}

	/**
	 * 
	 */

	public void preTraitementCreditDebitPauseMultiService() {
		for (HorairePlageTravail hpt : getPlageTravailArray()) {

			NSArray<PlageOccupation> array = hpt.getPlageOccupationArrayForJour(this);

			// classement chronologique pour que la journée soit bien traitée du début
			// à la fin
			array = CktlSort.sortedArray(array, PlageOccupation.D_DEB);

			for (PlageOccupation po : array) {
				po.preTraitementCreditDebitPauseMultiService();
			}

		}

	}

	/**
	 * Indique l'index en minutes depuis le début de la semaine du début de
	 * l'occupation pour le jour this {@link Jour}
	 * 
	 * Si le début est avant 00:00, alors on prend minuit du matin, si la fin est
	 * après alors on ajoute le nombre de minutes
	 * 
	 * A noter que cette méthode n'est appelée en amont que pour les jours
	 * contenus dans l'absence en paramètre {@link Absence}
	 * 
	 * S'il s'agit d'un congé à la demi journée, alors on est obligé de connaitre
	 * l'horaire associé pour sélectionner les demi journées de travail attendue
	 * 
	 * @param absence
	 */
	public final int minutesDebutPourJour(A_Absence absence) {
		int minutesDebutPourJour = 0;

		minutesDebutPourJour = Jour.DUREE_JOURNEE_EN_MINUTES * (dayOfWeek() - 1);

		if (DateCtrlHamac.isSameDay(getDate(), absence.dateDebut())) {

			if (absence.isAbsenceMinute()) {

				minutesDebutPourJour += DateCtrlHamac.minutesDepuisMinuit(absence.dateDebut());

			} else {

				// absence à la demi journée
				if (absence.isDebuteLeMatin()) {
					// si débute le matin, on commence à minuit
				} else if (absence.isDebuteLApresMidi()) {
					// débute l'après midi, c'est l'embauche PM qui prime
					EOQualifier qual = ERXQ.isTrue(HorairePlageTravail.IS_PM_KEY);
					NSArray<HorairePlageTravail> arrayPm = EOQualifier.filteredArrayWithQualifier(
							getPlageTravailArray(), qual);
					if (arrayPm.count() > 0) {
						// prendre chronologiquement s'il y a plusieurs plages
						arrayPm = CktlSort.sortedArray(arrayPm, HorairePlageTravail.DEBUT_MINUTES_DANS_SEMAINE_KEY);
						HorairePlageTravail plage = (HorairePlageTravail) arrayPm.objectAtIndex(0);
						minutesDebutPourJour = plage.getDebutMinutesDansSemaine();
					} else {
						// avancer au soir
						minutesDebutPourJour += Jour.DUREE_JOURNEE_EN_MINUTES;
					}
				}

			}

		}

		return minutesDebutPourJour;
	}

	/**
	 * Indique l'index en minutes depuis le début de la semaine du début de
	 * l'occupation pour le jour this {@link Jour}
	 * 
	 * Si le début est avant 00:00, alors on prend minuit du matin, si la fin est
	 * après alors on ajoute le nombre de minutes
	 * 
	 * A noter que cette méthode n'est appelée en amont que pour les jours
	 * contenus dans l'occupation en paramètre {@link A_Absence}
	 * 
	 * S'il s'agit d'un congé à la demi journée, alors on est obligé de connaitre
	 * l'horaire associé pour sélectionner les demi journées de travail attendue
	 * 
	 * @param absence
	 */
	public final int minutesFinPourJour(A_Absence absence) {
		int minutesFinPourJour = 0;

		minutesFinPourJour = Jour.DUREE_JOURNEE_EN_MINUTES * dayOfWeek();
		if (DateCtrlHamac.isSameDay(getDate(), absence.dateFin())) {

			if (absence.isAbsenceMinute()) {

				minutesFinPourJour = Jour.DUREE_JOURNEE_EN_MINUTES * (dayOfWeek() - 1) + DateCtrlHamac.minutesDepuisMinuit(absence.dateFin());

			} else {

				// absence à la demi journée
				if (absence.isTermineLApresMidi()) {
					// si termine l'après midi, on termine à minuit

				} else if (absence.isTermineLeMatin()) {

					// se termine le matin, c'est l'heure de débauche AM qui prime
					EOQualifier qual = ERXQ.isTrue(HorairePlageTravail.IS_AM_KEY);
					NSArray<HorairePlageTravail> arrayAm = EOQualifier.filteredArrayWithQualifier(
							getPlageTravailArray(), qual);
					if (arrayAm.count() > 0) {
						// prendre chronologiquement s'il y a plusieurs plages
						arrayAm = CktlSort.sortedArray(arrayAm, HorairePlageTravail.DEBUT_MINUTES_DANS_SEMAINE_KEY);
						HorairePlageTravail plage = (HorairePlageTravail) arrayAm.lastObject();
						minutesFinPourJour = plage.getFinMinutesDansSemaine();

					} else {

						// XXX tmp dev

					}
				}

			}

		}

		return minutesFinPourJour;
	}

	/**
	 * 
	 * @return
	 */
	public final NSTimestamp getDate() {
		return date;
	}

	/**
	 * 
	 * @return
	 */
	public final Semaine getSemaine() {
		return semaine;
	}

	/**
	 * Ajouter un etat. On utiliser le & logique pour avoir N etats.
	 */
	public void addStatut(int value) {
		if ((codeStatut & value) == 0) {
			codeStatut += value;
		}
	}

	/**
	 * forcer un statut en supprimant tous les autres
	 */
	public void setStatut(int value) {
		codeStatut = value;
	}

	/**
	 * @param jourTS
	 * @return
	 */
	public int dayOfWeek() {
		if (_dayOfWeek == null) {
			_dayOfWeek = Integer.valueOf(DateCtrlHamac.getNumJourSemaineForDate(getDate()));
		}
		return _dayOfWeek.intValue();
	}

	public boolean isLundi() {
		return dayOfWeek() == FixedGregorianCalendar.MONDAY;
	}

	public boolean isMardi() {
		return dayOfWeek() == FixedGregorianCalendar.TUESDAY;
	}

	public boolean isMercredi() {
		return dayOfWeek() == FixedGregorianCalendar.WEDNESDAY;
	}

	public boolean isJeudi() {
		return dayOfWeek() == FixedGregorianCalendar.THURSDAY;
	}

	public boolean isVendredi() {
		return dayOfWeek() == FixedGregorianCalendar.FRIDAY;
	}

	public boolean isSamedi() {
		return dayOfWeek() == FixedGregorianCalendar.SATURDAY;
	}

	public boolean isDimanche() {
		return dayOfWeek() == FixedGregorianCalendar.SUNDAY;
	}

	public boolean isFerie() {
		return hasStatutFerie();
	}

	public boolean isChome() {
		return hasStatutChome();
	}

	public boolean isFerme() {
		return hasStatutFerme();
	}

	public boolean isCongeAM() {
		return hasStatutCongeAM();
	}

	public boolean isCongePM() {
		return hasStatutCongePM();
	}

	public boolean isCongeLegalAM() {
		return hasStatutCongeLegalAM() || hasStatutCongeMaladieAM();
	}

	public boolean isCongeLegalPM() {
		return hasStatutCongeLegalPM() || hasStatutCongeMaladiePM();
	}

	public boolean isCongesCompAm() {
		return hasStatutCongesCompAm();
	}

	public boolean isCongesCompPm() {
		return hasStatutCongesCompPm();
	}

	public boolean isSansStatus() {
		return hasStatutSansStatut();
	}

	public boolean isHorsAffectation() {
		return hasStatutHorsAffectation();
	}

	@Deprecated
	public boolean isTravaille() {
		return hasStatutTravaille();
	}

	@Deprecated
	public boolean isTravailleAM() {
		return hasStatutTravailleAM();
	}

	@Deprecated
	public boolean isTravaillePM() {
		return hasStatutTravaillePM();
	}

	public boolean isHoraireForce() {
		return hasStatutHoraireForce();
	}

	public boolean isHighlightOccFirstDay() {
		return hasStatutHighlightOccFirstDay();
	}

	public boolean isHighlightOccMiddleDay() {
		return hasStatutHighlightOccMiddleDay();
	}

	public boolean isHighlightOccLastDay() {
		return hasStatutHighlightOccLastDay();
	}

	public boolean isHighlightOccOnlyDay() {
		return hasStatutHighlightOccOnlyDay();
	}

	public boolean isHighlightToday() {
		return hasStatutHighlightToday();
	}

	public boolean isEnCoursDeValidation() {
		return hasStatutEnCoursDeValidation();
	}

	public boolean isEnCoursDeSuppression() {
		return hasStatutEnCoursDeSuppression();
	}

	public boolean isAfficherNumeroSemaine() {
		return hasStatutPorteNumeroSemaine();
	}

	public boolean isAfficherTooltipHoraire() {
		return hasStatutPorteTooltipHoraire();
	}

	public boolean isVacanceScolaire() {
		return hasStatutVacanceScolaire();
	}

	private boolean hasStatutSansStatut() {
		return codeStatut == 0;
	}

	private boolean hasStatutTravaille() {
		return (codeStatut & STATUS_TRAVAILLE) != 0;
	}

	private boolean hasStatutTravailleAM() {
		return (codeStatut & STATUS_TRAVAILLE_AM) != 0;
	}

	private boolean hasStatutTravaillePM() {
		return (codeStatut & STATUS_TRAVAILLE_PM) != 0;
	}

	// private boolean hasStatutConge() { return (codeStatut & STATUS_CONGE)!=0; }
	private boolean hasStatutCongeAM() {
		return (codeStatut & STATUS_CONGE_AM) != 0;
	}

	private boolean hasStatutCongePM() {
		return (codeStatut & STATUS_CONGE_PM) != 0;
	}

	private boolean hasStatutCongeLegalAM() {
		return (codeStatut & STATUS_CONGE_LEGAL_AM) != 0;
	}

	private boolean hasStatutCongeLegalPM() {
		return (codeStatut & STATUS_CONGE_LEGAL_PM) != 0;
	}

	private boolean hasStatutCongeMaladieAM() {
		return (codeStatut & STATUT_CONGE_MALADIE_AM) != 0;
	}

	private boolean hasStatutCongeMaladiePM() {
		return (codeStatut & STATUT_CONGE_MALADIE_PM) != 0;
	}

	private boolean hasStatutCongesCompAm() {
		return (codeStatut & STATUS_CONGES_COMP_AM) != 0;
	}

	private boolean hasStatutCongesCompPm() {
		return (codeStatut & STATUS_CONGES_COMP_PM) != 0;
	}

	private boolean hasStatutFerme() {
		return (codeStatut & STATUS_FERMETURE) != 0;
	}

	private boolean hasStatutFerie() {
		return (codeStatut & STATUS_FERIE) != 0;
	}

	private boolean hasStatutChome() {
		return (codeStatut & STATUS_CHOME) != 0;
	}

	private boolean hasStatutHorsAffectation() {
		return (codeStatut & STATUS_HORS_AFFECTATION) != 0;
	}

	private boolean hasStatutHoraireForce() {
		return (codeStatut & STATUS_HORAIRE_FORCE) != 0;
	}

	private boolean hasStatutHighlightOccFirstDay() {
		return (codeStatut & HIGHLIGHT_OCC_FIRST_DAY) != 0;
	}

	private boolean hasStatutHighlightOccMiddleDay() {
		return (codeStatut & HIGHLIGHT_OCC_MIDDLE_DAY) != 0;
	}

	private boolean hasStatutHighlightOccLastDay() {
		return (codeStatut & HIGHLIGHT_OCC_LAST_DAY) != 0;
	}

	private boolean hasStatutHighlightOccOnlyDay() {
		return (codeStatut & HIGHLIGHT_OCC_ONLY_DAY) != 0;
	}

	private boolean hasStatutHighlightToday() {
		return (codeStatut & HIGHLIGHT_TODAY) != 0;
	}

	private boolean hasStatutEnCoursDeValidation() {
		return (codeStatut & STATUS_EN_COURS_DE_VALIDATION) != 0;
	}

	private boolean hasStatutEnCoursDeSuppression() {
		return (codeStatut & STATUS_EN_COURS_DE_SUPPRESSION) != 0;
	}

	private boolean hasStatutPorteNumeroSemaine() {
		return (codeStatut & STATUT_PORTE_NUMERO_SEMAINE) != 0;
	}

	private boolean hasStatutPorteTooltipHoraire() {
		return (codeStatut & STATUT_PORTE_TOOLTIP_HORAIRE) != 0;
	}

	private boolean hasStatutVacanceScolaire() {
		return (codeStatut & STATUT_VACANCE_SCOLAIRE) != 0;
	}

	public final NSMutableArray<TravailAttendu> getTravailAttendus() {
		if (travailAttendus == null) {
			travailAttendus = new NSMutableArray<TravailAttendu>();
		}
		return travailAttendus;
	}

	private NSMutableDictionary<String, Integer> getDicoDureeAssociee() {
		if (dicoDureeAssociee == null) {
			dicoDureeAssociee = new NSMutableDictionary<String, Integer>();
		}
		return dicoDureeAssociee;
	}

	private NSMutableDictionary<String, Integer> getDicoDureeComptabilisee() {
		if (dicoDureeComptabilisee == null) {
			dicoDureeComptabilisee = new NSMutableDictionary<String, Integer>();
		}
		return dicoDureeComptabilisee;
	}

	private NSMutableDictionary<String, Integer> getDicoDureeComptabiliseeAm() {
		if (dicoDureeComptabiliseeAm == null) {
			dicoDureeComptabiliseeAm = new NSMutableDictionary<String, Integer>();
		}
		return dicoDureeComptabiliseeAm;
	}

	private NSMutableDictionary<String, Integer> getDicoDureeComptabiliseePm() {
		if (dicoDureeComptabiliseePm == null) {
			dicoDureeComptabiliseePm = new NSMutableDictionary<String, Integer>();
		}
		return dicoDureeComptabiliseePm;
	}

	private NSMutableDictionary<String, Float> getDicoQuotite() {
		if (dicoQuotite == null) {
			dicoQuotite = new NSMutableDictionary<String, Float>();
		}
		return dicoQuotite;
	}

	@Override
	public int dureeAssocieeMinutes(String cStructure) {

		Integer dureeAssociee = null;

		if (!StringCtrl.isEmpty(cStructure)) {

			dureeAssociee = getDicoDureeAssociee().objectForKey(cStructure);

			if (dureeAssociee == null) {
				int minutes = 0;

				for (HorairePlageTravail plage : getPlageTravailArray()) {

					if (plage.getCStructure().equals(cStructure)) {
						// la durée de l'horaire
						minutes += plage.getDureeMinutes();

					}

				}

				dureeAssociee = Integer.valueOf(minutes);
				getDicoDureeAssociee().setObjectForKey(dureeAssociee, cStructure);
			}

		} else {

			int minutes = 0;
			for (TravailAttendu attendu : getTravailAttendus()) {
				minutes += dureeAssocieeMinutes(attendu.getCStructure());
			}
			dureeAssociee = Integer.valueOf(minutes);

		}

		return dureeAssociee.intValue();

	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure) {
		Integer dureeComptabilisee = null;

		if (!StringCtrl.isEmpty(cStructure)) {

			dureeComptabilisee = getDicoDureeComptabilisee().objectForKey(cStructure);

			if (dureeComptabilisee == null) {
				int minutes = 0;
				int minutesAm = 0;
				int minutesPm = 0;

				for (HorairePlageTravail plage : getPlageTravailArray()) {

					int minutesPlageTravail = 0;

					if (plage.getCStructure().equals(cStructure)) {
						// la durée de l'horaire

						// a ne faire que si la pause est maintenue
						if (isPauseRTTAccordee()) {
							minutesPlageTravail += plage.getDureeComptabilisee();
						} else {
							minutesPlageTravail += plage.getDureeMinutes();
						}

					}

					minutes += minutesPlageTravail;

					if (plage.isAm()) {
						minutesAm += minutesPlageTravail;
					} else {
						minutesPm += minutesPlageTravail;
					}

				}

				dureeComptabilisee = Integer.valueOf(minutes);
				getDicoDureeComptabilisee().setObjectForKey(dureeComptabilisee, cStructure);
				getDicoDureeComptabiliseeAm().setObjectForKey(Integer.valueOf(minutesAm), cStructure);
				getDicoDureeComptabiliseePm().setObjectForKey(Integer.valueOf(minutesPm), cStructure);

			}

		} else {

			int minutes = 0;
			for (TravailAttendu attendu : getTravailAttendus()) {
				minutes += dureeComptabiliseeMinutes(attendu.getCStructure());
			}
			dureeComptabilisee = Integer.valueOf(minutes);

		}

		return dureeComptabilisee.intValue();
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int dureeComptabiliseeMinutes = 0;

		boolean isAComptabiliser = false;

		if (dateDebut != null && dateFin != null) {
			if (DateCtrlHamac.isAfterEq(getDate(), dateDebut) &&
					DateCtrlHamac.isBeforeEq(getDate(), dateFin)) {
				isAComptabiliser = true;
			}
		} else if (dateDebut != null) {
			if (DateCtrlHamac.isAfterEq(getDate(), dateDebut)) {
				isAComptabiliser = true;
			}
		} else if (dateFin != null) {
			if (DateCtrlHamac.isBeforeEq(getDate(), dateFin)) {
				isAComptabiliser = true;
			}
		} else {
			isAComptabiliser = true;
		}

		if (isAComptabiliser) {
			dureeComptabiliseeMinutes = dureeComptabiliseeMinutes(cStructure);
		}

		return dureeComptabiliseeMinutes;
	}

	/**
	 * @param cStructure
	 * @return
	 */
	public int dureeComptabiliseeMinutesAm(String cStructure) {
		Integer minutes = null;

		minutes = getDicoDureeComptabiliseeAm().objectForKey(cStructure);
		if (minutes == null) {
			// forcer le remplissage du dico
			dureeComptabiliseeMinutes(cStructure);
			// c'est maintenant bon
			minutes = getDicoDureeComptabiliseeAm().objectForKey(cStructure);
		}

		return minutes.intValue();
	}

	/**
	 * @param cStructure
	 * @return
	 */
	public int dureeComptabiliseeMinutesPm(String cStructure) {
		Integer minutes = null;

		minutes = getDicoDureeComptabiliseePm().objectForKey(cStructure);
		if (minutes == null) {
			// forcer le remplissage du dico
			dureeComptabiliseeMinutes(cStructure);
			// c'est maintenant bon
			minutes = getDicoDureeComptabiliseePm().objectForKey(cStructure);
		}

		return minutes.intValue();
	}

	/**
	 * La quotité attendue pour le jour
	 */
	public float quotite(String cStructure) {

		Float quotite = null;

		if (!StringCtrl.isEmpty(cStructure)) {

			quotite = getDicoQuotite().objectForKey(cStructure);

			if (quotite == null) {

				float f_quotiteTotale = (float) 0;

				for (TravailAttendu attendu : getTravailAttendus()) {

					boolean isPlageACompter = false;

					if (StringCtrl.isEmpty(cStructure)) {
						isPlageACompter = true;
					}

					if (!isPlageACompter &&
								attendu.getCStructure().equals(cStructure)) {
						isPlageACompter = true;
					}

					if (isPlageACompter) {
						f_quotiteTotale += attendu.getQuotite().floatValue();
					}

				}

				quotite = new Float(f_quotiteTotale);
				getDicoQuotite().setObjectForKey(quotite, cStructure);
			}

		} else {

			float f_quotiteTotale = (float) 0;
			for (TravailAttendu attendu : getTravailAttendus()) {
				f_quotiteTotale += quotite(attendu.getCStructure());
			}
			quotite = new Float(f_quotiteTotale);

		}

		return quotite.floatValue();
	}

	@Override
	public int dureeRealiseeMinutes(String cStructure) {
		int dureeRealisee = 0;

		NSTimestamp now = DateCtrlHamac.now();
		NSTimestamp nowMinuit = DateCtrlHamac.remonterToMinuit(now);

		if (DateCtrlHamac.isBefore(getDate(), nowMinuit)) {

			// tout ce qui est passé est comptabilisé
			dureeRealisee = dureeAssocieeMinutes(cStructure);

		} else if (DateCtrlHamac.isSameDay(getDate(), nowMinuit)) {

			// jour j : calcul selon le temps écoulé dans la journée
			if (!StringCtrl.isEmpty(cStructure)) {

				if (!NSArrayCtrl.isEmpty(getPlageTravailArray())) {

					int minutesEcoulees = DateCtrlHamac.minutesDepuisMinuit(now) + Jour.DUREE_JOURNEE_EN_MINUTES * (dayOfWeek() - 1);

					for (HorairePlageTravail plage : getPlageTravailArray()) {

						if (plage.getCStructure().equals(cStructure)) {
							if (minutesEcoulees > plage.getDebutMinutesDansSemaine()) {
								int finDansPlage = minutesEcoulees;
								if (minutesEcoulees > plage.getFinMinutesDansSemaine()) {
									finDansPlage = plage.getFinMinutesDansSemaine();
								}
								dureeRealisee += finDansPlage - plage.getDebutMinutesDansSemaine();
							}
						}

					}
				}
			} else {
				for (TravailAttendu attendu : getTravailAttendus()) {
					dureeRealisee += dureeRealiseeMinutes(attendu.getCStructure());
				}
			}

		}

		return dureeRealisee;
	}

	@Override
	public Planning getPlanning() {
		return getSemaine().getMois().getPlanning();
	}

	/**
	 * Durée associée sur une plage horaire et sur un service
	 * 
	 * @param cStructure
	 *          service concerné
	 * @param dateDebut
	 *          date debut
	 * @param dateFin
	 *          dateFin
	 * @return
	 */
	// public int dureeAssociee(String cStructure, NSTimestamp dateDebut,
	// NSTimestamp dateFin) {
	// int minutes = 0;
	//
	// if (!NSArrayCtrl.isEmpty(getPlageTravailArray())) {
	//
	// int minutesDebut = Jour.DUREE_JOURNEE_EN_MINUTES * (dayOfWeek() - 1);
	// int minutesFin = Jour.DUREE_JOURNEE_EN_MINUTES * dayOfWeek();
	//
	// if (DateCtrlHamac.isSameDay(getDate(), dateDebut)) {
	// minutesDebut += DateCtrlHamac.minutesDepuisMinuit(dateDebut);
	// }
	// if (DateCtrlHamac.isSameDay(getDate(), dateFin)) {
	// minutesFin = Jour.DUREE_JOURNEE_EN_MINUTES * (dayOfWeek() - 1) +
	// DateCtrlHamac.minutesDepuisMinuit(dateFin);
	// }
	//
	// // determiner les minutes de début et de fin depuis minuit;
	//
	// for (HorairePlageTravail plage : getPlageTravailArray()) {
	//
	// if (plage.getCStructure().equals(cStructure)) {
	//
	// // ne prendre en compte que ce qui est dans la plage
	// if (minutesDebut <= plage.getFinMinutesDansSemaine() &&
	// minutesFin >= plage.getDebutMinutesDansSemaine()) {
	// // retailler sur la plage
	// int debutDansPlage = plage.getDebutMinutesDansSemaine();
	// if (minutesDebut > debutDansPlage) {
	// debutDansPlage = minutesDebut;
	// }
	// int finDansPlage = plage.getFinMinutesDansSemaine();
	// if (minutesFin < finDansPlage) {
	// finDansPlage = minutesFin;
	// }
	// minutes += finDansPlage - debutDansPlage;
	// }
	// }
	//
	// }
	//
	// }
	//
	// return minutes;
	// }

	/**
	 * On s'en sert pour permettre de savoir si la plage n'a pas déjà été débitée
	 * par une autre occupation
	 * 
	 * @return
	 */
	public final NSMutableArray<A_Absence> getAbsenceArray() {
		if (absenceArray == null) {
			absenceArray = new NSMutableArray<A_Absence>();
		}
		return absenceArray;
	}

	/**
	 * XXX tmp en public le temps de dev la gestion de la pause accordée sur
	 * horaire multi service
	 * 
	 * @return
	 */
	public final NSMutableArray<HorairePlageTravail> getPlageTravailArray() {
		if (plageTravailArray == null) {
			plageTravailArray = new NSMutableArray<HorairePlageTravail>();
		}
		return plageTravailArray;
	}

	/**
	 * 
	 * @return
	 */
	public final NSMutableArray<HorairePlagePause> getPlagePauseArray() {
		if (plagePauseArray == null) {
			plagePauseArray = new NSMutableArray<HorairePlagePause>();
		}
		return plagePauseArray;
	}

	public final static String PRESENCE_DEMI_JOURNEE_NON_TRAVAIL = "0";
	private final static String PRESENCE_DEMI_JOURNEE_TRAVAIL = "1";
	private final static String PRESENCE_DEMI_JOURNEE_ABSENCE = "2";
	private final static String PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE = "3";
	private final static String PRESENCE_DEMI_JOURNEE_ABSENCE_EN_COURS_VAL = "4";

	private String getPresenceHoraireDemiJournee(boolean isAm) {
		String presence = "";

		EOQualifier qual = ERXQ.isTrue(isAm ? HorairePlageTravail.IS_AM_KEY : HorairePlageTravail.IS_PM_KEY);
		if (EOQualifier.filteredArrayWithQualifier(getPlageTravailArray(), qual).count() > 0) {
			if (isAm) {
				if (isFerme() || isCongeLegalAM() || isCongeAM()) {
					if (isEnCoursDeValidation() && !getPlanning().getEoPlanning().isPrev()) {
						presence += PRESENCE_DEMI_JOURNEE_ABSENCE_EN_COURS_VAL;
					} else {
						presence += PRESENCE_DEMI_JOURNEE_ABSENCE;
					}
				} else if (isCongesCompAm()) {
					presence += PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE;
				} else {
					presence += PRESENCE_DEMI_JOURNEE_TRAVAIL;
				}
			} else {
				if (isFerme() || isCongeLegalPM() || isCongePM()) {
					if (isEnCoursDeValidation() && !getPlanning().getEoPlanning().isPrev()) {
						presence += PRESENCE_DEMI_JOURNEE_ABSENCE_EN_COURS_VAL;
					} else {
						presence += PRESENCE_DEMI_JOURNEE_ABSENCE;
					}
				} else if (isCongesCompPm()) {
					presence += PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE;
				} else {
					presence += PRESENCE_DEMI_JOURNEE_TRAVAIL;
				}
			}
		} else {
			presence += PRESENCE_DEMI_JOURNEE_NON_TRAVAIL;
		}

		return presence;
	}

	private final static String CLASSE_CSS_TRAVAIL = "travail";
	private final static String CLASSE_CSS_ABSENCE = "absence";
	private final static String CLASSE_CSS_ABSENCE_PARTIELLE = "absencePartielle";
	private final static String CLASSE_CSS_TRAVAIL_PREV = "travailPrev";
	private final static String CLASSE_CSS_ABSENCE_PREV = "absencePrev";
	private final static String CLASSE_CSS_ABSENCE_EN_COURS_DE_VAL = "absenceEnCoursDeVal";

	private final static String CODE_TRAVAIL_REEL = "T";
	private final static String CODE_ABSENCE_REEL = "A";
	private final static String CODE_ABSENCE_EN_COURS_VAL = "V";
	private final static String CODE_NON_TRAVAIL = "N";
	private final static String CODE_TRAVAIL_PREV = "P";
	private final static String CODE_ABSENCE_PREV = "B";

	public final static String getClasseCssPourPresence(
			boolean isReel, String presence) {
		return getCodePourPresence(isReel, presence, true);
	}

	public final static String getCodePourPresence(boolean isReel, String presence) {
		return getCodePourPresence(isReel, presence, false);
	}

	private final static String getCodePourPresence(boolean isReel, String presence, boolean isClasseCss) {
		String code = "";

		if (presence.equals(PRESENCE_DEMI_JOURNEE_NON_TRAVAIL)) {
			if (isClasseCss) {

			} else {
				code = CODE_NON_TRAVAIL;
			}
		} else if (presence.equals(PRESENCE_DEMI_JOURNEE_TRAVAIL)) {
			if (isClasseCss) {
				if (isReel) {
					code = CLASSE_CSS_TRAVAIL;
				} else {
					code = CLASSE_CSS_TRAVAIL_PREV;
				}
			} else {
				if (isReel) {
					code = CODE_TRAVAIL_REEL;
				} else {
					code = CODE_TRAVAIL_PREV;
				}
			}
		} else if (presence.equals(PRESENCE_DEMI_JOURNEE_ABSENCE)) {
			if (isClasseCss) {
				if (isReel) {
					code = CLASSE_CSS_ABSENCE;
				} else {
					code = CLASSE_CSS_ABSENCE_PREV;
				}
			} else {
				if (isReel) {
					code = CODE_ABSENCE_REEL;
				} else {
					code = CODE_ABSENCE_PREV;
				}
			}
		} else if (presence.equals(PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE)) {
			if (isClasseCss) {
				code = CLASSE_CSS_ABSENCE_PARTIELLE;
			} else {

			}
		} else if (presence.equals(PRESENCE_DEMI_JOURNEE_ABSENCE_EN_COURS_VAL)) {
			if (isClasseCss) {
				code = CLASSE_CSS_ABSENCE_EN_COURS_DE_VAL;
			} else {
				code = CODE_ABSENCE_EN_COURS_VAL;
			}
		}

		return code;
	}

	/**
	 * Le jour au format "presence", i.e, "00", "10" ... pour l'affichage du
	 * planning de service
	 * 
	 * @return
	 */
	public String getPresenceHoraireDemiJourneeAm() {
		return getPresenceHoraireDemiJournee(true);
	}

	/**
	 * Le jour au format "presence", i.e, "00", "10" ... pour l'affichage du
	 * planning de service
	 * 
	 * @return
	 */
	public String getPresenceHoraireDemiJourneePm() {
		return getPresenceHoraireDemiJournee(false);
	}

	@Override
	public String toString() {
		String toString = "";

		toString = "Jour " + getDate() + " " + hashCode();

		return toString;
	}

	public String getCode() {
		if (code == null) {
			code = DateCtrlHamac.dateToString(getDate(), CODE_JOUR_DATE_FORMAT);
		}
		return code;
	}

	/**
	 * Nombre de demi journée de congé légal
	 * 
	 * @return
	 */
	public int nbDemiJourneeCongeLegal() {
		int nb = 0;

		nb = nbDemiJourneeConge(true);

		return nb;
	}

	/**
	 * Nombre de demi journée de congé maladie
	 * 
	 * @return
	 */
	public int nbDemiJourneeCongeMaladie() {
		int nb = 0;

		nb = nbDemiJourneeConge(true, true);

		return nb;
	}

	/**
	 * Nombre de demi journée de congé
	 * 
	 * @return
	 */
	public int nbDemiJourneeConge() {
		int nb = 0;

		nb = nbDemiJourneeConge(false);

		return nb;
	}

	/**
	 * 
	 * @param isLegal
	 *          : indique s'il faut regarder les congés légaux ou non
	 * @param isMaladie
	 *          : si isLegal est à <code>true</code>, alors ne se concentrer que
	 *          sur les congés maladie TODO
	 * @return
	 */
	private int nbDemiJourneeConge(
			boolean isLegal, boolean... isMaladieArray) {
		int nb = 0;

		if (isLegal) {

			boolean isMaladieUniquement = false;

			if (isMaladieArray.length > 0) {
				isMaladieUniquement = isMaladieArray[0];
			}

			if (isMaladieUniquement) {
				// maladie uniquement

				if (hasStatutCongeMaladieAM()) {
					nb++;
				}
				if (hasStatutCongeMaladiePM()) {
					nb++;
				}

			} else {
				// legal et maladie

				if (hasStatutCongeLegalAM() ||
						hasStatutCongeMaladieAM()) {
					nb++;
				}
				if (hasStatutCongeLegalPM() ||
						hasStatutCongeMaladiePM()) {
					nb++;
				}

			}

		} else {

			// conges non légaux

			if (hasStatutCongeAM()) {
				nb++;
			}
			if (hasStatutCongePM()) {
				nb++;
			}
		}

		return nb;
	}

	/**
	 * TODO ne fonctionne pas avec les horaires forcés ?
	 * 
	 * La durée total de présence, en soustrayant toutes les occupations et
	 * absences :
	 * 
	 * - horaires
	 * 
	 * - heures supp
	 * 
	 * 
	 * @return
	 */
	public int dureePresence() {
		int duree = 0;

		for (HorairePlageTravail hpt : getPlageTravailArray()) {
			duree += hpt.getDureeMinutes();
			for (PlageOccupation po : hpt.getPlageOccupationArray()) {
				duree += po.dureeCumuleeSegments(this);
			}
		}

		return duree;
	}

	/**
	 * Permet de savoir si la pause RTT peut être accordée : durée de travail
	 * comptabilisée >= 6h00
	 * 
	 * @return
	 */
	public boolean isPauseRTTAccordable() {
		boolean isPauseRTTAccordable = false;

		isPauseRTTAccordable = HoraireJournalier.isPauseRTTAccordableForDuree(dureeComptabiliseeMinutes());

		return isPauseRTTAccordable;
	}

	/**
	 * Permet de savoir si la pause RTT peut être octroyée ou non : durée de
	 * présence effective >= 6h00
	 * 
	 * @return
	 */
	public boolean isPauseRTTAccordee() {
		boolean isPauseRTTAccordee = false;

		isPauseRTTAccordee = HoraireJournalier.isPauseRTTAccordableForDuree(dureePresence());

		return isPauseRTTAccordee;
	}

	/**
	 * TODO tmp pour dev : affichage d'informations sur les horaires et les débits
	 * effectués
	 * 
	 * @return
	 */
	public String title() {
		String title = "";

		for (HorairePlageTravail hpt : getPlageTravailArray()) {

			if (!StringCtrl.isEmpty(title)) {
				title += " ";
			}

			title += hpt.toShortString();

			//

		}

		return title;
	}

	// affichage css

	private final static String CLASSE_CSS_JOUR = "jour";
	private final static String CLASSE_CSS_HORS_PLANNING = "horsPlanning";
	private final static String CLASSE_CSS_FERIE = "ferie";
	private final static String CLASSE_CSS_CHOME = "chome";
	private final static String CLASSE_CSS_CONGE = "conge";
	private final static String CLASSE_CSS_CONGE_MINUTE = "congeMinute";
	private final static String CLASSE_CSS_CONGE_AM = "congeAM";
	private final static String CLASSE_CSS_CONGE_PM = "congePM";
	private final static String CLASSE_CSS_CONGE_LEGAL = "congeLegal";
	private final static String CLASSE_CSS_CONGE_LEGAL_AM = "congeLegalAM";
	private final static String CLASSE_CSS_CONGE_LEGAL_PM = "congeLegalPM";
	private final static String CLASSE_CSS_FERME = "ferme";
	private final static String CLASSE_CSS_HORAIRE_FORCE = "horaireForce";

	private final static String CLASSE_CSS_CONGE_EN_COURS_DE_VALIDATION = "congeV";
	private final static String CLASSE_CSS_CONGE_EN_COURS_DE_SUPPRESSION = "congeS";
	private final static String CLASSE_CSS_CONGE_AM_EN_COURS_DE_VALIDATION = "congeAMV";
	private final static String CLASSE_CSS_CONGE_AM_EN_COURS_DE_SUPPRESSION = "congeAMS";
	private final static String CLASSE_CSS_CONGE_PM_EN_COURS_DE_VALIDATION = "congePMV";
	private final static String CLASSE_CSS_CONGE_PM_EN_COURS_DE_SUPPRESSION = "congePMS";

	/**
	 * classe css d'un jour sur le planning pour mise en valeur lors de la
	 * validation
	 */
	public final static String CLASSE_CSS_HIGHLIGHT_TODAY = "occToday";
	public final static String CLASSE_CSS_HIGHLIGHT_OCC_FIRST_DAY = "occFirstDay";
	public final static String CLASSE_CSS_HIGHLIGHT_OCC_MIDDLE_DAY = "occMiddleDay";
	public final static String CLASSE_CSS_HIGHLIGHT_OCC_LAST_DAY = "occLastDay";
	public final static String CLASSE_CSS_HIGHLIGHT_OCC_ONLY_DAY = "occOnlyDay";

	/**
	 * La liste des classes
	 * 
	 * @return
	 */
	public String getClasseCss() {
		String classeCss = "";

		// mise en valeur
		if (isHighlightToday()) {
			classeCss += CLASSE_CSS_HIGHLIGHT_TODAY;
		} else if (isHighlightOccFirstDay()) {
			classeCss += CLASSE_CSS_HIGHLIGHT_OCC_FIRST_DAY;
		} else if (isHighlightOccMiddleDay()) {
			classeCss += CLASSE_CSS_HIGHLIGHT_OCC_MIDDLE_DAY;
		} else if (isHighlightOccLastDay()) {
			classeCss += CLASSE_CSS_HIGHLIGHT_OCC_LAST_DAY;
		} else if (isHighlightOccOnlyDay()) {
			classeCss += CLASSE_CSS_HIGHLIGHT_OCC_ONLY_DAY;
		} else {
			classeCss = CLASSE_CSS_JOUR;
		}

		classeCss += " ";

		//
		if (isHorsAffectation()) {
			classeCss += CLASSE_CSS_HORS_PLANNING;
		} else if (isChome()) {
			classeCss += CLASSE_CSS_CHOME;
		} else if (isFerie()) {
			classeCss += CLASSE_CSS_FERIE;
		} else if (isCongeLegalAM() && isCongeLegalPM()) {
			classeCss += CLASSE_CSS_CONGE_LEGAL;
		} else if (isCongeLegalAM()) {
			classeCss += CLASSE_CSS_CONGE_LEGAL_AM;
		} else if (isCongeLegalPM()) {
			classeCss += CLASSE_CSS_CONGE_LEGAL_PM;
		} else if (isFerme()) {
			classeCss += CLASSE_CSS_FERME;
		} else if (isCongeAM() && isCongePM()) {
			if (isEnCoursDeValidation()) {
				classeCss += CLASSE_CSS_CONGE_EN_COURS_DE_VALIDATION;
			} else if (isEnCoursDeSuppression()) {
				classeCss += CLASSE_CSS_CONGE_EN_COURS_DE_SUPPRESSION;
			} else {
				classeCss += CLASSE_CSS_CONGE;
			}
		} else if (isCongeAM()) {
			if (isEnCoursDeValidation()) {
				classeCss += CLASSE_CSS_CONGE_AM_EN_COURS_DE_VALIDATION;
			} else if (isEnCoursDeSuppression()) {
				classeCss += CLASSE_CSS_CONGE_AM_EN_COURS_DE_SUPPRESSION;
			} else {
				classeCss += CLASSE_CSS_CONGE_AM;
			}
		} else if (isCongePM()) {
			if (isEnCoursDeValidation()) {
				classeCss += CLASSE_CSS_CONGE_PM_EN_COURS_DE_VALIDATION;
			} else if (isEnCoursDeSuppression()) {
				classeCss += CLASSE_CSS_CONGE_PM_EN_COURS_DE_SUPPRESSION;
			} else {
				classeCss += CLASSE_CSS_CONGE_PM;
			}
		} else if (isCongesCompAm() || isCongesCompPm()) {
			classeCss += CLASSE_CSS_CONGE_MINUTE;
		}

		return classeCss;
	}

	public final String getClasseCssVacanceScolaire() {
		String classeCss = "nvac";

		if (hasStatutVacanceScolaire()) {
			classeCss = "vac";
		}

		return classeCss;
	}

	// controle de chevauchement

	/**
	 * Indique s'il est possible de déposer l'occupation
	 */
	public boolean isDisponible(A_Absence absence) {

		// vérifier le non chevauchement avec d'autres présences à la minute
		if (absence.isAbsenceMinute() &&
				absence.isPresence()) {

			EOQualifier qual = ERXQ.and(
							ERXQ.isTrue(A_Absence.IS_PRESENCE_KEY),
							ERXQ.or(
									ERXQ.between(A_Absence.DATE_DEBUT_KEY, absence.dateDebut(), absence.dateFin(), false),
									ERXQ.between(A_Absence.DATE_FIN_KEY, absence.dateDebut(), absence.dateFin(), false),
									ERXQ.and(
											ERXQ.lessThanOrEqualTo(A_Absence.DATE_DEBUT_KEY, absence.dateDebut()),
											ERXQ.greaterThanOrEqualTo(A_Absence.DATE_FIN_KEY, absence.dateFin()))));

			NSArray<A_Absence> absencePresenceArray = EOQualifier.filteredArrayWithQualifier(
						getAbsenceArray(), qual);

			if (absencePresenceArray.count() > 0) {
				return false;
			}

			return true;
		}

		// jour férié et chomé
		if (isChome() || isFerie()) {
			return true;
		}

		// absence à la minute
		if (absence.isAbsenceMinute()) {

			//
			for (HorairePlageTravail hpt : getPlageTravailArray()) {

				NSArray<PlageOccupation> poArray = hpt.getPlageOccupationArrayForJour(this);

				if (poArray.count() > 0) {

					for (PlageOccupation po : poArray) {

						NSTimestamp deb = po.getAbsence().dateDebut();
						NSTimestamp fin = po.getAbsence().dateFin();

						NSTimestamp debNew = absence.dateDebut();
						NSTimestamp finNew = absence.dateFin();

						if ((DateCtrlHamac.isAfter(debNew, deb) && DateCtrlHamac.isBefore(debNew, fin)) ||
								(DateCtrlHamac.isAfter(finNew, deb) && DateCtrlHamac.isBefore(finNew, fin)) ||
								(DateCtrlHamac.isAfter(debNew, deb) && DateCtrlHamac.isBefore(finNew, fin))) {
							return false;
						}

					}

				} else {

					return true;

				}

			}

			return true;

		}

		// absence à la demi journée
		NSTimestamp debutMinuit = DateCtrlHamac.remonterToMinuit(absence.dateDebut());
		NSTimestamp finMinuit = DateCtrlHamac.remonterToMinuit(absence.dateFin());

		//
		for (HorairePlageTravail hpt : getPlageTravailArray()) {

			NSArray<PlageOccupation> poArray = hpt.getPlageOccupationArrayForJour(this);

			// exclure l'absence en cours de saisie
			int poCount = 0;
			for (PlageOccupation po : poArray) {
				if (po.getAbsence() != absence) {
					poCount++;
				}
			}

			// int poCount = hpt.getPlageOccupationArrayForJour(this).count();

			// jour inclus dans l'absence
			if (DateCtrl.isBefore(debutMinuit, getDate()) &&
					DateCtrl.isAfter(finMinuit, getDate())) {

				if (poCount > 0) {
					return false;
				}

			} else {

				// jour en limite d'absence

				// commence et termine ce jour
				if (DateCtrl.isSameDay(absence.dateDebut(), debutMinuit) &&
						DateCtrl.isSameDay(absence.dateFin(), debutMinuit)) {

					// matin
					if (poCount > 0 && hpt.isAm() && absence.isDebuteLeMatin()) {
						return false;
					}

					// après midi
					if (poCount > 0 && hpt.isPm() && absence.isTermineLApresMidi()) {
						return false;
					}

				} else if (DateCtrl.isSameDay(absence.dateDebut(), debutMinuit)) {
					// commence ce jour

					if (poCount > 0 && (
								hpt.isAm() && (absence.isDebuteLeMatin() || absence.isDebuteLApresMidi()) ||
								hpt.isPm() && (absence.isDebuteLApresMidi()))) {
						return false;
					}

				} else if (DateCtrl.isSameDay(absence.dateFin(), debutMinuit)) {
					// termine ce jour

					if (poCount > 0 && (
							hpt.isAm() && (absence.isTermineLeMatin()) ||
							hpt.isPm() && (absence.isTermineLeMatin() || absence.isTermineLApresMidi()))) {
						return false;

					}

				}

			}

		}

		return true;
	}

	private NSMutableDictionary<String, Integer> _dicoNbJourOuvre;

	private NSMutableDictionary<String, Integer> getDicoNbJourOuvre() {
		if (_dicoNbJourOuvre == null) {
			_dicoNbJourOuvre = new NSMutableDictionary<String, Integer>();
		}
		return _dicoNbJourOuvre;
	}

	@Override
	public int nbJourOuvre(String cStructure) {
		Integer nb = getDicoNbJourOuvre().objectForKey(cStructure);

		if (nb == null) {
			int i_nb = 0;

			if (isOuvre()) {
				EOQualifier qualAttenduStructure = ERXQ.equals(TravailAttendu.C_STRUCTURE_KEY, cStructure);
				if (EOQualifier.filteredArrayWithQualifier(getTravailAttendus(), qualAttenduStructure).count() > 0) {
					i_nb = 1;
				}
			}

			nb = Integer.valueOf(i_nb);
			getDicoNbJourOuvre().setObjectForKey(nb, cStructure);
		}

		return nb.intValue();
	}

	private Boolean _isOuvre;

	/**
	 * Indique si le jour est ouvré, indipendamment de quelconque structure
	 * d'affectation
	 * 
	 * @return
	 */
	private boolean isOuvre() {
		if (_isOuvre == null) {
			if (!isFerie() && !isChome() && !isSamedi() && !isDimanche()) {
				_isOuvre = Boolean.TRUE;
			} else {
				_isOuvre = Boolean.FALSE;
			}
		}
		return _isOuvre.booleanValue();
	}

	private Boolean _isOuvrable;

	/**
	 * Indique si le jour est ouvrable, indipendamment de quelconque structure
	 * d'affectation
	 * 
	 * @return
	 */
	public boolean isOuvrable() {
		if (_isOuvrable == null) {
			if (!isFerie() && !isChome() && !isDimanche()) {
				_isOuvrable = Boolean.TRUE;
			} else {
				_isOuvrable = Boolean.FALSE;
			}
		}
		return _isOuvrable.booleanValue();
	}

	private NSMutableDictionary<String, Integer> _dicoNbJourAttendu;

	private NSMutableDictionary<String, Integer> getDicoNbJourAttendu() {
		if (_dicoNbJourAttendu == null) {
			_dicoNbJourAttendu = new NSMutableDictionary<String, Integer>();
		}
		return _dicoNbJourAttendu;
	}

	private NSMutableDictionary<TravailAttendu, Integer> _dicoNbJourTravailAttendu;

	private NSMutableDictionary<TravailAttendu, Integer> getDicoNbJourTravailAttendu() {
		if (_dicoNbJourTravailAttendu == null) {
			_dicoNbJourTravailAttendu = new NSMutableDictionary<TravailAttendu, Integer>();
		}
		return _dicoNbJourTravailAttendu;
	}

	@Override
	public int nbJourAttendu(String cStructure) {
		Integer nb = getDicoNbJourAttendu().objectForKey(cStructure);
		if (nb == null) {
			int i_nb = 0;
			EOQualifier qualAttenduStructure = ERXQ.equals(TravailAttendu.C_STRUCTURE_KEY, cStructure);
			if (EOQualifier.filteredArrayWithQualifier(getTravailAttendus(), qualAttenduStructure).count() > 0) {
				i_nb = 1;
			}
			nb = Integer.valueOf(i_nb);
			getDicoNbJourAttendu().setObjectForKey(nb, cStructure);
		}
		return nb.intValue();
	}

	public int nbJourAttendu(TravailAttendu travailAttendu) {
		Integer nb = getDicoNbJourAttendu().objectForKey(travailAttendu);
		if (nb == null) {
			int i_nb = 0;
			if (getTravailAttendus().containsObject(travailAttendu)) {
				i_nb = 1;
			}
			nb = Integer.valueOf(i_nb);
			getDicoNbJourTravailAttendu().setObjectForKey(nb, travailAttendu);
		}
		return nb.intValue();
	}

	@Override
	public int nbJourOuvre() {
		int n = 0;

		if (isOuvre()) {
			n = 1;
		}

		return n;
	}

	@Override
	public int nbJourOuvre(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int n = 0;

		int nbJourOuvre = nbJourOuvre();

		if (nbJourOuvre > 0) {

			boolean isAComptabiliser = false;

			if (dateDebut != null && dateFin != null) {
				if (DateCtrlHamac.isAfterEq(getDate(), dateDebut) &&
						DateCtrlHamac.isBeforeEq(getDate(), dateFin)) {
					isAComptabiliser = true;
				}
			} else if (dateDebut != null) {
				if (DateCtrlHamac.isAfterEq(getDate(), dateDebut)) {
					isAComptabiliser = true;
				}
			} else if (dateFin != null) {
				if (DateCtrlHamac.isBeforeEq(getDate(), dateFin)) {
					isAComptabiliser = true;
				}
			} else {
				isAComptabiliser = true;
			}

			if (isAComptabiliser) {
				n = 1;
			}
		}

		return n;
	}

	@Override
	public int nbJourOuvrable() {
		int n = 0;

		if (isOuvrable()) {
			n = 1;
		}

		return n;
	}

	/**
	 * Indique si le jour est actif (non férié, non chomé ...)
	 * 
	 * @return
	 */
	public boolean isActif() {
		boolean isActif = true;

		if (hasStatutChome() ||
				hasStatutFerie() ||
				hasStatutHorsAffectation()/*
																	 * || hasStatutSansStatut()
																	 */) {
			isActif = false;
		}

		return isActif;
	}

	/**
	 * Indique si le matin est travaillé
	 * 
	 * @return
	 */
	public boolean isTravailleAm() {
		boolean isTravailleAm = false;

		if (getPresenceHoraireDemiJourneeAm().equals(PRESENCE_DEMI_JOURNEE_TRAVAIL) ||
				getPresenceHoraireDemiJourneeAm().equals(PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE)) {
			isTravailleAm = true;
		}

		return isTravailleAm;
	}

	/**
	 * Indique si l'apres midi est travaillé
	 * 
	 * @return
	 */
	public boolean isTravaillePm() {
		boolean isTravaillePm = false;

		if (getPresenceHoraireDemiJourneePm().equals(PRESENCE_DEMI_JOURNEE_TRAVAIL) ||
				getPresenceHoraireDemiJourneePm().equals(PRESENCE_DEMI_JOURNEE_ABSENCE_PARTIELLE)) {
			isTravaillePm = true;
		}

		return isTravaillePm;
	}

	/**
	 * Le nombre de demi journées de travail
	 * 
	 * @return
	 */
	public int nbDemiJourneesTravaillees() {
		int n = 0;

		if (isTravailleAm()) {
			n++;
		}

		if (isTravaillePm()) {
			n++;
		}

		return n;
	}

	/**
	 * Indique si le jour tombe un samedi et s'il est travaillé le matin
	 * 
	 * @param jour
	 * @return
	 */
	public boolean isTravailleSamediMatin() {
		boolean isTravailleSamediMatin = false;

		if (isSamedi() &&
				isTravailleAm() &&
				isCongeAM() == false &&
				isCongeLegalAM() == false &&
				isFerme() == false) {
			isTravailleSamediMatin = true;
		}

		return isTravailleSamediMatin;
	}

	/**
	 * Indique si le jour tombe un samedi et s'il est travaillé l'après midi
	 * 
	 * @param jour
	 * @return
	 */
	public boolean isTravailleSamediApresMidi() {
		boolean isTravailleSamediApresMidi = false;

		if (isSamedi() &&
				isTravaillePm() &&
				isCongePM() == false &&
				isCongeLegalPM() == false &&
				isFerme() == false) {
			isTravailleSamediApresMidi = true;
		}

		return isTravailleSamediApresMidi;
	}

	/**
	 * L'horaire de travail du matin
	 * 
	 * @return
	 */
	public final HorairePlageTravail getPlageTravailAm() {
		HorairePlageTravail hptAm = null;

		NSArray<HorairePlageTravail> array = EOQualifier.filteredArrayWithQualifier(
				getPlageTravailArray(), ERXQ.isTrue(HorairePlageTravail.IS_AM_KEY));

		if (array.count() > 0) {
			hptAm = array.objectAtIndex(0);
		}

		return hptAm;
	}

	/**
	 * L'horaire de travail de l'apres midi
	 * 
	 * @return
	 */
	public final HorairePlageTravail getPlageTravailPm() {
		HorairePlageTravail hptPm = null;

		NSArray<HorairePlageTravail> array = EOQualifier.filteredArrayWithQualifier(
				getPlageTravailArray(), ERXQ.isTrue(HorairePlageTravail.IS_PM_KEY));

		if (array.count() > 0) {
			hptPm = array.objectAtIndex(0);
		}

		return hptPm;
	}

	private int getMinutesTavailleesMini() {
		return HamacCktlConfig.intForKey(EOParametre.MINUTES_HEBDO_MINI_HOR_BONUS, getPlanning().getEoPlanning().toPeriode());
	}
	
	private int getDebutJourneeBonus() {
		return HamacCktlConfig.intForKey(EOParametre.DEBUT_JOURNEE_BONUS, getPlanning().getEoPlanning().toPeriode());
	}
	
	private int getFinJourneeBonus() {
		return HamacCktlConfig.intForKey(EOParametre.FIN_JOURNEE_BONUS, getPlanning().getEoPlanning().toPeriode());
	}
	
	private int getCoefSamediMatin() {
		return HamacCktlConfig.intForKey(EOParametre.COEF_SAMEDI_MATIN_BONUS, getPlanning().getEoPlanning().toPeriode());
	}
	
	private int getCoefDebordement() {
		return HamacCktlConfig.intForKey(EOParametre.COEF_DEBORDEMENT_BONUS, getPlanning().getEoPlanning().toPeriode());
	}
	
	private float getCoefHsupSamApremDimJf() {
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_SAM_APREM_DIM_JF, getPlanning().getEoPlanning().toPeriode());
	}
	
	/**
	 * retoune le bonus en minutes pour une journée de travail
	 * 
	 * @param heuresJour
	 * @param jour
	 * @return
	 */
	public int getBonusHoraire() {

		int bonus = 0;

		if (getPlageTravailArray().count() > 0) {

			if (isLundi() ||
					isMardi() ||
					isMercredi() ||
					isJeudi() ||
					isVendredi()) {

				int cumul = 0;

				// matin
				if (isTravailleAm() &&
						!isCongeAM() &&
						!isCongeLegalAM() &&
						!isFerme()) {

					cumul += cumulMatinAvant(getDebutJourneeBonus());

				}

				// apres midi
				if (isTravaillePm() &&
						!isCongePM() &&
						!isCongeLegalPM() &&
						!isFerme()) {

					cumul += cumulApresMidiApres(getFinJourneeBonus());

				}

				if (cumul >= getMinutesTavailleesMini()) {
					bonus += (int) ((float) cumul * (getCoefDebordement() - (float) 1));
				}

			} else if (isSamedi()) {

				if (getSemaine().isSamediMatinABonifier()) {
					// pour pas de plafond
					bonusHoraireSamediMatin = (int) ((float) cumulMatinAvant(getPlageTravailAm().getFinMinutesDansJour()) * (getCoefSamediMatin() - (float) 1));
					bonus += bonusHoraireSamediMatin;
				}

				if (getSemaine().isSamediApresMidiABonifier()) {
					// pour pas de seuil
					bonusHoraireSamediApresMidi = (int) ((float) cumulApresMidiApres(getPlageTravailPm().getDebutMinutesDansJour()) * (getCoefHsupSamApremDimJf() - (float) 1));
					bonus += bonusHoraireSamediApresMidi;
				}

			}

		}

		return bonus;

	}

	/**
	 * @param cumul
	 * @return
	 */
	private int cumulApresMidiApres(int finJourneeBonus) {

		int cumul = 0;

		HorairePlageTravail pm = getPlageTravailPm();
		// System.out.println(toString() + " °°°°° am:" + pm.toString());

		// determiner le restant libre
		NSArray<A_Segment> segmentArray = new NSArray<A_Segment>();
		NSArray<PlageOccupation> poArray = pm.getPlageOccupationArrayForJour(this);
		for (PlageOccupation po : poArray) {
			segmentArray = segmentArray.arrayByAddingObjectsFromArray(po.getDispo());
		}

		if (segmentArray.count() > 0) {

			// dispo en partie
			for (A_Segment segment : segmentArray) {
				// System.out.println(toString() + " dispo => " +
				// segment.toString());

				if (segment.getFin() > finJourneeBonus) {

					if (segment.getDebut() > finJourneeBonus) {
						cumul += segment.getFin() - segment.getDebut();
					} else {
						cumul += segment.getFin() - finJourneeBonus;
					}

				}

			}

		} else {
			// completement dispo
			// System.out.println(toString() + " tout dispo => ");

			// apres 19h00
			if (pm.getFinMinutesDansJour() > finJourneeBonus) {
				if (pm.getDebutMinutesDansJour() > finJourneeBonus) {
					cumul += pm.getFinMinutesDansJour() - pm.getDebutMinutesDansJour();
				} else {
					cumul += pm.getFinMinutesDansJour() - finJourneeBonus;
				}
			}

		}
		return cumul;
	}

	/**
	 * @param cumul
	 * @return
	 */
	private final int cumulMatinAvant(int debutJourneeBonus) {

		int cumul = 0;

		HorairePlageTravail am = getPlageTravailAm();
		// System.out.println(toString() + " °°°°° am:" + am.toString());

		// determiner le restant libre
		NSArray<A_Segment> segmentArray = new NSArray<A_Segment>();
		NSArray<PlageOccupation> poArray = am.getPlageOccupationArrayForJour(this);
		for (PlageOccupation po : poArray) {
			segmentArray = segmentArray.arrayByAddingObjectsFromArray(po.getDispo());
		}

		if (segmentArray.count() > 0) {
			// dispo en partie
			for (A_Segment segment : segmentArray) {
				// System.out.println(toString() + " dispo => " +
				// segment.toString());

				if (segment.getDebut() < debutJourneeBonus) {

					if (segment.getFin() < debutJourneeBonus) {
						cumul += segment.getFin() - segment.getDebut();
					} else {
						cumul += debutJourneeBonus - segment.getDebut();
					}

				}

			}
		} else {
			// completement dispo
			// System.out.println(toString() + " tout dispo => ");

			// avant 7h00
			if (am.getDebutMinutesDansJour() < debutJourneeBonus) {
				cumul += debutJourneeBonus - am.getDebutMinutesDansJour();
			}

		}
		return cumul;
	}

	private int bonusHoraireSamediMatin, bonusHoraireSamediApresMidi;

	public int getBonusHoraireSamediMatin() {
		return bonusHoraireSamediMatin;
	}

	public int getBonusHoraireSamediApresMidi() {
		return bonusHoraireSamediApresMidi;
	}

	/**
	 * La valeur horaire converte par un congé maladie
	 * 
	 * @return
	 */
	public int getDureeCongeMaladie() {
		int minutes = 0;

		// les congés maladie ne sont qu'à la journée ...
		if (hasStatutCongeMaladieAM() && hasStatutCongeMaladiePM()) {
			minutes = dureeAssocieeMinutes(null);
		}

		return minutes;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCongeLegalJourneeComplete() {
		return isCongeLegalAM() && isCongeLegalPM();
	}

	/**
	 * Pour les impots, il faut donner le nombre de jours travailles. Cette
	 * methode indique si ce jour est concerne.
	 */
	public boolean isJourTravailleImpot() {
		boolean isJourTravailleImpot = false;

		if (dureeComptabiliseeMinutes() > 0 &&
				!isFerme() &&
				!(isCongeAM() && isCongePM()) &&
				!isCongeLegalJourneeComplete()) {
			isJourTravailleImpot = true;
		}

		return isJourTravailleImpot;
	}

	/**
	 * Est-ce que le jour contient une occupation
	 * 
	 * @return
	 */
	public final boolean isJourOccupeConge() {
		boolean isJourOccupeConge = false;

		if (isCongeAM() ||
				isCongePM() ||
				isCongeLegalJourneeComplete() ||
				isCongeLegalAM() ||
				isCongeLegalPM() ||
				isFerme() ||
				isCongesCompAm() ||
				isCongesCompPm()) {
			isJourOccupeConge = true;
		}

		return isJourOccupeConge;
	}

	/**
	 * Indique le nombre de minutes résiduelles hors horaires pour la période
	 * spécifiée en paramètre. Cette méthode est utile pour la gestion des heures
	 * supplémentaires adossées à des congés
	 * 
	 * @param mDebut
	 * @param mFin
	 * @return
	 */
	public final int getMinutesHorsHoraires(int mDebut, int mFin) {
		int minutes = mFin - mDebut;

		for (HorairePlageTravail hpt : getPlageTravailArray()) {

			if (!(mFin < hpt.getDebutMinutesDansJour()) &&
					!(mDebut > hpt.getFinMinutesDansJour())) {

				if (mDebut >= hpt.getDebutMinutesDansJour() &&
						mFin <= hpt.getFinMinutesDansJour()) {
					minutes = 0;
				} else if (mDebut <= hpt.getDebutMinutesDansJour() &&
						mFin >= hpt.getFinMinutesDansJour()) {
					minutes -= hpt.getFinMinutesDansJour() - hpt.getDebutMinutesDansJour();
				} else if (mDebut <= hpt.getDebutMinutesDansJour() &&
						mFin <= hpt.getFinMinutesDansJour()) {
					minutes -= mFin - hpt.getDebutMinutesDansJour();
				} else {
					minutes -= hpt.getFinMinutesDansJour() - mDebut;
				}

			}

		}

		return minutes;
	}
}
