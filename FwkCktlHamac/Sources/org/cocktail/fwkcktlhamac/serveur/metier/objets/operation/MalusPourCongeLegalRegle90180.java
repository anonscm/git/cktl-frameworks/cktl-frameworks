/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class MalusPourCongeLegalRegle90180
		extends A_MalusPourCongeLegal {

	/**
	 * @param minutes
	 */
	public MalusPourCongeLegalRegle90180(int minutes) {
		super(minutes);
	}

	@Override
	public String libelle() {
		return "Malus congés légaux (règle 90j/180j)";
	}

}
