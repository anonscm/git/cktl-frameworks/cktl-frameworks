/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class BonificationHoraireSamediApresMidi
		extends A_BonificationHoraire {

	private int numeroSemaine;

	/**
	 * @param minutes
	 */
	public BonificationHoraireSamediApresMidi(int numeroSemaine, int minutes) {
		super(minutes);
		this.numeroSemaine = numeroSemaine;
	}

	@Override
	public String libelleBonification() {
		return "travail du samedi après midi S" + numeroSemaine;
	}

}
