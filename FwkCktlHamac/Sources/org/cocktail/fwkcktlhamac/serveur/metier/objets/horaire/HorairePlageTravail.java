/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageOccupation;
import org.cocktail.fwkcktlhamac.serveur.util.HoraireFieldCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Plage de travail sur un service.
 * 
 * TODO interdire le chevauchement sur 2 jours
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HorairePlageTravail
		extends A_HorairePlageJournalier {

	public final static String IS_AM_KEY = "isAm";
	public final static String IS_PM_KEY = "isPm";
	public final static String C_STRUCTURE_KEY = "cStructure";

	// la structure d'affectation
	private String cStructure;

	// indique l'horaire est sur la matinée ou l'apres midi
	private boolean isAm;

	// liste des PlageOccupation associées
	private NSMutableArray<PlageOccupation> plageOccupationArray;

	// la pause si celle ci est inclue ou accolée dans la plage
	private HorairePlagePause horairePlagePause;

	// si horairePlagePause est non vide, alors si celle ci est inclue ou accolée
	private boolean isPauseInclue;

	/**
	 * 
	 * @param horaire
	 * @param strChaineBrute
	 */
	public HorairePlageTravail(
			Horaire horaire, String strChaineBrute, boolean isAm) {
		super(horaire, strChaineBrute);
		setHoraire(horaire);
		setIsAm(isAm);
		setDebutMinutesDansSemaine(Integer.parseInt(HoraireFieldCtrl.getValeur(getStrChaineBrute(), 0)));
		setFinMinutesDansSemaine(Integer.parseInt(HoraireFieldCtrl.getValeur(getStrChaineBrute(), 1)));
		setStructure(HoraireFieldCtrl.getCle(getStrChaineBrute()));
		affecteHoraireJournalier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXCustomObject#toString()
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(getHoraireJournalier().getLibelle());
		sb.append(" ");
		sb.append(TimeCtrl.stringForMinutes(getDebutMinutesDansJour()));
		sb.append("-");
		sb.append(TimeCtrl.stringForMinutes(getFinMinutesDansJour()));

		if (getHoraire().getEoHoraire() != null) {
			sb.append(" service=" + getHoraire().getEoHoraire().getEoStructure(getCStructure()).lcStructure() + " ");
		} else {

		}

		if (isAm()) {
			sb.append("AM");
		} else {
			sb.append("PM");
		}

		// pause
		if (getHorairePlagePause() != null) {
			sb.append(" pause ").append(getHorairePlagePause().toShortString());
		}

		return sb.toString();
	}

	/**
	 * 
	 */
	public String toShortString() {
		StringBuffer sb = new StringBuffer();

		sb.append(TimeCtrl.stringForMinutes(getDebutMinutesDansJour()));
		sb.append("-");
		sb.append(TimeCtrl.stringForMinutes(getFinMinutesDansJour()));

		// cas des horaires forcé, la structure est à null
		if (getHoraire().getEoHoraire() != null) {
			sb.append("@");
			sb.append(getHoraire().getEoHoraire().getEoStructure(getCStructure()).lcStructure());
		}

		return sb.toString();
	}

	public final String getCStructure() {
		return cStructure;
	}

	private final void setStructure(String cStructure) {
		this.cStructure = cStructure;
	}

	public final boolean isAm() {
		return isAm;
	}

	private final void setIsAm(boolean isAm) {
		this.isAm = isAm;
	}

	public final boolean isPm() {
		return !isAm;
	}

	public final HorairePlagePause getHorairePlagePause() {
		return horairePlagePause;
	}

	protected final void setHorairePlagePause(HorairePlagePause horairePlagePause) {
		this.horairePlagePause = horairePlagePause;
	}

	public final NSMutableArray<PlageOccupation> getPlageOccupationArray() {
		if (plageOccupationArray == null) {
			plageOccupationArray = new NSMutableArray<PlageOccupation>();
		}
		return plageOccupationArray;
	}

	/**
	 * Filtrer par rapport au jour
	 * 
	 * @param jour
	 * @return
	 */
	public final NSArray<PlageOccupation> getPlageOccupationArrayForJour(Jour jour) {
		NSArray<PlageOccupation> array = getPlageOccupationArray().immutableClone();

		EOQualifier qual = ERXQ.equals(PlageOccupation.JOUR_KEY, jour);
		array = (NSMutableArray<PlageOccupation>) EOQualifier.filteredArrayWithQualifier(array, qual);

		return array;
	}

	/**
	 * Pour affichage des horaires seuls uniquement (sinon il faut tenir compte
	 * des absences)
	 * 
	 * @return
	 */
	public int getDureeComptabilisee() {
		int minutes = 0;
		minutes = getDureeMinutes();

		if (getHoraireJournalier().isPauseRTTAccordable()) {

			if (getHoraireJournalier().isMultiService()) {

				// pause inclue
				if (getHoraireJournalier().isPauseInclueDansTempsTravail()) {

					if (getHorairePlagePause() != null) {

						// la pause est inclue : on enleve le bout de pause de l'autre
						// service
						HorairePlageTravail autrePlage = null;
						if (isAm()) {
							autrePlage = getHoraireJournalier().getHorairePlageTravailPm();
						} else {
							autrePlage = getHoraireJournalier().getHorairePlageTravailAm();
						}
						minutes -= getHoraireJournalier().getMinutesMultiServiceProrata(autrePlage.getCStructure(), Horaire.PAUSE_RTT_DUREE);

					} else {

						// la pause est sur l'autre demi journée de l'autre service :
						// on ajoute le bout de pause lui appartenant
						minutes += getHoraireJournalier().getMinutesMultiServiceProrata(getCStructure(), Horaire.PAUSE_RTT_DUREE);

					}
				} else {
					// pause superposée à la pause méridienne (bordeaux)
					// on ajoute la durée correspondant au prorata
					minutes += getHoraireJournalier().getMinutesMultiServiceProrata(getCStructure(), Horaire.PAUSE_RTT_DUREE);
				}

			} else {

				// mono service

				// XXX imposer que la pause RTT soit accollée à une demi journée
				// et ajouter les 20 minutes à celle ci
				if (getHoraireJournalier().isPauseInclueDansTempsTravail() == false) {

					if (getHoraireJournalier().getHorairePlagePause() != null && (
								getHoraireJournalier().getHorairePlagePause().getDebutMinutesDansJour() == getFinMinutesDansJour() ||
								getHoraireJournalier().getHorairePlagePause().getFinMinutesDansJour() == getDebutMinutesDansJour())) {
						minutes += Horaire.PAUSE_RTT_DUREE;
					}

				}

			}

		}

		return minutes;
	}

	/**
	 * Indique la plage de travail est impactée par une absence. On tiens compte
	 * de la pause si celle ci est accolée à la plage de travail
	 * 
	 * @param absence
	 * @return
	 */
	public boolean chevauche(A_Absence absence, Jour jour) {
		boolean chevauche = true;

		//
		if (jour.minutesDebutPourJour(absence) >= getFinMinutesDansSemaine() ||
				jour.minutesFinPourJour(absence) <= getDebutMinutesDansSemaine()) {
			chevauche = false;
		}

		return chevauche;
	}

	/**
	 * @return
	 */
	public final boolean isPauseInclue() {
		return isPauseInclue;
	}

	/**
	 * @param isPauseInclue
	 */
	protected final void setIsPauseInclue(boolean isPauseInclue) {
		this.isPauseInclue = isPauseInclue;
	}

}
