/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class IndemnisationAncienRegime
		extends A_CetTransactionAnnuelleDebit {

	/**
	 * @param eoCetTransactionAnnuelle
	 */
	public IndemnisationAncienRegime(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	@Override
	public String libelle() {
		return "Indemnisation sur CET (ancien régime) (au " + DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur()) + ")";
	}

}
