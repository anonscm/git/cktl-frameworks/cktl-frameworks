/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;

/**
 * Motif des classes affichant les données clé du CET d'un agent.
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_CetAffichage {

	// commun

	public EOPeriode toPeriode();

	// écran situation

	public Integer soldeAncienRegime();

	public Integer soldeRegimePerenneDebutPeriode();

	public Integer soldeRegimePerenneFinPeriode();

	// écran liste des demandes

	public Integer soldeAncienRegimeInitial();

	public Integer soldeAncienRegimeFinal();

	public Integer reliquatInitial();

	public Integer soldeRegimePerenneInitial();

	public Integer soldeRegimePerenneFinal();

}
