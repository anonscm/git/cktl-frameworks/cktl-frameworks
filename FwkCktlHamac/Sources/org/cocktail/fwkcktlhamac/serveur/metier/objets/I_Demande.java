/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Objet représentant une demande attendant validation
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_Demande {

	public final static String DEMANDE_DELEGATE_KEY = "demandeDelegate";
	public final static String DEMANDEUR_NOM_PRENOM_KEY = DEMANDE_DELEGATE_KEY + ".demandeur.nomPrenomAffichage";
	public final static String DATE_DEMANDE_KEY = "dateDemande";
	public final static String DATE_EFFECTIVE_KEY = "dateEffective";
	public final static String DEMANDE_C_STRUCTURE_ARRAY_CHAINE_KEY = "demandeDelegate.eoStructureArrayAFaireValiderChaine";
	public final static String TO_TYPE_SEQUENCE_VALIDATION_KEY = "toTypeSequenceValidation";
	public final static String PERS_ID = "persId";

	public DemandeDelegate getDemandeDelegate();

	public NSArray<EOVisa> tosVisa();

	public Integer persId();

	public IPersonne toPersonne();

	public NSArray<EODelegationHistorique> tosDelegationHistorique();

	public EOTypeSequenceValidation toTypeSequenceValidation();

	public String nature();

	public EOEditingContext editingContext();

	public String toString();

	public NSTimestamp dModification();

	public NSTimestamp dateDemande();

	public NSTimestamp dateEffective();

	/**
	 * Ajout d'un historique de délégation
	 * 
	 * @param personne
	 */
	public void addHistoriqueDelegation(Integer persId);

	/**
	 * L'état de validation actuel de l'objet
	 * 
	 * @return
	 */
	public EOTypeStatut toTypeStatut();

	public void setToTypeStatutRelationship(EOTypeStatut toTypeStatut);

	// definition locale car surcharge impossible (interface)
	public final static String TO_TYPE_STATUT = "toTypeStatut";
	// definition locale car surcharge impossible (interface)
	public final static String NATURE = "nature";

	public final static EOQualifier QUAL_EN_ATTENTE_VALIDATION = ERXQ.and(
			ERXQ.equals(NATURE, EOPlanning.REEL),
			ERXQ.isNotNull(TO_TYPE_STATUT),
			ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_NON_VALIDE),
			ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_EN_COURS_DE_MODIFICATION),
			ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_VALIDE),
			ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_REFUSE),
			ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_SUPPRIME));

}
