/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.A_FwkCktlHamacRecord;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Classe dédiée à la gestion des emails
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class MailCenter {

	public final static String ALERT_PREFIX_LIBELLE_MODIF_P_REEL = "Modification du planning réel";
	public final static String ALERT_PREFIX_LIBELLE_VALID_P_PREV = "Validation du planning prévisionnel";

	public final static String ALERT_MAIL_SUBJECT_PREFIX = "[Hamac] ";

	/** le titre du mail venant du demandeur */
	private final static String PREFIX_MAIL_TITLE_DEMANDE = ALERT_MAIL_SUBJECT_PREFIX + "demande de ";
	private final static String PREFIX_MAIL_TITLE_INFORMATION = ALERT_MAIL_SUBJECT_PREFIX + "pour information ";
	/** le nom du demandeur */
	public final static String VAR_DEMANDEUR = "%DEMANDEUR%";
	/** le libelle de l'alerte */
	public final static String VAR_LIBELLE_ALERTE = "%LIBELLE_ALERTE%";
	/** l'URL de l'application Conges */
	public final static String VAR_APP_URL = "%APPURL%";
	/** la partie specifique selon la nature de l'alerte */
	public final static String VAR_SPEC = "%SPEC%";
	/** le verbe de l'action a realiser par le valideur */
	public final static String VAR_VERBE = "%VERBE%";
	/** l'adresse email de sam associee au plugin sam-conges */
	public final static String VAR_SAM_MAIL = "%APP_SAM_MAIL%";
	// /** la terminaison de l'attribut de la table correspondant a la demande */
	// public final static String VAR_ATTR = "%ATTR%";
	/** la valeur du hashcode associee a l'alerte */
	public final static String VAR_CHAINE_SAM = "%HASH%";
	/** la variable du texte relatif a la delegation */
	public final static String VAR_CONTENT_DELEGATION = "%CONTENT_DELEGATION%";
	/**
	 * Le contenu du message envoye pour une alerte generique existante, pour
	 * information, sans SAM
	 */
	private final static String PATTERN_EXISTING_ALERT_INFORMATION =
			VAR_DEMANDEUR + VAR_CONTENT_DELEGATION + " concernant :\n" + VAR_LIBELLE_ALERTE;

	/**
	 * Le contenu du message envoye pour une alerte generique nouvelle, pour
	 * information, sans SAM
	 */
	private final static String PATTERN_NEW_ALERT_INFORMATION =
			"Une demande de " + PATTERN_EXISTING_ALERT_INFORMATION + "\n vient d'etre créée.";

	/** Le contenu du message envoye pour une alerte generique nouvelle, sans SAM */
	private final static String PATTERN_NEW_ALERT =
			PATTERN_NEW_ALERT_INFORMATION + "\n\n" +
					VAR_SPEC + "Vous pouvez la consulter et/ou la valider à cette adresse:\n" +
					VAR_APP_URL;

	/** Le contenu du message envoye pour une alerte generique existant, sans SAM */
	private final static String PATTERN_EXISTING_ALERT =
			PATTERN_EXISTING_ALERT_INFORMATION + "\n" +
					VAR_SPEC;

	/** Le contenu du message envoye pour une alerte generique nouvelle, avec SAM */
	private final static String PATTERN_NEW_ALERT_SAM =
			PATTERN_NEW_ALERT + "\n" +
					"\n" +
					"----------------\n" +
					"Actions par mail\n" +
					"\n" + VAR_CHAINE_SAM;

	/** Le contenu du message envoye pour une alerte generique existante, avec SAM */
	private final static String PATTERN_EXISTING_ALERT_SAM =
			PATTERN_EXISTING_ALERT + "\n" +
					VAR_CHAINE_SAM;

	/** Nouvelle occupation : le nombre de conges restants */
	public final static String VAR_CONGES_RESTANTS = "%CONGES_RESTANTS%";
	/** Nouvelle occupation : la phrase contenant toutes ses specifites */
	public final static String VAR_SPEC_NOUVELLE_OCCUPATION =
			"Congés restants après validation : " + VAR_CONGES_RESTANTS + "\n" +
					"\n";

	/** mode debug : la variable contenant l'adresse email de l'administrateur */
	public final static String VAR_ADMIN_MAIL = "%APP_ADMIN_MAIL%";
	/** mode debug : la variable contenant l'adresse email du destinataire */
	public final static String VAR_MAIL_TO = "%MAIL_TO%";
	/** mode debug : la variable contenant l'adresse email des cc */
	public final static String VAR_MAIL_CC = "%MAIL_CC%";

	private final static String CONTENT_DEBUG_CC = ", cc : " + VAR_MAIL_CC;

	/** mode debug : le prefix de tout le message email reexpedie */
	private final static String PREFIX_CONTENT_DEBUG =
			"---------\n" +
					"Attention, le mail suivant n'a ete envoye qu'a(ux) adresse(s) suivante(s) : \n" +
					VAR_ADMIN_MAIL + "\n" +
					"En production, les destinataires seront : " + VAR_MAIL_TO + CONTENT_DEBUG_CC + "\n" +
					"---------\n" +
					"\n";

	/** l'ensemble des caracteres utilises pour construire les hashcode */
	public final static String HASH_DICTIONARY = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789";

	private final static String PREFIX_TITRE_TEST = "MODE TEST: ";

	public final static String PREFIX_CONTENT_MAIL_GLOBAL = "Ceci est un message automatique de relance Hamac.\n" +
			"Vous devez intervenir afin de donner suite aux demandes en attente de décision de votre part.\n" +
			"Veuillez trouver ci-dessous la liste des demandes en attente";

	// delegation

	/** la variable du nom du delegue */
	public final static String VAR_DELEGUE = "%DELEGUE%";
	/** le texte concernant la delegation */
	public final static String CONTENT_DELEGATION = " (de " + VAR_DELEGUE + " par délégation)";

	// libelles des reponses lies au traitement des alerte

	public final static String ALERTE_LIBELLE_ACCEPTEE = "ACCEPTEE";
	public final static String ALERTE_LIBELLE_REFUSEE = "REFUSEE";
	public final static String ALERTE_LIBELLE_VISEE = "VISEE";

	// public static boolean envoyerMessage(

	/**
	 * instance de LRUserInfoDB pour effectuer les recherches d'email
	 */
	private CktlUserInfoDB _userInfo;
	// private CktlWebSession session;
	private DemandeDelegate demandeDelegate;
	private EOVisa eoVisa;

	public MailCenter(
			CktlWebSession session, DemandeDelegate demandeDelegate, EOVisa eoVisa) {
		this((CktlWebApplication) session.application(), session.context(), session.dataBus(), session.mailBus(), session.defaultEditingContext(), demandeDelegate, eoVisa);
	}

	private CktlMailBus mailBus;
	private EOEditingContext ec;
	private CktlDataBus dataBus;
	private CktlWebApplication app;
	private WOContext context;

	public MailCenter(
			CktlWebApplication app,
			WOContext context,
			CktlDataBus dataBus,
			CktlMailBus mailBus,
			EOEditingContext ec,
			DemandeDelegate demandeDelegate,
			EOVisa eoVisa) {
		super();
		this.app = app;
		this.context = context;
		this.dataBus = dataBus;
		this.mailBus = mailBus;
		this.ec = ec;
		this.demandeDelegate = demandeDelegate;
		this.eoVisa = eoVisa;
	}

	private EOVisa getEoVisa() {
		return eoVisa;
	}

	private DemandeDelegate getDemandeDelegate() {
		return demandeDelegate;
	}

	// private CktlWebSession getSession() {
	// return session;
	// }

	private CktlUserInfoDB getUserInfo() {
		if (_userInfo == null)
			_userInfo = new CktlUserInfoDB(dataBus);
		return _userInfo;
	}

	/**
	 * retourne la liste de tous les email des individu passes en parametre :
	 * liste de <code>IPersonne</code> a passer directement en parametre a la
	 * methode d'envoi de message. Si l'individu est celui concerne par l'alerte,
	 * alors son adresse sera ignoree.
	 */
	private final static String formatedEmailsIgnore(
			MailCenter mc, NSArray<IPersonne> individus, Integer persIdToIgnore) {
		StringBuffer strMails = new StringBuffer();
		// EOIndividu individuSelf = affectationAnnuelle().individu();
		Enumeration<IPersonne> enumIndividus = individus.objectEnumerator();

		// recup des responsables
		while (enumIndividus.hasMoreElements()) {
			IPersonne unIndividu = enumIndividus.nextElement();
			// on enleve l'individu ciblé des destinataires des mail si c'est lui meme
			if (unIndividu.persId().intValue() == persIdToIgnore.intValue()) {
				continue;
			}

			mc.getUserInfo().compteForPersId(unIndividu.persId(), true);
			String email = mc.getUserInfo().email();

			if (!StringCtrl.isEmpty(email)) {
				strMails.append(email).append(",");
			}
		}

		// on enleve la derniere virgule
		if (strMails.length() > 0) {
			strMails.deleteCharAt(strMails.length() - 1);
		}
		return strMails.toString();
	}

	private String adresseRedirection() {
		return HamacCktlConfig.stringForKey(EOParametre.EMAIL_ADRESSE_REDIRECTION);
	}

	private boolean isActiverRedirection() {
		return HamacCktlConfig.booleanForKey(EOParametre.EMAIL_ACTIVER_REDIRECTION);
	}

	private String appAdminMail = adresseRedirection();
	private String appSamMail = HamacCktlConfig.stringForKey(EOParametre.SAM_EMAIL_ADRESSE_PLUGIN);
	private boolean appUseSam = (HamacCktlConfig.intForKey(EOParametre.SAM_FREQUENCE_RECHERCHE) > 0);

	/**
	 * La construction des chaines liés à SAM
	 */
	private final static String getChaineSAM(
			boolean isAppUseSam, String strAppSamMail, DemandeDelegate delegate, EOTypeVisa eoTypeVisa) {

		String str = "";

		// liste des services concernés
		NSArray<EOStructure> eoStructureArray = delegate.getEoStructureAFaireValiderArray();

		for (EOStructure eoStructureApplication : eoStructureArray) {

			// liste des décisions
			NSArray<String> decisionArray = new NSArray<String>(new String[] {
					A_FwkCktlHamacRecord.OUI, A_FwkCktlHamacRecord.NON });
			for (String decisionON : decisionArray) {

				// listes des statuts
				NSArray<EOTypeStatut> eoTypeStatutArray = delegate.getEoTypeStatutPossibleArray(eoTypeVisa, eoStructureApplication, decisionON);

				for (EOTypeStatut eoTypeStatutSuivant : eoTypeStatutArray) {

					String verbe = "accepter";
					if (decisionON.equals(A_FwkCktlHamacRecord.NON)) {
						verbe = "refuser";
					}

					String visa = "la validation";
					if (eoTypeVisa.isNiveauVisa()) {
						visa = "le visa";
					}

					String chaineService = "";
					if (eoStructureArray.count() > 1) {
						chaineService = " pour la partie \"" + eoStructureApplication.lcStructure() + "\"";
					}

					String chaineStatutSuivant = "";
					if (eoTypeStatutArray.count() > 1) {
						chaineStatutSuivant = " (le statut sera alors : \"" + eoTypeStatutSuivant.staLibelle() + "\")";
					}

					str += "Pour " + verbe + " " + visa + " de la demande" + chaineService + chaineStatutSuivant + ":\n";
					str += "\t" + strAppSamMail + "?subject=";

					// si un seul statut, on ne le précise possible, alors ne pas
					// l'indiquer
					if (eoTypeStatutArray.count() == 1) {
						str += SAMCenter.encodeDemande(
								delegate.getDemande(), eoTypeVisa, null, eoStructureApplication, decisionON);
					} else {
						str += SAMCenter.encodeDemande(
								delegate.getDemande(), eoTypeVisa, eoTypeStatutSuivant, eoStructureApplication, decisionON);
					}

					str += "\n\n";

				}

			}

		}

		return str;

	}

	/**
	 * Permet d'envoyer a tous les responsables et valideurs un mail pour les
	 * avertir d'un NOUVEL evenement
	 * 
	 * @param informations
	 *          TODO
	 * @param demande
	 *          TODO
	 */
	public void sendMailsNouvelleAlerte(
			NSArray<IPersonne> responsables,
			NSArray<IPersonne> viseurs,
			NSArray<IPersonne> informations,
			IPersonne personneConcerne) {
		sendMailsNouvelleAlerte(
				this, responsables, viseurs, informations, personneConcerne);
	}

	/**
	 * Permet d'envoyer a tous les responsables et valideurs un mail pour les
	 * avertir d'un NOUVEL evenement
	 * 
	 * @param informations
	 *          TODO
	 * @param demande
	 *          TODO
	 */
	private final static void sendMailsNouvelleAlerte(
			MailCenter mc,
			NSArray<IPersonne> responsables,
			NSArray<IPersonne> viseurs,
			NSArray<IPersonne> informations,
			IPersonne personneConcerne) {

		String emailIndividu = "";
		String emailsResponsables = formatedEmailsIgnore(mc, responsables, personneConcerne.persId());
		String emailsViseurs = formatedEmailsIgnore(mc, viseurs, personneConcerne.persId());
		String emailInformations = formatedEmailsIgnore(mc, informations, personneConcerne.persId());

		DemandeDelegate dlg = mc.getDemandeDelegate();
		boolean isActiverRedirection = mc.isActiverRedirection();
		String adresseRedirection = mc.adresseRedirection();
		boolean isAppUseSam = mc.appUseSam;
		String appSamMail = mc.appSamMail;
		EOEditingContext ec = mc.ec;
		CktlMailBus mailBus = mc.mailBus;
		String appURLConnexionDirecte = mc.appURLConnexionDirecte();
		CktlUserInfo ui = mc.getUserInfo();

		// construire le dico de validation
		Hashtable<String, String> dico = buildDico(ec, isAppUseSam, appSamMail, appURLConnexionDirecte, dlg, personneConcerne);

		// construire le dico specifiques a la nature de l'alerte
		Hashtable<String, String> dicoSpec = buildDicoSpec();

		// construire le dico specifique a la la delegation
		ArrayList<Hashtable<String, String>> dicoArray = updateDicoAndbuildDicoDelegation(dlg, dico);
		dico = dicoArray.get(0);
		Hashtable<String, String> dicoDelegation = dicoArray.get(1);

		// envoi aux responsables
		if (emailsResponsables.length() > 0) {

			String titre = PREFIX_MAIL_TITLE_DEMANDE + "validation : " + dlg.libelle();
			titre = StringCtrl.replace(titre, "\n", " "); // on met titre a plat

			String contentValidation = buildContentMailValidation(
					isAppUseSam, isActiverRedirection, emailsResponsables, adresseRedirection, dico, dicoSpec, dicoDelegation, true);

			// en mode test, on prefixe le message et le destinataire devient
			// l'administrateur
			if (isActiverRedirection) {
				titre = PREFIX_TITRE_TEST + titre;
				emailsResponsables = adresseRedirection;
			}

			ui.compteForPersId(personneConcerne.persId(), true);
			emailIndividu = ui.email();
			mailBus.sendMail(emailIndividu, emailsResponsables, null, titre, contentValidation);

			// si c'est une demande par delegation, on previent la personne
			// concernee (le from etant le demandeur)
			if (dlg.isDemandeParDelegation()) {
				titre = PREFIX_MAIL_TITLE_DEMANDE + "validation : " + dlg.libelle() + " par delegation";
				titre = StringCtrl.replace(titre, "\n", " "); // on met titre a plat

				String contentDelegation = buildContentMailDelegation(
						dlg.personneDelegue(), isActiverRedirection, emailIndividu, adresseRedirection, titre);

				// le mail de l'agent concerne
				ui.compteForPersId(personneConcerne.persId(), true);
				String emailTo = ui.email();

				// en mode test, on prefixe le message et le destinataire devient
				// l'administrateur
				if (isActiverRedirection) {
					titre = PREFIX_TITRE_TEST + titre;
					emailTo = adresseRedirection;
				}

				// le mail du delegue
				ui.compteForPersId(dlg.personneDelegue().persId(), true);
				String emailDelegue = ui.email();

				mailBus.sendMail(emailDelegue, emailTo, null, titre, contentDelegation);

			}
		}

		// envoi aux viseurs
		if (emailsViseurs.length() > 0) {
			String titre = PREFIX_MAIL_TITLE_DEMANDE + "visa : " + dlg.libelle();
			titre = StringCtrl.replace(titre, "\n", " "); // on met titre a plat

			String contentVisa = buildContentMailVisa(
					ec, isAppUseSam, appSamMail, dlg, emailsViseurs, isActiverRedirection, adresseRedirection, dico, dicoSpec, dicoDelegation, true);

			if (isActiverRedirection) {
				titre = PREFIX_TITRE_TEST + titre;
				emailsViseurs = adresseRedirection;
			}
			mailBus.sendMail(emailIndividu, emailsViseurs, null, titre, contentVisa);
		}

		// envoi aux informations
		if (emailInformations.length() > 0) {
			String titre = PREFIX_MAIL_TITLE_INFORMATION + " : " + dlg.libelle();
			titre = StringCtrl.replace(titre, "\n", " "); // on met titre a plat

			String contentInformation = buildContentMailInformation(isActiverRedirection, emailInformations, adresseRedirection, dico, dicoSpec, dicoDelegation, true);

			if (isActiverRedirection) {
				titre = PREFIX_TITRE_TEST + titre;
				emailInformations = adresseRedirection;
			}
			mailBus.sendMail(emailIndividu, emailInformations, null, titre, contentInformation);
		}

	}

	/**
	 * Le contenu du mail d'une demande de validation
	 * 
	 * @param isNouvelleDemande
	 *          TODO
	 * 
	 * @return
	 */
	public final static String buildContentMailValidation(
			boolean isAppUseSam,
			boolean isActiverRedirection,
			String emailsResponsables,
			String adresseRedirection,
			Hashtable<String, String> dico,
			Hashtable<String, String> dicoSpec,
			Hashtable<String, String> dicoDelegation,
			boolean isNouvelleDemande) {
		String content = "";

		content =
				StringCtrl.replaceWithDico(
						StringCtrl.replaceWithDico(
								StringCtrl.replaceWithDico(isNouvelleDemande ? (isAppUseSam ? PATTERN_NEW_ALERT_SAM : PATTERN_NEW_ALERT) : (isAppUseSam ? PATTERN_EXISTING_ALERT_SAM : PATTERN_EXISTING_ALERT), dico),
								dicoSpec),
								dicoDelegation);

		if (isActiverRedirection) {
			content = StringCtrl.replace(StringCtrl.replace(PREFIX_CONTENT_DEBUG,
					VAR_ADMIN_MAIL, adresseRedirection), VAR_MAIL_TO, emailsResponsables)
					+ content;
			// oter la mention CC s'il n'y en a pas
			content = StringCtrl.replace(content, CONTENT_DEBUG_CC, "");
		}

		return content;
	}

	/**
	 * Le contenu du mail d'une demande de visa
	 * 
	 * @param isNouvelleDemande
	 *          TODO
	 * 
	 * @return
	 */
	public final static String buildContentMailVisa(
			EOEditingContext ec,
			boolean isAppUseSam,
			String strAppSamMail,
			DemandeDelegate dlg,
			String emailsViseurs,
			boolean isActiverRedirection,
			String adresseRedirection,
			Hashtable<String, String> dico,
			Hashtable<String, String> dicoSpec,
			Hashtable<String, String> dicoDelegation,
			boolean isNouvelleDemande) {
		String content = "";

		// remplacer le verbe et les codes pour le visa
		dico.put(clean(VAR_VERBE), "viser");
		dico.put(clean(VAR_CHAINE_SAM), getChaineSAM(isAppUseSam, strAppSamMail, dlg, EOTypeVisa.getEoTypeVisaVisa(ec)));

		content =
				StringCtrl.replaceWithDico(
						StringCtrl.replaceWithDico(
								StringCtrl.replaceWithDico(isNouvelleDemande ? (isAppUseSam ? PATTERN_NEW_ALERT_SAM : PATTERN_NEW_ALERT) : (isAppUseSam ? PATTERN_EXISTING_ALERT_SAM : PATTERN_EXISTING_ALERT), dico),
								dicoSpec),
						dicoDelegation);

		if (isActiverRedirection) {
			content = StringCtrl.replace(StringCtrl.replace(PREFIX_CONTENT_DEBUG,
					VAR_ADMIN_MAIL, adresseRedirection), VAR_MAIL_TO, emailsViseurs) + content;
			// oter la mention CC s'il n'y en a pas
			content = StringCtrl.replace(content, CONTENT_DEBUG_CC, "");
		}

		return content;
	}

	/**
	 * Le contenu du mail d'une personne a informer
	 * 
	 * @param isNouvelleDemande
	 *          TODO
	 * 
	 * @return
	 */
	private final static String buildContentMailInformation(
			boolean isActiverRedirection,
			String emailInformations,
			String adresseRedirection,
			Hashtable<String, String> dico,
			Hashtable<String, String> dicoSpec,
			Hashtable<String, String> dicoDelegation,
			boolean isNouvelleDemande) {
		String content = "";

		content =
					StringCtrl.replaceWithDico(
							StringCtrl.replaceWithDico(
									StringCtrl.replaceWithDico(isNouvelleDemande ? PATTERN_NEW_ALERT_INFORMATION : PATTERN_EXISTING_ALERT_INFORMATION, dico),
									dicoSpec),
							dicoDelegation);

		if (isActiverRedirection) {
			content = StringCtrl.replace(StringCtrl.replace(PREFIX_CONTENT_DEBUG,
						VAR_ADMIN_MAIL, adresseRedirection), VAR_MAIL_TO, emailInformations) + content;
			// oter la mention CC s'il n'y en a pas
			content = StringCtrl.replace(content, CONTENT_DEBUG_CC, "");
		}

		return content;
	}

	/**
	 * Le contenu du mail de saisie par délégation pour informer le déléguant
	 * 
	 * @return
	 */
	private final static String buildContentMailDelegation(
			IPersonne personneDelegue,
			boolean isActiverRedirection,
			String emailIndividu,
			String adresseRedirection,
			String titre) {

		String contentDelegation = "";

		contentDelegation = personneDelegue.getNomPrenomAffichage() + " a fait une demande en votre nom :\n" + titre;

		// en mode test, on prefixe le message et le destinataire devient
		// l'administrateur
		if (isActiverRedirection) {
			contentDelegation = StringCtrl.replace(StringCtrl.replace(PREFIX_CONTENT_DEBUG,
					VAR_ADMIN_MAIL, adresseRedirection), VAR_MAIL_TO, emailIndividu)
					+ contentDelegation;
		}

		return contentDelegation;

	}

	/**
	 * Dico de validation
	 * 
	 * @param mc
	 * @param personneConcerne
	 * @return
	 */
	public final static Hashtable<String, String> buildDico(
			EOEditingContext ec,
			boolean isAppUseSam,
			String strAppSamMail,
			String appURLConnexionDirecte,
			DemandeDelegate delegate,
			IPersonne personneConcerne) {

		// construire le dico de validation
		Hashtable<String, String> dico = new Hashtable<String, String>();
		dico.put(clean(VAR_DEMANDEUR), personneConcerne.getNomPrenomAffichage());
		dico.put(clean(VAR_LIBELLE_ALERTE), delegate.libelle());
		// dico.put(clean(VAR_APP_URL), appUrl);
		dico.put(clean(VAR_APP_URL), appURLConnexionDirecte);
		dico.put(clean(VAR_VERBE), "valider");

		// les specificites liees a SAM
		if (isAppUseSam) {
			dico.put(clean(VAR_SAM_MAIL), strAppSamMail);
			// dico.put(clean(VAR_ATTR), "VAL");
			dico.put(clean(VAR_CHAINE_SAM), getChaineSAM(isAppUseSam, strAppSamMail, delegate, EOTypeVisa.getEoTypeVisaValidation(ec)));
		}
		dico.put(clean(VAR_SPEC), "");

		return dico;

	}

	/**
	 * Dico de specificites
	 * 
	 * @return
	 */
	public final static Hashtable<String, String> buildDicoSpec() {
		// construire le dico specifiques a la nature de l'alerte
		Hashtable<String, String> dicoSpec = new Hashtable<String, String>();

		// -- rajouter les specificites selon la nature de l'alerte --
		// boolean hasSpecConges = !isValidationPrev() && !isModificationReel() &&
		// !ConstsOccupation.CODE_EN_COURS_DE_SUPPRESSION.equals(occupation().status())
		// &&
		// !occupation().isAbsenceBilan() &&
		// !occupation().isAbsenceCET();
		//
		// if (hasSpecConges) {
		// // specificite de nouvelle occupation
		// dico.put(clean(VAR_SPEC), VAR_SPEC_NOUVELLE_OCCUPATION);
		// dicoSpec.put(clean(VAR_CONGES_RESTANTS),
		// planning.congesGlobalRestants());
		// } else {
		// aucune specificite
		// }

		return dicoSpec;
	}

	/**
	 * Dico délagation + modifiation du dico initial.
	 * 
	 * - element 0 : dico modifié
	 * 
	 * - element 1 : dico délégation
	 * 
	 * @param mc
	 * @param dico
	 * @return
	 */
	public final static ArrayList<Hashtable<String, String>> updateDicoAndbuildDicoDelegation(
			DemandeDelegate delegate, Hashtable<String, String> dico) {

		Hashtable<String, String> dicoModifie = new Hashtable<String, String>(dico);

		Hashtable<String, String> dicoDelegation = new Hashtable<String, String>();
		if (delegate.isDemandeParDelegation()) {
			//
			dicoModifie.put(clean(VAR_CONTENT_DELEGATION), CONTENT_DELEGATION);
			dicoDelegation.put(clean(VAR_DELEGUE), delegate.personneDelegue().getNomPrenomAffichage());
		} else {
			// aucune specificite
			dicoModifie.put(clean(VAR_CONTENT_DELEGATION), "");
		}

		ArrayList<Hashtable<String, String>> result = new ArrayList<Hashtable<String, String>>();

		result.add(dicoModifie);
		result.add(dicoDelegation);

		return result;
	}

	/**
	 * Envoi de mail au demandeur que l'operation est terminee. On envoi egalement
	 * le mail au(x) valideur(s) ainsi qu'aux eventuels autres viseurs
	 */
	public void sendMailAlerteTraitee(
			NSArray<IPersonne> responsables,
			NSArray<IPersonne> viseurs,
			NSArray<IPersonne> informations) {

		getUserInfo().compteForPersId(getDemandeDelegate().getDemandeur().persId(), true);
		String emailTo = getUserInfo().email();
		getUserInfo().compteForPersId(eoVisa.persIdViseur(), true);
		String emailFrom = getUserInfo().email();
		String emailCc = null;

		// on previent les viseurs et les valideurs
		// on catch l'exception des responsables, s'agissant d'un mail d'un viseur,
		// le probleme
		// des responsable aura forcement eu lieu lors de la demande par l'agent
		try {
			NSArray<IPersonne> destList = informations.arrayByAddingObjectsFromArray(viseurs.arrayByAddingObjectsFromArray(responsables));
			boolean shouldAddDelegue = false;

			// ajouter le demandeur delegue dans les destinataires
			if (getDemandeDelegate().isDemandeParDelegation()) {
				HamacApplicationUser destUi = new HamacApplicationUser(
						ec, getDemandeDelegate().personneDelegue().persId());
				// on l'ajoute pas si ces preferences disent qu'il ne le faut pas
				if (destUi.isMailRecipisseDelegation()) {
					shouldAddDelegue = true;
				}
			}

			// on enleve les responsables qui ne veulent pas du mail de recipisse
			// sauf pour les informations qui recoivent tout le temps
			NSArray<IPersonne> destMailList = new NSArray<IPersonne>();
			for (int i = 0; i < destList.count(); i++) {
				IPersonne dest = destList.objectAtIndex(i);

				// information => pas le choix
				boolean isEnvoyer = false;
				if (informations.containsObject(dest)) {
					isEnvoyer = true;
				}

				// sinon d'apres les prefs
				if (!isEnvoyer) {
					HamacApplicationUser destUi = new HamacApplicationUser(
							ec, dest.persId());
					if (destUi.isMailRecipisse()) {
						isEnvoyer = true;
					}
				}

				if (isEnvoyer) {
					destMailList = destMailList.arrayByAddingObject(dest);
				}
			}

			// ajout du delegue
			if (shouldAddDelegue) {
				destMailList = destMailList.arrayByAddingObject(getDemandeDelegate().personneDelegue());
			}

			// enlever celui qui envoi l'alerte
			emailCc = formatedEmailsIgnore(this, destMailList, getEoVisa().persIdViseur());

		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}

		String adjectifDecision = "";
		if (getEoVisa().isAccepte()) {
			if (getEoVisa().toTypeVisa().isNiveauValidation()) {
				adjectifDecision = ALERTE_LIBELLE_ACCEPTEE;
			} else {
				adjectifDecision = ALERTE_LIBELLE_VISEE;
			}
		} else {
			adjectifDecision = ALERTE_LIBELLE_REFUSEE;
		}

		String title = ALERT_MAIL_SUBJECT_PREFIX + "Demande " + adjectifDecision + " : " + getDemandeDelegate().libelle();
		String content = "Votre demande :\n" + getDemandeDelegate().libelle() +
				"\na ete " + adjectifDecision + " par " + getEoVisa().toPersonneViseur().getNomPrenomAffichage();
		if (!StringCtrl.isEmpty(getEoVisa().visCommentaire())) {
			content += "\n\nCommentaire du responsable :\n" + getEoVisa().visCommentaire();
		}

		if (isActiverRedirection()) {
			String adresseRedirection = adresseRedirection();
			title = "MODE TEST: " + title;
			content =
					StringCtrl.replace(
							StringCtrl.replace(
									StringCtrl.replace(
											PREFIX_CONTENT_DEBUG, VAR_ADMIN_MAIL, adresseRedirection),
										VAR_MAIL_TO, emailTo),
										VAR_MAIL_CC, emailCc) + content;
			emailTo = adresseRedirection;
			emailCc = null;
		}

		mailBus.sendMail(emailFrom, emailTo, emailCc, title, content);
	}

	/**
	 * Pour utiliser StringCtrl.replaceWithDico(), il faut recuperer les noms de
	 * variable sans les caraceteres '%'. Cette methode enleve ce caractere avant
	 * et apres.
	 * 
	 * @param varName
	 * @return
	 */
	private final static String clean(String varName) {
		return StringCtrl.replace(varName, "%", "");
	}

	/**
	 * Envoyer l'accuse que l'operation s'est bien deroulee a l'utilisateur.
	 */
	public void sendMailOperationSuccessfull(
			I_Demande demande, Number persIdViseur, boolean isAccept, boolean isVisa, EOStructure eoStructureApplication) {

		getUserInfo().compteForPersId(persIdViseur, true);
		String emailTo = getUserInfo().email();

		String content = "Votre mail demandant d" +
				(isAccept ? "'accepter" : "e refuser") + " " +
				(isVisa ? "le visa" : "la validation") + " de l'alerte : \n\"" +
				demande.toString() + "\"\n(planning de " + demande.toPersonne().getNomPrenomAffichage() + ")" +
				" a été traité avec succès par l'application.";

		String title = ALERT_MAIL_SUBJECT_PREFIX + "Operation réussie";

		if (isActiverRedirection()) {
			title = "MODE TEST: " + title;
			content = "email à destination de : " + emailTo + "\n\n" + content;
			emailTo = adresseRedirection();
		}

		mailBus.sendMail(appAdminMail, emailTo, null, title, content);
	}

	/**
	 * Envoyer un email au demandeur et a l'administrateur en copie cachee,
	 * indiquantl'erreur survenue.
	 */
	public void sendMailError(
			String emailTo, String errorMessage) {

		String title = ALERT_MAIL_SUBJECT_PREFIX + "Erreur de traitement de votre message";
		String content = errorMessage + "\nVotre message ne sera pas traite.";
		if (isActiverRedirection()) {
			title = "MODE TEST: " + title;
			content = "email à destination de : " + emailTo + "\n\n" + content;
		} else {
			// on envoi en plus au demandeur en mode normal
			mailBus.sendMail(appAdminMail, emailTo, null, title, content);
		}
		// quoi qu'il en soit, on envoi a l'admin
		mailBus.sendMail(appAdminMail, appAdminMail, null, title,
				"Le message suivant a ete envoye a <" + emailTo + ">\n-----\n" + content);
	}

	private String appURLConnexionDirecte() {
		return appURLConnexionDirecte(app, context);
	}

	/**
	 * Retourne le lien de connexion à l'application. Si la valeur est définie
	 * dans les paramètres, alors on la prend, sinon on laisse webobjects trouver
	 * comme un grand
	 */
	public final static String appURLConnexionDirecte(CktlWebApplication app, WOContext context) {
		String url = null;

		url = HamacCktlConfig.stringForKey(EOParametre.EMAIL_URL_APPLICATION_HAMAC);

		if (StringCtrl.isEmpty(url)) {
			url = app.getApplicationURL(context) /*
																						 * + "/wa/connexion"
																						 */;
		}

		return url;

	}

}
