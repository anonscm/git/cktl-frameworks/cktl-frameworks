/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import com.webobjects.foundation.NSTimestamp;

/**
 * Objet réprésentant un solde
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_Solde {

	public final static String IS_CONGE_NATIF_KEY = "isCongeNatif";
	public final static String IS_RELIQUAT_NATIF_KEY = "isReliquatNatif";
	public final static String IS_RELIQUAT_MANUEL_KEY = "isReliquatManuel";
	public final static String IS_RELIQUAT_HSUP_KEY = "isReliquatHsup";
	public final static String IS_CONGE_MANUEL_KEY = "isCongeManuel";
	public final static String IS_BALANCE_KEY = "isBalance";
	public final static String IS_REGULARISATION_KEY = "isRegularisation";
	public final static String IS_CONGE_LEGAL_KEY = "isCongeLegal";
	public final static String IS_CONGE_RH_KEY = "isCongeRh";
	public final static String IS_DECHARGE_SYNDICALE_KEY = "isDechargeSyndicale";
	public final static String IS_JRTI_KEY = "isJrti";
	public final static String IS_CET_KEY = "isCet";
	public final static String IS_INDEM_CET_2008_1136_KEY = "isIndemCet20081136";
	public final static String IS_RECUPERATION_KEY = "isRecuperation";
	public final static String IS_HEURE_SUPPLEMENTAIRE_MANUEL_KEY = "isHeureSupplementaireManuel";
	public final static String IS_HEURE_SUPPLEMENTAIRE_NON_VALIDE_KEY = "isHeureSupplementaireNonValide";

	public final static String SOLDE_DELEGATE_KEY = "soldeDelegate";
	public final static String OPERATION_ARRAY_KEY = SOLDE_DELEGATE_KEY + "." + "operationArray";
	public final static String TRANSACTION_ARRAY_KEY = SOLDE_DELEGATE_KEY + "." + "transactionArray";

	public final static String D_DEB_KEY = "dDeb";
	public final static String D_FIN_KEY = "dFin";

	public final static String C_STRUCTURE = "cStructure";

	// public final static String RESTANT_KEY = SOLDE_DELEGATE_KEY + "." +
	// SoldeDelegate.RESTANT_KEY;

	/**
	 * L'instance de la classe de gestion
	 * 
	 * @return
	 */
	public SoldeDelegate getSoldeDelegate();

	/** forcer le recalcul du cache */
	public void clearCache();

	/**
	 * Mémorise la valeur restant (base de données ...)
	 * 
	 * @param restant
	 */
	public void setRestant(Integer restant);

	/**
	 * 
	 * @return
	 */
	public String libelle();

	public boolean isCongeNatif();

	public boolean isReliquatNatif();

	public boolean isReliquatManuel();
	
	public boolean isReliquatHsup();

	public boolean isCongeManuel();

	public boolean isBalance();

	public boolean isRegularisation();

	public boolean isCongeLegal();

	public boolean isCongeRh();

	public boolean isDechargeSyndicale();

	public boolean isJrti();

	public boolean isCet();

	public boolean isIndemCet20081136();

	public boolean isRecuperation();

	public boolean isHeureSupplementaireManuel();

	public boolean isHeureSupplementaireNonValide();

	/**
	 * Les opérations ayant une date de valeur entre {@link #dDeb()} et
	 * {@link #dFin()} peuvent consommer ce solde
	 * 
	 * @return
	 */
	public NSTimestamp dDeb();

	/**
	 * Les opérations ayant une date de valeur entre {@link #dDeb()} et
	 * {@link #dFin()} peuvent consommer ce solde
	 * 
	 * @return
	 */
	public NSTimestamp dFin();

	/**
	 * Le service d'attachement du solde. Ne rien mettre si le solde n'est pas
	 * dépendant d'un service (congés légaux ...)
	 * 
	 * @return
	 */
	public String cStructure();

}
