/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 * 
 */
public class LigneFicheRose
		implements I_LigneFicheRose {

	private String libelleType;
	private String motif;
	private String cStructureCourante;

	private NSMutableDictionary<String, Integer> dicoDebitCng;
	private NSMutableDictionary<String, Integer> dicoDebitRel;
	private NSMutableDictionary<String, Integer> dicoRestantCng;
	private NSMutableDictionary<String, Integer> dicoRestantRel;

	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;

	public LigneFicheRose(String libelleType, String motif) {
		super();
		this.libelleType = libelleType;
		this.motif = motif;
		dicoDebitCng = new NSMutableDictionary<String, Integer>();
		dicoDebitRel = new NSMutableDictionary<String, Integer>();
		dicoRestantCng = new NSMutableDictionary<String, Integer>();
		dicoRestantRel = new NSMutableDictionary<String, Integer>();
	}

	public final String getCStructureCourante() {
		return cStructureCourante;
	}

	public final void setCStructureCourante(String cStructureCourante) {
		this.cStructureCourante = cStructureCourante;
	}

	public final String libelleType() {
		return libelleType;
	}

	public final String motif() {
		return motif;
	}

	private final Integer getTotalDicoForStructure(NSMutableDictionary<String, Integer> dico) {
		Integer total = null;

		if (getCStructureCourante() != null) {
			total = dico.objectForKey(cStructureCourante);
		} else {
			if (dico.allKeys().count() > 0) {
				int i_total = 0;
				for (String key : dico.allKeys()) {
					i_total += dico.objectForKey(key).intValue();
				}
				total = Integer.valueOf(i_total);
			}
		}

		return total;
	}

	public final Integer getDebitTotal() {
		Integer total = null;

		if (getDebitConge() != null) {
			total = getDebitConge();
		}

		if (getDebitReliquat() != null) {
			if (total == null) {
				total = getDebitReliquat();
			} else {
				total = total + getDebitReliquat();
			}
		}

		return total;
	}

	public final Integer getDebitConge() {
		return getTotalDicoForStructure(dicoDebitCng);
	}

	public final Integer getDebitReliquat() {
		return getTotalDicoForStructure(dicoDebitRel);
	}

	public final Integer getRestantCng() {
		return getTotalDicoForStructure(dicoRestantCng);
	}

	public final Integer getRestantRel() {
		return getTotalDicoForStructure(dicoRestantRel);
	}

	public final void setDebitCng(Integer val) {
		dicoDebitCng.setObjectForKey(val, cStructureCourante);
	}

	public final void setDebitRel(Integer val) {
		dicoDebitRel.setObjectForKey(val, cStructureCourante);
	}

	public final void setRestantCng(Integer val) {
		dicoRestantCng.setObjectForKey(val, cStructureCourante);
	}

	public final void setRestantRel(Integer val) {
		dicoRestantRel.setObjectForKey(val, cStructureCourante);
	}

	public final void setRestantCngFrom(NSArray<I_LigneFicheRose> array, int aDebiter) {
		int restantIntermediaire = getDernierNonVideForStructure(array, cStructureCourante, true, false) - aDebiter;
		setRestantCng(restantIntermediaire);
	}

	public final void setRestantRelFrom(NSArray<I_LigneFicheRose> array, int aDebiter) {
		Integer restant = getDernierNonVideForStructure(array, cStructureCourante, false, true);
		if (restant == null) {
			restant = Integer.valueOf(0);
		}
		int restantIntermediaire = restant.intValue() - aDebiter;
		setRestantRel(restantIntermediaire);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getDateDebut
	 * ()
	 */
	public NSTimestamp dateDebut() {
		return dateDebut;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getDateFin
	 * ()
	 */
	public NSTimestamp dateFin() {
		return dateFin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#
	 * getNbDemiJournee()
	 */
	public Float getNbJourneeSurHoraire() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#
	 * libelleTypeCourt()
	 */
	public String libelleTypeCourt() {
		return FormatterCtrl.getLibelleCourt(libelleType(), TAILLE_TYPE_LIBELLE_COURT);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#motifCourt
	 * ()
	 */
	public String motifCourt() {
		return FormatterCtrl.getLibelleCourt(motif(), TAILLE_MOTIF_COURT);
	}

	/**
	 * Pour les affichages incrémentaux, on recherche la derniere valeur non vide
	 * 
	 * @param array
	 * @param cStructure
	 * @param isConges
	 * @param isReliquat
	 * @return
	 */
	private final static Integer getDernierNonVideForStructure(
			NSArray<I_LigneFicheRose> array, String cStructure, boolean isConges, boolean isReliquat) {
		Integer dernier = null;

		int i = array.count() - 1;

		while (dernier == null && i >= 0) {
			I_LigneFicheRose lfr = array.objectAtIndex(i);
			if (lfr.getCStructureCourante() == null || (lfr.getCStructureCourante().equals(cStructure))) {
				// XXX faire générique
				if (isConges && lfr.getRestantCng() != null) {
					dernier = lfr.getRestantCng();
				} else if (isReliquat && lfr.getRestantRel() != null) {
					dernier = lfr.getRestantRel();
				}
			}
			i--;
		}

		return dernier;
	}

	public final void setDateDebut(NSTimestamp dateDebut) {
		this.dateDebut = dateDebut;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#
	 * isHeureSupplementaire()
	 */
	public boolean isHeureSupplementaire() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#
	 * isAbsenceAssimileeCCompPourBalance()
	 */
	public boolean isAbsenceAssimileeCCompPourBalance() {
		// TODO Auto-generated method stub
		return false;
	}

	public final void setDateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}

}
