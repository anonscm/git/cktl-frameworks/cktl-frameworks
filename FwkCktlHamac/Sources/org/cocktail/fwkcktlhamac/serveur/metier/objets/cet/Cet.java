/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import java.math.BigDecimal;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetSaisieManuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_DebitRoulant;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeFactory;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.common.CktlSort;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe représentative d'un compte épargne temps
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Cet {

	private EOPlanning eoPlanning;
	private NSArray<I_Solde> _crediteurFromTransactionArray;
	private NSArray<EOCetTransactionAnnuelle> _transactionArray;
	private NSArray<EOCetSaisieManuelle> _cetSaisieManuelleArray;
	private NSArray<EOPlanning> _eoPlanningCourantEtPrecedentArray;
	private NSArray<EOCetVidage> _vidageArray;

	public Cet(EOPlanning eoPlanning) {
		super();
		// System.out.println("new Cet " + eoPlanning);
		this.eoPlanning = eoPlanning;
		reinitialiser();
	}

	private final EOPlanning getEoPlanning() {
		return eoPlanning;
	}

	private final EOEditingContext getEditingContext() {
		return getEoPlanning().editingContext();
	}

	private final Integer getPersId() {
		return getEoPlanning().persId();
	}

	/**
	 * 
	 * @return
	 */
	private NSArray<I_Solde> getCrediteurFromTransactionArray() {
		if (_crediteurFromTransactionArray == null) {

			_crediteurFromTransactionArray = new NSArray<I_Solde>();

			for (EOCetTransactionAnnuelle transaction : getTransactionArray()) {
				_crediteurFromTransactionArray = _crediteurFromTransactionArray.arrayByAddingObjectsFromArray(
						transaction.getCrediteurArray());
			}

		}

		return _crediteurFromTransactionArray;
	}

	/**
	 * La classe permettant d'effectuer les débits roulants sur les soldes CET
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantCet
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeCetArray
		 * @param operation
		 * @param valeur
		 */
		public DebitRoulantCet(
				NSArray<I_Solde> eoSoldeCetArray, I_Operation operation, int valeur) {
			super(eoSoldeCetArray, operation, valeur);
		}

	}

	/**
	 * refaire les calculs associés
	 */
	private void reinitialiser() {

		// raz cache
		_cetSaisieManuelleArray = null;
		_crediteurFromTransactionArray = null;
		_eoPlanningCourantEtPrecedentArray = null;
		_eoSoldeIndemCet20081136Array = null;
		_transactionArray = null;
		_vidageArray = null;

		// raz cache des soldes
		NSArray<I_Solde> soldeANettoyerArray = getSoldeArray();
		SoldeFactory.clearCache(soldeANettoyerArray);

		// tous les objets consommateurs de CET
		NSMutableArray<I_Operation> operationCetArray = new NSMutableArray<I_Operation>();

		// décret 2008-1136
		for (EOSolde eoSolde : getSoldeIndemCet20081136Array()) {
			operationCetArray.addObject(eoSolde);
		}

		// occupations
		NSArray<EOOccupation> occupationCetArray = EOQualifier.filteredArrayWithQualifier(
					getEoPlanning().findOccupation(),
				// eoOccupationArray(),
				ERXQ.and(EOOccupation.QUAL_CET, EOOccupation.QUAL_OCC_NON_ANNULE));

		for (EOOccupation eoOccupation : occupationCetArray) {
			operationCetArray.addObject(eoOccupation);
		}

		// transactions annuelles crédits (épargne décidée, et à défaut celle qui
		// est demandée)
		for (EOCetTransactionAnnuelle transaction : getTransactionArray()) {
			if (transaction.getEpargneDecidee() != null) {
				operationCetArray.add(transaction.getEpargneDecidee());
			} else if (transaction.getEpargneDemandee() != null) {
				operationCetArray.add(transaction.getEpargneDemandee());
			}
		}

		// transactions annuelles débits (indem, rafp, ...)
		for (EOCetTransactionAnnuelle transaction : getTransactionArray()) {
			operationCetArray.addObjectsFromArray(transaction.getDebiteurArray());
		}

		// vidages manuels
		operationCetArray.addObjectsFromArray(getVidageArray());

		// classer par date de début de valeur
		operationCetArray = new NSMutableArray<I_Operation>(
				CktlSort.sortedArray(
						operationCetArray, I_Operation.D_DEB));
		// System.out.println("[" + eoPlanning.nature() + "] " +
		// eoPlanning.toString() + " Cet.reinitialiser() " + this.toString());

		for (I_Operation operationCet : operationCetArray) {

			if (operationCet instanceof EOSolde) {

				// décret 2008-1136
				EOSolde solde20081136 = (EOSolde) operationCet;

				int soldeRestant = 0;
				if (solde20081136.getSoldeDelegate().restant() != null) {
					soldeRestant = solde20081136.getSoldeDelegate().restant().intValue();
				}

				if (soldeRestant < 0) {

					// les soldes disponibles avant la date de valeur
					NSArray<I_Solde> soldeDispoArray = getSoldeDispoArray(
							solde20081136.toPlanning().toPeriode().perDDebut());

					new DebitRoulantCet(
								soldeDispoArray,
								solde20081136,
								-soldeRestant);

				}

			} else if (operationCet instanceof EOOccupation) {

				// occupation ... il faut charger le(s) planning(s) associé(s)
				EOOccupation eoOccupation = (EOOccupation) operationCet;

				// si le planning courant n'est pas celui de l'occupation, alors on fait
				// un débit en valeur plutot que de passer par la mécanique des segments
				// afin d'éviter de tout recharger ...

				NSArray<EOOccupationPlanningCalcul> array = eoOccupation.tosOccupationPlanningCalcul();
				for (EOOccupationPlanningCalcul calcul : array) {

					EOPlanning eoPlanningForOccupation = calcul.toPlanning();

					if (eoPlanningForOccupation != getEoPlanning() &&
							eoPlanningForOccupation.isBefore(getEoPlanning())) {

						// les soldes disponibles pour cette occupation
						NSArray<I_Solde> soldeDispoArray = getSoldeDispoArray(eoOccupation.dateDebut());

						new DebitRoulantCet(
									soldeDispoArray,
									eoOccupation,
									calcul.coutMinute().intValue());

					}

				}

			} else if (operationCet instanceof Epargne) {

				// on ne fait rien de spécial, car les débits se font dans la classe
				// Planning, et les crédits passent dans les soldes

			} else if (operationCet instanceof A_CetTransactionAnnuelleDebit) {

				// débit cet suite à transaction annuelle (indemnisation / RAFP ...)
				A_CetTransactionAnnuelleDebit debit = (A_CetTransactionAnnuelleDebit) operationCet;

				// les soldes disponibles avant la date de valeur
				NSArray<I_Solde> soldeDispoArray = getSoldeDispoArray(debit.dateValeur());

				new DebitRoulantCet(
							soldeDispoArray,
							debit,
							debit.getValeur());

			} else if (operationCet instanceof EOCetVidage) {

				// vidage manuel
				EOCetVidage vidage = (EOCetVidage) operationCet;

				// date
				NSTimestamp dateValeur = vidage.getOperationDelegate().dDeb();

				// les soldes disponibles
				NSArray<I_Solde> soldeDispoArray = getSoldeDispoArray(dateValeur);

				// valeur
				int valeur = getCreditAncienTotal().intValue() - getDebitAncienTotal().intValue();
				if (!vidage.isVidageAncien()) {
					valeur += getCreditPerenneTotal(dateValeur).intValue() - getDebitPerenneTotal(dateValeur).intValue();
				}

				new DebitRoulantCet(
						soldeDispoArray, vidage, valeur);

			} else {

				// TODO

			}

		}

		// // tmp affichage
		// for (I_Operation operationCet : operationCetArray) {
		// System.out.println("\t\toperationCet=" + operationCet + " " +
		// Integer.toHexString(operationCet.hashCode()));
		// }

	}

	private NSArray<EOSolde> _eoSoldeIndemCet20081136Array;

	/**
	 * Liste de tous les soldes relatifs à une indemnisation liée au décret
	 * 2008-1136
	 * 
	 * @return
	 */
	private NSArray<EOSolde> getSoldeIndemCet20081136Array() {
		if (_eoSoldeIndemCet20081136Array == null) {

			_eoSoldeIndemCet20081136Array = new NSArray<EOSolde>();
			for (int i = 0; i < getEoPlanningCourantEtPrecedentArray().count(); i++) {
				EOSolde eoSolde = getEoPlanningCourantEtPrecedentArray().objectAtIndex(i).getSoldeIndemCet20081136();
				if (eoSolde != null) {
					_eoSoldeIndemCet20081136Array = _eoSoldeIndemCet20081136Array.arrayByAddingObject(eoSolde);
				}
			}
		}
		return _eoSoldeIndemCet20081136Array;
	}

	/**
	 * Liste de toutes les transactions annuelles CET associées
	 * 
	 * 
	 * @return
	 */
	private NSArray<EOCetTransactionAnnuelle> getTransactionArray() {
		if (_transactionArray == null) {

			_transactionArray = new NSArray<EOCetTransactionAnnuelle>();

			for (EOPlanning eoPlanning : getEoPlanningCourantEtPrecedentArray()) {
				EOCetTransactionAnnuelle transaction = eoPlanning.getEoCetTransactionAnnuelle();

				if (transaction != null) {
					//
					transaction.clearCache();
					_transactionArray = _transactionArray.arrayByAddingObject(transaction);
				}
			}
		}
		return _transactionArray;
	}

	/**
	 * Liste de tous les vidages CET associés
	 * 
	 * 
	 * @return
	 */
	private NSArray<EOCetVidage> getVidageArray() {
		if (_vidageArray == null) {

			_vidageArray = new NSArray<EOCetVidage>();

			for (EOPlanning eoPlanning : getEoPlanningCourantEtPrecedentArray()) {

				NSArray<EOCetVidage> array = eoPlanning.tosCetVidage();

				for (EOCetVidage vidage : array) {

					vidage.getOperationDelegate().clearCache();

					_vidageArray = _vidageArray.arrayByAddingObject(vidage);
				}
			}
		}
		return _vidageArray;
	}

	/**
	 * 
	 * @return
	 */
	private NSArray<EOPlanning> getEoPlanningCourantEtPrecedentArray() {
		if (_eoPlanningCourantEtPrecedentArray == null) {
			NSMutableArray<EOPlanning> array = new NSMutableArray<EOPlanning>();

			EOPlanning eoPlanning = getEoPlanning();
			while (eoPlanning != null) {
				array.addObject(eoPlanning);
				eoPlanning = eoPlanning.getEoPlanningPrecedent();
			}

			// on rempile dans l'autre sens
			_eoPlanningCourantEtPrecedentArray = new NSArray<EOPlanning>();
			for (int i = 0; i < array.count(); i++) {
				EOPlanning item = array.objectAtIndex(array.count() - 1 - i);
				_eoPlanningCourantEtPrecedentArray = _eoPlanningCourantEtPrecedentArray.arrayByAddingObject(item);
			}
		}
		return _eoPlanningCourantEtPrecedentArray;
	}

	/**
	 * Liste des saisies manuelles courantes et précédentes
	 * 
	 * @return
	 */
	private NSArray<EOCetSaisieManuelle> getCetSaisieManuelleArray() {
		if (_cetSaisieManuelleArray == null) {

			_cetSaisieManuelleArray = EOCetSaisieManuelle.getTransactionArrayFor(
					getEditingContext(),
					getEoPlanning().nature(),
					getPersId(),
					getEoPlanning().toPeriode().perDFin());

		}
		return _cetSaisieManuelleArray;
	}

	private NSArray<I_Solde> soldeCetArray;

	/**
	 * La liste de tous les soldes disponibles
	 * 
	 * @return
	 */
	public NSArray<I_Solde> getSoldeArray() {

		if (soldeCetArray == null) {

			/* NSArray<I_Solde> */soldeCetArray = new NSArray<I_Solde>();
			// saisies manuelles
			soldeCetArray = soldeCetArray.arrayByAddingObjectsFromArray(getCetSaisieManuelleArray());
			// décisions
			soldeCetArray = soldeCetArray.arrayByAddingObjectsFromArray(getCrediteurFromTransactionArray());
			// classement par date de validité
			soldeCetArray = CktlSort.sortedArray(soldeCetArray, I_Solde.D_DEB_KEY);
			//
			// for (I_Solde solde : soldeCetArray) {
			// System.out.println("\tsolde=" + solde + " " +
			// Integer.toHexString(solde.hashCode()));
			// }

		}

		return soldeCetArray;
	}

	private NSArray<I_Solde> getSoldeAncienArray() {
		return EOQualifier.filteredArrayWithQualifier(
				getSoldeArray(), ERXQ.isTrue(I_CetCredit.IS_ANCIEN_REGIME_KEY));
	}

	private NSArray<I_Solde> getSoldePerenneArray(NSTimestamp avantDate) {
		return EOQualifier.filteredArrayWithQualifier(
				getSoldeArray(), ERXQ.and(
						ERXQ.isFalse(I_CetCredit.IS_ANCIEN_REGIME_KEY),
						ERXQ.lessThanOrEqualTo(I_CetCredit.DATE_VALEUR_KEY, avantDate)));
	}

	/**
	 * Liste des soldes disponibles pour une date donnée (à partir de
	 * {@link #getSoldeArray()})
	 * 
	 * @param date
	 * @return
	 */
	private NSArray<I_Solde> getSoldeDispoArray(NSTimestamp date) {
		NSArray<I_Solde> array = null;

		// les soldes disponibles avant la date de valeur
		EOQualifier qual = ERXQ.lessThanOrEqualTo(
					I_Solde.D_DEB_KEY, date);

		array = EOQualifier.filteredArrayWithQualifier(
				getSoldeArray(), qual);

		return array;
	}

	// totaux

	/**
	 * @return
	 */
	public final BigDecimal getCreditPerenneTotal(NSTimestamp avantDate) {
		BigDecimal total = null;

		total = (BigDecimal) getSoldePerenneArray(avantDate).valueForKey(
				"@sum." + I_CetCredit.CREDIT_KEY);

		return total;
	}

	/**
	 * @return
	 */
	public final BigDecimal getCreditAncienTotal() {
		BigDecimal total = null;

		total = (BigDecimal) getSoldeAncienArray().valueForKey(
				"@sum." + I_CetCredit.CREDIT_KEY);

		return total;
	}

	/**
	 * @return
	 */
	public final BigDecimal getCreditTotal() {
		BigDecimal total = null;

		total = (BigDecimal) getSoldeArray().valueForKey(
				"@sum." + I_CetCredit.CREDIT_KEY);

		return total;
	}

	/**
	 * @return
	 */
	public final Integer getDebitPerenneTotal(NSTimestamp avantDate) {
		int total = 0;

		for (I_Solde solde : getSoldePerenneArray(avantDate)) {
			for (I_Operation operation : solde.getSoldeDelegate().getOperationArray()) {
				if (DateCtrlHamac.isBeforeEq(operation.getOperationDelegate().dDeb(), avantDate)) {
					total += operation.getOperationDelegate().getDebitSurSolde(solde).intValue();
				}
			}
		}

		return Integer.valueOf(total);
	}

	/**
	 * @return
	 */
	public final Integer getDebitAncienTotal() {
		int total = 0;

		for (I_Solde solde : getSoldeAncienArray()) {
			for (I_Operation operation : solde.getSoldeDelegate().getOperationArray()) {
				total += operation.getOperationDelegate().getDebitSurSolde(solde).intValue();
			}
		}

		return Integer.valueOf(total);
	}

	/**
	 * @return
	 */
	public final BigDecimal getDebitTotal() {
		BigDecimal total = null;

		total = (BigDecimal) getSoldeArray().valueForKey(
				"@sum." + I_CetCredit.DEBIT_KEY);

		return total;
	}

	/**
	 * @return
	 */
	public final Integer getSoldeTotal() {
		int total = 0;

		total = getCreditTotal().intValue() - getDebitTotal().intValue();

		return Integer.valueOf(total);
	}
}
