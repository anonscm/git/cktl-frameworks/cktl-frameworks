/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageOccupation;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

/**
 * Opération permettant de créditer un service du fait de la disparition de la
 * pause RTT dans le cas d'un horaire multiservice
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class TransfertDisparitionPauseRtt
		implements I_Operation {

	private OperationDelegate operationDelegate;
	private PlageOccupation plageOccupation;
	private double taux;

	/**
	 * @param plageOccupation
	 * @param taux
	 * @param isPauseExclue
	 *          TODO
	 */
	public TransfertDisparitionPauseRtt(PlageOccupation plageOccupation, double taux) {
		super();
		this.plageOccupation = plageOccupation;
		this.taux = taux;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	/**
	 * La {@link PlageOccupation} pour l'affichage
	 * 
	 * @return
	 */
	private final PlageOccupation getPlageOccupation() {
		return plageOccupation;
	}

	@Override
	public String toString() {
		String toString = "";

		toString += "Transfert ";
		toString += (taux * (double) 100) + "% pause RTT suite au(x) congé(s) du " +
					DateCtrlHamac.dateToString(getPlageOccupation().getJour().getDate());

		return toString;
	}

}
