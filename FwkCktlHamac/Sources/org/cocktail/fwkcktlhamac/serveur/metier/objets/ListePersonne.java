/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPreference;
import org.cocktail.fwkcktlhamac.serveur.util.MiscCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Objet représentant une liste de personne
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class ListePersonne {

	private NSMutableArray<IPersonne> personneArray;
	private String titre;

	public ListePersonne() {
		super();
		personneArray = new NSMutableArray<IPersonne>();
	}

	public final void addPersonne(IPersonne eoIndividu) {
		if (!personneArray.containsObject(eoIndividu)) {
			personneArray.addObject(eoIndividu);
		}
	}

	public final void removePersonne(IPersonne eoIndividu) {
		personneArray.removeIdenticalObject(eoIndividu);
	}

	public final NSMutableArray<IPersonne> getPersonneArray() {
		return personneArray;
	}

	public final void setPersonneArray(NSMutableArray<IPersonne> personneArray) {
		this.personneArray = personneArray;
	}

	public final String getTitre() {
		return titre;
	}

	public final void setTitre(String titre) {
		this.titre = titre;
	}

	

	/**
	 * Instancier la liste de {@link ListePersonne} a partir de la chaine
	 * 
	 * @param ec
	 * @param chaine
	 * @return
	 */
	public final static NSMutableArray<ListePersonne> getListePersonneArrayFromString(
			EOEditingContext ec, String chaine) {
		NSMutableArray<ListePersonne> array = new NSMutableArray<ListePersonne>();

		NSArray<String> strListeArray = NSArray.componentsSeparatedByString(chaine, EOPreference.SEP_LISTE);

		for (String strListe : strListeArray) {

			ListePersonne lp = new ListePersonne();

			NSArray<String> strTitrePersonne = NSArray.componentsSeparatedByString(strListe, EOPreference.SEP_TITRE_PERSONNE);

			// titre
			lp.setTitre(strTitrePersonne.objectAtIndex(0));

			// personnes
			NSArray<String> strListePersonne = NSArray.componentsSeparatedByString(strTitrePersonne.objectAtIndex(1), EOPreference.SEP_PERSONNE);

			for (String strPersId : strListePersonne) {

				EOIndividu eoIndividu = EOIndividu.individuWithPersId(ec, Integer.valueOf(Integer.parseInt(strPersId)));

				lp.addPersonne(eoIndividu);

			}

			array.addObject(lp);

		}

		return array;
	}

	/**
	 * La réciproque de
	 * {@link #getListePersonneArrayFromString(EOEditingContext, String)}
	 * 
	 * @param array
	 */
	private final static String getStringFromListePersonneArray(NSMutableArray<ListePersonne> array) {
		String str = "";

		for (ListePersonne lp : array) {

			if (!StringCtrl.isEmpty(lp.getTitre())) {
				str += lp.getTitre();
			}

			str += EOPreference.SEP_TITRE_PERSONNE;

			String strPers = "";

			for (IPersonne personne : lp.getPersonneArray()) {

				strPers += Integer.toString(personne.persId().intValue());
				strPers += EOPreference.SEP_PERSONNE;

			}

			// oter le dernier separateur
			if (strPers.length() > 0) {
				strPers = strPers.substring(0, strPers.length() - EOPreference.SEP_PERSONNE.length());
			}

			str += strPers;
			str += EOPreference.SEP_LISTE;
		}

		// oter le dernier separateur
		if (str.length() > 0) {
			str = str.substring(0, str.length() - EOPreference.SEP_LISTE.length());
		}

		return str;
	}

	/**
	 * Enregistrer les modifications des listes
	 * 
	 * @param user
	 */
	public final static void sauvegarderChangement(HamacApplicationUser user) {
		String strListePersonne = getStringFromListePersonneArray(user.getListePersonneArray());
		System.out.println(">> " + strListePersonne);
		// recharger les preferences
		EOPreference.updatePrefAppli(user, null, null, null, null, null, strListePersonne, null);
		sauverListes(user);
		EOPreference.loadPreferences(user);
	}

	private final static boolean sauverListes(HamacApplicationUser user) {
		try {
			MiscCtrl.sauvegarde(user.getEditingContext());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Construire un qualifier pour filtrer des demandes a partir de l'objet
	 * courant
	 * 
	 * @return
	 */
	public final EOQualifier buildQualifierFromDemande() {
		EOQualifier qual = null;

		if (getPersonneArray().count() > 0) {
			for (IPersonne personne : getPersonneArray()) {
				qual = ERXQ.or(qual,
						ERXQ.equals(I_Demande.PERS_ID, personne.persId()));
			}
		} else {
			// pas de résultat
			qual = ERXQ.isNull(I_Demande.PERS_ID);
		}

		return qual;
	}
}
