/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.cocktail.fwkcktlhamac.serveur.metier.EOAbsenceLegale;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVacanceScolaire;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FixedGregorianCalendar;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe factory de {@link Planning}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public final class PlanningFactory {

	/**
	 * Construire la structure année - mois - semaine - jours en lien avec la
	 * période associée au planning
	 * 
	 * @param eoVacanceScolaireArray
	 *          TODO
	 */
	protected final static NSMutableArray<Mois> construireCalendrier(
			NSTimestamp dateDebut,
			NSTimestamp dateFin,
			NSArray<NSTimestamp> joursFeries,
			NSArray<TravailAttendu> attenduArray,
			Planning planning,
			NSArray<EOVacanceScolaire> eoVacanceScolaireArray) {

		NSMutableArray<Mois> moisArray = new NSMutableArray<Mois>();

		// construction du planning : mois, semaine, jours
		NSTimestamp dateJour = dateDebut;

		// pour construire les liens entre semaines
		Semaine semainePrecedente = null;

		boolean isTodayPositionne = false;
		NSTimestamp today = DateCtrlHamac.now();

		while (DateCtrlHamac.isBeforeEq(dateJour, dateFin)) {

			FixedGregorianCalendar dateJourGc = new FixedGregorianCalendar();
			dateJourGc.setTime(dateJour);

			int prevNumMois = dateJourGc.get(Calendar.MONTH);
			int numMois = prevNumMois;

			Mois mois = new Mois(dateJour, planning);

			while (DateCtrlHamac.isBeforeEq(dateJour, dateFin) && numMois == prevNumMois) {

				Semaine semaine = new Semaine(dateJour, mois);
				semaine.setSemainePrecedente(semainePrecedente);

				int prevNumSemaine = dateJourGc.get(Calendar.WEEK_OF_YEAR);
				int numSemaine = prevNumSemaine;

				semaine.setNumeroSemaine(numSemaine);

				while (DateCtrlHamac.isBeforeEq(dateJour, dateFin) &&
						numSemaine == prevNumSemaine &&
						numMois == prevNumMois) {

					Jour jour = new Jour(dateJour, semaine);

					// aujourd'hui
					if (!isTodayPositionne &&
							DateCtrlHamac.isSameDay(dateJour, today)) {
						jour.addStatut(Jour.HIGHLIGHT_TODAY);
						isTodayPositionne = true;
					}

					//
					NSArray<TravailAttendu> attenduJourArray = TravailAttendu.filtrerTravailAttenduPourDate(
							attenduArray, jour.getDate());

					if (attenduJourArray.count() == 0) {
						jour.addStatut(I_ConstsJour.STATUS_HORS_AFFECTATION);
					}

					// jour chome
					if (jour.isDimanche()) {
						jour.addStatut(I_ConstsJour.STATUS_CHOME);
					}

					// jour ferie
					if (joursFeries.containsObject(dateJour)) {
						jour.addStatut(I_ConstsJour.STATUS_FERIE);
					}

					// vacance scolaire
					if (EOVacanceScolaire.isVacanceScolaire(eoVacanceScolaireArray, dateJour)) {
						jour.addStatut(I_ConstsJour.STATUT_VACANCE_SCOLAIRE);
					}

					for (TravailAttendu attendu : attenduJourArray) {
						jour.addTravailAttendu(attendu);
					}

					semaine.addJour(jour);

					// fixer le dernier jour (s'arretera tout seul en fin de boucle)
					semaine.setDernierJour(dateJour);

					dateJour = dateJour.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);

					dateJourGc.setTime(dateJour);
					numMois = dateJourGc.get(Calendar.MONTH);
					numSemaine = dateJourGc.get(Calendar.WEEK_OF_YEAR);
				}

				semaine.determinerStatutsJourPourInterface();

				mois.addSemaine(semaine);

				if (semainePrecedente != null) {
					semainePrecedente.setSemaineSuivante(semaine);
				}
				semainePrecedente = semaine;
			}
			moisArray.addObject(mois);
		}

		return moisArray;

	}

	// tmp editions

	private final static String CSV_COLUMN_SEPARATOR = "\t";
	private final static String CSV_NEW_LINE = "\n";

	private final static String SPARKLINE_START_LINE = "data.addRow([\"";
	private final static String SPARKLINE_START_LINE_AFTER_DATE = "\"";
	private final static String SPARKLINE_SEPARATOR = ", ";
	private final static String SPARKLINE_END_LINE = "]);\n";

	private final static String DRAW_FUNCTION_START = "google.load('visualization', '1', {'packages':['annotatedtimeline']});\n" +
			"google.setOnLoadCallback(drawVisualization);\n" +
			"function drawVisualization() {\n" +
			"\tvar data = new google.visualization.DataTable();\n";
	private final static String ADD_COLUMN_START = "\tdata.addColumn(";
	private final static String ADD_COLUMN_END = ");\n";
	private final static String ADD_ROWS_START = "data.addRows([";
	private final static String ADD_SINGLE_ROW_START = "\t[";
	private final static String ADD_SINGLE_ROW_END = "]";
	private final static String ADD_SINGLE_ROW_END_WITH_NEXT = ",\n";
	private final static String ADD_SINGLE_ROW_END_LAST = "\n\t]);\n";
	private final static String DRAW_FUNCTION_END = "\tvar annotatedtimeline = new google.visualization.AnnotatedTimeLine(\n" +
			"\t\tdocument.getElementById('visualization'));\n" +
			"\tannotatedtimeline.draw(data, {'displayAnnotations': true});\n}";

	public final static String getCodeJsAnnotedTimeLine(
			Planning planning,
			NSArray<I_Operation> operationArray,
			NSArray<I_Solde> soldeArray) {

		return getMapExport(planning, operationArray, soldeArray).get("annotedTimeLine");

	}

	/**
	 * Temporaire : export CSV des courbes de soldes par opération
	 */
	private final static HashMap<String, String> getMapExport(
			Planning planning,
			NSArray<I_Operation> operationArray,
			NSArray<I_Solde> soldeArray) {

		HashMap<String, String> map = new HashMap<String, String>();

		String strCsv = "";

		String strSparkline = "";

		String strAnnotedTimeLine = "";

		strAnnotedTimeLine += DRAW_FUNCTION_START;

		strCsv += "date";

		strAnnotedTimeLine += ADD_COLUMN_START + "'date', 'Date'" + ADD_COLUMN_END;

		for (I_Solde solde : soldeArray) {
			String strSolde = solde.toString();
			if (solde instanceof EOSolde) {
				strSolde = ((EOSolde) solde).toTypeSolde().stypCode();
			}
			strCsv += CSV_COLUMN_SEPARATOR + solde.toString();
			strAnnotedTimeLine += ADD_COLUMN_START + "'number', '" + StringCtrl.chaineSansAccents(strSolde) + "'" + ADD_COLUMN_END;
		}

		strCsv += CSV_NEW_LINE;

		strAnnotedTimeLine += ADD_ROWS_START;

		NSMutableDictionary<I_Solde, String> dico = new NSMutableDictionary<I_Solde, String>();

		NSMutableArray<String> codeArray = new NSMutableArray<String>();

		int dureeJour = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, planning.getEoPlanning().toPeriode());
		
		for (I_Operation operation : operationArray) {

			NSTimestamp dateCourante = operation.getOperationDelegate().dDeb();

			// positionner les operations sans date ou antérieures au début de l'année
			if (operation.getOperationDelegate().dDeb() == null ||
					DateCtrlHamac.isBefore(operation.getOperationDelegate().dDeb(), planning.getPremierJour())) {
				dateCourante = planning.getPremierJour();
			}

			strCsv += DateCtrlHamac.dateToString(dateCourante);

			strSparkline += SPARKLINE_START_LINE;
			strSparkline += DateCtrlHamac.dateToString(dateCourante);
			strSparkline += SPARKLINE_START_LINE_AFTER_DATE;

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(dateCourante);

			strAnnotedTimeLine += ADD_SINGLE_ROW_START;
			strAnnotedTimeLine += "new Date(" + gc.get(GregorianCalendar.YEAR) + ", " + (gc.get(GregorianCalendar.MONTH) + 1) + ", " + gc.get(GregorianCalendar.DAY_OF_MONTH) + ")";

			for (I_Solde solde : soldeArray) {

				// ignorer les soldes sans transaction
				// if (solde.getSoldeDelegate().getOperationArray().count() == 0) {
				// continue;
				// }

				strCsv += CSV_COLUMN_SEPARATOR;
				strSparkline += SPARKLINE_SEPARATOR;
				strAnnotedTimeLine += ",";

				String restant = null;

				String code = "$" + solde.hashCode() + "$";

				// est-ce que le solde a été impacté par l'opération
				if (solde.getSoldeDelegate().getOperationArray().contains(operation)) {
					restant = "" + FormatterCtrl.enJourArrondi2ChiffresApresVirgule(
							solde.getSoldeDelegate().restantApresOperation(operation),
							dureeJour);
					dico.setObjectForKey(restant, solde);

					if (!codeArray.contains(code)) {
						strCsv = StringCtrl.replace(strCsv, code, restant);
						strSparkline = StringCtrl.replace(strSparkline, code, restant);
						strAnnotedTimeLine = StringCtrl.replace(strAnnotedTimeLine, code, restant.equals("0") ? "null" : restant);
						codeArray.add(code);
					}

				} else {
					restant = "";
					if (StringCtrl.isEmpty(dico.objectForKey(solde))) {
						// restant = code;
						restant = "0";
					} else {
						restant = dico.objectForKey(solde);
					}
				}

				strCsv += restant;
				strSparkline += restant;
				strAnnotedTimeLine += restant;

				if (soldeArray.indexOfIdenticalObject(solde) == soldeArray.count() - 1) {
					strAnnotedTimeLine += ADD_SINGLE_ROW_END;
					if (operationArray.indexOfIdenticalObject(operation) == operationArray.count() - 1) {
						strAnnotedTimeLine += ADD_SINGLE_ROW_END_LAST;
					} else {
						strAnnotedTimeLine += ADD_SINGLE_ROW_END_WITH_NEXT;
					}
				}

			}

			strCsv += CSV_NEW_LINE;
			strSparkline += SPARKLINE_END_LINE;

		}

		strAnnotedTimeLine += DRAW_FUNCTION_END;
		//
		// System.out.println(strCsv);
		// System.out.println(strSparkline);
		// System.out.println(strAnnotedTimeLine);

		map.put("csv", strCsv);
		map.put("sparkLine", strSparkline);
		map.put("annotedTimeLine", strAnnotedTimeLine);

		return map;

	}

	/**
	 * Liste des absences légales issues de ManGUE (uniquement pour les planning
	 * d'individus)
	 * 
	 * @return
	 */
	private final static NSArray<EOAbsenceLegale> getAbsenceLegaleArray(Planning planning) {
		NSArray<EOAbsenceLegale> absencesArray = new NSArray<EOAbsenceLegale>();

		IPersonne proprietaire = planning.getEoPlanning().toPersonne();

		if (proprietaire.isIndividu()) {

			EOEditingContext ec = planning.getEoPlanning().editingContext();

			EOIndividu eoIndividu = EOIndividu.individuWithPersId(ec, proprietaire.persId());

			EOQualifier qual = ERXQ.equals(EOAbsenceLegale.TO_INDIVIDU_KEY, eoIndividu);

			absencesArray = EOAbsenceLegale.fetchAll(ec, qual, null);
		}

		return absencesArray;
	}

	/**
	 * La liste de toutes les absences gérables par l'application : occupations
	 * hamac + absences légales.
	 * 
	 * Elles sont classées pour que soit correctement fait le chargement (par
	 * priorité et date)
	 * 
	 * @return
	 */
	public final static NSArray<A_Absence> getAbsenceArraySortedChargementArray(
			Planning planning) {

		NSArray<A_Absence> _absenceArraySorted = null;

		//
		NSArray<EOOccupation> occupationArray = planning.getEoPlanning().getOccupationArray(planning.getTravailAttenduArray());
		NSArray<EOAbsenceLegale> absenceLegaleArray = getAbsenceLegaleArray(planning);

		_absenceArraySorted = new NSArray<A_Absence>();

		for (int i = 0; i < occupationArray.count(); i++) {
			_absenceArraySorted = _absenceArraySorted.arrayByAddingObject(occupationArray.objectAtIndex(i));
		}

		for (int i = 0; i < absenceLegaleArray.count(); i++) {
			_absenceArraySorted = _absenceArraySorted.arrayByAddingObject(absenceLegaleArray.objectAtIndex(i));
		}

		_absenceArraySorted = filtrerAbsenceArraySorted(planning, _absenceArraySorted);

		return _absenceArraySorted;

	}
	
	
	/**
	 * La liste de toutes les occupations hamac sans les absences légales
	 * 
	 * Elles sont classées pour que soit correctement fait le chargement (par
	 * priorité et date)
	 * 
	 * @return
	 */
	public final static NSArray<A_Absence> getAbsenceArraySortedChargementSansLegalArray(
			Planning planning) {

		NSArray<A_Absence> _absenceArraySorted = null;

		//
		NSArray<EOOccupation> occupationArray = planning.getEoPlanning().getOccupationArray(planning.getTravailAttenduArray());
	
		_absenceArraySorted = new NSArray<A_Absence>();

		for (int i = 0; i < occupationArray.count(); i++) {
			_absenceArraySorted = _absenceArraySorted.arrayByAddingObject(occupationArray.objectAtIndex(i));
		}

		_absenceArraySorted = filtrerAbsenceArraySorted(planning, _absenceArraySorted);

		return _absenceArraySorted;

	}
	
	/**
	 * Classement des absences pour que soit correctement fait le chargement (par
	 * priorité et date)
	 * @param planning
	 * @param _absenceArraySorted
	 * @return
	 */
	private final static NSArray<A_Absence> filtrerAbsenceArraySorted(Planning planning, NSArray<A_Absence> _absenceArraySorted) {
		
		// classement par priorité + chronologique
		_absenceArraySorted = CktlSort.sortedArray(_absenceArraySorted,
					A_Absence.PRIORITE_KEY + "," + A_Absence.DATE_DEBUT_KEY);

		// filtrer sur la période du planning
		NSTimestamp dDebut = TimeCtrl.remonterAMinuit(planning.getEoPlanning().toPeriode().perDDebut());
		NSTimestamp dFin = TimeCtrl.remonterAMinuit(planning.getEoPlanning().toPeriode().perDFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0));

		EOQualifier qual = A_Absence.getQualifierAbsencesIncluesDansPeriode(dDebut, dFin);

		// oter celle qui sont refusées / supprimées ...
		qual = ERXQ.and(ERXQ.isTrue(A_Absence.IS_VISIBLE_KEY), qual);
		_absenceArraySorted = EOQualifier.filteredArrayWithQualifier(_absenceArraySorted, qual);
		
		return _absenceArraySorted;
		
	}
	
	
	
}
