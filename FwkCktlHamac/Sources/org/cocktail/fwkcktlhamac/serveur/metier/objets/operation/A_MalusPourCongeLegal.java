/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_MalusPourCongeLegal
		implements I_Operation {

	private OperationDelegate operationDelegate;
	private int minutes;
	private EOPeriode periode;

	public A_MalusPourCongeLegal(int minutes) {
		super();
		this.minutes = minutes;
	}

	public A_MalusPourCongeLegal(int minutes, EOPeriode periode) {
		super();
		this.minutes = minutes;
		this.periode = periode;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	public abstract String libelle();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "";

		str = libelle() + " (" + TimeCtrl.stringForMinutes(minutes) + ")";

		return str;
	}

	public final int getMinutes() {
		return minutes;
	}

	public final EOPeriode getPeriode() {
		return periode;
	}


}
