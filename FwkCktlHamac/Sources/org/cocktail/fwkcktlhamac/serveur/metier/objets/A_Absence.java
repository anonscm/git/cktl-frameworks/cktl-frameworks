/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonneTypageComposante;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAbsenceLegale;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.A_OccupationTypeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.HeureSupplementaireDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.EOStructureFixed;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Methodes communes aux objets décrivant une occupation : {@link EOOccupation}
 * et {@link EOAbsenceLegale}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_Absence
		extends A_ToManyPersonneTypageComposante
		implements I_Operation, I_Demande, I_LigneFicheRose {

	public final static String DATE_DEBUT_KEY = "dateDebut";
	public final static String DATE_FIN_KEY = "dateFin";
	public final static String PRIORITE_KEY = "priorite";
	public final static String IS_VISIBLE_KEY = "isVisible";
	public final static String IS_ABSENCE_LEGALE_KEY = "isAbsenceLegale";
	public final static String IS_PRESENCE_KEY = "isPresence";
	public final static String DUREE_KEY = "duree";
	public final static String DUREE_NEGATIVE_KEY = "dureeNegative";
	public final static String IS_VALIDEE_KEY = "isValidee";

	private final static int MINUTES_MIDI_DANS_JOUR = 12 * 60;

	private NSMutableArray<PlageOccupation> _plageOccupationArray;
	private OperationDelegate _operationDelegate;
	private NSArray<EOPlanning> _eoPlanningArrayPourOccupation;
	
	/**
	 * Etat de validation
	 * 
	 * @return
	 */
	public abstract EOTypeStatut toTypeStatut();

	/**
	 * date de début de l'absence
	 */
	public abstract NSTimestamp dateDebut();

	/**
	 * date de fin de l'absence
	 */
	public abstract NSTimestamp dateFin();

	/**
	 * motif de l'absence
	 */
	public abstract String motif();

	/**
	 * motif de l'absence (court)
	 * 
	 * @return
	 */
	public String motifCourt() {
		return FormatterCtrl.getLibelleCourt(motif(), TAILLE_MOTIF_COURT);
	}

	/**
	 * Le libellé texte du type de l'absence
	 * 
	 * @return
	 */
	public abstract String libelleType();

	/**
	 * Le libellé texte du type de l'absence (court)
	 * 
	 * @return
	 */
	public String libelleTypeCourt() {
		return FormatterCtrl.getLibelleCourt(libelleType(), TAILLE_TYPE_LIBELLE_COURT);
	}

	/**
	 * Absence acceptée par le(s) responsable(s)
	 * 
	 * @return
	 */
	public abstract boolean isValidee();

	/**
	 * Absence visible (i.e. non supprimée / refusée)
	 * 
	 * @return
	 */
	public abstract boolean isVisible();

	/**
	 * Mémoriser la valeur du cout de l'absence
	 */
	public abstract void setCoutMinute(EOPlanning eoPlanning);

	/**
	 * La priorité de cette occupation par rapport aux autres
	 */
	public abstract int priorite();

	/**
	 * Indique s'il faut forcer les horaires pour ce type d'absence si la durée
	 * totale dépasse 1 semaine
	 * 
	 * @return
	 */
	public abstract boolean isHoraireForceRecouvrementSemaine(EOPeriode periode);

	/**
	 * La classe de gestion de l'occupation
	 * 
	 * @return
	 */
	public abstract A_OccupationTypeDelegate getOccupationTypeDelegate();

	public abstract NSTimestamp dCreation();

	public abstract NSTimestamp dModification();

	//

	/**
	 * 
	 */
	public final NSMutableArray<PlageOccupation> getPlageOccupationArray() {
		if (_plageOccupationArray == null) {
			_plageOccupationArray = new NSMutableArray<PlageOccupation>();
		}
		return _plageOccupationArray;
	}

	/**
	 * Forcer le RAZ des plages d'occupations pour ne pas avoir de doublons lors
	 * du rechargement d'un planning pour une {@link A_Absence} ayant déjà été
	 * chargée
	 * 
	 * XXX renommer en clearCache
	 */
	public final void resetPlageOccupation() {
		_plageOccupationArray = null;
		_eoPlanningArrayPourOccupation = null;
	}

	/**
	 * 
	 */
	public final void addPlageOccupation(PlageOccupation plage) {
		getPlageOccupationArray().addObject(plage);
	}

	/**
	 * Durée total sur temps de travail
	 * 
	 * TODO élargire aux application recreditées
	 * 
	 * @return
	 */
	public int getDuree() {
		int duree = 0;

		if (isAbsenceLegale()) {
			for (PlageOccupation plage : getPlageOccupationArray()) {
				duree += plage.getPlageTravail().getDureeMinutes();
			}
		} else {
			duree = getDebit();
		}

		return duree;
	}

	/**
	 * Nombre de journées occupées (n'est applicable que pour les absences à la
	 * 1/2 journée
	 */
	public final Float getNbJourneeSurHoraire() {
		float nb = (float) 0;

		if (isAbsenceDemiJournee()) {
			nb = ((float) getPlageOccupationArray().count()) * (float) 0.5;
		}

		return nb;
	}

	/**
	 * Tooltip demi journée
	 * 
	 * @return
	 */
	public final String getTipNbJourneeSurHoraire() {
		String tip = "";

		if (isAbsenceDemiJournee()) {

			boolean isJourChange = true;
			String prevStrDate = null;

			for (PlageOccupation po : getPlageOccupationArray()) {

				String strDate = DateCtrlHamac.dateToString(po.getJour().getDate());

				if (prevStrDate == null ||
						!strDate.equals(prevStrDate)) {
					isJourChange = true;
				} else {
					isJourChange = false;
				}

				if (isJourChange) {
					tip += "; " + strDate + " : ";
				} else {
					tip += "-";
				}

				if (po.getPlageTravail().isAm()) {
					tip += "AM";
				} else if (po.getPlageTravail().isPm()) {
					tip += "PM";
				}

				prevStrDate = strDate;

			}

			// oter le premier ", "
			if (tip.length() > 0) {
				tip = tip.substring(2, tip.length());
			}

		}

		return tip;
	}

	/**
	 * Tooltip
	 * 
	 * @return
	 */
	public final String getTipOccupationHoraire() {
		String tip = "";

		if (isAbsenceDemiJournee()) {

			boolean isJourChange = true;
			String prevStrDate = null;

			for (PlageOccupation po : getPlageOccupationArray()) {

				String strDate = DateCtrlHamac.dateToString(po.getJour().getDate());

				if (prevStrDate == null ||
							!strDate.equals(prevStrDate)) {
					isJourChange = true;
				} else {
					isJourChange = false;
				}

				if (isJourChange) {
					tip += "; " + strDate + " : ";
				} else {
					tip += ", ";
				}

				for (A_Segment segment : po.getSegmentArrayADebiter()) {
					tip += TimeCtrl.stringForMinutes(segment.getDebut()) + "-" + TimeCtrl.stringForMinutes(segment.getFin());
				}

				prevStrDate = strDate;

			}

			// oter le premier ", "
			if (tip.length() > 0) {
				tip = tip.substring(2, tip.length());
			}

		} else if (isAbsenceMinute()) {

			if (isPresence()) {
				//
				if (isHeureSupplementaire()) {
					tip = ((HeureSupplementaireDelegate) getOccupationTypeDelegate()).getChaineTip();
				}

			} else {

				// détail sur l'horaire impacté
				if (getPlageOccupationArray().count() > 0) {
					tip = "Plage de travail impactée : " + getPlageOccupationArray().objectAtIndex(0).getPlageTravail();
				}

			}

		}

		return tip;
	}

	/**
	 * Pour affichage dans la fiche rose
	 * 
	 * @return
	 */
	public int getDureeNegative() {
		return -getDuree();
	}

	/**
	 * Débit total indépendamment du solde
	 * 
	 * @return
	 */
	public int getDebit() {
		int debit = 0;

		for (PlageOccupation plage : getPlageOccupationArray()) {
			debit += plage.getDebitSurSolde(null);
		}

		return debit;
	}

	/**
	 * Indique si l'absence est légale (i.e. c'est un objet du type
	 * {@link EOAbsenceLegale})
	 * 
	 * @return
	 */
	public boolean isAbsenceLegale() {
		boolean isAbsenceLegale = false;

		if (this instanceof EOAbsenceLegale) {
			isAbsenceLegale = true;
		}

		return isAbsenceLegale;
	}

	/**
	 * Indique si l'absence est à débiter à la minute
	 * 
	 * @return
	 */
	public boolean isAbsenceMinute() {
		boolean isAbsenceMinute = false;

		// les congés légaux sont tous à la demi journée
		if (isAbsenceLegale() == false) {
			EOOccupation eoOccupation = (EOOccupation) this;
			if (eoOccupation.toTypeOccupation().isOccupationMinute()) {
				isAbsenceMinute = true;
			}
		}

		return isAbsenceMinute;
	}

	/**
	 * Indique si l'absence est à débiter à la demi journée
	 * 
	 * @return
	 */
	public boolean isAbsenceDemiJournee() {
		boolean isAbsenceDemiJournee = false;

		if (isAbsenceMinute() == false) {
			isAbsenceDemiJournee = true;
		}

		return isAbsenceDemiJournee;
	}

	/**
	 * Indique si le début est le matin
	 * 
	 * @return
	 */
	public boolean isDebuteLeMatin() {
		boolean isDebuteLeMatin = false;

		int minutesDebut = TimeCtrl.getMinutesOfDay(dateDebut());

		if ((isAbsenceDemiJournee() && minutesDebut == 0) ||
				(isAbsenceMinute() && minutesDebut >= 0 && minutesDebut <= MINUTES_MIDI_DANS_JOUR)) {
			isDebuteLeMatin = true;
		}

		return isDebuteLeMatin;
	}

	/**
	 * Indique si la fin est le matin
	 * 
	 * @return
	 */
	public boolean isTermineLeMatin() {
		boolean isTermineLeMatin = false;

		int minutesFin = TimeCtrl.getMinutesOfDay(dateFin());

		if ((isAbsenceDemiJournee() && minutesFin == 0) ||
				(isAbsenceMinute() && minutesFin >= 0 && minutesFin <= MINUTES_MIDI_DANS_JOUR)) {
			isTermineLeMatin = true;
		}

		return isTermineLeMatin;
	}

	/**
	 * Indique si le début est l'apres midi
	 * 
	 * @return
	 */
	public boolean isDebuteLApresMidi() {
		boolean isDebuteLApresMidi = false;

		int minutesDebut = TimeCtrl.getMinutesOfDay(dateDebut());

		if ((isAbsenceDemiJournee() && minutesDebut == MINUTES_MIDI_DANS_JOUR) ||
				(isAbsenceMinute() && minutesDebut >= MINUTES_MIDI_DANS_JOUR && minutesDebut <= Jour.DUREE_JOURNEE_EN_MINUTES)) {
			isDebuteLApresMidi = true;
		}

		return isDebuteLApresMidi;
	}

	/**
	 * Indique si la fin est l'apres midi
	 * 
	 * @return
	 */
	public boolean isTermineLApresMidi() {
		boolean isTermineLApresMidi = false;

		int minutesFin = TimeCtrl.getMinutesOfDay(dateFin());

		if ((isAbsenceDemiJournee() && minutesFin == MINUTES_MIDI_DANS_JOUR) ||
				(isAbsenceMinute() && minutesFin >= MINUTES_MIDI_DANS_JOUR && minutesFin <= Jour.DUREE_JOURNEE_EN_MINUTES)) {
			isTermineLApresMidi = true;
		}

		return isTermineLApresMidi;
	}

	/**
	 * Indique si l'absence est du type heures supplémentaires
	 * 
	 * @return
	 */
	public boolean isHeureSupplementaire() {
		boolean isHeureSupplementaire = false;

		if (getOccupationTypeDelegate() instanceof HeureSupplementaireDelegate) {
			isHeureSupplementaire = true;
		}

		return isHeureSupplementaire;
	}

	/**
	 * Indique si l'absence est une présence
	 */
	public boolean isPresence() {
		boolean isPresence = false;

		if (this instanceof EOOccupation) {
			EOOccupation eoOccupation = (EOOccupation) this;
			if (eoOccupation.toTypeOccupation().isPresence()) {
				isPresence = true;
			}
		}

		return isPresence;
	}

	// tempo pour affichage fiche rose

	@Override
	public String toString() {
		String toString = "";

		String dateFormatDebut = null;
		String dateFormatFin = null;

		if (isAbsenceDemiJournee()) {
			dateFormatDebut = I_ConstsJour.ABS_DATE_FORMAT_ABS_DEMI_JOURNEE_DEBUT;
			dateFormatFin = I_ConstsJour.ABS_DATE_FORMAT_ABS_DEMI_JOURNEE_FIN;
		} else {
			dateFormatDebut = I_ConstsJour.ABS_DATE_FORMAT_ABS_MINUTE_DEBUT;
			dateFormatFin = I_ConstsJour.ABS_DATE_FORMAT_ABS_MINUTE_FIN;
		}

		toString = DateCtrlHamac.dateToString(dateDebut(), dateFormatDebut) + " - " +
				DateCtrlHamac.dateToString(dateFin(), dateFormatFin) + " " +
				libelleType();

		if (isAbsenceLegale() == false &&
				!StringCtrl.isEmpty(motif())) {
			toString += " : " + motif();
		}

		return toString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, this);
		}
		return _operationDelegate;
	}

	/**
	 * La {@link EOStructure} d'application s'il y a lieu :
	 * 
	 * - heures supplémentaires
	 * 
	 * - astreinte
	 * 
	 * - ...
	 * 
	 */
	public abstract EOStructure toStructureApplication();

	/**
	 * La liste des {@link EOPlanning}, et indirectement des plannings concernés
	 * par une occupation. On se base sur la(les) période d'après les dates
	 * 
	 * @return
	 */
	public final NSArray<EOPlanning> getEoPlanningArrayPourOccupation() {
		if (_eoPlanningArrayPourOccupation == null) {
			_eoPlanningArrayPourOccupation = getEoPlanningArrayPourOccupationEtPersId(persId());
		}
		return _eoPlanningArrayPourOccupation;
	}

	/**
	 * La liste des {@link EOPlanning}, et indirectement des plannings concernés
	 * par une occupation. On se base sur la(les) période d'après les dates
	 * 
	 * @return
	 */
	public final NSArray<EOPlanning> getEoPlanningArrayPourOccupationAnneeCivile(NSTimestamp dateDebut, NSTimestamp dateFin) {
		return getEoPlanningArrayPourOccupationEtPersId(persId(), dateDebut, dateFin);
	}
	
	/**
	 * La liste des {@link EOPlanning}, et indirectement des plannings concernés
	 * par une occupation. On se base sur la(les) période d'après les dates
	 * 
	 * @return
	 */
	public final NSArray<EOPlanning> getEoPlanningArrayPourOccupationEtPersId(Number persId) {
		return getEoPlanningArrayPourOccupationEtPersId(persId, dateDebut(), dateFin());
	}
	
	
	public final NSArray<EOPlanning> getEoPlanningArrayPourOccupationEtPersId(Number persId, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray<EOPlanning> array = new NSMutableArray<EOPlanning>();

		// liste des périodes concernées
		NSArray<EOPeriode> periodeArray = EOPeriode.findSortedPeriodeArray(
					editingContext(), dateDebut, dateFin);

		String nature = EOPlanning.REEL;

		if (isAbsenceLegale() == false) {
			nature = ((EOOccupation) this).nature();
		}

		for (EOPeriode periode : periodeArray) {
			EOPlanning eoPlanning = EOPlanning.findEoPlanning(
						editingContext(),
						persId,
						nature,
						periode);

			// si pas de planning (ex : fermeture de service)
			if (eoPlanning != null) {
				array.addObject(eoPlanning);
			}

		}

		return array;
	}
	

	/**
	 * @param user
	 * @return
	 */
	public boolean isSuppressionAutorisee(HamacApplicationUser user) {
		boolean isSuppressionAutorisee = true;

		// seul l'utilisateur connecte
		// TODO autoriser par délégation
		if (user.getPersId().intValue() != persId().intValue()) {
			isSuppressionAutorisee = false;
		}

		// pas les absences légales
		if (isSuppressionAutorisee) {
			if (isAbsenceLegale()) {
				isSuppressionAutorisee = false;
			}
		}

		// pas les fermetures
		if (isSuppressionAutorisee) {
			if (isFermeture()) {
				isSuppressionAutorisee = false;
			}
		}

		// pas celles déjà en cours de suppression
		if (isSuppressionAutorisee) {
			if (this instanceof EOOccupation && (
					((EOOccupation) this).toTypeStatut().isStatutEnCoursDeSuppression() ||
					((EOOccupation) this).toTypeStatut().isStatutEnCoursDeSuppressionVise())) {
				isSuppressionAutorisee = false;
			}
		}

		return isSuppressionAutorisee;
	}

	/**
	 * @return
	 */
	public boolean isFermeture() {
		boolean isFermeture = false;

		if (this instanceof EOOccupation &&
				((EOOccupation) this).toTypeOccupation().isFermeture()) {
			isFermeture = true;
		}

		return isFermeture;
	}

	/**
	 * XXX pas utilisé pour l'instant
	 * 
	 * On ne peut modifier que les conges normaux en cours de validation sur
	 * l'affectation annuelle courante
	 */
	public boolean isModificationAutorisee(HamacApplicationUser user) {
		boolean isModificationAutorisee = true;

		// seul l'utilisateur connecte
		// TODO autoriser par délégation
		if (user.getPersId().intValue() != persId().intValue()) {
			isModificationAutorisee = false;
		}

		// pas les absences légales
		if (isModificationAutorisee) {
			if (isAbsenceLegale()) {
				isModificationAutorisee = false;
			}
		}

		// pas les fermetures
		if (isModificationAutorisee) {
			if (isFermeture()) {
				isModificationAutorisee = false;
			}
		}

		// uniquement celles en cours de validation
		if (isModificationAutorisee) {
			if (this instanceof EOOccupation && (
					!((EOOccupation) this).toTypeStatut().isStatutEnCoursDeValidation() &&
					!((EOOccupation) this).toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle())) {
				isModificationAutorisee = false;
			}
		}
		return isModificationAutorisee;
	}

	/**
	 * Qualifier pour obtenir des absences inclues meme partiellement dans une
	 * période
	 * 
	 * @param prefix
	 * @param dateDebutKey
	 * @param dateFinKey
	 * @param dDebut
	 * @param dFin
	 * @return
	 */
	public final static EOQualifier getQualifierAbsencesIncluesDansPeriode(
			String prefix, String dateDebutKey, String dateFinKey, NSTimestamp dDebut, NSTimestamp dFin, EOPeriode eoPeriode) {

		EOQualifier qual = null;

		String dDebutKey = DATE_DEBUT_KEY;
		if (dateDebutKey != null) {
			dDebutKey = dateDebutKey;
		}

		String dFinKey = DATE_FIN_KEY;
		if (dateFinKey != null) {
			dFinKey = dateFinKey;
		}

		if (prefix != null) {
			dDebutKey = prefix + "." + dDebutKey;
			dFinKey = prefix + "." + dFinKey;
		}

		// plafonner date de fin à la période
		if (eoPeriode != null) {
			if (dDebut == null) {
				dDebut = eoPeriode.perDDebut();
			}
			if (dFin == null) {
				dFin = eoPeriode.perDFin();
			}
		}

		qual = ERXQ.or(
					ERXQ.and(
							ERXQ.lessThan(dDebutKey, dDebut),
							ERXQ.greaterThanOrEqualTo(dFinKey, dDebut)),
					ERXQ.and(
							ERXQ.lessThanOrEqualTo(dDebutKey, dFin),
							ERXQ.greaterThan(dFinKey, dFin)),
					ERXQ.and(
							ERXQ.greaterThanOrEqualTo(dDebutKey, dDebut),
							ERXQ.lessThanOrEqualTo(dFinKey, dFin)),
					ERXQ.and(
							ERXQ.lessThanOrEqualTo(dDebutKey, dDebut),
							ERXQ.greaterThanOrEqualTo(dFinKey, dFin)));

		return qual;

	}

	/**
	 * La date de début à usage exclusif du serveur de planning
	 * 
	 * @return
	 */
	public final NSTimestamp getDateDebutPourServeurPlanning() {

		NSTimestamp dateDebut = dateDebut();

		if (isAbsenceDemiJournee()) {

			if ((isDebuteLeMatin() && isTermineLeMatin()) ||
					isDebuteLApresMidi() && isTermineLApresMidi()) {

				if (getPlageOccupationArray().count() > 0) {

					PlageOccupation poDebut = getPlageOccupationArray().objectAtIndex(0);

					if (poDebut.getSegmentArrayADebiter().count() > 0) {

						int debut = poDebut.getSegmentArrayADebiter().objectAtIndex(0).getDebut();

						int hours = debut / 60;
						int minutes = debut % 60;

						dateDebut = poDebut.getJour().getDate().timestampByAddingGregorianUnits(0, 0, 0, hours, minutes, 0);

					}

				}

			}
		}
		return dateDebut;
	}

	public final static EOQualifier getQualifierAbsencesIncluesDansPeriode(
			NSTimestamp dDebut, NSTimestamp dFin) {
		return getQualifierAbsencesIncluesDansPeriode(null, null, null, dDebut, dFin, null);
	}

	/**
	 * La date de fin à usage exclusif du serveur de planning
	 * 
	 * @return
	 */
	public final NSTimestamp getDateFinPourServeurPlanning() {

		NSTimestamp dateFin = dateFin();

		if (isAbsenceDemiJournee()) {

			if (!isDebuteLApresMidi() && isTermineLApresMidi()) {

				dateFin = DateCtrlHamac.remonterToMinuit(dateFin).timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59);

			} else {

				if (getPlageOccupationArray().count() > 0) {

					PlageOccupation poFin = getPlageOccupationArray().lastObject();

					if (poFin.getSegmentArrayADebiter().count() > 0) {

						int debut = poFin.getSegmentArrayADebiter().lastObject().getFin();

						int hours = debut / 60;
						int minutes = debut % 60;

						dateFin = poFin.getJour().getDate().timestampByAddingGregorianUnits(0, 0, 0, hours, minutes, 0);

					}

				}

			}
		}

		return dateFin;
	}

	// DEBUT FICHE ROSE

	/**
	 * Dans l'écran balance HSUP / CCOMP, les CCOMP sont les débits. Mais on peut
	 * aussi on rajouter d'autre via la réparition des débits sur solde BALANCE
	 * 
	 * @return
	 */
	public final boolean isAbsenceAssimileeCCompPourBalance() {
		boolean isAbsenceAssimileCCompPourBalance = false;

		if (!isHeureSupplementaire() &&
				this instanceof EOOccupation) {

			EOOccupation eoOccupation = (EOOccupation) this;
			EOQualifier qual = ERXQ.equals(EORepartTypeOccupationTypeSolde.TO_TYPE_OCCUPATION_KEY, eoOccupation.toTypeOccupation());

			NSArray<EORepartTypeOccupationTypeSolde> array = EORepartTypeOccupationTypeSolde.fetchAll(
					eoOccupation.editingContext(), qual, CktlSort.newSort(EORepartTypeOccupationTypeSolde.PRIORITE_KEY));

			if (array.count() > 0) {
				// vérifier si le premier est la balance
				EORepartTypeOccupationTypeSolde eoRepart = array.objectAtIndex(0);

				if (eoRepart.toTypeSolde().isBalance()) {
					// test sur la validité O/N
					if (eoRepart.etatValidation() == null) {
						isAbsenceAssimileCCompPourBalance = true;
					} else if (eoRepart.etatValidation().equals(OUI) && isValidee()) {
						isAbsenceAssimileCCompPourBalance = true;
					} else if (eoRepart.etatValidation().equals(NON) && !isValidee()) {
						isAbsenceAssimileCCompPourBalance = true;
					}

				}

			}

		}

		return isAbsenceAssimileCCompPourBalance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getDebitTotal
	 * ()
	 */

	public final Integer getDebitTotal() {
		Integer total = null;

		if (getDebitConge() != null) {
			total = getDebitConge();
		}

		if (getDebitReliquat() != null) {
			if (total == null) {
				total = getDebitReliquat();
			} else {
				total = total + getDebitReliquat();
			}
		}

		return total;
	}

	// TODO a factoriser

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getDebitConge
	 * ()
	 */
	public Integer getDebitConge() {
		Integer debit = null;

		NSArray<I_Solde> eoSoldeArray = getOperationDelegate().getSoldeRestantApresTransactionDico().allKeys();

		if (eoSoldeArray.count() > 0) {

			for (I_Solde solde : eoSoldeArray) {

				if (solde.isCongeNatif()) {

					if (cStructureCourante == null) {
						if (debit == null) {
							debit = Integer.valueOf(0);
						}
						debit += getOperationDelegate().getDebitSurSolde(solde);

					} else {
						if (solde.cStructure() == cStructureCourante) {
							debit = getOperationDelegate().getDebitSurSolde(solde);
						}

					}

				}

			}

		}

		return debit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#
	 * getDebitReliquat()
	 */
	public Integer getDebitReliquat() {
		Integer debit = null;

		NSArray<I_Solde> eoSoldeArray = getOperationDelegate().getSoldeRestantApresTransactionDico().allKeys();

		if (eoSoldeArray.count() > 0) {

			for (I_Solde solde : eoSoldeArray) {

				if (solde.isReliquatNatif()) {

					if (cStructureCourante == null) {
						if (debit == null) {
							debit = Integer.valueOf(0);
						}
						debit += getOperationDelegate().getDebitSurSolde(solde);

					} else {
						if (solde.cStructure() == cStructureCourante) {
							debit = getOperationDelegate().getDebitSurSolde(solde);
						}

					}

				}

			}

		}

		return debit;
	}

	private String cStructureCourante;

	public void setCStructureCourante(String cStructureCourante) {
		this.cStructureCourante = cStructureCourante;
	}

	public String getCStructureCourante() {
		return cStructureCourante;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getRestantCng
	 * ()
	 */
	public Integer getRestantCng() {
		// return getOperationDelegate().getSoldeCongeApresDebit();
		Integer restant = null;

		NSArray<I_Solde> eoSoldeArray = getOperationDelegate().getSoldeRestantApresTransactionDico().allKeys();

		if (eoSoldeArray.count() > 0) {

			for (I_Solde solde : eoSoldeArray) {

				if (solde.isCongeNatif()) {

					if (cStructureCourante == null) {
						if (restant == null) {
							restant = Integer.valueOf(0);
						}
						restant += getOperationDelegate().getSoldeApresDebit(solde);

					} else {
						if (solde.cStructure() == cStructureCourante) {
							restant = getOperationDelegate().getSoldeApresDebit(solde);
						}

					}

				}

			}

		}

		return restant;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_LigneFicheRose#getRestantRel
	 * ()
	 */
	public Integer getRestantRel() {
		// return getOperationDelegate().getSoldeReliquatApresDebit();
		Integer restant = null;

		NSArray<I_Solde> eoSoldeArray = getOperationDelegate().getSoldeRestantApresTransactionDico().allKeys();

		if (eoSoldeArray.count() > 0) {

			for (I_Solde solde : eoSoldeArray) {

				if (solde.isReliquatNatif()) {

					if (cStructureCourante == null) {
						if (restant == null) {
							restant = Integer.valueOf(0);
						}
						restant += getOperationDelegate().getSoldeApresDebit(solde);

					} else {
						if (solde.cStructure() == cStructureCourante) {
							restant = getOperationDelegate().getSoldeApresDebit(solde);
						}

					}

				}

			}

		}

		return restant;
	}

	/**
	 * Cas particulier des congés compensateurs, la structure de l'horaire débité
	 * 
	 * @return
	 */
	public EOStructure getEoStructureDebitCongeCompensateur() {
		EOStructure eoStructure = null;

		if (isAbsenceAssimileeCCompPourBalance()) {

			// cas d'un congés compensateur posé hors horaires (suite à chgt d'horaire
			// par exemple)
			if (getPlageOccupationArray().count() > 0) {
				// retrouver sur l'horaire
				String cStructure = getPlageOccupationArray().objectAtIndex(0).getPlageTravail().getCStructure();
				eoStructure = EOStructureFixed.getEoStructureInContext(editingContext(), cStructure);
			}

		}

		return eoStructure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#dateDemande()
	 */
	public NSTimestamp dateDemande() {
		return dModification();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#dateEffective()
	 */
	public NSTimestamp dateEffective() {
		return dateDebut();
	}

}
