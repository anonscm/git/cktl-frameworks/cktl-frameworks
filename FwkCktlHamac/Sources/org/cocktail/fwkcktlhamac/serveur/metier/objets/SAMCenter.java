/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlwebapp.common.util.CRIpto;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Classe de gestion des actions liées à SAM (Validations par mail)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class SAMCenter {

	private final static String OLD_SEP = "$";
	private final static String SEP = "#";

	// clés dictionnaire
	public final static String demande_KEY = "demande";
	public final static String eoTypeVisa_KEY = "eoTypeVisa";
	public final static String eoTypeStatutSuivant_KEY = "eoTypeStatutSuivant";
	public final static String decisionON_KEY = "decisionON";
	public final static String eoStructureApplication_KEY = "eoStructureApplication";

	/**
	 * TODO
	 * 
	 * Decoder une demande en chaine de validation SAM
	 * 
	 * @return
	 */
	public final static NSDictionary<String, Object> decodeDemande(
			EOEditingContext ec, String chaineEncodee) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		String chaineDecodee = chaineEncodee;

		if (HamacCktlConfig.booleanForKey(EOParametre.SAM_ENCODER_CHAINE, false)) {
			chaineDecodee = CRIpto.decrypt(chaineDecodee);
		}

		// XXX tmp changement du séparateur (pre-traitement à enlever à terme)
		String chaineATraiter = chaineEncodee;
		chaineATraiter = StringCtrl.replace(chaineATraiter, OLD_SEP, SEP);
		// fin XXX

		NSArray<String> array = NSArray.componentsSeparatedByString(chaineATraiter, SEP);

		// demande
		I_Demande demande = null;
		String entityName = array.objectAtIndex(0);
		Integer demandeId = Integer.valueOf(Integer.parseInt(array.objectAtIndex(1)));
		if (entityName.equals(EOPlanning.ENTITY_NAME)) {
			demande = EOPlanning.fetchRequiredByKeyValue(ec, EOPlanning.PLA_ID_KEY, demandeId);
		} else if (entityName.equals(EOOccupation.ENTITY_NAME)) {
			demande = EOOccupation.fetchRequiredByKeyValue(ec, EOOccupation.OCC_ID_KEY, demandeId);
		}
		dico.setObjectForKey(demande, demande_KEY);

		// type visa
		EOTypeVisa eoTypeVisa = EOTypeVisa.fetchRequiredByKeyValue(ec, EOTypeVisa.VTYP_CODE_KEY, array.objectAtIndex(2));
		dico.setObjectForKey(eoTypeVisa, eoTypeVisa_KEY);

		// type statut suivant
		String staCode = array.objectAtIndex(3);
		if (!StringCtrl.isEmpty(staCode)) {
			EOTypeStatut eoTypeStatutSuivant = EOTypeStatut.fetchRequiredByKeyValue(ec, EOTypeStatut.STA_CODE_KEY, staCode);
			dico.setObjectForKey(eoTypeStatutSuivant, eoTypeStatutSuivant_KEY);
		}

		// type visa
		String decisionON = array.objectAtIndex(4);
		dico.setObjectForKey(decisionON, decisionON_KEY);

		// structure d'application
		EOStructure cStructure = EOStructure.fetchRequiredByKeyValue(ec, EOStructure.C_STRUCTURE_KEY, array.objectAtIndex(5));
		dico.setObjectForKey(cStructure, eoStructureApplication_KEY);

		return dico.immutableClone();
	}

	/**
	 * Encoder une demande en chaine de validation SAM
	 * 
	 * @return
	 */
	public final static String encodeDemande(
			I_Demande demande, EOTypeVisa eoTypeVisa, EOTypeStatut eoTypeStatutSuivant,
			EOStructure eoStructureApplication, String decisionON) {
		String str = "";

		// demande entité
		if (demande instanceof EOPlanning) {
			str += EOPlanning.ENTITY_NAME;
		} else if (demande instanceof EOOccupation) {
			str += EOOccupation.ENTITY_NAME;
		}

		str += SEP;

		// demande ID
		if (demande instanceof EOPlanning) {
			str += ((EOPlanning) demande).plaId().intValue();
		} else if (demande instanceof EOOccupation) {
			str += ((EOOccupation) demande).occId().intValue();
		}

		str += SEP;

		// type visa
		str += eoTypeVisa.vtypCode();

		str += SEP;

		// type statut suivant
		if (eoTypeStatutSuivant != null) {
			str += eoTypeStatutSuivant.staCode();
		}

		str += SEP;

		// décision
		str += decisionON;

		str += SEP;

		// structure
		str += eoStructureApplication.cStructure();

		if (HamacCktlConfig.booleanForKey(EOParametre.SAM_ENCODER_CHAINE, false)) {
			str = CRIpto.crypt(str);
		}

		return str;
	}

	/**
	 * 
	 * @param ec
	 * @param emailDomaine
	 * @param grhumDomainePrincipal
	 * @return
	 * @throws NSValidation.ValidationException
	 */
	public final static EOIndividu findIndividuForEmail(EOEditingContext ec, String emailDomaine, String grhumDomainePrincipal)
			throws NSValidation.ValidationException {

		EOIndividu eoIndividu = null;
		try {
			eoIndividu = EOIndividu.fetchFirstRequiredByQualifier(ec,
					SAMCenter.qualForEmailEquals(emailDomaine, grhumDomainePrincipal));
		} catch (Exception e) {
			e.printStackTrace();
			String errMsg = "Impossible de déterminer la personne associée à l'adresse email \"" + emailDomaine + "\"";
			throw new NSValidation.ValidationException(errMsg);
		}

		return eoIndividu;
	}

	/**
	 * Recopié de {@link EOIndividu} et adapté pour avoir aussi les mails
	 * login+domaine principal
	 * 
	 * @param emailDomaine
	 * @return
	 */
	public final static EOQualifier qualForEmailEquals(String emailDomaine, String grhumDomainePrincipal) {

		ERXKeyValueQualifier qualRPA = ERXQ.is(
				ERXQ.keyPath(EOIndividu.TO_REPART_PERSONNE_ADRESSES_KEY, EORepartPersonneAdresse.E_MAIL_KEY),
				emailDomaine);
		EOQualifier qual = qualRPA;

		String email = null;
		String domaine = null;

		System.out.println("qualForEmailEquals() emailDomaine=[" + emailDomaine + "] grhumDomainePrincipal=[" + grhumDomainePrincipal + "]");

		if (emailDomaine.contains("@")) {

			// compte email
			email = emailDomaine.substring(0, emailDomaine.indexOf("@")).toLowerCase();
			domaine = emailDomaine.substring(emailDomaine.indexOf("@") + 1).toLowerCase();
			ERXAndQualifier qualCEM = ERXQ.is(
					ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.TO_COMPTE_EMAILS_KEY, EOCompteEmail.CEM_EMAIL_KEY),
					email).and(
					ERXQ.is(
							ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.TO_COMPTE_EMAILS_KEY, EOCompteEmail.CEM_DOMAINE_KEY),
							domaine));
			qual = qualRPA.or(qualCEM);

			System.out.println("qualForEmailEquals() email=[" + email + "] domaine=[" + domaine + "]");

			// login + domaine principal
			if (!StringCtrl.isEmpty(grhumDomainePrincipal) &&
					!StringCtrl.isEmpty(domaine) &&
					grhumDomainePrincipal.equalsIgnoreCase(domaine)) {

				EOQualifier qualLoginDomainePrincipal =
						ERXQ.is(
								ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.CPT_LOGIN_KEY),
								email).and(
										ERXQ.is(
												ERXQ.keyPath(EOIndividu.TO_COMPTES_KEY, EOCompte.TO_VLANS_KEY, EOVlans.DOMAINE_KEY),
												grhumDomainePrincipal));
				// qual = qualRPA.or(qualLoginDomainePrincipal);
				qual = ERXQ.or(qual, qualLoginDomainePrincipal);

				System.out.println("qualForEmailEquals() qual=" + qual);
			}

		}

		return qual;
	}
}
