/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Plage horaire associé à une journée (travail ou pause)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_HorairePlageJournalier
		extends ERXCustomObject {

	// NSKeyValueCoding
	public final static String DEBUT_MINUTES_DANS_SEMAINE_KEY = "debutMinutesDansSemaine";
	public final static String FIN_MINUTES_DANS_SEMAINE_KEY = "finMinutesDansSemaine";
	public final static String DUREE_MINUTES_KEY = "dureeMinutes";
	public final static String C_STRUCTURE_KEY = "cStructure";

	private Horaire horaire;
	private HoraireJournalier horaireJournalier;
	private int debutMinutesDansSemaine;
	private int finMinutesDansSemaine;
	private String strChaineBrute;

	/**
	 * 
	 */
	public A_HorairePlageJournalier(
			Horaire horaire, String strChaineBrute) {
		super();
		this.horaire = horaire;
		this.strChaineBrute = strChaineBrute;
	}

	/**
	 * Trouver et affecter l'objet {@link HoraireJournalier}
	 */
	protected void affecteHoraireJournalier() {
		int indexJour = getIndexJour(getDebutMinutesDansSemaine());
		EOQualifier qual = ERXQ.equals(HoraireJournalier.INDEX_JOUR_KEY, indexJour);
		HoraireJournalier hj = (HoraireJournalier) EOQualifier.filteredArrayWithQualifier(
					getHoraire().getHoraireJournalierArray(), qual).objectAtIndex(0);
		setHoraireJournalier(hj);
	}

	/**
	 * Méthode interne utilitaire permettant de savoir sur quel jour de la semaine
	 * tombe les minutes minutesDansSemaine
	 * 
	 * TODO ne gère pour l'instant qu'un seul jour (d'apres la date de début). A
	 * voir si cela sert d'en gerer plus ?
	 * 
	 * @return
	 */
	private static int getIndexJour(
			int minutesDansSemaine) {

		int indexJour = 0;

		indexJour = (minutesDansSemaine / Jour.DUREE_JOURNEE_EN_MINUTES) + 1;

		return indexJour;
	}

	/**
	 * Duree totale en minutes
	 * 
	 * @return
	 */
	public int getDureeMinutes() {
		int dureeMinutes = 0;

		dureeMinutes = getFinMinutesDansSemaine() - getDebutMinutesDansSemaine();

		return dureeMinutes;
	}

	/**
	 * @return
	 */
	public final Horaire getHoraire() {
		return horaire;
	}

	/**
	 * @return
	 */
	public final HoraireJournalier getHoraireJournalier() {
		return horaireJournalier;
	}

	/**
	 * @param horaire
	 */
	public final void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}

	/**
	 * @param horaireJournalier
	 */
	public final void setHoraireJournalier(HoraireJournalier horaireJournalier) {
		this.horaireJournalier = horaireJournalier;
		// relation inverse
		if (this instanceof HorairePlageTravail) {
			horaireJournalier.getHorairePlageTravailArray().add((HorairePlageTravail) this);
		} else if (this instanceof HorairePlagePause) {
			horaireJournalier.setHorairePlagePause((HorairePlagePause) this);
		}
	}

	/**
	 * @return
	 */
	public final int getDebutMinutesDansSemaine() {
		return debutMinutesDansSemaine;
	}

	/**
	 * @param debutMinutesDansSemaine
	 */
	protected final void setDebutMinutesDansSemaine(int debutMinutesDansSemaine) {
		this.debutMinutesDansSemaine = debutMinutesDansSemaine;
	}

	/**
	 * @return
	 */
	public final int getFinMinutesDansSemaine() {
		return finMinutesDansSemaine;
	}

	/**
	 * @param finMinutesDansSemaine
	 */
	protected final void setFinMinutesDansSemaine(int finMinutesDansSemaine) {
		this.finMinutesDansSemaine = finMinutesDansSemaine;
	}

	/**
	 * @return
	 */
	public final int getDebutMinutesDansJour() {
		return DateCtrlHamac.getMinutesDansJour(getDebutMinutesDansSemaine());
	}

	/**
	 * @return
	 */
	public final int getFinMinutesDansJour() {
		return DateCtrlHamac.getMinutesDansJour(getFinMinutesDansSemaine());
	}

	/**
	 * @return
	 */
	public final String getStrChaineBrute() {
		return strChaineBrute;
	}

}
