/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetSaisieManuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.A_OccupationTypePresenceDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.A_CetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.Recuperation;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;

/**
 * Classe interne de gestion des débits sur solde
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Transaction
		extends ERXCustomObject {

	public final static String SOLDE_KEY = "solde";
	public final static String OPERATION_KEY = "operation";
	public final static String ABSENCE_KEY = "absence";
	public final static String JOUR_KEY = "jour";
	public final static String DATE_KEY = "date";
	public final static String MINUTES_DEBUT_DANS_JOUR_KEY = "minutesDebutDansJour";
	public final static String MINUTES_FIN_DANS_JOUR_KEY = "minutesFinDansJour";
	public final static String MINUTES_TOTAL_KEY = "minutesTotal";
	public final static String IS_CONSOMMEE_KEY = "isConsommee";

	private I_Solde solde;
	private I_Operation operation;

	// soit ces 2
	private int minutesDebutDansJour = -1;
	private int minutesFinDansJour = -1;

	// ou soit celui la
	private int aDebiter = A_DEBITER_PAR_DEFAUT;
	private final static int A_DEBITER_PAR_DEFAUT = (int) Math.pow(2, 31) - 1;

	// indique si consommée ou non
	private boolean isConsommee;

	/**
	 * 
	 */
	private Transaction(I_Solde solde,
			I_Operation operation) {
		super();
		this.solde = solde;
		this.operation = operation;
		this.isConsommee = false;
	}

	/**
	 * Transaction pour laquelle il faudra contrôler le non chevauchement
	 * 
	 * @deprecated
	 * @see Transaction#Transaction(I_Solde, I_Operation, A_Segment, boolean)
	 * 
	 * @param solde
	 * @param operation
	 * @param minutesDebutDansJour
	 * @param minutesFinDansJour
	 * @param isReelle
	 */
	public Transaction(
			I_Solde solde,
			I_Operation operation,
			int minutesDebutDansJour,
			int minutesFinDansJour) {
		this(solde, operation);
		this.minutesDebutDansJour = minutesDebutDansJour;
		this.minutesFinDansJour = minutesFinDansJour;
		traitementPostConstructeur();
	}

	/**
	 * Transaction pour laquelle il faudra contrôler le non chevauchement
	 * 
	 * @param solde
	 * @param operation
	 * @param minutesDebutDansJour
	 * @param minutesFinDansJour
	 * @param isReelle
	 */
	public Transaction(
			I_Solde solde,
			I_Operation operation,
			A_Segment segment) {
		this(solde, operation);
		this.minutesDebutDansJour = segment.getDebut();
		this.minutesFinDansJour = segment.getFin();
		traitementPostConstructeur();
	}

	/**
	 * Transaction "anonyme", i.e. pas de référence à un jour
	 * 
	 * @param solde
	 * @param operation
	 * @param aDebiter
	 * @param isReelle
	 *          TODO
	 */
	public Transaction(
			I_Solde solde,
			I_Operation operation,
			int aDebiter) {
		this(solde, operation);
		this.aDebiter = aDebiter;
		traitementPostConstructeur();
	}

	/**
	 * Traitement commun à l'appel de tous les constructeurs
	 */
	private void traitementPostConstructeur() {
		notifierNouvelleTransaction();
		notifierConsommationTransaction();
	}

	/**
	 * 
	 */
	private void notifierNouvelleTransaction() {
		getOperation().getOperationDelegate().notifierNouvelleTransaction(this);
	}

	public final I_Solde getSolde() {
		return solde;
	}

	public final int getMinutesDebutDansJour() {
		return minutesDebutDansJour;
	}

	public final int getMinutesFinDansJour() {
		return minutesFinDansJour;
	}

	public final int getADebiter() {
		return aDebiter;
	}

	/**
	 * Indique si le jour est précisé ou si c'est juste un débit
	 * 
	 * @return
	 */
	private boolean isDebitAnonyme() {
		boolean isDebitAnonyme = false;

		if (getADebiter() != A_DEBITER_PAR_DEFAUT) {
			isDebitAnonyme = true;
		}

		return isDebitAnonyme;
	}

	/**
	 * 
	 * @return
	 */
	public final int getMinutesTotal() {
		int minutesTotal = 0;

		if (isDebitAnonyme()) {
			minutesTotal = getADebiter();
		} else {
			// cas particulier pour les heures supp. : on prend la durée bonifiée
			if (getAbsence().isPresence()) {
				// TODO a adapter à la période réelle
				minutesTotal = -((A_OccupationTypePresenceDelegate) getAbsence().getOccupationTypeDelegate()).getDureeBonifiee(
						getMinutesDebutDansJour(), getMinutesFinDansJour());
			} else {
				minutesTotal = getMinutesFinDansJour() - getMinutesDebutDansJour();
			}
		}

		return minutesTotal;
	}

	public final I_Operation getOperation() {
		return operation;
	}

	/**
	 * 
	 * @return
	 */
	public final boolean isDebitOccupation() {
		boolean isDebitOccupation = false;

		if (getAbsence() != null) {
			isDebitOccupation = true;
		}

		return isDebitOccupation;
	}

	/**
	 * La plage occupation si l'objet débiteur est de type {@link PlageOccupation}
	 * 
	 * @return
	 */
	private final PlageOccupation getPlageOccupation() {
		PlageOccupation plageOccupation = null;

		if (getOperation() instanceof PlageOccupation) {
			plageOccupation = (PlageOccupation) getOperation();
		}

		return plageOccupation;
	}

	/**
	 * La recuperation si l'objet débiteur est de type {@link Recuperation}
	 * 
	 * @return
	 */
	private final Recuperation getRecuperation() {
		Recuperation recuperation = null;

		if (getOperation() instanceof Recuperation) {
			recuperation = (Recuperation) getOperation();
		}

		return recuperation;
	}

	/**
	 * L'absence si l'objet débiteur est de type {@link PlageOccupation}
	 * 
	 * @return
	 */
	public final A_Absence getAbsence() {
		A_Absence absence = null;

		if (getPlageOccupation() != null) {
			absence = getPlageOccupation().getAbsence();
		} else if (getOperation() instanceof A_Absence) {
			// heure supp
			absence = (A_Absence) getOperation();
		}

		return absence;
	}

	/**
	 * Le solde consommateur si l'objet débiteur est de type {@link I_Solde}
	 * 
	 * @return
	 */
	public final I_Solde getSoldeConsommateur() {
		I_Solde soldeConsommateur = null;

		if (getOperation() instanceof I_Solde) {
			soldeConsommateur = (I_Solde) getOperation();
		}

		return soldeConsommateur;
	}

	/**
	 * Le débiteur CET si l'objet débiteur est de type
	 * {@link A_CetTransactionAnnuelle}
	 * 
	 * @return
	 */
	public final A_CetTransactionAnnuelle getDebiteurCetTransactionAnnuelle() {
		A_CetTransactionAnnuelle debiteurCet = null;

		if (getOperation() instanceof A_CetTransactionAnnuelle) {
			debiteurCet = (A_CetTransactionAnnuelle) getOperation();
		}

		return debiteurCet;
	}

	/**
	 * La saisie manuelle CET si l'objet débiteur est de type
	 * {@link getDebiteurCetSaisieManuelle}
	 * 
	 * @return
	 */
	public final EOCetSaisieManuelle getDebiteurCetSaisieManuelle() {
		EOCetSaisieManuelle eoSaisie = null;

		if (getOperation() instanceof EOCetSaisieManuelle) {
			eoSaisie = (EOCetSaisieManuelle) getOperation();
		}

		return eoSaisie;
	}

	/**
	 * Le {@link Jour} associé s'il s'agit d'une occupation ou d'un récupération
	 * 
	 * @return
	 */
	public final Jour getJour() {
		Jour jour = null;

		if (getPlageOccupation() != null) {
			jour = getPlageOccupation().getJour();
		}

		return jour;
	}

	/**
	 * La date de la transaction, utilisée pour le classement d'une liste de
	 * {@link Transaction}, pour appliquer les débits de manière chronologique (ne
	 * concerne que les absences et les récupérations négatives pour l'instant)
	 * 
	 * @return
	 */
	public final NSTimestamp getDate() {
		NSTimestamp date = null;

		if (getJour() != null) {
			date = getJour().getDate();
		} else if (getRecuperation() != null) {
			date = getRecuperation().dDeb();
		}

		return date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXCustomObject#toString()
	 */
	@Override
	public String toString() {
		String toString = "";

		if (isDebitAnonyme()) {

			if (getPlageOccupation() != null) {

				Jour jour = getPlageOccupation().getJour();
				toString += DateCtrlHamac.dateToString(jour.getDate(), I_ConstsJour.DEBIT_DATE_JOUR_FORMAT) + " ";
				toString += TimeCtrl.stringForMinutes(jour.minutesDebutPourJour(getAbsence()) % Jour.DUREE_JOURNEE_EN_MINUTES) + " - ";
				toString += TimeCtrl.stringForMinutes(jour.minutesFinPourJour(getAbsence()) % Jour.DUREE_JOURNEE_EN_MINUTES) + " ";
				toString += getPlageOccupation().getAbsence().libelleType() + " ";

			} else if (getRecuperation() != null) {

				toString += getRecuperation().toString() + " ";

			} else {
				toString += getOperation().toString() + " ";
			}

			toString += "(" + TimeCtrl.stringForMinutes(getMinutesTotal()) + ")";

		} else {
			if (getPlageOccupation() != null) {
				toString += DateCtrlHamac.dateToString(getPlageOccupation().getJour().getDate(), I_ConstsJour.DEBIT_DATE_JOUR_FORMAT) + " ";
				toString += getPlageOccupation().getAbsence().libelleType() + " ";
			}
			toString += TimeCtrl.stringForMinutes(getMinutesDebutDansJour()) + " - " +
					TimeCtrl.stringForMinutes(getMinutesFinDansJour());
		}

		// toString += " " + Integer.toHexString(hashCode());

		return toString;
	}

	/**
	 * @return
	 */
	public boolean isDebit() {
		boolean isDebit = false;

		if (getMinutesTotal() > 0) {
			isDebit = true;
		}

		return isDebit;
	}

	/**
	 * 
	 */
	public void notifierConsommationTransaction() {
		if (isConsommee() == false) {

			// CktlLog.log("notifierConsommationTransaction() : " + toString());

			getSolde().getSoldeDelegate().notifierConsommationTransaction(this);

			setIsConsommee(true);
			getOperation().getOperationDelegate().notifierConsommationTransaction(this);

		} else {
			// CktlLog.log("!!!!!!!!!! notifierConsommationTransaction() transaction déjà consommée !!!!!!! "
			// + getOperation().toString());
		}
	}

	public final boolean isConsommee() {
		return isConsommee;
	}

	private final void setIsConsommee(boolean isConsommee) {
		this.isConsommee = isConsommee;
	}
}
