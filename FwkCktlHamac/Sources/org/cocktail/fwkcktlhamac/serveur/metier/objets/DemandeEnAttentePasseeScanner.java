/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXDefaultEditingContextDelegate;

/**
 * Thread de gestion des demandes passées non validée pour effectuer des
 * relantes
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class DemandeEnAttentePasseeScanner
		extends Thread {

	private int delay = 1000;
	private String appSamMail = HamacCktlConfig.stringForKey(EOParametre.SAM_EMAIL_ADRESSE_PLUGIN);
	private boolean isAppUseSam = (HamacCktlConfig.intForKey(EOParametre.SAM_FREQUENCE_RECHERCHE) > 0);
	private CktlMailBus mailBus = ((CktlWebApplication) CktlWebApplication.application()).mailBus();
	private String appURLConnexionDirecte = "http://trucmuch.com";

	@Override
	public void run() {
		Timer timer = new Timer(true);
		System.out.println(
				getClass().getName() + ".run() envoi des mails de relance toutes les " + delay + " secondes.");
		timer.schedule(new CheckDemandeTask(), 0, 1000 * delay);
	}

	private class CheckDemandeTask
			extends TimerTask {

		@Override
		public void run() {

			EOEditingContext ec = new EOEditingContext();
			ec.setDelegate(new ERXDefaultEditingContextDelegate());

			EOPeriode currentPeriode = EOPeriode.getCurrentPeriode(ec);

			NSArray<I_Demande> demandeArray = (NSArray<I_Demande>) DemandeDelegate.getDemandeEtStructureArray(
					ec, currentPeriode, null).objectAtIndex(0);

			// responsable -> demande -> structure / type visa
			NSMutableDictionary<IPersonne, NSMutableDictionary<I_Demande, NSMutableDictionary<EOStructure, EOTypeVisa>>> dico = DemandeDelegate.getDico(ec, demandeArray);

			for (IPersonne personne : dico.allKeys()) {

				String motifPourPersonne = "DEBUG: destinataire original : " + personne.getNomCompletAffichage() + "\n================================\n\n";
				motifPourPersonne += MailCenter.PREFIX_CONTENT_MAIL_GLOBAL + "\n\n";

				int index = 0;

				for (I_Demande demande : dico.objectForKey(personne).allKeys()) {

					index++;

					DemandeDelegate dlg = demande.getDemandeDelegate();
					IPersonne personneConcerne = dlg.getDemandeur();

					// construire le dico de validation
					Hashtable<String, String> dicoVal = MailCenter.buildDico(
							ec, isAppUseSam, appSamMail, appURLConnexionDirecte, dlg, personneConcerne);

					// construire le dico specifiques a la nature de l'alerte
					Hashtable<String, String> dicoSpec = MailCenter.buildDicoSpec();

					// construire le dico specifique a la la delegation
					ArrayList<Hashtable<String, String>> dicoArray = MailCenter.updateDicoAndbuildDicoDelegation(demande.getDemandeDelegate(), dicoVal);
					dicoVal = dicoArray.get(0);
					Hashtable<String, String> dicoDelegation = dicoArray.get(1);

					motifPourPersonne += "--- [demande n°" + index + "] ---\n";

					for (EOStructure eoStructure : dico.objectForKey(personne).get(demande).allKeys()) {

						EOTypeVisa eoTypeVisa = dico.objectForKey(personne).get(demande).get(eoStructure);

						if (eoTypeVisa.isNiveauVisa()) {
							motifPourPersonne += MailCenter.buildContentMailVisa(
									ec, isAppUseSam, appSamMail, dlg, null, false, null, dicoVal, dicoSpec, dicoDelegation, false);
						} else if (eoTypeVisa.isNiveauValidation()) {
							motifPourPersonne += MailCenter.buildContentMailValidation(
									isAppUseSam, false, null, null, dicoVal, dicoSpec, dicoDelegation, false);
						}

					}

				}

				motifPourPersonne += "\nVous pouvez consulter et/ou valider à cette adresse:\n\t" + appURLConnexionDirecte;

				mailBus.sendMail("ctarade@univ-lr.fr", "ctarade@univ-lr.fr", null, "[Hamac] relance", motifPourPersonne);

			}

		}
	}

}
