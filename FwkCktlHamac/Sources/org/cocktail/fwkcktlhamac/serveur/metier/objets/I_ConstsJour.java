/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public interface I_ConstsJour {

	/** les etats possible d'un jour */
	public final static int STATUS_SANS_STATUS = 0;
	public final static int STATUS_TRAVAILLE = (int) Math.pow(2, 0);
	public final static int STATUS_TRAVAILLE_AM = (int) Math.pow(2, 1);
	public final static int STATUS_TRAVAILLE_PM = (int) Math.pow(2, 2);
	public final static int STATUS_CONGE = (int) Math.pow(2, 3);
	public final static int STATUS_CONGE_AM = (int) Math.pow(2, 4);
	public final static int STATUS_CONGE_PM = (int) Math.pow(2, 5);
	public final static int STATUS_CONGE_LEGAL = (int) Math.pow(2, 6);
	public final static int STATUS_CONGE_LEGAL_AM = (int) Math.pow(2, 7);
	public final static int STATUS_CONGE_LEGAL_PM = (int) Math.pow(2, 8);
	public final static int STATUS_FERMETURE = (int) Math.pow(2, 9);
	public final static int STATUS_FERIE = (int) Math.pow(2, 10);
	public final static int STATUS_CHOME = (int) Math.pow(2, 11);
	public final static int STATUS_HORS_AFFECTATION = (int) Math.pow(2, 12);
	public final static int STATUS_HORAIRE_FORCE = (int) Math.pow(2, 13);
	public final static int STATUS_CONGES_COMP_AM = (int) Math.pow(2, 14);
	public final static int STATUS_CONGES_COMP_PM = (int) Math.pow(2, 28);

	/**
	 * etats pour la mise en valeur des occupations sur le planning pour
	 * validation
	 */
	public final static int HIGHLIGHT_OCC_FIRST_DAY = (int) Math.pow(2, 15);
	public final static int HIGHLIGHT_OCC_MIDDLE_DAY = (int) Math.pow(2, 16);
	public final static int HIGHLIGHT_OCC_LAST_DAY = (int) Math.pow(2, 17);
	public final static int HIGHLIGHT_OCC_ONLY_DAY = (int) Math.pow(2, 18);
	public final static int HIGHLIGHT_TODAY = (int) Math.pow(2, 19);

	/** etats de validation de l'occupation */
	public final static int STATUS_EN_COURS_DE_VALIDATION = (int) Math.pow(2, 20);
	public final static int STATUS_EN_COURS_DE_SUPPRESSION = (int) Math.pow(2, 21);

	/** interface */
	public final static int STATUT_PORTE_NUMERO_SEMAINE = (int) Math.pow(2, 22);
	public final static int STATUT_PORTE_TOOLTIP_HORAIRE = (int) Math.pow(2, 23);

	/** */
	public final static int STATUT_CONGE_MALADIE = (int) Math.pow(2, 24);
	public final static int STATUT_CONGE_MALADIE_AM = (int) Math.pow(2, 25);
	public final static int STATUT_CONGE_MALADIE_PM = (int) Math.pow(2, 26);

	/** vacance scolaires */
	public final static int STATUT_VACANCE_SCOLAIRE = (int) Math.pow(2, 27);

	public final static String CODE_JOUR_DATE_FORMAT = "%Y_%m_%d";

	public final static String ABS_DATE_FORMAT_ABS_MINUTE_DEBUT = "%a %d/%m/%y %H:%M";
	public final static String ABS_DATE_FORMAT_ABS_DEMI_JOURNEE_DEBUT = "%a %d/%m/%y %p";
	public final static String ABS_DATE_FORMAT_ABS_MINUTE_FIN = "%H:%M";
	public final static String ABS_DATE_FORMAT_ABS_DEMI_JOURNEE_FIN = "%a %d/%m/%y %p";

	public final static String DEBIT_DATE_JOUR_FORMAT = "%a %d/%m";
}
