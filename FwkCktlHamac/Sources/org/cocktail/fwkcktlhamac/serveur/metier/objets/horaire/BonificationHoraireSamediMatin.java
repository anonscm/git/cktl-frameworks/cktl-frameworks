/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

/**
 * Opération permettant de créditer le droit à congé d'un service du fait du
 * travail le samedi matin
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class BonificationHoraireSamediMatin
		extends A_BonificationHoraire {

	private int numeroSemaine;

	public BonificationHoraireSamediMatin(int numeroSemaine, int minutes) {
		super(minutes);
		this.numeroSemaine = numeroSemaine;
	}

	@Override
	public String libelleBonification() {
		return "travail du samedi matin S" + numeroSemaine;
	}

}
