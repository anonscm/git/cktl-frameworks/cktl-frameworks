/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.A_FwkCktlHamacRecord;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlagePause;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlageTravail;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePreRequis;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePreRequisPourService;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FixedGregorianCalendar;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class Semaine
		extends A_ObjetPlanning {

	public final static String JOURS_KEY = "jours";
	public final static String CODE_KEY = "code";
	public final static String IS_ACTIVE_KEY = "isActive";
	public final static String IS_ASSOCIEE_KEY = "isAssociee";
	public final static String IS_SEMAINE_HAUTE_ASSOCIEE_KEY = "isSemaineHauteAssociee";
	public final static String IS_SEMAINE_VACANCE_SCOLAIRE = "isSemaineVacanceScolaire";

	private NSMutableArray<Jour> jours;
	private Mois mois;
	private int numeroSemaine;
	private NSMutableArray<Horaire> horaires;
	private String code;
	private Boolean _isActive;

	//
	private Semaine semainePrecedente;
	private Semaine semaineSuivante;

	// cache
	private Boolean isAForcer;
	private NSArray<Jour> jourAForcerArray;

	// jours à forcer
	// try catch pour ne pas planter en test junit
	private static NSArray<NSTimestamp> DATE_A_FORCER_ARRAY;
	{

		DATE_A_FORCER_ARRAY = new NSArray<NSTimestamp>();

		try {

			String strHolydayLocal = HamacCktlConfig.stringForKey(EOParametre.JOURS_A_FORCER);
			if (!StringCtrl.isEmpty(strHolydayLocal)) {
				NSArray<String> strHolydayLocalArray = NSArray.componentsSeparatedByString(strHolydayLocal, ",");
				for (String strHolyday : strHolydayLocalArray) {
					DATE_A_FORCER_ARRAY = DATE_A_FORCER_ARRAY.arrayByAddingObject(
							DateCtrlHamac.stringToDate(strHolyday));
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 
	 * @param unPremierJour
	 * @param unMois
	 */
	public Semaine(NSTimestamp unPremierJour, Mois unMois) {
		super(unPremierJour);
		jours = new NSMutableArray<Jour>();
		mois = unMois;
		setLibelle(Integer.toString(getNumeroSemaine()));
	}

	/**
	 * 
	 * @param element
	 */
	public void addJour(Jour element) {
		getJours().add(element);
	}

	public NSMutableArray<Horaire> getHoraires() {
		if (horaires == null) {
			horaires = new NSMutableArray<Horaire>();
		}
		return horaires;
	}

	/**
	 * 
	 * @param element
	 */
	private void addHoraire(Horaire element) {
		getHoraires().add(element);
	}

	/**
	 * 
	 * @return
	 */
	public final Mois getMois() {
		return mois;
	}

	/**
	 * 
	 * @return
	 */
	public final NSArray<Jour> getJours() {
		return jours;
	}

	/**
	 * 
	 * @return
	 */
	public final NSArray<Jour> getJours(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray<Jour> jours = getJours();

		EOQualifier qual = null;

		if (dateDebut != null && dateFin != null) {
			qual = ERXQ.and(
					ERXQ.greaterThanOrEqualTo(Jour.DATE_KEY, dateDebut),
					ERXQ.lessThanOrEqualTo(Jour.DATE_KEY, dateFin));
		} else if (dateDebut != null) {
			qual = ERXQ.greaterThanOrEqualTo(Jour.DATE_KEY, dateDebut);
		} else if (dateFin != null) {
			qual = ERXQ.lessThanOrEqualTo(Jour.DATE_KEY, dateFin);
		}

		jours = EOQualifier.filteredArrayWithQualifier(jours, qual);

		return jours;
	}

	/**
	 * 
	 * @return
	 */
	public final int getNumeroSemaine() {
		return numeroSemaine;
	}

	/**
	 * 
	 * @return
	 */
	public final String getNumeroSemaineSur2Chiffres() {
		String str = "";

		if (getNumeroSemaine() < 10) {
			str += "0";
		}

		str += Integer.toString(getNumeroSemaine());

		return str;
	}

	/**
	 * 
	 * @param numeroSemaine
	 */
	public final void setNumeroSemaine(int numeroSemaine) {
		this.numeroSemaine = numeroSemaine;
	}

	@Override
	public int dureeAssocieeMinutes(String cStructure) {
		int dureeAssocieeMinutes = 0;

		for (Jour jourItem : getJours()) {
			dureeAssocieeMinutes += jourItem.dureeAssocieeMinutes(cStructure);
		}

		return dureeAssocieeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure) {
		int dureeComptabiliseeMinutes = 0;

		for (Jour jourItem : getJours()) {
			dureeComptabiliseeMinutes += jourItem.dureeComptabiliseeMinutes(cStructure);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public int dureeComptabiliseeMinutes(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int dureeComptabiliseeMinutes = 0;

		for (Jour jourItem : getJours()) {
			dureeComptabiliseeMinutes += jourItem.dureeComptabiliseeMinutes(cStructure, dateDebut, dateFin);
		}

		return dureeComptabiliseeMinutes;
	}

	@Override
	public float quotite(String cStructure) {
		float quotite = 0;

		for (Jour jourItem : getJours()) {
			quotite += jourItem.quotite(cStructure);
		}

		quotite = quotite / (float) getJours().count();

		return quotite;
	}

	@Override
	public int dureeRealiseeMinutes(String cStructure) {
		int dureeRealiseeMinutes = 0;

		for (Jour jourItem : getJours()) {
			dureeRealiseeMinutes += jourItem.dureeRealiseeMinutes(cStructure);
		}

		return dureeRealiseeMinutes;
	}

	@Override
	public Planning getPlanning() {
		return getMois().getPlanning();
	}

	/**
	 * Le nombre de jours candidat pour être forcé (i.e. lun, mar, mer, jeu ou
	 * ven)
	 * 
	 * @return
	 */
	public int nbJourCandidatAForcer() {
		int nb = 0;

		NSArray<Jour> jourArray = getJours();

		for (int i = 0; i < jourArray.count(); i++) {
			Jour jour = jourArray.objectAtIndex(i);
			if (isJourCandidatAForcer(jour)) {
				nb++;
			}
		}

		return nb;
	}

	/**
	 * 
	 * @param jour
	 * @return
	 */
	private boolean isJourCandidatAForcer(Jour jour) {
		boolean isCandidatAForcer = false;

		if (jour.isLundi() ||
				jour.isMardi() ||
				jour.isMercredi() ||
				jour.isJeudi() ||
				jour.isVendredi()) {
			isCandidatAForcer = true;
		}

		return isCandidatAForcer;
	}

	/**
	 * Le nombre total des jours ouverts à forcer.
	 * 
	 * @return
	 */
	public int nbJourAForcer() {
		int nb = 0;

		boolean shouldContinuer = true;

		NSArray<Jour> jourArray = getJours();

		for (int i = 0; i < jourArray.count() && shouldContinuer; i++) {

			Jour jour = jourArray.objectAtIndex(i);

			if (isJourCandidatAForcer(jour)) {

				NSArray<A_Absence> absenceArray = jour.getAbsenceArray();

				boolean isAuMoinsUneAbsenceHoraireForceRecouvrementSemaine = false;

				for (int j = 0; j < absenceArray.count() && shouldContinuer; j++) {

					A_Absence absence = absenceArray.objectAtIndex(j);

					isAuMoinsUneAbsenceHoraireForceRecouvrementSemaine =
							isAuMoinsUneAbsenceHoraireForceRecouvrementSemaine || absence.isHoraireForceRecouvrementSemaine(getPlanning().getEoPlanning().toPeriode());

				}

				if (isAuMoinsUneAbsenceHoraireForceRecouvrementSemaine) {
					nb++;
					// 1 seule absence suffit, on passe au jour suivant
					continue;
				} else if (jour.isMardi() ||
						jour.isMercredi() ||
						jour.isJeudi()) {
					nb = 0;
					shouldContinuer = false;
				}

			}

		}

		return nb;
	}

	private final int NB_JOUR_OUVERT_SEMAINE_COMPLETE_FORCE = 5;

	/**
	 * FIXME controle pour les semaines en début ou fin de planning !!!!!!!!
	 * Indique si la semaine doit être positionnée en horaire forcé : une
	 * occupation ou plusieurs occupations typées forcé qui recouvrent les 5 jours
	 * de la semaine (lun au ven). TODO controler aussi pour les semaines en
	 * limite de mois et d'annee
	 * 
	 * @return
	 */
	public boolean isAForcer() {

		if (isAForcer == null) {

			isAForcer = Boolean.FALSE;

			// nombre de jours ouverts
			int nbJourCandidatAForcer = nbJourCandidatAForcer();
			int nbJourAForcer = nbJourAForcer();
			if (nbJourCandidatAForcer == NB_JOUR_OUVERT_SEMAINE_COMPLETE_FORCE) {
				// semaine complete
				if (nbJourCandidatAForcer == nbJourAForcer) {
					isAForcer = Boolean.TRUE;
				}
			} else {
				// semaine incomplete
				if (DateCtrlHamac.isLundi(getPremierJour()) == false &&
						getSemainePrecedente() != null) {
					// controle semaine precedente
					if (nbJourCandidatAForcer == nbJourAForcer &&
							getSemainePrecedente().nbJourCandidatAForcer() == getSemainePrecedente().nbJourAForcer()) {
						isAForcer = Boolean.TRUE;
					}
				} else if (getSemaineSuivante() != null) {
					// controle semaine suivante
					if (nbJourCandidatAForcer == nbJourAForcer &&
							getSemaineSuivante().nbJourCandidatAForcer() == getSemaineSuivante().nbJourAForcer()) {
						isAForcer = Boolean.TRUE;
					}
				}

			}

		}

		return isAForcer.booleanValue();
	}

	/**
	 * Indique si au moins 1 jour est à forcer. N'est pas liée à la méthode
	 * {@link #isAForcer()}
	 * 
	 * @return
	 */
	public boolean isAuMoinsUnJourAForcer() {
		boolean isAuMoinsUnJourAForcer = false;

		if (getJourArrayAForcer().count() > 0) {
			isAuMoinsUnJourAForcer = true;
		}

		return isAuMoinsUnJourAForcer;
	}

	/**
	 * La liste des jours à forcer au sein de la semaine
	 * 
	 * @return
	 */
	public NSArray<Jour> getJourArrayAForcer() {
		if (jourAForcerArray == null) {

			NSArray<Jour> jourArray = getJours();

			jourAForcerArray = new NSArray<Jour>();

			for (int i = 0; i < DATE_A_FORCER_ARRAY.count(); i++) {
				NSTimestamp date = DATE_A_FORCER_ARRAY.objectAtIndex(i);

				EOQualifier qual = ERXQ.equals(Jour.DATE_KEY, date);
				NSArray<Jour> array = EOQualifier.filteredArrayWithQualifier(jourArray, qual);

				jourAForcerArray = jourAForcerArray.arrayByAddingObjectsFromArray(array);
			}

		}
		return jourAForcerArray;
	}

	@Override
	public void clearCache() {
		super.clearCache();
		isAForcer = null;
		jourAForcerArray = null;
	}

	public final Semaine getSemainePrecedente() {
		return semainePrecedente;
	}

	public final void setSemainePrecedente(Semaine semainePrecedente) {
		this.semainePrecedente = semainePrecedente;
	}

	public final Semaine getSemaineSuivante() {
		return semaineSuivante;
	}

	public final void setSemaineSuivante(Semaine semaineSuivante) {
		this.semaineSuivante = semaineSuivante;
	}

	@Override
	public String toString() {
		String toString = "";

		toString = "Semaine code=" + getCode() + " libelle=" + getLibelle() + " numero=" + getNumeroSemaine();

		return toString;
	}

	public final String getCode() {
		if (code == null) {
			code = getCodeForDate(getPremierJour());
		}
		return code;
	}

	/**
	 * Préfixe la valeur "O" ou "N" à la fin de {@link #getCode()} selon si la
	 * semaine possède le statut {@link #isSemaineVacanceScolaire()} ou non
	 * 
	 * @return
	 */
	public final String getCodeAvecVacanceScolaire() {
		String code = null;

		if (isSemaineVacanceScolaire()) {
			code = A_FwkCktlHamacRecord.OUI;
		} else {
			code = A_FwkCktlHamacRecord.NON;
		}

		code += "_" + getCode();

		return code;
	}

	/**
	 * Obtenir le code d'un objet {@link Semaine} d'après une date
	 * 
	 * @param date
	 * @return
	 */
	public final static String getCodeForDate(NSTimestamp date) {
		String code = null;

		NSTimestamp lundi = DateCtrlHamac.reculerToLundi(date);
		FixedGregorianCalendar gc = new FixedGregorianCalendar();
		gc.setTime(lundi);

		// constition du code : annee_numero. Le numéro doit être sur 2 chiffres
		int annee = gc.get(FixedGregorianCalendar.YEAR);
		int numeroSemaine = gc.get(FixedGregorianCalendar.WEEK_OF_YEAR);

		// cas particulier pour la semaine 01, si commence en décembre, alors
		// l'année est N+1
		if (numeroSemaine == 1 &&
				gc.get(FixedGregorianCalendar.MONTH) == FixedGregorianCalendar.DECEMBER) {
			annee++;
		}

		code = annee + "_";
		if (numeroSemaine < 10) {
			code += "0";
		}
		code += numeroSemaine;

		return code;
	}

	/**
	 * Obtenir le date d'une semaine d'après son code
	 * 
	 * @param code
	 * @return
	 */
	public final static NSTimestamp getDateForCode(String code) {
		NSTimestamp date = null;

		// extraction de l'année
		String strAnnee = code.substring(0, 4);

		// extraction du numéro
		String strNumero = code.substring(5, 7);
		int numero = 0;
		if (strNumero.startsWith("0")) {
			numero = Integer.parseInt(strNumero.substring(1));
		} else {
			numero = Integer.parseInt(strNumero);
		}

		// obtenir le numéro de la semaine du premier jour
		NSTimestamp date1erJanvier = DateCtrlHamac.stringToDate("01/01/" + strAnnee);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date1erJanvier);

		NSTimestamp dateLundiSemaine1 = null;

		if (gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY ||
				gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.TUESDAY ||
				gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.WEDNESDAY ||
				gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.THURSDAY) {
			// semaine 1 année n
			dateLundiSemaine1 = DateCtrlHamac.avancerToDimanche(date1erJanvier).timestampByAddingGregorianUnits(0, 0, -6, 0, 0, 0);

		} else {
			// derniere semaine année n-1
			dateLundiSemaine1 = DateCtrlHamac.avancerToDimanche(date1erJanvier).timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		}

		date = dateLundiSemaine1.timestampByAddingGregorianUnits(0, 0, 7 * (numero - 1), 0, 0, 0);

		return date;
	}

	/**
	 * Indique si une semaine est complète ou non (semaine complète = semaine de 7
	 * jours du lundi au dimanche)
	 * 
	 * @return
	 */
	public boolean isComplete() {
		boolean isComplete = true;

		if (getJours().count() < 7) {
			isComplete = false;
		}

		return isComplete;
	}

	/**
	 * Effectuer les associations horaires semaine pour l'objet {@link Semaine}
	 * courant.
	 * 
	 * @param eoPlanning
	 */
	public void associerHoraire(EOPlanning eoPlanning) {

		NSArray<Horaire> horaireAssociationArray = null;

		if (isAForcer()) {

			// horaire forcé construire un objet Horaire fictif
			Horaire horaire = new Horaire(getJours(), eoPlanning.toPeriode());
			horaireAssociationArray = new NSArray<Horaire>(horaire);

		} else {

			// horaire réellement associé au planning
			NSArray<EOAssociationHoraire> associationSemaineArray = EOAssociationHoraire.filterAssociationForSemaineInArray(
					this, eoPlanning.tosAssociationHoraire());

			horaireAssociationArray = getEoHoraireArrayForEoAssociationArray(associationSemaineArray);

			// un ou plusieurs jours à modifier dans l'horaire
			if (isAuMoinsUnJourAForcer()) {

				NSArray<Horaire> horaireCustomArray = new NSArray<Horaire>();

				for (int i = 0; i < horaireAssociationArray.count(); i++) {
					Horaire horaire = horaireAssociationArray.objectAtIndex(i);
					Horaire horaireModifie = new Horaire(horaire, getJourArrayAForcer(), getPlanning().getEoPlanning().toPeriode());
					horaireCustomArray = horaireCustomArray.arrayByAddingObject(horaireModifie);
				}

				horaireAssociationArray = horaireCustomArray.immutableClone();

			}

		}

		for (Horaire horaire : horaireAssociationArray) {
			addHoraire(horaire);
		}

		for (Jour jourItem : getJours()) {

			// ne rien associer hors affectation, sur les dimanches et jours
			// fériés
			if (jourItem.isHorsAffectation() == false &&
					jourItem.isFerie() == false &&
					jourItem.isChome() == false) {

				// les horaires réellement associés
				for (Horaire horaire : horaireAssociationArray) {

					NSArray<HorairePlageTravail> plageTravailArray = horaire.getPlageTravailArrayFor(jourItem);

					for (HorairePlageTravail plageTravail : plageTravailArray) {
						jourItem.addPlageTravail(plageTravail);
					}

					NSArray<HorairePlagePause> plagePauseArray = horaire.getPlagePauseArrayFor(jourItem);

					for (HorairePlagePause plagePause : plagePauseArray) {
						jourItem.addPlagePause(plagePause);
					}

				}

			}

		}

	}

	/**
	 * Trouve les objets {@link Horaire} pour une liste d'association
	 * 
	 * @param horaireArray
	 * @param associationArray
	 * 
	 * @return
	 */
	private NSArray<Horaire> getEoHoraireArrayForEoAssociationArray(
			NSArray<EOAssociationHoraire> associationArray) {
		NSMutableArray<Horaire> result = new NSMutableArray<Horaire>();

		NSArray<EOHoraire> eoHoraireArray = (NSArray<EOHoraire>) associationArray.valueForKey(EOAssociationHoraire.TO_HORAIRE_KEY);

		for (EOHoraire eoHoraire : eoHoraireArray) {
			EOQualifier qual = ERXQ.equals(Horaire.EO_HORAIRE_KEY, eoHoraire);
			NSArray<Horaire> horaireArrayForHoraire = EOQualifier.filteredArrayWithQualifier(
					getPlanning().getHoraireArray(), qual);
			result.add(horaireArrayForHoraire.objectAtIndex(0));
		}

		return result;
	}

	/**
	 * La liste des codes de semaines qui doivent exister pour un {@link Planning}
	 * selon une liste des {@link TravailAttendu}
	 * 
	 * @param attenduArray
	 * @return
	 */
	public static NSArray<String> getSemaineCodeForTravailAttenduArray(
			Planning planning, NSArray<TravailAttendu> attenduArray) {
		NSMutableArray<String> array = new NSMutableArray<String>();

		//
		for (TravailAttendu attendu : attenduArray) {

			// retailler à la période du planning
			NSTimestamp debut = attendu.getDateDebut();
			if (debut == null || DateCtrl.isBefore(debut, planning.getEoPlanning().toPeriode().perDDebut())) {
				debut = planning.getEoPlanning().toPeriode().perDDebut();
			}

			NSTimestamp fin = attendu.getDateFin();
			if (fin == null || DateCtrl.isAfter(fin, planning.getEoPlanning().toPeriode().perDFin())) {
				fin = planning.getEoPlanning().toPeriode().perDFin();
			}

			NSTimestamp date = debut;

			while (!DateCtrl.isAfter(date, fin)) {

				String semaineCode = getCodeForDate(date);

				if (!array.containsObject(semaineCode)) {
					array.addObject(semaineCode);
				}

				date = date.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
			}

		}

		return array.immutableClone();
	}

	@Override
	public int nbJourOuvre(String cStructure) {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourOuvre(cStructure);

		}

		return n;
	}

	@Override
	public int nbJourAttendu(String cStructure) {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourAttendu(cStructure);

		}

		return n;
	}

	@Override
	public int nbJourAttendu(TravailAttendu travailAttendu) {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourAttendu(travailAttendu);

		}

		return n;
	}

	@Override
	public int nbJourOuvre() {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourOuvre();

		}

		return n;
	}

	@Override
	public int nbJourOuvre(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourOuvre(dateDebut, dateFin);

		}

		return n;
	}

	@Override
	public int nbJourOuvrable() {
		int n = 0;

		for (Jour jour : getJours()) {

			n += jour.nbJourOuvrable();

		}

		return n;
	}

	/**
	 * Donne le nombre de demi-journées de la semaine qui contiennent un horaire
	 * 
	 * @return
	 */
	public int nbDemiJourneeAssociee(String cStructure, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int n = 0;

		for (Jour jour : getJours()) {

			if (jour.dureeComptabiliseeMinutesAm(cStructure) > 0) {
				n++;
			}

			if (jour.dureeComptabiliseeMinutesPm(cStructure) > 0) {
				n++;
			}

		}

		return n;
	}

	/**
	 * Donne le nombre de journées de la semaine qui contiennent un horaire
	 * 
	 * @return
	 */
	public int nbJourneeAssociee(String cStructure) {
		int n = 0;

		for (Jour jour : getJours()) {

			if (jour.dureeComptabiliseeMinutesAm(cStructure) > 0 ||
					jour.dureeComptabiliseeMinutesPm(cStructure) > 0) {
				n++;
			}

		}

		return n;
	}

	/**
	 * La couleur de l'horaire
	 * 
	 * TODO traiter le multi horaire ??
	 * 
	 * @return
	 */
	public String getStyleHoraire() {
		String couleur = "#FFFFFF";

		// getEoHoraire() peut être à null en cas d'horaire forcé
		if (getHoraires().count() > 0 &&
				getHoraires().objectAtIndex(0).getEoHoraire() != null &&
				!StringCtrl.isEmpty(getHoraires().objectAtIndex(0).getEoHoraire().horCouleurHtml())) {
			couleur = getHoraires().objectAtIndex(0).getEoHoraire().horCouleurHtml();
		}

		couleur = "background-color: " + couleur;

		return couleur;
	}

	/**
	 * 
	 * @return
	 */
	public String getTooltipHtmlString() {
		String htmlString = "";

		if (getHoraires().count() > 0) {
			htmlString = getHoraires().objectAtIndex(0).toHtmlString("S" + getNumeroSemaineSur2Chiffres());
		}

		return htmlString;
	}

	/**
	 * Indique si la semaine est active, i.e. on peut y associer un horaire
	 * 
	 * @return
	 */
	public boolean isActive() {
		if (_isActive == null) {
			NSArray<Jour> jourActifArray = EOQualifier.filteredArrayWithQualifier(
					getJours(),
					ERXQ.isTrue(Jour.IS_ACTIF_KEY));

			if (jourActifArray.count() > 0) {
				_isActive = Boolean.TRUE;
			} else {
				_isActive = Boolean.FALSE;
			}

		}
		return _isActive.booleanValue();
	}

	/**
	 * Indique si la semaine a au moins un horaire associé
	 * 
	 * @return
	 */
	public boolean isAssociee() {
		boolean isAssociee = false;

		if (getHoraires().count() > 0) {
			isAssociee = true;
		}

		return isAssociee;
	}

	/**
	 * Indique si la semaine a un horaire associé "haut", i.e. que le plus haut
	 * des plafonds de tous les prerequis de la semaine est atteint
	 * 
	 * @return
	 */
	public boolean isSemaineHauteAssociee() {
		boolean isSemaineHauteAssociee = false;

		if (isAssociee()) {

			// déterminer la borne max
			int dureeMax = 0;

			NSMutableDictionary<String, HorairePreRequis> dico = getPlanning().getDicoHorairePreRequis();

			for (HorairePreRequis hpr : dico.allValues()) {

				if (hpr.getSemaineCodeArray().containsObject(getCode())) {

					int cumul = 0;

					for (HorairePreRequisPourService hprps : hpr.getParServiceArray()) {

						cumul += hprps.getDureeHebdoMaxi();

					}

					if (cumul > dureeMax) {
						dureeMax = cumul;
					}

				}

			}

			int dureeSemaine = dureeAssocieeMinutes();

			// bien prendre les semaines completes
			if (getSemainePrecedente() != null &&
					getSemainePrecedente().getCode().equals(getCode())) {
				dureeSemaine += getSemainePrecedente().dureeAssocieeMinutes();
			} else if (getSemaineSuivante() != null &&
					getSemaineSuivante().getCode().equals(getCode())) {
				dureeSemaine += getSemaineSuivante().dureeAssocieeMinutes();
			}

			//
			if (dureeSemaine >= dureeMax) {
				isSemaineHauteAssociee = true;
			}

		}

		return isSemaineHauteAssociee;
	}

	/**
	 * @param eoPlanning
	 * @return
	 */
	private boolean isEoHoraireAssocieRepondAuPreRequis(EOPlanning eoPlanning) {
		boolean isOk = false;

		NSArray<EOAssociationHoraire> associationSemaineArray = EOAssociationHoraire.filterAssociationForSemaineInArray(
				this, eoPlanning.tosAssociationHoraire());

		for (EOAssociationHoraire asso : associationSemaineArray) {
			isOk = isOk || EOAssociationHoraire.isHoraireRepondAuPrerequis(asso.toHoraire(), eoPlanning, getCode());
		}

		return isOk;
	}

	/**
	 * Indique si l'horaire associé à une semaine peut être modifié ou non.
	 * accessibilite lien numero de semaine sur le planning.
	 * 
	 * @return
	 */
	public boolean isHoraireAssociable(HamacApplicationUser user) {
		boolean isHoraireAssociable = true;

		// seulement les semaines "actives"
		if (!isActive()) {
			isHoraireAssociable = false;
		}

		if (isHoraireAssociable) {

			isHoraireAssociable = false;
			
			EOPlanning eoPlanning = getPlanning().getEoPlanning();

			// condition pour le planning réel
			if (eoPlanning.isReel()) {

				// seul un super admin peut modifications des semaines précédentes
				// même si ces dernières ont déjà un horaire associé
				if (user.isPeriodeModifiableParAdmin(eoPlanning)) {
					isHoraireAssociable = true;
				} else {				
					// autorisé ?
					if (user.isAutoriseAModifierAssociationHoraireSemaine(eoPlanning)) {
	
						// le planning doit être en cours de modification
						if (eoPlanning.toTypeStatut().isEtatPlanningAutorisantModification()) {
							
							// pas de modification des semaines précédentes, sauf si aucun
							// horaire n'a été associé ou que l'horaire associé ne réponds pas
							// au prérequis attendu pour la semaine
							if ((getHoraires().count() == 0) ||
									(DateCtrlHamac.isAfterEq(getPremierJour(), DateCtrlHamac.reculerToLundi(DateCtrlHamac.now()))) ||
									(!isEoHoraireAssocieRepondAuPreRequis(eoPlanning))) {
								isHoraireAssociable = true;
							}
	
							// autre cas possible, la semaine n'a pas d'enregistrement
							// historique (i.e. il n'y a jamais eu de réel précédent)
							if (isHoraireAssociable == false) {
	
								EOQualifier qualSemaine = ERXQ.equals(
										EOAssociationHoraire.SEMAINE_CODE_KEY, getCode());
	
								NSArray<EOAssociationHoraire> histArray = eoPlanning.getEoPlanningHist().tosAssociationHoraire(qualSemaine);
	
								if (histArray.count() == 0) {
									isHoraireAssociable = true;
								}
	
							}
	
						}
					}
				}

			}

			// condition pour le planning de test
			if (eoPlanning.isTest()) {

				// autorisé ?
				if (user.isAutoriseAModifierAssociationHoraireSemaine(eoPlanning)) {
					isHoraireAssociable = true;
				}

			}

		}

		return isHoraireAssociable;
	}

	// bonifications

	/**
	 * Le nombre total de demi journee travaillees dans la semaine, du lundi au
	 * vendredi. Si la semaine n'est pas complete, alors on regarde les jours
	 * manquants sur la semaine precedente.
	 * 
	 * EDIT : le samedi matin doit lui aussi être travaillé
	 * 
	 * Cette methode est toujours appelee un samedi.
	 */
	private int nbDemiJourTravaillee() {
		int total = 0;
		int endIndex = 6;

		Jour jourLun = (Jour) getJours().objectAtIndex(0);
		if (!jourLun.isLundi()) {
			// pas de lundi dans la semaine, on va chercher le chercher dans la
			// semaine precedente
			if (semainePrecedente != null && semainePrecedente.getJours().count() > 0) {
				jourLun = (Jour) semainePrecedente.getJours().objectAtIndex(0);
				total += jourLun.nbDemiJourneesTravaillees();
			} else {
				// pas de semaine precedente, arret
				return 0;
			}

			Jour jourMar = (Jour) getJours().objectAtIndex(0);
			if (!jourMar.isMardi()) {
				if (semainePrecedente.getJours().count() > 1) {
					jourMar = (Jour) semainePrecedente.getJours().objectAtIndex(1);
					total += jourMar.nbDemiJourneesTravaillees();
				} else {
					// pas trouvable dans la semaine precedente, arrete
					return 0;
				}

				Jour jourMer = (Jour) getJours().objectAtIndex(0);
				if (!jourMer.isMercredi()) {
					if (semainePrecedente.getJours().count() > 2) {
						jourMer = (Jour) semainePrecedente.getJours().objectAtIndex(2);
						total += jourMer.nbDemiJourneesTravaillees();
					} else {
						// pas trouvable dans la semaine precedente, arrete
						return 0;
					}

					Jour jourJeu = (Jour) getJours().objectAtIndex(0);
					if (!jourJeu.isJeudi()) {
						if (semainePrecedente.getJours().count() > 3) {
							jourJeu = (Jour) semainePrecedente.getJours().objectAtIndex(3);
							total += jourJeu.nbDemiJourneesTravaillees();
						} else {
							// pas trouvable dans la semaine precedente, arrete
							return 0;
						}

						Jour jourVen = (Jour) getJours().objectAtIndex(0);
						if (!jourVen.isVendredi()) {
							if (semainePrecedente.getJours().count() > 4) {
								jourVen = (Jour) semainePrecedente.getJours().objectAtIndex(4);
								total += jourVen.nbDemiJourneesTravaillees();
							} else {
								// pas trouvable dans la semaine precedente, arrete
								return 0;
							}

							// cas particulier si c'est un samedi le premier
							// jour de la semaine, on force endIndex a 0
							// (pas de traitement de la semaine en cours ..)
							Jour jourSam = (Jour) getJours().objectAtIndex(0);
							if (jourSam.isSamedi()) {
								endIndex = 1;
							}

						} else {
							// c'est bien le vendredi, on passe au traitement de la semaine
							// actuelle
							endIndex = 2;
						}
					} else {
						// c'est bien le jeudi, on passe au traitement de la semaine
						// actuelle
						endIndex = 3;
					}
				} else {
					// c'est bien le mercredi, on passe au traitement de la semaine
					// actuelle
					endIndex = 4;
				}
			} else {
				// c'est bien le mardi, on passe au traitement de la semaine actuelle
				endIndex = 5;
			}
		}

		// traitement pour la semaine en cours
		for (int i = 0; i < endIndex; i++) {
			Jour jourItem = (Jour) getJours().objectAtIndex(i);
			if (jourItem.isSamedi() == false) {
				total += jourItem.nbDemiJourneesTravaillees();
			} else {
				// le samedi on ne regarde que le matin
				if (jourItem.isTravailleSamediMatin()) {
					total += 1;
				}
			}
		}

		return total;
	}

	/**
	 * Retourne le samedi s'il existe dans la semaine
	 * 
	 * @return
	 */
	private Jour getSamedi() {
		Jour samedi = null;

		NSArray<Jour> array = EOQualifier.filteredArrayWithQualifier(
				getJours(), ERXQ.isTrue(Jour.IS_SAMEDI_KEY));

		if (array.count() > 0) {
			samedi = array.objectAtIndex(0);
		}

		return samedi;
	}

	/**
	 * Le samedi matin, la bonification ne s'applique que si les 10 demi journees
	 * sont travaillees.
	 * 
	 * Cette methode effectue l'imputation ou pas du bonus
	 */
	public boolean isSamediMatinABonifier() {

		boolean isSamediMatinABonifier = false;

		Jour samedi = getSamedi();
		if (samedi != null) {
			// faire le traitement que si le samedi matin est bien travaillé
			if (samedi.isTravailleAm()) {

				if (nbDemiJourTravaillee() >= 11) {
					isSamediMatinABonifier = true;
				}

			}
		}

		return isSamediMatinABonifier;
	}

	/**
	 * Le samedi aprés midi, la bonification s'applique s'il est travaillé
	 * 
	 * Cette methode effectue l'imputation ou pas du bonus
	 */
	public boolean isSamediApresMidiABonifier() {

		boolean isSamediApresMidiABonifier = false;

		Jour samedi = getSamedi();
		if (samedi != null) {
			// faire le traitement que si le samedi matin est bien travaillé
			if (samedi.isTravaillePm()) {
				isSamediApresMidiABonifier = true;
			}
		}

		return isSamediApresMidiABonifier;
	}

	/**
	 * Calcul des droits à congés (récupérations).
	 * 
	 * @return
	 */
	public boolean isSemaineEnBoutDeFile() {
		boolean isSemaineEnBoutDeFile = false;

		if (getSemainePrecedente() != null &&
				getSemainePrecedente().getCode().equals(getCode())) {
			isSemaineEnBoutDeFile = true;
		}

		if (!isSemaineEnBoutDeFile) {
			if (getSemaineSuivante() == null ||
					!getSemaineSuivante().getCode().equals(getCode())) {
				isSemaineEnBoutDeFile = true;
			}
		}

		return isSemaineEnBoutDeFile;
	}

	/**
	 * Calcul des droits à congés (récupérations).
	 * 
	 * @return
	 */
	public boolean isSemaineEnDebutDeFile() {
		boolean isSemaineEnDebutDeFile = false;

		if (getSemainePrecedente() == null) {
			isSemaineEnDebutDeFile = true;
		}

		if (!isSemaineEnDebutDeFile) {
			if (getSemainePrecedente() != null &&
					!getSemainePrecedente().getCode().equals(getCode())) {
				isSemaineEnDebutDeFile = true;
			}
		}

		return isSemaineEnDebutDeFile;
	}

	// gestion du clic ou non

	/**
	 * sur le planning on affiche le lien associe horaire par defaut sur le jeudi
	 * si pas de jeudi, alors on le met sur le premier jour de la semaine.
	 */
	public void determinerStatutsJourPourInterface() {

		Jour jourPortantNumeroSemaine = null;
		Jour jourPortantTooltipHoraire = null;

		// numéro de la semaine
		EOQualifier qual = ERXQ.isTrue(Jour.IS_JEUDI_KEY);
		NSArray<Jour> records = EOQualifier.filteredArrayWithQualifier(getJours(), qual);
		if (records.count() > 0) {
			jourPortantNumeroSemaine = records.lastObject();
		} else if (getJours().count() > 0) {
			jourPortantNumeroSemaine = getJours().objectAtIndex(0);
		}
		if (jourPortantNumeroSemaine != null) {
			jourPortantNumeroSemaine.addStatut(I_ConstsJour.STATUT_PORTE_NUMERO_SEMAINE);
		}

		// tooltip horaire
		if (isActive()) {
			// jour précédent s'il porte le numéro de semaine et que ce dernier n'est
			// pas un dimanche
			if (jourPortantNumeroSemaine != null) {
				int indexJour = getJours().indexOfIdenticalObject(jourPortantNumeroSemaine);
				if (!jourPortantNumeroSemaine.isDimanche()) {
					if (indexJour < getJours().count() - 1) {
						jourPortantTooltipHoraire = getJours().objectAtIndex(indexJour + 1);
					}
				} else {
					// sinon le jour suivant s'il porte le numéro de semaine
					// TODO ?
				}
			}
		}
		if (jourPortantTooltipHoraire != null) {
			jourPortantTooltipHoraire.addStatut(I_ConstsJour.STATUT_PORTE_TOOLTIP_HORAIRE);
		}

	}

	/**
	 * Indique si au moins un jour de la semaine sont inclus dans une période de
	 * vacance scolaire (on ignore le samedi et le dimanche)
	 * 
	 * @return
	 */
	public final boolean isSemaineVacanceScolaire() {
		boolean isSemaineVacanceScolaire = true;

		boolean isAutreJourSamedi = false;

		int i = 0;
		while (isSemaineVacanceScolaire &&
				i < getJours().count() - 1) {
			Jour jour = getJours().objectAtIndex(i);
			if (!jour.isSamedi() &&
					!jour.isDimanche()) {
				isAutreJourSamedi = true;
				if (!jour.isVacanceScolaire()) {
					isSemaineVacanceScolaire = false;
				}
			}
			i++;
		}

		if (!isAutreJourSamedi &&
				isSemaineVacanceScolaire) {
			isSemaineVacanceScolaire = false;
		}

		return isSemaineVacanceScolaire;
	}
}
