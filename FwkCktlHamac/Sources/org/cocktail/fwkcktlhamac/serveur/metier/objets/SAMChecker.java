/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.fwkcktlhamac.serveur.I_ConstantesApplication;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTacheSam;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXDefaultEditingContextDelegate;

/**
 * Thread de gestion des décisions par mail via SAM
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class SAMChecker
		extends Thread {

	private CktlWebApplication application;
	private int delay;
	private static String grhumDomainePrincipal;

	public SAMChecker(CktlWebApplication application, int delay) {
		super();
		this.application = application;
		this.delay = delay;
		grhumDomainePrincipal = application.config().stringForKey(I_ConstantesApplication.GRHUM_DOMAINE_PRINCIPAL_KEY);
		System.out.println("new SAMChecker() grhumDomainePrincipal=" + getGrhumDomainePrincipal());
	}

	@Override
	public void run() {
		Timer timer = new Timer(true);
		System.out.println(
				getClass().getName() + ".run() vérification des décisions par mails toutes les " + delay + " secondes.");
		timer.schedule(new TableTacheSAMTask(), 0, 1000 * delay);
	}

	/**
	 * Traitement des entrées de la table de {@link EOTacheSam}
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class TableTacheSAMTask
			extends TimerTask {

		@Override
		public void run() {
			try {
				effectuerTraitement();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private synchronized void effectuerTraitement() {

			// "Occupation$9759$VIS$VISE$O$25$93616"
			// "Occupation$9759$VIS$NON_VALIDE$O$25$93616"

			// System.out.println(getClass().getName() + ".run()");

			EOEditingContext ec = new EOEditingContext();
			ec.setDelegate(new ERXDefaultEditingContextDelegate());

			// lister les taches en attente
			NSArray<EOTacheSam> eoTacheSamArray = EOTacheSam.findTacheSamNonTraiteeArray(ec);

			for (EOTacheSam eoTacheSam : eoTacheSamArray) {

				String emailFrom = eoTacheSam.tsaEmailFrom();
				Integer persIdViseur = null;

				try {
					EOIndividu eoIndividuViseur = SAMCenter.findIndividuForEmail(
							ec, emailFrom, getGrhumDomainePrincipal());
					persIdViseur = eoIndividuViseur.persId();
				} catch (Exception e) {
					// expéditeur inconnu
					e.printStackTrace();

					MailCenter mc = new MailCenter(application, null, application.dataBus(), application.mailBus(), ec, null, null);
					mc.sendMailError(emailFrom, e.getMessage());

					// mettre à l'état en erreur
					eoTacheSam.setTsaEtat(EOTacheSam.ETAT_EN_ERREUR);
					eoTacheSam.setTsaResultat(e.getMessage());

					continue;
				}

				try {

					String chaine = eoTacheSam.tsaChaine();

					NSDictionary<String, Object> dico = SAMCenter.decodeDemande(ec, chaine);

					I_Demande demande = (I_Demande) dico.objectForKey(SAMCenter.demande_KEY);
					EOTypeVisa eoTypeVisa = (EOTypeVisa) dico.objectForKey(SAMCenter.eoTypeVisa_KEY);
					EOTypeStatut eoTypeStatutSuivant = (EOTypeStatut) dico.objectForKey(SAMCenter.eoTypeStatutSuivant_KEY);
					String decisionON = (String) dico.objectForKey(SAMCenter.decisionON_KEY);
					EOStructure eoStructureApplication = (EOStructure) dico.objectForKey(SAMCenter.eoStructureApplication_KEY);

					DemandeDelegate delegate = demande.getDemandeDelegate();

					// si le statut suivant n'est pas définit, c'est qu'il n'y a pas le
					// choix et donc on prend le suivant par défaut
					if (eoTypeStatutSuivant == null) {
						eoTypeStatutSuivant = delegate.getEoTypeStatutPossibleArray(eoTypeVisa, eoStructureApplication, decisionON).objectAtIndex(0);
					}

					try {

						if (delegate.isVisaAutorise(eoStructureApplication, eoTypeVisa, persIdViseur, decisionON, eoTypeStatutSuivant)) {
							delegate.doViser(eoStructureApplication, eoTypeVisa, persIdViseur, decisionON);
							EOVisa eoVisa = delegate.appliquerVisa(eoStructureApplication, eoTypeVisa, eoTypeStatutSuivant);
							delegate.envoyerMail(application, null, application.mailBus(), application.dataBus(), ec, eoVisa);
							MailCenter mc = new MailCenter(application, null, application.dataBus(), application.mailBus(), ec, delegate, eoVisa);
							mc.sendMailOperationSuccessfull(demande, persIdViseur, decisionON.equals("O"), eoTypeVisa.isNiveauVisa(), eoStructureApplication);

							// mettre à l'état traitée avec succès
							eoTacheSam.setTsaEtat(EOTacheSam.ETAT_TRAITEE);

						}

					} catch (Exception exceptionValidation) {
						// erreur de validation

						exceptionValidation.printStackTrace();

						MailCenter mc = new MailCenter(application, null, application.dataBus(), application.mailBus(), ec, null, null);
						mc.sendMailError(emailFrom, exceptionValidation.getMessage());

						// mettre à l'état en erreur
						eoTacheSam.setTsaEtat(EOTacheSam.ETAT_TRAITEE);
						eoTacheSam.setTsaResultat(exceptionValidation.getMessage());

					}

				} catch (Exception exceptionFatale) {
					// erreur fatale

					exceptionFatale.printStackTrace();

					MailCenter mc = new MailCenter(application, null, application.dataBus(), application.mailBus(), ec, null, null);
					mc.sendMailError(emailFrom, exceptionFatale.getMessage());

					// mettre à l'état en erreur
					eoTacheSam.setTsaEtat(EOTacheSam.ETAT_EN_ERREUR);
					eoTacheSam.setTsaResultat(exceptionFatale.getMessage());

				}

			}

			// sauvegarder
			try {
				ec.lock();
				ec.saveChanges();
			} catch (Exception e) {
				ec.revert();
			} finally {
				ec.unlock();
				// on nettoye
				ec.dispose();
			}

		}
	}

	private static final String getGrhumDomainePrincipal() {
		return grhumDomainePrincipal;
	}

}
