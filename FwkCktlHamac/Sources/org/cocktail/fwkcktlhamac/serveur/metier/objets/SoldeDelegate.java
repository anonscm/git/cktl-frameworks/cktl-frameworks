/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetSaisieManuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.A_CetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.Recuperation;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Rassemble les traitements communs à toutes les classes implémentant
 * l'interface {@link I_Solde}. Classe à part afin de pouvoir faire de
 * l'héritage et une interface ...
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class SoldeDelegate {

	public final static String CLEAR_CACHE_KEY = "clearCache";

	private I_Solde solde;

	// cache
	private Integer _restant;
	private NSMutableArray<Transaction> _transactionArray;
	private NSMutableDictionary<Transaction, Integer> _dicoRestantTransaction;
	private NSMutableDictionary<I_Operation, Integer> _dicoRestantOperation;

	// valeur initial du solde avec quelconque débit ou crédit
	private Integer init;

	/**
	 * 
	 */
	public SoldeDelegate(I_Solde solde) {
		super();
		this.solde = solde;
		init = restant();
	}

	/**
	 * Nettoyage de toutes les valeurs du cache
	 */
	public void clearCache() {
		_restant = null;
		_transactionArray = null;
		_dicoRestantTransaction = null;
		_dicoRestantOperation = null;

		// System.out.println("SoldeDelegate.clearCache() " + solde.toString());
	}

	private I_Solde getSolde() {
		return solde;
	}

	/**
	 * Gestion d'un solde fictif pour les besoin de gestion de débit roulant a
	 * posteriori (pauses multiservices ...)
	 * 
	 * @return
	 */
	public Integer restant() {

		Integer value = _restant;

		if (value == null) {
			// TODO ajouter une méthode dans l'interface I_Solde
			if (getSolde() instanceof EOSolde) {
				value = ((EOSolde) getSolde()).restant();
			} else if (getSolde() instanceof EOCetSaisieManuelle) {
				value = ((EOCetSaisieManuelle) getSolde()).csmValeur();
			} else if (getSolde() instanceof A_CetTransactionAnnuelle) {
				value = ((A_CetTransactionAnnuelle) getSolde()).getValeur();
			} else if (getSolde() instanceof Recuperation) {
				value = ((Recuperation) getSolde()).getValeur();
			}

			_restant = value;

		}

		return value;
	}

	/**
	 * Liste des {@link Transaction} utilisant ce solde
	 */
	public NSMutableArray<Transaction> getTransactionArray() {
		if (_transactionArray == null) {
			_transactionArray = new NSMutableArray<Transaction>();
			// System.out.println("getOperationArray() new _transactionArray");
		}
		return _transactionArray;
	}

	/**
	 * Utilisé par l'écran d'affichage des soldes
	 * 
	 * @return
	 */
	public NSMutableArray<I_Operation> getOperationArray() {
		NSMutableArray<I_Operation> array = new NSMutableArray<I_Operation>();

		for (int i = 0; i < getTransactionArray().count(); i++) {
			Transaction transaction = getTransactionArray().objectAtIndex(i);

			I_Operation operation = transaction.getOperation().getOperationDelegate().getOperationParente();

			if (!array.containsObject(operation)) {
				array.addObject(operation);
			}

		}

		return array;
	}

	/**
	 * 
	 * @param operation
	 * @return
	 */
	public int valeur(I_Operation operation) {
		int valeur = 0;

		/*
		 * if (operation instanceof A_Absence) {
		 * 
		 * A_Absence absence = (A_Absence) operation;
		 * 
		 * // if (absence.isHeureSupplementaire()) { // valeur =
		 * -((HeureSupplementaireDelegate) //
		 * absence.getOccupationTypeDelegate()).getDureeBonifiee(); // } else {
		 * valeur = absence.getDebitSurSolde(getSolde()); // }
		 * 
		 * } else if (operation instanceof EOSolde) {
		 * 
		 * EOSolde eoSolde = (EOSolde) operation; valeur =
		 * eoSolde.restant().intValue();
		 * 
		 * // soldes "créditants" if (eoSolde.isBalance()) { valeur = -valeur; }
		 * else if (eoSolde.isCongeManuel() || eoSolde.isReliquatNatif()) { valeur =
		 * -valeur; } else if (eoSolde.isCongeLegal() || eoSolde.isJrti() ||
		 * eoSolde.isIndemCet20081136()) { // cas particulier pour le "malus" de
		 * congé légaux ou les JRTI, on // prend la valeur // TODO on doit pouvoir
		 * faire plus propre valeur = 0; for (int i = 0; i <
		 * getTransactionArray().count(); i++) { Transaction transaction =
		 * getTransactionArray().objectAtIndex(i); if (transaction.getOperation() ==
		 * eoSolde) { valeur += transaction.getMinutesTotal(); } } } } else
		 */{
			valeur = operation.getOperationDelegate().getDebitSurSolde(getSolde());
		}

		return valeur;
	}

	/**
	 * 
	 * @param operation
	 * @return
	 */
	public int restantApresOperation(I_Operation operation) {
		Integer valeur = null;

		if (operation != null) {
			valeur = getDicoRestantOperation().objectForKey(operation);
		} else {
			valeur = init;
		}

		try {
			return valeur.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 
	 * @param operation
	 * @return
	 */
	public int restantApresTransaction(Transaction transaction) {
		Integer valeur = null;

		if (transaction != null) {
			Integer valDico = getDicoRestantTransaction().objectForKey(transaction);
			valeur = valDico;
		} else {
			valeur = init;
		}

		return valeur.intValue();
	}

	/**
	 * "Prévenir" le solde qu'une transction a été appliquée
	 * 
	 * @param transaction
	 */
	public final void notifierNouvelleTransaction(Transaction transaction) {
		getTransactionArray().addObject(transaction);
	}

	/**
	 * Effectue le débit ou le crédit lié à la transaction
	 * 
	 * @param transaction
	 */
	public final void notifierConsommationTransaction(
			Transaction transaction) {

		// CktlLog.log("--------------- notifierConsommationTransaction " +
		// transaction);

		if (transaction.isDebit()) {
			debiter(transaction);
		} else {
			crediter(transaction);
		}

		memoriserRestantApresTransaction(transaction);

		// on la ré-ordonne la liste des transactions selon l'ordre de leur
		// consommation
		NSArray<Transaction> transactionConsommeeArray = EOQualifier.filteredArrayWithQualifier(
					getTransactionArray(), ERXQ.isTrue(Transaction.IS_CONSOMMEE_KEY));

		NSArray<Transaction> transactionNonConsommeeArray = EOQualifier.filteredArrayWithQualifier(
					getTransactionArray(), ERXQ.isFalse(Transaction.IS_CONSOMMEE_KEY));

		_transactionArray = null;
		getTransactionArray().addObjectsFromArray(transactionConsommeeArray);
		getTransactionArray().addObjectsFromArray(transactionNonConsommeeArray);

	}

	/**
	 * Stocker la valeur intermédiaire du solde restant juste après le débit de la
	 * transaction ainsi que l'opération associée
	 * 
	 * @param transaction
	 */
	private void memoriserRestantApresTransaction(Transaction transaction) {
		getDicoRestantTransaction().setObjectForKey(restant(), transaction);
		getDicoRestantOperation().setObjectForKey(restant(), transaction.getOperation().getOperationDelegate().getOperationParente());

		// CktlLog.log("°°°°°°°°°°°°°°°°°° memoriserRestantApresTransaction() restant()="
		// +
		// TimeCtrl.stringForMinutes(restant().intValue()) + " transaction=" +
		// transaction);
		// CktlLog.log("°°°°°°°°°°°°°°°°°° memoriserRestantApresTransaction() restant()="
		// +
		// TimeCtrl.stringForMinutes(restant().intValue()) + " operation=" +
		// transaction.getOperation().getOperationDelegate().getOperationParente());

	}

	/**
	 * Enlever des minutes
	 */
	private void debiter(Transaction transaction) {
		int minutes = transaction.getMinutesTotal();

		_restant = Integer.valueOf(restant().intValue() - minutes);
		getSolde().setRestant(restant());
		// CktlLog.log("*************** debiter() restant()=" +
		// TimeCtrl.stringForMinutes(restant().intValue()));

	}

	/**
	 * Ajouter des minutes
	 */
	private void crediter(Transaction transaction) {
		int minutes = -transaction.getMinutesTotal();

		_restant = Integer.valueOf(restant().intValue() + minutes);
		getSolde().setRestant(restant());
		// CktlLog.log("*************** crediter() restant()=" +
		// TimeCtrl.stringForMinutes(restant().intValue()));

	}

	/**
	 * 
	 * @return
	 */
	private NSMutableDictionary<Transaction, Integer> getDicoRestantTransaction() {
		if (_dicoRestantTransaction == null) {
			_dicoRestantTransaction = new NSMutableDictionary<Transaction, Integer>();
		}
		return _dicoRestantTransaction;
	}

	/**
	 * 
	 * @return
	 */
	private NSMutableDictionary<I_Operation, Integer> getDicoRestantOperation() {
		if (_dicoRestantOperation == null) {
			_dicoRestantOperation = new NSMutableDictionary<I_Operation, Integer>();
		}
		return _dicoRestantOperation;
	}

	public final Integer getInit() {
		return init;
	}

	/**
	 * Tableaux de bord : la valeur du solde au 31/12 (reliquat, congés ...)
	 * 
	 * @return
	 */
	public Integer getRestantAu(NSTimestamp dateButoirInclue) {
		Integer restant = null;

		NSMutableArray<I_Operation> operationArray = getOperationArray();
		for (I_Operation operation : operationArray) {

			for (Transaction transaction : operation.getOperationDelegate().getTransactionArray()) {
				if (transaction.getDate() != null &&
						transaction.getDate().before(dateButoirInclue)) {
					restant = restantApresTransaction(transaction);
				}
			}

		}

		return restant;
	}

	/**
	 * Tableaux de bord : la valeur du solde au 31/12 (reliquat, congés ...)
	 * 
	 * @return
	 */
	public Integer getConsommationAu(NSTimestamp dateButoirInclue) {
		int consommation = 0;

		for (Transaction transaction : getTransactionArray()) {
			if (transaction.getDate() != null &&
					DateCtrlHamac.isBeforeEq(transaction.getDate(), dateButoirInclue) &&
					transaction.getSolde() == getSolde()) {
				consommation += transaction.getMinutesTotal();

			}
		}

		return consommation;
	}
}
