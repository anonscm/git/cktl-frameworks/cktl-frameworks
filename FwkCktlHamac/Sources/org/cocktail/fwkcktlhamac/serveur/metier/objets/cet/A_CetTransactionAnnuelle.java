/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;

import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXCustomObject;

/**
 * Décrire un sous objet d'une transaction annuelle CET répresentant un crédit
 * ou un débit
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_CetTransactionAnnuelle
		extends ERXCustomObject
			implements I_CetCredit {

	private EOCetTransactionAnnuelle eoCetTransactionAnnuelle;
	private int valeur;

	public A_CetTransactionAnnuelle(
			EOCetTransactionAnnuelle eoCetTransactionAnnuelle,
			int valeur) {
		super();
		this.eoCetTransactionAnnuelle = eoCetTransactionAnnuelle;
		this.valeur = valeur;
	}

	public abstract String libelle();

	@Override
	public String toString() {
		String toString = "";

		StringBuffer sbConversionJour = new StringBuffer();
		sbConversionJour = FormatterCtrl.getNewMinuteToJourCetFormatter(eoCetTransactionAnnuelle.toPeriode()).format(
				Integer.valueOf(getValeur()), sbConversionJour, null);

		toString =
				// getClass().getName().substring(getClass().getPackage().getName().length()
				// + 1) +
				libelle()
		// +
		// " (" + TimeCtrl.stringForMinutes(getValeur()) + " soit " +
		// sbConversionJour.toString() +
		// /*
		// * TimeCtrl.getJoursForMinutes(getValeur(),
		// * Jour.DUREE_JOUR_EN_MINUTES) +
		// */" à " +
		// TimeCtrl.stringForMinutes(Jour.DUREE_JOUR_EN_MINUTES) + ") au " +
		// DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur())
		;

		return toString;
	}

	public final int getValeur() {
		return valeur;
	}

	public final EOCetTransactionAnnuelle getEoCetTransactionAnnuelle() {
		return eoCetTransactionAnnuelle;
	}

	public Integer credit() {
		return Integer.valueOf(valeur);
	}
	
	public Integer debit() {
		return null;
	}

	public NSTimestamp dateValeur() {
		return getEoCetTransactionAnnuelle().dateValeur();
	}

	public boolean isAncienRegime() {
		boolean isAncienRegime = false;

		if (DateCtrlHamac.isBeforeEq(dateValeur(), CetFactory.getDateCetDebutRegimePerenne())) {
			isAncienRegime = true;
		}

		return isAncienRegime;
	}
}
