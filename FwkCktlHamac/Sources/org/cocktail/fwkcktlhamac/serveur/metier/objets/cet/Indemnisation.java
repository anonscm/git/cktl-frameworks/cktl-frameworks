/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class Indemnisation
		extends A_CetTransactionAnnuelleDebit {

	/**
	 * @param eoCetTransactionAnnuelle
	 */
	public Indemnisation(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	@Override
	public String libelle() {
		return "Indemnisation sur CET (au " + DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur()) + ")";
	}
}
