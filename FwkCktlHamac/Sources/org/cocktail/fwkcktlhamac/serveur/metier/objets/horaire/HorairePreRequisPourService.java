/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import er.extensions.eof.ERXCustomObject;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class HorairePreRequisPourService
		extends ERXCustomObject {

	public final static String C_STRUCTURE_KEY = "cStructure";

	private EOStructure eoStructure;
	private int dureeHebdoMini, dureeHebdoMaxi;

	/**
		 * 
		 */
	public HorairePreRequisPourService(EOStructure eoStructure, int dureeHebdoMini, int dureeHebdoMaxi) {
		super();
		this.eoStructure = eoStructure;
		this.dureeHebdoMini = dureeHebdoMini;
		this.dureeHebdoMaxi = dureeHebdoMaxi;
	}

	public final EOStructure getEoStructure() {
		return eoStructure;
	}

	public final String getCStructure() {
		return getEoStructure().cStructure();
	}

	public final int getDureeHebdoMini() {
		return dureeHebdoMini;
	}

	public final int getDureeHebdoMaxi() {
		return dureeHebdoMaxi;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXCustomObject#toString()
	 */
	@Override
	public String toString() {
		String toString = "";

		toString += getEoStructure().lcStructure() + " mini=" + TimeCtrl.stringForMinutes(getDureeHebdoMini()) + " maxi=" + TimeCtrl.stringForMinutes(getDureeHebdoMaxi());

		return toString;
	}

}
