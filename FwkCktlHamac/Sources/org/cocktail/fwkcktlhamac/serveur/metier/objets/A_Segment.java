/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlagePause;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * Classe de gestion interne pour avoir tous les morceaux de temps non
 * consommés. Un objet de ce type est sensé etre inclus dans une plage de
 * travail
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_Segment {

	public final static String DEBUT_KEY = "debut";
	public final static String FIN_KEY = "fin";
	public final static String JOUR_KEY = "jour";

	private int debut;
	private int fin;

	public A_Segment(int debut, int fin) {
		super();
		this.debut = debut;
		this.fin = fin;

		if (getDebut() > getFin()) {
			throw new IllegalStateException("Le début (" + getDebut() + ") est après la fin (" + getFin() + ")");
		}

	}

	/**
	 * Le jour associé
	 * 
	 * @return
	 */
	public abstract Jour getJour();

	public final int getDebut() {
		return debut;
	}

	public final int getFin() {
		return fin;
	}

	/**
	 * Réservé à la gestion des pauses multiservices
	 * 
	 * @param debut
	 */
	protected final void setDebut(int debut) {
		this.debut = debut;
	}

	/**
	 * Réservé à la gestion des pauses multiservices
	 * 
	 * @param fin
	 */
	protected final void setFin(int fin) {
		this.fin = fin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "";

		toString = "Segment debut=" + TimeCtrl.stringForMinutes(getDebut()) + " fin=" + TimeCtrl.stringForMinutes(getFin());

		return toString;
	}

	/**
	 * Indique le segment chevauche une la plage de pause
	 * 
	 * @param pause
	 * @return
	 */
	protected final boolean chevauche(HorairePlagePause pause) {
		boolean chevauche = true;

		if (pause.getDebutMinutesDansJour() >= getFin() ||
				pause.getFinMinutesDansJour() <= getDebut()) {
			chevauche = false;
		}

		return chevauche;
	}

}
