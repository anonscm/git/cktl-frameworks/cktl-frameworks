package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

/**
 * Classe représentant une demande sur un type de visa et une structure
 * @author juliencallewaert
 *
 */
public class DemandeDataBean {

	
	
	private DemandeDelegate demandeDelegate;
	private EOTypeVisa typeVisa;
	private EOStructure structure;
	private EOTypeStatut typeStatutSuivant;
	
	
	public DemandeDataBean(DemandeDelegate demandeDelegate,
			EOTypeVisa typeVisa, EOStructure structure, EOTypeStatut typeStatutSuivant) {
		super();
		this.demandeDelegate = demandeDelegate;
		this.typeVisa = typeVisa;
		this.structure = structure;
		this.typeStatutSuivant = typeStatutSuivant;
	}
	
	
	public DemandeDelegate getDemandeDelegate() {
		return demandeDelegate;
	}
	
	public void setDemandeDelegate(DemandeDelegate demandeDelegate) {
		this.demandeDelegate = demandeDelegate;
	}
	
	public EOTypeVisa getTypeVisa() {
		return typeVisa;
	}
	
	public void setTypeVisa(EOTypeVisa typeVisa) {
		this.typeVisa = typeVisa;
	}
	
	public EOStructure getStructure() {
		return structure;
	}

	public void setStructure(EOStructure structure) {
		this.structure = structure;
	}

	public EOTypeStatut getTypeStatutSuivant() {
		return typeStatutSuivant;
	}

	public void setTypeStatutSuivant(EOTypeStatut typeStatutSuivant) {
		this.typeStatutSuivant = typeStatutSuivant;
	}
	
	
	
}
