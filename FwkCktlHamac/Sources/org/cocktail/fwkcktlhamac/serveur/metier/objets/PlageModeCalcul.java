package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.foundation.NSTimestamp;

/**
 * Classe représentant les plages de dates concernant les modes de calculs
 * pour le droit à congés
 * 
 *
 */
public class PlageModeCalcul {

	
	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	
	/**
	 * modeCalculTitulaire à true = mode de calcul d'un titulaire
	 * modeCalculTitulaire à false = mode de calcul d'un contractuel
	 */
	private boolean isModeCalculTitulaire;
	
	public NSTimestamp getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(NSTimestamp dateDebut) {
		this.dateDebut = dateDebut;
	}
	public NSTimestamp getDateFin() {
		return dateFin;
	}
	public void setDateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}
	public boolean isModeCalculTitulaire() {
		return isModeCalculTitulaire;
	}
	public void setModeCalculTitulaire(boolean isModeCalculTitulaire) {
		this.isModeCalculTitulaire = isModeCalculTitulaire;
	}
	
	
	/**
	 * 
	 * @param dateDebut date
	 * @param dateFin date
	 * @param isModeCalculTitulaire boolean
	 */
	public PlageModeCalcul(NSTimestamp dateDebut, NSTimestamp dateFin,
			boolean isModeCalculTitulaire) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.isModeCalculTitulaire = isModeCalculTitulaire;
	}
	
	public final static String STR_MODE_CALCUL_TITULAIRE = "Mode de calcul Titulaire";
	public final static String STR_MODE_CALCUL_CONTRACTUEL = "Mode de calcul Contractuel";
	
	
	@Override
	public String toString() {
		String str = "";
		if (isModeCalculTitulaire()) {
			str += STR_MODE_CALCUL_TITULAIRE;
		} else {
			str += STR_MODE_CALCUL_CONTRACTUEL;
		}
		str += " du ";
		str += DateCtrlHamac.dateToString(dateDebut);
		str += " au ";
		str += DateCtrlHamac.dateToString(dateFin);
		
		return str;
		
	}
	
	
}
