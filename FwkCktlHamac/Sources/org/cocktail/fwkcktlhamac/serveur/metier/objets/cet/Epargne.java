/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class Epargne
		extends A_CetTransactionAnnuelle
		implements I_Solde, I_Operation {

	/**
	 * @param eoCetTransactionAnnuelle
	 * @param valeur
	 */
	public Epargne(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	public boolean isCongeNatif() {
		return false;
	}

	public boolean isReliquatNatif() {
		return false;
	}

	public boolean isBalance() {
		return false;
	}

	public boolean isRegularisation() {
		return false;
	}

	public boolean isCongeLegal() {
		return false;
	}

	public boolean isCongeRh() {
		return false;
	}

	public boolean isDechargeSyndicale() {
		return false;
	}

	public boolean isJrti() {
		return false;
	}

	public String libelle() {
		return "CET : Epargne de " + TimeCtrl.stringForMinutes(getValeur()) + " au " +
				DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur());
	}

	public NSTimestamp dDeb() {
		return getEoCetTransactionAnnuelle().dateValeur();
	}

	public NSTimestamp dFin() {
		return null;
	}

	private SoldeDelegate _soldeDelegate;

	public SoldeDelegate getSoldeDelegate() {
		if (_soldeDelegate == null) {
			_soldeDelegate = new SoldeDelegate(this);
		}
		return _soldeDelegate;
	}

	public void clearCache() {
		_soldeDelegate = null;
	}

	public void setRestant(Integer restant) {

	}

	public String cStructure() {
		return null;
	}

	public boolean isCet() {
		return true;
	}

	public boolean isReliquatManuel() {
		return false;
	}

	private OperationDelegate _operationDelegate;

	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, this);
		}
		return _operationDelegate;
	}

	public boolean isCongeManuel() {
		return false;
	}

	public boolean isIndemCet20081136() {
		return false;
	}

	public boolean isRecuperation() {
		return false;
	}

	public boolean isHeureSupplementaireManuel() {
		return false;
	}

	public boolean isHeureSupplementaireNonValide() {
		return false;
	}

	public boolean isReliquatHsup() {
		return false;
	}

	public Integer debit() {
		return null;
	}

}
