/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_BonificationHoraire
		implements I_Operation {

	private OperationDelegate operationDelegate;
	protected int minutes;

	public A_BonificationHoraire(int minutes) {
		super();
		this.minutes = minutes;
	}

	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	public abstract String libelleBonification();

	@Override
	public String toString() {
		String toString = "";

		toString += "Bonification horaire ";
		toString += libelleBonification();
		toString += " (" + TimeCtrl.stringForMinutes(minutes) + ")";

		return toString;
	}

}