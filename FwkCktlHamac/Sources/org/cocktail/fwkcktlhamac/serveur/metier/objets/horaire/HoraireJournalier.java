/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import java.text.DateFormatSymbols;
import java.util.Locale;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.MiscCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Représente une journée dans un cycle hebdomadaire
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HoraireJournalier
		extends ERXCustomObject {

	private final static int DUREE_PRESENCE_MINIMUM_POUR_PAUSE_RTT_EN_MINUTES = 6 * 60;

	public final static String INDEX_JOUR_KEY = "indexJour";

	private Horaire horaire;
	private int indexJour;
	private NSMutableArray<HorairePlageTravail> horairePlageTravailArray;
	private HorairePlagePause horairePlagePause;
	private String libelle;
	private String libelleCourt;

	private NSMutableDictionary<String, Double> pourcentageServiceDico;

	public final static DateFormatSymbols SYMBOLS = new DateFormatSymbols(Locale.FRANCE);
	
	public final static int INDEX_LUNDI = 2;
	public final static int INDEX_MARDI = 3;
	public final static int INDEX_MERCREDI = 4;
	public final static int INDEX_JEUDI = 5;
	public final static int INDEX_VENDREDI = 6;
	private final static int INDEX_SAMEDI = 7;
	public final static int INDEX_DIMANCHE = 1;
	
	/**
	 * 
	 */
	public HoraireJournalier(
			Horaire horaire, int indexJour) {
		super();
		this.horaire = horaire;
		this.indexJour = indexJour;

		// determiner le libellé
		libelle = SYMBOLS.getWeekdays()[indexJour];
		libelleCourt = SYMBOLS.getShortWeekdays()[indexJour];

	}

	/**
	 * @return
	 */
	public final NSMutableArray<HorairePlageTravail> getHorairePlageTravailArray() {
		if (horairePlageTravailArray == null) {
			horairePlageTravailArray = new NSMutableArray<HorairePlageTravail>();
		}
		return horairePlageTravailArray;
	}

	/**
	 * @return
	 */
	public final Horaire getHoraire() {
		return horaire;
	}

	/**
	 * @return
	 */
	public final int getIndexJour() {
		return indexJour;
	}

	/**
	 * @return
	 */
	public final String getLibelle() {
		return libelle;
	}

	/**
	 * @return
	 */
	public final String getLibelleCourt() {
		return libelleCourt;
	}

	/**
	 * Le dictionnaire indiquant la répartition du temps en pourcentage des
	 * périodes de travail sur plusieurs services
	 * 
	 * @return
	 */
	public final NSMutableDictionary<String, Double> getPourcentageServiceDico() {
		if (pourcentageServiceDico == null) {
			pourcentageServiceDico = new NSMutableDictionary<String, Double>();
			for (HorairePlageTravail hpt : getHorairePlageTravailArray()) {

				double f_pourcentage = (double) 0;

				Double pourcentage = pourcentageServiceDico.objectForKey(hpt.getCStructure());

				if (pourcentage != null) {
					f_pourcentage = pourcentage.floatValue();
				}

				f_pourcentage += (double) hpt.getDureeMinutes() /
						((Number) getHorairePlageTravailArray().valueForKey("@sum." + HorairePlageTravail.DUREE_MINUTES_KEY)).floatValue();

				pourcentageServiceDico.setObjectForKey(new Double(f_pourcentage), hpt.getCStructure());
			}
		}
		return pourcentageServiceDico;
	}

	private Boolean isMultiService;

	/**
	 * Indique si les horaires de la journée touche plus d'1 service
	 * 
	 * @return
	 */
	public boolean isMultiService() {
		if (isMultiService == null) {
			if (getPourcentageServiceDico().allKeys().count() > 1) {
				isMultiService = Boolean.TRUE;
			} else {
				isMultiService = Boolean.FALSE;
			}
		}
		return isMultiService;
	}

	/**
	 * @return
	 */
	public final HorairePlagePause getHorairePlagePause() {
		return horairePlagePause;
	}

	/**
	 * @param horairePlagePause
	 */
	public final void setHorairePlagePause(HorairePlagePause horairePlagePause) {
		this.horairePlagePause = horairePlagePause;
	}

	/**
	 * Trouver l'horaire d'un demi journée
	 * 
	 * @param amPm
	 * @return
	 */
	private HorairePlageTravail getHorairePlageTravailForAmPm(String amPm) {
		HorairePlageTravail hpt = null;

		NSArray<HorairePlageTravail> array = EOQualifier.filteredArrayWithQualifier(
				getHorairePlageTravailArray(), ERXQ.isTrue(amPm));

		if (array.count() > 0) {
			hpt = array.objectAtIndex(0);
		}

		return hpt;
	}

	/**
	 * @return
	 */
	public HorairePlageTravail getHorairePlageTravailAm() {
		return getHorairePlageTravailForAmPm(HorairePlageTravail.IS_AM_KEY);
	}

	/**
	 * @return
	 */
	public HorairePlageTravail getHorairePlageTravailPm() {
		return getHorairePlageTravailForAmPm(HorairePlageTravail.IS_PM_KEY);
	}

	/**
	 * Permet de savoir si la pause RTT peut être accordée : durée de travail
	 * comptabilisée >= 6h00
	 * 
	 * @return
	 */
	public static boolean isPauseRTTAccordableForDuree(int dureeTravailTotal) {
		boolean isPauseRTTAccordable = false;

		if (dureeTravailTotal >= DUREE_PRESENCE_MINIMUM_POUR_PAUSE_RTT_EN_MINUTES) {
			isPauseRTTAccordable = true;
		}

		return isPauseRTTAccordable;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPauseRTTAccordable() {
		boolean isPauseRTTAccordable = false;

		try {
			int minutes = 0;

			if (getHorairePlageTravailAm() != null) {
				minutes += getHorairePlageTravailAm().getDureeMinutes();
			}

			if (getHorairePlageTravailPm() != null) {
				minutes += getHorairePlageTravailPm().getDureeMinutes();
			}

			isPauseRTTAccordable = isPauseRTTAccordableForDuree(minutes);

		} catch (Exception e) {

			e.printStackTrace();

		}

		return isPauseRTTAccordable;
	}

	/**
	 * @deprecated
	 * 
	 *             Indique si la pause est inclue dans le temps de travail :
	 * 
	 *             - 8h00 12h00 pause à 11h40 => inclue
	 * 
	 *             - 8h00 12h00 pause à 12h00 => pas inclue
	 * 
	 * 
	 * @return
	 */
	public boolean isPauseInclueDansTempsTravail() {
		boolean isInclue = false;

		if (isPauseRTTAccordable()) {

			if (getHorairePlageTravailAm() != null &&
					getHorairePlageTravailAm().getHorairePlagePause() != null) {
				isInclue = true;
			}

			if (!isInclue) {

				if (getHorairePlageTravailPm() != null &&
						getHorairePlageTravailPm().getHorairePlagePause() != null) {
					isInclue = true;
				}

			}
		}

		return isInclue;
	}

	private NSMutableDictionary<String, Integer> minutesMultiServiceProrataDico;

	/**
	 * La valeur à débiter / créditer en cas d'horaire journalier multi service
	 * sur les différents droits à congés
	 * 
	 * @return
	 */
	public int getMinutesMultiServiceProrata(String cStructure, int valeur) {
		if (minutesMultiServiceProrataDico == null) {
			minutesMultiServiceProrataDico = new NSMutableDictionary<String, Integer>();
		}

		Integer minutesDico = minutesMultiServiceProrataDico.objectForKey(cStructure);

		if (minutesDico == null) {
			int minutes = 0;

			// on prend le pourcentage de celui qui n'a pas la pause
			double pourcentage = (double) 0;

			for (HorairePlageTravail hpt : getHorairePlageTravailArray()) {
				if (hpt.getCStructure().equals(cStructure)) {
					pourcentage += getPourcentageServiceDico().objectForKey(hpt.getCStructure()).doubleValue();
				}
			}

			// arrondir
			minutes = MiscCtrl.arrondi(((double) valeur) * pourcentage);

			minutesDico = Integer.valueOf(minutes);

			minutesMultiServiceProrataDico.setObjectForKey(minutesDico, cStructure);
		}

		return minutesDico.intValue();
	}

	/**
	 * Pour affichage dans la gestion des horaires. Ne pas utiliser pour calculer
	 * la valeur dans les jours du planning
	 * 
	 * @return
	 */
	public int getDureeComptabilisee() {
		int minutes = 0;

		for (HorairePlageTravail hpt : getHorairePlageTravailArray()) {
			minutes += hpt.getDureeComptabilisee();
		}

		return minutes;
	}

	/**
	 * retoune le bonus en minutes pour une journée de travail
	 * 
	 * @param heuresJour
	 * @param jour
	 * @return
	 */
	public int getBonus() {

		int h1, h2, h3, h4;
		h1 = h2 = h3 = h4 = 0;

		if (getHorairePlageTravailAm() != null) {
			h1 = getHorairePlageTravailAm().getDebutMinutesDansJour();
			h2 = getHorairePlageTravailAm().getFinMinutesDansJour();
		}

		if (getHorairePlageTravailPm() != null) {
			h3 = getHorairePlageTravailPm().getDebutMinutesDansJour();
			h4 = getHorairePlageTravailPm().getFinMinutesDansJour();
		}
		int bonus = 0;

		// samedi
		if (getIndexJour() == INDEX_SAMEDI) {
			// le bonus ne s'applique que si les 10 1/2 (lundi au vendredi)
			// journees precedentes sont travaillees

			int bonusSamediMatin = 0;
			if (getHoraire().isTravaille10DemiJourneeLundiVendredi()) {
				bonusSamediMatin = (int) ((h2 - h1) * (getCoefSamediMatin() - (float) 1));
			}

			getHoraire().setBonusSamediMatin(bonusSamediMatin);
			bonus += bonusSamediMatin;

			// samedi apres midi travaillé donne bonif de 1,5
			int bonusSamediApresMidi = (int) ((h4 - h3) * (getCoefHsupSamApremDimJf() - (float) 1));
			getHoraire().setBonusSamediApresMidi(bonusSamediApresMidi);
			bonus += bonusSamediApresMidi;
		}

		// avant 7h00 ou après 19h00, pour 2h00 travaillees minimum : 1:00 -> 1h12
		int cumul = 0;

		// avant 7h00
		int debutJourneeBonus = getDebutJourneeBonus();
		if (h1 != h2 && h1 < debutJourneeBonus) {
			cumul += debutJourneeBonus - h1;
		}

		// apres 19h00
		int finJourneeBonus = getFinJourneeBonus();
		if (h4 > finJourneeBonus) {
			if (h3 > finJourneeBonus) {
				cumul += h4 - h3;
			} else {
				cumul += h4 - finJourneeBonus;
			}
		}

		if (cumul >= getMinutesTravailleesMini()) {
			bonus += (int) ((float) cumul * (getCoefDebordement() - (float) 1));
		}

		return bonus;
	}

	private final static String CLASSE_JOURNEE_NEGATIF = "negatif";
	private final static String CLASSE_JOURNEE_POSITIF = "positif";

	/**
	 * Couleur de l'affichage selon la valeur de référence d'une journée
	 * 
	 * @param hj
	 * @param dureeJour
	 * @return
	 */
	public final static String getClasseStrJourneeComptabilise(
			HoraireJournalier hj, int dureeJour) {
		String classe = CLASSE_JOURNEE_POSITIF;

		int dureeComptabilisee = hj.getDureeComptabilisee();
		int dureeJourReference = dureeJour;

		if (dureeComptabilisee < dureeJourReference) {
			classe = CLASSE_JOURNEE_NEGATIF;
		}

		return classe;
	}

	/**
	 * Libellé du jour selon l'index (interface de saisie)
	 * 
	 * @param index
	 * @return
	 */
	public final static String getJourLibelleForIndex(int index) {
		String libelle = "";

		int i = (index + 1) % 8;

		libelle = HoraireJournalier.SYMBOLS.getWeekdays()[i];

		return libelle;
	}

	/**
	 * Affichage au format HTML (pour tooltip par exemple)
	 */
	public String toHtmlString() {
		String toString = "";

		toString += "<tr>";

		toString += "<td class=jour>" + getLibelle() + "</td>";

		toString += "<td>";
		if (isPauseRTTAccordable()) {
			toString += "OUI";
		} else {
			toString += "NON";
		}
		toString += "</td>";

		toString += "<td>";
		if (getHorairePlageTravailAm() != null) {
			toString += getHorairePlageTravailAm().toShortString();
		}
		toString += "</td>";

		toString += "<td class=comptabiliseS>";
		if (getHorairePlageTravailAm() != null) {
			toString += TimeCtrl.stringForMinutes(getHorairePlageTravailAm().getDureeComptabilisee());
		}
		toString += "</td>";

		toString += "<td>";
		if (getHorairePlageTravailPm() != null) {
			toString += getHorairePlageTravailPm().toShortString();
		}
		toString += "</td>";

		toString += "<td class=comptabiliseS>";
		if (getHorairePlageTravailPm() != null) {
			toString += TimeCtrl.stringForMinutes(getHorairePlageTravailPm().getDureeComptabilisee());
		}
		toString += "</td>";

		toString += "<td>";
		if (getHorairePlagePause() != null) {
			toString += getHorairePlagePause().toShortString();
		}
		toString += "</td>";

		toString += "<td class=comptabilise>";
		if (getDureeComptabilisee() > 0) {
			toString += TimeCtrl.stringForMinutes(getDureeComptabilisee());
		}
		toString += "</td>";

		toString += "</tr>";

		return toString;
	}
	
	private int getMinutesTravailleesMini() {
		return HamacCktlConfig.intForKey(EOParametre.MINUTES_HEBDO_MINI_HOR_BONUS, getHoraire().getPeriode()); 
	}
	
	private int getDebutJourneeBonus() {
		return HamacCktlConfig.intForKey(EOParametre.DEBUT_JOURNEE_BONUS, getHoraire().getPeriode()); 
	}
	
	private int getFinJourneeBonus() {
		return HamacCktlConfig.intForKey(EOParametre.FIN_JOURNEE_BONUS, getHoraire().getPeriode()); 
	}
	
	private int getCoefSamediMatin() {
		return HamacCktlConfig.intForKey(EOParametre.COEF_SAMEDI_MATIN_BONUS, getHoraire().getPeriode());
	}
	
	private int getCoefDebordement() {
		return HamacCktlConfig.intForKey(EOParametre.COEF_DEBORDEMENT_BONUS, getHoraire().getPeriode());
	}
	
	private float getCoefHsupSamApremDimJf() {
		return HamacCktlConfig.floatForKey(EOParametre.COEF_HSUP_SAM_APREM_DIM_JF, getHoraire().getPeriode());
	}
	
}
