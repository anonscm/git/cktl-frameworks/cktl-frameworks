/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * Pause RTT journalière.
 * 
 * TODO Peut être intercallée entre plusieurs {@link HorairePlageTravail}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HorairePlagePause
		extends A_HorairePlageJournalier {

	// la plage de travail contenant ou étant accolée à la pause
	private HorairePlageTravail horairePlageTravail;

	/**
	 * @param horaire
	 * @param strChaineBrute
	 */
	public HorairePlagePause(
			Horaire horaire, String strChaineBrute) {
		super(horaire, strChaineBrute);
		setDebutMinutesDansSemaine(Integer.parseInt(getStrChaineBrute()));
		setFinMinutesDansSemaine(getDebutMinutesDansSemaine() + Horaire.PAUSE_RTT_DUREE);
		affecteHoraireJournalier();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXCustomObject#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(getHoraireJournalier().getLibelle());

		sb.append(" ");
		sb.append(TimeCtrl.stringForMinutes(getDebutMinutesDansJour()));
		sb.append(" - ");
		sb.append(TimeCtrl.stringForMinutes(getFinMinutesDansJour()));

		return sb.toString();
	}

	/**
	 * Affichage court
	 * 
	 * @return
	 */
	public String toShortString() {
		StringBuffer sb = new StringBuffer();

		sb.append(TimeCtrl.stringForMinutes(getDebutMinutesDansJour()));
		sb.append("-");
		sb.append(TimeCtrl.stringForMinutes(getFinMinutesDansJour()));

		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXCustomObject#toLongString()
	 */
	@Override
	public String toLongString() {
		StringBuffer sb = new StringBuffer();

		sb.append(" (dont pause ");
		sb.append(toShortString());

		if (getHoraireJournalier().isMultiService()) {

			for (String cStructure : getHoraireJournalier().getPourcentageServiceDico().allKeys()) {

				sb.append(" ");
				sb.append(getHoraireJournalier().getPourcentageServiceDico().objectForKey(cStructure) * (float) 100);
				sb.append("%@");
				sb.append(getHoraire().getEoHoraire().getEoStructure(cStructure).lcStructure());
			}

			sb.append(")");
		}

		return sb.toString();
	}

	/**
	 * Affecte l'objet en cours à la {@link HorairePlageTravail} qui contient la
	 * pause
	 */
	public void affecterHorairePlageTravail() {
		boolean isPauseTrouvee = false;
		for (int i = 0; i < getHoraireJournalier().getHorairePlageTravailArray().count() && !isPauseTrouvee; i++) {
			HorairePlageTravail hpt = getHoraireJournalier().getHorairePlageTravailArray().objectAtIndex(i);

			// recherche pause inclue dans plage
			if (getDebutMinutesDansSemaine() >= hpt.getDebutMinutesDansSemaine() &&
					getFinMinutesDansSemaine() <= hpt.getFinMinutesDansSemaine()) {
				isPauseTrouvee = true;
				hpt.setHorairePlagePause(this);
				horairePlageTravail = hpt;
				hpt.setIsPauseInclue(true);
			}

			// recherche pause exclue (accolée au matin ou l'après midi)
			if (getDebutMinutesDansSemaine() == hpt.getFinMinutesDansSemaine() ||
					getFinMinutesDansSemaine() == hpt.getDebutMinutesDansSemaine() - Horaire.PAUSE_RTT_DUREE) {
				isPauseTrouvee = true;
				hpt.setHorairePlagePause(this);
				horairePlageTravail = hpt;
				hpt.setIsPauseInclue(false);
			}
		}
	}

	/**
	 * @return
	 */
	public final HorairePlageTravail getHorairePlageTravail() {
		return horairePlageTravail;
	}
}
