/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Méthodes métiers liées au CET
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public final class CetFactory {

	// on met 01/01/2009 car les CET 31/12/2008 sont mis a la date de valeur
	// 01/01/2009 .....
	private final static String STR_DATE_CET_DEBUT_REGIME_PERENNE = "01/01/2009";
	private final static NSTimestamp DATE_CET_DEBUT_REGIME_PERENNE = DateCtrlHamac.stringToDate(STR_DATE_CET_DEBUT_REGIME_PERENNE);

	public final static NSTimestamp getDateCetDebutRegimePerenne() {
		return DATE_CET_DEBUT_REGIME_PERENNE;
	}

	/**
	 * Donne le plafond des minutes epargnables en CET
	 * 
	 * @param quotiteMoyenne
	 *          la quotité moyenne de l'agent sur toute la période
	 * @param plafondAnnuelEpargneCet
	 *          le plafond pour la période, pour 1 100% (paramètre établissement)
	 * @return
	 */
	protected final static int plafondEpargneCet(
			float quotiteMoyenne, int plafondAnnuelEpargneCet) {
		int plafond = 0;

		if (quotiteMoyenne < (float) 0 ||
				quotiteMoyenne > (float) 1) {
			throw new IllegalStateException(
					"La quotité doit être entre 0 et 1 (" + quotiteMoyenne + ")");
		}

		plafond = Integer.valueOf(Math.round(quotiteMoyenne * (float) plafondAnnuelEpargneCet));

		return plafond;
	}

	/**
	 * La valeur maximale, en jours, que l'agent peut demander en blocage CET.
	 * 
	 * La règle est (1 jour = xx:xx) : - ne pas dépasser le plafond maximum de 25
	 * j., déduit du droit à congés normal pour un agent de l'éduction national de
	 * 45 j., auquel on ôte les 20 j. d'épargne minimum pour accèder (paramètre de
	 * l'application PLAFOND_EPARGNE_CET_<ANNEE>)
	 * 
	 * Possible uniquement si lors du planning N-1, l'agent a pris au moins 20 j.
	 * sur ses congés annuels. Mais on ne bloque pas sur cette contrainte (cas des
	 * changement de service ou mutation ... ce sera un message affiché au
	 * demandeur et à l'administrateur qui prendra la décision qui convient) Ces
	 * 20 j. peuvent être paramétrés avec
	 * SEUIL_CONGES_CONSOMMES_JOUR_7H00_NM1_POUR_EPARGNE_CET_<ANNEE>
	 * 
	 * Valeurs à maximiser par le reliquats de congés (on n'épargne pas plus que
	 * ce que l'on a en reliquats) Valeurs à ponderer à la (aux) quotité(s)
	 * d'affectation au service
	 */
	protected final static int reliquatPourBlocageCetMaxEnJours(
			int reliquatInitialEnMinutes,
			float quotiteMoyenne,
			int plafondAnnuelEpargneCet,
			int dureeJourEnMinutes) {

		int nbJours = 0;

		if (reliquatInitialEnMinutes > 0) {

			// ramener en jours
			nbJours = reliquatInitialEnMinutes / dureeJourEnMinutes;

			// plafonner
			int plafond = plafondEpargneCet(quotiteMoyenne, plafondAnnuelEpargneCet);
			if (nbJours > plafond) {
				nbJours = plafond;
			}

		}

		return nbJours;
	}

	/**
	 * Indique si l'épargne qui va être demandée conduit l'utilisateur a devoir
	 * faire une option (oui s'il dépasse le seuil de 20 jours en CET)
	 */
	protected final static boolean isOptionPossible(
			int soldeEnMinutes,
			int epargneEnJours,
			int dureeJourEnMinutes) {
		boolean isOptionPossible = false;

		// voir si avec son epargne, il depasse les 20 jours
		int totalAvecEpargne = soldeEnMinutes + epargneEnJours * dureeJourEnMinutes;

		if (totalAvecEpargne > 20 * dureeJourEnMinutes) {
			isOptionPossible = true;
		}

		return isOptionPossible;
	}

	/**
	 * La valeur minimum que l'agent doit mettre en CET en considérant que son
	 * épargne sera de epargneEnMinutes, et qu'il y aurait un transfert de CET de
	 * l'ancien vers le nouveau système.
	 * 
	 * L'agent doit conserver au minimum 20 jours en CET (épargne inclue)
	 */
	protected final static int maintienObligatoireCetPourDemandeEpargneCetEnMinutes(
			int soldeEnMinutes,
			int epargneEnJours,
			int dureeJourEnMinutes) {

		int minutes = 0;

		// voir si avec son epargne, il depasse les 20 jours
		int totalSansEpargne = soldeEnMinutes;

		int totalAvecEpargne = totalSansEpargne + epargneEnJours * dureeJourEnMinutes;
		boolean hasMoinsDe20JoursEnCetAvecEpargne = true;
		if (totalAvecEpargne > 20 * dureeJourEnMinutes) {
			hasMoinsDe20JoursEnCetAvecEpargne = false;
		}

		if (hasMoinsDe20JoursEnCetAvecEpargne) {
			// moins de 20 jours : toute l'epargne doit etre en CET
			minutes = epargneEnJours * dureeJourEnMinutes;
		} else {
			// plus de 20 jours : le delta entre total sans epargne si
			// - de 20 jours et 20 jours
			if (totalSansEpargne < 20 * dureeJourEnMinutes) {
				minutes = (20 * dureeJourEnMinutes - totalSansEpargne);
				// on arrondit au jour supérieur
				int cetMinimumPourEpargneCetEnJours = minutes / dureeJourEnMinutes;
				// faut-il arrondir ?
				if (minutes != 0 &&
						minutes % dureeJourEnMinutes != 0) {
					cetMinimumPourEpargneCetEnJours += 1;
				}
				minutes = cetMinimumPourEpargneCetEnJours * dureeJourEnMinutes;
			}
		}

		return minutes;
	}

	/**
	 * La valeur maximum de congés d'une épargne CET qui peuvent être maintenus
	 * dans le CET de l'agent, en considérant que son épargne sera
	 * epargneEnMinutes, en supplément d'un maintien fixe obligatoire de 20j. déjà
	 * appliqué.
	 * 
	 * A partir de 20 jours épargnés, la progression maximum est de 10 jours. Pour
	 * moins de 20, la progression maximum est de 20 jours, maximisé par un total
	 * épargné en CET de 30 jours.
	 * 
	 * @param epargneEnMinutes
	 *          : la valeur de l'épargne demandée
	 * @return
	 */
	protected final static int maintienCetMaximumAuDela20JEnMinutes(
			int soldeEnMinutes,
			int epargneEnMinutes,
			int dureeJourEnMinutes) {

		int totalSansEpargne = soldeEnMinutes;

		// le nombre de minutes de progression du solde max pour cette épargne
		int progressionMax = 0;

		// de la progression uniquement s'il y a épargne
		if (epargneEnMinutes > 0) {

			// le solde est sous la barre des 20j.
			if (totalSansEpargne < 20 * dureeJourEnMinutes) {
				// progression maximisée jusqu'au 30ème j. de solde
				progressionMax = 30 * dureeJourEnMinutes - totalSansEpargne;
				// accroissement maximum de 30j.
				if (progressionMax > 30 * dureeJourEnMinutes) {
					progressionMax = 30 * dureeJourEnMinutes;
				}

			} else {
				// le solde est au dessus des 20j.
				// la progression maximum est de 10j.
				progressionMax = 10 * dureeJourEnMinutes;
			}

			// maximiser la progression à hauteur de l'épargne
			if (progressionMax > epargneEnMinutes) {
				progressionMax = epargneEnMinutes;
			}

		}

		// valeur du maintien forcé en régime pérenne associé à cette épargne
		int maintienForce = 20 * dureeJourEnMinutes;
		// s'il n'atteint pas les 20, on maintient le tout
		// normalement, ce cas ne se présente jamais
		if (totalSansEpargne + epargneEnMinutes < 20 * dureeJourEnMinutes) {
			maintienForce = totalSansEpargne + epargneEnMinutes;
		}

		// valeur finale
		int maintienMaxAuDela20J = totalSansEpargne + progressionMax - maintienForce;

		// maximisé à 40j
		if (maintienMaxAuDela20J > 40 * dureeJourEnMinutes) {
			maintienMaxAuDela20J = 40 * dureeJourEnMinutes;
		}

		return maintienMaxAuDela20J;
	}

	/**
	 * La valeur du dépassement de CET de l'agent s'il fait une épargne de
	 * epargneEnMinutes sur les 20 jours de maintien obligatoire.
	 * 
	 * @param epargneEnMinutes
	 * @return
	 */
	protected final static int depassementSeuil20JoursPourEpargneCetEnJourEntier(
			int soldeEnMinutes,
			int epargneEnMinutes,
			int dureeJourEnMinutes) {
		int depassement = 0;

		// voir si avec son epargne, il depasse les 20 jours
		int totalAvecEpargne = soldeEnMinutes + epargneEnMinutes;
		if (totalAvecEpargne > 20 * dureeJourEnMinutes) {
			depassement = (totalAvecEpargne - 20 * dureeJourEnMinutes) / dureeJourEnMinutes;
		}

		return depassement;
	}

	/**
	 * Indique si la periode de saisie CET est ouverte
	 * 
	 * @return
	 */
	public final static boolean isPeriodeDemandeEpargneCet(
			NSTimestamp dateReference, NSTimestamp dateDebutDemandeCet, NSTimestamp dateFinDemandeCet) {
		boolean isPeriodeDemandeEpargneCet = false;

		if (DateCtrlHamac.isAfterEq(dateReference, dateDebutDemandeCet) &&
				DateCtrlHamac.isBeforeEq(dateReference, dateFinDemandeCet)) {
			isPeriodeDemandeEpargneCet = true;

		}

		return isPeriodeDemandeEpargneCet;
	}

	/**
	 * Indique si le statut de l'agent lui permet de faire une demande d'épargne
	 * CET
	 * 
	 * @return
	 */
	protected final static boolean isStatutAutorise(
			boolean isAppVerifierStatutDemandeEpargneCet,
			boolean isAppAutoriserDemandeEpargneCetCddNonCdi,
			boolean isContractuel,
			boolean isContractuelCDI) {
		boolean isStatutAutorise = false;

		// faut-il vérifier le statut
		if (isAppVerifierStatutDemandeEpargneCet) {
			// voir alors si on autorise les demandes uniquement pour les
			// CDI et les fonctionnaires
			if (!isAppAutoriserDemandeEpargneCetCddNonCdi) {
				if (!isContractuel ||
						isContractuelCDI) {
					isStatutAutorise = true;
				}
			} else {
				isStatutAutorise = true;
			}
		} else {
			// pas de vérification de statut
			isStatutAutorise = true;
		}

		return isStatutAutorise;
	}

	/**
	 * Indique si l'agent peut faire une demande d'épargne CET : - une demande
	 * d'épargne n'est pas encore faite - la période de saisie est ouverte - son
	 * statut est autorisé 
	 * 
	 * @param shouldIgnorePeriodeOuverture
	 *          TODO
	 * @return
	 */
	protected final static boolean isEpargneFaisable(
			boolean shouldIgnorePeriodeOuverture,
			NSTimestamp dateReference,
			NSTimestamp dateDebutDemandeCet,
			NSTimestamp dateFinDemandeCet,
			EOPlanning eoPlanning,
			boolean isAppVerifierStatutDemandeEpargneCet,
			boolean isAppAutoriserDemandeEpargneCetCddNonCdi,
			boolean isContractuel,
			boolean isContractuelCDI,
			int dureeJourMinute,
			int seuilReliquatJourPourEpargneCet) {
		boolean isEpargneFaisable = true;

		// verification que la demande n'est pas faite
		if (!isEpargneCetDemandeNonFaite(eoPlanning)) {
			isEpargneFaisable = false;
		}

		// verification que la période est ouverte
		if (!shouldIgnorePeriodeOuverture) {
			if (isEpargneFaisable && !isPeriodeDemandeEpargneCet(
					dateReference, dateDebutDemandeCet, dateFinDemandeCet)) {
				isEpargneFaisable = false;
			}
		}

		// verification du statut
		if (isEpargneFaisable && !isStatutAutorise(
				isAppVerifierStatutDemandeEpargneCet,
				isAppAutoriserDemandeEpargneCetCddNonCdi,
				isContractuel,
				isContractuelCDI)) {
			isEpargneFaisable = false;
		}

		// verification que le seuil minimum de reliquats est atteint
		if (isEpargneFaisable &&
				eoPlanning.getPlanning().getCalculDroitCongeDelegate().getReliquatInitialMinutes(null) < seuilReliquatJourPourEpargneCet * dureeJourMinute) {
			isEpargneFaisable = false;
		}

		return isEpargneFaisable;
	}

	/**
	 * Indique si l'agent peut décider parmi ses options - la période de saisie
	 * est ouverte - autorisé - il possède au moins 20 jours de solde avec ou sans
	 * l'épargne
	 * 
	 * @return
	 */
	protected final static boolean isOptionFaisable(
			boolean shouldIgnorePeriodeOuverture,
			NSTimestamp dateReference,
			NSTimestamp dateDebutDemandeCet,
			NSTimestamp dateFinDemandeCet,
			boolean isAppVerifierStatutDemandeEpargneCet,
			boolean isAppAutoriserDemandeEpargneCetCddNonCdi,
			boolean isContractuel,
			boolean isContractuelCDI,
			int soldeEnMinutes,
			int epargneEnMinutes,
			int dureeJourMinute) {
		boolean isOptionFaisable = true;

		// verification que la période est ouverte
		if (!shouldIgnorePeriodeOuverture) {
			if (!isPeriodeDemandeEpargneCet(
					dateReference, dateDebutDemandeCet, dateFinDemandeCet)) {
				isOptionFaisable = false;
			}
		}

		// verification du statut
		if (isOptionFaisable && !isStatutAutorise(
				isAppVerifierStatutDemandeEpargneCet,
				isAppAutoriserDemandeEpargneCetCddNonCdi,
				isContractuel,
				isContractuelCDI)) {
			isOptionFaisable = false;
		}

		// verification s'il a de l'excedent sur les 20 jours à maintenir quoi qu'il
		// en soit
		if (isOptionFaisable &&
				depassementSeuil20JoursPourEpargneCetEnJourEntier(
						soldeEnMinutes, epargneEnMinutes, dureeJourMinute) == 0) {
			isOptionFaisable = false;
		}

		return isOptionFaisable;
	}

	/**
	 * Indique si aucune demande d'épargne CET n'a été faite (ni demande, ni
	 * décision)
	 */
	public final static boolean isEpargneCetDemandeNonFaite(EOPlanning eoPlanning) {
		boolean isEpargneCetDemandeNonFaite = true;

		EOCetTransactionAnnuelle transaction = eoPlanning.getEoCetTransactionAnnuelle();

		if (transaction != null && (
					transaction.cetDemEpargne() != null ||
					transaction.cetDecEpargne() != null)) {
			isEpargneCetDemandeNonFaite = false;
		}

		return isEpargneCetDemandeNonFaite;
	}

	/**
	 * Indique si l'agent peut faire une demande de passage de son CET en régime
	 * pérénne : - demande de passage en CET pérenne non faite (peut être fait une
	 * seule et unique fois par agent) - période ouverte - possède un solde non
	 * vide en CET au 31/12/2008
	 * 
	 * @param shouldIgnorePeriodeOuverture
	 *          indique s'il faut ignorer si la periode CET est ouverte
	 */
	protected final static boolean isDemandePassageRegimePerenneFaisable(
			boolean shouldIgnorePeriodeOuverture,
			NSTimestamp dateReference,
			NSTimestamp dateDebutDemandeCet,
			NSTimestamp dateFinDemandeCet,
			EOPlanning eoPlanning) {
		boolean isDemandePassageRegimePerenneFaisable = true;

		// est-ce que l'agent ou la DRH déjà statué sur son ancien régime CET ?
		if (!isJamaisStatueAncienRegime(false, eoPlanning)) {
			isDemandePassageRegimePerenneFaisable = false;
		}

		// verification que la période est ouverte
		if (!shouldIgnorePeriodeOuverture) {
			if (isDemandePassageRegimePerenneFaisable &&
					!isPeriodeDemandeEpargneCet(dateReference, dateDebutDemandeCet, dateFinDemandeCet)) {
				isDemandePassageRegimePerenneFaisable = false;
			}
		}

		// vérifier s'il possède du solde de CET au 31/12/2008
		if (isDemandePassageRegimePerenneFaisable &&
				!isCET31122008Restant(eoPlanning)) {
			isDemandePassageRegimePerenneFaisable = false;
		}

		return isDemandePassageRegimePerenneFaisable;
	}

	/**
	 * Indique si l'agent possède encore du CET sur l'ancien régime du 31/12/2008
	 */
	protected final static boolean isCET31122008Restant(EOPlanning eoPlanning) {
		boolean isCET31122008Restant = false;

		if (minutesRestantesAncienRegime(eoPlanning) > 0) {
			isCET31122008Restant = true;
		}

		return isCET31122008Restant;
	}

	/**
	 * Indique si l'agent est autorisé à exercer son droit d'option sur l'ancien
	 * régime : uniquement si l'agent n'a jamais exercé de droit d'option sur son
	 * ancien régime
	 * 
	 * @return
	 */
	protected final static boolean isDroitOptionAncienRegimeApresRenoncementPossible(EOPlanning eoPlanning) {
		boolean isDroitOptionAncienRegimeApresRenoncementPossible = true;

		isDroitOptionAncienRegimeApresRenoncementPossible = isJamaisStatueAncienRegime(true, eoPlanning);

		return isDroitOptionAncienRegimeApresRenoncementPossible;
	}

	/**
	 * Donne le total de minutes restantes sur le CET ancien régime
	 */
	protected final static int minutesRestantesAncienRegime(EOPlanning eoPlanning) {
		int minutesRestantes = 0;

		// NSArray<I_Solde> soldeCetArray = eoPlanning.getCet().getSoldeArray();
		//
		// // ceux de l'ancien régime avec du crédit
		// EOQualifier qual = ERXQ.and(
		// ERXQ.isTrue(I_CetCredit.IS_ANCIEN_REGIME_KEY),
		// ERXQ.greaterThan(I_CetCredit.CREDIT_KEY, Integer.valueOf(0)));
		//
		// soldeCetArray = EOQualifier.filteredArrayWithQualifier(
		// soldeCetArray, qual);
		//
		// minutesRestantes = ((BigDecimal) soldeCetArray.valueForKeyPath("@sum." +
		// I_CetCredit.CREDIT_KEY)).intValue();

		minutesRestantes = eoPlanning.getCet().getCreditAncienTotal().intValue() - eoPlanning.getCet().getDebitAncienTotal().intValue();

		return minutesRestantes;
	}

	/**
	 * Donne le total de minutes restantes sur le CET perenne
	 */
	protected final static int minutesRestantesRegimePerenne(EOPlanning eoPlanning, NSTimestamp avantDate) {
		int minutesRestantes = 0;

		// NSArray<I_Solde> soldeCetArray = eoPlanning.getCet().getSoldeArray();
		//
		// // ceux du regime pérenne avec du crédit
		// EOQualifier qual = ERXQ.and(
		// ERXQ.isFalse(I_CetCredit.IS_ANCIEN_REGIME_KEY),
		// ERXQ.lessThanOrEqualTo(I_CetCredit.DATE_VALEUR_KEY, avantDate),
		// ERXQ.greaterThan(I_CetCredit.CREDIT_KEY, Integer.valueOf(0)));
		//
		// soldeCetArray = EOQualifier.filteredArrayWithQualifier(
		// soldeCetArray, qual);
		//
		// minutesRestantes = ((BigDecimal) soldeCetArray.valueForKeyPath("@sum." +
		// I_CetCredit.CREDIT_KEY)).intValue();

		minutesRestantes = eoPlanning.getCet().getCreditPerenneTotal(avantDate).intValue() - eoPlanning.getCet().getDebitPerenneTotal(avantDate).intValue();

		return minutesRestantes;
	}

	/**
	 * Indique si aucune demande de transfert de l'ancien régime vers le régime
	 * pérenne n'a été faite (ni demande, ni décision)
	 * 
	 * @param isVerifierToutesAnnee
	 *          le controle doit se faire sur toutes les années. mettre
	 *          <code>false</code> pour la annee de {@link #affAnn()}
	 */
	protected final static boolean isJamaisStatueAncienRegime(
			boolean isVerifierToutesAnnee, EOPlanning eoPlanning) {
		// on va rechercher s'il existe au moins un mouvement
		// qui permet de savoir s'il y a eu demande et/ou décision
		// sur l'ancien régime
		boolean isJamaisStatueAncienRegime = true;

		//
		if (isVerifierToutesAnnee) {

			// sur toutes les années
			NSArray<EOPlanning> eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
					eoPlanning.editingContext(), eoPlanning.persId(), eoPlanning.nature(), eoPlanning.temPersonneComposante(), null, null);
			int index = 0;

			while (isJamaisStatueAncienRegime && index < eoPlanningArray.count()) {
				isJamaisStatueAncienRegime = isJamaisStatueAncienRegime(
						false, eoPlanningArray.objectAtIndex(index));
				index++;
			}

		} else {

			EOCetTransactionAnnuelle transaction = eoPlanning.getEoCetTransactionAnnuelle();

			// seulement pour cette année
			if (transaction != null &&
					transaction.isDemandeOuDecisionFaiteSurAncienRegime()) {
				isJamaisStatueAncienRegime = false;
			}

		}

		return isJamaisStatueAncienRegime;
	}

	/**
	 * Determiner si la personne connectée est autorisée a faire une demande de
	 * transfert en RAFP
	 */
	protected final static boolean isPasAutoriseARafp(
			boolean isAppVerifierStatutDemandeEpargneCet, boolean isContractuel) {
		boolean isPasAutoriseARafp = true;

		// faut-il prendre en compte le statut
		if (isAppVerifierStatutDemandeEpargneCet) {
			isPasAutoriseARafp = isContractuel;
		} else {
			isPasAutoriseARafp = false;
		}

		return isPasAutoriseARafp;
	}

	// recap

	/**
	 * Indique si l'agent a exercicé son droit d'option sur l'ancien régime
	 */
	protected final static boolean isExerciceDroitOptionAncienRegimeFait(EOPlanning eoPlanning) {

		boolean isFait = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);
		if (transaction.cetDemIndemAncien() != null &&
				transaction.cetDemMaintAncien() != null &&
				transaction.cetDemRafpAncien() != null && (
						transaction.cetDemIndemAncien().intValue() > 0 ||
								transaction.cetDemMaintAncien().intValue() > 0 ||
						transaction.cetDemRafpAncien().intValue() > 0)) {
			isFait = true;
		}

		return isFait;
	}

	/**
	 * Indique si l'agent a exercicé son droit d'option sur le régime pérènne
	 */
	protected final static boolean isExerciceDroitOptionFait(EOPlanning eoPlanning) {

		boolean isFait = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);
		if (transaction.cetDemIndemPerenne().intValue() > 0 ||
				transaction.cetDemMaintPerenne().intValue() > 0 ||
				transaction.cetDemRafpPerenne().intValue() > 0) {
			isFait = true;
		}

		return isFait;
	}

	// simulation

	/**
	 * Donne le nombre de minutes de congés restantes après l'épargne demandée.
	 * TODO c'est pas très très propre la création puis suppression derriere ...
	 * 
	 * @param epargneEnMinutes
	 * @return
	 */
	protected final static int congesRestantsApresEpargneEnMinutes(
			int epargneEnMinutes, EOPlanning eoPlanning) {
		int minutesCongesRestantsApresEpargne = 0;

		// si l'épargne est vide, alors pas besoin de lancer l'artillerie lourde,
		// les congés restants sont identiques !

		if (epargneEnMinutes > 0) {

			EOEditingContext ec = eoPlanning.editingContext();

			// on va créer un faux enregistrement de demande d'épargne
			EOCetTransactionAnnuelle transactionFake = EOCetTransactionAnnuelle.creer(
					ec,
					DateCtrl.now(),
					EOPlanning.SIMU,
					eoPlanning.persId(),
					eoPlanning.toPeriode());
			transactionFake.setCetDemEpargne(epargneEnMinutes);

			// recopier vers le planning de simulation CET en conservant la
			// transaction
			EOPlanning eoPlanningSimu = EOPlanning.dupliquerVers(
					eoPlanning.getEoPlanningReel(), EOPlanning.SIMU, transactionFake);

			// on memorise la valeur de congés restants issus de ce calcul
			minutesCongesRestantsApresEpargne = eoPlanningSimu.getPlanning().getCalculDroitCongeDelegate().getDroitCongeRestantMinutes(null);

			// et on revert un coup
			ec.undo();
			ec.revert();

		} else {

			// valeur du planning
			minutesCongesRestantsApresEpargne = eoPlanning.getPlanning().getCalculDroitCongeDelegate().getDroitCongeRestantMinutes(null);

		}

		return minutesCongesRestantsApresEpargne;
	}

	// raz

	/**
	 * Effectue la suppression de l'ensemble de la demande CET liées au planning.
	 */
	protected final static void doSupprimeDemande(EOPlanning eoPlanning, EOEditingContext ec) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		ec.deleteObject(transaction);

	}

	/**
	 * Effectue la suppression des décisions liées au droit d'option
	 */
	protected final static void doSupprimeDecisionDroitOption(EOPlanning eoPlanning) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		transaction.setCetDecMaintPerenne(null);
		transaction.setCetDecMaintForcePerenne(null);
		transaction.setCetDecIndemPerenne(null);
		transaction.setCetDecRafpPerenne(null);

	}

	/**
	 * Effectue la suppression de l'ensemble des décisions CET liées au planning.
	 */
	protected final static void doSupprimeDecision(EOPlanning eoPlanning) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		// ancien regime
		transaction.setCetDecTransAncienPerenne(null);
		transaction.setCetDecIndemAncien(null);
		transaction.setCetDecRafpAncien(null);
		transaction.setCetDecMaintAncien(null);

		// décision d'epargne
		transaction.setCetDecEpargne(null);

		// décision droit d'option
		doSupprimeDecisionDroitOption(eoPlanning);

	}

	// demandes

	/**
	 * L'enregistrement {@link EOCetTransactionAnnuelle} pour le planning courant
	 * {@link #eoPlanning}
	 * 
	 * @param isCreerSiNonExistant
	 *          TODO
	 */
	protected final static EOCetTransactionAnnuelle getEOCetTransactionAnnuelle(
			EOPlanning eoPlanning, boolean isCreerSiNonExistant) {
		EOCetTransactionAnnuelle transaction = null;

		// retrouver l'enregistrement s'il existe
		transaction = eoPlanning.getEoCetTransactionAnnuelle();

		// n'a-t-elle pas été insérée dans le context alors que la sauvegarde n'a
		// pas été faite ?
		if (transaction == null) {
			EOEditingContext ec = eoPlanning.editingContext();

			if (ec.insertedObjects().count() > 0) {
				EOQualifier qual = ERXQ.equals("entityName", EOCetTransactionAnnuelle.ENTITY_NAME);
				NSArray<EOEnterpriseObject> eoArray = EOQualifier.filteredArrayWithQualifier(
						ec.insertedObjects(), qual);
				int i = 0;
				while (transaction == null && i < eoArray.count()) {
					EOCetTransactionAnnuelle eo = (EOCetTransactionAnnuelle) eoArray.objectAtIndex(i);
					if (eo.persId().intValue() == eoPlanning.persId().intValue() &&
							eo.toPeriode() == eoPlanning.toPeriode() &&
							eo.nature().equals(eoPlanning.nature())) {
						transaction = eo;
					}

					i++;
				}
			}

			// bon ben toujours pas trouvé, on l'instancie
			if (transaction == null && isCreerSiNonExistant) {
				transaction = EOCetTransactionAnnuelle.creer(
						ec,
						DateCtrlHamac.now(),
						eoPlanning.nature(),
						eoPlanning.persId(),
						eoPlanning.toPeriode());
				// forcer la to-one vers personne
				transaction.setToPersonne(eoPlanning.toPersonne());
			}

		}

		return transaction;
	}

	/**
	 * Enregistre une demande sur l'ancien régime CET d'un agent.
	 */
	protected final static void doDemandeAncienRegime(
			EOPlanning eoPlanning,
			int minutesTransfertRegimePerenne,
			int joursIndemnisation,
			int joursTransfertRafp,
			int minutesMaintienCet,
			int dureeJourMinute) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		transaction.setCetDemTransAncienPerenne(Integer.valueOf(minutesTransfertRegimePerenne));
		transaction.setCetDemIndemAncien(Integer.valueOf(joursIndemnisation * dureeJourMinute));
		transaction.setCetDemRafpAncien(Integer.valueOf(joursTransfertRafp * dureeJourMinute));
		transaction.setCetDemMaintAncien(Integer.valueOf(minutesMaintienCet));

	}

	/**
	 * Enregistre une demande sur le régime pérénne d'un agent. Cette demande doit
	 * mentionner :
	 * 
	 * - la valeur de l'épargne (0 si pas d'épargne)
	 * 
	 * - le contenu de l'exercice du droit d'option (dont 20j doivent être placés
	 * en maintien)
	 */
	protected final static void doDemandeRegimePerenne(
			EOPlanning eoPlanning,
			int joursEpargne,
			int joursMaintienCet,
			int minutesMaintienCetForce,
			int joursIndemnisation,
			int joursTransfertRafp,
			int dureeJourMinute) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		transaction.setCetDemEpargne(Integer.valueOf(joursEpargne * dureeJourMinute));
		transaction.setCetDemMaintPerenne(Integer.valueOf(joursMaintienCet * dureeJourMinute));
		transaction.setCetDemMaintForcePerenne(Integer.valueOf(minutesMaintienCetForce));
		transaction.setCetDemIndemPerenne(Integer.valueOf(joursIndemnisation * dureeJourMinute));
		transaction.setCetDemRafpPerenne(Integer.valueOf(joursTransfertRafp * dureeJourMinute));

	}

	// les decisions

	/**
	 * Enregistre une décision sur l'ancien régime CET d'un agent. Cette décision
	 * doit mentionner :
	 * 
	 * - le nombre de minutes à transferer en régime pérenne
	 * 
	 * - à défaut : le contenu de l'exercice du droit d'option doivent être créés.
	 */
	private final static void doDecisionAncienRegime(
			EOPlanning eoPlanning,
			Integer minutesTransfertRegimePerenne,
			Integer minutesIndemnisationAncienRegime,
			Integer minutesTransfertRafpAncienRegime,
			Integer minutesMaintienCetAncienRegime) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		transaction.setCetDecTransAncienPerenne(minutesTransfertRegimePerenne);
		transaction.setCetDecIndemAncien(minutesIndemnisationAncienRegime);
		transaction.setCetDecRafpAncien(minutesTransfertRafpAncienRegime);
		transaction.setCetDecMaintAncien(minutesMaintienCetAncienRegime);
	}

	/**
	 * Enregistre une décision sur le régime pérénne d'un agent. Cette décision
	 * doit mentionner : - la valeur de l'épargne (0 si pas d'épargne) - le
	 * contenu de l'exercice du droit d'option (dont 20j doivent être placés en
	 * maintien)
	 * 
	 * Si l'un des paramètres est <code>null</code> alors, on le considère à 0.
	 */
	private final static void doDecisionRegimePerenne(
			EOPlanning eoPlanning,
			Integer minutesEpargne,
			Integer minutesMaintienCet,
			Integer minutesMaintienCetForce,
			Integer minutesIndemnisation,
			Integer minutesTransfertRafp
			) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		// memoriser le montant de l'épargne totale
		if (minutesEpargne != null) {
			transaction.setCetDecEpargne(minutesEpargne.intValue());
		}

		// la valeur en maintien CET
		if (minutesMaintienCet != null) {
			transaction.setCetDecMaintPerenne(minutesMaintienCet.intValue());
		}

		// la valeur en maintien obligatoire CET
		if (minutesMaintienCetForce != null) {
			transaction.setCetDecMaintForcePerenne(minutesMaintienCetForce.intValue());
		}

		// la valeur en indemnisation
		if (minutesIndemnisation != null) {
			transaction.setCetDecIndemPerenne(minutesIndemnisation.intValue());
		}

		// la valeur en transfert RAFP
		if (minutesTransfertRafp != null) {
			transaction.setCetDecRafpPerenne(minutesTransfertRafp.intValue());
		}
	}

	/**
	 * Prise de la décision sur l'ancien régime :
	 * 
	 * - indemnisation
	 * 
	 * - RAFP
	 * 
	 * - maintien
	 * 
	 * - transfert CET pérenne
	 * 
	 * Tous ces éléments sont identiques à la demande
	 */
	private final static void doDecisionAncienRegimeIdentiqueALaDemande(EOPlanning eoPlanning) {

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		if (transaction.cetDemTransAncienPerenne() != null) {
			doDecisionAncienRegime(
					eoPlanning,
					transaction.cetDemTransAncienPerenne(),
					transaction.cetDemIndemAncien(),
					transaction.cetDemRafpAncien(),
					transaction.cetDemMaintAncien());
		}

	}

	/**
	 * Acceptation de la demande à l'identique
	 * 
	 * @return
	 */
	private final static void doAccepterIdentique(EOPlanning eoPlanning) {

		doDecisionAncienRegimeIdentiqueALaDemande(eoPlanning);

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, true);

		doDecisionRegimePerenne(
				eoPlanning,
				transaction.cetDemEpargne(),
				transaction.cetDemMaintPerenne(),
				transaction.cetDemMaintForcePerenne(),
				transaction.cetDemIndemPerenne(),
				transaction.cetDemRafpPerenne());
	}

	/**
	 * Passage de l'état de demande vers l'état en attente de saisie d'une autre
	 * épargne par le gestionnaire.
	 */
	protected final static boolean doSaisieDecisionAutreEpargne(
			EOPlanning eoPlanning,
			int reliquatInitialEnMinutes,
			int minutesDecisionEpargne,
			int dureeJourMinute,
			float quotiteMoyenne,
			int plafondAnnuelEpargneCet) throws Exception {

		boolean isDecisionOk = true;

		// pas d'épargne négative
		if (minutesDecisionEpargne < 0) {
			throw new Exception("L'épargne ne peut pas être négative");
		}

		// pas supérieure aux reliquats
		if (minutesDecisionEpargne > reliquatInitialEnMinutes) {
			throw new Exception("L'épargne (" +
					FormatterCtrl.enJourArrondi2ChiffresApresVirgule(minutesDecisionEpargne, dureeJourMinute) + "j.) " +
					"ne peut pas être supérieure aux reliquats de congés (" +
					FormatterCtrl.enJourArrondi2ChiffresApresVirgule(reliquatInitialEnMinutes, dureeJourMinute) + "j.)");
		}

		int reliquatPourBlocageCetMaxEnMinutes = reliquatPourBlocageCetMaxEnJours(
				reliquatInitialEnMinutes,
				quotiteMoyenne,
				plafondAnnuelEpargneCet,
				dureeJourMinute) * dureeJourMinute;

		// pas supérieure au plafond
		if (minutesDecisionEpargne > reliquatPourBlocageCetMaxEnMinutes) {
			throw new Exception("L'épargne  (" +
					FormatterCtrl.enJourArrondiEntierInferieur(minutesDecisionEpargne, dureeJourMinute) + " j.) " +
					"ne peut pas être supérieure au plafond maximum (" +
					FormatterCtrl.enJourArrondi2ChiffresApresVirgule(reliquatPourBlocageCetMaxEnMinutes, dureeJourMinute) + "j.)");
		}

		if (isDecisionOk) {

			doDecisionAncienRegimeIdentiqueALaDemande(eoPlanning);

			doDecisionRegimePerenne(
					eoPlanning,
					Integer.valueOf(minutesDecisionEpargne),
					null,
					null,
					null,
					null);
		}

		return isDecisionOk;
	}

	/**
	 * Passage de l'état de épargne saisie vers droit d'option saisi par le
	 * gestionnaire.
	 * 
	 * @return <code>true</code> si pas d'erreur de validation.
	 */
	protected final static boolean doSaisieDecisionAutreDroitOption(
			EOPlanning eoPlanning,
			int soldeEnMinutes,
			int minutesMaintienCet,
			int minutesIndemnisation,
			int minutesTransfertRafp,
			int dureeJourEnMinutes) throws Exception {

		boolean isDecisionOk = true;

		// toutes les valeurs doivent être positives
		if (minutesMaintienCet < 0 ||
				minutesIndemnisation < 0 ||
				minutesTransfertRafp < 0) {
			throw new Exception("Le droit d'option ne doit pas contenir de valeurs négatives");
		}

		// la somme des options doit être égale au dépassement de 20 jours
		int alimentationEnMinutes = 0;
		if (isDecisionOk) {

			EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

			if (transaction.cetDecTransAncienPerenne() != null &&
					transaction.cetDecTransAncienPerenne().intValue() > 0) {
				alimentationEnMinutes += transaction.cetDecTransAncienPerenne().intValue();
			}
			if (transaction.cetDecEpargne() != null &&
					transaction.cetDecEpargne().intValue() > 0) {
				alimentationEnMinutes += transaction.cetDecEpargne().intValue();
			}

			int depassementEnJours7h00Arrondi = depassementSeuil20JoursPourEpargneCetEnJourEntier(
					soldeEnMinutes, alimentationEnMinutes, dureeJourEnMinutes);
			int sommeEnJours7h00 = (minutesMaintienCet + minutesIndemnisation + minutesTransfertRafp) / dureeJourEnMinutes;

			if (sommeEnJours7h00 != depassementEnJours7h00Arrondi) {
				String errorMessage = "La somme des options n'est pas égale au total soumis au droit d'option : \\n" +
						"- maintien : " + minutesMaintienCet / dureeJourEnMinutes + "\\n" +
						"- indemnisation : " + minutesIndemnisation / dureeJourEnMinutes + "\\n" +
						"- transfert RAFP : " + minutesTransfertRafp / dureeJourEnMinutes + "\\n" +
						"- somme : " + sommeEnJours7h00 + "\\n" +
						"- total attendu : " + depassementEnJours7h00Arrondi;
				throw new Exception(errorMessage);
			}

		}

		// le maintien ne doit excéder un certain seuil
		if (isDecisionOk &&
				minutesMaintienCet > maintienCetMaximumAuDela20JEnMinutes(soldeEnMinutes, alimentationEnMinutes, dureeJourEnMinutes)) {
			String errorMessage = "Le maintien en CET dépasse le maximum autorisé pour cette épargne";
			throw new Exception(errorMessage);
		}

		// calcul du maintienFORCE

		int minutesMaintienCetForce = Integer.valueOf(
				maintienObligatoireCetPourDemandeEpargneCetEnMinutes(soldeEnMinutes, alimentationEnMinutes, dureeJourEnMinutes) / dureeJourEnMinutes);

		if (isDecisionOk) {

			doDecisionRegimePerenne(
					eoPlanning,
					null,
					Integer.valueOf(minutesMaintienCet),
					Integer.valueOf(minutesMaintienCetForce),
					Integer.valueOf(minutesIndemnisation),
					Integer.valueOf(minutesTransfertRafp));

		}

		return isDecisionOk;
	}

	/**
	 * Prise de décision sur l'acceptation ou non à l'identique de la demande.
	 */
	protected final static void setIsAdminDemandeAccepteeALIdentique(EOPlanning eoPlanning, boolean isAcceptee) {

		if (isAcceptee) {
			doAccepterIdentique(eoPlanning);
		} else {
			doSupprimeDecision(eoPlanning);
		}
	}

	// ecran de gestion des demandes CET : les états

	public final static int CET_ETAT_INCONNU = 0;
	public final static int CET_ETAT_DEMANDE_NON_TRAITEE = 1;
	public final static int CET_ETAT_DEMANDE_AUTRE_EPARGNE_SAISIE = 2;
	public final static int CET_ETAT_DEMANDE_ACCEPTEE_IDENTIQUE = 3;
	public final static int CET_ETAT_DEMANDE_ACCEPTEE_DIFFERENTE = 4;

	/**
	 * Retourne l'etat de la demande CET
	 * 
	 * @return
	 */
	protected final static int etatDemande(EOPlanning eoPlanning) {
		int etatDemande = CET_ETAT_INCONNU;

		if (isAdminDemandeAccepteeALIdentique(eoPlanning)) {
			etatDemande = CET_ETAT_DEMANDE_ACCEPTEE_IDENTIQUE;
		} else if (isAdminDemandeAccepteeDecisionDifferente(eoPlanning)) {
			etatDemande = CET_ETAT_DEMANDE_ACCEPTEE_DIFFERENTE;
		} else if (isAdminDemandeAutreEpargneSaisie(eoPlanning)) {
			etatDemande = CET_ETAT_DEMANDE_AUTRE_EPARGNE_SAISIE;
		} else if (isAdminDemandeNonTraitee(eoPlanning)) {
			etatDemande = CET_ETAT_DEMANDE_NON_TRAITEE;
		}

		return etatDemande;
	}

	/**
	 * Indique peut passer à l'état donné en parametre
	 * 
	 * @return
	 */
	protected final static boolean isTransitionEtatAutorisee(EOPlanning eoPlanning, int etatSuivant) {
		boolean isTransitionEtatAutorisee = false;

		int etatDemande = etatDemande(eoPlanning);

		if (etatSuivant == CET_ETAT_DEMANDE_ACCEPTEE_IDENTIQUE) {
			isTransitionEtatAutorisee = (etatDemande == CET_ETAT_DEMANDE_NON_TRAITEE);
		} else if (etatSuivant == CET_ETAT_DEMANDE_ACCEPTEE_DIFFERENTE) {
			isTransitionEtatAutorisee = (etatDemande == CET_ETAT_DEMANDE_AUTRE_EPARGNE_SAISIE);
		} else if (etatSuivant == CET_ETAT_DEMANDE_AUTRE_EPARGNE_SAISIE) {
			isTransitionEtatAutorisee = (etatDemande == CET_ETAT_DEMANDE_NON_TRAITEE);
		} else if (etatSuivant == CET_ETAT_DEMANDE_NON_TRAITEE) {
			isTransitionEtatAutorisee = (etatDemande != CET_ETAT_DEMANDE_NON_TRAITEE && etatDemande != CET_ETAT_INCONNU);
		}

		return isTransitionEtatAutorisee;
	}

	// etat de validation

	/**
	 * Indique si la demande et la decision CET sont exactement les mêmes en terme
	 * de valeur
	 */
	protected final static boolean isAdminDemandeAccepteeALIdentique(EOPlanning eoPlanning) {
		boolean isAdminDemandeAccepteeALIdentique = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction.isAdminDemandeAccepteeALIdentique()) {
			isAdminDemandeAccepteeALIdentique = true;
		}

		return isAdminDemandeAccepteeALIdentique;
	}

	/**
	 * Indique la decision CET est faite mais pas à l'identique de la demande
	 */
	protected final static boolean isAdminDemandeAccepteeDecisionDifferente(EOPlanning eoPlanning) {
		boolean isAdminDemandeAccepteeDecisionDifferente = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction.isAdminDemandeAccepteeDecisionDifferente()) {
			isAdminDemandeAccepteeDecisionDifferente = true;
		}

		return isAdminDemandeAccepteeDecisionDifferente;
	}

	/**
	 * Indique s'il faut retourner a l'agent la demande, pour qu'il accepte la
	 * décision qui a été différente (épargne modifiée, et droit d'option aussi)
	 */
	protected final static boolean isAdminDemandeAutreEpargneSaisie(EOPlanning eoPlanning) {
		boolean isAdminDemandeAutreEpargneSaisie = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction.isAdminDemandeAutreEpargneSaisie()) {
			isAdminDemandeAutreEpargneSaisie = true;
		}

		return isAdminDemandeAutreEpargneSaisie;
	}

	/**
	 * Indique s'il faut retourner a l'agent la demande, pour qu'il accepte la
	 * décision qui a été différente (épargne modifiée, et droit d'option aussi)
	 */
	protected final static boolean isAdminDemandeNonTraitee(EOPlanning eoPlanning) {
		boolean isAdminDemandeNonTraitee = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction.isAdminDemandeNonTraitee()) {
			isAdminDemandeNonTraitee = true;
		}

		return isAdminDemandeNonTraitee;
	}

	/**
	 * Indique si {@link #isAdminDemandeAccepteeALIdentique()} ou
	 * {@link #isAdminDemandeAccepteeDecisionDifferente()}
	 * 
	 * @see PageCET
	 * @return
	 */
	protected final static boolean isAdminDemandeAcceptee(EOPlanning eoPlanning) {
		boolean isAdminDemandeAcceptee = false;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction.isAdminDemandeAccepteeALIdentique() ||
				transaction.isAdminDemandeAccepteeDecisionDifferente()) {
			isAdminDemandeAcceptee = true;
		}

		return isAdminDemandeAcceptee;
	}

	// valeurs calculées

	/**
	 * le solde du CET de la demande après transfert, épargne et exercice du droit
	 * d'option
	 * 
	 * @return
	 */
	public static int soldeApresDemandeTransfertEpargneEtExerciceDroitDOptionEnMinutes(
			int soldeEnMinutes, EOPlanning eoPlanning) {

		int solde = soldeEnMinutes;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction != null) {

			if (transaction.cetDemTransAncienPerenne() != null) {
				solde += transaction.cetDemTransAncienPerenne().intValue();
			}

			if (transaction.cetDemEpargne() != null) {
				solde += transaction.cetDemEpargne().intValue();
			}

			if (transaction.cetDemIndemPerenne() != null) {
				solde -= transaction.cetDemIndemPerenne().intValue();
			}

			if (transaction.cetDemRafpPerenne() != null) {
				solde -= transaction.cetDemRafpPerenne().intValue();
			}

		}

		return solde;
	}

	/**
	 * le solde du CET de la decision après transfert, épargne et exercice du
	 * droit d'option
	 * 
	 * @return
	 */
	public static int soldeApresDecisionTransfertEpargneEtExerciceDroitDOptionEnMinutes(
			int soldeEnMinutes, EOPlanning eoPlanning) {

		int solde = soldeEnMinutes;

		EOCetTransactionAnnuelle transaction = getEOCetTransactionAnnuelle(eoPlanning, false);

		if (transaction != null) {

			if (transaction.cetDecTransAncienPerenne() != null) {
				solde += transaction.cetDecTransAncienPerenne().intValue();
			}

			if (transaction.cetDecEpargne() != null) {
				solde += transaction.cetDecEpargne().intValue();
			}

			if (transaction.cetDecIndemPerenne() != null) {
				solde -= transaction.cetDecIndemPerenne().intValue();
			}

			if (transaction.cetDecRafpPerenne() != null) {
				solde -= transaction.cetDecRafpPerenne().intValue();
			}

		}

		return solde;
	}

}
