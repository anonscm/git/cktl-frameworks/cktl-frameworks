/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class MalusPourCongeLegalRegleRTT
		extends A_MalusPourCongeLegal {

	/**
	 * @param minutes
	 */
	public MalusPourCongeLegalRegleRTT(int minutes, EOPeriode periode) {
		super(minutes, periode);
	}

	@Override
	public String libelle() {
		String libelle = "";
		
		int dureeJourMalus = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, getPeriode());
		int jours = FormatterCtrl.enJourArrondiEntierInferieur(getMinutes(), dureeJourMalus);

		libelle += "Malus congés légaux (réduction de ";
		libelle += jours;
		libelle += " jours d'ARTT à ";
		libelle += TimeCtrl.stringForMinutes(dureeJourMalus);
		libelle += ")";

		return libelle;
	}
}
