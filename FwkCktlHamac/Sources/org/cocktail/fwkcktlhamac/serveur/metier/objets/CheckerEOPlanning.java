/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.util.NoSuchElementException;

import org.cocktail.fwkcktlgrh.common.utilities.StringCtrl;
import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPersonnePrivilege;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * Classe rassemblant tous les check de la classe {@link EOPlanning}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class CheckerEOPlanning {

	private EOPlanning eoPlanning;

	public CheckerEOPlanning(EOPlanning eoPlanning) {
		super();
		this.eoPlanning = eoPlanning;
	}

	/**
	 * @return
	 */
	private final EOPlanning getEoPlanning() {
		return eoPlanning;
	}

	/**
	 * Controle régulier du planning pour s'assurer que tout est correct (pour
	 * prévenir les changements de sources par exemple)
	 * 
	 * @return
	 * @throws IllegalStateException
	 */
	public void checkCoherence()
			throws IllegalStateException {
		String strMessage = "";

		// toutes les semaines associées

		try {
			checkToutesLesSemainesAssociees();
		} catch (IllegalStateException e) {
			strMessage += e.getMessage();
		}

		// limites de semaines hautes
		try {
			checkPlafondSemainesHautes();
		} catch (IllegalStateException e) {
			if (strMessage.length() > 0) {
				strMessage += "\n";
			}
			strMessage += e.getMessage();
		}

		// plafond du droit à congé non atteint
		try {
			checkPlafondDroitConges();
		} catch (IllegalStateException e) {
			if (strMessage.length() > 0) {
				strMessage += "\n";
			}
			strMessage += e.getMessage();
		}

		// associations
		NSArray<String> semaineIncoherenteArray = getCodeSemaineIncoherentArray();
		if (semaineIncoherenteArray.count() > 0) {
			if (strMessage.length() > 0) {
				strMessage += "\n";
			}
			strMessage += "Incoherence des associations des horaires aux semaines : ";
			strMessage += StringCtrl.arrayToStringSeparatedBy(semaineIncoherenteArray, ", ");
		}

		if (strMessage.length() > 0) {
			throw new IllegalStateException(strMessage);
		}
	}

	/**
	 * @return
	 */
	public boolean isDemandeValidationAutorisee()
			throws IllegalStateException {
		
		try {

			// toutes les semaines associées
			checkToutesLesSemainesAssociees();

			// limites de semaines hautes
			checkPlafondSemainesHautes();

			// plafond du droit à congé non atteint
			checkPlafondDroitConges();

			// état actuel en cours de validation ou en cours de modification
			checkStatutPourDemandeDeValidation();

			// si suite à modification, il faut qu'il y ait eu au moins un changement,
			// sinon il suffit d'annuler la demande de modification
			checkSiChangementPourStatutEnCoursDeModification();

		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					getEoPlanning().toString() + " Demande de validation non autorisée : " + e.getMessage());
		}

		// associations
		NSArray<String> semaineIncoherenteArray = getCodeSemaineIncoherentArray();
		if (semaineIncoherenteArray.count() > 0) {
			String strMessage = "Incoherence des associations des horaires aux semaines ";
			strMessage += "(" + semaineIncoherenteArray.count() + ")";
			strMessage += " : ";
			strMessage += StringCtrl.arrayToStringSeparatedBy(semaineIncoherenteArray, ", ");

			throw new IllegalStateException(
					getEoPlanning().toString() + " Demande de validation non autorisée : " + strMessage);
		}

		return true;
	}

	/**
	 * TODO
	 * 
	 * @return
	 */
	public boolean isAnnulationDemandeValidationAutorisee()
			throws IllegalStateException {
		try {

			// seul un planning en cours de validation
			checkStatutPourAnnulationDemandeDeValidation();

		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					getEoPlanning().toString() + " Annulation de demande de validation non autorisée : " + e.getMessage());
		}

		return true;
	}

	/**
	 * @return
	 * @throws IllegalStateException
	 */
	public boolean isDemandeModificationAutorisee()
			throws IllegalStateException {

		try {
			// seul un planning valide peut être invalidé
			checkStatutPourDemandeDeModification();

			// un historique doit exister (i.e. au moins 1 réel à été fait une
			// première fois)
			// checkExistencePlanningNature(EOPlanning.HIST, false);

		} catch (IllegalStateException e) {

			throw new IllegalStateException(
					getEoPlanning().toString() + " Demande de modification non autorisée : " + e.getMessage());

		}

		return true;
	}

	/**
	 * @return
	 * @throws IllegalStateException
	 */
	public boolean isAnnulationDemandeModificationAutorisee()
			throws IllegalStateException {

		try {

			//
			checkStatutPourAnnulationDemandeDeModification();

			// l'historique doit être exactement identique
			checkExistencePlanningNature(EOPlanning.HIST, true);

		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					getEoPlanning().toString() + " Annulation de demande de modification non autorisée : " + e.getMessage());
		}

		return true;

	}

	/**
	 * @return
	 * @throws IllegalStateException
	 */
	public boolean isAssociationHoraireAutorisee(HamacApplicationUser hamacApplicationUser)
			throws IllegalStateException {

		try {
			//
			checkAssociationHoraireAutorisee(hamacApplicationUser);

		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					getEoPlanning().toString() + " Association des horaires non autorisée : " + e.getMessage());
		}

		return true;
	}

	/**
	 * @return
	 * @throws IllegalStateException
	 */
	public boolean isSaisieOccupationAutorisee(HamacApplicationUser hamacApplicationUser)
			throws IllegalStateException {

		try {
			//
			checkSaisieOccupationAutorisee(hamacApplicationUser);

		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					getEoPlanning().toString() + " Saisie d'occupation non autorisée : " + e.getMessage());
		}

		return true;
	}

	// check

	/**
	 * Controler que toutes les semaines du planning ont bien un horaire associé
	 * 
	 * @throws IllegalStateException
	 *           s'il en manque
	 */
	private void checkToutesLesSemainesAssociees()
			throws IllegalStateException {

		EOQualifier qualSemaineNonAssociee =
				ERXQ.and(
						ERXQ.isTrue(Semaine.IS_ACTIVE_KEY),
						ERXQ.isFalse(Semaine.IS_ASSOCIEE_KEY));
		NSArray<Semaine> semaineActiveArray = EOQualifier.filteredArrayWithQualifier(
				getEoPlanning().getPlanning().getSemaineArray(),
				qualSemaineNonAssociee);

		if (semaineActiveArray.count() > 0) {

			StringBuffer sb = new StringBuffer();
			sb.append("Les semaines suivantes n'ont pas d'horaire associé : ");

			NSMutableArray<String> numeroSemaineTraiteArray = new NSMutableArray<String>();

			for (Semaine semaine : semaineActiveArray) {
				String strNumero = Integer.toString(semaine.getNumeroSemaine());
				if (!numeroSemaineTraiteArray.containsObject(strNumero)) {
					numeroSemaineTraiteArray.addObject(strNumero);
					sb.append(strNumero);
					if (semaineActiveArray.indexOfObject(semaine) != semaineActiveArray.count() - 1) {
						sb.append(", ");
					}
				}
			}

			throw new IllegalStateException(sb.toString());

		}

	}

	/**
	 * Controler que le nombre d'associations d'horaires dits "hauts" n'excéde pas
	 * le plafond
	 * 
	 * @throws IllegalStateException
	 *           s'il en manque
	 */
	private void checkPlafondSemainesHautes()
			throws IllegalStateException {

		int maxSemaineHautes = HamacCktlConfig.intForKey(EOParametre.MAX_SEMAINE_HAUTES, getEoPlanning().toPeriode());
		// passeDroit
		if (EOPersonnePrivilege.is(getEoPlanning(), EOPersonnePrivilege.TEM_DEP_SEM_HAUTES_KEY)) {
			maxSemaineHautes = HamacCktlConfig.intForKey(EOParametre.MAX_SEMAINE_HAUTES_PASSE_DROIT, getEoPlanning().toPeriode());
		}

		if (maxSemaineHautes > 0) {

			EOQualifier qualSemaineNonAssociee =
					ERXQ.and(
							ERXQ.isTrue(Semaine.IS_ACTIVE_KEY),
							ERXQ.isTrue(Semaine.IS_SEMAINE_HAUTE_ASSOCIEE_KEY));
			NSArray<Semaine> semaineActiveArray = EOQualifier.filteredArrayWithQualifier(
					getEoPlanning().getPlanning().getSemaineArray(),
					qualSemaineNonAssociee);

			// ne compte que les numéros unique ....
			int countUnique = 0;

			NSMutableArray<String> codeSemaineTraite = new NSMutableArray<String>();

			for (Semaine semaine : semaineActiveArray) {

				String code = semaine.getCode();

				if (!codeSemaineTraite.containsObject(code)) {
					countUnique++;
					codeSemaineTraite.addObject(code);
				}

			}

			if (countUnique > maxSemaineHautes) {

				throw new IllegalStateException(
						"Le nombre maximum de " + maxSemaineHautes + " semaines hautes a été dépassé (" + countUnique + ")");

			}

		}

	}

	/**
	 * Controler que le planning ne dépasse pas le plafond de droit à congés
	 * 
	 * @throws IllegalStateException
	 *           si dépassement
	 */
	private void checkPlafondDroitConges()
			throws IllegalStateException {
		int plafondConges = getEoPlanning().getPlanning().getDroitCongeMinuteMaximum(null);

		if (plafondConges != 0) {

			int conges = getEoPlanning().getPlanning().getCalculDroitCongeDelegate().getDroitCongeMinutes(null);

			if (conges > plafondConges) {
				throw new IllegalStateException(
						"Le plafond maximum de " + TimeCtrl.stringForMinutes(plafondConges) + " de droit à congés généré est dépassé (actuellement " +
								TimeCtrl.stringForMinutes(conges) + ")");
			}
		}
	}

	/**
	 * Controler que le statut actuel du planning lui interdit une demande de
	 * validation
	 * 
	 * @throws IllegalStateException
	 */
	private void checkStatutPourDemandeDeValidation()
			throws IllegalStateException {

		if (!getEoPlanning().toTypeStatut().isEtatPlanningAutorisantValidation()) {
			throw new IllegalStateException(
					"le statut actuel interdit la validation : " + getEoPlanning().toTypeStatut().staLibelle());
		}

	}

	/**
	 * Controler que le statut actuel du planning lui interdit une demande de
	 * modification
	 * 
	 * @throws IllegalStateException
	 */
	private void checkStatutPourDemandeDeModification()
			throws IllegalStateException {

		// seulement sur la période courante
		checkPeriodeCouranteOuFuture();

		if (getEoPlanning().toTypeStatut() != null &&
				!getEoPlanning().toTypeStatut().isStatutValide()) {
			throw new IllegalStateException(
					"le statut actuel n'est pas le statut 'valide' : " + getEoPlanning().toTypeStatut().staLibelle());
		}

	}

	/**
	 * Controle qu'il existe un planning de la nature demandée, et que ce dernier
	 * contient exactement les mêmes semaines que le réel.
	 * 
	 * @param nature
	 *          historique, intervalidation ...
	 * 
	 * @param isCheckHoraireIdentique
	 *          vérifier que les horaires sont identiques
	 * 
	 * 
	 * @throws IllegalStateException
	 * @throws NoSuchElementException
	 */
	private void checkExistencePlanningNature(
			String nature, boolean isCheckHoraireIdentique)
			throws IllegalStateException, NoSuchElementException {

		EOPlanning eoPlanningNature = EOPlanning.findEoPlanning(
				getEoPlanning().editingContext(), getEoPlanning().persId(), nature, getEoPlanning().toPeriode());

		if (eoPlanningNature == null) {
			// planning inexistant
			throw new NoSuchElementException(
					"Il n'existe pas de planning \"" + nature + "\" pour ce planning");

		} else {

			String err = "";

			NSMutableArray<String> codeAbsentArray = getCodeSemaineAbsentArray(eoPlanningNature);
			if (codeAbsentArray.count() > 0) {
				err = "Les associations \"" + nature + "\" horaires pour les semaines suivantes sont absentes : ";
				err += StringCtrl.arrayToStringSeparatedBy(codeAbsentArray, ", ");
			}

			if (isCheckHoraireIdentique) {

				NSMutableArray<String> codeDiffArray = getCodeSemaineDifferentArray(eoPlanningNature);
				if (codeDiffArray.count() > 0) {

					if (err.length() > 0) {
						err += "\n";
					}
					err = "Les associations \"" + nature + "\" horaires pour les semaines suivantes sont différentes : ";
					err += StringCtrl.arrayToStringSeparatedBy(codeDiffArray, ", ");

				}

			}

			if (err.length() > 0) {
				throw new IllegalStateException(err);
			}

		}

	}

	/**
	 * Effectue une comparaison entre les occupations des 2 plannings. Si elles ne
	 * sont pas strictement identiques en dates, alors exception. Le planning de
	 * référence reste {@link #getEoPlanning()}
	 * 
	 * XXX controler sur les occupations à cheval
	 * 
	 * @param autreEoPlanning
	 *          la planning de comparaison
	 * @throws IllegalStateException
	 */
	private final void checkOccupationsDirectesIdentiques(EOPlanning autreEoPlanning)
			throws IllegalStateException {

		NSArray<EOOccupation> eoOccupationArray = getEoPlanning().findOccupation();
		NSArray<EOOccupation> eoOccupationAutreArray = autreEoPlanning.findOccupation();

		// ne conserver que celles sur la période du planning de réference
		EOQualifier qualPeriode = EOOccupation.getQualPeriode(getEoPlanning().toPeriode());
		eoOccupationArray = EOQualifier.filteredArrayWithQualifier(eoOccupationArray, qualPeriode);
		eoOccupationAutreArray = EOQualifier.filteredArrayWithQualifier(eoOccupationAutreArray, qualPeriode);

		// allez hop dans un sens
		int i = 0;
		while (i < eoOccupationArray.count()) {
			EOOccupation eoOccupation = eoOccupationArray.objectAtIndex(i);
			if (!EOOccupation.isContenuDansTableauMemeTypeEtDates(eoOccupation, eoOccupationAutreArray)) {
				throw new IllegalStateException("L'occupation " + eoOccupation + " n'a pas été trouvée");
			}
			i++;
		}

		// et dans l'autre
		i = 0;
		while (i < eoOccupationAutreArray.count()) {
			EOOccupation eoOccupationAutre = eoOccupationAutreArray.objectAtIndex(i);
			if (!EOOccupation.isContenuDansTableauMemeTypeEtDates(eoOccupationAutre, eoOccupationArray)) {
				throw new IllegalStateException("L'occupation " + eoOccupationAutre + " n'a pas été trouvée");
			}
			i++;
		}

	}

	// methodes internes utilitaires

	/**
	 * Donne la liste des codes des semaines absentes d'un planning a comparer
	 * 
	 * @param eoPlanningAComparer
	 * @return
	 */
	private NSMutableArray<String> getCodeSemaineAbsentArray(EOPlanning eoPlanningAComparer) {

		NSMutableArray<String> codeAbsentArray = new NSMutableArray<String>();

		//
		NSArray<String> codeAComparerArray = NSArrayCtrl.flattenArray(
				(NSArray<String>) eoPlanningAComparer.tosAssociationHoraire().valueForKey(EOAssociationHoraire.SEMAINE_CODE_KEY));

		for (EOAssociationHoraire eoAsso : getEoPlanning().tosAssociationHoraire()) {
			String code = eoAsso.semaineCode();
			if (!codeAComparerArray.containsObject(eoAsso.semaineCode())) {
				if (!codeAbsentArray.contains(code)) {
					codeAbsentArray.add(code);
				}
			}
		}

		return codeAbsentArray;
	}

	/**
	 * Donne la liste des codes des semaines ayant un horaire différent d'un
	 * planning a comparer
	 * 
	 * @param eoPlanningAComparer
	 * @return
	 */
	private NSMutableArray<String> getCodeSemaineDifferentArray(EOPlanning eoPlanningAComparer) {

		NSMutableArray<String> codeDiffArray = new NSMutableArray<String>();

		//
		NSArray<String> codeAComparerArray = NSArrayCtrl.flattenArray(
				(NSArray<String>) eoPlanningAComparer.tosAssociationHoraire().valueForKey(EOAssociationHoraire.SEMAINE_CODE_KEY));

		for (EOAssociationHoraire eoAsso : getEoPlanning().tosAssociationHoraire()) {
			EOQualifier qual =
					ERXQ.and(
							ERXQ.equals(EOAssociationHoraire.SEMAINE_CODE_KEY, eoAsso.semaineCode()),
							ERXQ.equals(EOAssociationHoraire.TO_HORAIRE_KEY, eoAsso.toHoraire()));
			NSArray<EOAssociationHoraire> eoAssoNatureFoundArray = EOQualifier.filteredArrayWithQualifier(
					eoPlanningAComparer.tosAssociationHoraire(), qual);
			if (eoAssoNatureFoundArray.count() == 0) {
				codeDiffArray.addObject(eoAsso.semaineCode());
			}
		}

		return codeDiffArray;
	}

	/**
	 * Donne la liste des codes des semaines ayant une incohérence
	 * 
	 * @return
	 */
	private NSMutableArray<String> getCodeSemaineIncoherentArray()
			throws IllegalStateException {

		NSMutableArray<String> codeArray = new NSMutableArray<String>();

		for (EOAssociationHoraire eoAsso : getEoPlanning().tosAssociationHoraire()) {
			try {
				eoAsso.checkCoherence();
			} catch (NSValidation.ValidationException e) {
				codeArray.addObject(eoAsso.semaineCode());
			}
		}

		return codeArray;
	}

	/**
	 * Controler que le statut actuel du planning lui interdit une annulation de
	 * demande de validation
	 * 
	 * @throws IllegalStateException
	 */
	private void checkStatutPourAnnulationDemandeDeValidation()
			throws IllegalStateException {

		if (getEoPlanning().toTypeStatut() != null &&
				!getEoPlanning().toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
			throw new IllegalStateException(
					"Le statut actuel n'est pas le statut 'en cours de validation prévisionnelle' : " + getEoPlanning().toTypeStatut().staLibelle());
		}

	}

	/**
	 * Controler que le statut actuel du planning lui interdit une annulation de
	 * demande de validation
	 * 
	 * @throws IllegalStateException
	 */
	private void checkStatutPourAnnulationDemandeDeModification()
			throws IllegalStateException {

		if (!getEoPlanning().toTypeStatut().isStatutEnCoursDeModification()) {
			throw new IllegalStateException(
					"Le statut actuel n'est pas le statut 'en cours de modification' : " + getEoPlanning().toTypeStatut().staLibelle());
		}

	}

	/**
	 * Controler qu'il y a eu au moins un changement dans les associations
	 * horaires semaines pour un état actuel "en cours de modification"
	 */
	private void checkSiChangementPourStatutEnCoursDeModification()
			throws IllegalStateException {

		if (getEoPlanning().toTypeStatut().isStatutEnCoursDeModification()) {

			EOPlanning eoPlanningIntervalidation = EOPlanning.findEoPlanning(
					getEoPlanning().editingContext(), getEoPlanning().persId(), EOPlanning.INTR, getEoPlanning().toPeriode());

			if (eoPlanningIntervalidation == null ||
					getCodeSemaineDifferentArray(eoPlanningIntervalidation).count() == 0) {

				throw new IllegalStateException(
						"statut en cours de modification et aucune différence avec le planning intervalidation");

			}

		}

	}

	/**
	 * Controler que le planning autorise la saisie d'occupation : si planning
	 * réel, alors il doit être validé et sur la période courante ou future, sinon
	 * seulement sur la période courante ou future.
	 * 
	 * @throws IllegalStateException
	 */
	private final void checkSaisieOccupationAutorisee(HamacApplicationUser hamacApplicationUser)
			throws IllegalStateException {

		if (!hamacApplicationUser.isPeriodeModifiableParAdmin(getEoPlanning())) { 
			if (!getEoPlanning().isTest() && !getEoPlanning().toTypeStatut().isStatutValide()) {
				// planning réel (ou prévisionnel) ?
				if (getEoPlanning().isReel()) {
		
					boolean isSaisieAutorisee = false;
		
					// planning valide (donc réel) => OK
					if (getEoPlanning().toTypeStatut().isStatutValide() && !hamacApplicationUser.isSuperAdmin()) {
						isSaisieAutorisee = true;
					}
		
					 if (!isSaisieAutorisee) {
						 throw new IllegalStateException();
					 }
		
				}
		
				checkPeriodeCouranteOuFuture();

				}
		}
	}

	/**
	 * Controler s'il faut / s'il est possible de saisir une occupations sur le
	 * planning prévisionnel
	 * 
	 * @throws IllegalStateException
	 */
	public final void checkOccupationSurPrevisionnelPossible()
			throws IllegalStateException {
		if (getEoPlanning().toTypeStatut().isStatutNonValide()) {
			checkOccupationsDirectesIdentiques(getEoPlanning().getEoPlanningPrev());
		} else {
			throw new IllegalStateException("Impossible de saisir des abs. prévisionnelles sur un planning du type " + getEoPlanning().toTypeStatut().staLibelle());
		}
	}

	/**
	 * Controler que le planning autorise l'association des horaires aux semaines
	 * : si planning réel, alors il doit être en cours de validation ou de
	 * modification. La période doit être la courante
	 * 
	 * @throws IllegalStateException
	 */
	private void checkAssociationHoraireAutorisee(HamacApplicationUser hamacApplicationUser)
			throws IllegalStateException {
		
		if (!hamacApplicationUser.isPeriodeModifiableParAdmin(getEoPlanning())) {
			if (getEoPlanning().isReel()) {
				if (getEoPlanning().toTypeStatut().isStatutEnCoursDeModification() == false &&
						getEoPlanning().toTypeStatut().isStatutNonValide() == false) {
					throw new IllegalStateException(
							"planning réel n'ayant ni le statut 'en cours de modification', ni le statut 'invalide'");
				}
			}

			checkPeriodeCouranteOuFuture();
		}
	}

	/**
	 * Controler que la période du planning est bien la période courante ou une
	 * période future
	 */
	private final void checkPeriodeCouranteOuFuture() {

		EOPeriode eoPeriodeCourante = EOPeriode.getCurrentPeriode(getEoPlanning().editingContext());
		
		NSTimestamp dateAComparer;
		
		if (eoPeriodeCourante == null) {
			dateAComparer = DateCtrl.now();
		} else {
			dateAComparer = eoPeriodeCourante.perDDebut();
		}
		
		if (DateCtrlHamac.isBefore(
					getEoPlanning().toPeriode().perDFin(),
					dateAComparer)) {
			
			String erreur = "la période du planning (" + getEoPlanning().toPeriode().perLibelle() 
					+ ") est une période déjà écoulée (période courante : ";
			 
			if (eoPeriodeCourante != null) {
				erreur += eoPeriodeCourante.perLibelle();
			} else {
				erreur += "<pas de période courante>";
			}
			erreur += ")";
							
			
			throw new IllegalStateException(erreur);
		}

	}

	// "envisageabilité". On ne gère pas les motifs, c'est juste un pré-filtre aux
	// checks

	/**
	 * Seul un planning réel sur la période courante peut être validable, et son
	 * statut doit être en cours de modification ou invalidé
	 * 
	 * @return
	 */
	public boolean isDemandeValidationEnvisageable() {
		boolean isEnvisageable = false;

		// période courante
		try {
			checkPeriodeCouranteOuFuture();
			isEnvisageable = true;
		} catch (IllegalStateException e) {
			isEnvisageable = false;
		}

		// planning réel
		if (isEnvisageable) {
			if (!getEoPlanning().isReel()) {
				isEnvisageable = false;
			}
		}

		// statut
		if (isEnvisageable) {
			if (getEoPlanning().toTypeStatut().isEtatPlanningAutorisantValidation() == false) {
				isEnvisageable = false;
			}
		}

		// on regarde pas non plus l'histoire du planning intervalidation (ce
		// n'est pas un motif explicite pour l'utilisateur)
		if (isEnvisageable) {
			try {
				checkSiChangementPourStatutEnCoursDeModification();
			} catch (IllegalStateException e) {
				isEnvisageable = false;
			}
		}

		return isEnvisageable;
	}
	
}
