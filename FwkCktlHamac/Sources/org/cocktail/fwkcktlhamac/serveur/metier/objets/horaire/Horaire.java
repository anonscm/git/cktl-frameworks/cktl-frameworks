/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.HoraireFieldCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Horaire "non EOF"
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Horaire
		extends ERXCustomObject {

	// FIXME interdire la saisie de cycle recouvrant plus de 2 jours !!!! car les
	// débits à la demi journée ne marcheront plus !!!!

	// TODO début de journée paramètrable ?
	private int DEBUT_JOURNEE_FORCE_AM = 9 * 60;

	// parametres horaire
	public final static int AM_DEBUT_MINI = HamacCktlConfig.intForKey(EOParametre.AM_DEBUT_MINI);
	public final static int AM_DEBUT_MAXI = HamacCktlConfig.intForKey(EOParametre.AM_DEBUT_MAXI);
	public final static int PM_FIN_MINI = HamacCktlConfig.intForKey(EOParametre.PM_FIN_MINI);
	public final static int PM_FIN_MAXI = HamacCktlConfig.intForKey(EOParametre.PM_FIN_MAXI);
	public final static int PAUSE_RTT_GAIN = HamacCktlConfig.intForKey(EOParametre.PAUSE_RTT_GAIN);
	public final static int PAUSE_MERIDIENNE_DUREE_MINI = HamacCktlConfig.intForKey(EOParametre.PAUSE_MERIDIENNE_DUREE_MINI);
	public final static int PAUSE_MERIDIENNE_DEBUT_MINI = HamacCktlConfig.intForKey(EOParametre.PAUSE_MERIDIENNE_DEBUT_MINI);
	public final static int PAUSE_MERIDIENNE_FIN_MAXI = HamacCktlConfig.intForKey(EOParametre.PAUSE_MERIDIENNE_FIN_MAXI);
	public final static int PAUSE_RTT_DUREE = HamacCktlConfig.intForKey(EOParametre.PAUSE_RTT_DUREE);
	public final static int DEMI_JOURNEE_DUREE_MAXI = HamacCktlConfig.intForKey(EOParametre.DEMI_JOURNEE_DUREE_MAXI);
	public final static int DUREE_HORAIRE_HEBDO_MINI = HamacCktlConfig.intForKey(EOParametre.DUREE_HORAIRE_HEBDO_MINI);
	public final static int DUREE_HORAIRE_HEBDO_MAXI = HamacCktlConfig.intForKey(EOParametre.DUREE_HORAIRE_HEBDO_MAXI);
	public final static int DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES = HamacCktlConfig.intForKey(EOParametre.DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES);

	// XXX a mettre en parametre !
	public final static String NOUVEL_HORAIRE_LIBELLE_DEFAUT = "";// "********* nouvel horaire *********";
	public final static int DEBUT_AM_DEFAUT = 8 * 60;
	public final static int FIN_AM_DEFAUT = 12 * 60;
	public final static int DEBUT_PM_DEFAUT = 14 * 60;
	public final static int DEBUT_PAUSE_RTT_DEFAUT = 11 * 60 + 40;

	public final static String EO_HORAIRE_KEY = "eoHoraire";
	private final static String NOM_HORAIRE_NON_EOF_DEFAULT = "horaire automatique";

	private String nom;
	private EOPeriode periode;

	// variable utilisée en mode EOF
	private EOHoraire eoHoraire;

	// listes des jours de la semaine
	private static int[] INDEX_JOUR_TAB = new int[] {
			GregorianCalendar.SUNDAY,
			GregorianCalendar.MONDAY,
			GregorianCalendar.TUESDAY,
			GregorianCalendar.WEDNESDAY,
			GregorianCalendar.THURSDAY,
			GregorianCalendar.FRIDAY,
			GregorianCalendar.SATURDAY };

	private NSMutableArray<HoraireJournalier> horaireJournalierArray;
	private NSMutableArray<HorairePlageTravail> horairePlageTravailArray;
	private NSMutableArray<HorairePlagePause> horairePlagePauseArray;

	/**
	 * Constructeur par défaut. Ne doit être appelé que par les constructeurs de
	 * la classe {@link Horaire} ou par l'interface de saisie des horaires pour
	 * simuler les durées compatibilisée et bonifications
	 */
	public Horaire(EOPeriode periode) {
		super();
		creerHoraireJournalierArray();
		setPeriode(periode);
	}

	/**
	 * A utiliser en mode non EOF (horaires forcés par exemple)
	 */
	public Horaire(NSArray<Jour> jourArray, EOPeriode periode) {
		this(periode);
		setJourArray(jourArray);
	}

	private void initHoraire(EOHoraire eoHoraire) {
		setEoHoraire(eoHoraire);
		eoHoraire.setHoraire(this);

		// XXX a mettre ailleurs : calcul et affectation du bonus à l'horaire
		for (HoraireJournalier hj : getHoraireJournalierArray()) {
			hj.getBonus();
		}
	}
	
	/**
	 * A utiliser pour adosser un objet EOF {@link EOHoraire}
	 */
	public Horaire(EOHoraire eoHoraire, EOPeriode periode) {
		this(periode);
		initHoraire(eoHoraire);
	}

	/**
	 * Forcer un des jours de l'horaire (seulement quand il s'agit d'un ou
	 * plusieurs jours, sinon utiliser {@link #Horaire(NSArray)})
	 * 
	 * Cette méthode remplace les plages journalières par un horaire forcé
	 * 
	 * @param horaire
	 * @param jourArray
	 */
	public Horaire(Horaire horaire, NSArray<Jour> jourArray, EOPeriode periode) {
		this(periode);
		setHoraireEtJourArray(horaire, jourArray);
	}

	/**
	 * 
	 */
	private void creerHoraireJournalierArray() {
		horaireJournalierArray = new NSMutableArray<HoraireJournalier>();
		for (int indexJour : INDEX_JOUR_TAB) {
			HoraireJournalier hj = new HoraireJournalier(this, indexJour);
			horaireJournalierArray.add(hj);
		}
	}

	/**
	 * Obtenir l'index du tableau INDEX_JOUR_TAB associée au jour dayOfWeek
	 * 
	 * @see GregorianCalendar.DAY_OF_WEEK
	 * 
	 * @param dayOfWeek
	 * @return
	 */
	public static int getIndexForGCDayOfWeek(int dayOfWeek) {
		int index = -1;

		for (int i = 0; i < INDEX_JOUR_TAB.length; i++) {
			if (INDEX_JOUR_TAB[i] == dayOfWeek) {
				index = i;
			}
		}

		return index;
	}

	/**
	 * @param plage
	 */
	private void addPlageTravail(HorairePlageTravail plage) {
		getHorairePlageTravailArray().addObject(plage);
	}

	/**
	 * 
	 * @param plage
	 */
	private void addPlagePause(HorairePlagePause plage) {
		getHorairePlagePauseArray().addObject(plage);
	}

	/**
	 * 
	 * @return
	 */
	public final EOHoraire getEoHoraire() {
		return eoHoraire;
	}

	/**
	 * Construction de l'objet selon le contenu de l'horaire EOF {@link EOHoraire}
	 * 
	 * @param eoHoraire
	 */
	private final void setEoHoraire(EOHoraire eoHoraire) {
		this.eoHoraire = eoHoraire;

		setNom(getEoHoraire().horLibelle());

		// raz des plages
		setHorairePlageTravailArray(new NSMutableArray<HorairePlageTravail>());
		setHorairePlagePauseArray(new NSMutableArray<HorairePlagePause>());

		setHorPlagesTravailAm(getEoHoraire().horPlagesTravailAm());
		setHorPlagesTravailPm(getEoHoraire().horPlagesTravailPm());
		setHorPauses(getEoHoraire().horPauses());

	}

	/**
	 * 
	 * @param horPlagesTravailAm
	 */
	public void setHorPlagesTravailAm(String horPlagesTravailAm) {

		// liste des plages de travail
		NSArray<String> strPlageTravailArrayAm = HoraireFieldCtrl.array(horPlagesTravailAm);

		// am
		for (String strAM : strPlageTravailArrayAm) {
			// creation objet HorairePlageTravail
			HorairePlageTravail hptAM = new HorairePlageTravail(this, strAM, true);
			addPlageTravail(hptAM);
		}
	}

	/**
	 * 
	 * @param horPlagesTravailAm
	 */
	public void setHorPlagesTravailPm(String horPlagesTravailPm) {

		NSArray<String> strPlageTravailArrayPm = HoraireFieldCtrl.array(horPlagesTravailPm);
		// pm
		for (String strPM : strPlageTravailArrayPm) {
			// creation objet HorairePlageTravail
			HorairePlageTravail hptPM = new HorairePlageTravail(this, strPM, false);
			addPlageTravail(hptPM);
		}
	}

	/**
	 * 
	 * @param horPlagesTravailAm
	 */
	public void setHorPauses(String horPauses) {

		// pauses
		NSArray<String> strPausesArray = HoraireFieldCtrl.array(horPauses);
		for (String strPause : strPausesArray) {
			// creation objet HorairePlagePause
			HorairePlagePause hpp = new HorairePlagePause(this, strPause);
			addPlagePause(hpp);
			hpp.affecterHorairePlageTravail();
		}
	}

	/**
	 * Construction de l'objet selon le travail attendu journalier
	 * 
	 * @param jourArray
	 */
	private final void setJourArray(NSArray<Jour> jourArray) {

		setNom(NOM_HORAIRE_NON_EOF_DEFAULT);

		// raz des plages
		setHorairePlageTravailArray(new NSMutableArray<HorairePlageTravail>());
		setHorairePlagePauseArray(new NSMutableArray<HorairePlagePause>());

		// liste des jours
		for (Jour jour : jourArray) {

			NSArray<HorairePlageTravail> plageArray = getPlageTravailForcePourJour(jour);

			for (int i = 0; i < plageArray.count(); i++) {

				HorairePlageTravail plage = plageArray.objectAtIndex(i);
				addPlageTravail(plage);

			}

		}

	}

	/**
	 * Construction d'un objet à partir d'un autre en ne forçant que quelques
	 * jours
	 * 
	 * @param horaire
	 * @param jourArray
	 */
	private final void setHoraireEtJourArray(Horaire horaire, NSArray<Jour> jourArray) {

		setNom(horaire.getNom());

		// raz des plages
		setHorairePlageTravailArray(new NSMutableArray<HorairePlageTravail>());
		setHorairePlagePauseArray(new NSMutableArray<HorairePlagePause>());

		// liste des jours à copier
		for (HorairePlageTravail plage : horaire.getHorairePlageTravailArray()) {

			boolean isPlageAAjouter = true;

			// ne mettre que les plages qui ne sont pas dans les jours forcés
			for (int i = 0; i < jourArray.count() && isPlageAAjouter; i++) {

				Jour jour = jourArray.objectAtIndex(i);

				if (horaire.isPlageDansJour(plage, jour)) {
					isPlageAAjouter = false;
				}

			}

			if (isPlageAAjouter) {
				addPlageTravail(plage);
			}
		}

		// liste des jours à forcer
		for (Jour jour : jourArray) {

			NSArray<HorairePlageTravail> plageArray = getPlageTravailForcePourJour(jour);

			for (int i = 0; i < plageArray.count(); i++) {

				HorairePlageTravail plage = plageArray.objectAtIndex(i);
				addPlageTravail(plage);

			}

		}

	}

	/**
	 * Retourne une liste de {@link HorairePlageTravail} forcé pour un jour donné
	 * 
	 * @param jour
	 * @return
	 */
	private NSArray<HorairePlageTravail> getPlageTravailForcePourJour(Jour jour) {

		NSArray<HorairePlageTravail> array = new NSArray<HorairePlageTravail>();

		// ne forcer ni le samedi ni le dimanche
		if (jour.isSamedi() == false &&
				jour.isDimanche() == false) {

			NSArray<TravailAttendu> travailAttenduArray = jour.getTravailAttendus();

			// faire avancer les horaires progressivement dans la journée

			int minutesDebut = jourDebutMinutes(jour) + DEBUT_JOURNEE_FORCE_AM;
			int minutesFin = minutesDebut;

			NSMutableDictionary<String, Integer> dicoRestant = new NSMutableDictionary<String, Integer>();

			// pour chaque demi journée
			for (int i = 0; i < 2; i++) {

				// pour chaque service
				for (int j = 0; j < travailAttenduArray.count(); j++) {

					TravailAttendu travailAttendu = travailAttenduArray.objectAtIndex(j);

					minutesDebut = minutesFin;

					boolean isAm = true;
					if (i == 1) {
						isAm = false;
					}

					if (isAm) {

						// am
						int duree = (int) ((float) HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, getPeriode()) * jour.quotite(travailAttendu.getCStructure()) / (float) 100);
						int dureeDemiJournee = duree / 2;
						int restant = duree - dureeDemiJournee;
						minutesFin += dureeDemiJournee;
						dicoRestant.setObjectForKey(Integer.valueOf(restant), travailAttendu.getCStructure());

					} else {

						// pm

						if (j == 0) {
							// décalage d'1h00
							minutesDebut += 60;
							minutesFin += 60;
						}

						minutesFin += dicoRestant.objectForKey(travailAttendu.getCStructure()).intValue();

					}

					// creation objet HorairePlageTravail
					String triplet = HoraireFieldCtrl.createTriplet(
							minutesDebut, minutesFin, travailAttendu.getCStructure());
					HorairePlageTravail plage = new HorairePlageTravail(this, triplet, isAm);
					array = array.arrayByAddingObject(plage);

				}

			}

		}

		return array;
	}

	/**
	 * TODO a centraliser
	 * 
	 * @param jour
	 * @return
	 */
	private static int jourDebutMinutes(Jour jour) {

		int jourDebutMinutes = 0;

		int numJour = jour.dayOfWeek();

		// ex : lundi 00:00
		jourDebutMinutes = (Jour.DUREE_JOURNEE_EN_MINUTES * (numJour - 1));

		return jourDebutMinutes;
	}

	/**
	 * TODO a centraliser
	 * 
	 * @param jour
	 * @return
	 */
	private static int jourFinMinutes(Jour jour) {

		int jourFinMinutes = 0;

		int numJour = jour.dayOfWeek();

		// ex : lundi 00:00
		jourFinMinutes = (Jour.DUREE_JOURNEE_EN_MINUTES * numJour) - 1;

		return jourFinMinutes;
	}

	/**
	 * Le qualifier permettant d'interroger les plages horaires selon un
	 * {@link Jour}
	 * 
	 * @param jour
	 * @param isCheckService
	 *          faut-il controler que le c_structure est bien celui de l'un des
	 *          {@link TravailAttendu} du {@link Jour} passé en paramètre
	 * @return
	 */
	private EOQualifier getPlageHoraireQualifierForJour(
			Jour jour, boolean isCheckService) {

		EOQualifier qual = null;

		NSArray<TravailAttendu> attenduArray = TravailAttendu.filtrerTravailAttenduPourDate(
				jour.getPlanning().getTravailAttenduArray(), jour.getDate());

		if (attenduArray.count() > 0) {
			int jourDebutMinutes = jourDebutMinutes(jour);
			int jourFinMinutes = jourFinMinutes(jour);

			qual = ERXQ.not(
					ERXQ.or(
							ERXQ.greaterThan(A_HorairePlageJournalier.DEBUT_MINUTES_DANS_SEMAINE_KEY, Integer.valueOf(jourFinMinutes)),
							ERXQ.lessThan(A_HorairePlageJournalier.FIN_MINUTES_DANS_SEMAINE_KEY, Integer.valueOf(jourDebutMinutes))));

			if (isCheckService) {

				NSArray<EOQualifier> arrayQualService = new NSArray<EOQualifier>();

				for (TravailAttendu attendu : attenduArray) {
					arrayQualService = arrayQualService.arrayByAddingObject(
							ERXQ.equals(A_HorairePlageJournalier.C_STRUCTURE_KEY, attendu.getCStructure()));
				}

				qual = ERXQ.and(
						qual, ERXQ.or(
								arrayQualService));

			}

		}

		return qual;
	}

	/**
	 * Liste des {@link HorairePlageTravail} associés a un jour
	 * 
	 * @param jour
	 * 
	 * @return
	 */
	public NSArray<HorairePlageTravail> getPlageTravailArrayFor(Jour jour) {

		NSArray<HorairePlageTravail> result = null;

		result = EOQualifier.filteredArrayWithQualifier(
				getHorairePlageTravailArray(), getPlageHoraireQualifierForJour(jour, true));

		return result;
	}

	/**
	 * Liste des {@link HorairePlagePause} associés a un jour
	 * 
	 * @param jour
	 * 
	 * @return
	 */
	public NSArray<HorairePlagePause> getPlagePauseArrayFor(Jour jour) {

		NSArray<HorairePlagePause> result = null;

		result = EOQualifier.filteredArrayWithQualifier(
				getHorairePlagePauseArray(), getPlageHoraireQualifierForJour(jour, false));

		return result;
	}

	/**
	 * 
	 * @param plage
	 * @param jour
	 * @return
	 */
	private boolean isPlageDansJour(HorairePlageTravail plage, Jour jour) {
		boolean isDansJour = false;

		if (getPlageTravailArrayFor(jour).containsObject(plage)) {
			isDansJour = true;
		}

		return isDansJour;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("horaire : " + getNom());

		for (HorairePlageTravail plageTravail : getHorairePlageTravailArray()) {
			sb.append("\n travail ");
			sb.append(plageTravail.toString() + " @ " + plageTravail.getCStructure());
		}

		for (HorairePlagePause pause : getHorairePlagePauseArray()) {
			sb.append("\n pause ");
			sb.append(pause.toString());
		}

		return sb.toString();
	}

	private String _toHtmlString;

	public void clearCache() {
		_toHtmlString = null;
	}

	private final static String LIBELLE_SEMAINE_KEY = "%LIBELLE_SEMAINE_KEY%";

	private String getToHtmlString() {
		if (StringCtrl.isEmpty(_toHtmlString)) {

			String toString = "";

			toString += "";

			toString += "<table class=detailHoraire>";

			toString += "<caption";
			if (getEoHoraire() != null && !StringCtrl.isEmpty(getEoHoraire().horCouleurHtml())) {
				toString += " style='background-color:" + getEoHoraire().horCouleurHtml() + ";'";
			}
			toString += ">";

			toString += "<i>" + LIBELLE_SEMAINE_KEY + "</i>";
			toString += " - ";
			if (getEoHoraire() != null) {
				toString += getEoHoraire().libelleComptabilise();
			} else {
				toString += "Horaire forcé";
			}
			toString += "</caption>";

			toString +=
					"<tr>" +
							"<th>Jour</th>" +
							"<th>Pause ?</th>" +
							"<th>AM</th>" +
							"<th>Comptabilisé</th>" +
							"<th>PM</th>" +
							"<th>Comptabilisé</th>" +
							"<th>Pause ARTT</th>" +
							"<th>Total</th>" +
							"</tr>";

			for (HoraireJournalier hj : getHoraireJournalierArray()) {
				// on affiche pas le dimanche
				if (hj.getIndexJour() != HoraireJournalier.INDEX_DIMANCHE) {
					toString += hj.toHtmlString();
				}
			}

			toString += "</table>";

			return toString;

		}

		return _toHtmlString;
	}

	/**
	 * Affichage au format HTML (pour tooltip par exemple)
	 */
	public String toHtmlString(String libelleSemaine) {
		String toString = "";

		toString = StringCtrl.replace(getToHtmlString(), LIBELLE_SEMAINE_KEY, libelleSemaine);

		return toString;
	}

	/**
	 * @return
	 */
	public final String getNom() {
		return nom;
	}

	/**
	 * @param nom
	 */
	private final void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return
	 */
	private final NSMutableArray<HorairePlageTravail> getHorairePlageTravailArray() {
		if (horairePlageTravailArray == null) {
			horairePlageTravailArray = new NSMutableArray<HorairePlageTravail>();
		}
		return horairePlageTravailArray;
	}

	/**
	 * @return
	 */
	private final NSMutableArray<HorairePlagePause> getHorairePlagePauseArray() {
		if (horairePlagePauseArray == null) {
			horairePlagePauseArray = new NSMutableArray<HorairePlagePause>();
		}
		return horairePlagePauseArray;
	}

	/**
	 * @param horairePlageTravailArray
	 */
	private final void setHorairePlageTravailArray(NSMutableArray<HorairePlageTravail> horairePlageTravailArray) {
		this.horairePlageTravailArray = horairePlageTravailArray;
	}

	/**
	 * @param horairePlagePauseArray
	 */
	private final void setHorairePlagePauseArray(NSMutableArray<HorairePlagePause> horairePlagePauseArray) {
		this.horairePlagePauseArray = horairePlagePauseArray;
	}

	public final NSMutableArray<HoraireJournalier> getHoraireJournalierArray() {
		return horaireJournalierArray;
	}

	/**
	 * Nombre de demi journée définies dans l'horaire
	 * 
	 * @return
	 */
	public int getNbDemiJournee() {
		int nb = 0;

		nb = getHorairePlageTravailArray().count();

		return nb;
	}

	/**
	 * L'horaire possede-t-il du travail sur les 10 demi journées du lundi au
	 * vendredi
	 * 
	 * @return
	 */
	public boolean isTravaille10DemiJourneeLundiVendredi() {

		int nb = 0;

		for (HoraireJournalier hj : getHoraireJournalierArray()) {
			if (hj.getIndexJour() == HoraireJournalier.INDEX_LUNDI ||
					hj.getIndexJour() == HoraireJournalier.INDEX_MARDI ||
					hj.getIndexJour() == HoraireJournalier.INDEX_MERCREDI ||
					hj.getIndexJour() == HoraireJournalier.INDEX_JEUDI ||
					hj.getIndexJour() == HoraireJournalier.INDEX_VENDREDI) {
				if (hj.getHorairePlageTravailAm() != null &&
						hj.getHorairePlageTravailPm() != null &&
						hj.getHorairePlageTravailAm().getDureeComptabilisee() > 0 &&
						hj.getHorairePlageTravailPm().getDureeComptabilisee() > 0) {
					nb += 2;
				}
			}
		}

		return nb == 10;
	}

	/**
	 * @return
	 */
	public int getDureeComptabilisee() {
		int minutes = 0;

		minutes = getDureeComptabilisee(null);

		return minutes;
	}

	/**
	 * @param cStructure
	 * @return
	 */
	public int getDureeComptabilisee(String cStructure) {
		int minutes = 0;

		for (HorairePlageTravail hpt : getHorairePlageTravailArray()) {
			if (StringCtrl.isEmpty(cStructure) ||
					hpt.getCStructure().equals(cStructure)) {
				minutes += hpt.getDureeComptabilisee();
			}
		}

		return minutes;
	}

	/**
	 * @param eoStructure
	 * @param hpr
	 * @throws NSValidation.ValidationException
	 */
	public void checkRepondAuPreRequisPourService(HorairePreRequisPourService hprps)
			throws NSValidation.ValidationException {

		// test des bornes
		int dureeComptabilisee = getDureeComptabilisee(hprps.getCStructure());

		int dureeHoraireHebdoMini = hprps.getDureeHebdoMini();
		int dureeHoraireHebdoMaxi = hprps.getDureeHebdoMaxi();

		if (dureeComptabilisee < dureeHoraireHebdoMini) {

			throw new NSValidation.ValidationException(
						"l'horaire associé doit au minimum comptabiliser " + TimeCtrl.stringForMinutes((int) dureeHoraireHebdoMini) +
								" de travail sur le service " + hprps.getEoStructure().lcStructure() + "(" + TimeCtrl.stringForMinutes(dureeComptabilisee) + ")");

		} else if (dureeComptabilisee > dureeHoraireHebdoMaxi) {

			throw new NSValidation.ValidationException(
					"l'horaire associé doit au maximum comptabiliser " + TimeCtrl.stringForMinutes((int) dureeHoraireHebdoMaxi) +
								" de travail sur le service " + hprps.getEoStructure().lcStructure() + "(" + TimeCtrl.stringForMinutes(dureeComptabilisee) + ")");

		}
	}

	/**
	 * Liste des pré-requis du planning auquel l'horaire répond
	 * 
	 * @return
	 */
	public NSMutableArray<HorairePreRequis> getPreRequisSatisfaitArray(Planning planning) {
		NSMutableArray<HorairePreRequis> _preRequisSatisfaitArray = new NSMutableArray<HorairePreRequis>();

		for (HorairePreRequis preRequis : planning.getDicoHorairePreRequis().allValues()) {

			boolean isPreRequisOk = true;

			for (HorairePreRequisPourService preRequisService : preRequis.getParServiceArray()) {

				try {
					checkRepondAuPreRequisPourService(preRequisService);
				} catch (NSValidation.ValidationException e) {
					isPreRequisOk = false;
				}

			}

			if (isPreRequisOk) {
				_preRequisSatisfaitArray.addObject(preRequis);
			}

		}

		return _preRequisSatisfaitArray;
	}

	/**
	 * Indique si un horaire est supprimable. Les conditions pour le changer sont
	 * :
	 * 
	 * - il n'est associe a aucune semaine
	 * 
	 * - il n'est associe que sur le planning de test
	 * 
	 * - planning invalide et pas de reel
	 * 
	 * @return
	 */
	public boolean isSupprimable() {
		boolean isOk = true;

		// associations autre que test
		NSArray<EOAssociationHoraire> array = getEoHoraire().tosAssociationHoraire(
				ERXQ.notEquals(
						EOAssociationHoraire.TO_PLANNING_KEY + "." + EOPlanning.NATURE, EOPlanning.TEST));

		if (array.count() > 0) {
			isOk = false;
		}

		return isOk;
	}

	/**
	 * Indique si un horaire est modifiable. Les conditions pour le changer sont :
	 * 
	 * - il n'est associe a aucune semaine
	 * 
	 * - il n'est associe que sur le planning de test
	 * 
	 * - planning invalide et pas de reel
	 * 
	 * @return
	 */
	public boolean isModifiable() {
		boolean isOk = true;

		// associations autre que test
		NSArray<EOAssociationHoraire> array = getEoHoraire().tosAssociationHoraire(
				ERXQ.notEquals(
						EOAssociationHoraire.TO_PLANNING_KEY + "." + EOPlanning.NATURE, EOPlanning.TEST));

		if (array.count() > 0) {
			isOk = false;
		}

		return isOk;
	}

	/**
	 * Vérification du non dépassement de la durée maximum hebdomadaire autorisée
	 * (selon privilèges de l'utilisateur)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public static void checkDureeHebdomadaireMaximum(
			int dureeComptabilisee, float quotiteReference, boolean isHorsNormes, boolean isTpa)
			throws NSValidation.ValidationException {

		if (isHorsNormes) {

			int dureeMaxi = 0;

			if (isTpa) {
				dureeMaxi = (int) Horaire.DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES;
			} else {
				dureeMaxi = (int) ((float) Horaire.DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES * quotiteReference / (float) 100);
			}

			if (dureeComptabilisee > dureeMaxi) {
				throw new NSValidation.ValidationException(
						"Vous depassez le maximum d'heures (" + TimeCtrl.stringForMinutes(dureeMaxi) +
								") autorisées pour un horaire hebdomadaire (" + TimeCtrl.stringForMinutes(dureeComptabilisee) + ") (profil hors normes)");
			}

		} else {

			int dureeMaxi = 0;

			if (isTpa) {
				dureeMaxi = (int) Horaire.DUREE_HORAIRE_HEBDO_MAXI;
			} else {
				dureeMaxi = (int) ((float) Horaire.DUREE_HORAIRE_HEBDO_MAXI * quotiteReference / (float) 100);
			}

			if (dureeComptabilisee > dureeMaxi) {
				throw new NSValidation.ValidationException(
						"Vous depassez le maximum d'heures (" + TimeCtrl.stringForMinutes(dureeMaxi) +
								") autorisées pour un horaire hebdomadaire (" + TimeCtrl.stringForMinutes(dureeComptabilisee) + ")");
			}

		}
	}

	/**
	 * Vérification de l'accomplissement de la durée minimum hebdomadaire
	 * autorisée
	 * 
	 * @param dureeComptabilisee
	 * @param isTpa
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public static void checkDureeHebdomadaireMinimum(
			int dureeComptabilisee, float quotiteReference, boolean isTpa) throws NSValidation.ValidationException {

		// les Temps Partiel Annualisés n'ont pas de seuil
		if (!isTpa) {

			int dureeMini = (int) ((float) Horaire.DUREE_HORAIRE_HEBDO_MINI * quotiteReference / (float) 100);

			if (dureeComptabilisee < dureeMini) {
				throw new NSValidation.ValidationException(
							"Vous n'atteignez le minimum d'heures (" + TimeCtrl.stringForMinutes(dureeMini) +
									") à réaliser pour un horaire hebdomadaire (" + TimeCtrl.stringForMinutes(dureeComptabilisee) + ")");
			}

		}

	}

	/**
	 * verification existence et unicite du nom
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public static void checkNomHoraire(Planning planning, String libelle) throws NSValidation.ValidationException {

		if (StringCtrl.isEmpty(libelle)) {
			throw new NSValidation.ValidationException("Le libellé de l'horaire n'est pas renseigné");
		}
		
		if (!StringUtils.isAlphanumericSpace(libelle)) {
			throw new NSValidation.ValidationException("Le libellé de l'horaire doit être alphanumérique");
		}
		
		
		// TODO unicité

	}

	/**
	 * Le contrôle de la durée hebdomadaire sur la saisie du cycle n'est possible
	 * que pour les plannings ayant une quotité globale constante, sur tous les
	 * jours (100% toute l'année par exemple).
	 * 
	 * TODO à compléter ou à virer à terme !
	 */
	public static boolean isCheckMinimumMaximumPossible(Planning planning) {
		boolean isOk = false;

		NSArray<Jour> jourArray = planning.getJourArray();

		if (jourArray.count() > 0) {

			isOk = true;

			int i = 0;

			float prevQuotite = (float) 0;
			while (isOk && i < jourArray.count()) {

				Jour jour = jourArray.objectAtIndex(i);

				// les jours à quotité 0 ne sont pas significatifs
				if (jour.quotite() != (float) 0 &&
						prevQuotite != (float) 0 &&
						jour.quotite() != prevQuotite) {
					isOk = false;
				}

				// ne mémoriser que les jours avec quotité
				if (jour.quotite() != (float) 0) {
					prevQuotite = jour.quotite();
				}

				i++;
			}

		}

		return isOk;
	}

	private int bonusSamediMatin, bonusSamediApresMidi;

	/**
	 * @param bonusSamediMatin
	 */
	public void setBonusSamediMatin(int bonusSamediMatin) {
		this.bonusSamediMatin = bonusSamediMatin;
	}

	/**
	 * @param bonusSamediApresMidi
	 */
	public void setBonusSamediApresMidi(int bonusSamediApresMidi) {
		this.bonusSamediApresMidi = bonusSamediApresMidi;
	}

	public final int getBonusSamediMatin() {
		return bonusSamediMatin;
	}

	public final int getBonusSamediApresMidi() {
		return bonusSamediApresMidi;
	}

	/**
	 * Affichage des horaires pour un planning : ne pas proposée ceux associés sur
	 * les périodes passées ou celles à venir
	 * 
	 * @param array
	 * @return
	 */
	public final static NSArray<Horaire> filtrerHoraire(
			NSArray<Horaire> array, Planning planning) {
		NSArray<Horaire> arrayFiltre = new NSArray<Horaire>();

		for (Horaire horaire : array) {
			boolean isAGarder = false;

			if (horaire.getEoHoraire().tosAssociationHoraire().count() == 0) {
				isAGarder = true;
			}

			if (!isAGarder) {
				int i = 0;

				while (!isAGarder && i < horaire.getEoHoraire().tosAssociationHoraire().count()) {
					EOAssociationHoraire eoAsso = horaire.getEoHoraire().tosAssociationHoraire().objectAtIndex(i);
					if (eoAsso.toPlanning().getEoPlanningReel() == planning.getEoPlanning().getEoPlanningReel()) {
						isAGarder = true;
					}
					i++;
				}
			}

			if (isAGarder) {
				arrayFiltre = arrayFiltre.arrayByAddingObject(horaire);
			}

		}

		return arrayFiltre;
	}

	public EOPeriode getPeriode() {
		return periode;
	}

	public void setPeriode(EOPeriode periode) {
		this.periode = periode;
	}

}
