/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class DroitCongeInitial implements I_Operation {

	private OperationDelegate operationDelegate;
	private int minutes;

	public DroitCongeInitial(int minutes) {
		super();
		this.minutes = minutes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "";

		str = "Droit à congé initial de " + TimeCtrl.stringForMinutes(minutes);

		return str;
	}

}
