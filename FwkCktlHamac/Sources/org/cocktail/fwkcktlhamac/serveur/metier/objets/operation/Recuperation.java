/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.operation;

import java.math.BigDecimal;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Solde de récupération (ARTT)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class Recuperation
		implements I_Solde, I_Operation {

	private Semaine semaine;
	private NSTimestamp dDeb;
	private NSTimestamp dFin;
	private SoldeDelegate soldeDelegate;
	private Integer valeur;
	private EOStructure eoStructure;

	private final static String VALEUR_KEY = "valeur";
	private final static Integer ZERO = Integer.valueOf(0);

	@Deprecated
	public final static EOQualifier QUAL_NEGATIVE = ERXQ.lessThan(VALEUR_KEY, ZERO);
	public final static EOQualifier QUAL_POSITIVE = ERXQ.greaterThan(VALEUR_KEY, ZERO);

	private Integer dureeRecuperationMois = null;

	private static Integer dureeRecuperationMoisEnDur = null;

	// XXX tmp modif via interface

	public int getDureeRecuperationMois() {
		if (dureeRecuperationMois == null) {
			if (dureeRecuperationMoisEnDur != null) {
				dureeRecuperationMois = dureeRecuperationMoisEnDur;
			} else {
				dureeRecuperationMois = Integer.valueOf(HamacCktlConfig.intForKey(
						EOParametre.DUREE_RECUPERATION_MOIS, getSemaine().getPlanning().getEoPlanning().toPeriode()));
			}
		}
		return dureeRecuperationMois.intValue();
	}

	public void setDureeRecuperationMois(Object object) {
		if (object instanceof String) {
			setDureeRecuperationMois((String) object);
		} else if (object instanceof BigDecimal) {
			setDureeRecuperationMois((BigDecimal) object);
		}
	}

	public void setDureeRecuperationMois(String nbMois) {
		if (!StringCtrl.isEmpty(nbMois)) {
			dureeRecuperationMois = Integer.valueOf(Integer.parseInt(nbMois));
			dureeRecuperationMoisEnDur = dureeRecuperationMois;
		}
	}

	public void setDureeRecuperationMois(BigDecimal nbMois) {
		if (nbMois != null) {
			dureeRecuperationMois = nbMois.intValue();
			dureeRecuperationMoisEnDur = dureeRecuperationMois;
		}
	}

	// fin XXX tmp

	/**
	 * @param semaine
	 * @param valeur
	 * @param eoStructure
	 */
	public Recuperation(
			Semaine semaine, Integer valeur, EOStructure eoStructure) {
		super();
		this.semaine = semaine;

		this.dDeb = DateCtrlHamac.reculerToLundi(getSemaine().getPremierJour());
		this.dFin = dDeb().timestampByAddingGregorianUnits(0, 0, 6, 0, 0, 0);
		this.valeur = valeur;
		this.eoStructure = eoStructure;
		soldeDelegate = new SoldeDelegate(this);

	}

	public SoldeDelegate getSoldeDelegate() {
		return soldeDelegate;
	}

	public void clearCache() {
		soldeDelegate = null;
	}

	public void setRestant(Integer restant) {

	}

	public boolean isCongeNatif() {
		return false;
	}

	public boolean isReliquatNatif() {
		return false;
	}

	public boolean isBalance() {
		return false;
	}

	public boolean isRegularisation() {
		return false;
	}

	public boolean isCongeLegal() {
		return false;
	}

	public boolean isCongeRh() {
		return false;
	}

	public boolean isDechargeSyndicale() {
		return false;
	}

	public boolean isJrti() {
		return false;
	}

	public NSTimestamp dDeb() {
		return dDeb;
	}

	public NSTimestamp dFin() {
		NSTimestamp dFinValidite = null;

		dFinValidite = dFin.timestampByAddingGregorianUnits(0, getDureeRecuperationMois(), 0, 0, 0, 0);

		return dFinValidite;
	}

	public final Integer getValeur() {
		return valeur;
	}

	public String libelle() {
		String libelle = "";

		libelle = "ATT de " + TimeCtrl.stringForMinutes(getValeur()) + " S" + getSemaine().getNumeroSemaine() +
				" sur " + eoStructure.lcStructure() + " (valide du " + DateCtrlHamac.dateToString(dDeb()) + " au " + DateCtrlHamac.dateToString(dFin()) + ")";

		return libelle;
	}

	@Override
	public String toString() {
		return libelle();
	}

	public String cStructure() {
		return eoStructure.cStructure();
	}

	public boolean isCet() {
		return false;
	}

	public boolean isCongeManuel() {
		return false;
	}

	public boolean isIndemCet20081136() {
		return false;
	}

	public boolean isRecuperation() {
		return true;
	}

	public boolean isHeureSupplementaireManuel() {
		return false;
	}

	public boolean isHeureSupplementaireNonValide() {
		return false;
	}

	public boolean isReliquatManuel() {
		return false;
	}

	private final Semaine getSemaine() {
		return semaine;
	}

	private OperationDelegate _operationDelegate;

	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, this);
		}
		return _operationDelegate;
	}

	public Integer initial() {
		return getValeur();
	}

	public boolean isReliquatHsup() {
		return false;
	}
}
