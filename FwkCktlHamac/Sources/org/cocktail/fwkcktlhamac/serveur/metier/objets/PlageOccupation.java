/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlageTravail;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.operation.TransfertDisparitionPauseRtt;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.MiscCtrl;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXCustomObject;
import er.extensions.eof.ERXQ;

/**
 * Permet de flecher une plage horaire sur un jour et une absence
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class PlageOccupation
		extends ERXCustomObject
		implements I_Operation {

	private Jour jour;
	private A_Absence absence;
	private HorairePlageTravail plageTravail;
	private ServiceSegment serviceSegment;

	public final static String JOUR_KEY = "jour";

	/**
	 * Gestion du débit sur plusieurs soldes. Utilisé par les objects
	 * {@link PlageOccupation}
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	public class DebitRoulantPlageOccupation
			extends A_DebitRoulant {

		/**
		 * @param soldeArray
		 * @param minutesDebutDebit
		 * @param minutesFinDebit
		 */
		public DebitRoulantPlageOccupation(
				NSArray<I_Solde> soldeArray, PlageOccupation plage, int minutesDebutDebit, int minutesFinDebit) {
			super(soldeArray, plage, minutesDebutDebit, minutesFinDebit);
		}

	}

	/**
	 * Gestion du débit sur plusieurs soldes. A n'utiliser que pour les
	 * "bricolages" avec des segments forcés
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	public class DebitRoulantSegmentForce
			extends A_DebitRoulant {

		/**
		 * @param soldeArray
		 * @param minutesDebutDebit
		 * @param minutesFinDebit
		 */
		public DebitRoulantSegmentForce(
				NSArray<I_Solde> soldeArray, PlageOccupation plage, int minutesDebutDebit, int minutesFinDebit) {
			super(soldeArray, plage, minutesDebutDebit, minutesFinDebit);
		}

	}

	/**
	 * Pour crédit hors plage de travail (heures supp ...)
	 * 
	 * @param jour
	 * @param presence
	 */
	public PlageOccupation(
			Jour jour, A_Absence presence) {
		super();
		this.jour = jour;
		this.absence = presence;

		if (absence.isPresence() == false) {
			throw new IllegalAccessError("L'occupation " + presence + " est une absence (seules les présences sont autorisées)");
		}

		traitementPostConstructeur();
	}

	/**
	 * Pour débit sur horaire de travail
	 * 
	 * @param jour
	 * @param absence
	 * @param plageTravail
	 */
	public PlageOccupation(
			Jour jour, A_Absence absence, HorairePlageTravail plageTravail) {
		super();
		this.jour = jour;
		this.absence = absence;
		this.plageTravail = plageTravail;

		if (absence.isPresence()) {
			throw new IllegalAccessError("L'occupation " + absence + " est une presence (seules les absences sont autorisées)");
		}

		// notification de la plage de travail
		getPlageTravail().getPlageOccupationArray().add(this);

		traitementPostConstructeur();
	}

	/**
	 * Pour débit sur segment fictif
	 * 
	 * @param jour
	 * @param absence
	 * @param plageTravail
	 */
	public PlageOccupation(
			Jour jour, A_Absence absence, ServiceSegment serviceSegment) {
		super();
		this.jour = jour;
		this.absence = absence;
		this.serviceSegment = serviceSegment;

		traitementPostConstructeur();
	}

	private void traitementPostConstructeur() {
		// CktlLog.log("new PlageOccupation " + this);
	}

	/**
	 * Methodes simple de soustraction de tableau
	 * 
	 * @param debutPlage
	 * @param finPlage
	 * @param exclusion
	 * @return
	 */
	private NSMutableArray<A_Segment> createPlageOccupationSegmentArrayRestant(
			int debutPlage, int finPlage, A_Segment exclusion) {

		NSMutableArray<A_Segment> segmentArray = new NSMutableArray<A_Segment>();

		// CktlLog.log("createPlageOccupationSegmentArrayRestant() debutPlage=" +
		// TimeCtrl.stringForMinutes(debutPlage) + " " +
		// "finPlage=" + TimeCtrl.stringForMinutes(finPlage) + " exclusion=" +
		// exclusion);

		if (exclusion.getDebut() <= debutPlage &&
				exclusion.getFin() >= finPlage) {
			// rien
		} else if (exclusion.getDebut() > finPlage ||
				exclusion.getFin() < debutPlage) {
			// hors plage => la plage entiere
			A_Segment plage = new PlageOccupationSegment(debutPlage, finPlage, this);
			segmentArray.addObject(plage);
		} else {
			if (exclusion.getFin() >= finPlage) {
				// le bout du début
				A_Segment segmentDebut = new PlageOccupationSegment(debutPlage, exclusion.getDebut(), this);
				segmentArray.addObject(segmentDebut);
			} else if (exclusion.getDebut() <= debutPlage) {
				// le bout de la fin
				A_Segment segmentFin = new PlageOccupationSegment(exclusion.getFin(), finPlage, this);
				segmentArray.addObject(segmentFin);
			} else {
				// les bouts du début et la fin
				A_Segment segmentDebut = new PlageOccupationSegment(debutPlage, exclusion.getDebut(), this);
				A_Segment segmentFin = new PlageOccupationSegment(exclusion.getFin(), finPlage, this);
				segmentArray.addObject(segmentDebut);
				segmentArray.addObject(segmentFin);
			}
		}

		// CktlLog.log(" >> arrayRestant() createPlageOccupationSegmentArrayRestant="
		// +
		// segmentArray);

		return segmentArray;
	}

	/**
	 * La liste des plages libres dans une grande plage en excluant d'autres
	 * plages
	 * 
	 * @return
	 */
	private NSMutableArray<A_Segment> getPlageOccupationSegmentLibreArray(
			int debutPlage, int finPlage, NSArray<A_Segment> exclusionArray) {

		NSMutableArray<A_Segment> array = new NSMutableArray<A_Segment>();

		int debut = debutPlage;
		int fin = finPlage;

		if (NSArrayCtrl.isEmpty(exclusionArray)) {

			// tout seul => la plage complète

			PlageOccupationSegment segment = new PlageOccupationSegment(debut, fin, this);
			array.addObject(segment);

		} else {

			// si au moins une exclusion englobe la fenetre d'interrogation, on arrete

			EOQualifier qualContenant = ERXQ.and(
					ERXQ.lessThanOrEqualTo(PlageOccupationSegment.DEBUT_KEY, debutPlage),
					ERXQ.greaterThanOrEqualTo(PlageOccupationSegment.FIN_KEY, finPlage));

			NSArray<A_Segment> exclusionContenanteArray = EOQualifier.filteredArrayWithQualifier(
					exclusionArray, qualContenant);

			if (exclusionContenanteArray.count() == 0) {

				// si pas d'intersection, on retourne la plage complète
				EOQualifier qualIntersection = ERXQ.or(
						ERXQ.lessThanOrEqualTo(PlageOccupationSegment.FIN_KEY, debutPlage),
						ERXQ.greaterThanOrEqualTo(PlageOccupationSegment.DEBUT_KEY, finPlage));

				NSArray<A_Segment> exclusionHorsPlageArray = EOQualifier.filteredArrayWithQualifier(
						exclusionArray, qualIntersection);

				if (exclusionArray.count() == exclusionHorsPlageArray.count()) {

					PlageOccupationSegment segment = new PlageOccupationSegment(debut, fin, this);
					array.addObject(segment);

				} else {

					// classement chronologique
					exclusionArray = CktlSort.sortedArray(exclusionArray, PlageOccupationSegment.DEBUT_KEY);

					A_Segment exclusionPrecedente = null;
					A_Segment exclusionSuivante = null;

					// XXX modif bug franck : oter les exclusions hors plage
					NSArray<A_Segment> exclusionDansPlageArray = new NSArray<A_Segment>();
					for (A_Segment exclusion : exclusionArray) {
						if (!exclusionHorsPlageArray.containsObject(exclusion)) {
							exclusionDansPlageArray = exclusionDansPlageArray.arrayByAddingObject(exclusion);
						}
					}

					for (int i = 0; i < exclusionDansPlageArray.count(); i++) {
						A_Segment exclusion = exclusionDansPlageArray.objectAtIndex(i);

						if (i < exclusionDansPlageArray.count() - 1) {
							exclusionSuivante = exclusionDansPlageArray.objectAtIndex(i + 1);
						}

						if (exclusionPrecedente != null) {
							debut = exclusionPrecedente.getFin();

						}

						if (exclusionSuivante != null) {
							fin = exclusionSuivante.getDebut();
						}

						NSArray<A_Segment> arrayLocal = createPlageOccupationSegmentArrayRestant(debut, fin, exclusion);

						array.addObjectsFromArray(arrayLocal);

						exclusionPrecedente = exclusion;
					}

				}

			}

		}

		return array;
	}

	/**
	 * 
	 */
	public String toString() {
		String toString = "";

		toString = "PlageOccupation " +
				"jour=" + DateCtrlHamac.dateToString(getJour().getDate());

		toString += " " + getAbsence().getOccupationTypeDelegate().getClass().getName();

		toString += " absence=" + getAbsence();

		if (getPlageTravail() != null) {
			toString += " dans la plage <" + getPlageTravail() + ">";
		}

		toString += " cStructure=" + getCStructure();

		return toString;
	}

	/**
	 * 
	 */
	private final Planning getPlanning() {
		return getJour().getPlanning();
	}

	private NSMutableArray<A_Segment> _segmentArrayADebiter;

	/**
	 * Les plages à débiter
	 * 
	 * La journée a déjà être pu être "consommée" par un autre solde ou une autre
	 * occupation (auquel cas on va passer par {@link #getJour()} pour avoir accès
	 * aux données nécéssaires)
	 * 
	 * XXX mis en public pour le tip dans la fiche rose
	 * 
	 * @return
	 */
	public NSMutableArray<A_Segment> getSegmentArrayADebiter() {

		if (_segmentArrayADebiter == null) {

			_segmentArrayADebiter = new NSMutableArray<A_Segment>();

			NSArray<A_Segment> segmentArray = getSegmentArrayPourMemePlageTravailEtMemeJourEtMemeAbsence();

			if (segmentArray.count() == 0) {

				// on recherche parmi les autres occupations ayant eu lieu ce jour
				NSMutableArray<A_Absence> absenceArray = new NSMutableArray<A_Absence>(getJour().getAbsenceArray().immutableClone());

				// oter l'absence courante
				absenceArray.removeIdenticalObject(getAbsence());

				// tous les autres segments
				NSMutableArray<A_Segment> otherSegmentArray = new NSMutableArray<A_Segment>();

				for (int i = 0; i < absenceArray.count(); i++) {
					A_Absence absence = absenceArray.objectAtIndex(i);

					NSArray<PlageOccupation> plageOccArray = absence.getPlageOccupationArray();

					for (int j = 0; j < plageOccArray.count(); j++) {
						PlageOccupation po = plageOccArray.objectAtIndex(j);

						otherSegmentArray.addObjectsFromArray(
								po.getSegmentArrayPourMemePlageTravailEtMemeJourEtMemeAbsence());

					}
				}

				// oter les debits qui ne sont pas sur le jour
				NSArray<A_Segment> otherPlageOccupationSegmentJourArray = EOQualifier.filteredArrayWithQualifier(
						otherSegmentArray, ERXQ.equals(A_Segment.JOUR_KEY, getJour()));

				if (otherPlageOccupationSegmentJourArray.count() > 0) {
					// allez hop au boulot
					_segmentArrayADebiter = getPlageOccupationSegmentLibreArray(
							getMinutesDebutJourDansPlageTravail(),
							getMinutesFinJourDansPlageTravail(),
							otherPlageOccupationSegmentJourArray);

				} else {

					// non trouvé
					PlageOccupationSegment segment = new PlageOccupationSegment(
							getMinutesDebutJourDansPlageTravail(), getMinutesFinJourDansPlageTravail(), this);
					_segmentArrayADebiter.add(segment);

				}
			} else {
				// débit déjà fait : on prend à partir de la prochaine minute
				A_Segment debitDernier = segmentArray.lastObject();
				PlageOccupationSegment segment = new PlageOccupationSegment(debitDernier.getDebut() + 1, getMinutesFinJourDansPlageTravail(), this);
				_segmentArrayADebiter.add(segment);
			}

		}

		return _segmentArrayADebiter;
	}

	/**
	 * La liste des transactions liées à cette plage parmi la liste des soldes
	 * disponibles
	 * 
	 * @return
	 */
	private NSArray<Transaction> getTransactionArraySurSoldeDisponibleForThis() {
		NSArray<Transaction> transactionArray = null;

		// soldes disponibles
		NSArray<I_Solde> eoSoldeForThis = getPlanning().getSoldeArray(
				null, getAbsence(), getCStructure(), getJour().getDate());

		// prendre les débits enregistrés
		transactionArray = NSArrayCtrl.flattenArray(
				(NSArray<Transaction>) eoSoldeForThis.valueForKeyPath(I_Solde.TRANSACTION_ARRAY_KEY));

		// ayant pour objet débiteur this
		transactionArray = EOQualifier.filteredArrayWithQualifier(
				transactionArray,
				ERXQ.equals(Transaction.OPERATION_KEY, this));

		// sur le même jour
		transactionArray = EOQualifier.filteredArrayWithQualifier(
					transactionArray,
					ERXQ.equals(Transaction.JOUR_KEY, getJour()));

		return transactionArray;
	}

	/**
	 * @return
	 */
	private NSArray<A_Segment> getSegmentArrayPourMemePlageTravailEtMemeJourEtMemeAbsence() {

		// // soldes disponibles
		// NSArray<I_Solde> eoSoldeForThis = getPlanning().getSoldeArray(
		// null, getAbsence(), getCStructure(), getJour().getDate());
		//
		// // prendre les débits enregistrés
		// transactionArray = NSArrayCtrl.flattenArray(
		// (NSArray<Transaction>)
		// eoSoldeForThis.valueForKeyPath(I_Solde.TRANSACTION_ARRAY_KEY));
		//
		// // ayant pour objet débiteur this
		// transactionArray = EOQualifier.filteredArrayWithQualifier(
		// transactionArray,
		// ERXQ.equals(Transaction.OPERATION_KEY, this));
		//
		// // sur le même jour
		// transactionArray = EOQualifier.filteredArrayWithQualifier(
		// transactionArray,
		// ERXQ.equals(Transaction.JOUR_KEY, getJour()));

		NSMutableArray<A_Segment> segmentArray = new NSMutableArray<A_Segment>();

		for (A_Absence absence : getJour().getAbsenceArray()) {

			for (PlageOccupation po : absence.getPlageOccupationArray()) {

				for (A_Segment segment : po.getSegmentArrayADebiter()) {

					if (segment instanceof PlageOccupationSegment) {

						PlageOccupationSegment plageOccupationSegment = (PlageOccupationSegment) segment;

						if (plageOccupationSegment.getPlageOccupation().getPlageTravail() == getPlageTravail() &&
								plageOccupationSegment.getPlageOccupation().getJour() == getJour() &&
								plageOccupationSegment.getPlageOccupation().getAbsence() == getAbsence()) {

							segmentArray.addObject(segment);

						}

					}

				}

			}

		}

		return segmentArray;

	}

	/**
	 * La liste des débits liés à cette plage parmi tous les soldes
	 * 
	 * @return
	 */
	private NSArray<Transaction> getTransactionArraySurToutSoldeForThis() {
		NSArray<Transaction> transactionArray = null;

		// soldes disponibles
		NSArray<EOSolde> eoSoldeForThis = getPlanning().getEoPlanning().tosSolde();

		// prendre les débits enregistrés
		transactionArray = NSArrayCtrl.flattenArray(
				(NSArray<Transaction>) eoSoldeForThis.valueForKeyPath(I_Solde.TRANSACTION_ARRAY_KEY));

		// ayant pour objet débiteur this
		transactionArray = EOQualifier.filteredArrayWithQualifier(
				transactionArray,
				ERXQ.equals(Transaction.OPERATION_KEY, this));

		return transactionArray;
	}

	/**
	 * Gestion du débit sur plusieurs soldes
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	private class DebitRoulantTransfertPause
			extends A_DebitRoulant {

		/**
		 * @param eoSoldeArray
		 * @param operation
		 * @param aDebiter
		 */
		public DebitRoulantTransfertPause(
				I_Solde soldeConges, TransfertDisparitionPauseRtt operation, int aDebiter) {
			super(new NSArray<I_Solde>(soldeConges), operation, aDebiter);
		}

	}

	/**
	 * Réserver les objets {@link PlageOccupationSegment}
	 */
	public final void preparerSegments() {
		getSegmentArrayADebiter();

		//
		//
		// System.out.print("preparerSegments()");
		//
		// for (PlageOccupationSegment segment : getSegmentArrayADebiter()) {
		// System.out.println("\n\tsegment=" + segment);
		// }

	}

	/**
	 * Effectuer les opérations préalables pour que les débits et crédits puissent
	 * se faire.
	 */
	public final void appliquerCreditDebit() {

		// soldes disponibles par priorité
		NSArray<I_Solde> soldeArray = getPlanning().getSoldeArray(
				null, getAbsence(), getCStructure(), getJour().getDate());

		NSArray<A_Segment> segmentArray = getSegmentArrayADebiter();

		// System.out.print("appliquerCreditDebit()");

		for (int i = 0; i < segmentArray.count(); i++) {

			A_Segment segment = segmentArray.objectAtIndex(i);

			// System.out.println("\n\tsegment=" + segment);

			if (getAbsence().isPresence()) {
				// crédit
				// il n'y a qu'un solde : balance HSUP/CC pour les heures
				// supplémentaires et droit à congés pour les astreintes
				I_Solde soldeCredit = soldeArray.objectAtIndex(0);
				new Transaction(
						soldeCredit, this, segment);

			} else {

				// débit roulant
				new DebitRoulantPlageOccupation(
						soldeArray, this, segment.getDebut(), segment.getFin());
			}

		}
	}

	/**
	 * Traitement des pauses méridiennes pour les multi-service. Cette méthode est
	 * appelée une seule fois par jour.
	 */

	public final void preTraitementCreditDebitPauseMultiService() {

		if (getJour().getIsPreTraitementCreditPauseMultiServiceEffectue()) {
			return;
		}

		// System.out.println("preTraitementCreditDebitPauseMultiService() jour : "
		// + getJour());

		if (getPlageTravail().getHoraireJournalier().isMultiService() &&
				getPlageTravail().getHoraireJournalier().getHorairePlagePause() != null) {

			// variations DCI si la pause est supprimée
			if (getJour().isPauseRTTAccordable() &&
					getJour().isPauseRTTAccordee() == false) {
				traitementMultiservicePauseNonAccordee();
			}

			// répartition du débit du pause si conservée mais partiellement
			// ou complètement occultée par une absence
			if (getJour().isPauseRTTAccordee()) {
				traitementMultiservicePauseAccordee();
			}

			getJour().setIsPreTraitmentCreditPauseMultiServiceEffectue(true);
		}
	}

	/**
	 * 
	 */
	private void traitementMultiservicePauseNonAccordee() {

		// System.out.println("traitementMultiservicePauseNonAccordee() jour : " +
		// getJour());

		// variation DCI : augmentation pour celui qui a la pause, diminution
		// pour l'autre
		NSMutableDictionary<String, Double> dicoPourcentage = getPlageTravail().getHoraireJournalier().getPourcentageServiceDico();

		// trouver le cStructure du service B
		String cStructureB = null;
		if (dicoPourcentage.allKeys().objectAtIndex(0).equals(getPlageTravail().getCStructure())) {
			cStructureB = dicoPourcentage.allKeys().objectAtIndex(1);
		} else {
			cStructureB = dicoPourcentage.allKeys().objectAtIndex(0);
		}

		double pourcentageB = dicoPourcentage.objectForKey(cStructureB).floatValue();
		double pourcentageA = (float) 1 - pourcentageB;
		int valeurPauseB = MiscCtrl.arrondi(((double) Horaire.PAUSE_RTT_DUREE) * pourcentageB);
		int valeurPauseA = Horaire.PAUSE_RTT_DUREE - valeurPauseB;

		EOSolde eoSoldeCongeA = getPlanning().getEoPlanning().getSoldeCongeNatif(getCStructure());
		EOSolde eoSoldeCongeB = getPlanning().getEoPlanning().getSoldeCongeNatif(cStructureB);

		EOSolde soldeACrediter = null;
		EOSolde soldeADebiter = null;
		double pourcentage = (double) 0;
		int valeurPause = 0;

		// tout dépend ou est placée la pause
		if (getPlageTravail().getHorairePlagePause() != null) {
			// la pause est dans cette plage

			// crédit sur A de la valeur de B (et debit de B)

			soldeACrediter = eoSoldeCongeA;
			soldeADebiter = eoSoldeCongeB;
			pourcentage = pourcentageB;
			valeurPause = valeurPauseB;

		} else {
			// la pause est dans l'autre plage

			// crédit sur B de la valeur de A (et debit de A)

			soldeACrediter = eoSoldeCongeB;
			soldeADebiter = eoSoldeCongeA;
			pourcentage = pourcentageA;
			valeurPause = valeurPauseA;

		}

		// credit
		TransfertDisparitionPauseRtt transfertPause = new TransfertDisparitionPauseRtt(this, pourcentage);
		new Transaction(soldeACrediter, transfertPause, -valeurPause);

		// débit
		new DebitRoulantTransfertPause(soldeADebiter, transfertPause, valeurPause);

	}

	/**
	 * 
	 */
	private void traitementMultiservicePauseAccordee() {

		// System.out.println("traitementMultiservicePauseAccordee() jour : " +
		// getJour());

		// editer les segments de plage concernés
		NSArray<PlageOccupation> plageOccArray = new NSArray<PlageOccupation>();
		for (HorairePlageTravail htp : getJour().getPlageTravailArray()) {
			plageOccArray = plageOccArray.arrayByAddingObjectsFromArray(htp.getPlageOccupationArrayForJour(getJour()));
		}

		for (PlageOccupation po : plageOccArray) {
			// System.out.println("\tpo = " + po);

			for (A_Segment segment : po.getSegmentArrayADebiter()) {
				// System.out.println("\t\tsegment=" + segment);

				if (segment.chevauche(getPlageTravail().getHoraireJournalier().getHorairePlagePause())) {

					// réduire le segment et en créer un autre pour l'autre
					// service en complément de la durée initiale totale

					NSMutableDictionary<String, Double> dicoPourcentage = getPlageTravail().getHoraireJournalier().getPourcentageServiceDico();

					// trouver le cStructure du service B
					String cStructureB = null;
					if (dicoPourcentage.allKeys().objectAtIndex(0).equals(getPlageTravail().getCStructure())) {
						cStructureB = dicoPourcentage.allKeys().objectAtIndex(1);
					} else {
						cStructureB = dicoPourcentage.allKeys().objectAtIndex(0);
					}

					// les soldes disponibles sur le service B
					// NSArray<I_Solde> soldeBArray = getPlanning().getSoldeArray(null,
					// getAbsence(), cStructureB, getJour().getDate());

					// XXX ça sert à quoi cet appel ???
					getPlanning().getSoldeArray(null, getAbsence(), getCStructure(), getJour().getDate());

					int debutPause = getPlageTravail().getHoraireJournalier().getHorairePlagePause().getDebutMinutesDansJour();
					int finPause = getPlageTravail().getHoraireJournalier().getHorairePlagePause().getFinMinutesDansJour();

					int debutSegment = segment.getDebut() % Jour.DUREE_JOURNEE_EN_MINUTES;
					int finSegment = segment.getFin() % Jour.DUREE_JOURNEE_EN_MINUTES;

					double pourcentA = dicoPourcentage.objectForKey(getPlageTravail().getCStructure()).floatValue();
					double pourcentB = 1.0 - pourcentA;

					// la pause est inclue dans le segment
					if (debutSegment <= debutPause && finSegment >= finPause) {

						// System.out.println("modification du segment : " + segment +
						// " (pause inclue dans le segment)");

						// 1 segment devient 2
						// avant-apres => avant-finPauseA | debutPauseB-apres
						int finPauseA = debutPause + MiscCtrl.arrondi(((double) Horaire.PAUSE_RTT_DUREE) * pourcentA);
						int debutPauseB = finPauseA;

						// reduction du premier segment par l'avant
						segment.setFin(finPauseA);
						// System.out.println("\tapres mise à jour : " + segment);

						// segment pour le restant de la pause sur le service B
						PlageOccupation newPo = creerPlageOccupationComplement(debutPauseB, finSegment, cStructureB);

						// System.out.println("\t\tnouveau plageOccupation " + newPo);

					} else {

						// chevauche par l'arrière ou par l'avant

						// 1 segment devient 2
						if (debutSegment <= debutPause) {

							// System.out.println("modification du segment : " + segment +
							// " (chevauchement par l'arriere)");

							// avant-pauseA | pauseA-pauseB
							int finPauseA = debutPause + (int) ((float) (finSegment - debutPause) * pourcentA);
							int debutPauseB = finPauseA;

							// reduction premier segment par l'avant
							segment.setFin(finPauseA);
							// System.out.println("\tapres mise à jour : " + segment);

							// CktlLog.log("ancien débit A " + debitRoulant);
							// creation débit suivant pour le restant de la pause sur
							// le service B

							// segment pour le restant de la pause sur le service B
							PlageOccupation newPo = creerPlageOccupationComplement(debutPauseB, finSegment, cStructureB);
							// System.out.println("\t\tnouveau plageOccupation " + newPo);

						} else {

							// System.out.println("modification du segment : " + segment +
							// " (chevauchement par l'avant)");

							// pauseB-pauseA | pauseA-apres

							int finPauseB = debutSegment + (int) ((float) (finPause - debutSegment) * pourcentB);
							int debutPauseA = finPauseB;

							// reduction premier segment par l'arriere
							segment.setDebut(debutPauseA);
							// System.out.println("\tapres mise à jour : " + segment);

							// CktlLog.log("ancien débit A " + debitRoulant);
							// creation débit précédent pour le restant de la pause sur
							// le service B
							PlageOccupation newPo = creerPlageOccupationComplement(finPauseB, debutSegment, cStructureB);
							// System.out.println("\t\tnouveau plageOccupation " + newPo);

						}

					}

				}

			}

		}

	}

	/**
	 * Méthode interne pour la création de segment sur l'autre service pour la
	 * gestion de la pause multi service (on créer un plage de travail fictive
	 * pour répondre à ce besoin)
	 * 
	 * @param debut
	 * @param fin
	 * @param cStructure
	 */
	private final PlageOccupation creerPlageOccupationComplement(int debut, int fin, String cStructure) {

		// interdire si la structure est la même
		if (cStructure.equals(getCStructure())) {
			throw new IllegalAccessError("Impossible de créer une plage fictive pour la même structure");
		}

		// // segment sur l'autre service : il faut créer une plage de travail
		// fictive
		// HorairePlageTravail hptFictiveB = new HorairePlageTravail(
		// getPlageTravail().getHoraire(),
		// HoraireFieldCtrl.createTriplet(debut, fin, cStructure),
		// getPlageTravail().isAm());
		//
		// PlageOccupation newPo = new PlageOccupation(getJour(), getAbsence(),
		// hptFictiveB);
		//
		// // on force le segment
		// newPo._segmentArrayADebiter = new NSMutableArray<PlageOccupationSegment>(
		// new PlageOccupationSegment(debut, fin, newPo));

		PlageOccupation newPo = new PlageOccupation(
				getJour(), getAbsence(),
				new ServiceSegment(debut, fin, cStructure, getJour()));

		// on force le segment
		newPo._segmentArrayADebiter = new NSMutableArray<A_Segment>(
				new ServiceSegment(debut, fin, cStructure, getJour()));

		getAbsence().addPlageOccupation(newPo);

		return newPo;
	}

	/**
	 * Détermine le début de l'absence au sein de la {@link HorairePlageTravail}
	 * affectée. Si la pause est accolée, alors il faut la prendre en compte.
	 */
	private final int getMinutesDebutJourDansPlageTravail() {
		int debutDansPlage = 0;

		if (getPlageTravail() != null) {

			int minutesDebut = getJour().minutesDebutPourJour(getAbsence());

			debutDansPlage = getPlageTravail().getDebutMinutesDansSemaine();
			if (minutesDebut > debutDansPlage) {
				debutDansPlage = minutesDebut;
			}

		} else {
			// heure supp .
			debutDansPlage = getJour().minutesDebutPourJour(getAbsence());

		}

		// ramener au jour
		debutDansPlage = debutDansPlage % Jour.DUREE_JOURNEE_EN_MINUTES;

		return debutDansPlage;
	}

	/**
	 * 
	 */
	private final int getMinutesFinJourDansPlageTravail() {
		int finDansPlage = 0;

		if (getPlageTravail() != null) {

			int minutesFin = getJour().minutesFinPourJour(getAbsence());

			finDansPlage = getPlageTravail().getFinMinutesDansSemaine();
			if (minutesFin < finDansPlage) {
				finDansPlage = minutesFin;
			}

		} else {
			// heures supp.
			finDansPlage = getJour().minutesFinPourJour(getAbsence());

		}

		// ramener au jour
		finDansPlage = finDansPlage % Jour.DUREE_JOURNEE_EN_MINUTES;

		return finDansPlage;
	}

	/**
	 * Filtrer par la nature du solde
	 * 
	 * @param attributeToBeTrue
	 * @return
	 */
	public final int getDebitSurSoldeMatching(String attributeToBeTrue) {
		int minutes = 0;

		NSArray<Transaction> transactionArray = getTransactionArraySurSoldeDisponibleForThis();

		if (!StringCtrl.isEmpty(attributeToBeTrue)) {
			transactionArray = EOQualifier.filteredArrayWithQualifier(
					transactionArray, ERXQ.isTrue(
							Transaction.SOLDE_KEY + "." + attributeToBeTrue));
		}

		for (int i = 0; i < transactionArray.count(); i++) {
			Transaction transaction = transactionArray.objectAtIndex(i);
			minutes += transaction.getMinutesTotal();
		}

		return minutes;
	}

	/**
	 * Filtrer par l'objet {@link I_Solde}
	 * 
	 * @param attributeToBeTrue
	 * @return
	 */
	public final int getDebitSurSolde(I_Solde solde) {
		int minutes = 0;

		NSArray<Transaction> transactionArray = getTransactionArraySurToutSoldeForThis();

		for (int i = 0; i < transactionArray.count(); i++) {
			Transaction transaction = transactionArray.objectAtIndex(i);
			if (solde == null ||
					transaction.getSolde() == solde) {
				minutes += transaction.getMinutesTotal();
			}
		}

		return minutes;
	}

	/**
	 * 
	 */
	public final Jour getJour() {
		return jour;
	}

	/**
	 * 
	 */
	public final A_Absence getAbsence() {
		return absence;
	}

	/**
	 * Le service concerné
	 * 
	 * @return
	 */
	private final String getCStructure() {
		String cStructure = null;

		if (getPlageTravail() != null) {
			cStructure = getPlageTravail().getCStructure();
		} else if (getAbsence().isPresence()) {
			cStructure = getAbsence().toStructureApplication().cStructure();
		} else if (serviceSegment != null) {
			cStructure = serviceSegment.getCStructure();
		}

		return cStructure;
	}

	/**
	 * 
	 */
	public final HorairePlageTravail getPlageTravail() {
		return plageTravail;
	}

	private OperationDelegate _operationDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, getAbsence());
		}
		return _operationDelegate;
	}

	/**
	 * @param jour
	 * @return
	 */
	public int dureeCumuleeSegments(Jour jour) {

		int duree = 0;

		if (getJour() == jour) {

			for (A_Segment segment : getSegmentArrayADebiter()) {

				duree = duree - (segment.getFin() - segment.getDebut());

			}

		}

		return duree;

	}

	// XXX tmp pour calcul des bonificatinos

	/**
	 * @return
	 */
	public NSArray<A_Segment> getDispo() {
		NSArray<A_Segment> array = null;

		array = getPlageOccupationSegmentLibreArray(
				getPlageTravail().getDebutMinutesDansJour(),
				getPlageTravail().getFinMinutesDansJour(),
				getSegmentArrayADebiter());

		return array;
	}

}
