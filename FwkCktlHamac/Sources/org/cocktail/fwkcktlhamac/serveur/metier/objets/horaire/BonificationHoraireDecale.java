/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class BonificationHoraireDecale
		extends A_BonificationHoraire {

	private NSTimestamp dateJour;

	/**
	 * 
	 */
	public BonificationHoraireDecale(NSTimestamp dateJour, int minutes) {
		super(minutes);
		this.dateJour = dateJour;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_BonificationHoraire#
	 * libelleBonification()
	 */
	@Override
	public String libelleBonification() {
		return "décalé " + DateCtrlHamac.dateToString(dateJour);
	}

}
