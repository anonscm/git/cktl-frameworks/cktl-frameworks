/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets.cet;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class TransfertRafp
		extends A_CetTransactionAnnuelleDebit {

	/**
	 * @param eoCetTransactionAnnuelle
	 */
	public TransfertRafp(EOCetTransactionAnnuelle eoCetTransactionAnnuelle, int valeur) {
		super(eoCetTransactionAnnuelle, valeur);
	}

	@Override
	public String libelle() {
		return "Transfert CET vers RAFP (au " + DateCtrlHamac.dateToString(getEoCetTransactionAnnuelle().dateValeur()) + ")";
	}

}
