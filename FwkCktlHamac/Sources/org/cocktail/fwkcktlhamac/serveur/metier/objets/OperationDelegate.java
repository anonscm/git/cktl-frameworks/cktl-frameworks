/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.objets;

import java.math.BigDecimal;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetVidage;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.A_CetTransactionAnnuelle;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class OperationDelegate {

	private I_Operation operation;
	private I_Operation operationParente;

	public final static String TRANSACTION_ARRAY_KEY = "transactionArray";

	public final static String CLEAR_CACHE_KEY = "clearCache";
	public final static String D_DEB_KEY = "dDeb";

	// cache
	private NSMutableDictionary<I_Solde, Integer> _soldeRestantApresTransactionDico;
	private NSMutableArray<Transaction> _transactionArray;

	/**
	 * @param operation
	 */
	public OperationDelegate(I_Operation operation, I_Operation operationParente) {
		super();
		this.operation = operation;
		this.operationParente = operationParente;
	}

	/**
	 * Nettoyage de toutes les valeurs du cache
	 */
	public void clearCache() {
		_soldeRestantApresTransactionDico = null;
		_transactionArray = null;

		// System.out.println("OperationDelegate.clearCache() " +
		// operation.toString());

	}

	/**
	 * @return
	 */
	public final I_Operation getOperation() {
		return operation;
	}

	/**
	 * L'opération parente si elle existe (par exemple, une plage occupation
	 * remonte jusqu'à l'absence)
	 */
	public I_Operation getOperationParente() {
		return operationParente;
	}

	/**
	 * la date de début / commencement de l'opération (uniquement pour un pseudo
	 * affichage chronologique)
	 */
	public NSTimestamp dDeb() {
		NSTimestamp dDeb = null;

		if (getOperationParente() instanceof A_Absence) {
			// cas d'une occupation
			dDeb = ((A_Absence) getOperationParente()).dateDebut();
		} else if (getOperation() instanceof I_Solde) {
			// cas d'un solde
			I_Solde solde = (I_Solde) getOperation();
			// cas particulier du bilan qui se fait en fin d'année
			if (solde.isBalance()) {
				dDeb = solde.dFin();
			} else {
				dDeb = solde.dDeb();
			}
		} else if (getOperation() instanceof A_CetTransactionAnnuelle) {
			// cet : en début d'année civile
			dDeb = ((A_CetTransactionAnnuelle) getOperation()).getEoCetTransactionAnnuelle().dateValeur();
		} else if (getOperation() instanceof EOCetVidage) {
			// vidage : date de fin de période
			dDeb = ((EOCetVidage) getOperation()).toPlanning().toPeriode().perDFin();
		} else if (getOperation() instanceof EOSoldePoursuite) {
			// poursuite de solde : disparition du solde source a j+1
			dDeb = ((EOSoldePoursuite) getOperation()).toSoldeSrc().dateDisparition().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		}

		return dDeb;
	}

	/**
	 * 
	 * @return
	 */
	public final NSMutableDictionary<I_Solde, Integer> getSoldeRestantApresTransactionDico() {
		if (_soldeRestantApresTransactionDico == null) {
			_soldeRestantApresTransactionDico = new NSMutableDictionary<I_Solde, Integer>();
		}
		return _soldeRestantApresTransactionDico;
	}

	/**
	 * @param transaction
	 */
	public void notifierNouvelleTransaction(Transaction transaction) {

		// notification locale
		getTransactionArray().addObject(transaction);

		// seule l'opération parente est autorisée à notifier le solde lié à la
		// transaction
		if (getOperation() != getOperationParente()) {
			getOperationParente().getOperationDelegate().notifierNouvelleTransaction(transaction);
		} else {
			// notification vers le solde associé
			transaction.getSolde().getSoldeDelegate().notifierNouvelleTransaction(transaction);
		}

	}

	/**
	 * Notification suite à la consommation de la transaction
	 * 
	 * @param transaction
	 */
	public void notifierConsommationTransaction(Transaction transaction) {

		if (getOperation() == getOperationParente()) {

			// mémoriser solde restant
			I_Solde solde = transaction.getSolde();
			getSoldeRestantApresTransactionDico().setObjectForKey(solde.getSoldeDelegate().restant(), solde);
		} else {

			// ne notifier que l'opération parente
			getOperationParente().getOperationDelegate().notifierConsommationTransaction(transaction);

		}
	}

	/**
	 * Liste des sous-objets {@link Transaction} associés
	 * 
	 * @return
	 */
	public NSMutableArray<Transaction> getTransactionArray() {
		if (_transactionArray == null) {
			_transactionArray = new NSMutableArray<Transaction>();
		}
		return _transactionArray;
	}

	/**
	 * Utilisé par l'écran d'affichage des operations
	 * 
	 * @return
	 */
	public NSMutableArray<I_Solde> getSoldeArray() {
		NSMutableArray<I_Solde> array = new NSMutableArray<I_Solde>();

		for (int i = 0; i < getTransactionArray().count(); i++) {
			Transaction transaction = getTransactionArray().objectAtIndex(i);

			I_Solde solde = transaction.getSolde();

			if (!array.containsObject(solde)) {
				array.addObject(solde);
			}

		}
		return array;
	}

	/**
	 * @param unSolde
	 * @return
	 */
	public final Integer getSoldeApresDebit(I_Solde unSolde) {
		Integer val = null;

		NSArray<I_Solde> soldeArray = getSoldeRestantApresTransactionDico().allKeys();
		for (I_Solde solde : soldeArray) {
			if (solde == unSolde) {
				val = getSoldeRestantApresTransactionDico().objectForKey(solde);
				// TODO virer ce break et faire une boucle propre
				break;
			}
		}

		return val;
	}

	/**
	 * 
	 * @param solde
	 * @return
	 */
	public final Integer getDebitSurSolde(I_Solde solde) {
		int val = 0;

		NSArray<Transaction> transactionArray = getTransactionArray();
		for (Transaction transaction : transactionArray) {
			if (transaction.getSolde() == solde &&
					transaction.getOperation().getOperationDelegate().getOperationParente() == getOperation().getOperationDelegate().getOperationParente()) {
				val += transaction.getMinutesTotal();
			}
		}

		return Integer.valueOf(val);
	}

	/**
	 * 
	 * @return
	 */
	public final Integer getDebitTotal() {
		BigDecimal val = null;

		NSArray<Transaction> transactionArray = getTransactionArray();
		val = (BigDecimal) transactionArray.valueForKey("@sum." + Transaction.MINUTES_TOTAL_KEY);

		Integer valInt = Integer.valueOf(val.intValue());

		return valInt;
	}

	/**
	 * @see #getDebitSurSolde(I_Solde)
	 * @return
	 */
	@Deprecated
	public final Integer getSoldeReliquatApresDebit() {
		Integer val = null;

		NSArray<I_Solde> eoSoldeArray = getSoldeRestantApresTransactionDico().allKeys();

		for (int i = 0; i < eoSoldeArray.count() && val == null; i++) {
			I_Solde eoSolde = (I_Solde) eoSoldeArray.objectAtIndex(i);
			if (eoSolde.isReliquatNatif()) {
				val = eoSolde.getSoldeDelegate().restantApresOperation(getOperation());
			}
		}

		return val;
	}

	/**
	 * @see #getDebitSurSolde(I_Solde)
	 * @return
	 */
	@Deprecated
	public final Integer getSoldeCongeApresDebit() {
		Integer val = null;

		NSArray<I_Solde> eoSoldeArray = getSoldeRestantApresTransactionDico().allKeys();

		for (int i = 0; i < eoSoldeArray.count() && val == null; i++) {
			I_Solde eoSolde = (I_Solde) eoSoldeArray.objectAtIndex(i);
			if (eoSolde.isCongeNatif()) {
				val = eoSolde.getSoldeDelegate().restantApresOperation(getOperation());
			}
		}

		return val;
	}

	/**
	 * @see #getDebitSurSolde(I_Solde)
	 * @return
	 */
	@Deprecated
	public final Integer getSoldeBalanceApresDebit() {
		Integer val = null;

		NSArray<I_Solde> eoSoldeArray = getSoldeRestantApresTransactionDico().allKeys();
		for (int i = 0; i < eoSoldeArray.count() && val == null; i++) {
			I_Solde eoSolde = (I_Solde) eoSoldeArray.objectAtIndex(i);
			if (eoSolde.isBalance()) {
				val = eoSolde.getSoldeDelegate().restantApresOperation(getOperation());
			}
		}

		return val;
	}

	/**
	 * @see #getDebitSurSolde(I_Solde)
	 * @return
	 */
	@Deprecated
	public final Integer getSoldeRegularisationApresDebit() {
		Integer val = null;

		NSArray<I_Solde> eoSoldeArray = getSoldeRestantApresTransactionDico().allKeys();
		for (int i = 0; i < eoSoldeArray.count() && val == null; i++) {
			I_Solde eoSolde = (I_Solde) eoSoldeArray.objectAtIndex(i);
			if (eoSolde.isRegularisation()) {
				val = eoSolde.getSoldeDelegate().restantApresOperation(getOperation());
			}
		}

		return val;
	}
}
