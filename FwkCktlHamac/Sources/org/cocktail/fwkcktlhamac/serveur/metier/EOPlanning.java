/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.CheckerEOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.DemandeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Presence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.Cet;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowAbsenceSaisie;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowPlanningAnnulerDemandeModification;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowPlanningAnnulerDemandeValidation;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowPlanningAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowPlanningDemandeModification;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.WorkflowPlanningDemandeValidation;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOPlanning
		extends _EOPlanning
		implements I_Demande, I_GestionAutoDCreationDModification {

	public EOPlanning() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	// planning réel
	public final static String REEL = "R";
	// plannning prévisionnel. Initialisé en même temps que le premier réel. N'est
	// plus jamais modifié ensuite.
	public final static String PREV = "P";
	// planning test
	public final static String TEST = "T";
	// planning inter-validation ; RAZ lors de la validation du planning. Pour
	// faire les comparaison avant / après modification, utilisé par le
	// responsable.
	public final static String INTR = "I";
	// planning historique ; derniere valeurs du planning réel validé s'il a
	// existé. est vide si le réel n'a jamais été validé. utilisé pour savoir s'il
	// faut gérer le planning prévisionnel ou non
	public final static String HIST = "H";
	// planning temporaire pour simulation du connecteur. Est bloqué à la
	// sauvegarde car la valeur est interdite dans la base de données
	public final static String CONN = "C";
	// planning temporaire pour simulation du cet
	public final static String SIMU = "S";

	private final static String STR_SORT_NOM_ET_DATE = "toPersonne." + IPersonne.NOM_PRENOM_COMPLET_AFFICHAGE_KEY + "," + TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY;

	/** les codes des types de plannings "imprimables" */
	public final static NSArray<String> TYPES_PLANNINGS_IMPRIMABLES = new NSArray<String>(new String[] { REEL, PREV });

	private Planning _planning;
	private Cet _cet;
	private EOPlanning _eoPlanningPrecedent;
	private NSArray<EOOccupation> _eoOccupationAnnuelleArray;
	private EOTypeSequenceValidation _toTypeSequenceValidation;
	private DemandeDelegate _demandeDelegate;
	private String _toString;

	private EOPlanning _eoPlanningReel, _eoPlanningTest, _eoPlanningHist, _eoPlanningCetS, _eoPlanningPrev = null;

	private Presence _presence;

	
	private boolean isOngletsVisibles = true;
	

	public Presence presence() {
		if (_presence == null) {
			_presence = new Presence(this);
		}
		return _presence;
	}

	public void clearCache() {

		if (_planning != null) {

			_planning.clearCache();
			_planning = null;

		}

		_cet = null;
		_eoPlanningPrecedent = null;
		_eoOccupationAnnuelleArray = null;
		_eoSourceArray = null;
		_toTypeSequenceValidation = null;
		_demandeDelegate = null;
		_toString = null;
		_eoPlanningReel = null;
		_eoPlanningTest = null;
		_eoPlanningHist = null;
		_eoPlanningCetS = null;

		_presence = null;
	}

	/**
	 * Chargement automatique de l'objet {@link Planning} associé
	 */
	public Planning getPlanning() {
		if (!isPlanningCompletCharge()) {
			_planning = new Planning(this);
			_planning.charger();
		}
		return _planning;
	}

	/**
	 * Savoir si le planning "complet" (pas le planning "rapide") a été monté en
	 * mémoire
	 * 
	 * @return
	 */
	public boolean isPlanningCompletCharge() {
		boolean isPlanningCharge = false;

		if (_planning != null) {
			isPlanningCharge = true;
		}

		return isPlanningCharge;
	}

	/**
	 * Setter bidon pour non plantage des interfaces
	 * 
	 * @param planning
	 */
	public void setPlanning(Planning planning) {

	}

	/**
	 * @return
	 */
	public NSArray<EOOccupation> getEoOccupationAnnuleeArray() {
		if (_eoOccupationAnnuelleArray == null) {

			EOQualifier qual = ERXQ.and(
							ERXQ.equals(EOAnnulation.PERS_ID_KEY, persId()),
							ERXQ.equals(EOAnnulation.TEM_PERSONNE_COMPOSANTE_KEY, temPersonneComposante()));

			NSArray<EOAnnulation> eoAnnulationArray = EOAnnulation.fetchAll(
					editingContext(), qual);

			_eoOccupationAnnuelleArray = (NSArray<EOOccupation>) eoAnnulationArray.valueForKey(EOAnnulation.TO_OCCUPATION_KEY);

		}

		return _eoOccupationAnnuelleArray;
	}

	/**
	 * La liste des occupations associées (via le persId)
	 * 
	 * @return
	 */
	public final NSArray<EOOccupation> getEoOccupationDirecte() {
		NSMutableArray<EOOccupation> result = new NSMutableArray<EOOccupation>();

		result.addObjectsFromArray(findOccupation());

		// oter les annulations
		result.removeObjectsInArray(getEoOccupationAnnuleeArray());

		return result;
	}

	/**
	 * La liste des occupation par héritage
	 * 
	 * @return
	 */
	private final NSArray<EOOccupation> tosOccupationHeritee(
												NSArray<TravailAttendu> travailAttenduArray) {
		NSMutableArray<EOOccupation> result = null;

		result = new NSMutableArray<EOOccupation>(
					EOOccupation.getEoOccupationHeritee(
							this, travailAttenduArray));

		result.removeObjectsInArray(getEoOccupationAnnuleeArray());

		// TODO enlever les occupations qui ne chevauchent pas le planning pour le
		// service

		return result;
	}

	/**
	 * Obtenir le ou les soldes répondant aux critères
	 * 
	 * @param cStructure
	 * @param typeSolde
	 * @return
	 */
	private NSArray<EOSolde> tosSolde(
											String cStructure, String typeSolde) {
		NSArray<EOSolde> result = null;

		NSMutableArray<EOQualifier> qualArray = new NSMutableArray<EOQualifier>();

		qualArray.addObject(ERXQ.equals(EOSolde.TO_PLANNING_KEY, this));

		if (cStructure != null) {
			qualArray.addObject(
						ERXQ.equals(EOSolde.TO_STRUCTURE_KEY + "." + EOStructure.C_STRUCTURE_KEY, cStructure));
		} else {
			qualArray.addObject(
						ERXQ.isNull(EOSolde.TO_STRUCTURE_KEY));
		}

		if (!StringCtrl.isEmpty(typeSolde)) {
			qualArray.addObject(
						ERXQ.equals(EOSolde.TO_TYPE_SOLDE_KEY + "." + EOTypeSolde.STYP_CODE_KEY, typeSolde));
		}

		EOQualifier qual = new EOAndQualifier(qualArray);

		result = tosSolde(qual);

		return result;
	}

	/**
	 * Donne le solde associé un planning selon sa nature et le type de solde
	 * 
	 * @return
	 */
	public EOSolde getEoSolde(
											String cStructure, String typeSolde, NSTimestamp dateDebutValidite, NSTimestamp dateFinValidite) {
		EOSolde eoSolde = null;

		NSArray<EOSolde> array = tosSolde(cStructure, typeSolde);

		if (dateDebutValidite != null && dateFinValidite != null) {
			EOQualifier qual = ERXQ.and(
						ERXQ.equals(EOSolde.D_DEB_VALIDITE_KEY, dateDebutValidite),
						ERXQ.equals(EOSolde.D_FIN_VALIDITE_KEY, dateFinValidite));

			array = EOQualifier.filteredArrayWithQualifier(array, qual);
		}

		if (array.count() > 0) {
			eoSolde = array.objectAtIndex(0);
		}

		return eoSolde;
	}

	/**
	 * Donne les soldes associés un planning selon sa nature et le type de solde
	 * 
	 * @return
	 */
	public NSArray<EOSolde> getEoSoldeArray(
											String cStructure, String typeSolde, NSTimestamp dateDebutValidite, NSTimestamp dateFinValidite) {
		NSArray<EOSolde> array = tosSolde(cStructure, typeSolde);

		if (dateDebutValidite != null && dateFinValidite != null) {
			EOQualifier qual = ERXQ.and(
						ERXQ.equals(EOSolde.D_DEB_VALIDITE_KEY, dateDebutValidite),
						ERXQ.equals(EOSolde.D_FIN_VALIDITE_KEY, dateFinValidite));

			array = EOQualifier.filteredArrayWithQualifier(array, qual);
		}

		return array;
	}

	/**
	 * Liste des soldes disponibles à une date par ordre de priorité
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public NSArray<EOSolde> getEoSoldeArrayForDate(
											String cStructure, NSTimestamp date) {
		NSArray<EOSolde> array = tosSolde(cStructure, null);

		EOQualifier qual = ERXQ.and(
					ERXQ.lessThanOrEqualTo(EOSolde.D_DEB_VALIDITE_KEY, date),
					ERXQ.greaterThanOrEqualTo(EOSolde.D_FIN_VALIDITE_KEY, date));

		array = EOQualifier.filteredArrayWithQualifier(array, qual);

		return array;
	}

	/**
	 * Le solde de congés natif
	 * 
	 * @return
	 */
	public EOSolde getSoldeCongeNatif(String cStructure) {
		EOSolde eoSoldeCongeNatif = null;

		eoSoldeCongeNatif = getEoSolde(
												cStructure, EOTypeSolde.CONGE_NATIF, null, null);

		return eoSoldeCongeNatif;
	}

	/**
	 * Le solde de reliquat natif
	 * 
	 * @return
	 */
	public EOSolde getSoldeReliquatNatif(String cStructure) {
		EOSolde eoSoldeReliquatNatif = null;

		eoSoldeReliquatNatif = getEoSolde(
												cStructure, EOTypeSolde.RELIQUAT_NATIF, null, null);

		return eoSoldeReliquatNatif;
	}

	/**
	 * La balance heure supp / congés compensateurs
	 * 
	 * @return
	 */
	public EOSolde getSoldeBalanceNatif(String cStructure) {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												cStructure, EOTypeSolde.BALANCE, null, null);

		return eoSolde;
	}

	/**
	 * Le reliquat heures supp
	 * 
	 * @return
	 */
	public EOSolde getSoldeReliquatHsup(String cStructure) {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												cStructure, EOTypeSolde.RELIQUAT_HSUP, null, null);

		return eoSolde;
	}
	
	/**
	 * Le solde des congés légaux
	 * 
	 * @return
	 */
	public EOSolde getSoldeCongeLegal() {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												null, EOTypeSolde.CONGE_LEGAL, null, null);

		return eoSolde;
	}

	/**
	 * Le solde des congés RH
	 * 
	 * @return
	 */
	public EOSolde getSoldeCongeRh() {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												null, EOTypeSolde.CONGE_RH, null, null);

		return eoSolde;
	}

	/**
	 * Le solde JRTI
	 * 
	 * @return
	 */
	public EOSolde getSoldeJrti(String cStructure) {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												cStructure, EOTypeSolde.JRTI, null, null);

		return eoSolde;
	}

	/**
	 * Le solde indemnisation sur cet decret 2008-1136
	 * 
	 * @return
	 */
	public EOSolde getSoldeIndemCet20081136() {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												null, EOTypeSolde.INDEM_CET_2008_1136, null, null);

		return eoSolde;
	}

	/**
	 * Le solde des congés RH
	 * 
	 * @return
	 */
	public EOSolde getSoldeDechargeSyndicale() {
		EOSolde eoSolde = null;

		eoSolde = getEoSolde(
												null, EOTypeSolde.DECH_SYND, null, null);

		return eoSolde;
	}

	/**
	 * La balance heure supp / congés compensateurs
	 * 
	 * @return
	 */

	public NSArray<EOSolde> getSoldeRegularisationArray(String cStructure) {
		NSArray<EOSolde> eoSoldeArray = null;

		eoSoldeArray = getEoSoldeArray(
											cStructure, EOTypeSolde.REGULARISATION, null, null);

		return eoSoldeArray;
	}

	/**
	 * Le solde de congés du dernier planning
	 * 
	 * FIXME controler la structure !
	 * 
	 * @param cStructure
	 * @return
	 */
	public EOSolde getEoSoldeCongeNatifDernier(String cStructure) {

		EOSolde eoSolde = null;

		EOPlanning prev = getEoPlanningPrecedent();

		if (prev != null) {

			EOQualifier qual = ERXQ.isTrue(I_Solde.IS_CONGE_NATIF_KEY);
			if (!StringCtrl.isEmpty(cStructure)) {
				qual = ERXQ.and(
						qual, ERXQ.equals(EOSolde.C_STRUCTURE_KEY, cStructure));
			}

			NSArray<EOSolde> eoSoldeCongeArray = prev.tosSolde(qual);
			if (eoSoldeCongeArray.count() > 0) {
				eoSolde = eoSoldeCongeArray.lastObject();
			}
		}

		return eoSolde;

	}
	
	/**
	 * La balance du planning précédent
	 * @param cStructure
	 * @return
	 */
	public EOSolde getEoSoldeBalanceDernier(String cStructure) {
		
		EOSolde eoSolde = null;
		
		EOPlanning prev = getEoPlanningPrecedent();
		
		if (prev != null) {
			
			EOQualifier qual = ERXQ.isTrue(I_Solde.IS_BALANCE_KEY);
			if (!StringCtrl.isEmpty(cStructure)) {
				qual = ERXQ.and(
						qual, ERXQ.equals(EOSolde.C_STRUCTURE_KEY, cStructure));
			}
			NSArray<EOSolde> eoSoldeBalanceArray = prev.tosSolde(qual);
			if (eoSoldeBalanceArray.count() > 0) {
				eoSolde = eoSoldeBalanceArray.lastObject();
			}
			
		}
		
		return eoSolde;
	}
	
	
	
	

	/**
	 * La liste des soldes aptes à être gérés directement par l'interface de
	 * gestion des privilèges (régularisation de solde, ...)
	 * 
	 * @return
	 */
	public NSArray<EOSolde> getEoSoldeArrayPourGestionPrivilege() {
		NSArray<EOSolde> array = null;

		EOQualifier qual = ERXQ.or(
				ERXQ.isTrue(I_Solde.IS_CONGE_NATIF_KEY),
				ERXQ.isTrue(I_Solde.IS_REGULARISATION_KEY),
				ERXQ.isTrue(I_Solde.IS_DECHARGE_SYNDICALE_KEY),
				ERXQ.isTrue(I_Solde.IS_RELIQUAT_NATIF_KEY));

		// n'afficher que les soldes ayant une valeur en dur
		// qual = ERXQ.and(
		// qual,
		// ERXQ.isNotNull(EOSolde.INIT_KEY));

		array = tosSolde(qual);

		return array;
	}

	/**
	 * Le planning précédent chronologiquement
	 * 
	 * @return
	 */
	public EOPlanning getEoPlanningPrecedent() {
		if (_eoPlanningPrecedent == null) {

			EOQualifier qual = ERXQ.and(
						ERXQ.equals(PERS_ID_KEY, persId()),
						ERXQ.lessThan(EOPlanning.TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY, toPeriode().perDDebut()),
						ERXQ.equals(NATURE_KEY, nature()));

			NSArray<EOPlanning> array = fetchAll(
							editingContext(), qual);

			// les plannings ajoutés mais non sauvegardés (ex : planning simu cet)
			NSArray<EOEnterpriseObject> contextArray = getObjectsInsertedInContextMatching(
					editingContext(), ENTITY_NAME, qual);

			for (EOEnterpriseObject object : contextArray) {
				array = array.arrayByAddingObject((EOPlanning) object);
			}

			if (array.count() > 0) {
				array = CktlSort.sortedArray(array, EOPlanning.TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY);
				_eoPlanningPrecedent = array.lastObject();
			}
		}

		return _eoPlanningPrecedent;
	}

	/**
	 * Obtenir un enregistrement proprietaire sans période
	 * 
	 * @param edc
	 * @param persId
	 * @param codeTypeSource
	 * @return
	 */
	public static EOPlanning findEoPlanning(
				EOEditingContext edc, Number persId, String nature, EOPeriode eoPeriode) {

		EOPlanning eoPlanning = null;

		EOQualifier qualPeriode = null;
		if (eoPeriode != null) {
			qualPeriode = ERXQ.equals(TO_PERIODE_KEY, eoPeriode);
		}

		EOQualifier qual = ERXQ.and(
					ERXQ.equals(PERS_ID_KEY, persId),
					ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, A_FwkCktlHamacRecord.NON),
					ERXQ.equals(NATURE_KEY, nature),
					qualPeriode);

		// est-il dans le EOEditingContext
		NSArray<EOEnterpriseObject> memoireArray = getObjectsInsertedInContextMatching(
				edc, EOPlanning.ENTITY_NAME, qual);

		if (memoireArray.count() > 0) {
			// si oui, il n'y en a qu'un
			eoPlanning = (EOPlanning) memoireArray.objectAtIndex(0);
		} else {
			// non, bon ben en base
			eoPlanning = EOPlanning.fetchByQualifier(edc, qual);
		}

		return eoPlanning;

	}

	/***
	 * Effectue la comparaison avec un autre planning, indiquant les différences.
	 * La structure retournée est un simple tableau a exploiter par l'interface.
	 * 
	 * @param planning
	 * @return
	 */
	public NSMutableArray<String> rapportComparaison(EOPlanning eoPlanning) {

		NSMutableArray<String> rapport = new NSMutableArray<String>();

		if (eoPlanning != null) {

			if (eoPlanning == this) {

				throw new IllegalStateException("La comparaison est faite sur le même objet");

			} else {

				// plannings associés
				Planning planning = getPlanning();
				Planning autrePlanning = eoPlanning.getPlanning();
				
				// soldes
				int minutesSrc = 0;
				int minutesDst = 0;

				// droit à congé initial
				for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

					minutesSrc = planning.getCalculDroitCongeDelegate().getDroitCongeDepuisCalculMinutes(eoStructure.cStructure());
					minutesDst = autrePlanning.getCalculDroitCongeDelegate().getDroitCongeDepuisCalculMinutes(eoStructure.cStructure());

					if (minutesSrc != minutesDst) {
						rapport.addObject("Droit à congé initial sur " +
									eoStructure.lcStructure() + " de " + 
									eoPlanning.toPersonne().getNomPrenomAffichage() + " : " +
									TimeCtrl.stringForMinutes(minutesSrc) + "(1) " +
									TimeCtrl.stringForMinutes(minutesDst) + "(2) ");
					}

					// droit à congé restant
					minutesSrc = planning.getCalculDroitCongeDelegate().getDroitCongeRestantMinutes(eoStructure.cStructure());
					minutesDst = autrePlanning.getCalculDroitCongeDelegate().getDroitCongeRestantMinutes(eoStructure.cStructure());

					if (minutesSrc != minutesDst) {
						rapport.addObject("Droit à congé restant sur " +
									eoStructure.lcStructure() + " de " +
									eoPlanning.toPersonne().getNomPrenomAffichage() + " : " +
									TimeCtrl.stringForMinutes(minutesSrc) + "(1) " +
									TimeCtrl.stringForMinutes(minutesDst) + "(2) ");
					}

				}

				// TODO les autres soldes

				// travail attendu

			}

		} else {

			throw new IllegalStateException("Le 2ème planning n'existe pas");

		}

		return rapport.mutableClone();
	}

	/**
	 * Obtenir une liste de {@link EOPlanning} d'après des critères
	 * 
	 * @param ec
	 * @param persId
	 * @param nature
	 * @param temPersonneComposante
	 * @param cStructure
	 * @param eoPeriode
	 * @return
	 */
	public static NSArray<EOPlanning> findSortedEoPlanningArray(
			EOEditingContext ec,
			Number persId,
			String nature,
			String temPersonneComposante, // TODO a prendre en compte
			String cStructure,
			EOPeriode eoPeriode) {

		return findSortedEoPlanningArray(ec, getQualifierEoPlanningArray(persId, null, nature, eoPeriode), cStructure);
		
	}

	private static NSArray<EOPlanning> findSortedEoPlanningArray(EOEditingContext ec, EOQualifier qual, String cStructure) {
		
		NSArray<EOPlanning> array = fetchAll(ec, qual);

		// filtrage sur la structure a posteriori car on se base sur
		// getEoSourceArray()
		if (!StringCtrl.isEmpty(cStructure)) {
			NSMutableArray<EOPlanning> arrayMutable = new NSMutableArray<EOPlanning>();
			for (EOPlanning eoPlanning : array) {
				NSArray<String> cStructureArray = (NSArray<String>) eoPlanning.getEoSourceArray().valueForKey(
						EOPlanningSource.C_STRUCTURE_KEY);
				if (cStructureArray.containsObject(cStructure)) {
					arrayMutable.add(eoPlanning);
				}
			}
			array = arrayMutable.immutableClone();
		}

		// classement alpha et par date
		return CktlSort.sortedArray(array, STR_SORT_NOM_ET_DATE);
		
	}
	
	/**
	 * 
	 * @param persId
	 * @param personneArray
	 * @param nature
	 * @param eoPeriode
	 * @return
	 */
	private static EOQualifier getQualifierEoPlanningArray(Number persId, NSArray<IPersonne> personneArray, String nature, EOPeriode eoPeriode) {
		
		EOQualifier qual = ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, A_FwkCktlHamacRecord.NON);

		NSArray<EOQualifier> personneIdQual = new NSMutableArray<EOQualifier>();
		
		if (!NSArrayCtrl.isEmpty(personneArray)) {
			
			for (IPersonne personne : personneArray) {
				personneIdQual.add(ERXQ.equals(PERS_ID_KEY, personne.persId()));
			}
			
			qual = ERXQ.and(ERXQ.or(personneIdQual));
		} else if (persId != null) {
			qual = ERXQ.and(
					qual, ERXQ.equals(PERS_ID_KEY, persId));
		}

		if (nature != null) {
			qual = ERXQ.and(
					qual, ERXQ.equals(NATURE_KEY, nature));
		}

		if (eoPeriode != null) {
			qual = ERXQ.and(
					qual, ERXQ.equals(TO_PERIODE_KEY, eoPeriode));
		}
		
		return qual;
		
	}
	
	/**
	 * Obtenir une liste de {@link EOPlanning} d'après des critères
	 * 
	 * @param ec
	 * @param persId
	 * @param nature
	 * @param temPersonneComposante
	 * @param cStructure
	 * @param eoPeriode
	 * @return
	 */
	public static NSArray<EOPlanning> findSortedEoPlanningPersonneArray(
			EOEditingContext ec,
			NSArray<IPersonne> personneArray,
			String nature,
			String temPersonneComposante, // TODO a prendre en compte
			String cStructure,
			EOPeriode eoPeriode) {

		return findSortedEoPlanningArray(ec, getQualifierEoPlanningArray(null, personneArray, nature, eoPeriode), cStructure);
	}
	
	/**
	 * Instancier un certain nombre de {@link EOPlanning} attendus par rapport aux
	 * données du connecteur pour une personne
	 * 
	 * 
	 * @return
	 */
	public static NSArray<EOPlanning> spawnEoPlanningArraySurConnecteur(
			EOEditingContext ec, NSArray<IPersonne> personneArray) {

		NSArray<EOPlanning> array = new NSArray<EOPlanning>();

		
		NSArray<Integer> arrayPersId = new NSMutableArray<Integer>();
		for (IPersonne personne : personneArray) {
			arrayPersId.add(personne.persId());
		}
		
		
		
		NSArray<A_EOSource> eoSourceArray =  A_EOSource.getSourceArrayFor(
					getClassConnecteurMangueHamac(), ec, null, null, arrayPersId, null);
		
		
		// liste des périodes attendues
		NSArray<EOPeriode> eoPeriodeArray = EOPeriode.getEoPeriodeArrayForSource(ec, eoSourceArray);

		for (EOPeriode eoPeriode : eoPeriodeArray) {

			for (IPersonne personne : personneArray) {
				
				//
				EOPlanning eoPlanningConnecteur = creationSiNonExistant(
						ec, personne, CONN, NON, eoPeriode);
	
				EOPlanning eoPlanningReel = findEoPlanning(
						ec, personne.persId(), REEL, eoPeriode);
	
				if (eoPlanningReel != null) {
	
					// recopie des associations horaires semaines
					if (eoPlanningConnecteur.tosAssociationHoraire().count() == 0) {
						eoPlanningReel.recopierAssociationHoraireSemaineReelVers(CONN);
	
					}
	
					// soldes
					if (eoPlanningReel.tosSolde().count() > 0) {
						for (EOSolde eoSoldeReel : eoPlanningReel.tosSolde()) {
							eoSoldeReel.dupliquerVers(eoPlanningConnecteur, true);
						}
					}
	
				}
	
				array = array.arrayByAddingObject(eoPlanningConnecteur);
			
			}
		}

		return array;
	}

	/**
	 * Parmi un tableau de proprietaire, ne choisir que le courant, et à défaut le
	 * dernier (on se base sur la période)
	 * 
	 * @param ec
	 * @param eoPlanningArray
	 * @return
	 */
	public static EOPlanning getCourantOuDernierEoPlanning(
				EOEditingContext ec, NSArray<EOPlanning> eoPlanningArray) {

		EOPlanning eoPlanning = null;

		if (!NSArrayCtrl.isEmpty(eoPlanningArray)) {
			EOPeriode currentPeriode = EOPeriode.getCurrentPeriode(ec);
			// planning en cours
			if (currentPeriode != null) {
				EOQualifier qual = ERXQ.equals(EOPlanning.TO_PERIODE_KEY, currentPeriode);
				NSArray<EOPlanning> array = EOQualifier.filteredArrayWithQualifier(eoPlanningArray, qual);
				if (array.count() > 0) {
					// planning courant
					eoPlanning = array.lastObject();
				} else {
					// dernier planning
					eoPlanning = eoPlanningArray.lastObject();
				}
			} else {
				// dernier planning
				eoPlanning = eoPlanningArray.lastObject();
			}

		}

		return eoPlanning;
	}

	/**
	 * RAZ des soldes initiaux avant calcul des débits. On passe le planning en
	 * paramètre pour éviter les boucles infinies en rappelant
	 * {@link #getPlanning()}
	 */
	public void reinitialiserSoldes() {

		// CktlLog.log("reinitialiserSoldes() ");

		// remettre à zéro des valeurs restantes des soldes
		for (EOStructure structure : getPlanning().getEoStructureAttenduArray()) {

			String cStructure = structure.cStructure();

			// types automatiques par service: reliquat et congés
			for (String typeSolde : EOTypeSolde.SOLDE_NATIF_PAR_SERVICE_ARRAY) {

				EOSolde eoSolde = getEoSolde(
								cStructure, typeSolde, null, null);

				// creation de l'enregistrement en base si besoin
				if (eoSolde == null) {
					eoSolde = EOSolde.createSolde(
									editingContext(),
									null,
									null,
									typeSolde,
									structure,
									this,
									null);

				} else {
					eoSolde.memoriserSoldePrecedent();
				}

				Integer restant = null;

				if (eoSolde.isReliquatNatif()) {
					restant = Integer.valueOf(0);
				} else if (eoSolde.isCongeNatif()) {
					restant = Integer.valueOf(0);
				} else if (eoSolde.isReliquatHsup()) {
					restant = Integer.valueOf(0);
				} else if (eoSolde.init() != null) {
					// valeur de solde manuelle
					restant = eoSolde.init();
				}

				if (restant == null) {
					restant = Integer.valueOf(0);
				}

				// seule cette valeur est exploitée
				eoSolde.setRestant(restant);

			}

			// types mis manuellement, on ne réinitialise que la valeur restante
			// car la valeur restante est recalculée systématiquement
			NSArray<EOSolde> eoSoldeArray = tosSolde();
			for (int i = 0; i < eoSoldeArray.count(); i++) {
				EOSolde eoSolde = eoSoldeArray.objectAtIndex(i);

				// les soldes natifs sont déjà traités dans l'itération précédente
				if (eoSolde.isTypeSoldeNatif() == false) {
					eoSolde.memoriserSoldePrecedent();
					eoSolde.setRestant(eoSolde.init());
				}

			}

		}

		// types automatiques non liés à un service
		for (String typeSolde : EOTypeSolde.SOLDE_NATIF_SANS_SERVICE_ARRAY) {

			// TODO remettre la nature obligatoire
			EOSolde eoSolde = getEoSolde(
						null, typeSolde, null, null);

			// creation de l'enregistrement en base si besoin
			if (eoSolde == null) {

				eoSolde = EOSolde.createSolde(
							editingContext(),
							null,
							null,
							typeSolde,
							null,
							this,
							null);

			} else {
				eoSolde.memoriserSoldePrecedent();
			}

			Integer restant = null;

			if (eoSolde.init() != null) {
				// valeur de solde manuelle
				restant = eoSolde.init();
			} else {
				restant = Integer.valueOf(0);
			}

			eoSolde.setRestant(restant);

		}

	}

	//
	private final static String STR_QUAL_SOLDE_RELIQUAT_NATIF_INIT =
				EOSolde.TO_PLANNING_KEY + "=%@ and " +
						EOSolde.TO_STRUCTURE_KEY + "." + EOStructure.C_STRUCTURE_KEY + "=%@ and " +
						EOSolde.TO_TYPE_SOLDE_KEY + "." + EOTypeSolde.STR_QUAL_IS_RELIQUAT_NATIF + " and" +
						EOSolde.D_DEB_VALIDITE_KEY + "=nil and " + EOSolde.D_FIN_VALIDITE_KEY + "=nil";

	/**
	 * Le solde de reliquat natif attendu pour tout planning
	 * 
	 * @param dbPlanning
	 * @param cStructure
	 * @return
	 */
	private EOSolde getSoldeReliquatInit(String cStructure) {
		EOSolde eoSolde = null;

		EOQualifier qual = CktlDataBus.newCondition(
					STR_QUAL_SOLDE_RELIQUAT_NATIF_INIT,
					new NSArray<Object>(new Object[] {
							this, cStructure, Boolean.TRUE }));

		NSArray<EOSolde> array = EOQualifier.filteredArrayWithQualifier(
					tosSolde(), qual);

		if (array.count() > 0) {
			eoSolde = array.objectAtIndex(0);
		}

		return eoSolde;
	}

	/**
	 * Récupérer la valeur initiale des reliquats de congés sur un service.
	 */
	public int getReliquatInitial(String cStructure) {

		int minutes = 0;

		// trouver un enregistrement
		EOSolde eoSoldeReliquat = getSoldeReliquatInit(cStructure);
		if (eoSoldeReliquat != null) {
			// d'abord voir s'il y a une valeur manuelle avec période standard
			if (eoSoldeReliquat.init() != null) {
				minutes = eoSoldeReliquat.init();
			} else {
				// dernier cas (le plus courant) c'est le restant de l'année N-1 sur la
				// même structure
				EOSolde eoSoldeConge = getEoSoldeCongeNatifDernier(cStructure);
				if (eoSoldeConge != null) {
					minutes = eoSoldeConge.restant();
				}
			}
		}

		return minutes;

	}

	/**
	 * Récupérer la valeur restante des reliquats de congés sur un service.
	 */
	public int getReliquatRestant(String cStructure) {

		int minutes = 0;

		// trouver un enregistrement
		EOSolde eoSoldeReliquat = getSoldeReliquatInit(cStructure);
		if (eoSoldeReliquat != null &&
				eoSoldeReliquat.restant() != null) {
			minutes = eoSoldeReliquat.restant().intValue();
		}

		return minutes;

	}

	/**
	 * @return
	 */
	public final NSArray<EOOccupation> findOccupation() {

		// les occupations associées au planning, par nature. Pour le planning
		// "connecteur", on prend les occupation réelles
		String nature = nature();
		if (isConnecteur()) {
			nature = REEL;
		}

		//
		EOQualifier qual = ERXQ.and(
				ERXQ.equals(EOOccupation.PERS_ID_KEY, persId()),
				ERXQ.equals(EOOccupation.NATURE_KEY, nature));

		NSArray<EOOccupation> array = EOOccupation.fetchAll(editingContext(), qual, null);

		// les occupations ajoutées mais non sauvegardées (ex : planning simu cet)
		NSArray<EOEnterpriseObject> contextArray = getObjectsInsertedInContextMatching(
				editingContext(), EOOccupation.ENTITY_NAME, qual);

		for (EOEnterpriseObject object : contextArray) {
			array = array.arrayByAddingObject((EOOccupation) object);
		}

		return array;
	}

	/**
	 * Donne la liste des objets {@link TravailAttendu} attendus pour le planning
	 * en cours
	 * 
	 * @return
	 */
	public NSArray<TravailAttendu> getTravailAttenduArray() {
		NSArray<TravailAttendu> travailAttenduArray = null;

		travailAttenduArray = TravailAttendu.getTravailAttenduSortedArrayForPlanning(this);

		return travailAttenduArray;
	}

	/**
	 * La liste des {@link EOStructure} dont du travail est attendu sur ce
	 * planning. Il est possible de prédifinir la base de travail attendu. Le cas
	 * échéant, la méthode ré-interroge la source.
	 * 
	 * La liste est restreinte aux services autorisés sur la période
	 * 
	 * @param isCourantOuFuturUniquement
	 *          TODO
	 * 
	 * @return
	 */
	public NSMutableArray<EOStructure> getEoStructureAttenduArray(
			NSArray<TravailAttendu> travailAttenduArray, boolean isCourantOuFuturUniquement) {
		NSMutableArray<EOStructure> eoStructureAttenduArray = new NSMutableArray<EOStructure>();

		NSArray<TravailAttendu> attenduArray = travailAttenduArray;

		if (travailAttenduArray == null) {
			attenduArray = getTravailAttenduArray();
		}
		
		if (isCourantOuFuturUniquement) {
			EOQualifier qual = ERXQ.isTrue(TravailAttendu.IS_COURANT_OU_FUTUR_KEY);
			attenduArray = EOQualifier.filteredArrayWithQualifier(attenduArray, qual);
		}

		for (TravailAttendu attendu : attenduArray) {

			NSArray<String> cStructureArray = (NSArray<String>) eoStructureAttenduArray.valueForKey(EOStructure.C_STRUCTURE_KEY);

			if (!cStructureArray.containsObject(attendu.getCStructure())) {
				EOStructure eoStructure = EOStructure.fetchByKeyValue(
							editingContext(), EOStructure.C_STRUCTURE_KEY, attendu.getCStructure());

				// seules les structures autorisées
				if (toPeriode().getEOStructureAutoriseForPeriode().containsObject(eoStructure)) {
					eoStructureAttenduArray.addObject(eoStructure);
				}

			}
		}

		return eoStructureAttenduArray;
	}

	// tmp dev : suppression du lien REPART_PLANNING_SOURCE, on fait du cache

	private NSArray<A_EOSource> _eoSourceArray;

	/**
	 * La liste des sources de données associées. Est auto-déterminée d'après la
	 * nature du planning
	 * 
	 * @return
	 */
	public NSArray<A_EOSource> getEoSourceArray() {
		if (_eoSourceArray == null) {

			// la source est définie d'après la nature
			Class<?> clazz = EOPlanningSource.class;
			if (isConnecteur()) {
				clazz = getClassConnecteurMangueHamac();
			}

			_eoSourceArray = EOPlanningSource.getSourceArrayFor(
					clazz, editingContext(), toPeriode(), null, new NSArray<Integer>(persId()), null);
		}
		return _eoSourceArray;
	}

	/**
	 * Gestionnaire des demandes
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#getDemandeDelegate
	 *      ()
	 */
	public DemandeDelegate getDemandeDelegate() {
		if (_demandeDelegate == null) {
			_demandeDelegate = new DemandeDelegate(this);
		}
		return _demandeDelegate;
	}

	/**
	 * XXX type de sequence de validation en paramètre ?
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 *      toTypeSequenceValidation()
	 */
	public EOTypeSequenceValidation toTypeSequenceValidation() {
		if (_toTypeSequenceValidation == null) {
			_toTypeSequenceValidation = EOTypeSequenceValidation.getEoTypeSequenceValidationPlanning(editingContext());
		}
		return _toTypeSequenceValidation;
	}

	// fin XXX

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXGenericRecord#toString()
	 */
	@Override
	public String toString() {
		if (_toString == null) {
			if (toPersonne() != null && toPeriode() != null) {
				_toString = toPersonne().getNomPrenomAffichage() + " " + toPeriode().perLibelle();
			} else {
				_toString = PERS_ID_KEY + "=" + persId();
			}
			if (isConnecteur()) {
				_toString += " (connecteur)";
			}
		}
		return _toString;
	}

	// autorisation des modifications

	/**
	 * Indique si les conditions rendent envisageable une action. Par exemple, un
	 * planning de test ne pourrait jamais être validé. Un planning réel oui. Ca
	 * permet d'afficher une information justifiant pourquoi ça bloque
	 * 
	 * @param clazz
	 * @return
	 */
	public boolean isEnvisageable(Class<?> clazz) {
		boolean isEnvisageable = false;

		if (clazz == WorkflowPlanningDemandeValidation.class) {
			// demande de validation
			isEnvisageable = getCheckerEOPlanning().isDemandeValidationEnvisageable();
		}

		return isEnvisageable;
	}

	/**
	 * Indique si l'éxécution d'une action peut être autorisée.
	 * 
	 * @param clazz
	 * @return
	 */
	public boolean isAutorisee(Class<?> clazz, HamacApplicationUser hamacApplicationUser)
			throws IllegalStateException {

		boolean isAutorisee = false;

		if (clazz == WorkflowPlanningDemandeValidation.class) {
			// demande de validation
			isAutorisee = getCheckerEOPlanning().isDemandeValidationAutorisee();
		} else if (clazz == WorkflowPlanningAnnulerDemandeValidation.class) {
			// annulation demande de validation
			isAutorisee = getCheckerEOPlanning().isAnnulationDemandeValidationAutorisee();
		} else if (clazz == WorkflowPlanningDemandeModification.class) {
			// demande de modification
			isAutorisee = getCheckerEOPlanning().isDemandeModificationAutorisee();
		} else if (clazz == WorkflowPlanningAnnulerDemandeModification.class) {
			// annulation demande de modification
			isAutorisee = getCheckerEOPlanning().isAnnulationDemandeModificationAutorisee();
		} else if (clazz == WorkflowAbsenceSaisie.class) {
			// saisie d'une occupation
			isAutorisee = getCheckerEOPlanning().isSaisieOccupationAutorisee(hamacApplicationUser);
		} else if (clazz == WorkflowPlanningAssociationHoraire.class) {
			// association d'horaires
			isAutorisee = getCheckerEOPlanning().isAssociationHoraireAutorisee(hamacApplicationUser);
		}

		return isAutorisee;
	}

	/**
	 * Controle régulier du planning pour s'assurer que tout est correct (pour
	 * prévenir les changements de sources par exemple)
	 * 
	 * @return
	 * @throws IllegalStateException
	 */
	public void checkCoherence()
			throws IllegalStateException {
		getCheckerEOPlanning().checkCoherence();
	}

	private CheckerEOPlanning _checkerEOPlanning;

	private CheckerEOPlanning getCheckerEOPlanning() {
		if (_checkerEOPlanning == null) {
			_checkerEOPlanning = new CheckerEOPlanning(this);
		}
		return _checkerEOPlanning;
	}

	//

	/**
	 * @return
	 */
	public boolean isReel() {
		boolean isReel = false;

		if (nature().equals(REEL)) {
			isReel = true;
		}

		return isReel;
	}

	/**
	 * @return
	 */
	public boolean isTest() {
		boolean isTest = false;

		if (nature().equals(TEST)) {
			isTest = true;
		}

		return isTest;
	}

	/**
	 * @return
	 */
	public boolean isHist() {
		boolean isHist = false;

		if (nature().equals(HIST)) {
			isHist = true;
		}

		return isHist;
	}

	/**
	 * @return
	 */
	public boolean isPrev() {
		boolean isPrev = false;

		if (nature().equals(PREV)) {
			isPrev = true;
		}

		return isPrev;
	}

	/**
	 * @return
	 */
	public boolean isSimulationCet() {
		boolean isTest = false;

		if (nature().equals(SIMU)) {
			isTest = true;
		}

		return isTest;
	}

	/**
	 * @return
	 */
	public boolean isConnecteur() {
		boolean isConnecteur = false;

		if (nature().equals(CONN)) {
			isConnecteur = true;
		}

		return isConnecteur;
	}
	
	/**
	 * @return
	 */
	public static boolean isConnecteurHamacV3() {
		
		boolean isConnecteurHamacV3 = false;
		
		String tableName = HamacCktlConfig.stringForKey(EOParametre.CONNECTEUR_TABLE);
		if (!StringCtrl.isEmpty(tableName) &&
				(tableName.equalsIgnoreCase(EOParametre.CONNECTEUR_HAMACV3) || tableName.equalsIgnoreCase(EOParametre.CONNECTEUR_JAVA))) {
			isConnecteurHamacV3 = true;
		}
		return isConnecteurHamacV3;
	}
	
	/**
	 * @return
	 */
	public static Class<? extends A_EOSource> getClassConnecteurMangueHamac() {
		
		Class<? extends A_EOSource> clazz;
		
		String tableName = HamacCktlConfig.stringForKey(EOParametre.CONNECTEUR_TABLE);
		if (!StringCtrl.isEmpty(tableName) &&
				(tableName.equalsIgnoreCase(EOParametre.CONNECTEUR_HAMACV3) || tableName.equalsIgnoreCase(EOParametre.CONNECTEUR_JAVA))) {
			clazz = EOAffectationsMangueHamac.class;
		} else {
			clazz = EOVConnecteurAffectation.class;
		}
		
		return clazz;
	}
	

	// spécifités planning prévisionnel

	/**
	 * Création d'un objet s'il n'existe ni dans le {@link EOEditingContext}, ni
	 * en base de données.
	 * 
	 * @param editingContext
	 * @param personne
	 * @param nature
	 * @param temPersonneComposante
	 * @param eoPeriode
	 * @return
	 */
	public static EOPlanning creationSiNonExistant(
			EOEditingContext editingContext, IPersonne personne, String nature,
			String temPersonneComposante, EOPeriode eoPeriode) {
		EOPlanning eoPlanning = null;

		Integer persId = personne.persId();

		// rechercher d'abord dans le editingcontext s'il vient tout juste d'etre
		// inséré, mais pas encore en base de données (cas de plusieurs associations
		// sans sauvegardes intermédiaires)
		if (editingContext.insertedObjects().count() > 0) {
			EOQualifier qual = ERXQ.equals("entityName", ENTITY_NAME);
			NSArray<EOEnterpriseObject> eoArray = EOQualifier.filteredArrayWithQualifier(
					editingContext.insertedObjects(), qual);
			int i = 0;
			while (eoPlanning == null && i < eoArray.count()) {
				EOPlanning eo = (EOPlanning) eoArray.objectAtIndex(i);
				if (eo.persId().intValue() == persId.intValue() &&
						eo.nature().equals(nature) &&
						eo.temPersonneComposante().equals(temPersonneComposante) &&
						eo.toPeriode() == eoPeriode) {
					eoPlanning = eo;
				}

				i++;
			}
		}

		// pas dans le context => go en base
		if (eoPlanning == null) {
			eoPlanning = findEoPlanning(editingContext, persId, nature, eoPeriode);
		}

		if (eoPlanning == null) {
			eoPlanning = createEOPlanning(editingContext, DateCtrl.now(), DateCtrl.now(), nature, persId, temPersonneComposante, eoPeriode);
			//
			eoPlanning.setToPersonne(personne);
		}

		return eoPlanning;
	}

	/**
	 * Création d'un planning si ce dernier est inexistant
	 * 
	 * @return
	 */
	protected EOPlanning creationSiNonExistant(String natureDst) {
		EOPlanning eoPlanning = null;

		eoPlanning = creationSiNonExistant(
				editingContext(), toPersonne(), natureDst, temPersonneComposante(), toPeriode());

		return eoPlanning;
	}

	private boolean _isDupliquerAbsencePrevisionnellesDansReel = false;

	/**
	 * Gestion du changement de statut du planning réel
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 *      postTraitementChangementStatut
	 *      (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)
	 */
	public void setToTypeStatutRelationship(EOTypeStatut eoTypeStatutSuivant) {
		if (isReel()) {

			// vers le statut valide
			if (eoTypeStatutSuivant.isStatutValide()) {

				// effacement du planning intervalidation
				EOPlanning eoPlanningIntervalidation = findEoPlanning(editingContext(), persId(), INTR, toPeriode());
				if (eoPlanningIntervalidation != null) {
					editingContext().deleteObject(eoPlanningIntervalidation);
				}

				// si provenant du planning prévisionnel, alors on va nettoyer les
				// occupations réelles si la configuration l'indique
				if (toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {

					// le statut suivant dépend de la configuration
					EOTypeStatut eoTypeStatutOccSuivant = null;
					if (_isDupliquerAbsencePrevisionnellesDansReel) {
						eoTypeStatutOccSuivant = EOTypeStatut.getEoTypeStatutEnCoursDeValidation(editingContext());
					} else {
						eoTypeStatutOccSuivant = EOTypeStatut.getEoTypeStatutSupprime(editingContext());
					}

					// passer les occupations réelles avec le statut validation
					// prévisionnelle au statut souhaité

					NSArray<EOOccupation> eoOccupationAModifierArray = findOccupation();
					EOQualifier qual = ERXQ.equals(
							EOOccupation.TO_TYPE_STATUT, EOTypeStatut.getEoTypeStatutEnCoursDeValidationPrevisionnelle(editingContext()));
					eoOccupationAModifierArray = EOQualifier.filteredArrayWithQualifier(eoOccupationAModifierArray, qual);

					for (EOOccupation eoOcc : eoOccupationAModifierArray) {
						eoOcc.setToTypeStatutRelationship(eoTypeStatutOccSuivant);
					}

					// TODO s'il faut dupliquer, on va générer les emails de notification
					if (_isDupliquerAbsencePrevisionnellesDansReel) {

					}

				}

			}

			// vers le statut en cours de modification
			if (eoTypeStatutSuivant.isStatutEnCoursDeModification()) {

				// effacement du planning intervalidation
				EOPlanning eoPlanningIntervalidation = findEoPlanning(editingContext(), persId(), INTR, toPeriode());
				if (eoPlanningIntervalidation != null) {
					editingContext().deleteObject(eoPlanningIntervalidation);
				}

				// recopier le planning actuel vers le planning historique
				if (toTypeStatut().isStatutValide()) {
					recopierAssociationHoraireSemaineReelVers(HIST);
				}

			}

			// vers le statut en cours de validation
			if (eoTypeStatutSuivant.isStatutEnCoursDeValidation()) {

			}

		}

		super.setToTypeStatutRelationship(eoTypeStatutSuivant);

	}

	/**
	 * Recopier les associations horaires semaines
	 * 
	 * @param natureDst
	 * @return
	 */
	private void recopierAssociationHoraireSemaineReelVers(String natureDst) {
		EOPlanning eoPlanningDst = creationSiNonExistant(natureDst);
		for (EOAssociationHoraire eoAssoSrc : tosAssociationHoraire()) {
			eoAssoSrc.repercuter(eoPlanningDst, true, false);
		}
	}

	/**
	 * Le planning de test associé. S'il n'existe pas alors on l'instancie
	 * 
	 * @return
	 */
	public EOPlanning getEoPlanningTest() {

		if (_eoPlanningTest == null) {

			if (isTest()) {
				_eoPlanningTest = this;
			} else {
				// créer le planning de test s'il n'existe pas déjà
				_eoPlanningTest = creationSiNonExistant(TEST);
			}

		}

		return _eoPlanningTest;

	}

	/**
	 * Le planning historique associé. S'il n'existe pas alors on l'instancie
	 * 
	 * @return
	 */
	public EOPlanning getEoPlanningHist() {

		if (_eoPlanningHist == null) {

			if (isHist()) {
				_eoPlanningHist = this;
			} else {
				// créer le planning historique s'il n'existe pas déjà
				_eoPlanningHist = creationSiNonExistant(HIST);
			}

		}

		return _eoPlanningHist;

	}

	/**
	 * Le planning prévisionnel associé.
	 * 
	 * @return
	 */
	public final EOPlanning getEoPlanningPrev() {

		if (_eoPlanningPrev == null) {

			if (isPrev()) {
				_eoPlanningPrev = this;
			} else {
				// créer le planning prévisionnel s'il n'existe pas déjà
				_eoPlanningPrev = creationSiNonExistant(PREV);
			}

		}

		return _eoPlanningPrev;

	}

	/**
	 * Le planning pour faire les simulations sur CET.. S'il n'existe pas alors on
	 * l'instancie
	 * 
	 * @return
	 */
	public EOPlanning getEoPlanningSimulationCet() {

		if (_eoPlanningCetS == null) {

			if (isSimulationCet()) {
				_eoPlanningCetS = this;
			} else {
				// créer le planning de test s'il n'existe pas déjà
				_eoPlanningCetS = creationSiNonExistant(SIMU);
			}

		}

		return _eoPlanningCetS;

	}

	/**
	 * Le planning réel associé. S'il n'existe pas alors on l'instancie
	 * 
	 * @return
	 */
	public EOPlanning getEoPlanningReel() {

		if (_eoPlanningReel == null) {

			if (isReel()) {
				_eoPlanningReel = this;
			} else {
				// créer le planning reel s'il n'existe pas déjà
				_eoPlanningReel = creationSiNonExistant(REEL);
			}

		}

		return _eoPlanningReel;

	}

	/**
	 * Recopie tous les {@link EOPlanning} associés au persId pointé par
	 * {@link #persId()}
	 * 
	 * @param eoPlanningSrc
	 * @param natureDst
	 * 
	 * @return
	 */
	public static EOPlanning dupliquerVers(
			EOPlanning eoPlanningSrc, String natureDst, EOCetTransactionAnnuelle transactionANePasEffacer) {

		EOEditingContext ec = eoPlanningSrc.editingContext();
		String natureSrc = eoPlanningSrc.nature();

		// création des objets EOPlanningDst
		NSArray<EOPlanning> eoPlanningDstArray = new NSArray<EOPlanning>();

		NSArray<EOPlanning> eoPlanningSrcArray = findSortedEoPlanningArray(
				ec, eoPlanningSrc.persId(), natureSrc, eoPlanningSrc.temPersonneComposante(), null, null);

		for (EOPlanning src : eoPlanningSrcArray) {
			EOPlanning dst = src.creationSiNonExistant(natureDst);
			eoPlanningDstArray = eoPlanningDstArray.arrayByAddingObject(dst);
		}

		// *********
		// nettoyage
		// *********

		// occupations
		NSArray<EOOccupation> eoOccupationArray = EOOccupation.fetchAll(
				ec, ERXQ.and(
						ERXQ.equals(EOOccupation.PERS_ID_KEY, eoPlanningSrc.persId()),
						ERXQ.equals(EOOccupation.NATURE_KEY, natureDst)));

		for (EOOccupation eoOccupation : eoOccupationArray) {
			ec.deleteObject(eoOccupation);
		}

		// soldes
		for (EOPlanning eoPlanning : eoPlanningDstArray) {
			for (EOSolde eoSolde : eoPlanning.tosSolde()) {
				ec.deleteObject(eoSolde);
			}
		}

		// cet saisies manuelles
		NSArray<EOCetSaisieManuelle> saisieArray = EOCetSaisieManuelle.getTransactionArrayFor(
				ec, natureSrc, eoPlanningSrc.persId(), eoPlanningSrc.toPeriode().perDFin());
		for (EOCetSaisieManuelle saisie : saisieArray) {
			EOCetSaisieManuelle saisieDst = EOCetSaisieManuelle.createEOCetSaisieManuelle(
					ec, saisie.csmDateValeur(), saisie.csmValeur(), DateCtrlHamac.now(), DateCtrlHamac.now(), natureDst, saisie.persId());
		}

		// cet transactions annuelles
		NSArray<EOCetTransactionAnnuelle> transactionDstArray = EOCetTransactionAnnuelle.getEoCetTransactionArray(
				ec, eoPlanningSrc.persId(), natureDst);

		for (EOCetTransactionAnnuelle transaction : transactionDstArray) {
			if (transaction != transactionANePasEffacer) {
				ec.deleteObject(transaction);
			}
		}

		// *********
		// recopie
		// *********

		// occupations
		for (EOOccupation eoOccupation : eoPlanningSrc.getEoOccupationDirecte()) {
			eoOccupation.dupliquerVers(natureDst);
		}

		// associations + soldes
		for (EOPlanning src : eoPlanningSrcArray) {
			src.recopierAssociationHoraireSemaineReelVers(natureDst);
			EOPlanning eoPlanningDst = src.creationSiNonExistant(natureDst);
			for (EOSolde eoSoldeSrc : src.tosSolde()) {
				eoSoldeSrc.dupliquerVers(eoPlanningDst, true);
			}
		}

		// cet
		NSArray<EOCetTransactionAnnuelle> transactionSrcArray = EOCetTransactionAnnuelle.getEoCetTransactionArray(
				ec, eoPlanningSrc.persId(), natureSrc);
		for (EOCetTransactionAnnuelle transactionSrc : transactionSrcArray) {
			transactionSrc.dupliquerVers(natureDst);
		}

		// retourner le planning dst sur la meme période que la planning source
		EOPlanning eoPlanningDst = eoPlanningSrc.creationSiNonExistant(natureDst);

		return eoPlanningDst;

	}

	/**
	 * Ajout d'un historique de délégation
	 * 
	 * @param personne
	 */
	public void addHistoriqueDelegation(Integer persId) {
		EODelegationHistorique eoDelegationHistorique = EODelegationHistorique.create(editingContext(), persId);
		eoDelegationHistorique.setToPlanningRelationship(this);
	}

	// XXX trouver un autre moyen pour connaitre la fin d'application du droit
	// contractuel

	private HamacApplicationUser planningHamacApplicationUser;

	private void setPlanningHamacApplicationUser(HamacApplicationUser user) {
		planningHamacApplicationUser = user;
	}

	/**
	 * L'information est positionnée sur le premier planning (faire plus propre).
	 * Bidouille faite car on a pas la main sur EOIndividu
	 */
	public HamacApplicationUser getPlanningHamacApplicationUser() {
		//
		EOPlanning eoPlanning = this;
		while (eoPlanning.getEoPlanningPrecedent() != null) {
			eoPlanning = eoPlanning.getEoPlanningPrecedent();
		}
		if (eoPlanning.planningHamacApplicationUser == null) {
			eoPlanning.setPlanningHamacApplicationUser(new HamacApplicationUser(editingContext(), persId()));
		}
		return eoPlanning.planningHamacApplicationUser;
	}

	// CET
	// TODO a mettre ailleurs ?

	/**
	 * @return
	 */
	public Cet getCet() {
		// System.out.println(plaId() + " EOPlanning.getCet() _cet=" + _cet);
		if (_cet == null) {
			_cet = new Cet(this);
		}
		return _cet;
	}

	/**
	 * Utilisé pour le nettoyage de cache
	 */
	public boolean isCetInstancie() {
		boolean isCetInstancie = false;

		if (_cet != null) {
			isCetInstancie = true;
		}

		return isCetInstancie;
	}

	/**
	 * Comparer les périodes du planning
	 * 
	 * @param other
	 * @return
	 */
	public boolean isBefore(EOPlanning other) {
		boolean isBefore = false;

		if (DateCtrl.isBefore(toPeriode().perDDebut(), other.toPeriode().perDDebut())) {
			isBefore = true;
		}

		return isBefore;
	}

	/**
	 * @return
	 */
	public EOCetTransactionAnnuelle getEoCetTransactionAnnuelle() {
		EOCetTransactionAnnuelle record = null;

		NSArray<EOCetTransactionAnnuelle> array = EOCetTransactionAnnuelle.getEoCetTransactionArray(this);
		if (array.count() > 0) {
			record = array.objectAtIndex(0);
		}

		return record;
	}

	/**
	 * Liste des occupations liées à ce planning, pour les structures passées en
	 * paramètre :
	 * 
	 * - occupations directes
	 * 
	 * - occupations héritées
	 * 
	 * @return
	 */
	public final NSArray<EOOccupation> getOccupationArray(NSArray<TravailAttendu> travailAttenduArray) {
		NSMutableArray<EOOccupation> occupationArray = new NSMutableArray<EOOccupation>();

		// occupations directes
		occupationArray.addObjectsFromArray(getEoOccupationDirecte());

		// occupations héritées
		occupationArray.addObjectsFromArray(
				tosOccupationHeritee(travailAttenduArray));

		return occupationArray.immutableClone();
	}

	// vidages

	/**
	 * Donne l'enregistrement de vidage associé
	 * 
	 * @param cviType
	 * @return
	 */
	public EOCetVidage getVidage(String cviType) {

		EOCetVidage vidage = null;

		EOQualifier qual = ERXQ.equals(EOCetVidage.CVI_TYPE_KEY, cviType);
		NSArray<EOCetVidage> array = tosCetVidage(qual);
		if (array.count() > 0) {
			vidage = array.objectAtIndex(0);
		}

		return vidage;
	}

	/**
	 * Le dernier enregistrement {@link EOPlanning} associé
	 * 
	 * @return
	 */
	public static EOPlanning findEoPlanningPourPersIdAndPeriode(
			EOEditingContext ec, Integer aPersId, EOPeriode eoPeriode, boolean isEoPlanningDoitEtreSurPeriode) {
		EOPlanning eoPlanning = null;

		NSArray<EOPlanning> eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
				ec, aPersId, EOPlanning.REEL, EOPlanning.OUI, null, null);

		if (eoPlanningArray.count() > 0) {

			// de préférence celui sur la période selectionnée
			EOQualifier qualPeriode = ERXQ.equals(EOPlanning.TO_PERIODE_KEY, eoPeriode);
			NSArray<EOPlanning> eoPlanningPeriodeArray = EOQualifier.filteredArrayWithQualifier(eoPlanningArray, qualPeriode);

			if (eoPlanningPeriodeArray.count() > 0) {
				eoPlanning = eoPlanningPeriodeArray.objectAtIndex(0);
			} else if (isEoPlanningDoitEtreSurPeriode) {
				// a défaut le dernier
				eoPlanning = eoPlanningArray.lastObject();
			}

		}

		return eoPlanning;
	}

	private Integer _plaId;

	/**
	 * Clé primaire
	 * 
	 * @return
	 */
	public Integer plaId() {
		if (_plaId == null) {
			_plaId = (Integer) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey(PLA_ID_KEY);
		}
		return _plaId;
	}

	/**
	 * Indique si la saisie d'une occupation saisie sur le réel est a propager le
	 * prévisionnel ou non
	 * 
	 * @return
	 */
	public final boolean isOccupationARepercuterSurPrevisionnel() {
		boolean isARepercuter = true;

		try {
			getCheckerEOPlanning().checkOccupationSurPrevisionnelPossible();
		} catch (Exception e) {
			isARepercuter = false;
		}

		return isARepercuter;
	}

	/**
	 * Filtrer une liste de plannings
	 * 
	 * @param eoPlanningArray
	 * @param eoPeriode
	 * @param eoStructure
	 * @return
	 */
	public final static NSArray<EOPlanning> filtrer(
			NSArray<EOPlanning> eoPlanningArray,
			EOPeriode eoPeriode,
			EOStructure eoStructure,
			NSMutableArray<IPersonne> personneArray,
			HamacApplicationUser applicationUser,
			boolean filtrerParDateExactePlanning
			) {

		NSMutableArray<EOPlanning> eoPlanningMutableArray = new NSMutableArray<EOPlanning>();

		// tableau de personne en entrée, mais sans rien
		if (personneArray != null &&
				personneArray.count() == 0) {
			return eoPlanningMutableArray;
		}

		// ne conserver que ceux autorisés sur la période
		eoPlanningArray = EOQualifier.filteredArrayWithQualifier(
				eoPlanningArray, ERXQ.equals(TO_PERIODE_KEY, eoPeriode));

		// filtrer sur un service
		if (eoStructure != null) {

			// seulement les plannings du service
			for (EOPlanning eoPlanning : eoPlanningArray) {

				boolean isOk = false;
				int i = 0;

				NSArray<A_EOSource> eoSourceArray = eoPlanning.getEoSourceArray();

				while (!isOk && i < eoSourceArray.count()) {
					if (eoSourceArray.objectAtIndex(i).toStructureAffectation() == eoStructure) {
						isOk = true;
					}
					i++;
				}

				if (isOk) {
					if (!eoPlanningMutableArray.containsObject(eoPlanning)) {
						eoPlanningMutableArray.add(eoPlanning);
					}
				}
			}

		} else {

			// tous en otant les éventuels doublons
			eoPlanningMutableArray = new NSMutableArray<EOPlanning>(NSArrayCtrl.removeDuplicate(eoPlanningArray));

		}

		// filtrer sur les personnes
		if (!NSArrayCtrl.isEmpty(personneArray)) {
			EOQualifier qual = null;

			for (IPersonne personne : personneArray) {
				qual = ERXQ.or(
						qual, ERXQ.equals(PERS_ID_KEY, personne.persId()));
			}

			eoPlanningMutableArray = new NSMutableArray<EOPlanning>(
					EOQualifier.filteredArrayWithQualifier(
							eoPlanningMutableArray, qual));

		}

		
		// Si pas admin et pas rh global, alors on filtre via la date du planning sur les structures accrédités
		// sauf si planning d'une personne cible accréditée
		if (!applicationUser.isSuperAdmin() && !applicationUser.isSuperFonctionnel() && filtrerParDateExactePlanning) {
			
			NSArray<EOStructure> structureAcrediteArray = applicationUser.getEoStructureAcrediteArray();
			NSArray<Integer> persIdAcrediteArray = applicationUser.getPersIdCibleAcrediteArray();
			NSTimestamp dateJour = DateCtrl.now();
			NSMutableArray<EOPlanning> eoPlanningMutableTmpArray = new NSMutableArray<EOPlanning>();
			
			for (EOPlanning eoPlanning : eoPlanningMutableArray) {
				NSArray<A_EOSource> eoSourceArray = eoPlanning.getEoSourceArray();

				if (persIdAcrediteArray.contains(eoPlanning.persId())) {
					eoPlanningMutableTmpArray.add(eoPlanning);
				}
				
				for (A_EOSource a_EOSource : eoSourceArray) {
					if (DateCtrl.isBetween(dateJour, a_EOSource.dDebut(), a_EOSource.dFin())
							|| (a_EOSource.dFin() == null && DateCtrl.isAfter(dateJour, a_EOSource.dDebut()))) {
						for (EOStructure structure : structureAcrediteArray) {
							if (structure.cStructure().equals(a_EOSource.cStructure())) {
								eoPlanningMutableTmpArray.add(eoPlanning);
							}
						}
					}
					
				}
			}
			
			eoPlanningMutableArray = new NSMutableArray<EOPlanning>(NSArrayCtrl.removeDuplicate(eoPlanningMutableTmpArray));
			
		}
		
		//
		NSArray<EOPlanning> eoPlanningArraySorted = eoPlanningMutableArray.immutableClone();

		// TODO classer alphabetiquement
		eoPlanningArraySorted = CktlSort.sortedArray(
				eoPlanningArraySorted, EOPlanning.TOS_INDIVIDU_KEY + "." + EOIndividu.NOM_PRENOM_AFFICHAGE_KEY);

		return eoPlanningArraySorted;

	}

	/**
	 * Effectuer une remise à zéro d'un planning (association, occupations, ...)
	 * 
	 * @return
	 */
	public final static boolean remettreAZero(EOEditingContext ec, Number persId, String temPersonneComposante, EOPeriode eoPeriode) {
		boolean isRazOk = true;

		// plannings concernés
		NSArray<EOPlanning> eoPlanningArray = findSortedEoPlanningArray(ec, persId, null, null, null, eoPeriode);

		// les horaires orphelins pour cette personne
		NSArray<EOHoraire> eoHoraireArray = EOHoraire.findHoraireArray(ec, persId);

		for (EOHoraire eoHoraire : eoHoraireArray) {
			if (eoHoraire.tosAssociationHoraire().count() == 0) {
				ec.deleteObject(eoHoraire);
			}
		}

		for (EOPlanning eoPlanning : eoPlanningArray) {

			supprimerOccupations(ec, eoPlanning);

			// TODO les annulations d'occupations ?

			// ne pas supprimer le réel, seulement changer son statut
			if (eoPlanning.isReel()) {
				eoPlanning.setToTypeStatutRelationship(
						EOTypeStatut.getEoTypeStatutNonValide(ec));
				supprimerAssociationHoraire(ec, eoPlanning);

				// forcer sa reconstruction
				eoPlanning.clearCache();

			} else {
				ec.deleteObject(eoPlanning);
			}

		}

		return isRazOk;
	}

	private static void supprimerAssociationHoraire(EOEditingContext ec,
			EOPlanning eoPlanning) {
		// virer les associations uniquement
		for (EOAssociationHoraire eoAsso : eoPlanning.tosAssociationHoraire()) {
			ec.deleteObject(eoAsso);
		}
	}

	private static void supprimerOccupations(EOEditingContext ec,
			EOPlanning eoPlanning) {
		// les occupations de cette personne pour la période (ne pas toucher
		// celles qui chevauchent d'autres périodes)
		NSArray<EOOccupation> eoOccupationArray = eoPlanning.findOccupation();

		for (EOOccupation eoOccupation : eoOccupationArray) {
			if (eoOccupation.isStrictementIncluseDansPeriode(eoPlanning.toPeriode())) {
				ec.deleteObject(eoOccupation);
			}
		}
	}


	/**
	 * Effectuer une suppression d'un planning (association, occupations, ...)
	 * 
	 * @return
	 */
	public final static boolean supprimer(EOEditingContext ec, Number persId, String temPersonneComposante, EOPeriode eoPeriode) {
		boolean isSuppressionOk = true;

		// plannings concernés
		NSArray<EOPlanning> eoPlanningArray = findSortedEoPlanningArray(ec, persId, null, null, null, eoPeriode);

		for (EOPlanning eoPlanning : eoPlanningArray) {
			supprimerOccupations(ec, eoPlanning);
			supprimerAssociationHoraire(ec, eoPlanning);
			ec.deleteObject(eoPlanning);
		}

		return isSuppressionOk;
	}
	
	
	/**
	 * Date de la dernière demande de modification : on prends la date max de
	 * dernière modification d'une association horaire
	 */
	public NSTimestamp dateDemande() {
		NSTimestamp dateDemande = null;

		NSArray<EOAssociationHoraire> assoArray = tosAssociationHoraire();
		dateDemande = (NSTimestamp) assoArray.valueForKey("@max." + EOAssociationHoraire.D_MODIFICATION_KEY);

		return dateDemande;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#dateEffective()
	 */
	public NSTimestamp dateEffective() {
		return dateDemande();
	}

	public boolean isOngletsVisibles() {
		return isOngletsVisibles;
	}

	public void setOngletsVisibles(boolean isOngletsVisibles) {
		this.isOngletsVisibles = isOngletsVisibles;
	}
}
