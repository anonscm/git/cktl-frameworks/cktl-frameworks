/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOHoraire
		extends _EOHoraire
		implements I_GestionAutoDCreationDModification {

	// FIXME interdire la saisie de cycle recouvrant plus de 2 jours !!!! car les
	// débits à la demi journée ne marcheront plus !!!!

	public EOHoraire() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	// XXX tmp pour affichage

	private NSMutableDictionary<String, EOStructure> eoStructureDico;

	public EOStructure getEoStructure(String cStructure) {
		if (eoStructureDico == null) {
			eoStructureDico = new NSMutableDictionary<String, EOStructure>();
		}
		EOStructure eoStructure = eoStructureDico.objectForKey(cStructure);
		if (eoStructure == null) {
			eoStructure = EOStructure.structurePourCode(editingContext(), cStructure);
			eoStructureDico.setObjectForKey(eoStructure, cStructure);
		}
		return eoStructure;
	}

	private Horaire _horaire;

	public void setHoraire(Horaire horaire) {
		_horaire = horaire;
	}

	public Horaire getHoraire() {
		return _horaire;
	}

	public String libelleComptabilise() {
		String libelle = "";

		libelle += horLibelle() + " (" + TimeCtrl.stringForMinutes(getHoraire().getDureeComptabilisee()) + ")";

		return libelle;
	}

	/**
	 * La liste des horaires d'une personne
	 * 
	 * @param ec
	 * @param persId
	 * @return
	 */
	public final static NSArray<EOHoraire> findHoraireArray(EOEditingContext ec, Number persId) {
		NSArray<EOHoraire> array = null;

		EOQualifier qual = ERXQ.equals(PERS_ID_KEY, persId);

		array = fetchAll(ec, qual);

		return array;
	}
}
