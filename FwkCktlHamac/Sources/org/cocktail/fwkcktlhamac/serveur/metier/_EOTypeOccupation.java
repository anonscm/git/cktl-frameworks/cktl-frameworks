/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeOccupation.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOTypeOccupation extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "TypeOccupation";
	public static final String ENTITY_TABLE_NAME = "HAMAC.TYPE_OCCUPATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "otypId";

	public static final String DATE_DEBUT_VALIDITE_KEY = "dateDebutValidite";
	public static final String DATE_FIN_VALIDITE_KEY = "dateFinValidite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HORAIRE_FORCE_TYPE_KEY = "horaireForceType";
	public static final String NB_JOUR_MAX_KEY = "nbJourMax";
	public static final String NB_JOUR_OUVRABLE_MAX_KEY = "nbJourOuvrableMax";
	public static final String NB_MAX_PAR_ANNEE_CIVILE_KEY = "nbMaxParAnneeCivile";
	public static final String NB_MAX_PAR_PERIODE_CONTRACTUEL_KEY = "nbMaxParPeriodeContractuel";
	public static final String NB_MAX_PAR_PERIODE_TITULAIRE_KEY = "nbMaxParPeriodeTitulaire";
	public static final String OTYP_CLASSE_JAVA_KEY = "otypClasseJava";
	public static final String OTYP_CODE_KEY = "otypCode";
	public static final String OTYP_DESCRIPTION_KEY = "otypDescription";
	public static final String OTYP_LIBELLE_KEY = "otypLibelle";
	public static final String OTYP_QUANTUM_KEY = "otypQuantum";
	public static final String OTYP_TEMOIN_RH_DROIT_KEY = "otypTemoinRhDroit";

// Attributs non visibles
	public static final String OTYP_ID_KEY = "otypId";
	public static final String TSV_ID_KEY = "tsvId";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_VALIDITE_COLKEY = "D_DEBUT_VALIDITE";
	public static final String DATE_FIN_VALIDITE_COLKEY = "D_FIN_VALIDITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HORAIRE_FORCE_TYPE_COLKEY = "HORAIRE_FORCE_TYPE";
	public static final String NB_JOUR_MAX_COLKEY = "NB_JOUR_MAX";
	public static final String NB_JOUR_OUVRABLE_MAX_COLKEY = "NB_JOUR_OUVRABLE_MAX";
	public static final String NB_MAX_PAR_ANNEE_CIVILE_COLKEY = "NB_MAX_PAR_ANNEE_CIVILE";
	public static final String NB_MAX_PAR_PERIODE_CONTRACTUEL_COLKEY = "NB_MAX_PAR_PERIODE_CONTRACTUEL";
	public static final String NB_MAX_PAR_PERIODE_TITULAIRE_COLKEY = "NB_MAX_PAR_PERIODE_TITULAIRE";
	public static final String OTYP_CLASSE_JAVA_COLKEY = "OTYP_CLASSE_JAVA";
	public static final String OTYP_CODE_COLKEY = "OTYP_CODE";
	public static final String OTYP_DESCRIPTION_COLKEY = "OTYP_DESCRIPTION";
	public static final String OTYP_LIBELLE_COLKEY = "OTYP_LIBELLE";
	public static final String OTYP_QUANTUM_COLKEY = "OTYP_QUANTUM";
	public static final String OTYP_TEMOIN_RH_DROIT_COLKEY = "OTYP_TEMOIN_RH_DROIT";

	public static final String OTYP_ID_COLKEY = "OTYP_ID";
	public static final String TSV_ID_COLKEY = "TSV_ID";


	// Relationships
	public static final String TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY = "tosRepartTypeOccupationTypeSolde";
	public static final String TO_TYPE_SEQUENCE_VALIDATION_KEY = "toTypeSequenceValidation";



	// Accessors methods
  public NSTimestamp dateDebutValidite() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_VALIDITE_KEY);
  }

  public void setDateDebutValidite(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_VALIDITE_KEY);
  }

  public NSTimestamp dateFinValidite() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_VALIDITE_KEY);
  }

  public void setDateFinValidite(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_VALIDITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer horaireForceType() {
    return (Integer) storedValueForKey(HORAIRE_FORCE_TYPE_KEY);
  }

  public void setHoraireForceType(Integer value) {
    takeStoredValueForKey(value, HORAIRE_FORCE_TYPE_KEY);
  }

  public Double nbJourMax() {
    return (Double) storedValueForKey(NB_JOUR_MAX_KEY);
  }

  public void setNbJourMax(Double value) {
    takeStoredValueForKey(value, NB_JOUR_MAX_KEY);
  }

  public Integer nbJourOuvrableMax() {
    return (Integer) storedValueForKey(NB_JOUR_OUVRABLE_MAX_KEY);
  }

  public void setNbJourOuvrableMax(Integer value) {
    takeStoredValueForKey(value, NB_JOUR_OUVRABLE_MAX_KEY);
  }

  public Integer nbMaxParAnneeCivile() {
    return (Integer) storedValueForKey(NB_MAX_PAR_ANNEE_CIVILE_KEY);
  }

  public void setNbMaxParAnneeCivile(Integer value) {
    takeStoredValueForKey(value, NB_MAX_PAR_ANNEE_CIVILE_KEY);
  }

  public Integer nbMaxParPeriodeContractuel() {
    return (Integer) storedValueForKey(NB_MAX_PAR_PERIODE_CONTRACTUEL_KEY);
  }

  public void setNbMaxParPeriodeContractuel(Integer value) {
    takeStoredValueForKey(value, NB_MAX_PAR_PERIODE_CONTRACTUEL_KEY);
  }

  public Integer nbMaxParPeriodeTitulaire() {
    return (Integer) storedValueForKey(NB_MAX_PAR_PERIODE_TITULAIRE_KEY);
  }

  public void setNbMaxParPeriodeTitulaire(Integer value) {
    takeStoredValueForKey(value, NB_MAX_PAR_PERIODE_TITULAIRE_KEY);
  }

  public String otypClasseJava() {
    return (String) storedValueForKey(OTYP_CLASSE_JAVA_KEY);
  }

  public void setOtypClasseJava(String value) {
    takeStoredValueForKey(value, OTYP_CLASSE_JAVA_KEY);
  }

  public String otypCode() {
    return (String) storedValueForKey(OTYP_CODE_KEY);
  }

  public void setOtypCode(String value) {
    takeStoredValueForKey(value, OTYP_CODE_KEY);
  }

  public String otypDescription() {
    return (String) storedValueForKey(OTYP_DESCRIPTION_KEY);
  }

  public void setOtypDescription(String value) {
    takeStoredValueForKey(value, OTYP_DESCRIPTION_KEY);
  }

  public String otypLibelle() {
    return (String) storedValueForKey(OTYP_LIBELLE_KEY);
  }

  public void setOtypLibelle(String value) {
    takeStoredValueForKey(value, OTYP_LIBELLE_KEY);
  }

  public String otypQuantum() {
    return (String) storedValueForKey(OTYP_QUANTUM_KEY);
  }

  public void setOtypQuantum(String value) {
    takeStoredValueForKey(value, OTYP_QUANTUM_KEY);
  }

  public Integer otypTemoinRhDroit() {
    return (Integer) storedValueForKey(OTYP_TEMOIN_RH_DROIT_KEY);
  }

  public void setOtypTemoinRhDroit(Integer value) {
    takeStoredValueForKey(value, OTYP_TEMOIN_RH_DROIT_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation toTypeSequenceValidation() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation)storedValueForKey(TO_TYPE_SEQUENCE_VALIDATION_KEY);
  }

  public void setToTypeSequenceValidationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation oldValue = toTypeSequenceValidation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SEQUENCE_VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SEQUENCE_VALIDATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde> tosRepartTypeOccupationTypeSolde() {
    return (NSArray)storedValueForKey(TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde> tosRepartTypeOccupationTypeSolde(EOQualifier qualifier) {
    return tosRepartTypeOccupationTypeSolde(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde> tosRepartTypeOccupationTypeSolde(EOQualifier qualifier, boolean fetch) {
    return tosRepartTypeOccupationTypeSolde(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde> tosRepartTypeOccupationTypeSolde(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde.TO_TYPE_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartTypeOccupationTypeSolde();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartTypeOccupationTypeSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY);
  }

  public void removeFromTosRepartTypeOccupationTypeSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde createTosRepartTypeOccupationTypeSoldeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeOccupationTypeSolde");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde) eo;
  }

  public void deleteTosRepartTypeOccupationTypeSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_TYPE_OCCUPATION_TYPE_SOLDE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosRepartTypeOccupationTypeSoldeRelationships() {
    Enumeration objects = tosRepartTypeOccupationTypeSolde().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartTypeOccupationTypeSoldeRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EORepartTypeOccupationTypeSolde)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTypeOccupation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeOccupation createEOTypeOccupation(EOEditingContext editingContext, NSTimestamp dateDebutValidite
, NSTimestamp dateFinValidite
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer horaireForceType
, String otypClasseJava
, String otypCode
, String otypDescription
, String otypLibelle
, String otypQuantum
, Integer otypTemoinRhDroit
, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation toTypeSequenceValidation			) {
    EOTypeOccupation eo = (EOTypeOccupation) createAndInsertInstance(editingContext, _EOTypeOccupation.ENTITY_NAME);    
		eo.setDateDebutValidite(dateDebutValidite);
		eo.setDateFinValidite(dateFinValidite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setHoraireForceType(horaireForceType);
		eo.setOtypClasseJava(otypClasseJava);
		eo.setOtypCode(otypCode);
		eo.setOtypDescription(otypDescription);
		eo.setOtypLibelle(otypLibelle);
		eo.setOtypQuantum(otypQuantum);
		eo.setOtypTemoinRhDroit(otypTemoinRhDroit);
    eo.setToTypeSequenceValidationRelationship(toTypeSequenceValidation);
    return eo;
  }

  
	  public EOTypeOccupation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeOccupation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeOccupation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeOccupation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeOccupation object = (EOTypeOccupation)createAndInsertInstance(editingContext, _EOTypeOccupation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeOccupation localInstanceIn(EOEditingContext editingContext, EOTypeOccupation eo) {
    EOTypeOccupation localInstance = (eo == null) ? null : (EOTypeOccupation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeOccupation#localInstanceIn a la place.
   */
	public static EOTypeOccupation localInstanceOf(EOEditingContext editingContext, EOTypeOccupation eo) {
		return EOTypeOccupation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOTypeOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeOccupation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeOccupation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeOccupation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeOccupation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeOccupation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeOccupation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeOccupation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeOccupation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
