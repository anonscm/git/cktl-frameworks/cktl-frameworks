/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartTypeOccupationTypeSolde.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EORepartTypeOccupationTypeSolde extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "RepartTypeOccupationTypeSolde";
	public static final String ENTITY_TABLE_NAME = "HAMAC.REPART_TYPE_OCC_TYPE_SOLDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rtoId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETAT_VALIDATION_KEY = "etatValidation";
	public static final String PRIORITE_KEY = "priorite";

// Attributs non visibles
	public static final String OTYP_ID_KEY = "otypId";
	public static final String RTO_ID_KEY = "rtoId";
	public static final String STYP_ID_KEY = "stypId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETAT_VALIDATION_COLKEY = "ETAT_VALIDATION";
	public static final String PRIORITE_COLKEY = "PRIORITE";

	public static final String OTYP_ID_COLKEY = "OTYP_ID";
	public static final String RTO_ID_COLKEY = "RTO_ID";
	public static final String STYP_ID_COLKEY = "STYP_ID";


	// Relationships
	public static final String TO_TYPE_OCCUPATION_KEY = "toTypeOccupation";
	public static final String TO_TYPE_SOLDE_KEY = "toTypeSolde";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String etatValidation() {
    return (String) storedValueForKey(ETAT_VALIDATION_KEY);
  }

  public void setEtatValidation(String value) {
    takeStoredValueForKey(value, ETAT_VALIDATION_KEY);
  }

  public Integer priorite() {
    return (Integer) storedValueForKey(PRIORITE_KEY);
  }

  public void setPriorite(Integer value) {
    takeStoredValueForKey(value, PRIORITE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation toTypeOccupation() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation)storedValueForKey(TO_TYPE_OCCUPATION_KEY);
  }

  public void setToTypeOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation oldValue = toTypeOccupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_OCCUPATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_OCCUPATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde toTypeSolde() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde)storedValueForKey(TO_TYPE_SOLDE_KEY);
  }

  public void setToTypeSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde oldValue = toTypeSolde();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SOLDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SOLDE_KEY);
    }
  }
  

/**
 * Créer une instance de EORepartTypeOccupationTypeSolde avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORepartTypeOccupationTypeSolde createEORepartTypeOccupationTypeSolde(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer priorite
, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation toTypeOccupation, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde toTypeSolde			) {
    EORepartTypeOccupationTypeSolde eo = (EORepartTypeOccupationTypeSolde) createAndInsertInstance(editingContext, _EORepartTypeOccupationTypeSolde.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPriorite(priorite);
    eo.setToTypeOccupationRelationship(toTypeOccupation);
    eo.setToTypeSoldeRelationship(toTypeSolde);
    return eo;
  }

  
	  public EORepartTypeOccupationTypeSolde localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartTypeOccupationTypeSolde)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartTypeOccupationTypeSolde creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartTypeOccupationTypeSolde creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORepartTypeOccupationTypeSolde object = (EORepartTypeOccupationTypeSolde)createAndInsertInstance(editingContext, _EORepartTypeOccupationTypeSolde.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartTypeOccupationTypeSolde localInstanceIn(EOEditingContext editingContext, EORepartTypeOccupationTypeSolde eo) {
    EORepartTypeOccupationTypeSolde localInstance = (eo == null) ? null : (EORepartTypeOccupationTypeSolde)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORepartTypeOccupationTypeSolde#localInstanceIn a la place.
   */
	public static EORepartTypeOccupationTypeSolde localInstanceOf(EOEditingContext editingContext, EORepartTypeOccupationTypeSolde eo) {
		return EORepartTypeOccupationTypeSolde.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EORepartTypeOccupationTypeSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartTypeOccupationTypeSolde fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartTypeOccupationTypeSolde fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartTypeOccupationTypeSolde eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartTypeOccupationTypeSolde)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartTypeOccupationTypeSolde fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartTypeOccupationTypeSolde fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartTypeOccupationTypeSolde eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartTypeOccupationTypeSolde)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartTypeOccupationTypeSolde fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartTypeOccupationTypeSolde eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartTypeOccupationTypeSolde ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartTypeOccupationTypeSolde fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
