/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.foundation.NSArray;

/**
 * Définit le shéma d'un objet dont le viseur peut être de nature différente.
 * Soit un agent, soit un groupe ou soit une composante.
 * 
 * Chaque cible est pointée par son persId (voir {@link EOPersonne#persId()}. On
 * doit pouvoir qualifier les groupes, afin de savoir, dans l'hypothèse ou c'est
 * une composante, s'il faut le considérer comme composante ou simple groupe.
 * 
 * Cas particulier : pour cibler tout l'établissement, il suffit de positionner
 * la racine et de la typer composante.
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_ToManyPersonneViseur
		extends A_ToManyPersonne {

	/**
	 * Le viseur de l'objet s'il s'agit d'un individu
	 * 
	 * @return
	 */
	public abstract NSArray<EOIndividu> tosIndividuViseur();

	/**
	 * Le propriètaire de l'objet s'il s'agit d'une structure
	 * 
	 * @return
	 */
	public abstract NSArray<EOStructure> tosStructureViseur();

	/**
	 * 
	 */
	public abstract void addToTosIndividuViseurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object);

	/**
	 * 
	 */
	public abstract void removeFromTosIndividuViseurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object);

	/**
	 * 
	 */
	public abstract void addToTosStructureViseurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object);

	/**
	 * 
	 */
	public abstract void removeFromTosStructureViseurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object);

	/**
   * 
   */
	public abstract void setPersIdViseur(Integer value);

	/**
	 * @return La personne proprietaire de l'objet.
	 */
	public IPersonne toPersonneViseur() {
		return toPersonne();
	}

	/**
	 * Affecte la proprietaire.
	 * 
	 * @param personne
	 */
	public void setToPersonneViseur(IPersonne personne) {
		setToPersonne(personne);
	}

	public abstract Integer persIdViseur();

	public Integer persId() {
		return persIdViseur();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#
	 * addToTosIndividuRelationship
	 * (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)
	 */
	@Override
	public void addToTosIndividuRelationship(EOIndividu object) {
		addToTosIndividuViseurRelationship(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#
	 * addToTosStructureRelationship
	 * (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)
	 */
	@Override
	public void addToTosStructureRelationship(EOStructure object) {
		addToTosStructureViseurRelationship(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#
	 * removeFromTosIndividuRelationship
	 * (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)
	 */
	@Override
	public void removeFromTosIndividuRelationship(EOIndividu object) {
		removeFromTosIndividuViseurRelationship(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#
	 * removeFromTosStructureRelationship
	 * (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)
	 */
	@Override
	public void removeFromTosStructureRelationship(EOStructure object) {
		removeFromTosStructureViseurRelationship(object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#setPersId(java
	 * .lang.Integer)
	 */
	@Override
	public void setPersId(Integer value) {
		setPersIdViseur(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#tosIndividu()
	 */
	@Override
	public NSArray<EOIndividu> tosIndividu() {
		return tosIndividuViseur();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.A_ToManyPersonne#tosStructure()
	 */
	@Override
	public NSArray<EOStructure> tosStructure() {
		return tosStructureViseur();
	}
}
