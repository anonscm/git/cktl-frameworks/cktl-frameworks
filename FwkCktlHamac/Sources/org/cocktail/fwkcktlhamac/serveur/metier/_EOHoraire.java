/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOHoraire.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOHoraire extends A_ToManyPersonneTypageComposante  {
	public static final String ENTITY_NAME = "Horaire";
	public static final String ENTITY_TABLE_NAME = "HAMAC.HORAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "horId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HOR_COULEUR_HTML_KEY = "horCouleurHtml";
	public static final String HOR_LIBELLE_KEY = "horLibelle";
	public static final String HOR_PAUSES_KEY = "horPauses";
	public static final String HOR_PLAGES_TRAVAIL_AM_KEY = "horPlagesTravailAm";
	public static final String HOR_PLAGES_TRAVAIL_PM_KEY = "horPlagesTravailPm";
	public static final String PERS_ID_KEY = "persId";
	public static final String TEM_PERSONNE_COMPOSANTE_KEY = "temPersonneComposante";

// Attributs non visibles
	public static final String HOR_ID_KEY = "horId";
	public static final String PLA_ID_KEY = "plaId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HOR_COULEUR_HTML_COLKEY = "HOR_COULEUR_HTML";
	public static final String HOR_LIBELLE_COLKEY = "HOR_LIBELLE";
	public static final String HOR_PAUSES_COLKEY = "HOR_PAUSES";
	public static final String HOR_PLAGES_TRAVAIL_AM_COLKEY = "HOR_PLAGES_TRAVAIL_AM";
	public static final String HOR_PLAGES_TRAVAIL_PM_COLKEY = "HOR_PLAGES_TRAVAIL_PM";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TEM_PERSONNE_COMPOSANTE_COLKEY = "TEM_COMPOSANTE";

	public static final String HOR_ID_COLKEY = "HOR_ID";
	public static final String PLA_ID_COLKEY = "PLA_ID";


	// Relationships
	public static final String TO_PLANNING_KEY = "toPlanning";
	public static final String TOS_ASSOCIATION_HORAIRE_KEY = "tosAssociationHoraire";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String horCouleurHtml() {
    return (String) storedValueForKey(HOR_COULEUR_HTML_KEY);
  }

  public void setHorCouleurHtml(String value) {
    takeStoredValueForKey(value, HOR_COULEUR_HTML_KEY);
  }

  public String horLibelle() {
    return (String) storedValueForKey(HOR_LIBELLE_KEY);
  }

  public void setHorLibelle(String value) {
    takeStoredValueForKey(value, HOR_LIBELLE_KEY);
  }

  public String horPauses() {
    return (String) storedValueForKey(HOR_PAUSES_KEY);
  }

  public void setHorPauses(String value) {
    takeStoredValueForKey(value, HOR_PAUSES_KEY);
  }

  public String horPlagesTravailAm() {
    return (String) storedValueForKey(HOR_PLAGES_TRAVAIL_AM_KEY);
  }

  public void setHorPlagesTravailAm(String value) {
    takeStoredValueForKey(value, HOR_PLAGES_TRAVAIL_AM_KEY);
  }

  public String horPlagesTravailPm() {
    return (String) storedValueForKey(HOR_PLAGES_TRAVAIL_PM_KEY);
  }

  public void setHorPlagesTravailPm(String value) {
    takeStoredValueForKey(value, HOR_PLAGES_TRAVAIL_PM_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String temPersonneComposante() {
    return (String) storedValueForKey(TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public void setTemPersonneComposante(String value) {
    takeStoredValueForKey(value, TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)storedValueForKey(TO_PLANNING_KEY);
  }

  public void setToPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning oldValue = toPlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLANNING_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire() {
    return (NSArray)storedValueForKey(TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier) {
    return tosAssociationHoraire(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier, boolean fetch) {
    return tosAssociationHoraire(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire> tosAssociationHoraire(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire.TO_HORAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosAssociationHoraire();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public void removeFromTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire createTosAssociationHoraireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AssociationHoraire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_ASSOCIATION_HORAIRE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire) eo;
  }

  public void deleteTosAssociationHoraireRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ASSOCIATION_HORAIRE_KEY);
  }

  public void deleteAllTosAssociationHoraireRelationships() {
    Enumeration objects = tosAssociationHoraire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosAssociationHoraireRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOHoraire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOHoraire createEOHoraire(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String horLibelle
, Integer persId
, String temPersonneComposante
			) {
    EOHoraire eo = (EOHoraire) createAndInsertInstance(editingContext, _EOHoraire.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setHorLibelle(horLibelle);
		eo.setPersId(persId);
		eo.setTemPersonneComposante(temPersonneComposante);
    return eo;
  }

  
	  public EOHoraire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOHoraire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOHoraire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOHoraire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOHoraire object = (EOHoraire)createAndInsertInstance(editingContext, _EOHoraire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOHoraire localInstanceIn(EOEditingContext editingContext, EOHoraire eo) {
    EOHoraire localInstance = (eo == null) ? null : (EOHoraire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOHoraire#localInstanceIn a la place.
   */
	public static EOHoraire localInstanceOf(EOEditingContext editingContext, EOHoraire eo) {
		return EOHoraire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOHoraire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOHoraire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOHoraire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOHoraire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOHoraire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOHoraire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOHoraire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOHoraire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOHoraire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOHoraire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOHoraire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOHoraire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOHoraire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
