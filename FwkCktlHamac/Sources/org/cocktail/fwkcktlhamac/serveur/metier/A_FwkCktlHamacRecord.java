/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXQ;

/**
 * Classe abstraite pour les entit̩s m̩tier
 * 
 * @author Xxxx YYYY <xxx.yyyy at cocktail.org>
 */
@SuppressWarnings("rawtypes")
public abstract class A_FwkCktlHamacRecord extends ERXGenericRecord {

	public static final String OUI = "O";
	public static final String NON = "N";

	private Map attributesTaillesMax;
	private NSArray attributesObligatoires;

	/**
	 * Verifie si la longueur d'une chaine est bien inferieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 *          -1 si illimite.
	 * @return true si l=-1 ou si le nombre de caracteres de s est inferieur ou
	 *         egal a l.
	 */
	protected boolean verifieLongueurMax(String s, int l) {
		return (l == -1 || s == null || s.trim().length() <= l);
	}

	/**
	 * Verifie si la longeur d'une chaine est bien superieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 * @return true si l=0 ou si le nombre de caracteres de s est superieur ou
	 *         egal a l.
	 */
	protected boolean verifieLongueurMin(String s, int l) {
		return (s == null || s.trim().length() >= l);
	}

	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim). A appeler en
	 * debut de ValidateObjectMetier.
	 */
	protected void trimAllString() {
		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String array_element = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(array_element) != null && obj.valueForKey(array_element) instanceof String) {
				obj.takeValueForKey(((String) obj.valueForKey(array_element)).trim(), array_element);
			}
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		if (eoenterpriseobject == null) {
			return null;
		} else {
			com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
			return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);
		}
	}

	public static A_FwkCktlHamacRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	/**
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites
	 *          Un tableau d'objets {@link ISpecificite}. Ce tableau est affecte a
	 *          l'objet juste apres sa creation, avant l'insertion dans l'editing
	 *          context.
	 * @return
	 */
	public static A_FwkCktlHamacRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		} else {
			A_FwkCktlHamacRecord eoenterpriseobject = (A_FwkCktlHamacRecord) eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	/**
	 * @return Une map avec en cl̩ le nom de l'attribut et en valeur un integer
	 *         contenant la taille max de l'attribut.
	 */
	public Map attributesTaillesMax() {
		if (attributesTaillesMax == null) {
			attributesTaillesMax = attributeMaxSizesForEntityName(this.editingContext(), entityName());
		}
		return attributesTaillesMax;
	}

	public NSArray attributesObligatoires() {
		if (attributesObligatoires == null) {
			attributesObligatoires = requiredAttributeForEntityName(this.editingContext(), entityName());
		}
		return attributesObligatoires;
	}

	/**
	 * V̩rife si les champs obligatoires sont bien saisis (en fonction de ce qui
	 * est indiqu̩ dans le mod̬le)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesObligatoires() throws NSValidation.ValidationException {
		Iterator iterator = attributesObligatoires().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) == null) {
				throw new NSValidation.ValidationException("Le champ " + getDisplayName(key) + " est obligatoire.");
			}
		}
	}

	/**
	 * Renvoie le nom d'affichage de la propri̩t̩. Utilise {@link #displayNames()}
	 * .
	 * 
	 * @param propertyName
	 *          nom de la propri̩t̩ (attribut ou relation)
	 * @return
	 */
	public String getDisplayName(String propertyName) {
		if (displayNames().get(propertyName) != null) {
			return (String) displayNames().get(propertyName);
		}
		return propertyName;
	}

	/**
	 * Renvoie une Map contenant en cl̩ le nom de la propri̩t̩ et en valeur le nom
	 * d'affichage (parlant) de cette propri̩t̩. Par exemple <CP, Code Postal>.
	 * Par d̩faut la Map est vide, il faut surcharger la m̩thode.
	 */
	public Map displayNames() {
		return new HashMap();
	}

	/**
	 * Verifie la longueur maximale des champs saisis (a partir de la taille
	 * indiqu̩e dans le mod̬le).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesLongueursMax() throws NSValidation.ValidationException {
		Iterator iterator = attributesTaillesMax().keySet().iterator();
		// System.out.println(attributesTaillesMax().keySet());
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) != null && ((String) valueForKey(key)).length() > ((Integer) attributesTaillesMax().get(key)).intValue()) {
				throw new NSValidation.ValidationException("La taille du champ " + getDisplayName(key) + " ne doit pas d̩passer " + ((Integer) attributesTaillesMax().get(key)).intValue() + " caract̬res.");
			}
		}
	}

	/**
	 * @return true si l'objet possede un globalID temporaire (n'existe pas encore
	 *         dans la base de donn̩es).
	 */
	public boolean hasTemporaryGlobalID() {
		return (globalID() != null && globalID() instanceof EOTemporaryGlobalID);
	}

	/**
	 * @return le globalID de l'objet à partir de l'editingContext associ̩. Null
	 *         si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}

	public String dressExceptionMsg(Exception e) {
		return this.getClass().getSimpleName() + " : " + toDisplayString() + ":" + e.getLocalizedMessage();
	}

	public String toDisplayString() {
		return toString();
	}

	public static final String executeStoredProcedureNamed = "executeStoredProcedureNamed";
	public static final String primaryKeyForObject = "primaryKeyForObject";
	public static final String rawRowsForSQL = "rawRowsForSQL";
	public static final String objectsWithFetchSpecificationAndBindings = "objectsWithFetchSpecificationAndBindings";
	public static final String requiredAttributesForObject = "requiredAttributesForObject";
	public static final String attributesMaxSizesForObject = "attributesMaxSizesForObject";

	public static NSDictionary primaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) throws Exception {
		return EOUtilities.primaryKeyForObject(ec, eo);
	}

	public static NSArray rawRowsForSQL(EOEditingContext ec, String modelName, String sqlString, NSArray keys) throws Throwable {
		return EOUtilities.rawRowsForSQL(ec, modelName, sqlString, keys);
	}

	public static NSDictionary executeStoredProcedureNamed(EOEditingContext ec, String name, NSDictionary args) throws Throwable {
		return EOUtilities.executeStoredProcedureNamed(ec, name, args);
	}

	public static Map attributeMaxSizesForEntityName(EOEditingContext ec, String name) {

		HashMap res = new HashMap();

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";

		try {
			// Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width", new Class[] {});

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

			});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
						String.class
				});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
							attName
					});

				Number width = (Number) myeoAttribute_width.invoke(object, new Object[] {});
				if (width != null && width.intValue() > 0) {
					res.put(attName, Integer.valueOf(width.intValue()));
				}
			}

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray requiredAttributeForEntityName(EOEditingContext ec, String name) {

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";

		try {
			// Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_allowsNull = myeoAttribute.getDeclaredMethod("allowsNull", new Class[] {});

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

			});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
						String.class
				});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			NSMutableArray res = new NSMutableArray();
			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
							attName
					});
				Boolean xxx = (Boolean) myeoAttribute_allowsNull.invoke(object, new Object[] {});
				if (!xxx.booleanValue()) {
					res.addObject(attName);
				}
			}
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray objectsWithFetchSpecificationAndBindings(EOEditingContext ec, String entityName, String fetchSpecName, NSDictionary bindings) throws Throwable {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, entityName, fetchSpecName, bindings);
	}

	public static NSArray objectsMatchingKeyAndValue(EOEditingContext ec, String entityName, String key, Object value) {
		NSDictionary dict = new NSDictionary(value, key);
		NSArray results = objectsMatchingValues(ec, entityName, dict);
		return results;
	}

	public static NSArray objectsMatchingValues(EOEditingContext ec, String name, NSDictionary values) {
		EOQualifier qualifier = EOQualifier.qualifierToMatchAllValues(values);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(name, qualifier, null);
		NSArray results = ec.objectsWithFetchSpecification(fetchSpec);
		return results;
	}

	/**
	 * @param editingContext
	 * @return L'editingContext de plus haut niveau (avant enregistrement dans la
	 *         base ). Renvoi null si editingContext est nul.
	 */
	public static EOEditingContext getTopLevelEditingContext(EOEditingContext editingContext) {
		if (editingContext == null) {
			return null;
		}
		EOEditingContext edc = editingContext;
		while (!(edc.parentObjectStore() instanceof EOObjectStoreCoordinator)) {
			edc = (EOEditingContext) edc.parentObjectStore();
		}
		return edc;
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void validateForInsert() throws NSValidation.ValidationException {
		if (this instanceof I_GestionAutoDCreationDModification) {
			NSTimestamp now = DateCtrlHamac.now();
			((I_GestionAutoDCreationDModification) this).setDCreation(now);
			((I_GestionAutoDCreationDModification) this).setDModification(now);
		}
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		if (this instanceof I_GestionAutoDCreationDModification) {

			boolean shouldChangeDModification = false;
			// on met a jour la date uniquement si des attributs ont changé
			NSDictionary<String, ?> dico = changesFromSnapshot(editingContext().committedSnapshotForObject(this));
			NSArray<String> keys = dico.allKeys();
			NSArray<String> attributeKeys = attributeKeys();
			for (int i = 0; i < keys.count() && !shouldChangeDModification; i++) {
				String key = keys.objectAtIndex(i);
				if (attributeKeys.containsObject(key)) {
					shouldChangeDModification = true;
				}
			}
			if (shouldChangeDModification) {
				((I_GestionAutoDCreationDModification) this).setDModification(DateCtrlHamac.now());
			}
		}
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
	}

	/**
	 * Liste des objets repondant à la requete. A utiliser si ces derniers sont
	 * dans le {@link EOEditingContext} et pas encore en base de données
	 * 
	 * @param ec
	 * @param entityName
	 * @param qual
	 * @return
	 */
	public static NSArray<EOEnterpriseObject> getObjectsInsertedInContextMatching(
			EOEditingContext ec, String entityName, EOQualifier qual) {

		NSArray<EOEnterpriseObject> array = new NSArray<EOEnterpriseObject>();

		if (ec.insertedObjects().count() > 0) {
			EOQualifier qualEntity = ERXQ.equals("entityName", entityName);
			NSArray<EOEnterpriseObject> eoArrayForEntity = EOQualifier.filteredArrayWithQualifier(
					ec.insertedObjects(), qualEntity);

			if (eoArrayForEntity.count() > 0) {
				array = EOQualifier.filteredArrayWithQualifier(eoArrayForEntity, qual);
			}
		}

		return array;

	}
}
