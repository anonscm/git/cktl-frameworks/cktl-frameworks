/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactory;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetCredit;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOCetSaisieManuelle
		extends _EOCetSaisieManuelle
		implements I_Solde, I_CetCredit {

	public EOCetSaisieManuelle() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	/**
	 * Affichage dans la console
	 */
	public String toString() {
		String toString = "";

		toString = "Saisie Manuelle au " + DateCtrlHamac.dateToString(csmDateValeur()) +
				" \"" + csmMotif() + "\" " + FormatterCtrl.getAffichageHeuresSoitEnJourAXHeures(
						csmValeur().intValue(), HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, EOPeriode.getCurrentPeriode(editingContext())));

		return toString;
	}

	public boolean isCongeNatif() {
		return false;
	}

	public boolean isReliquatNatif() {
		return false;
	}

	public boolean isBalance() {
		return false;
	}

	public boolean isRegularisation() {
		return false;
	}

	public boolean isCongeLegal() {
		return false;
	}

	public boolean isCongeRh() {
		return false;
	}

	public boolean isDechargeSyndicale() {
		return false;
	}

	public boolean isJrti() {
		return false;
	}

	public String libelle() {
		return "CET : Saisie manuelle " + TimeCtrl.stringForMinutes(csmValeur()) + " au " + DateCtrlHamac.dateToString(csmDateValeur());
	}

	public NSTimestamp dDeb() {
		return csmDateValeur();
	}

	public NSTimestamp dFin() {
		return null;
	}

	private SoldeDelegate _soldeDelegate;

	public SoldeDelegate getSoldeDelegate() {
		if (_soldeDelegate == null) {
			_soldeDelegate = new SoldeDelegate(this);
		}
		return _soldeDelegate;
	}

	public void clearCache() {
		_soldeDelegate = null;
	}

	public NSMutableArray<I_Operation> getOperationArray() {
		return null;
	}

	public void setRestant(Integer restant) {

	}

	public String cStructure() {
		return null;
	}

	public boolean isCet() {
		return true;
	}

	public boolean isReliquatManuel() {
		return false;
	}

	public boolean isCongeManuel() {
		return false;
	}

	public boolean isIndemCet20081136() {
		return false;
	}

	public boolean isRecuperation() {
		return false;
	}

	public boolean isHeureSupplementaireManuel() {
		return false;
	}

	public boolean isHeureSupplementaireNonValide() {
		return false;
	}

	public boolean isReliquatHsup() {
		return false;
	}

	public Integer credit() {
		if (csmValeur() != null && csmValeur() >= 0) {
			return csmValeur();
		} 
		return null;
	}
	
	public NSTimestamp dateValeur() {
		return csmDateValeur();
	}

	/**
	 * Date par défaut pour une nouvelle saisie manuelle
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public static NSTimestamp getDefautDatePourPeriode(NSTimestamp dateReference) {
		NSTimestamp date = null;

		date = DateCtrlHamac.date1erJanAnneeUniv(dateReference);

		return date;
	}

	/**
	 * Indique si la transaction concerne une opération sur le régime pérenne ou
	 * non. On se base sur la date de valeur de la transaction, le régime pérenne
	 * s'applique après le 31/12/2009
	 * 
	 * @return
	 */
	public boolean isTransactionRegimePerenne() {
		return DateCtrlHamac.isAfterEq(dateValeur(), CetFactory.getDateCetDebutRegimePerenne());
	}

	// suppression

	/**
	 * Indique s'il est possible d'effacer cette transaction
	 * 
	 * @see CetEtat
	 */
	public boolean isSuppressionAutorisee() {
		boolean isSuppressionAutorisee = true;

		// ne pas autoriser la suppression pour des transactions
		// de l'ancien régime ayant des rachats de CET 2008-1136
		if (!isTransactionRegimePerenne()) {

			// sur la base
			EOQualifier qual20081136 = ERXQ.and(
					ERXQ.equals(EOSolde.TO_PLANNING_KEY + "." + EOPlanning.PERS_ID_KEY, persId()),
					ERXQ.equals(EOSolde.TO_PLANNING_KEY + "." + EOPlanning.NATURE, EOPlanning.REEL),
					ERXQ.greaterThan(EOSolde.INIT_KEY, Integer.valueOf(0)));

			NSArray<EOSolde> array = EOSolde.fetchAll(
					editingContext(), qual20081136);

			// sur le méthodes java
			array = EOQualifier.filteredArrayWithQualifier(
					array, ERXQ.isTrue(EOSolde.IS_INDEM_CET_2008_1136_KEY));

			if (array.count() > 0) {
				isSuppressionAutorisee = false;
			}
		}

		// ne pas autoriser les suppression pour des transactions ayant des
		// demandes ou décisions sur l'ancien régime
		if (isSuppressionAutorisee &&
				!isTransactionRegimePerenne()) {

			// recherche de mouvements sur l'ancien régime
			EOQualifier qual = ERXQ.and(
					ERXQ.equals(EOCetTransactionAnnuelle.PERS_ID_KEY, persId()),
					ERXQ.equals(EOCetTransactionAnnuelle.NATURE_KEY, EOPlanning.REEL));

			NSArray<EOCetTransactionAnnuelle> array = EOCetTransactionAnnuelle.fetchAll(
					editingContext(), qual);

			array = EOQualifier.filteredArrayWithQualifier(
					array, ERXQ.isTrue(EOCetTransactionAnnuelle.IS_DEMANDE_OU_DECISION_FAITE_SUR_ANCIEN_REGIME_KEY));

			if (array.count() > 0) {
				isSuppressionAutorisee = false;
			}

		}

		return isSuppressionAutorisee;
	}

	/**
	 * Liste des transactions associées à un pers_id (sur le planning réel)
	 * 
	 * @return
	 */
	public static NSArray<I_Solde> getTransactionAsSoldeArray(EOEditingContext ec, Integer persId) {

		NSArray<I_Solde> array = new NSArray<I_Solde>();

		// la liste des transactions manuelles
		EOQualifier qual = ERXQ.and(
				ERXQ.equals(PERS_ID_KEY, persId),
				ERXQ.equals(NATURE_KEY, EOPlanning.REEL));

		NSArray<EOCetSaisieManuelle> saisieArray = EOCetSaisieManuelle.fetchAll(ec, qual);

		for (EOCetSaisieManuelle saisie : saisieArray) {
			array = array.arrayByAddingObject(saisie);
		}

		return array;
	}

	public boolean isAncienRegime() {
		boolean isAncienRegime = false;

		if (DateCtrlHamac.isBeforeEq(dateValeur(), CetFactory.getDateCetDebutRegimePerenne())) {
			isAncienRegime = true;
		}
		return isAncienRegime;
	}

	/**
	 * Liste des transactions. Interroge d'abord la mémoire puis la base de
	 * données.
	 * 
	 * @return
	 */
	public static NSArray<EOCetSaisieManuelle> getTransactionArrayFor(
			EOEditingContext ec, String nature, Integer persId, NSTimestamp avantDate) {
		NSArray<EOCetSaisieManuelle> array = new NSArray<EOCetSaisieManuelle>();

		EOQualifier qual = ERXQ.and(
				ERXQ.equals(NATURE_KEY, nature),
				ERXQ.equals(PERS_ID_KEY, persId),
				ERXQ.lessThanOrEqualTo(CSM_DATE_VALEUR_KEY, avantDate));

		NSArray<EOEnterpriseObject> enterpriseObjectArray = getObjectsInsertedInContextMatching(ec, ENTITY_NAME, qual);

		if (enterpriseObjectArray.count() > 0) {

			for (EOEnterpriseObject object : enterpriseObjectArray) {
				array = array.arrayByAddingObject((EOCetSaisieManuelle) object);
			}

		} else {
			// en base
			array = fetchAll(ec, qual);
		}

		return array;
	}

	public Integer debit() {
		if (csmValeur() != null && csmValeur() < 0) {
			return -csmValeur();
		}
		return null;
	}
}
