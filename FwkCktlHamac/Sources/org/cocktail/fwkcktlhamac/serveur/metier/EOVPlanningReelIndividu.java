/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOVPlanningReelIndividu extends _EOVPlanningReelIndividu {

	public EOVPlanningReelIndividu() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	/**
	 * Retourne la chaine qualifier permettant d'effectuer une recherche a partir
	 * de l'entite <code>VPlanningReelIndividu</code>
	 * 
	 * @param searchString
	 *          : liste de mots a rechercher, séparés par des espaces
	 * 
	 *          TODO tenir compte de plus d'un espace, de la structure
	 * 
	 * @return
	 */
	private final static EOQualifier getStringQualifierNomPrenom(String searchString) {
		NSArray<String> arrSearch = NSArray.componentsSeparatedByString(searchString.toUpperCase(), " ");

		EOQualifier qual = null;

		if (arrSearch.count() > 0) {

			String firstWord = (String) arrSearch.objectAtIndex(0);

			if (arrSearch.count() == 1) {

				// 1 seul mot : on cherche sur le nom / prenom
				qual = ERXQ.or(
						ERXQ.likeInsensitive(NOM_USUEL_KEY, "*" + firstWord + "*"),
						ERXQ.likeInsensitive(PRENOM_KEY, "*" + firstWord + "*"));

			} else if (arrSearch.count() >= 2) {

				String secondWord = (String) arrSearch.objectAtIndex(1);

				// 2 mots : on cherche dans le couple nom / prenom
				qual = ERXQ.or(
						ERXQ.likeInsensitive(NOM_USUEL_KEY, "*" + firstWord + "*" + secondWord + "*"),
						ERXQ.likeInsensitive(NOM_USUEL_KEY, "*" + secondWord + "*" + firstWord + "*"),
						ERXQ.likeInsensitive(PRENOM_KEY, "*" + firstWord + "*" + secondWord + "*"),
						ERXQ.likeInsensitive(PRENOM_KEY, "*" + secondWord + "*" + firstWord + "*"),
						ERXQ.and(
								ERXQ.likeInsensitive(NOM_USUEL_KEY, "*" + firstWord + "*"),
								ERXQ.likeInsensitive(PRENOM_KEY, "*" + secondWord + "*")),
						ERXQ.and(
								ERXQ.likeInsensitive(NOM_USUEL_KEY, "*" + secondWord + "*"),
								ERXQ.likeInsensitive(PRENOM_KEY, "*" + firstWord + "*"))
						);
			}

			// ajouter close sur persId et noIndividu
			for (String str : arrSearch) {

				try {
					int n = Integer.parseInt(str);

					qual = ERXQ.or(qual,
							ERXQ.equals(PERS_ID_KEY, Integer.valueOf(n)),
							ERXQ.equals(NO_INDIVIDU_KEY, Integer.valueOf(n)));

				} catch (Exception e) {

				}

			}

		}

		return qual;
	}

	/**
	 * Effectuer une recherche
	 * 
	 * @param ec
	 * @param searchString
	 * @param listePeriode
	 * @param eoPlanningArray
	 *          a renseigner s'il faut filtrer sur une liste pré-existante
	 * @return
	 */
	public final static NSArray<EOVPlanningReelIndividu> findRecordsLike(
			EOEditingContext ec, String searchString, List<EOPeriode> listePeriode, NSArray<EOPlanning> eoPlanningArray) {

		NSArray<EOVPlanningReelIndividu> array = new NSArray<EOVPlanningReelIndividu>();

		if (StringCtrl.isEmpty(searchString) ||
				NSArrayCtrl.isEmpty(eoPlanningArray)) {
			return array;
		}

		EOQualifier qual = getStringQualifierNomPrenom(searchString);

		if (CollectionUtils.isNotEmpty(listePeriode)) {
			
			NSArray<EOQualifier> qualPeriode = new NSMutableArray<EOQualifier>();
			
			for (EOPeriode periode : listePeriode) {

				qualPeriode.add(ERXQ.equals(PER_LIBELLE_KEY, periode.perLibelle()));
				
			}
			qual = ERXQ.and(
					qual, ERXQ.or(qualPeriode));
		}

		array = fetchAll(ec, qual);

		// filtrer parmi la liste
		NSArray<EOQualifier> qualArray = new NSArray<EOQualifier>();

		for (EOPlanning eoPlanning : eoPlanningArray) {
			qualArray = qualArray.arrayByAddingObject(
						ERXQ.equals(TO_PLANNING_KEY, eoPlanning));
		}

		qual = ERXQ.or(qualArray);
		array = EOQualifier.filteredArrayWithQualifier(array, qual);

		return array;

	}
}
