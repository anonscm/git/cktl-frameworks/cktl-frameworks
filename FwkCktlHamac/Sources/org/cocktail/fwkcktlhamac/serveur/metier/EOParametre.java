/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.CktlSort;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEditingContextDelegate;
import er.extensions.eof.ERXQ;

public class EOParametre
		extends _EOParametre
		implements I_GestionAutoDCreationDModification {

	public EOParametre() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	//
	public final static String MODE_CALCUL_DEFAUT = "org.cocktail.hamac.calcul.defaut"; // OK
	public final static String UNITE = "org.cocktail.hamac.ui.unite"; // OK
	public final static String DUREE_JOUR = "org.cocktail.hamac.dureejour"; // OK
	public final static String DUREE_JOUR_AFFICHAGE = "org.cocktail.hamac.ui.dureejour"; // OK
	public final static String SEQUENCE_VALIDATION_PLANNING = "org.cocktail.hamac.planning.sequencevalidation"; // OK
	public final static String ADMIN_VOIR_DEMANDES_PERSONNELLES = "org.cocktail.hamac.validation.adminvoirdemandespersonnelles"; // OK
	public final static String JOURS_FERIES_COMPLEMENTAIRES = "org.cocktail.hamac.joursferiescomplementaires"; // OK
	public final static String JOURS_A_FORCER = "org.cocktail.hamac.joursaforcer"; // OK
	public final static String C_STRUCTURE_ADMIN_GLOBAL = "org.cocktail.hamac.droit.cstructureadminglobal"; // OK
	public final static String C_STRUCTURE_RH_GLOBAL = "org.cocktail.hamac.droit.cstructurerhglobal"; // OK

	// paramètres contractuels
	public final static String DUREE_JOUR_CONTRACTUEL = "org.cocktail.hamac.calcul.contractuel.dureejournee"; // OK
	public final static String SEUIL_MOIS_CHGT_MODE_CALCUL_CONTRACTUEL = "org.cocktail.hamac.calcul.contractuel.nombremoispourcalcultitulaire"; // OK
	public final static String NB_JOUR_CONGE_MENSUEL_CONTRACTUEL = "org.cocktail.hamac.calcul.contractuel.nombrejourmois"; // OK
	public final static String SEUIL_SEMAINES_CONTRATS_CONCOMITANTS_CONTRACTUEL = "org.cocktail.hamac.calcul.contractuel.nombresemainespourcontratsconcomitants"; // OK
	
	
	// parametres plage horaires
	public final static String AM_DEBUT_MINI = "org.cocktail.hamac.horaire.amdebutmini"; // OK
	public final static String AM_DEBUT_MAXI = "org.cocktail.hamac.horaire.amdebutmaxi"; // OK
	public final static String PM_FIN_MINI = "org.cocktail.hamac.horaire.pmfinmini"; // OK
	public final static String PM_FIN_MAXI = "org.cocktail.hamac.horaire.pmfinmaxi"; // OK
	public final static String PAUSE_RTT_GAIN = "org.cocktail.hamac.horaire.pauserttgain"; // OK
	public final static String PAUSE_MERIDIENNE_DUREE_MINI = "org.cocktail.hamac.horaire.pausemeridiennedureemini"; // OK
	public final static String PAUSE_MERIDIENNE_DEBUT_MINI = "org.cocktail.hamac.horaire.pausemeridiennedebutmini"; // OK
	public final static String PAUSE_MERIDIENNE_FIN_MAXI = "org.cocktail.hamac.horaire.pausemeridiennefinmaxi"; // OK
	public final static String PAUSE_RTT_DUREE = "org.cocktail.hamac.horaire.pauserttduree"; // OK
	public final static String DEMI_JOURNEE_DUREE_MAXI = "org.cocktail.hamac.horaire.demijourneedureemaxi"; // OK
	public final static String DUREE_HORAIRE_HEBDO_MINI = "org.cocktail.hamac.horaire.dureehebdomini"; // OK
	public final static String DUREE_HORAIRE_HEBDO_MAXI = "org.cocktail.hamac.horaire.dureehebdomaxi"; // OK
	public final static String DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES = "org.cocktail.hamac.horaire.dureehebdomaxihorsnormes"; // OK

	// parametres congés légaux
	public final static String DATE_FIN_APPLICATION_MALUS_90_180 = "org.cocktail.hamac.congeslegaux.datefinapplicationmalus90180"; // OK
	public final static String DATE_DEBUT_APPLICATION_MALUS_RTT = "org.cocktail.hamac.congeslegaux.datedebutapplicationmalusrtt"; // OK
	public final static String NB_JOUR_MALUS_RTT_PAR_AN = "org.cocktail.hamac.congeslegaux.nbjourmalusrttparan"; // OK
	public final static String TYPE_ABSENCE_LEGALE_MALUS_RTT = "org.cocktail.hamac.congeslegaux.typeabsencelegalemalusrtt"; // OK
	public final static String CONGES_LEGAUX_HORAIRE_FORCE = "org.cocktail.hamac.congeslegaux.horairetypeforce"; // OK
	public final static String DUREE_JOUR_MALUS_RTT = "org.cocktail.hamac.congeslegaux.dureejour"; // OK

	// bonifications hsup
	public final static String COEF_HSUP_DEBORDEMENT = "org.cocktail.hamac.hsup.coefdebordement"; // OK
	public final static String COEF_HSUP_SAM_MAT_5J = "org.cocktail.hamac.hsup.coefsammat5j"; // OK
	public final static String COEF_HSUP_SAM_APREM_DIM_JF = "org.cocktail.hamac.hsup.coefsamapremdimjf"; // OK
	public final static String COEF_HSUP_22H00_05H00 = "org.cocktail.hamac.hsup.coef22h00_05h00"; // OK
	public final static String HSUP_DUREE_MINI = "org.cocktail.hamac.hsup.dureemini"; // OK
	public final static String HSUP_SEUIL_MATIN_APRES_MIDI = "org.cocktail.hamac.hsup.seuilmatinapresmidi"; // OK
	public final static String HSUP_NB_MAX = "org.cocktail.hamac.hsup.nombremax"; // OK
	public final static String REPORT_HSUP_ACTIF = "org.cocktail.hamac.hsup.report"; // OK

	// a prendre en compte bonifications horaires
	public final static String MINUTES_HEBDO_MINI_HOR_BONUS = "org.cocktail.hamac.horaire.bonif.dureehebdomini"; // OK
	public final static String DEBUT_JOURNEE_BONUS = "org.cocktail.hamac.horaire.bonif.debutjourneefinbonus"; // OK
	public final static String FIN_JOURNEE_BONUS = "org.cocktail.hamac.horaire.bonif.finjourneedebutbonus"; // OK
	public final static String COEF_SAMEDI_MATIN_BONUS = "org.cocktail.hamac.horaire.bonif.coefsamedimatin"; // OK
	public final static String COEF_DEBORDEMENT_BONUS = "org.cocktail.hamac.horaire.bonif.coefdebordement"; // OK

	// paramètres annualisés
	public final static String MINUTES_DUES = "org.cocktail.hamac.calcul.titulaire.minutesdues"; // OK
	public final static String MINUTES_CONGES = "org.cocktail.hamac.calcul.titulaire.minutescongesmax"; // OK
	public final static String SEUIL_RECUPERATION = "org.cocktail.hamac.calcul.recuperation.seuil"; // OK
	public final static String DUREE_RECUPERATION_MOIS = "org.cocktail.hamac.calcul.recuperation.dureemois"; // OK
	public final static String DATE_FIN_VALIDITE_RELIQUAT = "org.cocktail.hamac.reliquat.finvalidite"; // OK
	public final static String DATE_CLOTURE_PERIODE = "org.cocktail.hamac.periode.datecloture"; // OK
	public final static String MAX_SEMAINE_HAUTES = "org.cocktail.hamac.semaineshautesmax"; // OK
	public final static String MAX_SEMAINE_HAUTES_PASSE_DROIT = "org.cocktail.hamac.semaineshautesmaxpassedroit"; // OK

	// systeme
	public final static String CONNECTEUR_TABLE = "org.cocktail.hamac.tableconnecteur"; // OK
	public final static String EMAIL_ACTIVER_REDIRECTION = "org.cocktail.hamac.mail.redirection"; // OK
	public final static String EMAIL_ADRESSE_REDIRECTION = "org.cocktail.hamac.mail.adresseredirection"; // OK
	public final static String URL_ANCIENNE_APPLICATION = "org.cocktail.hamac.urlancienneapplication";

	// email
	public final static String EMAIL_URL_APPLICATION_HAMAC = "org.cocktail.hamac.mail.urlapplication"; // OK

	// cet
	public final static String DATE_DEBUT_DEMANDE_CET = "org.cocktail.hamac.cet.dateouverture"; // OK
	public final static String DATE_FIN_DEMANDE_CET = "org.cocktail.hamac.cet.datefermeture"; // OK
	public final static String NOTE_CET_URL = "org.cocktail.hamac.cet.note"; // OK
	public final static String PLAFOND_EPARGNE_CET = "org.cocktail.hamac.cet.plafondepargne"; // OK
	public final static String SEUIL_RELIQUAT_EPARGNE_CET = "org.cocktail.hamac.cet.seuilreliquats"; // OK
	public final static String MESSAGE_EDITION_DEMANDE_EPARGNE_CET = "org.cocktail.hamac.cet.messageeditiondemandeepargne"; // OK
	public final static String VERIFIER_STATUT_DEMANDE_EPARGNE_CET = "org.cocktail.hamac.cet.verifierstatutdemandeepargne"; // OK
	public final static String AUTORISER_DEMANDE_EPARGNE_CET_CDD_NON_CDI = "org.cocktail.hamac.cet.autoriserepargnecddnoncdi"; // OK
	public final static String CET_OPTION_ANCIEN_SYSTEME_AU_DELA_20_JOURS = "org.cocktail.hamac.cet.optionanciensystemeaudela20jours"; // OK
	public final static String SIGNATURE_PRESIDENT = "org.cocktail.hamac.cet.signaturepresident"; // OK
	public final static String TITRE_RESPONSABLE_ETABLISSEMENT_DEMANDE = "org.cocktail.hamac.cet.titreresponsableetablissementdemande"; // OK

	// SAM
	public final static String SAM_FREQUENCE_RECHERCHE = "org.cocktail.hamac.sam.frequencerecherche"; // OK
	public final static String SAM_EMAIL_ADRESSE_PLUGIN = "org.cocktail.hamac.sam.email"; // OK
	public final static String SAM_ENCODER_CHAINE = "org.cocktail.hamac.sam.encoderchaine"; // OK

	// tableaux de bord
	public final static String STAT_POINT_INDICE = "org.cocktail.hamac.stat.pointindice"; // OK
	public final static String STAT_TAUX_CHARGE_FONCTIONNAIRE = "org.cocktail.hamac.stat.tauxchargefonctionnaire"; // OK
	public final static String STAT_TAUX_CHARGE_CONTRACTUEL = "org.cocktail.hamac.stat.tauxchargecontractuel"; // OK
	public final static String STAT_VALEUR_CET_INDEMNISATION_CAT_A = "org.cocktail.hamac.stat.valeurcetindemnisationcata"; // OK
	public final static String STAT_VALEUR_CET_INDEMNISATION_CAT_B = "org.cocktail.hamac.stat.valeurcetindemnisationcatb"; // OK
	public final static String STAT_VALEUR_CET_INDEMNISATION_CAT_C = "org.cocktail.hamac.stat.valeurcetindemnisationcatc"; // OK

	// doc
	public final static String DOCUMENTATION_CONTENU_HTML = "org.cocktail.hamac.doc.contenuhtml"; // OK

	//
	public final static String SUPPRESSION_OCCUPATION_ANCIENNE_VAL_SUP = "org.cocktail.hamac.occupation.suppoccpasseevalsup"; // OK

	// ********** VALEURS ***********
	public final static String CONNECTEUR_HAMACV2 = "HAMAC.V_CONNECTEUR_AFFECTATION";
	public final static String CONNECTEUR_HAMACV3 = "MANGUE.AFFECTATIONS_MANGUE_HAMAC";
	public final static String CONNECTEUR_JAVA = "HAMAC.CONNECTEUR";

	//

	private final static NSArray<String> PARAMETRE_INTERFACE_HEURE_SANS_LIMITE_ARRAY = new NSArray<String>(new String[] {
			MINUTES_DUES, MINUTES_CONGES, SEUIL_RECUPERATION,
			DUREE_HORAIRE_HEBDO_MINI, DUREE_HORAIRE_HEBDO_MAXI, DUREE_HORAIRE_HEBDO_MAXI_HORS_NORMES });

	private final static NSArray<String> PARAMETRE_INTERFACE_HEURE_LIMITE_24_ARRAY = new NSArray<String>(new String[] {
			DUREE_JOUR, DUREE_JOUR_CONTRACTUEL, DUREE_JOUR_MALUS_RTT,
			AM_DEBUT_MINI, AM_DEBUT_MAXI, PM_FIN_MINI,
			PM_FIN_MAXI, PAUSE_RTT_GAIN, PAUSE_MERIDIENNE_DUREE_MINI,
			PAUSE_MERIDIENNE_DEBUT_MINI, PAUSE_MERIDIENNE_FIN_MAXI, PAUSE_RTT_DUREE,
			DEMI_JOURNEE_DUREE_MAXI, HSUP_DUREE_MINI, HSUP_SEUIL_MATIN_APRES_MIDI,
			MINUTES_HEBDO_MINI_HOR_BONUS, DEBUT_JOURNEE_BONUS, FIN_JOURNEE_BONUS,
			DUREE_JOUR_AFFICHAGE });

	private final static NSArray<String> PARAMETRE_INTERFACE_DATE_ARRAY = new NSArray<String>(new String[] {
			DATE_FIN_VALIDITE_RELIQUAT, DATE_FIN_APPLICATION_MALUS_90_180, DATE_DEBUT_APPLICATION_MALUS_RTT,
			DATE_DEBUT_DEMANDE_CET, DATE_FIN_DEMANDE_CET , DATE_CLOTURE_PERIODE});

	private final static NSArray<String> PARAMETRE_BOOLEAN_ARRAY = new NSArray<String>(new String[] {
			EMAIL_ACTIVER_REDIRECTION, ADMIN_VOIR_DEMANDES_PERSONNELLES, VERIFIER_STATUT_DEMANDE_EPARGNE_CET,
			AUTORISER_DEMANDE_EPARGNE_CET_CDD_NON_CDI, CET_OPTION_ANCIEN_SYSTEME_AU_DELA_20_JOURS,
			SAM_ENCODER_CHAINE, SUPPRESSION_OCCUPATION_ANCIENNE_VAL_SUP, REPORT_HSUP_ACTIF });

	private final static NSArray<String> PARAMETRE_TEXTFIELD_TEXTE_ARRAY = new NSArray<String>(new String[] {
			EMAIL_URL_APPLICATION_HAMAC, EMAIL_ADRESSE_REDIRECTION, TYPE_ABSENCE_LEGALE_MALUS_RTT,
			C_STRUCTURE_ADMIN_GLOBAL, C_STRUCTURE_RH_GLOBAL, NOTE_CET_URL,
			SIGNATURE_PRESIDENT, SAM_EMAIL_ADRESSE_PLUGIN, JOURS_FERIES_COMPLEMENTAIRES,
			JOURS_A_FORCER, TITRE_RESPONSABLE_ETABLISSEMENT_DEMANDE, URL_ANCIENNE_APPLICATION });

	private final static NSArray<String> PARAMETRE_TEXTFIELD_ENTIER_ARRAY = new NSArray<String>(new String[] {
			SEUIL_MOIS_CHGT_MODE_CALCUL_CONTRACTUEL, SEUIL_SEMAINES_CONTRATS_CONCOMITANTS_CONTRACTUEL,
			DUREE_RECUPERATION_MOIS, NB_JOUR_MALUS_RTT_PAR_AN,
			PLAFOND_EPARGNE_CET, SEUIL_RELIQUAT_EPARGNE_CET, SAM_FREQUENCE_RECHERCHE,
			MAX_SEMAINE_HAUTES, MAX_SEMAINE_HAUTES_PASSE_DROIT, HSUP_NB_MAX });

	private final static NSArray<String> PARAMETRE_TEXTFIELD_FLOAT_ARRAY = new NSArray<String>(new String[] {
			NB_JOUR_CONGE_MENSUEL_CONTRACTUEL, COEF_HSUP_DEBORDEMENT, COEF_HSUP_SAM_MAT_5J,
			COEF_HSUP_SAM_APREM_DIM_JF, COEF_HSUP_22H00_05H00, COEF_SAMEDI_MATIN_BONUS,
			COEF_DEBORDEMENT_BONUS, STAT_TAUX_CHARGE_FONCTIONNAIRE, STAT_TAUX_CHARGE_CONTRACTUEL,
			STAT_POINT_INDICE, STAT_VALEUR_CET_INDEMNISATION_CAT_A, STAT_VALEUR_CET_INDEMNISATION_CAT_B,
			STAT_VALEUR_CET_INDEMNISATION_CAT_C });

	private final static NSArray<String> PARAMETRE_TEXT_ARRAY = new NSArray<String>(new String[] {
			MESSAGE_EDITION_DEMANDE_EPARGNE_CET, DOCUMENTATION_CONTENU_HTML });

	private final static NSArray<String> PARAMETRE_POPUP_ARRAY = new NSArray<String>(new String[] {
			CONNECTEUR_TABLE, MODE_CALCUL_DEFAUT, CONGES_LEGAUX_HORAIRE_FORCE,
			UNITE, SEQUENCE_VALIDATION_PLANNING });

	private final static NSArray<String> PARAMETRE_ANNUALISE_ARRAY = new NSArray<String>(new String[] {
			MINUTES_DUES, MINUTES_CONGES, DATE_FIN_VALIDITE_RELIQUAT, DATE_CLOTURE_PERIODE, 
			SEUIL_RECUPERATION, DUREE_RECUPERATION_MOIS, DATE_DEBUT_DEMANDE_CET,
			DATE_FIN_DEMANDE_CET, PLAFOND_EPARGNE_CET, SEUIL_RELIQUAT_EPARGNE_CET,
			MAX_SEMAINE_HAUTES, MAX_SEMAINE_HAUTES_PASSE_DROIT, STAT_POINT_INDICE,
			STAT_TAUX_CHARGE_FONCTIONNAIRE, STAT_TAUX_CHARGE_CONTRACTUEL, STAT_VALEUR_CET_INDEMNISATION_CAT_A,
			STAT_VALEUR_CET_INDEMNISATION_CAT_B, STAT_VALEUR_CET_INDEMNISATION_CAT_C, REPORT_HSUP_ACTIF, 
			DUREE_JOUR_CONTRACTUEL, NB_JOUR_CONGE_MENSUEL_CONTRACTUEL,
			MODE_CALCUL_DEFAUT, DUREE_JOUR, 
			CONGES_LEGAUX_HORAIRE_FORCE, NB_JOUR_MALUS_RTT_PAR_AN, TYPE_ABSENCE_LEGALE_MALUS_RTT,
			DUREE_JOUR_MALUS_RTT, MINUTES_HEBDO_MINI_HOR_BONUS, COEF_HSUP_SAM_MAT_5J,
			COEF_HSUP_SAM_APREM_DIM_JF, COEF_HSUP_22H00_05H00, HSUP_DUREE_MINI, HSUP_SEUIL_MATIN_APRES_MIDI,
			HSUP_NB_MAX, REPORT_HSUP_ACTIF, MINUTES_HEBDO_MINI_HOR_BONUS, DEBUT_JOURNEE_BONUS, 
			FIN_JOURNEE_BONUS, COEF_SAMEDI_MATIN_BONUS, COEF_DEBORDEMENT_BONUS, COEF_HSUP_DEBORDEMENT});

	
	public final boolean isTypeInterfaceHeuresSansLimite() {
		return isParametreContenuDans(paramKey(), PARAMETRE_INTERFACE_HEURE_SANS_LIMITE_ARRAY);
	}

	public final boolean isTypeInterfaceHeuresLimite24() {
		return isParametreContenuDans(paramKey(), PARAMETRE_INTERFACE_HEURE_LIMITE_24_ARRAY);
	}

	public final boolean isTypeInterfaceDate() {
		return isParametreContenuDans(paramKey(), PARAMETRE_INTERFACE_DATE_ARRAY);
	}

	public final boolean isTypeInterfaceCaseACocher() {
		return isParametreContenuDans(paramKey(), PARAMETRE_BOOLEAN_ARRAY);
	}

	public final boolean isTypeInterfaceTextfieldTexte() {
		return isParametreContenuDans(paramKey(), PARAMETRE_TEXTFIELD_TEXTE_ARRAY);
	}

	public final boolean isTypeInterfaceTextfieldEntier() {
		return isParametreContenuDans(paramKey(), PARAMETRE_TEXTFIELD_ENTIER_ARRAY);
	}

	public final boolean isTypeInterfaceTextfield1NombreApresVirgule() {
		return isParametreContenuDans(paramKey(), PARAMETRE_TEXTFIELD_FLOAT_ARRAY);
	}

	public final boolean isTypeInterfaceZoneDeTexte() {
		return isParametreContenuDans(paramKey(), PARAMETRE_TEXT_ARRAY);
	}

	public final boolean isTypeInterfacePopup() {
		return isParametreContenuDans(paramKey(), PARAMETRE_POPUP_ARRAY);
	}

	public final boolean isAnnualise() {
		return isParametreContenuDans(paramKey(), PARAMETRE_ANNUALISE_ARRAY);
	}

	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public final static boolean isParamKeyIsAnnualise(String paramKey) {
		return PARAMETRE_ANNUALISE_ARRAY.containsObject(paramKey);
	}

	public final static String PARAMETRE_TEXT_DEFAULT_VALUE = "à renseigner";

	/**
	 * La valeur par défaut d'un paramètre non annualisé
	 */
	public final static Object getValeurParDefautPourParamKey(String paramKey) {
		Object valeur = null;

		if (PARAMETRE_INTERFACE_HEURE_SANS_LIMITE_ARRAY.containsObject(paramKey) ||
					PARAMETRE_INTERFACE_HEURE_LIMITE_24_ARRAY.containsObject(paramKey) ||
					PARAMETRE_TEXTFIELD_ENTIER_ARRAY.containsObject(paramKey)) {
			valeur = Integer.valueOf(0);
		} else if (PARAMETRE_INTERFACE_DATE_ARRAY.containsObject(paramKey)) {
			valeur = DateCtrlHamac.now();
		} else if (PARAMETRE_BOOLEAN_ARRAY.containsObject(paramKey)) {
			valeur = Boolean.TRUE;
		} else if (PARAMETRE_TEXTFIELD_TEXTE_ARRAY.containsObject(paramKey) ||
					PARAMETRE_TEXT_ARRAY.containsObject(paramKey)) {
			valeur = PARAMETRE_TEXT_DEFAULT_VALUE;
		} else if (PARAMETRE_TEXTFIELD_FLOAT_ARRAY.containsObject(paramKey)) {
			valeur = new Float(0);
		} else if (PARAMETRE_POPUP_ARRAY.containsObject(paramKey)) {
			valeur = getParamValeurPossibleArray(paramKey);
		}

		return valeur;
	}

	/**
	 * Indique si le paramètre est contenu. Traite les cas ou des paramètres
	 * annualisés
	 * 
	 * @param paramKey
	 * @param array
	 * @return
	 */
	private final static boolean isParametreContenuDans(
			String paramKey, NSArray<String> array) {
		boolean isContenu = false;

		String paramKeyAnnualise = paramKey.substring(0, paramKey.lastIndexOf("."));

		boolean isAnnualise = false;
		if (PARAMETRE_ANNUALISE_ARRAY.containsObject(paramKeyAnnualise)) {
			isAnnualise = true;
		}

		if (isAnnualise) {
			if (array.containsObject(paramKeyAnnualise)) {
				isContenu = true;
			}
		} else {
			if (array.containsObject(paramKey)) {
				isContenu = true;
			}
		}

		return isContenu;
	}

	// transactions avec la base de données

	/** l'année universitaire si parametre annualisé */
	public NSTimestamp anneeUniv;
	/** la valeur du parametre en String */
	private String paramValueString;
	/** la valeur du parametre en NSTimestamp */
	private NSTimestamp paramValueNSTimestamp;
	/** la valeur du parametre en Boolean */
	private Boolean paramValueBoolean;
	/** la valeur du parametre en Integer */
	private Integer paramValueInteger;
	/** la valeur du parametre en Float */
	private Float paramValueFloat;

	/**
	 * Recuperer le parametre {@link String}
	 */
	public String getParamValueString() {
		paramValueString = paramValue();
		if (paramValueString == null || paramValueString.length() == 0) {
			System.err.println("Attention, la valeur du paramètre " + paramKey() + " est vide!");
		}
		return paramValueString;
	}

	/**
	 * Recuperer le parametre en {@link NSTimestamp}
	 */
	public NSTimestamp getParamValueNSTimestamp() {
		paramValueNSTimestamp = DateCtrlHamac.stringToDate(getParamValueString());
		return paramValueNSTimestamp;
	}

	/**
	 * Recuperer le parametre en {@link Boolean}
	 */
	public Boolean getParamValueBoolean() {
		paramValueBoolean = HamacCktlConfig.booleanForKey(paramKey()) ? Boolean.TRUE : Boolean.FALSE;
		return paramValueBoolean;
	}

	/**
	 * Recuperer le parametre en {@link Integer}
	 */
	public Integer getParamValueInteger() {
		paramValueInteger = Integer.valueOf(Integer.parseInt(getParamValueString()));
		return paramValueInteger;
	}

	/**
	 * Recuperer le parametre en {@link Float}
	 */
	public Float getParamValueFloat() {
		paramValueFloat = new Float(Float.parseFloat(getParamValueString()));
		return paramValueFloat;
	}

	// setters

	/**
	 * @param string
	 */
	public void setParamValueString(String string) {
		paramValueString = string;
		setParamValue(paramValueString);
		// sauvegarde
		try {
			editingContext().lock();
			editingContext().saveChanges();
			// forcer le rechargement des configs
			HamacCktlConfig.reset();
		} catch (Exception e) {
			e.printStackTrace();
			editingContext().revert();
		} finally {
			editingContext().unlock();
		}
	}

	/**
	 * @param nsTimestamp
	 */
	public void setParamValueNSTimestamp(NSTimestamp nsTimestamp) {
		paramValueNSTimestamp = nsTimestamp;
		setParamValueString(DateCtrlHamac.dateToString(paramValueNSTimestamp));
	}

	/**
	 * @param bolean
	 */
	public void setParamValueBoolean(Boolean bolean) {
		paramValueBoolean = bolean;
		setParamValueString(paramValueBoolean.booleanValue() == true ? OUI : NON);
	}

	/**
	 * @param integer
	 */
	public void setParamValueInteger(Integer integer) {
		paramValueInteger = integer;
		setParamValueString(Integer.toString(paramValueInteger.intValue()));
	}

	/**
	 * @param floate
	 */
	public void setParamValueFloat(Float floate) {
		paramValueFloat = floate;
		setParamValueString(Float.toString(paramValueFloat.floatValue()));
	}

	// valeurs possibles pour les popups

	private final static NSArray<String> getParamValeurPossibleArray(String paramKey) {
		NSMutableArray<String> array = new NSMutableArray<String>();

		if (paramKey.equals(CONNECTEUR_TABLE)) {
			array.addObject(CONNECTEUR_HAMACV2);
			array.addObject(CONNECTEUR_HAMACV3);
			array.addObject(CONNECTEUR_JAVA);
		} else if (StringUtils.startsWith(paramKey, MODE_CALCUL_DEFAUT)) {
			array.addObject(EOTypeCalcul.CODE_HEURES_ASSOCIEES_MOINS_HEURES_DUES);
			array.addObject(EOTypeCalcul.CODE_HEURES_ASSOCIEES_MOINS_HEURES_DUES_RECUPERATION);
			array.addObject(EOTypeCalcul.CODE_2_5_J);
		} else if (StringUtils.startsWith(paramKey, CONGES_LEGAUX_HORAIRE_FORCE)) {
			array.addObject(Integer.toString(EOTypeOccupation.TYPE_CONGE_HORAIRE_FORCE_NON));
			array.addObject(Integer.toString(EOTypeOccupation.TYPE_CONGE_HORAIRE_FORCE_SEMAINE));
		} else if (paramKey.equals(UNITE)) {
			array.addObject(Planning.TYPE_AFFICHAGE_HEURE);
			array.addObject(Planning.TYPE_AFFICHAGE_JOUR);
		} else if (paramKey.equals(SEQUENCE_VALIDATION_PLANNING)) {
			EOEditingContext ec = new EOEditingContext();
			ec.setDelegate(new ERXEditingContextDelegate());
			NSArray<EOTypeSequenceValidation> typeArray = EOTypeSequenceValidation.fetchAll(ec);
			for (EOTypeSequenceValidation type : typeArray) {
				array.addObject(type.tsvCode());
			}
		}
		return array.immutableClone();
	}

	public final NSArray<String> getParamValuePossibleArray() {
		return getParamValeurPossibleArray(paramKey());
	}

	/**
	 * Le paramètre sur la période précédente, les suivantes sinon
	 */
	private final static EOParametre getEoParametreExistantSurAutrePeriode(
			EOEditingContext ec, String paramKey, String paramKeyAnnualise) {
		EOParametre eoParametre = null;

		// forcément annualisé
		if (isParametreContenuDans(paramKeyAnnualise, PARAMETRE_ANNUALISE_ARRAY)) {

			// extraire la partie qualifiant la période
			String keyPeriode = paramKeyAnnualise.substring(paramKeyAnnualise.lastIndexOf(".") + 1);

			NSArray<EOPeriode> eoPeriodeArray = EOPeriode.findSortedPeriodeArray(ec, null, null);

			if (eoPeriodeArray.count() > 0) {

				EOQualifier qual = ERXQ.lessThan(EOPeriode.VALEUR_PARAMETRE_ANNUALISE_KEY, keyPeriode);
				NSArray<EOPeriode> eoPeriodePrecArray = EOQualifier.filteredArrayWithQualifier(eoPeriodeArray, qual);

				if (eoPeriodePrecArray.count() > 0) {
					// la précédente
					int i = 0;
					while (eoParametre == null && i < eoPeriodePrecArray.count()) {
						eoParametre = fetchByQualifier(
								ec, ERXQ.equals(PARAM_KEY_KEY, paramKey + "." + eoPeriodePrecArray.objectAtIndex(i).getValeurParametreAnnualise()));
						i++;
					}
				}

				// pas trouvé, on cherche dans les suivantes
				if (eoParametre == null) {
					qual = ERXQ.greaterThan(EOPeriode.VALEUR_PARAMETRE_ANNUALISE_KEY, keyPeriode);
					NSArray<EOPeriode> eoPeriodeSuivArray = EOQualifier.filteredArrayWithQualifier(eoPeriodeArray, qual);

					// la précédente
					int i = 0;
					while (eoParametre == null && i < eoPeriodeSuivArray.count()) {
						eoParametre = fetchByQualifier(
								ec, ERXQ.equals(PARAM_KEY_KEY, paramKey + "." + eoPeriodeSuivArray.objectAtIndex(i).getValeurParametreAnnualise()));
						i++;
					}

				}
				
				// pas trouvé, on cherche directement dans les paramètres
				if (eoParametre == null) {
					
					qual = ERXQ.startsWith(PARAM_KEY_KEY, paramKey + ".");
					CktlSort sort = CktlSort.newSort(D_MODIFICATION_KEY + "," + PARAM_ORDRE_KEY);

					NSArray<EOParametre> array = fetchAll(ec, qual, sort);
					
					if (!array.isEmpty()) {
						eoParametre = array.get(array.size() - 1);
					}
				}
			}

		}

		return eoParametre;
	}

	/**
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 */
	public final static EOParametre creerSiInexistant(
			EOEditingContext ec, Integer persId, String paramKey, String paramKeyAnnualise) {

		EOParametre eoParametre = null;

		// existant
		eoParametre = EOParametre.fetchByKeyValue(
				ec, EOParametre.PARAM_KEY_KEY, paramKeyAnnualise);

		if (eoParametre == null) {
			eoParametre = getEoParametreExistantSurAutrePeriode(ec, paramKey, paramKeyAnnualise);
			if (eoParametre != null) {
				EOParametre eoParametreNew = EOParametre.createEOParametre(
						ec, DateCtrlHamac.now(), DateCtrlHamac.now(), paramKeyAnnualise);
				eoParametreNew.setParamCommentaires(eoParametre.paramCommentaires());
				eoParametreNew.setPersIdCreation(persId);
				eoParametreNew.setPersIdModification(persId);
				eoParametreNew.setToParametresTypeRelationship(eoParametre.toParametresType());
				eoParametreNew.setParamValueString(eoParametre.paramValue());
				eoParametre = eoParametreNew;
			}
		}

		// création auto avec valeur par défaut
		if (eoParametre == null) {
			String strDefaultParamValue = "";
			eoParametre = EOParametre.createEOParametre(ec, DateCtrlHamac.now(), DateCtrlHamac.now(), paramKeyAnnualise);
			Object defaultParamValue = getValeurParDefautPourParamKey(paramKey);
			if (defaultParamValue instanceof NSTimestamp) {
				strDefaultParamValue = DateCtrlHamac.dateToString((NSTimestamp) defaultParamValue);
			} else if (defaultParamValue instanceof Boolean) {
				strDefaultParamValue = (((Boolean) defaultParamValue).booleanValue()) ? A_FwkCktlHamacRecord.OUI : A_FwkCktlHamacRecord.NON;
			} else if (defaultParamValue instanceof String) {
				strDefaultParamValue = (String) defaultParamValue;
			} else if (defaultParamValue instanceof Float) {
				strDefaultParamValue = Float.toString(((Float) defaultParamValue).floatValue());
			}
			eoParametre.setParamCommentaires("[a compléter]");
			eoParametre.setPersIdCreation(persId);
			eoParametre.setPersIdModification(persId);
			eoParametre.setParamValueString(strDefaultParamValue);
		}

		return eoParametre;
	}
}
