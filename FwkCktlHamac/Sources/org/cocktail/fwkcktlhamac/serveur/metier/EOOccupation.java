/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.A_OccupationTypeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.DemandeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HoraireJournalier;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePlageTravail;
import org.cocktail.fwkcktlhamac.serveur.metier.workflow.I_WorkflowItem;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.EOStructureFixed;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOOccupation
		extends _EOOccupation
		implements I_WorkflowItem, I_GestionAutoDCreationDModification {

	public EOOccupation() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		NSDictionary<String, ?> dico = changesFromSnapshot(editingContext().committedSnapshotForObject(this));
		NSArray<String> keys = dico.allKeys();

		// quelque chose a changé ?
		if (keys.count() > 0) {

			// on ne se pré-occupe pas s'il s'agit d'un visa ...
			boolean isSeulementVisa = false;
			if (keys.count() == 1 && keys.objectAtIndex(0).equals(TOS_VISA_KEY)) {
				isSeulementVisa = true;
			}

			// idem si c'est juste le changement de statut
			boolean isSeulementTypeStatut = false;
			if (keys.count() == 1 && keys.objectAtIndex(0).equals(TO_TYPE_STATUT_KEY)) {
				isSeulementTypeStatut = true;
			}

			// ou encore du cout
			boolean isSeulementCoutMinute = false;
			if (keys.count() == 1 && keys.objectAtIndex(0).equals(TOS_OCCUPATION_PLANNING_CALCUL_KEY)) {
				isSeulementCoutMinute = true;
			}

			boolean isValidationAFaire = true;

			if (isSeulementVisa || isSeulementTypeStatut || isSeulementCoutMinute) {
				isValidationAFaire = false;
			}

			if (isValidationAFaire) {
				// chargement de planning ...
				getOccupationTypeDelegate().validationGenerale();
			}

		}

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	private A_OccupationTypeDelegate _occupationTypeDelegate;

	/**
	 * 
	 */
	public final A_OccupationTypeDelegate getOccupationTypeDelegate() {
		if (_occupationTypeDelegate == null) {
			_occupationTypeDelegate = spawnTypeOccupationDelegateInstance(this);
		}
		return _occupationTypeDelegate;
	}

	/**
	 * retourne une instance du constructeur de la classe de gestion d'un type
	 * d'absence
	 * 
	 * @param absence
	 * @return
	 */
	private static A_OccupationTypeDelegate spawnTypeOccupationDelegateInstance(EOOccupation eoOccupation) {
		Class<?> theClass = null;
		try {
			theClass = Class.forName(eoOccupation.toTypeOccupation().otypClasseJava());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			Class<?> argumentTypes[] = { A_Absence.class };
			Object arguments[] = { eoOccupation };
			return (A_OccupationTypeDelegate) theClass.getConstructor(argumentTypes).newInstance(arguments);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static final EOQualifier QUAL_OCC_NON_ANNULE = ERXQ.and(ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_SUPPRIME),
				ERXQ.notEquals(TO_TYPE_STATUT + "." + EOTypeStatut.STA_CODE_KEY, EOTypeStatut.STATUT_CODE_REFUSE));
	
	

	/**
	 * Liste des occupations héritées pour le planning dont les services
	 * d'affectation sont passés en paramètre (pb d'ordre d'algo, voir pour virer
	 * ce paramètre)
	 * 
	 * @return
	 */
	protected final static NSArray<EOOccupation> getEoOccupationHeritee(
			EOPlanning eoPlanning, NSArray<TravailAttendu> travailAttenduArray) {

		NSMutableArray<EOOccupation> eoOccupationArray = null;

		// la période
		EOPeriode eoPeriode = eoPlanning.toPeriode();

		// les services
		NSMutableArray<EOQualifier> qualServiceOccArray = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> qualServiceAnnArray = new NSMutableArray<EOQualifier>();

		for (int i = 0; i < travailAttenduArray.count(); i++) {
			TravailAttendu travailAttendu = travailAttenduArray.objectAtIndex(i);

			EOStructure eoStructure = EOStructure.structurePourCode(
					eoPlanning.editingContext(), travailAttendu.getCStructure());

			EOQualifier qualPeriode = getQualifierAbsencesIncluesDansPeriode(
					null, OCC_DATE_DEBUT_KEY, OCC_DATE_FIN_KEY, travailAttendu.getDateDebut(), travailAttendu.getDateFin(), eoPeriode);

			EOQualifier qualCible = ERXQ.and(
					ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, NON),
					ERXQ.equals(PERS_ID_KEY, eoStructure.persId()));

			qualServiceOccArray.addObject(
					ERXQ.and(
							qualPeriode, qualCible));

			qualServiceAnnArray.add(
					qualCible);

		}

		// les composantes
		NSMutableArray<EOQualifier> qualComposanteOccArray = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> qualComposanteAnnArray = new NSMutableArray<EOQualifier>();

		for (int i = 0; i < travailAttenduArray.count(); i++) {
			TravailAttendu travailAttendu = travailAttenduArray.objectAtIndex(i);

			EOStructure eoComposante = EOStructureFixed.toComposante(
					EOStructure.structurePourCode(
							eoPlanning.editingContext(), travailAttendu.getCStructure()));

			EOQualifier qualPeriode = getQualifierAbsencesIncluesDansPeriode(
					null, OCC_DATE_DEBUT_KEY, OCC_DATE_FIN_KEY, travailAttendu.getDateDebut(), travailAttendu.getDateFin(), eoPeriode);

			EOQualifier qualCible = ERXQ.and(
					ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, OUI),
					ERXQ.equals(PERS_ID_KEY, eoComposante.persId()));

			qualComposanteOccArray.addObject(
					ERXQ.and(
							qualPeriode, qualCible));

			qualComposanteAnnArray.add(
					qualCible);

		}

		// les racines
		NSMutableArray<EOQualifier> qualRacineArray = new NSMutableArray<EOQualifier>();

		NSArray<EOStructure> racineArray = EOStructureForGroupeSpec.getGroupesRacine(eoPlanning.editingContext());

		for (int i = 0; i < racineArray.count(); i++) {
			EOStructure racine = racineArray.objectAtIndex(i);

			qualRacineArray.addObject(
					ERXQ.and(
							ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, OUI),
							ERXQ.equals(PERS_ID_KEY, racine.persId())));

		}

		EOQualifier qualServicesOcc = ERXQ.or(qualServiceOccArray);
		EOQualifier qualComposantesOcc = ERXQ.or(qualComposanteOccArray);
		EOQualifier qualRacines = ERXQ.or(qualRacineArray);

		// si les 3 qualifiers sont vides, on ne retourne rien !
		if (qualServicesOcc == null &&
				qualComposantesOcc == null &&
				qualRacines == null) {

			eoOccupationArray = new NSMutableArray<EOOccupation>();
			return eoOccupationArray;

		}

		// mix des 3 cibles
		EOQualifier qualCiblesOcc = ERXQ.or(
				qualServicesOcc,
				qualComposantesOcc,
				qualRacines);

		EOEditingContext editingContext = eoPeriode.editingContext();

		EOQualifier qualOcc = ERXQ.and(
				getQualPeriode(eoPlanning.toPeriode()),
				qualCiblesOcc);

		eoOccupationArray = new NSMutableArray(
				fetchAll(editingContext, qualOcc, null, true));

		// oter les annulations
		EOQualifier qualPerso =
				ERXQ.and(
						ERXQ.equals(EOAnnulation.TEM_PERSONNE_COMPOSANTE_KEY, eoPlanning.temPersonneComposante()),
						ERXQ.equals(EOAnnulation.PERS_ID_KEY, eoPlanning.persId()));

		EOQualifier qualServicesAnn = ERXQ.or(qualServiceAnnArray);
		EOQualifier qualComposantesAnn = ERXQ.or(qualComposanteAnnArray);

		EOQualifier qualCiblesAnn = ERXQ.or(
				qualServicesAnn,
				qualComposantesAnn,
				qualRacines);

		EOQualifier qualAnn = ERXQ.or(
				qualCiblesAnn, qualPerso);

		NSArray<EOAnnulation> eoAnnulationArray = EOAnnulation.fetchAll(
				editingContext, qualAnn, null, true);

		// annulations service / composante / racines
		NSArray<EOOccupation> eoOccupationAnnuleeArray =
				(NSArray<EOOccupation>) eoAnnulationArray.valueForKey(EOAnnulation.TO_OCCUPATION_KEY);

		// soustraction
		eoOccupationArray.removeObjectsInArray(eoOccupationAnnuleeArray);

		return eoOccupationArray.immutableClone();
	}

	@Override
	public NSTimestamp dateDebut() {
		return occDateDebut();
	}

	@Override
	public NSTimestamp dateFin() {
		return occDateFin();
	}

	@Override
	public String motif() {
		return occMotif();
	}

	@Override
	public int priorite() {
		int priorite = 3;
		// pour les heures supp, priorité supplémentaire
		if (isHeureSupplementaire()) {
			priorite = 2;
		}
		return priorite;
	}

	@Override
	public boolean isHoraireForceRecouvrementSemaine(EOPeriode periode) {
		boolean isForceSemaine = false;

		if (toTypeOccupation().horaireForceType().intValue() == EOTypeOccupation.TYPE_CONGE_HORAIRE_FORCE_SEMAINE) {
			isForceSemaine = true;
		}

		return isForceSemaine;
	}

	@Override
	public String libelleType() {
		String libelleType = null;

		libelleType = toTypeOccupation().otypLibelle();

		// information sur la provenance
		if (isEtablissement()) {
			libelleType += " (etab. " + toPersonne().persLibelleAffichage() + ")";
		} else if (isComposante()) {
			libelleType += " (composante " + toPersonne().persLibelleAffichage() + ")";
		} else if (isService()) {
			libelleType += " (service " + toPersonne().persLibelleAffichage() + ")";
		} else if (toTypeOccupation().isFermeture()) {
			// cas particulier de la fermeture, on indique si elle est liée à l'agent
			libelleType += " (agent)";
		}

		return libelleType;
	}

	@Override
	public boolean isValidee() {
		boolean isValidee = false;

		if (toTypeStatut().isStatutValide()) {
			isValidee = true;
		}

		return isValidee;
	}

	@Override
	public boolean isVisible() {
		boolean isVisible = true;

		if (toTypeStatut().isStatutRefuse() ||
				toTypeStatut().isStatutSupprime()) {
			isVisible = false;
		}

		return isVisible;
	}

	/**
	 * On mémorise que le débit CET ... à voir s'il faut mémoriser les autres
	 * types ?
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence#setCoutMinute
	 *      (Integer)
	 */
	@Override
	public void setCoutMinute(EOPlanning eoPlanning) {
		NSArray<EOOccupationPlanningCalcul> opcArray = null;

		int minutes = 0;

		EOQualifier qual = ERXQ.isTrue(I_Solde.IS_CET_KEY);

		NSArray<I_Solde> soldeCetArray =
				EOQualifier.filteredArrayWithQualifier(getOperationDelegate().getSoldeArray(), qual);

		for (I_Solde solde : soldeCetArray) {
			minutes += getOperationDelegate().getDebitSurSolde(solde);
		}

		if (minutes > 0) {

			opcArray = tosOccupationPlanningCalcul(
					ERXQ.equals(EOOccupationPlanningCalcul.TO_PLANNING_KEY, eoPlanning));

			if (opcArray.count() > 0) {
				// mise à jour
				EOOccupationPlanningCalcul opc = opcArray.objectAtIndex(0);
				opc.setCoutMinute(Integer.valueOf(minutes));

			} else {

				NSTimestamp now = DateCtrlHamac.now();
				// creation de l'enregistrement
				EOOccupationPlanningCalcul.createEOOccupationPlanningCalcul(
						editingContext(), Integer.valueOf(minutes), now, now, this, eoPlanning);

			}

		}

	}

	private DemandeDelegate _demandeDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#getDemandeDelegate
	 * ()
	 */
	public DemandeDelegate getDemandeDelegate() {
		if (_demandeDelegate == null) {
			_demandeDelegate = new DemandeDelegate(this);
		}
		return _demandeDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * toTypeSequenceValidation()
	 */
	public EOTypeSequenceValidation toTypeSequenceValidation() {

		EOTypeSequenceValidation toTypeSequenceValidation = null;

		// XXX cas d'une suppression d'un congé validé dont la date est passée, on
		// passe au niveau supérieur (RH général)
		boolean isSuppressionAFaireValiderSuperieur = HamacCktlConfig.booleanForKey(EOParametre.SUPPRESSION_OCCUPATION_ANCIENNE_VAL_SUP, false);
		if (isSuppressionAFaireValiderSuperieur && toTypeStatut().isStatutEnCoursDeSuppression() && getDemandeDelegate().isCommenceeOuPassee()) {
			toTypeSequenceValidation = toTypeOccupation().toTypeSequenceValidation().getEoTypeSequenceValidationSuperieur();
		} else {
			toTypeSequenceValidation = toTypeOccupation().toTypeSequenceValidation();
		}

		return toTypeSequenceValidation;
	}

	/**
	 * Obtenir la liste des services impactés par l'occupation. La recherche se
	 * fait sans passer par l'objet {@link Planning} afin d'avoir un traitement le
	 * plus rapide possible (utilisé par la liste des demandes). On passe par des
	 * méthodes qui ne charge pas les {@link Planning} mais par des connections
	 * manuelles.
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getEoStructureAFaireValiderArrayPourDemande() {
		NSMutableArray<String> array = new NSMutableArray<String>();

		if (isPresence()) {
			// le cas le plus facile ...
			return new NSArray<EOStructure>(toStructureApplication());
		}

		// long tDebut = System.currentTimeMillis();

		// liste des plannings
		NSArray<EOPlanning> eoPlanningArray = getEoPlanningArrayPourOccupation();

		for (EOPlanning eoPlanning : eoPlanningArray) {

			// liste des codes des semaines à tester
			NSMutableArray<String> codeSemaineArray = new NSMutableArray<String>();
			NSTimestamp date = dateDebut();
			while (DateCtrl.isBeforeEq(date, dateFin())) {
				String codeDate = Semaine.getCodeForDate(date);
				if (!codeSemaineArray.containsObject(codeDate)) {
					codeSemaineArray.addObject(codeDate);
				}
				date = date.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
			}

			for (String codeSemaine : codeSemaineArray) {

				EOQualifier qual = ERXQ.and(
						ERXQ.equals(EOAssociationHoraire.TO_PLANNING_KEY, eoPlanning),
						ERXQ.equals(EOAssociationHoraire.SEMAINE_CODE_KEY, codeSemaine));

				// il y en a au plus 1
				EOAssociationHoraire eoAsso = null;
				// NSArray<EOAssociationHoraire> eoAssoArray =
				// eoPlanning.tosAssociationHoraire(qual);
				NSArray<EOAssociationHoraire> eoAssoArray = EOQualifier.filteredArrayWithQualifier(eoPlanning.tosAssociationHoraire(), qual);
				if (eoAssoArray.count() > 0) {
					eoAsso = eoAssoArray.objectAtIndex(0);
				}

				if (eoAsso != null) {

					// horaire associé
					Horaire horaire = null;
					if (eoAsso.toHoraire() != null) {
						horaire = new Horaire(eoAsso.toHoraire(), eoPlanning.toPeriode());
					}

					if (horaire != null) {

						NSTimestamp dateDebutSemaine = Semaine.getDateForCode(codeSemaine);
						NSTimestamp dateFinSemaine = dateDebutSemaine.timestampByAddingGregorianUnits(0, 0, 6, 0, 0, 0);

						NSTimestamp dateDebutJour = dateDebut();

						if (DateCtrlHamac.isBefore(dateDebutJour, dateDebutSemaine)) {
							dateDebutJour = dateDebutSemaine;
						}

						NSTimestamp dateFinJour = dateFinSemaine;

						if (DateCtrl.isAfter(dateFinJour, dateFin())) {
							dateFinJour = dateFin();
						}

						while (DateCtrl.isBeforeEq(dateDebutJour, dateFinJour)) {

							// determiner sur quel horaire journalier se positionner
							GregorianCalendar gc = new GregorianCalendar();
							gc.setTime(dateDebutJour);
							int index = Horaire.getIndexForGCDayOfWeek(gc.get(GregorianCalendar.DAY_OF_WEEK));

							HoraireJournalier hj = horaire.getHoraireJournalierArray().objectAtIndex(index);

							for (HorairePlageTravail htp : hj.getHorairePlageTravailArray()) {

								if (isAbsenceDemiJournee()) {

									if (DateCtrlHamac.isDemiJourneeInclue(dateDebutJour, dateFinJour, htp.isAm())) {
										if (!array.containsObject(htp.getCStructure())) {
											array.add(htp.getCStructure());
										}
									}

								} else {

									// controle de chevauchement
									if (!(DateCtrlHamac.getMinutesDansJour(dateDebutJour) > htp.getFinMinutesDansJour() || DateCtrlHamac.getMinutesDansJour(dateFinJour) < htp.getDebutMinutesDansJour())) {
										if (!array.containsObject(htp.getCStructure())) {
											array.add(htp.getCStructure());
										}
									}

								}

							}

							dateDebutJour = dateDebutJour.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
						}

					}

				}

			}

			// System.out.println("array=" + array);

		}

		NSMutableArray<EOStructure> eoStructureArray = new NSMutableArray<EOStructure>();

		for (String cStructure : array) {
			EOStructure eoStructure = EOStructureFixed.getEoStructureInContext(
					editingContext(), cStructure);
			eoStructureArray.addObject(eoStructure);
		}

		// CktlLog.log("EOOccupation.getEoStructureArrayPourDemande() : " +
		// (System.currentTimeMillis() - tDebut) + "ms");

		return eoStructureArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * postTraitementChangementStatut
	 * (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)
	 */
	public void postTraitementChangementStatut(EOTypeStatut eoTypeStatutSuivant) {
		// TODO Auto-generated method stub

	}

	//

	/**
	 * Le qualifier pour interroger des {@link EOOccupation} sur une
	 * {@link EOPeriode}
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public static EOQualifier getQualPeriode(EOPeriode eoPeriode) {

		EOQualifier qualPeriode = ERXQ.not(
				ERXQ.or(
						ERXQ.lessThan(OCC_DATE_FIN_KEY, eoPeriode.perDDebut()),
						ERXQ.greaterThan(OCC_DATE_DEBUT_KEY, eoPeriode.perDFin())));

		return qualPeriode;

	}

	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public final static EOQualifier getQualifierInclusionPeriode(
			NSTimestamp dateDebut, NSTimestamp dateFin) {
		EOQualifier qual = ERXQ.not(
				ERXQ.and(
						ERXQ.greaterThan(
								EOPlanning.TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY, dateFin),
						ERXQ.greaterThan(
								EOPlanning.TO_PERIODE_KEY + "." + EOPeriode.PER_D_FIN_KEY, dateDebut)));

		return qual;
	}

	/**
	 * Liste des {@link EOOccupation} affectées directement aux racines sur la
	 * période
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EOOccupation> getEoOccupationRacine(
			EOEditingContext ec, EOPeriode eoPeriode, EOTypeOccupation eoTypeOccupation) {
		NSArray<EOOccupation> array = null;

		// les racines
		NSMutableArray<EOQualifier> qualRacineArray = new NSMutableArray<EOQualifier>();

		NSArray<EOStructure> racineArray = EOStructureForGroupeSpec.getGroupesRacine(ec);

		for (int i = 0; i < racineArray.count(); i++) {
			EOStructure racine = racineArray.objectAtIndex(i);

			qualRacineArray.addObject(
					ERXQ.and(
							ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, OUI),
							ERXQ.equals(PERS_ID_KEY, racine.persId())));

		}

		// + periode
		EOQualifier qual = ERXQ.and(
				new EOOrQualifier(qualRacineArray),
				getQualPeriode(eoPeriode),
				ERXQ.equals(TO_TYPE_OCCUPATION_KEY, eoTypeOccupation));

		array = fetchAll(ec, qual);

		return array;
	}

	/**
	 * Liste des {@link EOOccupation} affectées directement à la composante sur la
	 * période
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EOOccupation> getEoOccupationComposante(
			EOEditingContext ec, EOStructure composante, EOPeriode eoPeriode, EOTypeOccupation eoTypeOccupation) {
		NSArray<EOOccupation> array = null;

		// + periode
		EOQualifier qual = ERXQ.and(
				ERXQ.and(
						ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, OUI),
						ERXQ.equals(PERS_ID_KEY, composante.persId()),
						getQualPeriode(eoPeriode)),
						ERXQ.equals(TO_TYPE_OCCUPATION_KEY, eoTypeOccupation));

		array = fetchAll(ec, qual);

		return array;
	}

	/**
	 * Liste des {@link EOOccupation} affectées directement aux racines sur la
	 * période
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EOOccupation> getEoOccupationService(
			EOEditingContext ec, EOStructure service, EOPeriode eoPeriode, EOTypeOccupation eoTypeOccupation) {
		NSArray<EOOccupation> array = null;

		// + periode
		EOQualifier qual = ERXQ.and(
				ERXQ.and(
						ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, NON),
						ERXQ.equals(PERS_ID_KEY, service.persId()),
						getQualPeriode(eoPeriode)),
						ERXQ.equals(TO_TYPE_OCCUPATION_KEY, eoTypeOccupation));

		array = fetchAll(ec, qual);

		return array;
	}

	/**
	 * Indique si l'occupation a été annulée
	 * 
	 * @param ec
	 * @param persId
	 * @param temPersonneComposante
	 * @return
	 */
	public boolean isAnnuleePour(EOEditingContext ec, Integer persId, String temPersonneComposante) {
		boolean isAnnulee = false;

		if (getEoAnnulationPour(ec, persId, temPersonneComposante) != null) {
			isAnnulee = true;
		}

		return isAnnulee;
	}

	public EOAnnulation getEoAnnulationPour(EOEditingContext ec, Integer persId, String temPersonneComposante) {
		EOAnnulation eoAnnulation = null;

		EOQualifier qual = ERXQ.and(
				ERXQ.equals(PERS_ID_KEY, persId),
				ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, temPersonneComposante));

		NSArray<EOAnnulation> array = tosAnnulation(qual);
		if (tosAnnulation(qual).count() > 0) {
			eoAnnulation = array.objectAtIndex(0);
		}

		return eoAnnulation;
	}

	/**
	 * Annuler une occupation
	 * 
	 * @param persId
	 * @param tempPersonneComposante
	 * @return
	 */
	public boolean annulerPour(Integer persId, String temPersonneComposante) {
		boolean isOk = false;

		EOAnnulation eoAnnulation = getEoAnnulationPour(
				editingContext(), persId, temPersonneComposante);

		if (eoAnnulation == null) {

			EOAnnulation.createEOAnnulation(
					editingContext(), DateCtrlHamac.now(), DateCtrlHamac.now(), EOPlanning.REEL,
					persId, temPersonneComposante, this);

			isOk = true;

		}

		return isOk;
	}

	/**
	 * Desannuler une occupation
	 * 
	 * @param persId
	 * @param tempPersonneComposante
	 * @return
	 */
	public boolean desannulerPour(Integer persId, String temPersonneComposante) {
		boolean isOk = false;

		EOAnnulation eoAnnulation = getEoAnnulationPour(
				editingContext(), persId, temPersonneComposante);

		if (eoAnnulation != null) {
			editingContext().deleteObject(eoAnnulation);
		}

		return isOk;
	}

	/**
	 * Ajout d'un historique de délégation
	 * 
	 * @param personne
	 */
	public void addHistoriqueDelegation(Integer persId) {
		EODelegationHistorique eoDelegationHistorique = EODelegationHistorique.create(editingContext(), persId);
		eoDelegationHistorique.setToOccupationRelationship(this);
	}

	/**
	 * Recopier à l'identique vers un autre planning
	 * 
	 * @param eoPlanning
	 * @return
	 */
	protected final EOOccupation dupliquerVers(String natureDst) {
		EOOccupation eoOccupationCopie = EOOccupation.createEOOccupation(
				editingContext(), DateCtrlHamac.now(), DateCtrlHamac.now(),
				natureDst, dateDebut(), dateFin(),
				persId(), temPersonneComposante(), toTypeOccupation(),
				toTypeStatut());
		eoOccupationCopie.setToStructureApplicationRelationship(toStructureApplication());
		eoOccupationCopie.setOccMotif(occMotif());
		return eoOccupationCopie;
	}

	public final static EOQualifier QUAL_CET = ERXQ.and(
			ERXQ.isFalse(IS_ABSENCE_LEGALE_KEY),
			ERXQ.equals(TO_TYPE_OCCUPATION_KEY + "." + EOTypeOccupation.OTYP_CODE_KEY, EOTypeOccupation.CODE_CET_VALUE));

	private Integer _occId;

	/**
	 * Clé primaire
	 * 
	 * @return
	 */
	public Integer occId() {
		if (_occId == null) {
			_occId = (Integer) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey(OCC_ID_KEY);
		}
		return _occId;
	}

	/**
	 * Recherche dans le tableau l'occupation
	 * 
	 * @param eoOccupationATrouver
	 * @param arrayAChercher
	 * @return
	 */
	public final static boolean isContenuDansTableauMemeTypeEtDates(
			EOOccupation eoOccupationATrouver, NSArray<EOOccupation> arrayAChercher) {
		boolean isContenu = false;

		int i = 0;
		while (i < arrayAChercher.count() && !isContenu) {
			EOOccupation eoOccupationAComparer = arrayAChercher.objectAtIndex(i);
			if (eoOccupationAComparer.occDateDebut().getTime() == eoOccupationATrouver.occDateDebut().getTime() &&
					eoOccupationAComparer.occDateFin().getTime() == eoOccupationATrouver.occDateFin().getTime() &&
					eoOccupationAComparer.toTypeOccupation().otypCode().equals(eoOccupationATrouver.toTypeOccupation().otypCode())) {
				isContenu = true;
			}
			i++;
		}

		return isContenu;
	}

	/**
	 * Récepercuter une création / supprerssion d'occupation du réel vers le
	 * prévisionnel
	 * 
	 * TODO gérer les modifications
	 * 
	 * @param eoPlanningDst
	 * @param isASupprimer
	 * @param connectedUser
	 * @param isValidationMetierARealiser
	 *          faut-il faire un controle de cohérence sur la copie
	 * @return
	 */
	public final EOOccupation repercuter(
			EOPlanning eoPlanningDst,
			boolean isASupprimer,
			HamacApplicationUser connectedUser,
			boolean isValidationMetierARealiser) {
		EOOccupation eoOccupation = null;

		// construction du qualifier permettant de retrouver l'occupation
		EOQualifier qual = ERXQ.and(
				ERXQ.and(
						ERXQ.equals(TEM_PERSONNE_COMPOSANTE_KEY, temPersonneComposante()),
						ERXQ.equals(PERS_ID_KEY, persId()),
						ERXQ.equals(OCC_DATE_DEBUT_KEY, occDateDebut()),
						ERXQ.equals(OCC_DATE_FIN_KEY, occDateFin()),
						ERXQ.equals(TO_TYPE_OCCUPATION_KEY, toTypeOccupation()),
						QUAL_OCC_NON_ANNULE,
						ERXQ.equals(NATURE_KEY, eoPlanningDst.nature())));

		if (toStructureApplication() != null) {
			qual = ERXQ.and(
					qual, ERXQ.equals(TO_STRUCTURE_APPLICATION_KEY, toStructureApplication()));
		}

		NSArray<EOOccupation> array = EOOccupation.fetchAll(editingContext(), qual);

		if (array.count() > 0) {
			eoOccupation = array.objectAtIndex(0);
		}

		if (eoOccupation != null && isASupprimer) {
			// positionner à supprimé
			eoOccupation.setToTypeStatutRelationship(
					EOTypeStatut.getEoTypeStatutSupprime(editingContext()));
		} else if (eoOccupation == null && !isASupprimer) {
			// n'existe pas encore => création
			eoOccupation = createEOOccupation(
					editingContext(), dateDebut(), dateFin(),
					motif(), toTypeOccupation(), toStructureApplication(),
					false, eoPlanningDst.nature(), persId(),
					temPersonneComposante(), toPersonne(), connectedUser,
					isValidationMetierARealiser, toTypeStatut());
		}

		return eoOccupation;
	}

	/**
	 * Création d'un enregistrement en cours de validation
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @param motif
	 * @param eoTypeOccupation
	 * @param eoStructureApplication
	 * @param isImmediatementValide
	 * @param isValidationMetierAEffectuer
	 *          faut-il faire un controle de cohérence suite à création (appel à
	 *          la méthode {@link #validateObjectMetier()}
	 * @param eoTypeStatut
	 *          Le statut que devra prendre l'occupation suite à la création
	 * @param now
	 */
	public final static EOOccupation createEOOccupation(
			EOEditingContext ec,
			NSTimestamp dateDebut,
			NSTimestamp dateFin,
			String motif,
			EOTypeOccupation eoTypeOccupation,
			EOStructure eoStructureApplication,
			boolean isImmediatementValide,
			String nature,
			Integer persId,
			String temPersonneComposante,
			IPersonne personne,
			HamacApplicationUser connectedUser,
			boolean isValidationMetierAEffectuer,
			EOTypeStatut eoTypeStatut) {

		NSTimestamp now = DateCtrlHamac.now();

		EOOccupation newEoOccupation = EOOccupation.createEOOccupation(
				ec,
				now,
				now,
				nature,
				dateDebut,
				dateFin,
				persId,
				temPersonneComposante,
				EOTypeOccupation.localInstanceIn(ec, eoTypeOccupation),
				eoTypeStatut);

		newEoOccupation.setOccMotif(motif);
		newEoOccupation.setToStructureApplicationRelationship(eoStructureApplication);

		// forcer la to one toPersonne qui reste vide à l'instanciation
		newEoOccupation.setToPersonne(personne);

		// mémoriser le demandeur si c'est un autre
		if (connectedUser.getPersId().intValue() != personne.persId().intValue()) {
			newEoOccupation.addHistoriqueDelegation(connectedUser.getPersId());
		}

		// test de validité
		if (isValidationMetierAEffectuer) {
			newEoOccupation.validateObjectMetier();
		}

		// validation immédiate (période de fermeture ...)
		if (isImmediatementValide) {
			newEoOccupation.setToTypeStatutRelationship(EOTypeStatut.getEoTypeStatutValide(ec));
		}

		CktlLog.log(" createEOOccupation() persId=" + persId + " du " + dateDebut + " au " + dateFin + " (" + eoTypeOccupation.otypCode() + ") [" + nature + "]");

		return newEoOccupation;
	}

	/**
	 * Le libellé parle de lui meme
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public final boolean isStrictementIncluseDansPeriode(EOPeriode eoPeriode) {
		boolean isStrictementIncluseDansPeriode = true;

		if (DateCtrlHamac.isBefore(dateDebut(), eoPeriode.perDDebut()) ||
				DateCtrlHamac.isAfter(dateFin(), TimeCtrl.remonterAMinuit(eoPeriode.perDFin()).timestampByAddingGregorianUnits(0, 0, 1, 0, 0, -1))) {
			isStrictementIncluseDansPeriode = false;
		}

		return isStrictementIncluseDansPeriode;
	}

}
