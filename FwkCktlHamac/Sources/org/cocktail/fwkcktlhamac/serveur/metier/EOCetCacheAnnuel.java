/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetAffichage;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOCetCacheAnnuel extends _EOCetCacheAnnuel
		implements I_CetAffichage {

	public EOCetCacheAnnuel() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	/**
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetAffichage
	 */

	public Integer soldeAncienRegime() {
		return soldeAncien();
	}

	public Integer soldeRegimePerenneDebutPeriode() {
		return soldePerenneDebutPeriode();
	}

	public Integer soldeRegimePerenneFinPeriode() {
		return soldePerenneFinPeriode();
	}

	public Integer soldeAncienRegimeInitial() {
		return soldeAncienInit();
	}

	public Integer soldeAncienRegimeFinal() {
		return soldeAncienFinal();
	}

	public Integer reliquatInitial() {
		return reliquatInit();
	}

	public Integer soldeRegimePerenneInitial() {
		return soldePerenneInit();
	}

	public Integer soldeRegimePerenneFinal() {
		return soldePerenneFinal();
	}

	// ajouts

	public static EOCetCacheAnnuel getCetCacheAnnuel(EOEditingContext ec, EOPeriode eoPeriode, Integer persId) {
		EOQualifier qual = ERXQ.and(
				ERXQ.equals(EOCetCacheAnnuel.TO_PERIODE_KEY, eoPeriode),
				ERXQ.equals(EOCetCacheAnnuel.PERS_ID_KEY, persId));

		NSArray<EOCetCacheAnnuel> objects = EOCetCacheAnnuel.fetchAll(ec, qual);

		EOCetCacheAnnuel object = null;

		if (objects.count() > 0) {
			// maj
			object = objects.objectAtIndex(0);
		}

		return object;
	}
	
	
	/**
	 * 
	 * @param edc editingContext
	 * @param periode periode
	 */
	public static void supprimerParPeriode(EOEditingContext edc, EOPeriode periode) {
		
		NSArray<EOCetCacheAnnuel> cetCacheAnnuelArray = fetchAll(
				edc, ERXQ.equals(EOCetCacheAnnuel.TO_PERIODE_KEY, periode));

		for (EOCetCacheAnnuel eoCetCacheAnnuel : cetCacheAnnuelArray) {
			edc.deleteObject(eoCetCacheAnnuel);
		}
		
	}

	
	
}
