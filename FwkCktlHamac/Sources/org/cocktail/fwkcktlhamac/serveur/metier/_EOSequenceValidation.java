/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSequenceValidation.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOSequenceValidation extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "SequenceValidation";
	public static final String ENTITY_TABLE_NAME = "HAMAC.SEQUENCE_VALIDATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "svaId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_ENVOI_MAIL_KEY = "temEnvoiMail";

// Attributs non visibles
	public static final String SVA_ID_KEY = "svaId";
	public static final String TND_ID_KEY = "tndId";
	public static final String TPR_ID_KEY = "tprId";
	public static final String TSV_ID_KEY = "tsvId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_ENVOI_MAIL_COLKEY = "TEM_ENVOI_MAIL";

	public static final String SVA_ID_COLKEY = "SVA_ID";
	public static final String TND_ID_COLKEY = "TND_ID";
	public static final String TPR_ID_COLKEY = "TPR_ID";
	public static final String TSV_ID_COLKEY = "TSV_ID";


	// Relationships
	public static final String TO_TYPE_NIVEAU_DROIT_KEY = "toTypeNiveauDroit";
	public static final String TO_TYPE_PRIVILEGE_KEY = "toTypePrivilege";
	public static final String TO_TYPE_SEQUENCE_VALIDATION_KEY = "toTypeSequenceValidation";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temEnvoiMail() {
    return (String) storedValueForKey(TEM_ENVOI_MAIL_KEY);
  }

  public void setTemEnvoiMail(String value) {
    takeStoredValueForKey(value, TEM_ENVOI_MAIL_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit toTypeNiveauDroit() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit)storedValueForKey(TO_TYPE_NIVEAU_DROIT_KEY);
  }

  public void setToTypeNiveauDroitRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit oldValue = toTypeNiveauDroit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_NIVEAU_DROIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_NIVEAU_DROIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege toTypePrivilege() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege)storedValueForKey(TO_TYPE_PRIVILEGE_KEY);
  }

  public void setToTypePrivilegeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege oldValue = toTypePrivilege();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_PRIVILEGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_PRIVILEGE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation toTypeSequenceValidation() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation)storedValueForKey(TO_TYPE_SEQUENCE_VALIDATION_KEY);
  }

  public void setToTypeSequenceValidationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation oldValue = toTypeSequenceValidation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SEQUENCE_VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SEQUENCE_VALIDATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOSequenceValidation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSequenceValidation createEOSequenceValidation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temEnvoiMail
, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit toTypeNiveauDroit, org.cocktail.fwkcktlhamac.serveur.metier.EOTypePrivilege toTypePrivilege, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSequenceValidation toTypeSequenceValidation			) {
    EOSequenceValidation eo = (EOSequenceValidation) createAndInsertInstance(editingContext, _EOSequenceValidation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemEnvoiMail(temEnvoiMail);
    eo.setToTypeNiveauDroitRelationship(toTypeNiveauDroit);
    eo.setToTypePrivilegeRelationship(toTypePrivilege);
    eo.setToTypeSequenceValidationRelationship(toTypeSequenceValidation);
    return eo;
  }

  
	  public EOSequenceValidation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSequenceValidation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSequenceValidation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSequenceValidation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSequenceValidation object = (EOSequenceValidation)createAndInsertInstance(editingContext, _EOSequenceValidation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSequenceValidation localInstanceIn(EOEditingContext editingContext, EOSequenceValidation eo) {
    EOSequenceValidation localInstance = (eo == null) ? null : (EOSequenceValidation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSequenceValidation#localInstanceIn a la place.
   */
	public static EOSequenceValidation localInstanceOf(EOEditingContext editingContext, EOSequenceValidation eo) {
		return EOSequenceValidation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSequenceValidation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSequenceValidation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSequenceValidation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSequenceValidation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSequenceValidation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSequenceValidation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSequenceValidation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSequenceValidation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSequenceValidation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSequenceValidation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSequenceValidation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSequenceValidation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
