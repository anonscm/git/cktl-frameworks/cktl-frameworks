/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODroit.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EODroit extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "Droit";
	public static final String ENTITY_TABLE_NAME = "HAMAC.DROIT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "droId";

	public static final String C_STRUCTURE_PLANNING_KEY = "cStructurePlanning";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CIBLE_KEY = "persIdCible";
	public static final String PERS_ID_TITULAIRE_KEY = "persIdTitulaire";
	public static final String TEM_CIBLE_CDC_KEY = "temCibleCdc";
	public static final String TEM_CIBLE_HERITAGE_KEY = "temCibleHeritage";
	public static final String TEM_TITULAIRE_CDC_KEY = "temTitulaireCdc";
	public static final String TEM_TITULAIRE_HERITAGE_KEY = "temTitulaireHeritage";

// Attributs non visibles
	public static final String DRO_ID_KEY = "droId";
	public static final String PER_ID_KEY = "perId";
	public static final String TND_ID_KEY = "tndId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_PLANNING_COLKEY = "C_STRUCTURE_PLANNING";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_CIBLE_COLKEY = "PERS_ID_CIBLE";
	public static final String PERS_ID_TITULAIRE_COLKEY = "PERS_ID_TITULAIRE";
	public static final String TEM_CIBLE_CDC_COLKEY = "TEM_CIBLE_CDC";
	public static final String TEM_CIBLE_HERITAGE_COLKEY = "TEM_CIBLE_HERITAGE";
	public static final String TEM_TITULAIRE_CDC_COLKEY = "TEM_TITULAIRE_CDC";
	public static final String TEM_TITULAIRE_HERITAGE_COLKEY = "TEM_TITULAIRE_HERITAGE";

	public static final String DRO_ID_COLKEY = "DRO_ID";
	public static final String PER_ID_COLKEY = "PER_ID";
	public static final String TND_ID_COLKEY = "TND_ID";


	// Relationships
	public static final String TO_PERIODE_KEY = "toPeriode";
	public static final String TOS_INDIVIDU_CIBLE_KEY = "tosIndividuCible";
	public static final String TOS_INDIVIDU_TITULAIRE_KEY = "tosIndividuTitulaire";
	public static final String TOS_STRUCTURE_CIBLE_KEY = "tosStructureCible";
	public static final String TOS_STRUCTURE_TITULAIRE_KEY = "tosStructureTitulaire";
	public static final String TO_STRUCTURE_PLANNING_KEY = "toStructurePlanning";
	public static final String TO_TYPE_NIVEAU_DROIT_KEY = "toTypeNiveauDroit";



	// Accessors methods
  public String cStructurePlanning() {
    return (String) storedValueForKey(C_STRUCTURE_PLANNING_KEY);
  }

  public void setCStructurePlanning(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PLANNING_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persIdCible() {
    return (Integer) storedValueForKey(PERS_ID_CIBLE_KEY);
  }

  public void setPersIdCible(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CIBLE_KEY);
  }

  public Integer persIdTitulaire() {
    return (Integer) storedValueForKey(PERS_ID_TITULAIRE_KEY);
  }

  public void setPersIdTitulaire(Integer value) {
    takeStoredValueForKey(value, PERS_ID_TITULAIRE_KEY);
  }

  public String temCibleCdc() {
    return (String) storedValueForKey(TEM_CIBLE_CDC_KEY);
  }

  public void setTemCibleCdc(String value) {
    takeStoredValueForKey(value, TEM_CIBLE_CDC_KEY);
  }

  public String temCibleHeritage() {
    return (String) storedValueForKey(TEM_CIBLE_HERITAGE_KEY);
  }

  public void setTemCibleHeritage(String value) {
    takeStoredValueForKey(value, TEM_CIBLE_HERITAGE_KEY);
  }

  public String temTitulaireCdc() {
    return (String) storedValueForKey(TEM_TITULAIRE_CDC_KEY);
  }

  public void setTemTitulaireCdc(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_CDC_KEY);
  }

  public String temTitulaireHeritage() {
    return (String) storedValueForKey(TEM_TITULAIRE_HERITAGE_KEY);
  }

  public void setTemTitulaireHeritage(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_HERITAGE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode)storedValueForKey(TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERIODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructurePlanning() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_PLANNING_KEY);
  }

  public void setToStructurePlanningRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructurePlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_PLANNING_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit toTypeNiveauDroit() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit)storedValueForKey(TO_TYPE_NIVEAU_DROIT_KEY);
  }

  public void setToTypeNiveauDroitRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit oldValue = toTypeNiveauDroit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_NIVEAU_DROIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_NIVEAU_DROIT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuCible() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_CIBLE_KEY);
  }

  public NSArray tosIndividuCible(EOQualifier qualifier) {
    return tosIndividuCible(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuCible(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividuCible();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_CIBLE_KEY);
  }

  public void removeFromTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_CIBLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuCibleRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_CIBLE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_CIBLE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuCibleRelationships() {
    Enumeration objects = tosIndividuCible().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuCibleRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuTitulaire() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_TITULAIRE_KEY);
  }

  public NSArray tosIndividuTitulaire(EOQualifier qualifier) {
    return tosIndividuTitulaire(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividuTitulaire(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividuTitulaire();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_TITULAIRE_KEY);
  }

  public void removeFromTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_TITULAIRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuTitulaireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_TITULAIRE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_TITULAIRE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuTitulaireRelationships() {
    Enumeration objects = tosIndividuTitulaire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuTitulaireRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureCible() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_CIBLE_KEY);
  }

  public NSArray tosStructureCible(EOQualifier qualifier) {
    return tosStructureCible(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureCible(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructureCible();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_CIBLE_KEY);
  }

  public void removeFromTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_CIBLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureCibleRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_CIBLE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureCibleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_CIBLE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureCibleRelationships() {
    Enumeration objects = tosStructureCible().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureCibleRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureTitulaire() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_TITULAIRE_KEY);
  }

  public NSArray tosStructureTitulaire(EOQualifier qualifier) {
    return tosStructureTitulaire(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructureTitulaire(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructureTitulaire();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_TITULAIRE_KEY);
  }

  public void removeFromTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_TITULAIRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureTitulaireRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_TITULAIRE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_TITULAIRE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureTitulaireRelationships() {
    Enumeration objects = tosStructureTitulaire().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureTitulaireRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODroit avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODroit createEODroit(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCible
, Integer persIdTitulaire
, String temCibleCdc
, String temCibleHeritage
, String temTitulaireCdc
, String temTitulaireHeritage
, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeNiveauDroit toTypeNiveauDroit			) {
    EODroit eo = (EODroit) createAndInsertInstance(editingContext, _EODroit.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCible(persIdCible);
		eo.setPersIdTitulaire(persIdTitulaire);
		eo.setTemCibleCdc(temCibleCdc);
		eo.setTemCibleHeritage(temCibleHeritage);
		eo.setTemTitulaireCdc(temTitulaireCdc);
		eo.setTemTitulaireHeritage(temTitulaireHeritage);
    eo.setToTypeNiveauDroitRelationship(toTypeNiveauDroit);
    return eo;
  }

  
	  public EODroit localInstanceIn(EOEditingContext editingContext) {
	  		return (EODroit)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODroit creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODroit creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODroit object = (EODroit)createAndInsertInstance(editingContext, _EODroit.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODroit localInstanceIn(EOEditingContext editingContext, EODroit eo) {
    EODroit localInstance = (eo == null) ? null : (EODroit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODroit#localInstanceIn a la place.
   */
	public static EODroit localInstanceOf(EOEditingContext editingContext, EODroit eo) {
		return EODroit.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EODroit> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODroit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODroit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODroit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODroit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODroit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODroit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODroit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODroit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODroit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODroit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
