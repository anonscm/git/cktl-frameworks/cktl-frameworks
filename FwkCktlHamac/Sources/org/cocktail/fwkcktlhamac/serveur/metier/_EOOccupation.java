/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOccupation.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOOccupation extends A_Absence  {
	public static final String ENTITY_NAME = "Occupation";
	public static final String ENTITY_TABLE_NAME = "HAMAC.OCCUPATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "occId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NATURE_KEY = "nature";
	public static final String OCC_DATE_DEBUT_KEY = "occDateDebut";
	public static final String OCC_DATE_FIN_KEY = "occDateFin";
	public static final String OCC_MOTIF_KEY = "occMotif";
	public static final String PERS_ID_KEY = "persId";
	public static final String TEM_PERSONNE_COMPOSANTE_KEY = "temPersonneComposante";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String OCC_ID_KEY = "occId";
	public static final String OTYP_ID_KEY = "otypId";
	public static final String STA_ID_KEY = "staId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NATURE_COLKEY = "NATURE";
	public static final String OCC_DATE_DEBUT_COLKEY = "OCC_DATE_DEBUT";
	public static final String OCC_DATE_FIN_COLKEY = "OCC_DATE_FIN";
	public static final String OCC_MOTIF_COLKEY = "OCC_MOTIF";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TEM_PERSONNE_COMPOSANTE_COLKEY = "TEM_COMPOSANTE";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String OCC_ID_COLKEY = "OCC_ID";
	public static final String OTYP_ID_COLKEY = "OTYP_ID";
	public static final String STA_ID_COLKEY = "STA_ID";


	// Relationships
	public static final String TOS_ANNULATION_KEY = "tosAnnulation";
	public static final String TOS_DELEGATION_HISTORIQUE_KEY = "tosDelegationHistorique";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_OCCUPATION_PLANNING_CALCUL_KEY = "tosOccupationPlanningCalcul";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";
	public static final String TO_STRUCTURE_APPLICATION_KEY = "toStructureApplication";
	public static final String TOS_VISA_KEY = "tosVisa";
	public static final String TO_TYPE_OCCUPATION_KEY = "toTypeOccupation";
	public static final String TO_TYPE_STATUT_KEY = "toTypeStatut";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String nature() {
    return (String) storedValueForKey(NATURE_KEY);
  }

  public void setNature(String value) {
    takeStoredValueForKey(value, NATURE_KEY);
  }

  public NSTimestamp occDateDebut() {
    return (NSTimestamp) storedValueForKey(OCC_DATE_DEBUT_KEY);
  }

  public void setOccDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, OCC_DATE_DEBUT_KEY);
  }

  public NSTimestamp occDateFin() {
    return (NSTimestamp) storedValueForKey(OCC_DATE_FIN_KEY);
  }

  public void setOccDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, OCC_DATE_FIN_KEY);
  }

  public String occMotif() {
    return (String) storedValueForKey(OCC_MOTIF_KEY);
  }

  public void setOccMotif(String value) {
    takeStoredValueForKey(value, OCC_MOTIF_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String temPersonneComposante() {
    return (String) storedValueForKey(TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public void setTemPersonneComposante(String value) {
    takeStoredValueForKey(value, TEM_PERSONNE_COMPOSANTE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureApplication() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_APPLICATION_KEY);
  }

  public void setToStructureApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation toTypeOccupation() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation)storedValueForKey(TO_TYPE_OCCUPATION_KEY);
  }

  public void setToTypeOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation oldValue = toTypeOccupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_OCCUPATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_OCCUPATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut toTypeStatut() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)storedValueForKey(TO_TYPE_STATUT_KEY);
  }

  public void setToTypeStatutRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut oldValue = toTypeStatut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_STATUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_STATUT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation> tosAnnulation() {
    return (NSArray)storedValueForKey(TOS_ANNULATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation> tosAnnulation(EOQualifier qualifier) {
    return tosAnnulation(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation> tosAnnulation(EOQualifier qualifier, boolean fetch) {
    return tosAnnulation(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation> tosAnnulation(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation.TO_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosAnnulation();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosAnnulationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_ANNULATION_KEY);
  }

  public void removeFromTosAnnulationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ANNULATION_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation createTosAnnulationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Annulation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_ANNULATION_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation) eo;
  }

  public void deleteTosAnnulationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_ANNULATION_KEY);
  }

  public void deleteAllTosAnnulationRelationships() {
    Enumeration objects = tosAnnulation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosAnnulationRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOAnnulation)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique() {
    return (NSArray)storedValueForKey(TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier) {
    return tosDelegationHistorique(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier, boolean fetch) {
    return tosDelegationHistorique(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique> tosDelegationHistorique(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique.TO_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosDelegationHistorique();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public void removeFromTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique createTosDelegationHistoriqueRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DelegationHistorique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_DELEGATION_HISTORIQUE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique) eo;
  }

  public void deleteTosDelegationHistoriqueRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_DELEGATION_HISTORIQUE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosDelegationHistoriqueRelationships() {
    Enumeration objects = tosDelegationHistorique().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosDelegationHistoriqueRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EODelegationHistorique)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul() {
    return (NSArray)storedValueForKey(TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier) {
    return tosOccupationPlanningCalcul(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier, boolean fetch) {
    return tosOccupationPlanningCalcul(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul> tosOccupationPlanningCalcul(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul.TO_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosOccupationPlanningCalcul();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public void removeFromTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul createTosOccupationPlanningCalculRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OccupationPlanningCalcul");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul) eo;
  }

  public void deleteTosOccupationPlanningCalculRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_OCCUPATION_PLANNING_CALCUL_KEY);
  }

  public void deleteAllTosOccupationPlanningCalculRelationships() {
    Enumeration objects = tosOccupationPlanningCalcul().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosOccupationPlanningCalculRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOOccupationPlanningCalcul)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa() {
    return (NSArray)storedValueForKey(TOS_VISA_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier) {
    return tosVisa(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier, boolean fetch) {
    return tosVisa(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOVisa> tosVisa(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa.TO_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOVisa.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosVisa();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
  }

  public void removeFromTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOVisa createTosVisaRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Visa");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_VISA_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOVisa) eo;
  }

  public void deleteTosVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOVisa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_VISA_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVisaRelationships() {
    Enumeration objects = tosVisa().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVisaRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOVisa)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOOccupation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOccupation createEOOccupation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String nature
, NSTimestamp occDateDebut
, NSTimestamp occDateFin
, Integer persId
, String temPersonneComposante
, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation toTypeOccupation, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut toTypeStatut			) {
    EOOccupation eo = (EOOccupation) createAndInsertInstance(editingContext, _EOOccupation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNature(nature);
		eo.setOccDateDebut(occDateDebut);
		eo.setOccDateFin(occDateFin);
		eo.setPersId(persId);
		eo.setTemPersonneComposante(temPersonneComposante);
    eo.setToTypeOccupationRelationship(toTypeOccupation);
    eo.setToTypeStatutRelationship(toTypeStatut);
    return eo;
  }

  
	  public EOOccupation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOccupation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOccupation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOccupation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOccupation object = (EOOccupation)createAndInsertInstance(editingContext, _EOOccupation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOOccupation localInstanceIn(EOEditingContext editingContext, EOOccupation eo) {
    EOOccupation localInstance = (eo == null) ? null : (EOOccupation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOccupation#localInstanceIn a la place.
   */
	public static EOOccupation localInstanceOf(EOEditingContext editingContext, EOOccupation eo) {
		return EOOccupation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOOccupation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOccupation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOccupation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOccupation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOccupation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOccupation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOccupation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOccupation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOccupation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
