/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.A_OccupationTypeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.delegate.occupation.CongeLegalDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.DemandeDelegate;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOAbsenceLegale
		extends _EOAbsenceLegale {

	public final static String AM = "am";
	public final static String PM = "pm";

	public EOAbsenceLegale() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {

		// interdire la création
		throw new NSValidation.ValidationException("Hamac n'est pas autorisé à faire des saisie d'absences légales !");

	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {

		// interdire la modification
		throw new NSValidation.ValidationException("Hamac n'est pas autorisé à modifier des absences légales !");

	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {

		// interdire la suppression
		throw new NSValidation.ValidationException("Hamac n'est pas autorisé à supprimer des absences légales !");

	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	@Override
	public NSTimestamp dateDebut() {
		NSTimestamp dateDebut = null;

		dateDebut = absDebut();
		if (absAmpmDebut().equals(PM)) {
			dateDebut = dateDebut.timestampByAddingGregorianUnits(0, 0, 0, 12, 0, 0);
		}

		return dateDebut;
	}

	@Override
	public NSTimestamp dateFin() {
		NSTimestamp dateFin = null;

		dateFin = absFin();
		if (absAmpmFin().equals(PM)) {
			dateFin = dateFin.timestampByAddingGregorianUnits(0, 0, 0, 12, 0, 0);
		}

		return dateFin;
	}

	@Override
	public String motif() {
		String motif = "";

		motif = toTypeAbsence().llTypeAbsence();

		return motif;
	}

	@Override
	public int priorite() {
		return 1;
	}

	private static Integer congesLegauxHoraireForce = null;

	private int getCongesLegauxHoraireForce(EOPeriode periode) {
		
			congesLegauxHoraireForce = Integer.valueOf(HamacCktlConfig.intForKey(
					EOParametre.CONGES_LEGAUX_HORAIRE_FORCE, periode));
		
		return congesLegauxHoraireForce.intValue();
	}

	@Override
	public boolean isHoraireForceRecouvrementSemaine(EOPeriode periode) {
		boolean isHoraireForceRecouvrementSemaine = false;

		if (getCongesLegauxHoraireForce(periode) == EOTypeOccupation.TYPE_CONGE_HORAIRE_FORCE_SEMAINE) {
			isHoraireForceRecouvrementSemaine = true;
		}

		return isHoraireForceRecouvrementSemaine;
	}

	@Override
	public String libelleType() {
		String libelleType = null;

		libelleType = toTypeAbsence().llTypeAbsence();

		return libelleType;
	}

	private A_OccupationTypeDelegate _occupationTypeDelegate;

	@Override
	public A_OccupationTypeDelegate getOccupationTypeDelegate() {
		if (_occupationTypeDelegate == null) {
			_occupationTypeDelegate = new CongeLegalDelegate(this);
		}
		return _occupationTypeDelegate;
	}

	@Override
	public boolean isValidee() {
		return true;
	}

	/**
	 * Absence légale toujours visible
	 */
	@Override
	public boolean isVisible() {
		return true;
	}

	/**
	 * pas de champ de mémorisation pour cette table ...
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence#setCoutMinute(org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning,
	 *      int)
	 */
	@Override
	public void setCoutMinute(EOPlanning eoPlanning) {
		// pas de champ de mémorisation pour cette table ...
	}

	/**
	 * Les congés légaux n'ont pas de structure particuliere
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence#
	 *      toStructureApplication()
	 */
	@Override
	public EOStructure toStructureApplication() {
		return null;
	}

	private EOTypeStatut _toStatut;

	/**
	 * Toutes les absences légales ont le statut validé
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence#toStatut()
	 */
	@Override
	public EOTypeStatut toTypeStatut() {
		if (_toStatut == null) {
			_toStatut = EOTypeStatut.getEoTypeStatutValide(editingContext());
		}
		return _toStatut;
	}

	@Override
	public String temPersonneComposante() {
		return NON;
	}

	@Override
	public NSArray<EOIndividu> tosIndividu() {
		return new NSArray<EOIndividu>(toIndividu());
	}

	@Override
	public NSArray<EOStructure> tosStructure() {
		return new NSArray<EOStructure>();
	}

	@Override
	public void addToTosIndividuRelationship(EOIndividu object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeFromTosIndividuRelationship(EOIndividu object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addToTosStructureRelationship(EOStructure object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeFromTosStructureRelationship(EOStructure object) {
		// TODO Auto-generated method stub

	}

	@Override
	public Integer persId() {
		return toPersonne().persId();
	}

	@Override
	public void setPersId(Integer value) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#getDemandeDelegate
	 * ()
	 */
	public DemandeDelegate getDemandeDelegate() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#tosVisa()
	 */
	public NSArray<EOVisa> tosVisa() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * toTypeSequenceValidation()
	 */
	public EOTypeSequenceValidation toTypeSequenceValidation() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#nature()
	 */
	public String nature() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * setToTypeStatutRelationship
	 * (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)
	 */
	public void setToTypeStatutRelationship(EOTypeStatut toTypeStatut) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * postTraitementChangementStatut
	 * (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut)
	 */
	public void postTraitementChangementStatut(EOTypeStatut eoTypeStatutSuivant) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * tosDelegationHistorique()
	 */
	public NSArray<EODelegationHistorique> tosDelegationHistorique() {
		return new NSArray<EODelegationHistorique>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Demande#
	 * addHistoriqueDelegation(java.lang.Integer)
	 */
	public void addHistoriqueDelegation(Integer persId) {
		// TODO Auto-generated method stub

	}

}
