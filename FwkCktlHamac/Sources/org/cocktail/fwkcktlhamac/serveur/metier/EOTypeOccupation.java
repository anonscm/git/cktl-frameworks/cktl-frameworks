/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTypeOccupation extends _EOTypeOccupation {

	public EOTypeOccupation() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	private final static String STR_QUANTUM_MINUTE = "M";
	private final static String STR_QUANTUM_DEMI_J = "J";
	public final static NSArray<String> STR_QUANTUM_ARRAY = new NSArray<String>(new String[] { STR_QUANTUM_DEMI_J, STR_QUANTUM_MINUTE });

	public final static String CODE_CET_VALUE = "C_CET";
	public final static String CODE_FERMETURE_VALUE = "C_PFD";

	// XXX pas terrible
	public final static String CODE_HSUP_VALUE = "H_SUP";
	public final static String CODE_ASTREINTE_VALUE = "H_ASTREINTE";

	public final static String CODE_CONGE_ANNUEL = "C_ANN";

	public final static int TYPE_CONGE_HORAIRE_FORCE_NON = 0;
	public final static int TYPE_CONGE_HORAIRE_FORCE_SEMAINE = 1;
	
	public final static int TYPE_CONGE_RH_SOUS_RESERVE = 0;
	public final static int TYPE_CONGE_RH_DE_DROIT = 1;
	
	public final static String IS_OCCUPATION_DEMI_JOURNEE_KEY = "isOccupationDemiJournee";
	public final static String IS_SEQUENCE_VALIDATION_CLASSIQUE_KEY = "isSequenceValidationClassique";

	public final static NSArray<Integer> HORAIRE_FORCE_TYPE_ARRAY = new NSArray<Integer>(new Integer[] {
			Integer.valueOf(TYPE_CONGE_HORAIRE_FORCE_NON),
			Integer.valueOf(TYPE_CONGE_HORAIRE_FORCE_SEMAINE) });
	
	public final static NSArray<Integer> CONGE_RH_TYPE_ARRAY = new NSArray<Integer>(new Integer[] {
			Integer.valueOf(TYPE_CONGE_RH_SOUS_RESERVE),
			Integer.valueOf(TYPE_CONGE_RH_DE_DROIT) });
	
	

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeOccupation getEoTypeOccupationFermeture(EOEditingContext ec) {
		return fetchRequiredByKeyValue(ec, OTYP_CODE_KEY, CODE_FERMETURE_VALUE);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static EOTypeOccupation getEoTypeOccupationCongeAnnuel(EOEditingContext ec) {
		return fetchRequiredByKeyValue(ec, OTYP_CODE_KEY, CODE_CONGE_ANNUEL);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFermeture() {
		boolean isFermeture = false;

		if (otypCode().equals(CODE_FERMETURE_VALUE)) {
			isFermeture = true;
		}

		return isFermeture;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isRH() {
		return this.toTypeSequenceValidation().isRH();
	}
	
	public boolean isRHSousReserve() {
		return this.isRH() && this.otypTemoinRhDroit().equals(TYPE_CONGE_RH_SOUS_RESERVE);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isOccupationMinute() {
		boolean isOccupationMinute = false;

		if (otypQuantum().equals(STR_QUANTUM_MINUTE)) {
			isOccupationMinute = true;
		}

		return isOccupationMinute;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOccupationDemiJournee() {
		boolean isOccupationDemiJournee = false;

		if (otypQuantum().equals(STR_QUANTUM_DEMI_J)) {
			isOccupationDemiJournee = true;
		}

		return isOccupationDemiJournee;
	}

	/**
	 * XXX doublon avec {@link A_Absence#isPresence()}
	 * 
	 * Indique si l'absence est une présence
	 * 
	 * @return
	 */
	public boolean isPresence() {
		boolean isPresence = false;

		if (otypCode().equals(CODE_HSUP_VALUE) ||
				otypCode().equals(CODE_ASTREINTE_VALUE)) {
			isPresence = true;
		}

		return isPresence;
	}

	/**
	 * @return
	 */
	public NSArray<EOTypeSolde> getEoTypeSoldeSortedArray(
			Planning planning, boolean isEtatValidation) {
		NSArray<EOTypeSolde> eoTypeSoldeArray = new NSArray<EOTypeSolde>();

		EOQualifier qualValidation = ERXQ.or(
				ERXQ.equals(EORepartTypeOccupationTypeSolde.ETAT_VALIDATION_KEY, isEtatValidation ? OUI : NON),
				ERXQ.isNull(EORepartTypeOccupationTypeSolde.ETAT_VALIDATION_KEY));

		NSArray<EORepartTypeOccupationTypeSolde> array = tosRepartTypeOccupationTypeSolde(qualValidation);

		// classement par priorité
		array = CktlSort.sortedArray(array, EORepartTypeOccupationTypeSolde.PRIORITE_KEY);

		for (EORepartTypeOccupationTypeSolde repart : array) {
			eoTypeSoldeArray = eoTypeSoldeArray.arrayByAddingObject(repart.toTypeSolde());
		}

		return eoTypeSoldeArray;
	}

	// affichage

	public String getLibelleAvecQuantum() {
		String disp = "";

		disp += otypLibelle();

		disp += " ";

		if (isOccupationDemiJournee()) {
			disp += " (à la 1/2 journée)";
		} else {
			disp += " (à la minute)";
		}

		return disp;
	}

	public final boolean isSequenceValidationClassique() {
		boolean TSV_CODE_CLASSIQUE_KEY = false;

		if (toTypeSequenceValidation().tsvCode().equals(EOTypeSequenceValidation.TSV_CODE_CLASSIQUE_KEY)) {
			TSV_CODE_CLASSIQUE_KEY = true;
		}

		return TSV_CODE_CLASSIQUE_KEY;
	}

	
	
	
	/**
	 * TODO classifier par occ 1/2j, min, drh ...
	 * 
	 * @param ec
	 * @return
	 */
	public final static NSArray<Object> getEoTypeOccupationArrayPourPageSaisieOccupation(
			EOEditingContext ec) {

		NSArray<Object> eoTypeOccupationArray = new NSArray<Object>();

		NSArray<EOTypeOccupation> allObjects = fetchAllValides(ec);
		allObjects = CktlSort.sortedArray(allObjects, OTYP_LIBELLE_KEY);
		// XXX types les occupations pas saisissable par un agent
		// XXX oter les fermetures
		{
			EOQualifier qual = ERXQ.notEquals(OTYP_CODE_KEY, CODE_FERMETURE_VALUE);
			allObjects = EOQualifier.filteredArrayWithQualifier(allObjects, qual);
		}

		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				"--- Occupations à la 1/2 journée ---");
		EOQualifier qual = ERXQ.and(
				ERXQ.isTrue(IS_OCCUPATION_DEMI_JOURNEE_KEY),
				ERXQ.isTrue(IS_SEQUENCE_VALIDATION_CLASSIQUE_KEY));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObjectsFromArray(
				EOQualifier.filteredArrayWithQualifier(allObjects, qual));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				" ");

		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				"--- Occupations à la minute ---");
		qual = ERXQ.and(
				ERXQ.isFalse(IS_OCCUPATION_DEMI_JOURNEE_KEY),
				ERXQ.isTrue(IS_SEQUENCE_VALIDATION_CLASSIQUE_KEY));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObjectsFromArray(
				EOQualifier.filteredArrayWithQualifier(allObjects, qual));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				" ");

		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				"--- Occupations à la 1/2 journée (RH) ---");
		qual = ERXQ.and(
				ERXQ.isTrue(IS_OCCUPATION_DEMI_JOURNEE_KEY),
				ERXQ.isFalse(IS_SEQUENCE_VALIDATION_CLASSIQUE_KEY));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObjectsFromArray(
				EOQualifier.filteredArrayWithQualifier(allObjects, qual));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				" ");

		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObject(
				"--- Occupations à la minute (RH) ---");
		qual = ERXQ.and(
				ERXQ.isFalse(IS_OCCUPATION_DEMI_JOURNEE_KEY),
				ERXQ.isFalse(IS_SEQUENCE_VALIDATION_CLASSIQUE_KEY));
		eoTypeOccupationArray = eoTypeOccupationArray.arrayByAddingObjectsFromArray(
				EOQualifier.filteredArrayWithQualifier(allObjects, qual));

		return eoTypeOccupationArray;
	}

	private static NSArray<EOTypeOccupation> fetchAllValides(EOEditingContext ec) {
		
		NSTimestamp dateJour = DateCtrlHamac.getDateJour();
		
		EOQualifier qual = ERXQ.and(ERXQ.or(ERXQ.isNull(DATE_DEBUT_VALIDITE_KEY) , ERXQ.lessThanOrEqualTo(DATE_DEBUT_VALIDITE_KEY, dateJour)), 
				ERXQ.or(ERXQ.greaterThanOrEqualTo(DATE_FIN_VALIDITE_KEY, dateJour), ERXQ.isNull(DATE_DEBUT_VALIDITE_KEY)));
		return fetchAll(ec, qual);
	}
	
	public static NSArray<EOTypeOccupation> fetchAllValidesCourantesEtFutures(EOEditingContext ec) {
		EOQualifier qual = ERXQ.not(ERXQ.lessThanOrEqualTo(DATE_FIN_VALIDITE_KEY, DateCtrl.getDateJour()));
		return fetchAll(ec, qual, CktlSort.newSort(EOTypeOccupation.OTYP_LIBELLE_KEY));
	}
	
	
}
