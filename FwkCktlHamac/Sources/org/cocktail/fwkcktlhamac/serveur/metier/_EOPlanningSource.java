/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlanningSource.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOPlanningSource extends A_EOSource  {
	public static final String ENTITY_NAME = "PlanningSource";
	public static final String ENTITY_TABLE_NAME = "HAMAC.PLANNING_SOURCE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "psoId";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String DATE_IMPORT_KEY = "dateImport";
	public static final String DATE_SUPPRESSION_KEY = "dateSuppression";
	public static final String D_DEBUT_KEY = "dDebut";
	public static final String D_FIN_KEY = "dFin";
	public static final String PERS_ID_KEY = "persId";
	public static final String QUOTITE_KEY = "quotite";

// Attributs non visibles
	public static final String PSO_ID_KEY = "psoId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String DATE_IMPORT_COLKEY = "DATE_IMPORT";
	public static final String DATE_SUPPRESSION_COLKEY = "DATE_SUPPRESSION";
	public static final String D_DEBUT_COLKEY = "D_DEBUT";
	public static final String D_FIN_COLKEY = "D_FIN";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String QUOTITE_COLKEY = "QUOTITE";

	public static final String PSO_ID_COLKEY = "PSO_ID";


	// Relationships
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";
	public static final String TO_STRUCTURE_AFFECTATION_KEY = "toStructureAffectation";



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public NSTimestamp dateImport() {
    return (NSTimestamp) storedValueForKey(DATE_IMPORT_KEY);
  }

  public void setDateImport(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_IMPORT_KEY);
  }

  public NSTimestamp dateSuppression() {
    return (NSTimestamp) storedValueForKey(DATE_SUPPRESSION_KEY);
  }

  public void setDateSuppression(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_SUPPRESSION_KEY);
  }

  public NSTimestamp dDebut() {
    return (NSTimestamp) storedValueForKey(D_DEBUT_KEY);
  }

  public void setDDebut(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEBUT_KEY);
  }

  public NSTimestamp dFin() {
    return (NSTimestamp) storedValueForKey(D_FIN_KEY);
  }

  public void setDFin(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer quotite() {
    return (Integer) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(Integer value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureAffectation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_AFFECTATION_KEY);
  }

  public void setToStructureAffectationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_AFFECTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_AFFECTATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPlanningSource avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPlanningSource createEOPlanningSource(EOEditingContext editingContext, String cStructure
, NSTimestamp dateImport
, NSTimestamp dDebut
, Integer persId
, Integer quotite
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureAffectation			) {
    EOPlanningSource eo = (EOPlanningSource) createAndInsertInstance(editingContext, _EOPlanningSource.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setDateImport(dateImport);
		eo.setDDebut(dDebut);
		eo.setPersId(persId);
		eo.setQuotite(quotite);
    eo.setToStructureAffectationRelationship(toStructureAffectation);
    return eo;
  }

  
	  public EOPlanningSource localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlanningSource)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanningSource creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPlanningSource creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPlanningSource object = (EOPlanningSource)createAndInsertInstance(editingContext, _EOPlanningSource.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPlanningSource localInstanceIn(EOEditingContext editingContext, EOPlanningSource eo) {
    EOPlanningSource localInstance = (eo == null) ? null : (EOPlanningSource)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPlanningSource#localInstanceIn a la place.
   */
	public static EOPlanningSource localInstanceOf(EOEditingContext editingContext, EOPlanningSource eo) {
		return EOPlanningSource.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOPlanningSource> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPlanningSource fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPlanningSource fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlanningSource eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlanningSource)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlanningSource fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlanningSource fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlanningSource eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlanningSource)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPlanningSource fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlanningSource eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlanningSource ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlanningSource fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
