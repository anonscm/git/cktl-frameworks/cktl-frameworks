package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.I_ConstantesApplication;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionGardeEnfantCtrl
		extends A_EditionCtrl {

	private final static String REPORT_FILENAME = "DemandeGardeEnfant";
	private final static String JASPER_FILENAME = "report/demandeGarde/DemandeGardeEnfant.jasper";
	private final static String RECORD_PATH = "/demande";
	private final static String PRINT_LABEL = "Impression de la demande de garde d'enfant";

	private NSTimestamp dateImpression;
	private String feminin;
	private String parentAffichage;
	private String enfantPrenom;
	private String enfantAffichage;
	private String dateDebutAffichage, dateFinAffichage;
	private String strNbDemiJourneesTravail;
	private String pluriel;
	private String motif;

	public EditionGardeEnfantCtrl(
			CktlConfig config,
			NSTimestamp dateImpression,
			String feminin,
			String parentAffichage,
			String enfantPrenom,
			String enfantAffichage,
			String dateDebutAffichage,
			String dateFinAffichage,
			String strNbDemiJourneesTravail,
			String pluriel,
			String motif) {
		super(config);
		this.dateImpression = dateImpression;
		this.feminin = feminin;
		this.parentAffichage = parentAffichage;
		this.enfantPrenom = enfantPrenom;
		this.enfantAffichage = enfantAffichage;
		this.dateDebutAffichage = dateDebutAffichage;
		this.dateFinAffichage = dateFinAffichage;
		this.strNbDemiJourneesTravail = strNbDemiJourneesTravail;
		this.pluriel = pluriel;
		this.motif = motif;
	}

	@Override
	public final String getReportFilename() {
		return REPORT_FILENAME + "_" + StringCtrl.normalize(enfantPrenom) + ".pdf";
	}

	@Override
	public final String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}

	@Override
	public final StringWriter creerXml() throws Exception {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment("Edition de la demande d'autorisation d'absence pour garde d'enfant");

		w.startElement("demande");
		{
			w.writeElement("dateImpression", DateCtrlHamac.dateToString(dateImpression));
			w.writeElement("feminin", feminin);
			w.writeElement("prenomNomQualite", parentAffichage);
			w.writeElement("prenomNeLeDNaissance", enfantAffichage);
			w.writeElement("dateDebutAMPM", dateDebutAffichage);
			w.writeElement("dateFinAMPM", dateFinAffichage);
			w.writeElement("nbDemiJourneesTravail", strNbDemiJourneesTravail);
			w.writeElement("pluriel", pluriel);
			w.writeElement("motif", motif);
			w.writeElement("ville", getConfig().stringForKey(I_ConstantesApplication.GRHUM_VILLE_PARAM_KEY));
			String mainLogoUrl = getConfig().stringForKey(I_ConstantesApplication.MAIN_LOGO_URL_PARAM_KEY);
			if (mainLogoUrl != null) {
				w.writeElement("mainLogoUrl", mainLogoUrl);
			}
		}

		w.endElement(); // "demande"
		w.endDocument();
		w.close();

		return sw;

	}
}
