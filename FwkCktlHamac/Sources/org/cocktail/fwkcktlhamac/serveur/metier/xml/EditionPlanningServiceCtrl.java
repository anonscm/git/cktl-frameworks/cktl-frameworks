/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;
import java.util.Hashtable;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Mois;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionPlanningServiceCtrl
		extends A_EditionCtrl {

	private boolean isEnCouleur;
	private String titreEdition;
	private NSArray<Mois> moisArray;
	private NSArray<EOPlanning> eoPlanningArray;
	private NSArray<String> typePlanningArray;
	private NSTimestamp dateDebutPlanningEdition, dateFinPlanningEdition;
	
	/**
	 * @param config
	 */
	public EditionPlanningServiceCtrl(
			CktlConfig config,
			boolean isEnCouleur,
			String titreEdition,
			NSArray<Mois> moisArray,
			NSArray<EOPlanning> eoPlanningArray,
			NSArray<String> typePlanningArray,
			NSTimestamp dateDebutPlanningEdition,
			NSTimestamp dateFinPlanningEdition) {
		super(config);
		this.isEnCouleur = isEnCouleur;
		this.titreEdition = titreEdition;
		this.moisArray = moisArray;
		this.eoPlanningArray = eoPlanningArray;
		this.typePlanningArray = typePlanningArray;
		this.dateDebutPlanningEdition = dateDebutPlanningEdition;
		this.dateFinPlanningEdition = dateFinPlanningEdition;
	}

	private final static String REPORT_FILENAME = "EditionPlanningService.pdf";
	private final static String JASPER_FILENAME = "report/congesService/CongesServiceDemiJournee.jasper";
	private final static String JASPER_FILENAME_SANS_PREV = "report/congesService/CongesServiceDemiJourneeSansPrev.jasper";
	private final static String RECORD_PATH = "/planning/Individu/periode/detj";
	private final static String PRINT_LABEL = "Edition du planning du service par demi journees";

	@Override
	public final String getReportFilename() {
		return REPORT_FILENAME;
	}

	@Override
	public final String getJasperFilename() {
		if (this.typePlanningArray.containsObject(EOPlanning.REEL) && !this.typePlanningArray.containsObject(EOPlanning.PREV)) {
			return JASPER_FILENAME_SANS_PREV;
		}
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}
	
	private final NSDictionary<String, Object> buildDico() {

		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		EditionFactory.feedDicoLogoEtablissement(getConfig(), dico);
		
		NSArray<Mois> lesMois = new NSMutableArray<Mois>();

		if (this.dateDebutPlanningEdition != null && this.dateFinPlanningEdition != null 
				&& DateCtrlHamac.isBeforeEq(this.dateDebutPlanningEdition, this.dateFinPlanningEdition)) {
			for (Mois mois : this.moisArray) {
				if (DateCtrlHamac.isBetweenEq(mois.getPremierJour(), this.dateDebutPlanningEdition, this.dateFinPlanningEdition) 
						|| DateCtrlHamac.isBetweenEq(mois.getDernierJour(), this.dateDebutPlanningEdition, this.dateFinPlanningEdition) 
						|| DateCtrlHamac.isBetweenEq(this.dateDebutPlanningEdition, mois.getPremierJour(), mois.getDernierJour()) 
						|| DateCtrlHamac.isBetweenEq(this.dateFinPlanningEdition, mois.getPremierJour(), mois.getDernierJour())) {
					lesMois.add(mois);
				}
			}
		}
		
		
		// le service
		dico.setObjectForKey(titreEdition, "leService");

		// les mois
		dico.setObjectForKey(lesMois, "lesMois");

		// les jours
		NSArray<NSArray<Jour>> lesJoursDesMois = new NSArray<NSArray<Jour>>();
		
		for (Mois mois : lesMois) {
			lesJoursDesMois = lesJoursDesMois.arrayByAddingObject(lesJoursPourMois(mois, this.dateDebutPlanningEdition, this.dateFinPlanningEdition));
			dico.setObjectForKey(lesJoursDesMois, "lesJoursDesMois");
		}
		
		

		// les individus
		// NSArray recsAffAnn = displayedAffectationAnnuelles();
		dico.setObjectForKey(eoPlanningArray.valueForKeyPath(
				EOPlanning.TO_PERSONNE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY), "lesIndividus");

		// le type d'impression
		dico.setObjectForKey(Boolean.valueOf(isEnCouleur), "enCouleur");

		// les couleurs
		NSArray<String> couleurs = new NSArray<String>();
		couleurs = couleurs.arrayByAddingObject("white");// classeNonTravail());
		couleurs = couleurs.arrayByAddingObject("blue");// classeTravail());
		couleurs = couleurs.arrayByAddingObject("deeppink");// classeAbsence());
		couleurs = couleurs.arrayByAddingObject("cornflowerblue");// classeTravail());
		couleurs = couleurs.arrayByAddingObject("plum");// classeAbsence());
		couleurs = couleurs.arrayByAddingObject("yellow");// classeAbsenceEnCoursVal());
		dico.setObjectForKey(couleurs, "lesCouleurs");

		// les couleurs
		NSArray lesCouleursDesIndividus = new NSArray();
		for (int i = 0; i < eoPlanningArray.count(); i++) {
			// pour tous les individus
			EOPlanning uneAffAnn = eoPlanningArray.objectAtIndex(i);
			NSArray lesCouleursPEtR2 = new NSArray();
			for (int index = 0; index < lesMois.count(); index++) {
				// par mois
				Mois moisEnCours = lesMois.objectAtIndex(index);
				NSArray lesCouleursDuMois = new NSArray();
				for (int j = 0; j < typePlanningArray.count(); j++) {
					// planning reel puis previsionnel
					String typePlanningEnCours = typePlanningArray.objectAtIndex(j);

					EOPlanning eoPlanning = null;
					boolean isReel = false;
					if (typePlanningEnCours.equals(EOPlanning.REEL)) {
						eoPlanning = uneAffAnn.getEoPlanningReel();
						isReel = true;
					} else if (typePlanningEnCours.equals(EOPlanning.PREV)) {
						eoPlanning = uneAffAnn.getEoPlanningPrev();
					} else {
						continue;
					}

					NSArray<String> presencesHorairesPourAffAnn = eoPlanning.presence().getPresenceArray(
							moisEnCours.getPremierJour(), moisEnCours.getDernierJour());

					NSArray<String> lesCodesCouleurs = new NSArray<String>();
					for (int k = 0; k < presencesHorairesPourAffAnn.count(); k++) {
						// etats des demi journees pour le mois
						String unePresence = (String) presencesHorairesPourAffAnn.objectAtIndex(k);
						lesCodesCouleurs = lesCodesCouleurs.arrayByAddingObject(
								Jour.getCodePourPresence(isReel, unePresence.substring(0, 1)));
						lesCodesCouleurs = lesCodesCouleurs.arrayByAddingObject(
								Jour.getCodePourPresence(isReel, unePresence.substring(1)));
					}

					lesCouleursDuMois = lesCouleursDuMois.arrayByAddingObject(lesCodesCouleurs);
				}
				lesCouleursPEtR2 = lesCouleursPEtR2.arrayByAddingObject(lesCouleursDuMois);
			}
			lesCouleursDesIndividus = lesCouleursDesIndividus.arrayByAddingObject(lesCouleursPEtR2);
		}

		dico.setObjectForKey(new NSArray(lesCouleursDesIndividus), "lesCodesCouleurs");

		return dico;
	}

	/**
	 * Méthode interne
	 * 
	 * @param mois
	 * @return
	 */
	private static NSArray<Jour> lesJoursPourMois(Mois mois, NSTimestamp dateDebutPlanningEdition, NSTimestamp dateFinPlanningEdition) {

		NSArray<Jour> lesJours = new NSArray<Jour>();

		for (Semaine semaine : mois.getSemaines()) {
			NSArray<Jour> jourArray = semaine.getJours();
			
			for (Jour jour : jourArray) {
				if (DateCtrlHamac.isBetweenEq(jour.getDate(), dateDebutPlanningEdition, dateFinPlanningEdition)) {
					lesJours = lesJours.arrayByAddingObject(jour);
				}
			}
			
			
		}

		return lesJours;
	}

	@Override
	public StringWriter creerXml() throws Exception {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		NSDictionary<String, Object> dico = buildDico();

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());

		SimpleDateFormat sdfPeriode = new SimpleDateFormat("MMMM yyyy");
		SimpleDateFormat sdfJourLibelle = new SimpleDateFormat("E");
		SimpleDateFormat sdfJourNumero = new SimpleDateFormat("dd");

		NSArray<Mois> lesMois = (NSArray) dico.objectForKey("lesMois");
		NSArray<NSArray<Jour>> lesJoursDesMois = (NSArray) dico.objectForKey("lesJoursDesMois");

		w.startElement("planning");
		{

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_MAIN_LOGO_URL, dico);

			w.writeElement("enCouleur", Boolean.toString(isEnCouleur));
			w.startElement("entetePlanning");
			{
				w.writeElement("service", StringCtrl.replace(titreEdition, "&", "et"));
				for (int i = 0; i < lesMois.count(); i++) {
					Mois unMois = lesMois.objectAtIndex(i);
					NSArray<Jour> lesJours = lesJoursDesMois.objectAtIndex(i);
					w.startElement("periode");
					{
						w.writeElement("nomPeriode", sdfPeriode.format(unMois.getPremierJour()));
						w.writeElement("nbJour", Integer.toString(lesJours.count()));
						for (int j = 0; j < lesJours.count(); j++) {
							Jour jour = lesJours.objectAtIndex(j);
							String strJour = sdfJourLibelle.format(jour.getDate()).substring(0, 1).toUpperCase() + sdfJourNumero.format(jour.getDate());
							w.writeElement("jour", strJour);
						}
					}
					w.endElement(); // "periode"
				}
			}
			w.endElement(); // "entetePlanning"

			// recup des couleurs
			NSArray<String> lesCouleurs = (NSArray) dico.objectForKey("lesCouleurs");
			w.writeElement("couleurNonTravail", lesCouleurs.objectAtIndex(0));
			w.writeElement("couleurTravailR", lesCouleurs.objectAtIndex(1));
			w.writeElement("couleurAbsenceR", lesCouleurs.objectAtIndex(2));
			w.writeElement("couleurTravailP", lesCouleurs.objectAtIndex(3));
			w.writeElement("couleurAbsenceP", lesCouleurs.objectAtIndex(4));
			w.writeElement("couleurAbsenceREnCoursVal", lesCouleurs.objectAtIndex(5));

			NSArray<String> lesIndividus = (NSArray) dico.objectForKey("lesIndividus");
			NSArray<NSArray<NSArray<NSArray<String>>>> lesCouleursDesIndividus = (NSArray) dico.objectForKey("lesCodesCouleurs");
			for (int i = 0; i < lesIndividus.count(); i++) {
				String unIndividu = lesIndividus.objectAtIndex(i);

				NSArray<NSArray<NSArray<String>>> lesCouleursPEtR = lesCouleursDesIndividus.objectAtIndex(i);

				w.startElement("Individu");
				{
					w.writeElement("nom", unIndividu);

					for (int j = 0; j < lesMois.count(); j++) {

						Mois unMois = lesMois.objectAtIndex(j);

						NSArray<NSArray<String>> lesCouleursDuMois = (NSArray) lesCouleursPEtR.objectAtIndex(j);

						w.startElement("periode");
						{

							w.writeElement("nomPeriode", sdfPeriode.format(unMois.getPremierJour()));

							NSArray<Jour> lesJours = lesJoursDesMois.objectAtIndex(j);

							w.writeElement("nbJour", Integer.toString(lesJours.count()));

							for (int k = 0; k < lesJours.count(); k++) {

								Jour jour = lesJours.objectAtIndex(k);

								w.startElement("detj");
								{
									String strJour = sdfJourLibelle.format(jour.getDate()).substring(0, 1).toUpperCase() + sdfJourNumero.format(jour.getDate());
									w.writeElement("jour", strJour);

									for (int l = 0; l < typePlanningArray.count(); l++) {

										NSArray<String> lesCodesCouleurs = lesCouleursDuMois.objectAtIndex(l);
										for (int m = k; m < k + 1; m++) {

											Hashtable<String, String> dicoAmPm = new Hashtable<String, String>();

											dicoAmPm.put("am", lesCodesCouleurs.objectAtIndex(2 * m));
											dicoAmPm.put("pm", lesCodesCouleurs.objectAtIndex(2 * m + 1));

											// dans l'édition, la première ligne s'appelle "R" et
											// l'autre "P"
											if (l == 0) {
												w.writeElement("R", dicoAmPm);
											} else {
												w.writeElement("P", dicoAmPm);
											}
										}

									}

								}
								w.endElement();

							}

							// for (int l = 0; l < typePlanningArray.count(); l++) {
							// String typePlanning = typePlanningArray.objectAtIndex(l);
							// NSArray<String> lesCodesCouleurs =
							// lesCouleursDuMois.objectAtIndex(l);
							// if (l == 0) {
							// w.writeElement("nbJour",
							// Integer.toString(lesCodesCouleurs.count() / 2));
							// }
							// Hashtable<String, String> dicoAmPm = new Hashtable<String,
							// String>();
							// for (int k = 0; k < lesCodesCouleurs.count(); k++) {
							// String unCodeCouleur = lesCodesCouleurs.objectAtIndex(k);
							// if (k % 2 == 1) {
							// dicoAmPm.put("pm" + (((k - 1) / 2) + 1), unCodeCouleur);
							// // w.writeElement("h", dicoAmPm);
							// } else {
							// dicoAmPm.put("am" + ((k / 2) + 1), unCodeCouleur);
							// }
							// }
							// w.writeElement(typePlanning, dicoAmPm);
							// }
						}
						w.endElement(); // "periode
					}
				}
				w.endElement(); // "Individu"
			}

			w.writeElement("noteBasDePage", "Imprimé le " + DateCtrlHamac.dateToString(DateCtrlHamac.now()));

		}

		w.endElement(); // "planning"

		w.endDocument();
		w.close();

		System.out.println(sw.toString());

		return sw;
	}

	public final boolean isEnCouleur() {
		return isEnCouleur;
	}

	public NSTimestamp getDateDebutPlanningEdition() {
		return dateDebutPlanningEdition;
	}

	public void setDateDebutPlanningEdition(NSTimestamp dateDebutPlanningEdition) {
		this.dateDebutPlanningEdition = dateDebutPlanningEdition;
	}

	public NSTimestamp getDateFinPlanningEdition() {
		return dateFinPlanningEdition;
	}

	public void setDateFinPlanningEdition(NSTimestamp dateFinPlanningEdition) {
		this.dateFinPlanningEdition = dateFinPlanningEdition;
	}
}
