/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactoryConsumer;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionReformulationDemandeCetCtrl
		extends A_EditionCtrl {

	private EOCetTransactionAnnuelle transaction;
	private EOPlanning eoPlanning;
	private boolean isAppVerifierStatutDemandeEpargneCet;
	private int alimentationMinutes;

	/**
	 * @param config
	 */
	public EditionReformulationDemandeCetCtrl(
			CktlConfig config,
			EOCetTransactionAnnuelle transaction,
			EOPlanning eoPlanning,
			boolean isAppVerifierStatutDemandeEpargneCet,
			int alimentationMinutes) {
		super(config, eoPlanning.toPeriode());
		this.transaction = transaction;
		this.eoPlanning = eoPlanning;
		this.isAppVerifierStatutDemandeEpargneCet = isAppVerifierStatutDemandeEpargneCet;
		this.alimentationMinutes = alimentationMinutes;
	}

	private final static String REPORT_FILENAME = "ReformulationDemandeCet.pdf";
	private final static String JASPER_FILENAME = "report/reformulationCet/CongesReformulationDemandeCet.jasper";
	private final static String RECORD_PATH = "/demande";
	private final static String PRINT_LABEL = "Edition de la reformulation de la demande CET";

	@Override
	public final String getReportFilename() {
		return REPORT_FILENAME;
	}

	@Override
	public final String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}

	private NSDictionary<String, Object> buildDico() {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		// dates
		dico.setObjectForKey(DateCtrlHamac.dateToString(DateCtrlHamac.now()), ConstsPrint.XML_KEY_DATE_IMPRESSION);
		dico.setObjectForKey(DateCtrlHamac.dateToString(transaction.dModification()), ConstsPrint.XML_KEY_DATE_DEMANDE);

		// URL du logo
		EditionFactory.feedDicoLogoEtablissement(getConfig(), dico);

		// president
		EditionFactory.feedDicoLibelleEtablissement(getConfig(), dico);
		EditionFactory.feedDicoPresident(getConfig(), dico);
		dico.setObjectForKey(HamacCktlConfig.stringForKey(EOParametre.SIGNATURE_PRESIDENT), ConstsPrint.XML_KEY_SIGNATURE_PRESIDENT);

		// ville
		EditionFactory.feedDicoVilleEtablissement(getConfig(), dico);

		// nom prenom civilité qualité
		EOIndividu eoIndividuDemandeur = (EOIndividu) transaction.toPersonne();
		dico.setObjectForKey(eoIndividuDemandeur.nomUsuel(), ConstsPrint.XML_KEY_NOM_DEMANDEUR);
		dico.setObjectForKey(eoIndividuDemandeur.prenom(), ConstsPrint.XML_KEY_PRENOM_DEMANDEUR);
		dico.setObjectForKey(eoIndividuDemandeur.toCivilite().cCivilite(), ConstsPrint.XML_KEY_CIVILITE);
		String strQualite = ".......................";
		if (!StringCtrl.isEmpty(eoIndividuDemandeur.indQualite())) {
			strQualite = StringCtrl.capitalizeWords(eoIndividuDemandeur.indQualite());
		}
		dico.setObjectForKey(strQualite, ConstsPrint.XML_KEY_QUALITE);

		// données RH
		NSArray<String> affectations = EditionFactory.listePeresToComposante(eoPlanning.getPlanning().getEoStructureAttenduArray());
		dico.setObjectForKey(affectations, ConstsPrint.XML_KEY_AFFECTATION_LISTE);
		if (isAppVerifierStatutDemandeEpargneCet) {
			String grade = GrhumFactory.getLibelleGradeForIndividu(eoPlanning);
			dico.setObjectForKey(grade, ConstsPrint.XML_KEY_GRADE_DEMANDEUR);
		}

		// CET
		int epargneDemandeEnJour7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemEpargne().intValue(), getDureeJourMinutes());
		dico.setObjectForKey(Integer.valueOf(epargneDemandeEnJour7h00), ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_JOURS_7H00);

		int epargneDecisionEnJour7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDecEpargne().intValue(), getDureeJourMinutes());
		dico.setObjectForKey(Integer.valueOf(epargneDecisionEnJour7h00), ConstsPrint.XML_KEY_DECISION_EPARGNE_EN_JOURS_7H00);

		dico.setObjectForKey(EditionFactory.getStrAnneeUnivNm1(eoPlanning), ConstsPrint.XML_KEY_ANNEE_UNIV_N_M_1);

		CetFactoryConsumer consumer = new CetFactoryConsumer(true, eoPlanning);
		boolean isDroitOption = consumer.isOptionFaisable(alimentationMinutes);
		dico.setObjectForKey(
				isDroitOption ? "true" : "false", ConstsPrint.XML_KEY_IS_EXERCICE_DROIT_OPTION);
		int valeurEpargneSoumiseAOptionEnJourA7h00Arrondi = 0;
		if (isDroitOption) {
			valeurEpargneSoumiseAOptionEnJourA7h00Arrondi = consumer.depassementSeuil20JoursPourEpargneCetEnJourEntier(alimentationMinutes);
		}
		dico.setObjectForKey(
				Integer.valueOf(valeurEpargneSoumiseAOptionEnJourA7h00Arrondi), ConstsPrint.XML_KEY_DECISION_TOTAL_OPTIONS_EN_JOURS_7H00);
		if (isDroitOption) {
			String strDate = DateCtrlHamac.dateToString(
					DateCtrlHamac.date1erJanAnneeUniv(eoPlanning.toPeriode().perDDebut()).timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0),
					"%d %B %Y");
			dico.setObjectForKey(
					strDate, ConstsPrint.XML_KEY_DATE_VALEUR_CET);
			dico.setObjectForKey(
					("Droit d'option de jours inscrits au compte épargne temps au " + strDate).toUpperCase(), ConstsPrint.XML_KEY_TITRE);
			dico.setObjectForKey(
					Integer.valueOf(valeurEpargneSoumiseAOptionEnJourA7h00Arrondi + 20), ConstsPrint.XML_KEY_DECISION_TOTAL_EN_JOURS_7H00);
			float soldeCetApresDecisionEpargneEnJours7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(
					consumer.soldeApresDecisionTransfertEpargneEtExerciceDroitDOptionEnMinutes(), getDureeJourMinutes());
			dico.setObjectForKey(
					new Float(soldeCetApresDecisionEpargneEnJours7h00), ConstsPrint.XML_KEY_SOLDE_CET_APRES_DECISION_EPARGNE_EN_JOURS_7H00);

		}

		return dico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.xml.A_EditionCtrl#creerXml()
	 */
	@Override
	public StringWriter creerXml() throws Exception {

		NSDictionary<String, Object> dico = buildDico();

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());
		w.startElement("demande");
		{

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_IMPRESSION, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_DEMANDE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_MAIN_LOGO_URL, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRHUM_ETAB, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRHUM_PRESIDENT, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SIGNATURE_PRESIDENT, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_VILLE, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_NOM_DEMANDEUR, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_PRENOM_DEMANDEUR, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_CIVILITE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_QUALITE, dico);

			NSArray affectationLabelList = (NSArray) dico.objectForKey(ConstsPrint.XML_KEY_AFFECTATION_LISTE);
			String strAffectationLabel = "";
			for (int i = 0; i < affectationLabelList.count(); i++) {
				strAffectationLabel += (String) affectationLabelList.objectAtIndex(i);
				if (i < affectationLabelList.count() - 1) {
					strAffectationLabel += ", ";
				}
			}
			w.writeElement(ConstsPrint.XML_KEY_AFFECTATION_LISTE, strAffectationLabel);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRADE_DEMANDEUR, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DECISION_EPARGNE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ANNEE_UNIV_N_M_1, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_IS_EXERCICE_DROIT_OPTION, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DECISION_TOTAL_OPTIONS_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_VALEUR_CET, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TITRE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DECISION_TOTAL_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SOLDE_CET_APRES_DECISION_EPARGNE_EN_JOURS_7H00, dico);

		}
		w.endElement(); // "demande"
		w.endDocument();
		w.close();

		return sw;
	}

}
