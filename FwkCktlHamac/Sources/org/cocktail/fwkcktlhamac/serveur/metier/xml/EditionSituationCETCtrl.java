/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.IOException;
import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactoryConsumer;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetCredit;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionSituationCETCtrl extends A_EditionCtrl {

	private final static String REPORT_FILENAME = "SituationCET";
	private final static String JASPER_FILENAME = "report/situationCet/CongesSituationCet.jasper";
	private final static String RECORD_PATH = "/situations/situation";
	private final static String PRINT_LABEL = "Edition de la situation CET";

	private NSArray<EOPlanning> eoPlanningArray;
	private NSTimestamp dateArret;
	private EOEditingContext ec;

	/**
	 * @param config
	 */
	public EditionSituationCETCtrl(CktlConfig config, NSArray<EOPlanning> eoPlanningArray, EOPeriode periode, NSTimestamp dateArret) {
		super(config, periode);
		if (!eoPlanningArray.isEmpty()) {
			ec = eoPlanningArray.lastObject().editingContext();
		}
		this.eoPlanningArray = eoPlanningArray;
		this.dateArret = dateArret;
		
	}

	@Override
	public final String getReportFilename() {

		if (eoPlanningArray.count() == 1) {
			EOIndividu eoIndividu = (EOIndividu) eoPlanningArray.lastObject().toPersonne();
			return REPORT_FILENAME + "_" + StringCtrl.toBasicString(eoIndividu.nomUsuel()) + "_" +
					StringCtrl.toBasicString(eoIndividu.prenom()) + ".pdf";
		} else {
			return REPORT_FILENAME + "_" + DateCtrlHamac.dateToString(dateArret) + ".pdf";
		}

	}

	@Override
	public final String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.xml.A_EditionCtrl#creerXml()
	 */
	@Override
	public StringWriter creerXml() throws Exception {
		NSDictionary<String, Object> dico = buildDico();

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());

		w.startElement("situations");
		{

			// date de l'arret
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_DATE_ARRET, dico);

			// logo
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_MAIN_LOGO_URL, dico);

			// adresse de l'établissement
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_LIBELLE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_1, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_2, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_CP_VILLE, dico);

			// on boucle sur autant de dicos qu'il y a de situations a sortir
			NSArray<NSDictionary<String, Object>> arrayDico = (NSArray<NSDictionary<String, Object>>) dico.objectForKey(ConstsPrint.SITUATION_ARRAY_DICO);

			for (int i = 0; i < arrayDico.count(); i++) {

				NSDictionary<String, Object> dicoSitu = arrayDico.objectAtIndex(i);

				w.startElement("situation");
				{

					// identité
					writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_CIVILITE, dicoSitu);
					writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_NOM_DEMANDEUR, dicoSitu);
					writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_PRENOM_DEMANDEUR, dicoSitu);
					writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRADE_DEMANDEUR, dicoSitu);

					// ancien regime
					writeLignesCredit(w, false, dicoSitu);

					// régime pérenne
					writeLignesCredit(w, true, dicoSitu);

					// solde final
					writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_SOLDE_FINAL_EN_JOURS_7H00, dicoSitu);
				}

				w.endElement(); // "situation"

			}

		}
		w.endElement(); // "situations"
		w.endDocument();
		w.close();

		return sw;
	}

	private NSDictionary<String, Object> buildDico() {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		dico.setObjectForKey(
				DateCtrlHamac.dateToString(dateArret), ConstsPrint.XML_KEY_SITUATION_DATE_ARRET);

		// adresse de l'etablissement
		EditionFactory.feedDicoAdresseEtablissement(ec, dico);

		// URL du logo
		EditionFactory.feedDicoLogoEtablissement(getConfig(), dico);

		// XXX filtrer à faire en amont
		NSMutableArray<Integer> persIdTraiteArray = new NSMutableArray<Integer>();

		// un seul dico car une seule situation
		NSMutableArray<NSDictionary<String, Object>> arrayDicoSituation = new NSMutableArray<NSDictionary<String, Object>>();
		for (EOPlanning eoPlanning : eoPlanningArray) {
			if (!persIdTraiteArray.containsObject(eoPlanning.persId())) {
				arrayDicoSituation.addObject(dicoSituationForPlanning(eoPlanning));
				persIdTraiteArray.addObject(eoPlanning.persId());
			}
		}
		dico.setObjectForKey(arrayDicoSituation, ConstsPrint.SITUATION_ARRAY_DICO);

		return dico.immutableClone();
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public NSDictionary<String, Object> dicoSituationForPlanning(EOPlanning eoPlanning) {

		EOIndividu eoIndividu = (EOIndividu) eoPlanning.toPersonne();

		NSMutableDictionary<String, Object> dicoSituation = new NSMutableDictionary<String, Object>();

		dicoSituation.setObjectForKey(eoIndividu.toCivilite().cCivilite(), ConstsPrint.XML_KEY_CIVILITE);
		dicoSituation.setObjectForKey(eoIndividu.prenom(), ConstsPrint.XML_KEY_PRENOM_DEMANDEUR);
		dicoSituation.setObjectForKey(eoIndividu.nomUsuel(), ConstsPrint.XML_KEY_NOM_DEMANDEUR);

		// grade
		dicoSituation.setObjectForKey(
				GrhumFactory.getLibelleGradeForIndividu(eoPlanning),
				ConstsPrint.XML_KEY_GRADE_DEMANDEUR);

		// la liste de toutes les transactions
		NSArray<I_Solde> soldeCetArray = eoPlanning.getCet().getSoldeArray();

		// ancien regime
		feedDicoForRegime(dicoSituation, false, eoPlanning, soldeCetArray);

		// regime perenne
		feedDicoForRegime(dicoSituation, true, eoPlanning, soldeCetArray);

		return dicoSituation;
	}

	/**
	 * 
	 * @param inDico
	 * @param isRegimePerenne
	 */
	private void feedDicoForRegime(
			NSMutableDictionary<String, Object> inDico, boolean isRegimePerenne,
			EOPlanning eoPlanning, NSArray<I_Solde> allSoldeArray) {

		EOQualifier qual = ERXQ.equals(
				I_CetCredit.IS_ANCIEN_REGIME_KEY, Boolean.valueOf(!isRegimePerenne));

		NSArray<I_Solde> soldeArrayForRegime = EOQualifier.filteredArrayWithQualifier(allSoldeArray, qual);

		if (soldeArrayForRegime.count() > 0) {
			// construire l'objet qui contiendra toutes les infos pour la classe
			// d'impression
			feedDicoForTransactions(inDico, isRegimePerenne, eoPlanning, soldeArrayForRegime);
		}
	}

	/**
	 * Ajouter les entrées au dico
	 */
	private void feedDicoForTransactions(
				NSMutableDictionary<String, Object> inDico,
				boolean isRegimePerenne,
				EOPlanning eoPlanning,
				NSArray<I_Solde> soldeArray) {

		NSMutableArray<NSMutableDictionary<String, Object>> array = new NSMutableArray<NSMutableDictionary<String, Object>>();

		float soldeIntermediaireEnJour7h00 = (float) 0;

		for (int i = 0; i < soldeArray.count(); i++) {

			I_Solde solde = soldeArray.objectAtIndex(i);

			// premiere transaction : le solde est le credit initial
			if (i == 0) {
				soldeIntermediaireEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(solde.getSoldeDelegate().getInit(), getDureeJourMinutes());
			} else {
				// transactions suivantes : on ajoute la valeur du crédit
				soldeIntermediaireEnJour7h00 += FormatterCtrl.enJourArrondi2ChiffresApresVirgule(solde.getSoldeDelegate().getInit(), getDureeJourMinutes());
			}

			// ajout du suffixe pour les transaction de l'ancien régime
			String suffix = "";
			if (!isRegimePerenne) {
				suffix = ConstsPrint.SUFFIX_ANCIEN_REGIME;
			}

			NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
			dico.setObjectForKey(solde.libelle(),
						ConstsPrint.XML_KEY_SITUATION_TRANSACTION_LIBELLE + suffix);
			dico.setObjectForKey(new Float(soldeIntermediaireEnJour7h00),
						ConstsPrint.XML_KEY_SITUATION_TRANSACTION_SOLDE_INITIAL_EN_JOURS_7H00 + suffix);
			dico.setObjectForKey(
					FormatterCtrl.enJourArrondi2ChiffresApresVirgule(
							((I_CetCredit) solde).credit() != null ? ((I_CetCredit) solde).credit() : ((I_CetCredit) solde).debit() , getDureeJourMinutes()),
						ConstsPrint.XML_KEY_SITUATION_TRANSACTION_VALEUR_EN_JOURS_7H00 + suffix);

			NSDictionary<String, Object> dicoDebitables = getDicoDebitablesForSolde(
							solde, suffix, i == 0, soldeIntermediaireEnJour7h00);
			// recuperation du solde intermediaire
			soldeIntermediaireEnJour7h00 = ((Float) dicoDebitables.objectForKey(ConstsPrint.DICO_KEY_SITUATION_SOLDE_INTERMEDIAIRE + suffix)).floatValue();
			dico.setObjectForKey(dicoDebitables,
						ConstsPrint.DICO_KEY_SITUATION_DICO_DEBITABLES);
			array.addObject(dico);

		}

		// solde final
		CetFactoryConsumer consumer = new CetFactoryConsumer(true, eoPlanning);
		int soldeFinalMinutes = consumer.minutesRestantesAncienRegime() + consumer.minutesRestantesRegimePerenne();
		float soldeFinalEnJours7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(soldeFinalMinutes, getDureeJourMinutes());
		inDico.setObjectForKey(new Float(soldeFinalEnJours7h00), ConstsPrint.XML_KEY_SITUATION_SOLDE_FINAL_EN_JOURS_7H00);

		// le array contenant les transactions
		String keyArray = ConstsPrint.DICO_KEY_SITUATION_ARRAY_LIGNE_CREDIT_ANCIEN_REGIME;
		if (isRegimePerenne) {
			keyArray = ConstsPrint.DICO_KEY_SITUATION_ARRAY_LIGNE_CREDIT_REGIME_PERENNE;
		}
		inDico.setObjectForKey(array, keyArray);
	}

	/**
	 *
   */
	private NSDictionary<String, Object> getDicoDebitablesForSolde(
			I_Solde solde, String suffix, boolean isPremiereTransaction, float soldeIntermediaire) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		NSArray<I_Operation> debitables = solde.getSoldeDelegate().getOperationArray();

		// alimenter le dico pour chaque debitable
		if (!NSArrayCtrl.isEmpty(debitables)) {
			NSArray<NSDictionary<String, Object>> arrayDicoDebitable = new NSArray<NSDictionary<String, Object>>();
			//
			for (int i = 0; i < debitables.count(); i++) {
				I_Operation debitable = debitables.objectAtIndex(i);
				NSMutableDictionary<String, Object> dicoDebitable = new NSMutableDictionary<String, Object>();
				dicoDebitable.setObjectForKey(
						debitable.toString(), ConstsPrint.XML_KEY_SITUATION_DEBITABLE_LIBELLE + suffix);
				float valeurEnJours7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(debitable.getOperationDelegate().getDebitSurSolde(solde), getDureeJourMinutes());
				dicoDebitable.setObjectForKey(
						new Float(valeurEnJours7h00), ConstsPrint.XML_KEY_SITUATION_DEBITABLE_VALEUR_EN_JOURS_7H00 + suffix);
				// soustraire cette valeur pour connaitre le solde suivant
				soldeIntermediaire = soldeIntermediaire - valeurEnJours7h00;
				// ne conserver que la valeur absolue, car les arrondis peuvent faire
				// afficher -0.00
				if (soldeIntermediaire < (float) 0) {
					soldeIntermediaire = -soldeIntermediaire;
				}
				dicoDebitable.setObjectForKey(
						new Float(soldeIntermediaire), ConstsPrint.XML_KEY_SITUATION_DEBITABLE_SOLDE_FINAL_EN_JOURS_7H00 + suffix);
				arrayDicoDebitable = arrayDicoDebitable.arrayByAddingObject(dicoDebitable.immutableClone());
			}
			dico.setObjectForKey(
					arrayDicoDebitable, ConstsPrint.DICO_KEY_SITUATION_ARRAY_DEBITABLES);
		} else {
			// si pas de debitable, alors on indique explicitement une ligne vide
			NSMutableDictionary<String, Object> dicoDebitableVide = new NSMutableDictionary<String, Object>();
			dicoDebitableVide.setObjectForKey(
					"-", ConstsPrint.XML_KEY_SITUATION_DEBITABLE_LIBELLE + suffix);
			dicoDebitableVide.setObjectForKey(
					"-", ConstsPrint.XML_KEY_SITUATION_DEBITABLE_VALEUR_EN_JOURS_7H00 + suffix);
			dicoDebitableVide.setObjectForKey(
					new Float(soldeIntermediaire), ConstsPrint.XML_KEY_SITUATION_DEBITABLE_SOLDE_FINAL_EN_JOURS_7H00 + suffix);
			dico.setObjectForKey(
					new NSArray<NSDictionary<String, Object>>(dicoDebitableVide.immutableClone()), ConstsPrint.DICO_KEY_SITUATION_ARRAY_DEBITABLES);
		}

		// stockage du solde intermediaire
		dico.setObjectForKey(new Float(soldeIntermediaire), ConstsPrint.DICO_KEY_SITUATION_SOLDE_INTERMEDIAIRE + suffix);

		return dico.immutableClone();
	}

	/**
	 * 
	 * @param w
	 * @param isRegimePerenne
	 * @throws IOException
	 */
	private void writeLignesCredit(CktlXMLWriter w, boolean isRegimePerenne, NSDictionary dico) throws IOException {
		NSArray lignes = null;
		if (isRegimePerenne) {
			lignes = (NSArray) dico.objectForKey(ConstsPrint.DICO_KEY_SITUATION_ARRAY_LIGNE_CREDIT_REGIME_PERENNE);
		} else {
			lignes = (NSArray) dico.objectForKey(ConstsPrint.DICO_KEY_SITUATION_ARRAY_LIGNE_CREDIT_ANCIEN_REGIME);
		}
		if (!NSArrayCtrl.isEmpty(lignes)) {
			String bloc = ConstsPrint.XML_KEY_SITUATION_BLOC_ANCIEN_REGIME;
			if (isRegimePerenne) {
				bloc = ConstsPrint.XML_KEY_SITUATION_BLOC_REGIME_PERENNE;
			}
			w.startElement(bloc);
			{
				for (int i = 0; i < lignes.count(); i++) {
					NSDictionary dicoLigne = (NSDictionary) lignes.objectAtIndex(i);

					// ajout du suffixe pour les transaction de l'ancien régime
					String suffix = "";
					if (!isRegimePerenne) {
						suffix = ConstsPrint.SUFFIX_ANCIEN_REGIME;
					}

					w.startElement(ConstsPrint.XML_KEY_SITUATION_TRANSACTION + suffix);
					{
						writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_TRANSACTION_LIBELLE + suffix, dicoLigne);
						writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_TRANSACTION_SOLDE_INITIAL_EN_JOURS_7H00 + suffix, dicoLigne);
						writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_TRANSACTION_VALEUR_EN_JOURS_7H00 + suffix, dicoLigne);

						NSDictionary dicoDebitableLigne = (NSDictionary) dicoLigne.valueForKey(ConstsPrint.DICO_KEY_SITUATION_DICO_DEBITABLES);

						writeDebitables(w, suffix, dicoDebitableLigne);
					}
					w.endElement(); // ConstsPrint.XML_KEY_SITUATION_TRANSACTION
				}
			}
			w.endElement(); // element
		}
	}

	/**
	 * Methode interne permettant d'alimenter le fichier XML avec une liste de
	 * debitables (cette methode est appelée 2 fois)
	 * 
	 * @param w
	 * @param dico
	 * @throws IOException
	 */
	private void writeDebitables(CktlXMLWriter w, String suffix, NSDictionary dico) throws IOException {
		// les debitables
		NSArray arrayDicoDebitable = (NSArray) dico.objectForKey(ConstsPrint.DICO_KEY_SITUATION_ARRAY_DEBITABLES);
		for (int i = 0; i < arrayDicoDebitable.count(); i++) {
			NSDictionary dicoDebitable = (NSDictionary) arrayDicoDebitable.objectAtIndex(i);
			w.startElement(ConstsPrint.XML_KEY_SITUATION_LIGNE_DEBITABLE + suffix);
			{
				writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_DEBITABLE_LIBELLE + suffix, dicoDebitable);
				writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_DEBITABLE_VALEUR_EN_JOURS_7H00 + suffix, dicoDebitable);
				writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SITUATION_DEBITABLE_SOLDE_FINAL_EN_JOURS_7H00 + suffix, dicoDebitable);
			}
			w.endElement(); // ConstsPrint.XML_KEY_SITUATION_LIGNE_DEBITABLE
		}
	}
}
