package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.services.ContratsService;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EditionAttestationTravailCtrl extends A_EditionCtrl {

	private final static String REPORT_FILENAME = "AttestationTravail";
	private final static String JASPER_FILENAME = "report/attestationTravail/AttestationJoursTravailles.jasper";
	private final static String RECORD_PATH = "/attestation";
	private final static String PRINT_LABEL = "Edition de l'attestation de travail d'un agent";
	
	private EOIndividu eoIndividuAgent;
	private EOPlanning eoPlanning;
	private Integer annee;
	private EOEditingContext ec;
	
	private NSTimestamp premierJourPresence, dernierJourPresence;
	private Integer nbJoursTravailles = Integer.valueOf(0);
	
	private NSArray<Jour> joursArray = new NSMutableArray<Jour>();
	
	/**
	 * @param eoPlanning planning
	 * @param annee annee concernée
	 * @param config config
	 */
	public EditionAttestationTravailCtrl(
			CktlConfig config,
			Integer annee,
			EOPlanning eoPlanning) {
		super(config);
		this.eoPlanning = eoPlanning;
		if (eoPlanning != null) {
			this.eoIndividuAgent = (EOIndividu) eoPlanning.toPersonne();
			this.ec = eoPlanning.editingContext();
		}
		this.annee = annee;
		
		
	}

	
	@Override
	public String getReportFilename() {
		return REPORT_FILENAME;
	}

	@Override
	public String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public String getPrintLabel() {
		return PRINT_LABEL;
	}
	
	

	@Override
	public StringWriter creerXml() throws Exception {

		NSDictionary<String, Object> dico = buildDico();

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());
		w.startElement("attestation");

		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_IMPRESSION, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_MAIN_LOGO_URL, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_VILLE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_PRENOM_NOM_PRESIDENT, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_AFFICHAGE_E_PRESIDENT, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRHUM_PRESIDENT, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_CIV_NOM_PRENOM_AGENT, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_AFFICHAGE_E_AGENT, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_QUALITE_AGENT, dico);
		
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_NB_JOURS_PRESENCE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_D_DEBUT_PRESENCE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ATTESTATION_D_FIN_PRESENCE, dico);
		
		
		
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_LIBELLE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_1, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_2, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_CP_VILLE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_CONTACT_TELEPHONE, dico);
		writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_CONTACT_FAX, dico);

		w.endElement(); // "attestation"
		w.endDocument();
		w.close();

		return sw;
	}
	
	
	private NSDictionary<String, Object> buildDico() {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();
	
		NSTimestamp dateJour = DateCtrl.now();
		
		determinerListeJours();
		
		// URL du logo
		EditionFactory.feedDicoLogoEtablissement(getConfig(), dico);
		
		dico.setObjectForKey(DateCtrlHamac.dateToString(DateCtrlHamac.now()), ConstsPrint.XML_KEY_DATE_IMPRESSION);
		
		EditionFactory.feedDicoVilleEtablissement(getConfig(), dico);
		
		EOStructure racineEtablissement = EOStructure.rechercherEtablissement(ec);
		EOIndividu presidentEtablissement = racineEtablissement.toResponsable();
		
		if (presidentEtablissement != null) {
		
			dico.setObjectForKey(presidentEtablissement.prenomAffichage() + " " + presidentEtablissement.nomAffichage(), ConstsPrint.XML_KEY_ATTESTATION_PRENOM_NOM_PRESIDENT);
			
			if (EOCivilite.C_CIVILITE_MADAME.equals(presidentEtablissement.toCivilite().getCode()) || EOCivilite.C_CIVILITE_MADEMOISELLE.equals(presidentEtablissement.toCivilite().getCode())) {
				dico.setObjectForKey(true, ConstsPrint.XML_KEY_ATTESTATION_AFFICHAGE_E_PRESIDENT);
			}
		}
		
		EditionFactory.feedDicoPresident(getConfig(), dico);
		
		if (eoIndividuAgent != null) {
		
			dico.setObjectForKey(eoIndividuAgent.toCivilite().lCivilite() + " " + eoIndividuAgent.prenomAffichage() + " " + eoIndividuAgent.nomAffichage(), ConstsPrint.XML_KEY_ATTESTATION_CIV_NOM_PRENOM_AGENT);
			
			if (EOCivilite.C_CIVILITE_MADAME.equals(eoIndividuAgent.toCivilite().getCode()) || EOCivilite.C_CIVILITE_MADAME.equals(eoIndividuAgent.toCivilite().getCode())) {
				dico.setObjectForKey(true, ConstsPrint.XML_KEY_ATTESTATION_AFFICHAGE_E_AGENT);
			}
			
			
			NSArray<EOIndividu> arrayIndividu = new NSMutableArray<EOIndividu>();
			arrayIndividu.add(eoIndividuAgent);
			
			NSArray<EOContrat> contrats = ContratsService.creerNouvelleInstance(ec).contratsPourIndividusEtDate(arrayIndividu, dateJour);
			
			if (!contrats.isEmpty()) {
				dico.setObjectForKey("AGENT CONTRACTUEL", ConstsPrint.XML_KEY_ATTESTATION_QUALITE_AGENT);		
			} else {
				String libelleCorps = GrhumFactory.getLibelleCorpsForIndividu(eoIndividuAgent, dateJour, dateJour);
				dico.setObjectForKey(libelleCorps, ConstsPrint.XML_KEY_ATTESTATION_QUALITE_AGENT);
			}
			
		}
		
		dico.setObjectForKey(nbJoursTravailles, ConstsPrint.XML_KEY_ATTESTATION_NB_JOURS_PRESENCE);
		dico.setObjectForKey(DateCtrlHamac.dateToString(premierJourPresence, "dd MMMM yyyy"), ConstsPrint.XML_KEY_ATTESTATION_D_DEBUT_PRESENCE);
		dico.setObjectForKey(DateCtrlHamac.dateToString(dernierJourPresence, "dd MMMM yyyy"), ConstsPrint.XML_KEY_ATTESTATION_D_FIN_PRESENCE);
		
		EditionFactory.feedDicoAdresseEtablissement(ec, dico);
		EditionFactory.feedDicoCoordonneesEtablissement(ec, dico);
		
		eoPlanning.clearCache();
		
		return dico;
	}

	/**
	 * Liste les jours présents dans les planning
	 * @return
	 */
	private void determinerListeJours() {
		
		NSTimestamp dateAnneeSelectionnee = new NSTimestamp(NSTimestamp.valueOf(annee + "-01-01 00:00:00"));
		
		NSTimestamp dateDebutAnneeCivile = DateCtrlHamac.dateToDebutAnneeCivile(dateAnneeSelectionnee);
		NSTimestamp dateFinAnneeCivile = DateCtrlHamac.dateToFinAnneeCivile(dateAnneeSelectionnee);
		
		Planning planning = eoPlanning.getPlanning();
		
		NSArray<Jour> arrayJoursPlanning = planning.getJours(dateDebutAnneeCivile, dateFinAnneeCivile);
		
		joursArray.addAll(arrayJoursPlanning);
		
		EOPlanning eoPlanningPrecedent = eoPlanning.getEoPlanningPrecedent();
		
		if (eoPlanningPrecedent != null) {
		
			Planning planningPrecedent = eoPlanningPrecedent.getPlanning();
			
			while (eoPlanningPrecedent != null && !eoPlanningPrecedent.toPeriode().perDFin().before(dateDebutAnneeCivile)) {
				
				joursArray.addAll(planningPrecedent.getJours(dateDebutAnneeCivile, dateFinAnneeCivile));
				
				eoPlanningPrecedent = eoPlanningPrecedent.getEoPlanningPrecedent();
				if (eoPlanningPrecedent != null) {
					planningPrecedent = eoPlanningPrecedent.getPlanning();
				}
				
			}
		
		}
		
		calculerInfosDepuisJoursArray();
		
	}
	
	private void calculerInfosDepuisJoursArray() {
		
		if (joursArray != null) {
		
			for (Jour jour : joursArray) {
				
				if ((premierJourPresence == null || DateCtrlHamac.isBefore(jour.getDate(), premierJourPresence)) && jour.quotite() > 0) {
					premierJourPresence = jour.getDate();
				}
				if ((dernierJourPresence == null || DateCtrlHamac.isAfter(jour.getDate(), dernierJourPresence)) && jour.quotite() > 0) {
					dernierJourPresence = jour.getDate();
				}
				
				if (jour.isJourTravailleImpot()) {
					nbJoursTravailles += 1;
				}
			}
		
		}
		
	}
	
	
	public Integer getAnnee() {
		return annee;
	}


	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

}
