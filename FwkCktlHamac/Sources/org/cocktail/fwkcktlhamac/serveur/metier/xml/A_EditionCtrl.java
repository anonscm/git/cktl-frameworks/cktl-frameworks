/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.IOException;
import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public abstract class A_EditionCtrl {

	private CktlConfig config;
	private int dureeJourMinutes;
	private EOPeriode periode;

	public A_EditionCtrl(CktlConfig config, EOPeriode periode) {
		super();
		this.config = config;
		this.periode = periode;
		dureeJourMinutes = HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, this.periode);
	}
	
	public A_EditionCtrl(CktlConfig config) {
		super();
		this.config = config;
	}

	protected CktlConfig getConfig() {
		return config;
	}

	protected int getDureeJourMinutes() {
		return dureeJourMinutes;
	}

	public abstract String getReportFilename();

	public abstract String getJasperFilename();

	public abstract String getRecordPath();

	public abstract String getPrintLabel();

	public abstract StringWriter creerXml() throws Exception;

	/**
	 * Ecrire dans le {@link LRXMLWriter} w la valeur du dictionnaire
	 * {@link #dico()} ayant pour la clé key avec la clé identique
	 * 
	 * @param dico
	 * @param key
	 * @throws IOException
	 */
	protected static void writeElementSameKeyForDico(
			CktlXMLWriter w, String key, NSDictionary<String, Object> dico) throws IOException {
		writeElement(w, key, dico.objectForKey(key));
	}

	/**
	 * 
	 * @param w
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	protected static void writeElement(CktlXMLWriter w, String key, Object value) throws IOException {

		if (value != null) {
			String strObject = value.toString();

			if (value instanceof Float) {
				// pour les float, ce sont des jours à afficher avec 2 virgules
				NSNumberFormatter numberFormat = new NSNumberFormatter("0.00");
				strObject = numberFormat.format((Float) value);
			} else if (value instanceof Integer) {
				// les integer doivent être affichés normalement
				strObject = ((Integer) value).toString();
			}

			w.writeElement(key, strObject);

		}

	}

	public EOPeriode getPeriode() {
		return periode;
	}
}
