/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import org.cocktail.fwkcktlhamac.serveur.I_ConstantesApplication;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Ensemble de méthodes utilitaires utilisées par les éditions à déplacer dans
 * des frameworks ...
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class EditionFactory {

	/**
	 * retourne la liste des structures pere jusqu'a la composante d appartenance
	 * ex : LABO l3i -> LABOS -> SCIENCES
	 * 
	 * @param eoStructure
	 * @return
	 */
	private static NSArray<String> listePeresToComposante(EOStructure eoStructure) {
		NSArray<String> array = new NSArray<String>(eoStructure.llStructure());

		EOStructure prevPere = eoStructure;
		EOStructure currentPere = prevPere.toStructurePere();

		while (!currentPere.isComposante() && prevPere != currentPere) {
			prevPere = currentPere;
			currentPere = prevPere.toStructurePere();
			array = array.arrayByAddingObject(currentPere.llStructure());
		}
		if (currentPere != eoStructure) {
			array = array.arrayByAddingObject(currentPere.llStructure());
		}

		array = NSArrayCtrl.removeDuplicate(array);

		return array;
	}

	/**
	 * La même avec plusieurs structures
	 * 
	 * @param eoStructure
	 * @return
	 */
	public static NSArray<String> listePeresToComposante(NSArray<EOStructure> eoStructureArray) {
		NSArray<String> array = new NSArray<String>();

		for (EOStructure eoStructure : eoStructureArray) {
			array = array.arrayByAddingObjectsFromArray(listePeresToComposante(eoStructure));
		}

		return array;
	}

	/**
	 * Récupérer l'adresse de l'établissement racine
	 */
	private static EOAdresse getAdresse(EOStructure eoStructure) {
		EOAdresse adresse = null;

		EORepartPersonneAdresse repart = eoStructure.adresseProfessionnelle();
		if (repart != null) {
			adresse = repart.toAdresse();
		}

		return adresse;
	}

	/**
	 * Complete le dictionnaire avec l'adresse de l'établissement
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoAdresseEtablissement(EOEditingContext ec, NSMutableDictionary<String, Object> dico) {

		// adresse de l'etablissement
		EOStructure etablissement = EOStructure.rechercherEtablissement(ec);
		dico.setObjectForKey(etablissement.llStructure(), ConstsPrint.XML_KEY_ETAB_ADRESSE_LIBELLE);
		EOAdresse adresse = getAdresse(etablissement);
		String adrAdresse1 = "";
		if (adresse != null) {
			if (!StringCtrl.isEmpty(adresse.adrAdresse1())) {
				adrAdresse1 = adresse.adrAdresse1();
			}
			String adrAdresse2 = "";
			if (!StringCtrl.isEmpty(adresse.adrAdresse2())) {
				adrAdresse2 = adresse.adrAdresse2();
			}
			String codePostalVille = "";
			if (!StringCtrl.isEmpty(adresse.codePostal()) && !StringCtrl.isEmpty(adresse.ville())) {
				codePostalVille = adresse.codePostal() + " " + adresse.ville();
			}
			dico.setObjectForKey(adrAdresse1, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_1);
			dico.setObjectForKey(adrAdresse2, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_2);
			dico.setObjectForKey(codePostalVille, ConstsPrint.XML_KEY_ETAB_ADRESSE_CP_VILLE);
		}
		
	}
	

	/**
	 * Complete le dictionnaire avec les coordonnées de l'établissement
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoCoordonneesEtablissement(EOEditingContext ec, NSMutableDictionary<String, Object> dico) {

		// coordonnées de l'etablissement
		EOStructure etablissement = EOStructure.rechercherEtablissements(ec).lastObject();
		
		NSArray<EOPersonneTelephone> arrayPersonneTelephone = etablissement.toPersonneTelephones(EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK);
		
		boolean isTelephoneTrouve = false;
		boolean isFaxTrouve = false;
		
		for (EOPersonneTelephone eoPersonneTelephone : arrayPersonneTelephone) {
			eoPersonneTelephone.telPrincipal();
			if (EOTypeNoTel.TYPE_NO_TEL_TEL.equals(eoPersonneTelephone.typeNo()) && (!isTelephoneTrouve || eoPersonneTelephone.isTelPrincipal())) {
				dico.setObjectForKey(eoPersonneTelephone.noTelephone(), ConstsPrint.XML_KEY_ETAB_CONTACT_TELEPHONE);
				isTelephoneTrouve = true;
			}
			if (EOTypeNoTel.TYPE_NO_TEL_FAX.equals(eoPersonneTelephone.typeNo()) && (!isFaxTrouve || eoPersonneTelephone.isTelPrincipal())) {
				dico.setObjectForKey(eoPersonneTelephone.noTelephone(), ConstsPrint.XML_KEY_ETAB_CONTACT_FAX);
				isFaxTrouve = true;
			}
		}
		
	}
	

	/**
	 * Complete le dictionnaire avec l'URL du logo de l'établissement
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoLogoEtablissement(CktlConfig config, NSMutableDictionary<String, Object> dico) {
		
		String mainLogoUrl = config.stringForKey(I_ConstantesApplication.MAIN_LOGO_URL_PARAM_KEY);
		if (mainLogoUrl != null) {
			dico.setObjectForKey(mainLogoUrl, ConstsPrint.XML_KEY_MAIN_LOGO_URL);
		}
	}

	/**
	 * Complete le dictionnaire avec le nom du président
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoPresident(CktlConfig config, NSMutableDictionary<String, Object> dico) {
		dico.setObjectForKey(
				config.stringForKey(I_ConstantesApplication.GRHUM_PRESIDENT_PARAM_KEY), ConstsPrint.XML_KEY_GRHUM_PRESIDENT);
	}

	/**
	 * Complete le dictionnaire avec le nom de l'établissement
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoLibelleEtablissement(CktlConfig config, NSMutableDictionary<String, Object> dico) {
		dico.setObjectForKey(
				config.stringForKey(I_ConstantesApplication.GRHUM_ETAB_PARAM_KEY), ConstsPrint.XML_KEY_GRHUM_ETAB);
	}

	/**
	 * Complete le dictionnaire avec la ville de l'établissement
	 * 
	 * @param ec
	 * @param dico
	 */
	public final static void feedDicoVilleEtablissement(CktlConfig config, NSMutableDictionary<String, Object> dico) {
		dico.setObjectForKey(
				config.stringForKey(I_ConstantesApplication.GRHUM_VILLE_PARAM_KEY), ConstsPrint.XML_KEY_VILLE);
	}

	/**
	 * L'année universitaire précedente au format XXXX/YYYY
	 */
	public static String getStrAnneeUnivNm1(EOPlanning eoPlanning) {
		String strAnneeUnivNm1 = DateCtrlHamac.anneeLibelleForPeriode(
				eoPlanning.toPeriode().perDDebut().timestampByAddingGregorianUnits(-1, 0, 0, 0, 0, 0), 
				eoPlanning.toPeriode().perDFin().timestampByAddingGregorianUnits(-1, 0, 0, 0, 0, 0));

		return strAnneeUnivNm1;
	}

}
