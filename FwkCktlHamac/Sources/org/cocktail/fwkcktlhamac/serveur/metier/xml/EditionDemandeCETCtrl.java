/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactoryConsumer;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.FormatterCtrl;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionDemandeCETCtrl extends A_EditionCtrl {

	private final static String REPORT_FILENAME = "DemandeCET";
	private final static String JASPER_FILENAME = "report/demandeCet/CongesDemandeCET.jasper";
	private final static String RECORD_PATH = "/demande";
	private final static String PRINT_LABEL = "Edition de la demande de bascule de CET";

	private String messageEditionDemandeEpargneCet;
	private EOIndividu eoIndividuDemandeur;
	private boolean isAppVerifierStatutDemandeEpargneCet;
	private EOEditingContext ec;
	private EOCetTransactionAnnuelle transaction;
	private EOPlanning eoPlanning;
	private CetFactoryConsumer cetFactoryConsumer;

	/**
	 * @param config
	 */
	public EditionDemandeCETCtrl(
			CktlConfig config,
			EOPlanning eoPlanning,
			boolean isAppVerifierStatutDemandeEpargneCet,
			String messageEditionDemandeEpargneCet) {
		super(config, eoPlanning.toPeriode());
		this.eoPlanning = eoPlanning;
		this.messageEditionDemandeEpargneCet = messageEditionDemandeEpargneCet;
		this.eoIndividuDemandeur = (EOIndividu) eoPlanning.toPersonne();
		this.isAppVerifierStatutDemandeEpargneCet = isAppVerifierStatutDemandeEpargneCet;
		this.ec = eoPlanning.editingContext();
		this.transaction = eoPlanning.getEoCetTransactionAnnuelle();
		cetFactoryConsumer = new CetFactoryConsumer(true, eoPlanning);
	}

	@Override
	public final String getReportFilename() {
		return REPORT_FILENAME + "_" + StringCtrl.toBasicString(eoIndividuDemandeur.nomUsuel()) + "_" +
				StringCtrl.toBasicString(eoIndividuDemandeur.prenom()) + "_" +
				StringCtrl.replace(DateCtrlHamac.anneeLibelleForPeriode(eoPlanning.toPeriode().perDDebut(), eoPlanning.toPeriode().perDFin()), "/", "_") + ".pdf";
	}

	@Override
	public final String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}

	@Override
	public StringWriter creerXml() throws Exception {

		NSDictionary<String, Object> dico = buildDico();

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());

		w.startElement("demande");
		{

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_NOM_DEMANDEUR, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_PRENOM_DEMANDEUR, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_CIVILITE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_QUALITE, dico);

			NSArray<String> affectationLabelList = (NSArray<String>) dico.objectForKey(ConstsPrint.XML_KEY_AFFECTATION_LISTE);
			String strAffectationLabel = "";
			for (int i = 0; i < affectationLabelList.count(); i++) {
				strAffectationLabel += affectationLabelList.objectAtIndex(i);
				if (i < affectationLabelList.count() - 1) {
					strAffectationLabel += ", ";
				}
			}
			w.writeElement(ConstsPrint.XML_KEY_AFFECTATION_LISTE, strAffectationLabel);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_GRADE_DEMANDEUR, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_LIBELLE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_1, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_ADRESSE_2, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ETAB_ADRESSE_CP_VILLE, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ANNEE_CIVILE_N, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_ANNEE_UNIV_N_M_1, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_IS_MAINTIEN_ANCIEN_CET, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_IS_RENONCEMENT, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_IS_EPARGNE, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_IS_EXERCICE_DROIT_OPTION, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_JOURS_7H00_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_JOURS_7H00_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_JOURS_7H00_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_JOURS_7H00_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_HEURES_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_HEURES_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_HEURES_ANCIEN_SYSTEME, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_HEURES_ANCIEN_SYSTEME, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SOLDE_CET_AVANT_DEMANDE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TOTAL_EPARGNE_DEMANDEE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DUREE_JOURNEE_CET, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_HEURES, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_HEURES, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_HEURES, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_HEURES, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TRANSFERT_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TRANSFERT_EN_HEURES, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_HEURES, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_ANNEE_CIVILE_N, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_JOURS_7H00, dico);
			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_JOURS_7H00, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SOLDE_CET_APRES_EPARGNE_ET_DECISION_EN_JOURS_7H00, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_MAIN_LOGO_URL, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_VILLE, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_DATE_IMPRESSION, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TITRE, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SUFFIXE_PHRASE_ACCORD_PRESIDENT, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_SUFFIXE_PHRASE_SOLDE_CET_FINAL, dico);

			writeElementSameKeyForDico(w, ConstsPrint.XML_KEY_TITRE_RESPONSABLE_ETABLISSEMENT_DEMANDE, dico);

		}

		w.endElement(); // "demande"
		w.endDocument();
		w.close();

		System.out.println(sw.toString());

		return sw;
	}

	private NSDictionary<String, Object> buildDico() {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		// initialisation valeurs par défaut
		dico.setObjectForKey(new Float(0.00), ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_JOURS_7H00);
		dico.setObjectForKey("00:00",  ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_HEURES);
		dico.setObjectForKey(new Float(0.00), ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_JOURS_7H00);
		dico.setObjectForKey("00:00",  ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_HEURES);
		dico.setObjectForKey(new Float(0.00), ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_JOURS_7H00);
		dico.setObjectForKey("00:00",  ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_HEURES);
		dico.setObjectForKey(new Float(0.00), ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_JOURS_7H00);
		dico.setObjectForKey("00:00",  ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_HEURES);
		dico.setObjectForKey(new Float(0.00), ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_JOURS_7H00);
		dico.setObjectForKey("00:00",  ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_HEURES);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_SOLDE_CET_AVANT_DEMANDE_EN_JOURS_7H00);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_TOTAL_EPARGNE_DEMANDEE_EN_JOURS_7H00);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_JOURS_7H00);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_JOURS_7H00);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_JOURS_7H00);
		dico.setObjectForKey(Integer.valueOf(0), ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_JOURS_7H00);
		
		
		// message a afficher lorsque le fichier PDF est disponible
		if (!StringCtrl.isEmpty(messageEditionDemandeEpargneCet)) {
			dico.setObjectForKey(messageEditionDemandeEpargneCet, ConstsPrint.DICO_KEY_ENDING_MESSAGE);
		}

		// URL du logo
		EditionFactory.feedDicoLogoEtablissement(getConfig(), dico);

		// president
		dico.setObjectForKey(
				HamacCktlConfig.stringForKey(EOParametre.TITRE_RESPONSABLE_ETABLISSEMENT_DEMANDE),
				ConstsPrint.XML_KEY_TITRE_RESPONSABLE_ETABLISSEMENT_DEMANDE);

		// nom prenom civilité qualité
		dico.setObjectForKey(eoIndividuDemandeur.nomUsuel(), ConstsPrint.XML_KEY_NOM_DEMANDEUR);
		dico.setObjectForKey(eoIndividuDemandeur.prenom(), ConstsPrint.XML_KEY_PRENOM_DEMANDEUR);
		dico.setObjectForKey(eoIndividuDemandeur.toCivilite().cCivilite(), ConstsPrint.XML_KEY_CIVILITE);
		String strQualite = ".......................";
		if (!StringCtrl.isEmpty(eoIndividuDemandeur.indQualite())) {
			strQualite = StringCtrl.capitalizeWords(eoIndividuDemandeur.indQualite());
		}
		dico.setObjectForKey(strQualite, ConstsPrint.XML_KEY_QUALITE);

		// données RH
		NSArray<String> affectations = EditionFactory.listePeresToComposante(eoPlanning.getPlanning().getEoStructureAttenduArray());
		dico.setObjectForKey(affectations, ConstsPrint.XML_KEY_AFFECTATION_LISTE);
		if (isAppVerifierStatutDemandeEpargneCet) {
			String grade = GrhumFactory.getLibelleGradeForIndividu(eoPlanning);
			dico.setObjectForKey(grade, ConstsPrint.XML_KEY_GRADE_DEMANDEUR);
		}

		// adresse de l'etablissement
		EditionFactory.feedDicoAdresseEtablissement(ec, dico);

		// annees de references
		String strAnneeCivileEnCours = DateCtrlHamac.dateToString(
				DateCtrlHamac.dateToDebutAnneeCivile(eoPlanning.toPeriode().perDDebut())).substring(6, 10);
		dico.setObjectForKey(strAnneeCivileEnCours, ConstsPrint.XML_KEY_ANNEE_CIVILE_N);
		dico.setObjectForKey(EditionFactory.getStrAnneeUnivNm1(eoPlanning), ConstsPrint.XML_KEY_ANNEE_UNIV_N_M_1);

		//
		// -- spécifités de la demande d'épargne CET
		//

		// - == les variables de base == -

		// maintenir des jours dans l'ancien CET O/N (possible une seule fois)
		boolean isMaintienAncienCet = false;
		if (transaction.cetDemMaintAncien() != null &&
				transaction.cetDemMaintAncien().intValue() > 0) {
			isMaintienAncienCet = true;
		}
		dico.setObjectForKey(
				isMaintienAncienCet ? "true" : "false", ConstsPrint.XML_KEY_IS_MAINTIEN_ANCIEN_CET);

		// exercice du droit d'option si maintien ancien CET
		if (isMaintienAncienCet) {
			int demandeTransfertRafpEnJourA7h00AncienSysteme = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemRafpAncien().intValue(), getDureeJourMinutes());
			int demandeIndemnisationEnJourA7h00AncienSysteme = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemIndemAncien().intValue(), getDureeJourMinutes());
			float demandeMaintienCetEnJourA7h00AncienSysteme = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(transaction.cetDemMaintAncien().intValue(), getDureeJourMinutes());
			float totalEnJourA7h00 =
					(float) demandeTransfertRafpEnJourA7h00AncienSysteme +
							(float) demandeIndemnisationEnJourA7h00AncienSysteme +
							demandeMaintienCetEnJourA7h00AncienSysteme;

			dico.setObjectForKey(
					new Float(totalEnJourA7h00), ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_JOURS_7H00_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					Integer.valueOf(demandeTransfertRafpEnJourA7h00AncienSysteme), ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_JOURS_7H00_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					Integer.valueOf(demandeIndemnisationEnJourA7h00AncienSysteme), ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_JOURS_7H00_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					new Float(demandeMaintienCetEnJourA7h00AncienSysteme), ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_JOURS_7H00_ANCIEN_SYSTEME);

			String demandeTransfertRafpEnHeuresAncienSysteme = TimeCtrl.stringForMinutes(transaction.cetDemRafpAncien().intValue());
			String demandeIndemnisationEnHeuresAncienSysteme = TimeCtrl.stringForMinutes(transaction.cetDemIndemAncien().intValue());
			String demandeMaintienCetEnHeuresAncienSysteme = TimeCtrl.stringForMinutes(transaction.cetDemMaintAncien().intValue());
			String totalEnHeures =
					TimeCtrl.stringForMinutes(
							TimeCtrl.getMinutes(demandeTransfertRafpEnHeuresAncienSysteme) +
									TimeCtrl.getMinutes(demandeIndemnisationEnHeuresAncienSysteme) +
									TimeCtrl.getMinutes(demandeMaintienCetEnHeuresAncienSysteme));

			dico.setObjectForKey(
					totalEnHeures, ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_HEURES_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					demandeTransfertRafpEnHeuresAncienSysteme, ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_HEURES_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					demandeIndemnisationEnHeuresAncienSysteme, ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_HEURES_ANCIEN_SYSTEME);
			dico.setObjectForKey(
					demandeMaintienCetEnHeuresAncienSysteme, ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_HEURES_ANCIEN_SYSTEME);
		}

		// renoncer O/N à ses jours de l'ancien CET
		boolean isRenoncement = false;
		if (transaction.cetDemTransAncienPerenne() != null &&
				transaction.cetDemTransAncienPerenne().intValue() > 0) {
			isRenoncement = true;
		}
		dico.setObjectForKey(
				isRenoncement ? "true" : "false", ConstsPrint.XML_KEY_IS_RENONCEMENT);

		// valeur du transfert si renoncement
		int transfertEnMinutes = 0;
		if (isRenoncement) {
			transfertEnMinutes = transaction.cetDemTransAncienPerenne().intValue();
		}

		// valeur de l'epargne demandée sur les reliquats de congés N-1
		boolean isEpargne = false;
		int epargneEnMinutes = 0;
		if (transaction.cetDemEpargne() != null &&
				transaction.cetDemEpargne().intValue() > 0) {
			isEpargne = true;
			epargneEnMinutes = transaction.cetDemEpargne().intValue();
		}
		dico.setObjectForKey(
				isEpargne ? "true" : "false", ConstsPrint.XML_KEY_IS_EPARGNE);

		// exercice du droit d'option O/N
		boolean isExerciceDroitOption = cetFactoryConsumer.isOptionPossible(epargneEnMinutes + transfertEnMinutes);
		dico.setObjectForKey(
				isExerciceDroitOption ? "true" : "false", ConstsPrint.XML_KEY_IS_EXERCICE_DROIT_OPTION);

		// -- EPARGNE OU RENONCEMENT : tableau d'alimentation --

		// solde avant demande
		int soldeCetAvantDemandeEnMinutes = 0;
		if (cetFactoryConsumer.isCetExistant()) {
			soldeCetAvantDemandeEnMinutes = cetFactoryConsumer.minutesRestantesInitialesRegimePerenne();
		}

		// tableau récapitulatif d'alimentation
		if (isRenoncement || isEpargne) {

			float soldeCetAvantDemandeEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(soldeCetAvantDemandeEnMinutes, getDureeJourMinutes());
			dico.setObjectForKey(
					new Float(soldeCetAvantDemandeEnJour7h00), ConstsPrint.XML_KEY_SOLDE_CET_AVANT_DEMANDE_EN_JOURS_7H00);

			// montant total de l'épargne demandée
			if (isEpargne) {
				dico.setObjectForKey(
						Integer.valueOf(FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemEpargne().intValue(), getDureeJourMinutes())),
						ConstsPrint.XML_KEY_TOTAL_EPARGNE_DEMANDEE_EN_JOURS_7H00);
			}

			// droits à congés année N-1
			int droitCongeInitialAnneePrecedenteMinute = 0;
			int consommationCongesAnneePrecedenteMinute = 0;
			EOPlanning eoPlanningPrecedent = eoPlanning.getEoPlanningPrecedent();
			if (eoPlanningPrecedent != null) {
				droitCongeInitialAnneePrecedenteMinute = eoPlanningPrecedent.getPlanning().getCalculDroitCongeDelegate().getDroitCongeMinutes(null);
				consommationCongesAnneePrecedenteMinute = droitCongeInitialAnneePrecedenteMinute - eoPlanningPrecedent.getPlanning().getCalculDroitCongeDelegate().getDroitCongeRestantMinutes(null);
			}
			float droitCongesAnneePrecedenteEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(droitCongeInitialAnneePrecedenteMinute, getDureeJourMinutes());
			String droitCongesAnneePrecedenteEnHeures = TimeCtrl.stringForMinutes(droitCongeInitialAnneePrecedenteMinute);
			dico.setObjectForKey(
					new Float(droitCongesAnneePrecedenteEnJour7h00), ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_JOURS_7H00);
			dico.setObjectForKey(
					droitCongesAnneePrecedenteEnHeures, ConstsPrint.XML_KEY_DROIT_A_CONGES_N_M_1_EN_HEURES);

			// consommation conges annee N-1
			float consommationCongesAnneePrecedenteEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(consommationCongesAnneePrecedenteMinute, getDureeJourMinutes());
			String consommationCongesAnneePrecedenteEnHeures = TimeCtrl.stringForMinutes(consommationCongesAnneePrecedenteMinute);
			dico.setObjectForKey(
					new Float(consommationCongesAnneePrecedenteEnJour7h00), ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_JOURS_7H00);
			dico.setObjectForKey(
					consommationCongesAnneePrecedenteEnHeures, ConstsPrint.XML_KEY_CONSOMMATION_CONGES_N_M_1_EN_HEURES);

			// reliquats
			int reliquatInitialMinute = eoPlanning.getPlanning().getCalculDroitCongeDelegate().getReliquatInitialMinutes(null);
			float reliquatInitialEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(reliquatInitialMinute, getDureeJourMinutes());
			String reliquatInitialEnHeure = TimeCtrl.stringForMinutes(reliquatInitialMinute);
			dico.setObjectForKey(
					new Float(reliquatInitialEnJour7h00), ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_JOURS_7H00);
			dico.setObjectForKey(
					reliquatInitialEnHeure, ConstsPrint.XML_KEY_RELIQUATS_N_M_1_EN_HEURES);

			// valeur du transfert en régime pérenne si renoncement
			if (isRenoncement) {
				Float transfertEnJour7h00 = new Float(FormatterCtrl.enJourArrondi2ChiffresApresVirgule(
						transaction.cetDemTransAncienPerenne().intValue(), getDureeJourMinutes()));
				String transfertEnHeures = TimeCtrl.stringForMinutes(transaction.cetDemTransAncienPerenne().intValue());
				dico.setObjectForKey(
						transfertEnJour7h00, ConstsPrint.XML_KEY_TRANSFERT_EN_JOURS_7H00);
				dico.setObjectForKey(
						transfertEnHeures, ConstsPrint.XML_KEY_TRANSFERT_EN_HEURES);
			}

			// valeur de l'epargne demandée
			if (isEpargne) {
				int epargneDemandeEnJour7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemEpargne().intValue(), getDureeJourMinutes());
				String epargneDemandeEnHeures = TimeCtrl.stringForMinutes(transaction.cetDemEpargne().intValue());
				dico.setObjectForKey(
						Integer.valueOf(epargneDemandeEnJour7h00), ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_JOURS_7H00);
				dico.setObjectForKey(
						epargneDemandeEnHeures, ConstsPrint.XML_KEY_DEMANDE_EPARGNE_EN_HEURES);
			}

			// soldes après versements
			int soldeCetApresDemandeEpargneEnMinutes = soldeCetAvantDemandeEnMinutes;
			if (isRenoncement) {
				soldeCetApresDemandeEpargneEnMinutes += transaction.cetDemTransAncienPerenne().intValue();
			}
			if (isEpargne) {
				soldeCetApresDemandeEpargneEnMinutes += transaction.cetDemEpargne().intValue();
			}

			float soldeCetApresDemandeEpargneEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(soldeCetApresDemandeEpargneEnMinutes, getDureeJourMinutes());
			String soldeCetApresDemandeEpargneEnHeures = TimeCtrl.stringForMinutes(soldeCetApresDemandeEpargneEnMinutes);
			dico.setObjectForKey(
					new Float(soldeCetApresDemandeEpargneEnJour7h00), ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_JOURS_7H00);
			dico.setObjectForKey(
					soldeCetApresDemandeEpargneEnHeures, ConstsPrint.XML_KEY_SOLDE_CET_APRES_DEMANDE_EN_HEURES);

		}

		// -- EXERCICE DU DROIT D'OPTION --

		// verifier si son solde final donnerait lieu a faire les options

		if (isExerciceDroitOption) {

			String strDateAnneeCivileN = DateCtrlHamac.dateToString(DateCtrlHamac.dateToDebutAnneeCivile(eoPlanning.toPeriode().perDFin()));

			dico.setObjectForKey(
					strDateAnneeCivileN, ConstsPrint.XML_KEY_DATE_ANNEE_CIVILE_N);

			int valeurEpargneSoumiseAOptionEnJourA7h00Arrondi = cetFactoryConsumer.depassementSeuil20JoursPourEpargneCetEnJourEntier(
					epargneEnMinutes + transfertEnMinutes);
			int demandeTransfertRafpEnJourA7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemRafpPerenne().intValue(), getDureeJourMinutes());
			int demandeIndemnisationEnJourA7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemIndemPerenne().intValue(), getDureeJourMinutes());
			int demandeMaintienCetEnJourA7h00 = FormatterCtrl.enJourArrondiEntierInferieur(transaction.cetDemMaintPerenne().intValue(), getDureeJourMinutes());

			dico.setObjectForKey(
					Integer.valueOf(valeurEpargneSoumiseAOptionEnJourA7h00Arrondi), ConstsPrint.XML_KEY_DEMANDE_TOTAL_OPTIONS_EN_JOURS_7H00);
			dico.setObjectForKey(
					Integer.valueOf(demandeTransfertRafpEnJourA7h00), ConstsPrint.XML_KEY_DEMANDE_TRANSFERT_RAFP_EN_JOURS_7H00);
			dico.setObjectForKey(
					Integer.valueOf(demandeIndemnisationEnJourA7h00), ConstsPrint.XML_KEY_DEMANDE_INDEMNISATION_EN_JOURS_7H00);
			dico.setObjectForKey(
					Integer.valueOf(demandeMaintienCetEnJourA7h00), ConstsPrint.XML_KEY_DEMANDE_MAINTIEN_CET_EN_JOURS_7H00);
			
		}

		// soldes après épargne et option
		int soldeCetApresDemandeEpargneEtOptionEnMinutes =
				soldeCetAvantDemandeEnMinutes
						+ transfertEnMinutes
						+ transaction.cetDemEpargne().intValue()
						- transaction.cetDemIndemPerenne().intValue()
						- transaction.cetDemRafpPerenne().intValue();

		float soldeCetApresDemandeEpargneEtOptionEnJour7h00 = FormatterCtrl.enJourArrondi2ChiffresApresVirgule(
				soldeCetApresDemandeEpargneEtOptionEnMinutes, getDureeJourMinutes());
		dico.setObjectForKey(
				new Float(soldeCetApresDemandeEpargneEtOptionEnJour7h00), ConstsPrint.XML_KEY_SOLDE_CET_APRES_EPARGNE_ET_DECISION_EN_JOURS_7H00);

		// -- bas de page

		// ville
		EditionFactory.feedDicoVilleEtablissement(getConfig(), dico);

		// date impression
		String strdateImpression = DateCtrl.dateToString(DateCtrl.now());
		// cas particulier pour la première année : vu le retard, on positionne
		// les demandes de 2009/2010 au 31/12/2009
		if (DateCtrlHamac.isSameDay(
				DateCtrlHamac.dateToDebutAnneeUniv(DateCtrlHamac.stringToDate("31/12/2009")), eoPlanning.toPeriode().perDDebut())) {
			strdateImpression = "31/12/2009";
		}

		dico.setObjectForKey(strdateImpression, ConstsPrint.XML_KEY_DATE_IMPRESSION);

		// le titre du document pour la demande sur régime pérenne
		StringBuffer sbTitle = new StringBuffer();
		if (isRenoncement) {
			if (!isEpargne && !isExerciceDroitOption) {
				sbTitle.append("transfert d'un CET crédité au 31/12/2008 sur le CET dispositif pérenne en vigueur à compter du 01/01/2009");
			} else {
				sbTitle.append("fermeture d'un CET crédité au 31/12/2008 et transfert des jours inscrits sur le CET ");
				sbTitle.append("dispositif pérenne en vigueur à compter du 01/01/2009");
			}
		}

		//
		boolean isOuverture = true;
		if (cetFactoryConsumer.isCetExistant()) {
			isOuverture = false;
		}

		if (isOuverture || isEpargne) {
			if (sbTitle.length() > 0) {
				sbTitle.append(" + ");
			}
			sbTitle.append("demande ");
			if (isOuverture) {
				sbTitle.append("d'ouverture ");
			}
			if (isOuverture && isEpargne) {
				sbTitle.append("et ");
			}
			if (isEpargne) {
				sbTitle.append("d'alimentation cet (congés acquis au titre de l'année universitaire ");
				sbTitle.append(EditionFactory.getStrAnneeUnivNm1(eoPlanning));
				sbTitle.append(")");
			}
		}

		/*
		 * if (isEpargne) { if (sbTitle.length() > 0) {
		 * sbTitle.append(" + demande "); } else {
		 * sbTitle.append("demande d'ouverture et "); } sbTitle.append(
		 * "d'alimentation cet (congés acquis au titre de l'année universitaire ");
		 * sbTitle.append(getStrAnneeUnivNm1()); sbTitle.append(")"); }
		 */

		if (isExerciceDroitOption) {
			if (sbTitle.length() > 0) {
				sbTitle.append(" + ");
			}
			sbTitle.append("exercice du droit d'option");
		}

		dico.setObjectForKey(sbTitle.toString().toUpperCase(), ConstsPrint.XML_KEY_TITRE);

		// la suite de la phrase 'Accord du président de l'université concernant
		// ...'
		StringBuffer sbSuffixAccordPresident = new StringBuffer();
		if (isRenoncement) {
			sbSuffixAccordPresident.append("le transfert des jours du CET crédité au 31/12/2008");
		}

		if (isEpargne) {
			if (sbSuffixAccordPresident.length() > 0) {
				if (isExerciceDroitOption) {
					sbSuffixAccordPresident.append(", ");
				} else {
					sbSuffixAccordPresident.append(" et ");
				}
			}
			sbSuffixAccordPresident.append("le versement de congés acquis au titre de l'année universitaire ");
			sbTitle.append(EditionFactory.getStrAnneeUnivNm1(eoPlanning));
		}

		if (isExerciceDroitOption) {
			if (sbSuffixAccordPresident.length() > 0) {
				sbSuffixAccordPresident.append(" et ");
			}
			sbSuffixAccordPresident.append("l'exercice du droit d'option");
		}

		dico.setObjectForKey(sbSuffixAccordPresident.toString(), ConstsPrint.XML_KEY_SUFFIXE_PHRASE_ACCORD_PRESIDENT);

		// la suite de la phrase 'Solde du CET après ...'
		StringBuffer sbSuffixSoldeCetFinal = new StringBuffer();
		if (isRenoncement || isEpargne) {
			sbSuffixSoldeCetFinal.append("versement");
		}

		if (isExerciceDroitOption) {
			if (sbSuffixSoldeCetFinal.length() > 0) {
				sbSuffixSoldeCetFinal.append(" et ");
			}
			sbSuffixSoldeCetFinal.append("exercice du droit d'option");
		}

		dico.setObjectForKey(sbSuffixSoldeCetFinal.toString(), ConstsPrint.XML_KEY_SUFFIXE_PHRASE_SOLDE_CET_FINAL);

		dico.setObjectForKey(TimeCtrl.stringForMinutes(getDureeJourMinutes()), ConstsPrint.XML_KEY_DUREE_JOURNEE_CET);

		return dico.immutableClone();
	}
}
