/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.xml;

import java.io.StringWriter;

import org.cocktail.fwkcktlhamac.serveur.I_ConstantesApplication;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HoraireJournalier;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class EditionHoraireAgentCtrl
		extends A_EditionCtrl {

	private final static String REPORT_FILENAME = "HoraireAgent.pdf";
	private final static String JASPER_FILENAME = "report/horaires/CongeshoraireAgent.jasper";
	private final static String RECORD_PATH = "/agents/agent";
	private final static String PRINT_LABEL = "Impression des horaires hebdomadaires";

	private NSArray<EOPlanning> eoPlanningArray;
	private EOStructure eoStructure;

	/**
	 * 
	 */
	public EditionHoraireAgentCtrl(
			CktlConfig config,
			NSArray<EOPlanning> eoPlanningArray,
			EOStructure eoStructure) {
		super(config);
		this.eoPlanningArray = eoPlanningArray;
		this.eoStructure = eoStructure;
	}

	@Override
	public final String getReportFilename() {
		return REPORT_FILENAME;
	}

	@Override
	public final String getJasperFilename() {
		return JASPER_FILENAME;
	}

	@Override
	public final String getRecordPath() {
		return RECORD_PATH;
	}

	@Override
	public final String getPrintLabel() {
		return PRINT_LABEL;
	}

	@Override
	public StringWriter creerXml() throws Exception {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		w.startDocument();
		w.writeComment(getPrintLabel());

		w.startElement("agents");
		{
			w.writeElement("annee-univ", "2012/2013");
			// TODO gérer tous les services
			if (eoStructure != null) {
				w.writeElement("service", eoStructure.lcStructure());
			}
			w.writeElement("date-impression", DateCtrlHamac.dateToString(DateCtrlHamac.now()));
			String mainLogoUrl = getConfig().stringForKey(I_ConstantesApplication.MAIN_LOGO_URL_PARAM_KEY);
			if (mainLogoUrl != null) {
				w.writeElement("mainLogoUrl", mainLogoUrl);
			}

			for (EOPlanning eoPlanning : eoPlanningArray) {

				Planning planning = null;

				// le planning rapide est suffisant
				if (eoPlanning.isPlanningCompletCharge()) {
					planning = eoPlanning.getPlanning();
				} else {
					planning = new Planning(eoPlanning);
					planning.chargementRapide();
				}

				// ne pas imprimer si pas d'horaires ...
				if (planning.getHoraireArray().count() == 0) {
					continue;
				}

				// au moins une semaine ?
				if (planning.getSemaineArray().count() == 0) {
					continue;
				}

				w.startElement("agent");
				{
					w.writeElement("nom", planning.getEoPlanning().toPersonne().getNomCompletAffichage());

					w.startElement("horaires");
					{
						for (Horaire horaire : planning.getHoraireArray()) {

							// associé à au moins une semaine ?
							NSArray<EOAssociationHoraire> eoAssoArray = horaire.getEoHoraire().tosAssociationHoraire(
									ERXQ.equals(
											EOAssociationHoraire.TO_PLANNING_KEY, planning.getEoPlanning()));
							if (eoAssoArray.count() == 0) {
								continue;
							}

							w.startElement("horaire");
							{

								w.writeElement("libelle", StringCtrl.replace(horaire.getNom(), "&", "et"));
								// pas de quotité
								// w.writeElement("quotite", null);
								// TODO gérer tous les services
								w.writeElement("total", TimeCtrl.stringForMinutes(horaire.getDureeComptabilisee()));
								w.writeElement("bonus", TimeCtrl.stringForMinutes(horaire.getBonusSamediMatin() + horaire.getBonusSamediApresMidi()));

								// tous les jours de l'horaire
								NSArray<HoraireJournalier> hjArray = horaire.getHoraireJournalierArray();

								for (HoraireJournalier hj : hjArray) {

									// ne pas mettre le dimanche
									if (hj.getIndexJour() == HoraireJournalier.INDEX_DIMANCHE) {
										continue;
									}

									w.startElement("jour");
									{
										w.writeElement("libelle", hj.getLibelleCourt());
										if (hj.getHorairePlageTravailAm() != null) {
											w.writeElement("debut-am", TimeCtrl.stringForMinutes(hj.getHorairePlageTravailAm().getDebutMinutesDansJour()));
											w.writeElement("fin-am", TimeCtrl.stringForMinutes(hj.getHorairePlageTravailAm().getFinMinutesDansJour()));
											String strService = hj.getHoraire().getEoHoraire().getEoStructure(hj.getHorairePlageTravailAm().getCStructure()).lcStructure();
											w.writeElement("service-am", strService);
										} else {
											w.writeElement("debut-am", " ");
											w.writeElement("fin-am", " ");
											w.writeElement("service-am", " ");
										}
										if (hj.getHorairePlageTravailPm() != null) {
											w.writeElement("debut-pm", TimeCtrl.stringForMinutes(hj.getHorairePlageTravailPm().getDebutMinutesDansJour()));
											w.writeElement("fin-pm", TimeCtrl.stringForMinutes(hj.getHorairePlageTravailPm().getFinMinutesDansJour()));
											String strService = hj.getHoraire().getEoHoraire().getEoStructure(hj.getHorairePlageTravailPm().getCStructure()).lcStructure();
											w.writeElement("service-pm", strService);
										} else {
											w.writeElement("debut-pm", " ");
											w.writeElement("fin-pm", " ");
											w.writeElement("service-pm", " ");
										}
										w.writeElement("total", TimeCtrl.stringForMinutes(hj.getDureeComptabilisee()));
										if (hj.getHorairePlagePause() != null) {
											w.writeElement("pause", TimeCtrl.stringForMinutes(hj.getHorairePlagePause().getDebutMinutesDansJour()));
										} else {
											w.writeElement("pause", " ");
										}
										w.writeElement("bonus", TimeCtrl.stringForMinutes(hj.getBonus()));

									}
									w.endElement(); // jour

								}

							}
							w.endElement(); // "horaire"

						}

						w.endElement(); // "horaires"
					}

					// le tableau des asso horaires-semaines
					w.startElement("semaines");
					for (Semaine semaine : planning.getSemaineArray()) {

						NSArray<Horaire> horaireArray = semaine.getHoraires();

						for (Horaire horaire : horaireArray) {

							// un ligne par semaine
							w.startElement("semaine");
							{
								w.writeElement("debut", DateCtrlHamac.dateToString(semaine.getPremierJour()));
								w.writeElement("fin", DateCtrlHamac.dateToString(semaine.getDernierJour()));
								w.writeElement("numero", Integer.toString(semaine.getNumeroSemaine()));
								w.writeElement("horaire", StringCtrl.replace(horaire.getNom(), "&", "et"));
							}
							w.endElement(); // "semaine"

						}

					}
					w.endElement(); // "semaines"

				}

				w.endElement(); // "agent"
			}
		}

		w.endElement(); // "agents"
		w.endDocument();
		w.close();

		System.out.println(sw.getBuffer().toString());

		return sw;

	}
}
