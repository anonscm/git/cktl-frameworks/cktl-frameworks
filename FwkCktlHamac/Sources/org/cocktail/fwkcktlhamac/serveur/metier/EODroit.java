/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.Acreditation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.EOStructureFixed;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EODroit extends _EODroit {

	public EODroit() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String TO_PERSONNE_TITULAIRE_KEY = "toPersonneTitulaire";
	public final static String TO_PERSONNE_CIBLE_KEY = "toPersonneCible";
	
	/**
	 * TODO controle de doublons
	 * 
	 * @param ec
	 * @param personneTitulaire
	 * @param personneCible
	 * @param eoTypeNiveauDroit
	 * @param isTitulaireCdc
	 * @param isCibleCdc
	 * @param isCibleHeritage
	 * @return
	 */
	public static EODroit create(
			EOEditingContext ec,
			IPersonne personneTitulaire,
			IPersonne personneCible,
			EOTypeNiveauDroit eoTypeNiveauDroit,
			boolean isTitulaireCdc,
			boolean isCibleCdc,
			boolean isCibleHeritage) {

		NSTimestamp now = DateCtrlHamac.now();

		EODroit eoDroit = createEODroit(
				ec, now, now, personneCible.persId(), personneTitulaire.persId(),
				NON, NON, NON, NON, eoTypeNiveauDroit);

		if (personneTitulaire.isStructure()) {
			if (isTitulaireCdc) {
				eoDroit.setTemTitulaireCdc(OUI);
			}
		}

		if (personneCible.isStructure()) {
			if (isCibleCdc) {
				eoDroit.setTemCibleCdc(OUI);
			}
			if (isCibleHeritage) {
				eoDroit.setTemCibleHeritage(OUI);
			}
		}

		return eoDroit;

	}

	/**
	 * @return
	 */
	public IPersonne toPersonneTitulaire() {
		IPersonne titulaire = null;

		if (tosIndividuTitulaire().count() > 0) {
			titulaire = tosIndividuTitulaire().objectAtIndex(0);
		} else if (tosStructureTitulaire().count() > 0) {
			titulaire = tosStructureTitulaire().objectAtIndex(0);
		}

		return titulaire;
	}

	/**
	 * @return
	 */
	public IPersonne toPersonneCible() {
		IPersonne cible = null;

		if (tosIndividuCible().count() > 0) {
			cible = tosIndividuCible().objectAtIndex(0);
		} else if (tosStructureCible().count() > 0) {
			cible = tosStructureCible().objectAtIndex(0);
		}

		return cible;
	}

	/**
	 * La liste des personnes ayant le niveau de droit sur un planning pour une
	 * structure
	 * 
	 * XXX traiter les plannings
	 * 
	 * XXX traiter l'héritage
	 * 
	 * @param eoOccupation
	 *          : facultatif
	 * @param eoStructure
	 * @param eoTypeNiveauDroit
	 * @return
	 */
	public static NSArray<EODroit> findDroitPourOccupation(
				EOOccupation eoOccupation, Planning planning) {

		// liste des sequences de validation
		NSArray<EOSequenceValidation> eoSequenceValidationArray =
				eoOccupation.toTypeOccupation().toTypeSequenceValidation().tosSequenceValidation();

		// liste des niveaux de droit associés
		NSArray<EOQualifier> qualArray = new NSArray<EOQualifier>();
		for (EOSequenceValidation eoSequenceValidation : eoSequenceValidationArray) {
			EOQualifier qualNiveauDroit = ERXQ.equals(TO_TYPE_NIVEAU_DROIT_KEY, eoSequenceValidation.toTypeNiveauDroit());
			qualArray = qualArray.arrayByAddingObject(qualNiveauDroit);
		}

		EOQualifier qual = new EOOrQualifier(qualArray);

		EOQualifier qualOccupation = ERXQ.equals(PERS_ID_CIBLE_KEY, eoOccupation.persId());

		qual = ERXQ.or(qual, qualOccupation);

		qualArray = new NSArray<EOQualifier>();
		for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {
			EOQualifier qualStructure = ERXQ.equals(PERS_ID_CIBLE_KEY, eoStructure.persId());
			qualArray = qualArray.arrayByAddingObject(qualStructure);
		}

		qual = ERXQ.or(qual, new EOOrQualifier(qualArray));

		EOEditingContext ec = eoOccupation.editingContext();

		NSArray<EODroit> array = fetchAll(ec, qual);

		return array;
	}

	/**
	 * Transforme une liste "complexe" de {@link EODroit} en liste "simple"
	 * {@link Acreditation}
	 * 
	 * @param eoDroitArray
	 * @return
	 */
	private static NSArray<Acreditation> getAcredidationArray(NSArray<EODroit> eoDroitArray) {
		NSMutableArray<Acreditation> array = new NSMutableArray<Acreditation>();

		for (EODroit eoDroit : eoDroitArray) {
			EOTypeNiveauDroit eoTypeNiveauDroit = eoDroit.toTypeNiveauDroit();
			NSArray<EOIndividu> eoIndividuTitulaireArray = eoDroit.getEoIndividuTitulaireDeduitArray();
			IPersonne personneCible = eoDroit.toPersonneCible();
			EOStructure eoStructurePlanning = eoDroit.toStructurePlanning();
			EOPeriode eoPeriode = eoDroit.toPeriode();

			array.addObjectsFromArray(
					toAcredidationArray(
							eoIndividuTitulaireArray, eoTypeNiveauDroit, personneCible, eoStructurePlanning, eoPeriode, eoDroit.isCibleHeritage()));
		}

		return array.immutableClone();
	}

	/**
	 * @param ec
	 * @param persIdCible
	 * @param eoStructureAFaireValiderArray
	 * @return
	 */
	public static NSArray<Acreditation> getAcreditationArrayPourCible(
			EOEditingContext ec, Integer persIdCible, NSArray<EOStructure> eoStructureAFaireValiderArray) {
		NSMutableArray<Acreditation> array = new NSMutableArray<Acreditation>();

		// pour éviter l'envoi de mail massif (sur un jour férié par exemple)
		if (eoStructureAFaireValiderArray.count() > 0) {
			array.addObjectsFromArray(getAcreditationArrayPourCibleSelonAnnuaire(ec, persIdCible, eoStructureAFaireValiderArray));
			array.addObjectsFromArray(getAcreditationArrayPourCibleSelonDroit(ec, persIdCible, eoStructureAFaireValiderArray));
		}

		return array.immutableClone();
	}

	/**
	 * La liste des acréditations accordées à un utilisateur
	 * 
	 * @param ec
	 * @param persIdTitulaire
	 * @return
	 */
	public static NSArray<Acreditation> getAcreditationArrayPourTitulaire(
			EOEditingContext ec, Integer persIdTitulaire) {
		
		NSArray<Acreditation> array = new NSMutableArray<Acreditation>();
		// tmp

		// d'apres les droits
		long tDebut = System.currentTimeMillis();
		NSArray<EODroit> eoDroitArray = fetchAll(ec);
		CktlLog.log("EODroit.fetchAll() : " + (System.currentTimeMillis() - tDebut) + "ms => " + eoDroitArray.count());

		tDebut = System.currentTimeMillis();
		array = getAcredidationArray(eoDroitArray);
		CktlLog.log("getAcredidationArray() : " + (System.currentTimeMillis() - tDebut) + "ms => " + array.count());

		// d'apres annuaire
		tDebut = System.currentTimeMillis();
		NSArray<EOStructure> eoStructureArrayAutorise = NSArrayCtrl.removeDuplicate(
				(NSArray<EOStructure>) EOServiceAutorise.fetchAll(ec).valueForKey(EOServiceAutorise.TO_STRUCTURE_KEY));
		CktlLog.log("EOServiceAutorise.fetchAll() : " + (System.currentTimeMillis() - tDebut) + "ms => " + eoStructureArrayAutorise.count());

		tDebut = System.currentTimeMillis();
		array = array.arrayByAddingObjectsFromArray(getAcreditationArrayPourCibleSelonAnnuaire(ec, eoStructureArrayAutorise));
		CktlLog.log("getAcreditationArrayPourCibleSelonAnnuaire() : " + (System.currentTimeMillis() - tDebut) + "ms => " + eoStructureArrayAutorise.count());

		EOQualifier qual = ERXQ.equals(
				Acreditation.EO_INDIVIDU_KEY + "." + EOIndividu.PERS_ID_KEY, persIdTitulaire);
		tDebut = System.currentTimeMillis();
		array = EOQualifier.filteredArrayWithQualifier(array, qual);
		CktlLog.log("filteredArrayWithQualifier() : " + (System.currentTimeMillis() - tDebut) + "ms => " + array.count());

		return array;
	}
	

	/**
	 * Transforme une liste de {@link EOIndividu} avec {@link EOTypeNiveauDroit}
	 * en liste "simple" {@link Acreditation}
	 * 
	 * @param eoIndividuArray
	 * @param eoTypeNiveauDroit
	 * @param cible
	 * @param eoStructurePlanning
	 * @param eoPeriode
	 * @param isHeritage
	 * @return
	 */
	private final static NSArray<Acreditation> toAcredidationArray(
			NSArray<EOIndividu> eoIndividuArray,
			EOTypeNiveauDroit eoTypeNiveauDroit,
			IPersonne cible,
			EOStructure eoStructurePlanning,
			EOPeriode eoPeriode,
			boolean isHeritage) {
		NSMutableArray<Acreditation> array = new NSMutableArray<Acreditation>();

		for (EOIndividu eoIndividu : eoIndividuArray) {
			Acreditation acreditation = new Acreditation(eoIndividu, eoTypeNiveauDroit, cible, eoStructurePlanning, eoPeriode, isHeritage);
			array.add(acreditation);
		}

		return array.immutableClone();
	}

	// XXX C_STRUCTURE en paramètre

	private static NSArray<Acreditation> getAcreditationArrayPourCibleSelonAnnuaire(
			EOEditingContext ec, NSArray<EOStructure> eoStructureAttenduArray) {
		return getAcreditationArrayPourCibleSelonAnnuaire(ec, null, eoStructureAttenduArray);
	}
	
	/**
	 * Liste des acréditations dépendantes de l'annuaire (chefs de service et RH
	 * global)
	 * 
	 * @param eoStructureAttenduArray
	 * @return
	 */
	private static NSArray<Acreditation> getAcreditationArrayPourCibleSelonAnnuaire(
			EOEditingContext ec, Integer persIdCible, NSArray<EOStructure> eoStructureAttenduArray) {
		NSMutableArray<Acreditation> array = new NSMutableArray<Acreditation>();

		for (EOStructure eoStructure : eoStructureAttenduArray) {

			// le niveau de droit "chef de service"
			EOTypeNiveauDroit eoTypeNiveauDroitCdc = EOTypeNiveauDroit.fetchByKeyValue(
					ec, EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_CHEF_DE_SERVICE);
			if (eoTypeNiveauDroitCdc != null) {
				if (eoStructure.toResponsable() != null && !eoStructure.toResponsable().persId().equals(persIdCible)) {
					array.addObjectsFromArray(
								toAcredidationArray(
										new NSArray<EOIndividu>(eoStructure.toResponsable()), eoTypeNiveauDroitCdc, eoStructure, eoStructure, null, false));
				} else {
					if (eoStructure.toStructurePere() != null && eoStructure.toStructurePere().toResponsable() != null) {
						array.addObjectsFromArray(
							toAcredidationArray(
									new NSArray<EOIndividu>(eoStructure.toStructurePere().toResponsable()), eoTypeNiveauDroitCdc, eoStructure, eoStructure, null, false));
					}
				}
			}

		}

		// RH Global
		EOTypeNiveauDroit eoTypeNiveauDroitFctGlobal = EOTypeNiveauDroit.fetchByKeyValue(
				ec, EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_FCT_GLOBAL);

		String cStructureRhGlobal = HamacCktlConfig.stringForKey(EOParametre.C_STRUCTURE_RH_GLOBAL);
		EOStructure eoStructureRhGlobal = EOStructureFixed.getEoStructureInContext(
						ec, cStructureRhGlobal);
		if (eoStructureRhGlobal != null) {
			NSArray<EOIndividu> eoIndividuRhGlobalArray = getEoIndividuMembre(eoStructureRhGlobal);
			array.addObjectsFromArray(
					toAcredidationArray(
							eoIndividuRhGlobalArray, eoTypeNiveauDroitFctGlobal, null, null, null, false));
		}

		// Admin Global
		EOTypeNiveauDroit eoTypeNiveauDroitAdmGlobal = EOTypeNiveauDroit.fetchByKeyValue(
					ec, EOTypeNiveauDroit.TND_CODE_KEY, EOTypeNiveauDroit.CODE_ADM);

		String cStructureAdminGlobal = HamacCktlConfig.stringForKey(EOParametre.C_STRUCTURE_ADMIN_GLOBAL);
		EOStructure eoStructureAdminGlobal =
				EOStructureFixed.getEoStructureInContext(ec, cStructureAdminGlobal);
		if (eoStructureAdminGlobal != null) {
			NSArray<EOIndividu> eoIndividuAdminGlobalArray = getEoIndividuMembre(eoStructureAdminGlobal);
			array.addObjectsFromArray(
						toAcredidationArray(
								eoIndividuAdminGlobalArray, eoTypeNiveauDroitAdmGlobal, null, null, null, false));
		}

		return array.immutableClone();
	}

	/**
	 * Liste des acréditations dépendantes de la gestion locales des droits
	 * 
	 * @param eoStructureAFaireValiderArray
	 * @return
	 */
	private static NSArray<Acreditation> getAcreditationArrayPourCibleSelonDroit(
			EOEditingContext ec, Integer persIdCible, NSArray<EOStructure> eoStructureAFaireValiderArray) {

		NSMutableArray<Acreditation> array = new NSMutableArray<Acreditation>();
		// la personne explicitement désignée
		EOQualifier qualPersonne = ERXQ.equals(PERS_ID_CIBLE_KEY, persIdCible);
		// NSArray<EODroit> result = fetchAll(ec, qualPersonne);
		// array.addObjectsFromArray(getAcredidationArray(result));

		// le(s) service(s) d'affectation(s)
		NSMutableArray<EOQualifier> qualArrayServiceAff = new NSMutableArray<EOQualifier>();
		for (EOStructure eoStructure : eoStructureAFaireValiderArray) {
			EOQualifier qualStructure = ERXQ.and(
						ERXQ.equals(TEM_CIBLE_CDC_KEY, NON),
						ERXQ.equals(PERS_ID_CIBLE_KEY, eoStructure.persId()));
			qualArrayServiceAff.addObject(qualStructure);
		}
		EOQualifier qualService = ERXQ.or(qualArrayServiceAff);
		// result = fetchAll(ec, qualService);
		// array.addObjectsFromArray(getAcredidationArray(result));

		// si chef de service
		NSMutableArray<EOQualifier> qualArrayCdc = new NSMutableArray<EOQualifier>();
		for (EOStructure eoStructure : eoStructureAFaireValiderArray) {
			if (eoStructure.toResponsable() != null &&
					eoStructure.toResponsable().persId().intValue() == persIdCible.intValue()) {
				EOQualifier qualCdc = ERXQ.and(
							ERXQ.equals(TEM_CIBLE_CDC_KEY, OUI),
							ERXQ.equals(PERS_ID_CIBLE_KEY, eoStructure.persId()));
				qualArrayCdc.addObject(qualCdc);
			}
		}
		EOQualifier qualCdc = null;
		if (qualArrayCdc.count() > 0) {
			qualCdc = ERXQ.or(qualArrayCdc);
			// result = fetchAll(ec, qualCdc);
			// array.addObjectsFromArray(getAcredidationArray(result));
		}

		// remonter aux sur-services pour avoir les droits hérités
		NSMutableArray<EOQualifier> qualArraySurService = new NSMutableArray<EOQualifier>();
		for (EOStructure eoStructure : eoStructureAFaireValiderArray) {
			EOStructure eoStructureCourante = eoStructure;
			while (eoStructureCourante != eoStructureCourante.toStructurePere()) {
				eoStructureCourante = eoStructureCourante.toStructurePere();
				EOQualifier qualSurService = ERXQ.and(
							ERXQ.equals(TEM_CIBLE_CDC_KEY, NON),
							ERXQ.equals(TEM_CIBLE_HERITAGE_KEY, OUI),
							ERXQ.equals(PERS_ID_CIBLE_KEY, eoStructureCourante.persId()));
				qualArraySurService.addObject(qualSurService);
			}
		}
		EOQualifier qualSurService = null;
		if (qualArraySurService.count() > 0) {
			qualSurService = ERXQ.or(qualArraySurService);
			// result = fetchAll(ec, qualSurService);
			// array.addObjectsFromArray(getAcredidationArray(result));
		}

		// remonter identique mais pour les chefs de service
		NSMutableArray<EOQualifier> qualArraySurServiceCdc = new NSMutableArray<EOQualifier>();
		for (EOStructure eoStructure : eoStructureAFaireValiderArray) {
			if (eoStructure.toResponsable() != null &&
					eoStructure.toResponsable().persId().intValue() == persIdCible.intValue()) {
				EOStructure eoStructureCourante = eoStructure;
				while (eoStructureCourante != eoStructureCourante.toStructurePere()) {
					eoStructureCourante = eoStructureCourante.toStructurePere();
					EOQualifier qualCdcSurService = ERXQ.and(
								ERXQ.equals(TEM_CIBLE_CDC_KEY, OUI),
								ERXQ.equals(TEM_CIBLE_HERITAGE_KEY, OUI),
								ERXQ.equals(PERS_ID_CIBLE_KEY, eoStructureCourante.persId()));
					qualArraySurServiceCdc.addObject(qualCdcSurService);
				}
			}
		}
		EOQualifier qualCdcSurService = null;
		if (qualArraySurServiceCdc.count() > 0) {
			qualCdcSurService = ERXQ.or(qualArraySurServiceCdc);
			// result = fetchAll(ec, qualCdcSurService);
			// array.addObjectsFromArray(getAcredidationArray(result));
		}

		// un méga qualifier
		EOQualifier megaQual = ERXQ.or(
				qualPersonne, qualService, qualCdc, qualSurService, qualCdcSurService);

		NSArray<EODroit> result = fetchAll(ec, megaQual);
		array.addObjectsFromArray(getAcredidationArray(result));

		return array.immutableClone();
	}

	/**
	 * Liste des {@link EOIndividu} titulaires déduis. Si c'est un groupe, alors
	 * on prend les membres via GRHUM.REPART_STRUCTURE ...
	 * 
	 * @return
	 */
	private NSArray<EOIndividu> getEoIndividuTitulaireDeduitArray() {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		if (toPersonneTitulaire().isIndividu()) {
			// individu
			array.addObject((EOIndividu) toPersonneTitulaire());
		} else {
			// groupe
			EOStructure eoStructure = (EOStructure) toPersonneTitulaire();

			// XXX temTitulaireHeritage à virer ???
			// XXX car prendre tous les membres de tous les sous-groupes ???? ça
			// risque de ramer beaucoup

			if (temTitulaireCdc().equals(OUI)) {
				EOIndividu cdc = eoStructure.toResponsable();
				if (cdc != null) {
					array.add(cdc);
				}
			} else {
				array.addObjectsFromArray(getEoIndividuMembre(eoStructure));
			}
		}

		return array.immutableClone();
	}

	/**
	 * Obtenir les membres d'un groupe jusqu'à arriver à des individus (méthode
	 * récursive). On ignore les structures membres d'elles memes
	 * 
	 * @param eoStructure
	 * @return
	 */
	private static NSArray<EOIndividu> getEoIndividuMembre(EOStructure eoStructure) {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		for (EORepartStructure repart : eoStructure.toRepartStructuresElts()) {
			IPersonne membre = repart.toPersonneElt();
			if (membre.isIndividu()) {
				array.addObject((EOIndividu) membre);
			} else if (membre.isStructure() && membre.persId().intValue() != eoStructure.persId().intValue()) {
				array.addObjectsFromArray(getEoIndividuMembre((EOStructure) membre));
			}
		}

		return array.immutableClone();
	}

	/**
	 * Liste des {@link EOIndividu} cible déduis. Si c'est un groupe, alors ce
	 * sont les membres via {@link EOPlanningSource}
	 * 
	 * @return
	 */
	public NSArray<EOIndividu> getEoIndividuCibleDeduitArray() {
		NSMutableArray<EOIndividu> array = new NSMutableArray<EOIndividu>();

		if (toPersonneCible().isIndividu()) {
			array.addObject((EOIndividu) toPersonneCible());
		} else {
			// groupe
			// TODO traiter les affectations de type structure (récursivité à mettre
			// en place) ???
			EOStructure eoStructure = (EOStructure) toPersonneCible();

			EOQualifier qual = ERXQ.equals(
					EOPlanningSource.C_STRUCTURE_KEY, eoStructure.cStructure());
			NSArray<EOPlanningSource> psArray = EOPlanningSource.fetchAll(
					editingContext(), qual);

			for (EOPlanningSource source : psArray) {
				IPersonne personne = source.toPersonne();
				if (personne.isIndividu()) {
					array.addObject((EOIndividu) personne);
				}
			}
		}

		return array.immutableClone();
	}

	/**
	 * Indique si la cible est un service. La cible doit être de type structure et
	 * le temoin cdc à non.
	 * 
	 * @return
	 */
	public boolean isCibleServiceSimple() {
		boolean isCibleServiceSimple = false;

		if (temCibleCdc().equals(NON) &&
				toPersonneCible().isStructure()) {
			isCibleServiceSimple = true;
		}

		return isCibleServiceSimple;
	}

	/**
	 * Indique si la cible est un chef de service. La cible doit être de type
	 * structure et le témoin cdc doit être a oui
	 * 
	 * @return
	 */
	public boolean isCibleChefDeService() {
		boolean isCibleChefDeService = false;

		if (temCibleCdc().equals(OUI) &&
				toPersonneCible().isStructure()) {
			isCibleChefDeService = true;
		}

		return isCibleChefDeService;
	}

	/**
	 * Indique si la cible est un service et ses sous services. La cible doit être
	 * de type structure et le témoin heritage doit être a oui
	 * 
	 * @return
	 */
	public boolean isCibleHeritage() {
		boolean isCibleHeritage = false;

		if (temCibleHeritage().equals(OUI) &&
				toPersonneCible().isStructure()) {
			isCibleHeritage = true;
		}

		return isCibleHeritage;
	}

	/**
	 * Indique si le titulaire est un service. Le titulaire doit être de type
	 * structure et le temoin cdc à non.
	 * 
	 * @return
	 */
	public boolean isTitulaireServiceSimple() {
		boolean isTitulaireServiceSimple = false;

		if (temTitulaireCdc().equals(NON) &&
				toPersonneTitulaire().isStructure()) {
			isTitulaireServiceSimple = true;
		}

		return isTitulaireServiceSimple;
	}

	/**
	 * Indique si le titulaire est un chef de service. Le titulaire doit être de
	 * type structure et le témoin cdc doit être a oui
	 * 
	 * @return
	 */
	public boolean isTitulaireChefDeService() {
		boolean isTitulaireChefDeService = false;

		if (temTitulaireCdc().equals(OUI) &&
				toPersonneTitulaire().isStructure()) {
			isTitulaireChefDeService = true;
		}

		return isTitulaireChefDeService;
	}

	/**
	 * Indique si le titulaire est un service et ses sous services. Le titulaire
	 * doit être de type structure et le témoin heritage doit être a oui
	 * 
	 * @return
	 */
	public boolean isTitulaireHeritage() {
		boolean isTitulaireHeritage = false;

		if (temTitulaireHeritage().equals(OUI) &&
				toPersonneTitulaire().isStructure()) {
			isTitulaireHeritage = true;
		}

		return isTitulaireHeritage;
	}
	
	/**
	 * 
	 * @param edc editingContext
	 * @param periode periode
	 */
	public static void supprimerParPeriode(EOEditingContext edc, EOPeriode periode) {
		
		NSArray<EODroit> droitArray = fetchAll(
				edc, ERXQ.equals(EODroit.TO_PERIODE_KEY, periode));

		for (EODroit eoDroit : droitArray) {
			edc.deleteObject(eoDroit);
		}
		
	}

	
	
}
