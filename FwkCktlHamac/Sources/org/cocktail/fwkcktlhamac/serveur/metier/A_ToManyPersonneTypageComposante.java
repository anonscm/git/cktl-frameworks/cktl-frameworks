/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;

/**
 * La personne affectée étant un groupe, voire une composante. Dans cette
 * hypothèse, on doit pouvoir la considérer comme une composante (i.e. appliquer
 * des privilèges hérités par ses sous services) ou simple groupe (sans
 * héritage)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_ToManyPersonneTypageComposante
		extends A_ToManyPersonne {

	/**
   * 
   */
	public abstract String temPersonneComposante();

	/**
	 * 
	 */
	public boolean isStructureEtAConsidererComposante() {
		boolean isStructureEtAConsidererComposante = false;

		if (toPersonne().isStructure() &&
				OUI.equals(temPersonneComposante()) &&
				((EOStructure) toPersonne()).isComposante()) {
			isStructureEtAConsidererComposante = true;
		}

		return isStructureEtAConsidererComposante;
	}

	/**
	 * 
	 */
	public boolean isStructureEtNEstPasAConsidererComposante() {
		boolean isStructureEtNEstPasAConsidererComposante = false;

		if (toPersonne().isStructure() &&
				NON.equals(temPersonneComposante())) {
			isStructureEtNEstPasAConsidererComposante = true;
		}

		return isStructureEtNEstPasAConsidererComposante;
	}

	/**
	 * TODO faire du cache
	 * 
	 * @return
	 */
	public boolean isEtablissement() {
		boolean isEtablissement = false;

		if (toPersonne() != null &&
				toPersonne().isStructure() &&
				OUI.equals(temPersonneComposante())) {

			EOStructure eoStructure = (EOStructure) toPersonne();

			if (eoStructure.cStructure().equals(eoStructure.cStructurePere()) ||
					eoStructure.cTypeStructure().equals(EOTypeStructure.TYPE_STRUCTURE_E) ||
					eoStructure.cTypeStructure().equals(EOTypeStructure.TYPE_STRUCTURE_ES)) {
				isEtablissement = true;
			}

		}

		return isEtablissement;
	}

	/**
	 * TODO faire du cache
	 * 
	 * @return
	 */
	public boolean isComposante() {
		boolean isComposante = false;

		if (toPersonne() != null &&
				toPersonne().isStructure() &&
				OUI.equals(temPersonneComposante()) &&
				!isEtablissement()) {
			isComposante = true;
		}

		return isComposante;
	}

	/**
	 * TODO faire du cache
	 * 
	 * @return
	 */
	public boolean isService() {
		boolean isService = false;

		if (toPersonne() != null &&
				toPersonne().isStructure() &&
				NON.equals(temPersonneComposante())) {
			isService = true;
		}

		return isService;
	}

}
