/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVisa.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOVisa extends A_ToManyPersonneViseur  {
	public static final String ENTITY_NAME = "Visa";
	public static final String ENTITY_TABLE_NAME = "HAMAC.VISA";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "visId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_HISTORISATION_KEY = "dHistorisation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_VISEUR_KEY = "persIdViseur";
	public static final String TEM_DEFINITIF_KEY = "temDefinitif";
	public static final String VIS_COMMENTAIRE_KEY = "visCommentaire";
	public static final String VIS_VISA_ACCEPTE_KEY = "visVisaAccepte";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String OCC_ID_KEY = "occId";
	public static final String PLA_ID_KEY = "plaId";
	public static final String VIS_ID_KEY = "visId";
	public static final String VTYP_ID_KEY = "vtypId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_HISTORISATION_COLKEY = "D_HISTORISATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_VISEUR_COLKEY = "PERS_ID_VISEUR";
	public static final String TEM_DEFINITIF_COLKEY = "TEM_DEFINITIF";
	public static final String VIS_COMMENTAIRE_COLKEY = "VIS_COMMENTAIRE";
	public static final String VIS_VISA_ACCEPTE_COLKEY = "VIS_VISA";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String OCC_ID_COLKEY = "OCC_ID";
	public static final String PLA_ID_COLKEY = "PLA_ID";
	public static final String VIS_ID_COLKEY = "VIS_ID";
	public static final String VTYP_ID_COLKEY = "VTYP_ID";


	// Relationships
	public static final String TO_OCCUPATION_KEY = "toOccupation";
	public static final String TO_PLANNING_KEY = "toPlanning";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";
	public static final String TO_STRUCTURE_AYANT_VISE_KEY = "toStructureAyantVise";
	public static final String TO_TYPE_VISA_KEY = "toTypeVisa";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dHistorisation() {
    return (NSTimestamp) storedValueForKey(D_HISTORISATION_KEY);
  }

  public void setDHistorisation(NSTimestamp value) {
    takeStoredValueForKey(value, D_HISTORISATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persIdViseur() {
    return (Integer) storedValueForKey(PERS_ID_VISEUR_KEY);
  }

  public void setPersIdViseur(Integer value) {
    takeStoredValueForKey(value, PERS_ID_VISEUR_KEY);
  }

  public String temDefinitif() {
    return (String) storedValueForKey(TEM_DEFINITIF_KEY);
  }

  public void setTemDefinitif(String value) {
    takeStoredValueForKey(value, TEM_DEFINITIF_KEY);
  }

  public String visCommentaire() {
    return (String) storedValueForKey(VIS_COMMENTAIRE_KEY);
  }

  public void setVisCommentaire(String value) {
    takeStoredValueForKey(value, VIS_COMMENTAIRE_KEY);
  }

  public String visVisaAccepte() {
    return (String) storedValueForKey(VIS_VISA_ACCEPTE_KEY);
  }

  public void setVisVisaAccepte(String value) {
    takeStoredValueForKey(value, VIS_VISA_ACCEPTE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation toOccupation() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation)storedValueForKey(TO_OCCUPATION_KEY);
  }

  public void setToOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation oldValue = toOccupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_OCCUPATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_OCCUPATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)storedValueForKey(TO_PLANNING_KEY);
  }

  public void setToPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning oldValue = toPlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLANNING_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureAyantVise() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_AYANT_VISE_KEY);
  }

  public void setToStructureAyantViseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureAyantVise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_AYANT_VISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_AYANT_VISE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa toTypeVisa() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa)storedValueForKey(TO_TYPE_VISA_KEY);
  }

  public void setToTypeVisaRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa oldValue = toTypeVisa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VISA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VISA_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOVisa avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVisa createEOVisa(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdViseur
, String temDefinitif
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureAyantVise, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeVisa toTypeVisa			) {
    EOVisa eo = (EOVisa) createAndInsertInstance(editingContext, _EOVisa.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdViseur(persIdViseur);
		eo.setTemDefinitif(temDefinitif);
    eo.setToStructureAyantViseRelationship(toStructureAyantVise);
    eo.setToTypeVisaRelationship(toTypeVisa);
    return eo;
  }

  
	  public EOVisa localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVisa)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVisa creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVisa creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVisa object = (EOVisa)createAndInsertInstance(editingContext, _EOVisa.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVisa localInstanceIn(EOEditingContext editingContext, EOVisa eo) {
    EOVisa localInstance = (eo == null) ? null : (EOVisa)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVisa#localInstanceIn a la place.
   */
	public static EOVisa localInstanceOf(EOEditingContext editingContext, EOVisa eo) {
		return EOVisa.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOVisa> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVisa fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVisa fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVisa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVisa)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVisa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVisa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVisa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVisa)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVisa fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVisa eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVisa ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVisa fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
