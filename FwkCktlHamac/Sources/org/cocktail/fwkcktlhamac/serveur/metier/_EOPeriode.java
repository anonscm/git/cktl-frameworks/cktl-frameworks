/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPeriode.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOPeriode extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "Periode";
	public static final String ENTITY_TABLE_NAME = "HAMAC.PERIODE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "perId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PER_D_DEBUT_KEY = "perDDebut";
	public static final String PER_D_FIN_KEY = "perDFin";
	public static final String PER_LIBELLE_KEY = "perLibelle";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String PER_ID_KEY = "perId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PER_D_DEBUT_COLKEY = "PER_D_DEBUT";
	public static final String PER_D_FIN_COLKEY = "PER_D_FIN";
	public static final String PER_LIBELLE_COLKEY = "PER_LIBELLE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String PER_ID_COLKEY = "PER_ID";


	// Relationships
	public static final String TOS_PLANNING_KEY = "tosPlanning";
	public static final String TOS_SERVICE_AUTORISE_KEY = "tosServiceAutorise";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp perDDebut() {
    return (NSTimestamp) storedValueForKey(PER_D_DEBUT_KEY);
  }

  public void setPerDDebut(NSTimestamp value) {
    takeStoredValueForKey(value, PER_D_DEBUT_KEY);
  }

  public NSTimestamp perDFin() {
    return (NSTimestamp) storedValueForKey(PER_D_FIN_KEY);
  }

  public void setPerDFin(NSTimestamp value) {
    takeStoredValueForKey(value, PER_D_FIN_KEY);
  }

  public String perLibelle() {
    return (String) storedValueForKey(PER_LIBELLE_KEY);
  }

  public void setPerLibelle(String value) {
    takeStoredValueForKey(value, PER_LIBELLE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning> tosPlanning() {
    return (NSArray)storedValueForKey(TOS_PLANNING_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning> tosPlanning(EOQualifier qualifier) {
    return tosPlanning(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning> tosPlanning(EOQualifier qualifier, boolean fetch) {
    return tosPlanning(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning> tosPlanning(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning.TO_PERIODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosPlanning();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_PLANNING_KEY);
  }

  public void removeFromTosPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_PLANNING_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning createTosPlanningRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Planning");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_PLANNING_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning) eo;
  }

  public void deleteTosPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_PLANNING_KEY);
  }

  public void deleteAllTosPlanningRelationships() {
    Enumeration objects = tosPlanning().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosPlanningRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise> tosServiceAutorise() {
    return (NSArray)storedValueForKey(TOS_SERVICE_AUTORISE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise> tosServiceAutorise(EOQualifier qualifier) {
    return tosServiceAutorise(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise> tosServiceAutorise(EOQualifier qualifier, boolean fetch) {
    return tosServiceAutorise(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise> tosServiceAutorise(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise.TO_PERIODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosServiceAutorise();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosServiceAutoriseRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_SERVICE_AUTORISE_KEY);
  }

  public void removeFromTosServiceAutoriseRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SERVICE_AUTORISE_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise createTosServiceAutoriseRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ServiceAutorise");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_SERVICE_AUTORISE_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise) eo;
  }

  public void deleteTosServiceAutoriseRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SERVICE_AUTORISE_KEY);
  }

  public void deleteAllTosServiceAutoriseRelationships() {
    Enumeration objects = tosServiceAutorise().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosServiceAutoriseRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOServiceAutorise)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPeriode avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPeriode createEOPeriode(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp perDDebut
, NSTimestamp perDFin
, String perLibelle
			) {
    EOPeriode eo = (EOPeriode) createAndInsertInstance(editingContext, _EOPeriode.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPerDDebut(perDDebut);
		eo.setPerDFin(perDFin);
		eo.setPerLibelle(perLibelle);
    return eo;
  }

  
	  public EOPeriode localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPeriode)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPeriode creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPeriode creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPeriode object = (EOPeriode)createAndInsertInstance(editingContext, _EOPeriode.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPeriode localInstanceIn(EOEditingContext editingContext, EOPeriode eo) {
    EOPeriode localInstance = (eo == null) ? null : (EOPeriode)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPeriode#localInstanceIn a la place.
   */
	public static EOPeriode localInstanceOf(EOEditingContext editingContext, EOPeriode eo) {
		return EOPeriode.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOPeriode> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPeriode fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPeriode fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPeriode)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPeriode fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPeriode eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPeriode)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPeriode fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPeriode eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPeriode ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPeriode fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
