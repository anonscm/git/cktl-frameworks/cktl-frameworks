/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.SoldeDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactory;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.I_CetCredit;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlhamac.serveur.util.TimeCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOSolde
		extends _EOSolde
		implements I_Operation, I_Solde, I_CetCredit, I_GestionAutoDCreationDModification {

	public EOSolde() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		// interdire un solde de type natif d'avoir quelque chose dans les
		// dates de validité (c'est la période qui le détermine automatiquement)
		// ou des dates butoir pour les reliquats
		if (isTypeSoldeNatif()) {
			if (super.dDebValidite() != null || super.dFinValidite() != null) {
				throw new NSValidation.ValidationException(
							"Les dates de validité doivent être vides pour un solde natif");
			}
		}

		// si l'une des date est renseignée, l'autre doit l'être aussi
		if ((super.dDebValidite() != null && super.dFinValidite() == null) ||
				(super.dDebValidite() == null && super.dFinValidite() != null)) {
			throw new NSValidation.ValidationException(
					"Si l'une des date est renseignée, l'autre doit l'être aussi.");
		}

		// test cohérence date début / fin
		if (super.dDebValidite() != null &&
				super.dFinValidite() != null &&
				DateCtrlHamac.isAfter(super.dDebValidite(), super.dFinValidite())) {
			throw new NSValidation.ValidationException(
					"Le début de validité doit être avant la fin ...");
		}

		// service obligatoire ?
		if (toTypeSolde().isServiceObligatoire() &&
				StringCtrl.isEmpty(cStructure())) {
			throw new NSValidation.ValidationException(
					"Le type de solde '" + toTypeSolde().stypLibelle() + "' doit nécéssairement être affecté à un service");
		}

		// service interdit ?
		if (toTypeSolde().isServiceInterdit() &&
				!StringCtrl.isEmpty(cStructure())) {
			throw new NSValidation.ValidationException(
					"Le type de solde '" + toTypeSolde().stypLibelle() + "' doit ne peut pas être affecté à un service");
		}

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier._EOSolde#dDebValidite()
	 */
	@Override
	public NSTimestamp dDebValidite() {
		NSTimestamp dDebValidite = null;

		if (isTypeSoldeNatif()) {
			dDebValidite = toPlanning().toPeriode().perDDebut();
		} else {
			dDebValidite = super.dDebValidite();
		}

		return dDebValidite;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier._EOSolde#dFinValidite()
	 */
	@Override
	public NSTimestamp dFinValidite() {
		NSTimestamp dFinValidite = null;

		if (isTypeSoldeNatif()) {
			if (isReliquat()) {
				//
				dFinValidite = HamacCktlConfig.dateForKey(
						EOParametre.DATE_FIN_VALIDITE_RELIQUAT,
						toPlanning().toPeriode(),
						DateCtrlHamac.dateToFinAnneeCivile(toPlanning().toPeriode().perDDebut()));
			} else {
				dFinValidite = toPlanning().toPeriode().perDFin();
			}
		} else {
			dFinValidite = super.dFinValidite();
		}

		return dFinValidite;
	}

	private Boolean _isTypeSoldeNatif;

	/**
	 * Indique si le solde doit être géré nativement par l'application
	 * 
	 * @return
	 */
	public boolean isTypeSoldeNatif() {
		if (_isTypeSoldeNatif == null) {
			if (EOTypeSolde.SOLDE_NATIF_PAR_SERVICE_ARRAY.contains(toTypeSolde().stypCode()) ||
					EOTypeSolde.SOLDE_NATIF_SANS_SERVICE_ARRAY.contains(toTypeSolde().stypCode())) {
				_isTypeSoldeNatif = Boolean.TRUE;
			} else {
				_isTypeSoldeNatif = Boolean.FALSE;
			}
		}
		return _isTypeSoldeNatif.booleanValue();
	}

	/**
	 * 
	 */
	public String libelle() {
		String toString = "";

		// XXX tmp on affiche la nature
		// toString += "[" + toPlanning().nature() + "] ";

		if (isIndemCet20081136()) {

			toString += "Indemnisation décret 2008-1136 de " +
					TimeCtrl.stringForMinutes(-init().intValue()) + " sur " + toPlanning().toPeriode().perLibelle();

		} else {

			boolean isAfficherValidite = true;
			boolean isAfficherInitial = true;
			boolean isAfficherRestant = true;
			boolean isAfficherStructure = true;

			if (isRegularisation()) {

				toString += "Régularisation de solde";

				if (!StringCtrl.isEmpty(solMotif())) {
					toString += " (" + solMotif() + ")";
				}
				isAfficherInitial = false;
				isAfficherRestant = false;

			} else if (isJrti()) {

				toString += "Rachat de jours de congé (JRTI)";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isCongeNatif()) {

				toString += "Droit à congé " + toPlanning().toPeriode().perLibelle();
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;

			} else if (isReliquatNatif() || isReliquat()) {

				toString += "Reliquat de congés";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;

			} else if (isCongeLegal()) {

				toString += "Congés légaux issus de ManGUE";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isCongeRh()) {

				toString += "Congés RH";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isDechargeSyndicale()) {

				toString += "Décharge syndicale";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isHeureSupplementaireNonValide()) {

				toString += "Heures supp. par encore validées";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isBalance()) {

				toString += "Balance heures supp. / congés compensateurs";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;

			} else if (isReliquatHsup()) {
				
				toString += "Reliquat de la balance heures supp. / congés compensateurs";
				isAfficherValidite = false;
				isAfficherInitial = false;
				isAfficherRestant = false;
				isAfficherStructure = false;
				
			} else {

				toString += "Solde " + toTypeSolde().stypCode();

			}

			if (isAfficherValidite) {
				toString += " deb:" + DateCtrlHamac.dateToString(dDebValidite()) +
						" fin:" + DateCtrlHamac.dateToString(dFinValidite());
			}

			if (isAfficherInitial &&
					init() != null) {
				toString += " init:" + TimeCtrl.stringForMinutes(init().intValue());
			}

			if (isAfficherRestant &&
					restant() != null) {
				toString += " restant:" + TimeCtrl.stringForMinutes(restant().intValue());
			}

			if (isAfficherStructure &&
					toStructure() != null) {
				toString += " service:" + toStructure().lcStructure();
			}

		}

		return toString;
	}

	/**
	 * Raccourci pour creer des soldes plus simplement
	 */
	public static EOSolde createSolde(
				EOEditingContext ec,
				NSTimestamp dDebValidite,
				NSTimestamp dFinValidite,
				String stypCode,
				EOStructure eoStructure,
				EOPlanning eoPlanning,
				Integer init) {
		EOSolde newSolde = null;

		EOTypeSolde eoSoldeType = EOTypeSolde.getTypeSoldeByCode(ec, stypCode);

		NSTimestamp now = DateCtrlHamac.now();

		newSolde = createEOSolde(
					ec, now, now, eoPlanning, eoSoldeType);
		newSolde.setInit(init);
		newSolde.setDDebValidite(dDebValidite);
		newSolde.setDFinValidite(dFinValidite);
		newSolde.setToStructureRelationship(eoStructure);

		return newSolde;
	}

	private Integer _restantPrecedent;

	/**
	 * XXX Valeur du solde restant avant la réinitialisation. Cette valeur est
	 * utilisée pour la gestion de la poursuite des soldes. On a donc toujours un
	 * chargement de retard. A virer quand la solution pour avoir le solde en
	 * temps réel sera trouvée
	 * 
	 * @return
	 */
	public Integer getRestantPrecedent() {
		return _restantPrecedent;
	}

	/**
	 * XXX est-ce toujours utilse ????
	 * 
	 */
	public void memoriserSoldePrecedent() {
		_restantPrecedent = restant();
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReliquat() {
		boolean isReliquat = false;

		if (toTypeSolde().isReliquatNatif() ||
				toTypeSolde().isReliquatManuel()) {
			isReliquat = true;
		}

		return isReliquat;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReliquatNatif() {
		boolean isReliquatNatif = false;

		if (toTypeSolde().isReliquatNatif()) {
			isReliquatNatif = true;
		}

		return isReliquatNatif;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReliquatManuel() {
		boolean isReliquatManuel = false;

		if (toTypeSolde().isReliquatManuel()) {
			isReliquatManuel = true;
		}

		return isReliquatManuel;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReliquatHsup() {
		boolean isReliquatHsup = false;

		if (toTypeSolde().isReliquatHsup()) {
			isReliquatHsup = true;
		}

		return isReliquatHsup;
	}

	
	/**
	 * 
	 * @return
	 */
	public boolean isCongeNatif() {
		boolean isCongeNatif = false;

		if (toTypeSolde().isCongeNatif()) {
			isCongeNatif = true;
		}

		return isCongeNatif;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isBalance() {
		boolean isBalance = false;

		if (toTypeSolde().isBalance()) {
			isBalance = true;
		}

		return isBalance;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isRegularisation() {
		boolean isRegularisation = false;

		if (toTypeSolde().isRegularisation()) {
			isRegularisation = true;
		}

		return isRegularisation;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCongeManuel() {
		boolean isCongeManuel = false;

		if (toTypeSolde().isCongeManuel()) {
			isCongeManuel = true;
		}

		return isCongeManuel;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCongeLegal() {
		boolean isCongeLegal = false;

		if (toTypeSolde().isCongeLegal()) {
			isCongeLegal = true;
		}

		return isCongeLegal;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCongeRh() {
		boolean isCongeRh = false;

		if (toTypeSolde().isCongeRh()) {
			isCongeRh = true;
		}

		return isCongeRh;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDechargeSyndicale() {
		boolean isDechargeSyndicale = false;

		if (toTypeSolde().isDechargeSyndicale()) {
			isDechargeSyndicale = true;
		}

		return isDechargeSyndicale;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isJrti() {
		boolean isJrti = false;

		if (toTypeSolde().isJrti()) {
			isJrti = true;
		}

		return isJrti;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isIndemCet20081136() {
		boolean isIndemCet20081136 = false;

		if (toTypeSolde().isIndemCet20081136()) {
			isIndemCet20081136 = true;
		}

		return isIndemCet20081136;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isExcedentHsup() {
		boolean isExcedentHsup = false;

		if (toTypeSolde().isBalance() ||
				toTypeSolde().isHeureSupplementaireManuel()) {
			isExcedentHsup = true;
		}

		return isExcedentHsup;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isRecuperation()
	 */
	public boolean isRecuperation() {
		// les heures sont des objets non EOSolde
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#
	 * isHeureSupplementaireManuel()
	 */
	public boolean isHeureSupplementaireManuel() {
		boolean isHeureSupplementaireManuel = false;

		if (toTypeSolde().isHeureSupplementaireManuel()) {
			isHeureSupplementaireManuel = true;
		}

		return isHeureSupplementaireManuel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#
	 * isHeureSupplementaireNonValide()
	 */
	public boolean isHeureSupplementaireNonValide() {
		boolean isHeureSupplementaireNonValide = false;

		if (toTypeSolde().isHeureSupplementaireNonValide()) {
			isHeureSupplementaireNonValide = true;
		}

		return isHeureSupplementaireNonValide;
	}

	// debits

	private EOSolde eoSoldePrecedent;

	public EOSolde eoSoldePrecedent() {
		return eoSoldePrecedent;
	}

	public void setEoSoldePrecedent(EOSolde eoSoldePrecedent) {
		this.eoSoldePrecedent = eoSoldePrecedent;
	}

	private SoldeDelegate _soldeDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#getSoldeDelegate()
	 */
	public SoldeDelegate getSoldeDelegate() {
		if (_soldeDelegate == null) {
			_soldeDelegate = new SoldeDelegate(this);
		}
		return _soldeDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#clearSoldeDelegate
	 * ()
	 */
	public void clearCache() {
		_soldeDelegate = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#dDeb()
	 */
	public NSTimestamp dDeb() {
		return dDebValidite();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#dFin()
	 */
	public NSTimestamp dFin() {
		return dFinValidite();
	}

	private OperationDelegate _operationDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (_operationDelegate == null) {
			_operationDelegate = new OperationDelegate(this, this);
		}
		return _operationDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXGenericRecord#toString()
	 */
	@Override
	public String toString() {
		return libelle() /*
											 * + (restant() != null ? " (restant : " +
											 * TimeCtrl.stringForMinutes(restant().intValue()) : "")
											 */;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#cStructure()
	 */
	public String cStructure() {
		String cStructure = null;

		if (toStructure() != null) {
			cStructure = toStructure().cStructure();
		}

		return cStructure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde#isCet()
	 */
	public boolean isCet() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Copier à l'identique le solde vers un autre planning
	 * 
	 * @param eoPlanningDst
	 * @return
	 */
	public EOSolde dupliquerVers(EOPlanning eoPlanningDst, boolean isRecopierRestant) {
		EOSolde newEoSolde = null;

		// erreur si même planning
		if (toPlanning() == eoPlanningDst) {
			throw new IllegalStateException("Impossible de dupliquer le solde d'un planning vers lui même");
		}

		newEoSolde = EOSolde.createSolde(
				editingContext(), super.dDebValidite(), super.dFinValidite(),
				toTypeSolde().stypCode(), toStructure(),
				eoPlanningDst, init());

		if (isRecopierRestant &&
				restant() != null) {
			newEoSolde.setRestant(restant());
		}
		newEoSolde.setSolMotif(solMotif());

		return newEoSolde;
	}

	public Integer credit() {
		return init();
	}

	public NSTimestamp dateValeur() {
		return null;
	}

	public Integer debit() {
		return null;
	}
	
	public boolean isAncienRegime() {
		boolean isAncienRegime = false;

		if (DateCtrlHamac.isBeforeEq(dateValeur(), CetFactory.getDateCetDebutRegimePerenne())) {
			isAncienRegime = true;
		}

		return isAncienRegime;
	}

	// poursuite de solde

	public final static String IS_CANDIDAT_POURSUITE_KEY = "isCandidatPoursuite";
	public final static String IS_EN_POURSUITE_KEY = "isEnPoursuite";
	public final static String IS_POURSUITE_COMPLETE_KEY = "isPoursuiteComplete";
	public final static String DATE_DISPARITION_KEY = "dateDisparition";

	/**
	 * Test du type et du restant pour poursuite
	 * 
	 * @return
	 */
	private boolean isPreRequisPoursuiteOk() {
		boolean isPreRequisPoursuiteOk = true;

		// de type reliquat ou balance ou droit à congé
		if (!isReliquat() && !isBalance() && !isCongeNatif()) {
			isPreRequisPoursuiteOk = false;
		}

		// // si restant vide, on laisse tomber
		// if (isPreRequisPoursuiteOk) {
		// if (restant() == null ||
		// restant().intValue() == 0) {
		// isPreRequisPoursuiteOk = false;
		// }
		// }

		// restant != 0
		if (isPreRequisPoursuiteOk) {

			if (restant() == null ||
						restant().intValue() <= 0) {
				isPreRequisPoursuiteOk = false;
			}

		}

		return isPreRequisPoursuiteOk;
	}

	/**
	 * Indique si le solde est candidat à une poursuite :
	 * 
	 * - de type reliquat ou balance
	 * 
	 * - restant > 0
	 * 
	 * - pas de poursuite réalisée
	 * 
	 * - planning suivant existant
	 * 
	 * TODO algo pas tip top ...
	 * 
	 * @return
	 */
	public final boolean isCandidatPoursuite() {
		boolean isCandidat = true;

		// premier filtre
		isCandidat = isPreRequisPoursuiteOk();

		// pas de poursuite réalisée
		if (isCandidat) {
			if (tosSoldeDst().count() > 0) {
				isCandidat = false;
			}
		}

		// changement de c_structure entre celui du solde et celui du travail
		// attendu
		if (isCandidat) {

			if (toStructure() != null) {

				NSArray<TravailAttendu> attenduArray = toPlanning().getTravailAttenduArray();

				if (attenduArray.count() > 0) {

					boolean isAttenduSurPeriode = false;

					int i = 0;

					while (!isAttenduSurPeriode && i < attenduArray.count()) {

						TravailAttendu at = attenduArray.objectAtIndex(i);

						// XXX voir pour un date de référence flottante
						NSTimestamp dateRef = DateCtrlHamac.now();

						if (at.getCStructure().equals(toStructure().cStructure())) {
							if (at.getDateFin() == null || (
									at.getDateFin() != null &&
									DateCtrlHamac.isAfter(at.getDateFin(), dateRef))) {
								isAttenduSurPeriode = true;
							}
						}

						i++;
					}

					// si 1 seul travail attendu au final, pas de poursuite possible
					if (isAttenduSurPeriode || attenduArray.count() == 1) {
						isCandidat = false;
					}

				}

			}

		}

		return isCandidat;
	}

	private NSTimestamp dateDisparition;

	// attention code en double avec #isCandidatPoursuite()

	public final NSTimestamp dateDisparition() {
		if (dateDisparition == null) {

			if (toStructure() != null) {

				NSArray<TravailAttendu> attenduArray = toPlanning().getTravailAttenduArray();

				if (attenduArray.count() > 0) {

					boolean isAttenduSurPeriode = false;

					int i = 0;

					while (!isAttenduSurPeriode && i < attenduArray.count()) {

						TravailAttendu at = attenduArray.objectAtIndex(i);

						// XXX voir pour un date de référence flottante
						NSTimestamp dateRef = DateCtrlHamac.now();

						if (at.getCStructure().equals(toStructure().cStructure())) {
							if (at.getDateFin() == null || (
									at.getDateFin() != null &&
									DateCtrlHamac.isAfter(at.getDateFin(), dateRef))) {
								isAttenduSurPeriode = true;
							} else {
								dateDisparition = at.getDateFin();
							}
						}

						i++;
					}

				}

			}

		}

		return dateDisparition;
	}

	/**
	 * La liste des {@link EOSolde} potentiels pour la poursuite
	 * 
	 * @return
	 */
	public final NSArray<EOSolde> getEoSoldeArrayCandidatPourPoursuite() {
		NSMutableArray<EOSolde> array = new NSMutableArray<EOSolde>();

		if (isCandidatPoursuite()) {

			NSMutableArray<EOQualifier> qualArray = new NSMutableArray<EOQualifier>();

			// doit être du même type
			qualArray.addObject(
					ERXQ.equals(TO_TYPE_SOLDE_KEY, toTypeSolde()));

			qualArray.addObject(
					ERXQ.greaterThanOrEqualTo(
							TO_PLANNING_KEY + "." + EOPlanning.TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY,
							toPlanning().toPeriode().perDDebut()));

			EOQualifier qualSolde = ERXQ.and(qualArray);

			// TODO
			NSArray<EOPlanning> eoPlanningArray = EOPlanning.findSortedEoPlanningArray(
					editingContext(), toPlanning().persId(), toPlanning().nature(),
					toPlanning().temPersonneComposante(), null, null);

			for (EOPlanning eoPlanning : eoPlanningArray) {
				array.addObjectsFromArray(
						eoPlanning.tosSolde(qualSolde));
			}

			// ôter lui même de la liste
			array.removeIdenticalObject(this);

		}

		return array.immutableClone();
	}

	/**
	 * 
	 * @param eoSoldeDst
	 * @param taux
	 * @return
	 */
	public final EOSoldePoursuite doEffectuerPoursuite(EOSolde eoSoldeDst, int pourcentage) {
		EOSoldePoursuite eoSoldePoursuite = null;

		int valeurPonderee = (int) (((float) restant()) * (float) ((float) pourcentage / (float) 100));

		eoSoldePoursuite = EOSoldePoursuite.createEOSoldePoursuite(
				editingContext(), DateCtrlHamac.now(), DateCtrlHamac.now(),
				pourcentage, valeurPonderee, eoSoldeDst,
				this);

		return eoSoldePoursuite;
	}

	/**
	 * Indique si le solde est actuellement poursuivi
	 * 
	 * @return
	 */
	public final boolean isEnPoursuite() {
		boolean isEnPoursuite = false;

		if (tosSoldeDst().count() > 0) {
			isEnPoursuite = true;
		}

		return isEnPoursuite;
	}

	/**
	 * Indique si la somme totale des poursuite couvre bien 100%
	 * 
	 * @return
	 */
	public final boolean isPoursuiteComplete() {
		boolean isEnPoursuiteComplete = false;

		int total = 0;

		for (EOSoldePoursuite eoSoldePoursuite : tosSoldePoursuiteAsSrc()) {
			total += eoSoldePoursuite.spoPourcentage();
		}

		if (total == 100) {
			isEnPoursuiteComplete = true;
		}

		return isEnPoursuiteComplete;
	}

	/**
	 * @return
	 */
	public NSArray<EOSolde> tosSoldeDst() {
		NSArray<EOSolde> array = new NSArray<EOSolde>();

		for (EOSoldePoursuite eoSoldePoursuite : tosSoldePoursuiteAsSrc()) {
			array = array.arrayByAddingObject(eoSoldePoursuite.toSoldeDst());
		}

		return array;
	}

}
