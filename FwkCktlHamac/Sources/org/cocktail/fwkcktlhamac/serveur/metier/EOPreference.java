/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.ListePersonne;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPreference
		extends _EOPreference
		implements I_GestionAutoDCreationDModification {

	public EOPreference() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	private final static String PRF_KEY_IS_MAIL_RECIPISSE = "isMailRecipisse";
	private final static String PRF_KEY_IS_MAIL_RECIPISSE_DELEGATION = "isMailRecipisseDelegation";
	private final static String PRF_KEY_NON_RECEPTION_MAIL_ANNUAIRE = "nonReceptionMailAnnuaire";
	private final static String PRF_KEY_IS_EXPORTER_MOTIF_SERVEUR_PLANNING = "isExporterMotifServeurPlanning";
	private final static String PRF_KEY_DUREE_JOURNEE_AFFICHAGE = "dureeJourneeAffichage";
	private final static String PRF_KEY_LISTE_PERSONNE = "listePersonne";
	private final static String PRF_KEY_MULTI_PLANNING_RADIO = "multiPlanningRadio";
	private final static String PRF_SEP = ";";
	private final static String PRF_SUB_SEP = ",";
	private final static String PRF_EQ = "=";

	public final static String MULTI_PLANNING_RADIO_TYPE_SERVICE = "S";
	public final static String MULTI_PLANNING_RADIO_TYPE_LISTE_PERSONNEL = "P";
	public final static String MULTI_PLANNING_RADIO_TYPE_TOUT = "T";

	public final static String SEP_LISTE = "|";
	public final static String SEP_TITRE_PERSONNE = ":";
	public final static String SEP_PERSONNE = "&";
	
	/**
	 * Mets a jour les preferences application de l'utilisateur
	 * <code>userInfo</code>.
	 * 
	 * @param userInfo
	 * @param isMailRecipisse
	 * @param isMailRecipisseDelegation
	 * @param cStructureNonReceptionArray
	 *          TODO
	 * @param isExporterMotifServeurPlanning
	 *          TODO
	 * @param dureeJourneeAffichage
	 *          TODO
	 * @param multiPlanningRadio
	 *          TODO
	 * @param transId
	 */
	public final static void updatePrefAppli(
			HamacApplicationUser userInfo,
			Boolean isMailRecipisse,
			Boolean isMailRecipisseDelegation,
			Map<String, List<Integer>> cStructureNonReceptionPersIdArray,
			Boolean isExporterMotifServeurPlanning,
			Integer dureeJourneeAffichage,
			String strListePersonne,
			String multiPlanningRadio) {

		EOPreference rec = (EOPreference) EOPreference.fetchByKeyValue(
				userInfo.getEditingContext(), PERS_ID_KEY, userInfo.getPersId());
		// on cree l'enregistrement s'il n'existe pas
		if (rec == null) {
			rec = createEOPreference(userInfo.getEditingContext(), DateCtrlHamac.now(), DateCtrlHamac.now(), userInfo.getPersId(), "");
		}
		// la chaine de preference apres modification
		StringBuilder prfPreference = new StringBuilder();

		// recipisse
		if (isMailRecipisse != null) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_IS_MAIL_RECIPISSE + PRF_EQ + Boolean.toString(isMailRecipisse.booleanValue()));
		} else {
			// recuperer la valeur de la base
			String strIsMailRecipisse = getPrfPreference(rec.preChaine(), PRF_KEY_IS_MAIL_RECIPISSE);
			if (strIsMailRecipisse != null) {
				prfPreference.append(PRF_KEY_IS_MAIL_RECIPISSE + PRF_EQ + strIsMailRecipisse);
			}
		}
		prfPreference.append(PRF_SEP);

		// recipisse delegation
		if (isMailRecipisseDelegation != null) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_IS_MAIL_RECIPISSE_DELEGATION + PRF_EQ + Boolean.toString(isMailRecipisseDelegation.booleanValue()));
		} else {
			// recuperer la valeur de la base
			String strIsMailRecipisseDelegation = getPrfPreference(rec.preChaine(), PRF_KEY_IS_MAIL_RECIPISSE_DELEGATION);
			if (strIsMailRecipisseDelegation != null) {
				prfPreference.append(PRF_KEY_IS_MAIL_RECIPISSE_DELEGATION + PRF_EQ + strIsMailRecipisseDelegation);
			}
		}
		prfPreference.append(PRF_SEP);

		// non reception mail
		if (cStructureNonReceptionPersIdArray != null &&
				cStructureNonReceptionPersIdArray.size() > 0) {
			prfPreference.append(PRF_KEY_NON_RECEPTION_MAIL_ANNUAIRE + PRF_EQ);
			int nombreStructure = cStructureNonReceptionPersIdArray.size();
			int indexStructureEnCours = 0;
			for (String cStructure : cStructureNonReceptionPersIdArray.keySet()) {
				indexStructureEnCours++;
				List<Integer> listePersId = cStructureNonReceptionPersIdArray.get(cStructure);
				prfPreference.append(cStructure);
				if (CollectionUtils.isNotEmpty(listePersId)) {
					prfPreference.append(SEP_TITRE_PERSONNE);
					int taillePersId = listePersId.size();
					int indexEnCours = 0;
					for (Integer persId : listePersId) {
						indexEnCours++;
						prfPreference.append(persId);						
						if (indexEnCours < taillePersId) {
							prfPreference.append(SEP_PERSONNE);
						}						
					}
				}				
				if (indexStructureEnCours < nombreStructure) {
					prfPreference.append(PRF_SUB_SEP);
				}				
			}
		} 
		prfPreference.append(PRF_SEP);

		// export des motifs via serveur de planning
		if (isExporterMotifServeurPlanning != null) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_IS_EXPORTER_MOTIF_SERVEUR_PLANNING + PRF_EQ + Boolean.toString(isExporterMotifServeurPlanning.booleanValue()));
		} else {
			// recuperer la valeur de la base
			String strIsExporterMotifServeurPlanning = getPrfPreference(rec.preChaine(), PRF_KEY_IS_EXPORTER_MOTIF_SERVEUR_PLANNING);
			if (strIsExporterMotifServeurPlanning != null) {
				prfPreference.append(PRF_KEY_IS_EXPORTER_MOTIF_SERVEUR_PLANNING + PRF_EQ + strIsExporterMotifServeurPlanning);
			}
		}
		prfPreference.append(PRF_SEP);

		// durée d'une journée pour affichage
		if (dureeJourneeAffichage != null) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_DUREE_JOURNEE_AFFICHAGE + PRF_EQ + Integer.toString(dureeJourneeAffichage.intValue()));
		} else {
			// recuperer la valeur de la base
			String strDureeJourneeAffichage = getPrfPreference(rec.preChaine(), PRF_KEY_DUREE_JOURNEE_AFFICHAGE);
			if (strDureeJourneeAffichage != null) {
				prfPreference.append(PRF_KEY_DUREE_JOURNEE_AFFICHAGE + PRF_EQ + strDureeJourneeAffichage);
			}
		}
		prfPreference.append(PRF_SEP);

		// listes de personne
		if (strListePersonne != null) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_LISTE_PERSONNE + PRF_EQ + strListePersonne);
		} else {
			// recuperer la valeur de la base
			String strListePersonneDb = getPrfPreference(rec.preChaine(), PRF_KEY_LISTE_PERSONNE);
			if (strListePersonneDb != null) {
				prfPreference.append(PRF_KEY_LISTE_PERSONNE + PRF_EQ + strListePersonneDb);
			}
		}
		prfPreference.append(PRF_SEP);

		// radio multi planning selection
		if (!StringCtrl.isEmpty(multiPlanningRadio)) {
			// construire pour stockage
			prfPreference.append(PRF_KEY_MULTI_PLANNING_RADIO + PRF_EQ + multiPlanningRadio);
		} else {
			// recuperer la valeur de la base
			String multiPlanningRadioDb = getPrfPreference(rec.preChaine(), PRF_KEY_MULTI_PLANNING_RADIO);
			if (multiPlanningRadioDb != null) {
				prfPreference.append(PRF_KEY_MULTI_PLANNING_RADIO + PRF_EQ + multiPlanningRadioDb);
			}
		}

		prfPreference.append(PRF_SEP);

		rec.setPreChaine(prfPreference.toString());
		// mise a jour des donnees dans la session
		loadPreferences(userInfo);
	}

	/**
	 * Charger les preferences application de l'utilisateur <code>persId</code>
	 * dans la structure de donnee utilisateur <code>userInfo</code>.
	 * 
	 * @param userInfo
	 */
	public static void loadPreferences(HamacApplicationUser userInfo) {

		EOPreference rec = (EOPreference) EOPreference.fetchByKeyValue(
				userInfo.getEditingContext(), PERS_ID_KEY, userInfo.getPersId());

		// ré-instancier le formatteur minutes <-> jours
		userInfo.resetHamacMinuteToJourFormatter();

		if (rec != null) {

			String strIsMailRecipisse = getPrfPreference(rec.preChaine(), PRF_KEY_IS_MAIL_RECIPISSE);
			if (strIsMailRecipisse != null) {
				boolean isMailRecipisse = valueOf(strIsMailRecipisse);
				userInfo.setIsMailRecipisse(isMailRecipisse);
			}

			String strIsMailRecipisseDelegation = getPrfPreference(rec.preChaine(), PRF_KEY_IS_MAIL_RECIPISSE_DELEGATION);
			if (strIsMailRecipisseDelegation != null) {
				boolean isMailRecipisseDelegation = valueOf(strIsMailRecipisseDelegation);
				userInfo.setIsMailRecipisseDelegation(isMailRecipisseDelegation);
			}

			userInfo.resetAllNonReceptionMailPourStructure();
			String strNonReceptionMailAnnuaire = getPrfPreference(rec.preChaine(), PRF_KEY_NON_RECEPTION_MAIL_ANNUAIRE);
			if (strNonReceptionMailAnnuaire != null) {
				
				List<String> cStructureArray = getListeStructure(strNonReceptionMailAnnuaire);
				for (String cStructure : cStructureArray) {
					List<Integer> listePersId = getListePersId(strNonReceptionMailAnnuaire, cStructure);
					userInfo.setIsNonReceptionMailPourStructure(true, cStructure, listePersId);
				}
			}

			String strIsExporterMotifServeurPlanning = getPrfPreference(rec.preChaine(), PRF_KEY_IS_EXPORTER_MOTIF_SERVEUR_PLANNING);
			if (strIsExporterMotifServeurPlanning != null) {
				boolean isExporterMotifServeurPlanning = valueOf(strIsExporterMotifServeurPlanning);
				userInfo.setIsExporterMotifServeurPlanning(isExporterMotifServeurPlanning);
			}

			String strDureeJourneeAffichage = getPrfPreference(rec.preChaine(), PRF_KEY_DUREE_JOURNEE_AFFICHAGE);
			if (strDureeJourneeAffichage != null) {
				Integer dureeJourneeAffichage = Integer.valueOf(Integer.parseInt(strDureeJourneeAffichage));
				userInfo.setDureeJourneeAffichage(dureeJourneeAffichage);
			} else {
				// par défaut, la durée en paramètre global
				userInfo.setDureeJourneeAffichage(Integer.valueOf(HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR_AFFICHAGE)));
			}

			String strListePersonne = getPrfPreference(rec.preChaine(), PRF_KEY_LISTE_PERSONNE);
			if (strListePersonne != null) {
				NSMutableArray<ListePersonne> array = ListePersonne.getListePersonneArrayFromString(userInfo.getEditingContext(), strListePersonne);
				userInfo.setListePersonneArray(array);
			}

			String strMultiPlanningRadio = getPrfPreference(rec.preChaine(), PRF_KEY_MULTI_PLANNING_RADIO);
			if (strMultiPlanningRadio != null) {
				userInfo.setMultiPlanningRadio(strMultiPlanningRadio);
			}
		}

	}

	/**
	 * Pour java 1.4.2 compliance ...
	 * 
	 * @param value
	 * @return
	 */
	private static boolean valueOf(String value) {
		if (!StringCtrl.isEmpty(value) && value.toUpperCase().equals("TRUE")) {
			return true;
		}
		return false;
	}

	/**
	 * Obtenir la valeur String d'un element
	 * 
	 * @param strPrefList
	 * @param aKey
	 * @return
	 */
	private static String getPrfPreference(String strPrefList, String aKey) {
		if (!StringCtrl.isEmpty(strPrefList)) {
			NSArray<String> prefList = NSArray.componentsSeparatedByString(strPrefList, PRF_SEP);
			for (int i = 0; i < prefList.count(); i++) {
				String keyValueStr = prefList.objectAtIndex(i);
				if (!StringCtrl.isEmpty(keyValueStr)) {
					NSArray<String> keyValueList = NSArray.componentsSeparatedByString(keyValueStr, PRF_EQ);
					String key = keyValueList.objectAtIndex(0);
					if (key.equals(aKey)) {
						return (String) keyValueList.objectAtIndex(1);
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 3:28&15&29,47,49:13&58&9514
	 * @return
	 */
	private static List<Integer> getListePersId(String valueList, String cStructure) {
		String[] strStructures = StringUtils.split(valueList, PRF_SUB_SEP);
		List<Integer> result = new ArrayList<Integer>();
		
		for (String ligneStructure : strStructures) {
			String[] ligneParStructure = StringUtils.split(ligneStructure, SEP_TITRE_PERSONNE);
			if (ligneParStructure[0].equals(cStructure)) {
				if (ligneParStructure.length > 1) {
					String[] persIdArray = StringUtils.split(ligneParStructure[1], SEP_PERSONNE);
					for (String string : persIdArray) {
						result.add(Integer.valueOf(string));
					}
				}
			}
		}
		return result;
		
	}
	
	/**
	 * 3:28&15&29,47,49:13&58&9514
	 * @return
	 */
	private static List<String> getListeStructure(String valueList) {
		String[] strStructures = StringUtils.split(valueList, PRF_SUB_SEP);
		List<String> result = new ArrayList<String>();
		
		for (String ligneStructure : strStructures) {
			String[] ligneParStructure = StringUtils.split(ligneStructure, SEP_TITRE_PERSONNE);
			result.add(ligneParStructure[0]);
		}
		return result;
		
	}
	
	

}
