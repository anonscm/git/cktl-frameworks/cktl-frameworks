/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePreRequis;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.HorairePreRequisPourService;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOAssociationHoraire
		extends _EOAssociationHoraire
		implements I_GestionAutoDCreationDModification {

	public EOAssociationHoraire() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String DATE_DEBUT_SEMAINE_KEY = "dateDebutSemaine";
	public final static String DATE_FIN_SEMAINE_KEY = "dateFinSemaine";

	/**
	 * 
	 * Enregistrement(s) associé(s) au code d'une semaine
	 */
	public static NSArray<EOAssociationHoraire> filterAssociationForSemaineInArray(
				Semaine semaine, NSArray<EOAssociationHoraire> array) {

		EOQualifier qual = ERXQ.equals(
					SEMAINE_CODE_KEY, semaine.getCode());

		NSArray<EOAssociationHoraire> result = EOQualifier.filteredArrayWithQualifier(array, qual);

		return result;
	}

	/**
	 * Création d'une association horaire pour la meme semaine si cette derniere
	 * est inexistante
	 * 
	 * @param eoPlanningDst
	 *          TODO
	 * @param isMettreAJourSiExistant
	 *          TODO
	 * @param isSupprimerSiIdentique
	 *          TODO
	 * @return
	 */
	protected EOAssociationHoraire repercuter(
			EOPlanning eoPlanningDst,
			boolean isMettreAJourSiExistant,
			boolean isSupprimerSiIdentique) {
		EOAssociationHoraire eoAsso = null;

		NSArray<EOAssociationHoraire> array = eoPlanningDst.tosAssociationHoraire(
				ERXQ.equals(SEMAINE_CODE_KEY, semaineCode()));

		if (array.count() > 0) {
			eoAsso = array.objectAtIndex(0);
		}

		if (eoAsso == null) {
			// n'existe pas encore => création
			NSTimestamp now = DateCtrlHamac.now();
			eoAsso = createEOAssociationHoraire(
					editingContext(), now, now, semaineCode(), toHoraire(), eoPlanningDst);
		} else {
			// existe : faut-il faire quelque chose
			if (isMettreAJourSiExistant) {

				if (eoAsso.toHoraire() != toHoraire()) {
					// l'horaire a été modifié
					eoAsso.setToHoraireRelationship(toHoraire());
					eoAsso.setDModification(DateCtrlHamac.now());
				} else {
					// l'horaire est identique
					if (isSupprimerSiIdentique) {
						supprimerSiIdentique(eoPlanningDst);
					}
				}
			}

		}

		return eoAsso;
	}

	/**
	 * Supprimer une association horaire-semaine si celle ci est identique
	 * 
	 * @param eoPlanningDst
	 */
	private void supprimerSiIdentique(EOPlanning eoPlanningDst) {

		EOAssociationHoraire eoAsso = null;

		eoAsso = fetchByQualifier(
				editingContext(),
				ERXQ.and(
						ERXQ.equals(TO_PLANNING_KEY, eoPlanningDst),
						ERXQ.equals(SEMAINE_CODE_KEY, semaineCode())));

		if (eoAsso != null) {
			editingContext().deleteObject(eoAsso);
		}

	}

	/**
	 * Traitement des modifications des associations horaires semaines. Méthodes
	 * centralisatrice afin de mémoriser les changements dans le planning des
	 * historiques de modifications intervalidation.
	 * 
	 * Cette méthode controle que l'association est possible (quotité,
	 * plafonds...)
	 * 
	 * @param eoAssoExistant
	 *          a renseigner si mise à jour, <code>null</code> sinon
	 * @param eoHoraire
	 *          l'horaire
	 * @param eoPlanningDst
	 *          le planning d'affectation
	 * @param semaineCode
	 *          obligatoire si eoAssoExistant est <code>null</code>
	 * @return
	 */
	public static EOAssociationHoraire associer(
			EOAssociationHoraire eoAssoExistant, EOHoraire eoHoraire, EOPlanning eoPlanningDst, String semaineCode)
				throws NSValidation.ValidationException {

		// si le planning n'est pas passé en paramètre, on prend celui affecté à
		// l'association existante
		EOPlanning eoPlanning = null;

		if (eoPlanningDst != null) {
			eoPlanning = eoPlanningDst;
		} else {
			eoPlanning = eoAssoExistant.toPlanning();
		}

		// ne peut être appelée qu'à partir du planning réel ou du planning de test
		if (!eoPlanning.isReel() &&
				!eoPlanning.isTest()) {

			throw new NSValidation.ValidationException(
					"EOAssociationHoraire.associer() : appel direct interdit sur le type de planning : " + eoPlanning.nature());

		}

		// si le code n'est pas passé en paramètre, on prend celui affecté à
		// l'association existante
		String code = semaineCode;
		if (code == null) {
			code = eoAssoExistant.semaineCode();
		}

		checkHoraireRepondAuPrerequis(eoHoraire, eoPlanning, code);
		checkDureeHebdoMaximumNonDepassee(eoHoraire, eoPlanning, code);

		EOAssociationHoraire eoAsso = null;

		// enregistrement existant
		eoAsso = eoAssoExistant;

		// si pas existant, on la créée
		NSTimestamp now = DateCtrlHamac.now();
		if (eoAsso == null) {

			eoAsso = EOAssociationHoraire.createEOAssociationHoraire(
					eoHoraire.editingContext(), now, now, code, eoHoraire, eoPlanning);

		} else {

			// uniquement si changement
			if (eoAssoExistant.toHoraire() != eoHoraire) {
				//
				eoAsso.setToHoraireRelationship(eoHoraire);
				eoAsso.setDModification(now);

			}

		}

		// traitement des historiques, prévisionnels ...

		if (eoPlanning.isReel()) {

			// prévisionnel
			EOPlanning eoPlanningPrevisionnel = eoPlanning.creationSiNonExistant(EOPlanning.PREV);
			eoAsso.repercuter(eoPlanningPrevisionnel, false, false);

			// intervalidation
			EOPlanning eoPlanningIntervalidation = eoPlanning.creationSiNonExistant(EOPlanning.INTR);
			eoAsso.repercuter(eoPlanningIntervalidation, true, true);

		}

		return eoAsso;
	}

	/**
	 * Check externe pour vérifier que l'association locale est bien cohérente
	 */
	public void checkCoherence()
			throws NSValidation.ValidationException {
		checkHoraireRepondAuPrerequis(toHoraire(), toPlanning(), semaineCode());
		checkDureeHebdoMaximumNonDepassee(toHoraire(), toPlanning(), semaineCode());
	}

	/**
	 * Vérifier que l'horaire associé répond bien au pré-requis de la semaine
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public final static void checkHoraireRepondAuPrerequis(
			EOHoraire eoHoraire, EOPlanning eoPlanning, String semaineCode) throws NSValidation.ValidationException {

		Planning planning = eoPlanning.getPlanning();

		//
		HorairePreRequis hpr = HorairePreRequis.findHorairePreRequisForSemaineCodeInDico(
				semaineCode, planning.getDicoHorairePreRequis());

		// hpr est a null en cas de réduction de planning par exemple
		if (hpr != null) {

			// on cumule les messages d'erreur s'il y en a plusieurs
			String message = "";

			for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

				String cStructure = eoStructure.cStructure();

				HorairePreRequisPourService hprps = hpr.getParService(cStructure);

				// peut etre a null si l'agent ne travaille pas sur cette semaine
				if (hprps != null) {
					try {
						eoHoraire.getHoraire().checkRepondAuPreRequisPourService(hprps);
					} catch (ValidationException e) {
						message += "\nSemaine " + semaineCode + " : " + e.getMessage();
					}

				}

			}

			if (message.length() > 0) {
				throw new NSValidation.ValidationException(message);
			}

		}

	}

	/**
	 * Vérifier que l'horaire associé répond bien au pré-requis de la semaine.
	 * Fait la même chose que
	 * {@link #checkHoraireRepondAuPrerequis(EOHoraire, EOPlanning, String)} sauf
	 * qu'elle retourne un boolean
	 * 
	 * @return
	 */
	public final static boolean isHoraireRepondAuPrerequis(EOHoraire eoHoraire, EOPlanning eoPlanning, String semaineCode) {
		boolean isOk = true;

		try {
			checkHoraireRepondAuPrerequis(eoHoraire, eoPlanning, semaineCode);
		} catch (NSValidation.ValidationException e) {
			isOk = false;
		}

		return isOk;
	}

	/**
	 * Vérifier que le quota d'heures maximum n'est pas dépassé
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private static void checkDureeHebdoMaximumNonDepassee(
			EOHoraire eoHoraire, EOPlanning eoPlanning, String semaineCode) throws NSValidation.ValidationException {

		// TODO independamment du service

		// TODO service par service

	}
}
