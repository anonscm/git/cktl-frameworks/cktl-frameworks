/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.EOStructureFixed;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.ibm.icu.util.GregorianCalendar;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEditingContextDelegate;
import er.extensions.eof.ERXQ;

public class EOPeriode
		extends _EOPeriode
		implements I_GestionAutoDCreationDModification {

	public EOPeriode() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		//
		if (perDDebut() == null) {
			throw new NSValidation.ValidationException("La date de début est vide");
		}
		//
		if (perDFin() == null) {
			throw new NSValidation.ValidationException("La date de fin est vide");
		}
		//
		if (DateCtrlHamac.isBefore(perDFin(), perDDebut())) {
			throw new NSValidation.ValidationException("La date de fin précede la date de début");
		}
		// TODO controle de non chevauchement avec celles existantes

		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		// TODO ne pas autoriser de chevauchement avec d'autres périodes

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	/**
	 * @param ec
	 * @param dateDebut
	 * @param dateFin
	 * @param isAutoriserServiceIdentiquePeriodePrecedente
	 *          Les services autorisés sont par défaut les mêmes que l'année
	 *          précédente (si elle existe)
	 * @return
	 */
	public static EOPeriode createEOPeriode(
			EOEditingContext ec, NSTimestamp dateDebut, NSTimestamp dateFin, boolean isAutoriserServiceIdentiquePeriodePrecedente) {
		EOPeriode eoPeriode = null;

		NSTimestamp now = DateCtrlHamac.now();

		eoPeriode = createEOPeriode(ec, now, now, dateDebut, dateFin, null);
		eoPeriode.setTemValide(OUI);

		// constitution du nom de la période 
		if (dateDebut != null) {
			String perLibelle = DateCtrlHamac.anneeLibelleForPeriode(dateDebut, dateFin);
			eoPeriode.setPerLibelle(perLibelle);
		}

		// copier coller les services autorisés de la période précedénte ?
		if (isAutoriserServiceIdentiquePeriodePrecedente) {
			EOPeriode eoPeriodePrecedente = eoPeriode.getPeriodePrecedente();
			if (eoPeriodePrecedente != null) {
				for (EOServiceAutorise eoServiceAutorise : eoPeriodePrecedente.tosServiceAutorise()) {
					EOServiceAutorise.createEOServiceAutorise(
							ec, now, now, eoPeriode, eoServiceAutorise.toStructure());
				}
			}
		}

		return eoPeriode;
	}

	/**
	 * Déterminer la période précédente, chronologiquement
	 * 
	 * @return
	 */
	public EOPeriode getPeriodePrecedente() {
		EOPeriode eoPeriodePrecedente = null;

		NSArray<EOPeriode> eoPeriodeArray = fetchAll(
				editingContext(), ERXQ.lessThan(PER_D_FIN_KEY, perDDebut()), CktlSort.newSort(PER_D_DEBUT_KEY));

		if (eoPeriodeArray.count() > 0) {
			eoPeriodePrecedente = eoPeriodeArray.lastObject();
		}

		return eoPeriodePrecedente;
	}

	/**
	 * Déterminer la période suivante, chronologiquement
	 * 
	 * @return
	 */
	public EOPeriode getPeriodeSuivante() {
		
		EOPeriode eoPeriodeSuivante = fetchFirstByQualifier(editingContext(), ERXQ.greaterThan(PER_D_DEBUT_KEY, perDFin()), CktlSort.newSort(PER_D_DEBUT_KEY));
		
		return eoPeriodeSuivante;
	}
	
	/**
	 * Autoriser le service sur la période s'il ne l'est pas déjà
	 * 
	 * @param eoStructure
	 * @param isCreerPlanningAbsent
	 *          Faut il créer les objects {@link EOPlanning} attendus (si non
	 *          existants)
	 * @return la liste des {@link EOPlanning} nouvellement créés
	 */
	public NSArray<EOPlanning> autoriserService(EOStructure eoStructure, boolean isCreerPlanningAbsent) {
		NSArray<EOPlanning> array = new NSArray<EOPlanning>();

		if (getEOStructureAutoriseForPeriode().containsObject(eoStructure) == false) {
			EOServiceAutorise eoServiceAutorise = EOServiceAutorise.createEOServiceAutorise(
					editingContext(), DateCtrlHamac.now(), DateCtrlHamac.now(), this, eoStructure);

			if (isCreerPlanningAbsent) {

				array = creerEOPlanningAbsents(eoStructure);

			}

		}

		return array;
	}

	/**
	 * Methode de création des plannings absents pour une structure. En d'autres
	 * terme, cela permet de créer les {@link EOPlanning} attendus pour les
	 * {@link A_EOSource} de la structure.
	 * 
	 * @param eoStructure
	 * @param array
	 * @return
	 */
	public NSArray<EOPlanning> creerEOPlanningAbsents(EOStructure eoStructure) {

		NSArray<EOPlanning> array = new NSArray<EOPlanning>();

		NSArray<EOPlanning> eoPlanningExistantArray = EOPlanning.findSortedEoPlanningArray(
				editingContext(), null, EOPlanning.REEL, NON, eoStructure.cStructure(), this);
		NSArray<Integer> eoPersIdExistantArray = (NSArray<Integer>) eoPlanningExistantArray.valueForKey(EOPlanning.PERS_ID_KEY);

		// les sources de données locales
		NSArray<A_EOSource> eoPlanningSourceAttenduArray = A_EOSource.getSourceArrayFor(
				EOPlanningSource.class, editingContext(), this, new NSArray<EOStructure>(eoStructure), null, null);

		// statut invalide par défaut pour tous les nouveaux plannings
		EOTypeStatut eoTypeStatutInvalide = null;

		for (A_EOSource eoSource : eoPlanningSourceAttenduArray) {
			if (!eoPersIdExistantArray.containsObject(eoSource.persId())) {
				EOPlanning eoPlanning = EOPlanning.creationSiNonExistant(
						editingContext(), eoSource.toPersonne(), EOPlanning.REEL, NON, this);
				//
				if (eoTypeStatutInvalide == null) {
					eoTypeStatutInvalide = EOTypeStatut.getEoTypeStatutNonValide(editingContext());
				}
				eoPlanning.setToTypeStatutRelationship(eoTypeStatutInvalide);
				//
				array = array.arrayByAddingObject(eoPlanning);
			}
		}

		return array;
	}

	/**
	 * Desautoriser le service sur la période s'il ne l'est pas déjà
	 * 
	 * @param eoStructure
	 * @return
	 */
	public void desautoriserService(EOStructure eoStructure) {

		NSArray<EOServiceAutorise> array = tosServiceAutorise(
				ERXQ.equals(EOServiceAutorise.TO_STRUCTURE_KEY, eoStructure));

		if (array.count() > 0) {
			editingContext().deleteObject(array.lastObject());
		}

	}

	/**
	 * Donne la periode en cours
	 * 
	 * @param ec
	 */
	public final static EOPeriode getCurrentPeriode(EOEditingContext ec) {
		EOPeriode currentPeriode = null;

		NSTimestamp now = DateCtrlHamac.now();
		NSTimestamp nowMinuit = DateCtrlHamac.remonterToMinuit(now);

		currentPeriode = fetchByQualifier(ec,
					CktlDataBus.newCondition(
							PER_D_DEBUT_KEY + "<=%@ and " + PER_D_FIN_KEY + ">=%@",
							new NSArray<NSTimestamp>(new NSTimestamp[] {
									nowMinuit, nowMinuit })));

		return currentPeriode;
	}

	private Integer _perId;

	/**
	 * Clé primaire
	 * 
	 * @return
	 */
	public Integer perId() {
		if (_perId == null) {
			_perId = (Integer) EOUtilities.primaryKeyForObject(editingContext(), this).valueForKey(PER_ID_KEY);
		}
		return _perId;
	}

	private NSArray<NSTimestamp> _dateJourFerieList;

	/**
	 * Liste des dates des jours fériés de la période
	 * 
	 * @return
	 */
	public NSArray<NSTimestamp> getDateJourFerieArray() {
		if (_dateJourFerieList == null) {
			_dateJourFerieList = DateCtrlHamac.joursFeriesEntre2Dates(perDDebut(), perDFin());
		}
		return _dateJourFerieList;
	}

	/**
	 * La liste des périodes contenant l'intervalle de temps passé en paramètre.
	 * Mettre les dates à null pour lister toutes les périodes
	 * 
	 * @param dDeb
	 * @param dFin
	 * @return
	 */
	public static NSArray<EOPeriode> findSortedPeriodeArray(
			EOEditingContext ec, NSTimestamp dDeb, NSTimestamp dFin) {
		NSArray<EOPeriode> array = null;

		//
		EOQualifier qual = null;

		if (dDeb != null && dFin != null) {
			qual = CktlDataBus.newCondition(
					"not(" + PER_D_DEBUT_KEY + ">%@ or " + PER_D_FIN_KEY + "<%@) or " +
							"(" + PER_D_DEBUT_KEY + "=nil and " + PER_D_FIN_KEY + "=nil) or " +
							"(" + PER_D_DEBUT_KEY + "<=%@ and " + PER_D_FIN_KEY + "=nil) or " +
							"(" + PER_D_DEBUT_KEY + "=nil and " + PER_D_FIN_KEY + ">=%@)",
					new NSArray<NSTimestamp>(
							new NSTimestamp[] {
									dFin,
									dDeb,
									dFin,
									dDeb }));

		}

		CktlSort sort = CktlSort.newSort(PER_D_DEBUT_KEY);

		array = fetchAll(ec, qual, sort);

		return array;
	}

	public final static String VALEUR_PARAMETRE_ANNUALISE_KEY = "valeurParametreAnnualise";

	/**
	 * La valeur pour la gestion des paramètres annualisés. Par exemple 2011/2012
	 * pour 20112012
	 * 
	 * @return
	 */
	public String getValeurParametreAnnualise() {
		String str = "";

		GregorianCalendar gcDebut = new GregorianCalendar();
		gcDebut.setTime(perDDebut());

		GregorianCalendar gcFin = new GregorianCalendar();
		gcFin.setTime(perDFin());

		str = gcDebut.get(GregorianCalendar.YEAR) + "" + gcFin.get(GregorianCalendar.YEAR);

		return str;
	}

	//

	/**
	 * Liste des {@link EOStructure} autorisées sur une {@link EOPeriode}
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public NSArray<EOStructure> getEOStructureAutoriseForPeriode() {
		NSArray<EOStructure> array = null;

		array = (NSArray<EOStructure>) tosServiceAutorise().valueForKey(EOServiceAutorise.TO_STRUCTURE_KEY);

		// classement par LL_STRUCTURE
		array = CktlSort.sortedArray(array, EOStructure.LL_STRUCTURE_KEY);

		return array;
	}

	private NSArray<EOStructure> eoStructureServiceArray;

	/**
	 * Liste des services issus du framework personne
	 * 
	 * @return
	 */
	private NSArray<EOStructure> getEoStructureServiceArray() {
		if (eoStructureServiceArray == null) {
			eoStructureServiceArray = buildEoStructureServiceArray(editingContext());
		}
		return eoStructureServiceArray;
	}

	/**
	 * Liste des services issus du framework personne
	 * 
	 * @return
	 */
	private final static NSArray<EOStructure> buildEoStructureServiceArray(EOEditingContext ec) {
		NSArray<EOStructure> array = new NSArray<EOStructure>(EOStructure.rechercherServicesEtablissements(ec));
		// virer les doublons
		array = NSArrayCtrl.removeDuplicate(array);
		return array;
	}

	/**
	 * Liste des {@link EOStructure} non autorisées sur une {@link EOPeriode}. On
	 * se référe à la liste des services courants
	 * 
	 * @param eoPeriode
	 * @return
	 */
	public NSArray<EOStructure> getEOStructureNonAutoriseForPeriode() {
		NSMutableArray<EOStructure> array = null;

		array = new NSMutableArray<EOStructure>(getEoStructureServiceArray());
		array.removeObjectsInArray(getEOStructureAutoriseForPeriode());

		return array.immutableClone();
	}

	/**
	 * La liste des {@link EOPeriode} attendues par rapport à une liste de
	 * {@link A_EOSource}
	 * 
	 * @return
	 */
	public static NSArray<EOPeriode> getEoPeriodeArrayForSource(
			EOEditingContext ec, NSArray<A_EOSource> eoSourceArray) {
		NSArray<EOPeriode> array = new NSArray<EOPeriode>();

		for (A_EOSource eoSource : eoSourceArray) {
			array = array.arrayByAddingObjectsFromArray(
					findSortedPeriodeArray(ec, eoSource.dDebut(), eoSource.dFin()));
		}

		array = NSArrayCtrl.removeDuplicate(array);

		return array;
	}

	/**
	 * La liste des composantes des structure ayant été autorisées sur la période
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getEOStructureComposanteAutoriseeArray() {
		NSArray<EOStructure> array = new NSArray<EOStructure>();

		for (EOStructure eoStructureAutorisee : getEOStructureAutoriseForPeriode()) {

			EOStructure composante = EOStructureFixed.toComposante(eoStructureAutorisee);

			// ne pas mettre la racine en tant que composante
			// if (composante.isEtablissement() == false) {
			// if (composante.toStructurePere() != composante) {
			if (!array.containsObject(composante)) {
				array = array.arrayByAddingObject(composante);
			}
			// }
		}

		return array;
	}

	/**
	 * La liste des structure autorisées ayant pour composante celle passée en
	 * paramètre
	 * 
	 * @param eoComposante
	 * @param isDeep
	 *          faut-il chercher tous les sous-services (i.e. ne pas s'arreter au
	 *          premier niveau)
	 * @param shouldAddSelf
	 *          ajouter la composante elle même a la liste des résultats
	 * @return
	 */
	public final static NSArray<EOStructure> getEOStructureAutoriseeArrayPourComposante(
			EOPeriode eoPeriode, EOStructure eoComposante, boolean isDeep, boolean shouldAddSelf) {
		NSArray<EOStructure> array = new NSArray<EOStructure>();

		NSArray<EOStructure> eoStructureArray = null;
		if (eoPeriode != null) {
			// période définie : seules les structures autorisés
			eoStructureArray = eoPeriode.getEOStructureAutoriseForPeriode();
		} else {
			// période non définies : tous les services
			eoStructureArray = buildEoStructureServiceArray(eoComposante.editingContext());
		}

		for (EOStructure eoStructureAutorisee : eoStructureArray) {

			EOStructure eoComposantePourStructure = EOStructureFixed.toComposante(eoStructureAutorisee);

			if (isDeep) {
				// recherche en profondeur : il faut déterminer si eoComposante est
				// quelque part dans l'arbo de eoComposantePourStructure

				EOStructure eoStructureIntermediaire = eoStructureAutorisee;

				NSArray<EOStructure> eoStructureParcourruArray = new NSArray<EOStructure>();
				boolean isTrouve = false;
				boolean isArreter = false;

				while (!isTrouve && !isArreter) {
					if (eoStructureIntermediaire == eoComposante) {
						isTrouve = true;
					} else {
						eoStructureParcourruArray = eoStructureParcourruArray.arrayByAddingObject(eoStructureIntermediaire);
					}
					if (eoStructureIntermediaire == eoStructureIntermediaire.toStructurePere()) {
						isArreter = true;
					}
					eoStructureIntermediaire = eoStructureIntermediaire.toStructurePere();
				}

				if (isTrouve) {

					for (EOStructure eoStructure : eoStructureParcourruArray) {
						if (!array.containsObject(eoStructure)) {
							array = array.arrayByAddingObject(eoStructure);
						}
					}

				}

			} else {
				// composante directe

				if (eoComposantePourStructure == eoComposante) {
					if (!array.containsObject(eoStructureAutorisee)) {
						array = array.arrayByAddingObject(eoStructureAutorisee);
					}
				}

			}

		}

		// ajouter la composante elle même
		if (shouldAddSelf) {
			if (!array.containsObject(eoComposante)) {
				array = array.arrayByAddingObject(eoComposante);
			}
		}

		return array;
	}

	/**
	 * 
	 * @return
	 */
	public final static NSTimestamp getDateDebutPremierePeriode() {
		NSTimestamp date = null;

		EOEditingContext ec = new EOEditingContext();
		ec.setDelegate(new ERXEditingContextDelegate());

		NSArray<EOPeriode> periodeArray = findSortedPeriodeArray(ec, null, null);
		if (periodeArray.count() > 0) {
			date = periodeArray.objectAtIndex(0).perDDebut();
		}

		return date;
	}
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public final static List<EOPeriode> getTroisPeriodesGlissantes(EOEditingContext ec) {
		
		List<EOPeriode> listePeriode = new ArrayList<EOPeriode>();
		EOPeriode periodeCourante = EOPeriode.getCurrentPeriode(ec);
		EOPeriode periodePrecedente = periodeCourante.getPeriodePrecedente();
		EOPeriode periodeSuivante = periodeCourante.getPeriodeSuivante();
		
		listePeriode.add(periodeCourante);
		if (periodePrecedente != null) {
			listePeriode.add(periodePrecedente);
		}
		if (periodeSuivante != null) {
			listePeriode.add(periodeSuivante);
		}
		return listePeriode;
	}
	
	public final static EOPeriode getPeriode(EOEditingContext ec, NSTimestamp date) {
		
		EOPeriode periode = fetchByQualifier(ec,
					CktlDataBus.newCondition(
							PER_D_DEBUT_KEY + "<=%@ and " + PER_D_FIN_KEY + ">=%@",
							new NSArray<NSTimestamp>(new NSTimestamp[] {
									date, date })));
		
		return periode;
		
	}
	
	
}
