/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.LigneFicheRose;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.OperationDelegate;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Transaction;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOSoldePoursuite
		extends _EOSoldePoursuite
		implements I_Operation {

	public EOSoldePoursuite() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		// TODO s'assurer que la quotité totale pour un même solde source ne
		// dépassent pas 100

		// TODO interdire la poursuite d'un solde ayant une validité postérieure
		// au solde destinataire

		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	/**
	 * Liste des enregistrements de poursuite pour un {@link EOPlanning}
	 * 
	 * @param eoPlanning
	 * @return
	 */
	public final static NSArray<EOSoldePoursuite> getSoldePoursuiteArrayForPlanning(
			EOPlanning eoPlanning) {
		NSArray<EOSoldePoursuite> array = new NSArray<EOSoldePoursuite>();

		EOQualifier qual = ERXQ.or(
				ERXQ.equals(TO_SOLDE_SRC_KEY + "." + EOSolde.TO_PLANNING_KEY, eoPlanning),
				ERXQ.equals(TO_SOLDE_DST_KEY + "." + EOSolde.TO_PLANNING_KEY, eoPlanning));

		array = fetchAll(eoPlanning.editingContext(), qual);

		return array;
	}

	private OperationDelegate operationDelegate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation#
	 * getOperationDelegate()
	 */
	public OperationDelegate getOperationDelegate() {
		if (operationDelegate == null) {
			operationDelegate = new OperationDelegate(this, this);
		}
		return operationDelegate;
	}

	private NSTimestamp dateDebutPoursuite;

	@Override
	public String toString() {

		String str = "";

		str += "Poursuite de solde ";

		if (dateDebutPoursuite != null) {
			str += "au " + DateCtrlHamac.dateToString(dateDebutPoursuite) + " ";
		}

		str += toSoldeSrc().toStructure().lcStructure() + " vers " + toSoldeDst().toStructure().lcStructure() + " (" + spoPourcentage().intValue() + "%)";

		return str;

	}

	private final String motifPourFicheRose() {
		String str = "";

		str += "Poursuite de solde ";

		str += toSoldeSrc().toStructure().lcStructure() + " vers " + toSoldeDst().toStructure().lcStructure() + " (" + spoPourcentage().intValue() + "%)";

		return str;
	}

	/**
	 * Controle et effectue les opérations liés à la poursuite de solde pour le
	 * {@link Jour} et la liste poursuiteArray
	 * 
	 * @param planning
	 *          TODO
	 */
	public final static void traitementPoursuiteSoldePourJour(
			Planning planning, NSArray<EOSoldePoursuite> poursuiteArray, Jour jour) {

		if (poursuiteArray.count() > 0) {

			EOQualifier qual = ERXQ.or(
					ERXQ.equals(I_Operation.D_DEB, jour.getDate()));

			NSArray<EOSoldePoursuite> array = EOQualifier.filteredArrayWithQualifier(poursuiteArray, qual);

			for (EOSoldePoursuite eoSoldePoursuite : array) {

				EOSolde eoSoldeSrc = eoSoldePoursuite.toSoldeSrc();
				EOSolde eoSoldeDst = eoSoldePoursuite.toSoldeDst();
				int valeurPonderee = (int) (((float) eoSoldeSrc.restant()) * (float) (eoSoldePoursuite.spoPourcentage().floatValue() / (float) 100));

				// crédit sur solde destinataire
				new Transaction(eoSoldeDst, eoSoldePoursuite, -valeurPonderee);

				// débit sur solde source
				new Transaction(eoSoldeSrc, eoSoldePoursuite, valeurPonderee);

				// mémoriser la valeur pondérée (sert pour l'écran de poursuite de solde
				// affichage)
				eoSoldePoursuite.setSpoValeurPonderee(Integer.valueOf(valeurPonderee));

				// date pour affichage
				eoSoldePoursuite.dateDebutPoursuite = jour.getDate();

				// ligne fiche rose (congé et reliquats pour l'instant ...)
				if ((eoSoldeSrc.isCongeNatif() && eoSoldeDst.isCongeNatif()) ||
						(eoSoldeSrc.isReliquat() && eoSoldeDst.isReliquat())) {

					LigneFicheRose lfrDebit = new LigneFicheRose(
							"Vidage " + eoSoldeSrc.toTypeSolde().stypLibelle() + " (" + eoSoldeSrc.toStructure().lcStructure() + ")",
							eoSoldePoursuite.motifPourFicheRose());
					lfrDebit.setCStructureCourante(eoSoldeSrc.toStructure().cStructure());
					lfrDebit.setDateFin(jour.getDate().timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0));

					LigneFicheRose lfrCredit = new LigneFicheRose(
							"Alimentation " + eoSoldeDst.toTypeSolde().stypLibelle() + " (" + eoSoldeDst.toStructure().lcStructure() + ")",
							eoSoldePoursuite.motifPourFicheRose());
					lfrCredit.setCStructureCourante(eoSoldeDst.toStructure().cStructure());
					lfrCredit.setDateDebut(jour.getDate());

					if (eoSoldeSrc.isCongeNatif() && eoSoldeDst.isCongeNatif()) {
						// sur congés
						lfrDebit.setDebitCng(valeurPonderee);
						lfrDebit.setRestantCng(0);
						lfrCredit.setDebitCng(-valeurPonderee);
						lfrCredit.setRestantCngFrom(planning.getLigneFicheRoseArray(), -valeurPonderee);
					} else if (eoSoldeSrc.isReliquat() && eoSoldeDst.isReliquat()) {
						// sur reliquats
						// sur congés
						lfrDebit.setDebitRel(valeurPonderee);
						lfrDebit.setRestantRel(0);
						lfrCredit.setDebitRel(-valeurPonderee);
						lfrCredit.setRestantRelFrom(planning.getLigneFicheRoseArray(), -valeurPonderee);
					}

					planning.getLigneFicheRoseArray().addObject(lfrDebit);
					planning.getLigneFicheRoseArray().addObject(lfrCredit);

				}

			}

		}

	}
}
