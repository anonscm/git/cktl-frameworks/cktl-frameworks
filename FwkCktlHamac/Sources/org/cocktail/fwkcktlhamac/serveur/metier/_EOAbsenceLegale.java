/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAbsenceLegale.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOAbsenceLegale extends A_Absence  {
	public static final String ENTITY_NAME = "AbsenceLegale";
	public static final String ENTITY_TABLE_NAME = "MANGUE.ABSENCES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "absNumero";

	public static final String ABS_ACTIVE_KEY = "absActive";
	public static final String ABS_AMPM_DEBUT_KEY = "absAmpmDebut";
	public static final String ABS_AMPM_FIN_KEY = "absAmpmFin";
	public static final String ABS_DEBUT_KEY = "absDebut";
	public static final String ABS_DUREE_TOTALE_KEY = "absDureeTotale";
	public static final String ABS_FIN_KEY = "absFin";
	public static final String ABS_MOTIF_KEY = "absMotif";
	public static final String ABS_VALIDE_KEY = "absValide";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String ABS_NUMERO_KEY = "absNumero";
	public static final String ABS_TYPE_CODE_KEY = "absTypeCode";
	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

//Colonnes dans la base de donnees
	public static final String ABS_ACTIVE_COLKEY = "ABS_ACTIVE";
	public static final String ABS_AMPM_DEBUT_COLKEY = "ABS_AMPM_DEBUT";
	public static final String ABS_AMPM_FIN_COLKEY = "ABS_AMPM_FIN";
	public static final String ABS_DEBUT_COLKEY = "ABS_DEBUT";
	public static final String ABS_DUREE_TOTALE_COLKEY = "ABS_DUREE_TOTALE";
	public static final String ABS_FIN_COLKEY = "ABS_FIN";
	public static final String ABS_MOTIF_COLKEY = "ABS_MOTIF";
	public static final String ABS_VALIDE_COLKEY = "ABS_VALIDE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String ABS_NUMERO_COLKEY = "ABS_NUMERO";
	public static final String ABS_TYPE_CODE_COLKEY = "ABS_TYPE_CODE";
	public static final String C_TYPE_EXCLUSION_COLKEY = "C_TYPE_EXCLUSION";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";


	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_OCCUPATION_TYPE_LEGAL_KEY = "toOccupationTypeLegal";
	public static final String TO_TYPE_ABSENCE_KEY = "toTypeAbsence";



	// Accessors methods
  public String absActive() {
    return (String) storedValueForKey(ABS_ACTIVE_KEY);
  }

  public void setAbsActive(String value) {
    takeStoredValueForKey(value, ABS_ACTIVE_KEY);
  }

  public String absAmpmDebut() {
    return (String) storedValueForKey(ABS_AMPM_DEBUT_KEY);
  }

  public void setAbsAmpmDebut(String value) {
    takeStoredValueForKey(value, ABS_AMPM_DEBUT_KEY);
  }

  public String absAmpmFin() {
    return (String) storedValueForKey(ABS_AMPM_FIN_KEY);
  }

  public void setAbsAmpmFin(String value) {
    takeStoredValueForKey(value, ABS_AMPM_FIN_KEY);
  }

  public NSTimestamp absDebut() {
    return (NSTimestamp) storedValueForKey(ABS_DEBUT_KEY);
  }

  public void setAbsDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ABS_DEBUT_KEY);
  }

  public String absDureeTotale() {
    return (String) storedValueForKey(ABS_DUREE_TOTALE_KEY);
  }

  public void setAbsDureeTotale(String value) {
    takeStoredValueForKey(value, ABS_DUREE_TOTALE_KEY);
  }

  public NSTimestamp absFin() {
    return (NSTimestamp) storedValueForKey(ABS_FIN_KEY);
  }

  public void setAbsFin(NSTimestamp value) {
    takeStoredValueForKey(value, ABS_FIN_KEY);
  }

  public String absMotif() {
    return (String) storedValueForKey(ABS_MOTIF_KEY);
  }

  public void setAbsMotif(String value) {
    takeStoredValueForKey(value, ABS_MOTIF_KEY);
  }

  public String absValide() {
    return (String) storedValueForKey(ABS_VALIDE_KEY);
  }

  public void setAbsValide(String value) {
    takeStoredValueForKey(value, ABS_VALIDE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupationLegal toOccupationTypeLegal() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupationLegal)storedValueForKey(TO_OCCUPATION_TYPE_LEGAL_KEY);
  }

  public void setToOccupationTypeLegalRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupationLegal value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupationLegal oldValue = toOccupationTypeLegal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_OCCUPATION_TYPE_LEGAL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_OCCUPATION_TYPE_LEGAL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence)storedValueForKey(TO_TYPE_ABSENCE_KEY);
  }

  public void setToTypeAbsenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence oldValue = toTypeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ABSENCE_KEY);
    }
  }
  

/**
 * Créer une instance de EOAbsenceLegale avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAbsenceLegale createEOAbsenceLegale(EOEditingContext editingContext, String absActive
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupationLegal toOccupationTypeLegal, org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence			) {
    EOAbsenceLegale eo = (EOAbsenceLegale) createAndInsertInstance(editingContext, _EOAbsenceLegale.ENTITY_NAME);    
		eo.setAbsActive(absActive);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToOccupationTypeLegalRelationship(toOccupationTypeLegal);
    eo.setToTypeAbsenceRelationship(toTypeAbsence);
    return eo;
  }

  
	  public EOAbsenceLegale localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAbsenceLegale)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAbsenceLegale creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAbsenceLegale creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAbsenceLegale object = (EOAbsenceLegale)createAndInsertInstance(editingContext, _EOAbsenceLegale.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAbsenceLegale localInstanceIn(EOEditingContext editingContext, EOAbsenceLegale eo) {
    EOAbsenceLegale localInstance = (eo == null) ? null : (EOAbsenceLegale)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAbsenceLegale#localInstanceIn a la place.
   */
	public static EOAbsenceLegale localInstanceOf(EOEditingContext editingContext, EOAbsenceLegale eo) {
		return EOAbsenceLegale.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOAbsenceLegale> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAbsenceLegale fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAbsenceLegale fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAbsenceLegale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAbsenceLegale)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAbsenceLegale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAbsenceLegale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAbsenceLegale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAbsenceLegale)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAbsenceLegale fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAbsenceLegale eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAbsenceLegale ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAbsenceLegale fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
