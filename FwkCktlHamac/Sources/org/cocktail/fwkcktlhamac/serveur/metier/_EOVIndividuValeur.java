/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVIndividuValeur.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOVIndividuValeur extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "VIndividuValeur";
	public static final String ENTITY_TABLE_NAME = "HAMAC.V_INDIVIDU_VALEUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "plaId";

	public static final String BALANCE_RESTANT_KEY = "balanceRestant";
	public static final String CONGES_RESTANT_KEY = "congesRestant";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PRENOM_KEY = "nomPrenom";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PER_ID_KEY = "perId";
	public static final String PER_LIBELLE_KEY = "perLibelle";
	public static final String PERS_ID_KEY = "persId";
	public static final String PRENOM_KEY = "prenom";
	public static final String RELIQUAT_RESTANT_KEY = "reliquatRestant";

// Attributs non visibles
	public static final String PLA_ID_KEY = "plaId";

//Colonnes dans la base de donnees
	public static final String BALANCE_RESTANT_COLKEY = "BALANCE_RESTANT";
	public static final String CONGES_RESTANT_COLKEY = "CONGES_RESTANT";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PRENOM_COLKEY = "PRENOM";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PER_ID_COLKEY = "PER_ID";
	public static final String PER_LIBELLE_COLKEY = "PER_LIBELLE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String RELIQUAT_RESTANT_COLKEY = "RELIQUAT_RESTANT";

	public static final String PLA_ID_COLKEY = "PLA_ID";


	// Relationships
	public static final String TO_PERIODE_KEY = "toPeriode";
	public static final String TO_PLANNING_KEY = "toPlanning";



	// Accessors methods
  public Integer balanceRestant() {
    return (Integer) storedValueForKey(BALANCE_RESTANT_KEY);
  }

  public void setBalanceRestant(Integer value) {
    takeStoredValueForKey(value, BALANCE_RESTANT_KEY);
  }

  public Integer congesRestant() {
    return (Integer) storedValueForKey(CONGES_RESTANT_KEY);
  }

  public void setCongesRestant(Integer value) {
    takeStoredValueForKey(value, CONGES_RESTANT_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomPrenom() {
    return (String) storedValueForKey(NOM_PRENOM_KEY);
  }

  public void setNomPrenom(String value) {
    takeStoredValueForKey(value, NOM_PRENOM_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer perId() {
    return (Integer) storedValueForKey(PER_ID_KEY);
  }

  public void setPerId(Integer value) {
    takeStoredValueForKey(value, PER_ID_KEY);
  }

  public String perLibelle() {
    return (String) storedValueForKey(PER_LIBELLE_KEY);
  }

  public void setPerLibelle(String value) {
    takeStoredValueForKey(value, PER_LIBELLE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public Integer reliquatRestant() {
    return (Integer) storedValueForKey(RELIQUAT_RESTANT_KEY);
  }

  public void setReliquatRestant(Integer value) {
    takeStoredValueForKey(value, RELIQUAT_RESTANT_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode)storedValueForKey(TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERIODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)storedValueForKey(TO_PLANNING_KEY);
  }

  public void setToPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning oldValue = toPlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLANNING_KEY);
    }
  }
  

/**
 * Créer une instance de EOVIndividuValeur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVIndividuValeur createEOVIndividuValeur(EOEditingContext editingContext, String cStructure
, String llStructure
, Integer noIndividu
, String nomPrenom
, String nomUsuel
, Integer perId
, String perLibelle
, Integer persId
, String prenom
, org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode, org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning			) {
    EOVIndividuValeur eo = (EOVIndividuValeur) createAndInsertInstance(editingContext, _EOVIndividuValeur.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setLlStructure(llStructure);
		eo.setNoIndividu(noIndividu);
		eo.setNomPrenom(nomPrenom);
		eo.setNomUsuel(nomUsuel);
		eo.setPerId(perId);
		eo.setPerLibelle(perLibelle);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
    eo.setToPeriodeRelationship(toPeriode);
    eo.setToPlanningRelationship(toPlanning);
    return eo;
  }

  
	  public EOVIndividuValeur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVIndividuValeur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVIndividuValeur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVIndividuValeur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVIndividuValeur object = (EOVIndividuValeur)createAndInsertInstance(editingContext, _EOVIndividuValeur.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVIndividuValeur localInstanceIn(EOEditingContext editingContext, EOVIndividuValeur eo) {
    EOVIndividuValeur localInstance = (eo == null) ? null : (EOVIndividuValeur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVIndividuValeur#localInstanceIn a la place.
   */
	public static EOVIndividuValeur localInstanceOf(EOEditingContext editingContext, EOVIndividuValeur eo) {
		return EOVIndividuValeur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOVIndividuValeur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVIndividuValeur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVIndividuValeur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVIndividuValeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVIndividuValeur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVIndividuValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVIndividuValeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVIndividuValeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVIndividuValeur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVIndividuValeur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVIndividuValeur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVIndividuValeur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVIndividuValeur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
