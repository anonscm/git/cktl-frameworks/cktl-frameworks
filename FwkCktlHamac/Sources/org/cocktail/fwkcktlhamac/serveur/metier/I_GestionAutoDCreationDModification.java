/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définir les classes pourlesquelles les dates de création et modificaiton sont
 * gérées automatiquement :
 * 
 * - création : à l'insertion de l'objet
 * 
 * - modification : lors d'un changement d'un attribut (et pas d'une toOne /
 * toMany)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public interface I_GestionAutoDCreationDModification {

	public NSTimestamp dCreation();

	public NSTimestamp dModification();

	public void setDCreation(NSTimestamp value);

	public void setDModification(NSTimestamp value);

}
