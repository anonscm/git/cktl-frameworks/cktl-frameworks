/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOVisa
		extends _EOVisa
		implements I_GestionAutoDCreationDModification {

	public EOVisa() {
		super();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();

	}

	/**
	 * Vous pouvez d̩finir un delegate qui sera appel̩ lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez d̩finir un delegate
	 * qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * d̩finir un delegate qui sera appel̩ lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	@Override
	public NSArray<EOIndividu> tosIndividuViseur() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NSArray<EOStructure> tosStructureViseur() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addToTosIndividuViseurRelationship(EOIndividu object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeFromTosIndividuViseurRelationship(EOIndividu object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addToTosStructureViseurRelationship(EOStructure object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeFromTosStructureViseurRelationship(EOStructure object) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return
	 */
	public final boolean isAccepte() {
		boolean isAccepte = false;

		if (visVisaAccepte() != null &&
				visVisaAccepte().equals(OUI)) {
			isAccepte = true;
		}

		return isAccepte;
	}

	/**
	 * @return
	 */
	public final boolean isRefuse() {
		boolean isRefuse = false;

		if (visVisaAccepte() != null &&
				visVisaAccepte().equals(NON)) {
			isRefuse = true;
		}

		return isRefuse;
	}

	/**
	 * @return
	 */
	public final boolean isDefinitif() {
		boolean isDefinitif = false;

		if (temDefinitif().equals(OUI)) {
			isDefinitif = true;
		}

		return isDefinitif;
	}

	/**
	 * @param ec
	 * @param persIdViseur
	 * @param toStructureAyantVise
	 * @param toTypeVisa
	 * @return
	 */
	public static EOVisa createEOVisa(
			EOEditingContext ec, Integer persIdViseur, EOStructure toStructureAyantVise, EOTypeVisa toTypeVisa) {
		EOVisa eoVisa = null;

		eoVisa = createEOVisa(
				ec, DateCtrlHamac.now(), DateCtrlHamac.now(),
				persIdViseur, NON, toStructureAyantVise,
				toTypeVisa);

		return eoVisa;
	}

	/**
	 * Commentaire lié à la validation de l'absence absence
	 * 
	 * N'afficher que pour les absences ayant le statut validé
	 * 
	 * @return
	 */
	public final static String commentaireValidation(A_Absence absence) {
		String commentaire = "";

		NSArray<String> chaineArray = commentaireValidationStringArray(absence);

		for (int i = 0; i < chaineArray.count(); i++) {
			commentaire += chaineArray.objectAtIndex(i);
			if (i < chaineArray.count() - 1) {
				commentaire += ", ";
			}
		}

		return commentaire;
	}

	/**
	 * Liste des chaines de validation pour affichage uniquement
	 * 
	 * @param absence
	 * @return
	 */
	public final static NSArray<String> commentaireValidationStringArray(A_Absence absence) {
		NSMutableArray<String> array = new NSMutableArray<String>();

		if (absence != null) {

			if (absence.isAbsenceLegale()) {
				array.addObject("saisie " + DateCtrlHamac.dateToString(absence.dCreation()) + "et validée par la RH");
			} else {
				EOOccupation eoOccupation = (EOOccupation) absence;

				if (!NSArrayCtrl.isEmpty(eoOccupation.tosDelegationHistorique())) {
					String chaine = "saisie le " + DateCtrlHamac.dateToString(eoOccupation.dCreation()) + " par ";
					chaine += eoOccupation.tosDelegationHistorique().lastObject().toPersonne().getNomPrenomAffichage();
					array.addObject(chaine);
				} else {
					array.addObject("saisie le " + DateCtrlHamac.dateToString(eoOccupation.dCreation()));
				}
				
				

				// n'afficher que les visas définitifs

				NSArray<EOVisa> eoVisaDefinitifArray = EOQualifier.filteredArrayWithQualifier(
						eoOccupation.tosVisa(), ERXQ.equals(EOVisa.TEM_DEFINITIF_KEY, A_FwkCktlHamacRecord.OUI));

				for (EOVisa eoVisa : eoVisaDefinitifArray) {

					String chaine = "";

					if (eoVisa.toTypeVisa().isNiveauVisa()) {
						chaine += "visa";

					} else if (eoVisa.toTypeVisa().isNiveauValidation()) {
						chaine += "validée";
					}

					if (eoVisa.toPersonneViseur() != null) {
						chaine += " par " + eoVisa.toPersonneViseur().getNomPrenomAffichage() + " (" +
								eoVisa.toStructureAyantVise().lcStructure() + ")";
					}

					chaine += " le ";
					chaine += DateCtrlHamac.dateToString(eoVisa.dCreation());

					array.addObject(chaine);

				}

			}

		} else {

			// TODO commentaire sur le planning

		}

		return array.immutableClone();
	}

}
