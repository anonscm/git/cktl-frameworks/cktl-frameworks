/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCetTransactionAnnuelle.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOCetTransactionAnnuelle extends A_ToManyPersonne  {
	public static final String ENTITY_NAME = "CetTransactionAnnuelle";
	public static final String ENTITY_TABLE_NAME = "HAMAC.CET_TRANSACTION_ANNUELLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ctaId";

	public static final String CET_DEC_DATE_KEY = "cetDecDate";
	public static final String CET_DEC_EPARGNE_KEY = "cetDecEpargne";
	public static final String CET_DEC_INDEM_ANCIEN_KEY = "cetDecIndemAncien";
	public static final String CET_DEC_INDEM_PERENNE_KEY = "cetDecIndemPerenne";
	public static final String CET_DEC_MAINT_ANCIEN_KEY = "cetDecMaintAncien";
	public static final String CET_DEC_MAINT_FORCE_PERENNE_KEY = "cetDecMaintForcePerenne";
	public static final String CET_DEC_MAINT_PERENNE_KEY = "cetDecMaintPerenne";
	public static final String CET_DEC_RAFP_ANCIEN_KEY = "cetDecRafpAncien";
	public static final String CET_DEC_RAFP_PERENNE_KEY = "cetDecRafpPerenne";
	public static final String CET_DEC_TRANS_ANCIEN_PERENNE_KEY = "cetDecTransAncienPerenne";
	public static final String CET_DEM_DATE_KEY = "cetDemDate";
	public static final String CET_DEM_EPARGNE_KEY = "cetDemEpargne";
	public static final String CET_DEM_INDEM_ANCIEN_KEY = "cetDemIndemAncien";
	public static final String CET_DEM_INDEM_PERENNE_KEY = "cetDemIndemPerenne";
	public static final String CET_DEM_MAINT_ANCIEN_KEY = "cetDemMaintAncien";
	public static final String CET_DEM_MAINT_FORCE_PERENNE_KEY = "cetDemMaintForcePerenne";
	public static final String CET_DEM_MAINT_PERENNE_KEY = "cetDemMaintPerenne";
	public static final String CET_DEM_RAFP_ANCIEN_KEY = "cetDemRafpAncien";
	public static final String CET_DEM_RAFP_PERENNE_KEY = "cetDemRafpPerenne";
	public static final String CET_DEM_TRANS_ANCIEN_PERENNE_KEY = "cetDemTransAncienPerenne";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NATURE_KEY = "nature";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles
	public static final String CTA_ID_KEY = "ctaId";
	public static final String PER_ID_KEY = "perId";

//Colonnes dans la base de donnees
	public static final String CET_DEC_DATE_COLKEY = "CET_DATE_DECISION";
	public static final String CET_DEC_EPARGNE_COLKEY = "CET_DEC_EPARGNE";
	public static final String CET_DEC_INDEM_ANCIEN_COLKEY = "CET_DEC_INDEM_ANCIEN";
	public static final String CET_DEC_INDEM_PERENNE_COLKEY = "CET_DEC_INDEM_PERENNE";
	public static final String CET_DEC_MAINT_ANCIEN_COLKEY = "CET_DEC_MAINT_ANCIEN";
	public static final String CET_DEC_MAINT_FORCE_PERENNE_COLKEY = "CET_DEC_MAINT_FORCE_PERENNE";
	public static final String CET_DEC_MAINT_PERENNE_COLKEY = "CET_DEC_MAINT_PERENNE";
	public static final String CET_DEC_RAFP_ANCIEN_COLKEY = "CET_DEC_RAFP_ANCIEN";
	public static final String CET_DEC_RAFP_PERENNE_COLKEY = "CET_DEC_RAFP_PERENNE";
	public static final String CET_DEC_TRANS_ANCIEN_PERENNE_COLKEY = "CET_DEC_TRANS_ANCIEN_PERENNE";
	public static final String CET_DEM_DATE_COLKEY = "CET_DATE_DEMANDE";
	public static final String CET_DEM_EPARGNE_COLKEY = "CET_DEM_EPARGNE";
	public static final String CET_DEM_INDEM_ANCIEN_COLKEY = "CET_DEM_INDEM_ANCIEN";
	public static final String CET_DEM_INDEM_PERENNE_COLKEY = "CET_DEM_INDEM_PERENNE";
	public static final String CET_DEM_MAINT_ANCIEN_COLKEY = "CET_DEM_MAINT_ANCIEN";
	public static final String CET_DEM_MAINT_FORCE_PERENNE_COLKEY = "CET_DEM_MAINT_FORCE_PERENNE";
	public static final String CET_DEM_MAINT_PERENNE_COLKEY = "CET_DEM_MAINT_PERENNE";
	public static final String CET_DEM_RAFP_ANCIEN_COLKEY = "CET_DEM_RAFP_ANCIEN";
	public static final String CET_DEM_RAFP_PERENNE_COLKEY = "CET_DEM_RAFP_PERENNE";
	public static final String CET_DEM_TRANS_ANCIEN_PERENNE_COLKEY = "CET_DEM_TRANS_ANCIEN_PERENNE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NATURE_COLKEY = "NATURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";

	public static final String CTA_ID_COLKEY = "CTA_ID";
	public static final String PER_ID_COLKEY = "PER_ID";


	// Relationships
	public static final String TO_PERIODE_KEY = "toPeriode";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";



	// Accessors methods
  public NSTimestamp cetDecDate() {
    return (NSTimestamp) storedValueForKey(CET_DEC_DATE_KEY);
  }

  public void setCetDecDate(NSTimestamp value) {
    takeStoredValueForKey(value, CET_DEC_DATE_KEY);
  }

  public Integer cetDecEpargne() {
    return (Integer) storedValueForKey(CET_DEC_EPARGNE_KEY);
  }

  public void setCetDecEpargne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_EPARGNE_KEY);
  }

  public Integer cetDecIndemAncien() {
    return (Integer) storedValueForKey(CET_DEC_INDEM_ANCIEN_KEY);
  }

  public void setCetDecIndemAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEC_INDEM_ANCIEN_KEY);
  }

  public Integer cetDecIndemPerenne() {
    return (Integer) storedValueForKey(CET_DEC_INDEM_PERENNE_KEY);
  }

  public void setCetDecIndemPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_INDEM_PERENNE_KEY);
  }

  public Integer cetDecMaintAncien() {
    return (Integer) storedValueForKey(CET_DEC_MAINT_ANCIEN_KEY);
  }

  public void setCetDecMaintAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEC_MAINT_ANCIEN_KEY);
  }

  public Integer cetDecMaintForcePerenne() {
    return (Integer) storedValueForKey(CET_DEC_MAINT_FORCE_PERENNE_KEY);
  }

  public void setCetDecMaintForcePerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_MAINT_FORCE_PERENNE_KEY);
  }

  public Integer cetDecMaintPerenne() {
    return (Integer) storedValueForKey(CET_DEC_MAINT_PERENNE_KEY);
  }

  public void setCetDecMaintPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_MAINT_PERENNE_KEY);
  }

  public Integer cetDecRafpAncien() {
    return (Integer) storedValueForKey(CET_DEC_RAFP_ANCIEN_KEY);
  }

  public void setCetDecRafpAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEC_RAFP_ANCIEN_KEY);
  }

  public Integer cetDecRafpPerenne() {
    return (Integer) storedValueForKey(CET_DEC_RAFP_PERENNE_KEY);
  }

  public void setCetDecRafpPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_RAFP_PERENNE_KEY);
  }

  public Integer cetDecTransAncienPerenne() {
    return (Integer) storedValueForKey(CET_DEC_TRANS_ANCIEN_PERENNE_KEY);
  }

  public void setCetDecTransAncienPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEC_TRANS_ANCIEN_PERENNE_KEY);
  }

  public NSTimestamp cetDemDate() {
    return (NSTimestamp) storedValueForKey(CET_DEM_DATE_KEY);
  }

  public void setCetDemDate(NSTimestamp value) {
    takeStoredValueForKey(value, CET_DEM_DATE_KEY);
  }

  public Integer cetDemEpargne() {
    return (Integer) storedValueForKey(CET_DEM_EPARGNE_KEY);
  }

  public void setCetDemEpargne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_EPARGNE_KEY);
  }

  public Integer cetDemIndemAncien() {
    return (Integer) storedValueForKey(CET_DEM_INDEM_ANCIEN_KEY);
  }

  public void setCetDemIndemAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEM_INDEM_ANCIEN_KEY);
  }

  public Integer cetDemIndemPerenne() {
    return (Integer) storedValueForKey(CET_DEM_INDEM_PERENNE_KEY);
  }

  public void setCetDemIndemPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_INDEM_PERENNE_KEY);
  }

  public Integer cetDemMaintAncien() {
    return (Integer) storedValueForKey(CET_DEM_MAINT_ANCIEN_KEY);
  }

  public void setCetDemMaintAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEM_MAINT_ANCIEN_KEY);
  }

  public Integer cetDemMaintForcePerenne() {
    return (Integer) storedValueForKey(CET_DEM_MAINT_FORCE_PERENNE_KEY);
  }

  public void setCetDemMaintForcePerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_MAINT_FORCE_PERENNE_KEY);
  }

  public Integer cetDemMaintPerenne() {
    return (Integer) storedValueForKey(CET_DEM_MAINT_PERENNE_KEY);
  }

  public void setCetDemMaintPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_MAINT_PERENNE_KEY);
  }

  public Integer cetDemRafpAncien() {
    return (Integer) storedValueForKey(CET_DEM_RAFP_ANCIEN_KEY);
  }

  public void setCetDemRafpAncien(Integer value) {
    takeStoredValueForKey(value, CET_DEM_RAFP_ANCIEN_KEY);
  }

  public Integer cetDemRafpPerenne() {
    return (Integer) storedValueForKey(CET_DEM_RAFP_PERENNE_KEY);
  }

  public void setCetDemRafpPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_RAFP_PERENNE_KEY);
  }

  public Integer cetDemTransAncienPerenne() {
    return (Integer) storedValueForKey(CET_DEM_TRANS_ANCIEN_PERENNE_KEY);
  }

  public void setCetDemTransAncienPerenne(Integer value) {
    takeStoredValueForKey(value, CET_DEM_TRANS_ANCIEN_PERENNE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String nature() {
    return (String) storedValueForKey(NATURE_KEY);
  }

  public void setNature(String value) {
    takeStoredValueForKey(value, NATURE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode)storedValueForKey(TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERIODE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCetTransactionAnnuelle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCetTransactionAnnuelle createEOCetTransactionAnnuelle(EOEditingContext editingContext, NSTimestamp cetDemDate
, NSTimestamp dCreation
, NSTimestamp dModification
, String nature
, Integer persId
, org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode			) {
    EOCetTransactionAnnuelle eo = (EOCetTransactionAnnuelle) createAndInsertInstance(editingContext, _EOCetTransactionAnnuelle.ENTITY_NAME);    
		eo.setCetDemDate(cetDemDate);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNature(nature);
		eo.setPersId(persId);
    eo.setToPeriodeRelationship(toPeriode);
    return eo;
  }

  
	  public EOCetTransactionAnnuelle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCetTransactionAnnuelle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCetTransactionAnnuelle creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCetTransactionAnnuelle creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCetTransactionAnnuelle object = (EOCetTransactionAnnuelle)createAndInsertInstance(editingContext, _EOCetTransactionAnnuelle.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCetTransactionAnnuelle localInstanceIn(EOEditingContext editingContext, EOCetTransactionAnnuelle eo) {
    EOCetTransactionAnnuelle localInstance = (eo == null) ? null : (EOCetTransactionAnnuelle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCetTransactionAnnuelle#localInstanceIn a la place.
   */
	public static EOCetTransactionAnnuelle localInstanceOf(EOEditingContext editingContext, EOCetTransactionAnnuelle eo) {
		return EOCetTransactionAnnuelle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOCetTransactionAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCetTransactionAnnuelle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCetTransactionAnnuelle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCetTransactionAnnuelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCetTransactionAnnuelle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCetTransactionAnnuelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCetTransactionAnnuelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCetTransactionAnnuelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCetTransactionAnnuelle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCetTransactionAnnuelle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCetTransactionAnnuelle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCetTransactionAnnuelle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCetTransactionAnnuelle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
