/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;

public class EOTypeSequenceValidation extends _EOTypeSequenceValidation {

	public EOTypeSequenceValidation() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String TSV_CODE_CLASSIQUE_KEY = "CLASSIQUE";
	public final static String TSV_CODE_RH_LOCAL_KEY = "RH_LOCAL";
	public final static String TSV_CODE_RH_KEY = "RH";

	private final static EOTypeSequenceValidation getEoTypeSequenceValidationForCode(EOEditingContext ec, String code) {
		EOTypeSequenceValidation eoTypeSequenceValidation = null;

		eoTypeSequenceValidation = fetchRequiredByKeyValue(ec, TSV_CODE_KEY, code);

		return eoTypeSequenceValidation;
	}
	
	/**
	 * type séquence validation RH Local ou RH ?
	 * @return true/false 
	 */
	public boolean isRH() {
		return TSV_CODE_RH_LOCAL_KEY.equals(tsvCode()) || TSV_CODE_RH_KEY.equals(tsvCode()); 
	}
	

	public static EOTypeSequenceValidation getEoTypeSequenceValidationPlanning(EOEditingContext ec) {
		EOTypeSequenceValidation eoTypeSequenceValidation = null;

		eoTypeSequenceValidation = getEoTypeSequenceValidationForCode(ec, HamacCktlConfig.stringForKey(EOParametre.SEQUENCE_VALIDATION_PLANNING));

		return eoTypeSequenceValidation;
	}

	/**
	 * Pour remonter hiérarchiquement jusqu'aux niveaux supérieurs de validation.
	 * 
	 * TODO hiérarchiser au niveau BDD
	 * 
	 * @return
	 */
	public EOTypeSequenceValidation getEoTypeSequenceValidationSuperieur() {
		EOTypeSequenceValidation eoTypeSequenceValidation = null;

		if (tsvCode().equals(TSV_CODE_CLASSIQUE_KEY)) {
			eoTypeSequenceValidation = getEoTypeSequenceValidationForCode(editingContext(), TSV_CODE_RH_LOCAL_KEY);
		} else if (tsvCode().equals(TSV_CODE_RH_LOCAL_KEY)) {
			eoTypeSequenceValidation = getEoTypeSequenceValidationForCode(editingContext(), TSV_CODE_RH_KEY);
		} else {
			// pas trouvé, on reste sur lui même
			eoTypeSequenceValidation = this;
		}

		return eoTypeSequenceValidation;
	}

}
