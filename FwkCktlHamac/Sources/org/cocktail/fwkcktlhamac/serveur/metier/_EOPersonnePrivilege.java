/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersonnePrivilege.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOPersonnePrivilege extends A_ToManyPersonne  {
	public static final String ENTITY_NAME = "PersonnePrivilege";
	public static final String ENTITY_TABLE_NAME = "HAMAC.PERSONNE_PRIVILEGE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "priId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_BENEFICIAIRE_KEY = "persIdBeneficiaire";
	public static final String TEM_AUCUNE_BONIFICATION_KEY = "temAucuneBonification";
	public static final String TEM_DEP_DROIT_CONGE_KEY = "temDepDroitConge";
	public static final String TEM_DEP_SEM_HAUTES_KEY = "temDepSemHautes";
	public static final String TEM_HORS_NORMES_KEY = "temHorsNormes";
	public static final String TEM_PASSE_DROIT_KEY = "temPasseDroit";
	public static final String TEM_PAUSE_MERIDIENNE_LIBRE_KEY = "temPauseMeridienneLibre";
	public static final String TEM_TPA_KEY = "temTpa";

// Attributs non visibles
	public static final String PER_ID_KEY = "perId";
	public static final String PRI_ID_KEY = "priId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_BENEFICIAIRE_COLKEY = "PERS_ID_BENEFICIAIRE";
	public static final String TEM_AUCUNE_BONIFICATION_COLKEY = "TEM_AUCUNE_BONIFICATION";
	public static final String TEM_DEP_DROIT_CONGE_COLKEY = "TEM_DEP_DROIT_CONGE";
	public static final String TEM_DEP_SEM_HAUTES_COLKEY = "TEM_DEP_SEM_HAUTES";
	public static final String TEM_HORS_NORMES_COLKEY = "TEM_HORS_NORMES";
	public static final String TEM_PASSE_DROIT_COLKEY = "TEM_PASSE_DROIT";
	public static final String TEM_PAUSE_MERIDIENNE_LIBRE_COLKEY = "TEM_PAUSE_MERIDIENNE_LIBRE";
	public static final String TEM_TPA_COLKEY = "TEM_TPA";

	public static final String PER_ID_COLKEY = "PER_ID";
	public static final String PRI_ID_COLKEY = "PRI_ID";


	// Relationships
	public static final String TO_PERIODE_KEY = "toPeriode";
	public static final String TOS_INDIVIDU_KEY = "tosIndividu";
	public static final String TOS_STRUCTURE_KEY = "tosStructure";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persIdBeneficiaire() {
    return (Integer) storedValueForKey(PERS_ID_BENEFICIAIRE_KEY);
  }

  public void setPersIdBeneficiaire(Integer value) {
    takeStoredValueForKey(value, PERS_ID_BENEFICIAIRE_KEY);
  }

  public String temAucuneBonification() {
    return (String) storedValueForKey(TEM_AUCUNE_BONIFICATION_KEY);
  }

  public void setTemAucuneBonification(String value) {
    takeStoredValueForKey(value, TEM_AUCUNE_BONIFICATION_KEY);
  }

  public String temDepDroitConge() {
    return (String) storedValueForKey(TEM_DEP_DROIT_CONGE_KEY);
  }

  public void setTemDepDroitConge(String value) {
    takeStoredValueForKey(value, TEM_DEP_DROIT_CONGE_KEY);
  }

  public String temDepSemHautes() {
    return (String) storedValueForKey(TEM_DEP_SEM_HAUTES_KEY);
  }

  public void setTemDepSemHautes(String value) {
    takeStoredValueForKey(value, TEM_DEP_SEM_HAUTES_KEY);
  }

  public String temHorsNormes() {
    return (String) storedValueForKey(TEM_HORS_NORMES_KEY);
  }

  public void setTemHorsNormes(String value) {
    takeStoredValueForKey(value, TEM_HORS_NORMES_KEY);
  }

  public String temPasseDroit() {
    return (String) storedValueForKey(TEM_PASSE_DROIT_KEY);
  }

  public void setTemPasseDroit(String value) {
    takeStoredValueForKey(value, TEM_PASSE_DROIT_KEY);
  }

  public String temPauseMeridienneLibre() {
    return (String) storedValueForKey(TEM_PAUSE_MERIDIENNE_LIBRE_KEY);
  }

  public void setTemPauseMeridienneLibre(String value) {
    takeStoredValueForKey(value, TEM_PAUSE_MERIDIENNE_LIBRE_KEY);
  }

  public String temTpa() {
    return (String) storedValueForKey(TEM_TPA_KEY);
  }

  public void setTemTpa(String value) {
    takeStoredValueForKey(value, TEM_TPA_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode)storedValueForKey(TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERIODE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu() {
    return (NSArray)storedValueForKey(TOS_INDIVIDU_KEY);
  }

  public NSArray tosIndividu(EOQualifier qualifier) {
    return tosIndividu(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> tosIndividu(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosIndividu();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public void removeFromTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu createTosIndividuRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Individu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_INDIVIDU_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) eo;
  }

  public void deleteTosIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_INDIVIDU_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosIndividuRelationships() {
    Enumeration objects = tosIndividu().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosIndividuRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure() {
    return (NSArray)storedValueForKey(TOS_STRUCTURE_KEY);
  }

  public NSArray tosStructure(EOQualifier qualifier) {
    return tosStructure(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> tosStructure(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosStructure();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public void removeFromTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createTosStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_Structure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_STRUCTURE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteTosStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_STRUCTURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosStructureRelationships() {
    Enumeration objects = tosStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosStructureRelationship((org.cocktail.fwkcktlpersonne.common.metier.EOStructure)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPersonnePrivilege avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersonnePrivilege createEOPersonnePrivilege(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdBeneficiaire
, String temAucuneBonification
, String temDepDroitConge
, String temDepSemHautes
, String temHorsNormes
, String temPasseDroit
, String temPauseMeridienneLibre
, String temTpa
, org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode toPeriode			) {
    EOPersonnePrivilege eo = (EOPersonnePrivilege) createAndInsertInstance(editingContext, _EOPersonnePrivilege.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdBeneficiaire(persIdBeneficiaire);
		eo.setTemAucuneBonification(temAucuneBonification);
		eo.setTemDepDroitConge(temDepDroitConge);
		eo.setTemDepSemHautes(temDepSemHautes);
		eo.setTemHorsNormes(temHorsNormes);
		eo.setTemPasseDroit(temPasseDroit);
		eo.setTemPauseMeridienneLibre(temPauseMeridienneLibre);
		eo.setTemTpa(temTpa);
    eo.setToPeriodeRelationship(toPeriode);
    return eo;
  }

  
	  public EOPersonnePrivilege localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersonnePrivilege)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonnePrivilege creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersonnePrivilege creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPersonnePrivilege object = (EOPersonnePrivilege)createAndInsertInstance(editingContext, _EOPersonnePrivilege.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersonnePrivilege localInstanceIn(EOEditingContext editingContext, EOPersonnePrivilege eo) {
    EOPersonnePrivilege localInstance = (eo == null) ? null : (EOPersonnePrivilege)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersonnePrivilege#localInstanceIn a la place.
   */
	public static EOPersonnePrivilege localInstanceOf(EOEditingContext editingContext, EOPersonnePrivilege eo) {
		return EOPersonnePrivilege.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOPersonnePrivilege> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersonnePrivilege fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersonnePrivilege fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonnePrivilege eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonnePrivilege)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersonnePrivilege fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersonnePrivilege fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonnePrivilege eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonnePrivilege)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersonnePrivilege fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonnePrivilege eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonnePrivilege ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersonnePrivilege fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
