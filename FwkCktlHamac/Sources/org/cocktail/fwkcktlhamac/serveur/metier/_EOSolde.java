/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSolde.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOSolde extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "Solde";
	public static final String ENTITY_TABLE_NAME = "HAMAC.SOLDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "solId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VALIDITE_KEY = "dDebValidite";
	public static final String D_FIN_VALIDITE_KEY = "dFinValidite";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INIT_KEY = "init";
	public static final String RESTANT_KEY = "restant";
	public static final String SOL_MOTIF_KEY = "solMotif";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String PLA_ID_KEY = "plaId";
	public static final String SOL_ID_KEY = "solId";
	public static final String STYP_ID_KEY = "stypId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VALIDITE_COLKEY = "D_DEB_VALIDITE";
	public static final String D_FIN_VALIDITE_COLKEY = "D_FIN_VALIDITE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INIT_COLKEY = "INIT";
	public static final String RESTANT_COLKEY = "RESTANT";
	public static final String SOL_MOTIF_COLKEY = "SOL_MOTIF";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String PLA_ID_COLKEY = "PLA_ID";
	public static final String SOL_ID_COLKEY = "SOL_ID";
	public static final String STYP_ID_COLKEY = "STYP_ID";


	// Relationships
	public static final String TO_PLANNING_KEY = "toPlanning";
	public static final String TOS_SOLDE_POURSUITE_AS_DST_KEY = "tosSoldePoursuiteAsDst";
	public static final String TOS_SOLDE_POURSUITE_AS_SRC_KEY = "tosSoldePoursuiteAsSrc";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_TYPE_SOLDE_KEY = "toTypeSolde";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebValidite() {
    return (NSTimestamp) storedValueForKey(D_DEB_VALIDITE_KEY);
  }

  public void setDDebValidite(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VALIDITE_KEY);
  }

  public NSTimestamp dFinValidite() {
    return (NSTimestamp) storedValueForKey(D_FIN_VALIDITE_KEY);
  }

  public void setDFinValidite(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VALIDITE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer init() {
    return (Integer) storedValueForKey(INIT_KEY);
  }

  public void setInit(Integer value) {
    takeStoredValueForKey(value, INIT_KEY);
  }

  public Integer restant() {
    return (Integer) storedValueForKey(RESTANT_KEY);
  }

  public void setRestant(Integer value) {
    takeStoredValueForKey(value, RESTANT_KEY);
  }

  public String solMotif() {
    return (String) storedValueForKey(SOL_MOTIF_KEY);
  }

  public void setSolMotif(String value) {
    takeStoredValueForKey(value, SOL_MOTIF_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning)storedValueForKey(TO_PLANNING_KEY);
  }

  public void setToPlanningRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning oldValue = toPlanning();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLANNING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLANNING_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde toTypeSolde() {
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde)storedValueForKey(TO_TYPE_SOLDE_KEY);
  }

  public void setToTypeSoldeRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde value) {
    if (value == null) {
    	org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde oldValue = toTypeSolde();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SOLDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SOLDE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsDst() {
    return (NSArray)storedValueForKey(TOS_SOLDE_POURSUITE_AS_DST_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsDst(EOQualifier qualifier) {
    return tosSoldePoursuiteAsDst(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsDst(EOQualifier qualifier, boolean fetch) {
    return tosSoldePoursuiteAsDst(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsDst(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite.TO_SOLDE_DST_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosSoldePoursuiteAsDst();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosSoldePoursuiteAsDstRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_DST_KEY);
  }

  public void removeFromTosSoldePoursuiteAsDstRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_DST_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite createTosSoldePoursuiteAsDstRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("SoldePoursuite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_SOLDE_POURSUITE_AS_DST_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite) eo;
  }

  public void deleteTosSoldePoursuiteAsDstRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_DST_KEY);
  }

  public void deleteAllTosSoldePoursuiteAsDstRelationships() {
    Enumeration objects = tosSoldePoursuiteAsDst().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosSoldePoursuiteAsDstRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsSrc() {
    return (NSArray)storedValueForKey(TOS_SOLDE_POURSUITE_AS_SRC_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsSrc(EOQualifier qualifier) {
    return tosSoldePoursuiteAsSrc(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsSrc(EOQualifier qualifier, boolean fetch) {
    return tosSoldePoursuiteAsSrc(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite> tosSoldePoursuiteAsSrc(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite.TO_SOLDE_SRC_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosSoldePoursuiteAsSrc();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosSoldePoursuiteAsSrcRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_SRC_KEY);
  }

  public void removeFromTosSoldePoursuiteAsSrcRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_SRC_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite createTosSoldePoursuiteAsSrcRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("SoldePoursuite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_SOLDE_POURSUITE_AS_SRC_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite) eo;
  }

  public void deleteTosSoldePoursuiteAsSrcRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SOLDE_POURSUITE_AS_SRC_KEY);
  }

  public void deleteAllTosSoldePoursuiteAsSrcRelationships() {
    Enumeration objects = tosSoldePoursuiteAsSrc().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosSoldePoursuiteAsSrcRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOSoldePoursuite)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOSolde avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSolde createEOSolde(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning toPlanning, org.cocktail.fwkcktlhamac.serveur.metier.EOTypeSolde toTypeSolde			) {
    EOSolde eo = (EOSolde) createAndInsertInstance(editingContext, _EOSolde.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToPlanningRelationship(toPlanning);
    eo.setToTypeSoldeRelationship(toTypeSolde);
    return eo;
  }

  
	  public EOSolde localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSolde)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSolde creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSolde creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSolde object = (EOSolde)createAndInsertInstance(editingContext, _EOSolde.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSolde localInstanceIn(EOEditingContext editingContext, EOSolde eo) {
    EOSolde localInstance = (eo == null) ? null : (EOSolde)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSolde#localInstanceIn a la place.
   */
	public static EOSolde localInstanceOf(EOEditingContext editingContext, EOSolde eo) {
		return EOSolde.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOSolde> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSolde fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSolde fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSolde eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSolde)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSolde fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSolde fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSolde eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSolde)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSolde fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSolde eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSolde ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSolde fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
