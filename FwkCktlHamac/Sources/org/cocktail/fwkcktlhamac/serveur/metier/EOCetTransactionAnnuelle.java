/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Operation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.A_CetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.Epargne;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.Indemnisation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.IndemnisationAncienRegime;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.TransfertAncienRegimeVersRegimePerenne;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.TransfertRafp;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.TransfertRafpAncienRegime;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.CktlSort;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOCetTransactionAnnuelle
		extends _EOCetTransactionAnnuelle
		implements I_GestionAutoDCreationDModification {

	public EOCetTransactionAnnuelle() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String IS_DEMANDE_OU_DECISION_FAITE_SUR_ANCIEN_REGIME_KEY = "isDemandeOuDecisionFaiteSurAncienRegime";

	/**
	 * 
	 * @return
	 */
	public static EOCetTransactionAnnuelle creer(
			EOEditingContext ec, NSTimestamp cetDemDate, String nature, Integer persId, EOPeriode eoPeriode) {
		EOCetTransactionAnnuelle transaction = createEOCetTransactionAnnuelle(
				ec, cetDemDate, DateCtrlHamac.now(), DateCtrlHamac.now(), nature, persId, eoPeriode);

		return transaction;
	}

	private NSTimestamp _dateValeur;

	/**
	 * Date de valeur constituée à partir de la période
	 * 
	 * @return
	 */
	public NSTimestamp dateValeur() {
		if (_dateValeur == null) {
			_dateValeur = DateCtrlHamac.date1erJanAnneeUniv(toPeriode().perDDebut());
		}
		return _dateValeur;
	}

	/**
	 * 
	 */
	public void clearCache() {
		_crediteurArray = null;
		_debiteurArray = null;
		_dateValeur = null;
	}

	private NSMutableArray<I_Operation> _debiteurArray;

	/**
	 * Liste des objets {@link A_CetTransactionAnnuelle} utilisé pour la gestion
	 * des débits / crédits CET sur les soldes
	 * 
	 * @return
	 */
	public NSMutableArray<I_Operation> getDebiteurArray() {
		if (_debiteurArray == null) {

			_debiteurArray = new NSMutableArray<I_Operation>();

			// transfert ancien régime => nouveau
			if (getTransfertAncienRegimeVersRegimePerenne() != null) {
				_debiteurArray.add(getTransfertAncienRegimeVersRegimePerenne());
			}

			// indemnisation
			if (!isAdminDemandeAutreEpargneSaisie() && cetDecIndemPerenne() != null && cetDecIndemPerenne().intValue() > 0) {
				_debiteurArray.add(new Indemnisation(this, cetDecIndemPerenne().intValue()));
			}

			// transfert RAFP
			if (!isAdminDemandeAutreEpargneSaisie() && cetDecRafpPerenne() != null && cetDecRafpPerenne().intValue() > 0) {
				_debiteurArray.add(new TransfertRafp(this, cetDecRafpPerenne().intValue()));
			}

			// indemnisation ancien regime
			if (cetDecIndemAncien() != null && cetDecIndemAncien().intValue() > 0) {
				_debiteurArray.add(new IndemnisationAncienRegime(this, cetDecIndemAncien().intValue()));
			}

			// transfert RAFP
			if (cetDecRafpAncien() != null && cetDecRafpAncien().intValue() > 0) {
				_debiteurArray.add(new TransfertRafpAncienRegime(this, cetDecRafpPerenne().intValue()));
			}

		}
		return _debiteurArray;
	}

	private NSMutableArray<I_Solde> _crediteurArray;

	/**
	 * Liste des objets {@link A_CetTransactionAnnuelle} utilisé pour la gestion
	 * des débits / crédits CET sur les soldes
	 * 
	 * @return
	 */
	public NSMutableArray<I_Solde> getCrediteurArray() {
		if (_crediteurArray == null) {
			_crediteurArray = new NSMutableArray<I_Solde>();

			// transfert ancien régime => nouveau
			if (getTransfertAncienRegimeVersRegimePerenne() != null) {
				_crediteurArray.add(getTransfertAncienRegimeVersRegimePerenne());
			}

			// épargne
			if (getEpargneDecidee() != null) {
				_crediteurArray.add(getEpargneDecidee());
			}

		}
		return _crediteurArray;
	}

	private Epargne _epargneDecidee;

	/**
	 * 
	 * @return
	 */
	public Epargne getEpargneDecidee() {
		if (_epargneDecidee == null) {

			// test préalable pour ne pas tenir compte de l'état batard en cours de
			// reformulation
			if (!isAdminDemandeAutreEpargneSaisie()) {
				// épargne decision
				if (cetDecEpargne() != null && cetDecEpargne().intValue() > 0) {
					_epargneDecidee = new Epargne(this, cetDecEpargne().intValue());
				}
			}
		}
		return _epargneDecidee;
	}

	private Epargne _epargneDemandee;

	/**
	 * 
	 * @return
	 */
	public Epargne getEpargneDemandee() {
		if (_epargneDemandee == null) {

			// demande d'épargne
			if (cetDemEpargne() != null && cetDemEpargne().intValue() > 0) {
				_epargneDemandee = new Epargne(this, cetDemEpargne().intValue());
			}
		}
		return _epargneDemandee;
	}

	private TransfertAncienRegimeVersRegimePerenne _transfertAncienRegimeVersRegimePerenne;

	/**
	 * 
	 * @return
	 */
	public TransfertAncienRegimeVersRegimePerenne getTransfertAncienRegimeVersRegimePerenne() {
		if (_transfertAncienRegimeVersRegimePerenne == null) {
			//
			if (cetDecTransAncienPerenne() != null && cetDecTransAncienPerenne().intValue() > 0) {
				_transfertAncienRegimeVersRegimePerenne = new TransfertAncienRegimeVersRegimePerenne(this, cetDecTransAncienPerenne().intValue());
			}
		}
		return _transfertAncienRegimeVersRegimePerenne;
	}

	/**
	 * @param eoPlanning
	 * @return
	 */
	public static NSArray<EOCetTransactionAnnuelle> getEoCetTransactionArray(
			EOPlanning eoPlanning) {
		NSArray<EOCetTransactionAnnuelle> array = null;

		EOEditingContext ec = eoPlanning.editingContext();

		array = getEoCetTransactionArray(ec, eoPlanning.persId(), eoPlanning.nature());

		// ne conserver que pour cette période
		array = EOQualifier.filteredArrayWithQualifier(
				array,
				ERXQ.equals(TO_PERIODE_KEY, eoPlanning.toPeriode()));

		return array;
	}

	/**
	 * @param eoPlanning
	 * @return
	 */
	public static NSArray<EOCetTransactionAnnuelle> getEoCetTransactionArray(
			EOEditingContext ec, Integer persId, String nature) {
		NSArray<EOCetTransactionAnnuelle> array = null;

		EOQualifier qual = ERXQ.and(
				ERXQ.equals(PERS_ID_KEY, persId),
				ERXQ.equals(NATURE_KEY, nature));

		// garder celles dans le context pas encore sauvées ...
		// rechercher d'abord dans le editingcontext s'il vient tout juste d'etre
		// inséré, mais pas encore en base de données (cas de plusieurs associations
		// sans sauvegardes intermédiaires)
		NSArray<EOEnterpriseObject> enterpriseObjectArray = getObjectsInsertedInContextMatching(
				ec, ENTITY_NAME, qual);

		if (enterpriseObjectArray.count() > 0) {
			array = new NSArray<EOCetTransactionAnnuelle>();
			for (EOEnterpriseObject enterpriseObject : enterpriseObjectArray) {
				array = array.arrayByAddingObject((EOCetTransactionAnnuelle) enterpriseObject);
			}
		} else {

			// en base
			array = fetchAll(ec, qual);

		}

		return array;
	}

	/**
	 * @return
	 */
	public boolean isDemandeOuDecisionFaiteSurAncienRegime() {
		boolean isDemandeOuDecisionFaiteSurAncienRegime = false;

		if (cetDemTransAncienPerenne() != null ||
				cetDecTransAncienPerenne() != null ||
				cetDemIndemAncien() != null ||
				cetDecIndemAncien() != null ||
				cetDemRafpAncien() != null ||
				cetDecRafpAncien() != null ||
				cetDemMaintAncien() != null ||
				cetDecMaintAncien() != null) {
			isDemandeOuDecisionFaiteSurAncienRegime = true;
		}

		return isDemandeOuDecisionFaiteSurAncienRegime;
	}

	/**
	 * Copier à l'identique la transaction vers une autre nature
	 * 
	 * @param nature
	 * @return
	 */
	public EOCetTransactionAnnuelle dupliquerVers(String nature) {
		EOCetTransactionAnnuelle transaction = null;

		// erreur si même nature
		if (nature.equals(nature())) {
			throw new IllegalStateException("Impossible de dupliquer la transaction CET vers une nature identique (" + nature + ")");
		}

		transaction = creer(editingContext(), cetDemDate(), nature, persId(), toPeriode());

		transaction.setCetDemEpargne(cetDemEpargne());
		transaction.setCetDemIndemAncien(cetDemIndemAncien());
		transaction.setCetDemIndemPerenne(cetDemIndemPerenne());
		transaction.setCetDemMaintAncien(cetDemMaintAncien());
		transaction.setCetDemMaintForcePerenne(cetDemMaintForcePerenne());
		transaction.setCetDemMaintPerenne(cetDemMaintPerenne());
		transaction.setCetDemRafpAncien(cetDemRafpAncien());
		transaction.setCetDemRafpPerenne(cetDemRafpPerenne());
		transaction.setCetDemTransAncienPerenne(cetDemTransAncienPerenne());

		transaction.setCetDecEpargne(cetDecEpargne());
		transaction.setCetDecIndemAncien(cetDecIndemAncien());
		transaction.setCetDecIndemPerenne(cetDecIndemPerenne());
		transaction.setCetDecMaintAncien(cetDecMaintAncien());
		transaction.setCetDecMaintForcePerenne(cetDecMaintForcePerenne());
		transaction.setCetDecMaintPerenne(cetDecMaintPerenne());
		transaction.setCetDecRafpAncien(cetDecRafpAncien());
		transaction.setCetDecRafpPerenne(cetDecRafpPerenne());
		transaction.setCetDecTransAncienPerenne(cetDecTransAncienPerenne());

		return transaction;
	}

	// pour interface

	/**
	 * Méthode interne de comparaison
	 * 
	 * @param valeur1
	 * @param valeur2
	 * @return
	 */
	private final static boolean isIdentique(Integer valeur1, Integer valeur2) {
		boolean isIdentique = false;

		if (valeur1 == null && valeur2 == null) {
			isIdentique = true;
		}

		if (!isIdentique) {
			if ((valeur1 == null && valeur2 != null) || (valeur1 != null && valeur2 == null)) {
				isIdentique = false;
			} else {
				if (valeur1.intValue() == valeur2.intValue()) {
					isIdentique = true;
				}
			}
		}

		return isIdentique;
	}

	/**
	 * Indique si la demande et la decision CET sont exactement les mêmes en terme
	 * de valeur
	 * 
	 * @return
	 */
	public boolean isAdminDemandeAccepteeALIdentique() {
		boolean isDemandeAccepteeALIdentique = true;

		// maintien ancien regime
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemMaintAncien(),
						cetDecMaintAncien())) {
			isDemandeAccepteeALIdentique = false;
		}

		// indemnisation ancien regime
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemIndemAncien(),
						cetDecIndemAncien())) {
			isDemandeAccepteeALIdentique = false;
		}

		// transfert RAFP ancien regime
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemRafpAncien(),
						cetDecRafpAncien())) {
			isDemandeAccepteeALIdentique = false;
		}

		// transfert vers régime pérenne
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemTransAncienPerenne(),
						cetDecTransAncienPerenne())) {
			isDemandeAccepteeALIdentique = false;
		}

		// épargne
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemEpargne(),
						cetDecEpargne())) {
			isDemandeAccepteeALIdentique = false;
		}

		// maintien
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemMaintPerenne(),
						cetDecMaintPerenne())) {
			isDemandeAccepteeALIdentique = false;
		}

		// maintien forcé
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemMaintForcePerenne(),
						cetDecMaintForcePerenne())) {
			isDemandeAccepteeALIdentique = false;
		}

		// indemnisation
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemIndemPerenne(),
						cetDecIndemPerenne())) {
			isDemandeAccepteeALIdentique = false;
		}

		// transfert RAFP
		if (isDemandeAccepteeALIdentique &&
				!isIdentique(
						cetDemRafpPerenne(),
						cetDecRafpPerenne())) {
			isDemandeAccepteeALIdentique = false;
		}

		return isDemandeAccepteeALIdentique;
	}

	/**
	 * Indique la decision CET est faite mais pas à l'identique de la demande
	 * 
	 * @return
	 */
	public boolean isAdminDemandeAccepteeDecisionDifferente() {
		boolean isDemandeAccepteeDecisionDifferente = true;

		// maintien ancien regime
		if (isDemandeAccepteeDecisionDifferente &&
				!isIdentique(
						cetDemMaintAncien(),
						cetDecMaintAncien())) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// indemnisation ancien regime
		if (isDemandeAccepteeDecisionDifferente &&
				!isIdentique(
						cetDemIndemAncien(),
						cetDecIndemAncien())) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// transfert RAFP ancien regime
		if (isDemandeAccepteeDecisionDifferente &&
				!isIdentique(
						cetDemRafpAncien(),
						cetDecRafpAncien())) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// transfert vers régime pérenne
		if (isDemandeAccepteeDecisionDifferente &&
				!isIdentique(
						cetDemTransAncienPerenne(),
						cetDecTransAncienPerenne())) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// épargne
		if (isDemandeAccepteeDecisionDifferente && (
				cetDemEpargne() == null ||
				cetDecEpargne() == null)) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// maintien
		if (isDemandeAccepteeDecisionDifferente && (
				cetDemMaintPerenne() == null ||
				cetDecMaintPerenne() == null)) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// maintien forcé
		if (isDemandeAccepteeDecisionDifferente && (
				cetDemMaintForcePerenne() == null ||
				cetDecMaintForcePerenne() == null)) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// indemnisation
		if (isDemandeAccepteeDecisionDifferente && (
				cetDemIndemPerenne() == null ||
				cetDecIndemPerenne() == null)) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// transfert RAFP
		if (isDemandeAccepteeDecisionDifferente && (
				cetDemRafpPerenne() == null ||
				cetDecRafpPerenne() == null)) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		// test de la différence des valeurs
		if (isDemandeAccepteeDecisionDifferente && (
				isIdentique(cetDemEpargne(), cetDecEpargne()) &&
						isIdentique(cetDemMaintPerenne(), cetDecMaintPerenne()) &&
							isIdentique(cetDemMaintForcePerenne(), cetDecMaintForcePerenne()) &&
							isIdentique(cetDemIndemPerenne(), cetDecIndemPerenne()) &&
					isIdentique(cetDemRafpPerenne(), cetDecRafpPerenne()))) {
			isDemandeAccepteeDecisionDifferente = false;
		}

		return isDemandeAccepteeDecisionDifferente;
	}

	/**
	 * Indique si {@link #isAdminDemandeAccepteeALIdentique()} ou
	 * {@link #isAdminDemandeAccepteeDecisionDifferente()}
	 * 
	 * @see PageCET
	 * @return
	 */
	public boolean isAdminDemandeAcceptee() {
		boolean isAdminDemandeAcceptee = false;

		if (isAdminDemandeAccepteeALIdentique() ||
				isAdminDemandeAccepteeDecisionDifferente()) {
			isAdminDemandeAcceptee = true;
		}

		return isAdminDemandeAcceptee;
	}

	/**
	 * Indique s'il faut retourner a l'agent la demande, pour qu'il accepte la
	 * décision qui a été différente (épargne modifiée, et droit d'option aussi)
	 */
	public boolean isAdminDemandeAutreEpargneSaisie() {
		boolean isAdminDemandeAutreEpargneSaisie = false;

		// maintien ancien regime
		if (isIdentique(
				cetDemMaintAncien(),
				cetDecMaintAncien())) {
			isAdminDemandeAutreEpargneSaisie = true;
		}

		// indemnisation ancien regime
		if (isAdminDemandeAutreEpargneSaisie && !isIdentique(
				cetDemIndemAncien(),
				cetDecIndemAncien())) {
			isAdminDemandeAutreEpargneSaisie = true;
		}

		// transfert RAFP ancien regime
		if (isAdminDemandeAutreEpargneSaisie && !isIdentique(
				cetDemRafpAncien(),
				cetDecRafpAncien())) {
			isAdminDemandeAutreEpargneSaisie = true;
		}

		// transfert vers régime pérenne
		if (isAdminDemandeAutreEpargneSaisie && !isIdentique(
				cetDemTransAncienPerenne(),
				cetDecTransAncienPerenne())) {
			isAdminDemandeAutreEpargneSaisie = true;
		}

		// demande épargne + maintien + maintien forcé + indemnisation + rafp
		// doivent exister
		if (isAdminDemandeAutreEpargneSaisie && (
					cetDemEpargne() == null ||
							cetDemMaintForcePerenne() == null ||
							cetDemMaintPerenne() == null ||
							cetDemIndemPerenne() == null ||
					cetDemRafpPerenne() == null)) {
			isAdminDemandeAutreEpargneSaisie = false;
		}

		// décision épargne doit exister
		if (isAdminDemandeAutreEpargneSaisie &&
				cetDecEpargne() == null) {
			isAdminDemandeAutreEpargneSaisie = false;
		}

		// décisions maintien + maintien forcé + indemnisation + rafp ne doivent pas
		// exister
		if (isAdminDemandeAutreEpargneSaisie && (
				cetDecMaintPerenne() != null ||
						cetDecMaintForcePerenne() != null ||
						cetDecIndemPerenne() != null ||
				cetDecRafpPerenne() != null)) {
			isAdminDemandeAutreEpargneSaisie = false;
		}

		return isAdminDemandeAutreEpargneSaisie;
	}

	/**
	 * Indique si on attend le retour de l'agent quant à la proposition différente
	 * d'épargne.
	 */
	public boolean isAdminDemandeNonTraitee() {
		boolean isNonTraitee = true;

		// demande épargne + maintien + maintien forcé + indemnisation + rafp
		// doivent exister
		if (cetDemEpargne() == null ||
				cetDemMaintPerenne() == null ||
				cetDemMaintForcePerenne() == null ||
				cetDemIndemPerenne() == null ||
				cetDemRafpPerenne() == null) {
			isNonTraitee = false;
		}

		// décisions épargne + maintien + maintien forcé + indemnisation + rafp ne
		// doivent pas exister
		if (isNonTraitee && (
				cetDecEpargne() != null ||
						cetDecMaintPerenne() != null ||
						cetDecMaintForcePerenne() != null ||
						cetDecIndemPerenne() != null ||
				cetDecRafpPerenne() != null)) {
			isNonTraitee = false;
		}

		return isNonTraitee;
	}

	/**
	 * La liste des individus ayant un CET
	 * 
	 * @param ec
	 * @param eoPeriode
	 * @return
	 */
	public final static NSArray<EOIndividu> getEoIndividuAyantUnCETArray(
			EOEditingContext ec, EOPeriode eoPeriode) {

		NSArray<EOCetSaisieManuelle> saisieArray = EOCetSaisieManuelle.fetchAll(ec);

		saisieArray = EOQualifier.filteredArrayWithQualifier(saisieArray,
				ERXQ.and(
						ERXQ.greaterThan(EOCetSaisieManuelle.CREDIT_KEY, Integer.valueOf(0)),
						ERXQ.lessThanOrEqualTo(EOCetSaisieManuelle.CSM_DATE_VALEUR_KEY, eoPeriode.perDFin())));

		NSArray<EOCetTransactionAnnuelle> transactionArray = EOCetTransactionAnnuelle.fetchAll(ec);
		transactionArray = EOQualifier.filteredArrayWithQualifier(
				transactionArray,
				ERXQ.lessThanOrEqualTo(EOCetTransactionAnnuelle.TO_PERIODE_KEY + "." + EOPeriode.PER_D_DEBUT_KEY, eoPeriode.perDDebut()));

		// construire un qualifier liste des persId
		NSMutableArray<EOQualifier> qualArray = new NSMutableArray<EOQualifier>();
		for (EOCetSaisieManuelle saisie : saisieArray) {
			qualArray.addObject(
					ERXQ.equals(EOIndividu.PERS_ID_KEY, saisie.persId()));
		}
		for (EOCetTransactionAnnuelle transaction : transactionArray) {
			qualArray.addObject(
					ERXQ.equals(EOIndividu.PERS_ID_KEY, transaction.persId()));
		}

		NSArray<EOIndividu> eoIndividuArray = new NSArray<EOIndividu>();

		// ça ramene tout le monde si qualArray ramene rien
		if (saisieArray.count() > 0 || transactionArray.count() > 0) {
			eoIndividuArray = EOIndividu.fetchAllValides(
					ec, ERXQ.or(qualArray), CktlSort.newSort(EOIndividu.NOM_AFFICHAGE_KEY + "," + EOIndividu.PRENOM_KEY));
		}

		return eoIndividuArray;

	}
	
	/**
	 * 
	 * @param edc editingContext
	 * @param periode periode
	 */
	public static void supprimerParPeriode(EOEditingContext edc, EOPeriode periode) {
		
		NSArray<EOCetTransactionAnnuelle> cetTransactionAnnuelleArray = fetchAll(
				edc, ERXQ.equals(EOCetTransactionAnnuelle.TO_PERIODE_KEY, periode));

		for (EOCetTransactionAnnuelle eoCetTransactionAnnuelle : cetTransactionAnnuelleArray) {
			edc.deleteObject(eoCetTransactionAnnuelle);
		}
		
	}
	
	
}
