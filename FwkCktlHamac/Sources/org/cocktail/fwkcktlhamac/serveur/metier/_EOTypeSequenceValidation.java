/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeSequenceValidation.java instead.
package org.cocktail.fwkcktlhamac.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;

@SuppressWarnings("all")
public abstract class _EOTypeSequenceValidation extends  A_FwkCktlHamacRecord {
	public static final String ENTITY_NAME = "TypeSequenceValidation";
	public static final String ENTITY_TABLE_NAME = "HAMAC.TYPE_SEQUENCE_VALIDATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tsvId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TSV_CODE_KEY = "tsvCode";
	public static final String TSV_LIBELLE_KEY = "tsvLibelle";

// Attributs non visibles
	public static final String TSV_ID_KEY = "tsvId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TSV_CODE_COLKEY = "TSV_CODE";
	public static final String TSV_LIBELLE_COLKEY = "TSV_LIBELLE";

	public static final String TSV_ID_COLKEY = "TSV_ID";


	// Relationships
	public static final String TOS_SEQUENCE_VALIDATION_KEY = "tosSequenceValidation";
	public static final String TOS_TYPE_OCCUPATION_KEY = "tosTypeOccupation";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String tsvCode() {
    return (String) storedValueForKey(TSV_CODE_KEY);
  }

  public void setTsvCode(String value) {
    takeStoredValueForKey(value, TSV_CODE_KEY);
  }

  public String tsvLibelle() {
    return (String) storedValueForKey(TSV_LIBELLE_KEY);
  }

  public void setTsvLibelle(String value) {
    takeStoredValueForKey(value, TSV_LIBELLE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation> tosSequenceValidation() {
    return (NSArray)storedValueForKey(TOS_SEQUENCE_VALIDATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation> tosSequenceValidation(EOQualifier qualifier) {
    return tosSequenceValidation(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation> tosSequenceValidation(EOQualifier qualifier, boolean fetch) {
    return tosSequenceValidation(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation> tosSequenceValidation(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation.TO_TYPE_SEQUENCE_VALIDATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosSequenceValidation();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosSequenceValidationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_SEQUENCE_VALIDATION_KEY);
  }

  public void removeFromTosSequenceValidationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SEQUENCE_VALIDATION_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation createTosSequenceValidationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("SequenceValidation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_SEQUENCE_VALIDATION_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation) eo;
  }

  public void deleteTosSequenceValidationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_SEQUENCE_VALIDATION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosSequenceValidationRelationships() {
    Enumeration objects = tosSequenceValidation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosSequenceValidationRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOSequenceValidation)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation> tosTypeOccupation() {
    return (NSArray)storedValueForKey(TOS_TYPE_OCCUPATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation> tosTypeOccupation(EOQualifier qualifier) {
    return tosTypeOccupation(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation> tosTypeOccupation(EOQualifier qualifier, boolean fetch) {
    return tosTypeOccupation(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation> tosTypeOccupation(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation.TO_TYPE_SEQUENCE_VALIDATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosTypeOccupation();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosTypeOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_TYPE_OCCUPATION_KEY);
  }

  public void removeFromTosTypeOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_TYPE_OCCUPATION_KEY);
  }

  public org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation createTosTypeOccupationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("TypeOccupation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_TYPE_OCCUPATION_KEY);
    return (org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation) eo;
  }

  public void deleteTosTypeOccupationRelationship(org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_TYPE_OCCUPATION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosTypeOccupationRelationships() {
    Enumeration objects = tosTypeOccupation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosTypeOccupationRelationship((org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTypeSequenceValidation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeSequenceValidation createEOTypeSequenceValidation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String tsvCode
, String tsvLibelle
			) {
    EOTypeSequenceValidation eo = (EOTypeSequenceValidation) createAndInsertInstance(editingContext, _EOTypeSequenceValidation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTsvCode(tsvCode);
		eo.setTsvLibelle(tsvLibelle);
    return eo;
  }

  
	  public EOTypeSequenceValidation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeSequenceValidation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeSequenceValidation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeSequenceValidation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeSequenceValidation object = (EOTypeSequenceValidation)createAndInsertInstance(editingContext, _EOTypeSequenceValidation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeSequenceValidation localInstanceIn(EOEditingContext editingContext, EOTypeSequenceValidation eo) {
    EOTypeSequenceValidation localInstance = (eo == null) ? null : (EOTypeSequenceValidation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeSequenceValidation#localInstanceIn a la place.
   */
	public static EOTypeSequenceValidation localInstanceOf(EOEditingContext editingContext, EOTypeSequenceValidation eo) {
		return EOTypeSequenceValidation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<EOTypeSequenceValidation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeSequenceValidation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeSequenceValidation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeSequenceValidation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeSequenceValidation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeSequenceValidation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeSequenceValidation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeSequenceValidation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeSequenceValidation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeSequenceValidation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeSequenceValidation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeSequenceValidation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeSequenceValidation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
