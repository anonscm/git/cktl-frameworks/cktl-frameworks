/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlhamac.serveur.metier;

import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOTypeSolde extends _EOTypeSolde {

	public EOTypeSolde() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de
	 * cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate
	 * qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez
	 * définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	// ajouts

	public final static String CONGE_NATIF = "CONGES___";
	private final static String CONGE_MANUEL = "CNG_MANU_";
	public final static String RELIQUAT_NATIF = "REL_STAT_";
	public final static String DECH_SYND = "DECH_SYND";
	private final static String HSUP_MANUEL = "HSUP_STAT";
	private final static String RELIQUAT_MANUEL = "REL_DYN__";
	public final static String RELIQUAT_HSUP = "HSUP_DYN_";
	public final static String BALANCE = "BALANCE__";
	public final static String REGULARISATION = "CNG_SUPP_";
	public final static String CONGE_LEGAL = "CNG_LEGAL";
	public final static String CONGE_RH = "CNG_RH___";
	public final static String JRTI = "JRTI_____";
	public final static String INDEM_CET_2008_1136 = "2008-1136";
	public final static String RECUPERATION = "RECUPERATION";
	private final static String HSUP_NON_VALIDE = "HSUP_NVAL";
	public final static String CET = "CET______";

	// les types attendus automatiquement pour un planning

	// ceux qui dépendent d'un service
	public final static NSArray<String> SOLDE_NATIF_PAR_SERVICE_ARRAY = new NSArray<String>(
					new String[] { CONGE_NATIF, RELIQUAT_NATIF, BALANCE, HSUP_NON_VALIDE, RELIQUAT_HSUP });

	// ceux qui ne dépendent pas d'un service
	public final static NSArray<String> SOLDE_NATIF_SANS_SERVICE_ARRAY = new NSArray<String>(
				new String[] { CONGE_LEGAL, CONGE_RH, DECH_SYND });

	// les autres types non obligatoires ou à validité variable, dépendant d'un
	// service
	public final static NSArray<String> TYPES_MANU_PAR_SERVICE_ARRAY = new NSArray<String>(
			new String[] { RELIQUAT_MANUEL, REGULARISATION });

	// les autres types non obligatoires ou à validité variable, ne dépendant pas
	// d'un service
	public final static NSArray<String> TYPES_MANU_SANS_SERVICE_ARRAY = new NSArray<String>(
			new String[] { DECH_SYND });

	// TODO revoir ce système de cache ...

	public static EOTypeSolde getTypeSoldeByCode(EOEditingContext ec, String type) {
		EOTypeSolde eoSoldeType = null;

		eoSoldeType = EOTypeSolde.fetchRequiredByKeyValue(ec, STYP_CODE_KEY, type);

		return eoSoldeType;
	}

	public final static String STR_QUAL_IS_CONGE_NATIF = STYP_CODE_KEY + "='" + CONGE_NATIF + "'";
	public final static String STR_QUAL_IS_RELIQUAT_NATIF = STYP_CODE_KEY + "='" + RELIQUAT_NATIF + "'";

	// a ne pas utiliser pour des fetch en base de données

	public boolean isCongeNatif() {
		boolean isTypeCongeNatif = false;

		if (stypCode().equals(CONGE_NATIF)) {
			isTypeCongeNatif = true;
		}

		return isTypeCongeNatif;
	}

	public boolean isReliquatNatif() {
		boolean isTypeReliquatNatif = false;

		if (stypCode().equals(RELIQUAT_NATIF)) {
			isTypeReliquatNatif = true;
		}

		return isTypeReliquatNatif;
	}

	public boolean isReliquatManuel() {
		boolean isTypeReliquatManuel = false;

		if (stypCode().equals(RELIQUAT_MANUEL)) {
			isTypeReliquatManuel = true;
		}

		return isTypeReliquatManuel;
	}

	public boolean isCongeManuel() {
		boolean isTypeCongeManuel = false;

		if (stypCode().equals(CONGE_MANUEL)) {
			isTypeCongeManuel = true;
		}

		return isTypeCongeManuel;
	}

	public boolean isCongeLegal() {
		boolean isCongeLegal = false;

		if (stypCode().equals(CONGE_LEGAL)) {
			isCongeLegal = true;
		}

		return isCongeLegal;
	}

	public boolean isCongeRh() {
		boolean isCongeRh = false;

		if (stypCode().equals(CONGE_RH)) {
			isCongeRh = true;
		}

		return isCongeRh;
	}

	public boolean isDechargeSyndicale() {
		boolean isDechargeSyndicale = false;

		if (stypCode().equals(DECH_SYND)) {
			isDechargeSyndicale = true;
		}

		return isDechargeSyndicale;
	}

	public boolean isJrti() {
		boolean isJrti = false;

		if (stypCode().equals(JRTI)) {
			isJrti = true;
		}

		return isJrti;
	}

	public boolean isIndemCet20081136() {
		boolean isIndemCet20081136 = false;

		if (stypCode().equals(INDEM_CET_2008_1136)) {
			isIndemCet20081136 = true;
		}

		return isIndemCet20081136;
	}

	public boolean isBalance() {
		boolean isBalance = false;

		if (stypCode().equals(BALANCE)) {
			isBalance = true;
		}

		return isBalance;
	}

	public boolean isRegularisation() {
		boolean isRegularisation = false;

		if (stypCode().equals(REGULARISATION)) {
			isRegularisation = true;
		}

		return isRegularisation;
	}

	public boolean isRecuperation() {
		boolean isRecuperation = false;

		if (stypCode().equals(RECUPERATION)) {
			isRecuperation = true;
		}

		return isRecuperation;
	}

	public boolean isHeureSupplementaireManuel() {
		boolean isHeureSupplementaireManuel = false;

		if (stypCode().equals(HSUP_MANUEL)) {
			isHeureSupplementaireManuel = true;
		}

		return isHeureSupplementaireManuel;
	}

	public boolean isHeureSupplementaireNonValide() {
		boolean isHeureSupplementaireNonValide = false;

		if (stypCode().equals(HSUP_NON_VALIDE)) {
			isHeureSupplementaireNonValide = true;
		}

		return isHeureSupplementaireNonValide;

	}

	public boolean isCet() {
		boolean isCet = false;

		if (stypCode().equals(CET)) {
			isCet = true;
		}

		return isCet;

	}
	
	public boolean isReliquatHsup() {
		boolean isReliquatHsup = false;

		if (stypCode().equals(RELIQUAT_HSUP)) {
			isReliquatHsup = true;
		}

		return isReliquatHsup;
	}
	

	public boolean isServiceObligatoire() {
		boolean isServiceObligatoire = false;

		if (SOLDE_NATIF_PAR_SERVICE_ARRAY.containsObject(stypCode()) ||
				TYPES_MANU_PAR_SERVICE_ARRAY.containsObject(stypCode())) {
			isServiceObligatoire = true;
		}

		return isServiceObligatoire;
	}

	public boolean isServiceInterdit() {
		boolean isServiceInterdit = false;

		if (SOLDE_NATIF_SANS_SERVICE_ARRAY.containsObject(stypCode()) ||
				TYPES_MANU_SANS_SERVICE_ARRAY.containsObject(stypCode())) {
			isServiceInterdit = true;
		}

		return isServiceInterdit;
	}

	/**
	 * *
	 * 
	 * @return
	 */
	public boolean isTypeNatif() {
		boolean isTypeNatif = false;

		if (SOLDE_NATIF_PAR_SERVICE_ARRAY.contains(stypCode()) ||
				SOLDE_NATIF_SANS_SERVICE_ARRAY.contains(stypCode())) {
			isTypeNatif = true;
		}

		return isTypeNatif;
	}

	/**
	 * La qualifier permettant de filtrer sur des objets de type {@link I_Solde}.
	 * On tape sur les méthodes booleenes de l'interface {@link I_Solde} plutôt
	 * que par {@link EOSolde#toTypeSolde()} car les {@link I_Solde} ne sont pas
	 * forcément des {@link EOSolde}
	 * 
	 * @return
	 */
	public EOQualifier getQualifierPourISolde() {
		EOQualifier qual = null;

		String key = null;

		if (isCongeNatif()) {
			key = I_Solde.IS_CONGE_NATIF_KEY;
		} else if (isReliquatNatif()) {
			key = I_Solde.IS_RELIQUAT_NATIF_KEY;
		} else if (isReliquatManuel()) {
			key = I_Solde.IS_RELIQUAT_MANUEL_KEY;
		} else if (isCongeManuel()) {
			key = I_Solde.IS_CONGE_MANUEL_KEY;
		} else if (isCongeLegal()) {
			key = I_Solde.IS_CONGE_LEGAL_KEY;
		} else if (isCongeRh()) {
			key = I_Solde.IS_CONGE_RH_KEY;
		} else if (isDechargeSyndicale()) {
			key = I_Solde.IS_DECHARGE_SYNDICALE_KEY;
		} else if (isJrti()) {
			key = I_Solde.IS_JRTI_KEY;
		} else if (isIndemCet20081136()) {
			key = I_Solde.IS_INDEM_CET_2008_1136_KEY;
		} else if (isBalance()) {
			key = I_Solde.IS_BALANCE_KEY;
		} else if (isRegularisation()) {
			key = I_Solde.IS_REGULARISATION_KEY;
		} else if (isRecuperation()) {
			key = I_Solde.IS_RECUPERATION_KEY;
		} else if (isHeureSupplementaireManuel()) {
			key = I_Solde.IS_HEURE_SUPPLEMENTAIRE_MANUEL_KEY;
		} else if (isHeureSupplementaireNonValide()) {
			key = I_Solde.IS_HEURE_SUPPLEMENTAIRE_NON_VALIDE_KEY;
		} else if (isCet()) {
			key = I_Solde.IS_CET_KEY;
		}

		// exception si null
		if (key == null) {
			throw new IllegalStateException("Impossible de trouver le type associé à " + this);
		}

		qual = ERXQ.isTrue(key);

		return qual;
	}
}
