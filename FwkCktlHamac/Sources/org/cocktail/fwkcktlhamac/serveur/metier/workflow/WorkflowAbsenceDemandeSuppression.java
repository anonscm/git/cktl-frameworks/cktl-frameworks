package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation;

/**
 * Gestion du workflow de validation d'une occupation
 * 
 * @author ctarade
 */
public class WorkflowAbsenceDemandeSuppression
		extends A_WorkflowAbsence {

	private EOTypeStatut eoStatutEnCoursDeSuppression;
	private EOTypeStatut eoStatutSupprime;
	private EOOccupation eoOccupationASupprimer;
	private EOPlanning eoPlanning;

	/**
	 * @param user
	 * @param ec
	 */
	public WorkflowAbsenceDemandeSuppression(
			HamacApplicationUser user, CktlWebSession session, A_Absence absence, EOPlanning eoPlanning) {
		super(user, session, absence);
		this.eoPlanning = eoPlanning;
	}

	/**
	 * 
	 */
	public EOOccupation demanderSuppressionOccupation()
			throws NSValidation.ValidationException {

		// faudra-t-il la répercuter sur le prévisionnel
		boolean isARepercuterSurPrevisionnel = false;

		// si c'est un planning qui n'a jamais été validé (prévisionnel donc)
		// alors on reporte sur le prévisionnel
		if (eoPlanning != null &&
				eoPlanning.isReel() &&
				eoPlanning.isOccupationARepercuterSurPrevisionnel()) {
			isARepercuterSurPrevisionnel = true;
		}

		// XXX elargir aux autres types
		eoOccupationASupprimer = (EOOccupation) getAbsence();

		if (isARepercuterSurPrevisionnel) {
			eoOccupationASupprimer.repercuter(
					eoPlanning.getEoPlanningPrev(), true, getUser(), false);
		}

		// TODO contrôler si user autorisé à faire une saisie
		/*
		 * if (hasDroitPourAction("saisir", null) == false) { throw new
		 * NSValidation.ValidationException("action interdite !"); }
		 */

		if (eoOccupationASupprimer.toTypeStatut().isStatutEnCoursDeValidation() ||
				eoOccupationASupprimer.toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {

			// suppression automatique pour les absences en cours de validation
			eoOccupationASupprimer.setToTypeStatutRelationship(getEoTypeStatutSupprime());

		} else {

			eoOccupationASupprimer.setToTypeStatutRelationship(getEoTypeStatutEnCoursDeSuppression());

			// historiser les éventuels visas existants
			for (EOVisa eoVisa : eoOccupationASupprimer.tosVisa()) {
				eoVisa.setDHistorisation(DateCtrlHamac.now());
			}

		}

		// TODO test de validité ?
		eoOccupationASupprimer.validateObjectMetier();

		// TODO propager sur le(s) planning(s) impacté(s) et le(s) recharger

		return eoOccupationASupprimer;
	}

	/**
	 * @return
	 */
	private final EOTypeStatut getEoTypeStatutEnCoursDeSuppression() {
		if (eoStatutEnCoursDeSuppression == null) {
			//
			eoStatutEnCoursDeSuppression = EOTypeStatut.getEoTypeStatutEnCoursDeSuppression(ec());
		}
		return eoStatutEnCoursDeSuppression;
	}

	/**
	 * @return
	 */
	private final EOTypeStatut getEoTypeStatutSupprime() {
		if (eoStatutSupprime == null) {
			//
			eoStatutSupprime = EOTypeStatut.getEoTypeStatutSupprime(ec());
		}
		return eoStatutSupprime;
	}

	@Override
	public void envoyerNotification() {

		// l'envoi du mail ne se fait que si l'état final n'est pas "supprimée"
		if (!eoOccupationASupprimer.toTypeStatut().isStatutSupprime()) {
			eoOccupationASupprimer.getDemandeDelegate().envoyerMail(getSession(), null);
		}
	}

}
