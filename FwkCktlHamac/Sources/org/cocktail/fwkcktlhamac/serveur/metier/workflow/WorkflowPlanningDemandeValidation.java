/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.EOVisa;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class WorkflowPlanningDemandeValidation
		extends A_WorkflowPlanningDemandeChangementStatut {

	private final static String TITRE = "Demande de validation de planning";
	private final static String MESSAGE = "Vos responsables ont été notifiés de votre demande de validation.";

	/**
	 * @param user
	 * @param ec
	 * @param planning
	 */
	public WorkflowPlanningDemandeValidation(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	@Override
	protected boolean doDemande() throws ValidationException {
		boolean result = true;

		EOPlanning eoPlanning = getPlanning().getEoPlanning();

		// passer le statut en cours de validation
		eoPlanning.setToTypeStatutRelationship(
				EOTypeStatut.getEoTypeStatutEnCoursDeValidationPrevisionnelle(ec()));

		// historiser les éventuels visas existants
		for (EOVisa eoVisa : eoPlanning.tosVisa()) {
			eoVisa.setDHistorisation(DateCtrlHamac.now());
		}

		eoPlanning.getDemandeDelegate().envoyerMail(getSession(), null);

		// tracer delegation
		if (getUser().getPersId().intValue() != eoPlanning.persId().intValue()) {
			eoPlanning.addHistoriqueDelegation(getUser().getPersId());
		}

		return result;
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

}
