/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class WorkflowPlanningDemandeModification
		extends A_WorkflowPlanningDemandeChangementStatut {

	private final static String TITRE = "Demande de modification de planning";
	private final static String MESSAGE = "Vous pouvez maintenant modifier vos associations horaires semaines, puis demander la validation de votre planning";

	/**
	 * @param user
	 * @param ec
	 * @param planning
	 */
	public WorkflowPlanningDemandeModification(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	@Override
	protected boolean doDemande() throws ValidationException {
		boolean result = true;

		getPlanning().getEoPlanning().setToTypeStatutRelationship(
				EOTypeStatut.getEoTypeStatutEnCoursDeModification(ec()));

		return result;
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

}
