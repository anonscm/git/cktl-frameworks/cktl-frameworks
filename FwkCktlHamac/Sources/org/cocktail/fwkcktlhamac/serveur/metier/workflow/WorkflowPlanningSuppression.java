package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

public class WorkflowPlanningSuppression 
	extends A_WorkflowPlanningDemandeChangementStatut {

	
	/**
	 * @param user
	 * @param session
	 * @param planning
	 */
	public WorkflowPlanningSuppression(HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}
	
	private final static String TITRE = "Suppression d'un planning";
	private final static String MESSAGE = "La suppression du planning a bien été effectuée";
	
	@Override
	protected boolean doDemande() throws ValidationException {
		boolean isSuppressionOK = false;
		
		isSuppressionOK = EOPlanning.supprimer(ec(), 
				getPlanning().getEoPlanning().persId(),
				getPlanning().getEoPlanning().temPersonneComposante(),
				getPlanning().getEoPlanning().toPeriode());
		
		return isSuppressionOK;
		
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

	
	
}
