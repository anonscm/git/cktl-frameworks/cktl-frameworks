package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Gestion du workflow de validation d'une occupation
 * 
 * @author ctarade
 */
public class WorkflowAbsenceModification
		extends A_WorkflowAbsence {

	private Planning planning;

	// mémoriser pour envoyer la notification à la sauvegarde
	private WorkflowAbsenceSaisie wfSaisie;
	private EOOccupation eoOccupationModifiee;

	/**
	 * @param user
	 * @param ec
	 */
	public WorkflowAbsenceModification(
			HamacApplicationUser user, CktlWebSession session, EOOccupation eoOccupation, Planning planning) {
		super(user, session, eoOccupation);
		this.planning = planning;
	}

	/**
	 * Modification : suppression puis resaisie
	 */
	public EOOccupation modificationOccupation(
			NSTimestamp newDateDebut,
			NSTimestamp newDateFin,
			String newMotif) throws NSValidation.ValidationException {
		EOOccupation eoOccupation = (EOOccupation) getAbsence();

		EOTypeOccupation eoTypeOccupation = eoOccupation.toTypeOccupation();
		EOStructure eoStructureApplication = eoOccupation.toStructureApplication();

		// XXX mettre l'absence en attente de validation de modification
		WorkflowAbsenceSuppression wfSuppression = new WorkflowAbsenceSuppression(
				getUser(), getSession(), getAbsence(), getPlanning());
		wfSuppression.supprimerOccupation();

		// il faut recharger le planning pour que les objets Jour se reconstruisent
		getPlanning().clearCache();
		getPlanning().charger();

		wfSaisie = new WorkflowAbsenceSaisie(
				getUser(), getSession(), getPlanning().getEoPlanning(), null, null, null);
		eoOccupationModifiee = wfSaisie.saisieOccupation(
				newDateDebut, newDateFin, newMotif, eoTypeOccupation, eoStructureApplication, false);

		// TODO propager sur le(s) planning(s) impacté(s) et le(s) recharger

		return eoOccupation;
	}

	/**
	 * @return
	 */
	public Planning getPlanning() {
		return planning;
	}

	@Override
	public void envoyerNotification() {
		wfSaisie.envoyerNotification();
	}

}
