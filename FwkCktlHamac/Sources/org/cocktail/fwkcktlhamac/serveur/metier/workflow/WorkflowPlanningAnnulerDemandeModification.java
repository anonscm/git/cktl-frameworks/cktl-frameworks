/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Gestion de la demande de validation d'un planning
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class WorkflowPlanningAnnulerDemandeModification
		extends A_WorkflowPlanningDemandeChangementStatut {

	private final static String TITRE = "Annulation de demande de validation de planning";
	private final static String MESSAGE = "Votre annulation a bien été prise en compte";

	/**
	 * @param user
	 * @param ec
	 * @param planning
	 */
	public WorkflowPlanningAnnulerDemandeModification(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	@Override
	protected boolean doDemande() throws ValidationException {
		boolean result = true;

		EOTypeStatut eoTypeStatutSuivant = EOTypeStatut.getEoTypeStatutValide(ec());

		// l'annulation de demande de modification passe le planning au statut
		// valide
		getPlanning().getEoPlanning().setToTypeStatutRelationship(eoTypeStatutSuivant);

		return result;
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

}
