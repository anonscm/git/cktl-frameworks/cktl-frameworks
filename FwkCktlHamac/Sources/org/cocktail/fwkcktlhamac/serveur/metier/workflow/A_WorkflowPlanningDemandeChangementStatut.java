/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import java.lang.reflect.InvocationTargetException;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation;

/**
 * Gestion de toutes les demandes qui vont impacter le statut du planning
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_WorkflowPlanningDemandeChangementStatut
		extends A_WorkflowPlanning {

	/**
	 * @param user
	 * @param ec
	 * @param planning
	 */
	public A_WorkflowPlanningDemandeChangementStatut(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	/**
	 * L'action associée
	 * 
	 * @return
	 * @throws NSValidation.ValidationException
	 */
	protected abstract boolean doDemande() throws NSValidation.ValidationException;

	/**
	 * Le titre du message d'information après éxécution du WF
	 * 
	 * @return
	 */
	public abstract String titreWorkflow();

	/**
	 * Le contenu du message d'information après éxécution du WF
	 * 
	 * @return
	 */
	public abstract String messageWorkFlow();

	/**
	 * La méthode à lancer pour éxécuter le workflow
	 * 
	 * @throws Exception
	 */
	public void execWorkflow() throws Exception {
		if (doDemande()) {

			Exception exception = null;

			try {
				ec().lock();
				ec().saveChanges();

			} catch (NSValidation.ValidationException eve) {
				ec().revert();
				exception = eve;
			} catch (Exception e) {
				ec().revert();
				exception = e;
			} finally {
				ec().unlock();
			}

			// remonter l'exception si elle existe
			if (exception != null) {
				throw exception;
			}

		}
	}

	/**
	 * retourne une instance du constructeur de la classe de gestion d'un type
	 * d'absence
	 * 
	 * @param absence
	 * @return
	 */
	public static A_WorkflowPlanningDemandeChangementStatut spawnWorkflowInstance(
			Class<?> theClass, HamacApplicationUser user, CktlWebSession session, Planning planning) {
		try {
			Class<?> argumentTypes[] = { HamacApplicationUser.class, CktlWebSession.class, Planning.class };
			Object arguments[] = { user, session, planning };
			return (A_WorkflowPlanningDemandeChangementStatut) theClass.getConstructor(argumentTypes).newInstance(arguments);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		return null;
	}

}
