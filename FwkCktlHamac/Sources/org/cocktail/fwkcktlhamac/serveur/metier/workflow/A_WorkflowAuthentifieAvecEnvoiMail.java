/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Workflow nécéssitant la connexion d'un utilisateur
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_WorkflowAuthentifieAvecEnvoiMail
		implements I_Workflow {

	protected final static String TITRE_MAIL_SAISIE_OCCUPATION = "Saisie d'une nouvelle occupation";
	protected final static String TITRE_MAIL_SUPPRESSION_OCCUPATION = "Suppression d'une occupation";
	protected final static String TITRE_MAIL_MODIFICATION_OCCUPATION = "Modification d'une occupation";

	private HamacApplicationUser user;
	private CktlWebSession session;
	private String errorMessage;

	public A_WorkflowAuthentifieAvecEnvoiMail(HamacApplicationUser user, CktlWebSession session) {
		super();
		this.user = user;
		this.session = session;
		errorMessage = "";
	}

	public final HamacApplicationUser getUser() {
		return user;
	}

	public final CktlWebSession getSession() {
		return session;
	}

	public final EOEditingContext ec() {
		return session.defaultEditingContext();
	}

	public final String getErrorMessage() {
		return errorMessage;
	}

	public final void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
