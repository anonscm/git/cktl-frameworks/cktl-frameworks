package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOAssociationHoraire;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Semaine;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.TravailAttendu;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.horaire.Horaire;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/**
 * Gestion du workflow de validation d'une occupation
 * 
 * @author ctarade
 */
public class WorkflowPlanningAssociationHoraire
		extends A_WorkflowPlanning {

	/**
	 * @param user
	 * @param ec
	 */
	public WorkflowPlanningAssociationHoraire(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	/**
	 * Associer l'horaire au(s) semaine(s). Si pas d'horaire associé, alors on
	 * associe aussi dans le planning prévisionnel
	 * 
	 */
	public void associerHoraire(
			NSArray<Semaine> semaineArray, Horaire horaire)
				throws NSValidation.ValidationException {

		if (semaineArray.count() > 0) {

			NSArray<String> codeSemaineArray = (NSArray<String>) semaineArray.valueForKey(Semaine.CODE_KEY);
			codeSemaineArray = NSArrayCtrl.removeDuplicate(codeSemaineArray);

			NSMutableArray<String> codeTraiteArray = new NSMutableArray<String>();

			// TODO interdire la modif pour certains cas
			EOPlanning eoPlanning = getPlanning().getEoPlanningAssoHoraire();

			//
			NSMutableArray<EOQualifier> qualArray = new NSMutableArray<EOQualifier>();
			for (String codeSemaine : codeSemaineArray) {
				qualArray.addObject(ERXQ.equals(EOAssociationHoraire.SEMAINE_CODE_KEY, codeSemaine));
			}

			EOQualifier qual = ERXQ.and(
					ERXQ.equals(EOAssociationHoraire.TO_PLANNING_KEY, eoPlanning),
					new EOOrQualifier(qualArray));

			NSArray<EOAssociationHoraire> associationArray = eoPlanning.tosAssociationHoraire(qual);

			// mise à jour pour les enregistrements déjà existants
			for (EOAssociationHoraire asso : associationArray) {
				EOAssociationHoraire.associer(asso, horaire.getEoHoraire(), null, null);
				codeTraiteArray.addObject(asso.semaineCode());
			}

			// création des enregistrements absents
			NSArray<TravailAttendu> attenduArray = getPlanning().getTravailAttenduArray();

			NSArray<String> codeArray = Semaine.getSemaineCodeForTravailAttenduArray(getPlanning(), attenduArray);

			//
			NSArray<String> codeATraiterArray = codeSemaineArray;

			for (String code : codeArray) {

				if (codeATraiterArray.containsObject(code) &&
						!codeTraiteArray.contains(code)) {

					EOAssociationHoraire.associer(
							null, horaire.getEoHoraire(), eoPlanning, code);

				}

			}

		}

	}
}
