package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation;

/**
 * @deprecated
 * 
 * @see WorkflowAbsenceDemandeSuppression
 * 
 *      Gestion du workflow de validation d'une occupation
 * 
 * @author ctarade
 */
public class WorkflowAbsenceSuppression
		extends A_WorkflowAbsence {

	private Planning planning;

	private EOTypeStatut eoStatutEnCoursDeSuppression;
	private EOTypeStatut eoStatutSupprime;

	/**
	 * @param user
	 * @param ec
	 */
	public WorkflowAbsenceSuppression(
			HamacApplicationUser user, CktlWebSession session, A_Absence absence, Planning planning) {
		super(user, session, absence);
		this.planning = planning;
	}

	/**
	 * 
	 */
	public EOOccupation supprimerOccupation()
			throws NSValidation.ValidationException {

		// faudra-t-il la répercuter sur le prévisionnel
		boolean isARepercuterSurPrevisionnel = false;

		// si c'est un planning qui n'a jamais été validé (prévisionnel donc)
		// alors on reporte sur le prévisionnel
		if (planning != null &&
				planning.getEoPlanning().isReel() &&
				planning.getEoPlanning().isOccupationARepercuterSurPrevisionnel()) {
			isARepercuterSurPrevisionnel = true;
		}

		// XXX elargir aux autres types
		EOOccupation eoOccupation = (EOOccupation) getAbsence();
		if (eoOccupation.toTypeStatut().isStatutEnCoursDeValidation() ||
				eoOccupation.toTypeStatut().isStatutEnCoursDeValidationPrevisionnelle()) {
			// suppression automatique pour les absences en cours de validation
			eoOccupation.setToTypeStatutRelationship(getEoTypeStatutSupprime());
		} else {
			eoOccupation.setToTypeStatutRelationship(getEoTypeStatutEnCoursDeSuppression());
		}

		// tracer delegation
		if (getUser().getPersId().intValue() != eoOccupation.persId().intValue()) {
			eoOccupation.addHistoriqueDelegation(getUser().getPersId());
		}

		if (isARepercuterSurPrevisionnel) {
			eoOccupation.repercuter(
					planning.getEoPlanning().getEoPlanningPrev(), true, getUser(), false);
		}

		// TODO propager sur le(s) planning(s) impacté(s) et le(s) recharger

		return eoOccupation;
	}

	/**
	 * @return
	 */
	private final EOTypeStatut getEoTypeStatutEnCoursDeSuppression() {
		if (eoStatutEnCoursDeSuppression == null) {
			//
			eoStatutEnCoursDeSuppression = EOTypeStatut.getEoTypeStatutEnCoursDeSuppression(ec());
		}
		return eoStatutEnCoursDeSuppression;
	}

	/**
	 * @return
	 */
	private final EOTypeStatut getEoTypeStatutSupprime() {
		if (eoStatutSupprime == null) {
			//
			eoStatutSupprime = EOTypeStatut.getEoTypeStatutSupprime(ec());
		}
		return eoStatutSupprime;
	}

	@Override
	public void envoyerNotification() {

	}

}
