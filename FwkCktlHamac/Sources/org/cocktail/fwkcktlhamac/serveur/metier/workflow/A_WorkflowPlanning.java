/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

/**
 * Workflow dédié à la gestion d'un planning
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_WorkflowPlanning
		extends A_WorkflowAuthentifieAvecEnvoiMail {

	private Planning planning;

	/**
	 * @param user
	 * @param ec
	 */
	public A_WorkflowPlanning(HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session);
		this.planning = planning;
	}

	/**
	 * @return
	 */
	public final Planning getPlanning() {
		return planning;
	}

}
