/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 * 
 */
public class WorkflowPlanningRAZ
		extends A_WorkflowPlanningDemandeChangementStatut {

	/**
	 * @param user
	 * @param session
	 * @param planning
	 */
	public WorkflowPlanningRAZ(HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	private final static String TITRE = "Remise à zéro d'un planning";
	private final static String MESSAGE = "La remise à zéro du planning a bien été effectuée";

	@Override
	protected boolean doDemande() throws ValidationException {
		boolean isRazOK = false;

		isRazOK = EOPlanning.remettreAZero(
				ec(),
				getPlanning().getEoPlanning().persId(),
				getPlanning().getEoPlanning().temPersonneComposante(),
				getPlanning().getEoPlanning().toPeriode());

		return isRazOK;
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

}
