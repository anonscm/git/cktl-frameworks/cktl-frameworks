/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class WorkflowPlanningAnnulerDemandeValidation
		extends A_WorkflowPlanningDemandeChangementStatut {

	private final static String TITRE = "Annulation de modification de planning";
	private final static String MESSAGE = "Votre annulation a bien été prise en compte";

	/**
	 * @param user
	 * @param ec
	 * @param planning
	 */
	public WorkflowPlanningAnnulerDemandeValidation(
			HamacApplicationUser user, CktlWebSession session, Planning planning) {
		super(user, session, planning);
	}

	@Override
	protected boolean doDemande() throws ValidationException {
		boolean result = true;

		// XXX gestion du rollback sur une annulation de demande de validation :
		// plusieurs états possibles (non valide, en cours de modification ...)

		EOTypeStatut eoTypeStatutSuivant = EOTypeStatut.getEoTypeStatutNonValide(ec());

		EOPlanning eoPlanning = getPlanning().getEoPlanning();

		eoPlanning.setToTypeStatutRelationship(eoTypeStatutSuivant);

		// eoPlanning.getDemandeDelegate().envoyerMail(getSession(), null);

		return result;
	}

	@Override
	public String titreWorkflow() {
		return TITRE;
	}

	@Override
	public String messageWorkFlow() {
		return MESSAGE;
	}

}
