package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeOccupation;
import org.cocktail.fwkcktlhamac.serveur.metier.EOTypeStatut;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Gestion du workflow de validation d'une occupation
 * 
 * @author ctarade
 */
public class WorkflowAbsenceSaisie
		extends A_WorkflowAbsence {

	private EOPlanning eoPlanning;
	private Integer persIdSiEoPlanningVide;
	private String temPersonneComposanteSiEoPlanningVide;
	private String natureSiPlanningVide;

	// mémoriser pour l'envoi de notification
	private EOOccupation newEoOccupation;

	/**
	 * 
	 * @param user
	 * @param session
	 * @param planning
	 * @param persIdSiEoPlanningVide
	 * @param temPersonneComposanteSiEoPlanningVide
	 * @param natureSiPlanningVide
	 */
	public WorkflowAbsenceSaisie(
			HamacApplicationUser user,
			CktlWebSession session,
			EOPlanning eoPlanning,
			Integer persIdSiEoPlanningVide,
			String temPersonneComposanteSiEoPlanningVide,
			String natureSiPlanningVide) {
		// l'absence est volontairement à null car c'est une saisie
		super(user, session, null);
		this.eoPlanning = eoPlanning;
		this.persIdSiEoPlanningVide = persIdSiEoPlanningVide;
		this.temPersonneComposanteSiEoPlanningVide = temPersonneComposanteSiEoPlanningVide;
		this.natureSiPlanningVide = natureSiPlanningVide;
	}

	private IPersonne getPersonne() {
		if (eoPlanning != null) {
			return eoPlanning.toPersonne();
		} else {
			IPersonne personne = EOIndividu.individuWithPersId(getSession().defaultEditingContext(), getPersId());
			if (personne == null) {
				personne = EOStructure.structureWithPersId(getSession().defaultEditingContext(), getPersId());
			}
			return personne;
		}
	}

	private Integer getPersId() {
		if (eoPlanning != null) {
			return eoPlanning.persId();
		} else {
			return persIdSiEoPlanningVide;
		}
	}

	private String getTemPersonneComposante() {
		if (eoPlanning != null) {
			return eoPlanning.temPersonneComposante();
		} else {
			return temPersonneComposanteSiEoPlanningVide;
		}
	}

	private String getNature() {
		if (eoPlanning != null) {
			return eoPlanning.nature();
		} else {
			return natureSiPlanningVide;
		}
	}

	/**
	 * @param isEnvoyerNotification
	 *          TODO
	 * @param isImmediatementValide
	 *          TODO
	 * 
	 */
	public EOOccupation saisieOccupation(
			NSTimestamp dateDebut,
			NSTimestamp dateFin,
			String motif,
			EOTypeOccupation eoTypeOccupation,
			EOStructure eoStructureApplication,
			boolean isImmediatementValide) throws NSValidation.ValidationException {
		newEoOccupation = null;

		// faudrat-il la répercuter sur le prévisionnel
		boolean isARepercuterSurPrevisionnel = false;
		EOTypeStatut eoTypeStatut = null;

		// si c'est un planning qui n'a jamais été validé (prévisionnel donc)
		// alors on reporte sur le prévisionnel. A noter que le statut est
		// explicitement désigné comme en cours de validation prévisionnelle
		if (eoPlanning != null &&
				eoPlanning.isReel() &&
				eoPlanning.isOccupationARepercuterSurPrevisionnel()) {
			isARepercuterSurPrevisionnel = true;
			eoTypeStatut = EOTypeStatut.getEoTypeStatutEnCoursDeValidationPrevisionnelle(ec());
		} else {
			eoTypeStatut = EOTypeStatut.getEoTypeStatutEnCoursDeValidation(ec());
		}

		// occupation en cours de validation par défaut
		newEoOccupation = EOOccupation.createEOOccupation(ec(), dateDebut, dateFin,
				motif, eoTypeOccupation, eoStructureApplication,
				isImmediatementValide, getNature(), getPersId(),
				getTemPersonneComposante(), getPersonne(), getUser(),
				true, eoTypeStatut);

		// report vers prévisionnel
		if (isARepercuterSurPrevisionnel) {

			newEoOccupation.repercuter(
					eoPlanning.getEoPlanningPrev(), false, getUser(), false);
		}

		return newEoOccupation;
	}

	@Override
	public void envoyerNotification() {
		newEoOccupation.getDemandeDelegate().envoyerMail(getSession(), null);
	}

}
