/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.metier.workflow;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.A_Absence;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

/**
 * Workflow dédié à la manipulation d'absence
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public abstract class A_WorkflowAbsence
		extends A_WorkflowAuthentifieAvecEnvoiMail {

	private A_Absence absence;

	/**
	 * @param user
	 * @param session
	 * @param absence
	 *          TODO
	 */
	public A_WorkflowAbsence(
			HamacApplicationUser user, CktlWebSession session, A_Absence absence) {
		super(user, session);
		this.absence = absence;
	}

	/**
	 * @return
	 */
	public final A_Absence getAbsence() {
		return absence;
	}

	public abstract void envoyerNotification();

}
