package org.cocktail.fwkcktlhamac.serveur.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageModeCalcul;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.PlageModeCalculCompare;

import com.webobjects.foundation.NSTimestamp;

/**
 * 
 *
 */
public final class PlageModeCalculService {

	private PlageModeCalculService() {
		
	}
	
	/**
	 * Détermine parmi une liste de périodes, les différents mode de calcul à appliquer selon les dates
	 * 
	 * Si une période de contrat excéde nbMois, le mode de calcul devient pour le contractuel comme si c'était un titulaire
	 * Les contrats pouvant se succéder, un nombre de semaines permet de savoir si ces contrats sont concomitants ou non.
	 * 
	 * 
	 * @param nbMois nombre Mois à partir duquel le mode de calcul d'un contrat doit être comme pour les titulaires
	 * @param nbSemaines nombre Semaines jusqu'auquel deux contrats successifs sont considérés comme concomitants
	 * @param arraySimpleDebutFin
	 * @return
	 */
	public final static List<PlageModeCalcul> getPlageModeCalculPourSortedPeriodes(
			int nbMois, int nbSemaines, List<SimpleDebutFin> arraySimpleDebutFin) {

		List<PlageModeCalcul> periodeArray = new ArrayList<PlageModeCalcul>();
		
		List<SimpleDebutFin> arraySimpleDebutFinTitulaire = new ArrayList<SimpleDebutFin>();
		List<SimpleDebutFin> arraySimpleDebutFinContractuel = new ArrayList<SimpleDebutFin>();
		
		for (SimpleDebutFin simpleDebutFin : arraySimpleDebutFin) {
			
			if (simpleDebutFin.isTitulaire()) {
				arraySimpleDebutFinTitulaire.add(simpleDebutFin);
			} else {
				gererAjoutArrayContractuel(arraySimpleDebutFinContractuel, simpleDebutFin);
			}
			
		}
		
		List<PlageModeCalcul> periodeArrayTitulaire = new ArrayList<PlageModeCalcul>();
		List<PlageModeCalcul> periodeArrayContractuel = new ArrayList<PlageModeCalcul>();
		
		if (!arraySimpleDebutFinTitulaire.isEmpty()) {
			periodeArrayTitulaire = getPlageModeCalculTitulaire(arraySimpleDebutFinTitulaire);
			periodeArray.addAll(periodeArrayTitulaire);
		}
		if (!arraySimpleDebutFinContractuel.isEmpty()) {
			periodeArrayContractuel = getPlageModeCalculContractuel(arraySimpleDebutFinContractuel, nbMois, nbSemaines);
			periodeArray.addAll(periodeArrayContractuel);
		}
		
		periodeArray = traiterChevauchementContratsSurCarriere(periodeArrayTitulaire, periodeArrayContractuel);
		
		
		// On trie le résultat final par date de début
		Collections.sort(periodeArray, new PlageModeCalculCompare());
		
		// on fusionne les plages voisines ayant le même mode de calcul
		periodeArray = fusionnerPlagesVoisines(periodeArray);
		
		// On trie à nouveau
		Collections.sort(periodeArray, new PlageModeCalculCompare());
		
		return periodeArray;
		
	}

	/**
	 * Gére notamment les cas où plusieurs contrats existent sur une même période
	 * @param arraySimpleDebutFinContractuel
	 * @param periodeContrat
	 */
	private static void gererAjoutArrayContractuel(List<SimpleDebutFin> arraySimpleDebutFinContractuel, SimpleDebutFin periodeContrat) {
		
		if (CollectionUtils.isNotEmpty(arraySimpleDebutFinContractuel)) {
		
			SimpleDebutFin plageModeCalculAncien = arraySimpleDebutFinContractuel.get(arraySimpleDebutFinContractuel.size() - 1);
			
			if (DateCtrlHamac.isBefore(periodeContrat.getDateDebut(), plageModeCalculAncien.getDateFin())) {
				
				if (DateCtrlHamac.isAfter(periodeContrat.getDateFin(), plageModeCalculAncien.getDateFin())) {
					arraySimpleDebutFinContractuel.add(new SimpleDebutFin(plageModeCalculAncien.getDateFin(), periodeContrat.getDateFin(), false));
				}
				
			} else {
				arraySimpleDebutFinContractuel.add(periodeContrat);
			}
			
		} else {
			arraySimpleDebutFinContractuel.add(periodeContrat);
		}
		
	}

	/**
	 * Fusion des plages voisines de maximum un jour ayant le même mode de calcul
	 * @param periodeArray
	 * @return
	 */
	public static List<PlageModeCalcul> fusionnerPlagesVoisines(List<PlageModeCalcul> periodeArray) {
		
		List<PlageModeCalcul> listePlageASupprimer = new ArrayList<PlageModeCalcul>();
		List<PlageModeCalcul> listePlageAAjouter = new ArrayList<PlageModeCalcul>();
		
		PlageModeCalcul plageFusion = null;
		boolean estPlageSuivanteFusion = false;
		
		for (int i = 0; i < periodeArray.size(); i++) {
			PlageModeCalcul plageCourante = periodeArray.get(i);
			PlageModeCalcul plageSuivante;
			if (i < periodeArray.size() - 1) {
				plageSuivante = periodeArray.get(i + 1);
			} else {
				if (estPlageSuivanteFusion) {
					if (DateCtrlHamac.nbJoursEntre(plageFusion.getDateFin(), plageCourante.getDateDebut()) <= 1 
							&& plageFusion.isModeCalculTitulaire() == plageCourante.isModeCalculTitulaire()) {
						plageFusion.setDateFin(plageCourante.getDateFin());
						listePlageASupprimer.add(plageCourante);
					} 
					listePlageAAjouter.add(plageFusion);
				} 
				break;
			}
			
			
			if (estPlageSuivanteFusion) {
				if (DateCtrlHamac.nbJoursEntre(plageFusion.getDateFin(), plageCourante.getDateDebut()) <= 1 
						&& plageFusion.isModeCalculTitulaire() == plageCourante.isModeCalculTitulaire()) {
					plageFusion.setDateFin(plageCourante.getDateFin());
					listePlageASupprimer.add(plageCourante);
				} else {
					listePlageAAjouter.add(plageFusion);
					estPlageSuivanteFusion = false;
				}
			} else {
			
				if (DateCtrlHamac.nbJoursEntre(plageCourante.getDateFin(), plageSuivante.getDateDebut()) <= 1 
						&& plageCourante.isModeCalculTitulaire() == plageSuivante.isModeCalculTitulaire()) {
					listePlageASupprimer.add(plageCourante);
					listePlageASupprimer.add(plageSuivante);
					plageFusion = new PlageModeCalcul(plageCourante.getDateDebut(), plageCourante.getDateFin(), plageCourante.isModeCalculTitulaire());
					estPlageSuivanteFusion = true;
				} else {
					estPlageSuivanteFusion = false;
				}
			}
			
		}
		
		List<PlageModeCalcul> periodeArraySortie = new ArrayList<PlageModeCalcul>();
		
		for (PlageModeCalcul plageModeCalcul : periodeArray) {
			if (!listePlageASupprimer.contains(plageModeCalcul)) {
				periodeArraySortie.add(plageModeCalcul);
			}
		}
		periodeArraySortie.addAll(listePlageAAjouter);
		
		
		return periodeArraySortie;
		
		
	}

	/**
	 * Les carrières prennent le pas sur les contrats dans le mode de calcul pour les congés.
	 * Autrement dit, si carrière entre le 01/01/2010 et le 01/01/2015 et contrat entre 01/01/2011 et 01/01/2012, la plage de date du contrat est supprimé
	 * @param plageTitulaireArray
	 * @param plageContractuelArray
	 */
	private static List<PlageModeCalcul> traiterChevauchementContratsSurCarriere(List<PlageModeCalcul> plageTitulaireArray, List<PlageModeCalcul> plageContractuelArray) {

		List<PlageModeCalcul> listePlageFinal = new ArrayList<PlageModeCalcul>();
		
		for (PlageModeCalcul simpleDebutFinContractuel : plageContractuelArray) {
			List<PlageModeCalcul> simpleDebutFinAAjouter = traiterChevauchementPlageContractuel(simpleDebutFinContractuel, plageTitulaireArray);
			if (simpleDebutFinAAjouter != null && simpleDebutFinAAjouter.size() > 0) {
				listePlageFinal.addAll(simpleDebutFinAAjouter);
			}
		}
		
		listePlageFinal.addAll(plageTitulaireArray);
		
		return listePlageFinal;
	}
	
	/**
	 * 
	 * @param plageContractuelACorriger
	 * @param plageTitulaireArray
	 * @return
	 */
	private static List<PlageModeCalcul> traiterChevauchementPlageContractuel(PlageModeCalcul plageContractuelACorriger, List<PlageModeCalcul> plageTitulaireArray) {
		
		NSTimestamp dateDebut = plageContractuelACorriger.getDateDebut();
		NSTimestamp dateFin = plageContractuelACorriger.getDateFin();
		
		List<PlageModeCalcul> sortieArray = new ArrayList<PlageModeCalcul>();
		
		boolean ajoutContractuel = true;
		
		for (PlageModeCalcul simpleDebutFin : plageTitulaireArray) {
			if (simpleDebutFin.getDateFin() == null && DateCtrlHamac.isAfter(dateDebut, simpleDebutFin.getDateDebut())) {
				dateFin = null;
				ajoutContractuel = false;
			} else {
				if (DateCtrlHamac.isBetweenEq(dateDebut, simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin())) {
					dateDebut = simpleDebutFin.getDateFin();
				}
				if (dateFin != null && DateCtrlHamac.isBetweenEq(dateFin, simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin())) {
					dateFin = simpleDebutFin.getDateDebut();
				}
				if (DateCtrlHamac.isAfterNull(dateFin, simpleDebutFin.getDateFin()) && DateCtrlHamac.isBefore(dateDebut, simpleDebutFin.getDateDebut())) {
					sortieArray.add(new PlageModeCalcul(dateDebut, simpleDebutFin.getDateDebut(), plageContractuelACorriger.isModeCalculTitulaire()));
					sortieArray.add(new PlageModeCalcul(simpleDebutFin.getDateFin(), dateFin, plageContractuelACorriger.isModeCalculTitulaire()));
					dateFin = dateDebut;
				}
			}
		}
		
		if ((dateFin == null || DateCtrlHamac.isAfterNull(dateFin, dateDebut)) && ajoutContractuel) {
			sortieArray.add(new PlageModeCalcul(dateDebut, dateFin, plageContractuelACorriger.isModeCalculTitulaire()));
		}		
		return sortieArray;
	}
	
	/**
	 * @see PlageModeCalculService#getPlageModeCalculPourSortedPeriodes(int, int, List)
	 */
	private static List<PlageModeCalcul> getPlageModeCalculTitulaire(List<SimpleDebutFin> arraySimpleDebutFinTitulaire) {
		
		List<PlageModeCalcul> periodeArrayTitulaire = new ArrayList<PlageModeCalcul>();
		
		// traitement des périodes titulaires
		for (int i = 0; i < arraySimpleDebutFinTitulaire.size(); i++) {

			SimpleDebutFin periode = arraySimpleDebutFinTitulaire.get(i);

			periodeArrayTitulaire.add(new PlageModeCalcul(periode.getDateDebut(), periode.getDateFin(), true));

		}
		
		return periodeArrayTitulaire;
		
	}
	
	/**
	 * @see PlageModeCalculService#getPlageModeCalculPourSortedPeriodes(int, int, List)
	 */
	private static List<PlageModeCalcul> getPlageModeCalculContractuel(List<SimpleDebutFin> arraySimpleDebutFinContractuel, int nbMois, int nbSemaines) {
		
		List<PlageModeCalcul> periodeArrayContractuel = new ArrayList<PlageModeCalcul>();
		
		int plafond = nbMois * DateCtrlHamac.NB_JOURS_PAR_MOIS_COMPLET_30;

		NSTimestamp date = null;
		
		// déterminer la premiere date
		for (SimpleDebutFin periode : arraySimpleDebutFinContractuel) {
			if (date == null || DateCtrlHamac.isBefore(periode.getDateDebut(), date)) {
				date = periode.getDateDebut();
			}
		}
				
		List<PlageModeCalcul> plageModeCalculADeterminer = new ArrayList<PlageModeCalcul>();
		
		boolean modeCalculTitulaire = false;
		
		NSTimestamp derniereDateFinUtilisee = null;
		
		for (SimpleDebutFin simpleDebutFin : arraySimpleDebutFinContractuel) {
			
			if (derniereDateFinUtilisee != null && !DateCtrlHamac.isDatesCompriseEnNSemaines(derniereDateFinUtilisee, simpleDebutFin.getDateDebut(), nbSemaines)) {
				modeCalculTitulaire = false;
				periodeArrayContractuel.addAll(plageModeCalculADeterminer);
				plageModeCalculADeterminer.clear();
			}
			
			if (plageModeCalculADeterminer.isEmpty()) {
				
				if (simpleDebutFin.getDateFin() == null || DateCtrlHamac.getTotalJours360(simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin()) > plafond) {
					
					modeCalculTitulaire = true;
					
					periodeArrayContractuel.add(new PlageModeCalcul(simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin(), true));
					
				} else {
					
					plageModeCalculADeterminer.add(new PlageModeCalcul(simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin(), modeCalculTitulaire));
					
				}
				
			} else {
				
				if (simpleDebutFin.getDateFin() == null || DateCtrlHamac.getTotalJours360(plageModeCalculADeterminer.get(0).getDateDebut(), simpleDebutFin.getDateFin()) > plafond) {
					
					periodeArrayContractuel.add(new PlageModeCalcul(plageModeCalculADeterminer.get(0).getDateDebut(), 
							simpleDebutFin.getDateFin(), true));
					modeCalculTitulaire = true;
					plageModeCalculADeterminer.clear();
					
				} else {
					plageModeCalculADeterminer.add(new PlageModeCalcul(simpleDebutFin.getDateDebut(), simpleDebutFin.getDateFin(), modeCalculTitulaire));
				}
				
			}
			
			derniereDateFinUtilisee = simpleDebutFin.getDateFin();
			
			
		}
		
		if (!plageModeCalculADeterminer.isEmpty()) {
			periodeArrayContractuel.addAll(plageModeCalculADeterminer);
		}
				
		return periodeArrayContractuel;
		
	}
	
	
	
	/**
	 * Classe simple de gestion des périodes
	 * 
	 * @author Cyril TARADE <cyril.tarade at cocktail.org>
	 */
	public static class SimpleDebutFin {

		private NSTimestamp dateDebut, dateFin;
		private boolean isTitulaire;

		public SimpleDebutFin(
				NSTimestamp dateDebut, NSTimestamp dateFin, boolean isTitulaire) {
			super();
			this.dateDebut = dateDebut;
			this.dateFin = dateFin;
			this.isTitulaire = isTitulaire;
		}

		/**
		 * Le jour est-il contenu dans la période
		 * 
		 * @param date
		 * @return
		 */
		public boolean isJourContenu(NSTimestamp date) {
			boolean isJourContenu = false;

			if (DateCtrlHamac.isAfterEq(date, dateDebut) && (
						dateFin == null || DateCtrlHamac.isBeforeEq(date, dateFin))) {
				isJourContenu = true;
			}

			return isJourContenu;
		}
		
		public NSTimestamp getDateDebut() {
			return this.dateDebut;
		}
		
		public NSTimestamp getDateFin() {
			return this.dateFin;
		}
		
		public boolean isTitulaire() {
			return this.isTitulaire;
		}
		
	}

	
	/**
	 * 
	 * @param arraySimpleDebutFin
	 * @param date
	 * @return
	 */
	public static PlageModeCalculService.SimpleDebutFin recupererSimpleDebutFinDate(List<SimpleDebutFin> arraySimpleDebutFin, NSTimestamp date) {
		
		for (SimpleDebutFin simpleDebutFin : arraySimpleDebutFin) {
			if (simpleDebutFin.isJourContenu(date)) {
				return simpleDebutFin;
			}
		}
		return null;
	}
	
	private static boolean estSimpleDebutFinPresentDansListe(SimpleDebutFin simpleDebutFinAComparer, List<SimpleDebutFin> listeSimpleDebutFin) {
		
		boolean isEquals = false;
		
		for (SimpleDebutFin simpleDebutFin : listeSimpleDebutFin) {
			
			boolean memeDateDebut = false;
			boolean memeDateFin = false;
			boolean memeMode = false;
			
			if ((simpleDebutFin.getDateDebut() == null && simpleDebutFinAComparer.getDateDebut() == null) || (simpleDebutFin.getDateDebut() != null && simpleDebutFin.getDateDebut().equals(simpleDebutFinAComparer.getDateDebut()))) {
				memeDateDebut = true;
			}
			
			if ((simpleDebutFin.getDateFin() == null && simpleDebutFinAComparer.getDateFin() == null) || (simpleDebutFin.getDateFin() != null && simpleDebutFin.getDateFin().equals(simpleDebutFinAComparer.getDateFin()))) {
				memeDateFin = true;
			}
			
			if (simpleDebutFin.isTitulaire() == simpleDebutFinAComparer.isTitulaire()) {
				memeMode = true;
			}
			if (memeDateDebut && memeDateFin && memeMode) {
				isEquals = true;
				break;
			}
		}
		return isEquals;
	}
	
	/**
	 * On enlève les doublons
	 * @param listePeriode
	 * @return
	 */
	public static List<SimpleDebutFin> filtrerDoublons(List<SimpleDebutFin> listePeriode) {
		
		List<SimpleDebutFin> listeSortie = new ArrayList<SimpleDebutFin>();
		
		for (SimpleDebutFin simpleDebutFin : listePeriode) {
			if (!estSimpleDebutFinPresentDansListe(simpleDebutFin, listeSortie)) {
				listeSortie.add(simpleDebutFin);
			}
		}
		
		return listeSortie;
		
	}
	
	
}
