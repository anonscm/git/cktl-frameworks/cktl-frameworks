/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Jour;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe ajoutant des methodes a DateCtrl
 * 
 * @author Emmanuel Geze <egeze at univ-lr.fr>
 * @author Cyril Tarade <cyril.tarade at univ-lr.fr>
 */
public class DateCtrlHamac
		extends DateCtrl {

	// TODO parametre
	private static String debutAnnee = "01/09";

	public final static int NB_JOURS_PAR_MOIS_COMPLET_30 = 30;
	public final static int NB_JOURS_PAR_AN_COMPLET_360 = 360;
	public final static long NB_MS_PAR_JOUR = (1000 * 60 * 60 * 24);
	public final static int NB_JOURS_PAR_SEMAINE = 7;

	/**
	 * Methode qui donne les jours feries entre deux dates firstDay et lastDay
	 * inclus
	 * 
	 * @param firstDayTS
	 * @param lastDayTS
	 * @return
	 */
	public static NSArray<NSTimestamp> joursFeriesEntre2Dates(NSTimestamp firstDayTS, NSTimestamp lastDayTS) {

		NSMutableArray<NSTimestamp> joursFeriesEntre2Dates = null;
		NSArray<NSTimestamp> joursFeries = null;
		Enumeration<NSTimestamp> enumJoursFeries = null;
		GregorianCalendar firstDayGC, lastDayGC = null;
		int firstDayYear, lastDayYear = 0;

		firstDayGC = new GregorianCalendar();
		firstDayGC.setTime(firstDayTS);
		firstDayYear = firstDayGC.get(Calendar.YEAR);
		lastDayGC = new GregorianCalendar();
		lastDayGC.setTime(lastDayTS);
		lastDayYear = lastDayGC.get(Calendar.YEAR);

		joursFeries = holidaysFR(firstDayYear);

		// les jours fériés "locaux"
		String strHolydayLocal = HamacCktlConfig.stringForKey(EOParametre.JOURS_FERIES_COMPLEMENTAIRES);
		if (!StringCtrl.isEmpty(strHolydayLocal)) {
			NSArray<String> strHolydayLocalArray = NSArray.componentsSeparatedByString(strHolydayLocal, ",");
			for (String strHolyday : strHolydayLocalArray) {
				joursFeries = joursFeries.arrayByAddingObject(
						stringToDate(strHolyday));
			}
		}

		while (firstDayYear < lastDayYear) {
			firstDayYear++;
			joursFeries = joursFeries.arrayByAddingObjectsFromArray(
					holidaysFR(firstDayYear));
		}

		joursFeriesEntre2Dates = new NSMutableArray<NSTimestamp>();
		enumJoursFeries = joursFeries.objectEnumerator();
		while (enumJoursFeries.hasMoreElements()) {
			NSTimestamp unJourFerie = enumJoursFeries.nextElement();
			if (isAfterEq(unJourFerie, firstDayTS) && isBeforeEq(unJourFerie, lastDayTS)) {
				joursFeriesEntre2Dates.addObject(unJourFerie);
			}
		}

		return joursFeriesEntre2Dates;
	}

	// Methode qui calcule le nbre de jours ouvres sur une periode donnee
	static public long nbreJoursOuvresEntre(NSTimestamp firstDayTS, NSTimestamp lastDayTS) {
		long nbreJoursOuvres = 0;

		if (isAfter(firstDayTS, lastDayTS)) {
			nbreJoursOuvres = -1;
		} else {
			NSArray<NSTimestamp> joursFeries = joursFeriesEntre2Dates(firstDayTS, lastDayTS);
			GregorianCalendar currentDayGC = null;
			NSTimestamp currentDayTS = null;
			int currentDayNum = 0;

			nbreJoursOuvres = lastDayTS.getTime() - firstDayTS.getTime();
			nbreJoursOuvres /= 1000; // Transfo en secondes
			nbreJoursOuvres /= 3600; // Transfo en heures
			nbreJoursOuvres /= 24; // Transfo en jours
			nbreJoursOuvres++;
			currentDayTS = new NSTimestamp(firstDayTS);
			currentDayGC = new GregorianCalendar();
			while (isAfterEq(lastDayTS, currentDayTS)) {
				currentDayGC.setTime(currentDayTS);
				currentDayNum = currentDayGC.get(Calendar.DAY_OF_WEEK);
				if (joursFeries.containsObject(currentDayTS) ||
						currentDayNum == Calendar.SATURDAY ||
						currentDayNum == Calendar.SUNDAY) {
					nbreJoursOuvres--;
				}
				currentDayTS = currentDayTS.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
			}
		}
		return nbreJoursOuvres;
	}

	/**
	 * remonte jusqu'au lundi 0:00
	 * 
	 * @param date
	 */
	public static NSTimestamp reculerToLundi(NSTimestamp date) {
		NSTimestamp lundiEnCours = date;
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(lundiEnCours);

		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == Calendar.SUNDAY) {
			dayOfWeek = 8;
		}

		// if (dayOfWeek == GregorianCalendar.SUNDAY) {
		// lundiEnCours = lundiEnCours.timestampByAddingGregorianUnits(0, 0, 1, 0,
		// 0, 0);
		/* } else { */
		lundiEnCours = lundiEnCours.timestampByAddingGregorianUnits(0, 0, -(dayOfWeek - 2), 0, 0, 0);
		// }
		return TimeCtrl.remonterAMinuit(lundiEnCours);
	}

	/**
	 * avance jusqu'au dimanche 0:00
	 * 
	 * @param date
	 */
	public static NSTimestamp avancerToDimanche(NSTimestamp date) {
		NSTimestamp dimanche = null;
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek != GregorianCalendar.SUNDAY) {
			dimanche = date.timestampByAddingGregorianUnits(0, 0, 8 - dayOfWeek, 0, 0, 0);
		} else {
			dimanche = date;
		}

		return TimeCtrl.remonterAMinuit(dimanche);
	}

	/**
	 * Donne le numero d'une semaine pour une date
	 * 
	 * @param date
	 * @return
	 */
	public static int weekNumber(NSTimestamp date) {
		FixedGregorianCalendar fixedCalendar = new FixedGregorianCalendar();
		fixedCalendar.setTime(date);
		return fixedCalendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * retourne le debut de l'annee universitaire d'une date ex : 02/02/2005 ->
	 * 01/09/2004
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToDebutAnneeUniv(NSTimestamp uneDate) {
		// on transltate la date sur l'annee civile : 01/09/2004 -> 01/01/2004
		int jourDebutAnnee = Integer.valueOf(debutAnnee.substring(0, 2)).intValue();
		int moisDebutAnnee = Integer.valueOf(debutAnnee.substring(3, 5)).intValue();

		NSTimestamp uneDateDecalee = uneDate.timestampByAddingGregorianUnits(
					jourDebutAnnee - 1, -(moisDebutAnnee - 1), 0, 0, 0, 0);
		GregorianCalendar nowGC = new GregorianCalendar();
		nowGC.setTime(uneDateDecalee);
		NSTimestamp debutAnneeDecalee = uneDateDecalee.timestampByAddingGregorianUnits(
					0,
					-nowGC.get(GregorianCalendar.MONTH),
					-nowGC.get(GregorianCalendar.DAY_OF_MONTH) + 1,
					-nowGC.get(GregorianCalendar.HOUR_OF_DAY),
					-nowGC.get(GregorianCalendar.MINUTE),
					-nowGC.get(GregorianCalendar.SECOND)
				);
		NSTimestamp debutAnnee = debutAnneeDecalee.timestampByAddingGregorianUnits(
					0, (moisDebutAnnee - 1), 0, 0, 0, 0);
		return debutAnnee;
	}

	/**
	 * retourne la fin de l'annee universitaire d'une date ex : 02/02/2005 ->
	 * 31/08/2005
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToFinAnneeUniv(NSTimestamp uneDate) {
		return dateToDebutAnneeUniv(uneDate).timestampByAddingGregorianUnits(1, 0, -1, 0, 0, 0);
	}

	/**
	 * retourne le debut de l'annee civile d'une date ex : 02/02/2005 ->
	 * 01/01/2005
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToDebutAnneeCivile(NSTimestamp uneDate) {
		GregorianCalendar nowGC = new GregorianCalendar();
		nowGC.setTime(uneDate);
		NSTimestamp debutAnneeCivile = uneDate.timestampByAddingGregorianUnits(
						0,
						-nowGC.get(GregorianCalendar.MONTH),
						-nowGC.get(GregorianCalendar.DAY_OF_MONTH) + 1,
						-nowGC.get(GregorianCalendar.HOUR_OF_DAY),
						-nowGC.get(GregorianCalendar.MINUTE),
						-nowGC.get(GregorianCalendar.SECOND)
						);
		return debutAnneeCivile;
	}

	/**
	 * retourne le 1er janvier associe a une date sur une annee univ ex :
	 * 02/02/2005 -> 01/01/2005 ex : 10/10/2004 -> 01/01/2005
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp date1erJanAnneeUniv(NSTimestamp uneDate) {
		NSTimestamp debutAnneeUniv = dateToDebutAnneeUniv(uneDate);
		int jourDebutAnnee = Integer.valueOf(debutAnnee.substring(0, 2)).intValue();
		int moisDebutAnnee = Integer.valueOf(debutAnnee.substring(3, 5)).intValue();
		int diffJour = -jourDebutAnnee + 1;
		int diffMois = (12 - moisDebutAnnee) + 1;
		return debutAnneeUniv.timestampByAddingGregorianUnits(
						0, diffMois, -diffJour, 0, 0, 0);
	}

	/**
	 * retourne la fin de l'annee civile d'une date
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToFinAnneeCivile(NSTimestamp uneDate) {
		return dateToDebutAnneeCivile(uneDate).timestampByAddingGregorianUnits(1, 0, -1, 0, 0, 0);
	}

	/**
	 * retourne le debut du mois d'une date
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToDebutMois(NSTimestamp uneDate) {
		// on transltate la date sur l'annee civile : 01/09/2004 -> 01/01/2004
		GregorianCalendar nowGC = new GregorianCalendar();
		nowGC.setTime(uneDate);
		NSTimestamp debutMois = uneDate.timestampByAddingGregorianUnits(
							0,
							0,
							-nowGC.get(GregorianCalendar.DAY_OF_MONTH) + 1,
							-nowGC.get(GregorianCalendar.HOUR_OF_DAY),
							-nowGC.get(GregorianCalendar.MINUTE),
							-nowGC.get(GregorianCalendar.SECOND)
							);
		return debutMois;
	}

	/**
	 * retourne la fin du mois d'une date
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateToFinMois(NSTimestamp uneDate) {
		return dateToDebutMois(uneDate).timestampByAddingGregorianUnits(0, 1, -1, 0, 0, 0);
	}

	/**
	 * savoir si 2 jour sont les meme (ignore les heures)
	 * 
	 * @param uneDate1
	 * @param uneDate2
	 * @return
	 */
	public static boolean isSameDay(NSTimestamp uneDate1, NSTimestamp uneDate2) {
		return dateToString(uneDate1).equals(dateToString(uneDate2));
	}

	/**
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp dateDebutAnnee(NSTimestamp uneDate) {
		NSTimestamp dateDebutAnnee = uneDate;
		GregorianCalendar myCalendar = new GregorianCalendar();

		int moisDebutAnnee = Integer.valueOf(debutAnnee.substring(3, 5)).intValue();
		myCalendar.setTime(uneDate);
		int year = myCalendar.get(GregorianCalendar.YEAR);
		int month = myCalendar.get(GregorianCalendar.MONTH) + 1;
		if (month < moisDebutAnnee)
			year--;
		myCalendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
		myCalendar.set(GregorianCalendar.MONTH, moisDebutAnnee - 1);
		myCalendar.set(GregorianCalendar.YEAR, year);
		myCalendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
		myCalendar.set(GregorianCalendar.MINUTE, 0);
		myCalendar.set(GregorianCalendar.SECOND, 0);

		dateDebutAnnee = new NSTimestamp(myCalendar.getTimeInMillis());

		return dateDebutAnnee;
	}

	/**
	 * retourne l'annee de la période. 
	 * ex : 10/10/2004 - 01/06/2005 -> 2004/2005
	 * 		10/10/2004 - 12/12/2004 -> 2004
	 * 
	 * @return
	 */
	public static String anneeLibelleForPeriode(NSTimestamp aDateDebut, NSTimestamp aDateFin) {
		GregorianCalendar myCalendarDebut = new GregorianCalendar();
		GregorianCalendar myCalendarFin = new GregorianCalendar();
		
		myCalendarDebut.setTime(aDateDebut);
		myCalendarFin.setTime(aDateFin);

		int anneeDebut = myCalendarDebut.get(GregorianCalendar.YEAR);
		int anneeFin = myCalendarFin.get(GregorianCalendar.YEAR);
		
		if (anneeDebut == anneeFin) {
			return String.valueOf(anneeDebut);
		} else {
			return anneeDebut + "/" + anneeFin; 
		}
	}

	// TODO 05/01/2005 : centraliser la methode (existe dans Planning.java)
	public static String to_duree(int minutes) {
		String to_duree = "";

		int lesMinutesAbs = Math.abs(minutes);

		int lesHeures = lesMinutesAbs / 60;
		int lesMinutes = lesMinutesAbs % 60;

		to_duree += lesHeures + "h";
		if (lesMinutes < 10)
			to_duree += "0";
		to_duree += lesMinutes;

		if (minutes < 0) {
			to_duree = "-" + to_duree;
		}

		return to_duree;
	}

	/**
	 * Retourne la liste des periodes <code>EOPeriodeAffectationAnnuelle</code>
	 * attendues par l'affectation pour une date de reference Modification de la
	 * methode pour tenir compte aussi des periodes qui n'incluent pas la date du
	 * jour, mais qui sont dans l'année universitaire en cours.
	 * 
	 * Exemples d'utilisation - dateRef = 15/06/2004 ET affectation =
	 * debut:30/06/2003 - fin:31/08/2008 >> "01/09/2003"->"31/08/2004" - dateRef =
	 * 15/06/2004 ET affectation = debut:30/06/2003 - fin:30/06/2004 >>
	 * "01/09/2003"->"30/06/2004" - dateRef = 15/06/2004 ET affectation =
	 * debut:15/01/2004 - fin: >> "15/01/2004"->"31/08/2004" - dateRef =
	 * 15/06/2004 ET affectation = debut:30/06/2004 - fin: >>
	 * "30/06/2004"->"31/08/2004" - dateRef = 15/06/2004 ET affectation =
	 * debut:01/09/2003 - fin:15/05/2004 >> "01/09/2003"->"15/05/2004"
	 * 
	 * @return <code>NSArray</code> de couples de dates debut / fin
	 */
	public static NSArray lesPeriodesAnneeUniv(NSTimestamp dateRef, NSTimestamp dateDebutAff, NSTimestamp dateFinAff) {
		// deduire l'année universitaire associee a la date dateRef
		NSTimestamp dDebAnneeUniv = dateToDebutAnneeUniv(dateRef);
		NSTimestamp dFinAnneeUniv = dateToFinAnneeUniv(dateRef);

		// retailler la periode par rapport a l'annee universitaire
		NSTimestamp dDebutPeriode = dateDebutAff;
		if (dateDebutAff == null || isBefore(dateDebutAff, dDebAnneeUniv)) {
			dDebutPeriode = dDebAnneeUniv;
		}
		NSTimestamp dFinPeriode = dateFinAff;
		if (dateFinAff == null || isAfter(dateFinAff, dFinAnneeUniv)) {
			dFinPeriode = dFinAnneeUniv;
		}

		return new NSArray(new NSTimestamp[] { dDebutPeriode, dFinPeriode });
	}

	/**
	 * Donne le nombre de jours entre 2 dates
	 */
	public static int nbJoursEntre(NSTimestamp debut, NSTimestamp fin) {
		return (int) ((fin.getTime() - debut.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * Donne le numéro de jour attendu pour l'attribut
	 * {@link EOPlageService#hjouNumJourSemaine()} 1=dimanche, ... , 7=samedi
	 */
	public static int getNumJourSemaineForDate(NSTimestamp date) {
		FixedGregorianCalendar gc = new FixedGregorianCalendar();
		gc.setTime(date);
		int numJourSemaine = gc.get(FixedGregorianCalendar.DAY_OF_WEEK);
		return numJourSemaine;
	}

	/**
	 * mettre une date a minuit (le jour meme)
	 * 
	 * @param uneDate
	 * @return
	 */
	public static NSTimestamp remonterToMinuit(NSTimestamp uneDate) {
		GregorianCalendar nowGC = new GregorianCalendar();
		nowGC.setTime(uneDate);
		return uneDate.timestampByAddingGregorianUnits(0, 0, 0,
				-nowGC.get(GregorianCalendar.HOUR_OF_DAY), -nowGC.get(GregorianCalendar.MINUTE), -nowGC.get(GregorianCalendar.SECOND));
	}

	/**
	 * Donne le nombre total de minutes écoulées minuit de la date
	 * 
	 * @param uneDate
	 * @return
	 */
	public static int minutesDepuisMinuit(NSTimestamp uneDate) {
		int minutes = 0;

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(uneDate);

		minutes = 60 * gc.get(GregorianCalendar.HOUR_OF_DAY) + gc.get(GregorianCalendar.MINUTE);

		return minutes;
	}

	/**
	 * Indique une date tombe un lundi (utilisé pour le controle des jours d'une
	 * semaine)
	 * 
	 * @param uneDate
	 * @return
	 */
	public static boolean isLundi(NSTimestamp uneDate) {
		int dayOfWeek = getNumJourSemaineForDate(uneDate);
		return dayOfWeek == FixedGregorianCalendar.MONDAY;
	}

	/**
	 * TODO traiter le cas d'une periode unique a 1 jour qui tombe le 31 ...
	 * 
	 * Retourne la duree d'une periode sur la base de 1 mois = 30 jours 1 an = 12
	 * mois = 360 jours
	 * 
	 * @param dateDebut
	 * @param dateDebut
	 * 
	 * @return le nombre de jours de cette periode sur cette base
	 */
	public static int getTotalJours360(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int dureePeriode = 0;
		NSTimestamp dateDebutMoisEnCours = DateCtrlHamac.dateToDebutMois(dateDebut);
		NSTimestamp dateFinMoisEnCours = DateCtrlHamac.dateToFinMois(dateDebut);
		while (DateCtrlHamac.isBeforeEq(dateDebutMoisEnCours, dateFin)) {
			boolean moisComplet = true;

			// recadrage du debut et de la fin sur la periode
			if (DateCtrlHamac.isBefore(dateDebutMoisEnCours, dateDebut)) {
				dateDebutMoisEnCours = dateDebut;
				moisComplet = false;
			}

			if (DateCtrlHamac.isAfter(dateFinMoisEnCours, dateFin)) {
				dateFinMoisEnCours = dateFin;
				moisComplet = false;
			}

			// si le mois est complet, on compte 30 jours
			if (moisComplet) {
				dureePeriode += 30;
			} else {
				// +1 car de j à j+i, il y a i+1 jours complets
				dureePeriode += (dateFinMoisEnCours.getTime() - dateDebutMoisEnCours.getTime()) / (1000 * 60 * 60 * 24) + 1;
			}

			// avance au mois suivant
			dateDebutMoisEnCours = DateCtrlHamac.dateToDebutMois(dateDebutMoisEnCours).timestampByAddingGregorianUnits(0, 1, 0, 0, 0, 0);
			dateFinMoisEnCours = dateDebutMoisEnCours.timestampByAddingGregorianUnits(0, 1, -1, 0, 0, 0);
		}
		return dureePeriode;
	}

	/**
	 * Donne la valeur en minutes relativement à début de la journée (minuit)
	 * 
	 * @return
	 */
	public static int getMinutesDansJour(int minutes) {
		return minutes % Jour.DUREE_JOURNEE_EN_MINUTES;
	}

	public static int nbMinutesEntre(NSTimestamp date1, NSTimestamp date2) {
		return (int) ((date2.getTime() - date1.getTime()) / (1000 * 60));
	}
	
	
	/**
	 * Donne la valeur en minutes relativement à début de la journée (minuit)
	 * 
	 * @return
	 */
	public static int getMinutesDansJour(NSTimestamp date) {
		int minutes = 0;

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);

		minutes = gc.get(GregorianCalendar.HOUR_OF_DAY) * 60 + gc.get(GregorianCalendar.MINUTE);

		return minutes;

	}

	/**
	 * Indique si la demi journée est inclue dans la période indiquée. Respecte le
	 * format des date pour les occupations à la demi journée. Les dates doivent
	 * être au format AM/PM
	 * 
	 * Exemple : 10/10/2012 AM - 10/10/2012 AM => demi journée matin oui
	 * 
	 * Exemple : 10/10/2012 AM - 10/10/2012 AM => demi journée après midi non
	 * 
	 * Exemple : 10/10/2012 AM - 10/10/2012 PM => demi journée matin oui
	 * 
	 * Exemple : 10/10/2012 AM - 10/10/2012 PM => demi journée après midi oui
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static boolean isDemiJourneeInclue(NSTimestamp dateDebut, NSTimestamp dateFin, boolean isMatin) {
		boolean isInclue = false;

		if (isBeforeEq(dateDebut, dateFin)) {

			NSTimestamp dateTest = null;
			if (isMatin) {
				dateTest = getDateAM(dateDebut);
			} else {
				dateTest = getDatePM(dateDebut);
			}

			if (isBeforeEq(dateDebut, dateTest) &&
					isAfterEq(dateFin, dateTest)) {
				isInclue = true;
			}

		}

		return isInclue;
	}

	/**
	 * Donne la valeur AM d'une date (i.e. positionnée à minuit)
	 * 
	 * @param date
	 * @return
	 */
	public static NSTimestamp getDateAM(NSTimestamp date) {
		NSTimestamp dateAM = null;

		dateAM = remonterToMinuit(date);

		return dateAM;
	}

	/**
	 * Donne la valeur AM d'une date (i.e. positionnée à 12:00:00)
	 * 
	 * @param date
	 * @return
	 */
	public static NSTimestamp getDatePM(NSTimestamp date) {
		NSTimestamp datePM = null;

		datePM = getDateAM(date).timestampByAddingGregorianUnits(0, 0, 0, 12, 0, 0);

		return datePM;
	}

	/**
	 * Donne le nombre de jours dans le mois pointé par dateDebutMois.
	 * Fonctionnement avec la règle du 30ème (si mois complet, alors 30 jours,
	 * sinon c'est le nombre de jours séparant le début ou la fin du mois)
	 * 
	 * @param dateDebutMois
	 *          la date du mois (doit être exactement le 1er du mois)
	 * @param dateDebut
	 *          la date de début
	 * @param dateFin
	 *          la date de fin
	 * @return
	 */
	public static int nbJourMoisTrentieme(NSTimestamp dateDebutMois, NSTimestamp dateDebut, NSTimestamp dateFin) {
		int nb = 0;

		NSTimestamp dateFinMois = dateDebutMois.timestampByAddingGregorianUnits(0, 1, -1, 0, 0, 0);

		if (DateCtrlHamac.isAfter(dateDebut, dateFin) ||
				DateCtrlHamac.isAfter(dateDebut, dateFinMois) ||
				DateCtrlHamac.isBefore(dateFin, dateDebutMois)) {
			// incohérence des paramètres ou hors période

		} else if (DateCtrl.isBeforeEq(dateDebut, dateDebutMois) &&
				DateCtrlHamac.isAfterEq(dateFin, dateFinMois)) {
			// mois complet
			nb = NB_JOURS_PAR_MOIS_COMPLET_30;

		} else {
			// mois incomplet

			NSTimestamp dateSeuil = dateDebut;
			if (DateCtrlHamac.isBefore(dateSeuil, dateDebutMois)) {
				dateSeuil = dateDebutMois;
			}

			NSTimestamp dateButoir = dateFin;
			if (DateCtrlHamac.isAfter(dateButoir, dateFinMois)) {
				dateButoir = dateFinMois;
			}

			// cas particulier pour le début en milieu de mois mais qui finit la fin
			// du mois : la fin du mois est toujours considérée au 30
			if (DateCtrl.isAfter(dateSeuil, dateDebutMois) &&
					DateCtrl.isSameDay(dateFin, dateFinMois)) {

				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(dateSeuil);
				int numeroDuJour = gc.get(GregorianCalendar.DAY_OF_MONTH);

				if (numeroDuJour < NB_JOURS_PAR_MOIS_COMPLET_30) {
					// jusqu'au 29
					nb = NB_JOURS_PAR_MOIS_COMPLET_30 - numeroDuJour + 1;
				} else {
					// sinon c'est 1
					nb = 1;
				}

			} else {

				NSTimestamp date = dateSeuil;
				while (DateCtrlHamac.isBeforeEq(date, dateButoir)) {
					nb++;
					date = date.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
				}

			}

		}

		return nb;
	}


	
	
	/**
	 * Indique si 2 dates sont séparés de plus de n semaines
	 * ex : date 1 : 10/10, date 2 : 18/10, nbSemaines : 1 -> false
	 * 		date 1 : 10/10, date 2 : 17/10, nbSemaines : 1 -> true
	 * 
	 * @param date1
	 * @param date2
	 * @param nbSemaines
	 * @return
	 */
	public static boolean isDatesCompriseEnNSemaines(NSTimestamp date1, NSTimestamp date2, int nbSemaines) {
		
		boolean isDatesCompriseEnNSemaines = false;
		
		int nbJoursEntreDates = DateCtrl.nbJoursEntreDates(date1, date2);
		
		nbJoursEntreDates = Math.abs(nbJoursEntreDates);
		
		if (nbJoursEntreDates <= nbSemaines * NB_JOURS_PAR_SEMAINE) {
			isDatesCompriseEnNSemaines = true;
		}
		
		return isDatesCompriseEnNSemaines;
	}
	
	
	/**
	 * Indique si 2 date sont sur le même jour du mois. Exemple 10/10 et 10/12
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isMemeJourMois(NSTimestamp date1, NSTimestamp date2) {
		boolean isMemeJourMois = false;

		GregorianCalendar gc1 = new GregorianCalendar();
		gc1.setTime(date1);

		GregorianCalendar gc2 = new GregorianCalendar();
		gc1.setTime(date2);

		if (gc1.get(GregorianCalendar.DAY_OF_MONTH) == gc2.get(GregorianCalendar.DAY_OF_MONTH)) {
			isMemeJourMois = true;
		}

		return isMemeJourMois;
	}

	/**
	 * Compte le nombre de demi journées entre 2 dates. Les dates doivent être au
	 * format AM/PM
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public final static int getNbDemiJourneeEntre(NSTimestamp dateDebut, NSTimestamp dateFin) {
		int nb = 0;

		if (dateDebut == null || dateFin == null) {
			throw new NSValidation.ValidationException("Les deux dates doivent être renseignées");
		}

		if (!isBefore(dateFin, dateDebut)) {
			NSTimestamp date = dateDebut;

			nb = 1;
			while (!isAfterEq(date, dateFin)) {
				date = date.timestampByAddingGregorianUnits(0, 0, 0, 12, 0, 0);
				nb++;
			}

		}

		return nb;
	}
	
	/**
	 * Si date1 est null et pas date2 -> true
	 * Si date1 est non null et date2 null -> false
	 * Si date1 est null et date2 null -> false
	 * Sinon appel de isAfter
	 * @param date1 date
	 * @param date2 date
	 * @return isAfter ?
	 */
	public static boolean isAfterNull(NSTimestamp date1, NSTimestamp date2) {
		if (date1 == null && date2 != null) {
			return true;
		}
		if (date1 == null && date2 == null) {
			return false;
		}
		if (date1 != null && date2 == null) {
			return false;
		}
		return isAfter(date1, date2);
	}
	
	
}
