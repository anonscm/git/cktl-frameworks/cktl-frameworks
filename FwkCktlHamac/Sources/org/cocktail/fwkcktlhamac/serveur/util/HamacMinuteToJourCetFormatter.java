/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.text.FieldPosition;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;

/**
 * Formattage pour le CET (durée de la journée fixe)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HamacMinuteToJourCetFormatter
		extends HamacMinuteToJourFormatter {

	/**
	 * @param user
	 */
	public HamacMinuteToJourCetFormatter(EOPeriode periode) {
		super(null);
		setDureeJour(HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, periode));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer,
	 * java.text.FieldPosition)
	 */
	@Override
	public StringBuffer format(Object obj, StringBuffer stringbuffer, FieldPosition fieldposition) {
		return format(obj, stringbuffer, fieldposition, getDureeJour());
	}
}
