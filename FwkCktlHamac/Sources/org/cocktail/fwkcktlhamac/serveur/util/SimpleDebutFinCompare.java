package org.cocktail.fwkcktlhamac.serveur.util;

import java.util.Comparator;

import org.cocktail.fwkcktlhamac.serveur.util.PlageModeCalculService.SimpleDebutFin;


/**
 * 
 * Classe permettant de les ordonner
 *
 */
public class SimpleDebutFinCompare implements Comparator<SimpleDebutFin> {

	/**
	 * @param o1 objet1
	 * @param o2 objet2
	 * @return a negative integer, zero, or a positive integer as the
	 * 	       first argument is less than, equal to, or greater than the
	 *	       second.
	 */
	public int compare(SimpleDebutFin o1, SimpleDebutFin o2) {
		if (o1.getDateDebut().after(o2.getDateDebut())) {
			return 1;
		} else {
			return -1;
		}
	}
	
}
