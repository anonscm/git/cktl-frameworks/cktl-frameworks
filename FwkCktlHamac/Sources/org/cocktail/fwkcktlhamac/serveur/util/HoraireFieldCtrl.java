/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Outils permettant l'exploitation du contenu des attributs utilisés dans les
 * table de HAMAC.
 * 
 * Par exemple, pouvoir extraire une données d'un champ multi-évalué :
 * "1000@25;2000@20;" Il y a 2 deux données 1000 et 2000 correpondantes aux clés
 * 25 et 20, soit 2 couples valeur,clé
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HoraireFieldCtrl {

	private final static String SEPARATEUR_DUREE = "-";
	private final static String SEPARATEUR_VALEUR_CLE = "@";
	private final static String SEPARATEUR_COUPLE = ";";

	/**
	 * Retourner la chaine sous forme de tableau contenant l'ensemble des chaines
	 * uniques valeur,clé
	 * 
	 * @param chaine
	 * @return
	 */
	public static NSArray<String> array(String chaine) {
		NSArray<String> array = NSArray.componentsSeparatedByString(
				chaine, SEPARATEUR_COUPLE);

		NSMutableArray<String> arrayNonVide = new NSMutableArray<String>();
		// supprimer les chaines vides
		for (String element : array) {
			if (!StringCtrl.isEmpty(element)) {
				arrayNonVide.addObject(element);
			}
		}

		return arrayNonVide;
	}

	/**
	 * Extraction de la clé d'un champ encodé par exemple : getCle(1000@25)
	 * retourne 25
	 * 
	 * @param chaine
	 * @return
	 */
	public static String getCle(String element) {
		NSArray<String> coupleValeurCle = NSArray.componentsSeparatedByString(
				element, SEPARATEUR_VALEUR_CLE);
		return coupleValeurCle.objectAtIndex(1);
	}

	/**
	 * Extraction de la clé d'un champ encodé par exemple : getValeur(1000@25)
	 * retourne 1000
	 * 
	 * @param chaine
	 * @return
	 */
	public static String getValeur(String element) {
		NSArray<String> coupleValeurCle = NSArray.componentsSeparatedByString(
				element, SEPARATEUR_VALEUR_CLE);
		return coupleValeurCle.objectAtIndex(0);
	}

	/**
	 * Extraction d'une des valeurs par exemple : getValeur(1000-2000@25,1)
	 * retourne 2000
	 * 
	 * @param chaine
	 * @return
	 */
	public static String getValeur(String element, int index) {
		NSArray<String> chaineValeur = NSArray.componentsSeparatedByString(
				getValeur(element), SEPARATEUR_DUREE);
		return chaineValeur.objectAtIndex(index);
	}

	/**
	 * Construction d'un triplet début / fin / cStructure
	 * 
	 * @param minutesDebut
	 * @param minutesFin
	 * @param cStructure
	 * @return
	 */
	public static String createTriplet(int minutesDebut, int minutesFin, String cStructure) {
		String triplet = "";

		triplet = Integer.toString(minutesDebut) +
				SEPARATEUR_DUREE + Integer.toString(minutesFin) +
				SEPARATEUR_VALEUR_CLE + cStructure;

		return triplet;
	}
}
