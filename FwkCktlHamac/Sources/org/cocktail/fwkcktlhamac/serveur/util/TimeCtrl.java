/*
 * Copyright Université de La Rochelle 2004
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant à gérer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.fwkcktlhamac.serveur.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author ctarade
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class TimeCtrl {

	// GET MINUTES : Retourne le nombre de minutes correspondant à la chaine
	// string au format %H:%M (l'inverse de stringFor: )
	public static int getMinutes(String chaine) {
		int nombre = 0;

		if (StringCtrl.isEmpty(chaine) || ("00:00".equals(chaine)) || ("".equals(chaine)) || ("..:..".equals(chaine)))
			return 0;

		// completion par la gauche si nécéssaire
		if (chaine.startsWith(":")) {
			chaine = "00" + chaine;
		}

		// idem à droit
		if (chaine.endsWith(":")) {
			chaine = chaine + "00";
		}

		NSArray<String> str = NSArray.componentsSeparatedByString(chaine, ":");

		if (str.count() == 1) {
			nombre = Integer.parseInt(str.objectAtIndex(0)) * 60;
		} else {
			if ((((Number) Integer.valueOf((String) str.objectAtIndex(0))).intValue()) < 0) {
				nombre = (-((((Number) Integer.valueOf((String) str.objectAtIndex(0))).intValue()) * 60) + ((((Number) Integer.valueOf((String) str.objectAtIndex(1)))
						.intValue())));
			} else {
				nombre = (((((Number) Integer.valueOf((String) str.objectAtIndex(0))).intValue()) * 60) + ((((Number) Integer.valueOf((String) str.objectAtIndex(1)))
						.intValue())));
			}
		}

		// if ((((Number) Integer.valueOf((String) str.objectAtIndex(0))).intValue()) <
		// 0)
		if (str.objectAtIndex(0).startsWith("-")) {
			nombre = -nombre; // on a passe une valeur negative
		}

		return nombre;
	}

	// STRING FOR MINUTES
	// Formatte le nombre de minutes en une chaine au format %H:%M (l'inverse de
	// numberOfMinutesFor: )
	public static String stringForMinutes(int numberOfMinutes) {
		String formattedString;
		int hours, minutes;
		boolean negatif = false;

		if (numberOfMinutes == 0)
			return "00:00";

		if (numberOfMinutes < 0) {
			negatif = true;
			numberOfMinutes = -numberOfMinutes;
		}

		hours = numberOfMinutes / 60;
		minutes = numberOfMinutes % 60;

		if (hours < 10)
			formattedString = "0" + hours;
		else
			formattedString = "" + hours;

		if (minutes < 10)
			formattedString = formattedString + ":0" + minutes;
		else
			formattedString = formattedString + ":" + minutes;

		if (negatif)
			formattedString = "-" + formattedString;

		return formattedString;
	}

	/**
	 * retourne le nombre de minutes ecoulées dans la jouréee
	 * 
	 * @param date
	 * @return
	 */
	public static int getMinutesOfDay(NSTimestamp aDate) {

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(aDate);
		return calendar.get(Calendar.MINUTE) + calendar.get(Calendar.HOUR_OF_DAY) * 60;

	}

	/**
	 * mettre une date a minuit (le jour meme)
	 * 
	 * @param aDate
	 * @return
	 */
	public static NSTimestamp remonterAMinuit(NSTimestamp aDate) {
		GregorianCalendar nowGC = new GregorianCalendar();
		nowGC.setTime(aDate);
		return aDate.timestampByAddingGregorianUnits(
				0,
				0,
				0,
				-nowGC.get(GregorianCalendar.HOUR_OF_DAY),
				-nowGC.get(GregorianCalendar.MINUTE),
				-nowGC.get(GregorianCalendar.SECOND));
	}

	/**
	 * Transforme une durée en minutes en jours défini sur une duée type
	 * 
	 * @param minutesTotal
	 * @param minutesJour
	 * @return
	 */
	public static float getJoursForMinutes(int minutesTotal, int minutesJour) {
		float jours = (float) minutesTotal / (float) minutesJour;
		return jours;
	}

	/**
	 * Formatter correctement voir completer une chaine passée dans les textfields
	 * 
	 * @param chaine
	 * @return
	 */
	public static String formatter(String chaine, boolean isLimit24h) {
		String chaineFormattee = chaine;

		if (chaineFormattee == null) {
			chaineFormattee = "";
		}

		// récupération du signe
		boolean isNegatif = false;
		if (chaineFormattee.startsWith("-")) {
			isNegatif = true;
		}

		// remplacement des séparateurs
		chaineFormattee = StringCtrl.replace(chaineFormattee, ".", ":");
		chaineFormattee = StringCtrl.replace(chaineFormattee, ",", ":");
		chaineFormattee = StringCtrl.replace(chaineFormattee, ";", ":");
		chaineFormattee = StringCtrl.replace(chaineFormattee, "/", ":");
		chaineFormattee = StringCtrl.replace(chaineFormattee, "=", ":");

		// ne garder que les chiffres
		chaineFormattee = onlyKeep(chaineFormattee, "0123456789:");

		if (StringCtrl.isEmpty(chaineFormattee)) {
			chaineFormattee = "00:00";
		}

		// ne conserver qu'un seul séparateur
		if (StringCtrl.containsIgnoreCase(chaineFormattee, ":") == false) {
			if (chaineFormattee.length() == 1) {
				chaineFormattee = "0" + chaineFormattee + ":00";
			} else if (chaineFormattee.length() == 2) {
				chaineFormattee = chaineFormattee + ":00";
			} else if (chaineFormattee.length() == 3) {
				chaineFormattee = "0" + chaineFormattee.substring(0, 1) + ":" + chaineFormattee.substring(1, 3);
			} else {
				chaineFormattee = chaineFormattee.substring(0, chaineFormattee.length() - 2) +
						":" + chaineFormattee.substring(chaineFormattee.length() - 2, chaineFormattee.length());
			}
		} else if (stringOccur(chaineFormattee, ":") > 1) {
			int index = chaineFormattee.indexOf(":");
			chaineFormattee = chaineFormattee.substring(0, index) + chaineFormattee.substring(index + 1, chaineFormattee.length());
		}

		// completion si nécéssaire
		if (chaineFormattee.startsWith(":")) {

			chaineFormattee = "00" + chaineFormattee;

		} else {

			// 2 chiffres minimum avant les ":"
			NSArray<String> hm = NSArray.componentsSeparatedByString(chaineFormattee, ":");

			if (hm.count() == 0) {
				chaineFormattee = "00:" + chaineFormattee;
			}

			hm = NSArray.componentsSeparatedByString(chaineFormattee, ":");

			if (hm.objectAtIndex(0).length() == 1) {
				chaineFormattee = "0" + chaineFormattee;
			}

			// 2 chiffres exactement après les ":"
			if (hm.count() == 1) {
				chaineFormattee += "00";
			}

		}

		if (isNegatif) {
			chaineFormattee = "-" + chaineFormattee;
		}

		// retailler entre 00:00 et 23:59 si demandé
		if (isLimit24h) {
			int minutes = getMinutes(chaineFormattee);

			if (minutes < 0 || minutes > M_23_H_59) {

				if (minutes < 0) {
					minutes = 0;
				} else {
					minutes = M_23_H_59;
				}

				chaineFormattee = stringForMinutes(minutes);
			}

		}

		return chaineFormattee;
	}

	private final static int M_23_H_59 = 23 * 60 + 59;

	/**
	 * ne conserve que les caracteres de goodChar de la chaine. reduit la chaine
	 * ne consequence
	 * 
	 * @param chaine
	 * @param goodChar
	 * @return
	 */
	private static String onlyKeep(String chaine, String goodChar) {
		String newChaine = "";
		for (int i = 0; i < chaine.length(); i++) {
			String currentChar = "" + chaine.charAt(i);
			if (StringCtrl.like(goodChar, "*" + currentChar + "*") == true)
				newChaine += currentChar;
		}
		return newChaine;
	}

	/**
	 * Renvoie le nombre d'occurrences de la sous-chaine de caractères spécifiée
	 * dans la chaine de caractères spécifiée
	 * 
	 * @param text
	 *          chaine de caractères initiale
	 * @param string
	 *          sous-chaine de caractères dont le nombre d'occurrences doit etre
	 *          compté
	 * @return le nombre d'occurrences du pattern spécifié dans la chaine de
	 *         caractères spécifiée
	 */
	private static final int stringOccur(String text, String string) {
		return regexOccur(text, Pattern.quote(string));
	}

	/**
	 * Renvoie le nombre d'occurrences du pattern spécifié dans la chaine de
	 * caractères spécifiée
	 * 
	 * @param text
	 *          chaine de caractères initiale
	 * @param regex
	 *          expression régulière dont le nombre d'occurrences doit etre compté
	 * @return le nombre d'occurrences du pattern spécifié dans la chaine de
	 *         caractères spécifiée
	 */
	private static final int regexOccur(String text, String regex) {
		Matcher matcher = Pattern.compile(regex).matcher(text);
		int occur = 0;
		while (matcher.find()) {
			occur++;
		}
		return occur;
	}

}
