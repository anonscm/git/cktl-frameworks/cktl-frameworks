/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 *
 */
public class FixedGregorianCalendar extends GregorianCalendar {
  
	public FixedGregorianCalendar() {
		super();
		setFirstDayOfWeek(Calendar.MONDAY);
		setMinimalDaysInFirstWeek(4); // le premier Jeudi de Janvier...
  }
}
