/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import org.cocktail.fwkcktlgrh.common.Constantes;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Ensemble de méthodes utilitaires a remonter dans les fwk
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class GrhumFactory {

	private final static String LIBELLE_STATUT_INCONNU = "Inconnu";
	private final static String LIBELLE_STATUT_FONCTIONNAIRE = "Titulaire";
	private final static String LIBELLE_STATUT_CONTRACTUEL = "Non titulaire";
	private final static String LIBELLE_STATUT_CDI = "Contractuel en CDI";

	private final static int STATUT_INCONNU = -1;
	private final static int STATUT_FONCTIONNAIRE = 0;
	private final static int STATUT_CONTRACTUEL = 1;
	private final static int STATUT_CDI = 2;

	/**
	 * Donner le libellé du grade d'un agent - fonctionnaire : le grade s'il est
	 * trouvé, sinon le libellé "fonctionnaire" - contractuel : le libelle
	 * "contractuel" ou "cdi"
	 * 
	 * @return
	 */
	public static String getLibelleGradeForIndividu(EOPlanning eoPlanning) {
		return getLibelleGradeForIndividu(
				(EOIndividu) eoPlanning.toPersonne(), eoPlanning.toPeriode().perDDebut(), eoPlanning.toPeriode().perDFin());
	}

	/**
	 * Donner le libellé du grade d'un agent - fonctionnaire : le grade s'il est
	 * trouvé, sinon le libellé "fonctionnaire" - contractuel : le libelle
	 * "contractuel" ou "cdi"
	 * 
	 * @return
	 */
	public static String getLibelleGradeForIndividu(EOIndividu eoIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		String strStatut = LIBELLE_STATUT_INCONNU;

		int statut = getStatut(eoIndividu, dateDebut, dateFin);

		if (statut == STATUT_FONCTIONNAIRE) {

			strStatut = LIBELLE_STATUT_FONCTIONNAIRE;

			NSArray<EOElements> lesElementsCarriere = findElementCarriereForIndividuAndDateInContext(
					eoIndividu.editingContext(), eoIndividu, dateDebut, dateFin);

			// essayer de retrouver le grade de l'individu
			try {
				if (lesElementsCarriere.count() > 0) {
					strStatut = ((EOElements) lesElementsCarriere.lastObject()).toGrade().llGrade() + " (" + LIBELLE_STATUT_FONCTIONNAIRE + ")";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (statut == STATUT_CONTRACTUEL) {
			strStatut = LIBELLE_STATUT_CONTRACTUEL;

		} else if (statut == STATUT_CDI) {
			strStatut = LIBELLE_STATUT_CDI;
		}
		return strStatut;
	}
	
	/**
	 * Donner le libellé du corps d'un agent 
	 * 
	 * @return
	 */
	public static String getLibelleCorpsForIndividu(EOIndividu eoIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		String strCorps = LIBELLE_STATUT_INCONNU;

		NSArray<EOElements> lesElementsCarriere = findElementCarriereForIndividuAndDateInContext(
				eoIndividu.editingContext(), eoIndividu, dateDebut, dateFin);

		if (lesElementsCarriere.count() > 0) {
			strCorps = ((EOElements) lesElementsCarriere.lastObject()).toCorps().llCorps();
		}
		
		return strCorps;
	}
	
	

	private final static CktlSort ELEMENT_CARRIERE_SORT = CktlSort.newSort(EOElements.D_EFFET_ELEMENT_KEY + "," + EOElements.D_FIN_ELEMENT_KEY);

	/**
	 * liste des elements de carriere d'un individu pour une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param date
	 * @return
	 */
	public static NSArray<EOElements> findElementCarriereForIndividuAndDateInContext(
			EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		String strQual = null;
		NSArray args = null;
		if (dateFin != null) {
			strQual =
					EOElements.TO_INDIVIDU_KEY + "=%@ AND (" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " = nil) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " >= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " <= %@) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " >= %@) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " >= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " <= %@) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " >= %@ AND " + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " >= %@) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " >= %@ AND " + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " = nil)" +
							")";
			args = new NSArray(new Object[] {
					individu,
					dateDebut,
					dateDebut, dateFin,
					dateDebut, dateFin,
					dateDebut, dateDebut, dateFin,
					dateDebut, dateFin, dateFin,
					dateDebut, dateFin });
		} else {
			strQual =
					"toIndividu = %@ AND (" +
							"(" + EOElements.D_FIN_ELEMENT_KEY + " = nil) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " <= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " >= %@) OR" +
							"(" + EOElements.D_EFFET_ELEMENT_KEY + " >= %@ AND " + EOElements.D_FIN_ELEMENT_KEY + " >= %@)" +
							")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}

		return EOElements.fetchAll(ec, CktlDataBus.newCondition(strQual, args), ELEMENT_CARRIERE_SORT);
	}

	/**
	 * Determiner le statut d'un agent sur une période.
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private static int getStatut(EOIndividu eoIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray lesElementsCarriere = findElementCarriereForIndividuAndDateInContext(
				eoIndividu.editingContext(), eoIndividu, dateDebut, dateFin);
		// contractuel ou (fonctionnaire ou fonctionnaire stagiaire)
		int statut = STATUT_INCONNU;
		if (lesElementsCarriere.count() > 0) {
			statut = STATUT_FONCTIONNAIRE;
		} else {
			statut = STATUT_CONTRACTUEL;
			if (isContractuelCDI(eoIndividu, dateDebut, dateFin)) {
				statut = STATUT_CDI;
			}
		}
		return statut;
	}

	/**
	 * Indique si l'agent est en contrat sur la période de l'affectation annuelle
	 * 
	 * @return
	 */
	public static boolean isContractuel(EOPlanning eoPlanning) {
		return isContractuel(eoPlanning.editingContext(), eoPlanning.toPersonne(), eoPlanning.toPeriode().perDDebut(), eoPlanning.toPeriode().perDFin());
	}

	/**
	 * Indique si l'agent est en contrat sur la période de l'affectation annuelle
	 * 
	 * @return
	 */
	public static boolean isContractuel(EOEditingContext ec, IPersonne personne, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray lesElementsCarriere = findElementCarriereForIndividuAndDateInContext(
				ec, (EOIndividu) personne, dateDebut, dateFin);
		return lesElementsCarriere.count() == 0;
	}
	
	/**
	 * Indique si l'agent est en CDI sur la période de l'affectation annuelle
	 * 
	 * @return
	 */
	public static boolean isContractuelCDI(EOPlanning eoPlanning) {
		return isContractuelCDI(
				(EOIndividu) eoPlanning.toPersonne(), eoPlanning.toPeriode().perDDebut(), eoPlanning.toPeriode().perDFin());
	}

	/**
	 * Indique si l'agent est en CDI sur la période de l'affectation annuelle
	 * 
	 * @return
	 */
	private static boolean isContractuelCDI(EOIndividu eoIndividu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSArray contrats = findContratForIndividuAndDateInContext(
				eoIndividu.editingContext(), eoIndividu, dateDebut, dateFin);

		EOQualifier qualCdi = ERXQ.equals(EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY + "." + EOTypeContratTravail.TEM_CDI_KEY, Constantes.VRAI);

		return EOQualifier.filteredArrayWithQualifier(contrats, qualCdi).count() > 0;
	}

	private final static CktlSort CONTRAT_SORT = CktlSort.newSort(EOContrat.D_FIN_CONTRAT_TRAV_KEY);

	/**
	 * liste des contrats d'un individu pour une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param date
	 * @return
	 */
	public static NSArray<EOContrat> findContratForIndividuAndDateInContext(
			EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		String strQual = null;
		NSArray args = null;
		if (dateFin != null) {
			strQual =
					EOContrat.TO_INDIVIDU_KEY + "=%@ AND (" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " = nil) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " >= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " <= %@) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " >= %@) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " >= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " <= %@) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " >= %@ AND " + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " >= %@) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " >= %@ AND " + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " = nil)" +
							")";
			args = new NSArray(new Object[] {
					individu,
					dateDebut,
					dateDebut, dateFin,
					dateDebut, dateFin,
					dateDebut, dateDebut, dateFin,
					dateDebut, dateFin, dateFin,
					dateDebut, dateFin });
		} else {
			strQual =
					"toIndividu = %@ AND (" +
							"(" + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " = nil) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " <= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " >= %@) OR" +
							"(" + EOContrat.D_DEB_CONTRAT_TRAV_KEY + " >= %@ AND " + EOContrat.D_FIN_CONTRAT_TRAV_KEY + " >= %@)" +
							")";
			args = new NSArray(new Object[] { individu, dateDebut, dateDebut, dateDebut, dateDebut });
		}

		return EOContrat.fetchAll(ec, CktlDataBus.newCondition(strQual, args), CONTRAT_SORT);
	}

}
