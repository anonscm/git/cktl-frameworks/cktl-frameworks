/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;

import com.webobjects.foundation.NSNumberFormatter;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class HamacMinuteToJourFormatter
		extends Format {

	private final static String STR_NF_DUREE_JOUR = "0.00";
	private final static NSNumberFormatter NF_DUREE_JOUR = new NSNumberFormatter(STR_NF_DUREE_JOUR);

	// durée par défaut de la journée
	private int dureeJour;
	
	public HamacMinuteToJourFormatter(HamacApplicationUser user) {
		super();
		if (user != null) {
			dureeJour = user.getDureeJourneeAffichage();
		}
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer stringbuffer, FieldPosition fieldposition) {
		return format(obj, stringbuffer, fieldposition, getDureeJour());
	}

	/**
	 * @param obj
	 * @param stringbuffer
	 * @param fieldposition
	 * @param dureeJour
	 * @return
	 */
	public static StringBuffer format(Object obj, StringBuffer stringbuffer, FieldPosition fieldposition, int dureeJour) {
		if (obj instanceof Number) {
			Number number = ((Number) obj);
			int minutes = number.intValue();

			Float jours = new Float(TimeCtrl.getJoursForMinutes(minutes, dureeJour));

			// n'afficher que si c'est non vide
			if (jours.floatValue() != (float) 0.0) {
				// formater 2 chiffres après la virgule
				String strJours = NF_DUREE_JOUR.format(jours);
				stringbuffer.append(strJours + "j");
			}

			return stringbuffer;
		} else {
			throw new IllegalArgumentException("Cannot format given Object as a Number");
		}
	}

	/*
	 * TODO à faire si nécéssaire ?? (non-Javadoc)
	 * 
	 * @see java.text.Format#parseObject(java.lang.String,
	 * java.text.ParsePosition)
	 */
	@Override
	public Object parseObject(String s, ParsePosition parseposition) {
		if (s == null) {
			return null;
		}
		String[] sArray = s.split(":");
		int res = Integer.decode(sArray[0]).intValue();
		if (sArray.length > 1) {
			res = (res * 60) + Integer.decode(sArray[1]).intValue();
		}
		return Integer.valueOf(res);
	}

	public final int getDureeJour() {
		return dureeJour;
	}

	public final void setDureeJour(int value) {
		dureeJour = value;
	}

}
