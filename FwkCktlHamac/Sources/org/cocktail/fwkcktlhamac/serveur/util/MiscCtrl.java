/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classes utilitaires diverses
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class MiscCtrl {

	//
	/**
	 * arrondir à l'entier inférieur ou supérieur.
	 * 
	 * n < x < n+0,5 => n n+0,5 < x < n+1 => n+1
	 * 
	 */
	public final static int arrondi(double valeur) {
		return (int) Math.floor(valeur + 0.5);
	}

	/**
	 * 
	 * @throws Exception
	 */
	public final static boolean sauvegarde(EOEditingContext ec)
			throws Exception {
		try {
			ec.lock();
			ec.saveChanges();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			ec.revert();
			throw e;
		} finally {
			ec.unlock();
		}
	}

}
