/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Modification liées à {@link EOStructure}
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class EOStructureFixed {

	/**
	 * Donne la composante d'une structure sans boucle infinie
	 * 
	 * @param eoStructure
	 * @return
	 */
	public final static EOStructure toComposante(EOStructure eoStructure) {
		EOStructure composante = null;

		if (eoStructure.isComposante() ||
				eoStructure == eoStructure.toStructurePere() ||
				// ajout type etablissement (cas IUT)
				eoStructure.isEtablissement()) {
			composante = eoStructure;
		} else {
			composante = toComposante(eoStructure.toStructurePere());
		}

		return composante;
	}

	/**
	 * XXX mémoriser la structure dans le contexte pour éviter les accès multiples
	 * répétés à la base (cause accès via FwkPersonne). A surveiller si ça pète
	 * pas avec le temps ...
	 * 
	 * @param ec
	 * @param cStructure
	 * @return
	 */
	public final static EOStructure getEoStructureInContext(
			EOEditingContext editingContext, String cStructure) {
		EOStructure eoStructure = (EOStructure) editingContext.userInfo().objectForKey(cStructure);

		if (eoStructure == null) {
			eoStructure = EOStructure.structurePourCode(
					editingContext, cStructure);
			editingContext.setUserInfoForKey(eoStructure, cStructure);
		}

		return eoStructure;
	}

	/**
	 * Affiche le nom de la structure et sa composante
	 * 
	 * @param eoStructure
	 * @return
	 */
	public final static String getAffichageAvecComposante(EOStructure eoStructure) {
		String str = "";

		str += eoStructure.lcStructure();

		EOStructure eoComposante = toComposante(eoStructure);
		if (eoComposante != null) {
			str += " (" + eoComposante.lcStructure() + ")";
		}

		return str;
	}

}
