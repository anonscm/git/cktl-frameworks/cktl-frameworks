/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class HamacMinuteToHeureFormatter
		extends Format {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer,
	 * java.text.FieldPosition)
	 */
	@Override
	public StringBuffer format(Object obj, StringBuffer stringbuffer, FieldPosition fieldposition) {
		if (obj instanceof Number) {
			Number number = ((Number) obj);
			int minutes = number.intValue();

			stringbuffer.append(TimeCtrl.stringForMinutes(minutes));

			return stringbuffer;
		} else {
			throw new IllegalArgumentException("Cannot format given Object as a Number");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.text.Format#parseObject(java.lang.String,
	 * java.text.ParsePosition)
	 */
	@Override
	public Object parseObject(String s, ParsePosition parseposition) {
		if (s == null) {
			return null;
		}
		String[] sArray = s.split(":");
		int res = Integer.decode(sArray[0]).intValue();
		if (sArray.length > 1) {
			res = (res * 60) + Integer.decode(sArray[1]).intValue();
		}
		return Integer.valueOf(res);
	}

}
