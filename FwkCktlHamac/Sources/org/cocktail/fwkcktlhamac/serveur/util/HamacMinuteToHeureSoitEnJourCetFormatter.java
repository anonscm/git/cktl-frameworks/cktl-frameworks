/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import java.text.FieldPosition;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;

/**
 * Pour affichage des valeurs du CET du type : 21:00 (soit 3j. à 7:00)
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HamacMinuteToHeureSoitEnJourCetFormatter
		extends HamacMinuteToJourCetFormatter {

	public HamacMinuteToHeureSoitEnJourCetFormatter(EOPeriode periode) {
		super(periode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer,
	 * java.text.FieldPosition)
	 */
	@Override
	public StringBuffer format(Object obj, StringBuffer stringbuffer, FieldPosition fieldposition) {
		if (obj instanceof Number) {
			Number number = ((Number) obj);
			int minutes = number.intValue();

			stringbuffer.append(FormatterCtrl.getAffichageHeuresSoitEnJourAXHeures(minutes, getDureeJour()));

			return stringbuffer;
		} else {
			throw new IllegalArgumentException("Cannot format given Object as a Number : " + obj);
		}
	}

}
