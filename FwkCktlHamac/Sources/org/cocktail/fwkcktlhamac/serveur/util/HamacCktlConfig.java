/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Methode utilitaire pour accèder facilement aux paramètres directement depuis
 * le framework.
 * 
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 */
public class HamacCktlConfig {

	private final static String APP_CONFIG_KEY = "hamacCktlConfig";
	private final static String RESET_APP_CONFIG_KEY = "resetHamacCktlConfig";

	private static CktlConfig _config;

	private final static String DEFAULT_ARRAY_SEPARATOR = ",";

	/**
	 * @return
	 */
	private static CktlWebApplication app() {
		return (CktlWebApplication) CktlWebApplication.application();
	}

	/**
	 * 
	 */
	public static void reset() {
		_config = null;
		app().valueForKey(RESET_APP_CONFIG_KEY);
	}

	/**
	 * @return
	 */
	private static CktlConfig getConfig() {
		if (_config == null) {
			if (app() != null) {
				_config = (CktlConfig) app().valueForKey(APP_CONFIG_KEY);
			}
		}
		return _config;
	}

	/**
	 * 
	 */
	public static int intForKey(String key) {
		int result = 0;

		if (getConfig() != null) {
			result = getConfig().intForKey(key);
		}

		return result;
	}

	/**
	 * Valeur int. Valeur par défaut redéfinissable.
	 */
	public static int intForKey(String key, int valueIfNull) {
		int result = 0;

		if (!StringCtrl.isEmpty(stringForKey(key))) {
			result = intForKey(key);
		} else {
			result = valueIfNull;
		}

		return result;
	}

	/**
	 * Valeur boolean. Valeur par défaut <code>false</code>
	 */
	public static boolean booleanForKey(String key) {
		boolean result = false;

		result = getConfig().booleanForKey(key);

		return result;
	}

	/**
	 * Valeur boolean. Valeur par défaut redéfinissable.
	 */
	public static boolean booleanForKey(String key, boolean valueIfNull) {
		boolean result = false;

		if (!StringCtrl.isEmpty(stringForKey(key))) {
			result = booleanForKey(key);
		} else {
			result = valueIfNull;
		}

		return result;
	}

	/**
	 * 
	 */
	public static boolean booleanForKey(String key, EOPeriode eoPeriode) {
		boolean result = false;

		result = booleanForKey(getKeyAnnualisee(key, eoPeriode));

		return result;
	}
	
	
	/**
	 * 
	 */
	public static String stringForKey(String key) {
		String result = null;

		result = getConfig().stringForKey(key);

		return result;
	}

	/**
	 * 
	 */
	public static float floatForKey(String key) {
		float result = (float) 0;

		String strValue = stringForKey(key);
		if (strValue != null) {
			result = Float.parseFloat(strValue);
		}

		return result;
	}

	/**
	 * 
	 */
	public static NSTimestamp dateForKey(String key) {
		NSTimestamp result = null;

		String strDate = getConfig().stringForKey(key);

		if (!StringCtrl.isEmpty(strDate)) {
			result = DateCtrlHamac.stringToDate(getConfig().stringForKey(key));
		}

		return result;
	}

	// gestion des paramètres annualisés

	public final static String getKeyAnnualisee(String key, EOPeriode eoPeriode) {
		return key + "." + eoPeriode.getValeurParametreAnnualise();
	}

	/**
	 * 
	 */
	public static int intForKey(String key, EOPeriode eoPeriode) {
		int result = 0;

		result = intForKey(getKeyAnnualisee(key, eoPeriode));

		return result;
	}

	/**
	 * 
	 */
	public static int intForKey(String key, EOPeriode eoPeriode, int valueIfNull) {
		int result = 0;

		String keyAnnualisee = getKeyAnnualisee(key, eoPeriode);

		if (!StringCtrl.isEmpty(stringForKey(keyAnnualisee))) {
			result = intForKey(keyAnnualisee);
		} else {
			result = valueIfNull;
		}

		return result;
	}

	/**
	 * 
	 */
	public static String stringForKey(String key, EOPeriode eoPeriode) {
		String result = "";

		result = stringForKey(getKeyAnnualisee(key, eoPeriode));

		return result;
	}

	/**
	 * 
	 */
	public static float floatForKey(String key, EOPeriode eoPeriode) {
		float result = 0;

		result = floatForKey(getKeyAnnualisee(key, eoPeriode));

		return result;
	}

	/**
	 * 
	 */
	public static NSTimestamp dateForKey(String key, EOPeriode eoPeriode, NSTimestamp valueIfNull) {
		NSTimestamp result = null;

		result = dateForKey(getKeyAnnualisee(key, eoPeriode));

		if (result == null) {
			result = valueIfNull;
		}

		return result;
	}

	/**
	 * Attention, les espaces sont supprimés !
	 * 
	 * @param key
	 * @param separator
	 * @return
	 */
	private static NSArray<String> arrayForKey(String key, String separator, EOPeriode periode) {

		NSArray<String> array = new NSArray<String>();

		String str = stringForKey(key, periode);
		if (!StringCtrl.isEmpty(str)) {
			array = NSArray.componentsSeparatedByString(str.trim(), separator);
		}

		return array;
	}

	public static NSArray<String> arrayForKey(String key, EOPeriode periode) {
		return arrayForKey(key, DEFAULT_ARRAY_SEPARATOR, periode);
	}

}
