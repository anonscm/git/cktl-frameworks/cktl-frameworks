/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.util;

import org.cocktail.fwkcktlhamac.serveur.HamacApplicationUser;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public class FormatterCtrl {

	// convertisseurs

	private final static HamacMinuteToHeureFormatter _minuteToHeureFormatter;
	
	static {
		_minuteToHeureFormatter = new HamacMinuteToHeureFormatter();
	}

	public final static HamacMinuteToHeureFormatter getMinuteToHeureFormatter() {
		return _minuteToHeureFormatter;
	}

	public final static HamacMinuteToJourFormatter getNewMinuteToJourFormatter(HamacApplicationUser user) {
		return new HamacMinuteToJourFormatter(user);
	}
	
	public final static HamacMinuteToJourCetFormatter getNewMinuteToJourCetFormatter(EOPeriode periode) {
		return new HamacMinuteToJourCetFormatter(periode);
	}
	
	public final static HamacMinuteToHeureSoitEnJourCetFormatter getNewMinuteToHeureSoitEnJourCetFormatter(EOPeriode periode) {
		return new HamacMinuteToHeureSoitEnJourCetFormatter(periode);
	}

	/**
	 * 
	 * @param minutes
	 * @param dureeJourMinute
	 * @return
	 */
	public final static String getAffichageSoitEnJourAXHeures(int minutes, int dureeJourMinute) {
		StringBuffer sb = new StringBuffer();
		sb = HamacMinuteToJourFormatter.format(
				Integer.valueOf(minutes), sb, null, dureeJourMinute);

		sb.insert(0, "soit ");
		sb.append(" à ").append(TimeCtrl.stringForMinutes(dureeJourMinute));

		return sb.toString();
	}

	/**
	 * Méthode interne pour l'affichage. "17h30 (soit 2.5j à 7h00)"
	 * 
	 * @param minutes
	 * @param dureeJourMinute
	 * @return
	 */
	public final static String getAffichageHeuresSoitEnJourAXHeures(int minutes, int dureeJourMinute) {
		return TimeCtrl.stringForMinutes(minutes) + " (" +
				getAffichageSoitEnJourAXHeures(minutes, dureeJourMinute) + ")";
	}

	/**
	 * Nombre de minutes en jours, arrondi à l'entier inférieur
	 * 
	 * @param minutes
	 * @param dureeJour
	 * @return
	 */
	public final static int enJourArrondiEntierInferieur(int minutes, int dureeJourMinutes) {
		return minutes / dureeJourMinutes;
	}

	/**
	 * Nombre de minutes en jours, arrondi au deuxième chiffre après la virgule
	 * 
	 * @param minutes
	 * @param dureeJour
	 * @return
	 */
	public final static float enJourArrondi2ChiffresApresVirgule(int minutes, int dureeJourMinutes) {

		if (minutes != 0) {
			StringBuffer sb = new StringBuffer();
			sb = HamacMinuteToJourFormatter.format(Integer.valueOf(minutes), sb, null, dureeJourMinutes);
			return Float.parseFloat(sb.toString().substring(0, sb.toString().lastIndexOf("j")));
		} else {
			return (float) 0;
		}

	}

	public final static String getLibelleCourt(String libelleLong, int max) {
		String libelleCourt = libelleLong;

		if (StringCtrl.isEmpty(libelleCourt) == false) {
			libelleCourt = StringCtrl.cut(libelleCourt, max);

			if (libelleCourt.length() != libelleLong.length()) {
				libelleCourt += " (...)";
			}
		}

		return libelleCourt;
	}
}
