/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.stat;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.EOSolde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSTimestamp;

/**
 * Conteneur pour les heures supplémentaires
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class HeuresSupplementairesContainer
		extends A_TableauDeBordContainer {

	private int statHeuresSuppRestantes;

	/**
	 * @param eoPlanning
	 */
	public HeuresSupplementairesContainer(EOPlanning eoPlanning) {
		super(eoPlanning);
	}

	@Override
	protected void calculerStat() {

		statHeuresSuppRestantes = 0;
		Planning planning = getEoPlanning().getPlanning();
		NSTimestamp datePremierJour = planning.getPremierJour();
		NSTimestamp dateButoir = DateCtrlHamac.dateToFinAnneeCivile(datePremierJour);

		for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

			String cStructure = eoStructure.cStructure();

			I_Solde soldeBalance = getEoPlanning().getSoldeBalanceNatif(cStructure);

			if (soldeBalance != null) {
			
				Integer restant = soldeBalance.getSoldeDelegate().getRestantAu(dateButoir);
				
				if (HamacCktlConfig.booleanForKey(EOParametre.REPORT_HSUP_ACTIF, getEoPlanning().toPeriode())) {
				
					EOSolde soldeBalancePrev = getEoPlanning().getEoSoldeBalanceDernier(cStructure);
					
					if (soldeBalancePrev != null && soldeBalancePrev.restant() != null && soldeBalancePrev.restant() > 0) {
						restant += soldeBalancePrev.restant();
					}
				
				}
				
				if (restant != null && restant != 0) {
					statHeuresSuppRestantes += restant;
				}
				
			}
			
			

		}

	}

	public final int getStatHeuresSuppRestantes() {
		return statHeuresSuppRestantes;
	}

	public final float gettStatHeuresSuppEnJour() {
		return ((float) getStatHeuresSuppRestantes()) / (float) getDureeJour();
	}

	public final float getMontantAProvisionnerBrut() {
		float montant = (float) 0.0;

		if (getSalaireBrutJournalier() != null) {
			montant = gettStatHeuresSuppEnJour() * getSalaireBrutJournalier().floatValue();

			// pas de négatif
			if (montant < (float) 0.0) {
				montant = (float) 0.0;
			}

		}

		return montant;
	}

	public final float getMontantAProvisionnerCharge() {
		return (getMontantAProvisionnerBrut() * getTauxDeCharge().floatValue()) / (float) 100.0;
	}

}
