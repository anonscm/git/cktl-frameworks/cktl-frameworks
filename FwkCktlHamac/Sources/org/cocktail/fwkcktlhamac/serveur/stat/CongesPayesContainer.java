/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.stat;

import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.I_Solde;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.Planning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSTimestamp;

/**
 * Conteneur pour les congés payés
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class CongesPayesContainer
		extends A_TableauDeBordContainer {

	private int statDureeAssociee;
	private int statConsommationConge;
	private int statConsommationReliquat;
	private int statHeuresDues;
	private int statReliquatsRestants;

	/**
	 * @param eoPlanning
	 */
	public CongesPayesContainer(EOPlanning eoPlanning) {
		super(eoPlanning);
	}

	@Override
	protected void calculerStat() {

		statDureeAssociee = 0;
		statConsommationConge = 0;
		statConsommationReliquat = 0;
		statHeuresDues = 0;
		statReliquatsRestants = 0;

		Planning planning = getEoPlanning().getPlanning();
		NSTimestamp datePremierJour = planning.getPremierJour();
		NSTimestamp dateButoir = DateCtrlHamac.dateToFinAnneeCivile(datePremierJour);

		for (EOStructure eoStructure : planning.getEoStructureAttenduArray()) {

			String cStructure = eoStructure.cStructure();

			statDureeAssociee += planning.dureeComptabiliseeMinutes(cStructure, datePremierJour, dateButoir);

			I_Solde soldeConge = getEoPlanning().getSoldeCongeNatif(cStructure);
			statConsommationConge += soldeConge.getSoldeDelegate().getConsommationAu(dateButoir);

			I_Solde soldeReliquat = getEoPlanning().getSoldeReliquatNatif(cStructure);

			statHeuresDues += planning.getCalculDroitCongeDelegate().getDuMinutes(cStructure, dateButoir);

			try {
				statReliquatsRestants += soldeReliquat.getSoldeDelegate().getRestantAu(dateButoir);
			} catch (Exception e) {
				// ceux qui n'ont pas de planning osef ...
			}

		}

	}

	public final int getStatDureeAssociee() {
		return statDureeAssociee;
	}

	public final int getStatConsommationConge() {
		return statConsommationConge;
	}

	public final int getStatConsommationReliquat() {
		return statConsommationReliquat;
	}

	public final int getConsommationCongeEtReliquat() {
		return getStatConsommationConge() + getStatConsommationReliquat();
	}

	public final int getStatHeuresDues() {
		return statHeuresDues;
	}

	public final int getCongesGeneres() {
		return getStatDureeAssociee() - getStatHeuresDues();
	}

	public final int getStatReliquatsRestants() {
		return statReliquatsRestants;
	}

	public final int getCongesRestants() {
		return getCongesGeneres() + getStatReliquatsRestants() - getConsommationCongeEtReliquat();
	}

	private final float getCongesRestantsEnJour() {
		return ((float) getCongesGeneres() + getStatReliquatsRestants() - getConsommationCongeEtReliquat()) / (float) getDureeJour();
	}

	public final float getCongesRestantsEnJourEntierInferieur() {
		return (float) ((int) getCongesRestantsEnJour());
	}

	public final float getMontantAProvisionnerBrut() {
		float montant = (float) 0.0;

		if (getSalaireBrutJournalier() != null) {
			montant = getCongesRestantsEnJourEntierInferieur() * getSalaireBrutJournalier().floatValue();

			// pas de négatif
			if (montant < (float) 0.0) {
				montant = (float) 0.0;
			}

		}

		return montant;
	}

	public final float getMontantAProvisionnerCharge() {
		return (getMontantAProvisionnerBrut() * getTauxDeCharge().floatValue()) / (float) 100.0;
	}
}
