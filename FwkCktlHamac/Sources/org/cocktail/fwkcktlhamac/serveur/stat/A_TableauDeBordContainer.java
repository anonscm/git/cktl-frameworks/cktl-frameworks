/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.stat;

import java.math.BigDecimal;
import java.util.Hashtable;

import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.util.DateCtrlHamac;
import org.cocktail.fwkcktlhamac.serveur.util.GrhumFactory;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Conteneur génériques pour les statistiques sur les agents
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public abstract class A_TableauDeBordContainer {

	private final static String NO_INDIVIDU_KEY = "noIndividu";
	private final static String DATE_KEY = "date";

	private final static String STR_REQUETE_INDICE = "SELECT /*i.no_individu, indice.c_indice_brut, */indice.c_indice_majore" +
			"	FROM GRHUM.INDIVIDU_ULR i" +
			"	INNER JOIN MANGUE.carriere c ON i.no_individu = c.no_dossier_pers" +
			"	INNER JOIN MANGUE.element_carriere ec ON ec.no_dossier_pers = c.no_dossier_pers AND ec.no_seq_carriere = c.no_seq_carriere" +
			"	INNER JOIN GRHUM.passage_echelon pe ON pe.c_grade = ec.c_grade AND pe.c_echelon = ec.c_echelon" +
			"	INNER JOIN GRHUM.indice indice ON indice.c_indice_brut = pe.c_indice_brut AND indice.D_MAJORATION < TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy') AND (indice.D_FERMETURE > TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy') OR indice.D_FERMETURE IS NULL)" +
			"	WHERE i.no_individu = %" + NO_INDIVIDU_KEY + "%" +
			"	AND indice.d_fermeture IS NULL" +
			"	AND ec.tem_valide = 'O'" +
			"	AND ec.d_effet_element <= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			"	AND" +
			"	  (" +
			"	     ec.d_fin_element  IS NULL " +
			" OR ec.d_fin_element >= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy') " +
			" ) ";

	private final static String STR_REQUETE_INDICE_CHEVRON = "SELECT /*i.no_individu, indice.c_indice_brut, */indice.c_indice_majore " +
			"FROM GRHUM.INDIVIDU_ULR i" +
			"	INNER JOIN MANGUE.carriere c ON i.no_individu = c.no_dossier_pers" +
			"	INNER JOIN MANGUE.element_carriere ec ON ec.no_dossier_pers = c.no_dossier_pers AND ec.no_seq_carriere = c.no_seq_carriere" +
			"	INNER JOIN GRHUM.passage_chevron pc ON pc.c_grade = ec.c_grade AND pc.c_echelon = ec.c_echelon AND pc.c_chevron = ec.c_chevron" +
			"	INNER JOIN GRHUM.indice indice ON indice.c_indice_brut = pc.c_indice_brut" +
			"	WHERE i.no_individu = %" + NO_INDIVIDU_KEY + "%" +
			"	AND indice.d_fermeture IS NULL" +
			"	AND ec.tem_valide = 'O'" +
			"	AND ec.d_effet_element <= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			"	AND" +
			"	  (" +
			"	     ec.d_fin_element  IS NULL" +
			"	     OR ec.d_fin_element >= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			"	     )   ";

	private final static String STR_REQUETE_INDICE_CONTRACTUEL = "SELECT /*i.no_individu, */ca.indice_contrat" +
			" FROM GRHUM.INDIVIDU_ULR i" +
			" INNER JOIN MANGUE.CONTRAT c ON i.no_individu = c.no_dossier_pers" +
			" INNER JOIN MANGUE.CONTRAT_AVENANT ca ON ca.no_seq_contrat = c.no_seq_contrat " +
			" WHERE i.no_individu = %" + NO_INDIVIDU_KEY + "%" +
			" AND c.tem_annulation = 'N'" +
			" AND ca.tem_annulation = 'N'" +
			" AND ca.d_deb_contrat_av <= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			" AND" +
			"  (" +
			"     ca.d_fin_contrat_av  IS NULL" +
			"    OR ca.d_fin_contrat_av >= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			"  )   ";
	private final static String STR_REQUETE_SALAIRE_CONTRACTUEL = "SELECT /*i.no_individu, */ca.montant" +
			" FROM GRHUM.INDIVIDU_ULR i" +
			" INNER JOIN MANGUE.CONTRAT c ON i.no_individu = c.no_dossier_pers" +
			" INNER JOIN MANGUE.CONTRAT_AVENANT ca ON ca.no_seq_contrat = c.no_seq_contrat " +
			" WHERE i.no_individu = %" + NO_INDIVIDU_KEY + "%" +
			" AND c.tem_annulation = 'N'" + " AND ca.tem_annulation = 'N'" +
			" AND ca.d_deb_contrat_av <= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			" AND" + "  (" + "     ca.d_fin_contrat_av  IS NULL" +
			"    OR ca.d_fin_contrat_av >= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')"
			+ "  )   ";

	private final static String STR_REQUETE_CATEGORIE = "SELECT /*i.no_individu, g.c_grade,*/ g.c_categorie/*, g.lc_grade*/" +
			" FROM GRHUM.INDIVIDU_ULR i " +
			" INNER JOIN MANGUE.carriere c ON i.no_individu = c.no_dossier_pers" +
			" INNER JOIN MANGUE.element_carriere ec ON ec.no_dossier_pers = c.no_dossier_pers AND ec.no_seq_carriere = c.no_seq_carriere " +
			" INNER JOIN GRHUM.grade g ON g.c_grade = ec.c_grade " +
			" WHERE i.no_individu =  %" + NO_INDIVIDU_KEY + "% " +
			" AND ec.tem_valide = 'O'" +
			" AND ec.d_effet_element <= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy') " +
			" AND " +
			" (" +
			"     ec.d_fin_element  IS NULL" +
			"    OR ec.d_fin_element >= TO_DATE('%" + DATE_KEY + "%', 'dd/mm/yyyy')" +
			"  ) ";

	private EOPlanning eoPlanning;
	private Boolean isContractuel = null;
	private String statut;
	private String categorie;
	private Float tauxDeCharge;
	private Integer inm;
	private Float forfait;
	private Float salaireBrutJournalier;
	private Float coutMoyenJournalierCharge;

	/**
	 * 
	 */
	public A_TableauDeBordContainer(EOPlanning eoPlanning) {
		super();
		this.eoPlanning = eoPlanning;
		calculerStatCommune();
		calculerStat();

	}

	private final static Object getResultFromSQL(EOEditingContext ec, String sql) {

		Object result = null;

		NSArray<NSDictionary<String, Object>> dicoArray = EOUtilities.rawRowsForSQL(
				ec, "FwkCktlPersonne", sql, null);

		for (NSDictionary<String, Object> dico : dicoArray) {
			for (String key : dico.allKeys()) {
				Object value = dico.objectForKey(key);
				if (value != NSKeyValueCoding.NullValue) {
					result = dico.objectForKey(key);
				}
			}
		}

		return result;
	}

	private final void calculerStatCommune() {
		isContractuel = Boolean.valueOf(GrhumFactory.isContractuel(getEoPlanning()));
		if (isContractuel()) {
			statut = "CON";
			tauxDeCharge = HamacCktlConfig.floatForKey(
					EOParametre.STAT_TAUX_CHARGE_CONTRACTUEL, getEoPlanning().toPeriode());
		} else {
			statut = "TIT";
			tauxDeCharge = HamacCktlConfig.floatForKey(
					EOParametre.STAT_TAUX_CHARGE_FONCTIONNAIRE, getEoPlanning().toPeriode());
		}

		Hashtable<String, String> dico = new Hashtable<String, String>();
		dico.put(NO_INDIVIDU_KEY, Integer.toString(getEoPlanning().toPersonne().getNumeroInt().intValue()));
		dico.put(DATE_KEY, DateCtrlHamac.dateToString(DateCtrlHamac.date1erJanAnneeUniv(getEoPlanning().toPeriode().perDDebut())));

		// inm carriere
		String requete = StringCtrl.replaceWithDico(STR_REQUETE_INDICE, dico);
		Object object = getResultFromSQL(getEoPlanning().editingContext(), requete);

		// System.out.println("requete=" + requete);

		// Object object = getResultFromSQL(
		// getEoPlanning().editingContext(),
		// "select GRHUM.CHERCHER_INDICE_INDIVIDU (" +
		// getEoPlanning().toPersonne().getNumeroInt().intValue() + ") from dual");

		if (object != null &&
				object != NSKeyValueCoding.NullValue) {
			inm = Integer.valueOf(((Long) object).intValue());
		}

		// pas trouvé, on cherche dans les chevrons
		if (inm == null) {

			requete = StringCtrl.replaceWithDico(STR_REQUETE_INDICE_CHEVRON,
					dico);
			object = getResultFromSQL(getEoPlanning().editingContext(), requete);

			if (object != null &&
					object != NSKeyValueCoding.NullValue) {
				inm = Integer.valueOf(((Long) object).intValue());
			}

		}

		// pas trouvé, on cherche dans les contractuel
		if (inm == null) {

			requete = StringCtrl.replaceWithDico(STR_REQUETE_INDICE_CONTRACTUEL,
					dico);
			object = getResultFromSQL(getEoPlanning().editingContext(), requete);

			if (object != null &&
					object != NSKeyValueCoding.NullValue) {
				inm = Integer.valueOf(Integer.parseInt((String) object));
			}

		}

		// forfait brut si pas d'inm trouvé
		if (inm == null) {
			requete = StringCtrl.replaceWithDico(STR_REQUETE_SALAIRE_CONTRACTUEL,
						dico);
			object = getResultFromSQL(getEoPlanning().editingContext(), requete);

			if (object != null &&
						object != NSKeyValueCoding.NullValue) {
				forfait = new Float(((BigDecimal) object).floatValue());
			}
		}

		// requete = StringCtrl.replaceWithDico(STR_REQUETE_CATEGORIE, dico);
		// object = getResultFromSQL(getEoPlanning().editingContext(), requete);
		//
		object = getResultFromSQL(
				getEoPlanning().editingContext(),
				"select c_categorie from grhum.corps where c_corps=substr(MANGUE.get_c_grade_Agent_date("
						+ getEoPlanning().toPersonne().getNumeroInt().intValue() + ", " +
								"to_date('" + DateCtrlHamac.dateToString(DateCtrlHamac.date1erJanAnneeUniv(getEoPlanning().toPeriode().perDDebut())) + "', 'dd/mm/yyyy')),1,3)");
		if (object != null &&
				object != NSKeyValueCoding.NullValue) {
			categorie = (String) object;
		}

		salaireBrutJournalier = calculerSalaireBrutJournalier(inm, forfait, getValeurPointIndice());

		if (salaireBrutJournalier != null) {
			coutMoyenJournalierCharge = (salaireBrutJournalier.floatValue() * ((float) 100.0 + tauxDeCharge.floatValue())) / (float) 100.0;
		}

	}

	protected abstract void calculerStat();

	public final EOPlanning getEoPlanning() {
		return eoPlanning;
	}

	public final float getValeurPointIndice() {
		return HamacCktlConfig.floatForKey(EOParametre.STAT_POINT_INDICE, getEoPlanning().toPeriode());
	}

	public final int getDureeJour() {
		return HamacCktlConfig.intForKey(EOParametre.DUREE_JOUR, getEoPlanning().toPeriode());
	}

	public final boolean isContractuel() {
		return isContractuel.booleanValue();
	}

	public final String getStatut() {
		return statut;
	}

	public final Float getTauxDeCharge() {
		return tauxDeCharge;
	}

	public final Integer getInm() {
		return inm;
	}

	public final Float getForfait() {
		return forfait;
	}

	public final Float getSalaireBrutJournalier() {
		return salaireBrutJournalier;
	}

	public final Float getCoutMoyenJournalierCharge() {
		return coutMoyenJournalierCharge;
	}

	public final String getCategorie() {
		return categorie;
	}

	/**
	 * 
	 * @param inm
	 * @param forfait
	 * @param pointIndice
	 * @return
	 */
	protected final static Float calculerSalaireBrutJournalier(
			Integer inm, Float forfait, float pointIndice) {
		Float salaireBrutJournalier = null;

		if (inm != null || forfait != null) {
			if (inm != null) {
				salaireBrutJournalier = ((float) inm.intValue()) * pointIndice / (float) 360.0;
			} else {
				salaireBrutJournalier = forfait.floatValue() / (float) 30.0;
			}
		} else {
			salaireBrutJournalier = new Float(0);
		}

		return salaireBrutJournalier;
	}
}
