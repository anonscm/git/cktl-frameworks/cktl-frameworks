/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur.stat;

import org.cocktail.fwkcktlhamac.serveur.metier.EOCetTransactionAnnuelle;
import org.cocktail.fwkcktlhamac.serveur.metier.EOParametre;
import org.cocktail.fwkcktlhamac.serveur.metier.EOPlanning;
import org.cocktail.fwkcktlhamac.serveur.metier.objets.cet.CetFactoryConsumer;
import org.cocktail.fwkcktlhamac.serveur.util.HamacCktlConfig;

/**
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 * 
 */
public class ValorisationCETContainer
		extends A_TableauDeBordContainer {

	private Integer cetAncienRegime;
	private Integer cetRegimePerenne;
	private int cetDroitOptionIndemnisation;
	private int cetDroitOptionTransfertRafp;
	private float valeurJourAIndemniser;

	/**
	 * @param eoPlanning
	 */
	public ValorisationCETContainer(EOPlanning eoPlanning) {
		super(eoPlanning);

	}

	@Override
	protected void calculerStat() {

		CetFactoryConsumer consumer = new CetFactoryConsumer(true, getEoPlanning());
		if (consumer != null) {
			cetAncienRegime = consumer.minutesRestantesAncienRegime();
			cetRegimePerenne = consumer.minutesRestantesRegimePerenneFinPeriode();
			cetDroitOptionIndemnisation = 0;
			cetDroitOptionTransfertRafp = 0;
			EOCetTransactionAnnuelle transaction = consumer.getEOCetTransactionAnnuelle();
			if (transaction != null) {
				if (transaction.cetDemIndemAncien() != null) {
					cetDroitOptionIndemnisation += transaction.cetDemIndemAncien().intValue();
				}
				if (transaction.cetDemIndemPerenne() != null) {
					cetDroitOptionIndemnisation += transaction.cetDemIndemPerenne().intValue();
				}
				if (transaction.cetDemRafpAncien() != null) {
					cetDroitOptionTransfertRafp += transaction.cetDemRafpAncien().intValue();
				}
				if (transaction.cetDemRafpPerenne() != null) {
					cetDroitOptionTransfertRafp += transaction.cetDemRafpPerenne().intValue();
				}
			}
		}

		if (getCategorie() != null) {
			if (getCategorie().equals("A")) {
				valeurJourAIndemniser = HamacCktlConfig.floatForKey(EOParametre.STAT_VALEUR_CET_INDEMNISATION_CAT_A, getEoPlanning().toPeriode());
			} else if (getCategorie().equals("B")) {
				valeurJourAIndemniser = HamacCktlConfig.floatForKey(EOParametre.STAT_VALEUR_CET_INDEMNISATION_CAT_B, getEoPlanning().toPeriode());
			} else if (getCategorie().equals("C")) {
				valeurJourAIndemniser = HamacCktlConfig.floatForKey(EOParametre.STAT_VALEUR_CET_INDEMNISATION_CAT_C, getEoPlanning().toPeriode());
			}
		}

	}

	public final Integer getCetAncienRegime() {
		return cetAncienRegime;
	}

	public final Integer getCetRegimePerenne() {
		return cetRegimePerenne;
	}

	public final int getCetDroitOptionIndemnisation() {
		return cetDroitOptionIndemnisation;
	}

	public final int getCetDroitOptionTransfertRafp() {
		return cetDroitOptionTransfertRafp;
	}

	public final float getValeurJourAIndemniser() {
		return valeurJourAIndemniser;
	}

	public final float getMontantAProvisionnerBrut() {
		float montant = (float) 0.0;

		int total = 0;

		if (getCetAncienRegime() != null) {
			total += getCetAncienRegime().intValue();
		}

		if (getCetRegimePerenne() != null) {
			total += getCetRegimePerenne().intValue();
		}

		float totalEnJour = (float) total / (float) getDureeJour();

		montant = totalEnJour * getSalaireBrutJournalier().floatValue();

		return montant;
	}

	public final float getMontantAProvisionnerCharge() {
		return getMontantAProvisionnerBrut() * getTauxDeCharge().floatValue() / (float) 100.0;
	}

	public final float getMontantAPayerBrut() {
		return (((float) getCetDroitOptionIndemnisation()) / (float) getDureeJour()) * getValeurJourAIndemniser();
	}

	public final float getMontantAPayerCharge() {
		return getMontantAPayerBrut() * getTauxDeCharge().floatValue() / (float) 100.0;
	}

	public final float getMontantRafp() {
		return (((float) getCetDroitOptionTransfertRafp()) / (float) getDureeJour()) * getValeurJourAIndemniser();
	}

}