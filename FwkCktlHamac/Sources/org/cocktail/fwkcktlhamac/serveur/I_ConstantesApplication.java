/**
 * 
 */
package org.cocktail.fwkcktlhamac.serveur;

/**
 * @author Cyril TARADE <cyril.tarade at cocktail.org>
 * 
 */
public interface I_ConstantesApplication {

	// parametres config
	public final static String GRHUM_VILLE_PARAM_KEY = "GRHUM_VILLE";
	public final static String MAIN_LOGO_URL_PARAM_KEY = "MAIN_LOGO_URL";
	public final static String GRHUM_PRESIDENT_PARAM_KEY = "GRHUM_PRESIDENT";
	public final static String GRHUM_ETAB_PARAM_KEY = "GRHUM_ETAB";
	public final static String GRHUM_DOMAINE_PRINCIPAL_KEY = "GRHUM_DOMAINE_PRINCIPAL";
	public final static String JASPER_LOCATION_KEY = "org.cocktail.hamac.reports.local.location";

}
