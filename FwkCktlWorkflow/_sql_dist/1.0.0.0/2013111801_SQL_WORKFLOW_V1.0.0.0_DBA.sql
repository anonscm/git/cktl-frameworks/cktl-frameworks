--
-- Patch DBA de WORKFLOW du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DBA
-- Schema : WORKFLOW
-- Numero de version : 1.0.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Chama LAATIK

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



------------------------------V20130618.171547__DBA_Creation_User.sql-------------------------------



declare

  dbn varchar2(255);
  ibn varchar2(255);
  ord_data varchar2(1024);
  ord_indx varchar2(1024);
  is_tbs int;
        
   c int;

begin  

-- Test d'existence du User Workflow
   select count(*) into c from dba_users where username = upper('WORKFLOW');
   if c <> 0 then
      execute immediate '
      DROP USER WORKFLOW CASCADE
      ';
   end if;   
  
  -- test d’existence du tablespace DATA
  select count(*) into is_tbs from dba_tablespaces where tablespace_name = 'DATA_CKTL_WKFL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into dbn from dba_data_files where upper(tablespace_name) = 'DATA_GRHUM';
    -- construction de l’ordre a executer
    ord_data := 'create tablespace DATA_CKTL_WKFL datafile '||chr(39)||dbn||'/data_cktl_wkfl.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace DATA : '||ord_data);
    -- creation du tablespace DATA
    execute immediate ord_data;
  end if;
  
  -- test d’existence du tablespace INDX
  select count(*) into is_tbs from dba_tablespaces where upper(tablespace_name) = 'INDX_CKTL_WKFL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into ibn from dba_data_files where upper(tablespace_name) = 'INDX_GRHUM';
    -- construction de l’ordre a executer
    ord_indx := 'create tablespace INDX_CKTL_WKFL datafile '||chr(39)||ibn||'/indx_cktl_wkfl.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace INDEXES : '||ord_indx);
    -- creation du tablespace INDX
    execute immediate ord_indx;
  end if;
end;
/

-- CREATION DU USER
CREATE USER WORKFLOW IDENTIFIED BY bdlmmdeworkflow
DEFAULT TABLESPACE DATA_CKTL_WKFL TEMPORARY TABLESPACE TEMP quota unlimited on DATA_CKTL_WKFL;
ALTER USER WORKFLOW quota unlimited on INDX_CKTL_WKFL;

--
-- Grants pour GRHUM
--
GRANT select, references on GRHUM.PERSONNE to WORKFLOW;
GRANT select, references on GRHUM.GD_APPLICATION to WORKFLOW;


