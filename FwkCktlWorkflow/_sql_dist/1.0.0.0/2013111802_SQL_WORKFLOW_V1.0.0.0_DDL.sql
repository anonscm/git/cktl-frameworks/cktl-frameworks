--
-- Patch DDL de WORKFLOW du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : WORKFLOW
-- Numero de version : 1.0.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Chama LAATIK

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20130618.171758__DDL_Creation_Tables.sql------------------------------




--
-- CIRCUIT_VALIDATION
--
CREATE TABLE WORKFLOW.CIRCUIT_VALIDATION
(
  ID_CIRCUIT_VALIDATION NUMBER	NOT NULL,
  ID_APP NUMBER(12,0) NOT NULL,
  C_CIRCUIT_VALIDATION	VARCHAR2(20) NOT NULL,
  NUMERO_VERSION NUMBER NOT NULL,
  LL_CIRCUIT_VALIDATION	VARCHAR2(100) NOT NULL,
  TEM_UTILISABLE VARCHAR2(1) DEFAULT 'N' NOT NULL,
  D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
  PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE WORKFLOW.CIRCUIT_VALIDATION ADD CONSTRAINT PK_CIRCUIT_VALIDATION PRIMARY KEY(ID_CIRCUIT_VALIDATION);
ALTER TABLE WORKFLOW.CIRCUIT_VALIDATION ADD CONSTRAINT UK_CIRCUIT_VALIDATION  UNIQUE (ID_APP, C_CIRCUIT_VALIDATION, NUMERO_VERSION);

ALTER TABLE WORKFLOW.CIRCUIT_VALIDATION ADD CONSTRAINT FK_CIRCUIT_VALID_GD_APPLI FOREIGN KEY (ID_APP) REFERENCES GRHUM.GD_APPLICATION (APP_ID);

ALTER TABLE WORKFLOW.CIRCUIT_VALIDATION ADD CONSTRAINT CHK_CIRCUIT_TEM_UTILISABLE CHECK (TEM_UTILISABLE IN ('O', 'N'));

CREATE SEQUENCE WORKFLOW.CIRCUIT_VALIDATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE WORKFLOW.CIRCUIT_VALIDATION IS 'Table des circuits de validation';    
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.ID_CIRCUIT_VALIDATION IS 'Clef primaire de la table CIRCUIT_VALIDATION';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.ID_APP IS 'Référence à l''application (cf. GRHUM.GD_APPLICATION.APP_ID)';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION IS 'Code du circuit de validation';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.NUMERO_VERSION IS 'Numéro de version du circuit';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.LL_CIRCUIT_VALIDATION IS 'Libellé long du circuit de validation';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.TEM_UTILISABLE IS 'Est-ce que le circuit de validation est utilisable ? (O/N)';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.D_MODIFICATION IS 'Date de la derniere modification de l''enregistrement';
COMMENT ON COLUMN WORKFLOW.CIRCUIT_VALIDATION.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

--
-- ETAPE
--
CREATE TABLE WORKFLOW.ETAPE
(
  ID_ETAPE 	 	NUMBER	NOT NULL ,
  C_ETAPE		VARCHAR2(20) NOT NULL,
  LL_ETAPE		VARCHAR2(100) NOT NULL,
  ID_CIRCUIT_VALIDATION	NUMBER NOT NULL,
  TEM_ETAPE_INITIALE VARCHAR2(1) DEFAULT 'N' NOT NULL 
);

ALTER TABLE WORKFLOW.ETAPE ADD CONSTRAINT PK_ETAPE PRIMARY KEY(ID_ETAPE);
ALTER TABLE WORKFLOW.ETAPE  ADD CONSTRAINT UK_ETAPE_CODE_CIRCUIT  UNIQUE (C_ETAPE, ID_CIRCUIT_VALIDATION);

ALTER TABLE WORKFLOW.ETAPE ADD CONSTRAINT FK_ETA_CIRCUIT_VALID FOREIGN KEY (ID_CIRCUIT_VALIDATION) REFERENCES WORKFLOW.CIRCUIT_VALIDATION (ID_CIRCUIT_VALIDATION);
ALTER TABLE WORKFLOW.ETAPE ADD CONSTRAINT CHK_ETA_TEM_ETAPE_INI CHECK (TEM_ETAPE_INITIALE IN ('O', 'N') );

CREATE INDEX WORKFLOW.IDX_ETA_ID_CIRCUIT_VALID ON WORKFLOW.ETAPE(ID_CIRCUIT_VALIDATION);

CREATE SEQUENCE WORKFLOW.ETAPE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE WORKFLOW.ETAPE IS 'Table des étapes générées(validation, annulation,...)';    
COMMENT ON COLUMN WORKFLOW.ETAPE.ID_ETAPE IS 'Clef primaire de la table ETAPE';
COMMENT ON COLUMN WORKFLOW.ETAPE.C_ETAPE IS 'Code de l''étape';
COMMENT ON COLUMN WORKFLOW.ETAPE.LL_ETAPE IS 'Libellé long de l''étape';
COMMENT ON COLUMN WORKFLOW.ETAPE.ID_CIRCUIT_VALIDATION IS 'Référence au circuit de validation (cf. WORKFLOW.CIRCUIT_VALIDATION.ID_CIRCUIT_VALIDATION)';
COMMENT ON COLUMN WORKFLOW.ETAPE.TEM_ETAPE_INITIALE IS 'Est-ce que c''est l''étape initiale ? (O:valide, N:invalide)';

--
-- DEMANDE
--
CREATE TABLE WORKFLOW.DEMANDE
(
  ID_DEMANDE NUMBER NOT NULL ,
  ID_ETAPE NUMBER NOT NULL
);

ALTER TABLE WORKFLOW.DEMANDE ADD CONSTRAINT PK_DEMANDE PRIMARY KEY(ID_DEMANDE);
ALTER TABLE WORKFLOW.DEMANDE ADD CONSTRAINT FK_DEMANDE_ID_ETAPE FOREIGN KEY (ID_ETAPE) REFERENCES WORKFLOW.ETAPE (ID_ETAPE);

CREATE INDEX WORKFLOW.IDX_DEMANDE_ID_ETAPE ON WORKFLOW.DEMANDE(ID_ETAPE);

CREATE SEQUENCE WORKFLOW.DEMANDE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE WORKFLOW.DEMANDE IS 'Table des demandes en cours';    
COMMENT ON COLUMN WORKFLOW.DEMANDE.ID_DEMANDE IS 'Clef primaire de la table DEMANDE';
COMMENT ON COLUMN WORKFLOW.DEMANDE.ID_ETAPE IS 'Référence à l''étape en cours (cf. WORKFLOW.ETAPE.ID_ETAPE)';

--
-- CHEMIN
--
CREATE TABLE WORKFLOW.CHEMIN
(
  ID_CHEMIN NUMBER NOT NULL,
  ID_ETAPE_DEPART NUMBER NOT NULL,
  ID_ETAPE_ARRIVEE NUMBER,
  C_TYPE_CHEMIN VARCHAR2(20) NOT NULL,
  LC_TYPE_CHEMIN VARCHAR2(50) NOT NULL
);

ALTER TABLE WORKFLOW.CHEMIN ADD CONSTRAINT PK_CHEMIN PRIMARY KEY(ID_CHEMIN);
ALTER TABLE WORKFLOW.CHEMIN ADD CONSTRAINT FK_CHEMIN_ETAPE_DEPART FOREIGN KEY (ID_ETAPE_DEPART) REFERENCES WORKFLOW.ETAPE (ID_ETAPE);
ALTER TABLE WORKFLOW.CHEMIN ADD CONSTRAINT FK_CHEMIN_ETAPE_ARRIVEE FOREIGN KEY (ID_ETAPE_ARRIVEE) REFERENCES WORKFLOW.ETAPE (ID_ETAPE);
ALTER TABLE WORKFLOW.CHEMIN ADD CONSTRAINT UK_CHEMIN_ETAPE_DEPART_TYP_CHE UNIQUE (ID_ETAPE_DEPART, C_TYPE_CHEMIN);

CREATE INDEX WORKFLOW.IDX_CHEMIN_ETAPE_DEPART ON WORKFLOW.CHEMIN(ID_ETAPE_DEPART);
CREATE INDEX WORKFLOW.IDX_CHEMIN_ETAPE_ARRIVEE ON WORKFLOW.CHEMIN(ID_ETAPE_ARRIVEE);

CREATE SEQUENCE WORKFLOW.CHEMIN_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE WORKFLOW.CHEMIN IS 'Table des chemins valides';   
COMMENT ON COLUMN WORKFLOW.CHEMIN.ID_CHEMIN IS 'Clef primaire de la table CHEMIN'; 
COMMENT ON COLUMN WORKFLOW.CHEMIN.ID_ETAPE_DEPART IS 'Référence à l''étape de départ (cf. WORKFLOW.ETAPE.ID_ETAPE), peut être à null si étape initiale';
COMMENT ON COLUMN WORKFLOW.CHEMIN.ID_ETAPE_ARRIVEE IS 'Référence à l''étape d''arrivée (cf. WORKFLOW.ETAPE.ID_ETAPE)';
COMMENT ON COLUMN WORKFLOW.CHEMIN.C_TYPE_CHEMIN IS 'Code du type de chemin';
COMMENT ON COLUMN WORKFLOW.CHEMIN.LC_TYPE_CHEMIN IS 'Libellé court du type de chemin';

--
-- HISTORIQUE_DEMANDE
--
CREATE TABLE WORKFLOW.HISTORIQUE_DEMANDE
(
  ID_HISTORIQUE_DEMANDE NUMBER NOT NULL ,
  ID_DEMANDE 		NUMBER NOT NULL ,
  ID_ETAPE_DEPART  	NUMBER,
  ID_ETAPE_ARRIVEE   	NUMBER NOT NULL,
  PERS_ID_MODIFICATION 	NUMBER NOT NULL,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL
  
);

ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT PK_HISTORIQUE_DEMANDE PRIMARY KEY(ID_HISTORIQUE_DEMANDE);
ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT FK_HD_ID_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES WORKFLOW.DEMANDE (ID_DEMANDE);
ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT FK_HD_ID_ETAPE_DEPART FOREIGN KEY (ID_ETAPE_DEPART) REFERENCES WORKFLOW.ETAPE (ID_ETAPE);
ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT FK_HD_ID_ETAPE_ARRIVEE FOREIGN KEY (ID_ETAPE_ARRIVEE) REFERENCES WORKFLOW.ETAPE (ID_ETAPE);
ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT FK_HD_PERS_ID_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);

CREATE INDEX WORKFLOW.IDX_HD_ID_DEMANDE ON WORKFLOW.HISTORIQUE_DEMANDE(ID_DEMANDE);
CREATE INDEX WORKFLOW.IDX_HD_ETAPE_DEPART ON WORKFLOW.HISTORIQUE_DEMANDE(ID_ETAPE_DEPART);
CREATE INDEX WORKFLOW.IDX_HD_ETAPE_ARRIVEE ON WORKFLOW.HISTORIQUE_DEMANDE(ID_ETAPE_ARRIVEE);
CREATE INDEX WORKFLOW.IDX_HD_PERS_MODIFICATION ON WORKFLOW.HISTORIQUE_DEMANDE(PERS_ID_MODIFICATION);

CREATE SEQUENCE WORKFLOW.HISTORIQUE_DEMANDE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE WORKFLOW.HISTORIQUE_DEMANDE IS 'Historisation des demandes qui change d''étape'; 
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.ID_HISTORIQUE_DEMANDE IS 'Clef primaire de la table HISTORIQUE_DEMANDE';   
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.ID_DEMANDE IS 'Référence à la demande historisée (cf. WORKFLOW.DEMANDE.ID_DEMANDE)';
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.ID_ETAPE_DEPART IS 'Référence à l''étape de départ (cf. WORKFLOW.ETAPE.ID_ETAPE), peut être à null si étape initiale';
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.ID_ETAPE_ARRIVEE IS 'Référence à l''étape d''arrivée (cf. WORKFLOW.ETAPE.ID_ETAPE)';
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification du statut de la demande';
COMMENT ON COLUMN WORKFLOW.HISTORIQUE_DEMANDE.D_MODIFICATION IS 'Date de modification de la demande';

-------------------------V20130701.111246__DDL_ajout_deferrable_sur_cle.sql-------------------------


ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE DROP CONSTRAINT FK_HD_ID_DEMANDE;
ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE ADD CONSTRAINT FK_HD_ID_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES WORKFLOW.DEMANDE (ID_DEMANDE) DEFERRABLE INITIALLY DEFERRED;


---------------------V20130730.101111__DDL_Alter_colonne_historique_demande.sql---------------------

ALTER TABLE WORKFLOW.HISTORIQUE_DEMANDE MODIFY PERS_ID_MODIFICATION NULL;

------------------------V20130911.113631__DDL_creation_table_db_version.sql-------------------------
CREATE TABLE WORKFLOW.DB_VERSION  (
	DBV_ID      NUMBER(38) NOT NULL,
	DBV_LIBELLE VARCHAR2(15) NOT NULL,
	DBV_DATE DATE NOT NULL ,
	DBV_INSTALL DATE,
	DBV_COMMENT VARCHAR2(2000),
	PRIMARY KEY (DBV_ID) 
);	
	CREATE SEQUENCE WORKFLOW.DB_VERSION_SEQ START WITH 1;
	COMMENT ON TABLE WORKFLOW.DB_VERSION IS 'Table contenant les versions des scripts passes';
	COMMENT ON COLUMN WORKFLOW.DB_VERSION.DBV_ID IS 'Identifiant de la version';
	COMMENT ON COLUMN WORKFLOW.DB_VERSION.DBV_LIBELLE IS 'Libelle de la version';
	COMMENT ON COLUMN WORKFLOW.DB_VERSION.DBV_DATE IS 'Date de release de la version';
	COMMENT ON COLUMN WORKFLOW.DB_VERSION.DBV_INSTALL IS 'Date d''installation de la version. Si non renseigne, la version n''est pas completement installee.';
	COMMENT ON COLUMN WORKFLOW.DB_VERSION.DBV_COMMENT IS 'Le commentaire : une courte description de cette version de la base de donnees.';

-----------------------V20131004.163314__DDL_Modification_taille_colonne.sql------------------------

alter table WORKFLOW.CIRCUIT_VALIDATION modify (C_CIRCUIT_VALIDATION VARCHAR2(30));



