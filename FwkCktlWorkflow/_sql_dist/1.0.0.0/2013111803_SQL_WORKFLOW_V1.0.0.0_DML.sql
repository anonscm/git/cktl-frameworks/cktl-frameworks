--
-- Patch DML de WORKFLOW du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : WORKFLOW
-- Numero de version : 1.0.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Chama LAATIK

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;




--DB_VERSION 
Insert into WORKFLOW.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(WORKFLOW.DB_VERSION_SEQ.NEXTVAL,'1.0.0.0',to_date('18/11/2013','DD/MM/YYYY'),sysdate,'Initialisation du user WORKFLOW');
COMMIT;
