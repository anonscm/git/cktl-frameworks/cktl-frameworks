package org.cocktail.fwkcktlworkflow.validation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;

/**
 * Assistant pour les circuits de validation.
 * Permet de faire les contrôles et de parcourir l'arbre du circuit.
 * 
 * @author Pascal MACOUIN
 */
public class CircuitValidationHelper {
	private static Logger log = Logger.getLogger(CircuitValidationHelper.class);
	private int niveau;
	
	/** Liste des étapes disponibles mais non utilisées dans ce circuit de validation. */
	private List<EOEtape> listeEtapesNonUtilisees;
	/** Liste des étapes utilisées dans ce circuit de validation. */
	private List<EOEtape> listeEtapesUtilisees = new ArrayList<EOEtape>();
	/** Ensemble des code des étapes déjà parcourue dans l'arborescence du circuit de validation. */
	private Set<String> etapesDejaParcourues = new HashSet<String>();
	
	// Attributs permettant le contrôle que tous les chemins d'un circuit amènent à une étape finale
	/** Liste des chemins non validés. */
	private List<EOChemin> listeCheminNonValides = new ArrayList<EOChemin>();
	/** Liste des chemins validés. */
	private List<EOChemin> listeCheminValides = new ArrayList<EOChemin>();
	/** Liste des étapes finales et des étapes ayant au moins un de leur chemin validé. */ 
	private List<EOEtape> listeEtapeAyantAuMoinsUnCheminValide = new ArrayList<EOEtape>();
	
	/** Est-ce que le circuit de validation doit être contrôlé ? */
	private boolean controler;
	
	private List<String> listeMessages = new ArrayList<String>();
	
	/**
	 * Contrôle et "normalise" le circuit de validation.
	 * <p>
	 * La "normalisation" est faite uniquement si les contrôles sont ok.
	 * 
	 * @param circuitValidation Un circuit de validation
	 */
	public void controlerEtNormaliser(EOCircuitValidation circuitValidation) {
	
		initialiser(circuitValidation);
		
		controler(circuitValidation);
		
		if (listeMessages.size() == 0) {
			// On normalise seulement si pas d'erreur
			normaliser(circuitValidation);
		}
	}

	private void initialiser(EOCircuitValidation circuitValidation) {
		listeMessages.clear();
		etapesDejaParcourues.clear();
		listeEtapesUtilisees.clear();
		listeEtapesNonUtilisees = circuitValidation.toEtapes().arrayList();
		listeCheminNonValides.clear();
		listeCheminValides.clear();
		listeEtapeAyantAuMoinsUnCheminValide.clear();
	}

	private void controler(EOCircuitValidation circuitValidation) {
		controler = true;
		
		niveau = 0;
		log.debug("Début du parcours du circuit " + circuitValidation.codeCircuitValidation());
		parcourirEtape(circuitValidation.etapeInitiale());
		log.debug("Fin du parcours du circuit " + circuitValidation.codeCircuitValidation());
		
		validerChemins();
	}
	
	/**
	 * "Normaliser" (le rendre normal) le circuit de validation.
	 * <p>
	 * On normalise seulement si tous les contrôles sont ok.
	 * <p>
	 * Normaliser :
	 * <ol>
	 * <li>Mettre à <code>null</code> les étapes d'arrivée au bout de tous les chemins des étapes non utilisées</li>
	 * </ol>
	 * 
	 * @param circuitValidation Un circuit de validation
	 */
	private void normaliser(EOCircuitValidation circuitValidation) {
		log.debug("Normalisation du circuit " + circuitValidation.codeCircuitValidation());
		
		// Pour toutes les étapes non utilisées, on met à null les étapes au bout des chemins
		for (EOEtape uneEtape : listeEtapesNonUtilisees) {
			for (EOChemin unChemin : uneEtape.toChemins()) {
				unChemin.setToEtapeArriveeRelationship(null);
			}
		}
	}
	
	/**
	 * Parcour tous les chemins de l'étape si cette étape n'a pas déjà été parcourue.
	 * 
	 * @param etape Une étape
	 * @return
	 */
	private void parcourirEtape(EOEtape etape) {
		niveau++;
		log.debug(leftPad() + "Début du parcours de l'étape " + etape.codeEtape());

		if (!etapesDejaParcourues.contains(etape.codeEtape())) {
			etapesDejaParcourues.add(etape.codeEtape());
			
			// Mise à jour des listes des étapes utilisées/non utilisées
			if (listeEtapesNonUtilisees.remove(etape)) {
				listeEtapesUtilisees.add(etape);
			}
			
			if (etape.isFinale()) {
				listeEtapeAyantAuMoinsUnCheminValide.add(etape);
			}
			
			int nombreDeCheminEmpruntable = 0;
			
			for (EOChemin chemin : etape.toChemins()) {
				if (chemin.toEtapeArrivee() != null) {
					nombreDeCheminEmpruntable++;
				}
				
				parcourirChemin(chemin);
			}
			
			if (controler) {
				controlerAuMoinsUnCheminEmpruntable(etape, nombreDeCheminEmpruntable);
			}
			
		}
		
		log.debug(leftPad() + "Fin du parcours de l'étape " + etape.codeEtape());
		niveau--;
	}

	/**
	 * Controle qu'il y ait au moins un chemin empruntable défini sur une étape.
	 * Obligatoire pour les étapes qui définissent un ou plusieurs chemins possibles.
	 * <p>
	 * Empile un message si ce n'est pas le cas.
	 * 
	 * @param etape L'étape
	 * @param nombreDeCheminEmpruntable Le nombre de chemin empruntable pour cette étape
	 */
	private void controlerAuMoinsUnCheminEmpruntable(EOEtape etape, int nombreDeCheminEmpruntable) {
		if (etape.toChemins().size() != 0 && nombreDeCheminEmpruntable == 0) {
			if (etape.toChemins().size() == 1) {
				listeMessages.add("L'étape \"" + etape.libelleEtape() + "\" doit définir une étape au bout du chemin \"" + etape.toChemins().get(0).libelleCourtTypeChemin() + "\".");
			} else {
				listeMessages.add("L'étape \"" + etape.libelleEtape() + "\" doit définir une étape au bout d'un de ces chemins.");
			}
		}
	}

	/**
	 * Parcour le chemin. Lance le parcour de l'étape s'il y en a une au bout de ce chemin.
	 * 
	 * @param chemin Un chemin
	 * @return
	 */
	private void parcourirChemin(EOChemin chemin) {
		niveau++;
		log.debug(leftPad() + "Début du parcours du chemin " + chemin.libelleCourtTypeChemin());
		
		if (chemin.toEtapeArrivee() != null) {
			listeCheminNonValides.add(chemin);
			
			parcourirEtape(chemin.toEtapeArrivee());
		}
		
		log.debug(leftPad() + "Fin du parcours du chemin " + chemin.libelleCourtTypeChemin());
		niveau--;
	}

	/**
	 * Permet de marquer "validé" les chemins qui mènent à une étape validée.
	 * <p>
	 * Une étape validée est une étape qui est finale ou qui a au moins un de ces chemins validés.
	 */
	private void validerChemins() {
		// Pour toutes les étapes qui sont "validées" (ont au moins un chemin validées)
		for (int iEtape = 0; iEtape < listeEtapeAyantAuMoinsUnCheminValide.size(); iEtape++) {
			EOEtape uneEtape = listeEtapeAyantAuMoinsUnCheminValide.get(iEtape);
			
			// On valide tous les chemins qui arrivent sur cette étape (et ainsi de suite)
			validerCheminsEntrantsEtape(uneEtape);
		}

		// On alerte sur le premier chemin non valide
		if (!listeCheminNonValides.isEmpty()) {
			EOChemin chemin = listeCheminNonValides.get(0);
			listeMessages.add("L'action \"" + chemin.libelleCourtTypeChemin() + "\" de l'étape \"" + chemin.toEtapeDepart().libelleEtape() + "\" mène à une voie sans issue.");
		}
	}
	
	/**
	 * Valide tous les chemins qui mène à cette étape.
	 * <p>
	 * L'étape à l'origine (de départ) du chemin validé est marquées comme validée.
	 * 
	 * @param etape Une étape "validées"
	 */
	private void validerCheminsEntrantsEtape(EOEtape etape) {
		for (int iChemin = 0; iChemin < listeCheminNonValides.size(); iChemin++) {
			EOChemin unChemin = listeCheminNonValides.get(iChemin);
			
			if (unChemin.toEtapeArrivee() == etape) {
				listeCheminValides.add(unChemin);
				listeCheminNonValides.remove(iChemin);
				iChemin--;
				
				if (!listeEtapeAyantAuMoinsUnCheminValide.contains(unChemin.toEtapeDepart())) {
					listeEtapeAyantAuMoinsUnCheminValide.add(unChemin.toEtapeDepart());
				}
			}
		}
	}

	private String leftPad() {
		return StringUtils.rightPad("", niveau * 3);
	}
	
	public List<EOEtape> getListeEtapesNonUtilisees() {
		return listeEtapesNonUtilisees;
	}

	public List<EOEtape> getListeEtapesUtilisees() {
		return listeEtapesUtilisees;
	}

	public List<String> getListeMessages() {
		return listeMessages;
	}

	/**
	 * Retourne la liste des chemins non validés.
	 * <p>
	 * Chemins ne pouvant jamais mener à une étape finale.
	 * 
	 * @return La liste des chemins non validés
	 */
	public List<EOChemin> getListeCheminNonValides() {
		return listeCheminNonValides;
	}

	/**
	 * Retourne la liste des chemins validés.
	 * <p>
	 * Chemins pouvant mener à une étape finale.
	 * 
	 * @return La liste des chemins validés
	 */
	public List<EOChemin> getListeCheminValides() {
		return listeCheminValides;
	}

	/**
	 * Retourne la liste des étapes finales et des étapes ayant au moins un de leur chemin validé.
	 * 
	 * @return La liste des étapes finales et des étapes ayant au moins un de leur chemin validé
	 */
	public List<EOEtape> getListeEtapeAyantAuMoinsUnCheminValide() {
		return listeEtapeAyantAuMoinsUnCheminValide;
	}
}
