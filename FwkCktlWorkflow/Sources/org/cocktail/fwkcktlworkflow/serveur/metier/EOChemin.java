package org.cocktail.fwkcktlworkflow.serveur.metier;

import org.apache.log4j.Logger;

/**
 * Entité représentant un Chemin. Un chemin permet de relier deux
 * {@link EOEtape}.
 * 
 * @author yannick
 * 
 */
public class EOChemin extends _EOChemin {
	
	private static final long serialVersionUID = 1206532064517709244L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOChemin.class);
}
