// DO NOT EDIT.  Make changes to EOCircuitValidation.java instead.
package org.cocktail.fwkcktlworkflow.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOCircuitValidation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "CircuitValidation";

  // Attribute Keys
  public static final ERXKey<String> CODE_CIRCUIT_VALIDATION = new ERXKey<String>("codeCircuitValidation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> LIBELLE_LONG_CIRCUIT_VALIDATION = new ERXKey<String>("libelleLongCircuitValidation");
  public static final ERXKey<Integer> NUMERO_VERSION = new ERXKey<Integer>("numeroVersion");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> TEMOIN_UTILISABLE = new ERXKey<String>("temoinUtilisable");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPES = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtapes");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> TO_GD_APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("toGdApplication");

  // Attributes
  public static final String CODE_CIRCUIT_VALIDATION_KEY = CODE_CIRCUIT_VALIDATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String LIBELLE_LONG_CIRCUIT_VALIDATION_KEY = LIBELLE_LONG_CIRCUIT_VALIDATION.key();
  public static final String NUMERO_VERSION_KEY = NUMERO_VERSION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String TEMOIN_UTILISABLE_KEY = TEMOIN_UTILISABLE.key();
  // Relationships
  public static final String TO_ETAPES_KEY = TO_ETAPES.key();
  public static final String TO_GD_APPLICATION_KEY = TO_GD_APPLICATION.key();

  private static Logger LOG = Logger.getLogger(_EOCircuitValidation.class);

  public EOCircuitValidation localInstanceIn(EOEditingContext editingContext) {
    EOCircuitValidation localInstance = (EOCircuitValidation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeCircuitValidation() {
    return (String) storedValueForKey(_EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY);
  }

  public void setCodeCircuitValidation(String value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating codeCircuitValidation from " + codeCircuitValidation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOCircuitValidation.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.DATE_MODIFICATION_KEY);
  }

  public String libelleLongCircuitValidation() {
    return (String) storedValueForKey(_EOCircuitValidation.LIBELLE_LONG_CIRCUIT_VALIDATION_KEY);
  }

  public void setLibelleLongCircuitValidation(String value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating libelleLongCircuitValidation from " + libelleLongCircuitValidation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.LIBELLE_LONG_CIRCUIT_VALIDATION_KEY);
  }

  public Integer numeroVersion() {
    return (Integer) storedValueForKey(_EOCircuitValidation.NUMERO_VERSION_KEY);
  }

  public void setNumeroVersion(Integer value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating numeroVersion from " + numeroVersion() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.NUMERO_VERSION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOCircuitValidation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.PERS_ID_MODIFICATION_KEY);
  }

  public String temoinUtilisable() {
    return (String) storedValueForKey(_EOCircuitValidation.TEMOIN_UTILISABLE_KEY);
  }

  public void setTemoinUtilisable(String value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
    	_EOCircuitValidation.LOG.debug( "updating temoinUtilisable from " + temoinUtilisable() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCircuitValidation.TEMOIN_UTILISABLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication)storedValueForKey(_EOCircuitValidation.TO_GD_APPLICATION_KEY);
  }
  
  public void setToGdApplication(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    takeStoredValueForKey(value, _EOCircuitValidation.TO_GD_APPLICATION_KEY);
  }

  public void setToGdApplicationRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication value) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
      _EOCircuitValidation.LOG.debug("updating toGdApplication from " + toGdApplication() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToGdApplication(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication oldValue = toGdApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCircuitValidation.TO_GD_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCircuitValidation.TO_GD_APPLICATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> toEtapes() {
    return (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>)storedValueForKey(_EOCircuitValidation.TO_ETAPES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> toEtapes(EOQualifier qualifier) {
    return toEtapes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> toEtapes(EOQualifier qualifier, boolean fetch) {
    return toEtapes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> toEtapes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape.TO_CIRCUIT_VALIDATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape.fetchEtapes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toEtapes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToEtapes(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape object) {
    includeObjectIntoPropertyWithKey(object, _EOCircuitValidation.TO_ETAPES_KEY);
  }

  public void removeFromToEtapes(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape object) {
    excludeObjectFromPropertyWithKey(object, _EOCircuitValidation.TO_ETAPES_KEY);
  }

  public void addToToEtapesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape object) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
      _EOCircuitValidation.LOG.debug("adding " + object + " to toEtapes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToEtapes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOCircuitValidation.TO_ETAPES_KEY);
    }
  }

  public void removeFromToEtapesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape object) {
    if (_EOCircuitValidation.LOG.isDebugEnabled()) {
      _EOCircuitValidation.LOG.debug("removing " + object + " from toEtapes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToEtapes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOCircuitValidation.TO_ETAPES_KEY);
    }
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape createToEtapesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOCircuitValidation.TO_ETAPES_KEY);
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape) eo;
  }

  public void deleteToEtapesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOCircuitValidation.TO_ETAPES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToEtapesRelationships() {
    Enumeration<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> objects = toEtapes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToEtapesRelationship(objects.nextElement());
    }
  }


  public static EOCircuitValidation createCircuitValidation(EOEditingContext editingContext, String codeCircuitValidation
, String libelleLongCircuitValidation
, Integer numeroVersion
, String temoinUtilisable
, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication toGdApplication) {
    EOCircuitValidation eo = (EOCircuitValidation) EOUtilities.createAndInsertInstance(editingContext, _EOCircuitValidation.ENTITY_NAME);    
		eo.setCodeCircuitValidation(codeCircuitValidation);
		eo.setLibelleLongCircuitValidation(libelleLongCircuitValidation);
		eo.setNumeroVersion(numeroVersion);
		eo.setTemoinUtilisable(temoinUtilisable);
    eo.setToGdApplicationRelationship(toGdApplication);
    return eo;
  }

  public static ERXFetchSpecification<EOCircuitValidation> fetchSpec() {
    return new ERXFetchSpecification<EOCircuitValidation>(_EOCircuitValidation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOCircuitValidation> fetchAllCircuitValidations(EOEditingContext editingContext) {
    return _EOCircuitValidation.fetchAllCircuitValidations(editingContext, null);
  }

  public static NSArray<EOCircuitValidation> fetchAllCircuitValidations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCircuitValidation.fetchCircuitValidations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCircuitValidation> fetchCircuitValidations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOCircuitValidation> fetchSpec = new ERXFetchSpecification<EOCircuitValidation>(_EOCircuitValidation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCircuitValidation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOCircuitValidation fetchCircuitValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCircuitValidation.fetchCircuitValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCircuitValidation fetchCircuitValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCircuitValidation> eoObjects = _EOCircuitValidation.fetchCircuitValidations(editingContext, qualifier, null);
    EOCircuitValidation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CircuitValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCircuitValidation fetchRequiredCircuitValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCircuitValidation.fetchRequiredCircuitValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCircuitValidation fetchRequiredCircuitValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCircuitValidation eoObject = _EOCircuitValidation.fetchCircuitValidation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CircuitValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCircuitValidation localInstanceIn(EOEditingContext editingContext, EOCircuitValidation eo) {
    EOCircuitValidation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
