package org.cocktail.fwkcktlworkflow.serveur.metier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationTechniqueException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

/**
 * La classe <code>EOCircuitValidation</code> représente un circuit de
 * validation sur lequel on peut faire circuler des {@link EODemande}s.
 * 
 * @author yannick
 * @author Pascal MACOUIN
 */
public class EOCircuitValidation extends _EOCircuitValidation {

	private static final long serialVersionUID = -364240707789444030L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOCircuitValidation.class);

	/**
	 * Recherche tous les circuits de validation d'une application.
	 * 
	 * @param ec Un editing context
	 * @param codeApplication Un code application
	 * @return La liste des circuits de validation de l'application
	 */
	public static NSArray<EOCircuitValidation> rechercherCircuitsValidation(EOEditingContext ec, String codeApplication) {
		/* Recherche des circuits en base de données. */
		EOQualifier qualifier = ERXQ.equals(ERXQ.keyPath(EOCircuitValidation.TO_GD_APPLICATION_KEY, EOGdApplication.APP_STR_ID_KEY), codeApplication)
				.and(EOCircuitValidation.NUMERO_VERSION.eq(1));
		NSArray<EOCircuitValidation> listeCircuitsValidation = EOCircuitValidation.fetchCircuitValidations(ec, qualifier, null);
		return listeCircuitsValidation;
	}

	/**
	 * Retourne toutes les versions de circuits d'un circuit de validation.
	 * 
	 * @param ec Un editing context
	 * @param codeApplication Un code application
	 * @param codeCircuit Un code circuit
	 * @return La différentes versions du circuit de validation (triées par numéro de version)
	 */
	public static NSArray<EOCircuitValidation> rechercherCircuitsValidation(EOEditingContext ec, String codeApplication, String codeCircuit) {
		/* Recherche des circuits en base de données. */
		EOQualifier qualifier = EOCircuitValidation.CODE_CIRCUIT_VALIDATION.eq(codeCircuit)
				.and(ERXQ.equals(ERXQ.keyPath(EOCircuitValidation.TO_GD_APPLICATION_KEY, EOGdApplication.APP_STR_ID_KEY), codeApplication));
		NSArray<EOCircuitValidation> listeCircuitValidations = EOCircuitValidation.fetchCircuitValidations(ec, qualifier, EOCircuitValidation.NUMERO_VERSION.ascs());
		return listeCircuitValidations;
	}
	
	/**
	 * Rechercher un circuit de validation pour une application.
	 * <p>
	 * Le circuit de validation doit être marqué "Utilisable".
	 * 
	 * @param ec Un editing context
	 * @param codeApplication Un code application
	 * @param codeCircuit Un code circuit
	 * @return Le circuit de validation utilisable si trouvé sinon une exception
	 */
	public static EOCircuitValidation rechercherCircuitValidationUtilisable(EOEditingContext ec, String codeApplication, String codeCircuit) {
		/* Recherche du circuit en base de données. */
		EOQualifier qualifier = EOCircuitValidation.CODE_CIRCUIT_VALIDATION.eq(codeCircuit)
				.and(EOCircuitValidation.TEMOIN_UTILISABLE.eq("O"))
				.and(ERXQ.equals(ERXQ.keyPath(EOCircuitValidation.TO_GD_APPLICATION_KEY, EOGdApplication.APP_STR_ID_KEY), codeApplication));
		NSArray<EOCircuitValidation> listeCircuitValidations = EOCircuitValidation.fetchCircuitValidations(ec, qualifier, null);
		
		if (listeCircuitValidations == null || listeCircuitValidations.size() == 0) {
			// Pas de circuit trouvé
			throw new CircuitValidationTechniqueException(
					"Le circuit %s pour l'application %s n'existe pas ou n'est pas utilisable.", codeCircuit, codeApplication);
		} else if (listeCircuitValidations.size() > 1) {
			// Plusieurs circuits utilisables trouvés. Cas normalement impossible.
			throw new CircuitValidationTechniqueException(
					"Le circuit %s pour l'application %s contient %s versions utilisables du circuit. Il n'en faudrait qu'une.", codeCircuit, codeApplication, listeCircuitValidations.size());
		}
		
		return listeCircuitValidations.get(0);
	}
	
	/**
	 * Créer une nouvelle version du circuit de validation.
	 * 
	 * @param circuitValidation Un circuit à dupliquer
	 * @param PersIdConnecte La personne qui demande la duplication
	 * @return Le circuit dupliqué (le numéro de version est à <code>null</code>)
	 * @see #rechercherNumeroVersionSuivant()
	 */
	public static EOCircuitValidation dupliquerCircuitValidation(EOCircuitValidation circuitValidation, Integer PersIdConnecte) {
		EOEditingContext ec = circuitValidation.editingContext();
		
		EOCircuitValidation nouvelleVersion = new EOCircuitValidation();
		
		nouvelleVersion.setCodeCircuitValidation(circuitValidation.codeCircuitValidation());
		nouvelleVersion.setLibelleLongCircuitValidation(circuitValidation.libelleLongCircuitValidation());
		nouvelleVersion.setNumeroVersion(null);
		nouvelleVersion.setTemoinUtilisable("N");
		nouvelleVersion.setDateModification(new NSTimestamp());
		nouvelleVersion.setPersIdModification(PersIdConnecte);
		ec.insertObject(nouvelleVersion);
		nouvelleVersion.setToGdApplicationRelationship(circuitValidation.toGdApplication());

		Map<String, EOEtape> mapEtapesNouvelleVersion = new HashMap<String, EOEtape>();
		
		// On duplique d'abord toutes les étapes
		for (EOEtape uneEtape : circuitValidation.toEtapes()) {
			EOEtape etape = new EOEtape();
			ec.insertObject(etape);
			
			etape.setToCircuitValidationRelationship(nouvelleVersion);
			etape.setCodeEtape(uneEtape.codeEtape());
			etape.setLibelleEtape(uneEtape.libelleEtape());
			etape.setTemoinEtapeInitiale(uneEtape.temoinEtapeInitiale());
			
			mapEtapesNouvelleVersion.put(etape.codeEtape(), etape);
		}		
		
		// On duplique ensuite tous les chemins de chaque étape
		for (EOEtape uneEtape : circuitValidation.toEtapes()) {
			for (EOChemin unChemin : uneEtape.toChemins()) {
				EOChemin chemin = new EOChemin();
				ec.insertObject(chemin);
				
				chemin.setCodeTypeChemin(unChemin.codeTypeChemin());
				chemin.setLibelleCourtTypeChemin(unChemin.libelleCourtTypeChemin());
				chemin.setToEtapeDepartRelationship(mapEtapesNouvelleVersion.get(unChemin.toEtapeDepart().codeEtape()));
				if (unChemin.toEtapeArrivee() != null) {
					chemin.setToEtapeArriveeRelationship(mapEtapesNouvelleVersion.get(unChemin.toEtapeArrivee().codeEtape()));
				}
			}
		}
		
		return nouvelleVersion;
	}
	
	/**
	 * Retourne le prochain numéro de version d'un circuit.
	 * 
	 * @return Le numéro de version suivant pour le circuit (de 1 à n) ((max + 1) ou 1 si c'est la première version)
	 */
	public Integer rechercherNumeroVersionSuivant() {
		EOQualifier qualifier = EOCircuitValidation.CODE_CIRCUIT_VALIDATION.eq(codeCircuitValidation())
				.and(ERXQ.equals(ERXQ.keyPath(EOCircuitValidation.TO_GD_APPLICATION_KEY, EOGdApplication.APP_STR_ID_KEY), toGdApplication().appStrId()));
		
		ERXFetchSpecification<EOCircuitValidation> fetchSpec = EOCircuitValidation.fetchSpec();
		fetchSpec.setQualifier(qualifier);
		fetchSpec.setSortOrderings(EOCircuitValidation.NUMERO_VERSION.descs());
		fetchSpec.setFetchLimit(1);
		
		NSArray<EOCircuitValidation> listeVersions = fetchSpec.fetchObjects(editingContext());
		
		Integer numeroVersion;
		
		if (listeVersions.isEmpty()) {
			numeroVersion = 1;
		} else {
			numeroVersion = listeVersions.get(0).numeroVersion() + 1;
		}
		
		return numeroVersion;
	}
	
	/**
	 * Rend "Utilisable" cette version du circuit.
	 * <p>
	 * Les autres versions du circuit passent à "Non utilisable".
	 */
	public void rendreUtilisable() {
		if (!isUtilisable()) {
			NSArray<EOCircuitValidation> listeCircuits = rechercherCircuitsValidation(editingContext(), toGdApplication().appStrId(), codeCircuitValidation());
			
			for (EOCircuitValidation unCircuit : listeCircuits) {
				if (unCircuit.isUtilisable()) {
					unCircuit.setTemoinUtilisable("N");
				}
			}
			
			setTemoinUtilisable("O");
		}
	}
	
	/**
	 * Retourne l'étape initiale du circuit.
	 * 
	 * @return L'étape initiale du circuit (ne retourne jamais <code>null</code>)
	 */
	public EOEtape etapeInitiale() {
		/* Recherche de l'étape initiale. */
		EOQualifier qualifier = EOEtape.TEMOIN_ETAPE_INITIALE.eq("O");
		NSArray<EOEtape> etapes = toEtapes(qualifier);
		
		if (etapes.size() == 0) {
			throw new CircuitValidationTechniqueException("Le circuit %s n'a pas d'étape initiale.", codeCircuitValidation());
		} else if (etapes.size() > 1) {
			throw new CircuitValidationTechniqueException("Le circuit %s a plusieurs étapes initiales.", codeCircuitValidation());
		}
		
		return etapes.get(0);
	}

	
	
	
	/**
	 * Retourne les étapes utilisées par ce circuit.
	 * <p>
	 * C'est un sous-ensemble des étapes disponibles (utilisables) pour un circuit (voir {@link #etapes()}).
	 * 
	 * @return La liste des étapes utilisées dans ce circuit
	 */
	public NSArray<EOEtape> etapesUtilisees() {
		NSArray<EOEtape> listeEtapesUtilisees = new NSMutableArray<EOEtape>();
		
		// L'étape initiale est toujours utilisées
		listeEtapesUtilisees.add(etapeInitiale());
		
		// On ajoute toutes les étapes d'arrivées d'un chemin
		for (EOEtape etape : toEtapes()) {
			for (EOChemin chemin : etape.toChemins()) {
				if (chemin.toEtapeArrivee() != null && !listeEtapesUtilisees.contains(chemin.toEtapeArrivee())) {
					listeEtapesUtilisees.add(chemin.toEtapeArrivee());
				}
			}
		}
		
		return listeEtapesUtilisees;
	}
	
	
	/**
	 * Retourne les étapes utilisées par ce circuit dans l'ordre du chemin.
	 * <p>
	 * C'est un sous-ensemble des étapes disponibles (utilisables) pour un circuit (voir {@link #etapes()}).
	 * @param codeTypeCheminToKeep codeTypeChemin sur lequel récupérer les étapes (ex. "VALIDER" ou "REFUSER")
	 * @param codeTypeCheminToLookAfter codeTypeChemin sur lequel on va regarder si une étape suivante avec codeTypeCheminToKeep est présente
	 * @return La liste des étapes utilisées dans ce circuit dans l'ordre défini par le chemin
	 */
	public NSArray<EOEtape> etapesUtiliseesSuivantOrdreChemin(String codeTypeCheminToKeep, String codeTypeCheminToLookAfter) {
		NSArray<EOEtape> listeEtapesUtilisees = new NSMutableArray<EOEtape>();
		
		EOEtape etapeDepart = etapeInitiale();
		
		// L'étape initiale est toujours utilisées
		listeEtapesUtilisees.add(etapeDepart);
		
		EOEtape etapeSuivante = null;
		
		if (!etapeDepart.isFinale()) {
			etapeSuivante = etapeDepart.etapeSuivante(codeTypeCheminToKeep);
		}
		boolean isFinale = false;
		
		while (etapeSuivante != null && !isFinale) {
			
			construireListeEtapesOrdreChemin(listeEtapesUtilisees, etapeSuivante, codeTypeCheminToKeep, codeTypeCheminToLookAfter);
			
			if (!etapeSuivante.isFinale()) {
				etapeSuivante = etapeSuivante.etapeSuivante(codeTypeCheminToKeep);
			} else {
				isFinale = true;
			}
		}
		
		return listeEtapesUtilisees;
	}
	
	/**
	 * Regarde de façon récursive toutes les étapes utilisées en passant par le chemin suivi par le circuit de validation
	 * @param listeEtapesUtilisees
	 * @param etapeEnCours
	 * @param codeTypeCheminToKeep
	 * @param codeTypeCheminToLookAfter
	 */
	private void construireListeEtapesOrdreChemin(NSArray<EOEtape> listeEtapesUtilisees, EOEtape etapeEnCours, String codeTypeCheminToKeep, String codeTypeCheminToLookAfter) {
		
		if (!listeEtapesUtilisees.contains(etapeEnCours)) {
			listeEtapesUtilisees.add(etapeEnCours);
		}
		
		EOEtape etapeAfter =  etapeEnCours.etapeSuivante(codeTypeCheminToLookAfter);
		
		if (etapeAfter != null && !listeEtapesUtilisees.containsObject(etapeAfter)) {
			EOEtape etapeSuivante = etapeAfter.etapeSuivante(codeTypeCheminToKeep);
			if (etapeSuivante != null) {
				construireListeEtapesOrdreChemin(listeEtapesUtilisees, etapeAfter, codeTypeCheminToKeep, codeTypeCheminToLookAfter);
			}
		}
		
	}
	
	
	/**
	 * Retourne la liste des demandes sur le circuit.
	 * 
	 * @return La liste des demandes se trouvant sur le circuit
	 */
	public NSArray<EODemande> demandes() {
		EOQualifier qualifier = EODemande.TO_ETAPE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(codeCircuitValidation())
				.and(EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.NUMERO_VERSION).eq(numeroVersion()));
		
		NSArray<EODemande> listeDemandes = EODemande.fetchDemandes(editingContext(), qualifier, null);
		
		return listeDemandes;
	}
	
	/**
	 * Est-ce qu'il y a des demandes sur le circuit ?
	 * 
	 * @return <code>true</code> s'il y a des demandes se trouvant sur le circuit
	 */
	public boolean hasDemandes() {
		for (EOEtape etape : etapesUtilisees()) {
			if (etape.toDemandes().size() != 0) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Est-ce qu'il y a eu des demandes sur le circuit ?
	 * <p>
	 * Recherche dans l'historique des demandes
	 * 
	 * @return <code>true</code> s'il y a des demandes se trouvant sur le circuit
	 */
	public boolean hasEuDemandes() {
		EOQualifier qualifier = EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(codeCircuitValidation())
				.and(EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.NUMERO_VERSION).eq(numeroVersion()));

		ERXFetchSpecification<EOHistoriqueDemande> fetchSpec = EOHistoriqueDemande.fetchSpec();
		fetchSpec.setQualifier(qualifier);
		fetchSpec.setFetchLimit(1);
		
		NSArray<EOHistoriqueDemande> listeHistoriqueDemandes = fetchSpec.fetchObjects(editingContext());
		
		return listeHistoriqueDemandes.size() != 0;
	}

	/**
	 * Est-ce que ce circuit est utilisable ?
	 * 
	 * @return <code>true</code> si le circuit est utilisable
	 */
	public boolean isUtilisable() {
		return "O".equals(temoinUtilisable());
	}
	
	/**
	 * Est-ce que le circuit est modifiable ?
	 * <p>
	 * Un circuit n'est plus modifiable dès qu'il y a une demande sur ce circuit.
	 * 
	 * @return <code>true</code> si le circuit est modifiable
	 */
	public boolean isModifiable() {
		return !hasEuDemandes();
	}
	
	@Override
	public void validateForSave() throws ValidationException {
		this.setDateModification(new NSTimestamp());
	}

	/**
	 * Est-ce que l'une des étapes du circuit de validation contient le code de l'étape passé en paramétre ?
	 * @param codeEtape code d'une étape 
	 * @return <code>true</code> si cette l'étape est utilisée par le circuit de validation
	 */
	public boolean estEtapeUtilisee(String codeEtape) {
		
		boolean existEtape = false;
		
		if (StringCtrl.isEmpty(codeEtape)) {
			return existEtape;
		}
		
		List<EOEtape> listeEtape = this.etapesUtilisees();
		
		for (EOEtape etapeAComparer : listeEtape) {
			
			if (codeEtape.equals(etapeAComparer.codeEtape())) {
				existEtape = true;
				break;
			}
			
		}
		return existEtape;
	}
	
	/**
	 * Retourne le texte pour la version du circuit .
	 * 
	 * @return Le texte affiché dans la combo-box de la liste des version du circuit de validation sélectionné
	 */
	public String texteVersion() {
		String texteVersion;
		
		if (this.numeroVersion() == null) {
			texteVersion = texteVersionNonExistante();
		} else {
			texteVersion = texteVersionExistante(true);
		}
		
		return texteVersion;
	}

	private String texteVersionExistante(boolean showUtilisation) {
		String texteVersion;
		String util = StringUtils.EMPTY;
		
		if (showUtilisation) {
			util += this.isUtilisable() ? "(active/" : "(";
			if (this.hasEuDemandes()) {
				util += "utilisée)";
			} else {
				util += "non utilisée)";
			}
		} else {
			util += this.isUtilisable() ? "(active)" : "";
		}
		
		texteVersion = "V" + this.numeroVersion() + " du " + DateCtrl.formatDateShort(this.dateModification()) + " " + util;
		return texteVersion;
	}

	private String texteVersionNonExistante() {
		return "Version en cours de définition...";
	}
	
	/**
	 * 
	 * 
	 * 
	 * @return Retourne un texte dit "simple" (pas de notion utilisé / pas utilisé) pour la version du circuit. 
	 */
	public String texteVersionSimple() {
		String texteVersion;
		
		if (this.numeroVersion() == null) {
			texteVersion = texteVersionNonExistante();
		} else {
			texteVersion = texteVersionExistante(false);
		}
		
		return texteVersion;
	}
	
	
}
