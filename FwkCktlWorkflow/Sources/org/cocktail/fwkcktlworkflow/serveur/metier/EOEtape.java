package org.cocktail.fwkcktlworkflow.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationTechniqueException;

import com.webobjects.foundation.NSArray;

/**
 * Cette classe représente une étape.
 * @author yannick
 * @see EOChemin
 * @see EOCircuitValidation
 *
 */
public class EOEtape extends _EOEtape implements IEtape {

	private static final long serialVersionUID = 7426932701334196725L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOEtape.class);
	
	/**
	 * Est-ce que cette étape est une étape initiale ?
	 * 
	 * @return <code>true</code> si c'est une étape initiale
	 */
	public boolean isInitiale() {
		return "O".equals(temoinEtapeInitiale());
	}
	
	/**
	 * Est-ce que cette étape est une étape finale ?
	 * 
	 * Une étape finale est une étape sans chemin possible.
	 * 
	 * TODO Est-ce qu'une étape doit être concidérée comme finale si elle à des chemins mais tous non empruntables ?
	 * 
	 * @return <code>true</code> si c'est une étape finale
	 */
	public boolean isFinale() {
		return toChemins().size() == 0;
	}
	
	/**
	 * Retourne le chemin correspondant à ce type de chemin.
	 * 
	 * Attention, ça ne veut pas dire que le chemin est empruntable (débouche sur une étape). 
	 * 
	 * @param codeTypeChemin Le code du type de chemin
	 * @return Le chemin ou <code>null</code> s'il n'y a pas de chemin pour ce type de chemin.
	 * @see #cheminEmpruntable(String)
	 */
	public EOChemin chemin(String codeTypeChemin) {
		NSArray<EOChemin> chemins = toChemins(EOChemin.CODE_TYPE_CHEMIN.eq(codeTypeChemin));
		
		if (chemins.size() > 1) {
			throw new CircuitValidationTechniqueException("L'étape %s a plusieurs chemins de type %s.", codeEtape(), codeTypeChemin);
		} else if (chemins.size() == 1) {
			return chemins.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * Retourne le chemin vers l'étape suivante correspondant à ce type de chemin.
	 * 
	 * @param codeTypeChemin Le code du type de chemin
	 * @return Le chemin ou <code>null</code> s'il n'y a pas de chemin empruntable pour ce type de chemin.
	 */
	public EOChemin cheminEmpruntable(String codeTypeChemin) {
		EOChemin chemin = chemin(codeTypeChemin);
		
		if (chemin != null && chemin.toEtapeArrivee() != null) {
			return chemin;
		} else {
			return null;
		}
	}
	
	/**
	 * Retourne <code>true</code> si le type chemin passé en paramètre peut être emprunté pour cette étape.
	 * 
	 * C'est-à-dire qu'une étape est définie au bout de ce chemin.
	 * 
	 * @param codeTypeChemin Le code du type de chemin
	 * @return <code>true</code> si on peut emprunter ce chemin
	 */
	public boolean isCheminEmpruntable(String codeTypeChemin) {
		return cheminEmpruntable(codeTypeChemin) != null ? true : false;
	}
	
	/**
	 * Retourne <code>true</code> si le type de chemin passé en paramètre est connu de l'étape.
	 * 
	 * Attention, ça ne veut pas dire que le chemin est empruntable (débouche sur une étape). 
	 * 
	 * @param codeTypeChemin Le code du type de chemin
	 * @return <code>true</code> si le type de chemin est connu de l'étape
	 * @see #isCheminEmpruntable(String)
	 */
	public boolean hasChemin(String codeTypeChemin) {
		return chemin(codeTypeChemin) != null ? true : false;
	}

	/**
	 * Retourne l'étape suivante d'une étape à l'aide du chemin
	 * @param codeTypeChemin type du chemin
	 * @return étape suivante
	 */
	public EOEtape etapeSuivante(String codeTypeChemin) {
		
		EOEtape etapeSuivante = null;
		
		if (!this.isFinale()) {
		
			for (EOChemin chemin : this.toChemins()) {
				
				if (codeTypeChemin.equals(chemin.codeTypeChemin())) {
					if (this.equals(chemin.toEtapeDepart())) {
						
						etapeSuivante = chemin.toEtapeArrivee();
						break;
					}
				}
			}
			
		}
		return etapeSuivante;
		
	}
	
	

	public String getCodeEtape() {
		return codeEtape();
	}
	
	public EOCircuitValidation getToCircuitValidation() {
		return toCircuitValidation();
	}
}
