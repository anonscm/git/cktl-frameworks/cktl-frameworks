// DO NOT EDIT.  Make changes to EODemande.java instead.
package org.cocktail.fwkcktlworkflow.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODemande extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Demande";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtape");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> TO_HISTORIQUE_DEMANDES = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande>("toHistoriqueDemandes");

  // Attributes
  // Relationships
  public static final String TO_ETAPE_KEY = TO_ETAPE.key();
  public static final String TO_HISTORIQUE_DEMANDES_KEY = TO_HISTORIQUE_DEMANDES.key();

  private static Logger LOG = Logger.getLogger(_EODemande.class);

  public EODemande localInstanceIn(EOEditingContext editingContext) {
    EODemande localInstance = (EODemande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtape() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape)storedValueForKey(_EODemande.TO_ETAPE_KEY);
  }
  
  public void setToEtape(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    takeStoredValueForKey(value, _EODemande.TO_ETAPE_KEY);
  }

  public void setToEtapeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    if (_EODemande.LOG.isDebugEnabled()) {
      _EODemande.LOG.debug("updating toEtape from " + toEtape() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtape(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape oldValue = toEtape();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODemande.TO_ETAPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODemande.TO_ETAPE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> toHistoriqueDemandes() {
    return (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande>)storedValueForKey(_EODemande.TO_HISTORIQUE_DEMANDES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> toHistoriqueDemandes(EOQualifier qualifier) {
    return toHistoriqueDemandes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> toHistoriqueDemandes(EOQualifier qualifier, boolean fetch) {
    return toHistoriqueDemandes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> toHistoriqueDemandes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande.TO_DEMANDE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande.fetchHistoriqueDemandes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toHistoriqueDemandes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToHistoriqueDemandes(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande object) {
    includeObjectIntoPropertyWithKey(object, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
  }

  public void removeFromToHistoriqueDemandes(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande object) {
    excludeObjectFromPropertyWithKey(object, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
  }

  public void addToToHistoriqueDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande object) {
    if (_EODemande.LOG.isDebugEnabled()) {
      _EODemande.LOG.debug("adding " + object + " to toHistoriqueDemandes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToHistoriqueDemandes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
    }
  }

  public void removeFromToHistoriqueDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande object) {
    if (_EODemande.LOG.isDebugEnabled()) {
      _EODemande.LOG.debug("removing " + object + " from toHistoriqueDemandes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToHistoriqueDemandes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
    }
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande createToHistoriqueDemandesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande) eo;
  }

  public void deleteToHistoriqueDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODemande.TO_HISTORIQUE_DEMANDES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToHistoriqueDemandesRelationships() {
    Enumeration<org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande> objects = toHistoriqueDemandes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToHistoriqueDemandesRelationship(objects.nextElement());
    }
  }


  public static EODemande createDemande(EOEditingContext editingContext, org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtape) {
    EODemande eo = (EODemande) EOUtilities.createAndInsertInstance(editingContext, _EODemande.ENTITY_NAME);    
    eo.setToEtapeRelationship(toEtape);
    return eo;
  }

  public static ERXFetchSpecification<EODemande> fetchSpec() {
    return new ERXFetchSpecification<EODemande>(_EODemande.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODemande> fetchAllDemandes(EOEditingContext editingContext) {
    return _EODemande.fetchAllDemandes(editingContext, null);
  }

  public static NSArray<EODemande> fetchAllDemandes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODemande.fetchDemandes(editingContext, null, sortOrderings);
  }

  public static NSArray<EODemande> fetchDemandes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODemande> fetchSpec = new ERXFetchSpecification<EODemande>(_EODemande.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODemande> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODemande fetchDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EODemande.fetchDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODemande fetchDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODemande> eoObjects = _EODemande.fetchDemandes(editingContext, qualifier, null);
    EODemande eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Demande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODemande fetchRequiredDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EODemande.fetchRequiredDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODemande fetchRequiredDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    EODemande eoObject = _EODemande.fetchDemande(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Demande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODemande localInstanceIn(EOEditingContext editingContext, EODemande eo) {
    EODemande localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
