package org.cocktail.fwkcktlworkflow.serveur.metier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationTechniqueException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXS;

/**
 * <p>
 * La classe <code>EODemande</code> représente une entité qui va circuler sur un
 * {@link EOCircuitValidation}.
 * </p>
 * <h4>Vocabulaire</h4>
 * <ul>
 * <li>Un circuit de validation est composé d'étapes</li>
 * <li>Les étapes sont reliées entres elles par des chemins</li>
 * <li>Ces chemins sont unidirectionnels</li>
 * <li>Un chemin relie une étape de départ à une étape d'arrivée</li>
 * <li>Une seule étape sur un circuit donné peut être l'étape initiale</li>
 * <li>Ce qui circule sur le circuit est une demande</li>
 * <li>Quand une demande est crée, elle est placée sur l'étape initiale</li>
 * <li>Elle n'a pas besoin de connaître spécifiquement son sujet</li>
 * <li>Une demande peut avancer vers une autre étape s'il existe un chemin</li>
 * <li>Chaque changement d'étape donne lieu à une historisation</li>
 * </ul>
 * <p>
 * Un circuit de validation est donc l'ensemble des étapes qui le composent. Ce
 * circuit peut-être représenté sous forme de graphe.
 * </p>
 * <p>
 * <i>Exemple</i> :
 * </p>
 *
 * <pre>
 * +----------------+   +---------+                 +---------+
 * | Etape initiale |-->| Etape 2 |---------------->| Etape 4 |
 * +----------------+   +---------+                 +---------+
 *                           |                           ^
 *                           |        +---------+        |
 *                           +------->| Etape 3 |--------+
 *                                    +---------+
 * </pre>
 * <p>
 * Dans cet exemple, il n'y a qu'un chemin pour passer de l'étape initiale à
 * l'étape 2. Pour quitter l'étape 2, en revanche, il y a deux chemins
 * possibles. C'est l'application utilisatrice qui décidera quel chemin
 * emprunter.
 * </p>
 * <h4>Création d'une demande</h4>
 * <p>
 * {@link EODemande#creerDemande}
 * </p>
 * <p>
 * <i>Exemple</i> :
 * </p>
 * <p>
 * <code>
 * EODemande demande = Demande.creerDemande(ec, "DEFAUT", appUser.persId());
 * </code>
 * </p>
 * <p>
 * Ceci permet de créer une demande en base de donnée, et de déclancher
 * l'enregistrement d'une trace de création. La demande est automatiquement
 * placée sur l'étape initiale du circuit.
 * </p>
 * <h4>Lire une demande depuis la base de données</h4>
 * <p>
 * Il n'y a pas de méthode pour lire directement une demande depuis la base. En
 * effet, en général on souhaite charger également les sujets associés aux
 * demandes.
 * </p>
 * <p>
 * Il existe donc une méthode qui fabrique un EOQualifier permettant de charger
 * les demandes en même temps que les sujets :
 * </p>
 * <p>
 * <code>EOQualifier {@link #qualifierPourEtape}</code>
 * </p>
 * <p>
 * <i>Exemple</i> :
 * </p>
 * <p>
 * <code>
 * EOQualifier qualifier = EODemande.qualifierPourEtape(EOFicheService.DEMANDE,
 * "ETAPE_2", "DEFAUT");<br/>
 * NSArray<EOFicheService> fiches =
 * EOFicheServices.fetchFicheServices(ec, qualifier, null);<br/>
 * </code>
 * </p>
 * <p>
 * Ici <code>EOFicheService.DEMANDE</code> indique que dans l'
 * <code>EOModeler</code>, on a créé une relation entre
 * <code>EOFicheService</code> et {@link EODemande}, et que cette relation se
 * nomme "demande".
 * </p>
 * <p>
 * Le qualifier peut être utiliser pour rafiner une recherche :
 * </p>
 * <p>
 * <code>EOQualifier qualifier = EOFicheService.REPARTITEUR.eq(repartiteur).and(
 * EODemande.qualifierPourEtape(EOFicheService.DEMANDE, "ETAPE_2", "DEFAUT"));</code>
 * </p>
 * <h4>Faire avancer une demande sur le circuit</h4>
 * <p>
 * Pour faire avancer une demande à une étape suivante, il faut appeler :
 * </p>
 * <p>
 * {@link EODemande#faireAvancerVers}
 * </p>
 * <p>
 * <i>Exemple</i> :
 * </p>
 * <p>
 * <code>demande.faireAvancerVers("ETAPE_3", appUser.persId());</code>
 * </p>
 * <p>
 * L'étape d'arrivée doit être directement reliée à l'étape de départ faute de
 * quoi une exception est levée.
 * </p>
 * <p>
 * Dans le cas ou il n'existe qu'un seul chemin entre l'étape de départ et
 * l'étape d'arrivée, il est possible d'appeler la méthode
 * </p>
 * <p>
 * {@link EODemande#faireAvancer}
 * </p>
 * <p>
 * Une exception est levée s'il n'y a pas de chemin partant de l'étape de
 * départ, ou s'il en existe plusieurs.
 * </p>
 *
 * @author yannick
 *
 */
public class EODemande extends _EODemande implements IDemande {
	/**
	 *
	 */
	private static final long serialVersionUID = -8594447462929994495L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODemande.class);

	/**
	 * Créer une instance de <code>EODemande</code>.
	 *
	 * @param ec
	 *            le contexte d'édition pour l'accès à la base de donnée.
	 * @param codeApplication
	 *            le code application sur laquelle le circuit est rattaché
	 * @param codeCircuit
	 *            le code du circuit sur lequel la demande va circuler.
	 * @param persIdDemandeur
	 *            l'identifiant de la personne qui créer la demande.
	 * @return une instance de <code>EODemande</code> dont l'étape active est
	 *         l'étape initiale du circuit indiqué par le paramètre
	 *         <code>codeCircuit</code>.
	 * @see EOEtape
	 */
	public static EODemande creerDemande(EOEditingContext ec,
			String codeApplication, String codeCircuit, Integer persIdDemandeur) {

		/* Recherche du circuit en base de données. */
		EOCircuitValidation circuitValidation = EOCircuitValidation
				.rechercherCircuitValidationUtilisable(ec, codeApplication, codeCircuit);
		
		// Recherche de l'étape initiale
		EOEtape etapeInitiale = circuitValidation.etapeInitiale();
		
		/* Création de la demande. */
		EODemande demande = EODemande.createDemande(ec, etapeInitiale);
		ec.insertObject(demande);

		/* Historisation de la création de la demande. */
		EOHistoriqueDemande.creerHistoriqueDemande(ec, persIdDemandeur,
				demande, null, demande.toEtape());

		return demande;
	}

	/**
	 * Cette methode permet de placer une demande existante sur l'etape initiale
	 * d'un circuit.
	 * @param codeApplication
     *            le code application sur laquelle le circuit est rattaché
	 * @param codeCircuit
	 *            code du nouveau circuit.
	 * @param persIdModification
	 *            l'identifiant de la personne qui créer la demande.
	 */
	public void changerCircuit(String codeApplication, String codeCircuit, Integer persIdModification) {

		// on récupere le nouveau circuit
		EOCircuitValidation newCircuitValidation = EOCircuitValidation
				.rechercherCircuitValidationUtilisable(editingContext(), codeApplication, codeCircuit);

		// si le circuit n'existe pas
		if (newCircuitValidation == null) {
			throw new CircuitValidationTechniqueException(
					"Le circuit %s n'existe pas", codeCircuit);
		}

		// on récupere l'étape initiale du nouveau circuit
		EOEtape etape = newCircuitValidation.etapeInitiale();

		// Sinon on rajoute la demande sur l'étape initiale du nouveau circuit
		avancerAvecHistoriqueVers(etape, persIdModification);
	}

	/**
	 * Fabrique une instance de {@link EOQualifier} permettant de rafinier une
	 * recherche en base de données.
	 *
	 * @param key
	 *            l'instance de {@link ERXKey} à partir de laquelle rechercher
	 *            les demandes.
	 * @param codeEtape
	 *            le code de l'étape sur laquelle la demande doit être
	 *            positionnée.
	 * @param codeCircuit
	 *            le code du circuit sur lequel l'étape doit se trouver.
	 * @return une instance de {@link EOQualifier}.
	 */
	public static EOQualifier qualifierPourEtape(ERXKey<EODemande> key,
			String codeEtape, String codeCircuit) {

		return key
				.dot(EODemande.TO_ETAPE)
				.dot(EOEtape.CODE_ETAPE_KEY)
				.eq(codeEtape)
				.and(key.dot(EODemande.TO_ETAPE).dot(EOEtape.TO_CIRCUIT_VALIDATION)
						.dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY).eq(codeCircuit));
	}
	
	/**
	 * Fait avancer la demande en empruntant le chemin correspondant au type de chemin passé en paramètre.
	 * 
	 * @param codeTypeChemin Le type de chemin à emprunter
	 * @param persIdModification L'identifiant de la personne qui fait avancer la demande
	 */
	public void faireAvancerSurChemin(String codeTypeChemin, Integer persIdModification) {
		EOChemin chemin = toEtape().cheminEmpruntable(codeTypeChemin);
		
		if (chemin == null) {
			if (toEtape().hasChemin(codeTypeChemin)) {
				throw new CircuitValidationTechniqueException("L'étape %s n'a pas de chemin de type %s.", toEtape().codeEtape(), codeTypeChemin);
			} else {
				throw new CircuitValidationTechniqueException("L'étape %s n'a pas de chemin empruntable de type %s.", toEtape().codeEtape(), codeTypeChemin);
			}
		}
		
		avancerAvecHistoriqueVers(chemin.toEtapeArrivee(), persIdModification);
	}
	
	/**
	 * Avance la demande vers l'étape d'arrivée en historisant le parcours.
	 * 
	 * @param etapeArrivee L'étape d'arrivée
	 * @param persIdModification L'identifiant de la personne qui fait avancer la demande
	 */
	private void avancerAvecHistoriqueVers(EOEtape etapeArrivee,
			Integer persIdModification) {
		EOHistoriqueDemande.creerHistoriqueDemande(editingContext(),
				persIdModification, this, toEtape(), etapeArrivee);
		setToEtapeRelationship(etapeArrivee);
	}

	/**
	 * A partir de l'historique de la demande, on récupère les cricuits que cette demande a parcouru.
	 * Les circuits sont retournés dans l'ordre chronologique.
	 * 
	 * @return La liste des circuits empruntés par cette demande
	 */	
	public NSArray<EOCircuitValidation> rechercherCircuitsEmpruntes() {
		return rechercherCircuitsEmpruntes(false);
	}
	
	/**
	 * A partir de l'historique de la demande, on récupère les cricuits que cette demande a parcouru.
	 * <p>
	 * Si <code>distinctCircuit</code> est à true, pour un même code circuit, on ne retourne qu'une seule version de ce circuit (la dernière utilisée).
	 * 
	 * @param distinctCircuit Est-ce qu'il il a un distinct sur le circuit ? (ne retourne qu'une seul version d'un même circuit ?)
	 * @return La liste des circuits empruntés par cette demande
	 */
	public NSArray<EOCircuitValidation> rechercherCircuitsEmpruntes(boolean distinctCircuit) {
		NSArray<EOCircuitValidation> listeCircuits = new NSMutableArray<EOCircuitValidation>();
		Map<String, EOCircuitValidation> circuits = new HashMap<String, EOCircuitValidation>();
		
		for (EOHistoriqueDemande historiqueDemande : historiqueChronologique()) {
			EOCircuitValidation circuit = historiqueDemande.toEtapeArrivee().toCircuitValidation();
			
			if (!distinctCircuit || (distinctCircuit && !listeCircuits.contains(circuit))) {
				if (circuits.containsKey(circuit.codeCircuitValidation())) {
					listeCircuits.remove(circuits.get(circuit.codeCircuitValidation()));
				}
				
				listeCircuits.add(circuit);
				circuits.put(circuit.codeCircuitValidation(), circuit);
			}
		}
		
		return listeCircuits;
	}
	
	/**
	 * Retourne l'historique dans l'ordre chronologique de la date de modification.
	 * 
	 * @return L'historique de la demande par ordre chronologique de la date de modification
	 */
	public NSArray<EOHistoriqueDemande> historiqueChronologique() {
		return historiqueChronologique(null);
	}

	/**
	 * Retourne l'historique dans l'ordre chronologique de la date de modification.
	 * 
	 * @param qualifier Un qualifieur
	 * @return L'historique de la demande par ordre chronologique de la date de modification
	 */
	public NSArray<EOHistoriqueDemande> historiqueChronologique(EOQualifier qualifier) {
		return toHistoriqueDemandes(qualifier, EOHistoriqueDemande.DATE_MODIFICATION.ascs(), true);
	}



	public IEtape getToEtape() {
		return toEtape();
	}

	public List<? extends IHistoriqueDemande> getToHistoriqueDemandes() {
		return toHistoriqueDemandes();
	}
	
	/**
	 * réinitialise une demande vers l'étape initiale de son circuit 
	 * de validation sans ajout dans l'historique
	 */
	public void reinitialiser() {
		reinitialiserSurCircuit(this.toEtape().toCircuitValidation());
	}
	
	/**
	 * réinitialise une demande vers l'étape initiale de son circuit 
	 * de validation sans ajout dans l'historique
	 * @param circuitValidation circuit de validation cible
	 */
	public void reinitialiserSurCircuit(EOCircuitValidation circuitValidation) {
		this.setToEtape(circuitValidation.etapeInitiale());
	}
	
	/**
	 * est-ce que la demande est sur l'étape initiale du circuit ?
	 * @return  true / false
	 */
	public boolean estSurEtapeInitiale() {
		return this.toEtape().equals(this.toEtape().toCircuitValidation().etapeInitiale());
	}
	
	
	public void supprimerAvecHistorique() {
		this.deleteAllToHistoriqueDemandesRelationships();
		this.delete();
	}
	
	
}
