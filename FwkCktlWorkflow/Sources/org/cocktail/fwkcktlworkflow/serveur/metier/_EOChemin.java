// DO NOT EDIT.  Make changes to EOChemin.java instead.
package org.cocktail.fwkcktlworkflow.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOChemin extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Chemin";

  // Attribute Keys
  public static final ERXKey<String> CODE_TYPE_CHEMIN = new ERXKey<String>("codeTypeChemin");
  public static final ERXKey<String> LIBELLE_COURT_TYPE_CHEMIN = new ERXKey<String>("libelleCourtTypeChemin");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPE_ARRIVEE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtapeArrivee");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPE_DEPART = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtapeDepart");

  // Attributes
  public static final String CODE_TYPE_CHEMIN_KEY = CODE_TYPE_CHEMIN.key();
  public static final String LIBELLE_COURT_TYPE_CHEMIN_KEY = LIBELLE_COURT_TYPE_CHEMIN.key();
  // Relationships
  public static final String TO_ETAPE_ARRIVEE_KEY = TO_ETAPE_ARRIVEE.key();
  public static final String TO_ETAPE_DEPART_KEY = TO_ETAPE_DEPART.key();

  private static Logger LOG = Logger.getLogger(_EOChemin.class);

  public EOChemin localInstanceIn(EOEditingContext editingContext) {
    EOChemin localInstance = (EOChemin)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeTypeChemin() {
    return (String) storedValueForKey(_EOChemin.CODE_TYPE_CHEMIN_KEY);
  }

  public void setCodeTypeChemin(String value) {
    if (_EOChemin.LOG.isDebugEnabled()) {
    	_EOChemin.LOG.debug( "updating codeTypeChemin from " + codeTypeChemin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChemin.CODE_TYPE_CHEMIN_KEY);
  }

  public String libelleCourtTypeChemin() {
    return (String) storedValueForKey(_EOChemin.LIBELLE_COURT_TYPE_CHEMIN_KEY);
  }

  public void setLibelleCourtTypeChemin(String value) {
    if (_EOChemin.LOG.isDebugEnabled()) {
    	_EOChemin.LOG.debug( "updating libelleCourtTypeChemin from " + libelleCourtTypeChemin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChemin.LIBELLE_COURT_TYPE_CHEMIN_KEY);
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeArrivee() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape)storedValueForKey(_EOChemin.TO_ETAPE_ARRIVEE_KEY);
  }
  
  public void setToEtapeArrivee(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    takeStoredValueForKey(value, _EOChemin.TO_ETAPE_ARRIVEE_KEY);
  }

  public void setToEtapeArriveeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    if (_EOChemin.LOG.isDebugEnabled()) {
      _EOChemin.LOG.debug("updating toEtapeArrivee from " + toEtapeArrivee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtapeArrivee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape oldValue = toEtapeArrivee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOChemin.TO_ETAPE_ARRIVEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOChemin.TO_ETAPE_ARRIVEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeDepart() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape)storedValueForKey(_EOChemin.TO_ETAPE_DEPART_KEY);
  }
  
  public void setToEtapeDepart(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    takeStoredValueForKey(value, _EOChemin.TO_ETAPE_DEPART_KEY);
  }

  public void setToEtapeDepartRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    if (_EOChemin.LOG.isDebugEnabled()) {
      _EOChemin.LOG.debug("updating toEtapeDepart from " + toEtapeDepart() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtapeDepart(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape oldValue = toEtapeDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOChemin.TO_ETAPE_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOChemin.TO_ETAPE_DEPART_KEY);
    }
  }
  

  public static EOChemin createChemin(EOEditingContext editingContext, String codeTypeChemin
, String libelleCourtTypeChemin
, org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeDepart) {
    EOChemin eo = (EOChemin) EOUtilities.createAndInsertInstance(editingContext, _EOChemin.ENTITY_NAME);    
		eo.setCodeTypeChemin(codeTypeChemin);
		eo.setLibelleCourtTypeChemin(libelleCourtTypeChemin);
    eo.setToEtapeDepartRelationship(toEtapeDepart);
    return eo;
  }

  public static ERXFetchSpecification<EOChemin> fetchSpec() {
    return new ERXFetchSpecification<EOChemin>(_EOChemin.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOChemin> fetchAllChemins(EOEditingContext editingContext) {
    return _EOChemin.fetchAllChemins(editingContext, null);
  }

  public static NSArray<EOChemin> fetchAllChemins(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChemin.fetchChemins(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChemin> fetchChemins(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOChemin> fetchSpec = new ERXFetchSpecification<EOChemin>(_EOChemin.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChemin> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOChemin fetchChemin(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChemin.fetchChemin(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChemin fetchChemin(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChemin> eoObjects = _EOChemin.fetchChemins(editingContext, qualifier, null);
    EOChemin eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Chemin that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChemin fetchRequiredChemin(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChemin.fetchRequiredChemin(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChemin fetchRequiredChemin(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChemin eoObject = _EOChemin.fetchChemin(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Chemin that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChemin localInstanceIn(EOEditingContext editingContext, EOChemin eo) {
    EOChemin localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
