package org.cocktail.fwkcktlworkflow.serveur.metier;

import java.util.Date;

public interface IHistoriqueDemande {

	IEtape toEtapeArrivee();
	
	Date dateModification();
	
	IDemande toDemande();
}
