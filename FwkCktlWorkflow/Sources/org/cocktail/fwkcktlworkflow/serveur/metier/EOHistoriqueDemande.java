package org.cocktail.fwkcktlworkflow.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente une entrée d'historique de modification de l'état
 * d'une demande.
 * 
 * @author yannick
 * @see EOEtape
 * @see EODemande
 * 
 */
public class EOHistoriqueDemande extends _EOHistoriqueDemande implements IHistoriqueDemande {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1732836443816661880L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOHistoriqueDemande.class);

	/**
	 * Cette méthode permet de créer une entrée d'historique de modification de
	 * l'état d'une demande.
	 * 
	 * @param ec
	 *            le contexte d'édition pour accéder à la base de données.
	 * @param persIdDemandeur
	 *            l'identifiant de la personne qui modifie l'état de la demande.
	 * @param demande
	 *            la demande dont l'état est modifiée.
	 * @param etapeDepart
	 *            l'étape de départ de la demande.
	 * @param etapeArrivee
	 *            l'étape d'arrivée de la demande.
	 * @return une instance de <code>EOHistoriqueDemande</code>.
	 */
	public static EOHistoriqueDemande creerHistoriqueDemande(
			EOEditingContext ec, Integer persIdDemandeur, EODemande demande,
			EOEtape etapeDepart, EOEtape etapeArrivee) {
		EOHistoriqueDemande historiqueDemande = EOHistoriqueDemande
				.createHistoriqueDemande(ec, new NSTimestamp(), demande,	etapeArrivee);
		historiqueDemande.setPersIdModification(persIdDemandeur);
		historiqueDemande.setToEtapeDepart(etapeDepart);
		ec.insertObject(historiqueDemande);
		return historiqueDemande;
	}

}
