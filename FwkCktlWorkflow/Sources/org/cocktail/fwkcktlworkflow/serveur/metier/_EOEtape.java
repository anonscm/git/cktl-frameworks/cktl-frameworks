// DO NOT EDIT.  Make changes to EOEtape.java instead.
package org.cocktail.fwkcktlworkflow.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEtape extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Etape";

  // Attribute Keys
  public static final ERXKey<String> CODE_ETAPE = new ERXKey<String>("codeEtape");
  public static final ERXKey<String> LIBELLE_ETAPE = new ERXKey<String>("libelleEtape");
  public static final ERXKey<String> TEMOIN_ETAPE_INITIALE = new ERXKey<String>("temoinEtapeInitiale");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> TO_CHEMINS = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin>("toChemins");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation> TO_CIRCUIT_VALIDATION = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation>("toCircuitValidation");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDES = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemandes");

  // Attributes
  public static final String CODE_ETAPE_KEY = CODE_ETAPE.key();
  public static final String LIBELLE_ETAPE_KEY = LIBELLE_ETAPE.key();
  public static final String TEMOIN_ETAPE_INITIALE_KEY = TEMOIN_ETAPE_INITIALE.key();
  // Relationships
  public static final String TO_CHEMINS_KEY = TO_CHEMINS.key();
  public static final String TO_CIRCUIT_VALIDATION_KEY = TO_CIRCUIT_VALIDATION.key();
  public static final String TO_DEMANDES_KEY = TO_DEMANDES.key();

  private static Logger LOG = Logger.getLogger(_EOEtape.class);

  public EOEtape localInstanceIn(EOEditingContext editingContext) {
    EOEtape localInstance = (EOEtape)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeEtape() {
    return (String) storedValueForKey(_EOEtape.CODE_ETAPE_KEY);
  }

  public void setCodeEtape(String value) {
    if (_EOEtape.LOG.isDebugEnabled()) {
    	_EOEtape.LOG.debug( "updating codeEtape from " + codeEtape() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtape.CODE_ETAPE_KEY);
  }

  public String libelleEtape() {
    return (String) storedValueForKey(_EOEtape.LIBELLE_ETAPE_KEY);
  }

  public void setLibelleEtape(String value) {
    if (_EOEtape.LOG.isDebugEnabled()) {
    	_EOEtape.LOG.debug( "updating libelleEtape from " + libelleEtape() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtape.LIBELLE_ETAPE_KEY);
  }

  public String temoinEtapeInitiale() {
    return (String) storedValueForKey(_EOEtape.TEMOIN_ETAPE_INITIALE_KEY);
  }

  public void setTemoinEtapeInitiale(String value) {
    if (_EOEtape.LOG.isDebugEnabled()) {
    	_EOEtape.LOG.debug( "updating temoinEtapeInitiale from " + temoinEtapeInitiale() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtape.TEMOIN_ETAPE_INITIALE_KEY);
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation toCircuitValidation() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation)storedValueForKey(_EOEtape.TO_CIRCUIT_VALIDATION_KEY);
  }
  
  public void setToCircuitValidation(org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation value) {
    takeStoredValueForKey(value, _EOEtape.TO_CIRCUIT_VALIDATION_KEY);
  }

  public void setToCircuitValidationRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation value) {
    if (_EOEtape.LOG.isDebugEnabled()) {
      _EOEtape.LOG.debug("updating toCircuitValidation from " + toCircuitValidation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToCircuitValidation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation oldValue = toCircuitValidation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtape.TO_CIRCUIT_VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtape.TO_CIRCUIT_VALIDATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> toChemins() {
    return (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin>)storedValueForKey(_EOEtape.TO_CHEMINS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> toChemins(EOQualifier qualifier) {
    return toChemins(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> toChemins(EOQualifier qualifier, boolean fetch) {
    return toChemins(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> toChemins(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin.TO_ETAPE_DEPART_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin.fetchChemins(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toChemins();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToChemins(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin object) {
    includeObjectIntoPropertyWithKey(object, _EOEtape.TO_CHEMINS_KEY);
  }

  public void removeFromToChemins(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin object) {
    excludeObjectFromPropertyWithKey(object, _EOEtape.TO_CHEMINS_KEY);
  }

  public void addToToCheminsRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin object) {
    if (_EOEtape.LOG.isDebugEnabled()) {
      _EOEtape.LOG.debug("adding " + object + " to toChemins relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToChemins(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEtape.TO_CHEMINS_KEY);
    }
  }

  public void removeFromToCheminsRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin object) {
    if (_EOEtape.LOG.isDebugEnabled()) {
      _EOEtape.LOG.debug("removing " + object + " from toChemins relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToChemins(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtape.TO_CHEMINS_KEY);
    }
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin createToCheminsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEtape.TO_CHEMINS_KEY);
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin) eo;
  }

  public void deleteToCheminsRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtape.TO_CHEMINS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToCheminsRelationships() {
    Enumeration<org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin> objects = toChemins().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToCheminsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> toDemandes() {
    return (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>)storedValueForKey(_EOEtape.TO_DEMANDES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> toDemandes(EOQualifier qualifier) {
    return toDemandes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> toDemandes(EOQualifier qualifier, boolean fetch) {
    return toDemandes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> toDemandes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande.TO_ETAPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlworkflow.serveur.metier.EODemande.fetchDemandes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDemandes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDemandes(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande object) {
    includeObjectIntoPropertyWithKey(object, _EOEtape.TO_DEMANDES_KEY);
  }

  public void removeFromToDemandes(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande object) {
    excludeObjectFromPropertyWithKey(object, _EOEtape.TO_DEMANDES_KEY);
  }

  public void addToToDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande object) {
    if (_EOEtape.LOG.isDebugEnabled()) {
      _EOEtape.LOG.debug("adding " + object + " to toDemandes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToDemandes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEtape.TO_DEMANDES_KEY);
    }
  }

  public void removeFromToDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande object) {
    if (_EOEtape.LOG.isDebugEnabled()) {
      _EOEtape.LOG.debug("removing " + object + " from toDemandes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToDemandes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtape.TO_DEMANDES_KEY);
    }
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande createToDemandesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlworkflow.serveur.metier.EODemande.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEtape.TO_DEMANDES_KEY);
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande) eo;
  }

  public void deleteToDemandesRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtape.TO_DEMANDES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToDemandesRelationships() {
    Enumeration<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> objects = toDemandes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDemandesRelationship(objects.nextElement());
    }
  }


  public static EOEtape createEtape(EOEditingContext editingContext, String codeEtape
, String libelleEtape
, String temoinEtapeInitiale
, org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation toCircuitValidation) {
    EOEtape eo = (EOEtape) EOUtilities.createAndInsertInstance(editingContext, _EOEtape.ENTITY_NAME);    
		eo.setCodeEtape(codeEtape);
		eo.setLibelleEtape(libelleEtape);
		eo.setTemoinEtapeInitiale(temoinEtapeInitiale);
    eo.setToCircuitValidationRelationship(toCircuitValidation);
    return eo;
  }

  public static ERXFetchSpecification<EOEtape> fetchSpec() {
    return new ERXFetchSpecification<EOEtape>(_EOEtape.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEtape> fetchAllEtapes(EOEditingContext editingContext) {
    return _EOEtape.fetchAllEtapes(editingContext, null);
  }

  public static NSArray<EOEtape> fetchAllEtapes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEtape.fetchEtapes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEtape> fetchEtapes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEtape> fetchSpec = new ERXFetchSpecification<EOEtape>(_EOEtape.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEtape> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEtape fetchEtape(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtape.fetchEtape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtape fetchEtape(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEtape> eoObjects = _EOEtape.fetchEtapes(editingContext, qualifier, null);
    EOEtape eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Etape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtape fetchRequiredEtape(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtape.fetchRequiredEtape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtape fetchRequiredEtape(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEtape eoObject = _EOEtape.fetchEtape(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Etape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtape localInstanceIn(EOEditingContext editingContext, EOEtape eo) {
    EOEtape localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
