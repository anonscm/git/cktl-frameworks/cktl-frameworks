package org.cocktail.fwkcktlworkflow.serveur.metier;

import java.util.List;

public interface IDemande {

	List<? extends IHistoriqueDemande> toHistoriqueDemandes();

	/**
	 * @return L'étape en cours sur cette demande
	 */
	IEtape toEtape();
	
	/**
	 * Fait avancer la demande en empruntant le chemin correspondant au type de chemin passé en paramètre.
	 * 
	 * @param codeTypeChemin Le type de chemin à emprunter
	 * @param persIdModification L'identifiant de la personne qui fait avancer la demande
	 */
	void faireAvancerSurChemin(String codeTypeChemin, Integer persIdModification);
}
