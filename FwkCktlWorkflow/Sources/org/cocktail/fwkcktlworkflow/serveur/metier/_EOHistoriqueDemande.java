// DO NOT EDIT.  Make changes to EOHistoriqueDemande.java instead.
package org.cocktail.fwkcktlworkflow.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOHistoriqueDemande extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "HistoriqueDemande";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemande");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPE_ARRIVEE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtapeArrivee");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape> TO_ETAPE_DEPART = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape>("toEtapeDepart");

  // Attributes
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_DEMANDE_KEY = TO_DEMANDE.key();
  public static final String TO_ETAPE_ARRIVEE_KEY = TO_ETAPE_ARRIVEE.key();
  public static final String TO_ETAPE_DEPART_KEY = TO_ETAPE_DEPART.key();

  private static Logger LOG = Logger.getLogger(_EOHistoriqueDemande.class);

  public EOHistoriqueDemande localInstanceIn(EOEditingContext editingContext) {
    EOHistoriqueDemande localInstance = (EOHistoriqueDemande)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOHistoriqueDemande.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOHistoriqueDemande.LOG.isDebugEnabled()) {
    	_EOHistoriqueDemande.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOHistoriqueDemande.DATE_MODIFICATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOHistoriqueDemande.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOHistoriqueDemande.LOG.isDebugEnabled()) {
    	_EOHistoriqueDemande.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOHistoriqueDemande.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(_EOHistoriqueDemande.TO_DEMANDE_KEY);
  }
  
  public void setToDemande(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    takeStoredValueForKey(value, _EOHistoriqueDemande.TO_DEMANDE_KEY);
  }

  public void setToDemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (_EOHistoriqueDemande.LOG.isDebugEnabled()) {
      _EOHistoriqueDemande.LOG.debug("updating toDemande from " + toDemande() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDemande(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = toDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOHistoriqueDemande.TO_DEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOHistoriqueDemande.TO_DEMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeArrivee() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape)storedValueForKey(_EOHistoriqueDemande.TO_ETAPE_ARRIVEE_KEY);
  }
  
  public void setToEtapeArrivee(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    takeStoredValueForKey(value, _EOHistoriqueDemande.TO_ETAPE_ARRIVEE_KEY);
  }

  public void setToEtapeArriveeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    if (_EOHistoriqueDemande.LOG.isDebugEnabled()) {
      _EOHistoriqueDemande.LOG.debug("updating toEtapeArrivee from " + toEtapeArrivee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtapeArrivee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape oldValue = toEtapeArrivee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOHistoriqueDemande.TO_ETAPE_ARRIVEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOHistoriqueDemande.TO_ETAPE_ARRIVEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeDepart() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape)storedValueForKey(_EOHistoriqueDemande.TO_ETAPE_DEPART_KEY);
  }
  
  public void setToEtapeDepart(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    takeStoredValueForKey(value, _EOHistoriqueDemande.TO_ETAPE_DEPART_KEY);
  }

  public void setToEtapeDepartRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape value) {
    if (_EOHistoriqueDemande.LOG.isDebugEnabled()) {
      _EOHistoriqueDemande.LOG.debug("updating toEtapeDepart from " + toEtapeDepart() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtapeDepart(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape oldValue = toEtapeDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOHistoriqueDemande.TO_ETAPE_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOHistoriqueDemande.TO_ETAPE_DEPART_KEY);
    }
  }
  

  public static EOHistoriqueDemande createHistoriqueDemande(EOEditingContext editingContext, NSTimestamp dateModification
, org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande, org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape toEtapeArrivee) {
    EOHistoriqueDemande eo = (EOHistoriqueDemande) EOUtilities.createAndInsertInstance(editingContext, _EOHistoriqueDemande.ENTITY_NAME);    
		eo.setDateModification(dateModification);
    eo.setToDemandeRelationship(toDemande);
    eo.setToEtapeArriveeRelationship(toEtapeArrivee);
    return eo;
  }

  public static ERXFetchSpecification<EOHistoriqueDemande> fetchSpec() {
    return new ERXFetchSpecification<EOHistoriqueDemande>(_EOHistoriqueDemande.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOHistoriqueDemande> fetchAllHistoriqueDemandes(EOEditingContext editingContext) {
    return _EOHistoriqueDemande.fetchAllHistoriqueDemandes(editingContext, null);
  }

  public static NSArray<EOHistoriqueDemande> fetchAllHistoriqueDemandes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOHistoriqueDemande.fetchHistoriqueDemandes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOHistoriqueDemande> fetchHistoriqueDemandes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOHistoriqueDemande> fetchSpec = new ERXFetchSpecification<EOHistoriqueDemande>(_EOHistoriqueDemande.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOHistoriqueDemande> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOHistoriqueDemande fetchHistoriqueDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHistoriqueDemande.fetchHistoriqueDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHistoriqueDemande fetchHistoriqueDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOHistoriqueDemande> eoObjects = _EOHistoriqueDemande.fetchHistoriqueDemandes(editingContext, qualifier, null);
    EOHistoriqueDemande eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one HistoriqueDemande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHistoriqueDemande fetchRequiredHistoriqueDemande(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHistoriqueDemande.fetchRequiredHistoriqueDemande(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHistoriqueDemande fetchRequiredHistoriqueDemande(EOEditingContext editingContext, EOQualifier qualifier) {
    EOHistoriqueDemande eoObject = _EOHistoriqueDemande.fetchHistoriqueDemande(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no HistoriqueDemande that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHistoriqueDemande localInstanceIn(EOEditingContext editingContext, EOHistoriqueDemande eo) {
    EOHistoriqueDemande localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
