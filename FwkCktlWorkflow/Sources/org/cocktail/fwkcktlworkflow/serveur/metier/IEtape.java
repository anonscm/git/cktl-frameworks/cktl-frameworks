package org.cocktail.fwkcktlworkflow.serveur.metier;

/**
 * Définit le contrat d'une étape
 */
public interface IEtape {

	/**
	 * @return code de l'étape
	 */
	String codeEtape();
	
	/**
	 * @return le circuit de validation
	 */
	EOCircuitValidation toCircuitValidation();	
	
	/**
	 * 
	 * @return le libelle de l'étape
	 */
	String libelleEtape();
}
