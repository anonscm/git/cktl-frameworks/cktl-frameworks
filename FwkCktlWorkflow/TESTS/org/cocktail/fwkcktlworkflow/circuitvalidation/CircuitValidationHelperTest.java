package org.cocktail.fwkcktlworkflow.circuitvalidation;

import org.cocktail.fwkcktlworkflow.validation.BaseCircuitValidationTest;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationHelper;
import org.junit.Assert;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class CircuitValidationHelperTest extends BaseCircuitValidationTest {
	
	@Test
	public void testReutilisationDuHelper() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		
		// Circuit simpliste, 2 étapes toutes utilisées
		EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_SIMPLISTE");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);		
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(2, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(1, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(2, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());

		// Circuit normal, 4 étapes toutes utilisées
		// Réutilisation du "helper"
		circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_NORMAL");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());
	}
	
	@Test
	public void testControlerEtNormaliserCircuitSimpliste() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		
		// Circuit simpliste, 2 étapes toutes utilisées
		EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_SIMPLISTE");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);		
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(2, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(1, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(2, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());
	}

	@Test
	public void testControlerEtNormaliserCircuitNormal() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		
		// Circuit normal, 4 étapes toutes utilisées
		// Réutilisation du "helper"
		EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_NORMAL");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());

		// Version 1 du circuit normal, 4 étapes, une non utilisées
		// Réutilisation du "helper"
		NSArray<EOCircuitValidation> listeCircuitValidation = EOCircuitValidation.rechercherCircuitsValidation(ec, "PECHE", "CIRCUIT_NORMAL");
		Assert.assertNotNull(listeCircuitValidation);
		circuitValidationHelper.controlerEtNormaliser(listeCircuitValidation.get(0));
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(1, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals("ETAPE1", circuitValidationHelper.getListeEtapesNonUtilisees().get(0).codeEtape());
		Assert.assertNull(circuitValidationHelper.getListeEtapesNonUtilisees().get(0).chemin("VALIDER").toEtapeArrivee()); // Normalisation
		Assert.assertEquals(3, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(3, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(3, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());
	}

	@Test
	public void testControlerEtNormaliserCircuitComplexeAvecBoucleInfinie() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		
		// 6 étapes toutes utilisées
		EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_COMPLEXE_BOUCLE_INFINIE");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(6, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(3, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(4, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());
	}

	@Test
	public void testControlerEtNormaliserCircuitComplexeSansBoucleInfinie() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		
		// 6 étapes toutes utilisées
		EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_COMPLEXE");
		Assert.assertNotNull(circuitValidation);
		circuitValidationHelper.controlerEtNormaliser(circuitValidation);
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesNonUtilisees());
		Assert.assertNotNull(circuitValidationHelper.getListeEtapesUtilisees());
		Assert.assertEquals(0, circuitValidationHelper.getListeEtapesNonUtilisees().size());
		Assert.assertEquals(6, circuitValidationHelper.getListeEtapesUtilisees().size());
		Assert.assertEquals(0, circuitValidationHelper.getListeCheminNonValides().size());
		Assert.assertEquals(8, circuitValidationHelper.getListeCheminValides().size());
		Assert.assertEquals(6, circuitValidationHelper.getListeEtapeAyantAuMoinsUnCheminValide().size());
	}
}
