package org.cocktail.fwkcktlworkflow.validation;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import com.wounit.rules.MockEditingContext;

public class BaseCircuitValidationTest {

	protected static final int PERS_ID_CREATION = 42;
	protected static final int PERS_ID_MODIFICATION = 7;

	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlWorkflow");

	@Before
	public void setUp() throws Exception {
		/* Application */
		EOGdApplication applicationTestee = new EOGdApplication();
		applicationTestee.setAppStrId("PECHE");
		applicationTestee.setAppLc("Peche");
		ec.insertSavedObject(applicationTestee);
		
		EOGdApplication applicationFantome = new EOGdApplication();
		applicationFantome.setAppStrId("FANTOME");
		applicationFantome.setAppLc("Appli fantôme");
		ec.insertSavedObject(applicationFantome);
		
		{
			
			/* Circuit */
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setToGdApplication(applicationTestee);
			circuitValidation.setCodeCircuitValidation("DEFAUT");
			circuitValidation.setTemoinUtilisable("O");
			ec.insertSavedObject(circuitValidation);
	
			/* Etapes */
			// circuit circuitValidation "DEFAULT"
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
	
			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE_2");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);
	
			EOEtape etape3 = new EOEtape();
			etape3.setCodeEtape("ETAPE_3");
			ec.insertSavedObject(etape3);
			etape3.setToCircuitValidationRelationship(circuitValidation);
	
			EOEtape etape4 = new EOEtape();
			etape4.setCodeEtape("ETAPE_4");
			ec.insertSavedObject(etape4);
			etape4.setToCircuitValidationRelationship(circuitValidation);
	
			/* Chemins */
			// circuit circuitValidation "DEFAULT"
			EOChemin chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeArriveeRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape2);
	
			chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape3);
	
			chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape3);
			chemin.setToEtapeArriveeRelationship(etape4);
	
			chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape4);
		}
		
		/*
		 * Circuit circuitValidation2 "AUTRE"
		 */
		{
			EOCircuitValidation circuitValidation2 = new EOCircuitValidation();
			circuitValidation2.setToGdApplication(applicationTestee);
			circuitValidation2.setCodeCircuitValidation("AUTRE");
			circuitValidation2.setTemoinUtilisable("O");
			ec.insertSavedObject(circuitValidation2);
	
			// Etapes
			EOEtape etapeInitialeAutre = new EOEtape();
			etapeInitialeAutre.setCodeEtape("INITIALE_AUTRE");
			etapeInitialeAutre.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitialeAutre);
			etapeInitialeAutre.setToCircuitValidationRelationship(circuitValidation2);
	
			EOEtape etape = new EOEtape();
			etape.setCodeEtape("ETAPE_AUTRE_2");
			ec.insertSavedObject(etape);
			etape.setToCircuitValidationRelationship(circuitValidation2);
	
			// Chemin
			EOChemin chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitialeAutre);
			chemin.setToEtapeArriveeRelationship(etape);
		}
		
		/* ========= Données pour les tests 2ème génération */
		{
			// Circuit
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setToGdApplication(applicationTestee);
			circuitValidation.setCodeCircuitValidation("NEXT_GEN");
			circuitValidation.setTemoinUtilisable("O");
			ec.insertSavedObject(circuitValidation);
	
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("etapeInitJUnitNextGen");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
	
			EOEtape etapeJUnit1 = new EOEtape();
			etapeJUnit1.setCodeEtape("etapeJUnit1");
			ec.insertSavedObject(etapeJUnit1);
			etapeJUnit1.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etapeJUnit2 = new EOEtape();
			etapeJUnit2.setCodeEtape("etapeJUnit2");
			ec.insertSavedObject(etapeJUnit2);
			etapeJUnit2.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etapeJUnit3 = new EOEtape();
			etapeJUnit3.setCodeEtape("etapeJUnit3");
			ec.insertSavedObject(etapeJUnit3);
			etapeJUnit3.setToCircuitValidationRelationship(circuitValidation);
	
			EOChemin chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etapeJUnit1);
			chemin.setCodeTypeChemin("typeJUnit1");
			
			chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeJUnit1);
			chemin.setToEtapeArriveeRelationship(etapeJUnit2);
			chemin.setCodeTypeChemin("typeJUnit2");
			
			chemin = new EOChemin();
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeJUnit1);
			chemin.setToEtapeArriveeRelationship(etapeJUnit3);
			chemin.setCodeTypeChemin("typeJUnit1");
		}
		
		{
			// Circuit "simpliste"
			// °°°°°°°°°°°°°°°°°°°
			// Version 1
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_SIMPLISTE");
			circuitValidation.setLibelleLongCircuitValidation("Circuit simpliste");
			circuitValidation.setNumeroVersion(1);
			circuitValidation.setTemoinUtilisable("O");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etapeFinale);
		}

		{
			// Circuit "simpliste" (sur l'application fantôme => non visible dans les tests)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			// Version 1
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_SIMPLISTE");
			circuitValidation.setLibelleLongCircuitValidation("Circuit simpliste");
			circuitValidation.setNumeroVersion(1);
			circuitValidation.setTemoinUtilisable("O");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationFantome);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etapeFinale);
		}
		
		{
			// Circuit "normal" (avec boucle)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			// Version 1 (non utilisable et etape1 non utilisée)
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_NORMAL");
			circuitValidation.setLibelleLongCircuitValidation("Circuit normal");
			circuitValidation.setNumeroVersion(1);
			circuitValidation.setTemoinUtilisable("N");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			// Etape1 présente mais non utilisée dans cette version du circuit
			EOEtape etape1 = new EOEtape();
			etape1.setCodeEtape("ETAPE1");
			etape1.setLibelleEtape("Etape 1");
			etape1.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape1);
			etape1.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE2");
			etape2.setLibelleEtape("Etape 2");
			etape2.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape2);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape2); // Devra être null après le "clean" du circuit

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeFinale);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeInitiale);
		}

		{
			// Circuit "normal" (avec boucle)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			// Version 2 (version utilisable)
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_NORMAL");
			circuitValidation.setLibelleLongCircuitValidation("Circuit normal");
			circuitValidation.setNumeroVersion(2);
			circuitValidation.setTemoinUtilisable("O");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape1 = new EOEtape();
			etape1.setCodeEtape("ETAPE1");
			etape1.setLibelleEtape("Etape 1");
			etape1.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape1);
			etape1.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE2");
			etape2.setLibelleEtape("Etape 2");
			etape2.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape2);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeFinale);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape1);
		}
		
		{
			// Circuit "normal" (avec boucle)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			// Version 3 (pas encore utilisable)
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_NORMAL");
			circuitValidation.setLibelleLongCircuitValidation("Circuit normal");
			circuitValidation.setNumeroVersion(3);
			circuitValidation.setTemoinUtilisable("N");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape1 = new EOEtape();
			etape1.setCodeEtape("ETAPE1");
			etape1.setLibelleEtape("Etape 1");
			etape1.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape1);
			etape1.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE2");
			etape2.setLibelleEtape("Etape 2");
			etape2.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape2);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeArriveeRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeFinale);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape1);
		}

		{
			//      |
			// +----------+
			// | INITIALE |
			// +----------+
			//      |    +--------+
			//      +--->| ETAPE1 |<---+
			//           +--------+    |
			//                |    +--------+
			//                +--->| ETAPE2 |
			//                |    +--------+
			//                |        |    +--------+
			//                |        +--->| FINALE |--
			//                |             +--------+
			//                |    +--------+
			//                +--->| ETAPE 3|<--+
			//                     +--------+   |
			//                         |    +-------+
			//                         +--->|ETAPE 4|
			//                              +-------+
			
			// Circuit "complexe" (avec boucle infinie)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_COMPLEXE_BOUCLE_INFINIE");
			circuitValidation.setLibelleLongCircuitValidation("Circuit avec boucle infinie");
			circuitValidation.setNumeroVersion(1);
			circuitValidation.setTemoinUtilisable("O");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape1 = new EOEtape();
			etape1.setCodeEtape("ETAPE1");
			etape1.setLibelleEtape("Etape 1");
			etape1.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape1);
			etape1.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE2");
			etape2.setLibelleEtape("Etape 2");
			etape2.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape3 = new EOEtape();
			etape3.setCodeEtape("ETAPE3");
			etape3.setLibelleEtape("Etape 3");
			etape3.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape3);
			etape3.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape4 = new EOEtape();
			etape4.setCodeEtape("ETAPE4");
			etape4.setLibelleEtape("Etape 4");
			etape4.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape4);
			etape4.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape2);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape3);
			
			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeFinale);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape3);
			chemin.setToEtapeArriveeRelationship(etape4);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape4);
			chemin.setToEtapeArriveeRelationship(etape3);
		}
	
		{
			//      |
			// +----------+
			// | INITIALE |
			// +----------+
			//      |    +--------+
			//      +--->| ETAPE1 |<---+
			//           +--------+    |
			//              ^ |    +--------+
			//              | +--->| ETAPE2 |
			//              | |    +--------+
			//              | |        |    +--------+
			//              | |        +--->| FINALE |--
			//              | |             +--------+
			//              | |    +--------+
			//              | +--->| ETAPE 3|<--+
			//              |      +--------+   |
			//              |          |    +-------+
			//              |          +--->|ETAPE 4|
			//              |          |    +-------+
			//              +----------+

			// Circuit "complexe" (sans boucle infinie)
			// Idem circuit précédent avec un chemin de plus
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			EOCircuitValidation circuitValidation = new EOCircuitValidation();
			circuitValidation.setCodeCircuitValidation("CIRCUIT_COMPLEXE");
			circuitValidation.setLibelleLongCircuitValidation("Circuit complexe sans boucle infinie");
			circuitValidation.setNumeroVersion(1);
			circuitValidation.setTemoinUtilisable("O");
			circuitValidation.setPersIdModification(PERS_ID_CREATION);
			ec.insertSavedObject(circuitValidation);
			circuitValidation.setToGdApplicationRelationship(applicationTestee);
			
			EOEtape etapeInitiale = new EOEtape();
			etapeInitiale.setCodeEtape("INITIALE");
			etapeInitiale.setLibelleEtape("Etape initiale");
			etapeInitiale.setTemoinEtapeInitiale("O");
			ec.insertSavedObject(etapeInitiale);
			etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape1 = new EOEtape();
			etape1.setCodeEtape("ETAPE1");
			etape1.setLibelleEtape("Etape 1");
			etape1.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape1);
			etape1.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape2 = new EOEtape();
			etape2.setCodeEtape("ETAPE2");
			etape2.setLibelleEtape("Etape 2");
			etape2.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape2);
			etape2.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etape3 = new EOEtape();
			etape3.setCodeEtape("ETAPE3");
			etape3.setLibelleEtape("Etape 3");
			etape3.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape3);
			etape3.setToCircuitValidationRelationship(circuitValidation);
			
			EOEtape etape4 = new EOEtape();
			etape4.setCodeEtape("ETAPE4");
			etape4.setLibelleEtape("Etape 4");
			etape4.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etape4);
			etape4.setToCircuitValidationRelationship(circuitValidation);

			EOEtape etapeFinale = new EOEtape();
			etapeFinale.setCodeEtape("FINALE");
			etapeFinale.setLibelleEtape("Etape finale");
			etapeFinale.setTemoinEtapeInitiale("N");
			ec.insertSavedObject(etapeFinale);
			etapeFinale.setToCircuitValidationRelationship(circuitValidation);
			
			EOChemin chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etapeInitiale);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape2);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape1);
			chemin.setToEtapeArriveeRelationship(etape3);
			
			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etapeFinale);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape2);
			chemin.setToEtapeArriveeRelationship(etape1);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape3);
			chemin.setToEtapeArriveeRelationship(etape4);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("VALIDER");
			chemin.setLibelleCourtTypeChemin("Valider");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape4);
			chemin.setToEtapeArriveeRelationship(etape3);

			chemin = new EOChemin();
			chemin.setCodeTypeChemin("REFUSER");
			chemin.setLibelleCourtTypeChemin("Refuser");
			ec.insertSavedObject(chemin);
			chemin.setToEtapeDepartRelationship(etape3);
			chemin.setToEtapeArriveeRelationship(etape1);
		}
	}

	@After
	public void tearDown() throws Exception {
	}
}
