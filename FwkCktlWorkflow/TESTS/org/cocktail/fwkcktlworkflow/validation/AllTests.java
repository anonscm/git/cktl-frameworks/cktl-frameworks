package org.cocktail.fwkcktlworkflow.validation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	DemandeTest.class,
	HistoriqueTest.class })
public class AllTests {

}
