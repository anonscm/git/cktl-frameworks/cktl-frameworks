package org.cocktail.fwkcktlworkflow.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationTechniqueException;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class DemandeTest extends BaseCircuitValidationTest {

	/*
	@Test
	public void testRoute1Ok() {
		
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);

		//EOCircuitValidation circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "DEFAUT");
		
		assertEquals("INITIALE", demande.toEtape().codeEtape());

		// Test de l'historisation sur une création d'une demande.
		EOQualifier qualifier = EOHistoriqueDemande.TO_DEMANDE.eq(demande);
		EOHistoriqueDemande historique = EOHistoriqueDemande
				.fetchHistoriqueDemande(ec, qualifier);
		assertNotNull(historique);
		assertNotNull(historique.toDemande());
		assertNotNull(historique.toEtapeArrivee());
		assertNotNull(historique.persIdModification());
		assertEquals((long) PERS_ID_CREATION,
				(long) historique.persIdModification());
		assertEquals("INITIALE", historique.toEtapeArrivee().codeEtape());

		// Test de historique aprés un changement d'étape vers l'étape suivante.
		demande.faireAvancer(PERS_ID_MODIFICATION);
		qualifier = EOHistoriqueDemande.TO_DEMANDE.eq(demande).and(
				EOHistoriqueDemande.TO_ETAPE_ARRIVEE.eq(demande.toEtape()));
		historique = EOHistoriqueDemande.fetchHistoriqueDemande(ec, qualifier);
		assertNotNull(historique);
		assertEquals("ETAPE_2", historique.toEtapeArrivee().codeEtape());

		// Test de historique aprés un changement d'étape vers une étape
		// spécifique.
		demande.faireAvancerVers("ETAPE_3", PERS_ID_MODIFICATION);
		qualifier = EOHistoriqueDemande.TO_DEMANDE.eq(demande).and(
				EOHistoriqueDemande.TO_ETAPE_ARRIVEE.eq(demande.toEtape()));
		historique = EOHistoriqueDemande.fetchHistoriqueDemande(ec, qualifier);
		assertEquals("ETAPE_3", historique.toEtapeArrivee().codeEtape());

		// On vérifie que la demande est bien sur l'étape attendue.
		assertEquals("ETAPE_3", demande.toEtape().codeEtape());
		demande.faireAvancer(PERS_ID_MODIFICATION);
		assertEquals("ETAPE_4", demande.toEtape().codeEtape());
	}*/

	/*
	@Test
	public void testRoute2Ok() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);
		demande.faireAvancer(PERS_ID_MODIFICATION);
		demande.faireAvancerVers("ETAPE_4", PERS_ID_MODIFICATION);
		assertEquals("ETAPE_4", demande.toEtape().codeEtape());
	}*/

	/*
	 @Test
	 
	public void testBrancheNonSpecifiee() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);
		boolean caught = false;
		try {
			demande.faireAvancer(PERS_ID_MODIFICATION);
		} catch (CircuitValidationTechniqueException e) {
			caught = true;
		}
		assertTrue("Une exception aurait du être levée", caught);
	}
*/
	/*
	@Test
	public void testBrancheInexistante() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);
		boolean caught = false;
		try {
			demande.faireAvancerVers("INCONNUE", PERS_ID_MODIFICATION);
		} catch (CircuitValidationTechniqueException e) {
			caught = true;
		}
		assertTrue("Une exception aurait du être levée", caught);
	}*/

	/*
	@Test
	public void testFinCircuit() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);
		
		demande.faireAvancer(PERS_ID_MODIFICATION);
		demande.faireAvancerVers("ETAPE_4", PERS_ID_MODIFICATION);
		
		boolean caught = false;
		try {
			demande.faireAvancer(PERS_ID_MODIFICATION);
		} catch (CircuitValidationTechniqueException e) {
			caught = true;
		}
		assertTrue("Une exception aurait du être levée", caught);
	}*/

	/*
	@Test
	public void testChangerCircuit() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE",
				"DEFAUT", PERS_ID_CREATION);
		demande.faireAvancer(PERS_ID_CREATION);
		demande.faireAvancerVers("ETAPE_3", PERS_ID_CREATION);
		demande.changerCircuit("PECHE", "AUTRE", PERS_ID_CREATION);

		// On vérifie que l'on a bien changé de circuit &&
		// que l'on est sur l'étape initiale du nouveau circuit.
		EOEtape etape = demande.toEtape();
		assertNotNull(etape);
		assertEquals("INITIALE_AUTRE", etape.codeEtape());
		assertEquals("AUTRE", etape.toCircuitValidation().codeCircuitValidation());

		// Test de historique aprés un changement de circuit
		EOQualifier qualifier = EOHistoriqueDemande.TO_DEMANDE.eq(demande).and(
				EOHistoriqueDemande.TO_ETAPE_ARRIVEE.eq(demande.toEtape()));
		EOHistoriqueDemande historique = EOHistoriqueDemande
				.fetchHistoriqueDemande(ec, qualifier);
		assertEquals("INITIALE_AUTRE", historique.toEtapeArrivee().codeEtape());
	}*/
	
	@Test
	public void testAvancerSurCheminExistant() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE", "NEXT_GEN", PERS_ID_CREATION);
		demande.faireAvancerSurChemin("typeJUnit1", PERS_ID_MODIFICATION);
		EOEtape etape = demande.toEtape();
		assertNotNull(etape);
		assertEquals("etapeJUnit1", etape.codeEtape());
	}
	
	@Test
	public void testAvancerSurMultiplesCheminExistant() {
		EODemande demande = EODemande.creerDemande(ec, "PECHE", "NEXT_GEN", PERS_ID_CREATION);
		demande.faireAvancerSurChemin("typeJUnit1", PERS_ID_MODIFICATION);
		demande.faireAvancerSurChemin("typeJUnit1", PERS_ID_MODIFICATION);
		EOEtape etape = demande.toEtape();
		assertNotNull(etape);
		assertEquals("etapeJUnit3", etape.codeEtape());

		demande = EODemande.creerDemande(ec, "PECHE", "NEXT_GEN", PERS_ID_CREATION);
		demande.faireAvancerSurChemin("typeJUnit1", PERS_ID_MODIFICATION);
		demande.faireAvancerSurChemin("typeJUnit2", PERS_ID_MODIFICATION);
		etape = demande.toEtape();
		assertNotNull(etape);
		assertEquals("etapeJUnit2", etape.codeEtape());
	}
}
