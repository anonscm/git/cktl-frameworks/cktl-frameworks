package org.cocktail.fwkcktlworkflow.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.junit.Test;

public class EtapeTest extends BaseCircuitValidationTest {

	/**
	 * Test de la méthode {@link EOEtape#isInitiale()}.
	 */
	@Test
	public void testIsInitiale() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		assertNotNull(circuit);

		EOEtape etape = circuit.etapeInitiale();
		assertNotNull(etape);
		assertTrue(etape.isInitiale());
	}
	
	/**
	 * Test de la méthode {@link EOEtape#isFinale()}.
	 */
	@Test
	public void testIsFinale() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		assertNotNull(circuit);
		
		EOEtape etape = circuit.etapeInitiale();
		assertNotNull(etape);
		assertTrue(etape.isInitiale());
		assertFalse(etape.isFinale());
		
		EOChemin chemin = etape.cheminEmpruntable("typeJUnit1");
		assertNotNull(chemin);
		
		etape = chemin.toEtapeArrivee();
		assertFalse(etape.isInitiale());
		assertFalse(etape.isFinale());
		
		chemin = etape.cheminEmpruntable("typeJUnit2");
		assertNotNull(chemin);
		
		etape = chemin.toEtapeArrivee();
		assertFalse(etape.isInitiale());
		assertTrue(etape.isFinale());
	}

	/**
	 * Test de la méthode {@link EOEtape#chemin(String)}.
	 */
	@Test
	public void testChemin() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		EOEtape etape = circuit.etapeInitiale();
		EOChemin chemin = etape.chemin("typeJUnit1");
		
		assertNotNull(circuit);
		assertNotNull(etape);
		assertNotNull(chemin);
		
		chemin = etape.chemin("typeJUnit2");
		assertNull(chemin);
	}
	
	/**
	 * Test de la méthode {@link EOEtape#cheminEmpruntable(String)}.
	 */
	@Test
	public void testCheminEmpruntable() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		EOEtape etape = circuit.etapeInitiale();
		EOChemin chemin = etape.cheminEmpruntable("typeJUnit1");
		
		assertNotNull(circuit);
		assertNotNull(etape);
		assertNotNull(chemin);
		
		chemin = etape.cheminEmpruntable("typeJUnit2");
		assertNull(chemin);
	}
	
	/**
	 * Test de la méthode {@link EOEtape#hasChemin(String)}.
	 */
	@Test
	public void testHasChemin() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		EOEtape etape = circuit.etapeInitiale();
		boolean chemin = etape.hasChemin("typeJUnit1");
		
		assertNotNull(circuit);
		assertNotNull(etape);
		assertTrue(chemin);
		
		chemin = etape.hasChemin("typeJUnit2");
		assertFalse(chemin);
	}
	
	/**
	 * Test de la méthode {@link EOEtape#isCheminEmpruntable(String)}.
	 */
	@Test
	public void testIsCheminEmpruntable() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "NEXT_GEN");
		EOEtape etape = circuit.etapeInitiale();
		boolean chemin = etape.isCheminEmpruntable("typeJUnit1");
		
		assertNotNull(circuit);
		assertNotNull(etape);
		assertTrue(chemin);
		
		chemin = etape.isCheminEmpruntable("typeJUnit2");
		assertFalse(chemin);
	}
}
