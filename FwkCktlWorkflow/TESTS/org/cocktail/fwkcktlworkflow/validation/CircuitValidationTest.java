package org.cocktail.fwkcktlworkflow.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class CircuitValidationTest extends BaseCircuitValidationTest {

	/**
	 * Test de la méthode {@link EOCircuitValidation#etapeInitiale()}.
	 */
	@Test
	public void testEtapeInitiale() {
		EOCircuitValidation circuit = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "DEFAUT");
		EOEtape etapeInitiale = circuit.etapeInitiale();
		
		assertEquals("DEFAUT", circuit.codeCircuitValidation());
		assertEquals("INITIALE", etapeInitiale.codeEtape());
		assertEquals(true, etapeInitiale.isInitiale());
	}
	
	/**
	 * Test de la méthode {@link EOCircuitValidation#rechercherCircuitsValidation(com.webobjects.eocontrol.EOEditingContext, String)}.
	 */
	@Test
	public void testRechercherCircuitsValidation() {
		NSArray<EOCircuitValidation> circuitValidations = EOCircuitValidation.rechercherCircuitsValidation(ec, "PECHE");
		
		assertNotNull(circuitValidations);
		assertEquals(4, circuitValidations.size());
		
		// Tous les circuits dans l'application "PECHE" et pas de doublon sur les codes
		Set<String> codesCircuits = new HashSet<String>();
		
		for (EOCircuitValidation unCircuit : circuitValidations) {
			assertEquals("PECHE", unCircuit.toGdApplication().appStrId());
			assertFalse(codesCircuits.contains(unCircuit.codeCircuitValidation()));
			codesCircuits.add(unCircuit.codeCircuitValidation());
		}
	}

	/**
	 * Test de la méthode {@link EOCircuitValidation#rechercherCircuitsValidation(com.webobjects.eocontrol.EOEditingContext, String, String)}.
	 */
	@Test
	public void testRechercherCircuitsValidation2() {
		NSArray<EOCircuitValidation> circuitValidations = EOCircuitValidation.rechercherCircuitsValidation(ec, "PECHE", "CIRCUIT_SIMPLISTE");
		
		assertNotNull(circuitValidations);
		assertEquals(1, circuitValidations.size());
		EOCircuitValidation circuit = circuitValidations.get(0);
		assertEquals(1, circuit.numeroVersion().intValue());
		assertEquals("PECHE", circuit.toGdApplication().appStrId());
		assertEquals("CIRCUIT_SIMPLISTE", circuit.codeCircuitValidation());

		circuitValidations = EOCircuitValidation.rechercherCircuitsValidation(ec, "PECHE", "CIRCUIT_NORMAL");
		
		assertNotNull(circuitValidations);
		assertEquals(3, circuitValidations.size());
		circuit = circuitValidations.get(0);
		assertEquals(1, circuit.numeroVersion().intValue());
		assertEquals("PECHE", circuit.toGdApplication().appStrId());
		assertEquals("CIRCUIT_NORMAL", circuit.codeCircuitValidation());
		circuit = circuitValidations.get(1);
		assertEquals(2, circuit.numeroVersion().intValue());
		assertEquals("PECHE", circuit.toGdApplication().appStrId());
		assertEquals("CIRCUIT_NORMAL", circuit.codeCircuitValidation());
		circuit = circuitValidations.get(2);
		assertEquals(3, circuit.numeroVersion().intValue());
		assertEquals("PECHE", circuit.toGdApplication().appStrId());
		assertEquals("CIRCUIT_NORMAL", circuit.codeCircuitValidation());
	}
	
	/**
	 * Test de la méthode {@link EOCircuitValidation#rechercherCircuitValidationUtilisable(com.webobjects.eocontrol.EOEditingContext, String, String)}.
	 */
	@Test
	public void testRechercherCircuitValidationUtilisable() {
		EOCircuitValidation circuitValidationSimple = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_SIMPLISTE");
		
		assertNotNull(circuitValidationSimple);
		assertEquals("PECHE", circuitValidationSimple.toGdApplication().appStrId());
		assertEquals("CIRCUIT_SIMPLISTE", circuitValidationSimple.codeCircuitValidation());
		assertEquals(1, circuitValidationSimple.numeroVersion().intValue());
		assertTrue(circuitValidationSimple.isUtilisable());

		EOCircuitValidation circuitValidationNormal = EOCircuitValidation.rechercherCircuitValidationUtilisable(ec, "PECHE", "CIRCUIT_NORMAL");
		
		assertNotNull(circuitValidationNormal);
		assertEquals("PECHE", circuitValidationNormal.toGdApplication().appStrId());
		assertEquals("CIRCUIT_NORMAL", circuitValidationNormal.codeCircuitValidation());
		assertEquals(2, circuitValidationNormal.numeroVersion().intValue());
		assertTrue(circuitValidationSimple.isUtilisable());
	}
}
