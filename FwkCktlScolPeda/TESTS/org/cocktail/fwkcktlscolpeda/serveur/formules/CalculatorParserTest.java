package org.cocktail.fwkcktlscolpeda.serveur.formules;

import static org.fest.assertions.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.formules.services.CalculFormuleServiceParboiled;
import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurMoyenneException;
import org.junit.Test;
import org.parboiled.errors.ParsingException;

public class CalculatorParserTest {

	@Test
	public void test_CalculatorParserDivision() throws ParsingException, CalculateurMoyenneException {
		String input = "10/3";	
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("3.33"));
	}
	
	@Test(expected=CalculateurMoyenneException.class)
	public void test_CalculatorParserDivisionByZero() throws ParsingException, CalculateurMoyenneException {
		String input = "10/0";	
		calculate(input);
	}
	
	@Test
	public void test_CalculatorParserAdditionDecimaux() throws ParsingException, CalculateurMoyenneException {
		String input = "0.1 + 0.2";	
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0.3"));
	}
	
	@Test
	public void test_CalculatorParser() throws ParsingException, CalculateurMoyenneException {
		String input = "((1+2)/3+4)*6";	
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("30.0"));
	}

	@Test
	public void test_CalculatorParserMin() throws ParsingException, CalculateurMoyenneException {
		String input = "MIN((3.45+1)*2,52.1)";
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("8.9"));
	}

	@Test
	public void test_CalculatorParserMax() throws ParsingException, CalculateurMoyenneException {
		String input = "MAX(52.1,(3.45+1)*2)";
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("52.1"));
	}

	@Test
	public void test_CalculatorParserFacteur() throws ParsingException, CalculateurMoyenneException {
		String input = "2^3^5";
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("32768"));
	}

	@Test
	public void test_CalculatorSquareRoot() throws ParsingException, CalculateurMoyenneException {
		String input = "SQRT(16+9)";
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("5"));
	}

	@Test
	public void test_CalculatorSuperior() throws ParsingException, CalculateurMoyenneException {
		String input = "5 > 10";
		BigDecimal resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));

		input = "10 > 5";
		resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));

		input = "10 > 10";
		resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));

		input = "2+3*5 > MAX(5/5, 3)";
		resultat = calculate(input);

		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
	}

	@Test
	public void test_CalculatorInferior() throws ParsingException, CalculateurMoyenneException {
		String input = "5 < 10";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));

		input = "10 < 5";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "10 < 10";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "2+3*5 < MAX(5/5, 3)";
		resultat = calculate(input);
	
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
	}
	@Test
	public void test_CalculatorInferiorOrEqual() throws ParsingException, CalculateurMoyenneException {
		String input = "5 I 10";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "10 I 5";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "10 I 10";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "2+3*5 I MAX(5/5, 3)";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
	}

	@Test
	public void test_CalculatorSuperiorOrEqual() throws ParsingException, CalculateurMoyenneException {
		String input = "10.5 S 10";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "10 S 10";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "9.9 S 10";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
	}

	@Test
	public void test_CalculatorAnd() throws ParsingException, CalculateurMoyenneException {
		String input = "1 & 0 ";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "2 > 3  & 4 S 3 ";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "(2 < 3) & 4 S 3 ";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));	
	}

	@Test
	public void test_CalculatorOr() throws ParsingException, CalculateurMoyenneException {
		String input = "1 | 0 ";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "2 > 3  | 4 S 3 ";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "(2 > 3) | 4 I 3 ";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
	}

	@Test
	public void test_CalculatorEqual() throws ParsingException, CalculateurMoyenneException {
		String input = "1.36 = 1.32 + 0.04 ";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
		
		input = "2.73 = 2.47";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
	}

	@Test
	public void test_CalculatorNotEqual() throws ParsingException, CalculateurMoyenneException {
		String input = "1.36 ! 1.32 + 0.04 ";
		BigDecimal resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("0"));
		
		input = "2.73 ! 2.47";
		resultat = calculate(input);
		
		assertThat(resultat).isEqualByComparingTo(new BigDecimal("1"));
	}

	private BigDecimal calculate(String input) throws ParsingException, CalculateurMoyenneException {
		CalculFormuleServiceParboiled calculService = new CalculFormuleServiceParboiled();
		return calculService.calculer(input);
	}
}
