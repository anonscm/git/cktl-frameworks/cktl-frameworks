package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class EOUETest extends EOScolTestCase {
	private static final String expectedCode = "codeUE";
	private static final String expectedLibelle = "libelleUEs";

	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	@Test
	public void testEnregistrer() {
		try {
			// Arrange
			EOUE ue = EOComposantFactory.createComposant(EOUE.class, null, editingContext, 2014, null);
			ue.setValidateur(new CodeValidateurSaisieObligatoire());
			ue.setCode(expectedCode);
			ue.setLibelle(expectedLibelle);

			// Assert Ready to Test

			// Act
			editingContext.saveChanges();

			// Assert
			NSArray<EOUE> ues = EOUE.fetchAllSco_UEs(editingContext);
			assertEquals(1, ues.count());
			assertEquals(expectedCode, ues.get(0).code());
			assertEquals(expectedLibelle, ues.get(0).libelle());
		} catch (Exception e) {
			e.printStackTrace();
			fail("impossible d'enregistrer un UE");
		}
	}

	@Test
	public void test_copy() {
		// Arrange
		EOUE ue = EOTestsHelper.creerUE(editingContext, "ue", "ue");
		ue.setChoixECTS(2);
		ue.setChoixMax(3);
		ue.setChoixMin(1);
		ue.setSansComposant(1);
		// Assert ready to test

		// Act
		EOUE newUE = ue.copy(2014, null);
		// Assert

		assertComposantsIdentiques(ue, newUE);
		assertEquals(ue.choixECTS(), newUE.choixECTS());
		assertEquals(ue.choixMax(), newUE.choixMax());
		assertEquals(ue.choixMin(), newUE.choixMin());
		assertEquals(ue.sansComposant(), newUE.sansComposant());
	}

	/**
	 * Initialise le contexte necessaireà la création d'un composant de type Regroupement
	 */
	private void initialiserContexte() {
		// initialiser le contexte
		EOTypeComposant typeComposantUE = new EOTypeComposant();
		typeComposantUE.setNom(ITypeComposant.TYPEUE_NOM);
		typeComposantUE.setLibelle(ITypeComposant.TYPEUE_NOM);
		typeComposantUE.setId(4);
		editingContext.insertSavedObject(typeComposantUE);
	}
}
