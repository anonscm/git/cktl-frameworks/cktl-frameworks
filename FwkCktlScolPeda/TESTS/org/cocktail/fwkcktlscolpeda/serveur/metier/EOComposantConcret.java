package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;



/**
 * Cette classe permet d'implémenter la classe abstraite EOComposant
 */
@SuppressWarnings("serial")
public class EOComposantConcret extends EOComposant {	
	
	public EOComposantConcret() {
	}

	public static EOComposantConcret createComposantConcret(EOEditingContext edc){	
		EOComposantConcret newComposant = new EOComposantConcret();
		edc.insertObject(newComposant);
		newComposant.setTypeComposantRelationship(newComposant.getTypeComposant());
		newComposant.initialise();
		return newComposant;	
	}
	
	public EOComposantConcret(String code, String libelle, Integer id, EOTypeComposant typeComposant) {
		this.setCode(code);
		this.setLibelle(libelle);
		this.setId(id);
		this.setTypeComposant(typeComposant);
	}
	
	@Override
    public EOTypeComposant getTypeComposant() {		
	    return this.typeComposant();
    }
	

	
	@Override
    public EOComposantConcret copy(Integer anneeVersion, Integer numeroMajeur) {
	    // TODO Auto-generated method stub
	    return null;
    }


	@SuppressWarnings("unchecked")
    @Override
    public EOComposantConcret getComposantAvecOrigine() {
	    // TODO Auto-generated method stub
	    return null;
    }
	
	

}