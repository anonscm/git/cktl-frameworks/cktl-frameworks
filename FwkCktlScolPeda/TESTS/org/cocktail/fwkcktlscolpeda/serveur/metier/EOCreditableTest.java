/**
 * 
 */
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Cette classe teste les comportements communs à tous les objets dérivant de EOComposant
 * @author ldr
 */
public class EOCreditableTest extends EOScolTestCase {
	
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
    public void test_estValideOK() {
	    // Arrange
		EOCreditable creditable = new EOCreditable();	
		creditable.setBaseNote(20);

	    // Assert ready to test

	    // Act

	    // Assert
		boolean estValide= creditable.estValide();
		assertTrue(estValide);		
    }	

	@Test
    public void test_estValideKO() {
	    // Arrange
		EOCreditable creditable = new EOCreditable();	

	    // Assert ready to test

	    // Act

	    // Assert
		boolean estValide= creditable.estValide();
		assertFalse(estValide);		
    }	

	@Test
    public void test_copy() {
	    // Arrange
		EOCreditable creditable = new EOCreditable();
		creditable.setBaseNote(10);
		creditable.setCreditECTS(new BigDecimal(1.5));
		creditable.setSansNote(true);
		creditable.setTravailEtudiant(new BigDecimal(1.2));
		editingContext.insertSavedObject(creditable);
		
		EOCreditable newCreditable= creditable.copy();
	    // Assert ready to test

	    // Act
	    
		// Assert
		assertEquals(creditable.baseNote(), newCreditable.baseNote());
		assertEquals(creditable.creditECTS(), newCreditable.creditECTS());
		assertEquals(creditable.sansNote(), newCreditable.sansNote());
		assertEquals(creditable.travailEtudiant(), newCreditable.travailEtudiant());
    }
}
