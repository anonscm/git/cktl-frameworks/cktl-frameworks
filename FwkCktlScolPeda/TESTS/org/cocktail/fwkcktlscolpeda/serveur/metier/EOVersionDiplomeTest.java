package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class EOVersionDiplomeTest extends EOScolTestCase {

	private static final Integer expectedAnnee = 2012;
	
	private EOStatutFormation statutFormationProduction;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaire;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
		

	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	// Tests
	@Test
	public void test_initialise() {
		// Arrange
		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, "code", "libelle", statutFormationProduction, gradeUniversitaire, typeFormationMaster, annee);
		try {
			EOVersionDiplome versionDiplome = EOTestsHelper.creerVersionDiplome(editingContext, "", "", annee);
			
			// Act
			versionDiplome.initialise(diplome, expectedAnnee);
			editingContext.saveChanges();
			// Assert
			assertEquals(1, EOVersionDiplome.fetchAllSco_VersionDiplomes(editingContext).count());
			assertEquals(expectedAnnee, versionDiplome.annee());
			assertEquals("v 2012 - " + diplome.code(), versionDiplome.code());
			assertEquals("La version 2012 du diplôme code", versionDiplome.libelle());
		} catch (Exception e) {
			fail("impossible d'initialiser la version de diplome");
		}
	}
	
	@Test
    public void test_copy() {
	    // Arrange
		EOVersionDiplome versionDiplome= EOTestsHelper.creerVersionDiplome(editingContext, "code", "libelle", 2013);
		editingContext.saveChanges();

	    // Assert ready to test
		EOVersionDiplome newVersionDiplome =  versionDiplome.copy(2014, null); 

	    // Act

	    // Assert
		assertComposantsIdentiques(versionDiplome, newVersionDiplome);
		assertEquals(versionDiplome.annee(), newVersionDiplome.annee());
    }
	

	// Private Helpers
	/**
	 * Initialise le contexte necessaire à la création d'un composant de type version Diplome
	 */
	private void initialiserContexte() {
		EOTestsHelper.creerTypeComposant(editingContext, 1, "DIPLOME", "Diplôme");
		EOTestsHelper.creerTypeComposant(editingContext, 2, "VERSION_DIPLOME", "Diplôme");
		
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		
		gradeUniversitaire = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
	}

}
