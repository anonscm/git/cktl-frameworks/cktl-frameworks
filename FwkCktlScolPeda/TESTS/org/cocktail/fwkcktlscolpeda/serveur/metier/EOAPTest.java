package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class EOAPTest extends EOScolTestCase {
	
	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	// Tests	


	@Test
	public void test_peutEtresupprime() {
		// Arrange
		EOTypeAP typeAPCMCustom = EOTestsHelper.creerTypeAP(editingContext, 1, "CM", "CM", new BigDecimal("1.25"));
		EOAP ap1 = EOTestsHelper.creerAP(editingContext, "ap1", typeAPCMCustom);
		EOAP ap2 = EOTestsHelper.creerAP(editingContext, "ap2", typeAPCMCustom);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		EOEC ec2 = EOTestsHelper.creerEC(editingContext, "ec2", "ec2");

		EOComposantFactory.createLien(ec1, ap1, EOTypeLien.typeComposer(editingContext));
		EOComposantFactory.createLien(ec1, ap2, EOTypeLien.typeComposer(editingContext));
		EOComposantFactory.createLien(ec2, ap2, EOTypeLien.typeComposer(editingContext));

		// Assert ready to test
		assertEquals(3, EOLien.fetchAllSco_Liens(editingContext).count());

		// Act

		// Assert
		assertTrue("ap1 peut être supprimé", ap1.peutEtresupprime());
		assertFalse("ap2 ne peut pas être supprimé", ap2.peutEtresupprime());
	}
	
	@Test
    public void test_copy() {
	    // Arrange
		EOTypeAP typeAP = EOTestsHelper.creerTypeAP(editingContext, 1, "ap", "ap", null);
		EOAP ap = EOTestsHelper.creerAP(editingContext, "code", 80, 10, typeAP);
		ap.setSeuil(15);
		ap.setValeurTempsEtudiant(new BigDecimal("1.4"));
		ap.setModeRelationship(EOModeAP.modePresentiel(editingContext));
		editingContext.saveChanges();
		
	    // Assert ready to test
		EOAP apCopy= ap.copy(2014, null);
		
	    // Act

	    // Assert
		assertComposantsIdentiques(ap, apCopy);
		assertEquals(ap.chargeEnseignementTheorique().nbGroupes(), apCopy.chargeEnseignementTheorique().nbGroupes());
		assertEquals(ap.seuil(), apCopy.seuil());
		assertEquals(ap.chargeEnseignementTheorique().valeurHeures(), apCopy.chargeEnseignementTheorique().valeurHeures());
		assertEquals(ap.valeurTempsEtudiant(), apCopy.valeurTempsEtudiant());
		assertEquals(ap.mode(), apCopy.mode());
		assertEquals(ap.typeAP(), apCopy.typeAP());
    }
	
	// Private helpers
	/**
	 * Initialise le contexte necessaire à la création d'un composant de type EC
	 */
	private void initialiserContexte() {
		// initialiser le contexte
		EOTypeComposant typeComposanteAP = new EOTypeComposant();
		typeComposanteAP.setNom(ITypeComposant.TYPEAP_NOM);
		typeComposanteAP.setLibelle(ITypeComposant.TYPEAP_NOM);
		typeComposanteAP.setId(1);
		editingContext.insertSavedObject(typeComposanteAP);

		EOTypeComposant typeComposanteEC = new EOTypeComposant();
		typeComposanteEC.setNom(ITypeComposant.TYPEEC_NOM);
		typeComposanteEC.setLibelle(ITypeComposant.TYPEEC_NOM);
		typeComposanteEC.setId(2);
		editingContext.insertSavedObject(typeComposanteEC);

		EOTypeLien typeLien = new EOTypeLien();
		typeLien.setId(1);
		typeLien.setNom(EOTypeLien.TYPECOMPOSER_LBL);
		editingContext.insertSavedObject(typeLien);

		EORegleLiaison regleLiaison = new EORegleLiaison();
		regleLiaison.setId(1);
		editingContext.insertSavedObject(regleLiaison);
		regleLiaison.setForbidden(0);
		regleLiaison.setAllowed(1);
		regleLiaison.setVisible(1);
		regleLiaison.setAllowedParentRelationship(typeComposanteEC);
		regleLiaison.setAllowedChildRelationship(typeComposanteAP);
		regleLiaison.setTypeLienRelationship(typeLien);
		
		EOModeAP modePresentiel = new EOModeAP();
		modePresentiel.setId(1);
		modePresentiel.setCode("PRES");
		modePresentiel.setLibelle("Présentiel");
		editingContext.insertSavedObject(modePresentiel);
		
	}

}
