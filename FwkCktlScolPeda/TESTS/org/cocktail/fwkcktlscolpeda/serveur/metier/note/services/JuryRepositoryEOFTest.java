package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJury;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryPersonnes;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryRole;
import org.junit.Before;
import org.junit.Test;

public class JuryRepositoryEOFTest extends EOScolTestCase {

	JuryRepositoryEOF juryRepositoryEOF;
	IJury jury;
	IIndividu individu;
	IJuryRole juryRole;
	
	@Before
	public void setUp() {
		juryRepositoryEOF = new JuryRepositoryEOF();
		juryRepositoryEOF.setEditingContext(editingContext);
		
		jury = editingContext.createSavedObject(EOJury.class);
		individu = editingContext.createSavedObject(EOIndividu.class);
		juryRole = editingContext.createSavedObject(EOJuryRole.class);
	}
	
	@Test
	public void testAjouterMembre() {
		juryRepositoryEOF.ajouterMembre(jury, individu, juryRole);
		
		assertThat(jury.toJuryPersonneses()).hasSize(1);
		IJuryPersonnes juryPersonne = jury.toJuryPersonneses().get(0);
		assertThat(juryPersonne.toIndividu()).isEqualTo(individu);
	}

	@Test
	public void testFetchMembres() {
		creerJuryPersonne();
		
		List<IIndividu> membres = juryRepositoryEOF.fetchMembres(jury);
		
		assertThat(membres).containsOnly(individu);
	}


	@Test
	public void testSupprimerMembre() {
		creerJuryPersonne();
		assertThat(jury.toJuryPersonneses()).hasSize(1);
		
		juryRepositoryEOF.supprimerMembre(jury, individu);
		
		assertThat(jury.toJuryPersonneses()).isEmpty();
	}

	private void creerJuryPersonne() {
		IJuryPersonnes juryPersonne = editingContext.createSavedObject(EOJuryPersonnes.class);
		juryPersonne.setToIndividuRelationship(individu);
		jury.addToToJuryPersonnesesRelationship(juryPersonne);
	}
	
}
