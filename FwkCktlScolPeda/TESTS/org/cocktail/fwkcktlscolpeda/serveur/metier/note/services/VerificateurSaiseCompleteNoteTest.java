package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.DiplomeSimpleContainer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.TestsInitializer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services.InscriptionPedagogiqueTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.FwkCktlScolPedaTestUtils.DummySequence;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;

public class VerificateurSaiseCompleteNoteTest extends EOScolTestCase {

    private DiplomeSimpleContainer diplome;
    private IInscriptionPedagogiqueElement ipLienL1Semestre1;
    private IInscriptionPedagogiqueElement ipLienSemestre1Ec1;
    private IInscriptionPedagogiqueElement ipLienSemestre1Ec2;
    private IInscriptionPedagogiqueElement ipLienSemestre1Ec3;
    private IInscriptionPedagogiqueElement ipLienEc1Ap1;
    private IInscriptionPedagogiqueElement ipLienAp1Ae1;
	
	@Before
	public void setUp() {
        super.setUp();
        DummySequence sequenceLiens = new DummySequence();
        TestsInitializer testsInitiator = new TestsInitializer(editingContext);
        Integer annee = Calendar.getInstance().get(Calendar.YEAR);
        diplome = EOTestsHelper.creerDiplomeSimple(testsInitiator, sequenceLiens, annee);
        diplome.getDiplome();
        creerIpElements();
	}
	
	private void creerIpElements() {
		
        EOInscription inscription = editingContext.createSavedObject(EOInscription.class);
        inscription.setToDiplomeRelationship((EODiplome) diplome.getDiplome());
		
        EOInscriptionPedagogique inscriptionPeda = editingContext.createSavedObject(EOInscriptionPedagogique.class);
        inscriptionPeda.setToInscriptionRelationship(inscription);
        
        EOQualifier qualifier = EOTypeInscriptionPeda.CODE.eq(ITypeInscriptionPeda.CODE_STANDARD);
        ITypeInscriptionPeda typeIp = EOTypeInscriptionPeda.fetchSco_TypeInscriptionPeda(editingContext, qualifier);
        
		InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienDiplomeVersionDiplome(), typeIp);
		InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienVersionDiplomeL1(), typeIp);
		InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienVersionDiplomeL2(), typeIp);
		ipLienL1Semestre1 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienL1Semestre1(), typeIp);
		ipLienSemestre1Ec1 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienSemestre1Ec1(), typeIp);
		ipLienSemestre1Ec2 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienSemestre1Ec2(), typeIp);
		ipLienSemestre1Ec3 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienSemestre1Ec3(), typeIp);
		ipLienEc1Ap1 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienEc1Ap1(), typeIp);
		ipLienAp1Ae1 = InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, diplome.getLienAp1Ae1(), typeIp);
		
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetQualifierIpElement() {
		VerificateurSaiseCompleteNote verificateurSaiseCompleteNote = new VerificateurSaiseCompleteNote(editingContext);
		List<? extends ILien> liensSemestre = Arrays.asList(
				diplome.getLienL1Semestre1(),
				diplome.getLienSemestre1Ec1(),
				diplome.getLienSemestre1Ec2(),
				diplome.getLienSemestre1Ec3(),
				diplome.getLienEc1Ap1(),
				diplome.getLienAp1Ae1());
		
		EOQualifier qualifier = 
				verificateurSaiseCompleteNote.getQualifierIpElement(diplome.getDiplome(), null, null, (List<ILien>) liensSemestre);
		
		List<IInscriptionPedagogiqueElement> ipElements = new ArrayList<IInscriptionPedagogiqueElement>(
				EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext, qualifier, null));

		assertThat(ipElements).containsOnly(ipLienL1Semestre1, ipLienSemestre1Ec1, ipLienSemestre1Ec2, ipLienSemestre1Ec3, ipLienEc1Ap1, ipLienAp1Ae1);
	
	}

}
