/**
 * 
 */
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.fest.assertions.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurGenereAuto;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe teste les comportements communs a tous les objets derivant de EOComposant
 * @author ldr
 */
public class EOComposantTest extends EOScolTestCase {
	private EOTypeLien typeLien;
	private static final String expectedCode = "unCode";
	private static final String expectedLibelle = "unLibelle";
	private static final String expectedCode2 = "unCode2";
	private static final String expectedLibelle2 = "unLibelle2";
	
	// EOStatutFormation
	private EOStatutFormation statutFormationConstruction;
	private EOStatutFormation statutFormationProduction;
	private EOStatutFormation statutFormationFictif;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
	

	@Before
	public void setUp() {
		super.setUp();
		
		statutFormationConstruction = EOTestsHelper.creerStatutFormation(editingContext, 1, "ECC", "En construction");
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		statutFormationFictif = EOTestsHelper.creerStatutFormation(editingContext, 3, "F", "Fictif");
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
		typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "lien");
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantConcret, typeLien);

		EOTestsHelper.creerTypeDestinationDescriptif(editingContext, 1, EOTypeDescriptifDestination.TYPEPUBLICATION_LBL, EOTypeDescriptifDestination.TYPEPUBLICATION_LBL);

		ERXLocalizer.initialize();
		ERXValidationFactory.defaultFactory().configureFactory();
	}

	/**
	 * Test de la fonction d'insertion
	 */
	@Test
	public void test_insert() {
		// Arrange
		EOComposant composant = new EOComposantConcret(expectedCode, expectedLibelle, 1, typeComposantConcret);
		composant.setValidateur(new CodeValidateurSaisieObligatoire());
		editingContext.insertSavedObject(composant);

		// Assert
		EOComposant insertedComposant = EOComposant.fetchSco_Composant(editingContext, ERXQ.equals(EOComposant.CODE_KEY, composant.code()));
		assertNotNull(insertedComposant);
		assertEquals(expectedCode, insertedComposant.code());
		assertEquals(expectedLibelle, insertedComposant.libelle());
	}

	@Test
	public void test_nonDescendantdeParent() {
		// Arrange
		EOComposant parent = EOTestsHelper.creerComposantConcret(editingContext, "parent", "parent", 1, typeComposantConcret);

		NSArray<EOComposant> childs = new NSMutableArray<EOComposant>();
		for (Integer i = 2; i < 5; i++) {
			String str = "child" + i.toString();
			EOComposant child = new EOComposantConcret(str, str, i, typeComposantConcret);
			editingContext.insertSavedObject(child);
			childs.add(child);
		}
		EOComposantFactory.createLien(parent, childs.get(0), typeLien);
		EOComposantFactory.createLien(parent, childs.get(1), typeLien);
		editingContext.saveChanges();

		// Assert Ready to Test
		assertEquals(4, EOComposant.fetchAllSco_Composants(editingContext).count());

		// Act
		NSArray<EOComposant> composants = EOComposant.nonDescendantdeParent(editingContext, parent, typeComposantConcret);

		// Assert
		assertEquals(1, composants.count());
		assertEquals("child4", composants.get(0).code());
	}

	@Test
	public void test_composantsSansLien() {
		// Arrange
		EOComposant comp1 = EOTestsHelper.creerComposantConcret(editingContext, "comp1", "comp1", 1, typeComposantConcret);
		EOComposant comp2 = EOTestsHelper.creerComposantConcret(editingContext, "comp2", "comp2", 2, typeComposantConcret);
		EOComposant comp3 = EOTestsHelper.creerComposantConcret(editingContext, "comp3", "comp3", 3, typeComposantConcret);
		EOComposant comp4 = EOTestsHelper.creerComposantConcret(editingContext, "comp4", "comp4", 4, typeComposantConcret);
		EOComposant comp5 = EOTestsHelper.creerComposantConcret(editingContext, "comp5", "comp5", 5, typeComposantConcret);
		EOComposant comp6 = EOTestsHelper.creerComposantConcret(editingContext, "comp6", "comp6", 6, typeComposantConcret);

		EOComposantFactory.createLien(comp1, comp2, typeLien);
		EOComposantFactory.createLien(comp1, comp3, typeLien);
		EOComposantFactory.createLien(comp2, comp4, typeLien);
		EOComposantFactory.createLien(comp3, comp4, typeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		NSArray<EOComposant> compSansLien = comp1.composantsSansLienAvec(typeComposantConcret);

		// Assert
		assertEquals(2, compSansLien.count());
	}

	@Test
	public void test_parents() {
		// Arrange
		EOComposant comp1 = new EOComposantConcret("comp1", "comp1", 1, typeComposantConcret);
		editingContext.insertSavedObject(comp1);
		EOComposant comp2 = new EOComposantConcret("comp2", "comp2", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp2);
		EOComposant comp3 = new EOComposantConcret("comp3", "comp3", 3, typeComposantConcret);
		editingContext.insertSavedObject(comp3);

		EOComposantFactory.createLien(comp2, comp1, typeLien);
		EOComposantFactory.createLien(comp3, comp1, typeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		List<IComposant> parents = comp1.parents();

		// Assert
		assertEquals(2, parents.size());
	}

	@Test
	public void test_listeCodesParents() {
		// Arrange
		EOComposant comp1 = new EOComposantConcret("comp1", "comp1", 1, typeComposantConcret);
		editingContext.insertSavedObject(comp1);
		EOComposant comp2 = new EOComposantConcret("comp2", "comp2", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp2);
		EOComposant comp3 = new EOComposantConcret("comp3", "comp3", 3, typeComposantConcret);
		editingContext.insertSavedObject(comp3);

		EOComposantFactory.createLien(comp2, comp1, typeLien);
		EOComposantFactory.createLien(comp3, comp1, typeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act

		// Assert
		assertEquals("comp3", comp1.listeCodesAutresParents(comp2));
	}

	@Test
	public void test_allAncestors() {
		// Arrange
		EOComposant comp1 = new EOComposantConcret("comp1", "comp1", 1, typeComposantConcret);
		editingContext.insertSavedObject(comp1);
		EOComposant comp2 = new EOComposantConcret("comp2", "comp2", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp2);
		EOComposant comp3 = new EOComposantConcret("comp3", "comp3", 3, typeComposantConcret);
		editingContext.insertSavedObject(comp3);
		EOComposant comp4 = new EOComposantConcret("comp4", "comp4", 4, typeComposantConcret);
		editingContext.insertSavedObject(comp4);

		EOComposantFactory.createLien(comp1, comp2, typeLien);
		EOComposantFactory.createLien(comp1, comp3, typeLien);
		EOComposantFactory.createLien(comp2, comp4, typeLien);
		EOComposantFactory.createLien(comp3, comp4, typeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		NSArray<EOComposant> ancetres = comp4.allAncestors();

		// Assert
		assertEquals(3, ancetres.count());
	}

	@Test
	public void test_allSuccessors() {
		// Arrange
		EOComposant comp1 = new EOComposantConcret("comp1", "comp1", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp1);
		EOComposant comp2 = new EOComposantConcret("comp2", "comp2", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp2);
		EOComposant comp3 = new EOComposantConcret("comp3", "comp3", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp3);
		EOComposant comp4 = new EOComposantConcret("comp4", "comp4", 2, typeComposantConcret);
		editingContext.insertSavedObject(comp4);

		EOComposantFactory.createLien(comp1, comp2, typeLien);
		EOComposantFactory.createLien(comp1, comp3, typeLien);
		EOComposantFactory.createLien(comp2, comp4, typeLien);
		EOComposantFactory.createLien(comp3, comp4, typeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		NSArray<EOComposant> successors = comp1.allSuccessors();

		// Assert
		assertEquals(3, successors.count());
	}

	@Test
	public void test_remplacerParent() {
		// Arrange
		EOComposant parent = new EOComposantConcret("parent", "parent", 1, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		EOComposant nouveauParent = new EOComposantConcret("nouveauparent", "nouveauparent", 2, typeComposantConcret);
		editingContext.insertSavedObject(nouveauParent);
		EOComposant enfant = new EOComposantConcret("enfant", "enfant", 3, typeComposantConcret);
		editingContext.insertSavedObject(enfant);
		EOComposantFactory.createLien(parent, enfant, typeLien);
		editingContext.saveChanges();

		// Assert Ready to Test
		EOLien lien = EOLien.fetchRequiredSco_Lien(editingContext, EOLien.CHILD.eq(enfant).and(EOLien.PARENT.eq(parent)));
		assertNotNull(lien);

		// Act
		try {
			enfant.remplacerParent(editingContext, parent, nouveauParent);
			editingContext.saveChanges();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		// Assert
		try {
			lien = EOLien.fetchRequiredSco_Lien(editingContext, EOLien.CHILD.eq(enfant).and(EOLien.PARENT.eq(parent)));
			fail();
		} catch (Exception e) {
			// nothing to do: exception expected
		}

		lien = EOLien.fetchRequiredSco_Lien(editingContext, EOLien.CHILD.eq(enfant).and(EOLien.PARENT.eq(nouveauParent)));
		assertEquals(nouveauParent, lien.parent());
		assertEquals(enfant, lien.child());
	}

	@Test
	public void test_remplacerParentNull() {
		// Arrange
		EOComposant parent = new EOComposantConcret("parent", "parent", 1, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		EOComposant enfant = new EOComposantConcret("enfant", "enfant", 3, typeComposantConcret);
		editingContext.insertSavedObject(enfant);

		EOComposantFactory.createLien(parent, enfant, typeLien);
		editingContext.saveChanges();

		// Assert Ready to Test
		EOLien lien = EOLien.fetchRequiredSco_Lien(editingContext, EOLien.CHILD.eq(enfant).and(EOLien.PARENT.eq(parent)));
		assertNotNull(lien);

		// Act
		// Assert
		try {
			enfant.remplacerParent(editingContext, parent, null);
			editingContext.saveChanges();
		} catch (Exception e) {
			fail(); // shouldn't throw exception
		}

		try {
			enfant.remplacerParent(editingContext, parent, enfant);
			editingContext.saveChanges();
			fail();
		} catch (Exception e) {
			// do nothing: test passes
		}
	}

	@Test
	public void test_supprimer() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);		
		editingContext.insertSavedObject(composant);
		composant.setComposantInfoVersion(EOComposantInfoVersion.create(editingContext, 2014, null));
		
		EOComposant parent = new EOComposantConcret("parent", "parent", 2, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		parent.setComposantInfoVersion(EOComposantInfoVersion.create(editingContext, 2014, null));
		
		EOComposant enfant = new EOComposantConcret("enfant", "enfant", 3, typeComposantConcret);
		editingContext.insertSavedObject(enfant);
		enfant.setComposantInfoVersion(EOComposantInfoVersion.create(editingContext, 2014, null));

		EOSessionComposant session = EOTestsHelper.creerSession(editingContext, "session");
		session.setComposantRelationship(composant);
		editingContext.saveChanges();

		EOComposantFactory.createLien(parent, composant, typeLien);
		EOComposantFactory.createLien(composant, enfant, typeLien);
		editingContext.saveChanges();

		composant.associerDomaine(EOTestsHelper.creerDomaine(editingContext));
		// TODO : stub implementationGeo
		// composant.setImplantationGeo(EOTestsHelper.creeImplantationGeo(editingContext));

		// Assert ready to test
		assertEquals("Liens", 2, EOLien.fetchAllSco_Liens(editingContext).count());
		assertEquals("SessionComposants", 1, EOSessionComposant.fetchAllSco_SessionComposants(editingContext).count());
		// assertNotNull(composant.domaine());

		// Act
		try {
			composant.supprimer();
			editingContext.saveChanges();
		} catch (Exception e) {
			e.printStackTrace();
			fail("La suppression du composant a échoué");
		}

		// Assert
		assertEquals("EOLien", 0, EOLien.fetchAllSco_Liens(editingContext).count());
		assertEquals("SessionComposants", 0, EOSessionComposant.fetchAllSco_SessionComposants(editingContext).count());
	}

	@Test
	public void test_supprimerComposantAvecLienCreditable() {
		// Arrange
		EOComposant parent = new EOComposantConcret("parent", "parent", 2, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		EOComposant enfant = new EOComposantConcret("enfant", "enfant", 3, typeComposantConcret);
		editingContext.insertSavedObject(enfant);
		EORegleLiaison regleLiaison = EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantConcret,
		    EOTypeLien.typeComposer(editingContext));
		editingContext.saveChanges();

		EOLienComposer lien = EOComposantFactory.createLienComposer(parent, enfant);
		lien.addToCreditablesRelationship(EOTestsHelper.creerCreditable(editingContext));
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		try {
			enfant.supprimer();
			editingContext.saveChanges();
		} catch (Exception e) {			
			e.printStackTrace();
			fail();
		}
		
		// Assert
		assertEquals("EOComposant",1, EOComposant.fetchAllSco_Composants(editingContext).count());
	}

	@Test
	public void test_supprimerComposantAvecDescriptif() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);
		composant.setComposantInfoVersionRelationship(EOComposantInfoVersion.create(editingContext, 2013, null));
	

		EODescriptif descriptif = EOComposantFactory.createDescriptif(composant);
		EODescriptifChampContenu champcontenu1 = EOTestsHelper.creerDescriptifChampContenu(editingContext, "contenu descriptif");
		champcontenu1.setDescriptifRelationship(descriptif);
		editingContext.saveChanges();

		// Assert ready to test
		assertEquals("EODescriptif", 1, EODescriptif.fetchAllSco_Descriptifs(editingContext).count());
		assertEquals("EODescriptifChampContenu", 1, EODescriptifChampContenu.fetchAllSco_DescriptifChampContenus(editingContext).count());

		// Act
		try {
			composant.supprimer();
			editingContext.saveChanges();
		} catch (Exception e) {			
			e.printStackTrace();
			fail("La suppression d'un composant avec descriptif a échoué");
		}

		// Assert
		assertEquals("EODescriptif", 0, EODescriptif.fetchAllSco_Descriptifs(editingContext).count());
		assertEquals("EODescriptifChampContenu", 0, EODescriptifChampContenu.fetchAllSco_DescriptifChampContenus(editingContext).count());
	}

	@Test
	public void test_associerCreditableOK() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		EOCreditable creditable = new EOCreditable();
		creditable.setCreditECTS(new BigDecimal(10));
		editingContext.insertSavedObject(creditable);

		// Assert ready to test
		assertTrue(composant.creditable() == null);

		// Act
		composant.associerCreditable(creditable);

		// Assert
		assertTrue(composant.creditable() != null);
	}

	@Test
	public void test_associerCreditableKO() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		EOCreditable creditable = null;

		// Assert ready to test
		assertTrue(composant.creditable() == null);

		// Act
		composant.associerCreditable(creditable);

		// Assert
		assertTrue(composant.creditable() == null);
	}

	@Test
	public void test_associerDomaineNull() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		EODomaine domaine = null;

		// Assert ready to test
		assertEquals(0, composant.domaines().size());

		// Act
		composant.associerDomaine(domaine);

		// Assert
		assertEquals(0, composant.domaines().size());
	}

	@Test
	public void test_associerDomaineOK() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		EODomaine domaine = EOTestsHelper.creerDomaine(editingContext);

		// Assert ready to test
		assertEquals(0, composant.domaines().size());

		// Act
		composant.associerDomaine(domaine);

		// Assert
		assertEquals(1, composant.domaines().size());
	}

	@Test
	public void test_associerLangue() {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);
		EOLangue langue = EOTestsHelper.creerLangue(editingContext);

		// Assert ready to test

		// Act
		// Assert
		composant.associerLangue(langue);
		assertEquals(1, composant.langues().count());

		composant.removeFromLangues(langue);
		assertEquals(0, composant.langues().count());
	}

	@Test
	public void test_setUnImplantationGeo() throws Exception {
		// Arrange
		EOImplantationGeo implantationGeo = EOTestsHelper.creerImplantationGeo(editingContext);

		EOComposant composant = EOTestsHelper.creerComposantConcret(editingContext, "comp", "comp", 1, typeComposantConcret);
		editingContext.insertObject(composant);
		composant.setImplantationGeo(implantationGeo);

		// Assert ready to test

		// Act
		// Assert
		try {
			editingContext.saveChanges();
			assertTrue(true); // test passe
		} catch (Exception e) {
			assertTrue(false); // test √©choue
		}

		assertEquals(implantationGeo, composant.getImplantationGeo());
	}

	@Test
	public void test_setUnImplantationGeoNULL() throws Exception {
		// Arrange
		EOComposant composant = new EOComposantConcret("comp", "comp", 1, typeComposantConcret);
		composant.setValidateur(new CodeValidateurGenereAuto());
		editingContext.insertObject(composant);

		composant.setImplantationGeo(null);

		// Assert ready to test

		// Act
		// Assert
		try {
			editingContext.saveChanges();
			assertTrue(true); // test passe
		} catch (Exception e) {
			assertTrue(false); // test √©choue
		}

		assertEquals(null, composant.getImplantationGeo());
	}

	@Test
	public void test_liensAvecLesComposantEnfants() {
		// Arrange
		EOComposant parent = new EOComposantConcret("parent", "parent", 1, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		EOComposant child1 = new EOComposantConcret("child1", "child1", 2, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOComposant child2 = new EOComposantConcret("child2", "child2", 3, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOComposant child3 = new EOComposantConcret("child3", "child3", 4, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOTypeLien autreTypeLien = EOTestsHelper.creerTypeLien(editingContext, 2, "autreLien");
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantConcret, autreTypeLien);

		EOComposantFactory.createLien(parent, child1, typeLien);
		EOComposantFactory.createLien(parent, child2, typeLien);
		EOComposantFactory.createLien(parent, child3, autreTypeLien);
		editingContext.saveChanges();
		// Assert ready to test

		// Act

		// Assert
		assertEquals(2, parent.liensAvecLesComposantsEnfants(typeLien).count());
		assertEquals(1, parent.liensAvecLesComposantsEnfants(autreTypeLien).count());
	}

	@Test
	public void test_liensComposerAvecLesComposantsEnfants() {
		// Arrange
		EOComposant parent = new EOComposantConcret("parent", "parent", 1, typeComposantConcret);
		editingContext.insertSavedObject(parent);
		EOComposant child1 = new EOComposantConcret("child1", "child1", 2, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOComposant child2 = new EOComposantConcret("child2", "child2", 3, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOComposant child3 = new EOComposantConcret("child3", "child3", 4, typeComposantConcret);
		editingContext.insertSavedObject(child1);
		EOTypeLien lienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantConcret, lienComposer);

		EOLienComposer lien1 = EOComposantFactory.createLienComposer(parent, child1);
		lien1.setId(1);
		EOLienComposer lien2 = EOComposantFactory.createLienComposer(parent, child2);
		lien2.setId(2);
		EOLienComposer lien3 = EOComposantFactory.createLienComposer(parent, child3);
		lien3.setId(3);
		editingContext.saveChanges();

		// Assert ready to test

		// Act

		// Assert
		assertEquals(3, parent.liensAvecLesComposantsEnfants(lienComposer).count());
		// TODO √† revoir
		// assertEquals(3, parent.liensComposerAvecLesComposantsEnfants().count());
	}

	@Test
	public void test_descriptifCourant() {
		// Arrange
		EOComposant composant = new EOComposantConcret("composant", "composant", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		// Assert ready to test
		assertEquals("descriptifs", 0, EODescriptif.fetchAllSco_Descriptifs(editingContext).count());

		// Act
		// Assert
		EODescriptif descriptif = composant.descriptifCourant();
		descriptif.setId(1);
		assertNotNull("descriptif", descriptif);
		// TODO: mocker le libelle du descriptif
		// assertEquals("En cours de cr√©ation", descriptif.libelle());

		editingContext.saveChanges();
	}

	@Test
	public void test_typesComposantEnfantAutorise() {
		// Arrange
		EOComposant composant = new EOComposantConcret("composant", "composant", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);

		EOTypeComposant type1 = EOTestsHelper.creerTypeComposant(editingContext, 2, "type1", "type1");
		EOTypeComposant type2 = EOTestsHelper.creerTypeComposant(editingContext, 3, "type2", "type2");
		EOTypeComposant type3 = EOTestsHelper.creerTypeComposant(editingContext, 4, "type3", "type3");
		EOTypeComposant type4 = EOTestsHelper.creerTypeComposant(editingContext, 5, "type4", "type4");
		EOTypeComposant type5 = EOTestsHelper.creerTypeComposant(editingContext, 6, "type5", "type5");

		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, type1, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, type2, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, type3, typeLienComposer);

		NSArray<EOTypeComposant> composantsAutorises = composant.typesComposantEnfantAutorise(null);

		assertEquals(3, composantsAutorises.count());

		NSArray<EOTypeComposant> composantsSupplementairesInterdits = new NSMutableArray<EOTypeComposant>();
		composantsSupplementairesInterdits.add(type1);
		composantsSupplementairesInterdits.add(type4);
		composantsSupplementairesInterdits.add(type5);

		composantsAutorises = composant.typesComposantEnfantAutorise(composantsSupplementairesInterdits);

		assertEquals(2, composantsAutorises.count());

	}

	@Test
	public void test_getAPCours() {
		// Arrange
		EOTypeComposant typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 2, "EC", "EC");
		EOTypeComposant typeComposantAP = EOTestsHelper.creerTypeComposant(editingContext, 1, "AP", "AP");

		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantEC, typeComposantAP, typeLienComposer);

		EOTypeAP typeAPCours = EOTestsHelper.creerTypeAP(editingContext, 1, "CM", "Cours", null);

		EOEC ec = EOTestsHelper.creerEC(editingContext, "ec", "ec");
		editingContext.insertSavedObject(ec);

		EOAP ap1 = EOTestsHelper.creerAP(editingContext, "ap1", typeAPCours);
		editingContext.insertSavedObject(ap1);

		EOLienComposer lienComposer = EOComposantFactory.createLienComposer(ec, ap1);

		// Assert ready to test
		EOLienComposer insertedLien = (EOLienComposer) EOLien.fetchSco_Lien(editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, ec).and(ERXQ.equals(EOLien.CHILD_KEY, ap1)).and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLienComposer)));
		assertNotNull(insertedLien);
		assertEquals(lienComposer, insertedLien);
		assertEquals(ec, insertedLien.parent());
		assertEquals(ap1, insertedLien.child());
		assertNotNull(typeComposantEC);
		assertNotNull(typeComposantAP);
		assertNotNull(typeLienComposer);

		// Act
		EOAP apReturn = ec.getAPCours();

		// Assert
		assertNotNull(apReturn);
		assertEquals(apReturn, ap1);

	}

	@Test
	public void test_getAPTD() {
		// Arrange
		EOTypeComposant typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 2, "EC", "EC");
		EOTypeComposant typeComposantAP = EOTestsHelper.creerTypeComposant(editingContext, 1, "AP", "AP");

		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantEC, typeComposantAP, typeLienComposer);

		EOTypeAP typeAPTD = EOTestsHelper.creerTypeAP(editingContext, 1, "TD", "TD", null);

		EOEC ec = EOTestsHelper.creerEC(editingContext, "ec", "ec");
		editingContext.insertSavedObject(ec);

		EOAP ap1 = EOTestsHelper.creerAP(editingContext, "ap1", typeAPTD);
		editingContext.insertSavedObject(ap1);

		EOLienComposer lienComposer = EOComposantFactory.createLienComposer(ec, ap1);

		// Assert ready to test
		EOLienComposer insertedLien = (EOLienComposer) EOLien.fetchSco_Lien(editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, ec).and(ERXQ.equals(EOLien.CHILD_KEY, ap1)).and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLienComposer)));
		assertNotNull(insertedLien);
		assertEquals(lienComposer, insertedLien);
		assertEquals(ec, insertedLien.parent());
		assertEquals(ap1, insertedLien.child());
		assertNotNull(typeComposantEC);
		assertNotNull(typeComposantAP);
		assertNotNull(typeLienComposer);

		// Act
		EOAP apReturn = ec.getAPTD();

		// Assert
		assertNotNull(apReturn);
		assertEquals(apReturn, ap1);

	}

	@Test
	public void test_getAPTP() {
		// Arrange
		EOTypeComposant typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 2, "EC", "EC");
		EOTypeComposant typeComposantAP = EOTestsHelper.creerTypeComposant(editingContext, 1, "AP", "AP");

		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret, typeComposantEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantEC, typeComposantAP, typeLienComposer);

		EOTypeAP typeAPTP = EOTestsHelper.creerTypeAP(editingContext, 1, "TP", "TP", null);

		EOEC ec = EOTestsHelper.creerEC(editingContext, "ec", "ec");
		editingContext.insertSavedObject(ec);

		EOAP ap1 = EOTestsHelper.creerAP(editingContext, "ap1", typeAPTP);
		editingContext.insertSavedObject(ap1);

		EOLienComposer lienComposer = EOComposantFactory.createLienComposer(ec, ap1);

		// Assert ready to test
		EOLienComposer insertedLien = (EOLienComposer) EOLien.fetchSco_Lien(editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, ec).and(ERXQ.equals(EOLien.CHILD_KEY, ap1)).and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLienComposer)));
		assertNotNull(insertedLien);
		assertEquals(lienComposer, insertedLien);
		assertEquals(ec, insertedLien.parent());
		assertEquals(ap1, insertedLien.child());
		assertNotNull(typeComposantEC);
		assertNotNull(typeComposantAP);
		assertNotNull(typeLienComposer);

		// Act
		EOAP apReturn = ec.getAPTP();

		// Assert
		assertNotNull(apReturn);
		assertEquals(apReturn, ap1);

	}

	@Test
	public void test_assignTo() {

		try {
			// Arrange
			EOComposant composant = EOTestsHelper.creerComposantConcret(editingContext, expectedCode, expectedLibelle, 1, typeComposantConcret);
			// ajout de structures
			for (int i = 0; i < 2; i++) {
				EOStructure structure = EOTestsHelper.creerStructure(editingContext, "structure" + i);
				composant.addToStructuresRelationship(structure);
			}
			// ajout de disciplines
			for (int i = 0; i < 5; i++) {
				EODiscipline discipline = EOTestsHelper.creerDiscipline(editingContext);
				composant.addToDisciplinesRelationship(discipline);
			}

			// ajout d'implantations geographiques
			for (int i = 0; i < 4; i++) {
				EOImplantationGeo implantGeo = EOTestsHelper.creerImplantationGeo(editingContext);
				composant.addToImplantationsGeosRelationship(implantGeo);
			}

			editingContext.saveChanges();

			// Assert ready to test

			// Act
			EOComposant newComposant = EOTestsHelper.creerComposantConcret(editingContext, expectedCode2, expectedLibelle2, 2, typeComposantConcret);
			;// EOComposantConcret.createComposantConcret(editingContext);
			composant.assignTo(newComposant);
			editingContext.saveChanges();

			// Assert
			assertComposantsIdentiques(composant, newComposant);

		} catch (Exception e) {
			e.printStackTrace();
			fail("assignToFailed");
		}
	}

	@Test
	public void test_enConstructionComposantSeul() {
		EOComposant newComposant = EOTestsHelper.creerComposantConcret(editingContext, "code", "libelle", 1, typeComposantConcret);
		assertTrue(newComposant.isVisibleEnConstruction(2013));
	}

	@Test
	public void test_enConstructionAvecDiplomes() {
		// Arrange
		EOTypeComposant typeComposantDiplome = EOTestsHelper
		    .creerTypeComposant(editingContext, 2, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEVERSIONDIPLOME_NOM,
		    ITypeComposant.TYPEVERSIONDIPLOME_NOM);

		EOTypeLien typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 1, "versionner");
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantConcret, typeLienComposer);

		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome1 = EOTestsHelper.creerDiplome(editingContext, "code1", "libelle", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome1.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome1);

		EODiplome diplome2 = EOTestsHelper.creerDiplome(editingContext, "code2", "libelle", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome2.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome2);

		EODiplome diplome3 = EOTestsHelper.creerDiplome(editingContext, "code3", "libelle", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome3.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome3);

		EOComposant newComposant = EOTestsHelper.creerComposantConcret(editingContext, "code", "libelle", 5, typeComposantConcret);

		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome1.getVersionDiplome(annee), newComposant, false);
		EOTestsHelper.creerLienComposer(editingContext, 2, (EOComposant) diplome2.getVersionDiplome(annee), newComposant, false);
		EOTestsHelper.creerLienComposer(editingContext, 3, (EOComposant) diplome3.getVersionDiplome(annee), newComposant, false);

		editingContext.saveChanges();
		// Assert ready to test
		assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count());

		// Act
		// Assert
		assertFalse(newComposant.isVisibleEnConstruction(annee));

		diplome2.setStatutFormation(statutFormationConstruction);
		assertTrue(newComposant.isVisibleEnConstruction(annee));

		diplome2.setStatutFormation(statutFormationProduction);
		assertFalse(newComposant.isVisibleEnConstruction(annee));

		diplome2.setStatutFormation(statutFormationFictif);
		assertFalse(newComposant.isVisibleEnConstruction(annee));

	}

	@Test
	public void test_enProductionComposantSeul() {
		EOComposant newComposant = EOTestsHelper.creerComposantConcret(editingContext, "code", "libelle", 1, typeComposantConcret);
		assertTrue(newComposant.isVisibleEnProduction(2013));
	}

	@Test
	public void test_enProductionAvecDiplomes() {
		// Arrange
		EOTypeComposant typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		//EOTypeComposant typeComposantPeriode = EOTestsHelper.creerTypeComposant(editingContext, 3, EOTypeComposant.TYPEPERIODE_LBL, EOTypeComposant.TYPEPERIODE_LBL);
		
		EOTypeLien typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 1, "versionner");
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantConcret, typeLienComposer);
		//EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantPeriode, typeLienComposer);

		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome1 = EOTestsHelper.creerDiplome(editingContext, "code1", "libelle", statutFormationConstruction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome1.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome1);

		EODiplome diplome2 = EOTestsHelper.creerDiplome(editingContext, "code2", "libelle", statutFormationConstruction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome2.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome2);

		EODiplome diplome3 = EOTestsHelper.creerDiplome(editingContext, "code3", "libelle", statutFormationConstruction, gradeUniversitaireMaster, typeFormationMaster, annee);
		diplome3.enregistrer(annee, 1);
		EOTestsHelper.setValidateur(diplome3);

		EOComposant newComposant = EOTestsHelper.creerComposantConcret(editingContext, "code", "libelle", 5, typeComposantConcret);
		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome1.getVersionDiplome(annee), newComposant, false);
		EOTestsHelper.creerLienComposer(editingContext, 2, (EOComposant) diplome2.getVersionDiplome(annee), newComposant, false);
		EOTestsHelper.creerLienComposer(editingContext, 3, (EOComposant) diplome3.getVersionDiplome(annee), newComposant, false);

		editingContext.saveChanges();
		
		// Assert ready to test
		assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count());

		// Act
		
		// Assert
		//System.err.println("newComposant.isVisibleEnProduction(annee) 1 : "+newComposant.isVisibleEnProduction(annee));
		assertTrue(newComposant.isVisibleEnProduction(annee));

		diplome1.setHabilitationRequise(1);
		diplome2.setHabilitationRequise(1);
		diplome3.setHabilitationRequise(1);
		//System.err.println("newComposant.isVisibleEnProduction(annee) 2 : "+newComposant.isVisibleEnProduction(annee));
		//assertFalse(newComposant.isVisibleEnProduction(annee));
		//System.err.println("newComposant.isVisibleEnConstruction(annee) 2 : "+newComposant.isVisibleEnConstruction(annee));
		assertTrue(newComposant.isVisibleEnConstruction(annee));

		diplome2.setStatutFormation(statutFormationProduction);
		//System.err.println("newComposant.isVisibleEnProduction(annee) 3 : "+newComposant.isVisibleEnProduction(annee));
		assertTrue(newComposant.isVisibleEnProduction(annee));
	}

	@Test
	public void test_childs() {
		// Arrange
		EOTypeLien typeLienA = EOTestsHelper.creerTypeLien(editingContext, null, "lien1");
		EOTypeLien typeLienB = EOTestsHelper.creerTypeLien(editingContext, null, "lien2");
		EOTypeComposant typeComposantParent = EOTestsHelper.creerTypeComposant(editingContext, null, "typeComposantParent", "typeComposantParent");
		EOTypeComposant typeComposantEnfant1 = EOTestsHelper.creerTypeComposant(editingContext, null, "typeComposantEnfant1", "typeComposantEnfant1");
		EOTypeComposant typeComposantEnfant2 = EOTestsHelper.creerTypeComposant(editingContext, null, "typeComposantEnfant2", "typeComposantEnfant2");
		EOTypeComposant typeComposantEnfant3 = EOTestsHelper.creerTypeComposant(editingContext, null, "typeComposantEnfant2", "typeComposantEnfant2");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantParent, typeComposantEnfant1, typeLienA);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantParent, typeComposantEnfant2, typeLienA);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantParent, typeComposantEnfant3, typeLienA);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantParent, typeComposantEnfant3, typeLienB);

		EOComposant parent = EOTestsHelper.creerComposantConcret(editingContext, "parent", "parent", null, typeComposantParent);

		EOComposant enfantType11 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType11", "enfantType11", null, typeComposantEnfant1);
		EOComposant enfantType12 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType11", "enfantType12", null, typeComposantEnfant1);
		EOComposant enfantType13 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType13", "enfantType13", null, typeComposantEnfant1);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType11, typeLienA);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType12, typeLienA);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType13, typeLienA);

		EOComposant enfantType21 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType21", "enfantType21", null, typeComposantEnfant2);
		EOComposant enfantType22 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType21", "enfantType21", null, typeComposantEnfant2);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType21, typeLienA);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType22, typeLienA);

		EOComposant enfantType31 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType31", "enfantType31", null, typeComposantEnfant3);
		EOComposant enfantType32 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType32", "enfantType32", null, typeComposantEnfant3);
		EOComposant enfantType33 = EOTestsHelper.creerComposantConcret(editingContext, "enfantType33", "enfantType33", null, typeComposantEnfant3);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType31, typeLienA);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType32, typeLienB);
		EOTestsHelper.creerLien(editingContext, null, parent, enfantType33, typeLienB);

		// Assert ready to test
		// Act
		// Assert
		assertEquals(8, parent.childs().size());
		assertEquals(3, parent.childs(typeComposantEnfant1).size());
		assertEquals(2, parent.childs(typeLienB).size());
		assertEquals(1, parent.childs(typeLienA, typeComposantEnfant3, null).size());
	}

	@Test
	public void test_getLiensAnnees() {
		// Arrange
		EOTypeComposant typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, null, ITypeComposant.TYPEDIPLOME_NOM,
		    ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, null, ITypeComposant.TYPEVERSIONDIPLOME_NOM,
		    ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTypeComposant typeComposantPeriode = EOTestsHelper.creerTypeComposant(editingContext, null, ITypeComposant.TYPEPERIODE_NOM,
		    ITypeComposant.TYPEPERIODE_NOM);
		EOTypePeriode typeAnnee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);
		EOTypePeriode typeSemestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, EOTypePeriode.CODE_SEMESTRE);
		
		EOTypeLien typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 1, "versionner");
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantPeriode, typeLienComposer);

		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, "diplome", "diplome", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, annee);
		EOVersionDiplome versiondiplome = EOTestsHelper.creerVersionDiplome(editingContext, "vdiplome", "vdiplome", 2013);
		EOPeriode  annee1 = EOTestsHelper.creerPeriode(editingContext, "annee1", "annee1", typeAnnee,1);		
		EOPeriode  annee2 = EOTestsHelper.creerPeriode(editingContext, "annee2", "annee2", typeAnnee, 2);		
		EOPeriode  annee2bis = EOTestsHelper.creerPeriode(editingContext, "annee2bis", "annee2bis", typeAnnee,2);
		EOPeriode semestre1 = EOTestsHelper.creerPeriode(editingContext, "semestre1", "semestre1", typeSemestre,1);
		
		
		EOTestsHelper.creerLien(editingContext, null, diplome, versiondiplome, typeLienVersionner);		
		EOTestsHelper.creerLien(editingContext, null, annee1, semestre1, typeLienComposer);
		EOTestsHelper.creerLien(editingContext, null, versiondiplome, annee1, typeLienComposer);
		EOTestsHelper.creerLien(editingContext, null, versiondiplome, annee2, typeLienComposer);
		EOTestsHelper.creerLien(editingContext, null, annee2, annee2bis, typeLienComposer);
		
		// Assert ready to test
		
		// Act
		
		// Assert
		assertThat(diplome.getLiensAnnees(1).size()).isEqualTo(1);
		assertThat(diplome.getLiensAnnees(2).size()).isEqualTo(2);
	}
}
