package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests de la classe EOComposantFactory
 */
public class EOComposantFactoryTest extends EOScolTestCase {
	// EOTypeComposant
	private EOTypeComposant typeAP;
	private EOTypeComposant typeAE;
	private EOTypeComposant typeUE;
	private EOTypeComposant typeEC;
	private EOTypeComposant typePeriode;
	private EOTypeComposant typeRegroupement;
	private EOTypeComposant typeParcours;
	private EOTypeComposant typeDiplome;
	private EOTypeComposant typeVersionDiplome;

	// EOStatutFormation
	private EOStatutFormation statutFormationProduction;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;

	private static final Integer ctePersId = 1;

	@Before
	public void setUp() {
		super.setUp();
		typeAP = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEAP_NOM, ITypeComposant.TYPEAP_NOM);
		typeAE = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAE_NOM);
		typeUE = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEUE_NOM, ITypeComposant.TYPEUE_NOM);
		typeEC = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		typePeriode = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		typeRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEREGROUPEMENT_NOM, ITypeComposant.TYPEREGROUPEMENT_NOM);
		typeParcours = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
		typeDiplome = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		typeVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);

		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");

		initialiserContexte();
	}

	@Test
	public void test_createComposantAP() {
		try {
			// Arrange
			EOTypeAP typeAPCMCustom = EOTestsHelper.creerTypeAP(editingContext, 1, "CM", "CM", new BigDecimal("1.25"));

			// Assert ready to test
			// Act
			EOAP ap = EOComposantFactory.createComposant(EOAP.class, ctePersId, editingContext,2014, null);
			ap.setCode("code");
			ap.setLibelle("libelle");
			ap.setId(1);
			ap.setModeRelationship(EOModeAP.modePresentiel(editingContext));
			ap.setModeAPId(1);
			ap.setValidateur(new CodeValidateurSaisieObligatoire());
			ap.setTypeAP(typeAPCMCustom);
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", ap);
			assertEquals("composant type", EOAP.class, ap.getClass());
			assertEquals(1, EOAP.fetchAllSco_APs(editingContext).count());
			assertEquals(typeAP, ap.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, ap.persIdCreation());
			assertNotNull(ap.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantAE() {

		try {
			// Arrange
			EOTypeAE typeAECMCustom = EOTestsHelper.creerTypeAE(editingContext, 1, "CM", "CM");
			
			// Assert ready to test
			// Act
			EOAE ae = EOComposantFactory.createComposant(EOAE.class, ctePersId, editingContext,2014, null);
			ae.setCode("code");
			ae.setLibelle("libelle");
			ae.setId(1);
			ae.setValidateur(new CodeValidateurSaisieObligatoire());
			ae.setTypeAE(typeAECMCustom);
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", ae);
			assertEquals("composant type", EOAE.class, ae.getClass());
			assertEquals(1, EOAE.fetchAllSco_AEs(editingContext).count());
			assertEquals(typeAE, ae.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, ae.persIdCreation());
			assertNotNull(ae.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantUE() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EOUE ue = EOComposantFactory.createComposant(EOUE.class, ctePersId, editingContext,2014, null);
			ue.setCode("code");
			ue.setLibelle("libelle");
			ue.setId(1);
			ue.setSansComposant(0);
			ue.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", ue);
			assertEquals("composant type", EOUE.class, ue.getClass());
			assertEquals(1, EOUE.fetchAllSco_UEs(editingContext).count());
			assertEquals(typeUE, ue.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, ue.persIdCreation());
			assertNotNull(ue.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantEC() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EOEC ec = EOComposantFactory.createComposant(EOEC.class, ctePersId, editingContext,2014, null);
			ec.setCode("code");
			ec.setLibelle("libelle");
			ec.setId(1);
			ec.setLibre(0);
			ec.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", ec);
			assertEquals("composant type", EOEC.class, ec.getClass());
			assertEquals(1, EOEC.fetchAllSco_ECs(editingContext).count());
			assertEquals(typeEC, ec.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, ec.persIdCreation());
			assertNotNull(ec.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantPeriode() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EOTypePeriode typePeriodeAnnee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);
			EOPeriode periode = EOComposantFactory.createComposant(EOPeriode.class, ctePersId, editingContext,2014, null);
			periode.setCode("code");
			periode.setLibelle("libelle");
			periode.setId(1);
			periode.setOrdre(1);
			periode.setToTypePeriodeRelationship(typePeriodeAnnee);
			periode.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", periode);
			assertEquals("composant type", EOPeriode.class, periode.getClass());
			assertEquals(1, EOPeriode.fetchAllSco_Periodes(editingContext).count());
			assertEquals(typePeriode, periode.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, periode.persIdCreation());
			assertNotNull(periode.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantRegroupement() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EORegroupement regroupement = EOComposantFactory.createComposant(EORegroupement.class, ctePersId, editingContext,2014, null);
			regroupement.setCode("code");
			regroupement.setLibelle("libelle");
			regroupement.setId(1);
			regroupement.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", regroupement);
			assertEquals("composant type", EORegroupement.class, regroupement.getClass());
			assertEquals(1, EORegroupement.fetchAllSco_Regroupements(editingContext).count());
			assertEquals(typeRegroupement, regroupement.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, regroupement.persIdCreation());
			assertNotNull(regroupement.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantParcours() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EOParcours parcours = EOComposantFactory.createComposant(EOParcours.class, ctePersId, editingContext,2014, null);
			parcours.setCode("code");
			parcours.setLibelle("libelle");
			parcours.setId(1);
			parcours.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", parcours);
			assertEquals("composant type", EOParcours.class, parcours.getClass());
			assertEquals(1, EOParcours.fetchAllSco_Parcourses(editingContext).count());
			assertEquals(typeParcours, parcours.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, parcours.persIdCreation());
			assertNotNull(parcours.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantDiplome() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EODiplome diplome = EOComposantFactory.createComposant(EODiplome.class, ctePersId, editingContext,2014, null);
			diplome.setCode("code");
			diplome.setLibelle("libelle");
			diplome.setId(1);
			diplome.setValidateur(new CodeValidateurSaisieObligatoire());
			diplome.setStatutFormationRelationship(statutFormationProduction);
			diplome.setGradeUniversitaireRelationship(gradeUniversitaireMaster);
			diplome.setTypeFormationRelationship(typeFormationMaster);
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", diplome);
			assertEquals("composant type", EODiplome.class, diplome.getClass());
			assertEquals(1, EODiplome.fetchAllSco_Diplomes(editingContext).count());
			assertEquals(typeDiplome, diplome.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, diplome.persIdCreation());
			assertNull(diplome.composantInfoVersion());
			assertEquals(0, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createComposantVersionDiplome() {

		try {
			// Arrange
			// Assert ready to test
			// Act
			EOVersionDiplome versionDiplome = EOComposantFactory.createComposant(EOVersionDiplome.class, ctePersId, editingContext,2014, null);
			versionDiplome.setCode("code");
			versionDiplome.setLibelle("libelle");
			versionDiplome.setId(1);
			versionDiplome.setAnnee(0);
			versionDiplome.setValidateur(new CodeValidateurSaisieObligatoire());
			editingContext.saveChanges();

			// Assert
			assertNotNull("composant is null", versionDiplome);
			assertEquals("composant type", EOVersionDiplome.class, versionDiplome.getClass());
			assertEquals(1, EOVersionDiplome.fetchAllSco_VersionDiplomes(editingContext).count());
			assertEquals(typeVersionDiplome, versionDiplome.typeComposant());
			assertEquals(1, EOComposant.fetchAllSco_Composants(editingContext).count());
			assertEquals(ctePersId, versionDiplome.persIdCreation());
			assertNotNull(versionDiplome.composantInfoVersion());
			assertEquals(1, EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext).count());
		} catch (InstantiationException e) {
			fail("InstantiationException");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail("IllegalAccessException");
			e.printStackTrace();
		}
	}

	@Test
	public void test_createDiplomeIntermediaire() {
		// Arrange
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, "diplome", "diplome", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, 2014);
		diplome.setId(1);
		
		// Assert ready to test
		// Act
		EODiplomeIntermediaire diplomeIntermediaire = EOComposantFactory.createDiplomeIntermediaire(diplome);
		diplomeIntermediaire.setLibelle("diplome intermediaire");
		editingContext.saveChanges();
		// Assert
		
		assertEquals(diplome, diplomeIntermediaire.diplome());
		assertTrue(diplome.diplomesIntermediaires().contains(diplomeIntermediaire));
	}

	// Private helpers
	/**
	 * Initialise le contexte
	 */
	private void initialiserContexte() {
		// initialiser le contexte
		EOModeAP modePresentiel = new EOModeAP();
		modePresentiel.setId(1);
		modePresentiel.setCode("PRES");
		modePresentiel.setLibelle("Présentiel");
		editingContext.insertSavedObject(modePresentiel);

	}

}
