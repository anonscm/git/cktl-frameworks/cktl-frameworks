package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

import com.wounit.annotations.Dummy;

import er.extensions.eof.ERXQ;

public class EOTypeAPTest extends EOScolTestCase {

    @Dummy
    private EOTypeAP eoTypeAP;

    private static final String expectedCode = "unCode";
    private static final String expectedLibelle = "unLibelle";

    @Before
    public void setUp() {
    	super.setUp();
    	eoTypeAP = new EOTypeAP();
    	eoTypeAP.setCode(expectedCode);
    	eoTypeAP.setLibelle(expectedLibelle);
    	eoTypeAP.setId(1);
    }

  
    @Test
    public void test_insert() {
		// Arrange
		editingContext.insertSavedObject(eoTypeAP);
	
		// Assert
		EOTypeAP insertedTypeAP = EOTypeAP.fetchSco_TypeAP(editingContext, ERXQ.equals(EOTypeAP.CODE_KEY, eoTypeAP.code()));
		assertNotNull(insertedTypeAP);
		assertEquals(expectedCode, insertedTypeAP.code());
		assertEquals(expectedLibelle, insertedTypeAP.libelle());
    }

    @Test
    public void test_validateCode_nouveauCode(){
		// Arrange
		editingContext.insertSavedObject(eoTypeAP);
	
		String newCode = "unAutreCode";
		EOTypeAP target = new EOTypeAP();
		editingContext.insertObject(target);
	
		// Act
		String actualCode = (String) target.validateCode(newCode);
	
		// Assert
		assertEquals(newCode, actualCode);
    }
    
    @Test
    public void test_validateCode_memeTypeAP(){
		// Arrange
		editingContext.insertSavedObject(eoTypeAP);
		// Act
		String actualCode = (String) eoTypeAP.validateCode(eoTypeAP.code());
	
		// Assert
		assertEquals(expectedCode, actualCode);
    }

    @Test
    public void test_validateCode_codeExistant() throws Exception {
		// Arrange
		editingContext.insertSavedObject(eoTypeAP);
	
		// Assert ready to test
		EOTypeAP insertedTypeAP = EOTypeAP.fetchSco_TypeAP(editingContext, ERXQ.equals(EOTypeAP.CODE_KEY, eoTypeAP.code()));
		assertNotNull(insertedTypeAP);
	
		String newCode = expectedCode;
		EOTypeAP target = new EOTypeAP();
		editingContext.insertObject(target);
		try {
		    // Act
		    target.validateCode(newCode);
		    fail("Expected exception was not thrown!");
	
		} catch (Exception e) {
		    // Assert
		    //do nothing - the test passes
		}
    }

}