package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.junit.Test;

public class CalculateurSommeEctSPourMaquetteTests extends MockitoScolTests {

	/**
	 * => regroupement 
	 * 	=> ue1 
	 * 		=> ec11 (creditable =1, obligatoire=1) 
	 * 		=> ec12 (creditable =2, obligatoire=1) 
	 * 		=> ec13 (creditable =3, obligatoire=1)
	 */
	@Test
	public void calculeCreditECTS_UE_tousSelectionnes() {
		//Arrange
		Integer childId = 1;
		IRegroupement regroupement = mock(IRegroupement.class);
		IUE ue1 = mock(IUE.class);
		DummyLienComposer regroupementUe1 = spyLienComposer(regroupement, ue1, childId++, true);
		IEC ec11 = mock(IEC.class);		
		mockCreditable(ec11, new BigDecimal(1));
		IEC ec12 = mock(IEC.class);
		mockCreditable(ec12, new BigDecimal(2));
		IEC ec13 = mock(IEC.class);
		mockCreditable(ec13, new BigDecimal(3));
		DummyLienComposer ue1ec11 = spyLienComposer(ue1, ec11, childId++, true);
		DummyLienComposer ue1ec12 = spyLienComposer(ue1, ec12, childId++, true);
		DummyLienComposer ue1ec13 = spyLienComposer(ue1, ec13, childId++, true);
		when(ue1.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue1ec11,ue1ec12, ue1ec13));
		when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(regroupementUe1));
		
		//Act
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());
		calculateurSommeCreditECTS.calcule(regroupementUe1);
		assertThat(regroupementUe1.getCreditECTSCalcule()).isEqualTo(new BigDecimal(6));
	}

	/**
	 *	=> regroupement 
	 * 	=> ue2 
	 * 		=> ec21 (creditable =1, obligatoire=1) 
	 * 		=> ec22 (creditable =2, obligatoire=0) 
	 * 		=> ec23 (creditable =3, obligatoire=1)
	 */
	@Test
	public void calculeCreditECTS_UE_unPasSelectionne() {
	//Arrange
			Integer childId = 1;
			IRegroupement regroupement = mock(IRegroupement.class);
			IUE ue2 = mock(IUE.class);
			DummyLienComposer regroupementUe2 = spyLienComposer(regroupement, ue2, childId++, true);
			IEC ec21 = mock(IEC.class);
			mockCreditable(ec21, new BigDecimal(4));
			IEC ec22 = mock(IEC.class);
			mockCreditable(ec22, new BigDecimal(5));
			IEC ec23 = mock(IEC.class);
			mockCreditable(ec23, new BigDecimal(6));
			DummyLienComposer ue2ec21 = spyLienComposer(ue2, ec21, childId++, true);
			DummyLienComposer ue2ec22 = spyLienComposer(ue2, ec22, childId++, false);
			DummyLienComposer ue2ec23 = spyLienComposer(ue2, ec23, childId++, true);
			when(ue2.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue2ec21,ue2ec22, ue2ec23));
			when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(regroupementUe2));
			
		// cas tests ou tous les enfants de l'UE ne sont pas selectionnes
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());
		calculateurSommeCreditECTS.calcule(regroupementUe2);
		assertThat(regroupementUe2.getCreditECTSCalcule()).isNull();

		mockCreditable(ue2, new BigDecimal(9));
		calculateurSommeCreditECTS.calcule(regroupementUe2);
		assertThat(regroupementUe2.getCreditECTSCalcule()).isEqualTo(new BigDecimal(9));
	}

	/**
	 * semestre 
	 * 	=> parcours
	 * 	 => ue 
	 * 		=> ecA (creditable = 3, obligatoire=1) 
	 * 		=> ecB (creditable = 2, obligatoire=1)
	 */
	@Test
	public void calculeCreditECTS_Parcours() {
		// Arrange
		Integer childId=1;
		IPeriode annee = mock(IPeriode.class);
		IPeriode semestre = mock(IPeriode.class);
		IParcours parcours = mock(IParcours.class);
		IUE ue = mock(IUE.class);
		IEC ecA = mock(IEC.class);
		IEC ecB = mock(IEC.class);
		mockCreditable(ecA, new BigDecimal(3));
		mockCreditable(ecB, new BigDecimal(2));

		// lien annee -semestre
		DummyLienComposer lienAnneeSemestre = spyLienComposer(annee, semestre, childId++, true);
		when(semestre.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienAnneeSemestre));

		// lien semestre - parcours
		DummyLienComposer lienSemestreParcours = spyLienComposer(semestre, parcours, childId++, true);
		when(semestre.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienSemestreParcours));

		// liens parcours - ue
		DummyLienComposer lienParcoursUE = spyLienComposer(parcours, ue, childId++, true);
		when(parcours.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienParcoursUE));

		// liens ue - ec
		DummyLienComposer lienUEECA = spyLienComposer(ue, ecA, childId++, true);
		DummyLienComposer lienUEECB = spyLienComposer(ue, ecB, childId++, true);
		when(ue.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienUEECA, lienUEECB));


		// Assert ready to test

		// Act
		// Assert
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());
		calculateurSommeCreditECTS.calcule(lienAnneeSemestre);
		assertThat(lienParcoursUE.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
		assertThat(lienSemestreParcours.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
		assertThat(lienAnneeSemestre.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));

		
		when(lienSemestreParcours.obligatoire()).thenReturn(false);	  
		calculateurSommeCreditECTS.calcule(lienAnneeSemestre);
		assertThat(lienParcoursUE.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
		assertThat(lienSemestreParcours.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
		assertThat(lienAnneeSemestre.getCreditECTSCalcule()).isNull();

	}

	/**
	 * Semestre => Regroupement => ecA (creditable = 20) => ecB (creditable = 18) => ecC (creditable = 5)
	 */
	@Test
	public void calculeCreditECTS_Regroupement() {
		// Arrange
		Integer childId=1;
		IPeriode semestre = mock(IPeriode.class);
		IRegroupement regroupement = mock(IRegroupement.class);
		when(regroupement.choixECTS()).thenReturn(null);
		when(regroupement.choixMin()).thenReturn(null);
		when(regroupement.choixMax()).thenReturn(null);
		IEC ecA = mock(IEC.class);
		IEC ecB = mock(IEC.class);
		IEC ecC = mock(IEC.class);
		mockCreditable(ecA, new BigDecimal(20));
		mockCreditable(ecB, new BigDecimal(18));
		mockCreditable(ecC, new BigDecimal(5));

		// Lien semestre - regroupement
		DummyLienComposer lienSmtRgp = spyLienComposer(semestre, regroupement, childId++, false);

		// Lien regroupement - ec
		DummyLienComposer lienRgpEcA = spyLienComposer(regroupement, ecA, childId++, false);
		DummyLienComposer lienRgpEcB = spyLienComposer(regroupement, ecB, childId++, false);
		DummyLienComposer lienRgpEcC = spyLienComposer(regroupement, ecC, childId++, false);
		when(lienRgpEcA.obligatoire()).thenReturn(false);
		when(lienRgpEcB.obligatoire()).thenReturn(false);
		when(lienRgpEcC.obligatoire()).thenReturn(false);
		when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienRgpEcA, lienRgpEcB, lienRgpEcC));

		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());

		// Rien n'est obligatoire
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECB obligatoire
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(18));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECB et ECC obligatoire
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		when(lienRgpEcC.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(23));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();
	}

	/**
	 * Semestre => Regroupement (choix ECTS = 40) => ecA (creditable = 20) => ecB (pas de creditable) => ecC (creditable = 5)
	 */
	@Test
	public void calculeCreditECTS_RegroupementChoixEcts() {
		// Arrange
		Integer childId=1;
		IPeriode semestre = mock(IPeriode.class);
		IRegroupement regroupement = mock(IRegroupement.class);
		when(regroupement.choixECTS()).thenReturn(40);
		when(regroupement.choixMin()).thenReturn(null);
		when(regroupement.choixMax()).thenReturn(null);
		IEC ecA = mock(IEC.class);
		IEC ecB = mock(IEC.class);
		IEC ecC = mock(IEC.class);
		mockCreditable(ecA, new BigDecimal(20));
//		mockCreditable(ecB, new BigDecimal(18));
		mockCreditable(ecC, new BigDecimal(5));

		// Lien semestre - regroupement
		DummyLienComposer lienSmtRgp = spyLienComposer(semestre, regroupement, childId++, false);

		// Lien regroupement - ec
		DummyLienComposer lienRgpEcA = spyLienComposer(regroupement, ecA, childId++, false);
		DummyLienComposer lienRgpEcB = spyLienComposer(regroupement, ecB, childId++, false);
		DummyLienComposer lienRgpEcC = spyLienComposer(regroupement, ecC, childId++, false);		
		when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienRgpEcA, lienRgpEcB, lienRgpEcC));

		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());

		// Rien d'obligatoire
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(40));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECB d'obligatoire
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(40));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECB et ECC obligatoire
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		when(lienRgpEcC.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECA, ECB et ECC obligatoire
		when(lienRgpEcA.obligatoire()).thenReturn(true);
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		when(lienRgpEcC.obligatoire()).thenReturn(true);
		when(regroupement.choixECTS()).thenReturn(40);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(25));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECB avec 18 créditable
		mockCreditable(ecB, new BigDecimal(18));
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(43));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();
	}

	/**
	 * Semestre => Regroupement (Choix min = 2) => ecA (creditable = 20) => ecB (creditable = 18) => ecC (creditable = 5)
	 */
	@Test
	public void calculeCreditECTS_RegroupementChoixMinMax() {
		// Arrange
		Integer childId = 1;
		IPeriode semestre = mock(IPeriode.class);
		IRegroupement regroupement = mock(IRegroupement.class);
		when(regroupement.choixECTS()).thenReturn(null);
		when(regroupement.choixMin()).thenReturn(2);
		when(regroupement.choixMax()).thenReturn(null);
		IEC ecA = mock(IEC.class);
		IEC ecB = mock(IEC.class);
		IEC ecC = mock(IEC.class);
		mockCreditable(ecA, new BigDecimal(20));
		mockCreditable(ecB, new BigDecimal(18));
		mockCreditable(ecC, new BigDecimal(5));

		// Lien semestre - regroupement
		DummyLienComposer lienSmtRgp = spyLienComposer(semestre, regroupement, childId++, false);

		// Lien regroupement - ec
		DummyLienComposer lienRgpEcA = spyLienComposer(regroupement, ecA, childId++, false);
		DummyLienComposer lienRgpEcB = spyLienComposer(regroupement, ecB, childId++, false);
		DummyLienComposer lienRgpEcC = spyLienComposer(regroupement, ecC, childId++, false);
		when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienRgpEcA, lienRgpEcB, lienRgpEcC));
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());

		// ECA, ECB et ECC non obligatoire
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(23));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECA obligatoire
		when(lienRgpEcA.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(25));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();

		// ECA et ECB obligatoire
		when(lienRgpEcB.obligatoire()).thenReturn(true);
		calculateurSommeCreditECTS.calcule(lienSmtRgp);
		assertThat(lienSmtRgp.getCreditECTSCalcule()).isEqualTo(new BigDecimal(38));
		assertThat(lienRgpEcA.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcB.getCreditECTSCalcule()).isNull();
		assertThat(lienRgpEcC.getCreditECTSCalcule()).isNull();
	}

}
