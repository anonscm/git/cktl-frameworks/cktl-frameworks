package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;


import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.junit.Test;

public class MaquetteElementProviderTests {

	@Test
  public void getChargeEnseignement() {
	  // Arrange
		MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);
		IChargeEnseignement chargeEnseignement = mock(IChargeEnseignement.class);
		IAP ap = mock(IAP.class); 
		when(ap.chargeEnseignementTheorique()).thenReturn(chargeEnseignement);
	  
		// Assert
		assertThat(maquetteElementProvider.getChargeEnseignement(ap)).isEqualTo(chargeEnseignement);		
  }
	
	
	@Test
  public void getChildAvecLien() {
	  // Arrange
		MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);
		ILien lien = mock(ILien.class);
		IComposant child = mock(IComposant.class);
		when(lien.child()).thenReturn(child);
	  
	  // Assert
		assertThat(maquetteElementProvider.getChildAvecLien(lien)).isEqualTo(child);		
  }
	
	@Test
  public void getCreditablePourComposant() {
	// Arrange
			MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);			
			IComposant composant = mock(IComposant.class);
			ICreditable creditable = mock(ICreditable.class);
			when(composant.creditable()).thenReturn(creditable);
		  
		  // Assert
			assertThat(maquetteElementProvider.getCreditablePourComposant(composant)).isEqualTo(creditable);		
  }
	
	
	@Test
  public void getCreditablePourLien() {
		MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);			
		ILienComposer lien = mock(ILienComposer.class);
		ICreditable creditable = mock(ICreditable.class);
		when(lien.creditable()).thenReturn(creditable);
	  
	  // Assert
		assertThat(maquetteElementProvider.getCreditablePourLien(lien)).isEqualTo(creditable);		
  }
	
	
	@Test
  public void getLiensAvecComposantParent() {
		 // Arrange
		MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);
		IComposant parent = mock(IComposant.class);
		ILien lien1 = mock(ILien.class);
		ILien lien2 = mock(ILien.class);
		ILien lien3 = mock(ILien.class);
		List<ILien> liens = Arrays.asList(lien1,lien2, lien3);
		doReturn(liens).when(parent).liensParentsWithoutDuplicate();
		
	  
	  // Assert
		assertThat(maquetteElementProvider.getLiensAvecComposantParent(parent)).isEqualTo(liens);		
  }
	
	
	@Test
  public void getParentAvecLien() {
		 // Arrange
		MaquetteElementProvider maquetteElementProvider = new MaquetteElementProvider(null);
		ILien lien = mock(ILien.class);
		IComposant parent = mock(IComposant.class);
		when(lien.parent()).thenReturn(parent);
	  
	  // Assert
		assertThat(maquetteElementProvider.getParentAvecLien(lien)).isEqualTo(parent);		
  }
	
}
