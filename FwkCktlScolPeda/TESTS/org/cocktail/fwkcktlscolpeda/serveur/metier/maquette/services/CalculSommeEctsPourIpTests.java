package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.google.common.collect.Lists;

@RunWith(value = MockitoJUnitRunner.class)
public class CalculSommeEctsPourIpTests extends MockitoScolTests {

	DummyLienComposer nullSemestre;
	@Mock
	IPeriode semestreX;
	
	DummyLienComposer semestreRegroupement;
	@Mock
	IRegroupement regroupement;

	DummyLienComposer regroupementUe1;
	@Mock
	IUE ue1;
	
	DummyLienComposer ue1ec11;
	@Mock
	IEC ec11;
	
	DummyLienComposer ue1ec12;
	@Mock
	IEC ec12;
	
	DummyLienComposer ue1ec13;
	
	@Mock
	IEC ec13;

	DummyLienComposer regroupementUe2;
	@Mock
	IUE ue2;		
	DummyLienComposer ue2ec21;
	@Mock
	IEC ec21;	
	DummyLienComposer ue2ec22;
	@Mock
	IEC ec22;
	DummyLienComposer ue2ec23;
	@Mock
	IEC ec23;

	DummyLienComposer regroupementUe3;
	@Mock
	IUE ue3;
	DummyLienComposer ue3ec31;
	@Mock
	IEC ec31;
	
	DummyLienComposer ue3ec32;
	@Mock
	IEC ec32;
	DummyLienComposer ue3ec33;
	@Mock
	IEC ec33;
	
	DummyLienComposer regroupementUe4 = new DummyLienComposer();
	@Mock
	IUE ue4;
	DummyLienComposer ue4ec41;
	@Mock
	IEC ec41;
	DummyLienComposer ue4ec42;
	@Mock
	IEC ec42;
	DummyLienComposer ue4ec43;
	@Mock
	IEC ec43;
	
	DummyLienComposer regroupementUe5 = new DummyLienComposer();
	@Mock
	IUE ue5;
	DummyLienComposer ue5ec51;
	@Mock
	IEC ec51;
	DummyLienComposer ue5ec52;
	@Mock
	IEC ec52;
	DummyLienComposer ue5ec53;
	@Mock
	IEC ec53;
	
	DummyLienComposer regroupementUe6 = new DummyLienComposer();
	@Mock
	IUE ue6;
	
	@Mock
	private IInscriptionPedagogique ip;
	@Mock
	private IInscriptionPedagogiqueElement ipElementRegroupement;

	// Inscriptions pedagogiques pour l'UE1
	@Mock
	private IInscriptionPedagogiqueElement ipElementUE1;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc11;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc12;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc13;

	// Inscriptions pedagogiques pour l'UE2
	@Mock
	private IInscriptionPedagogiqueElement ipElementUE2;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc21;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc23;

	// Inscriptions pedagogique pour l'UE4
	@Mock
	private IInscriptionPedagogiqueElement ipElementUE4;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc41;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc42;

	// Inscriptions pedagogique pour l'UE5
	@Mock
	private IInscriptionPedagogiqueElement ipElementUE5;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc51;
	@Mock
	private IInscriptionPedagogiqueElement ipElementEc52;

	@Mock
	private IInscriptionPedagogiqueElement ipElementUE6;

	@Before
	public void setUp() throws Exception {
		Integer childId=1;
		// cas de test ue1 tous les EC cochés pour inscription
		regroupementUe1 = spyLienComposer(regroupement, ue1, childId++, false);
		ue1ec11 =spyLienComposer(ue1, ec11, childId++, false);
		ue1ec12 =spyLienComposer(ue1, ec12, childId++, false);
		ue1ec13 =spyLienComposer(ue1, ec13, childId++, false);		
		when(ue1.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue1ec11, ue1ec12, ue1ec13));
		mockCreditable(ec11, new BigDecimal(1));
		mockCreditable(ec12, new BigDecimal(2));
		mockCreditable(ec13, new BigDecimal(3));
		
		when(ipElementUE1.toLien()).thenReturn(regroupementUe1);
		when(ipElementEc11.toLien()).thenReturn(ue1ec11);
		when(ipElementEc12.toLien()).thenReturn(ue1ec12);
		when(ipElementEc13.toLien()).thenReturn(ue1ec13);
		
		// cas de test ue2 2/3 EC cochés pour inscription
		regroupementUe2 = spyLienComposer(regroupement, ue2, childId++, false);
		ue2ec21 = spyLienComposer(ue2, ec21, childId++, false);
		ue2ec22 = spyLienComposer(ue2, ec22, childId++, false);
		ue2ec23 = spyLienComposer(ue2, ec23, childId++, false);
		when(ue2.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue2ec21, ue2ec22, ue2ec23));
		mockCreditable(ec21, new BigDecimal(4));
		mockCreditable(ec22, new BigDecimal(5));
		mockCreditable(ec23, new BigDecimal(6));
		when(ipElementUE2.toLien()).thenReturn(regroupementUe2);
		when(ipElementEc21.toLien()).thenReturn(ue2ec21);
		when(ipElementEc23.toLien()).thenReturn(ue2ec23);

		// cas de test ue3 aucun EC coché pour inscription		
		regroupementUe3 = spyLienComposer(regroupement, ue3, childId++, false);
		mockCreditable(ue3, new BigDecimal(8));
		ue3ec31 = spyLienComposer(ue3, ec31, childId++, false);
		ue3ec32 = spyLienComposer(ue3, ec32, childId++, false);
		ue3ec33 = spyLienComposer(ue3, ec33, childId++, false);		
		when(ue3.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue3ec31, ue3ec32, ue3ec33));
		mockCreditable(ec31, new BigDecimal(4));
		mockCreditable(ec32, new BigDecimal(5));
		mockCreditable(ec33, new BigDecimal(6));

		// cas de test ue4 un seul des ECs contenus dans l'UE a un creditable non null
		regroupementUe4 = spyLienComposer(regroupement, ue4, childId++, false);
		mockCreditable(ue4, null);
		ue4ec41 = spyLienComposer(ue4, ec41, childId++, false);
		ue4ec42 = spyLienComposer(ue4, ec42, childId++, false);
		ue4ec43 = spyLienComposer(ue4, ec43, childId++, false);		
		when(ue4.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue4ec41, ue4ec42, ue4ec43));
		mockCreditable(ec41, null);
		mockCreditable(ec42, new BigDecimal(5));
		mockCreditable(ec43, null);		
		when(ipElementUE4.toLien()).thenReturn(regroupementUe4);
		when(ipElementEc41.toLien()).thenReturn(ue4ec41);
		when(ipElementEc42.toLien()).thenReturn(ue4ec42);

		// cas de test ue5 certains EC cochés pour inscription n'ont pas de credit ECTS
		regroupementUe5 = spyLienComposer(regroupement, ue5, childId++, false);
		mockCreditable(ue5, new BigDecimal(4));
		ue5ec51 = spyLienComposer(ue5, ec51, childId++, false);
		ue5ec52 = spyLienComposer(ue5, ec52, childId++, false);
		ue5ec53 = spyLienComposer(ue5, ec53, childId++, false);	
		when(ue5.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(ue5ec51, ue5ec52, ue5ec53));
		mockCreditable(ec51, new BigDecimal(2));
		mockCreditable(ec52, null);
		mockCreditable(ec53, null);		
		when(ipElementUE5.toLien()).thenReturn(regroupementUe5);
		when(ipElementEc51.toLien()).thenReturn(ue5ec51);
		when(ipElementEc52.toLien()).thenReturn(ue5ec52);

		// cas de test ue6 sans enfants
		regroupementUe6 = spyLienComposer(regroupement, ue6, childId++, false);
		mockCreditable(ue6, new BigDecimal(2));
		when(ipElementUE6.toLien()).thenReturn(regroupementUe6);

		//semestre et regroupement
		when(regroupement.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(regroupementUe1, regroupementUe2, regroupementUe3, regroupementUe4, regroupementUe5));
			  
		semestreRegroupement = spyLienComposer(semestreX, regroupement, childId++, false);
		when(ipElementRegroupement.toLien()).thenReturn(semestreRegroupement);		
		when(semestreX.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(semestreRegroupement));
		
		nullSemestre = spyLienComposer(null, semestreX, childId++, false);
		when(nullSemestre.child()).thenReturn(semestreX);					
		
		// liste des ips
		when(ip.toInscriptionPedagogiqueElements()).thenAnswer(
		    getAnswerForElements(ipElementRegroupement, ipElementUE1, ipElementEc11, ipElementEc12, ipElementEc13, ipElementUE2, ipElementEc21, ipElementEc23,
		        ipElementUE4, ipElementEc41, ipElementEc42, ipElementUE5, ipElementEc51, ipElementEc52, ipElementUE6));

	}

	/**
	 * Lorsque tous les EC contenus dans une UE sont obligatoires, le nombre d'ECTS de l'UE est égal à la somme des ECTS de tous les EC.
	 */
	@Test
	public void calcule_TousSelectionnes() {
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe1);

		assertThat(regroupementUe1.getCreditECTSCalcule()).isEqualTo(new BigDecimal(6));
	}

	/**
	 * Lorsque tous les EC contenus dans une UE ont des ECTS non nuls et supérieurs à 0, le nombre d'ECTS de l'UE est égal à la somme des ECTS des EC choisis (et
	 * non à l'ECTS de l'UE défini dans la maquette).
	 * regroupement
	 * 	ue2
	 * 		ec21 credit = 4 sélectionné
	 * 		ec22 credit = 5 
	 * 		ec23 credit = 6 sélectionné
	 */
	@Test
	public void calcule_CertainsSelectionnes() {

		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe2);
		assertThat(regroupementUe2.getCreditECTSCalcule()).isEqualTo(new BigDecimal(10));

		// on change les ip selectionnes pour verifier que le calcul est correctement reinitialise
		when(ip.toInscriptionPedagogiqueElements()).thenAnswer(getAnswerForElements());
		regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe2);
		assertThat(regroupementUe2.getCreditECTSCalcule()).isNull();
	}

	/**
	 * Idem au cas ci-dessus, hormis qu'aucun EC n'est choisi, dans ce cas l'ECTS de l'UE est vide (et non à l'ECTS de l'UE défini dans la maquette).
	 */
	@Test
	public void calcule_AucunSelectionne() {

		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe3);

		assertThat(regroupementUe3.getCreditECTSCalcule()).isNull();
	}

	/**
	 * Lorsque tous les EC contenus dans une UE n'ont d'ECTS déterminés, le nombre d'ECTS de l'UE est égal à l'ECTS de l'UE de la maquette.
	 */
	@Test
	public void calcul_AucunCreditECTS() {
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe4);
		assertThat(regroupementUe4.getCreditECTSCalcule()).isEqualTo(new BigDecimal(5));
	}

	/**
	 * Lorsqu'au moins un des EC contenus dans une UE n'a pas d'ECTS, le nombre d'ECTS de l'UE est égal à l'ECTS de l'UE de la maquette quel que soit le choix
	 * réalisé sur les EC.
	 */
	@Test
	public void calcul_UnSansCreditECTS() {
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe5);
	//TODO vérifier que le changement de comportement est OK et mettre à jour le Wiki en conséquence
		assertThat(regroupementUe5.getCreditECTSCalcule()).isEqualTo(new BigDecimal(4));
	}

	/**
	 * Lorsqu'une UE n'a pas de composants enfants (EC par exemple), l'ECTS de l'UE est égal à l'ECTS de la maquette.
	 */
	@Test
	public void calcul_UESansEnfants() {
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(regroupementUe6);

		assertThat(regroupementUe6.getCreditECTSCalcule()).isEqualTo(new BigDecimal(2));
	}

	/**
	 * Calcul du credit ECTS sur le regroupement
	 */
	@Test
	public void calcul_Regroupement() {
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(ip);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(semestreRegroupement);
//TODO vérifier que le changement de comportement est OK et mettre à jour le Wiki en conséquence
		assertThat(semestreRegroupement.getCreditECTSCalcule()).isEqualTo(new BigDecimal(25));
	}

	/**
	 * s1
	 * 	=> p1
	 * 		=>r1
	 * 			=> ueA (creditable 1)
	 * 			=> ueB (creditable 2)
	 * 		=>r2
	 * 			=> ueA (creditable 1)
	 * 			=> ueC (creditable 3)
 	
	 */
	@Mock
	IPeriode s1;
	@Mock
	IParcours p1;
	@Mock 
	IRegroupement r1;
	@Mock
	IRegroupement r2;
	@Mock
	IUE ueA;
	@Mock
	IUE ueB;
	@Mock
	IUE ueC;
	@Test
  public void calcul_composant_mutualise() {
	  // Arrange
		DummyLienComposer liens1p1= spyLienComposer(s1, p1, 1, false);
		when(s1.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(liens1p1));
		DummyLienComposer lienp1r1= spyLienComposer(p1, r1, 2, false);
		DummyLienComposer lienp1r2= spyLienComposer(p1, r2, 3, false);
		when(p1.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienp1r1, lienp1r2));
		DummyLienComposer lienr1ueA= spyLienComposer(r1, ueA, 4, false);
		DummyLienComposer lienr1ueB= spyLienComposer(r1, ueB, 5, false);
		when(r1.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienr1ueA, lienr1ueB));
		DummyLienComposer lienr2ueA= spyLienComposer(r2, ueA, 4, false);
		DummyLienComposer lienr2ueC= spyLienComposer(r2, ueC, 6, false);
		when(r2.liensParentsWithoutDuplicate()).thenAnswer(getAnswerForLien(lienr2ueA, lienr2ueC));
		mockCreditable(ueA, new BigDecimal(1));
		mockCreditable(ueB, new BigDecimal(2));
		mockCreditable(ueC, new BigDecimal(3));
		IInscriptionPedagogiqueElement ipr1ueA = mockIPElement(lienr1ueA);
		IInscriptionPedagogiqueElement ipr1ueB = mockIPElement(lienr1ueB);
		IInscriptionPedagogiqueElement ipr2ueA = mockIPElement(lienr2ueA);
		IInscriptionPedagogiqueElement ipr2ueC = mockIPElement(lienr2ueC);
		IInscriptionPedagogiqueElement ipp1r1 = mockIPElement(lienp1r1);
		IInscriptionPedagogiqueElement ipp1r2 = mockIPElement(lienp1r2);
		IInscriptionPedagogiqueElement ips1p1 = mockIPElement(liens1p1);			
		IInscriptionPedagogique inscriptionPedagogique = mock(IInscriptionPedagogique.class);
		when(inscriptionPedagogique.toInscriptionPedagogiqueElements()).thenAnswer(
		    getAnswerForElements(ips1p1, ipp1r1, ipp1r2, ipr1ueA, ipr1ueB, ipr2ueA, ipr2ueC));
	  // Assert ready to test
	  // Act 
		RegleCalculCreditECTSPourIP regleCalculService = new RegleCalculCreditECTSPourIP(inscriptionPedagogique);
		regleCalculService.setMaquetteData(null);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(regleCalculService);
		calculateurSommeCreditECTS.calcule(liens1p1);
	  // Assert
		assertThat(lienp1r1.getCreditECTSCalcule()).isEqualTo(new BigDecimal(3));
		assertThat(lienp1r2.getCreditECTSCalcule()).isEqualTo(new BigDecimal(3));
		assertThat(liens1p1.getCreditECTSCalcule()).isEqualTo(new BigDecimal(6));
  }
	
	/**
	 * - S1 : semestre
	 *   - P1 : parcours 1
	 *     - R1 : regroupement 1
	 *       - UE1
	 *         - EC 1 
	 *         - EC 2
	 *     - R2 : regroupement 2
	 *       - UE1
	 *         - EC 1 
	 *         - EC 2
	 */
	@Test
	public void testLienConsultationComposantsMutualises() {
		IPeriode semestre1 = mockComposant(IPeriode.class, 1, null);
		IParcours parcours1 = mockComposant(IParcours.class, 2, null);
		IRegroupement regroupement1 = mockComposant(IRegroupement.class, 3, null);
		IRegroupement regroupement2 = mockComposant(IRegroupement.class, 4, null);
		IUE ue1 = mockComposant(IUE.class, 5, null);
		BigDecimal cinq = new BigDecimal(5);
		IEC ec1 = mockComposant(IEC.class, 6, cinq);
		IEC ec2 = mockComposant(IEC.class, 7, cinq);
		ILienConsultation lienSemestre1Parcours1 = spyLienConsultation(1, null, semestre1, parcours1);
		ILienConsultation lienParcours1Regroupement1 = spyLienConsultation(2, lienSemestre1Parcours1, parcours1, regroupement1);
		ILienConsultation lienParcours1Regroupement2 = spyLienConsultation(3, lienSemestre1Parcours1, parcours1, regroupement2);
		ILienConsultation lienRegroupement1Ue1 = spyLienConsultation(4, lienParcours1Regroupement1, regroupement1, ue1);
		ILienConsultation lienRegroupement2Ue1 = spyLienConsultation(5, lienParcours1Regroupement2, regroupement2, ue1);
		ILienConsultation lienUe1Ec1SousRegroupement1 = spyLienConsultation(6, lienRegroupement1Ue1, ue1, ec1);
		ILienConsultation lienUe1Ec2SousRegroupement1 = spyLienConsultation(7, lienRegroupement1Ue1, ue1, ec2);
		ILienConsultation lienUe1Ec1SousRegroupement2 = spyLienConsultation(6, lienRegroupement2Ue1, ue1, ec1);
		ILienConsultation lienUe1Ec2SousRegroupement2 = spyLienConsultation(7, lienRegroupement2Ue1, ue1, ec2);

		
		List<ILien> liensIp = Lists.newArrayList();
		liensIp.add(lienSemestre1Parcours1);
		liensIp.add(lienParcours1Regroupement1);
		liensIp.add(lienRegroupement1Ue1);
		liensIp.add(lienUe1Ec1SousRegroupement1);
		liensIp.add(lienUe1Ec2SousRegroupement1);
		liensIp.add(lienParcours1Regroupement2);
		liensIp.add(lienRegroupement2Ue1);
		liensIp.add(lienUe1Ec1SousRegroupement2);

		MaquetteData maquetteData = mock(MaquetteData.class);
		when(maquetteData.getComposant(semestre1.id())).thenReturn(semestre1);
		when(maquetteData.getComposant(parcours1.id())).thenReturn(parcours1);
		when(maquetteData.getComposant(regroupement1.id())).thenReturn(regroupement1);
		when(maquetteData.getComposant(regroupement2.id())).thenReturn(regroupement2);
		when(maquetteData.getComposant(ue1.id())).thenReturn(ue1);
		when(maquetteData.getComposant(ec1.id())).thenReturn(ec1);
		when(maquetteData.getComposant(ec2.id())).thenReturn(ec2);
		
		
		when(maquetteData.getLiensByParentId(semestre1.id())).thenReturn(getListeLiens(lienSemestre1Parcours1));
		when(maquetteData.getLiensByParentId(parcours1.id())).thenReturn(getListeLiens(lienParcours1Regroupement1, lienParcours1Regroupement2));
		when(maquetteData.getLiensByParentId(regroupement1.id())).thenReturn(getListeLiens(lienRegroupement1Ue1));
		when(maquetteData.getLiensByParentId(regroupement2.id())).thenReturn(getListeLiens(lienRegroupement2Ue1));
		when(maquetteData.getLiensByParentId(ue1.id())).thenReturn(getListeLiens(lienUe1Ec1SousRegroupement1, lienUe1Ec2SousRegroupement1, lienUe1Ec1SousRegroupement2, lienUe1Ec2SousRegroupement2));
		when(maquetteData.getLiensByParentId(ec1.id())).thenReturn(new ArrayList<ILien>());
		when(maquetteData.getLiensByParentId(ec2.id())).thenReturn(new ArrayList<ILien>());

		ICreditable creditableEc1 = ec1.creditable();
		when(maquetteData.getCreditableByComposantId(ec1.id())).thenReturn(creditableEc1);
		ICreditable creditableEc2 = ec2.creditable();
		when(maquetteData.getCreditableByComposantId(ec2.id())).thenReturn(creditableEc2);

		
		RegleCalculCreditECTSPourIP regle = new RegleCalculCreditECTSPourIP();
		regle.setLiensInscriptionPedagogique(liensIp);
		regle.setMaquetteData(maquetteData);
		CalculateurSommeCreditECTSBase calculateurEcts = new CalculateurSommeCreditECTSBase(regle, maquetteData);
		calculateurEcts.calcule(lienSemestre1Parcours1);
		BigDecimal dix = new BigDecimal(10);
		assertThat(lienSemestre1Parcours1.getCreditECTSCalcule()).isEqualTo(dix);
	}
	
	private <C extends IComposant> C mockComposant(Class<? extends C> typeComposant, Integer id, BigDecimal creditEcts) {
		C composant = mock(typeComposant);
		when(composant.id()).thenReturn(id);
		mockCreditable(composant, creditEcts);
		return composant;
	}
	
	private List<ILien> getListeLiens(ILien... liens) {
		return Arrays.asList(liens);
	}
	
	private ILienConsultation spyLienConsultation(Integer idLien, ILienConsultation lienParent, IComposant composantParent, IComposant composantChild) {
		ILienConsultation lien = Mockito.spy(new DummyLienConsultation());
		when(lien.id()).thenReturn(idLien);
		Integer parentId = composantParent.id();
		Integer childId = composantChild.id();
		when(lien.parentId()).thenReturn(parentId);
		when(lien.childId()).thenReturn(childId);
		String idLienContexte = null;
		if (lienParent == null) {
			idLienContexte = ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR + idLien;
		} else {
			idLienContexte = lienParent.idLienContexte() + ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR + idLien;
			lienParent.getChildren().add(lien);
		}
		when(lien.idLienContexte()).thenReturn(idLienContexte);
		return lien;
	}
	
	private IInscriptionPedagogiqueElement mockIPElement(ILien lien) {
		IInscriptionPedagogiqueElement ipElement = mock(IInscriptionPedagogiqueElement.class);
		when(ipElement.toLien()).thenReturn(lien);
	  return ipElement;
  }

	// Private Helpers
	private Answer<List<IInscriptionPedagogiqueElement>> getAnswerForElements(IInscriptionPedagogiqueElement... elements) {
		final List<IInscriptionPedagogiqueElement> listeElements = new ArrayList<IInscriptionPedagogiqueElement>(Arrays.asList(elements));
		Answer<List<IInscriptionPedagogiqueElement>> answer = new Answer<List<IInscriptionPedagogiqueElement>>() {

			public List<IInscriptionPedagogiqueElement> answer(InvocationOnMock invocation) throws Throwable {
				return listeElements;
			}
		};
		return answer;
	}
	
	public static class DummyLienConsultation implements ILienConsultation {

		private BigDecimal creditECTSCalcule;
		private List<ILienConsultation> children = new ArrayList<ILienConsultation>();
		
		public Integer choixECTS() {
			// TODO Auto-generated method stub
			return null;
		}

		public ICreditable creditable() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean obligatoire() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setObligatoire(Boolean value) {
			// TODO Auto-generated method stub
			
		}

		public BigDecimal coefficient() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setCoefficient(BigDecimal coeff) {
			// TODO Auto-generated method stub
			
		}

		public IComposant child() {
			// TODO Auto-generated method stub
			return null;
		}

		public IComposant parent() {
			// TODO Auto-generated method stub
			return null;
		}

		public BigDecimal getCreditECTSCalcule() {
			return creditECTSCalcule;
		}

		public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
			this.creditECTSCalcule = creditECTSCalcule;
		}

		public Boolean isNbCreditsEctsOk() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer getSommeMinutesEnseigneesCalculee() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
			// TODO Auto-generated method stub
			
		}

		public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeMinutesEnseignesCalculees, EOTypeAP typeAP) {
			// TODO Auto-generated method stub
			
		}

		public Integer sommeMinutesEnseigneesAPTD() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
			// TODO Auto-generated method stub
			
		}

		public Integer sommeMinutesEnseigneesAPTP() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
			// TODO Auto-generated method stub
			
		}

		public Integer sommeMinutesEnseigneesAPCours() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
			// TODO Auto-generated method stub
			
		}

		public Integer id() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isParentSemestre() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer childId() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer parentId() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer ordre() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setOrdre(Integer value) {
			// TODO Auto-generated method stub
			
		}

		public void assignTo(ILien nouveauLien) {
			// TODO Auto-generated method stub
			
		}

		public Integer idDiplome() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer idLien() {
			// TODO Auto-generated method stub
			return null;
		}

		public String idLienContexte() {
			// TODO Auto-generated method stub
			return null;
		}

		public String idLienContexteParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer idLienParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer idLienRoot() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer idVersionDiplome() {
			// TODO Auto-generated method stub
			return null;
		}

		public String llComposantChild() {
			// TODO Auto-generated method stub
			return null;
		}

		public String llComposantParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public String libelle() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer niveau() {
			// TODO Auto-generated method stub
			return null;
		}

		public String typeComposantNomChild() {
			// TODO Auto-generated method stub
			return null;
		}

		public String typeComposantNomParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public List<ILienConsultation> getChildren() {
			return children;
		}

		public void setChildren(List<ILienConsultation> children) {
			this.children = children;
		}

		public ILienConsultation getParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setParent(ILienConsultation parent) {
			// TODO Auto-generated method stub
			
		}

		public Boolean isChildParcours() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildUE() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildEC() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildRegroupement() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildAE() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildAP() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChildPeriode() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer typePeriodeId() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChoixMinMax() {
			// TODO Auto-generated method stub
			return null;
		}

		public Boolean isChoixEcts() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer choixMax() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer choixMin() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public String toString() {
			return "IdLienContexte : " + idLienContexte() + " - ChildId : " + childId(); 
		}
		
	}
	
	
}
