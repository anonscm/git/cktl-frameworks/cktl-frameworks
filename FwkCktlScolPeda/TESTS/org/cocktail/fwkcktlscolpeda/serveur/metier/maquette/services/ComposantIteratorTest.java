package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.DiplomeSimpleContainer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.TestsInitializer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.FwkCktlScolPedaTestUtils.DummySequence;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

public class ComposantIteratorTest extends EOScolTestCase {

    private ComposantIterator composantIterator;
    private DiplomeSimpleContainer diplome;
    
    @Before
    public void setUp() {
        super.setUp();
        DummySequence sequenceLiens = new DummySequence();
        TestsInitializer testsInitiator = new TestsInitializer(editingContext);
        diplome = EOTestsHelper.creerDiplomeSimple(testsInitiator, sequenceLiens, 2013);
        List<ILien> tousLienArbre = Lists.newArrayList(
                diplome.getLienDiplomeVersionDiplome(),
                diplome.getLienVersionDiplomeL1(),
                diplome.getLienVersionDiplomeL2(),
                diplome.getLienL1Semestre2(),
                diplome.getLienL1Semestre1(),
                diplome.getLienSemestre1Ec1(),
                diplome.getLienSemestre1Ec2(),
                diplome.getLienSemestre1Ec3());
        composantIterator = ComposantIterator.branchIterator(tousLienArbre, diplome.getLienSemestre1Ec1());
    }
    
    @Test
    public void testBranchIterator() {
        assertThat(composantIterator.hasNext());
        assertThat(composantIterator.next()).isEqualTo(diplome.getLienL1Semestre1());
        assertThat(composantIterator.hasNext());
        assertThat(composantIterator.next()).isEqualTo(diplome.getLienVersionDiplomeL1());
        assertThat(composantIterator.hasNext());
        assertThat(composantIterator.next()).isEqualTo(diplome.getLienDiplomeVersionDiplome());
        assertThat(!composantIterator.hasNext());
    }
    
}
