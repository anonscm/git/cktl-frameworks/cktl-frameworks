package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;


import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.junit.Test;

/**
 * Teste la classe qui permet de déplacement un composant verticalement
 */
public class AscenseurComposantTests extends MockitoScolTests {

	@Test
	public void monter() {
		// Arrange
		IComposant parent = mock(IComposant.class);
		IComposant enfantA = mock(IComposant.class);
		IComposant enfantB =mock(IComposant.class);
		IComposant enfantC = mock(IComposant.class);
		IComposant enfantD = mock(IComposant.class);
		IComposant enfantE = mock(IComposant.class);
		ILien lienA = creerLien(parent, enfantA, 1);
		ILien lienB = creerLien(parent, enfantB, 2);
		ILien lienC = creerLien(parent, enfantC, 3);
		ILien lienD = creerLien(parent, enfantD, 4);
		ILien lienE = creerLien(parent, enfantE, 5);
		when(parent.liensParents()).thenAnswer(getAnswerForLien(lienA, lienB, lienC, lienD, lienE));
		
		// Assert ready to test
		assertOrdre(lienA, lienB,lienC,lienD, lienE);
		// Act
		// Assert
		AscenseurComposant ascenseurComposant = new AscenseurComposant();
		
		ascenseurComposant.monter(lienD);
		assertOrdre(lienA, lienB,lienD,lienC, lienE);
		
		ascenseurComposant.monter(lienD);
		assertOrdre(lienA, lienD,lienB,lienC, lienE);
		
		ascenseurComposant.monter(lienD);
		assertOrdre(lienD, lienA,lienB,lienC, lienE);
		
		ascenseurComposant.monter(lienD);
		assertOrdre(lienD, lienA,lienB,lienC, lienE);
	}

	@Test
	public void descendre() {
		// Arrange
		IComposant parent = mock(IComposant.class);
		IComposant enfantA = mock(IComposant.class);
		IComposant enfantB =mock(IComposant.class);
		IComposant enfantC = mock(IComposant.class);
		IComposant enfantD = mock(IComposant.class);
		IComposant enfantE = mock(IComposant.class);
		ILien lienA = creerLien(parent, enfantA, 1);
		ILien lienB = creerLien(parent, enfantB, 2);
		ILien lienC = creerLien(parent, enfantC, 3);
		ILien lienD = creerLien(parent, enfantD, 4);
		ILien lienE = creerLien(parent, enfantE, 5);
		when(parent.liensParents()).thenAnswer(getAnswerForLien(lienA, lienB, lienC, lienD, lienE));
		
		// Assert ready to test
		assertOrdre(lienA, lienB,lienC,lienD, lienE);
		// Act
		// Assert
		AscenseurComposant ascenseurComposant = new AscenseurComposant();
		
		ascenseurComposant.descendre(lienC);
		assertOrdre(lienA, lienB,lienD,lienC, lienE);
		
		ascenseurComposant.descendre(lienC);
		assertOrdre(lienA, lienB,lienD,lienE, lienC);
		
		ascenseurComposant.descendre(lienC);
		assertOrdre(lienA, lienB,lienD,lienE, lienC);
	}
	
	private ILien creerLien(IComposant parent, IComposant enfant, Integer ordre) {
		DummyLienComposer lien = spy(new DummyLienComposer());
		doReturn(parent).when(lien).parent();
		doReturn(enfant).when(lien).child();
		lien.setOrdre(ordre);
	  return lien;	 
	}
	
	private void assertOrdre(ILien... liens){
		Integer ordre =1;
		for (ILien lien : liens) {
	    assertThat(lien.ordre()).isEqualTo(ordre++);
    }
		
	}
}
