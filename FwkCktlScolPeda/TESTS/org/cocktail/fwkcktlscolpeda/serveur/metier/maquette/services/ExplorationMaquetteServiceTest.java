package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;


import static org.fest.assertions.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Test;

public class ExplorationMaquetteServiceTest extends EOScolTestCase {

	private Integer year = Calendar.getInstance().get(Calendar.YEAR);
	private EODiplome diplome;
	private EODiplome diplomeSansSpecialite; 
	private EODiplome diplomeMultiParcours;
	private EOLien lienDiplomeVersion;
	private ExplorationMaquetteService explorationMaquetteService;
	private EOVersionDiplome versionDiplome;
	private EOVersionDiplome versionDiplomeSansSpecialite;
	private EOVersionDiplome versionDiplomeMultiParcours;
	private EOLienComposer lienSpecialiteLicence1;
	private EOParcours specialite;
	private EOParcours specialiteDiplomeMultiParcours;
	private EOParcours parcoursSemestre;
	private EOParcours diplomeMultiParcoursParcours1;
	private EOParcours diplomeMultiParcoursParcours2;
	private EOParcours diplomeMultiParcoursParcours3;
	
	@Before
	public void setUp() {
		super.setUp();
		creerStructureDiplome();
		explorationMaquetteService = new ExplorationMaquetteService(editingContext, true);
		
	}


	private void creerStructureDiplome() {

		EOTypeLien composer = EOTestsHelper.creerTypeLien(editingContext, 1, EOTypeLien.TYPECOMPOSER_LBL);
		EOTypeLien versionner = EOTestsHelper.creerTypeLien(editingContext, 2, EOTypeLien.TYPEVERSIONNER_LBL);
		
		EOTypePeriode annee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, "Année");
		EOTypePeriode semestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, "Semestre");
		
		EOTypeComposant typeDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeVersion = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTypeComposant typePeriode = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		EOTypeComposant typeEc = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTypeComposant typeParcours = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
		EOTypeComposant typeRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEREGROUPEMENT_NOM, ITypeComposant.TYPEREGROUPEMENT_NOM);
		
		EOTestsHelper.creerRegleLiaison(editingContext, typeDiplome, typeVersion, versionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeVersion, typeParcours, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeVersion, typePeriode, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typePeriode, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeRegroupement, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeRegroupement, typeParcours, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typePeriode, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typeParcours, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typeEc, composer);
		
		EOStatutFormation statutFormation = EOTestsHelper.creerStatutFormation(editingContext, 1, "S", "STATUT");
		
		EOGradeUniversitaire gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		EOTypeFormation typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
		diplome = EOTestsHelper.creerDiplome(editingContext, 1, "DIP", "DIPLOME", statutFormation, gradeUniversitaireMaster, typeFormationMaster, year);
		
		versionDiplome = EOTestsHelper.creerVersionDiplome(editingContext, 2, "VD1", "Version Diplome 1", year);
		
		lienDiplomeVersion = EOTestsHelper.creerLien(editingContext, 1, diplome, versionDiplome, versionner);

		specialite = EOTestsHelper.creerParcours(editingContext, 10, "SPE1", "Spécialité 1");
		EOTestsHelper.creerLienComposer(editingContext, 10, versionDiplome, specialite, false);
		
		EOPeriode licence1 = EOTestsHelper.creerPeriode(editingContext, 3, "L1", "Licence 1", annee, 1);
		lienSpecialiteLicence1 = EOTestsHelper.creerLienComposer(editingContext, 2, specialite, licence1, false);
		lienSpecialiteLicence1.set_ParentParcoursRelationship(specialite);
		lienSpecialiteLicence1.set_ChildPeriodeRelationship(licence1);
		
		EOPeriode licence2 = EOTestsHelper.creerPeriode(editingContext, 4, "L2", "Licence 2", annee, 2);
		EOTestsHelper.creerLienComposer(editingContext, 3, specialite, licence2, false);
		
		parcoursSemestre = EOTestsHelper.creerParcours(editingContext, 11, "PAR1", "Parcours 1");
		EOTestsHelper.creerLienComposer(editingContext, 9, licence1, parcoursSemestre, false);

		
		EOPeriode semestre1 = EOTestsHelper.creerPeriode(editingContext, 5, "S1", "Semestre 1", semestre, 1);
		EOTestsHelper.creerLienComposer(editingContext, 4, parcoursSemestre, semestre1, true);
		
		EOPeriode semestre2 = EOTestsHelper.creerPeriode(editingContext, 6, "S2", "Semestre 2", semestre, 2);
		EOTestsHelper.creerLienComposer(editingContext, 5, parcoursSemestre, semestre2, false);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, 7, "EC1", "Ec 1");
		EOTestsHelper.creerLienComposer(editingContext, 6, semestre1, ec1, true);
		
		EOEC ec2 = EOTestsHelper.creerEC(editingContext, 8, "EC2", "Ec 2");
		EOTestsHelper.creerLienComposer(editingContext, 7, semestre1, ec2, true);
		
		EOEC ec3 = EOTestsHelper.creerEC(editingContext, 9, "EC3", "Ec 3");
		EOTestsHelper.creerLienComposer(editingContext, 8, semestre1, ec3, false);
		
		diplomeSansSpecialite = EOTestsHelper.creerDiplome(editingContext, 11, "DIPNOSP", "DIPLOME SANS SPECIALITE", statutFormation, gradeUniversitaireMaster, typeFormationMaster, year);
		
		versionDiplomeSansSpecialite = EOTestsHelper.creerVersionDiplome(editingContext, 12, "VD2", "Version Diplome 2", year);
		
		lienDiplomeVersion = EOTestsHelper.creerLien(editingContext, 11, diplomeSansSpecialite, versionDiplomeSansSpecialite, versionner);

		EOLien lienVersionSansSpecialiteLicence1 = EOTestsHelper.creerLienComposer(editingContext, 10, versionDiplome, licence1, false);
		lienVersionSansSpecialiteLicence1.set_ParentVersionDiplomeRelationship(versionDiplomeSansSpecialite);
		lienVersionSansSpecialiteLicence1.set_ChildPeriodeRelationship(licence1);

		// Diplôme avec parcours années imbriqués sous une période
		int id = 30;
		diplomeMultiParcours = EOTestsHelper.creerDiplome(editingContext, id++, "DIPMULTIP", "DIPLOME MULTI PARCOURS", statutFormation, gradeUniversitaireMaster, typeFormationMaster, year);
		versionDiplomeMultiParcours = EOTestsHelper.creerVersionDiplome(editingContext, id++, "VD3", "Version Diplome multi parcours", year);
		EOTestsHelper.creerLien(editingContext, id++, diplomeMultiParcours, versionDiplomeMultiParcours, versionner);
		specialiteDiplomeMultiParcours = EOTestsHelper.creerParcours(editingContext, id++, "SPE1", "Spécialité 1");
		EOTestsHelper.creerLienComposer(editingContext, id++, versionDiplomeMultiParcours, specialiteDiplomeMultiParcours, false);
		EOPeriode periodeAnnee = EOTestsHelper.creerPeriode(editingContext, id++, "A1", "Année 1", annee, 1);
		EOTestsHelper.creerLienComposer(editingContext, id++, specialiteDiplomeMultiParcours, periodeAnnee, false);
		diplomeMultiParcoursParcours1 = EOTestsHelper.creerParcours(editingContext, id++, "PAR1", "Parcours 1");
		diplomeMultiParcoursParcours2 = EOTestsHelper.creerParcours(editingContext, id++, "PAR2", "Parcours 2");
		diplomeMultiParcoursParcours3 = EOTestsHelper.creerParcours(editingContext, id++, "PAR3", "Parcours 3");
		EOTestsHelper.creerLienComposer(editingContext, id++, periodeAnnee, diplomeMultiParcoursParcours1, false);
		EORegroupement regroupement = EOTestsHelper.creerRegroupement(editingContext, "RG1", "Regroupement 1");
		EOTestsHelper.creerLienComposer(editingContext, id++, diplomeMultiParcoursParcours1, regroupement, false);
		EOTestsHelper.creerLienComposer(editingContext, id++, regroupement, diplomeMultiParcoursParcours2, false);
		EOTestsHelper.creerLienComposer(editingContext, id++, periodeAnnee, diplomeMultiParcoursParcours2, false);
		regroupement = EOTestsHelper.creerRegroupement(editingContext, "RG2", "Regroupement 2");
		EOTestsHelper.creerLienComposer(editingContext, id++, diplomeMultiParcoursParcours2, regroupement, false);
		EOTestsHelper.creerLienComposer(editingContext, id++, regroupement, diplomeMultiParcoursParcours3, false);
		periodeAnnee = EOTestsHelper.creerPeriode(editingContext, id++, "A2", "Année 2", annee, 2);
		EOTestsHelper.creerLienComposer(editingContext, id++, specialiteDiplomeMultiParcours, periodeAnnee, false);
		EOTestsHelper.creerLienComposer(editingContext, id++, periodeAnnee, diplomeMultiParcoursParcours3, false);
	}
	
	@Test
	public void testSpecialitesDeDiplomeSurAnnee() {
		List<IParcours> specialites = explorationMaquetteService.getSpecialitesDeVersionDiplome(diplome, year);
		assertThat(specialites.size()).isEqualTo(1);
		assertThat(specialites.get(0).code()).isEqualTo("SPE1");
	}
	
	@Test
	public void testSpecialitesDeVersionDiplome() {
		List<IParcours> specialites = explorationMaquetteService.getSpecialitesDeVersionDiplome(versionDiplome);
		assertThat(specialites.size()).isEqualTo(1);
		assertThat(specialites.get(0).code()).isEqualTo("SPE1");
	}
	
	@Test
	public void testParcoursPourUneAnnee() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourUneAnnee(lienSpecialiteLicence1);
		assertThat(parcours.size()).isEqualTo(1);
		assertThat(parcours.get(0).code()).isEqualTo("PAR1");
	}
	
	@Test
	public void testParcoursPourSpecialiteEtNiveau() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourSpecialiteEtNiveau(specialite, 1);
		assertThat(parcours.size()).isEqualTo(1);
		assertThat(parcours.get(0).code()).isEqualTo("PAR1");
	}
	
	@Test
	public void testParcoursPourDiplomeEtAnnee() {
		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourDiplomeEtAnnee(diplomeSansSpecialite, year, 1);
		assertThat(parcours.size()).isEqualTo(1);
		assertThat(parcours.get(0).code()).isEqualTo("PAR1");
	}

	@Test
	public void testParcoursPourSpecialiteNiveauEtParcours1() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourSpecialiteNiveauEtParcours(diplome, year, specialite, 1, parcoursSemestre);
		assertThat(parcours.size()).isEqualTo(1);
		// L'ordre des parcours retournés est important
		assertThat(parcours.get(0).code()).isEqualTo("PAR1");
	}

	@Test
	public void testParcoursPourSpecialiteNiveauEtParcours2() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourSpecialiteNiveauEtParcours(diplomeMultiParcours, year, specialiteDiplomeMultiParcours, 1, diplomeMultiParcoursParcours3);
		assertThat(parcours.size()).isEqualTo(3);
		// L'ordre des parcours retournés est important
		assertThat(parcours.get(0).code()).isEqualTo("PAR3");
		assertThat(parcours.get(1).code()).isEqualTo("PAR2");
		assertThat(parcours.get(2).code()).isEqualTo("PAR1");
	}
	
	@Test
	public void testParcoursPourSpecialiteNiveauEtParcours3() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourSpecialiteNiveauEtParcours(diplomeMultiParcours, year, specialiteDiplomeMultiParcours, 1, diplomeMultiParcoursParcours2);
		assertThat(parcours.size()).isEqualTo(2);
		// L'ordre des parcours retournés est important
		assertThat(parcours.get(0).code()).isEqualTo("PAR2");
		assertThat(parcours.get(1).code()).isEqualTo("PAR1");
	}
	
	@Test
	public void testParcoursPourSpecialiteNiveauEtParcours4() {
		List<IParcours> parcours = explorationMaquetteService.getParcoursPourSpecialiteNiveauEtParcours(diplomeMultiParcours, year, specialiteDiplomeMultiParcours, 2, diplomeMultiParcoursParcours3);
		assertThat(parcours.size()).isEqualTo(1);
		// L'ordre des parcours retournés est important
		assertThat(parcours.get(0).code()).isEqualTo("PAR3");
	}
}
