package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;

import static org.fest.assertions.api.Assertions.assertThat;


public class AbstractMaquetteProcessorIterativeImplTest extends EOScolTestCase {

	private EODiplome diplome; 
	private DummyMaquetteProcessorIterativeImpl processor;
	private EOLien lienDiplomeVersion;
	
	
	@Before
	public void setUp() {
		super.setUp();
		creeerStructureDiplome();
		
	}
	

	private void creeerStructureDiplome() {
		Integer anneeYear = Calendar.getInstance().get(Calendar.YEAR);
		EOTypeLien composer = EOTestsHelper.creerTypeLien(editingContext, 1, EOTypeLien.TYPECOMPOSER_LBL);
		EOTypeLien versionner = EOTestsHelper.creerTypeLien(editingContext, 2, EOTypeLien.TYPEVERSIONNER_LBL);
		
		EOTypePeriode annee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, "Année");
		EOTypePeriode semestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, "Semestre");
		
		EOTypeComposant typeDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeVersion = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTypeComposant typePeriode = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		EOTypeComposant typeEc = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		
		EOTestsHelper.creerRegleLiaison(editingContext, typeDiplome, typeVersion, versionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeVersion, typePeriode, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typePeriode, composer);
		EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typeEc, composer);
		
		EOStatutFormation statutFormation = EOTestsHelper.creerStatutFormation(editingContext, 1, "S", "STATUT");
		
		EOGradeUniversitaire gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		EOTypeFormation typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
		diplome = EOTestsHelper.creerDiplome(editingContext, 1, "DIP", "DIPLOME", statutFormation, gradeUniversitaireMaster, typeFormationMaster, anneeYear);
		
		EOVersionDiplome vd = EOTestsHelper.creerVersionDiplome(editingContext, 2, "VD1", "Version Diplome 1", anneeYear);
		
		lienDiplomeVersion = EOTestsHelper.creerLien(editingContext, 1, diplome, vd, versionner);

		
		EOPeriode licence1 = EOTestsHelper.creerPeriode(editingContext, 3, "L1", "Licence 1", annee, 1);
		EOTestsHelper.creerLienComposer(editingContext, 2, vd, licence1, false);
		
		EOPeriode licence2 = EOTestsHelper.creerPeriode(editingContext, 4, "L2", "Licence 2", annee, 2);
		EOTestsHelper.creerLienComposer(editingContext, 3, vd, licence2, false);
		
		EOPeriode semestre1 = EOTestsHelper.creerPeriode(editingContext, 5, "S1", "Semestre 1", semestre, 1);
		EOTestsHelper.creerLienComposer(editingContext, 4, licence1, semestre1, false);
		
		EOPeriode semestre2 = EOTestsHelper.creerPeriode(editingContext, 6, "S2", "Semestre 2", semestre, 2);
		EOTestsHelper.creerLienComposer(editingContext, 5, licence1, semestre2, false);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, 7, "EC1", "Ec 1");
		EOTestsHelper.creerLienComposer(editingContext, 6, semestre1, ec1, true);
		
		EOEC ec2 = EOTestsHelper.creerEC(editingContext, 8, "EC2", "Ec 2");
		EOTestsHelper.creerLienComposer(editingContext, 7, semestre1, ec2, true);
		
		EOEC ec3 = EOTestsHelper.creerEC(editingContext, 9, "EC3", "Ec 3");
		EOTestsHelper.creerLienComposer(editingContext, 8, semestre1, ec3, false);
		
	}
	

	@Test
	public void test() {
		processor = new DummyMaquetteProcessorIterativeImpl(editingContext);
		processor.setRoot(lienDiplomeVersion);
		
		try {
			processor.proceeed();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertThat(processor.getResultat().size()).isEqualTo(8);
	}
	
	public class DummyMaquetteProcessorIterativeImpl extends AbstractMaquetteProcessorIterativeImpl {

		public DummyMaquetteProcessorIterativeImpl(EOEditingContext editingContext) {
			super(editingContext);
		}

		private List<ILien> resultat = new ArrayList<ILien>();
		
		public Boolean shouldProcessChildrenOf(ILien lien) {
			return true;
		}

		public void performActionsOn(ILien lien) {
			resultat.add(lien);
			
		}
		
		public List<ILien> getResultat() {
			return resultat;
		}
		
	}
	

}
