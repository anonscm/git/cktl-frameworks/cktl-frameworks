package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class MockitoScolTests {

	protected Answer<List<ILien>> getAnswerForLien(ILien... liens) {
		final List<ILien> liensParents = new ArrayList<ILien>(Arrays.asList(liens));
		Answer<List<ILien>> answer = new Answer<List<ILien>>() {

			public List<ILien> answer(InvocationOnMock invocation) throws Throwable {
				return liensParents;
			}
		};
		return answer;
	}
	
	protected void mockCreditable(IComposant composant, BigDecimal valeurCreditECTS) {
		if (valeurCreditECTS != null) {
			ICreditable creditable = mock(ICreditable.class);
			when(creditable.creditECTS()).thenReturn(valeurCreditECTS);
			when(composant.creditable()).thenReturn(creditable);
		}
	}

	protected DummyLienComposer spyLienComposer(IComposant parent, IComposant enfant, Integer childId, Boolean obligatoire) {
		DummyLienComposer lienComposer = spy(new DummyLienComposer());
		doReturn(parent).when(lienComposer).parent();
		doReturn(enfant).when(lienComposer).child();
		when(lienComposer.childId()).thenReturn(childId);
		when(lienComposer.obligatoire()).thenReturn(obligatoire);
		return lienComposer;
	}

	protected class DummyLienComposer implements ILienComposer {

		private BigDecimal creditECTSCalcule;
		private Integer ordre;

		public IComposant child() {
			// TODO Auto-generated method stub
			return null;
		}

		public IComposant parent() {
			// TODO Auto-generated method stub
			return null;
		}

		public BigDecimal getCreditECTSCalcule() {
			return creditECTSCalcule;
		}

		public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
			this.creditECTSCalcule = creditECTSCalcule;

		}

		public Integer getSommeMinutesEnseigneesCalculee() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
			// TODO Auto-generated method stub

		}

		public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
			// TODO Auto-generated method stub
			return null;
		}

		public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeHeuresEnseignesCalculees, EOTypeAP typeAP) {
			// TODO Auto-generated method stub

		}

		public BigDecimal sommeCreditECTSEnfants() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer sommeMinutesEnseigneesAPTD() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer sommeMinutesEnseigneesAPTP() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer sommeMinutesEnseigneesAPCours() {
			return null;
		}

		public Integer choixECTS() {
			return null;
		}

		public ICreditable creditable() {
			return null;
		}

		public void setObligatoire(Integer value) {

		}

		public Boolean isParentSemestre() {
			return null;
		}

		public Integer id() {
			return null;
		}

		public Boolean isNbCreditsEctsOk() {
			return null;
		}

		public Integer childId() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer parentId() {
			// TODO Auto-generated method stub
			return null;
		}

		public Integer ordre() {
			return ordre;
		}

		public void setOrdre(Integer value) {
			ordre = value;
		}

		public BigDecimal coefficient() {
	    // TODO Auto-generated method stub
	    return null;
    }

		public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
			
		}

		public void setCoefficient(BigDecimal coeff) {
			
		}

		public void assignTo(ILien nouveauLien) {
	    // TODO Auto-generated method stub
	    
    }

		public Boolean obligatoire() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setObligatoire(Boolean value) {
			// TODO Auto-generated method stub
			
		}

	}
}