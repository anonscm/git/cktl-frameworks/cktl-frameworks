package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;



import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(value=MockitoJUnitRunner.class)
public class CreditableServiceImplTest {
	
	@Mock
	IComposant composant;
	
	ILienComposer lienComposer;
	
	@Mock
	ICreditable creditable;
	
	CreditableService creditableService;
	
	@Before
	public void setUp() throws Exception {
		
		creditableService = new CreditableServiceImpl();
		
		Mockito.when(creditable.creditECTS()).thenReturn(BigDecimal.valueOf(10));
		Mockito.when(composant.creditable()).thenReturn(creditable);
		
	}

	@Test
	public void testCreditsEcts() {
		BigDecimal ects = creditableService.creditsEcts(composant, lienComposer);
		Assertions.assertThat(ects).isEqualTo(BigDecimal.TEN);
	}
	
	@Test
	public void testCreditsSurLien() {
		lienComposer = Mockito.mock(ILienComposer.class);
		BigDecimal cinq = BigDecimal.valueOf(5);
		Mockito.when(creditable.creditECTS()).thenReturn(cinq);		
		Mockito.when(lienComposer.creditable()).thenReturn(creditable);

		BigDecimal ects = creditableService.creditsEcts(composant, lienComposer);
		Assertions.assertThat(ects).isEqualTo(cinq);
	}

}
