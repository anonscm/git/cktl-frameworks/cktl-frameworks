package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Calendar;

import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.junit.Before;
import org.junit.Test;

import com.wounit.annotations.Dummy;

import er.extensions.eof.ERXQ;

public class EOParcoursTest extends EOScolTestCase {

	@Dummy
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);

	private static final String expectedCode = "unCode";
	private static final String expectedLibelle = "unLibelle";

	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaire;

	// EOStatutFormation
	private EOStatutFormation statutFormationProduction;

	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
	
	private EONiveauAccesDiplome niveauAccesDiplome;

	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	// Tests

	@Test
	public void testSupprimerParcours() {
		// Arrange
		EOParcours parcours = EOTestsHelper.creerParcours(editingContext, expectedCode, expectedLibelle);
		EOSiseDiscipline siseDiscipline = EOTestsHelper.creerSiseDiscipline(editingContext, "cdisc", "ldisc");
		EOSiseSecteurDisciplinaire secteurDisciplinaire = EOTestsHelper.creerSiseSecteurDisciplinaire(editingContext, "csect", "lsect", siseDiscipline);
		EODiplome diplome = creerEtInitialiserDiplome(2012, "Le diplome", "habilitable en production", 1, statutFormationProduction, gradeUniversitaire,
		    typeFormationMaster, secteurDisciplinaire);
		EOVersionDiplome versionDiplome = creerVersionDiplome(diplome, 2012, null);

		EOComposantFactory.createLien(versionDiplome, parcours, EOTypeLien.typeComposer(editingContext));
		editingContext.saveChanges();

		// Assert Ready to Test
		EOParcours parcoursEnregistre = EOParcours.fetchSco_Parcours(editingContext, ERXQ.equals(EOParcours.CODE_KEY, parcours.code()));
		assertNotNull(parcoursEnregistre);

		// Act
		try {
			parcours.supprimer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Assert
		EOParcours parcoursASupprime = EOParcours.fetchSco_Parcours(editingContext, ERXQ.equals(EOParcours.CODE_KEY, parcours.code()));
		assertNull(parcoursASupprime);
	}

	@Test
	public void test_copy() {
		// Arrange
		EOParcours parcours = EOTestsHelper.creerParcours(editingContext, "code", "libelle");

		// Assert ready to test
		EOParcours newParcours = parcours.copy(2014, null);

		// Act

		// Assert
		assertComposantsIdentiques(parcours, newParcours);
	}

	// Private Helpers
	/**
	 * Initialise le contexte necessaire à la création d'un composant de type Diplome
	 */
	private void initialiserContexte() {
		// initialiser le contexte
		EOTypeComposant typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, "DIPLOME", "Diplôme");
		EOTypeComposant typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 2, "VERSION_DIPLOME", "Diplôme");
		EOTypeComposant typeComposantParcours = EOTestsHelper.creerTypeComposant(editingContext, 3, "PARCOURS", "Parcours");

		EOTypeLien typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 1, "versionner");
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantParcours, typeLienComposer);

		// EOGradeUniversitaire
		gradeUniversitaire = EOTestsHelper.creerGradeUniversitaire(editingContext, "LICENCE", "Licence", 3);

		// EOStatutFormation
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");

		// EOTypeFormation
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
		niveauAccesDiplome = EOTestsHelper.creerNiveauAccesDiplome(editingContext, "libelle", 0);
	}

	/*
	 * private EODiplome creerDiplome(Integer id, String code, String libelle, EOStatutFormation statutFormation) { try { EODiplome unDiplome =
	 * EOComposantFactory.createComposant(EODiplome.class, null, editingContext); unDiplome.setId(1); unDiplome.setCode(code); unDiplome.setLibelle(libelle);
	 * unDiplome.setValidateur(new CodeValidateurGenereAuto()); unDiplome.setStatutFormationRelationship(statutFormation); return unDiplome; } catch (Exception e)
	 * { e.printStackTrace(); return null; } }
	 */

	private EODiplome creerEtInitialiserDiplome(Integer anneeDepart, String code, String libelle, Integer habilitationRequise, EOStatutFormation statutFormation,
	    EOGradeUniversitaire gradeUniversitaire, EOTypeFormation typeFormation, EOSiseSecteurDisciplinaire secteurDisciplinaire) {
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, code, libelle, statutFormation, gradeUniversitaire, typeFormation, year);
		diplome.setValidateur(new CodeValidateurSaisieObligatoire());
		editingContext.insertObject(diplome);

		diplome.setHabilitationRequise(habilitationRequise);
		diplome.setGradeUniversitaireRelationship(gradeUniversitaire);
		diplome.setSecteurDisciplinaireSiseRelationship(secteurDisciplinaire);
		diplome.setNiveauAccesDiplomeRelationship(niveauAccesDiplome);
		return diplome;
	}

	private EOVersionDiplome creerVersionDiplome(EODiplome diplome, int annee, String composante) {
		try {
			EOVersionDiplome versionDiplome = EOTestsHelper.creerVersionDiplome(editingContext, "", "", annee);
			versionDiplome.initialise(diplome, annee);

			return versionDiplome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
