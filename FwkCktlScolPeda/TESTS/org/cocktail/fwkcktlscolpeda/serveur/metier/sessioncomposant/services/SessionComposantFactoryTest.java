package org.cocktail.fwkcktlscolpeda.serveur.metier.sessioncomposant.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionComposantFactoryTest extends EOScolTestCase {

	private SessionComposantFactory factory;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		initAllTypesComposants();

		factory = new SessionComposantFactoryEOF(editingContext);
		EOTestsHelper.creerTypeFormule(editingContext, EOTypeFormule.TYPEMOYENNE_LBL);
		EOTestsHelper.creerTypeFormule(editingContext, EOTypeFormule.TYPEADMISSION_LBL);
		EOTestsHelper.creerTypeFormule(editingContext, EOTypeFormule.TYPECOMPENSATION_LBL);
		EOTestsHelper.creerTypeFormule(editingContext, EOTypeFormule.TYPEDETTES_LBL);
	}

	/**
	 * 2 sessions + 1 session spéciale
	 * 
	 * Formule Moyenne Admission Compensation
	 */
	@Test
	public void testCreerSessionsComposantVersionDiplome() {
		IDiplome composant = editingContext.createSavedObject(EODiplome.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typeVersionDiplome(editingContext));

		factory.creerSessionsComposant(composant);

		// sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session Spéciale", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}

	/**
	 * 2 sessions + 1 session spéciale
	 * 
	 * Formule Moyenne Admission Compensation
	 */
	@Test
	public void testCreerSessionsComposantPeriode() {
		IPeriode composant = editingContext.createSavedObject(EOPeriode.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typePeriode(editingContext));

		factory.creerSessionsComposant(composant);

		// sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session Spéciale", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}

	/**
	 * 2 sessions + 1 session spéciale
	 * 
	 * Formule Moyenne
	 */
	@Test
	public void testCreerSessionsComposantParcours() {
		IParcours composant = editingContext.createSavedObject(EOParcours.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typeParcours(editingContext));

		factory.creerSessionsComposant(composant);

		// sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session Spéciale", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}

	/**
	 * 2 sessions + 1 session spéciale
	 * 
	 * Formule Moyenne
	 */
	@Test
	public void testCreerSessionsComposantRegroupement() {
		IRegroupement composant = editingContext.createSavedObject(EORegroupement.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typeRegroupement(editingContext));

		factory.creerSessionsComposant(composant);

		// sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session Spéciale", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}

	/**
	 * 2 sessions + 1 session spéciale
	 * 
	 * Formule Moyenne Admission Compensation
	 */
	@Test
	public void testCreerSessionsComposantUE() {
		IUE composant = editingContext.createSavedObject(EOUE.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typeUE(editingContext));
		
		factory.creerSessionsComposant(composant);

		// sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session Spéciale", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}

	/**
	 * 3 sessions
	 * 
	 * Formule Moyenne Admission Compensation
	 */
	@Test
	public void testCreerSessionsComposantEC() {
		IEC composant = editingContext.createSavedObject(EOEC.class);
		((EOComposant) composant).setTypeComposantRelationship(EOTypeComposant.typeEC(editingContext));
		
		factory.creerSessionsComposant(composant);

		// Trois sessions
		Assert.assertEquals(3, composant.sessions().size());
		Assert.assertEquals("Session 1", composant.sessions().get(0).libelle());
		Assert.assertEquals(1, composant.sessions().get(0).numeroSession().intValue());
		Assert.assertEquals("Session 2", composant.sessions().get(1).libelle());
		Assert.assertEquals(2, composant.sessions().get(1).numeroSession().intValue());
		Assert.assertEquals("Session 3", composant.sessions().get(2).libelle());
		Assert.assertEquals(3, composant.sessions().get(2).numeroSession().intValue());

		// formules
		for (ISessionComposant sessionComposant : composant.sessions()) {
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEMOYENNE_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPEADMISSION_LBL));
			Assert.assertNotNull(sessionComposant.getFormule(EOTypeFormule.TYPECOMPENSATION_LBL));
			Assert.assertNull(sessionComposant.getFormule(EOTypeFormule.TYPEDETTES_LBL));
		}
	}
}
