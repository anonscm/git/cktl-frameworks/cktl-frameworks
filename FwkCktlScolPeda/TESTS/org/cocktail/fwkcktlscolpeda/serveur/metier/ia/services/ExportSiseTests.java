package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAcademie;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IBac;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISise;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeEchange;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.ConstantesEnqueteSise;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class ExportSiseTests {
	private ExportSiseServiceImpl exportSiseService;
	private IEtudiantAnnee etudiantAnnee;
	private IInfosEtudiant infosEtudiants;
	private IBac bac;
	private List<ICursus> listeCursus;
	private ICursus cursus;
	private ITypeFormation typeFormation;
	private IRne rne;
	protected String fonctio;

	@Before
	public void setUp() {
		etudiantAnnee = mock(IEtudiantAnnee.class);
		infosEtudiants = mock(IInfosEtudiant.class);
		bac = mock(IBac.class);
		when(infosEtudiants.toBac()).thenReturn(bac);
		when(etudiantAnnee.toInfosEtudiant()).thenReturn(infosEtudiants);
		exportSiseService = new ExportSiseServiceImpl();

		listeCursus = new ArrayList<ICursus>();
		cursus = mock(ICursus.class);
		typeFormation = mock(ITypeFormation.class);
		when(cursus.toTypeFormation()).thenReturn(typeFormation);
		rne = mock(IRne.class);
		when(cursus.toCRne()).thenReturn(rne);
		listeCursus.add(cursus);
	}

	@Test
	public void init() {
		// Arrange
		ICursus cursus1 = getCursus(2003, 2006, false);
		ICursus cursus2 = getCursus(2007, 2008, false);
		ICursus cursus3 = getCursus(2012, 2013, false);
		ICursus cursus4 = getCursus(2008, 2012, true);
		doReturn(Arrays.asList(cursus1, cursus2, cursus3, cursus4)).when(infosEtudiants).toCursus();

		// Act
		exportSiseService.init(null, etudiantAnnee, 2014, null);

		// Assert
		assertThat(exportSiseService.getDernierCursus()).isEqualTo(cursus3);
		assertThat(exportSiseService.getAnneeCursus()).isEqualTo(2013);
	}

	@Test
	public void init_cursusEnCours() {
		// Arrange
		ICursus cursus1 = getCursus(2003, 2006, false);
		ICursus cursus2 = getCursus(2007, 2008, false);
		ICursus cursus3 = getCursus(2012, null, false);
		ICursus cursus4 = getCursus(2008, 2012, true);
		doReturn(Arrays.asList(cursus1, cursus2, cursus3, cursus4)).when(infosEtudiants).toCursus();

		// Act
		exportSiseService.init(null, etudiantAnnee, 2014, null);

		// Assert
		assertThat(exportSiseService.getDernierCursus()).isEqualTo(cursus3);
		assertThat(exportSiseService.getAnneeCursus()).isEqualTo(2013);
	}

	@Test
	public void init_interruptEtud() {
		// Arrange
		ICursus cursus1 = getCursus(2003, 2006, false);
		ICursus cursus2 = getCursus(2007, 2008, false);
		ICursus cursus3 = getCursus(2012, 2013, true);
		ICursus cursus4 = getCursus(2008, 2012, false);
		doReturn(Arrays.asList(cursus1, cursus2, cursus3, cursus4)).when(infosEtudiants).toCursus();

		// Act
		exportSiseService.init(null, etudiantAnnee, 2014, null);

		// Assert
		assertThat(exportSiseService.getDernierCursus()).isEqualTo(cursus4);
		assertThat(exportSiseService.getAnneeCursus()).isEqualTo(2012);
	}

	private ICursus getCursus(Integer anneeDebut, Integer anneeFin, Boolean interruptionEtude) {
		ICursus cursus = mock(ICursus.class);
		when(cursus.anneeDebut()).thenReturn(anneeDebut);
		when(cursus.anneeFin()).thenReturn(anneeFin);
		when(cursus.interruptionEtud()).thenReturn(interruptionEtude);
		return cursus;
	}

	@Test
	public void getDipder_Bac() {
		// Arrange
		exportSiseService.init(null, etudiantAnnee, 2014, null);

		// Assert
		// Bac français obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn("000");
		when(bac.bacType()).thenReturn("BAC");
		assertThat(exportSiseService.getDipder()).isEqualTo("A");

		// Bac étranger obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn(ConstantesEnqueteSise.getBacCodeSiseEnseignementSecondaireEtranger());
		when(bac.bacType()).thenReturn("EQV");
		assertThat(exportSiseService.getDipder()).isEqualTo("1");

		// Bac français pas obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2013L);
		when(bac.bacCodeSise()).thenReturn("000");
		assertThat(exportSiseService.getDipder()).isEqualTo(ISise.ORACLE_EMPTY_STRING);

		// Bac français obtenu la même année mais avec un cursus non vide
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn("000");
		exportSiseService.setListeCursus(listeCursus);
		assertThat(exportSiseService.getDipder()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
	}

	@Test
	public void getDipDerCursus() {
		// Arrange
		when(infosEtudiants.etudAnbac()).thenReturn(2010L);

		ExportSiseServiceImpl exportSiseService = new ExportSiseServiceImpl();
		exportSiseService.setAnnee(2014);
		exportSiseService.setEtudiantAnnee(etudiantAnnee);
		exportSiseService.setListeCursus(listeCursus);
		exportSiseService.setDernierCursus(cursus);

		// Assert DUT
		when(typeFormation.code()).thenReturn("DUT");
		assertThat(exportSiseService.getDipder()).isEqualTo("C");

		// Assert CPEG
		when(typeFormation.code()).thenReturn("CPGE");
		assertThat(exportSiseService.getDipder()).isEqualTo("D");

		// Assert Ingénieur
		when(typeFormation.code()).thenReturn("DI");
		assertThat(exportSiseService.getDipder()).isEqualTo("E");
		when(typeFormation.code()).thenReturn("DIS");
		assertThat(exportSiseService.getDipder()).isEqualTo("E");

		// Assert DAEU
		when(typeFormation.code()).thenReturn("DAEU");
		assertThat(exportSiseService.getDipder()).isEqualTo("F");

		// Assert DEUG
		when(typeFormation.code()).thenReturn("DEUG");
		assertThat(exportSiseService.getDipder()).isEqualTo("I");

		// Assert Licence
		when(typeFormation.code()).thenReturn("LIC");
		assertThat(exportSiseService.getDipder()).isEqualTo("N");
		when(typeFormation.code()).thenReturn("LIP");
		assertThat(exportSiseService.getDipder()).isEqualTo("N");

		// Assert Maitrise
		when(typeFormation.code()).thenReturn("MAI");
		assertThat(exportSiseService.getDipder()).isEqualTo("Q");

		// Assert Master
		when(typeFormation.code()).thenReturn("MAS");
		assertThat(exportSiseService.getDipder()).isEqualTo("U");

	}

	@Test
	public void getSitupre() {
		// Arrange
		exportSiseService.init(null, etudiantAnnee, 2014, null);

		// Assert
		// Bac français obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn("000");
		assertThat(exportSiseService.getSitupre()).isEqualTo("A");

		// Bac étranger obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn(ConstantesEnqueteSise.getBacCodeSiseEnseignementSecondaireEtranger());
		assertThat(exportSiseService.getSitupre()).isEqualTo("Q");

		// Bac français pas obtenu l'année précédente
		when(infosEtudiants.etudAnbac()).thenReturn(2013L);
		when(bac.bacCodeSise()).thenReturn("000");
		assertThat(exportSiseService.getSitupre()).isEqualTo("T");


		// Bac français obtenu la même année mais avec un cursus non vide
		exportSiseService.setDernierCursus(cursus);
		exportSiseService.setAnneeCursus(2014);
		when(infosEtudiants.etudAnbac()).thenReturn(2014L);
		when(bac.bacCodeSise()).thenReturn("000");
		assertThat(exportSiseService.getSitupre()).isEqualTo("S");

		// dernier cursus antérieur à l'année précédente
		exportSiseService.setDernierCursus(cursus);
		exportSiseService.setAnneeCursus(2011);
		assertThat(exportSiseService.getSitupre()).isEqualTo("U");

		exportSiseService.setAnneeCursus(2014);
		// dernier cursus CPGE
		when(typeFormation.code()).thenReturn("CPGE");
		assertThat(exportSiseService.getSitupre()).isEqualTo("D");

		// dernier cursus IUT
		when(typeFormation.code()).thenReturn("DUT");
		assertThat(exportSiseService.getSitupre()).isEqualTo("C");

		// dernier cursus ingénieur
		when(typeFormation.code()).thenReturn("DI");
		assertThat(exportSiseService.getSitupre()).isEqualTo("E");
		when(typeFormation.code()).thenReturn("DIS");
		assertThat(exportSiseService.getSitupre()).isEqualTo("E");

		// dernier cursus étranger
		when(typeFormation.code()).thenReturn("EFEE");
		assertThat(exportSiseService.getSitupre()).isEqualTo("R");

		// dernier cursus université
		when(typeFormation.code()).thenReturn("LIC");
		assertThat(exportSiseService.getSitupre()).isEqualTo("H");
		when(typeFormation.code()).thenReturn("MAS");
		assertThat(exportSiseService.getSitupre()).isEqualTo("H");
		when(typeFormation.code()).thenReturn("DOC");
		assertThat(exportSiseService.getSitupre()).isEqualTo("H");
		when(cursus.toTypeFormation()).thenReturn(null);
		when(rne.cRne()).thenReturn("0750736T");
		assertThat(exportSiseService.getSitupre()).isEqualTo("H");

		// dernier cursus autres établissement
		List<String> listeRneE = new ArrayList<String>();
		listeRneE.add("069047XK");
		listeRneE.add("0590344D");
		listeRneE.add("0690195M");
		listeRneE.add("0490811R");
		listeRneE.add("0753541S");
		listeRneE.add("0310155A");
		listeRneE.add("0753494R");
		listeRneE.add("0753742K");
		listeRneE.add("0753486G");
		listeRneE.add("0753478Y");
		listeRneE.add("0692459Y");
		listeRneE.add("0851415B");
		listeRneE.add("0753488J");
		listeRneE.add("0753428U");
		listeRneE.add("0753431X");
		listeRneE.add("0753496T");
		listeRneE.add("0690193K");
		listeRneE.add("0940608A");
		listeRneE.add("0310153Y");
		listeRneE.add("0783043J");
		for (String coderne : listeRneE) {
			when(rne.cRne()).thenReturn(coderne);
			assertThat(exportSiseService.getSitupre()).isEqualTo("K");
		}
	}

	@Test
	@Ignore
	public void creerDonneesSisePourEtudiantAnnee() {
		// Arrange
		IPays pays = getPays();
		IAdresse adresseEtudiant = getAdresse("72000", "Le mans", pays);
		IAdresse adresseParent = getAdresse("64800", "Nay", pays);
		IIndividu individuEtudiant = getIndividuEtudiant();
		when(infosEtudiants.adresseEtudiant()).thenReturn(adresseEtudiant);
		when(infosEtudiants.adresseParent()).thenReturn(adresseParent);
		when(infosEtudiants.toIndividuEtudiant()).thenReturn(individuEtudiant);
		IBac bac = getBac();
		when(infosEtudiants.toBac()).thenReturn(bac);
		when(infosEtudiants.etudAnbac()).thenReturn(2013L);
		IProfession profession1 = getProfession("CodePro1");
		IProfession profession2 = getProfession("CodePro2");
		when(infosEtudiants.toProfession1()).thenReturn(profession1);
		when(infosEtudiants.toProfession2()).thenReturn(profession2);
		when(infosEtudiants.etudNumero()).thenReturn(2001);
		when(infosEtudiants.codeIne()).thenReturn("ABC");
		when(infosEtudiants.etudAnnee1inscSup()).thenReturn(0L);

		exportSiseService = spy(exportSiseService);
		doReturn(getDonneeSise()).when(exportSiseService).doCreateDonneesSise();

		// Assert ready to test
		exportSiseService.init(null, etudiantAnnee, 2014, "codeEtab");
		// Act
		ISise donneesSise = exportSiseService.creerDonneesSisePourEtudiantAnnee(getDonneeSise());
		// Assert

		assertThat(donneesSise.numins()).isEqualTo("2001");
		assertThat(donneesSise.idetu()).isEqualTo("ABC");
		assertThat(donneesSise.compos()).isEqualTo("codeEtab");
		assertThat(donneesSise.annais()).isEqualTo("1997");
		assertThat(donneesSise.sexe()).isEqualTo("2");
		//assertThat(donneesSise.bac()).isEqualTo("S");
		assertThat(donneesSise.anbac()).isEqualTo("2013");
		// assertThat(donneesSise.curpar()).isEqualTo("0");
		assertThat(donneesSise.nation()).isEqualTo("100");
		assertThat(donneesSise.paripa()).isEqualTo("100");
		assertThat(donneesSise.cpEtu()).isEqualTo("72000");
		assertThat(donneesSise.lcomEtu()).isEqualTo("Le mans");
		assertThat(donneesSise.paypar()).isEqualTo("100");
		assertThat(donneesSise.cpPar()).isEqualTo("64800");
		assertThat(donneesSise.lComRef()).isEqualTo("Nay");
		assertThat(donneesSise.pcspar()).isEqualTo("CodePro1");
		assertThat(donneesSise.pcspar2()).isEqualTo("CodePro2");

		assertThat(donneesSise.speciA()).isEqualTo("0123");
		assertThat(donneesSise.speciB()).isEqualTo("0123");
		assertThat(donneesSise.speciC()).isEqualTo("0123");
		assertThat(donneesSise.numed()).isEqualTo("    ");

		// assertThat(donneesSise.reimma()).isEqualTo("P");
		assertThat(donneesSise.ansup()).isEqualTo("0");
		assertThat(donneesSise.anetab()).isEqualTo(" ");
		assertThat(donneesSise.typrepa()).isEqualTo("");
	}

	@Test
	public void setInscriptionsInfos() {
		// Arrange
		IInscription inscription1 = getInscription("CodeRegime1");
		IInscription inscription2 = getInscription("CodeRegime2");

		// Assert
		// inscription principale non nulle
		ISise donneesSise = getDonneeSise();
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(inscription1);
		exportSiseService.init(null, etudiantAnnee, 2014, "codeEtab");
		exportSiseService.setInscriptionsInfos(donneesSise);
		assertThat(donneesSise.inspr()).isEqualTo("O");
		assertThat(donneesSise.regime()).isEqualTo("CodeRegime1");

		// inscription secondaire non nulle
		donneesSise = getDonneeSise();
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(null);
		when(etudiantAnnee.getInscriptionSecondaire()).thenReturn(inscription2);
		exportSiseService.init(null, etudiantAnnee, 2014, "codeEtab");
		exportSiseService.setInscriptionsInfos(donneesSise);
		assertThat(donneesSise.inspr()).isEqualTo("N");
		assertThat(donneesSise.regime()).isEqualTo("CodeRegime2");
	}

	@Test
	public void getEchang() {
		// Arrange
		IInscription inscription = getInscription("");
		ISise donneesSise = getDonneeSise();

		// sans inscription principale
		donneesSise.setInspr("N");
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		assertThat(exportSiseService.getEchang(donneesSise)).isEqualTo(ISise.ORACLE_EMPTY_STRING);

		// type échange nul
		donneesSise.setInspr("O");
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(inscription);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		assertThat(exportSiseService.getEchang(donneesSise)).isEqualTo("0");

		// type echange renseigné
		ITypeInscriptionFormation typeInscriptionFormation = mock(ITypeInscriptionFormation.class);
		ITypeEchange typeEchange = mock(ITypeEchange.class);
		when(inscription.toTypeInscriptionFormation()).thenReturn(typeInscriptionFormation);
		when(inscription.toTypeEchange()).thenReturn(typeEchange);

		// type echange arrivee
		when(typeInscriptionFormation.codeInscription()).thenReturn(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.SOCRATES_ERASMUS_A, "1");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.TEMPUS_A, "2");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.SOCRATES_COMENIUS_A, "3");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.LEONARDO_DA_VINCI_A, "5");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.AUTRES_PGMES_FINANCES_UE_A, "6");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.AUTRES_PGMES_ACCORDS_BI_A, "7");
		when(typeEchange.sens()).thenReturn(ITypeEchange.TYPE_ECHANGE_ARRIVEE);

		when(typeInscriptionFormation.codeInscription()).thenReturn("");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.SOCRATES_ERASMUS_A, ISise.ORACLE_EMPTY_STRING);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.TEMPUS_A, ISise.ORACLE_EMPTY_STRING);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.SOCRATES_COMENIUS_A, ISise.ORACLE_EMPTY_STRING);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.LEONARDO_DA_VINCI_A, ISise.ORACLE_EMPTY_STRING);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.AUTRES_PGMES_FINANCES_UE_A, ISise.ORACLE_EMPTY_STRING);
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_ARRIVEE, ITypeEchange.AUTRES_PGMES_ACCORDS_BI_A, ISise.ORACLE_EMPTY_STRING);

		when(typeInscriptionFormation.codeInscription()).thenReturn("");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.SOCRATES_ERASMUS_D, "B");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.TEMPUS_D, "C");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.SOCRATES_COMENIUS_D, "D");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.LEONARDO_DA_VINCI_D, "G");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.AUTRES_PGMES_FINANCES_UE_D, "H");
		checkTypeEchange(donneesSise, typeEchange, ITypeEchange.TYPE_ECHANGE_DEPART, ITypeEchange.AUTRES_PGMES_ACCORDS_BI_D, "J");
	}

	@Test
	public void getDiplom() {
		// Arrange
		IInscription inscription1 = mock(IInscription.class);
		IDiplome diplome1 = mock(IDiplome.class);
		when(diplome1.getSiseStr()).thenReturn("sise1");

		IInscription inscription2 = mock(IInscription.class);
		IDiplome diplome2 = mock(IDiplome.class);
		when(diplome2.getSiseStr()).thenReturn("sise2");

		// Assert
		//inscription primaire et secondaire  = null
		assertThat(exportSiseService.getDiplom()).isEqualTo(ISise.ORACLE_EMPTY_STRING);

		//inscription primaire = null, inscription secondaire <> null
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(null);
		when(etudiantAnnee.getInscriptionSecondaire()).thenReturn(inscription2);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		when(inscription2.toDiplome()).thenReturn(null);
		assertThat(exportSiseService.getDiplom()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		when(inscription2.toDiplome()).thenReturn(diplome2);
		assertThat(exportSiseService.getDiplom()).isEqualTo("sise2");

		//inscription primaire <> null
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(inscription1);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		when(inscription2.toDiplome()).thenReturn(null);
		assertThat(exportSiseService.getDiplom()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		when(inscription1.toDiplome()).thenReturn(diplome1);
		assertThat(exportSiseService.getDiplom()).isEqualTo("sise1");
		when(diplome1.getSiseStr()).thenReturn(null);
		assertThat(exportSiseService.getDiplom()).isNull();

	}

	@Test
	public void getNiveau() {
		// Arrange
		IInscription inscription1 = mock(IInscription.class);
		when(inscription1.niveau()).thenReturn(3);
		IInscription inscription2 = mock(IInscription.class);
		when(inscription2.niveau()).thenReturn(5);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		assertThat(exportSiseService.getDiplom()).isEqualTo(ISise.ORACLE_EMPTY_STRING);

		//inscription primaire et secondaire  = null
		assertThat(exportSiseService.getNiveau()).isEqualTo(ISise.ORACLE_EMPTY_STRING);

		//inscription primaire = null, inscription secondaire <> null
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(null);
		when(etudiantAnnee.getInscriptionSecondaire()).thenReturn(inscription2);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		assertThat(exportSiseService.getNiveau()).isEqualTo("5");

		//inscription primaire <> null
		when(etudiantAnnee.getInscriptionPrincipale()).thenReturn(inscription1);
		exportSiseService.init(null, etudiantAnnee, 2014, null);
		assertThat(exportSiseService.getNiveau()).isEqualTo("3");
	}

	@Test
	public void lancerImportTableSiseEcole() {
		// Arrange
		//infos obligatoire
		IPays pays = getPays();
		IAdresse adresseEtudiant = getAdresse("72000", "Le mans", pays);
		IAdresse adresseParent = getAdresse("64800", "Nay", pays);
		IIndividu individuEtudiant = getIndividuEtudiant();
		when(infosEtudiants.adresseEtudiant()).thenReturn(adresseEtudiant);
		when(infosEtudiants.adresseParent()).thenReturn(adresseParent);
		when(infosEtudiants.toIndividuEtudiant()).thenReturn(individuEtudiant);
		IProfession profession1 = getProfession("CodePro1");
		IProfession profession2 = getProfession("CodePro2");
		when(infosEtudiants.toProfession1()).thenReturn(profession1);
		when(infosEtudiants.toProfession2()).thenReturn(profession2);

		//infos specifiques aux écoles
		IAcademie academie = mock(IAcademie.class);
		when(academie.cAcademie()).thenReturn("AB123");

		exportSiseService = spy(exportSiseService);
		doReturn(getDonneeSise()).when(exportSiseService).doCreateDonneesSise();

		// Assert ready to test
		exportSiseService.init(null, etudiantAnnee, 2014, "codeEtab");


	}

	@Test
	public void lancerImportTableSiseUniv() {
		// Arrange
		//infos obligatoire
		IPays pays = getPays();
		IAdresse adresseEtudiant = getAdresse("72000", "Le mans", pays);
		IAdresse adresseParent = getAdresse("64800", "Nay", pays);
		IIndividu individuEtudiant = getIndividuEtudiant();
		when(infosEtudiants.adresseEtudiant()).thenReturn(adresseEtudiant);
		when(infosEtudiants.adresseParent()).thenReturn(adresseParent);
		when(infosEtudiants.toIndividuEtudiant()).thenReturn(individuEtudiant);
		IProfession profession1 = getProfession("CodePro1");
		IProfession profession2 = getProfession("CodePro2");
		when(infosEtudiants.toProfession1()).thenReturn(profession1);
		when(infosEtudiants.toProfession2()).thenReturn(profession2);
		IDepartement departement = mock(IDepartement.class);
		when(departement.cDepartement()).thenReturn("32");
		when(infosEtudiants.toDepartementEtabBac()).thenReturn(departement);

		//infos specifiques aux universités
		when(infosEtudiants.etudAnnee1inscUniv()).thenReturn(2014L);

		exportSiseService = spy(exportSiseService);
		doReturn(getDonneeSise()).when(exportSiseService).doCreateDonneesSise();
		doReturn("72452").when(exportSiseService).getCodeCommune(adresseEtudiant);
		doReturn("64812").when(exportSiseService).getCodeCommune(adresseParent);

		when(infosEtudiants.etudAnbac()).thenReturn(2013L);
		when(bac.bacCodeSise()).thenReturn("000");
		when(bac.bacType()).thenReturn("BAC");

		// Assert ready to test
		exportSiseService.init(null, etudiantAnnee, 2014, "codeEtab");

		// Assert
		ISise donneesSise= exportSiseService.lancerImportTableSise();
		assertThat(donneesSise.jonais()).isEqualTo("10");
		assertThat(donneesSise.monais()).isEqualTo("04");
		assertThat(donneesSise.aninsc()).isEqualTo("2014");
		assertThat(donneesSise.conv()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.activi()).isEqualTo(" ");
		assertThat(donneesSise.cometu()).isEqualTo("72452");
		assertThat(donneesSise.comref()).isEqualTo("64812");
		assertThat(donneesSise.depbac()).isEqualTo("32");
		assertThat(donneesSise.telens()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.dom1()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.dom2()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.dom3()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.dom4()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.dom5()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.parType()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
		assertThat(donneesSise.cge()).isEqualTo(ISise.ORACLE_EMPTY_STRING);
	}

	//Private Helpers
	private void checkTypeEchange(ISise donneesSise, ITypeEchange typeEchange, String sens, String code, String resultatAttendu) {
		when(typeEchange.sens()).thenReturn(sens);
		when(typeEchange.codeEchange()).thenReturn(code);
		assertThat(exportSiseService.getEchang(donneesSise)).isEqualTo(resultatAttendu);
	}

	private IInscription getInscription(String codeRegime) {
		IInscription inscription = mock(IInscription.class);
		IRegimeInscription regime = mock(IRegimeInscription.class);
		when(regime.code()).thenReturn(codeRegime);
		when(inscription.toRegimeInscription()).thenReturn(regime);
		return inscription;
	}

	private IBac getBac() {
		IBac bac = mock(IBac.class);
		when(bac.bacCodeSise()).thenReturn("S");
		when(bac.bacCodeSise()).thenReturn("000");
		return bac;
	}

	private IProfession getProfession(String code) {
		IProfession profession = mock(IProfession.class);
		when(profession.proCode()).thenReturn(code);
		return profession;
	}

	private IIndividu getIndividuEtudiant() {
		IIndividu individu = mock(IIndividu.class);
		when(individu.dNaissance()).thenReturn(getDate("10/04/1997"));
		ICivilite civilite = mock(ICivilite.class);
		when(civilite.getSexe()).thenReturn("2");
		when(individu.getCivilite()).thenReturn(civilite);
		IPays pays = getPays();
		when(individu.toPaysNationalite()).thenReturn(pays);
		when(individu.priseCptInsee()).thenReturn("P");
		return individu;
	}

	private IPays getPays() {
		IPays pays = mock(IPays.class);
		when(pays.getCode()).thenReturn(IPays.CODE_PAYS_FRANCE);
		return pays;
	}

	private IAdresse getAdresse(String codePostal, String ville, IPays pays) {
		IAdresse adresse = mock(IAdresse.class);
		when(adresse.codePostal()).thenReturn(codePostal);
		when(adresse.ville()).thenReturn(ville);
		when(adresse.toPays()).thenReturn(pays);
		return adresse;
	}

	private Date getDate(String strDate) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = simpleDateFormat.parse(strDate);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

	}

	private ISise getDonneeSise() {
		ISise donneesSise = new ISise() {
			private String typrepa;
			private String speciC;
			private String speciB;
			private String speciA;
			private String situpre;
			private String sexe;
			private String nation;
			private String niveau;
			private String numed;
			private String numins;
			private String paripa;
			private String paypar;
			private String pcspar;
			private String pcspar2;
			private String regime;
			private String reimma;
			private String lcomEtu;
			private String lComRef;
			private String inspr;
			private String idetu;
			private String echang;
			private String diplom;
			private String dipder;
			private String curpar;
			private String cpPar;
			private String cpEtu;
			private String compos;
			private String sbac;
			private String acabac;
			private String ansup;
			private String annais;
			private String anetab;
			private String anbac;
			private String amena;
			private String jonais;
			private String monais;
			private String aninsc;
			private String conv;
			private String activi;
			private String cometu;
			private String comref;
			private String depbac;
			private String telens;
			private String dom1;
			private String dom2;
			private String dom3;
			private String dom4;
			private String dom5;
			private String parType;
			private String cge;
			private String dispense;

			public String typrepa() {
				return typrepa;
			}

			public String speciC() {
				return speciC;
			}

			public String speciB() {
				return speciB;
			}

			public String speciA() {
				return speciA;
			}

			public String situpre() {
				return situpre;
			}

			public String sexe() {
				return sexe;
			}

			public void setTyprepa(String value) {
				typrepa = value;
			}

			public void setSpeciC(String value) {
				speciC = value;
			}

			public void setSpeciB(String value) {
				speciB = value;
			}

			public void setSpeciA(String value) {
				speciA = value;
			}

			public void setSitupre(String value) {
				situpre = value;
			}

			public void setSexe(String value) {
				sexe = value;
			}

			public void setReimma(String value) {
				reimma = value;
			}

			public void setRegime(String value) {
				regime = value;
			}

			public void setPcspar2(String value) {
				pcspar2 = value;
			}

			public void setPcspar(String value) {
				pcspar = value;
			}

			public void setPaypar(String value) {
				paypar = value;
			}

			public void setParipa(String value) {
				paripa = value;
			}

			public void setNumins(String value) {
				numins = value;
			}

			public void setNumed(String value) {
				numed = value;
			}

			public void setNiveau(String value) {
				niveau = value;
			}

			public void setNation(String value) {
				nation = value;
			}

			public void setLcomEtu(String value) {
				lcomEtu = value;
			}

			public void setLComRef(String value) {
				lComRef = value;
			}

			public void setInspr(String value) {
				inspr = value;
			}

			public void setIdetu(String value) {
				idetu = value;
			}

			public void setFonctio(String value) {
				fonctio = value;
			}

			public void setEchang(String value) {
				echang = value;
			}

			public void setDiplom(String value) {
				diplom = value;
			}

			public void setDipder(String value) {
				dipder = value;
			}

			public void setCurpar(String value) {
				curpar = value;
			}

			public void setCpPar(String value) {
				cpPar = value;
			}

			public void setCpEtu(String value) {
				cpEtu = value;
			}

			public void setCompos(String value) {
				compos = value;
			}

			public void setBac(String value) {
				sbac = value;
			}

			public void setAnsup(String value) {
				ansup = value;
			}

			public void setAnnais(String value) {
				annais = value;
			}

			public void setAnetab(String value) {
				anetab = value;
			}

			public void setAnbac(String value) {
				anbac = value;
			}

			public void setAmena(String value) {
				amena = value;
			}

			public String reimma() {
				return reimma;
			}

			public String regime() {
				return regime;
			}

			public String pcspar2() {
				return pcspar2;
			}

			public String pcspar() {
				return pcspar;
			}

			public String paypar() {
				return paypar;
			}

			public String paripa() {
				return paripa;
			}

			public String numins() {
				return numins;
			}

			public String numed() {
				return numed;
			}

			public String niveau() {
				return niveau;
			}

			public String nation() {
				return nation;
			}

			public String lcomEtu() {
				return lcomEtu;
			}

			public String lComRef() {
				return lComRef;
			}

			public String inspr() {
				return inspr;
			}

			public String idetu() {
				return idetu;
			}

			public String fonctio() {
				return fonctio;
			}

			public String echang() {
				return echang;
			}

			public String diplom() {
				return diplom;
			}

			public String dipder() {
				return dipder;
			}

			public String curpar() {
				return curpar;
			}

			public String cpPar() {
				return cpPar;
			}

			public String cpEtu() {
				return cpEtu;
			}

			public String compos() {
				return compos;
			}

			public String bac() {
				return sbac;
			}

			public String ansup() {
				return ansup;
			}

			public String annais() {
				return annais;
			}

			public String anetab() {
				return anetab;
			}

			public String anbac() {
				return anbac;
			}

			public String amena() {
				return amena;
			}

			public String jonais() {	      
				return jonais;
			}

			public void setJonais(String value) {
				jonais= value;

			}

			public String monais() {
				return monais;
			}

			public void setMonais(String value) {
				monais = value;
			}

			public String aninsc() {
				return aninsc;
			}

			public void setAninsc(String value) {
				aninsc= value;
			}

			public String conv() {
				return conv;
			}

			public void setConv(String value) {
				conv= value;
			}

			public String activi() {
				return activi;
			}

			public void setActivi(String value) {
				activi= value;
			}

			public String cometu() {
				return cometu;
			}

			public void setCometu(String value) {
				cometu= value;
			}

			public String comref() {
				return comref;
			}

			public void setComref(String value) {
				comref= value;
			}

			public String depbac() {
				return depbac;
			}

			public void setDepbac(String value) {
				depbac= value;
			}

			public String telens() {
				return telens;
			}

			public void setTelens(String value) {
				telens= value;

			}

			public String dom1() {
				return dom1;
			}

			public void setDom1(String value) {
				dom1=value;	      
			}

			public String dom2() {
				return dom2;
			}

			public void setDom2(String value) {
				dom2=value;
			}

			public String dom3() {
				return dom3;
			}

			public void setDom3(String value) {
				dom3=value;
			}

			public String dom4() {
				return dom4;
			}

			public void setDom4(String value) {
				dom4=value;	      
			}

			public String dom5() {
				return dom5;
			}

			public void setDom5(String value) {
				dom5=value;

			}

			public String parType() {
				return parType;
			}

			public void setParType(String value) {
				parType=value;
			}

			public String cge() {
				return cge;
			}

			public void setCge(String value) {
				cge=value;	      
			}

			public void setDispens(String string) {
				this.dispense = string;
			}

			public String acabac() {
				return acabac;
			}

			public void setAcabac(String value) {
				this.acabac = value;
			}

			public String dispens() {
				return dispense;
			}
		};
		return donneesSise;
	}

}
