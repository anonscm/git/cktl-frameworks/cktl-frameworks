package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePeriodeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePeriodeInscription;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(value = MockitoJUnitRunner.class)
public class PeriodeInscriptionServiceImplTests {

	@Test
  public void isInscriptionOuverteEtudiant() {
	  // Arrange 
		List<IParametragePeriodeInscription> parametragesPeriodeInscription= new ArrayList<IParametragePeriodeInscription>();
		parametragesPeriodeInscription.add(createParametragePeriodeInscription(1,6,2014,15,6,2014,EOParametragePeriodeInscription.CODE_ETUDIANT));
		parametragesPeriodeInscription.add(createParametragePeriodeInscription(1,7,2014,15,7,2014, EOParametragePeriodeInscription.CODE_ETUDIANT));
		    
		
	  // Assert ready to test 
	  // Act
		PeriodeInscriptionRepository repo = Mockito.mock(PeriodeInscriptionRepository.class);
		Mockito.when(repo.getListeParametragePeriodeInscriptionEtudiant()).thenReturn(parametragesPeriodeInscription);
		PeriodeInscriptionService periodeInscriptionService= new PeriodeInscriptionServiceImpl(repo);
		
	  // Assert
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(30, 5, 2014))).isFalse();
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(2, 6, 2014))).isTrue();
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(18, 6, 2014))).isFalse();
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(3, 7, 2014))).isTrue();
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(1, 8, 2014))).isFalse();
			
  }
	
	@Test
  public void isInscriptionOuverteAucunePeriodeDefinie() {
	  // Arrange 
		List<IParametragePeriodeInscription> parametragesPeriodeInscription= new ArrayList<IParametragePeriodeInscription>();
		
	  // Assert ready to test 
		PeriodeInscriptionRepository repo = Mockito.mock(PeriodeInscriptionRepository.class);
		Mockito.when(repo.getListeParametragePeriodeInscriptionEtudiant()).thenReturn(parametragesPeriodeInscription);
		PeriodeInscriptionService periodeInscriptionService= new PeriodeInscriptionServiceImpl(repo);
	  // Act 
		
	  // Assert
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteEtudiant(createDate(30, 5, 2014))).isFalse();
		Assertions.assertThat(periodeInscriptionService.isInscriptionOuverteGestionnaire(createDate(30, 5, 2014))).isTrue();
  }
	
	@Test
  public void typesFormationsOuvertesEtudiant() {
	  // Arrange 
	  // Assert ready to test 
	  // Act 
	  // Assert
  }

	private IParametragePeriodeInscription createParametragePeriodeInscription(int jourOuverture, int moisOuverture, int anneeOuverture, 
			int jourFermeture, int moisFermeture, int anneeFermeture, String profil) {
	  IParametragePeriodeInscription parametragePeriodeInscription = Mockito.mock(IParametragePeriodeInscription.class);
	  Mockito.when(parametragePeriodeInscription.dateOuverture()).thenReturn(createDate(jourOuverture, moisOuverture, anneeOuverture));
	  Mockito.when(parametragePeriodeInscription.dateFermeture()).thenReturn(createDate(jourFermeture, moisFermeture, anneeFermeture));
	  Mockito.when(parametragePeriodeInscription.profil()).thenReturn(profil);
	  return parametragePeriodeInscription;
  }

	private Date createDate(int jour, int mois, int annee) {
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00"));
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.DAY_OF_MONTH, jour);
		calendar.set(Calendar.MONTH, mois);
		calendar.set(Calendar.YEAR, annee);
		return calendar.getTime();
	}

}
