package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPedaParamManager;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class NumeroEtudiantGenerateurPrefixeSequenceTests {
	@Rule
	public MockEditingContext editingContext = new MockEditingContext("Sco_scolarite");

	@Test
	public void getNouveauNumero() throws Exception {
		// Arrange
		NumeroEtudiantGenerateurPrefixeSequence generateur = spy(new NumeroEtudiantGenerateurPrefixeSequence());

		FwkCktlScolPedaParamManager paramManager = mock(FwkCktlScolPedaParamManager.class);
		FwkCktlScolPeda.setParamManager(paramManager);
        when(paramManager.getGenerateurNumeroEtudiantPrefix()).thenReturn(34);
		// Assert
		doReturn(1).when(generateur).getSequenceNextValue();
		doReturn(0).when(generateur).getOffsetValue();
		assertThat(generateur.getNouveauNumero()).isEqualTo(3400001);

		doReturn(999).when(generateur).getSequenceNextValue();
		doReturn(543).when(generateur).getOffsetValue();
		assertThat(generateur.getNouveauNumero()).isEqualTo(3400456);
	}

	@Test
	public void getNouveauNumeroException() {
		// Arrange
		NumeroEtudiantGenerateurPrefixeSequence generateur = spy(new NumeroEtudiantGenerateurPrefixeSequence());

		try {
			generateur.getNouveauNumero();
		} catch (Exception e) {
			assertTrue(true);
		}

		FwkCktlScolPedaParamManager paramManager = mock(FwkCktlScolPedaParamManager.class);
		FwkCktlScolPeda.setParamManager(paramManager);
        when(paramManager.getGenerateurNumeroEtudiantPrefix()).thenReturn(34);

		try {
			generateur.getNouveauNumero();
		} catch (Exception e) {
			assertTrue(true);
		}

		doReturn(1).when(generateur).getSequenceNextValue();
		try {
			generateur.getNouveauNumero();
		} catch (Exception e) {
			assertTrue(true);
		}

		doReturn(0).when(generateur).getOffsetValue();
		try {
			generateur.getNouveauNumero();
			assertTrue(true);
		} catch (Exception e) {
			// fail()
		}
	}

	@Test
	public void getPrefix() {
		// Arrange
		NumeroEtudiantGenerateurPrefixeSequence generateur = new NumeroEtudiantGenerateurPrefixeSequence();

		FwkCktlScolPedaParamManager paramManager = mock(FwkCktlScolPedaParamManager.class);
		FwkCktlScolPeda.setParamManager(paramManager);

		// Assert ready to test
		// Act
		// Assert

		when(paramManager.getGenerateurNumeroEtudiantPrefix()).thenReturn(33);
		assertThat(generateur.getPrefix()).isEqualTo(33);

	}

}
