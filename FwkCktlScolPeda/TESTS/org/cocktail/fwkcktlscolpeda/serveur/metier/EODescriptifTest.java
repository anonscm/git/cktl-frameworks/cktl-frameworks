package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

public class EODescriptifTest extends EOScolTestCase {
	private EOComposant composant; 

	@Before
	public void setUp() {
		super.setUp();
		composant = new EOComposantConcret("composant", "composant", 1, typeComposantConcret);
		editingContext.insertSavedObject(composant);
		EOTestsHelper.creerTypeDestinationDescriptif(editingContext, 1, EOTypeDescriptifDestination.TYPEPUBLICATION_LBL, EOTypeDescriptifDestination.TYPEPUBLICATION_LBL);	
	}

	@Test
	public void test_supprimer() {
		// Arrange
		EODescriptif descriptif = EOComposantFactory.createDescriptif(composant);
		EODescriptifChampContenu champcontenu1 = EOTestsHelper.creerDescriptifChampContenu(editingContext,"contenu descriptif");
		champcontenu1.setDescriptifRelationship(descriptif);
		
		EODescriptifChampContenu champcontenu2 = EOTestsHelper.creerDescriptifChampContenu(editingContext,"contenu descriptif");
		champcontenu2.setDescriptifRelationship(descriptif);
		
		editingContext.saveChanges();
		
		// Assert ready to test
		assertEquals(1, EODescriptif.fetchAllSco_Descriptifs(editingContext).count());
		assertEquals(2, EODescriptifChampContenu.fetchAllSco_DescriptifChampContenus(editingContext).count());
		
		try {
			// Act			
	       descriptif.supprimer();	       
	       editingContext.saveChanges();
	       assertEquals(0, EODescriptif.fetchAllSco_Descriptifs(editingContext).count());
	       assertEquals(0, EODescriptifChampContenu.fetchAllSco_DescriptifChampContenus(editingContext).count());
        } catch (Exception e) {
	        fail("impossible de suupprimer le descriptif");
        }
		
	}
}
