package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.text.Format;
import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;

public class DureeMinutesEnHeuresCentiemesFormatTest {

	@Test
	public void testFormatter() throws ParseException {
		Format formatter = new DureeMinutesEnHeuresCentiemesFormat();
		
		int i = 0;
		while (i <= 180) {
			
			String affichage = formatter.format(i);
			Integer minute = (Integer) formatter.parseObject(affichage);
			
			Assert.assertEquals("Pour i = " + i, i, minute.intValue());
			
			i++;
		}
	}
}
