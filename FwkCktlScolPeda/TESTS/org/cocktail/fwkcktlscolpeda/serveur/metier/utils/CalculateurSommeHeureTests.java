package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantConcret;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Test;

public class CalculateurSommeHeureTests extends EOScolTestCase {

	private EOTypeAP typeAPCMCustom;
	private EOTypeAP typeAPTPCustom;
	private EOTypeAP typeAPTDCustom;
	private List<EOTypeAP> typeAPs;

	@Test
	public void test_calculerSommeHeuresEnseigneesEnfants() {
		ILien lien = prepareMaquette();

		// Assert
		ICalculateurSommeHeures calculateurSommeHeure = new CalculateurSommeHeure(editingContext, null);
		calculateurSommeHeure.setTypeAPs(typeAPs);

		calculateurSommeHeure.calculerSommeHeuresEnseignants(lien);
		assertEquals(new Integer(338), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPCMCustom));
		assertEquals(new Integer(180), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPTPCustom));
		assertEquals(new Integer(1370), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPTDCustom));
		assertEquals(new Integer(1888), lien.getSommeMinutesEnseigneesCalculee());
	}

	@Test
	public void test_calculerSommeHeuresEtudiant() {
		// Arrange
		ILien lien = prepareMaquette();

		// Assert
		ICalculateurSommeHeures calculateurSommeHeure = new CalculateurSommeHeure(editingContext, null);
		calculateurSommeHeure.setTypeAPs(typeAPs);

		
		calculateurSommeHeure.calculerSommeHeuresEtudiant(lien);
		assertEquals(new Integer(330), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPCMCustom));
		assertEquals(new Integer(390), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPTPCustom));
		assertEquals(new Integer(1140), lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAPTDCustom));
		assertEquals(new Integer(1860), lien.getSommeMinutesEnseigneesCalculee());
	}
	

	// Private Helper
	private ILien prepareMaquette() {
	  // Arrange
		EOTypeComposant typeParent = EOTestsHelper.creerTypeComposant(editingContext, 2, "parent", "parent");
		EOTypeComposant typeComposantPeriode = EOTestsHelper
		    .creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		EOTypeComposant typeComposantUE = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEUE_NOM, ITypeComposant.TYPEUE_NOM);
		EOTypeComposant typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTypeComposant typeComposantAP = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEAP_NOM, ITypeComposant.TYPEAP_NOM);

		typeAPCMCustom = EOTestsHelper.creerTypeAP(editingContext, 1, "CM", "CM", new BigDecimal("1.25"));
		typeAPTPCustom = EOTestsHelper.creerTypeAP(editingContext, 1, "TP", "TP", new BigDecimal("0.75"));
		typeAPTDCustom = EOTestsHelper.creerTypeAP(editingContext, 2, "TD", "TD", new BigDecimal("1"));
		typeAPs = new ArrayList<EOTypeAP>();
		typeAPs.add(typeAPCMCustom);
		typeAPs.add(typeAPTPCustom);
		typeAPs.add(typeAPTDCustom);

		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");
		EOTestsHelper.creerRegleLiaison(editingContext, typeParent, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantUE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantUE, typeComposantEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantEC, typeComposantAP, typeLienComposer);

		EOTypePeriode typePeriode = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);

		EOComposant parent = new EOComposantConcret("comp1", "comp1", 1, typeParent);
		editingContext.insertSavedObject(parent);
		EOPeriode periode = EOTestsHelper.creerPeriode(editingContext, "periode", "periode", typePeriode);
		EOUE ue1 = EOTestsHelper.creerUE(editingContext, "ue", "ue");
		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		EOEC ec2 = EOTestsHelper.creerEC(editingContext, "ec2", "ec2");

		EOAP ap1CM = EOTestsHelper.creerAP(editingContext, "ap1CM", 60, null, typeAPCMCustom);
		EOAP ap1TD = EOTestsHelper.creerAP(editingContext, "ap1TD", 230, 2, typeAPTDCustom);
		EOAP ap1TP = EOTestsHelper.creerAP(editingContext, "ap1TP", 150, null, typeAPTPCustom);

		EOAP ap2CM = EOTestsHelper.creerAP(editingContext, "ap2CM", 270, 1, typeAPCMCustom);
		EOAP ap2TD = EOTestsHelper.creerAP(editingContext, "ap2TD", 910, 1, typeAPTDCustom);
		EOAP ap2TP = EOTestsHelper.creerAP(editingContext, "ap2TP", 240, 1, typeAPTPCustom);

		ILien lien = EOTestsHelper.creerLienComposer(editingContext, 1, parent, periode, false);

		ILien lienPeriodeUE1 = EOTestsHelper.creerLienComposer(editingContext, 2, periode, ue1, false);

		ILien lienUE1EC1 = EOTestsHelper.creerLienComposer(editingContext, 2, ue1, ec1, true);
		ILien lienUE1EC2 = EOTestsHelper.creerLienComposer(editingContext, 2, ue1, ec2, true);

		ILien lienEC1CM = EOTestsHelper.creerLienComposer(editingContext, 1, ec1, ap1CM, false);
		ILien lienEC1TD = EOTestsHelper.creerLienComposer(editingContext, 1, ec1, ap1TD, false);
		ILien lienEC1TP = EOTestsHelper.creerLienComposer(editingContext, 1, ec1, ap1TP, true);
		ILien lienEC2CM = EOTestsHelper.creerLienComposer(editingContext, 1, ec2, ap2CM, true);
		ILien lienEC2TD = EOTestsHelper.creerLienComposer(editingContext, 1, ec2, ap2TD, true);
		ILien lienEC2TP = EOTestsHelper.creerLienComposer(editingContext, 1, ec2, ap2TP, true);
	  return lien;
  }
	

}
