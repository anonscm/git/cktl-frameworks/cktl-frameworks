package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantConcret;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.junit.Test;

public class ListeEnfantsAPsTests extends EOScolTestCase {
	
	@Test
    public void test_getListEnfants() {
	    // Arrange
		EOTypeComposant typeParent = EOTestsHelper.creerTypeComposant(editingContext, 2, "parent", "parent");
		EOTypeComposant typeComposantAP = EOTestsHelper.creerTypeComposant(editingContext, 3, "AP", "AP");
		EOTypeAP typeAPTPCustom = EOTestsHelper.creerTypeAP(editingContext, 1, "TPCustom", "Custom", null);
		EOTypeAP typeAPTDCustom = EOTestsHelper.creerTypeAP(editingContext, 2, "TDCustom", "Custom", null);
		
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");
		EOTestsHelper.creerRegleLiaison(editingContext, typeParent, typeComposantAP, typeLienComposer);
		
		EOComposant parent = new EOComposantConcret("comp1", "comp1", 1, typeParent);
		editingContext.insertSavedObject(parent);
		
		EOAP apTP1 = EOTestsHelper.creerAP(editingContext, "apTP1", 0, null, typeAPTPCustom);
		EOAP apTP2 = EOTestsHelper.creerAP(editingContext, "apTP2", 0, null, typeAPTPCustom);
		EOAP apTP3 = EOTestsHelper.creerAP(editingContext, "apTP3", 0, null, typeAPTPCustom);
		EOAP apTD1 = EOTestsHelper.creerAP(editingContext, "apTD1", 0, null, typeAPTDCustom);
		EOAP apTD2 = EOTestsHelper.creerAP(editingContext, "apTD2", 0, null, typeAPTDCustom);
		
		EOTestsHelper.creerLienComposer(editingContext, 1, parent, apTP1, false);
		EOTestsHelper.creerLienComposer(editingContext, 2, parent, apTP2, false);
		EOTestsHelper.creerLienComposer(editingContext, 3, parent, apTP3, false);
		EOTestsHelper.creerLienComposer(editingContext, 4, parent, apTD1, false);
		EOTestsHelper.creerLienComposer(editingContext, 5, parent, apTD2, false);

	    // Assert
		IListeComposantEnfantsStrategy<EOAP> listeEnfantsApTPs= new ListeEnfantsAPs(typeAPTPCustom);
		assertEquals(3, listeEnfantsApTPs.getListEnfants(parent).size());

		IListeComposantEnfantsStrategy<EOAP> listeEnfantsApTDs= new ListeEnfantsAPs(typeAPTDCustom);
		assertEquals(2, listeEnfantsApTDs.getListEnfants(parent).size());		
    }

}
