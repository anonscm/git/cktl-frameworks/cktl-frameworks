package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSKeyValueCoding.UnknownKeyException;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;

import er.extensions.eof.ERXEnterpriseObjectCache;
import er.extensions.localization.ERXLocalizer;

/**
 * 
 * @author Alexis Tual
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ParametragesAffichageTest extends EOScolTestCase {

	private final static String codeChamp = "codeIne";
	private final static String codeParametrageInscription = EOParametrageInscription.INITIAL;
	
	private ParametragesAffichage parametragesAffichage;
	
	@Dummy
	private EOParametrageInscription parametrageInscription;
	@Dummy
	private EOTypeParametreAffichage typeGestionnaire;
	@Dummy
	private EOTypeParametreAffichage typeAdmission;
	@Dummy
	private EOTypeParametreAffichage typePreinscription;
	@Dummy
	private EOTypeParametreAffichage typReinscription;
	
	@Before
	public void setUp() {
		super.setUp();
		// Nécessaire pour le cache
		ERXEnterpriseObjectCache<EOParametrageChamp> mockCache = 
				EOTestsHelper.createCacheMock(EOParametrageChamp.class, EOParametrageChamp.TO_CHAMP.dot(EOChamp.CODE_KEY).key(), editingContext);
		mockCache.setRetainObjects(true);
		EOParametrageChamp.setChampCacheForParametrage(mockCache, EOParametrageInscription.INITIAL);
		EOChamp champ = EOChamp.createSco_Champ(editingContext, "APP", codeChamp, new NSTimestamp(), new NSTimestamp(), true, 0, 999, 999);
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		parametrageInscription.setLibelle(codeParametrageInscription);
		EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		ERXLocalizer erxLocalizer = new ERXLocalizer("French");
		parametragesAffichage = new ParametragesAffichage(editingContext, erxLocalizer, typeConnexion, codeParametrageInscription, false);
	}
	
	@Test
	public void testIsChampVisible1PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		assertTrue(parametragesAffichage.isChampVisible(codeChamp));
	}
	
	@Test
	public void testIsChampVisible2PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.MODIFIABLE);
		assertTrue(parametragesAffichage.isChampVisible(codeChamp));
	}
	
	@Test
	public void testIsChampVisible3PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.INVISIBLE);
		assertFalse(parametragesAffichage.isChampVisible(codeChamp));
	}
	
	@Test
	public void testIsChampNotModifiable1PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.INVISIBLE);
		assertTrue(parametragesAffichage.isChampNotModifiable(codeChamp));
	}
	
	@Test
	public void testIsChampNotModifiable2PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		assertTrue(parametragesAffichage.isChampNotModifiable(codeChamp));
	}
	
	@Test
	public void testIsChampNotModifiable3PourGestionnaire() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.MODIFIABLE);
		assertFalse(parametragesAffichage.isChampNotModifiable(codeChamp));
	}
	
	@Test
	public void testValueForKeyPathIsVisible() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		Boolean resultat = (Boolean)parametragesAffichage.valueForKeyPath("@"+codeChamp+".isVisible");
		assertTrue(resultat);
	}
	
	@Test
	public void testValueForKeyPathIsNotModifiable() {
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		Boolean resultat = (Boolean)parametragesAffichage.valueForKeyPath("@"+codeChamp+".isNotModifiable");
		assertTrue(resultat);
	}
	
	@Test
	public void testValueForKeyPathChampInconnu() {
		Boolean resultat = (Boolean) parametragesAffichage.valueForKeyPath("@wrongChamp.isVisible");
		assertFalse(resultat);
	}
	
	@Test(expected=UnknownKeyException.class)
	public void testValueForKeyPathWrong1() {
		parametragesAffichage.valueForKeyPath("@.");
	}
	
	@Test(expected=UnknownKeyException.class)
	public void testValueForKeyPathWrong2() {
		parametragesAffichage.valueForKeyPath("wrong.key");
	}
	
	@Test(expected=UnknownKeyException.class)
	public void testValueForKeyPathWrong3() {
		parametragesAffichage.valueForKeyPath("@"+codeChamp+".wrongagain");
	}
	
}
