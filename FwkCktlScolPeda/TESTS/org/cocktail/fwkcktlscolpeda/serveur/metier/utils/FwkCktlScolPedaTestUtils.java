package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.Calendar;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.wounit.annotations.Dummy;

public class FwkCktlScolPedaTestUtils extends EOScolTestCase {

	@Dummy
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);

	// EOTypeComposant
	private EOTypeComposant typeComposantDiplome;
	private EOTypeComposant typeComposantVersionDiplome;
	private EOTypeComposant typeComposantPeriode;
	private EOTypeComposant typeComposantRegroupement;
	private EOTypeComposant typeComposantEC;

	// EOTypeLien
	private EOTypeLien typeLienComposer;
	private EOTypeLien typeLienVersionner;

	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaire;

	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;

	// EOStatutFormation
	private EOStatutFormation statutFormationConstruction;
	private EOStatutFormation statutFormationProduction;
	private EOStatutFormation statutFormationFictif;

	// EODiplome
	private EODiplome diplome1;
	private EODiplome diplome2;
	private EODiplome diplome3;

	// EOVersionDiplome
	private EOVersionDiplome versionDiplome12012;
	private EOVersionDiplome versionDiplome12013;
	private EOVersionDiplome versionDiplome22013;
	private EOVersionDiplome versionDiplome32013;

	// EOPeriode
	private EOPeriode periodeLicence12012;
	private EOPeriode periodeLicence12013;
	private EOPeriode periodeLicence22013;
	private EOPeriode periodeLicence32013;

	// EORegroupement
	private EORegroupement regroupement12012;
	private EORegroupement regroupement12013;
	private EORegroupement regroupement22013;
	private EORegroupement regroupement32013;

	// EOEC
	private EOEC ecNonRelie2012;
	private EOEC ecNonRelie2013;

	private FwkCktlScolPedaUtils fwkCktlScolPedaUtils;

	private EOSiseDiscipline siseDiscipline;

	private EOSiseSecteurDisciplinaire secteurDisciplinaire;

	private EONiveauAccesDiplome niveauAccesDiplome;

	@Before
	public void setUp() {
		super.setUp();

		fwkCktlScolPedaUtils = new FwkCktlScolPedaUtils();

		fwkCktlScolPedaUtils.setAnneeDebutHabilitation(2013);
		fwkCktlScolPedaUtils.setDureeHabilitation(5);

		initialiserContexte();
	}

	/**
	 * Initialise le contexte necessaire à la création d'un composant de type Diplome
	 */
	private void initialiserContexte() {
		// EOTypeComposant
		typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM,
		    ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		typeComposantPeriode = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		typeComposantRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEREGROUPEMENT_NOM, ITypeComposant.TYPEREGROUPEMENT_NOM);
		typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);

		// EOTypeLien
		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "composer");
		typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 2, "versionner");

		// EORegleLiaison
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantRegroupement, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantRegroupement, typeComposantRegroupement, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantEC, typeLienComposer);

		// EOGradeUniversitaire
		gradeUniversitaire = EOTestsHelper.creerGradeUniversitaire(editingContext, "LICENCE", "Licence", 3);

		// EOTypeFormation
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");

		// EOStatutFormation
		statutFormationConstruction = EOTestsHelper.creerStatutFormation(editingContext, 1, "ECC", "En construction");
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		statutFormationFictif = EOTestsHelper.creerStatutFormation(editingContext, 3, "F", "Fictif");

		siseDiscipline = EOTestsHelper.creerSiseDiscipline(editingContext, "cdisc", "ldisc");
		secteurDisciplinaire = EOTestsHelper.creerSiseSecteurDisciplinaire(editingContext, "csect", "lsect", siseDiscipline);
		
		niveauAccesDiplome = EOTestsHelper.creerNiveauAccesDiplome(editingContext, "libelle", 0);
	}

	private void initialiserDiplomesEtComposants() {
		// Preparer des diplomes
		diplome1 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME1", "Le diplome 1", statutFormationProduction, gradeUniversitaire, typeFormationMaster,
		    year);
		diplome1.setId(1);
		diplome2 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME2", "Le diplome 2", statutFormationProduction, gradeUniversitaire, typeFormationMaster,
		    year);
		diplome2.setId(2);
		diplome3 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME3", "Le diplome 3", statutFormationProduction, gradeUniversitaire, typeFormationMaster,
		    year);
		diplome3.setId(3);

		// Preparer des versions de diplomes
		versionDiplome12012 = EOTestsHelper.creerVersionDiplome(editingContext, "CODEVERSIONDIPLOME12012", "La version du diplome 1 en 2012", year - 1);
		versionDiplome12012.setId(4);
		versionDiplome12013 = EOTestsHelper.creerVersionDiplome(editingContext, "CODEVERSIONDIPLOME12013", "La version du diplome 1 en 2013", year);
		versionDiplome12013.setId(5);
		versionDiplome22013 = EOTestsHelper.creerVersionDiplome(editingContext, "CODEVERSIONDIPLOME22013", "La version du diplome 2 en 2013", year);
		versionDiplome22013.setId(6);
		versionDiplome32013 = EOTestsHelper.creerVersionDiplome(editingContext, "CODEVERSIONDIPLOME32013", "La version du diplome 3 en 2013", year);
		versionDiplome32013.setId(7);

		// Préparer des periodes
		EOTypePeriode typePeriode = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);

		periodeLicence12012 = EOTestsHelper.creerPeriode(editingContext, "L12012", "Licence 1 en 2012", typePeriode);
		periodeLicence12012.setId(8);
		periodeLicence12013 = EOTestsHelper.creerPeriode(editingContext, "L12013", "Licence 1 en 2013", typePeriode);
		periodeLicence12013.setId(9);
		periodeLicence22013 = EOTestsHelper.creerPeriode(editingContext, "L22013", "Licence 2 en 2013", typePeriode);
		periodeLicence22013.setId(10);
		periodeLicence32013 = EOTestsHelper.creerPeriode(editingContext, "L32013", "Licence 3 en 2013", typePeriode);
		periodeLicence32013.setId(10);

		// Preparer des regroupements
		regroupement12012 = EOTestsHelper.creerRegroupement(editingContext, "CODEREGROUPEMENT12012", "Le regroupement 1 en 2012");
		regroupement12012.setId(11);
		regroupement12013 = EOTestsHelper.creerRegroupement(editingContext, "CODEREGROUPEMENT12013", "Le regroupement 1 en 2013");
		regroupement12013.setId(12);
		regroupement22013 = EOTestsHelper.creerRegroupement(editingContext, "CODEREGROUPEMENT22013", "Le regroupement 2 en 2013");
		regroupement22013.setId(13);
		regroupement32013 = EOTestsHelper.creerRegroupement(editingContext, "CODEREGROUPEMENT32013", "Le regroupement 3 en 2013");
		regroupement32013.setId(14);

		// Preparer des ecs
		ecNonRelie2012 = EOTestsHelper.creerEC(editingContext, "CODEECNONRELIE2012", "L'EC non relie en 2012");
		ecNonRelie2012.setId(15);
		ecNonRelie2013 = EOTestsHelper.creerEC(editingContext, "CODEECNONRELIE2013", "L'EC non relie en 2013");
		ecNonRelie2013.setId(16);

		// Creer les diplomes
		// Le diplome 1 en 2012
		EOComposantFactory.createLien(diplome1, versionDiplome12012, typeLienVersionner);
		EOComposantFactory.createLien(versionDiplome12012, periodeLicence12012, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence12012, regroupement12012, typeLienComposer);
		// Le diplome 1 en 2013
		EOComposantFactory.createLien(diplome1, versionDiplome12013, typeLienVersionner);
		EOComposantFactory.createLien(versionDiplome12013, periodeLicence12013, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence12013, regroupement12013, typeLienComposer);
		// Le diplome 2 en 2013
		EOComposantFactory.createLien(diplome2, versionDiplome22013, typeLienVersionner);
		EOComposantFactory.createLien(versionDiplome22013, periodeLicence22013, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence22013, regroupement22013, typeLienComposer);
		// Le diplome 3 en 2013
		EOComposantFactory.createLien(diplome3, versionDiplome32013, typeLienVersionner);
		EOComposantFactory.createLien(versionDiplome32013, periodeLicence32013, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence32013, regroupement22013, typeLienComposer); // regroupement22013 mutualise avec le diplome 2 en 2013
		EOComposantFactory.createLien(periodeLicence32013, regroupement32013, typeLienComposer);
	}

	@Ignore
	@Test
	public void test_getDiplomesAVersionner() {
		// Arrange
		Integer anneeDepart = 2013;
		EODiplome diplome1 = creerEtInitialiserDiplome(anneeDepart, "d1", "habilitable en construction", 1, statutFormationConstruction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome2 = creerEtInitialiserDiplome(anneeDepart, "d2", "habilitable en production", 1, statutFormationProduction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome3 = creerEtInitialiserDiplome(anneeDepart, "d3", "non habilitable en construction", 0, statutFormationConstruction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome4 = creerEtInitialiserDiplome(anneeDepart, "d4", "non habilitable en production", 0, statutFormationProduction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome5 = creerEtInitialiserDiplome(anneeDepart, "d5", "non habilitable fictif", 0, statutFormationFictif, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome6 = creerEtInitialiserDiplome(anneeDepart, "d6", "habilitable fictif", 1, statutFormationFictif, gradeUniversitaire, typeFormationMaster);
		Integer anneeDebutHabilitation = fwkCktlScolPedaUtils.getAnneeDebutHabilitation();
		Integer anneeFinHabilitation = fwkCktlScolPedaUtils.getAnneeDebutHabilitation() + fwkCktlScolPedaUtils.getDureeHabilitation();

		// Assert Ready to Test
		assertNotNull(diplome1);
		assertNotNull(diplome2);
		assertNotNull(diplome3);
		assertNotNull(diplome4);
		assertNotNull(diplome5);
		assertNotNull(diplome6);

		// Act
		diplome1.enregistrer(anneeDepart, 0);
		EOTestsHelper.setValidateur(diplome1);
		diplome2.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome2);
		diplome3.enregistrer(anneeDepart, 0);
		EOTestsHelper.setValidateur(diplome3);
		diplome4.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome4);
		diplome5.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome5);
		diplome6.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome6);

		// Assert
		NSArray<EODiplome> diplomesAVersionner;
		for (int annee = anneeDebutHabilitation; annee < anneeFinHabilitation + 2; annee++) {
			diplomesAVersionner = fwkCktlScolPedaUtils.getDiplomesAVersionner(editingContext, anneeDepart, annee);
			assertEquals(4, diplomesAVersionner.count());
		}

		diplomesAVersionner = fwkCktlScolPedaUtils.getDiplomesAVersionner(editingContext, anneeDepart, anneeFinHabilitation + 3);
		assertEquals(2, diplomesAVersionner.count());

	}

	@Ignore
	@Test
	public void test_getDiplomesAHabiliter() {
		// Arrange
		Integer anneeReference = 2013;
		Integer anneeCible = 2015;
		EODiplome diplome1 = creerEtInitialiserDiplome(anneeReference, "d1", "habilitable en construction", 1, statutFormationConstruction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome2 = creerEtInitialiserDiplome(anneeReference, "d2", "habilitable en production", 1, statutFormationProduction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome3 = creerEtInitialiserDiplome(anneeReference, "d3", "non habilitable en construction", 0, statutFormationConstruction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome4 = creerEtInitialiserDiplome(anneeReference, "d4", "non habilitable en production", 0, statutFormationProduction, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome5 = creerEtInitialiserDiplome(anneeReference, "d5", "non habilitable fictif", 0, statutFormationFictif, gradeUniversitaire,
		    typeFormationMaster);
		EODiplome diplome6 = creerEtInitialiserDiplome(anneeReference, "d6", "habilitable fictif", 1, statutFormationFictif, gradeUniversitaire,
		    typeFormationMaster);
		Integer anneeDebutHabilitation = fwkCktlScolPedaUtils.getAnneeDebutHabilitation();
		Integer anneeFinHabilitation = fwkCktlScolPedaUtils.getAnneeDebutHabilitation() + fwkCktlScolPedaUtils.getDureeHabilitation();

		// Assert Ready to Test
		assertNotNull(diplome1);
		assertNotNull(diplome2);
		assertNotNull(diplome3);
		assertNotNull(diplome4);
		assertNotNull(diplome5);
		assertNotNull(diplome6);

		// Act
		diplome1.enregistrer(anneeReference, 0);
		EOTestsHelper.setValidateur(diplome1);
		diplome2.enregistrer(anneeReference, 1);
		EOTestsHelper.setValidateur(diplome2);
		diplome3.enregistrer(anneeReference, 0);
		EOTestsHelper.setValidateur(diplome3);
		diplome4.enregistrer(anneeReference, 1);
		EOTestsHelper.setValidateur(diplome4);
		diplome5.enregistrer(anneeReference, 1);
		EOTestsHelper.setValidateur(diplome5);
		diplome6.enregistrer(anneeReference, 1);
		EOTestsHelper.setValidateur(diplome6);

		// Assert
		NSArray<EODiplome> diplomesAHabiliter = fwkCktlScolPedaUtils.getDiplomesAHabiliter(editingContext, anneeReference, anneeCible);
		assertEquals(1, diplomesAHabiliter.count());

	}

	private EODiplome creerEtInitialiserDiplome(Integer anneeDepart, String code, String libelle, Integer habilitationRequise, EOStatutFormation statutFormation,
	    EOGradeUniversitaire gradeUniversitaire, EOTypeFormation typeFormation) {
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, code, libelle, statutFormation, gradeUniversitaire, typeFormation, year);
		diplome.setValidateur(new CodeValidateurSaisieObligatoire());
		editingContext.insertObject(diplome);

		diplome.setHabilitationRequise(habilitationRequise);
		// diplome.setGradeUniversitaireRelationship(gradeUniversitaire);

		diplome.setSecteurDisciplinaireSiseRelationship(secteurDisciplinaire);
		diplome.setNiveauAccesDiplomeRelationship(niveauAccesDiplome);
		return diplome;
	}

	/**
	 * Squence à but de test
	 * @author jlafourc
	 */
	public static class DummySequence {

		private Integer currentValue;

		/**
		 * Contructeur par défaut Initialise la séquence à 0
		 */
		public DummySequence() {
			this.currentValue = 0;
		}

		/**
		 * Ce constructeur prend une valeur initale
		 * @param initialValue la valeur initiale
		 */
		public DummySequence(Integer initialValue) {
			this.currentValue = initialValue;
		}

		public Integer getCurrentValue() {
			return currentValue;
		}

		public Integer getNextVal() {
			return currentValue++;
		}
	}

}
