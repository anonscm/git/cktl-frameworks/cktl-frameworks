package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.DiplomeSimpleContainer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.TestsInitializer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.FwkCktlScolPedaTestUtils.DummySequence;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.scol.maquette.RegimeInscriptionRead;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class InscriptionPedagogiqueServiceImplTest extends EOScolTestCase {
    
    private InscriptionPedagogiqueServiceImpl inscriptionPedagogiqueService;
    private DiplomeSimpleContainer diplome;
    private EOInscriptionPedagogique inscriptionPeda;
    private ITypeInscriptionPeda typeIp;
		private TestsInitializer testsInitiator;
		private DummySequence sequenceLiens;      
		private DummySequence sequenceComposants;
		private int annee;  
    
    @Before
    public void setUp() {
        super.setUp();
        sequenceLiens = new DummySequence();
        sequenceComposants = new DummySequence();
        testsInitiator = new TestsInitializer(editingContext);
        annee = Calendar.getInstance().get(Calendar.YEAR);
        diplome = EOTestsHelper.creerDiplomeSimple(testsInitiator, sequenceLiens, annee);
        diplome.getDiplome();
        
        inscriptionPedagogiqueService = new InscriptionPedagogiqueServiceImpl(editingContext);
        
        UserInfo userInfo = Mockito.mock(UserInfo.class);
        Mockito.when(userInfo.persId()).thenReturn(new Integer(10));
        inscriptionPedagogiqueService.setUserInfo(userInfo);
        
        EOQualifier qualifier = EOTypeInscriptionPeda.CODE.eq(ITypeInscriptionPeda.CODE_STANDARD);
        typeIp = EOTypeInscriptionPeda.fetchSco_TypeInscriptionPeda(editingContext, qualifier);
        
        EOInscription inscription = editingContext.createSavedObject(EOInscription.class);
        inscription.setToDiplomeRelationship((EODiplome) diplome.getDiplome());
        inscription.setAnnee(annee);
        
        inscriptionPeda = editingContext.createSavedObject(EOInscriptionPedagogique.class);
        inscriptionPeda.setToInscriptionRelationship(inscription);                
    }
    
    @Test
    public void testCreerIpsPourIa() {
	    // Arrange 
    	ITypeEtudiant typeEtudiant = EOTestsHelper.creerTypeEtudiant(editingContext, "TE", "typeEtudiant");
    	EODiplome undiplome = EOTestsHelper.creerDiplomePourIP(testsInitiator, sequenceLiens, sequenceComposants, annee);
    	EOVersionDiplome versionDiplome = (EOVersionDiplome) undiplome.getVersionDiplome(annee);
    	EOPeriode annee1 = (EOPeriode) versionDiplome.childs().get(0);
    	String idLienContext = String.format("/%d/%d", undiplome.id(), versionDiplome.id());
    	String idLienContextAnnee = String.format("/%d/%d/%d", undiplome.id(), versionDiplome.id(), annee1.id());
    	EOInscription inscription= EOTestsHelper.creerInscription(editingContext, undiplome, idLienContext, idLienContextAnnee);  	
    	inscription.setToDiplomeRelationship(undiplome);
      inscription.setNiveau(1);
               
	    // Act 
      inscriptionPedagogiqueService.creerIpsPourIa(editingContext, inscription, 1, typeEtudiant);
      
	    // Assert
      NSArray<EOInscriptionPedagogique> ips = EOInscriptionPedagogique.fetchSco_InscriptionPedagogiques(editingContext, EOInscriptionPedagogique.TO_INSCRIPTION.eq(inscription), null);
      
      assertThat(ips.size()).isEqualTo(3);
      EOInscriptionPedagogique ipPrincipale = Iterables.find(ips, new Predicate<EOInscriptionPedagogique>() {
  			public boolean apply(EOInscriptionPedagogique input) {
  	      return input.principale();
        }
  		});
      assertThat(ipPrincipale).isNotNull();
      assertThat(ipPrincipale.toInscriptionPedagogiqueElements().size()).isEqualTo(2);
           
    }
  
	
    @Test
    public void testSupprimerIpElementSimple() {
        IInscriptionPedagogiqueElement element = creerMockIpElement(diplome.getLienSemestre1Ec1());
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(1);

        inscriptionPedagogiqueService.supprimerIpElementSimple(element);
        editingContext.saveChanges();
        
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(0);
    }
    
    @Test
    public void testSupprimerIpElementParentsEtEnfants() {
        creerMockIpElement(diplome.getLienDiplomeVersionDiplome());
        creerMockIpElement(diplome.getLienVersionDiplomeL1());
        creerMockIpElement(diplome.getLienL1Semestre1());
        IInscriptionPedagogiqueElement element = creerMockIpElement(diplome.getLienSemestre1Ec1());
        creerMockIpElement(diplome.getLienEc1Ap1());
        creerMockIpElement(diplome.getLienAp1Ae1());
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(6);

        inscriptionPedagogiqueService.supprimerIpElement(element);
        editingContext.saveChanges();
        
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(0);
    }
    
    @Test
    public void testSupprimerIpElementParentsEtEnfantsSiIpDejaPresente() {
        IInscriptionPedagogiqueElement elementVD = creerMockIpElement(diplome.getLienDiplomeVersionDiplome());
        IInscriptionPedagogiqueElement elementL1 = creerMockIpElement(diplome.getLienVersionDiplomeL1());
        IInscriptionPedagogiqueElement elementS1 = creerMockIpElement(diplome.getLienL1Semestre1());
        IInscriptionPedagogiqueElement elementEc1 = creerMockIpElement(diplome.getLienSemestre1Ec1());
        IInscriptionPedagogiqueElement elementEc2 = creerMockIpElement(diplome.getLienSemestre1Ec2());
        creerMockIpElement(diplome.getLienEc1Ap1());
        creerMockIpElement(diplome.getLienAp1Ae1());
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(7);
        
        inscriptionPedagogiqueService.supprimerIpElement(elementEc1);
        editingContext.saveChanges();
        
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).hasSize(4);
        assertThat(inscriptionPeda.toInscriptionPedagogiqueElements()).containsOnly(
                (EOInscriptionPedagogiqueElement) elementVD,
                (EOInscriptionPedagogiqueElement) elementL1,
                (EOInscriptionPedagogiqueElement) elementS1,
                (EOInscriptionPedagogiqueElement) elementEc2
                );
        
    }
    
    private IInscriptionPedagogiqueElement creerMockIpElement(ILien lien) {
    	return InscriptionPedagogiqueTestsHelper.creerMockIpElement(editingContext, inscriptionPeda, lien, typeIp);
	}

}