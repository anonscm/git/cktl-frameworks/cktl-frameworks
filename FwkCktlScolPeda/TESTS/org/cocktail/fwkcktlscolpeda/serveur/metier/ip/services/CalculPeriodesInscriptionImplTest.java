package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Test;

public class CalculPeriodesInscriptionImplTest {

	private static final String ID_LIEN_CONSULTATION_DIPLOME = "/1/2";
	private static final String ID_LIEN_CONSULTATION_ANNEE = "/1/2/6";
	private static final String ID_LIEN_CONSULTATION_ANNEE_SEMESTRE1 = "/1/2/6/12";
	private static final String ID_LIEN_CONSULTATION_ANNEE_SEMESTRE2 = "/1/2/6/14";
	private static final String ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE = "/1/2/6/9";
	private static final String ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE1 = "/1/2/6/9/12";
	private static final String ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE2 = "/1/2/6/9/14";
	private static final String ID_LIEN_CONSULTATION_DIPLOME_PARCOURS_DIPLOME = "/1/3";
	private CalculPeriodesInscription calculPeriodesInscription;

	@Before
	public void setUp() {
		calculPeriodesInscription = spy(new CalculPeriodesInscriptionImpl(null));
	}

	// Structure du diplome
	// diplome
	// =>versionDiplome
	// => annee 1
	// => semestre 1
	// => semestre 2
	@Test
	public void getPeriodes_SansParcours() {
		// Arrange
		IScoInscription inscription = mock(IScoInscription.class);
		when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
		when(inscription.idLienConsultationAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE);
		ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
		ILienConsultation lienConsultVersionAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE);
		ILienConsultation lienConsultAnneeSemestre1 = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE_SEMESTRE1);
		ILienConsultation lienConsultAnneeSemestre2 = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE_SEMESTRE2);
		Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
		liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE, lienConsultVersionAnnee);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE_SEMESTRE1, lienConsultAnneeSemestre1);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE_SEMESTRE1, lienConsultAnneeSemestre2);

		((CalculPeriodesInscriptionImpl) doReturn(Arrays.asList(lienConsultAnneeSemestre1, lienConsultAnneeSemestre2)).when(calculPeriodesInscription))
		    .getLiensSous(lienConsultVersionAnnee, ITypeComposant.TYPEPERIODE_NOM);

		// Act
		List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
		List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
		// Assert
		assertThat(liensPeriodes).hasSize(2);
		assertThat(liensPeriodes).containsExactly(lienConsultAnneeSemestre1, lienConsultAnneeSemestre2);
		assertThat(lienPourIpSecondaires).hasSize(2);
		assertThat(lienPourIpSecondaires).containsExactly(lienConsultAnneeSemestre1, lienConsultAnneeSemestre2);
	}

	// Structure du diplome
	// diplome
	// =>versionDiplome
	// => annee 1
	// => parcoursAnnee 1
	// => semestre 1
	// => semestre 2
	@Test
	public void getPeriodes_AvecParcours() {
		// Arrange
		IScoInscription inscription = mock(IScoInscription.class);
		when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
		when(inscription.idLienConsultationAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE);
		when(inscription.idLienConsultationParcoursAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE);
		ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
		ILienConsultation lienConsultVersionAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE);
		ILienConsultation lienConsultAnneeParcoursAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE);
		ILienConsultation lienConsultParcoursAnneeSemestre1 = getLienConsultation(ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE1);
		ILienConsultation lienConsultParcoursAnneeSemestre2 = getLienConsultation(ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE2);
		Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
		liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE, lienConsultVersionAnnee);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE, lienConsultAnneeParcoursAnnee);
		liensInscription.put(ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE1, lienConsultParcoursAnneeSemestre1);
		liensInscription.put(ID_LIEN_CONSULTATION_PARCOURS_ANNEE_SEMESTRE2, lienConsultParcoursAnneeSemestre2);

		((CalculPeriodesInscriptionImpl) doReturn(
		    Arrays.asList(lienConsultParcoursAnneeSemestre1, lienConsultParcoursAnneeSemestre2)).when(calculPeriodesInscription)).getLiensSous(
		    lienConsultAnneeParcoursAnnee, ITypeComposant.TYPEPERIODE_NOM);

		// Act
		List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
		List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
		// Assert
		assertThat(liensPeriodes).hasSize(2);
		assertThat(liensPeriodes).containsExactly(lienConsultParcoursAnneeSemestre1, lienConsultParcoursAnneeSemestre2);
		assertThat(lienPourIpSecondaires).hasSize(2);
		assertThat(lienPourIpSecondaires).containsExactly(lienConsultParcoursAnneeSemestre1, lienConsultParcoursAnneeSemestre2);
	}

	// Structure du diplome
	// diplome
	// =>versionDiplome
	// => annee 1
	@Test
	public void getPeriodes_SansSemestre() {
		// Arrange
		IScoInscription inscription = mock(IScoInscription.class);
		when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
		when(inscription.idLienConsultationAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE);
		ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
		ILienConsultation lienConsultVersionAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE);
		Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
		liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE, lienConsultVersionAnnee);

		((CalculPeriodesInscriptionImpl) doReturn(new ArrayList<ILienConsultation>()).when(calculPeriodesInscription)).getLiensSous(lienConsultVersionAnnee, ITypeComposant.TYPEPERIODE_NOM);

		// Act
		List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
		List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
		// Assert
		assertThat(liensPeriodes).isEmpty();
		assertThat(lienPourIpSecondaires).containsExactly(lienConsultVersionAnnee);
	}

	// Structure du diplome
	// diplome
	// =>versionDiplome
	// => annee 1
	// => parcoursAnnee
	@Test
	public void getPeriodes_SansSemestreAvecParcoursAnnee() {
		// Arrange
		IScoInscription inscription = mock(IScoInscription.class);
		when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
		when(inscription.idLienConsultationAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE);
		when(inscription.idLienConsultationParcoursAnnee()).thenReturn(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE);
		ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
		ILienConsultation lienConsultVersionAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE);
		ILienConsultation lienConsultAnneeParcoursAnnee = getLienConsultation(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE);
		Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
		liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE, lienConsultVersionAnnee);
		liensInscription.put(ID_LIEN_CONSULTATION_ANNEE_PARCOURS_ANNEE, lienConsultAnneeParcoursAnnee);

		((CalculPeriodesInscriptionImpl) doReturn(new ArrayList<ILienConsultation>()).when(calculPeriodesInscription))
		    .getLiensSous(lienConsultAnneeParcoursAnnee, ITypeComposant.TYPEPERIODE_NOM);

		// Act
		 List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
		 List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
		 // Assert
		 assertThat(liensPeriodes).isEmpty();
		 assertThat(lienPourIpSecondaires).containsExactly(lienConsultAnneeParcoursAnnee);
	}
	
	// Structure du diplome
		// diplome
		// =>parcoursDiplome
		@Test
		public void getPeriodes_SansSemestreAvecParcoursDiplome() {
			// Arrange
			IScoInscription inscription = mock(IScoInscription.class);
			when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
			when(inscription.idLienConsultationParcoursDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME_PARCOURS_DIPLOME);
			ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
			ILienConsultation lienConsultParcoursDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME_PARCOURS_DIPLOME);
			Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
			liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);
			liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME_PARCOURS_DIPLOME, lienConsultParcoursDiplome);

			// Act
			 List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
			 List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
			 // Assert
			 assertThat(liensPeriodes).isEmpty();
			 assertThat(lienPourIpSecondaires).containsExactly(lienConsultParcoursDiplome);
		}
		
		// Structure du diplome
		// diplome
		// =>parcoursDiplome
		@Test
		public void getPeriodes_SansRien() {
			// Arrange
			IScoInscription inscription = mock(IScoInscription.class);
			when(inscription.idLienConsultationDiplome()).thenReturn(ID_LIEN_CONSULTATION_DIPLOME);
			ILienConsultation lienConsultDiplomeVersionDiplome = getLienConsultation(ID_LIEN_CONSULTATION_DIPLOME);
			Map<String, ILienConsultation> liensInscription = new HashMap<String, ILienConsultation>();
			liensInscription.put(ID_LIEN_CONSULTATION_DIPLOME, lienConsultDiplomeVersionDiplome);

			// Act
			 List<ILienConsultation> liensPeriodes = calculPeriodesInscription.getPeriodesSemestre(inscription, liensInscription);
			 List<ILienConsultation> lienPourIpSecondaires = calculPeriodesInscription.getLienPourIpSecondaires(inscription, liensInscription);
			 // Assert
			 assertThat(liensPeriodes).isEmpty();
			 assertThat(lienPourIpSecondaires).containsExactly(lienConsultDiplomeVersionDiplome);
		}		

	private ILienConsultation getLienConsultation(String idLienConsultation) {
		ILienConsultation lienConsultation = mock(ILienConsultation.class);
		when(lienConsultation.idLienContexte()).thenReturn(idLienConsultation);
		return lienConsultation;
	}

}
