package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionPeda;

import com.wounit.rules.MockEditingContext;

public class InscriptionPedagogiqueTestsHelper {

    public static IInscriptionPedagogiqueElement creerMockIpElement(MockEditingContext editingContext, IInscriptionPedagogique inscriptionPeda, ILien lien, ITypeInscriptionPeda typeIp) {
        IInscriptionPedagogiqueElement element = editingContext.createSavedObject(EOInscriptionPedagogiqueElement.class);
        element.setToInscriptionPedagogiqueRelationship(inscriptionPeda);
        element.setToLienRelationship(lien);
        element.setToTypeInscriptionPedaRelationship(typeIp);
        return element;
    }
	
}
