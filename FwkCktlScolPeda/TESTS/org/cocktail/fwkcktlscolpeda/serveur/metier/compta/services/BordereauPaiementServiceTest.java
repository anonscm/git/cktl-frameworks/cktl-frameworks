package org.cocktail.fwkcktlscolpeda.serveur.metier.compta.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(value = MockitoJUnitRunner.class)
public class BordereauPaiementServiceTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("Sco_scolarite");
	
	BordereauPaiementService serviceBordereauPaiement;

	@Dummy
	private EOPaiement prePaiementComptant;
	
	@Dummy
	private EOPaiementDetail detailPaiementComptant1;
	
	@Before
	public void setUp() {
		UserInfo userInfo = Mockito.mock(UserInfo.class);
		Mockito.when(userInfo.persId()).thenReturn(new Integer(10));

		serviceBordereauPaiement = new BordereauPaiementService(ec, userInfo);
		
		detailPaiementComptant1.setToPaiementRelationship(prePaiementComptant);
		detailPaiementComptant1.setMontantAPayer(new BigDecimal("25.00"));
	}
	
	@Test
	public void validerPaiement() {

		serviceBordereauPaiement.validerPaiement(prePaiementComptant);
		assertThat(prePaiementComptant.paiementValide()).isTrue();
		assertThat(prePaiementComptant.datePaiement()).isNotNull();

		assertThat(detailPaiementComptant1.montantAPayer()).isNull();
		assertThat(detailPaiementComptant1.montantPaye()).isEqualTo(new BigDecimal("25.00"));

	}

	@Test
	public void annulerPaiement() {
		// Arrange
		serviceBordereauPaiement.validerPaiement(prePaiementComptant);
		// Assert ready to test

		assertThat(prePaiementComptant.paiementValide()).isTrue();
		assertThat(prePaiementComptant.datePaiement()).isNotNull();
		// Act

		serviceBordereauPaiement.annulerPaiement(prePaiementComptant);
		// Assert
		assertThat(prePaiementComptant.paiementValide()).isFalse();
		assertThat(prePaiementComptant.datePaiement()).isNull();

		assertThat(detailPaiementComptant1.montantPaye()).isNull();
		assertThat(detailPaiementComptant1.montantAPayer()).isEqualTo(new BigDecimal("25.00"));
	}
}
