package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement.NoteFinaleCalculator;
import org.junit.Before;
import org.junit.Test;

public class EOInscriptionPedagogiqueElementTest {

	private ITypeNote typeNoteCalculee;
	private ITypeNote typeNoteSaisie;
	
	private INoteResultat noteResultatAdmis;

	@Before
	public void setUp() {
		typeNoteCalculee = mock(ITypeNote.class);
		when(typeNoteCalculee.code()).thenReturn(ITypeNote.NOTE_CALCULEE);

		typeNoteSaisie = mock(ITypeNote.class);
		when(typeNoteSaisie.code()).thenReturn(ITypeNote.NOTE_SAISIE);
		
		noteResultatAdmis = mock(INoteResultat.class);
		when(noteResultatAdmis.code()).thenReturn(INoteResultat.ADMIS);
		
	}

	@Test
	public void testNoteFinaleSession3PriseEnCompte() {
		INote mockNote3 = mockNote("12", 3, typeNoteCalculee);
		List<? extends INote> notes = Arrays.asList(
				mockNote("10", 1, typeNoteCalculee),
				mockNote("11", 2, typeNoteCalculee),
				mockNote3);
		IInscriptionPedagogiqueElement ipElement = mock(IInscriptionPedagogiqueElement.class);
		doReturn(notes).when(ipElement).toNotes();

		INote noteFinale = new NoteFinaleCalculator().noteFinale(ipElement);
		
		assertThat(noteFinale).isEqualTo(mockNote3);
	}
	
	@Test
	public void testNoteFinaleNoteSaisiePriseEnCompte() {
		INote noteCalculee = mockNote("12", 1, typeNoteCalculee);
		INote noteSaisie = mockNote("10", 1, typeNoteSaisie);
		List<? extends INote> notes = Arrays.asList(noteSaisie, noteCalculee);
		IInscriptionPedagogiqueElement ipElement = mock(IInscriptionPedagogiqueElement.class);
		doReturn(notes).when(ipElement).toNotes();
		
		INote noteFinale = new NoteFinaleCalculator().noteFinale(ipElement);
		
		assertThat(noteFinale).isEqualTo(noteSaisie);
	}

	@Test
	public void testNoteFinaleNonSaisie() {
		INote noteCalculee = mockNote("12", 1, typeNoteCalculee);
		INote noteSaisie = mockNote(null, 1, typeNoteSaisie);
		List<? extends INote> notes = Arrays.asList(noteSaisie, noteCalculee);
		IInscriptionPedagogiqueElement ipElement = mock(IInscriptionPedagogiqueElement.class);
		doReturn(notes).when(ipElement).toNotes();
		
		INote noteFinale = new NoteFinaleCalculator().noteFinale(ipElement);
		
		assertThat(noteFinale).isEqualTo(noteCalculee);
	}
	
	@Test
	public void testNoteFinaleResultatNonSaisie() {
		INote noteCalculee = mockNoteResultat(1, typeNoteCalculee, noteResultatAdmis);
		INote noteSaisie = mockNoteResultat(1, typeNoteSaisie, null);
		List<? extends INote> notes = Arrays.asList(noteSaisie, noteCalculee);
		IInscriptionPedagogiqueElement ipElement = mock(IInscriptionPedagogiqueElement.class);
		doReturn(notes).when(ipElement).toNotes();
		
		INote noteFinale = new NoteFinaleCalculator().noteResultatFinale(ipElement);
		
		assertThat(noteFinale).isEqualTo(noteCalculee);
	}

	private INote mockNoteResultat(int noSession, ITypeNote typeNote, INoteResultat noteResultat) {
		INote note = mockNote(null, noSession, typeNote);
		when(note.toNoteResultat()).thenReturn(noteResultat);
		return note;
	}
	
	private INote mockNote(String valeurNote, int noSession, ITypeNote typeNote) {
		INote note = mock(INote.class);
		when(note.noSession()).thenReturn(noSession);
		if (valeurNote != null) {
			when(note.noteInitiale()).thenReturn(new BigDecimal(valeurNote));
		}
		when(note.toTypeNote()).thenReturn(typeNote);
		return note;
	}

}
