package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Test;

/**
 * Tests de la classe EOSessionComposant
 */
public class EOSessionComposantTest extends EOScolTestCase {

	@Test
	public void test_copy() {
		// Arrange
		EOSessionComposant session = EOTestsHelper.creerSession(editingContext, "session");
		session.setActive(1);
		session.setSeuil(10);
		session.setLibelle("session");
		for (int i = 0; i < 2; i++) {
			EOFormule formule = EOTestsHelper.creerFormule(editingContext);
			formule.setSessionComposantRelationship(session);
		}
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		EOSessionComposant newSession = session.copy();

		// Assert
		assertEquals(session.active(), newSession.active());
		assertEquals(session.seuil(), newSession.seuil());
		assertEquals(session.libelle(), newSession.libelle());
		assertEquals(session.formules().count(), newSession.formules().count());

	}
}
