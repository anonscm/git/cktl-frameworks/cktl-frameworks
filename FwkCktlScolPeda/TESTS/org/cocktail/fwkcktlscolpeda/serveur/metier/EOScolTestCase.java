package org.cocktail.fwkcktlscolpeda.serveur.metier;

import junit.framework.TestCase;

import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

import er.extensions.foundation.ERXProperties;

public class EOScolTestCase extends TestCase {
	protected EOTypeComposant typeComposantConcret;
	private static UserInfo userInfo;

	@Rule
	public MockEditingContext editingContext = new MockEditingContext("Sco_scolarite");

	@Before
	public void setUp() {
		ERXProperties.setStringForKey("true", "er.extensions.ERXEC.safeLocking");

		// ERXProperties.setStringForKey("org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurGenereAuto",
		// "Validateur");//Validateur=org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurGenereAuto

		// On initialise les caches mock
		EOTestsHelper.initialiserAllCacheMock(editingContext);

		typeComposantConcret = EOTestsHelper.creerTypeComposant(editingContext, 1, "Concret", "Concret");
	}

	public void initAllTypesComposants() {
		EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAE_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEAP_NOM, ITypeComposant.TYPEAP_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEUE_NOM, ITypeComposant.TYPEUE_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEREGROUPEMENT_NOM, ITypeComposant.TYPEREGROUPEMENT_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 6, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 7, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 8, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTestsHelper.creerTypeComposant(editingContext, 9, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
	}

	@Test
	public void testDummy() {
		assertTrue(true);
	}

	public static UserInfo getUserInfo() {
		if (userInfo == null) {
			userInfo = new UserInfo() {

				public String prenom() {
					// TODO Auto-generated method stub
					return "userInfoPrenom";
				}

				public Number persId() {
					// TODO Auto-generated method stub
					return 42;
				}

				public String nomEtPrenom() {
					// TODO Auto-generated method stub
					return "userInfoNomEtPrenom";
				}

				public String nom() {
					// TODO Auto-generated method stub
					return "userInfoNom";
				}

				public Number noIndividu() {
					// TODO Auto-generated method stub
					return 100;
				}
			};
		}

		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * @param newComposant
	 */
	protected void assertComposantsIdentiques(EOComposant composant, EOComposant newComposant) {
		assertEquals(composant.code(), newComposant.code());
		assertEquals(composant.libelle(), newComposant.libelle());

		assertEquals(composant, newComposant.composantOrigine());

		assertEquals(composant.tagApplication(), newComposant.tagApplication());
		// relations
		assertEquals(composant.responsablesComposant().count(), newComposant.responsablesComposant().count());
		assertEquals(composant.typeComposant(), newComposant.typeComposant());

		// relations many to many
		assertEquals(composant.structures().count(), composant.structures().count());

		assertEquals(composant.implantationsGeos().count(), newComposant.implantationsGeos().count());
	}
}
