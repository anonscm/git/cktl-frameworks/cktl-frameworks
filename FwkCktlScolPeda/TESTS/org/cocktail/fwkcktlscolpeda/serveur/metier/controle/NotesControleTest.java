package org.cocktail.fwkcktlscolpeda.serveur.metier.controle;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Test;

public class NotesControleTest {

	
	
	@Test
	public void controleNoteBaseNoteAENotOK() {
		INote note = mock(INote.class);
		when(note.noteInitiale()).thenReturn(new BigDecimal(25));
		IInscriptionPedagogiqueElement inscripPeda = mock(IInscriptionPedagogiqueElement.class);
		EOLien lien = mock(EOLien.class);
		when(note.toInscriptionPedagogiqueElement()).thenReturn(inscripPeda);
		when(note.toInscriptionPedagogiqueElement().toLien()).thenReturn(lien);
		
		EOAE ae = mock(EOAE.class);
		when(ae.baseNote()).thenReturn(20);
	
		EOTypeComposant typeComposant = mock(EOTypeComposant.class);
		when(typeComposant.libelle()).thenReturn(ITypeComposant.TYPEAE_NOM);
		when(ae.typeComposant()).thenReturn(typeComposant);
		when(note.toInscriptionPedagogiqueElement().toLien().child()).thenReturn(ae);
	
		NotesControle controle = new NotesControle();
		assertTrue(!controle.controleNoteBaseNote(note));
	
	}

	@Test
	public void controleNoteBaseNoteAEOK() {
		INote note = mock(INote.class);
		when(note.noteInitiale()).thenReturn(new BigDecimal(18));
		IInscriptionPedagogiqueElement inscripPeda = mock(IInscriptionPedagogiqueElement.class);
		EOLien lien = mock(EOLien.class);
		when(note.toInscriptionPedagogiqueElement()).thenReturn(inscripPeda);
		when(note.toInscriptionPedagogiqueElement().toLien()).thenReturn(lien);
		
		EOAE ae = mock(EOAE.class);
		when(ae.baseNote()).thenReturn(20);
	
		EOTypeComposant typeComposant = mock(EOTypeComposant.class);
		when(typeComposant.libelle()).thenReturn(ITypeComposant.TYPEAE_NOM);
		when(ae.typeComposant()).thenReturn(typeComposant);
		when(note.toInscriptionPedagogiqueElement().toLien().child()).thenReturn(ae);
	
		NotesControle controle = new NotesControle();
		assertTrue(controle.controleNoteBaseNote(note));
	
	}
	
	@Test
	public void controleNoteBaseNoteECNotOK() {
		INote note = mock(INote.class);
		when(note.noteInitiale()).thenReturn(new BigDecimal(25));
		IInscriptionPedagogiqueElement inscripPeda = mock(IInscriptionPedagogiqueElement.class);
		EOLien lien = mock(EOLien.class);
		when(note.toInscriptionPedagogiqueElement()).thenReturn(inscripPeda);
		when(note.toInscriptionPedagogiqueElement().toLien()).thenReturn(lien);
		
		EOEC ec = mock(EOEC.class);
		EOCreditable creditable = mock(EOCreditable.class);
		when(creditable.baseNote()).thenReturn(20);
		when(ec.creditable()).thenReturn(creditable);
	
		EOTypeComposant typeComposant = mock(EOTypeComposant.class);
		when(typeComposant.libelle()).thenReturn(ITypeComposant.TYPEEC_NOM);
		when(ec.typeComposant()).thenReturn(typeComposant);
		when(note.toInscriptionPedagogiqueElement().toLien().child()).thenReturn(ec);
	
		NotesControle controle = new NotesControle();
		assertTrue(!controle.controleNoteBaseNote(note));
	
	}
	
	
}
