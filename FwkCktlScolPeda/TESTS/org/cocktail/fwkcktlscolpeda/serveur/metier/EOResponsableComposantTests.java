package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.junit.Test;

public class EOResponsableComposantTests extends EOScolTestCase {

	@Test
	public void test_copy() {
		// Arrange
		initAllTypesComposants();
		IPersonne individu = mock(IPersonne.class);
		when(individu.persId()).thenReturn(1);
		EOUE ue = EOTestsHelper.creerUE(editingContext, "ue", "ue");
		EOUE ueCopy = ue.copy(2014, 1);
		EOResponsableComposant responsableComposant = EOTestsHelper.creerResponsableComposant(editingContext, ue, individu);
		EOResponsableComposantRole responsableComposantRole1 = EOTestsHelper.creerResponsableComposantRole(editingContext, responsableComposant);		
		EOResponsableComposantRole responsableComposantRole2 = EOTestsHelper.creerResponsableComposantRole(editingContext, responsableComposant);
		responsableComposant.addToRoles(responsableComposantRole1);
		responsableComposant.addToRoles(responsableComposantRole2);
		
		// Assert ready to test
		assertThat(responsableComposant.roles().size()).isEqualTo(2);
		// Act
		IResponsableComposant responsableComposantCopy = responsableComposant.copyToComposant(ueCopy);
		// Assert
		//assertThat(responsableComposantCopy.responsable()).isEqualTo(individu);
		assertThat(responsableComposantCopy.composant()).isEqualTo(ueCopy);
		assertThat(responsableComposantCopy.roles().size()).isEqualTo(2);
	}

}
