package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import er.extensions.eof.ERXQ;

public class EOLienTest extends EOScolTestCase {

	private EOTypeComposant typeComposantConcret1;
	private EOTypeComposant typeComposantConcret2;

	private EOTypeLien typeLienComposer;
	private EOTypeLien typeLien2;

	private EOComposant composantParent;
	private EOComposant composantChild;
	private EOComposant composantParent2;
	private EOComposant composantChild2;

	private EOTypeDescriptifDestination typeDescriptifDestinationHabilitation;
	private EOTypeDescriptifDestination typeDescriptifDestinationPublication;

	private static final String expectedCode1 = "unCode1";
	private static final String expectedCode2 = "unCode2";
	private static final String expectedCode3 = "unCode3";
	private static final String expectedCode4 = "unCode4";
	private static final String expectedLibelle = "unLibelle";

	private EOTypePeriode typePeriodeSemestre;
	private EOPeriode semestre;

	private EOLien lienAvecSemestre;
	private EOLien lienSansSemestre;

	@Before
	public void setUp() {
		super.setUp();

		typeComposantConcret1 = EOTestsHelper.creerTypeComposant(editingContext, 1, "Concret1", "Concret1");
		editingContext.insertObject(typeComposantConcret1);
		typeComposantConcret2 = EOTestsHelper.creerTypeComposant(editingContext, 2, "Concret2", "Concret2");
		editingContext.insertObject(typeComposantConcret2);		
		initAllTypesComposants();
		
		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "Composer");
		editingContext.insertObject(typeLienComposer);
		typeLien2 = EOTestsHelper.creerTypeLien(editingContext, 2, "Test2");
		editingContext.insertObject(typeLien2);

		EORegleLiaison r1 = EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret1, typeComposantConcret2, typeLienComposer);
		editingContext.insertObject(r1);
		EORegleLiaison r2 = EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret1, typeComposantConcret2, typeLien2);
		editingContext.insertObject(r2);
		assertNotNull(r1);
		assertNotNull(r2);

		composantParent = EOTestsHelper.creerComposantConcret(editingContext, expectedCode1, expectedLibelle, 1, typeComposantConcret1);
		composantChild = EOTestsHelper.creerComposantConcret(editingContext, expectedCode2, expectedLibelle, 2, typeComposantConcret2);
		composantParent2 = EOTestsHelper.creerComposantConcret(editingContext, expectedCode3, expectedLibelle, 3, typeComposantConcret1);
		composantChild2 = EOTestsHelper.creerComposantConcret(editingContext, expectedCode4, expectedLibelle, 4, typeComposantConcret2);

		typeDescriptifDestinationHabilitation = EOTestsHelper.creerTypeDestinationDescriptif(editingContext, 1, EOTypeDescriptifDestination.TYPEHABILITATION_LBL,
		    "Habilitation");
		typeDescriptifDestinationPublication = EOTestsHelper.creerTypeDestinationDescriptif(editingContext, 2, EOTypeDescriptifDestination.TYPEPUBLICATION_LBL,
		    "Publication");

		typePeriodeSemestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, "");
		semestre = EOTestsHelper.creerPeriode(editingContext, "zz", "zzzzzz", typePeriodeSemestre);
		semestre.setTypeComposantRelationship(typeComposantConcret2);

		lienAvecSemestre = EOTestsHelper.creerLien(editingContext, 1, composantParent, semestre, typeLien2);
		lienAvecSemestre.setCreditECTSCalcule(BigDecimal.ZERO);

		lienSansSemestre = EOTestsHelper.creerLien(editingContext, 2, composantParent2, composantChild, typeLienComposer);
		lienSansSemestre.setCreditECTSCalcule(BigDecimal.ZERO);

	}

	/**
	 * Test de la fonction d'insertion d'un lien
	 */
	@Test
	public void test_insert() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);

		// Assert
		EOLien insertedLien = EOLien.fetchSco_Lien(
		    editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, composantParent).and(ERXQ.equals(EOLien.CHILD_KEY, composantChild))
		        .and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLienComposer)));
		assertNotNull(insertedLien);
		assertEquals(composantParent, insertedLien.parent());
		assertEquals(composantChild, insertedLien.child());
	}

	/**
	 * Test de la fonction de modification de EOLien
	 */
	@Test
	public void test_modifierLien() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(composantParent2);
		assertNotNull(composantChild2);
		assertNotNull(typeLien2);

		// Act
		lien.modifierLien(editingContext, composantParent2, composantChild2, typeLien2);
		editingContext.saveChanges();

		// Assert
		assertEquals(composantParent2, lien.parent());
		assertEquals(composantChild2, lien.child());
		assertEquals(typeLien2, lien.typeLien());

	}

	/**
	 * Test de la fonction de suppression de EOLien
	 */
	@Test
	public void test_supprimer() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);

		// Assert ready to test
		assertNotNull(lien);

		// Act
		lien.supprimer();

		// Assert
		EOLien deletedLien = EOLien.fetchSco_Lien(editingContext, ERXQ.equals(EOLien.PARENT_KEY, lien.parent()).and(ERXQ.equals(EOLien.CHILD_KEY, lien.child()))
		    .and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, lien.typeLien())));
		assertNull(deletedLien);

	}

	/**
	 * Test de la fonction creditable de EOLien
	 */
	@Test
	public void test_creditable() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		lien.addToCreditablesRelationship(creditable);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(creditable);

		// Assert
		assertNotNull(lien.creditable());

	}

	/**
	 * Test de la fonction getDescriptifTypeHabilitation de EOLien
	 */
	@Test
	public void test_getDescriptifTypeHabilitation() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);
		EODescriptif descriptif = EOTestsHelper
		    .creerDescriptif(editingContext, 1, composantChild, typeDescriptifDestinationHabilitation, "Descriptif Habilitation");
		lien.addToDescriptifsRelationship(descriptif);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(descriptif);

		// Act
		EODescriptif descriptifInserted = lien.getDescriptifTypeHabilitation();

		// Assert
		assertNotNull(descriptifInserted);
		assertEquals(descriptif, descriptifInserted);

		// Arrange
		lien.removeFromDescriptifsRelationship(descriptif);

		// Act
		EODescriptif descriptifRemoved = lien.getDescriptifTypeHabilitation();

		// Assert
		assertNull(descriptifRemoved);

	}

	/**
	 * Test de la fonction getDescriptifTypePublication de EOLien
	 */
	@Test
	public void test_getDescriptifTypePublication() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);
		EODescriptif descriptif = EOTestsHelper.creerDescriptif(editingContext, 1, composantChild, typeDescriptifDestinationPublication, "Descriptif Publication");
		lien.addToDescriptifsRelationship(descriptif);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(descriptif);

		// Act
		EODescriptif descriptifInserted = lien.getDescriptifTypePublication();

		// Assert
		assertNotNull(descriptifInserted);
		assertEquals(descriptif, descriptifInserted);

		// Arrange
		lien.removeFromDescriptifsRelationship(descriptif);

		// Act
		EODescriptif descriptifRemoved = lien.getDescriptifTypePublication();

		// Assert
		assertNull(descriptifRemoved);

	}

	/**
	 * Test de la fonction hasDescriptifTypeHabilitation de EOLien
	 */
	@Test
	public void test_hasDescriptifTypeHabilitation() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);
		EODescriptif descriptif = EOTestsHelper
		    .creerDescriptif(editingContext, 1, composantChild, typeDescriptifDestinationHabilitation, "Descriptif Habilitation");
		lien.addToDescriptifsRelationship(descriptif);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(descriptif);

		// Act
		boolean exist = lien.hasDescriptifTypeHabilitation();

		// Assert
		assertEquals(true, exist);

		// Arrange
		lien.removeFromDescriptifsRelationship(descriptif);

		// Act
		exist = lien.hasDescriptifTypeHabilitation();

		// Assert
		assertEquals(false, exist);

	}

	/**
	 * Test de la fonction hasDescriptifTypePublication de EOLien
	 */
	@Test
	public void test_hasDescriptifTypePublication() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLienComposer);
		EODescriptif descriptif = EOTestsHelper.creerDescriptif(editingContext, 1, composantChild, typeDescriptifDestinationPublication, "Descriptif Publication");
		lien.addToDescriptifsRelationship(descriptif);

		// Assert ready to test
		assertNotNull(lien);
		assertNotNull(descriptif);

		// Act
		boolean exist = lien.hasDescriptifTypePublication();

		// Assert
		assertEquals(true, exist);

		// Arrange
		lien.removeFromDescriptifsRelationship(descriptif);

		// Act
		exist = lien.hasDescriptifTypeHabilitation();

		// Assert
		assertEquals(false, exist);

	}

	@Test
	public void test_assignTo() {
		// Arrange
		EOLien lien = EOComposantFactory.createLien(composantParent, composantChild, typeLien2);
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		creditable.setBaseNote(12);
		lien.addToCreditablesRelationship(creditable);
		editingContext.saveChanges();

		EOLien nouveauLien = EOComposantFactory.createLien(composantParent, composantChild2, typeLienComposer);
		lien.assignTo(nouveauLien);
		// Assert ready to test

		// Act

		// Assert
		assertEquals(lien.creditable().baseNote(), nouveauLien.creditable().baseNote());
		assertEquals(lien.typeLien(), nouveauLien.typeLien());
	}

	/**
	 * Test de la fonction getSelectionLienDescriptifTypePublication de EOLien TODO : pas réussi à faire passer
	 */
	/*
	 * @Test public void test_getSelectionLienDescriptifTypePublication() { // Arrange lien = EOComposantFactory.createLien(composantParent, composantChild,
	 * typeLienComposer); lien.setId(1); editingContext.insertObject(lien); editingContext.saveChanges(); EODescriptif descriptif =
	 * EOTestsHelper.creerDescriptif(editingContext, 1, composantChild, typeDescriptifDestinationPublication, "Descriptif Publication");
	 * editingContext.insertObject(descriptif); editingContext.saveChanges(); lien.addToDescriptifsRelationship(descriptif); editingContext.saveChanges();
	 * 
	 * 
	 * // Assert ready to test assertNotNull(lien); assertNotNull(descriptif);
	 * 
	 * // Act System.err.println("test edintingContext : "+editingContext); lien.refetchObjectFromDBinEditingContext(editingContext); EOSelectionLienDescriptif
	 * sel = lien.getSelectionLienDescriptifTypePublication();
	 * 
	 * System.err.println("test sel : "+sel);
	 * 
	 * // Assert assertNotNull(sel); assertEquals(lien, sel.lien()); assertEquals(descriptif, sel.descriptif());
	 * 
	 * }
	 */

	@Test
	public void test_lien_isNbCreditsEctsOk() {
		assertNull(lienSansSemestre.isNbCreditsEctsOk());
		assertNotNull(lienAvecSemestre.isNbCreditsEctsOk());
	}

	/**
	 * Test du recalcul de l'ordre dans les liens A l'ajout d'un element : l'ordre du lien est egale au max des ordres + 1 A la suppression d'un lien sous un
	 * parent de type regroupement, ue ou ec : on ne recalcule pas l'ordre des liens
	 */
	@Test
	public void test_recalculerOrdreNOK() {
		// Arrange
		EOTestsHelper.creerRegleLiaison(editingContext, EOTypeComposant.typeRegroupement(editingContext), EOTypeComposant.typeEC(editingContext),
		    EOTypeLien.typeComposer(editingContext));
		EORegroupement composantParent = EOTestsHelper.creerRegroupement(editingContext, "R1", "Le regroupement 1");
		EOEC composantChild1 = EOTestsHelper.creerEC(editingContext, "EC1", "L'EC 1");
		EOEC composantChild2 = EOTestsHelper.creerEC(editingContext, "EC2", "L'EC 2");
		EOEC composantChild3 = EOTestsHelper.creerEC(editingContext, "EC3", "L'EC 3");
		EOLien lienChild1 = EOComposantFactory.createLienComposer(composantParent, composantChild1);
		EOLien lienChild2 = EOComposantFactory.createLienComposer(composantParent, composantChild2);
		EOLien lienChild3 = EOComposantFactory.createLienComposer(composantParent, composantChild3);
		editingContext.saveChanges();

		// Assert ready to test
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(2), lienChild2.ordre());
		assertEquals(new Integer(3), lienChild3.ordre());

		// Act
		lienChild2.supprimer();

		// Assert
		EOLien deletedLien = EOLien.fetchSco_Lien(
		    editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, composantParent).and(ERXQ.equals(EOLien.CHILD_KEY, composantChild2))
		        .and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, EOTypeLien.typeComposer(editingContext))));
		assertNull(deletedLien);
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(3), lienChild3.ordre());

		// Arrange
		EOEC composantChild4 = EOTestsHelper.creerEC(editingContext, "EC4", "L'EC 4");
		EOLien lienChild4 = EOComposantFactory.createLienComposer(composantParent, composantChild4);
		editingContext.saveChanges();

		// Assert ready to test
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(3), lienChild3.ordre());
		assertEquals(new Integer(4), lienChild4.ordre());

	}

	/**
	 * Test du recalcul de l'ordre dans les liens A l'ajout d'un element : l'ordre du lien est egale au max des ordres + 1 A la suppression d'un lien sous un
	 * parent de type diplome, version_diplome ou periode, parcours : on recalcule pas l'ordre des liens
	 */
	@Test
	public void test_recalculerOrdreOK() {
		// Arrange
		EOTestsHelper.creerRegleLiaison(editingContext, EOTypeComposant.typePeriode(editingContext), EOTypeComposant.typeParcours(editingContext),
		    EOTypeLien.typeComposer(editingContext));
		EOPeriode composantParent = EOTestsHelper.creerPeriode(editingContext, "L1", "Licence 1", typePeriodeSemestre);
		EOParcours composantChild1 = EOTestsHelper.creerParcours(editingContext, "P1", "Le parcours 1");
		EOParcours composantChild2 = EOTestsHelper.creerParcours(editingContext, "P2", "Le parcours 2");
		EOParcours composantChild3 = EOTestsHelper.creerParcours(editingContext, "P3", "Le parcours 3");
		EOLien lienChild1 = EOComposantFactory.createLienComposer(composantParent, composantChild1);
		EOLien lienChild2 = EOComposantFactory.createLienComposer(composantParent, composantChild2);
		EOLien lienChild3 = EOComposantFactory.createLienComposer(composantParent, composantChild3);
		editingContext.saveChanges();

		// Assert ready to test
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(2), lienChild2.ordre());
		assertEquals(new Integer(3), lienChild3.ordre());

		// Act
		lienChild2.supprimer();

		// Assert
		EOLien deletedLien = EOLien.fetchSco_Lien(
		    editingContext,
		    ERXQ.equals(EOLien.PARENT_KEY, composantParent).and(ERXQ.equals(EOLien.CHILD_KEY, composantChild2))
		        .and(ERXQ.equals(EOLien.TYPE_LIEN_KEY, EOTypeLien.typeComposer(editingContext))));
		assertNull(deletedLien);
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(2), lienChild3.ordre());

		// Arrange
		EOParcours composantChild4 = EOTestsHelper.creerParcours(editingContext, "P4", "Le parcours 4");
		EOLien lienChild4 = EOComposantFactory.createLienComposer(composantParent, composantChild4);
		editingContext.saveChanges();

		// Assert ready to test
		assertEquals(new Integer(1), lienChild1.ordre());
		assertEquals(new Integer(2), lienChild3.ordre());
		assertEquals(new Integer(3), lienChild4.ordre());

	}

}
