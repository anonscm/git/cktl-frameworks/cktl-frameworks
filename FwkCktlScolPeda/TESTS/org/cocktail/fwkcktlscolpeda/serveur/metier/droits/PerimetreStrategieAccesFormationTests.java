package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;

/**
 *         Tests de la stratégie accès à une formation
 */
public class PerimetreStrategieAccesFormationTests extends EOScolTestCase {
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);
	
	private EOGdStrategie strategieAccesFormation;
	private List<EOGdPerimetreGirofle> perimetres = new ArrayList<EOGdPerimetreGirofle>();
	private EODiplome diplome1;
	private EODiplome diplome2;
	private EODiplome diplome3;
	private EOPeriode periodeLicence1;
	private EOPeriode periodeLicence2;
	private EOPeriode periodeLicence3;
	private EOTypeComposant typeComposantPeriode;
	private EOTypeLien typeLienComposer;
	private EOTypeComposant typeComposantDiplome;
	
	// EOStatutFormation
	private EOStatutFormation statutFormationProduction;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	private EOTypeFormation typeFormationMaster;
		

	@Override
	@Before
	public void setUp() {
		super.setUp();
		strategieAccesFormation = EOTestsHelper.creerStrategie(editingContext, EOGdStrategie.STRATEGIE_ACCESFORMATION_CODE);	
		typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, "DIPLOME", "Diplôme");
		typeComposantPeriode = EOTestsHelper.creerTypeComposant(editingContext, 2, "PERIODE", "Période");

		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantPeriode, typeLienComposer);
		
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
	}

	@Test
	public void test_getQualifier() {
		// Arrange
		prepareComposants();
		preparePerimetres();

		// Assert ready to test
		PerimetreGirofleProvider perimetreProvider = new PerimetreGirofleProvider();
		perimetreProvider.setPerimetres(perimetres);
		EOQualifier qualifier = perimetreProvider.getAllQualifiers();

		// Act
		NSArray<EOComposant> listComposant = EOComposant.fetchSco_Composants(editingContext, qualifier, null);
		listComposant = ERXArrayUtilities.arrayWithoutDuplicates(listComposant);

		// Assert
		assertEquals(3, listComposant.count());
	}

	private void preparePerimetres() {
		//perimetre principal sur le diplome
		EOGdPerimetreGirofle perimetre1 = EOTestsHelper.creerPerimetre(editingContext, strategieAccesFormation);
		perimetre1.setComposant(diplome1);

		perimetres.add(perimetre1);
	}

	private void prepareComposants() {
		diplome1 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME1", "Le diplome 1", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome1.setId(1);
		diplome2 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME2", "Le diplome 2", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome2.setId(2);
		diplome3 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME3", "Le diplome 3", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome3.setId(3);
		EOTypePeriode typePeriode = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);

		// PERIODES
		periodeLicence1 = EOTestsHelper.creerPeriode(editingContext, "L1", "Licence 1", typePeriode);
		periodeLicence1.setId(2);
		periodeLicence2 = EOTestsHelper.creerPeriode(editingContext, "L2", "Licence 2", typePeriode);
		periodeLicence2.setId(3);
		periodeLicence3 = EOTestsHelper.creerPeriode(editingContext, "L3", "Licence 3", typePeriode);
		periodeLicence3.setId(4);
		
		EOComposantFactory.createLien(diplome1, periodeLicence1, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence1, periodeLicence2, typeLienComposer);
	}

}
