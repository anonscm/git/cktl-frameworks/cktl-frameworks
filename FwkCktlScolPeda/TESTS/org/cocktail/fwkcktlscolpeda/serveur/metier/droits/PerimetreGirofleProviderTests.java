package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXArrayUtilities;

public class PerimetreGirofleProviderTests extends EOScolTestCase {
	private EOGdStrategie strategieAccesElement;
	private EOGdStrategie strategieAccessStructure;

	@Before
	public void setUp() {
		strategieAccesElement = EOTestsHelper.creerStrategie(editingContext, EOGdStrategie.STRATEGIE_ACCESELEMENT_CODE);
		strategieAccessStructure = EOTestsHelper.creerStrategie(editingContext, EOGdStrategie.STRATEGIE_ACCESFORMATIONSCOMPOSANTE_CODE);
	}

	@Test
	public void test_ajouter() {
		// Arrange
		EOGdPerimetreGirofle perimetre = spy(createPerimetre(1, "libelle"));
		doReturn(1).when(perimetre).persIdCreation();
		doReturn(new NSTimestamp()).when(perimetre).dateCreation();		
		// Assert ready to test

		// Act
		
		editingContext.insertObject(perimetre);
		
		editingContext.saveChanges();

		// Assert
		assertEquals(EOGdPerimetreGirofle.fetchAllSco_GdPerimetreGirofles(editingContext).count(), 1);
	}

	@Test
	public void test_ajouter_perimetreSurComposant() {
		// Arrange
		EOTypeComposant typeComposant = EOTestsHelper.creerTypeComposant(editingContext, 1, "type", "type");
		EOTypeLien typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "typeLien");
		EORegleLiaison regleLiaison = EOTestsHelper.creerRegleLiaison(editingContext, typeComposant, typeComposant, typeLien);

		EOComposant composant1 = EOTestsHelper.creerComposantConcret(editingContext, "code1", "libelle", 1, typeComposant);
		EOComposant composant2 = EOTestsHelper.creerComposantConcret(editingContext, "code2", "libelle", 2, typeComposant);
		EOComposant composant3 = EOTestsHelper.creerComposantConcret(editingContext, "code3", "libelle", 3, typeComposant);
		EOComposant composant4 = EOTestsHelper.creerComposantConcret(editingContext, "code4", "libelle", 4, typeComposant);

		List<EOGdPerimetreGirofle> perimetres = new ArrayList<EOGdPerimetreGirofle>();
		EOGdPerimetreGirofle perimetre1 = creerPerimetreSurComposant(composant1);
		perimetres.add(perimetre1);

		EOGdPerimetreGirofle perimetre2 = creerPerimetreSurComposant(composant3);
		perimetres.add(perimetre2);

		editingContext.saveChanges();
		// Assert ready to test

		// Act
		PerimetreGirofleProvider perimetreProvider = new PerimetreGirofleProvider();
		perimetreProvider.setPerimetres(perimetres);
		EOQualifier qualifier = perimetreProvider.getAllQualifiers();

		NSArray<EOComposant> listComposant = EOComposant.fetchSco_Composants(editingContext, qualifier, null);
		listComposant = ERXArrayUtilities.arrayWithoutDuplicates(listComposant);

		// Assert
		assertEquals(2, listComposant.count());
	}

	@Test
	public void test_ajouterPerimetreSurComposante() throws Exception {
		// Arrange
		EOTypeComposant typeComposant = EOTestsHelper.creerTypeComposant(editingContext, 1, "type", "type");
		EOTypeLien typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "typeLien");
		EORegleLiaison regleLiaison = EOTestsHelper.creerRegleLiaison(editingContext, typeComposant, typeComposant, typeLien);

		EOComposant composant1 = EOTestsHelper.creerComposantConcret(editingContext, "code1", "libelle", 1, typeComposant);
		EOComposant composant2 = EOTestsHelper.creerComposantConcret(editingContext, "code2", "libelle", 2, typeComposant);
		EOComposant composant3 = EOTestsHelper.creerComposantConcret(editingContext, "code3", "libelle", 3, typeComposant);
		EOComposant composant4 = EOTestsHelper.creerComposantConcret(editingContext, "code4", "libelle", 4, typeComposant);

		EOTestsHelper.remplirTypeStructure(editingContext);
		EOStructure structure1 = EOTestsHelper.creerStructure(editingContext, "maStructure");
		composant1.setStructure(structure1);
		composant2.setStructure(structure1);
		EOStructure structure2 = EOTestsHelper.creerStructure(editingContext, "maStructure");
		composant3.setStructure(structure2);

		PerimetreGirofleProvider perimetreProvider = new PerimetreGirofleProvider();

		List<EOGdPerimetreGirofle> perimetres = new ArrayList<EOGdPerimetreGirofle>();
		EOGdPerimetreGirofle perimetre1 = creerPerimetreSurStructure(structure1);
		perimetres.add(perimetre1);

		EOGdPerimetreGirofle perimetre2 = creerPerimetreSurStructure(structure2);
		perimetres.add(perimetre2);

		// Assert ready to test
		editingContext.saveChanges();

		// Act
		perimetreProvider.setPerimetres(perimetres);
		EOQualifier qualifier = perimetreProvider.getAllQualifiers();

		NSArray<EOComposant> listComposant = EOComposant.fetchSco_Composants(editingContext, qualifier, null);
		listComposant = ERXArrayUtilities.arrayWithoutDuplicates(listComposant);

		// Assert
		assertEquals(3, listComposant.count());
	}

	//Private Helpers
	private EOGdPerimetreGirofle creerPerimetreSurComposant(EOComposant composant) {
		EOGdPerimetreGirofle perimetre = creerPerimetre();

		perimetre.setComposant(composant);
		perimetre.setComposantId(composant.id());
		perimetre.setStrategie(strategieAccesElement);
		return perimetre;
	}

	private EOGdPerimetreGirofle creerPerimetreSurStructure(EOStructure structure) {
		EOGdPerimetreGirofle perimetre = creerPerimetre();

		perimetre.setStructure(structure);
		//perimetre.setComposantId(composant.id());
		perimetre.setStrategie(strategieAccessStructure);

		return perimetre;
	}

	private EOGdPerimetreGirofle creerPerimetre() {
		EOGdPerimetreGirofle perimetre = spy(createPerimetre(1, "libelle"));
		doReturn(1).when(perimetre).persIdCreation();
		doReturn(new NSTimestamp()).when(perimetre).dateCreation();

		editingContext.insertSavedObject(perimetre);
		return perimetre;
	}

	private EOGdPerimetreGirofle createPerimetre(Integer appId, String libelle) {
		//On ne peut pas utiliser le create(editingContext) car l'injection du user n'est pas mise en place dans le test U
		// A voir si une factory ne pourrait pas être une soltion plus propre
		EOGdPerimetreGirofle perimetre = new EOGdPerimetreGirofle(); //  EOGdPerimetreGirofle.create(editingContext);
		perimetre.setAppId(appId);
		perimetre.setLibelle(libelle);

		return perimetre;
	}
}
