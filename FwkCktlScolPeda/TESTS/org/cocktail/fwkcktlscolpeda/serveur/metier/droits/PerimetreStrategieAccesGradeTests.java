package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;

public class PerimetreStrategieAccesGradeTests extends EOScolTestCase {
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);
	
	private EOGdStrategie strategieAccesGrade;
	private List<EOGdPerimetreGirofle> perimetres  = new ArrayList<EOGdPerimetreGirofle>();
	private EODiplome diplome1;
	private EODiplome diplome2;
	private EODiplome diplome3;
	private EOGradeUniversitaire grade1;
	private EOGradeUniversitaire grade2;
	
	// EOStatutFormation
	private EOStatutFormation statutFormationProduction;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
	

	@Before
	@Override
	public void setUp() {
		super.setUp();
		strategieAccesGrade = EOTestsHelper.creerStrategie(editingContext, EOGdStrategie.STRATEGIE_ACCESGRADE_CODE);
		EOTestsHelper.creerTypeComposant(editingContext, 1, "DIPLOME", "Diplôme");
		
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
	}

	@Test
	public void test_getQualifier() {
		// Arrange
		prepareComposants();
		preparePerimetres();

		// Assert ready to test
		PerimetreGirofleProvider perimetreProvider = new PerimetreGirofleProvider();
		perimetreProvider.setPerimetres(perimetres);
		EOQualifier qualifier = perimetreProvider.getAllQualifiers();

		// Act
		NSArray<EOComposant> listComposant = EOComposant.fetchSco_Composants(editingContext, qualifier, null);
		listComposant = ERXArrayUtilities.arrayWithoutDuplicates(listComposant);

		// Assert
		assertEquals(2, listComposant.count());
	}

	private void preparePerimetres() {
		 //perimetre principal sur le diplome
		 EOGdPerimetreGirofle perimetre1 = EOTestsHelper.creerPerimetre(editingContext, strategieAccesGrade);
		 perimetre1.setGradeUniversitaireRelationship(grade1);
		 
		 perimetres.add(perimetre1);	    
    }

	private void prepareComposants() {
		grade1= EOTestsHelper.creerGradeUniversitaire(editingContext, "LICENCE", "LICENCE", 3);
		grade2= EOTestsHelper.creerGradeUniversitaire(editingContext, "MASTER", "Master", 2);
		
		diplome1 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME1", "Le diplome 1", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome1.setGradeUniversitaire(grade1);
		
		diplome2 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME2", "Le diplome 2", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome2.setGradeUniversitaireRelationship(grade1);
		
		diplome3 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME3", "Le diplome 3", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome3.setGradeUniversitaireRelationship(grade2);
    }
}
