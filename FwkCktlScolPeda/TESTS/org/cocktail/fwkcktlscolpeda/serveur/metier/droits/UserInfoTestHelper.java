package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.wounit.rules.MockEditingContext;

/**
 * Crée un userInfo pour les tests unitaires
 */
public class UserInfoTestHelper {

	
	public static UserInfo getUserInfoTest(MockEditingContext editingContext){
		UserInfo userInfo = new UserInfo() {
			
			public Number persId() {
				return 1;
			}
			public String nom() {
				return "Nom";
			}
			public String prenom() {
				return "Prenom";
			}
			public String nomEtPrenom() {
				return "Nom Prénom";
			}
			public Number noIndividu() {
				return 1;
			}
		};
		
		return userInfo;
	}
	
	
}
