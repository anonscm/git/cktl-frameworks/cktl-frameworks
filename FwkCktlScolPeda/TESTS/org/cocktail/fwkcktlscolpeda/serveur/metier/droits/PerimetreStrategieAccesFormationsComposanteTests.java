package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;

public class PerimetreStrategieAccesFormationsComposanteTests extends EOScolTestCase {
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);
	
	// EOTypeComposant
	private EOTypeComposant typeComposantPeriode;
	private EOTypeComposant typeComposantRegroupement;
	private EOTypeComposant typeComposantDiplome;
	
	// EOTypeLien
	private EOTypeLien typeLienComposer;

	// Composants
	private EODiplome diplome1;
	private EODiplome diplome2;
	private EOPeriode periodeLicence1;
	private EOPeriode periodeLicence2;
	private EOPeriode periodeLicence3;
	private EORegroupement regroupement1;
	private EORegroupement regroupement2;
	private EORegroupement regroupement3;

	// EOGdStrategie
	private EOGdStrategie strategieAccesFormationComposante;

	private List<EOGdPerimetreGirofle> perimetres = new ArrayList<EOGdPerimetreGirofle>();
	
	// EOStructure
	private EOStructure structure1;
	private EOStructure structure2;
	
	// EOStatutFormation
	private EOStatutFormation statutFormationProduction;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
	

	@Before
	@Override
	public void setUp() {
		super.setUp();
		strategieAccesFormationComposante = EOTestsHelper.creerStrategie(editingContext, EOGdStrategie.STRATEGIE_ACCESFORMATIONSCOMPOSANTE_CODE);

		typeComposantDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, "DIPLOME", "Diplôme");
		typeComposantPeriode = EOTestsHelper.creerTypeComposant(editingContext, 2, "PERIODE", "Période");
		typeComposantRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 3, "REGROUPEMENT", "Regroupement");

		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantRegroupement, typeLienComposer);
		
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
	}

	@Test
	public void test_getQualifier() throws Exception {
		// Arrange
		prepareComposants();
		preparePerimetres();

		// Assert ready to test
		PerimetreGirofleProvider perimetreProvider = new PerimetreGirofleProvider();
		perimetreProvider.setPerimetres(perimetres);
		EOQualifier qualifier = perimetreProvider.getAllQualifiers();

		// Act
		NSArray<EOComposant> listComposant = EOComposant.fetchSco_Composants(editingContext, qualifier, null);
		listComposant = ERXArrayUtilities.arrayWithoutDuplicates(listComposant);
		
		// Assert
		//assertEquals(4, listComposant.count());
	}

	private void preparePerimetres() {
		
		 // perimetre sur la structure1
		 EOGdPerimetreGirofle perimetre1 = EOTestsHelper.creerPerimetre(editingContext, strategieAccesFormationComposante);
		 perimetre1.setStructureRelationship(structure1); 
		 
		 perimetres.add(perimetre1);

	}

	private void prepareComposants() throws Exception {
		diplome1 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME1", "Le diplome 1", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome1.setId(1);
		
		diplome2 = EOTestsHelper.creerDiplome(editingContext, "CODEDIPLOME2", "Le diplome 2", statutFormationProduction, gradeUniversitaireMaster, typeFormationMaster, year);
		diplome2.setId(8);

		EOTypePeriode typePeriode = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);

		// PERIODES
		periodeLicence1 = EOTestsHelper.creerPeriode(editingContext, "L1", "Licence 1", typePeriode);
		periodeLicence1.setId(2);
		periodeLicence2 = EOTestsHelper.creerPeriode(editingContext, "L2", "Licence 2", typePeriode);
		periodeLicence2.setId(3);
		periodeLicence3 = EOTestsHelper.creerPeriode(editingContext, "L3", "Licence 3", typePeriode);
		periodeLicence3.setId(4);

		// REGROUPEMENTS
		regroupement1 = EOTestsHelper.creerRegroupement(editingContext, "R1", "Regroupement 1");
		regroupement1.setId(5);
		regroupement2 = EOTestsHelper.creerRegroupement(editingContext, "R2", "Regroupement 2");
		regroupement2.setId(6);
		regroupement3 = EOTestsHelper.creerRegroupement(editingContext, "R3", "Regroupement 3");
		regroupement3.setId(7);

		
		EOComposantFactory.createLien(diplome1, periodeLicence1, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence1, regroupement1, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence1, regroupement2, typeLienComposer);
		EOComposantFactory.createLien(diplome2, periodeLicence2, typeLienComposer);
		EOComposantFactory.createLien(diplome2, periodeLicence3, typeLienComposer);
		EOComposantFactory.createLien(periodeLicence3, regroupement3, typeLienComposer);
		
		
		EOTestsHelper.remplirTypeStructure(editingContext);
		structure1 = EOTestsHelper.creerStructure(editingContext, "maStructure1");
		diplome1.setStructure(structure1);
		regroupement3.setStructure(structure1);
		structure2 = EOTestsHelper.creerStructure(editingContext, "maStructure2");
		diplome2.setStructure(structure2);

	}

}
