package org.cocktail.fwkcktlscolpeda.serveur.metier.descriptif.service;

import static org.fest.assertions.api.Assertions.assertThat;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeDescriptifDestination;
import org.junit.Before;
import org.junit.Test;

public class DescriptifProviderTests extends EOScolTestCase {

	@Before
	public void setUp() {
		super.setUp();
		EOTestsHelper.creerTypeDescriptif(editingContext, true);
		EOTestsHelper.creerTypeDescriptif(editingContext, false);
		initAllTypesComposants();
	}

	@Test
	public void testgetDescriptif() {
		// Arrange
		EOUE ue = EOTestsHelper.creerUE(editingContext, "code", "libelle");
		EODescriptif descriptif1 = EOTestsHelper.creerDescriptif(editingContext, "descriptif1", EOTypeDescriptifDestination.typeHabilitation(editingContext));
		EODescriptif descriptif2 = EOTestsHelper.creerDescriptif(editingContext, "descriptif2", EOTypeDescriptifDestination.typeHabilitation(editingContext));
		EODescriptif descriptif3 = EOTestsHelper.creerDescriptif(editingContext, "descriptif3", EOTypeDescriptifDestination.typePublication(editingContext));
		EODescriptif descriptif4 = EOTestsHelper.creerDescriptif(editingContext, "descriptif4", EOTypeDescriptifDestination.typeHabilitation(editingContext));
		ue.addToDescriptifsRelationship(descriptif1);
		ue.addToDescriptifsRelationship(descriptif2);
		ue.addToDescriptifsRelationship(descriptif3);
		ue.addToDescriptifsRelationship(descriptif4);
		editingContext.saveChanges();
		DescriptifProvider descriptifProvider = new DescriptifProviderImpl();

		// Assert ready to test
		assertThat(ue.descriptifs().count()).isEqualTo(4);
		assertThat(EOTypeDescriptifDestination.typeHabilitation(editingContext).type()).isEqualTo(ITypeDescriptifDestination.TYPEHABILITATION_LBL);

		// Act - Assert
		assertThat(descriptifProvider.getDescriptif(ue, true)).isEqualTo(descriptif1);
		assertThat(descriptifProvider.getDescriptif(ue, false)).isEqualTo(descriptif3);
	}

}
