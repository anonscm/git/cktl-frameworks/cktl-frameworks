package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.parboiled.errors.ParsingException;

@RunWith(MockitoJUnitRunner.class)
public class CalculResultatServiceTest {

	private static final String FORMULE_COMPENSATION = "((n1 + n2) / 2) S 10";
	private static final String FORMULE_ADMISSION = "((n1 + n2) / 2) S 10 et (n1 > 8) et (n2 > 8)";
	private static final String FORMULE_DETTE = "((n1 + n2) / 2) S 8";
	IScoInscription inscription = mock(IScoInscription.class);
	ILien lien = mock(ILien.class);
	IComposant composant = mock(IComposant.class);
	Integer noSession = 1;
	
	
	
	@Before
	public void setUp() throws Exception {
		when(lien.child()).thenReturn(composant);
	}

	
	@Test
	public void testCalculerResultatAdmis() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mockHelper(FORMULE_ADMISSION, FORMULE_COMPENSATION, FORMULE_DETTE, "10", "12");
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
    assertThat(resultat.code()).isEqualTo(INoteResultat.ADMIS);		
	}
	
	@Test
	public void testCalculerResultatCompensation() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mockHelper(FORMULE_ADMISSION, FORMULE_COMPENSATION, FORMULE_DETTE, "8", "12");
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
		assertThat(resultat.code()).isEqualTo(INoteResultat.ADMIS_COMPENSATION);
		
	}
	
	@Test
	public void testCalculerResultatDette() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mockHelper(FORMULE_ADMISSION, FORMULE_COMPENSATION, FORMULE_DETTE,  "8", "10");
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
		
		assertThat(resultat.code()).isEqualTo(INoteResultat.DETTE);
		
	}
	
	@Test
	public void testCalculerResultatAjourne() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mockHelper(FORMULE_ADMISSION, FORMULE_COMPENSATION, FORMULE_DETTE,  "8", "7.5");
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
		assertThat(resultat.code()).isEqualTo(INoteResultat.AJOURNE);
		
	}

	@Test
	public void testFormuleResultatNonBoolean() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mockHelper("((n1 + n2) / 2)", "((n1 + n2) / 2)", "8", "10");
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
		assertThat(resultat).isNull();
	}

	@Test
	public void testFormuleNulle() throws ParsingException, CalculateurMoyenneException {
		INoteResultatRepository noteResultatRepository = mockNoteResultatRepository();
		CalculateurFormuleHelper helper = mock(CalculateurFormuleHelper.class);
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPEADMISSION_LBL)).thenReturn(null);
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPECOMPENSATION_LBL)).thenReturn(null);
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPEDETTES_LBL)).thenReturn(null);
		CalculResultatService resultatService = new CalculResultatService(noteResultatRepository);
		resultatService.setCalculateurFormuleHelper(helper);
		Map<String, BigDecimal> notes = helper.getNotesValeurs(inscription, lien, noSession);
		Map<String, BigDecimal> coefficients = helper.getCoefficients(composant);
		BigDecimal moyenne =null;
		
		INoteResultat resultat = resultatService.calculerResultat(notes, coefficients, inscription, lien, moyenne, noSession);
		
		assertThat(resultat).isNull();
	}
	
	private CalculateurFormuleHelper mockHelper(String formuleAdmissionStr, String formuleCompensationStr, String formuleDetteStr, String...notes) {
		Map<String, BigDecimal> notesNumeriques = new HashMap<String, BigDecimal>();
		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		int i = 1;
		for (String note : notes) {
			notesNumeriques.put(CalculateurFormuleHelper.PREFIX_NOTE_DEFAULT + i, new BigDecimal(note));
			coefficients.put(CalculateurFormuleHelper.PREFIX_COEFF_DEFAULT + i, new BigDecimal(1));
			i++;
		}
		CalculateurFormuleHelper helper = mock(CalculateurFormuleHelper.class);
		IFormule formuleAdmission = mock(IFormule.class);
		when(formuleAdmission.formule()).thenReturn(formuleAdmissionStr);
		IFormule formuleCompensation = mock(IFormule.class);
		when(formuleCompensation.formule()).thenReturn(formuleCompensationStr);
		IFormule formuleDette = mock(IFormule.class);
		when(formuleDette.formule()).thenReturn(formuleDetteStr);		
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPEADMISSION_LBL)).thenReturn(formuleAdmission);
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPECOMPENSATION_LBL)).thenReturn(formuleCompensation);
		when(helper.getFormule(noSession, composant, ITypeFormule.TYPEDETTES_LBL)).thenReturn(formuleDette);
		when(helper.getNotesValeurs(inscription, lien, noSession)).thenReturn(notesNumeriques);
		when(helper.getCoefficients(composant)).thenReturn(coefficients);
		return helper;
	}

	private INoteResultatRepository mockNoteResultatRepository() {
		INoteResultatRepository noteResultatRepository = mock(INoteResultatRepository.class);
		INoteResultat mockNoteResultatAdmis = mockNoteResultat(INoteResultat.ADMIS);
		when(noteResultatRepository.getNoteResultat(INoteResultat.ADMIS)).thenReturn(mockNoteResultatAdmis);
		INoteResultat mockNoteResultatAjourne = mockNoteResultat(INoteResultat.AJOURNE);
		when(noteResultatRepository.getNoteResultat(INoteResultat.AJOURNE)).thenReturn(mockNoteResultatAjourne);
		INoteResultat mockNoteResultatAdmisCompensation = mockNoteResultat(INoteResultat.ADMIS_COMPENSATION);
		when(noteResultatRepository.getNoteResultat(INoteResultat.ADMIS_COMPENSATION)).thenReturn(mockNoteResultatAdmisCompensation);		
		INoteResultat mockNoteResultatDette = mockNoteResultat(INoteResultat.DETTE);
		when(noteResultatRepository.getNoteResultat(INoteResultat.DETTE)).thenReturn(mockNoteResultatDette);
		
		return noteResultatRepository;
	}

	private INoteResultat mockNoteResultat(String code) {
		INoteResultat noteResultat = mock(INoteResultat.class);
		when(noteResultat.code()).thenReturn(code);
		return noteResultat;
	}
	
	
}
