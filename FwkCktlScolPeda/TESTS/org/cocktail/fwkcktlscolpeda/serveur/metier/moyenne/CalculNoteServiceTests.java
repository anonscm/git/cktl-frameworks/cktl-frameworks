package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.junit.Before;
import org.junit.Test;


public class CalculNoteServiceTests {
	private ITypeNote typeNoteCalculee;
	private ITypeNote typeNoteSaisie;

	@Before
	public void setUp() {
		typeNoteCalculee = mock(ITypeNote.class);
		when(typeNoteCalculee.code()).thenReturn(ITypeNote.NOTE_CALCULEE);

		typeNoteSaisie = mock(ITypeNote.class);
		when(typeNoteSaisie.code()).thenReturn(ITypeNote.NOTE_SAISIE);
	}

	@Test
	public void calculerNotesComposantEtFilsMoyennePonderee() {
		// Arrange
		CalculNoteService calculNoteService = spy(new CalculNoteService(null));
		CalculResultatService calculResultatService = mock(CalculResultatService.class);
	  calculNoteService.setCalculResultatService(calculResultatService);
	
		IDiplome diplome1 = mock(IDiplome.class);
		IVersionDiplome versionDiplome1 = mock(IVersionDiplome.class);
		IParcours parcours = mock(IParcours.class);
		IUE ue = mock(IUE.class);
		IEC ec = mock(IEC.class);
		IAE ae1 = mock(IAE.class);
		when(ae1.baseNote()).thenReturn(20);
		IAE ae2 = mock(IAE.class);
		when(ae2.baseNote()).thenReturn(40);

		ILienComposer lienVersionDiplome = creerLienComposer(diplome1, versionDiplome1, 1, null);
		doReturn(Arrays.asList(lienVersionDiplome)).when(diplome1).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienVersionDiplome)).when(diplome1).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienParcours = creerLienComposer(versionDiplome1, parcours, 1, null);
		doReturn(Arrays.asList(lienParcours)).when(versionDiplome1).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienParcours)).when(versionDiplome1).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienue = creerLienComposer(parcours, ue, 1, null);
		doReturn(Arrays.asList(lienue)).when(parcours).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienue)).when(parcours).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienec = creerLienComposer(ue, ec, 1, null);
		doReturn(Arrays.asList(lienec)).when(ue).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienec)).when(ue).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienae1 = creerLienComposer(ec, ae1, 1, null);
		ILienComposer lienae2 = creerLienComposer(ec, ae2, 2, null);
		doReturn(Arrays.asList(lienae1, lienae2)).when(ec).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienae1, lienae2)).when(ec).liensComposerAvecLesComposantsEnfants();

		IScoInscription scoInscription = mock(IScoInscription.class);
		IInscriptionPedagogique inscriptionPeda = mock(IInscriptionPedagogique.class);
		when(inscriptionPeda.toInscription()).thenReturn(scoInscription);
		doReturn(Arrays.asList(inscriptionPeda)).when(scoInscription).toInscriptionsPedagogiques();

		IInscriptionPedagogiqueElement inscriptionPedaEltVersionDiplome = creerInscriptionPedagogiqueElement(lienVersionDiplome, inscriptionPeda);
		INote noteVersionDiplome = creerNote(inscriptionPedaEltVersionDiplome, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteVersionDiplome)).when(inscriptionPedaEltVersionDiplome).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltParcours = creerInscriptionPedagogiqueElement(lienParcours, inscriptionPeda);
		INote noteParcours = creerNote(inscriptionPedaEltParcours, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteParcours)).when(inscriptionPedaEltParcours).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltUE = creerInscriptionPedagogiqueElement(lienue, inscriptionPeda);
		INote noteUE = creerNote(inscriptionPedaEltUE, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteUE)).when(inscriptionPedaEltUE).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltEC = creerInscriptionPedagogiqueElement(lienec, inscriptionPeda);
		INote noteECC1 = creerNote(inscriptionPedaEltEC, typeNoteCalculee, 1, null);
		INote noteECS1 = creerNote(inscriptionPedaEltEC, typeNoteSaisie, 1, new BigDecimal(18));
		doReturn(Arrays.asList(noteECC1, noteECS1)).when(inscriptionPedaEltEC).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaElt11 = creerInscriptionPedagogiqueElement(lienae1, inscriptionPeda);
		INote note11 = creerNote(inscriptionPedaElt11, typeNoteSaisie, 1, new BigDecimal(12));
		doReturn(Arrays.asList(note11)).when(inscriptionPedaElt11).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaElt12 = creerInscriptionPedagogiqueElement(lienae2, inscriptionPeda);
		INote note12 = creerNote(inscriptionPedaElt12, typeNoteSaisie, 1, new BigDecimal(36));
		doReturn(Arrays.asList(note12)).when(inscriptionPedaElt12).toNotes();

		doReturn(
		    Arrays.asList(inscriptionPedaEltVersionDiplome, inscriptionPedaEltParcours, inscriptionPedaEltUE, inscriptionPedaEltEC, inscriptionPedaElt11,
		        inscriptionPedaElt12)).when(inscriptionPeda).toInscriptionPedagogiqueElements();

		// Assert ready to test

		// Act
		calculNoteService.calculerNotesComposantEtFils(scoInscription, lienVersionDiplome, 1);

		// Assert
		assertThat(noteUE.noteInitiale()).isEqualTo(new BigDecimal("18.00"));
		assertThat(noteParcours.noteInitiale()).isEqualTo(new BigDecimal("18.00"));
		assertThat(noteVersionDiplome.noteInitiale()).isEqualTo(new BigDecimal("18.00"));

	}

	@Test
	public void calculerNotesComposantEtFilsFormule() {
		// Arrange
		CalculNoteService calculNoteService = spy(new CalculNoteService(null));
		CalculResultatService calculResultatService = mock(CalculResultatService.class);
	  calculNoteService.setCalculResultatService(calculResultatService);

		IUE ue = mock(IUE.class);
		IEC ec = mock(IEC.class);
		IAE ae1 = mock(IAE.class);
		IAE ae2 = mock(IAE.class);
		IAE ae3 = mock(IAE.class);

		ILienComposer lienec = creerLienComposer(ue, ec, 1, null);
		doReturn(Arrays.asList(lienec)).when(ue).liensParents();
		ILienComposer lienae1 = creerLienComposer(ec, ae1, 1, new BigDecimal(5));
		ILienComposer lienae2 = creerLienComposer(ec, ae2, 2, new BigDecimal(2));
		ILienComposer lienae3 = creerLienComposer(ec, ae3, 3, new BigDecimal(3));
		doReturn(Arrays.asList(lienae1, lienae2, lienae3)).when(ec).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienae1, lienae2, lienae3)).when(ec).liensComposerAvecLesComposantsEnfants();

		IFormule formule = getformule("c1*n1 + c2*n2 + c3*n3 + 3");
		ISessionComposant session = mock(ISessionComposant.class);
		when(session.getFormule(ITypeFormule.TYPEMOYENNE_LBL)).thenReturn(formule);
		doReturn(Arrays.asList(session)).when(ec).sessions();
		final int numeroSession = 1;
		when(ec.getSession(numeroSession)).thenReturn(session);

		IScoInscription scoInscription = mock(IScoInscription.class);
		IInscriptionPedagogique inscriptionPeda = mock(IInscriptionPedagogique.class);
		when(inscriptionPeda.toInscription()).thenReturn(scoInscription);
		doReturn(Arrays.asList(inscriptionPeda)).when(scoInscription).toInscriptionsPedagogiques();

		IInscriptionPedagogiqueElement inscriptionPedaEltEC = creerInscriptionPedagogiqueElement(lienec, inscriptionPeda);
		INote noteECC1 = creerNote(inscriptionPedaEltEC, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteECC1)).when(inscriptionPedaEltEC).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaElt11 = creerInscriptionPedagogiqueElement(lienae1, inscriptionPeda);
		INote note11 = creerNote(inscriptionPedaElt11, typeNoteSaisie, 1, new BigDecimal(12));
		doReturn(Arrays.asList(note11)).when(inscriptionPedaElt11).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaElt12 = creerInscriptionPedagogiqueElement(lienae2, inscriptionPeda);
		INote note12 = creerNote(inscriptionPedaElt12, typeNoteSaisie, 1, new BigDecimal(0));
		doReturn(Arrays.asList(note12)).when(inscriptionPedaElt12).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaElt13 = creerInscriptionPedagogiqueElement(lienae3, inscriptionPeda);
		INote note13 = creerNote(inscriptionPedaElt13, typeNoteSaisie, 1, new BigDecimal(36));
		doReturn(Arrays.asList(note13)).when(inscriptionPedaElt13).toNotes();

		doReturn(Arrays.asList(inscriptionPedaEltEC, inscriptionPedaElt11, inscriptionPedaElt12, inscriptionPedaElt13)).when(inscriptionPeda).toInscriptionPedagogiqueElements();

		// Assert ready to test
		assertThat(calculNoteService.getCalculateur(lienec, numeroSession) instanceof CalculateurMoyenneFormule).isTrue();

		// Act
		calculNoteService.calculerNotesComposantEtFils(scoInscription, lienec, numeroSession);

		// Assert
		assertThat(noteECC1.noteInitiale()).isEqualTo(new BigDecimal("171.00"));
	}

	@Test
    public void test_calculerNotesComposantEtFilsMoyennePonderee_surchargeSession2() {
		// Arrange
		CalculNoteService calculNoteService = spy(new CalculNoteService(null));
		CalculResultatService calculResultatService = mock(CalculResultatService.class);
	  calculNoteService.setCalculResultatService(calculResultatService);

		IComposant diplome = creerComposant("DIPLOME", "CODE_DIPLOME");
		IComposant versionDiplome = creerComposant("VERSION_DIPLOME", "CODE_VERSION_DIPLOME");
		IComposant licence1 = creerComposant("LICENCE1", "CODE_LICENCE1");
		IComposant semestre1 = creerComposant("SEMESTRE1", "CODE_SEMESTRE_1");
		IComposant semestre2 = creerComposant("SEMESTRE2", "CODE_SEMESTRE_2");
		IUE ue11 = mock(IUE.class);
		IUE ue21 = mock(IUE.class);
		IEC ec21 = mock(IEC.class);
		IAE ae1 = mock(IAE.class);
		when(ae1.baseNote()).thenReturn(20);
		IAE ae2 = mock(IAE.class);
		when(ae2.baseNote()).thenReturn(20);
		
		ILienComposer lienVersionDiplome = creerLienComposer(diplome, versionDiplome, 1, null);
		doReturn(Arrays.asList(lienVersionDiplome)).when(diplome).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienVersionDiplome)).when(diplome).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienLicence1 = creerLienComposer(versionDiplome, licence1, 1, null);
		doReturn(Arrays.asList(lienLicence1)).when(versionDiplome).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienLicence1)).when(versionDiplome).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienSemestre1 = creerLienComposer(licence1, semestre1, 1, null);
		ILienComposer lienSemestre2 = creerLienComposer(licence1, semestre2, 2, null);
		doReturn(Arrays.asList(lienSemestre1, lienSemestre2)).when(licence1).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienSemestre1, lienSemestre2)).when(licence1).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienue11 = creerLienComposer(semestre1, ue11, 1, null);
		doReturn(Arrays.asList(lienue11)).when(semestre1).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienue11)).when(semestre1).liensComposerAvecLesComposantsEnfants();
		
		ILienComposer lienue21 = creerLienComposer(semestre2, ue21, 1, null);
		ILienComposer lienec21 = creerLienComposer(semestre2, ec21, 2, null);
		doReturn(Arrays.asList(lienue21, lienec21)).when(semestre2).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienue21, lienec21)).when(semestre2).liensComposerAvecLesComposantsEnfants();

		ILienComposer lienae1 = creerLienComposer(ec21, ae1, 1, null);
		ILienComposer lienae2 = creerLienComposer(ec21, ae2, 2, null);
		doReturn(Arrays.asList(lienae1, lienae2)).when(ec21).liensParentsWithoutDuplicate();
		doReturn(Arrays.asList(lienae1, lienae2)).when(ec21).liensComposerAvecLesComposantsEnfants();

		IScoInscription scoInscription = mock(IScoInscription.class);
		IInscriptionPedagogique inscriptionPeda = mock(IInscriptionPedagogique.class);
		when(inscriptionPeda.toInscription()).thenReturn(scoInscription);
		doReturn(Arrays.asList(inscriptionPeda)).when(scoInscription).toInscriptionsPedagogiques();
		
		IInscriptionPedagogiqueElement inscriptionPedaEltVersionDiplome = creerInscriptionPedagogiqueElement(lienVersionDiplome, inscriptionPeda);
		INote noteVersionDiplome = creerNote(inscriptionPedaEltVersionDiplome, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteVersionDiplome)).when(inscriptionPedaEltVersionDiplome).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltLicence1 = creerInscriptionPedagogiqueElement(lienLicence1, inscriptionPeda);
		INote noteLicence1 = creerNote(inscriptionPedaEltLicence1, typeNoteCalculee, 1, null);
		INote noteLicence1S2 = creerNote(inscriptionPedaEltLicence1, typeNoteCalculee, 2, null);
		doReturn(Arrays.asList(noteLicence1, noteLicence1S2)).when(inscriptionPedaEltLicence1).toNotes();
		
		IInscriptionPedagogiqueElement inscriptionPedaEltSemestre1 = creerInscriptionPedagogiqueElement(lienSemestre1, inscriptionPeda);
		INote noteSemestre1 = creerNote(inscriptionPedaEltSemestre1, typeNoteCalculee, 1, null);
		INote noteSemestre1S2 = creerNote(inscriptionPedaEltSemestre1, typeNoteCalculee, 2, null);
		doReturn(Arrays.asList(noteSemestre1, noteSemestre1S2)).when(inscriptionPedaEltSemestre1).toNotes();
		
		IInscriptionPedagogiqueElement inscriptionPedaEltSemestre2 = creerInscriptionPedagogiqueElement(lienSemestre2, inscriptionPeda);
		INote noteSemestre2 = creerNote(inscriptionPedaEltSemestre2, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteSemestre2)).when(inscriptionPedaEltSemestre2).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltUE11 = creerInscriptionPedagogiqueElement(lienue11, inscriptionPeda);
		INote noteUE11 = creerNote(inscriptionPedaEltUE11, typeNoteSaisie, 1, new BigDecimal(2));
		INote noteUE11S2 = creerNote(inscriptionPedaEltUE11, typeNoteSaisie, 2, new BigDecimal(12));
		doReturn(Arrays.asList(noteUE11, noteUE11S2)).when(inscriptionPedaEltUE11).toNotes();
		
		IInscriptionPedagogiqueElement inscriptionPedaEltUE21 = creerInscriptionPedagogiqueElement(lienue21, inscriptionPeda);
		INote noteUE21 = creerNote(inscriptionPedaEltUE21, typeNoteSaisie, 1, new BigDecimal(2.34));
		doReturn(Arrays.asList(noteUE21)).when(inscriptionPedaEltUE21).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltEC21 = creerInscriptionPedagogiqueElement(lienec21, inscriptionPeda);
		INote noteEC21C1 = creerNote(inscriptionPedaEltEC21, typeNoteCalculee, 1, null);
		doReturn(Arrays.asList(noteEC21C1)).when(inscriptionPedaEltEC21).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltAE1 = creerInscriptionPedagogiqueElement(lienae1, inscriptionPeda);
		INote noteAE1 = creerNote(inscriptionPedaEltAE1, typeNoteSaisie, 1, new BigDecimal(2.34));
		doReturn(Arrays.asList(noteAE1)).when(inscriptionPedaEltAE1).toNotes();

		IInscriptionPedagogiqueElement inscriptionPedaEltAE2 = creerInscriptionPedagogiqueElement(lienae2, inscriptionPeda);
		INote noteAE2 = creerNote(inscriptionPedaEltAE2, typeNoteSaisie, 1, new BigDecimal(2.34));
		doReturn(Arrays.asList(noteAE2)).when(inscriptionPedaEltAE2).toNotes();

		doReturn(
		    Arrays.asList(inscriptionPedaEltVersionDiplome, inscriptionPedaEltLicence1, inscriptionPedaEltSemestre1, inscriptionPedaEltSemestre2, inscriptionPedaEltUE11,
		    		inscriptionPedaEltUE21, inscriptionPedaEltEC21, inscriptionPedaEltAE1, inscriptionPedaEltAE2)).when(inscriptionPeda).toInscriptionPedagogiqueElements();
		
		// Assert ready to test
		assertThat(calculNoteService.getCalculateur(lienVersionDiplome, 1) instanceof CalculateurMoyennePonderee).isTrue();

		// Act
		calculNoteService.calculerNotesComposantEtFils(scoInscription, lienVersionDiplome, 1);
		
		// Assert
		assertThat(noteSemestre1.noteInitiale()).isEqualTo(new BigDecimal("2.00"));
		assertThat(noteSemestre2.noteInitiale()).isEqualTo(new BigDecimal("2.34"));
		assertThat(noteLicence1.noteInitiale()).isEqualTo(new BigDecimal("2.17"));
		assertThat(noteVersionDiplome.noteInitiale()).isEqualTo(new BigDecimal("2.17"));
		
		// Act
		calculNoteService.calculerNotesComposantEtFils(scoInscription, lienLicence1, 2);
		
		// Assert
		assertThat(noteSemestre1S2.noteInitiale()).isEqualTo(new BigDecimal("12.00"));
		assertThat(noteLicence1S2.noteInitiale()).isEqualTo(new BigDecimal("7.17"));
		
    }
	
	private IFormule getformule(String strformule) {
		IFormule formule = mock(IFormule.class);
		when(formule.formule()).thenReturn(strformule);
		when(formule.moyennePonderee()).thenReturn(0);
		ITypeFormule typeFormule = mock(ITypeFormule.class);
		when(typeFormule.type()).thenReturn(ITypeFormule.TYPEMOYENNE_LBL);
		when(formule.typeFormule()).thenReturn(typeFormule);
		return formule;
	}

	private INote creerNote(IInscriptionPedagogiqueElement inscriptionPedaEltVersionDiplome, ITypeNote typeNoteCalculee, Integer numSession, BigDecimal valeur) {
		INote note = spy(new INote() {

			private BigDecimal noteInitiale;

			public ITypeNote toTypeNote() {
				// TODO Auto-generated method stub
				return null;
			}

			public IInscriptionPedagogiqueElement toInscriptionPedagogiqueElement() {
				// TODO Auto-generated method stub
				return null;
			}

			public void setToTypeNoteRelationship(ITypeNote object) {
				// TODO Auto-generated method stub

			}

			public void setToInscriptionPedagogiqueElementRelationship(IInscriptionPedagogiqueElement object) {
				// TODO Auto-generated method stub

			}

			public void setNoteSurchargee(BigDecimal object) {
				// TODO Auto-generated method stub

			}

			public void setNoteInitiale(BigDecimal noteInitiale) {
				this.noteInitiale = noteInitiale;

			}

			public void setNoSession(Integer noSession) {
				// TODO Auto-generated method stub

			}

			public BigDecimal noteSurchargee() {
				// TODO Auto-generated method stub
				return null;
			}

			public BigDecimal noteInitiale() {
				// TODO Auto-generated method stub
				return noteInitiale;
			}

			public Integer noSession() {
				// TODO Auto-generated method stub
				return null;
			}

			public boolean isSession(int noSession) {
				// TODO Auto-generated method stub
				return false;
			}

			public boolean isCalculee() {
				// TODO Auto-generated method stub
				return false;
			}

			public boolean isSaisie() {
				// TODO Auto-generated method stub
				return false;
			}


			public INoteResultat toNoteResultat() {
				// TODO Auto-generated method stub
				return null;
			}

			public void setToNoteResultatRelationship(INoteResultat value) {
				// TODO Auto-generated method stub
				
			}

			public void setAbsence(Boolean absence) {
				// TODO Auto-generated method stub
				
			}

			public Boolean absence() {
				// TODO Auto-generated method stub
				return null;
			}

			public void setCodeAbsence(String code) {
				// TODO Auto-generated method stub
				
			}

			public String codeAbsence() {
				// TODO Auto-generated method stub
				return null;
			}

			public INoteResultat toNoteResultatCalcule() {
				// TODO Auto-generated method stub
				return null;
			}

			public void setToNoteResultatCalculeRelationship(
					INoteResultat resultat) {
				// TODO Auto-generated method stub
				
			}
		});
		when(note.toTypeNote()).thenReturn(typeNoteCalculee);
		when(note.toInscriptionPedagogiqueElement()).thenReturn(inscriptionPedaEltVersionDiplome);
		note.setNoteInitiale(valeur);
		when(note.noSession()).thenReturn(numSession);
		return note;
	}

	private IComposant creerComposant(String libelle, String code) {
		IComposant composant = mock(IComposant.class);
		when(composant.libelle()).thenReturn(libelle);
		when(composant.code()).thenReturn(code);
		return composant;
	}
	
	private IInscriptionPedagogiqueElement creerInscriptionPedagogiqueElement(ILien lien, IInscriptionPedagogique inscriptionPeda) {
		IInscriptionPedagogiqueElement element = mock(IInscriptionPedagogiqueElement.class);
		when(element.toLien()).thenReturn(lien);
		when(element.toInscriptionPedagogique()).thenReturn(inscriptionPeda);
		return element;
	}

	
	private ILienComposer creerLienComposer(IComposant parent, IComposant enfant, Integer ordre, BigDecimal coefficient){
		ILienComposer lien = mock(ILienComposer.class);
		when(lien.child()).thenReturn(enfant);
		when(lien.parent()).thenReturn(parent);
		when(lien.ordre()).thenReturn(ordre);
		when(lien.coefficient()).thenReturn(coefficient);

		return lien;
	}

	
}
