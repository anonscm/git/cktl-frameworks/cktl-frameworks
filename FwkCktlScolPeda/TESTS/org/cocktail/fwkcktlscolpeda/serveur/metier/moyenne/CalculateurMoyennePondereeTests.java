package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.parboiled.errors.ParsingException;

public class CalculateurMoyennePondereeTests {

	@Before
	public void setUp() {
	}

	@Test
	public void calculerMoyenne() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		notes.put(CalculateurFormuleHelper.getCleNote(2), new BigDecimal(36));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(true).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();
		BigDecimal moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));

		// Assert
		assertEquals(new BigDecimal("15.00"), moyenne);
	}

	@Test
	public void calculerMoyenne_noteNullePeutCalculerTrue() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		notes.put(CalculateurFormuleHelper.getCleNote(2), null);

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(true).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();
		BigDecimal moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));

		// Assert
		assertEquals(new BigDecimal("12.00"), moyenne);
	}

	@Test
	public void calculerMoyenne_noteNullePeutCalculerFalse() {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		notes.put(CalculateurFormuleHelper.getCleNote(2), null);

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(false).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		BigDecimal moyenne = null;
		try {
			moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		} catch (ParsingException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type ParsingException : " + e.getMessage());
			e.printStackTrace();
		} catch (CalculateurMoyenneException e) {
			assertEquals(null, moyenne);
			assertThat(e.getMessage()).isEqualTo(CalculateurMoyenneException.NOTE_NULLE_EXCEPTION);
		}
	}
	
	@Test
	public void calculerMoyenne_noteMissingPeutCalculerFalse() {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		//notes.put(CalculateurFormuleHelper.getCleNote(2), null);

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(false).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		BigDecimal moyenne = null;
		try {
			moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		} catch (ParsingException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type ParsingException : " + e.getMessage());
			e.printStackTrace();
		} catch (CalculateurMoyenneException e) {
			assertEquals(null, moyenne);
			assertThat(e.getMessage()).isEqualTo(CalculateurMoyenneException.NOTE_NULLE_EXCEPTION);
		}
	}
	
	@Test
	public void calculerMoyenne_noteMissingPeutCalculerTrue() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(true).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		
		BigDecimal	moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		assertEquals(new BigDecimal("12.00"), moyenne);
	}
	
	@Test
	public void calculerMoyenne_CoefficientMissing() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		notes.put(CalculateurFormuleHelper.getCleNote(2), new BigDecimal(16));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(true).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		
		BigDecimal	moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		assertEquals(new BigDecimal("10.00"), moyenne);
	}
	
	@Test
	public void calculerMoyenne_BaseNoteMissing() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(12));
		notes.put(CalculateurFormuleHelper.getCleNote(2), new BigDecimal(18));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(true).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		
		BigDecimal	moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		assertEquals(new BigDecimal("14.00"), moyenne);
	}

	@Test
	public void calculerMoyenne_CoefficientNull() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(10));
		notes.put(CalculateurFormuleHelper.getCleNote(2), new BigDecimal(16));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), null);

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), new BigDecimal(40));

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(false).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		BigDecimal moyenne = null;

		moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		assertEquals(new BigDecimal("9.00"), moyenne);
	}
	
	@Test
	public void calculerMoyenne_BaseNoteNulle() throws ParsingException, CalculateurMoyenneException {

		Map<String, BigDecimal> notes = new HashMap<String, BigDecimal>();
		notes.put(CalculateurFormuleHelper.getCleNote(1), new BigDecimal(10));
		notes.put(CalculateurFormuleHelper.getCleNote(2), new BigDecimal(16));

		Map<String, BigDecimal> coeffs = new HashMap<String, BigDecimal>();
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(1), new BigDecimal(2));
		coeffs.put(CalculateurFormuleHelper.getCleCoefficient(2), new BigDecimal(1));

		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(1), new BigDecimal(20));
		baseNotes.put(CalculateurFormuleHelper.getCleBaseNote(2), null);

		// Act
		CalculateurMoyenne calculateurMoyenne = spy(new CalculateurMoyennePonderee());
		doReturn(false).when((CalculateurMoyennePonderee) calculateurMoyenne).peutCalculerMemeSiManqueUneNote();

		BigDecimal moyenne = null;

		moyenne = calculateurMoyenne.calculerMoyenne(notes, coeffs, baseNotes, new BigDecimal(20));
		assertEquals(new BigDecimal("12.00"), moyenne);
	}
}
