package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import static org.fest.assertions.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Test;
import org.parboiled.errors.ParsingException;

public class CalculateurMoyenneFormuleTests extends EOScolTestCase {
	// EOTypeNote
	private EOTypeNote typeNoteSaisie;
	private EOTypeNote typeNoteCalculee;
	// EOTypeLien
	private EOTypeLien typeLienVersionner;
	private EOTypeLien typeLienComposer;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		EOTypeComposant typeDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM,
		    ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTypeComposant typeParcours = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
		EOTypeComposant typeRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEREGROUPEMENT_NOM,
		    ITypeComposant.TYPEREGROUPEMENT_NOM);
		EOTypeComposant typeUE = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEUE_NOM, ITypeComposant.TYPEUE_NOM);
		EOTypeComposant typeEC = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTypeComposant typeAE = EOTestsHelper.creerTypeComposant(editingContext, 6, ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAE_NOM);

		typeNoteSaisie = EOTestsHelper.creerTypeNote(editingContext, EOTypeNote.NOTE_SAISIE, EOTypeNote.NOTE_SAISIE);
		typeNoteCalculee = EOTestsHelper.creerTypeNote(editingContext, EOTypeNote.NOTE_CALCULEE, EOTypeNote.NOTE_CALCULEE);

		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "composer");
		typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 2, "versionner");

		EOTestsHelper.creerRegleLiaison(editingContext, typeDiplome, typeVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeVersionDiplome, typeParcours, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeRegroupement, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeUE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeAE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeUE, typeParcours, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeRegroupement, typeUE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeUE, typeEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeEC, typeAE, typeLienComposer);

		EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");

	}

	@Test
	public void test_calculerMoyenneSansErreur() {
		// Arrange
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule("n1*c1 + n2*c2");

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(15)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal("1.2"));
		coefficients.put("c2", new BigDecimal(0));

		// Assert ready to test

		// Act

		// Assert
		try {
			BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
			assertThat(moyenne).isEqualTo(new BigDecimal("12.00"));

		} catch (CalculateurMoyenneException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type CalculateurMoyenneException : " + e.getMessage());
		} catch (ParsingException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type ParsingException : " + e.getMessage());
		}

	}

	@Test
	public void test_calculerMoyenne_ExceptionNoteSansValeur() {
		// Arrange
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule("n1*c1 + n2*c2");

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal("1.2"));
		coefficients.put("c2", new BigDecimal(0));

		// Assert ready to test

		// Act

		// Assert
		try {
			BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
			assertThat(moyenne).isEqualTo(new BigDecimal("12"));

		} catch (CalculateurMoyenneException e) {
			assertThat(e.getMessage()).isEqualTo("La valeur pour la note 'n2' n'est pas renseignée pour la formule 'n1*c1 + n2*c2'.");
		} catch (ParsingException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type ParsingException : " + e.getMessage());
		}
	}

	@Test
	public void test_calculerMoyenne_ExceptionCoefSansValeur() {
		// Arrange
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule("n1*c1 + n2*c2");

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(15)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal("1.2"));

		// Assert ready to test

		// Act

		// Assert
		try {
			BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
			assertThat(moyenne).isEqualTo(new BigDecimal("12"));

		} catch (CalculateurMoyenneException e) {
			assertThat(e.getMessage()).isEqualTo("La valeur pour le coefficient 'c2' n'est pas renseignée pour la formule 'n1*c1 + n2*c2'.");
		} catch (ParsingException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type ParsingException : " + e.getMessage());
		}
	}

	@Test
	public void test_calculerMoyenneFormuleIncorrecte() {
		// Arrange
		String input = "(n1+/c1 +c2)*n2";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(15)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal("1.2"));
		coefficients.put("c2", new BigDecimal("2"));
		// Assert
		try {
			BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
			assertThat(moyenne).isEqualTo(new BigDecimal("12"));

		} catch (CalculateurMoyenneException e) {
			fail("Le calcul de moyenne a renvoyé une exception de type CalculateurMoyenneException : " + e.getMessage());
		} catch (ParsingException e) {
			assertThat(e.getMessage()).isEqualTo(CalculateurMoyenneException.ERRORS_FORMULE_EXCEPTION);
		}

	}

	@Test
	public void test_calculerMoyenneMin() throws ParsingException, CalculateurMoyenneException {
		// Arrange
		String input = "min(n1,n2)";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(15)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		// Assert

		BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(moyenne).isEqualTo(new BigDecimal("10.00"));
	}

	@Test
	public void test_calculerMoyenneMax() throws ParsingException, CalculateurMoyenneException {
		// Arrange
		String input = "Max(n1,n2)";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(15)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		// Assert

		BigDecimal moyenne = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(moyenne).isEqualTo(new BigDecimal("15.00"));
	}

	@Test
	public void test_calculerSuperieur() throws ParsingException, CalculateurMoyenneException {

		String input = "n1 > 5";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		// Assert

		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(admis).isEqualTo(new BigDecimal("1.00"));
	}

	@Test
	public void test_OperateursLogiques() throws ParsingException, CalculateurMoyenneException {
		// superieur strictement
		verifieOperateurLogique(10.2, (double) 10, ">", new BigDecimal("1.00"));
		verifieOperateurLogique(9.2, (double) 10, ">", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 10, (double) 10, ">", new BigDecimal("0.00"));

		// superieur ou égal
		verifieOperateurLogique(10.2, (double) 10, ">=", new BigDecimal("1.00"));
		verifieOperateurLogique(9.2, (double) 10, ">=", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 10, (double) 10, ">=", new BigDecimal("1.00"));

		// inférieur strictement
		verifieOperateurLogique(8.3, (double) 10, "<", new BigDecimal("1.00"));
		verifieOperateurLogique(11.5, (double) 10, "<", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 10, (double) 10, "<", new BigDecimal("0.00"));

		// inférieur ou égal
		verifieOperateurLogique(8.3, (double) 10, "<=", new BigDecimal("1.00"));
		verifieOperateurLogique(11.5, (double) 10, "<=", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 10, (double) 10, "<=", new BigDecimal("1.00"));

		// égal
		verifieOperateurLogique(8.0, (double) 8, "=", new BigDecimal("1.00"));
		verifieOperateurLogique(8.3, 8.4, "=", new BigDecimal("0.00"));

		// pas égal
		verifieOperateurLogique(8.0, (double) 8, "!=", new BigDecimal("0.00"));
		verifieOperateurLogique(8.3, 8.4, "!=", new BigDecimal("1.00"));

		// et
		verifieOperateurLogique((double) 1, (double) 1, "et", new BigDecimal("1.00"));
		verifieOperateurLogique((double) 1, (double) 0, "et", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 0, (double) 1, "et", new BigDecimal("0.00"));
		verifieOperateurLogique((double) 0, (double) 0, "et", new BigDecimal("0.00"));

		// ou
		verifieOperateurLogique((double) 1, (double) 1, "ou", new BigDecimal("1.00"));
		verifieOperateurLogique((double) 1, (double) 0, "ou", new BigDecimal("1.00"));
		verifieOperateurLogique((double) 0, (double) 1, "ou", new BigDecimal("1.00"));
		verifieOperateurLogique((double) 0, (double) 0, "ou", new BigDecimal("0.00"));
	}

	private void verifieOperateurLogique(Double valeurNote, Double valeurSeuil, String operator, BigDecimal resultatAttendu) throws ParsingException,
	    CalculateurMoyenneException {
		String input = "n1 " + operator + " " + valeurSeuil.toString();
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(valeurNote)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		// Assert

		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(admis).isEqualTo(resultatAttendu);
	}

	@Test
	public void test_calculerAdmission() throws ParsingException, CalculateurMoyenneException {

		String input = "(n1*c1+n2*c2+n3*c3)/(c1+c2+c3) >= 10 et n1>=8 et n2<20 et n3>7";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n3", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal(1));
		coefficients.put("c2", new BigDecimal(1));
		coefficients.put("c3", new BigDecimal(1));
		// Assert

		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(admis).isEqualTo(new BigDecimal("1.00"));

	}

	@Test
	public void test_calculerAdmissionKO() throws ParsingException, CalculateurMoyenneException {

		String input = "(n1*c1+n2*c2+n3*c3)/(c1+c2+c3) >= 10 et n1>=8 et n2<20 et n3>7";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(9)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n3", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		coefficients.put("c1", new BigDecimal(1));
		coefficients.put("c2", new BigDecimal(1));
		coefficients.put("c3", new BigDecimal(1));

		// Assert
		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, null);
		assertThat(admis).isEqualTo(new BigDecimal("0.00"));
	}
	
	@Test
	public void test_calculerAdmissionAvecMoyenne() throws ParsingException, CalculateurMoyenneException {

		String input = "moyenne >= 10 et n1>=8 et n2<20 et n3>7";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(9)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n3", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		
		INote valeurNoteMoyenne = EOTestsHelper.creerNote(editingContext, typeNoteCalculee, null, null, new BigDecimal(10.2));

		// Assert
		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, valeurNoteMoyenne);
		assertThat(admis).isEqualTo(new BigDecimal("1.00"));
	}
	
	@Test
	public void test_calculerAdmissionAvecMoyenneKO() throws ParsingException, CalculateurMoyenneException {

		String input = "moyenne >= 10 et n1>=8 et n2<20 et n3>7";
		CalculateurMoyenne calculateur = new CalculateurMoyenneFormule(input);

		Map<String, INote> notes = new HashMap<String, INote>();
		notes.put("n1", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(9)));
		notes.put("n2", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));
		notes.put("n3", EOTestsHelper.creerNote(editingContext, typeNoteSaisie, null, null, new BigDecimal(10)));

		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		
		INote valeurNoteMoyenne = EOTestsHelper.creerNote(editingContext, typeNoteCalculee, null, null, new BigDecimal(9));

		// Assert
		BigDecimal admis = ((CalculateurMoyenneFormule) calculateur).calculerMoyenne(notes, coefficients, valeurNoteMoyenne);
		assertThat(admis).isEqualTo(new BigDecimal("0.00"));
	}

}
