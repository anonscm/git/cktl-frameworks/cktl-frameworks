package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe teste les périmetres d'application
 */
public class EOPerimetreGirofleTest extends EOScolTestCase {

	@Test
	public void test_ajouter() {
		// Arrange	
		EOGdPerimetreGirofle perimetre = spy(new EOGdPerimetreGirofle());

		perimetre.setAppId(1);
		perimetre.setLibelle("test");
		doReturn(1).when(perimetre).persIdCreation();
		doReturn(new NSTimestamp()).when(perimetre).dateCreation();

		editingContext.insertObject(perimetre);

		// Act
		editingContext.saveChanges();

		// Assert
		assertEquals(EOGdPerimetreGirofle.fetchAllSco_GdPerimetreGirofles(editingContext).count(), 1);

	}
}
