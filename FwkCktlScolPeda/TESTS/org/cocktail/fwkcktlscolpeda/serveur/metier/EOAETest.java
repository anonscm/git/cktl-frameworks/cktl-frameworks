package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests de la classe EOAE
 */
public class EOAETest extends EOScolTestCase {

	@Before
	public void setUp() {
	    super.setUp();
	    initialiserContexte();
	}
	
	@Test
    public void test_copy() {
	    // Arrange
		EOTypeAE typeAE = EOTestsHelper.creerTypeAE(editingContext, 1, "ae", "ae");
		EOAE ae = EOTestsHelper.creerAE(editingContext, "CODEAE", "10", "11", typeAE);
		ae.setBaseNote(10);
		ae.setDureeMax(90);
		ae.setDureeMin(30);
		ae.setEpreuve(1);
		ae.setSansNote(1);
		ae.setTypeAERelationship(typeAE);
		editingContext.saveChanges();
	    // Assert ready to test

	    // Act
		EOAE newAE = ae.copy(2014, null);

	    // Assert
		assertComposantsIdentiques(ae, newAE);
		assertEquals(ae.baseNote(), newAE.baseNote());
		assertEquals(ae.dureeMax(), newAE.dureeMax());
		assertEquals(ae.dureeMin(), newAE.dureeMin());
		assertEquals(ae.epreuve(), newAE.epreuve());
		assertEquals(ae.sansNote(), newAE.sansNote());
		assertEquals(ae.typeAE(), newAE.typeAE());		
    }
	
	
	//Private Helper
	private void initialiserContexte() {
		// initialiser le contexte
		EOTypeComposant typeComposanteAE = new EOTypeComposant();
		typeComposanteAE.setNom(ITypeComposant.TYPEAE_NOM);
		typeComposanteAE.setLibelle(ITypeComposant.TYPEAE_NOM);
		typeComposanteAE.setId(1);
		editingContext.insertSavedObject(typeComposanteAE);
	}
}
