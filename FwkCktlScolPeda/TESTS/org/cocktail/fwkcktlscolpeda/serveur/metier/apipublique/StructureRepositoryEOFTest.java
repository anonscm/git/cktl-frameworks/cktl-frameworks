package org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.wounit.rules.MockEditingContext;

public class StructureRepositoryEOFTest {

	@Rule
    public MockEditingContext editingContext = new MockEditingContext("Sco_scolarite");
	
	private EOStructure etablissement;
	private EOStructure composanteSimpleVide;
	private EOStructure composanteSimple;
	private EOStructure composanteScol1;
	private EOStructure composanteScol1p1;
	private EOStructure etablissementSecondaire;
	private EOStructure composanteScol2;
	private EOStructure composanteScol2p1;
	private EOStructure departementEnseignement0;
	private EOStructure departementEnseignement1;
	private EOStructure departementEnseignement1p1;
	private EOStructure departementEnseignement1p1p1;
	private EOStructure departementEnseignement2;
	private EOStructure composanteScol3;
	private EOStructure departementEnseignement31;
	
	@Before
	public void setup() {
		EOTypeGroupe typeGroupeCS = EOTypeGroupe.creerInstance(editingContext);
		typeGroupeCS.setTgrpCode(EOTypeGroupe.TGRP_CODE_CS);

		EOTypeGroupe typeGroupeDE = EOTypeGroupe.creerInstance(editingContext);
		typeGroupeDE.setTgrpCode(EOTypeGroupe.TGRP_CODE_DE);
		
		EORepartTypeGroupe repartTypeGroupeCS = EORepartTypeGroupe.creerInstance(editingContext);
		repartTypeGroupeCS.setTypeGroupeRelationship(typeGroupeCS);
		repartTypeGroupeCS.setTgrpCode(EOTypeGroupe.TGRP_CODE_CS);
		
		EORepartTypeGroupe repartTypeGroupeDE = EORepartTypeGroupe.creerInstance(editingContext);
		repartTypeGroupeDE.setTypeGroupeRelationship(typeGroupeDE);
		repartTypeGroupeDE.setTgrpCode(EOTypeGroupe.TGRP_CODE_DE);

		etablissement = creerStructure("ETAB1", "E", null, null, "N");
		
		composanteSimpleVide = creerStructure("COMPO_SIMPLE_VIDE", "C", null, etablissement, "O");
		composanteSimple = creerStructure("COMPO_SIMPLE", "C", null, etablissement, "O");
		departementEnseignement0 = creerStructure("DEP_0", "C", repartTypeGroupeDE, composanteSimple, "O");
		composanteScol1 = creerStructure("COMPO_SCOL_1", "C", repartTypeGroupeCS, composanteSimple, "O");
		departementEnseignement1 = creerStructure("DEP_1", "C", repartTypeGroupeDE, composanteScol1, "O");
		departementEnseignement1p1 = creerStructure("DEP_1.1", "C", repartTypeGroupeDE, departementEnseignement1, "O");
		composanteScol1p1 = creerStructure("COMPO_SCOL_1.1", "C", repartTypeGroupeCS, composanteScol1, "O");
		departementEnseignement1p1p1 = creerStructure("DEP_1.1.1", "C", repartTypeGroupeDE, composanteScol1p1, "O");
		etablissementSecondaire = creerStructure("ETAB_SECOND", "ES", repartTypeGroupeCS, etablissement, "O");
		departementEnseignement2 = creerStructure("DEP_2", "C", repartTypeGroupeDE, etablissementSecondaire, "O");
		composanteScol2 = creerStructure("COMPO_SCOL_2", "C", repartTypeGroupeCS, etablissementSecondaire, "O");
		composanteScol2p1 = creerStructure("COMPO_SCOL_2.1", "C", repartTypeGroupeCS, composanteScol2, "O");
		composanteScol3 = creerStructure("COMPO_SCOL_3", "C", repartTypeGroupeCS, etablissementSecondaire, "N");
		departementEnseignement31 = creerStructure("DEP_3.1", "C", repartTypeGroupeDE, etablissementSecondaire, "N");
	}
	
	private EOStructure creerStructure(String code, String type, EORepartTypeGroupe repartTypeGroupe, EOStructure structurePere, String isArchivee) {
		EOStructure structure = Mockito.spy(new EOStructure());
		Mockito.doReturn(null).when(structure).construireCStructure();
		Mockito.doReturn(isArchivee).when(structure).temValide();		//Les structures archivées sont invalides dans le SI
		editingContext.insertObject(structure);
		structure.setCStructure(code);
		structure.setCTypeStructure(type);
		structure.setToStructurePereRelationship(structurePere != null ? structurePere : structure);
		structure.addToToRepartTypeGroupesRelationship(repartTypeGroupe);
		return structure;
	}
	
	@Test
	public void testGetStructuresAffectablesAuxComposants() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> structuresAffectablesAuxComposants = structureRepository.getStructuresAffectablesAuxComposants();
		
		assertEquals(5, structuresAffectablesAuxComposants.size());
	}

	@Test
	public void testGetStructuresAffectablesAuxComposantsIStructure() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> structuresAffectablesAuxComposants = structureRepository.getStructuresAffectablesAuxComposants(etablissement);
		
		assertEquals(5, structuresAffectablesAuxComposants.size());
		assertTrue(structuresAffectablesAuxComposants.contains(composanteScol1p1));
		assertTrue(structuresAffectablesAuxComposants.contains(composanteScol2));
	}

	@Test
	public void testGetComposantesScolarites() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> structuresAffectablesAuxComposants = structureRepository.getComposantesScolarites();
		
		assertEquals(5, structuresAffectablesAuxComposants.size());
	}

	@Test
	public void testGetComposantesScolaritesIStructure() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> structuresAffectablesAuxComposants = structureRepository.getComposantesScolarites(etablissement);
		
		assertEquals(5, structuresAffectablesAuxComposants.size());
		assertTrue(structuresAffectablesAuxComposants.contains(composanteScol1p1));
		assertTrue(structuresAffectablesAuxComposants.contains(composanteScol2));
	}

	@Test
	public void testGetDepartementsEnseignements() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> departements = structureRepository.getDepartementsEnseignements();
		
		assertEquals(5, departements.size());
	}

	@Test
	public void testGetDepartementsEnseignementsIStructure() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> departements = structureRepository.getDepartementsEnseignements(etablissement);
		
		assertEquals(5, departements.size());
		assertTrue(departements.contains(departementEnseignement2));
	}
	
	@Test
	public void testGetStructuresEnseignementIStructure() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		List<IStructure> structuresEnseignement = structureRepository.getStructuresEnseignement(etablissement);
		
		assertEquals(10, structuresEnseignement.size());
		assertTrue(structuresEnseignement.contains(departementEnseignement2));
	}
	
	@Test
	public void testGetStructuresEnseignement() {
		StructureRepository structureRepository = new StructureRepositoryEOF(editingContext);
		
		List<IStructure> structures = structureRepository.getStructuresEnseignement();
		
		assertEquals(10, structures.size());
	}

}
