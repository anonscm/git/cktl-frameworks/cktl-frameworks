package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class EOLienComposerTest extends EOScolTestCase {

	private EOTypeComposant typeComposantConcret1;
	private EOTypeComposant typeComposantConcret2;

	private EOTypeLien typeLienComposer;

	private EOComposant composantParent1;
	private EOComposant composantChild1;
	private EOComposant composantParent2;
	private EOComposant composantChild2;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		typeComposantConcret1 = EOTestsHelper.creerTypeComposant(editingContext, 1, "Concret1", "Concret1");
		editingContext.insertObject(typeComposantConcret1);
		typeComposantConcret2 = EOTestsHelper.creerTypeComposant(editingContext, 2, "Concret2", "Concret2");
		editingContext.insertObject(typeComposantConcret2);

		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, EOTypeLien.TYPECOMPOSER_LBL);
		editingContext.insertObject(typeLienComposer);

		EORegleLiaison r1 = EOTestsHelper.creerRegleLiaison(editingContext, typeComposantConcret1, typeComposantConcret2, typeLienComposer);
		editingContext.insertObject(r1);

		composantParent1 = EOTestsHelper.creerComposantConcret(editingContext, "parent1", "parent1", 1, typeComposantConcret1);
		composantChild1 = EOTestsHelper.creerComposantConcret(editingContext, "enfant1", "enfant1", 2, typeComposantConcret2);

		composantParent2 = spy(EOTestsHelper.creerComposantConcret(editingContext,"parent2", "parent2", 1, typeComposantConcret1));
		doReturn(new NSArray<EOLienComposer>()).when(composantParent2).liensComposerAvecLesComposantsEnfants();
		composantChild2 = EOTestsHelper.creerComposantConcret(editingContext,"enfant2", "enfant2", 2, typeComposantConcret2);
		editingContext.saveChanges();
	}

	@Test
	public void test_assignTo() {
		// Arrange
		EOLienComposer lienComposer = EOComposantFactory.createLienComposer(composantParent1, composantChild1);
		lienComposer.setBaseNote(1);
		lienComposer.setChoixECTS(12);
		lienComposer.setChoixMax(4);
		lienComposer.setChoixMin(4);
		lienComposer.setCoefficient(new BigDecimal(12.5));
		lienComposer.setDureeMax(new BigDecimal(41.2));
		lienComposer.setDureeMin(new BigDecimal(1.7));
		lienComposer.setEpreuve(1);
		lienComposer.setObligatoire(true);
		lienComposer.setOrdre(15);
		lienComposer.setSansComposant(1);
		lienComposer.setSansNote(1);
		lienComposer.setSeuil(12);
		lienComposer.setValeurTempsEtudiant((float) 4.2);
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		creditable.setBaseNote(12);
		lienComposer.addToCreditablesRelationship(creditable);
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		EOLienComposer nouveauLienComposer = EOComposantFactory.createLienComposer(composantParent2, composantChild2);
		lienComposer.assignTo(nouveauLienComposer);
		//
		// // Assert
		assertEquals(lienComposer.creditable().baseNote(), nouveauLienComposer.creditable().baseNote());
		assertEquals(lienComposer.typeLien(), nouveauLienComposer.typeLien());
		assertEquals(lienComposer.baseNote(), nouveauLienComposer.baseNote());
		assertEquals(lienComposer.choixECTS(), nouveauLienComposer.choixECTS());
		assertEquals(lienComposer.choixMax(), nouveauLienComposer.choixMax());
		assertEquals(lienComposer.choixMin(), nouveauLienComposer.choixMin());
		assertEquals(lienComposer.coefficient(), nouveauLienComposer.coefficient());
		assertEquals(lienComposer.dureeMax(), nouveauLienComposer.dureeMax());
		assertEquals(lienComposer.dureeMin(), nouveauLienComposer.dureeMin());
		assertEquals(lienComposer.epreuve(), nouveauLienComposer.epreuve());
		assertEquals(lienComposer.obligatoire(), nouveauLienComposer.obligatoire());
		assertEquals(lienComposer.ordre(), nouveauLienComposer.ordre());
		assertEquals(lienComposer.sansComposant(), nouveauLienComposer.sansComposant());
		assertEquals(lienComposer.sansNote(), nouveauLienComposer.sansNote());
		assertEquals(lienComposer.seuil(), nouveauLienComposer.seuil());
		assertEquals(lienComposer.valeurTempsEtudiant(), nouveauLienComposer.valeurTempsEtudiant());

	}
}
