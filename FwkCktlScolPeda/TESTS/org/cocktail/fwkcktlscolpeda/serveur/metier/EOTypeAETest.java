package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

import com.wounit.annotations.Dummy;

import er.extensions.eof.ERXQ;


public class EOTypeAETest extends EOScolTestCase {
	
    @Dummy
    private EOTypeAE eoTypeAE;
    
    private static final String expectedCode = "unCode";
    private static final String expectedLibelle = "unLibelle";

    @Before
    public void setUp() {
    	super.setUp();
    	eoTypeAE = new EOTypeAE();
    	eoTypeAE.setCode(expectedCode);
    	eoTypeAE.setLibelle(expectedLibelle);
    	eoTypeAE.setId(1);
    }

    @Test
	public void test_type() throws Exception {
		assertNotNull(EOTypeAE.class);
	}

	@Test
	public void test_instantiation() throws Exception {
		assertNotNull(eoTypeAE);
	}

	@Test
	public void test_insert() throws Exception {
		
		editingContext.insertSavedObject(eoTypeAE);
		
		EOTypeAE insertedTypeAE = EOTypeAE.fetchSco_TypeAE(editingContext, ERXQ.equals(EOTypeAE.CODE_KEY, eoTypeAE.code()));
		assertNotNull(insertedTypeAE);
		assertEquals(expectedCode, insertedTypeAE.code());
		assertEquals(expectedLibelle, insertedTypeAE.libelle());
	}

	@Test
	public void test_supprimer() throws Exception {
		// Arrange
		editingContext.insertSavedObject(eoTypeAE);

		EOTypeAE insertedTypeAE = EOTypeAE.fetchSco_TypeAE(editingContext, ERXQ.equals(EOTypeAE.CODE_KEY, eoTypeAE.code()));
		assertNotNull(insertedTypeAE);

		// Act
		editingContext.deleteObject(insertedTypeAE);
		editingContext.saveChanges();

		// Assert
		insertedTypeAE = EOTypeAE.fetchSco_TypeAE(editingContext, ERXQ.equals(EOTypeAE.CODE_KEY, eoTypeAE.code()));
		assertNull(insertedTypeAE);
	}

	@Test
    public void test_validateCode_nouveauCode(){
		// Arrange
		editingContext.insertSavedObject(eoTypeAE);
	
		String newCode = "unAutreCode";
		EOTypeAE target = new EOTypeAE();
		editingContext.insertObject(target);
	
		// Act
		String actualCode = (String) target.validateCode(newCode);
	
		// Assert
		assertEquals(newCode, actualCode);
    }
    
    @Test
    public void test_validateCode_memeTypeAE(){
		// Arrange
		editingContext.insertSavedObject(eoTypeAE);
		// Act
		String actualCode = (String) eoTypeAE.validateCode(eoTypeAE.code());
	
		// Assert
		assertEquals(expectedCode, actualCode);
    }

    @Test
    public void test_validateCode_codeExistant() throws Exception {
		// Arrange
		editingContext.insertSavedObject(eoTypeAE);
	
		// Assert ready to test
		EOTypeAE insertedTypeAE = EOTypeAE.fetchSco_TypeAE(editingContext, ERXQ.equals(EOTypeAE.CODE_KEY, eoTypeAE.code()));
		assertNotNull(insertedTypeAE);
	
		String newCode = expectedCode;
		EOTypeAE target = new EOTypeAE();
		editingContext.insertObject(target);
		try {
		    // Act
		    target.validateCode(newCode);
		    fail("Expected exception was not thrown!");
	
		} catch (Exception e) {
		    // Assert
		    //do nothing - the test passes
		}
    }

}
