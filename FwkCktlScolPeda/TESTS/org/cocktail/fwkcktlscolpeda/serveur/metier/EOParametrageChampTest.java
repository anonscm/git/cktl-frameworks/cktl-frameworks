package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;

import er.extensions.eof.ERXEnterpriseObjectCache;

@RunWith(MockitoJUnitRunner.class)
public class EOParametrageChampTest extends EOScolTestCase {

	@UnderTest
	private EOParametrageChamp parametrageChamp;
	@Dummy
	private EOChamp champ;
	@Dummy
	private EOParametrageInscription parametrageInscription;
	@Dummy
	private EOTypeParametreAffichage typeGestionnaire;
	@Dummy
	private EOTypeParametreAffichage typeAdmission;
	@Dummy
	private EOTypeParametreAffichage typePreinscription;
	@Dummy
	private EOTypeParametreAffichage typReinscription;
	
	@Before
	public void setUp() {
		super.setUp();
		// Nécessaire pour le cache
		ERXEnterpriseObjectCache<EOParametrageChamp> mockCache = 
				EOTestsHelper.createCacheMock(EOParametrageChamp.class, EOParametrageChamp.TO_CHAMP.dot(EOChamp.CODE_KEY).key(), editingContext);
		mockCache.setRetainObjects(true);
		EOParametrageChamp.setChampCacheForParametrage(mockCache, EOParametrageInscription.INITIAL);
	}
	
	@Test
	public void testIsModifiable() {
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		typeGestionnaire.setCode(EOTypeParametreAffichage.MODIFIABLE);
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		assertTrue(parametrageChamp.isModifiable(typeConnexion));
	}
	
	@Test
	public void testIsNotModifiable() {
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		assertFalse(parametrageChamp.isModifiable(typeConnexion));
	}
	
	@Test
	public void testIsLecture() {
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		typeGestionnaire.setCode(EOTypeParametreAffichage.LECTURE);
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		assertTrue(parametrageChamp.isLisible(typeConnexion));
	}
	
	@Test
	public void testIsInvisible() {
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		typeGestionnaire.setCode(EOTypeParametreAffichage.INVISIBLE);
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		assertTrue(parametrageChamp.isInvisible(typeConnexion));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testWrongTypeAffichageChamp() {
		String typeConnexion = EOParametrageChamp.TYPE_GESTIONNAIRE;
		typeGestionnaire.setCode("Wrong type champ");
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		parametrageChamp.isInvisible(typeConnexion);
	}
	
	@Test
	public void testParametreAffichageFor() {
		String parametrage = EOParametrageInscription.INITIAL;
		String codeChamp = "CadreA";
		champ.setCode(codeChamp);
		parametrageInscription.setLibelle(parametrage);
		parametrageChamp = EOParametrageChamp.createSco_ParametrageChamp(
				editingContext, new NSTimestamp(), new NSTimestamp(), 999, 999, champ,  
				parametrageInscription, typeAdmission, typeGestionnaire,
				typePreinscription, typReinscription);
		EOParametrageChamp resultat = EOParametrageChamp.parametrageChampFor(editingContext, codeChamp, parametrage);
		assertNotNull(resultat);
	}
	
}
