package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.wounit.rules.MockEditingContext;

public class TestsInitializer {

    private MockEditingContext editingContext;
    private EOTypeLien typeLiencomposer;
    private EOTypeLien typeLienVersionner;
    private EOTypePeriode typePeriodeAnnee;
    private EOTypePeriode typePeriodeSemestre;
    private EOTypeComposant typeDiplome;
    private EOTypeComposant typeVersion;
    private EOTypeComposant typePeriode;
    private EOTypeComposant typeEc;
    private EOTypeComposant typeParcours;
    private EOStatutFormation statutFormation;
 	private EOGradeUniversitaire gradeUniversitaireMaster;
 	private EOTypeFormation typeFormation;
 	
    public TestsInitializer(MockEditingContext editingContext) {
        this.editingContext = editingContext;
        init();
    }
    
    public void init() {
        typeLiencomposer = EOTestsHelper.creerTypeLien(editingContext, 1, EOTypeLien.TYPECOMPOSER_LBL);
        typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 2, EOTypeLien.TYPEVERSIONNER_LBL);
        
        typePeriodeAnnee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, "Année");
        typePeriodeSemestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, "Semestre");
        
        typeDiplome = EOTestsHelper.creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
        typeVersion = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
        typePeriode = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
        typeEc = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
        typeParcours = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
        EOTypeComposant typeAP = EOTestsHelper.creerTypeComposant(editingContext, 6, ITypeComposant.TYPEAP_NOM, ITypeComposant.TYPEAP_NOM);
        EOTypeComposant typeAE = EOTestsHelper.creerTypeComposant(editingContext, 7, ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAE_NOM);
        
        EOTestsHelper.creerRegleLiaison(editingContext, typeDiplome, typeVersion, typeLienVersionner);
        EOTestsHelper.creerRegleLiaison(editingContext, typeVersion, typePeriode, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typePeriode, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typeEc, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typeVersion, typeParcours, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typePeriode, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typePeriode, typeParcours, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeEc, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typeEc, typeAP, typeLiencomposer);
        EOTestsHelper.creerRegleLiaison(editingContext, typeAP, typeAE, typeLiencomposer);
        
        statutFormation = EOTestsHelper.creerStatutFormation(editingContext, 1, "S", "STATUT");
        
        gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 2);
        
        typeFormation = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
        
    }

    public EOTypeLien getTypeLiencomposer() {
        return typeLiencomposer;
    }

    public EOTypeLien getTypeLienVersionner() {
        return typeLienVersionner;
    }

    public EOTypePeriode getTypePeriodeAnnee() {
        return typePeriodeAnnee;
    }

    public EOTypePeriode getTypePeriodeSemestre() {
        return typePeriodeSemestre;
    }

    public EOTypeComposant getTypeDiplome() {
        return typeDiplome;
    }

    public EOTypeComposant getTypeVersion() {
        return typeVersion;
    }

    public EOTypeComposant getTypePeriode() {
        return typePeriode;
    }

    public EOTypeComposant getTypeEc() {
        return typeEc;
    }

    public EOTypeComposant getTypeParcours() {
        return typeParcours;
    }

    public EOStatutFormation getStatutFormation() {
        return statutFormation;
    }
    
    public EOGradeUniversitaire getGradeUniversitaire() {
    	return gradeUniversitaireMaster;
    }
    
    public EOTypeFormation getTypeFormation() {
    	return typeFormation;
    }
    
    public MockEditingContext getEditingContext() {
        return editingContext;
    }
    
}
