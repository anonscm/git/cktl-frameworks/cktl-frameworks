package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Calendar;

import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wounit.annotations.Dummy;

public class EODiplomeTest extends EOScolTestCase {

	@Dummy
	private EODomaine domaine;
	private EOSpecialite specialite;
	private EOMention mention;
	private EOGradeUniversitaire gradeUniversitaire;
	private EOTypeOrganisation typeOrganisation;
	private EOTypeFormation typeFormation;
	private EOStatutFormation statutFormationConstruction;
	private EOStatutFormation statutFormationProduction;
	private EOTypePeriode typePeriodeAnnee;
	private EOTypePeriode typePeriodeSemestre;
	private EOSiseSecteurDisciplinaire secteurDisciplinaire;
	private EOSiseDiscipline siseDiscipline;
	private EONiveauAccesDiplome niveauAccesDiplome;
	private static final String expectedCode = "unCode";
	private static final String expectedSISE = "1";
	private static final String expectedSISEEtablissement = "2";
	private static final String expectedLibelle = "unLibelle";
	private static final String expectedLibelleCourt = "unLibelleCourt";

	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	/**
	 * Initialise le contexte necessaire à la création d'un composant de type Diplome
	 */
	private void initialiserContexte() {
		EOTypeComposant typeComposantDiplome = EOTestsHelper
		    .creerTypeComposant(editingContext, 1, ITypeComposant.TYPEDIPLOME_NOM, ITypeComposant.TYPEDIPLOME_NOM);
		EOTypeComposant typeComposantVersionDiplome = EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEVERSIONDIPLOME_NOM,
		    ITypeComposant.TYPEVERSIONDIPLOME_NOM);
		EOTypeComposant typeComposantPeriode = EOTestsHelper
		    .creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
		EOTypeComposant typeComposantEC = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTypeComposant typeComposantRegroupement = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEREGROUPEMENT_NOM,
		    ITypeComposant.TYPEREGROUPEMENT_NOM);

		EOTypeLien typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 1, "versionner");
		EOTypeLien typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 2, "composer");

		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantDiplome, typeComposantVersionDiplome, typeLienVersionner);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantVersionDiplome, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantPeriode, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantEC, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeComposantPeriode, typeComposantRegroupement, typeLienComposer);

		// DOMAINE
		domaine = new EODomaine();
		domaine.setId(1);
		domaine.setCode("DOM");
		domaine.setLibelle("Domaine");
		editingContext.insertSavedObject(domaine);

		// SPECIALITE
		specialite = new EOSpecialite();
		specialite.setId(1);
		specialite.setCode("SPE");
		specialite.setLibelle("Spécialité");
		editingContext.insertSavedObject(specialite);

		// TYPE GRADE LICENCE
		gradeUniversitaire = EOTestsHelper.creerGradeUniversitaire(editingContext, "LICENCE", "Licence", 3);

		// TYPE_PERIODE SEMESTRIELLE
		typeOrganisation = new EOTypeOrganisation();
		typeOrganisation.setId(1);
		typeOrganisation.setType("SEMESTRIELLE");
		typeOrganisation.setLibelle("Semestre");
		typeOrganisation.setNbPeriodes(2);
		editingContext.insertSavedObject(typeOrganisation);

		// STATUT_FORMATION Formation Initiale
		statutFormationConstruction = EOTestsHelper.creerStatutFormation(editingContext, 1, "ECC", "En construction");
		statutFormationProduction = EOTestsHelper.creerStatutFormation(editingContext, 2, "EP", "En production");

		// TYPE_FORMATION Formation Initiale
		typeFormation = new EOTypeFormation();
		typeFormation.setId(1);
		typeFormation.setCode("FI");
		typeFormation.setLibelle("Formation Initiale");
		editingContext.insertSavedObject(typeFormation);

		typePeriodeAnnee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);
		typePeriodeSemestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, EOTypePeriode.CODE_SEMESTRE);
		
		siseDiscipline = EOTestsHelper.creerSiseDiscipline(editingContext,"cdisc","ldisc");
		secteurDisciplinaire = EOTestsHelper.creerSiseSecteurDisciplinaire(editingContext,"csect","lsect", siseDiscipline);
		niveauAccesDiplome = EOTestsHelper.creerNiveauAccesDiplome(editingContext,"libelle",0);
	}

	// Tests
	@Test
	public void testEnregistrerAvecGradeEtTypePeriode() {
		// Arrange
		int annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationConstruction);
		diplome.setGradeUniversitaireRelationship(gradeUniversitaire);
		diplome.setTypeOrganisationRelationship(typeOrganisation);

		// Assert Ready to Test
		assertNotNull(diplome);

		// Act
		EOTestsHelper.setValidateur(diplome);
		diplome.enregistrer(annee, 0);

		// Assert
		assertEquals(1, diplome.domaines().size());
		assertNotNull(diplome.statutFormation());
		assertNotNull(diplome.typeFormation());
		assertEquals(1, diplome.liensParents().size());
		assertNotNull(diplome.getVersionDiplome(annee));
		assertEquals((int) gradeUniversitaire.nbUnitesTemps(), diplome.getVersionDiplome(annee).liensParents().size());
		assertEquals((int) typeOrganisation.nbPeriodes(), ((EOLien) (diplome.getVersionDiplome(annee).liensParents().get(0))).child().liensParents().size());
		assertNotNull(diplome.habilitation());
	}

	@Test
	public void testEnregistrerAvecGradeEtSansTypePeriode() {
		// Arrange
		int annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationConstruction);
		diplome.setGradeUniversitaireRelationship(gradeUniversitaire);
		diplome.setTypeOrganisationRelationship(null);

		// Assert Ready to Test
		assertNotNull(diplome);

		// Act
		diplome.enregistrer(annee, 0);
		EOTestsHelper.setValidateur(diplome);

		// Assert
		assertEquals(1, diplome.domaines().size());
		assertNotNull(diplome.statutFormation());
		assertNotNull(diplome.typeFormation());
		assertEquals(1, diplome.liensParents().size());
		assertNotNull(diplome.getVersionDiplome(annee));
		assertEquals((int) gradeUniversitaire.nbUnitesTemps(), diplome.getVersionDiplome(annee).liensParents().size());
		assertEquals(0, ((EOLien) (diplome.getVersionDiplome(annee).liensParents().get(0))).child().liensParents().size());
		assertNotNull(diplome.habilitation());

	}

	@Test
	public void testSupprimerDiplome() {
		// Arrange
		int annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationConstruction);
		// diplome.setGradeUniversitaireRelationship(gradeUniversitaire);
		diplome.setTypeOrganisationRelationship(typeOrganisation);

		// Assert Ready to Test
		assertNotNull(diplome);

		// Act
		diplome.enregistrer(annee, 0);
		EOTestsHelper.setValidateur(diplome);

		// Assert Ready to Test
		assertEquals("periodes", 9, EOPeriode.fetchAllSco_Periodes(editingContext).count());
		assertEquals("version diplome", 1, EOVersionDiplome.fetchAllSco_VersionDiplomes(editingContext).count());
		assertEquals("liens parents", 1, diplome.liensParentsWithoutDuplicate().count());

		// Act
		try {
			diplome.supprimer();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Assert
		assertEquals("periodes", 0, EOPeriode.fetchAllSco_Periodes(editingContext).count());
		assertEquals("version diplome", 0, EOVersionDiplome.fetchAllSco_VersionDiplomes(editingContext).count());
		assertEquals("liens parents", 0, diplome.liensParentsWithoutDuplicate().count());
		

	}

	@Test
	public void test_copy() {
		// Arrange
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationConstruction);
		diplome.setSecteurDisciplinaireSise(secteurDisciplinaire);
		EODiplome newDiplome = diplome.copy(2014, null);
		newDiplome.setValidateur(new CodeValidateurSaisieObligatoire()); // normalement tiré par l'injection
		editingContext.saveChanges();

		// Assert Ready to Test
		assertNotNull(diplome);
		assertNotNull(newDiplome);

		// Act

		// Assert
		assertComposantsIdentiques(diplome, newDiplome);
		assertEquals(diplome.libelleCourt(), diplome.libelleCourt());
		assertEquals(diplome.disciplines().count(), diplome.disciplines().count());
		assertEquals(diplome.deliberationSemestrielle(), newDiplome.deliberationSemestrielle());
		assertEquals(diplome.gradeUniversitaire(), newDiplome.gradeUniversitaire());
		assertEquals(diplome.habilitation(), newDiplome.habilitation());
		assertEquals(diplome.mention(), newDiplome.mention());
		assertEquals(diplome.sise(), newDiplome.sise());
		assertEquals(diplome.siseEtablissement(), newDiplome.siseEtablissement());
		assertEquals(diplome.specialite(), newDiplome.specialite());
		assertEquals(diplome.statutFormation(), newDiplome.statutFormation());
		assertEquals(diplome.typeOrganisation(), newDiplome.typeOrganisation());
		assertEquals(diplome.typeFormation(), newDiplome.typeFormation());

		assertEquals(diplome.domaines().get(0), newDiplome.domaines().get(0));
		assertEquals(diplome.habilitationRequise(), newDiplome.habilitationRequise());
		
		assertEquals(diplome.secteurDisciplinaireSise(), newDiplome.secteurDisciplinaireSise());
		assertEquals(diplome.niveauAccesDiplome(), newDiplome.niveauAccesDiplome());
	}

	@Test
	public void test_filtrerDiplome() {
		// Arrange
		EODiplome dipl1 = creerEtInitialiserDiplome(statutFormationConstruction);
		dipl1.setSise("2345");
		dipl1.setSiseEtablissement("78945");

		EODiplome dipl2 = creerEtInitialiserDiplome(statutFormationConstruction);
		dipl2.setSise("289");
		dipl2.setSiseEtablissement("11124");
		editingContext.saveChanges();

		// Assert ready to test

		// Act

		// Assert
		assertEquals(2, EODiplome.fetchSco_Diplomes(editingContext, EODiplome.SISE_STR.contains("2"), null).count());
		assertEquals(1, EODiplome.fetchSco_Diplomes(editingContext, EODiplome.SISE_STR.contains("8"), null).count());
		assertEquals(2, EODiplome.fetchSco_Diplomes(editingContext, EODiplome.SISE_ETAB_STR.contains("4"), null).count());
		assertEquals(1, EODiplome.fetchSco_Diplomes(editingContext, EODiplome.SISE_ETAB_STR.contains("7"), null).count());
	}

	@Test
	public void test_versionner() {
		// Arrange
		Integer anneeDepart = 2014;
		Integer anneeCible = 2015;
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.setId(1);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(anneeDepart).setAnnee(anneeDepart);
		diplome.getVersionDiplome(anneeDepart).setId(2);

		EOPeriode p1 = EOTestsHelper.creerPeriode(editingContext, "p1", "p1", typePeriodeAnnee);
		p1.setId(3);

		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome.getVersionDiplome(anneeDepart), p1, false);
		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		ec1.setId(4);
		EOTestsHelper.creerLienComposer(editingContext, 2, p1, ec1, false);
		editingContext.saveChanges();
		// Assert ready to test
		assertEquals(1, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());
		assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count());
		assertEquals(5, EOLienComposer.fetchAllSco_LienComposers(editingContext).count());
		// Act

		diplome.versionner(anneeDepart, anneeCible);
		EOTestsHelper.setValidateur(diplome);
		editingContext.saveChanges();
		// Assert
		/*
		 * assertEquals(2, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());
		 * EOVersionDiplome vdCible = (EOVersionDiplome) diplome.getVersionDiplome(anneeCible); assertEquals(4, vdCible.childs().size()); EOPeriode p1cible =
		 * (EOPeriode) vdCible.childs().get(0); assertEquals(p1.libelle(), p1cible.libelle()); EOEC ec1Cible = (EOEC) p1cible.childs().get(0);
		 * assertEquals(ec1.libelle(), ec1Cible.libelle()); assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count()); assertEquals(4,
		 * EOLienComposer.fetchAllSco_LienComposers(editingContext).count());
		 */
	}

	@Ignore
	@Test
	public void test_versionnerdeuxFois() {
		// Arrange
		Integer anneeDepart = 2014;
		Integer anneeCible = 2015;
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.setId(1);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(anneeDepart).setAnnee(anneeDepart);
		diplome.getVersionDiplome(anneeDepart).setId(2);

		// Act
		diplome.versionner(anneeDepart, anneeCible);
		assertEquals(2, diplome.versionsDiplomes().count());
		diplome.versionner(anneeDepart, anneeCible);
		assertEquals(2, diplome.versionsDiplomes().count());
		diplome.versionner(anneeDepart, anneeCible+2);
		assertEquals(2, diplome.versionsDiplomes().count());
	}

	@Test
	public void test_versionnerComposantMutualise() {
		// Arrange
		Integer anneeDepart = 2014;
		Integer anneeCible = 2015;

		EODiplome diplome1 = creerEtInitialiserDiplome(statutFormationProduction);
		// diplome1.setGradeUniversitaire(null);
		diplome1.setTypeOrganisation(null);
		diplome1.setId(1);
		diplome1.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome1);
		diplome1.getVersionDiplome(anneeDepart).setId(2);
		
		EODiplome diplome2 = creerEtInitialiserDiplome(statutFormationProduction);
		// diplome2.setGradeUniversitaire(null);
		diplome2.setTypeOrganisation(null);
		diplome2.setId(3);
		diplome2.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome2);
		diplome2.getVersionDiplome(anneeDepart).setId(4);

		EOPeriode p1 = EOTestsHelper.creerPeriode(editingContext, "p1", "p1", typePeriodeAnnee);
		p1.setId(5);
		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome1.getVersionDiplome(anneeDepart), p1, false);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		ec1.setId(5);
		EOTestsHelper.creerLienComposer(editingContext, 2, p1, ec1, false);

		EOTestsHelper.creerLienComposer(editingContext, 3, (EOComposant) diplome2.getVersionDiplome(anneeDepart), p1, false);

		editingContext.saveChanges();

		// Assert ready to test
		/*
		 * for (EOComposant composant : EOComposant.fetchAllSco_Composants(editingContext)) { assertNotNull(composant.id()); }
		 */

		// Act
		diplome1.versionner(anneeDepart, anneeCible);
		EOTestsHelper.setValidateur(diplome1);
		editingContext.saveChanges();
		diplome2.versionner(anneeDepart, anneeCible);
		EOTestsHelper.setValidateur(diplome2);
		editingContext.saveChanges();

		// Assert
		assertEquals(14, EOPeriode.fetchAllSco_Periodes(editingContext).count());
		/*
		 * EOPeriode d1p1cible = (EOPeriode) diplome1.getVersionDiplome(anneeCible).childs().get(0); EOPeriode d2p1cible = (EOPeriode)
		 * diplome2.getVersionDiplome(anneeCible).childs().get(0); assertEquals(d1p1cible, d2p1cible);
		 */

	}

	@Test
	public void test_versionner_regroupement_mutualise() {
		// Arrange
		Integer anneeDepart = 2014;
		Integer anneeCible = 2015;
		Integer compId = 1;
		Integer lienId = 1;

		EODiplome diplome1 = creerEtInitialiserDiplome(statutFormationProduction);
		// diplome1.setGradeUniversitaire(null);
		diplome1.setTypeOrganisation(null);
		diplome1.setId(compId++);
		diplome1.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome1);
		diplome1.getVersionDiplome(anneeDepart).setId(compId++);
		EOPeriode p1 = EOTestsHelper.creerPeriode(editingContext, "p1", "p1", typePeriodeAnnee);
		p1.setId(compId++);

		EOTestsHelper.creerLienComposer(editingContext, lienId++, (EOComposant) diplome1.getVersionDiplome(anneeDepart), p1, false);

		EODiplome diplome2 = creerEtInitialiserDiplome(statutFormationProduction);
		// diplome2.setGradeUniversitaire(null);
		diplome2.setTypeOrganisation(null);
		diplome2.setId(compId++);
		diplome2.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome2);
		diplome2.getVersionDiplome(anneeDepart).setId(compId++);
		EOPeriode p2 = EOTestsHelper.creerPeriode(editingContext, "p2", "p2", typePeriodeAnnee);
		p2.setId(compId++);

		EOTestsHelper.creerLienComposer(editingContext, lienId++, (EOComposant) diplome2.getVersionDiplome(anneeDepart), p2, false);

		// ajout d'un regroupement mutualise
		EORegroupement r1 = EOTestsHelper.creerRegroupement(editingContext, "R1", "R1");
		r1.setId(compId++);
		EOTestsHelper.creerLienComposer(editingContext, lienId++, p1, r1, false);

		// Assert ready to test
		editingContext.saveChanges();
		// Assert ready to test
		/*
		 * for (EOComposant composant : EOComposant.fetchAllSco_Composants(editingContext)) { assertNotNull(composant.id()); }
		 */
		// Act
		diplome1.versionner(anneeDepart, anneeCible);
		diplome2.versionner(anneeDepart, anneeCible);
		EOTestsHelper.setValidateur(diplome1);
		EOTestsHelper.setValidateur(diplome2);
		// Assert

		assertEquals(2, EORegroupement.fetchAllSco_Regroupements(editingContext).count());
	}

	@Test
	public void test_habiliter() {
		// Arrange
		Integer anneeReference = 2014;
		Integer anneeCible = 2016;

		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.setId(1);
		// diplome.setGradeUniversitaire(null);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(anneeReference, 1);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(anneeReference).setAnnee(anneeReference);
		diplome.getVersionDiplome(anneeReference).setId(2);
		
		EOPeriode p1 = EOTestsHelper.creerPeriode(editingContext, "p1", "p1", typePeriodeAnnee);
		p1.setId(3);
		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome.getVersionDiplome(anneeReference), p1, false);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		ec1.setId(4);
		EOTestsHelper.creerLienComposer(editingContext, 2, p1, ec1, false);

		editingContext.saveChanges();

		// Assert ready to test
		assertEquals(1, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());
		assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count());
		assertEquals(5, EOLienComposer.fetchAllSco_LienComposers(editingContext).count());

		// Act
		EODiplome nouveauDiplome = (EODiplome) diplome.habiliter(anneeReference, anneeCible);
		EOTestsHelper.setValidateur(nouveauDiplome);
		// editingContext.saveChanges();

		// Assert
		/*
		 * assertNotNull(nouveauDiplome); assertNotSame(nouveauDiplome, diplome); assertEquals(1, nouveauDiplome.childs(EOTypeLien.typeVersionner(editingContext),
		 * EOTypeComposant.typeVersionDiplome(editingContext), null).size()); EOVersionDiplome vdCible = (EOVersionDiplome)
		 * nouveauDiplome.getVersionDiplome(anneeCible); assertEquals(1, vdCible.childs().size()); EOPeriode p1Cible = (EOPeriode) vdCible.childs().get(0);
		 * assertEquals(p1.libelle(), p1Cible.libelle()); assertNotSame(p1Cible, p1); EOEC ec1Cible = (EOEC) p1Cible.childs().get(0); assertEquals(ec1.libelle(),
		 * ec1Cible.libelle()); assertNotSame(ec1Cible, ec1);
		 */

		// Act (on essaye d'habiliter le diplome encore une fois pour la meme annee)
		EODiplome nouveauDiplome2 = (EODiplome) diplome.habiliter(anneeReference, anneeCible);

		// Assert
		assertNull(nouveauDiplome2);

	}

	@Test
	public void test_habiliterPassageEnProduction() {
		// Arrange
		Integer annee = 2014;

		EODiplome diplome = creerEtInitialiserDiplome(statutFormationConstruction);
		diplome.setId(1);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(annee, 0);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(annee).setAnnee(annee);
		diplome.getVersionDiplome(annee).setId(2);

		EOPeriode p1 = EOTestsHelper.creerPeriode(editingContext, "p1", "p1", typePeriodeAnnee);
		p1.setId(3);

		EOTestsHelper.creerLienComposer(editingContext, 1, (EOComposant) diplome.getVersionDiplome(annee), p1, false);
		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "ec1", "ec1");
		ec1.setId(4);
		EOTestsHelper.creerLienComposer(editingContext, 2, p1, ec1, false);
		editingContext.saveChanges();
		// Assert ready to test
		assertEquals(1, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());
		assertEquals(6, EOLien.fetchAllSco_Liens(editingContext).count());
		assertEquals(5, EOLienComposer.fetchAllSco_LienComposers(editingContext).count());
		int versionMajeureEnModelisation = ((EOVersionDiplome) diplome.getVersionDiplome(annee)).composantInfoVersion().versionMajeure();
		assertEquals(0, versionMajeureEnModelisation);
		// Act

		EOVersionDiplome nouvelleVersionEnHabilitation = diplome.habiliterPassageEnProduction((EOVersionDiplome)diplome.getVersionDiplome(annee));
		EOTestsHelper.setValidateur(diplome);
		editingContext.saveChanges();
		// Assert
		assertNotNull(nouvelleVersionEnHabilitation);
		int versionMajeureEnMaquettage1 = ((EOVersionDiplome) diplome.getVersionDiplome(annee)).composantInfoVersion().versionMajeure();
		assertEquals(1, versionMajeureEnMaquettage1);
		int versionMajeureEnMaquettage2 = ((EOVersionDiplome) nouvelleVersionEnHabilitation).composantInfoVersion().versionMajeure();
		assertEquals(1, versionMajeureEnMaquettage2);
		assertEquals(EOStatutFormation.statutEnProduction(editingContext), diplome.statutFormation());

	}
	
	@Test
	public void test_isEnConstruction() {
		// Arrange
		Integer anneeDepart = 2014;
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.setId(1);
		// diplome.setGradeUniversitaire(null);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(anneeDepart, 0);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(anneeDepart).setId(2);
		editingContext.saveChanges();

		// Assert ready to test
		assertNotNull(diplome);
		assertNotNull(diplome.getVersionDiplome(anneeDepart));
		assertEquals(1, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());

		// Act
		Boolean isEnConstruction = diplome.isVisibleEnConstruction(anneeDepart);

		// Assert
		assertFalse(isEnConstruction);
		// Arrange
		diplome.setHabilitationRequise(1);
		// Act
		isEnConstruction = diplome.isVisibleEnConstruction(anneeDepart);
		// Assert
		assertFalse(isEnConstruction);

		// Arrange
		diplome.setStatutFormationRelationship(statutFormationConstruction);
		// Act
		isEnConstruction = diplome.isVisibleEnConstruction(anneeDepart);
		// Assert
		assertTrue(isEnConstruction);

	}

	@Test
	public void test_isEnProduction() {
		// Arrange
		Integer anneeDepart = 2014;
		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.setId(1);
		// diplome.setGradeUniversitaire(null);
		diplome.setTypeOrganisation(null);
		diplome.enregistrer(anneeDepart, 1);
		EOTestsHelper.setValidateur(diplome);
		diplome.getVersionDiplome(anneeDepart).setId(2);
		editingContext.saveChanges();

		// Assert ready to test
		assertNotNull(diplome);
		assertNotNull(diplome.getVersionDiplome(anneeDepart));
		assertEquals(1, diplome.childs(EOTypeLien.typeVersionner(editingContext), EOTypeComposant.typeVersionDiplome(editingContext), null).size());

		// Act
		Boolean isEnProduction = diplome.isVisibleEnProduction(anneeDepart);

		// Assert
		assertTrue(isEnProduction);
		// Arrange
		diplome.setHabilitationRequise(1);
		// Act
		isEnProduction = diplome.isVisibleEnProduction(anneeDepart);
		// Assert
		assertTrue(isEnProduction);

		// Arrange
		diplome.setStatutFormationRelationship(statutFormationConstruction);
		// Act
		isEnProduction = diplome.isVisibleEnProduction(anneeDepart);
		// Assert
		assertFalse(isEnProduction);

	}

	@Test
	public void test_anneeDansPeriodeHabilitation() {
		// Arrange
		Integer anneeDepart = 2014;
		Integer anneeDebutHabilitation = 2014;
		Integer anneeFinHabilitation = 2019;

		EODiplome diplome = creerEtInitialiserDiplome(statutFormationProduction);
		diplome.enregistrer(anneeDepart, 1);
		// Assert ready to test

		// Act
		// Assert
		assertFalse(diplome.anneeDansPeriodeHabilitation(2013, anneeDebutHabilitation, anneeFinHabilitation));
		for (int annee = 2014; annee <= 2021; annee++) {
			assertTrue("annee " + annee, diplome.anneeDansPeriodeHabilitation(annee, anneeDebutHabilitation, anneeFinHabilitation));
		}
		assertFalse(diplome.anneeDansPeriodeHabilitation(2022, anneeDebutHabilitation, anneeFinHabilitation));
	}

	/**
	 * @return: un diplome initialise
	 */
	private EODiplome creerEtInitialiserDiplome(EOStatutFormation statutFormation) {
		Integer annee = Calendar.getInstance().get(Calendar.YEAR);
		EODiplome diplome = EOTestsHelper.creerDiplome(editingContext, "code", "diplome", statutFormation, gradeUniversitaire, typeFormation, annee);
		diplome.setValidateur(new CodeValidateurSaisieObligatoire());
		editingContext.insertObject(diplome);
		diplome.setTypeComposantRelationship(EOTypeComposant.typeDiplome(editingContext));

		diplome.addToDomainesRelationship(domaine);
		diplome.setCode(expectedCode);
		diplome.setSise(expectedSISE);
		diplome.setSiseEtablissement(expectedSISEEtablissement);
		diplome.setLibelle(expectedLibelle);
		diplome.setLibelleCourt(expectedLibelleCourt);
		diplome.setMentionRelationship(mention);
		diplome.setSpecialiteRelationship(specialite);
		//diplome.setTypeFormationRelationship(typeFormation);
		diplome.setTypeOrganisationRelationship(typeOrganisation);

		diplome.setDeliberationSemestrielle(1);
		//diplome.setGradeUniversitaire(gradeUniversitaire);
		// TODO eclaircir cette notion d'habilitation!!
		diplome.setHabilitation(null);
		diplome.setHabilitationRequise(1);
		
		diplome.setSecteurDisciplinaireSise(secteurDisciplinaire);
		diplome.setNiveauAccesDiplomeRelationship(niveauAccesDiplome);
		return diplome;
	}

}
