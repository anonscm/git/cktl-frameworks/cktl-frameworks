package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

public class HeuresMinutesUtilsTest {

	@Test
	public void testConversionEnCentieme() {
		
		BigDecimal i = BigDecimal.ZERO.setScale(1);
		BigDecimal increment = new BigDecimal("0.6"); // Il n'y aura que des multiples de 0.6 en base (0.01 centième d'heure * 60 => 0.6)
		BigDecimal max = new BigDecimal("60");
		
		while (i.compareTo(max) <= 0) {
			
			BigDecimal centieme = HeuresMinutesUtils.minuteBddEnCentieme(i);
			BigDecimal minute = HeuresMinutesUtils.centiemeEnMinuteBdd(centieme);
			
			Assert.assertTrue(centieme.scale() == 2);
			Assert.assertTrue(minute.scale() == 1);
			Assert.assertEquals("Pour i = " + i, i, minute);
			
			i = i.add(increment);
		}
	}

	@Test
	public void testConversionEnCentiemeInverse() {
		
		BigDecimal i = BigDecimal.ZERO;
		BigDecimal increment = new BigDecimal("0.01");
		
		while (i.compareTo(BigDecimal.ONE) <= 0) {
			
			BigDecimal minute = HeuresMinutesUtils.centiemeEnMinuteBdd(i);
			BigDecimal centieme = HeuresMinutesUtils.minuteBddEnCentieme(minute);
			
			Assert.assertTrue("Pour i = " + i, i.compareTo(centieme) == 0);
			
			i = i.add(increment);
		}
	}

	@Test
	public void testConversionEnMinute() {
		
		BigDecimal i = BigDecimal.ZERO.setScale(1);
		BigDecimal increment = new BigDecimal("0.6"); // Il n'y aura que des multiples de 0.6 en base (0.01 centième d'heure * 60 => 0.6)
		BigDecimal max = new BigDecimal("60");
		
		while (i.compareTo(max) <= 0) {
			
			Integer minute = HeuresMinutesUtils.minuteBddEnMinute(i);
			BigDecimal minuteBdd = HeuresMinutesUtils.minuteEnMinuteBdd(minute);
			
			Assert.assertTrue(minuteBdd.scale() == 1);
			Assert.assertEquals("Pour i = " + i, i.doubleValue(), minuteBdd.doubleValue(), 0.41d);
			
			i = i.add(increment);
		}
	}

	@Test
	public void testConversionEnMinuteInverse() {
		
		int i = 0;
		while (i <= 60) {
			
			BigDecimal minuteBdd = HeuresMinutesUtils.minuteEnMinuteBdd(i);
			Integer minute = HeuresMinutesUtils.minuteBddEnMinute(minuteBdd);
			
			Assert.assertEquals("Pour i = " + i, i, minute.intValue());
			
			i++;
		}
	}

	@Test
	public void testConversionEnHeureMinute() {
		
		BigDecimal i = BigDecimal.ZERO.setScale(1);
		BigDecimal increment = new BigDecimal("0.6"); // Il n'y aura que des multiples de 0.6 en base (0.01 centième d'heure * 60 => 0.6)
		BigDecimal max = new BigDecimal("60");
		
		while (i.compareTo(max) <= 0) {
			
			BigDecimal heureMinute = HeuresMinutesUtils.minuteBddEnHeureMinute(i);
			BigDecimal minute = HeuresMinutesUtils.heureMinuteEnMinuteBdd(heureMinute);
			
			Assert.assertTrue(heureMinute.scale() == 2);
			Assert.assertEquals("Pour i = " + i, i.doubleValue(), minute.doubleValue(), 0.41d);
			
			i = i.add(increment);
		}
	}

	@Test
	public void testConversionEnHeureMinuteInverse() {
		
		BigDecimal i = BigDecimal.ZERO;
		BigDecimal increment = new BigDecimal("0.01");
		BigDecimal basculeHeure = new BigDecimal("0.60");
		
		while (i.compareTo(BigDecimal.ONE) <= 0) {
			
			BigDecimal minute = HeuresMinutesUtils.heureMinuteEnMinuteBdd(i);
			BigDecimal heureMinute = HeuresMinutesUtils.minuteBddEnHeureMinute(minute);
			
			if (i.compareTo(basculeHeure) < 0 || i.compareTo(BigDecimal.ONE) == 0) {
				Assert.assertTrue("Pour i = " + i, i.compareTo(heureMinute) == 0);
			} else {
				Assert.assertTrue("Pour i = " + i, i.compareTo(heureMinute.subtract(new BigDecimal("0.4"))) == 0);
			}
			
			i = i.add(increment);
		}
	}
}
