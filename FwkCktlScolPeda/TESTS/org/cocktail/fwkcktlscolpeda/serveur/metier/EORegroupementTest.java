package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSArray;

public class EORegroupementTest extends EOScolTestCase {
	private static final String expectedCode = "codeRegroupement";
	private static final String expectedLibelle = "libelleRegroupement";

	@Before
	public void setUp() {
		super.setUp();
		EOTestsHelper.creerTypeComposant(editingContext, 2, ITypeComposant.TYPEREGROUPEMENT_NOM, ITypeComposant.TYPEREGROUPEMENT_NOM);
	}

	@Test
	public void testEnregistrer() {
		try {
	        // Arrange
	        EORegroupement regroupement = EOComposantFactory.createComposant(EORegroupement.class, null, editingContext,2014, null);
	        regroupement.setCode(expectedCode);
	        regroupement.setLibelle(expectedLibelle);
	        regroupement.setValidateur(new CodeValidateurSaisieObligatoire());
	        // Assert Ready to Test

	        // Act
	        editingContext.saveChanges();

	        // Assert
	        NSArray<EORegroupement> regroupements = EORegroupement.fetchAllSco_Regroupements(editingContext);
	        assertEquals(1, regroupements.count());
	        assertEquals(expectedCode, regroupements.get(0).code());
	        assertEquals(expectedLibelle, regroupements.get(0).libelle());
        } catch (Exception e) {	   
	        e.printStackTrace();
	        fail("impossible d'enregistrer un regroupement");
        } 
	}
	
	@Test
    public void test_copy() {
	    // Arrange
		EORegroupement regroupement = EOTestsHelper.creerRegroupement(editingContext, "code", "libelle");
		regroupement.setChoixECTS(1);
		regroupement.setChoixMax(3);
		regroupement.setChoixMin(1);
	    // Assert ready to test

	    // Act
		EORegroupement newRegroupement = regroupement.copy(2014, null);

	    // Assert
		
		assertComposantsIdentiques(regroupement, newRegroupement);
		assertEquals(regroupement.choixECTS(), newRegroupement.choixECTS());
		assertEquals(regroupement.choixMax(), newRegroupement.choixMax());
		assertEquals(regroupement.choixMin(), newRegroupement.choixMin());
		
    }
}
