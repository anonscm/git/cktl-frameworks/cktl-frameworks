package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests de la classe période
 */
public class EOPeriodeTest extends EOScolTestCase {
	private EOTypePeriode typePeriode;
	
	@Override
	@Before
	public void setUp() {
	    super.setUp();
	    EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEPERIODE_NOM, ITypeComposant.TYPEPERIODE_NOM);
	    typePeriode = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);
	}
	
	@Test
    public void test_copy() {
	    // Arrange
		EOPeriode periode = EOTestsHelper.creerPeriode(editingContext, "code", "libelle", typePeriode);
	    // Assert ready to test

	    // Act
		EOPeriode newPeriode = periode.copy(2014, null);

	    // Assert
		assertComposantsIdentiques(periode, newPeriode);
    }
	
}
