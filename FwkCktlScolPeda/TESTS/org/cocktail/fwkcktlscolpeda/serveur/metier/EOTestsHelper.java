package org.cocktail.fwkcktlscolpeda.serveur.metier;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CodeValidateurSaisieObligatoire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.FwkCktlScolPedaTestUtils.DummySequence;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Cette classe propose des méthodes utils pour les tests unitaires.
 */
public final class EOTestsHelper {

	private EOTestsHelper() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public static EOTypeLien creerTypeLien(MockEditingContext editingContext, Integer id, String nom) {
		EOTypeLien typeLien = new EOTypeLien();
		typeLien.setId(id);
		typeLien.setNom(nom);
		editingContext.insertSavedObject(typeLien);
		return typeLien;
	}

	/**
	 * @param id
	 * @param nom
	 * @param libelle
	 * @return type de composant
	 */
	public static EOTypeComposant creerTypeComposant(MockEditingContext editingContext, Integer id, String nom, String libelle) {
		EOTypeComposant typeComposant = new EOTypeComposant();
		typeComposant.setId(id);
		typeComposant.setNom(nom);
		typeComposant.setLibelle(libelle);
		editingContext.insertSavedObject(typeComposant);
		return typeComposant;
	}

	/**
	 * @param editingContext : contexte d'édition
	 * @param typeComposantParent: type du composant parent
	 * @param typeComposantEnfant : type du composant enfant
	 * @param typeLien : type de lien
	 * @return une règle de liaison par defaut allowed et visible.
	 */
	public static EORegleLiaison creerRegleLiaison(MockEditingContext editingContext, EOTypeComposant typeComposantParent, EOTypeComposant typeComposantEnfant,
	    EOTypeLien typeLien) {
		EORegleLiaison regleLiaison = new EORegleLiaison();
		editingContext.insertSavedObject(regleLiaison);
		regleLiaison.setAllowed(1);
		regleLiaison.setForbidden(0);
		regleLiaison.setVisible(1);
		regleLiaison.setAllowedParentRelationship(typeComposantParent);
		regleLiaison.setAllowedChildRelationship(typeComposantEnfant);
		regleLiaison.setTypeLienRelationship(typeLien);
		return regleLiaison;
	}

	public static EOLangue creerLangue(MockEditingContext editingContext) {
		EOLangue langue = new EOLangue();
		langue.setLlLangue("langue");
		editingContext.insertSavedObject(langue);
		return langue;
	}

	public static EOCreditable creerCreditable(MockEditingContext editingContext) {
		EOCreditable creditable = new EOCreditable();
		editingContext.insertSavedObject(creditable);
		return creditable;
	}

	public static EODomaine creerDomaine(MockEditingContext editingContext) {
		EODomaine domaine = new EODomaine();
		editingContext.insertSavedObject(domaine);
		return domaine;
	}

	public static EOImplantationGeo creerImplantationGeo(MockEditingContext editingContext) {
		EOImplantationGeo implantationGeo = new EOImplantationGeo();
		editingContext.insertSavedObject(implantationGeo);
		return implantationGeo;
	}

	public static EOSessionComposant creerSession(MockEditingContext editingContext, String libelle) {
		EOSessionComposant session = new EOSessionComposant();
		session.setLibelle(libelle);
		editingContext.insertSavedObject(session);
		return session;
	}

	public static EOTypeDescriptifDestination creerTypeDestinationDescriptif(MockEditingContext editingContext, Integer id, String type, String libelle) {
		EOTypeDescriptifDestination typeDescriptifDestination = new EOTypeDescriptifDestination();
		typeDescriptifDestination.setId(id);
		typeDescriptifDestination.setType(type);
		typeDescriptifDestination.setLibelle(libelle);
		typeDescriptifDestination.setReadOnly(1);
		editingContext.insertSavedObject(typeDescriptifDestination);
		return typeDescriptifDestination;
	}

	public static EODescriptif creerDescriptif(MockEditingContext editingContext, Integer id, EOComposant composant, EOTypeDescriptifDestination type,
	    String libelle) {
		EODescriptif descriptif = new EODescriptif();
		editingContext.insertSavedObject(descriptif);
		descriptif.setId(id);
		descriptif.setComposantRelationship(composant);
		descriptif.setComposantId(composant.id());
		descriptif.setDestinationRelationship(type);
		descriptif.setDestinationId(type.id());
		descriptif.setLibelle(libelle);
		return descriptif;
	}

	public static EODescriptifChampContenu creerDescriptifChampContenu(MockEditingContext editingContext, String libelle) {
		EODescriptifChampContenu champContenu = new EODescriptifChampContenu();
		champContenu.setLibelle(libelle);
		editingContext.insertSavedObject(champContenu);
		return champContenu;
	}

	public static EOComposant creerComposantConcret(MockEditingContext editingContext, String code, String libelle, Integer id, EOTypeComposant typeComposant) {
		EOComposantConcret composant;
		try {
			composant = EOComposantConcret.createComposantConcret(editingContext);
			remplitComposant(code, libelle, composant);
			composant.setId(id);
			composant.setTypeComposant(typeComposant);
			editingContext.saveChanges();
			return composant;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static <T extends EOEnterpriseObject> ERXEnterpriseObjectCache<T> createCacheMock(Class<T> clazz, String key, final MockEditingContext mockEc) {
		ERXEnterpriseObjectCache<T> cache = new ERXEnterpriseObjectCache<T>(clazz, key) {
			@Override
			protected ERXEC editingContext() {
				return mockEc;
			}
		};
		cache.setReuseEditingContext(true);
		return cache;
	}

	public static void initialiserAllCacheMock(final MockEditingContext mockEc) {
		EOTypeComposant.setTypeComposantCache(createCacheMock(EOTypeComposant.class, EOTypeComposant.NOM_KEY, mockEc));
		EOTypeLien.setTypeLienCache(createCacheMock(EOTypeLien.class, EOTypeLien.NOM_KEY, mockEc));
		EOTypeAP.setTypeAPCache(createCacheMock(EOTypeAP.class, EOTypeAP.CODE_KEY, mockEc));
		EOTypeAE.setTypeAECache(createCacheMock(EOTypeAE.class, EOTypeAE.CODE_KEY, mockEc));
		EOChamp.setChampCache(createCacheMock(EOChamp.class, EOChamp.CODE_KEY, mockEc));
		EOStatutFormation.setStatutFormationCache(createCacheMock(EOStatutFormation.class, EOStatutFormation.CODE_KEY, mockEc));
		EOGradeUniversitaire.setTypeGradeUniversitaireCache(createCacheMock(EOGradeUniversitaire.class, EOGradeUniversitaire.TYPE_KEY, mockEc));
		EOTypeNote.setTypeNoteCache(createCacheMock(EOTypeNote.class, EOTypeNote.CODE_KEY, mockEc));
		EOTypePeriode.setTypePeriodeCache(createCacheMock(EOTypePeriode.class, EOTypePeriode.CODE_KEY, mockEc));
		EOTypeInscriptionPeda.setTypeIpCache(createCacheMock(EOTypeInscriptionPeda.class, EOTypeInscriptionPeda.CODE_KEY, mockEc));
		EOTypeEtudiant.setTypeEtudiantCache(createCacheMock(EOTypeEtudiant.class, EOTypeEtudiant.CODE_KEY, mockEc));
		EOTypeFormule.setTypeFormuleCache(createCacheMock(EOTypeFormule.class, EOTypeFormule.TYPE_KEY, mockEc));
	}

	public static EOLien creerLien(MockEditingContext editingContext, Integer id, EOComposant parent, EOComposant enfant, EOTypeLien typeLien) {
		EOLien lien = EOComposantFactory.createLien(parent, enfant, typeLien);
		lien.setId(id);
		editingContext.saveChanges();
		return lien;
	}

	public static EOLienConsultation creerLienConsultation(MockEditingContext editingContext, String idContext, 
			EODiplome diplome, EOVersionDiplome versionDiplome, EOLien lien, EOLien lienRoot,
			EOComposant parent, EOComposant enfant) {
		EOLienConsultation lienConsultation = (EOLienConsultation) EOUtilities.createAndInsertInstance(editingContext, EOLienConsultation.ENTITY_NAME);
		lienConsultation.setIdLienContexte(idContext);
		lienConsultation.setIdDiplome(diplome.id());
		lienConsultation.setIdLien(lien.id());
		lienConsultation.setIdLienRoot(lienRoot.id());
		lienConsultation.setIdVersionDiplome(versionDiplome.id());
		lienConsultation.setParentId(parent.id());
		lienConsultation.setChildId(enfant.id());
		lienConsultation.setLlComposantParent(parent.libelle());
		lienConsultation.setLlComposantChild(enfant.libelle());
		lienConsultation.setTypeComposantNomParent(parent.typeComposant().libelle());
		lienConsultation.setTypeComposantNomChild(enfant.typeComposant().libelle());
		lienConsultation.setNiveau(1);
		lienConsultation.setObligatoire(true);
		lienConsultation.setOrdre(lien.ordre());
		editingContext.saveChanges();
		return lienConsultation;
	}

	public static EOLienComposer creerLienComposer(MockEditingContext editingContext, Integer id, EOComposant parent, EOComposant enfant, Boolean obligatoire) {
		EOLienComposer lienComposer = EOComposantFactory.createLienComposer(parent, enfant);
		lienComposer.setId(id);
		lienComposer.setObligatoire(obligatoire);
		editingContext.saveChanges();
		return lienComposer;
	}

	public static EOTypeAP creerTypeAP(MockEditingContext editingContext, Integer id, String code, String libelle, BigDecimal heureEquivalent) {
		EOTypeAP typeAP = new EOTypeAP();
		typeAP.setCode(code);
		typeAP.setLibelle(libelle);
		typeAP.setId(id);
		typeAP.setHeureEquivalent(heureEquivalent);
		editingContext.insertSavedObject(typeAP);

		return typeAP;
	}

	public static EOTypeAE creerTypeAE(MockEditingContext editingContext, Integer id, String code, String libelle) {
		EOTypeAE typeAE = new EOTypeAE();
		typeAE.setCode(code);
		typeAE.setLibelle(libelle);
		typeAE.setId(id);
		editingContext.insertSavedObject(typeAE);
		return typeAE;
	}

	private static void remplitComposant(String code, String libelle, EOComposant composant) {
		composant.setCode(code);
		composant.setLibelle(libelle);
		composant.setValidateur(new CodeValidateurSaisieObligatoire());
	}

	public static EOAP creerAP(MockEditingContext editingContext, String code, EOTypeAP typeAP) {
		try {
			EOAP ap = EOComposantFactory.createComposant(EOAP.class, null, editingContext, 2014, null);
			remplitComposant(code, code, ap);
			ap.setModeRelationship(EOModeAP.modePresentiel(editingContext));
			ap.setModeAPId(1);
			ap.setTypeAPRelationship(typeAP);
			editingContext.saveChanges();
			return ap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOAP creerAP(MockEditingContext editingContext, String code, Integer minutes, Integer nbGroupe, EOTypeAP typeAP) {
		try {
			EOAP ap = creerAP(editingContext, code, typeAP);
			ap.setTypeAPRelationship(typeAP);

			if (minutes != null || nbGroupe != null) {
				ITypeChargeEnseignement typeChargeEnseignement = EOTypeChargeEnseignement.createSco_TypeChargeEnseignement(editingContext, "THEORIQUE", "Théorique");
				IChargeEnseignement chargeEnseignement = EOComposantFactory.createChargeEnseignementSurAP(ap, typeChargeEnseignement, 1);
				((EOChargeEnseignement) chargeEnseignement).setUserInfo(EOScolTestCase.getUserInfo());
				chargeEnseignement.setValeurMinutes(minutes);
				chargeEnseignement.setNbGroupes(nbGroupe);
			}

			editingContext.saveChanges();
			return ap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOAE creerAE(MockEditingContext editingContext, String code, EOTypeAE typeAE) {
		try {
			EOAE ae = EOComposantFactory.createComposant(EOAE.class, null, editingContext, 2014, null);
			remplitComposant(code, code, ae);
			ae.setTypeAERelationship(typeAE);
			editingContext.saveChanges();
			return ae;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOAE creerAE(MockEditingContext editingContext, String code, String heureMin, String heureMax, EOTypeAE typeAE) {
		try {
			EOAE ae = creerAE(editingContext, code, typeAE);
			ae.setTypeAERelationship(typeAE);
			Integer valeurHeure = new Integer(heureMin);
			ae.setDureeMin(valeurHeure);
			valeurHeure = new Integer(heureMax);
			ae.setDureeMax(valeurHeure);
			editingContext.saveChanges();
			return ae;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOEC creerEC(MockEditingContext editingContext, Integer id, String code, String libelle) {
		EOEC ec = creerEC(editingContext, code, libelle);
		ec.setId(id);
		return ec;
	}

	public static EOEC creerEC(MockEditingContext editingContext, String code, String libelle) {
		EOEC ec;
		try {
			ec = EOComposantFactory.createComposant(EOEC.class, null, editingContext, 2014, null);
			remplitComposant(code, libelle, ec);
			ec.setLibre(0);
			editingContext.saveChanges();
			return ec;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static EOUE creerUE(MockEditingContext editingContext, String code, String libelle) {
		EOUE ue;
		try {
			ue = EOComposantFactory.createComposant(EOUE.class, null, editingContext, 2014, null);
			remplitComposant(code, libelle, ue);
			editingContext.saveChanges();
			return ue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static EORegroupement creerRegroupement(MockEditingContext editingContext, String code, String libelle) {
		EORegroupement regroupement;
		try {
			regroupement = EOComposantFactory.createComposant(EORegroupement.class, null, editingContext, 2014, null);
			remplitComposant(code, libelle, regroupement);
			editingContext.saveChanges();
			return regroupement;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static EOPeriode creerPeriode(MockEditingContext editingContext, Integer id, String code, String libelle, EOTypePeriode typePeriode, Integer niveau) {
		EOPeriode periode = creerPeriode(editingContext, code, libelle, typePeriode, niveau);
		periode.setId(id);
		return periode;
	}

	public static EOPeriode creerPeriode(MockEditingContext editingContext, String code, String libelle, EOTypePeriode typePeriode) {
		return creerPeriode(editingContext, code, libelle, typePeriode, 0);
	}

	public static EOPeriode creerPeriode(MockEditingContext editingContext, String code, String libelle, EOTypePeriode typePeriode, Integer niveau) {
		try {
			EOPeriode periode = EOComposantFactory.createComposant(EOPeriode.class, null, editingContext, 2014, null);
			remplitComposant(code, libelle, periode);
			periode.setOrdre(niveau); // par defaut
			periode.setToTypePeriodeRelationship(typePeriode);
			editingContext.saveChanges();
			return periode;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOParcours creerParcours(MockEditingContext editingContext, Integer id, String code, String libelle) {
		EOParcours parcours = creerParcours(editingContext, code, libelle);
		parcours.setId(id);
		return parcours;
	}

	public static EOParcours creerParcours(MockEditingContext editingContext, String code, String libelle) {
		try {
			EOParcours parcours = EOComposantFactory.createComposant(EOParcours.class, null, editingContext, 2014, null);
			remplitComposant(code, libelle, parcours);
			editingContext.saveChanges();
			return parcours;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EODiplome creerDiplome(MockEditingContext editingContext, Integer id, String code, String libelle, EOStatutFormation statutFormation,
	    EOGradeUniversitaire gradeUniversitaire, EOTypeFormation typeFormation, Integer annee) {
		EODiplome diplome = creerDiplome(editingContext, code, libelle, statutFormation, gradeUniversitaire, typeFormation, annee);
		diplome.setId(id);
		return diplome;
	}

	public static EODiplome creerDiplome(MockEditingContext editingContext, String code, String libelle, EOStatutFormation statutFormation,
	    EOGradeUniversitaire gradeUniversitaire, EOTypeFormation typeFormation, Integer annee) {
		EODiplome diplome;
		try {
			// TODO comprendre pk le validateur ne marche pas avec le spy
			// diplome = spy(EOComposantFactory.createComposant(EODiplome.class, null, editingContext));
			diplome = EOComposantFactory.createComposant(EODiplome.class, null, editingContext, annee, null);
			remplitComposant(code, libelle, diplome);
			diplome.setPersIdCreation(1);
			diplome.setDateCreation(new NSTimestamp());
			// doReturn(1).when(diplome).persIdCreation();
			// doReturn(new NSTimestamp()).when(diplome).dateCreation();
			// doReturn(new CodeValidateurSaisieObligatoire()).when(diplome).getValidateur();
			diplome.setStatutFormationRelationship(statutFormation);
			diplome.setGradeUniversitaireRelationship(gradeUniversitaire);
			diplome.setTypeFormationRelationship(typeFormation);
			editingContext.saveChanges();

			return diplome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOVersionDiplome creerVersionDiplome(MockEditingContext editingContext, Integer id, String code, String libelle, Integer annee) {
		EOVersionDiplome versionDiplome = creerVersionDiplome(editingContext, code, libelle, annee);
		versionDiplome.setId(id);
		return versionDiplome;
	}

	public static EOVersionDiplome creerVersionDiplome(MockEditingContext editingContext, String code, String libelle, Integer annee) {
		try {
			EOVersionDiplome versionDiplome = EOComposantFactory.createComposant(EOVersionDiplome.class, null, editingContext, annee, null);
			remplitComposant(code, libelle, versionDiplome);
			versionDiplome.setAnnee(annee);
			editingContext.saveChanges();
			return versionDiplome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOGdStrategie creerStrategie(MockEditingContext editingContext, String code) {
		EOGdStrategie strategie = new EOGdStrategie();
		strategie.setCode(code);
		strategie.setLibelle(code);
		editingContext.insertSavedObject(strategie);
		return strategie;
	}

	public static EOGdPerimetreGirofle creerPerimetre(MockEditingContext editingContext, EOGdStrategie strategie) {
		EOGdPerimetreGirofle perimetre = creerPerimetre(editingContext);

		perimetre.setStrategie(strategie);

		return perimetre;
	}

	private static EOGdPerimetreGirofle creerPerimetre(MockEditingContext editingContext) {
		EOGdPerimetreGirofle perimetre = spy(createPerimetre(1, "libelle"));
		doReturn(1).when(perimetre).persIdCreation();
		doReturn(new NSTimestamp()).when(perimetre).dateCreation();
		// doReturn(1).when(perimetre).primaryKey();

		editingContext.insertSavedObject(perimetre);
		return perimetre;
	}

	private static EOGdPerimetreGirofle createPerimetre(Integer appId, String libelle) {
		// On ne peut pas utiliser le create(editingContext) car l'injection du user n'est pas mise en place dans le test U
		// A voir si une factory ne pourrait pas être une soltion plus propre
		EOGdPerimetreGirofle perimetre = new EOGdPerimetreGirofle(); // EOGdPerimetreGirofle.create(editingContext);
		perimetre.setAppId(appId);
		perimetre.setLibelle(libelle);

		return perimetre;
	}

	public static EOGradeUniversitaire creerGradeUniversitaire(MockEditingContext editingContext, String type, String libelle, Integer unitesTemps) {
		EOGradeUniversitaire gradeUniversitaire = new EOGradeUniversitaire();
		gradeUniversitaire.setType(type);
		gradeUniversitaire.setId(1);
		gradeUniversitaire.setLibelle(libelle);
		gradeUniversitaire.setNbUnitesTemps(unitesTemps);
		editingContext.insertSavedObject(gradeUniversitaire);
		return gradeUniversitaire;
	}

	public static EOTypeFormation creerTypeFormation(MockEditingContext editingContext, String code, String libelle) {
		EOTypeFormation typeFormation = new EOTypeFormation();
		typeFormation.setId(1);
		typeFormation.setCode(code);
		typeFormation.setLibelle(libelle);
		typeFormation.setHabilitable(1);
		editingContext.insertSavedObject(typeFormation);
		return typeFormation;
	}

	public static EOStructure creerStructure(MockEditingContext editingContext, String libelle) {
		EOStructure structure = spy(new EOStructure());
		doReturn(libelle).when(structure).construireCStructure();
		doReturn(1).when(structure).construirePersId();
		editingContext.insertSavedObject(structure);
		return structure;
	}

	public static void remplirTypeStructure(MockEditingContext editingContext) {
		EOTypeStructure typeStructure = spy(new EOTypeStructure());
		typeStructure.setCTypeStructure(EOTypeStructure.TYPE_STRUCTURE_A);
		typeStructure.setDCreation(new NSTimestamp());
		editingContext.insertSavedObject(typeStructure);
	}

	public static EODiscipline creerDiscipline(MockEditingContext editingContext) {
		EODiscipline discipline = new EODiscipline();
		discipline.setCode("DISC");
		discipline.setLibelleLong("Discipline");
		discipline.setLibelleCourt("discipline");
		editingContext.insertSavedObject(discipline);
		return discipline;
	}

	public static EOFormule creerFormule(MockEditingContext editingContext) {
		EOFormule formule = new EOFormule();
		editingContext.insertSavedObject(formule);
		return formule;
	}

	public static EOTypeFormule creerTypeFormule(MockEditingContext editingContext, String codeTypeFormule) {
		EOTypeFormule typeFormule = new EOTypeFormule();
		typeFormule.setType(codeTypeFormule);
		editingContext.insertSavedObject(typeFormule);
		return typeFormule;
	}

	public static EOStatutFormation creerStatutFormation(MockEditingContext editingContext, Integer id, String code, String libelle) {
		EOStatutFormation statutFormation = new EOStatutFormation();
		statutFormation.setId(id);
		statutFormation.setCode(code);
		statutFormation.setLibelle(libelle);
		editingContext.insertSavedObject(statutFormation);
		return statutFormation;
	}

	public static void setValidateur(EODiplome diplome) {
		for (EOComposant composant : diplome.allSuccessors()) {
			composant.setValidateur(new CodeValidateurSaisieObligatoire());
		}
	}

	public static EOTypeNote creerTypeNote(MockEditingContext editingContext, String code, String libelle) {
		EOTypeNote typeNote = new EOTypeNote();
		typeNote.setCode(code);
		typeNote.setLibelle(libelle);
		editingContext.insertSavedObject(typeNote);
		return typeNote;
	}

	public static EOInscription creerInscription(MockEditingContext editingContext) {
		EOInscription inscription = new EOInscription();
		editingContext.insertSavedObject(inscription);
		return inscription;
	}
	
	public static EOInscription creerInscription(MockEditingContext editingContext, EODiplome diplome, String idLienConsultationDiplome, String idLienContextAnnee) {
		EOInscription inscription = new EOInscription();
		editingContext.insertSavedObject(inscription);
		inscription.setToDiplomeRelationship(diplome);
		inscription.setIdLienConsultationDiplome(idLienConsultationDiplome);
		inscription.setIdLienConsultationAnnee(idLienContextAnnee);
		return inscription;
	}

	public static EOInscriptionPedagogique creerInscriptionPedagogique(MockEditingContext editingContext, EOInscription inscription) {
		EOInscriptionPedagogique inscriptionPedagogique = new EOInscriptionPedagogique();
		editingContext.insertSavedObject(inscriptionPedagogique);
		inscriptionPedagogique.setToInscriptionRelationship(inscription);
		editingContext.saveChanges();
		return inscriptionPedagogique;
	}

	public static EOInscriptionPedagogiqueElement creerInscriptionPedagogiqueElement(MockEditingContext editingContext, EOLien lien,
	    EOInscriptionPedagogique inscription) {
		EOInscriptionPedagogiqueElement inscriptionPedagogiqueElement = new EOInscriptionPedagogiqueElement();
		editingContext.insertSavedObject(inscriptionPedagogiqueElement);
		inscriptionPedagogiqueElement.setToLienRelationship(lien);
		inscriptionPedagogiqueElement.setToInscriptionPedagogiqueRelationship(inscription);
		editingContext.saveChanges();
		return inscriptionPedagogiqueElement;
	}

	public static EONote creerNote(MockEditingContext editingContext, EOTypeNote typeNote, EOInscriptionPedagogiqueElement inscriptionPeda, Integer noSession) {
		EONote note = new EONote();
		editingContext.insertSavedObject(note);
		note.setToInscriptionPedagogiqueElementRelationship(inscriptionPeda);
		note.setToTypeNoteRelationship(typeNote);
		note.setNoSession(noSession);
		note.setPersIdCreation(1);
		note.setPersIdModification(1);
		editingContext.saveChanges();
		return note;
	}

	public static EONote creerNote(MockEditingContext editingContext, EOTypeNote typeNote, EOInscriptionPedagogiqueElement inscriptionPeda, Integer noSession,
	    BigDecimal valeur) {
		EONote note = creerNote(editingContext, typeNote, inscriptionPeda, noSession);
		note.setNoteInitiale(valeur);
		editingContext.saveChanges();
		return note;
	}

	// Private helpers
	public static void addCreditableToComposant(MockEditingContext editingContext, EOComposant composant, BigDecimal travailEtudiant, BigDecimal creditECTS) {
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		creditable.setTravailEtudiant(travailEtudiant);
		creditable.setCreditECTS(creditECTS);
		composant.addToCreditables(creditable);
		editingContext.saveChanges();
	}

	public static void addCreditableToLien(MockEditingContext editingContext, EOLien lien, BigDecimal travailEtudiant, BigDecimal creditECTS) {
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		creditable.setTravailEtudiant(travailEtudiant);
		creditable.setCreditECTS(creditECTS);
		lien.addToCreditables(creditable);
		editingContext.saveChanges();
	}

	public static EOTypePeriode creerTypePeriode(MockEditingContext editingContext, String code, String libelle) {
		EOTypePeriode typePeriode = new EOTypePeriode();
		typePeriode.setCode(code);
		typePeriode.setLibelle(libelle);
		editingContext.insertSavedObject(typePeriode);
		return typePeriode;
	}

	public static EOTypeEtudiant creerTypeEtudiant(MockEditingContext editingContext, String code, String libelle) {
		EOTypeEtudiant typeEtudiant = new EOTypeEtudiant();
		typeEtudiant.setCode(code);
		typeEtudiant.setLibelle(libelle);
		editingContext.insertSavedObject(typeEtudiant);
		return typeEtudiant;
	}

	public static DiplomeSimpleContainer creerDiplomeSimple(TestsInitializer init, DummySequence sequenceLiens, int annee) {
		MockEditingContext editingContext = init.getEditingContext();
		DiplomeSimpleContainer diplomeSimpleContainer = new DiplomeSimpleContainer();
		EODiplome diplomeSimple = EOTestsHelper.creerDiplome(editingContext, "DIPSIMPLE", "DIPLOME SIMPLE", init.getStatutFormation(),
		    init.getGradeUniversitaire(), init.getTypeFormation(), annee);
		diplomeSimpleContainer.setDiplome(diplomeSimple);
		EOVersionDiplome vd = EOTestsHelper.creerVersionDiplome(editingContext, "VD1", "Version Diplome 1", annee);
		EOTypePeriode typePeriodeAnnee = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_ANNEE, EOTypePeriode.CODE_ANNEE);
		EOTypePeriode typePeriodeSemestre = EOTestsHelper.creerTypePeriode(editingContext, EOTypePeriode.CODE_SEMESTRE, EOTypePeriode.CODE_SEMESTRE);

		EOLien lienDiplomeVersionDiplome = EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), diplomeSimple, vd, init.getTypeLienVersionner());
		diplomeSimpleContainer.setLienDiplomeVersionDiplome(lienDiplomeVersionDiplome);

		EOPeriode licence1 = EOTestsHelper.creerPeriode(editingContext, "L1", "Licence 1", typePeriodeAnnee);
		licence1.setToTypePeriodeRelationship(init.getTypePeriodeAnnee());
		licence1.setOrdre(1);
		EOLienComposer lienVersionDiplomeL1 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), vd, licence1, false);
		diplomeSimpleContainer.setLienVersionDiplomeL1(lienVersionDiplomeL1);

		EOPeriode licence2 = EOTestsHelper.creerPeriode(editingContext, "L2", "Licence 2", typePeriodeAnnee);
		licence2.setToTypePeriodeRelationship(init.getTypePeriodeAnnee());
		licence2.setOrdre(2);
		EOLienComposer lienVersionDiplomeL2 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), vd, licence2, false);
		diplomeSimpleContainer.setLienVersionDiplomeL2(lienVersionDiplomeL2);

		EOPeriode semestre1 = EOTestsHelper.creerPeriode(editingContext, "S1", "Semestre 1", typePeriodeSemestre);
		semestre1.setToTypePeriodeRelationship(init.getTypePeriodeSemestre());
		semestre1.setOrdre(1);
		EOLienComposer lienL1Semestre1 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), licence1, semestre1, false);
		diplomeSimpleContainer.setLienL1Semestre1(lienL1Semestre1);

		EOPeriode semestre2 = EOTestsHelper.creerPeriode(editingContext, "S2", "Semestre 2", typePeriodeSemestre);
		semestre2.setToTypePeriodeRelationship(init.getTypePeriodeSemestre());
		semestre2.setOrdre(2);
		EOLienComposer lienL1Semestre2 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), licence1, semestre2, false);
		diplomeSimpleContainer.setLienL1Semestre2(lienL1Semestre2);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "EC1", "Ec 1");
		EOLienComposer lienSemestre1Ec1 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), semestre1, ec1, true);
		diplomeSimpleContainer.setLienSemestre1Ec1(lienSemestre1Ec1);

		EOTypeAP typeApCours = editingContext.createSavedObject(EOTypeAP.class);
		typeApCours.setCode("CM");
		EOAP ap1 = EOTestsHelper.creerAP(editingContext, "AP1", typeApCours);
		EOLienComposer lienEc1Ap1 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), ec1, ap1, true);
		diplomeSimpleContainer.setLienEc1Ap1(lienEc1Ap1);

		EOTypeAE typeAeCours = editingContext.createSavedObject(EOTypeAE.class);
		typeAeCours.setCode("CM");
		EOAE ae1 = EOTestsHelper.creerAE(editingContext, "AE1", typeAeCours);
		EOLienComposer lienAp1Ae1 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), ap1, ae1, true);
		diplomeSimpleContainer.setLienAp1Ae1(lienAp1Ae1);

		EOEC ec2 = EOTestsHelper.creerEC(editingContext, "EC2", "Ec 2");
		EOLienComposer lienSemestre1Ec2 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), semestre1, ec2, true);
		diplomeSimpleContainer.setLienSemestre1Ec2(lienSemestre1Ec2);

		EOEC ec3 = EOTestsHelper.creerEC(editingContext, "EC3", "Ec 3");
		EOLienComposer lienSemestre1Ec3 = EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), semestre1, ec3, false);
		diplomeSimpleContainer.setLienSemestre1Ec3(lienSemestre1Ec3);

		return diplomeSimpleContainer;
	}

	/**
	 * @param init les éléments de base (typeComposant, statut de formation, etc...)
	 * @param sequenceLiens une séquence pour initialiser les liens,
	 * @return un diplome reduit à sa structure haute (pour le calcul des IPs) Diplome => VersionDiplome =>Annee1 => Semestre 1 =>Semestre 2
	 */
	public static EODiplome creerDiplomePourIP(TestsInitializer init, DummySequence sequenceLiens, DummySequence sequenceComposant, int annee) {
		MockEditingContext editingContext = init.getEditingContext();
		EODiplome diplome = creerDiplome(editingContext, sequenceComposant.getNextVal(), "DIP", "diplome", init.getStatutFormation(),
		    init.getGradeUniversitaire(), init.getTypeFormation(), annee);
		EOVersionDiplome versionDiplome = creerVersionDiplome(editingContext, sequenceComposant.getNextVal(), "VDIP", "versionDiplome", annee);
		EOLien lienDiplomeVersionDiplome = EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), diplome, versionDiplome, init.getTypeLienVersionner());
		String idLienContext = String.format("/%d/%d", diplome.id(), versionDiplome.id());
		EOTestsHelper.creerLienConsultation(editingContext, idLienContext, diplome, versionDiplome, lienDiplomeVersionDiplome, lienDiplomeVersionDiplome, diplome, versionDiplome);
		
		EOPeriode annee1 = creerPeriode(editingContext, sequenceComposant.getNextVal(), "CODE_ANNEE1", "Année 1", init.getTypePeriodeAnnee(), 1);
		EOLien lienVersionDiplomeAnnee1 = EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), versionDiplome, annee1, init.getTypeLiencomposer());
		idLienContext = String.format("/%d/%d/%d", diplome.id(), versionDiplome.id(), annee1.id());
		EOTestsHelper.creerLienConsultation(editingContext, idLienContext, diplome, versionDiplome, lienDiplomeVersionDiplome, lienVersionDiplomeAnnee1, versionDiplome, annee1);

		EOPeriode semestre1 = creerPeriode(editingContext, sequenceComposant.getNextVal(), "CODE_S1", "Semestre 1", init.getTypePeriodeSemestre(), 1);
		EOLien lienAnnee1Semestre1 = EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), annee1, semestre1, init.getTypeLiencomposer());
		idLienContext = String.format("/%d/%d/%d/%d", diplome.id(), versionDiplome.id(), annee1.id(), semestre1.id());
		EOTestsHelper.creerLienConsultation(editingContext, idLienContext, diplome, versionDiplome, lienDiplomeVersionDiplome, lienAnnee1Semestre1, annee1, semestre1);

		EOPeriode semestre2 = creerPeriode(editingContext, sequenceComposant.getNextVal(), "CODE_S2", "Semestre 2", init.getTypePeriodeSemestre(), 2);
		EOLien lienAnnee1Semestre2 = EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), annee1, semestre2, init.getTypeLiencomposer());
		idLienContext = String.format("/%d/%d/%d/%d", diplome.id(), versionDiplome.id(), annee1.id(), semestre2.id());
		EOTestsHelper.creerLienConsultation(editingContext, idLienContext, diplome, versionDiplome, lienDiplomeVersionDiplome, lienAnnee1Semestre2, annee1, semestre2);
		
		return diplome;
	}

	public static EOBourses creerBourse(MockEditingContext editindContext, Integer annee) {
		EOBourses bourse = new EOBourses();
		// bourse.setToEtudiantRelationship(etudiant);
		bourse.setAnnee(annee);
		editindContext.insertSavedObject(bourse);

		return bourse;
	}

	public static EOCursus creerCursus(MockEditingContext editindContext, Integer annee) {
		EOCursus cursus = new EOCursus();
		cursus.setAnneeDebut(annee);
		editindContext.insertSavedObject(cursus);
		return cursus;

	}

	public static EOEtudiantAnnee creerEudiantAnnee(MockEditingContext editindContext, Integer annee) {
		EOEtudiantAnnee etudAnne = new EOEtudiantAnnee();
		etudAnne.setAnnee(annee);
		editindContext.insertSavedObject(etudAnne);
		return etudAnne;
	}

	public static EOSiseDiscipline creerSiseDiscipline(MockEditingContext editingContext, String code, String libelle) {
		EOSiseDiscipline siseDiscipline = new EOSiseDiscipline();
		siseDiscipline.setCode(code);
		siseDiscipline.setLibelle(code);
		editingContext.insertSavedObject(siseDiscipline);
		return siseDiscipline;
	}

	public static EOSiseSecteurDisciplinaire creerSiseSecteurDisciplinaire(MockEditingContext editingContext, String code, String libelle,
	    EOSiseDiscipline siseDiscipline) {
		EOSiseSecteurDisciplinaire secteurDisciplinaire = new EOSiseSecteurDisciplinaire();
		secteurDisciplinaire.setCode(code);
		secteurDisciplinaire.setLibelle(code);
		editingContext.insertSavedObject(secteurDisciplinaire);
		secteurDisciplinaire.setSiseDisciplineRelationship(siseDiscipline);
		editingContext.saveChanges();
		return secteurDisciplinaire;
	}

	public static EONiveauAccesDiplome creerNiveauAccesDiplome(MockEditingContext editingContext, String libelle, int niveau) {
		EONiveauAccesDiplome niveauAccesDiplome = new EONiveauAccesDiplome();
		niveauAccesDiplome.setNiveau(niveau);
		niveauAccesDiplome.setLibelle(libelle);
		editingContext.insertSavedObject(niveauAccesDiplome);
		return niveauAccesDiplome;
	}

	public static EOResponsableComposant creerResponsableComposant(MockEditingContext editingContext, EOComposant composant, IPersonne individu) {
		EOResponsableComposant responsableComposant = EOResponsableComposant.create(editingContext, composant, individu.persId());
		responsableComposant.setComposantRelationship(composant);
		return responsableComposant;
	}

	public static EOResponsableComposantRole creerResponsableComposantRole(MockEditingContext editingContext, EOResponsableComposant responsableComposant) {
		EOResponsableComposantRole responsableComposantRole = (EOResponsableComposantRole) EOUtilities.createAndInsertInstance(editingContext,
		    EOResponsableComposantRole.ENTITY_NAME);
		responsableComposantRole.setResponsableComposantRelationship(responsableComposant);
		return responsableComposantRole;
	}

	public static EOTypeDescriptifDestination creerTypeDescriptif(MockEditingContext editingContext, Boolean isHabilitation) {
		EOTypeDescriptifDestination typeDescriptifDestination = new EOTypeDescriptifDestination();
		if (isHabilitation) {
			typeDescriptifDestination.setType(ITypeDescriptifDestination.TYPEHABILITATION_LBL);
		} else {
			typeDescriptifDestination.setType(ITypeDescriptifDestination.TYPEPUBLICATION_LBL);
		}
		editingContext.insertSavedObject(typeDescriptifDestination);
		return typeDescriptifDestination;
	}

	public static EODescriptif creerDescriptif(MockEditingContext editingContext, String titre, EOTypeDescriptifDestination typeDescriptifDestination) {
		EODescriptif descriptif = new EODescriptif();
		descriptif.setLibelle(titre);
		descriptif.setDestination(typeDescriptifDestination);
		editingContext.insertSavedObject(descriptif);
		return descriptif;
	}
}
