package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.UnderTest;

public class EOChampTest extends EOScolTestCase {

	@UnderTest
	private EOChamp champ;
	public static final String CODE_INE = "codeIne";

	@Before
	public void setUp() {
		super.setUp();
		champ = EOChamp.createSco_Champ(
				editingContext, "APP", CODE_INE, new NSTimestamp(), new NSTimestamp(), true, 0, 999, 999);
		editingContext.saveChanges();
	}
	
	@Test
	public void testChampForCode() {
		EOChamp champTest = EOChamp.champForCode(editingContext, CODE_INE);
		assertNotNull(champTest);
	}
	
}
