package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.junit.Before;
import org.junit.Test;

import com.wounit.annotations.Dummy;

public class EOECTest extends EOScolTestCase {
	@Dummy
	EOEC eoEC;
	EODiscipline discipline;

	@Before
	public void setUp() {
		super.setUp();
		initialiserContexte();
	}

	@Test
	public void test_copy() {
		// Arrange
		EOEC ec = EOTestsHelper.creerEC(editingContext, "code", "libelle");
		ec.setLibre(0);
		EOLangue langue1 = EOTestsHelper.creerLangue(editingContext);
		EOLangue langue2 = EOTestsHelper.creerLangue(editingContext);
		ec.addToLanguesRelationship(langue1);
		ec.addToLanguesRelationship(langue2);
		EOCreditable creditable = EOTestsHelper.creerCreditable(editingContext);
		creditable.setBaseNote(20);
		ec.associerCreditable(creditable);
		for (int i = 0; i < 3; i++) {
			EOSessionComposant session= EOTestsHelper.creerSession(editingContext, "session"+i);
			session.setComposantRelationship(ec);   
        }
		editingContext.saveChanges();
		// Assert ready to test

		// Act
		EOEC newEc = ec.copy(2014, null);

		// Assert
		assertComposantsIdentiques(ec, newEc);
		assertEquals(ec.libelleCourt(), ec.libelleCourt());
		assertEquals(ec.disciplines().count(), ec.disciplines().count());
		assertEquals(ec.libre(), newEc.libre());
		assertEquals(ec.langues().count(), newEc.langues().count());
		assertEquals(ec.creditable().baseNote(), newEc.creditable().baseNote());
		//assertEquals(ec.sessions().count(), newEc.sessions().count());
	}

	/**
	 * Initialise le contexte necessaireà la création d'un composant de type EC
	 */
	private void initialiserContexte() {
		// initialiser le contexte
		EOTypeComposant typeComposanteEC = new EOTypeComposant();
		typeComposanteEC.setNom("EC");
		typeComposanteEC.setLibelle("EC");
		typeComposanteEC.setId(2);
		editingContext.insertSavedObject(typeComposanteEC);
		discipline = EOTestsHelper.creerDiscipline(editingContext);
		editingContext.insertSavedObject(discipline);
		assertNotNull(discipline);
	}
}
