package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.junit.Before;
import org.junit.Test;

import com.wounit.annotations.Dummy;

public class EORegleLiaisonTest extends EOScolTestCase {

	@Dummy
	private EORegleLiaison regleLiaison;

	@Before
	public void setUp() {
		super.setUp();
	}

	// Tests
	@Test
	public void test_validateForSave() throws Exception {
		// Arrange
		EOTypeComposant typeComposant = EOTestsHelper.creerTypeComposant(editingContext, 1, "type", "type");
		EOTypeLien typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "typeLien");
		EORegleLiaison regleLiaison = EOTestsHelper.creerRegleLiaison(editingContext, typeComposant, typeComposant, typeLien);
		EOComposant parent = EOTestsHelper.creerComposantConcret(editingContext, "parent", "parent", 1, typeComposant);
		EOComposant enfant = EOTestsHelper.creerComposantConcret(editingContext, "enfant", "enfant", 1, typeComposant);
		regleLiaison.setVisible(1);

		// Act
		// Assert
		try {
			// Act
			regleLiaison.setForbidden(0);
			regleLiaison.setAllowed(1);
			regleLiaison.validateForSave();
		} catch (Exception e) {
			fail("Expected exception was thrown!");
		}

		try {
			// Act
			regleLiaison.setForbidden(1);
			regleLiaison.setAllowed(1);
			regleLiaison.validateForSave();
			fail("Expected exception was not thrown!");

		} catch (Exception e) {
			// Assert
			// do nothing - le test est correctement passé
		}
		
		try {
			// Act
			regleLiaison.setForbidden(0);
			regleLiaison.setAllowed(0);
			regleLiaison.validateForSave();
		} catch (Exception e) {
			// Assert
			fail("Expected exception was thrown!");
		}

		regleLiaison.setAllowed(1);
		EOLien lien = EOComposantFactory.createLien(parent, enfant, typeLien);
		editingContext.insertSavedObject(lien);
		try {
			// Act
			regleLiaison.setAllowed(0);
			regleLiaison.validateForSave();
			fail("Expected exception was not thrown!");

		} catch (Exception e) {
			// Assert
			// do nothing - le test est correctement passé
		}
	}

	@Test
	public void test_isValid() {
		// Arrange
		EOTypeComposant typeComposant1 = EOTestsHelper.creerTypeComposant(editingContext, 1, "type1", "type1");
		EOTypeComposant typeComposant2 = EOTestsHelper.creerTypeComposant(editingContext, 2, "type2", "type2");
		EOTypeLien typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "typeLien");

		// Assert ready to test
		assertFalse(EORegleLiaison.isValid(editingContext, typeComposant1, typeComposant2, typeLien));

		// Act
		EORegleLiaison regleLiaison = new EORegleLiaison();
		regleLiaison.setAllowedParent(typeComposant1);
		regleLiaison.setAllowedChild(typeComposant2);
		regleLiaison.setTypeLien(typeLien);
		editingContext.insertSavedObject(regleLiaison);
		assertFalse(EORegleLiaison.isValid(editingContext, typeComposant1, typeComposant2, typeLien));

		regleLiaison.setAllowed(1);
		regleLiaison.setForbidden(0);
		regleLiaison.setVisible(1);
		editingContext.saveChanges();

		// Assert
		assertTrue(EORegleLiaison.isValid(editingContext, typeComposant1, typeComposant2, typeLien));
	}

	// private helpers
	private EORegleLiaison initialiserRegleLiaison() {
		EOTypeComposant typeComposant1 = EOTestsHelper.creerTypeComposant(editingContext, 1, "type1", "type1");
		EOTypeComposant typeComposant2 = EOTestsHelper.creerTypeComposant(editingContext, 2, "type2", "type2");
		EOTypeLien typeLien = EOTestsHelper.creerTypeLien(editingContext, 1, "typeLien");
		EORegleLiaison regleLiaison = new EORegleLiaison();
		regleLiaison.setAllowedChild(typeComposant1);
		regleLiaison.setAllowedParent(typeComposant2);
		regleLiaison.setTypeLien(typeLien);
		editingContext.insertSavedObject(regleLiaison);
		return regleLiaison;
	}
}
