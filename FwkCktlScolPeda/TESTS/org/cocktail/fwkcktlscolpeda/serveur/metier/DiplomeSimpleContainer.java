package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * Container des composants d'un diplome, permet de faciliter les
 * assertions lors des tests.
 * 
 * @author Alexis Tual
 *
 */
public class DiplomeSimpleContainer {

    private IDiplome diplome;
    private ILien lienDiplomeVersionDiplome;
    private ILienComposer lienVersionDiplomeL1;
    private ILienComposer lienVersionDiplomeL2;
    private ILienComposer lienL1Semestre1;
    private ILienComposer lienL1Semestre2;
    private ILienComposer lienSemestre1Ec1;
    private ILienComposer lienSemestre1Ec2;
    private ILienComposer lienSemestre1Ec3;
    private ILienComposer lienEc1Ap1;
    private ILienComposer lienAp1Ae1;

    public IDiplome getDiplome() {
        return diplome;
    }

    public List<ILien> getLiens() {
    	return Arrays.asList(
    			lienDiplomeVersionDiplome, lienVersionDiplomeL1, lienVersionDiplomeL2, 
    			lienL1Semestre1, lienL1Semestre2, lienSemestre1Ec1, lienSemestre1Ec2, 
    			lienSemestre1Ec3, lienEc1Ap1, lienAp1Ae1);
    }
    
    public void setDiplome(IDiplome diplome) {
        this.diplome = diplome;
    }

    public ILien getLienDiplomeVersionDiplome() {
        return lienDiplomeVersionDiplome;
    }

    public void setLienDiplomeVersionDiplome(ILien lienDiplomeVersionDiplome) {
        this.lienDiplomeVersionDiplome = lienDiplomeVersionDiplome;
    }

    public ILienComposer getLienVersionDiplomeL1() {
        return lienVersionDiplomeL1;
    }

    public void setLienVersionDiplomeL1(ILienComposer lienVersionDiplomeL1) {
        this.lienVersionDiplomeL1 = lienVersionDiplomeL1;
    }

    public ILienComposer getLienVersionDiplomeL2() {
        return lienVersionDiplomeL2;
    }

    public void setLienVersionDiplomeL2(ILienComposer lienVersionDiplomeL2) {
        this.lienVersionDiplomeL2 = lienVersionDiplomeL2;
    }

    public ILienComposer getLienL1Semestre1() {
        return lienL1Semestre1;
    }

    public void setLienL1Semestre1(ILienComposer lienL1Semestre1) {
        this.lienL1Semestre1 = lienL1Semestre1;
    }

    public ILienComposer getLienL1Semestre2() {
        return lienL1Semestre2;
    }

    public void setLienL1Semestre2(ILienComposer lienL1Semestre2) {
        this.lienL1Semestre2 = lienL1Semestre2;
    }

    public ILienComposer getLienSemestre1Ec1() {
        return lienSemestre1Ec1;
    }

    public void setLienSemestre1Ec1(ILienComposer lienSemestre1Ec1) {
        this.lienSemestre1Ec1 = lienSemestre1Ec1;
    }

    public ILienComposer getLienSemestre1Ec2() {
        return lienSemestre1Ec2;
    }

    public void setLienSemestre1Ec2(ILienComposer lienSemestre1Ec2) {
        this.lienSemestre1Ec2 = lienSemestre1Ec2;
    }

    public ILienComposer getLienSemestre1Ec3() {
        return lienSemestre1Ec3;
    }

    public void setLienSemestre1Ec3(ILienComposer lienSemestre1Ec3) {
        this.lienSemestre1Ec3 = lienSemestre1Ec3;
    }

    public ILienComposer getLienEc1Ap1() {
        return lienEc1Ap1;
    }

    public void setLienEc1Ap1(ILienComposer lienEc1Ap1) {
        this.lienEc1Ap1 = lienEc1Ap1;
    }

    public ILienComposer getLienAp1Ae1() {
        return lienAp1Ae1;
    }

    public void setLienAp1Ae1(ILienComposer lienAp1Ae1) {
        this.lienAp1Ae1 = lienAp1Ae1;
    }
}
