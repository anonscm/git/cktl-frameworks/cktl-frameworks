package org.cocktail.scol.ip;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.fest.assertions.core.Condition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={FwkCktlScolPedaSpringConfig.class})
@ActiveProfiles("normal")
@Transactional
@Sql({"/diplomeavecspecialite.sql", "/ipread.sql"})
public class InscriptionPedagogiqueReadRepositoryTest {

	private static final int ID_PARCOURS = 27538;
	private static final int ANNEE = 2014;
	private static final int ID_DIPLOME = 27527;
	private static final long ID_IP = 99651L;
	private static final long ID_IP_VALIDEE = 99654L;
	private static final long ID_IP_PRINCIPALE = ID_IP_VALIDEE;
	private static final long ID_IP_EN_ATTENTE = 99652L;
	private static final long ID_IP_VALID_ETUDIANT = 99653L;
	private static final long ID_IA = 99417L;
	private static final long ID_ETUDIANT = 9900045L;
	@Autowired
	private InscriptionPedagogiqueReadRepository inscriptionPedagogiqueReadRepository;

	@Test
	public void testGetInscriptionsPedagogiques() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesPrincipales() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(ANNEE, false);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP);
			}
		});
		ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(ANNEE, true);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_PRINCIPALE);
			}
		});

	}
	
	@Test
	public void testGetInscriptionsPedagogiquesDiplome() {
		IDiplome diplome = Mockito.mock(IDiplome.class);
		Mockito.when(diplome.id()).thenReturn(ID_DIPLOME);
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(ANNEE, diplome);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesEtudiant() {
		IEtudiant etudiant = Mockito.mock(IEtudiant.class);
		Mockito.when(etudiant.id()).thenReturn(ID_ETUDIANT);
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(etudiant, ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP);
			}
		});
	}

	@Test
	public void testGetInscriptionsPedagogiquesParcours() {
		IParcours parcours = Mockito.mock(IParcours.class);
		Mockito.when(parcours.id()).thenReturn(ID_PARCOURS);
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiques(ANNEE, parcours);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesValidees() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiquesValidees(ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_VALIDEE);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesValideesEtudiant() {
		IEtudiant etudiant = Mockito.mock(IEtudiant.class);
		Mockito.when(etudiant.id()).thenReturn(ID_ETUDIANT);
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiquesValidees(etudiant, ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_VALIDEE);
			}
		});
	}
	
	@Test
	public void testGetInscriptionPedagogiqueValideeIdInscription() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiquesValidees(ID_IA, ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_VALIDEE);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesEnAttente() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiquesEnAttente(ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_EN_ATTENTE);
			}
		});
	}
	
	@Test
	public void testGetInscriptionsPedagogiquesValideesParEtudiant() {
		List<InscriptionPedagogiqueRead> ips = inscriptionPedagogiqueReadRepository.getInscriptionsPedagogiquesValideesParEtudiant(ANNEE);
		assertThat(ips).isNotEmpty();
		assertThat(ips).areAtLeast(1, new Condition<InscriptionPedagogiqueRead>() {
			@Override
			public boolean matches(InscriptionPedagogiqueRead ip) {
				return ip.getInscriptionPedagogiqueId().equals(ID_IP_VALID_ETUDIANT);
			}
		});
	}
	
}
