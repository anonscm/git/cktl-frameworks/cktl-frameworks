package org.cocktail.scol.support;


import javax.sql.DataSource;

import org.cocktail.scol.support.oracle.OracleQueryExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.SQLTemplates;
import com.zaxxer.hikari.HikariDataSource;

/**
 * 
 */
@Configuration
@EnableTransactionManagement
public class FwkCktlScolPedaNormalConfig {


	@Value("${dbConnectURLGLOBAL:systemProperties['dbConnectURLGLOBAL']}")
	private String url;

	@Value("${dbConnectUserGLOBAL:systemProperties['dbConnectUserGLOBAL']}")
	private String userName;

	@Value("${dbConnectPasswordGLOBAL:systemProperties['dbConnectPasswordGLOBAL']}")
	private String password;

	protected SQLTemplates oracleTmpl;

	public FwkCktlScolPedaNormalConfig() {
		this.oracleTmpl = OracleTemplates.builder().printSchema().build();
	}

	@Bean
	public DataSource dataSource() {
		final HikariDataSource ds = new HikariDataSource();
		ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		ds.setJdbcUrl(url);
		ds.setUsername(userName);
		ds.setPassword(password);
		ds.setAutoCommit(false);
		ds.setConnectionTestQuery("select 1 from dual");
		return ds;
	}

	@Bean
	public PlatformTransactionManager transactionManager(DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean
	public QueryDslJdbcTemplate queryDslJdbcTemplate(DataSource dataSource) {
		return new QueryDslJdbcTemplate(new JdbcTemplate(dataSource), oracleTmpl);
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public OracleQueryExecutor oracleQueryExecutor(JdbcTemplate jdbcTemplate, QueryDslJdbcTemplate queryDslJdbcTemplate) {
		return new OracleQueryExecutor(queryDslJdbcTemplate, jdbcTemplate, this.oracleTmpl);
	}

}
