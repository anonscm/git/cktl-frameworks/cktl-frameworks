package org.cocktail.scol.maquette;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { FwkCktlScolPedaSpringConfig.class })
@ActiveProfiles("normal")
@Transactional
@Sql("/periodeRegimeInscription.sql")
public class RegimeInscriptionReadRepositoryTest {	
	private static final String CODE_FORMATION_INITIALE_HORS_APPRENTISSAGE = "10";
	private static final String CODE_REPRISE_ETUDE_NON_FINANCEE = "11";
	private static final String CODE_CONTRAT_PROFESSIONALISATION = "22";
	private static final Long ID_PERIODE = 27600L;
	@Autowired
	RegimeInscriptionReadRepository repository;
	
	/**
	 * Teste que le repo est bien injecté (conf spring OK)
	 */
	@Test
	public void repositoryPresent() {
		assertThat(repository).isNotNull();
	}
	
	@Test
  public void getRegimesInscription() {
	  // Act
		List<RegimeInscriptionRead> regimes = repository.getRegimesInscriptions();
	  // Assert	
		assertThat(regimes.size()).isEqualTo(5);
		assertThat(findRegimeByCode(regimes, CODE_FORMATION_INITIALE_HORS_APPRENTISSAGE)).isNotNull();

  }
	
	private RegimeInscriptionRead findRegimeByCode(List<RegimeInscriptionRead> regimes, final String code) {
	  RegimeInscriptionRead regime = Iterables.find(regimes, new Predicate<RegimeInscriptionRead>() {
			public boolean apply(RegimeInscriptionRead input) {
	      return input.getCode().equals(code);
      }
		});
	  
	  return regime;
  }

	@Test
  public void getRegimesInscriptionPourPeriode() {
	  // Act 
		List<RegimeInscriptionRead> regimes = repository.getRegimesInscriptionsPeriode(ID_PERIODE);
	  // Assert
		assertThat(regimes.size()).isEqualTo(2);
		assertThat(findRegimeByCode(regimes, CODE_REPRISE_ETUDE_NON_FINANCEE)).isNotNull();
		assertThat(findRegimeByCode(regimes, CODE_CONTRAT_PROFESSIONALISATION)).isNotNull();
  }
}
