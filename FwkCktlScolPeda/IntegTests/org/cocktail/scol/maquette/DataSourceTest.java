package org.cocktail.scol.maquette;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=FwkCktlScolPedaSpringConfig.class)
@ActiveProfiles("normal")
@Transactional
public class DataSourceTest {

	Logger log = Logger.getLogger(this.getClass().getName());
	
    @Autowired
    DataSource dataSource;

    @Test
    public void dataSourcePresent() {
        Assert.notNull(dataSource);
        if (dataSource instanceof DriverManagerDataSource) {
        	DriverManagerDataSource dmds = (DriverManagerDataSource) dataSource;
            log.info("URL de connexion à la BD : " + dmds.getUrl());
        }
    }
}
