package org.cocktail.scol.maquette;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.scol.maquette.ComposantSyllabusRead.Descriptif;
import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={FwkCktlScolPedaSpringConfig.class})
@ActiveProfiles("normal")
@Transactional
@Sql("/syllabus.sql")
public class ComposantSyllabusReadRepositoryTest {

	@Autowired
	private ComposantSyllabusReadRepository composantSyllabusReadService;
	
	@Test
	public void testGetComposant() {
		ComposantSyllabusRead diplomeComposantSyllabusRead = composantSyllabusReadService.getDiplomeComposantSyllabusRead(18552L);
		verifierSyllabus(diplomeComposantSyllabusRead);
	}
	
	@Test
	public void testGetComposantsSyllabusRead() {
		List<ComposantSyllabusRead> composantsSyllabusRead = composantSyllabusReadService.getComposantsSyllabusRead(18552L);
		verifierStructureDiplome(composantsSyllabusRead);
	}

	private void verifierSyllabus(ComposantSyllabusRead composantRacine) {
		List<Descriptif> descriptifsHabilitation = composantRacine.getDescriptifsHabilitation();
		assertThat(descriptifsHabilitation).hasSize(2);
		assertThat(descriptifsHabilitation.get(0).getVersion()).isEqualTo("v 0.1");
		assertThat(descriptifsHabilitation.get(1).getVersion()).isEqualTo("v 0.2");
		assertThat(composantRacine.getSelectedDescriptifHabilitation()).isEqualTo(91);
		List<Descriptif> descriptifsPublication = composantRacine.getDescriptifsPublication();
		assertThat(descriptifsPublication).hasSize(2);
		assertThat(descriptifsHabilitation.get(0).getVersion()).isEqualTo("v 0.1");
		assertThat(descriptifsHabilitation.get(1).getVersion()).isEqualTo("v 0.2");
		assertThat(composantRacine.getSelectedDescriptifPublication()).isEqualTo(90);
	}

	private void verifierStructureDiplome(List<ComposantSyllabusRead> composantsSyllabusRead) {
		assertThat(composantsSyllabusRead).hasSize(3);
		assertThat(composantsSyllabusRead.get(1).getLibelle()).startsWith("Licence 2");
		assertThat(composantsSyllabusRead.get(2).getLibelle()).startsWith("Licence 3");
		ComposantSyllabusRead licence1 = composantsSyllabusRead.get(0);
		assertThat(licence1.getLibelle()).startsWith("Licence 1");
		assertThat(licence1.getChildren()).hasSize(1);
		ComposantSyllabusRead ue1 = licence1.getChildren().get(0);
		assertThat(ue1.getLibelle()).startsWith("UE 1 test syllabus");
		assertThat(ue1.getChildren()).hasSize(1);
		ComposantSyllabusRead ec1 = ue1.getChildren().get(0);
		assertThat(ec1.getLibelle()).startsWith("EC 1");
	}

}
