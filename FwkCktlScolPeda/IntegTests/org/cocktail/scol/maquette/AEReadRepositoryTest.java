package org.cocktail.scol.maquette;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.List;

import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={FwkCktlScolPedaSpringConfig.class})
@ActiveProfiles("normal")
@Transactional
public class AEReadRepositoryTest {
	  
	@Autowired 
	AEReadRepository repository;
	
	/**
   * Teste que le repo est bien injecté (conf spring OK)
   */
  @Test
  public void repositoryPresent() {
  	assertThat(repository).isNotNull();   
  }

  @Test
  public void getAEs() {
	  // Arrange 
  	Long annee = 2013L;  	  	
	  boolean isEnModelisation=false;
		// Act
  	List<AERead>  aes = repository.getAEs(annee, isEnModelisation);
	  // Assert  	
  	assertThat(aes).isNotNull();
  	assertThat(aes.size()).isGreaterThan(0);
  }
}