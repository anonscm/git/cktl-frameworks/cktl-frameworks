package org.cocktail.scol.maquette;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.scol.support.FwkCktlScolPedaSpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { FwkCktlScolPedaSpringConfig.class })
@ActiveProfiles("normal")
@Transactional
@Sql("/diplomeavecspecialite.sql")
public class LienConsultationReadRepositoryTest {
	private static final String GRADE_UNIVERSITAIRE_LICENCE = "L";
	private static final String TYPE_FORMATION_LICENCE = "LIC";
	private static final String CODE_PARCOURS_A = "code7362";
	private static final String CODE_PARCOURS_B = "code7363";
	private static final Long ID_LIEN_PARCOURS_A = 20518L;
	// Constantes - correspondent aux valeurs qui sont dans diplomeavecspecialite.sql
	private static final Long ANNEE = 2014L;
	private static final Long ID_COMPOSANT_DIPLOME = 27527L;
	private static final Long ID_LIEN_VERSION_DIPLOME = 20536L;
	private static final String CODE_COMPOSANT_DIPLOME = "code7361";
	private static final String CODE_ANNEE_LIC1 = "Licence 1 du diplôme v 2014 - code7361";
	private static final String CODE_ANNEE_LIC2 = "Licence 2 du diplôme v 2014 - code7361";
	private static final String CODE_ANNEE_LIC3 = "Licence 3 du diplôme v 2014 - code7361";
	@Autowired
	LienConsultationReadRepository repository;

	/**
	 * Teste que le repo est bien injecté (conf spring OK)
	 */
	@Test
	public void repositoryPresent() {
		assertThat(repository).isNotNull();
	}

	@Test
	public void getVersionDiplomeVersionMax() {
		LienConsultationRead lien = repository.getVersionDiplomeVersionMax(ID_COMPOSANT_DIPLOME, ANNEE);
		assertThat(lien.getIdLien()).isEqualTo(ID_LIEN_VERSION_DIPLOME); // id lien entre le diplome et la version de diplome
	}

	@Test
	public void getLiensParcoursSousDiplome() {
		LienConsultationRead lienVersionDiplome = mock(LienConsultationRead.class);
		when(lienVersionDiplome.getIdLien()).thenReturn(ID_LIEN_VERSION_DIPLOME);

		List<LienConsultationRead> liens = repository.getLiensParcoursSous(lienVersionDiplome);
		assertThat(liens.size()).isEqualTo(2);
		assertThat(liens.get(0).getCodeComposant()).isEqualTo(CODE_PARCOURS_A);
		assertThat(liens.get(1).getCodeComposant()).isEqualTo(CODE_PARCOURS_B);
	}

	@Test
	public void getLiensAnneeSous() {
		LienConsultationRead lienParcoursA = mock(LienConsultationRead.class);
		when(lienParcoursA.getIdLien()).thenReturn(ID_LIEN_PARCOURS_A);

		List<LienConsultationRead> liens = repository.getLiensAnneeSous(lienParcoursA, null);
		assertThat(liens.size()).isEqualTo(3);
		assertThat(liens.get(0).getCodeComposant()).isEqualTo(CODE_ANNEE_LIC1);
		assertThat(liens.get(1).getCodeComposant()).isEqualTo(CODE_ANNEE_LIC2);
		assertThat(liens.get(2).getCodeComposant()).isEqualTo(CODE_ANNEE_LIC3);
	}

	@Test
	public void getLiensAnneeSous_EmptyNiveaux() {
		List<Long> niveauxPossibles = new ArrayList<Long>();
		LienConsultationRead lienParcoursA = mock(LienConsultationRead.class);
		when(lienParcoursA.getIdLien()).thenReturn(ID_LIEN_PARCOURS_A);

		List<LienConsultationRead> liens = repository.getLiensAnneeSous(lienParcoursA, niveauxPossibles);
		assertThat(liens.size()).isEqualTo(0);
	}

	@Test
	public void getLiensAnneeSous_Niveaux() {
		List<Long> niveauxPossibles = new ArrayList<Long>();
		niveauxPossibles.add(1L);
		LienConsultationRead lienParcoursA = mock(LienConsultationRead.class);
		when(lienParcoursA.getIdLien()).thenReturn(ID_LIEN_PARCOURS_A);

		List<LienConsultationRead> liens = repository.getLiensAnneeSous(lienParcoursA, niveauxPossibles);
		assertThat(liens.size()).isEqualTo(1);
		assertThat(liens.get(0).getCodeComposant()).isEqualTo(CODE_ANNEE_LIC1);
	}

	@Test
	public void getDiplomesVersionMaxEnProduction_emptyListTypeFormationsOuvertes() {
		// Arrange
		List<String> typeFormationsOuvertes = new ArrayList<String>();

		// Act
		List<LienConsultationDiplomeRead> liens = repository.getDiplomesVersionMaxEnProduction(ANNEE, typeFormationsOuvertes, null);

		// Assert
		assertThat(liens.size()).isEqualTo(0);
	}

	@Test
	public void getDiplomesVersionMaxEnProduction_nullList() {
		// Act
		List<LienConsultationDiplomeRead> liens = repository.getDiplomesVersionMaxEnProduction(ANNEE, null, null);

		// Assert
		assertThat(liens.size()).isGreaterThan(0);
		LienConsultationDiplomeRead lien = checkLienConsultationExists(liens);
		assertThat(lien).isNotNull();
	}

	@Test
	public void getDiplomesVersionMaxEnProductionTypeFormationsOuvertes() {
		// Arrange
		List<String> typeFormationsOuvertes = new ArrayList<String>();
		typeFormationsOuvertes.add(TYPE_FORMATION_LICENCE);

		// Act
		List<LienConsultationDiplomeRead> liens = repository.getDiplomesVersionMaxEnProduction(ANNEE, typeFormationsOuvertes, null);

		// Assert
		LienConsultationDiplomeRead lien = checkLienConsultationExists(liens);
		assertThat(lien).isNotNull();
	}

	@Test
	public void getDiplomesVersionMaxEnProductionGradesUniversitairesInterdits() {
		// Arrange
		List<String> gradesUniversitaires = new ArrayList<String>();
		gradesUniversitaires.add(GRADE_UNIVERSITAIRE_LICENCE);

		// Act
		List<LienConsultationDiplomeRead> liens = repository.getDiplomesVersionMaxEnProduction(ANNEE, null, gradesUniversitaires);

		// Assert
		assertThat(liens.size()).isGreaterThan(0);

		LienConsultationDiplomeRead lien = checkLienConsultationExists(liens);
		assertThat(lien).isNull();
	}

	@Test
	public void getDiplomesVersionMaxEnProductionEmptyListGradesUniversitairesInterdits() {
		// Arrange
		List<String> gradesUniversitaires = new ArrayList<String>();
		// Act
		List<LienConsultationDiplomeRead> liens = repository.getDiplomesVersionMaxEnProduction(ANNEE, null, gradesUniversitaires);

		// Assert
		assertThat(liens.size()).isGreaterThan(0);

		LienConsultationDiplomeRead lien = checkLienConsultationExists(liens);
		assertThat(lien).isNotNull();
	}

	private LienConsultationDiplomeRead checkLienConsultationExists(List<LienConsultationDiplomeRead> liens) {
		LienConsultationDiplomeRead lien = Iterables.find(liens, new Predicate<LienConsultationDiplomeRead>() {
			public boolean apply(LienConsultationDiplomeRead input) {
				return input.getLienConsultationRead().getCodeComposantParent().equals(CODE_COMPOSANT_DIPLOME);
			}
		}, null);
		return lien;
	}

}
