package org.cocktail.fwkcktlscolpeda.serveur.formules.services;

import static org.parboiled.errors.ErrorUtils.printParseErrors;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurMoyenneException;
import org.parboiled.Node;
import org.parboiled.Parboiled;
import org.parboiled.errors.ParsingException;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.support.ParsingResult;


/**
 * Calcul d'une formule numerique en utilisant Parboiled
 */
public class CalculFormuleServiceParboiled implements CalculFormuleService {
	
	private static final int SCALE_RESULTAT_FINAL = 2;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public BigDecimal calculer(String input) throws ParsingException, CalculateurMoyenneException {
		CalculatorParser parser = Parboiled.createParser(CalculatorParser.class);
		ParsingResult<?> result = ReportingParseRunner.run(parser.Logic(), input);

		if(result.hasErrors()) {
			//System.out.println("\nParse Errors:\n" + printParseErrors(result));
			throw new ParsingException(CalculateurMoyenneException.ERRORS_FORMULE_EXCEPTION, new Throwable(printParseErrors(result)));
		}
		else {
			CalcNode nodeValue;
			BigDecimal resultat = null;
			try {
				nodeValue = ((Node<CalcNode>) result.parseTreeRoot).getValue();
				resultat = nodeValue.getValue().setScale(SCALE_RESULTAT_FINAL, RoundingMode.HALF_UP);
			} catch (ArithmeticException e) {
				throw new CalculateurMoyenneException(CalculateurMoyenneException.DIVISION_ZERO_EXCEPTION);
			}
			return resultat;
		}
	}

}
