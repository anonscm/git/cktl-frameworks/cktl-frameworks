package org.cocktail.fwkcktlscolpeda.serveur.formules.services;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurMoyenneException;
import org.parboiled.errors.ParsingException;

/**
 * Calcul d'une formule numerique
 */
public interface CalculFormuleService {

	/**
	 * @param input formule que l'on souhaite calculer, par exemple (1+2.0)/3
	 * @return resultat du calcul
	 * @throws Exception 
	 */
	BigDecimal calculer(String input) throws ParsingException, CalculateurMoyenneException;
}
