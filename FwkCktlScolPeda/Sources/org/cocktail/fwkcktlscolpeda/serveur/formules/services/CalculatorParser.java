package org.cocktail.fwkcktlscolpeda.serveur.formules.services;

import java.math.BigDecimal;

import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.DontLabel;
import org.parboiled.annotations.SuppressNode;
import org.parboiled.support.Var;

/**
 * A calculator parser building an AST representing the expression structure before performing the actual calculation.
 * The value field of the parse tree nodes is used for AST nodes.
 * As opposed to the CalculatorParser2 this parser also supports floating point operations, negative numbers, a "power"
 * and a "SQRT" operation as well as optional whitespace between the various expressions components.
 */
@BuildParseTree
public class CalculatorParser extends CalculatorBaseParser<CalcNode> {

	@Override
	public Rule InputLine() {
		return Sequence(Logic(), EOI);
	}

	public Rule Logic() {
		return OperatorRule(Comparator(), FirstOf("& ", "| "));
	}

	public Rule Comparator() {
		return OperatorRule(Expression(), FirstOf("= ", "! ", "> ", "< ", "S ", "I "));
	}

	public Rule Expression() {
		return OperatorRule(Term(), FirstOf("+ ", "- "));
	}

	Rule Term() {
		return OperatorRule(Factor(), FirstOf("* ", "/ "));
	}

	Rule Factor() {
		// by using toRule("^ ") instead of Ch('^') we make use of the fromCharLiteral(...) transformation below
		return OperatorRule(Atom(), toRule("^ "));
	}

	public Rule OperatorRule(Rule subRule, Rule operatorRule) {
		Var<Character> op = new Var<Character>();
		return Sequence(subRule, 
				ZeroOrMore(operatorRule, 
						op.set(matchedChar()), 
						subRule, 
						push(new CalcNode(op.get(), pop(1), pop()))
						)
				);
	}

	Rule Atom() {
		return FirstOf(Number(), SquareRoot(), Parens(), Min(), Max());
	}

	Rule SquareRoot() {
		return Sequence(
				"SQRT ",
				Parens(),

				// create a new AST node with a special operator 'R' and only one child
				push(new CalcNode('R', pop(), null))
				);
	}

	Rule Parens() {
		return Sequence("( ", Logic(), ") ");
	}

	Rule Number() {
		return Sequence(
				// we use another Sequence in the "Number" Sequence so we can easily access the input text matched
				// by the three enclosed rules with "match()" or "matchOrDefault()"
				Sequence(
						Optional('-'),
						OneOrMore(Digit()),
						Optional('.', OneOrMore(Digit()))
						),

						// the matchOrDefault() call returns the matched input text of the immediately preceding rule
						// or a default string (in this case if it is run during error recovery (resynchronization))
						push(new CalcNode(new BigDecimal(matchOrDefault("0")))),
						WhiteSpace()
				);
	}

	Rule Digit() {
		return CharRange('0', '9');
	}

	Rule WhiteSpace() {
		return ZeroOrMore(AnyOf(" \t\f"));
	}

	Rule Min() {
		return Sequence(
				"MIN ", 
				SplitLiterals(),
				push(new CalcNode('m', pop(1), pop()))         
				);
	}

	Rule Max() {
		return Sequence(
				"MAX ", 
				SplitLiterals(),
				push(new CalcNode('M', pop(1), pop()))         
				);
	}


	Rule SplitLiterals() {
		return Sequence(LPAR, Optional(Logic(), ZeroOrMore(COMMA, Logic())),
				RPAR
				);
	}

	// we redefine the rule creation for string literals to automatically match trailing whitespace if the string
	// literal ends with a space character, this way we don't have to insert extra whitespace() rules after each
	// character or string literal

	@Override
	protected Rule fromStringLiteral(String string) {
		return string.endsWith(" ") ?
				Sequence(String(string.substring(0, string.length() - 1)), WhiteSpace()) :
					String(string);
	}

	@SuppressNode
	@DontLabel
	Rule Terminal(String string) {
		return Sequence(string, WhiteSpace()).label('\'' + string + '\'');
	}

	@SuppressNode
	@DontLabel
	Rule Terminal(String string, Rule mustNotFollow) {
		return Sequence(string, TestNot(mustNotFollow), WhiteSpace()).label('\'' + string + '\'');
	}

	final Rule COMMA = Terminal(",");
	final Rule LPAR = Terminal("(");
	final Rule RPAR = Terminal(")");


}