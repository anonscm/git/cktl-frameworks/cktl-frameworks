package org.cocktail.fwkcktlscolpeda.serveur.formules.services;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.parboiled.trees.ImmutableBinaryTreeNode;

/**
 * The AST node for the calculators. The type of the node is carried as a Character that can either contain an operator char or be null. In the latter case the
 * AST node is a leaf directly containing a value.
 */
public class CalcNode extends ImmutableBinaryTreeNode<CalcNode> {
	private BigDecimal value;
	private Character operator;

	public static final int SCALE_OPERATIONS_INTERNES = 10;
	
	public CalcNode(BigDecimal value) {
		super(null, null);
		this.value = value;
	}

	public CalcNode(Character operator, CalcNode left, CalcNode right) {
		super(left, right);
		this.operator = operator;
	}

	public BigDecimal getValue() {
		if (operator == null)
			return value;
		switch (operator) {
		case '+':
			return left().getValue().add(right().getValue());
		case '-':
			return left().getValue().subtract(right().getValue());
		case '*':
			return left().getValue().multiply(right().getValue());
		case '/':
			return left().getValue().setScale(SCALE_OPERATIONS_INTERNES).divide(right().getValue(), RoundingMode.HALF_UP);
		case '^':
			return left().getValue().pow(right().getValue().intValue());
		case 'R':
			return BigDecimal.valueOf(Math.sqrt(left().getValue().doubleValue()));
		case 'm':
			return left().getValue().min(right().getValue());
		case 'M':
			return left().getValue().max(right().getValue());
		case '>':
			return sup(left().getValue(), right().getValue());
		case 'S':
			return supOrEqual(left().getValue(), right().getValue());
		case '<':
			return inf(left().getValue(), right().getValue());
		case 'I':
			return infOrEqual(left().getValue(), right().getValue());
		case '&':
			return and(left().getValue(), right().getValue());
		case '|':
			return or(left().getValue(), right().getValue());
		case '=':
			return equal(left().getValue(), right().getValue());
		case '!':
			return nequal(left().getValue(), right().getValue());
		default:
			throw new IllegalStateException();
		}
	}

	private BigDecimal or(BigDecimal value1, BigDecimal value2) {
		if (BigDecimal.ONE.compareTo(value1) == 0 ||  
			BigDecimal.ONE.compareTo(value2) == 0) {
				return BigDecimal.ONE;
			}
			return BigDecimal.ZERO;
	}

	private BigDecimal and(BigDecimal value1, BigDecimal value2) {
		if (BigDecimal.ONE.compareTo(value1) == 0 && 
			BigDecimal.ONE.compareTo(value2) == 0) {
			return BigDecimal.ONE;
		}
		return BigDecimal.ZERO;
	}
	
	private BigDecimal infOrEqual(BigDecimal value1, BigDecimal value2) {
		return value1.compareTo(value2) <= 0 ? BigDecimal.ONE : BigDecimal.ZERO;
		
	}
	
	private BigDecimal inf(BigDecimal value1, BigDecimal value2) {
		return value1.compareTo(value2) < 0 ? BigDecimal.ONE : BigDecimal.ZERO;
	}

	private BigDecimal supOrEqual(BigDecimal value1, BigDecimal value2) {
		return value1.compareTo(value2) >= 0 ? BigDecimal.ONE : BigDecimal.ZERO;

	}

	private BigDecimal sup(BigDecimal value1, BigDecimal value2) {
		return value1.compareTo(value2) > 0 ? BigDecimal.ONE : BigDecimal.ZERO;
	}
	
	private BigDecimal equal(BigDecimal value1, BigDecimal value2) {
		if (value1.compareTo(value2) == 0) {
			return BigDecimal.ONE;
		}
		return BigDecimal.ZERO;
	}

	private BigDecimal nequal(BigDecimal value1, BigDecimal value2) {
		if (value1.compareTo(value2) != 0) {
			return BigDecimal.ONE;
		}
		return BigDecimal.ZERO;
	}
	
	@Override
	public String toString() {
		return (operator == null ? "Value " + value : "Operator '" + operator + '\'') + " | " + getValue();
	}

}