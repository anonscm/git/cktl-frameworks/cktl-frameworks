package org.cocktail.fwkcktlscolpeda.serveur;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.ExportSiseService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.ExportSiseServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.InscriptionAdministrativeService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.InscriptionAdministrativeServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.PeriodeInscriptionRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.PeriodeInscriptionRepositoryEOF;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.PeriodeInscriptionService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.PeriodeInscriptionServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services.InscriptionPedagogiqueService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services.InscriptionPedagogiqueServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepositoryEOF;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.JuryRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.JuryRepositoryEOF;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.SessionPeriodeRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.SessionPeriodeRepositoryEOF;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.DefaultAnneeProvider;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IAnneeProvider;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.Validateur;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.woinject.WOSessionScoped;

import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;

/**
 * Cette classe permet de lier les classes injectees
 */
public class FwkCktlScolPedaModule extends AbstractModule {
	
	/** {@inheritDoc} */
	@Override
	protected void configure() {
		 bind(Validateur.class).to(getImplementationClassFor(Validateur.class)).in(Scopes.SINGLETON);
		 bind(InscriptionAdministrativeService.class).to(InscriptionAdministrativeServiceImpl.class);
		 bind(InscriptionPedagogiqueService.class).to(InscriptionPedagogiqueServiceImpl.class);
		 bind(CreditableService.class).to(CreditableServiceImpl.class).in(Scopes.SINGLETON);
		 bind(ComposantRepository.class).to(ComposantRepositoryEOF.class);
		 bind(PeriodeInscriptionRepository.class).to(PeriodeInscriptionRepositoryEOF.class);
		 bind(PeriodeInscriptionService.class).to(PeriodeInscriptionServiceImpl.class);
		 bind(ExportSiseService.class).to(ExportSiseServiceImpl.class); 
		 bind(JuryRepository.class).to(JuryRepositoryEOF.class);
		 bind(SessionPeriodeRepository.class).to(SessionPeriodeRepositoryEOF.class);
	};

	/**
	 * @return editing context pour la session
	 */
	@Provides @WOSessionScoped
	public EOEditingContext editingContext() {
	    return ERXEC.newEditingContext();
	}
	
	private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
		try {
            String implementationClassName = System.getProperty(baseClass.getSimpleName());
            @SuppressWarnings("unchecked")
            Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
            return clazz;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
        }
	}
	
	
    /**
     * @return le {@link IAnneeProvider} de la session (créé si besoin)
     */
	@Provides
	public IAnneeProvider anneeProvider() {
	    IAnneeProvider anneeProvider = null;
	    WOContext context = ERXWOContext.currentContext();
	    if (context != null && context.hasSession()) {
	        CktlWebSession session = (CktlWebSession) context.session();

	        if (session != null) {
	            String key = IAnneeProvider.class.getName();
	            anneeProvider = (IAnneeProvider) session.objectForKey(key);
	            if (anneeProvider == null) {
	                anneeProvider = new DefaultAnneeProvider();
	                session.setObjectForKey(anneeProvider, key);
	            }
	        }
	    }
	    return anneeProvider;
	}
	
}
