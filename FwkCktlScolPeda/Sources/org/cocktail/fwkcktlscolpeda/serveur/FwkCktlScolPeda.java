package org.cocktail.fwkcktlscolpeda.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

/**
 * Classe principale de framework scolpeda
 */
public class FwkCktlScolPeda extends ERXFrameworkPrincipal {
	public static final String MODEL_NAME = "Sco_scolarite";
	private static FwkCktlScolPedaParamManager paramManager = new FwkCktlScolPedaParamManager();
	
	public static final Logger LOG = Logger.getLogger(FwkCktlScolPeda.class);	
	
	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlScolPeda.class);
	}

	public static FwkCktlScolPedaParamManager getParamManager() {
	  return paramManager;
  }
	
	public static void setParamManager(FwkCktlScolPedaParamManager paramManager) {
	  FwkCktlScolPeda.paramManager = paramManager;
  }
	
	@Override
	public void finishInitialization() {
		ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
		moduleRegister.addModule(new FwkCktlScolPedaModule());
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();
	}
	
	

}
