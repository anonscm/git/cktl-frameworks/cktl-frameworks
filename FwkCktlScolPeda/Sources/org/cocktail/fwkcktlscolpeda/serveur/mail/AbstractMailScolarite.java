package org.cocktail.fwkcktlscolpeda.serveur.mail;

import javax.mail.MessagingException;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.mail.AbstractCktlMailComponent;

import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;
import er.javamail.ERMailDeliveryHTML;
import er.javamail.ERMailDeliveryPlainText;


public abstract class AbstractMailScolarite {
	private static Logger logger = Logger.getLogger(AbstractMailScolarite.class);
	
	public static final String EMAIL_ACTIF = "org.cocktail.mail.actif";
	public static final String EMAIL_EXPEDITEUR = "org.cocktail.mail.expediteur";
	
	private String from = null;
	
	/**
	 * Est-ce que l'envoi de mail est actif ? (valeur du paramètre {@value #EMAIL_ACTIF})
	 * @return <code>true</code> si actif
	 */
	public boolean getActif() {
		return ERXProperties.booleanForKeyWithDefault(EMAIL_ACTIF, false);
	}
	
	/**
	 * Retourne les paramètres variables du message pouvant être substitués (champs/valeur).
	 * <p>
	 * Surcharger cette méthode si vous avez des champs variables dans votre e-mail (sujet et/ou corps).
	 * <p>
	 * L'implémentation classique est celle-ci :
	 * <pre>
	 *   Map<String, Object> valuesMap = new HashMap<String, Object>();
	 *   valuesMap.put("prenom", etudiant.prenomAffichage());
	 *   valuesMap.put("nom", etudiant.nomAffichage());
	 *   
	 *   StrSubstitutor strSubtitutor = new StrSubstitutor(valuesMap);
	 *   
	 *   return strSubtitutor;
	 * </pre>
	 * 
	 * @return les paramètres variables du message pouvant être substitués
	 */
	protected StrSubstitutor getStrSubstitutor() {
		return null;
	}
		
	/**
	 * Retourne l'adresse 'From' de l'expéditeur.
	 * <p>
	 * Par défaut retourne la valeur du paramètre {@value #EMAIL_EXPEDITEUR}.
	 * <p>
	 * Vous pouvez surcharger cette méthode.
	 * 
	 * @return l'adresse 'From' de l'expéditeur
	 */
	protected String getFromAdresse() {
		if (from == null) {
			from = ERXProperties.stringForKey(EMAIL_EXPEDITEUR);
		}
		
		return from;
	}
	
	/**
	 * Retourne les adresses 'To' du ou des destinataires.
	 * @return les adresses 'To' du ou des destinataires
	 */
	protected abstract NSArray<String> getToAdresses();
	
	/**
	 * Retourne les adresses 'CC' (copie-carbone) du ou des destinataires.
	 * @return les adresses 'CC' (copie-carbone) du ou des destinataires
	 */
	protected abstract NSArray<String> getCCAdresses();

	/**
	 * Retourne les adresses 'BCC' (copie-carbone cachée) du ou des destinataires.
	 * @return les adresses 'BCC' (copie-carbone cachée) du ou des destinataires
	 */
	protected abstract NSArray<String> getBCCAdresses();

	/**
	 * Retourne le template utilisé pour générer le sujet de l'e-mail.
	 * @return le template utilisé pour générer le sujet de l'e-mail
	 */
	protected abstract String getTemplateSujet();
	
	/**
	 * Retourne le template utilisé pour générer le corps de l'e-mail.
	 * @return le template utilisé pour générer le corps de l'e-mail
	 */
	protected abstract String getTemplateCorps();
	
	/**
	 * Retourne le sujet de l'e-mail formatté.
	 * @param strSubtitutor les valeurs de substitution
	 * @return le sujet formatté
	 */
	protected String getSujet(StrSubstitutor strSubtitutor) {
		if (strSubtitutor == null) {
			return getTemplateSujet();
		}
		
		return strSubtitutor.replace(getTemplateSujet());
	}
	
	/**
	 * Retourne le corps de l'e-mail formatté.
	 * @param strSubtitutor les valeurs de substitution
	 * @return le corps formatté
	 */
	protected String getCorps(StrSubstitutor strSubtitutor) {
		if (strSubtitutor == null) {
			return getTemplateCorps();
		}
		
		return strSubtitutor.replace(getTemplateCorps());
	}
	
	/**
	 * Retourne le mail component
	 * @return AbstractCktlMailComponent : le component html du mail
	 */
	protected abstract AbstractCktlMailComponent getMailComponent();
	
	public Boolean sendMail() {
		boolean isDelivered = false;
		
		boolean envoiMailActif = getActif();
		String from = getFromAdresse();
		
		if (!envoiMailActif) {
			logger.info("L'envoi de mail est désactivé (" + EMAIL_ACTIF + " est à false).");
		} else if (from == null) {
			logger.info("Pas d'adresse d'expéditeur défini (" + EMAIL_EXPEDITEUR + "). L'envoi de ce mail est impossible.");
		} else {
			NSArray<String> tos = getToAdresses();
			
			if (tos.isEmpty()) {
				logger.error("Pas d'adresse de destinataire. Mail non envoyé.");
			} else {
				
				
				StrSubstitutor strSubstitutor = getStrSubstitutor();
				String sujet = getSujet(strSubstitutor);
				String corps = getCorps(strSubstitutor);
				NSArray<String> ccs = getCCAdresses();
				NSArray<String> bccs = getBCCAdresses();
				
				try {
					if (getMailComponent()!=null) {
						isDelivered = sendMailComponent(getMailComponent());
					}
					else {
						isDelivered = sendMailPlainText();
					}
				} catch (MessagingException e) {
					logger.error("Une erreur est survenue lors de l'envoi du mail");
					logger.error(e);
				} catch (Exception e) {
					logger.error("Une erreur est survenue lors de l'envoi du mail");
					logger.error(e);
				}
			}
		}
		
		return isDelivered;

	}
	
	/**
	 * Fonction d'envoie de mail
	 * @param messageComponent
	 * @return Boolean : vrai si le mail est envoye
	 * @throws Exception
	 */
	private Boolean sendMailPlainText() throws Exception {
		return sendMailPlainText(getFromAdresse(), getToAdresses());
	}
	
	private Boolean sendMailPlainText(String from, NSArray<String> to) throws Exception {
		boolean isDelivered = false;

		StrSubstitutor strSubstitutor = getStrSubstitutor();
		String sujet = getSujet(strSubstitutor);
		String corps = getCorps(strSubstitutor);
		NSArray<String> ccs = getCCAdresses();
		NSArray<String> bccs = getBCCAdresses();
		
		try {
			ERMailDeliveryPlainText mailDelivery = new ERMailDeliveryPlainText();
			
			mailDelivery.setSubject(sujet);
			mailDelivery.setTextContent(corps);
			mailDelivery.setFromAddress(from);
			mailDelivery.setToAddresses(to);
			mailDelivery.setCCAddresses(ccs);
			mailDelivery.setBCCAddresses(bccs);
			mailDelivery.sendMail();
			
			isDelivered = true;
		} catch (MessagingException e) {
			logger.error("Une erreur est survenue lors de l'envoi du mail");
			logger.error(e);
		} catch (Exception e) {
			logger.error("Une erreur est survenue lors de l'envoi du mail");
			logger.error(e);
		}
		
		return isDelivered;
	}
	
	/**
	 * Fonction d'envoie de mail
	 * @param messageComponent
	 * @return Boolean : vrai si le mail est envoye
	 * @throws Exception
	 */
	private Boolean sendMailComponent(AbstractCktlMailComponent messageComponent) throws Exception {
		return sendMailComponent(getFromAdresse(), getToAdresses(), messageComponent);
		//sendMail(ERXProperties.stringForKey("cktlmailfactory.from"), to, messageComponent);
	}
	
	/**
	 * Fonction d'envoie de mail
	 * @param from
	 * @param to
	 * @param messageComponent
	 * @return Boolean : vrai si le mail est envoye
	 * @throws Exception
	 */
	private Boolean sendMailComponent(String from, NSArray<String> to, AbstractCktlMailComponent messageComponent) throws Exception {
		boolean isDelivered = false;

		try {

			ERMailDeliveryHTML mail = new ERMailDeliveryHTML();
			 
			// Mise en place du composant HTML
			mail.setComponent(messageComponent);
			
			mail.newMail();
			mail.setSubject(messageComponent.objet());
			mail.setFromAddress(from);
			mail.setToAddresses(to);
			mail.setCCAddresses(getCCAdresses());
			mail.setBCCAddresses(getBCCAdresses());
			mail.sendMail();
			
			isDelivered = true;
		} catch (MessagingException e) {
			logger.error("Une erreur est survenue lors de l'envoi du mail");
			logger.error(e);
		} catch (Exception e) {
			logger.error("Une erreur est survenue lors de l'envoi du mail");
			logger.error(e);
		}
		
		return isDelivered;
	}

}
