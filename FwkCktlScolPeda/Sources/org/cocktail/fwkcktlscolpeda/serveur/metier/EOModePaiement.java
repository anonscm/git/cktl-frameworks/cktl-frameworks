package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.server.metier.EOModeRecouvrement;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Cette classe représente un mode de paiement.
 * 
 * @author Pascal MACOUIN
 */
public class EOModePaiement extends _EOModePaiement implements IModePaiement {
	private static final long serialVersionUID = -375175672519267941L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOModePaiement.class);
	
	/**
     * Retourne le plan comptable de paiement pour ce mode de paiement et cet exercice.
     * <p>
     * On recherche en premier lieu un plan comptable renseigné dans la table mode de paiement de la SCO_SCOLARITE.
     * Si le plan comptable n'est pas renseignée, on le recherche dans la table mode de recouvrement de MARACUJA.
     * 
     * @param exercice l'exercice (l'année)
     * @return le plan comptable pour ce mode de paiement et cet exercice
	 */
	public String getPlanComptablePaiementAUtilisePourExercice(int exercice) {
		if (toPlanComptablePaiement() != null) {
			return toPlanComptablePaiement().pcoNum();
		}
		
		EOQualifier qualifier = EOModeRecouvrement.MOD_VALIDITE.eq("VALIDE")
				.and(EOModeRecouvrement.TO_EXERCICE.dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice))
				.and(EOModeRecouvrement.MOD_CODE.eq(modCodePaiement()));
		
		EOModeRecouvrement modeRecouvrement = EOModeRecouvrement.fetchByQualifier(editingContext(), qualifier);
		
		if (modeRecouvrement != null && modeRecouvrement.toPlanComptableExerPaiement() != null) {
			return modeRecouvrement.toPlanComptableExerPaiement().pcoNum();
		}
		
		return null;
	}

	/**
     * Retourne le plan comptable de remboursement pour ce mode de paiement et cet exercice.
     * <p>
     * On recherche en premier lieu un plan comptable renseigné dans la table mode de paiement de la SCO_SCOLARITE.
     * Si le plan comptable n'est pas renseignée, on le recherche dans la table mode de paiement de MARACUJA.
     * 
     * @param exercice l'exercice (l'année)
     * @return le plan comptable pour ce mode de paiement et cet exercice
	 */
	public String getPlanComptableRemboursementAUtilisePourExercice(int exercice) {
		if (toPlanComptableRemboursement() != null) {
			return toPlanComptableRemboursement().pcoNum();
		}
		
		EOQualifier qualifier = org.cocktail.fwkcktlcompta.server.metier.EOModePaiement.MOD_VALIDITE.eq("VALIDE")
				.and(org.cocktail.fwkcktlcompta.server.metier.EOModePaiement.TO_EXERCICE.dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice))
				.and(org.cocktail.fwkcktlcompta.server.metier.EOModePaiement.MOD_CODE.eq(modCodeRemboursement()));
		
		org.cocktail.fwkcktlcompta.server.metier.EOModePaiement modePaiement = org.cocktail.fwkcktlcompta.server.metier.EOModePaiement.fetchByQualifier(editingContext(), qualifier);
		
		if (modePaiement != null && modePaiement.toPlanComptableExerPaiement() != null) {
			return modePaiement.toPlanComptableExerPaiement().pcoNum();
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isSimple() {
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isPrelevement() {
		return IModePaiement.CODE_MODE_PRELEVEMENT.equals(code());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isCheque() {
		return IModePaiement.CODE_MODE_CHEQUE.equals(code());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isVirement() {
		return IModePaiement.CODE_MODE_VIREMENT.equals(code());
	}
}
