// DO NOT EDIT.  Make changes to EOVersionDiplome.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOVersionDiplome extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant {
  public static final String ENTITY_NAME = "Sco_VersionDiplome";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> COMPOSANT_SUIVANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("composantSuivant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  // Relationships
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String COMPOSANT_SUIVANT_KEY = COMPOSANT_SUIVANT.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EOVersionDiplome.class);

  public EOVersionDiplome localInstanceIn(EOEditingContext editingContext) {
    EOVersionDiplome localInstance = (EOVersionDiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOVersionDiplome.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOVersionDiplome.LOG.isDebugEnabled()) {
    	_EOVersionDiplome.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVersionDiplome.ANNEE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> composantSuivant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>)storedValueForKey(_EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> composantSuivant(EOQualifier qualifier) {
    return composantSuivant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> composantSuivant(EOQualifier qualifier, boolean fetch) {
    return composantSuivant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> composantSuivant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome.fetchSco_VersionDiplomes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantSuivant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome object) {
    includeObjectIntoPropertyWithKey(object, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
  }

  public void removeFromComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome object) {
    excludeObjectFromPropertyWithKey(object, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
  }

  public void addToComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome object) {
    if (_EOVersionDiplome.LOG.isDebugEnabled()) {
      _EOVersionDiplome.LOG.debug("adding " + object + " to composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantSuivant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
    }
  }

  public void removeFromComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome object) {
    if (_EOVersionDiplome.LOG.isDebugEnabled()) {
      _EOVersionDiplome.LOG.debug("removing " + object + " from composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantSuivant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome createComposantSuivantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome) eo;
  }

  public void deleteComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOVersionDiplome.COMPOSANT_SUIVANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantSuivantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> objects = composantSuivant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantSuivantRelationship(objects.nextElement());
    }
  }


  public static EOVersionDiplome createSco_VersionDiplome(EOEditingContext editingContext, Integer annee
, Integer id
, String libelle
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion composantInfoVersion) {
    EOVersionDiplome eo = (EOVersionDiplome) EOUtilities.createAndInsertInstance(editingContext, _EOVersionDiplome.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setId(id);
		eo.setLibelle(libelle);
    eo.setComposantInfoVersionRelationship(composantInfoVersion);
    return eo;
  }

  public static ERXFetchSpecification<EOVersionDiplome> fetchSpecForSco_VersionDiplome() {
    return new ERXFetchSpecification<EOVersionDiplome>(_EOVersionDiplome.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOVersionDiplome> fetchAllSco_VersionDiplomes(EOEditingContext editingContext) {
    return _EOVersionDiplome.fetchAllSco_VersionDiplomes(editingContext, null);
  }

  public static NSArray<EOVersionDiplome> fetchAllSco_VersionDiplomes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVersionDiplome.fetchSco_VersionDiplomes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVersionDiplome> fetchSco_VersionDiplomes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOVersionDiplome> fetchSpec = new ERXFetchSpecification<EOVersionDiplome>(_EOVersionDiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVersionDiplome> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOVersionDiplome fetchSco_VersionDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVersionDiplome.fetchSco_VersionDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVersionDiplome fetchSco_VersionDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVersionDiplome> eoObjects = _EOVersionDiplome.fetchSco_VersionDiplomes(editingContext, qualifier, null);
    EOVersionDiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_VersionDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVersionDiplome fetchRequiredSco_VersionDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVersionDiplome.fetchRequiredSco_VersionDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVersionDiplome fetchRequiredSco_VersionDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVersionDiplome eoObject = _EOVersionDiplome.fetchSco_VersionDiplome(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_VersionDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVersionDiplome localInstanceIn(EOEditingContext editingContext, EOVersionDiplome eo) {
    EOVersionDiplome localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
