package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

/**
 * Informations communes à un étudiant ou un pre-étudiant pour une annee donnee
 */
public interface IEtudiantAnnee {
	
	public EOEditingContext editingContext();
	
	/**
	 * @return l'interface commune aux (pré-)etudiants
	 */
	IInfosEtudiant toInfosEtudiant();
	
	/**
	 * @return date de creation de l'enregistrement
	 */
	NSTimestamp dCreation();
	
	/**
	 * @param value date de creation
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return date de modification de l'enregistrement
	 */
	NSTimestamp dModification();

	/**
	 * @param value date de modification
	 */
	void setDModification(NSTimestamp value);

	/**
	 * @return utilisateur créateur de l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value identifiant de l'individu qui a cree l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	
	/**
	 * @return utilisateur modificateur de l'enregistrement
	 */
	Integer persIdModification();
	
	/**
	 * @param value identifiant de l'individu qui a modifie l'enregistrement
	 */
	void setPersIdModification(Integer value);

	/**
	 * @return vrai si l'étudiant est boursier
	 */
	boolean boursier();
	
	/**
	 * @return La liste des bourses de l'étudiant pour cette année
	 */
	List<? extends IBourses> toBourses();
	
	/**
	 * @return vrai si adherent a la GME
	 */
	Boolean adhMutuelle();

	/**
	 * @param value adherent a la GME
	 */
	void setAdhMutuelle(Boolean value);

	/**
	 * @return vrai si affilie a la securite sociale
	 */
	Boolean affSs();

	/**
	 * @param value affilie a la securite sociale
	 */
	void setAffSs(Boolean value);

	/**
	 * @return annee pour laquelle sont collectees les informations
	 */
	Integer annee();

	/**
	 * @param value annee pour laquelle sont collectees les informations
	 */
	void setAnnee(Integer value);

	/**
	 * @return vrai si assurance civile
	 */
	Boolean assuranceCivile();

	/**
	 * @param value assurance civile
	 */
	void setAssuranceCivile(Boolean value);

	/**
	 * @return compagnie d'assurance civile
	 */
	String assuranceCivileCie();

	/**
	 * @param value compagnie d'assurance civile
	 */
	void setAssuranceCivileCie(String value);

	/**
	 * @return numero d'assurance civile
	 */
	String assuranceCivileNum();

	/**
	 * @param value numero d'assurance civile
	 */
	void setAssuranceCivileNum(String value);

	/**
	 * @return autre cas d'exoneration des frais de scolarite
	 */
	String autreCasExo();

	/**
	 * @param value autre cas d'exoneration des frais de scolarite
	 */
	void setAutreCasExo(String value);

	/**
	 * @return vrai si l'etudiant a effectue sa participation a la journée d'appel
	 */
	Integer certificatPartAppel();

	/**
	 * @param value vrai si l'etudiant a effectue sa participation a la journée d'appel
	 */
	void setCertificatPartAppel(Integer value);
	
	/**
	 * @return vrai si l'etudiant est demandeur d'emploi
	 */
	Boolean demandeurEmploi();

	/**
	 * @param value vrai si l'etudiant est demandeur d'emploi
	 */
	void setDemandeurEmploi(Boolean value);

	
	/**
	 * @return duree d'interruption des etudes
	 */
	Integer dureeInterrupt();

	/**
	 * @param value duree d'interruption des etudes
	 */
	void setDureeInterrupt(Integer value);

	/**
	 * @return vrai si l'étudiant a des enfants à charge
	 */
	Boolean enfantsCharge();

	/**
	 * @param value vrai si l'étudiant a des enfants à charge
	 */
	void setEnfantsCharge(Boolean value);

	/**
	 * @return le nombre d'enfants à charge
	 */
	Integer nbEnfantsCharge();

	/**
	 * @param value le nombre d'enfants à charge
	 */
	void setNbEnfantsCharge(Integer value);

	/**
	 * @return vrai si l'etudiant a un handicap
	 */
	Boolean etudHandicap();

	/**
	 * @param value vrai si l'etudiant a un handicap
	 */
	void setEtudHandicap(Boolean value);

	/**
	 * @return vrai si l'etudiant est en sport etude
	 */
	Boolean etudSportHN();

	/**
	 * @param value vrai si l'etudiant est en sport etude
	 */
	void setEtudSportHN(Boolean value);

	/**
	 * @return vrai si la formation est financee
	 */
	Boolean formationFinancee();

	/**
	 * @param value vrai si la formation est financee
	 */
	void setFormationFinancee(Boolean value);

	/**
	 * @return vrai si les etudes ont ete interrompues
	 */
	Boolean interruptEtude();

	/**
	 * @param value vrai si les etudes ont ete interrompues
	 */
	void setInterruptEtude(Boolean value);


	/**
	 * @return vrai si l'etudiant a ete recense
	 */
	Integer recensement();

	/**
	 * @param value vrai si l'etudiant a ete recense
	 */
	void setRecensement(Integer value);

	/**
	 * @return libelle
	 */
	String salLibelle();

	/**
	 * @param value libelle
	 */
	void setSalLibelle(String value);

	/**
	 * @return vrai si l'etudiant fait du sport universitaire
	 */
	Boolean sportUniv();

	/**
	 * @param value vrai si l'etudiant fait du sport universitaire
	 */
	void setSportUniv(Boolean value);

	/**
	 * @return premier tuteur
	 */
	String tuteur1();

	/**
	 * @param value premier tuteur
	 */
	void setTuteur1(String value);

	/**
	 * @return second tuteur
	 */
	String tuteur2();

	/**
	 * @param value secon tuteur
	 */
	void setTuteur2(String value);

	/**
	 * @return type de regime
	 */
	String typeRegime();

	/**
	 * @param value type de regime
	 */
	void setTypeRegime(String value);

	/**
	 * @return activite professionnelle
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle toActiviteProfessionnelle();

	/**
	 * @param value activite professionnelle
	 */
	void setToActiviteProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle value);

	/**
	 * @return cotisation
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation toCotisation();

	/**
	 * @param value cotisation
	 */
	void setToCotisationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation value);


	/**
	 * @return mutuelle
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle toMutuelle();

	/**
	 * @param value mutuelle
	 */
	void setToMutuelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle value);

	/**
	 * @return origine des ressources
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources toOrigineRessources();

	/**
	 * @param value origine des ressources
	 */
	void setToOrigineRessourcesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources value);

	/**
	 * @return profession (du parent)
	 */
	IProfession toProfession1();

	/**
	 * @param value profession (du parent)
	 */
	void setToProfession1Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value);

	/**
	 * @return profession (du deuxième parent)
	 */
	IProfession toProfession2();

	/**
	 * @param value profession (du deuxième parent)
	 */
	void setToProfession2Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value);

	/**
	 * @return quotite de travail
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail toQuotiteTravail();

	/**
	 * @param value quotite de travail
	 */
	void setToQuotiteTravailRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail value);

	/**
	 * @return code RNE de l'etablissement
	 */
	org.cocktail.fwkcktlpersonne.common.metier.EORne toRne();

	/**
	 * @param value code RNE de l'etablissement
	 */
	void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value);

	/**
	 * @return situation familiale
	 */
	ISituationFamiliale toSituationFamiliale();

	/**
	 * @param value situation familiale
	 */
	void setToSituationFamilialeRelationship(ISituationFamiliale value);

	/**
	 * @return situation professionnelle
	 */
	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle toSituationProfessionnelle();

	/**
	 * @param value situation professionnelle
	 */
	void setToSituationProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle value);

	/**
	 * @return type de handicap
	 */
	org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap toTypeHandicap();

	/**
	 * @param value type de handicap
	 */
	void setToTypeHandicapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap value);
	
	
	/**
	 * @return la  liste des inscriptions correspondantes à 
	 * l'étudiant et à l'année
	 */
	List<? extends IInscription> toInscriptions();
	
	/**
	 * @return la  liste des inscriptions correspondantes à 
	 * l'étudiant et à l'année
	 */
	List<? extends IInscription> toInscriptions(EOQualifier qualifier);
	/**
	 * 
	 * @return l'email des parents
	 */
	String emailParent();

	/**
	 * 
	 * @param value email des parents
	 */
	void setEmailParent(String value);

	/**
	 * 
	 * @return commentaire (texte libre)
	 */
	String commentaire();

	/**
	 * 
	 * @param value commentaire (texte libre)
	 */
	void setCommentaire(String value);

	/**
	 * @return true si l'étudiant accepte que son adresse électronique figure sur l'annuaire de l'établissement
	 */
	Boolean accordAnnuaire();

	/**
	 * @param value true si l'étudiant accepte que son adresse électronique figure sur l'annuaire de l'établissement
	 */
	void setAccordAnnuaire(Boolean value);

	/**
	 * @return true si l'étudiant accepte que sa photo apparaisse dans l'annuaire de l''établissement
	 */
	Boolean accordPhoto();

	/**
	 * @param value true si l'étudiant accepte que sa photo apparaisse dans l'annuaire de l''établissement
	 */
	void setAccordPhoto(Boolean value);

	/**
	 * @return true si l'étudiant s'engage à respecter la charte de bon usage de l'informatique
	 */
	Boolean respectCharte();

	/**
	 * @param value true si l'étudiant s'engage à respecter la charte de bon usage de l'informatique
	 */
	void setRespectCharte(Boolean value);

	/**
	 * @return statut de boursier CNOUS (renvoyée par le WS Aglae)
	 */
	String statutBoursierCnous();

	/**
	 * @param value statut de boursier CNOUS (renvoyée par le WS Aglae)
	 */
	void setStatutBoursierCnous(String value);

	/**
	 * 
	 * @return le sport pratiqué par le sportif de haut niveau
	 */
	String sportPratique();

	/**
	 * 
	 * @param value le sport pratiqué par le sportif de haut niveau
	 */
	void setSportPratique(String value);

	/**
	 * 
	 * @return le niveau sportif du sportif de haut niveau
	 */
	EONiveauSportif toNiveauSportif();

	/**
	 * 
	 * @param value le niveau sportif du sportif de haut niveau
	 */
	void setToNiveauSportifRelationship(EONiveauSportif value);

	/**
	 * 
	 * @return le numéro étudiant de la base d'origine
	 */
	String etudNoOrigine();

	/**
	 * 
	 * @param value le numéro étudiant de la base d'origine
	 */
	void setEtudNoOrigine(String value);

	
	/**
	 * @return le régime particulier des parents
	 */
	IRegimeParticulier toRegimeParticulier();

	/**
	 * Affecte le régime particulier des parents.
	 * @param unRegimeParticulier un régime particulier
	 */
	void setToRegimeParticulierRelationship(IRegimeParticulier unRegimeParticulier);

	
	IInscription getInscriptionPrincipale();

	IInscription getInscriptionSecondaire() ;
	
	 IInscription getInscriptionTertiaire();
		/**
	 * @return Le paiement initial (le premier paiement)
	 */
	IPaiement toPaiementInitial();
	
	/**
	 * @return les paiements de l'étudiant pour cette année.
	 */
	List<? extends IPaiement> toPaiements();

	/**
	 * @return <code>true</code> si l'étudiant année est valide
	 */
	Boolean valide();
	

	/**
	 * @return true si le statut de la bourse est un échelon valide
	 */
	Boolean isStatutBourseCNOUSEchelonValide();
	
	String etabBacEtranger();

	void setEtabBacEtranger(String value);

}
