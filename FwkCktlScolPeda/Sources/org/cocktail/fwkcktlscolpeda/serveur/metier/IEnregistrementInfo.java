package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Information d'enregistrement d'un élément
 */
public interface IEnregistrementInfo {
	
	/**
	 * met à jour les infos de création et modification sur le composant
	 * @param utilisateurPersId : identifiant de l'utilisateur qui effectue la création ou la modification
	 */
	void actualiseInfoEnregistrement(Integer utilisateurPersId);

}
