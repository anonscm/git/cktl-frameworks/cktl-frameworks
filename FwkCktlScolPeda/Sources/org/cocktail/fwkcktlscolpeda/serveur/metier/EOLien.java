package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Representation d'un lien.
 */
public class EOLien extends _EOLien implements ILien, ISupprimable {
	private static final long serialVersionUID = 1L;
	
	private static final BigDecimal trente = new BigDecimal(30);
	private static final BigDecimal soixante = new BigDecimal(60);
	
	private BigDecimal creditECTSCalcule;
	private Integer sommeMinutesEnseigneesCalculee;
	private Map<EOTypeAP, Integer> sommeMinutesEnseigneesCalculeePourTypeAP;

	/**
	 * contructeur d'un EOLien
	 */
	public EOLien() {
		super();
		sommeMinutesEnseigneesCalculeePourTypeAP = new HashMap<EOTypeAP, Integer>();
	}

	/**
	 * @param edc : contexte d'édition
	 * @param parent : composant parent
	 * @param child : composant enfant
	 * @param typeLien : type de lien
	 */
	public void modifierLien(EOEditingContext edc, EOComposant parent, EOComposant child, EOTypeLien typeLien) {
		if (!parent.lienPossible(child, typeLien)) {
			throw new IllegalArgumentException("Liaison invalide : Le type " + child.typeComposant() + " ne peut pas être un sous-composant du type "
			    + parent.typeComposant());
		} else {
			this.setParentRelationship(parent);
			this.setChildRelationship(child);
			this.setTypeLienRelationship(typeLien);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimer() {
		try {
			avantSuppression();
			
			// recalculer l'ordre des liens si le parent n'est pas un regroupement ou un UE ou un EC
			// donc pour tous les autres tels que EODiplome, EOVersionDiplome, EOPeriode, EOParcours, EOAP et EOAE
			if (!this.parent().getClass().equals(EORegroupement.class) && !this.parent().getClass().equals(EOUE.class) && !this.parent().getClass().equals(EOEC.class)) {
				recalculerOrdre();
			}
			
			child().removeFromLiensChildsRelationship(this);
			parent().removeFromLiensParentsRelationship(this);
			Enumeration<EODescriptif> objects = descriptifs().immutableClone().objectEnumerator();
			while (objects.hasMoreElements()) {
				removeFromDescriptifsRelationship(objects.nextElement());
			}
			// supprimer toutes les creditables
			this.deleteAllCreditablesRelationships();
			
			// supprimer les charges d'enseignements
			this.deleteAllChargeEnseignementsRelationships();
			
			// supprimer les ips
			this.deleteAllInscriptionPedaElementsRelationship();

			// Supprimer l'objet
			delete();

		} catch (Exception e) {
			System.err.println("Erreur suppression lien : " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * operation supplementaire avant la suppression d'un lien
	 * @throws Exception : levée si l'opération a echoue/
	 */
	public void operationSuppressionSupplementaire() throws Exception {
	}

	/**
	 * {@inheritDoc}
	 */
	public EOCreditable creditable() {
		if ((creditables() != null) && (creditables().count() == 1)) {
			return creditables().get(0);
		}

		return null;
	}

	/**
	 * @return EOSelectionLienDescriptif : récupére le EOSelectionLienDescriptif de type habilitation pour ce lien
	 */
	public EOSelectionLienDescriptif getSelectionLienDescriptifTypeHabilitation() {
		return EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptif(editingContext(),
		    ERXQ.equals(EOSelectionLienDescriptif.LIEN_KEY, this).and(ERXQ.equals(EOSelectionLienDescriptif.DESCRIPTIF_KEY, getDescriptifTypeHabilitation())));
	}

	/**
	 * @return EODescriptif descriptif d'habilitation sélectionné pour le lien
	 */
	public EODescriptif getDescriptifTypeHabilitation() {
		EODescriptif descriptif = null;

		if (descriptifs(EODescriptif.DESTINATION.eq(EOTypeDescriptifDestination.typeHabilitation(editingContext()))).size() > 0) {
			descriptif = descriptifs(EODescriptif.DESTINATION.eq(EOTypeDescriptifDestination.typeHabilitation(editingContext()))).get(0);
		}

		return descriptif;
	}

	/**
	 * @return EOSelectionLienDescriptif : récupére le EOSelectionLienDescriptif de type publication pour ce lien
	 */
	public EOSelectionLienDescriptif getSelectionLienDescriptifTypePublication() {
		return EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptif(editingContext(),
		    ERXQ.equals(EOSelectionLienDescriptif.LIEN_KEY, this).and(ERXQ.equals(EOSelectionLienDescriptif.DESCRIPTIF_KEY, getDescriptifTypePublication())));
	}

	/**
	 * @return Boolean : indique si le lien a un descriptif de type habilitation
	 */
	public Boolean hasDescriptifTypeHabilitation() {
		return (getDescriptifTypeHabilitation() != null);
	}

	/**
	 * @return EODescriptif descriptif de publication sélectionné pour le lien
	 */
	public EODescriptif getDescriptifTypePublication() {
		EODescriptif descriptif = null;

		if (descriptifs(EODescriptif.DESTINATION.eq(EOTypeDescriptifDestination.typePublication(editingContext()))).size() > 0) {
			descriptif = descriptifs(EODescriptif.DESTINATION.eq(EOTypeDescriptifDestination.typePublication(editingContext()))).get(0);
		}

		return descriptif;
	}

	/**
	 * @return Boolean : indique si le lien a un descriptif de type publication
	 */
	public Boolean hasDescriptifTypePublication() {
		return (getDescriptifTypePublication() != null);
	}

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return "Lien (" + id() + ") de type " + typeLien() + " entre le composant parent " + parent() + " et le composant child " + child();
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean avantSuppression() throws Exception {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean peutEtresupprime() {
		return true;
	}

	/**
	 * @param nouveauLien : nouveau lien auquel on assigne les valeur du lien courant
	 */
	public void assignTo(ILien nouveauLien) {
		((EOLien) nouveauLien).setTypeLienRelationship(this.typeLien());
		for (EOCreditable creditable : creditables()) {
			((EOLien) nouveauLien).addToCreditablesRelationship(creditable.copy());
		}
		
		// Charges d'enseignement
		for (EOChargeEnseignement chargeEnseignement : chargeEnseignements()) {
			chargeEnseignement.copySurLien((EOLien) nouveauLien);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal getCreditECTSCalcule() {
		return creditECTSCalcule;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
		this.creditECTSCalcule = creditECTSCalcule;
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer getSommeMinutesEnseigneesCalculee() {
		return sommeMinutesEnseigneesCalculee;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
		this.sommeMinutesEnseigneesCalculee = sommeMinutesEnseigneesCalculee;
	}



	/**
	 * {@inheritDoc}
	 */
	public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(typeAP);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeHeuresEnseignesCalculees, EOTypeAP typeAP) {
		sommeMinutesEnseigneesCalculeePourTypeAP.put(typeAP, sommeHeuresEnseignesCalculees);
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPTD() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeTD(this.editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeTD(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPTP() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeTP(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeTP(this.editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPCours() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeCours(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeCours(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Boolean isParentSemestre() {
		
		if (parent() instanceof IPeriode) {
			IPeriode periode  = (IPeriode) parent();
			return periode.isSemestre();
		} 

		return false;
	}

	/**
	 * Retourne la charge d'enseignement pour ce lien et ce type.
	 * 
	 * @param typeChargeEnseignement un type de charge d'enseignement (voir constantes dans {@link EOTypeChargeEnseignement})
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
	public IChargeEnseignement chargeEnseignement(String typeChargeEnseignement) {
		List<EOChargeEnseignement> listeChargeEnseignement = chargeEnseignements(EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT.dot(EOTypeChargeEnseignement.CODE).eq(typeChargeEnseignement));
		
		IChargeEnseignement chargeEnseignement = null;
		
		if (!listeChargeEnseignement.isEmpty()) {
			chargeEnseignement = listeChargeEnseignement.get(0);
		}
		
		return chargeEnseignement;
	}
	
	/**
	 * Retourne la charge d'enseignement prévisionelle (saisie surcharge Peche) pour ce lien.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementPrevisionnelle() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_PREVISIONNELLE);
    }
    
	/**
	 * Retourne la charge d'enseignement théorique (saisie Girofle) pour ce lien.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementTheorique() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_THEORIQUE);
    }

    /**
     * {@inheritDoc}
     */
	public Boolean isNbCreditsEctsOk() {
		if (child()instanceof EOPeriode) {
			EOPeriode periode = (EOPeriode) child();
			if (getCreditECTSCalcule() == null) {
				return false;
			}
			if (periode.isSemestre()) {
				return trente.compareTo(getCreditECTSCalcule()) <= 0;
			}
			if (periode.isAnnee()) {
				return soixante.compareTo(getCreditECTSCalcule()) <= 0;
			}
		}
		return null;
	}
	
	/**
	 * recalcule l'ordre des liens du parent
	 */
	private void recalculerOrdre() {
		if (this.ordre() != null) {
			if (this.parent().liensComposerAvecLesComposantsEnfants() != null && this.parent().liensComposerAvecLesComposantsEnfants().size() > 0) {
				for (int i = this.ordre(); i < this.parent().liensComposerAvecLesComposantsEnfants().size(); i++) {
					// recuperer le lien dont l'ordre est i et le set à i
					this.parent().liensComposerAvecLesComposantsEnfants().get(i).setOrdre(i);
				}
			}
		}
	}
	
	/**
	 * @return Integer : indique le max ordre
	 */
	public Integer getLastOrdre() {
		Integer maxOrdre = 0;

		for (EOLien lien : this.parent().liensComposerAvecLesComposantsEnfants()) {
			if (lien.ordre() != null && lien.ordre() > maxOrdre) {
				maxOrdre = lien.ordre();
			}
		}
		
		return maxOrdre;
	}

	/**
	 * Suppression des elements d'ips relies au lien
	 */
	public void deleteAllInscriptionPedaElementsRelationship() {
		NSArray<EOInscriptionPedagogiqueElement> elements = EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext(), EOInscriptionPedagogiqueElement.TO_LIEN.eq(this), null);
		for (EOInscriptionPedagogiqueElement e : elements) {
			e.delete();
		}
	}
	
	@Override
	public EOPeriode _childPeriode() {
		Object composant = storedValueForKey(_EOLien._CHILD_PERIODE_KEY);
		if (composant instanceof EOPeriode) {
			return (EOPeriode) composant;
		} else {
			return null;
		}
	}
	
	@Override
	public EOParcours _childParcours() {
		Object composant = storedValueForKey(_EOLien._CHILD_PARCOURS_KEY);
		if (composant instanceof EOParcours) {
			return (EOParcours) composant;
		} else {
			return null;
		}
	}
	
	@Override
	public EOVersionDiplome _childVersionDiplome() {
		Object composant = storedValueForKey(_EOLien._CHILD_VERSION_DIPLOME_KEY);
		if (composant instanceof EOVersionDiplome) {
			return (EOVersionDiplome) composant;
		} else {
			return null;
		}
	}
	
	@Override
	public EODiplome _parentDiplome() {
		Object composant = storedValueForKey(_EOLien._PARENT_DIPLOME_KEY);
		if (composant instanceof EODiplome) {
			return (EODiplome) composant;
		} else {
			return null;
		}
	}
	
	@Override
	public EOParcours _parentParcours() {
		Object composant = storedValueForKey(_EOLien._PARENT_PARCOURS_KEY);
		if (composant instanceof EOParcours) {
			return (EOParcours) composant;
		} else {
			return null;
		}
	}
	
	@Override
	public EOVersionDiplome _parentVersionDiplome() {
		Object composant = storedValueForKey(_EOLien._PARENT_VERSION_DIPLOME_KEY);
		if (composant instanceof EOVersionDiplome) {
			return (EOVersionDiplome) composant;
		} else {
			return null;
		}
	}
	
	public NSArray<EOInscriptionPedagogiqueElement> inscriptionPedagogiqueElements() {
		return EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext(), EOInscriptionPedagogiqueElement.TO_LIEN.eq(this), null);
	}
	
	public static EOLien fetchWithParentAndChild(EOEditingContext edc, EOComposant parent, EOComposant child) {
		return EOLien.fetchSco_Lien(edc, EOLien.PARENT.eq(parent).and(EOLien.CHILD.eq(child)));
	}

	/**
	 * Rechercher un lien par sont id.
	 * @param edc Un editing context
	 * @param idLien L'id du lien
	 * @return Le lien trouvé (ou <code>null</code> si non trouvé)
	 */
	public static EOLien fetchById(EOEditingContext edc, Integer idLien) {
		NSArray<EOLien> listeLiens = EOLien.fetchSco_Liens(edc, EOLienComposer.ID.eq(idLien), null);
		
		EOLien lien = null;
		
		// A cause de l'héritage, plusieurs lignes identiques sont retournées par EOF pour une même clé primaire
		if (!listeLiens.isEmpty()) {
			lien = listeLiens.get(0);
		}
		
		return lien;
	}

	
}
