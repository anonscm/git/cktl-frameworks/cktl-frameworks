package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.List;

import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe gère les APs.
 */
public class EOAP extends _EOAP implements IAP {
	private static final long serialVersionUID = 1L;

	// Public methods

	@Override
	public Boolean peutEtresupprime() {
		return (parents().size() <= 1);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean avantSuppression() throws Exception {
		try {
			supprimerChargesEnseignement();
			supprimerMode();
			supprimerType();
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	protected void supprimerChargesEnseignement() throws Exception {
		if (chargeEnseignements() == null) {
			return;
		}
		Enumeration<EOChargeEnseignement> objects = chargeEnseignements().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOChargeEnseignement chargeEnseignement = objects.nextElement();
			chargeEnseignement.supprimer();
		}
	}
	
	private void supprimerMode() throws Exception {
		if (this.mode() != null) {
			setModeRelationship(null);
		}
	}
	
	private void supprimerType() throws Exception {
		if (this.typeAP() != null) {
			setTypeAPRelationship(null);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean supprimerSiSansValeurs() {
		if (chargeEnseignementVideOuNull()) {
			try {
				supprimer();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		
		return false;
	}
	
	protected boolean chargeEnseignementVideOuNull() {
		return (this.chargeEnseignementTheorique() == null 
				|| (this.chargeEnseignementTheorique() != null && this.chargeEnseignementTheorique().valeurMinutes() == null
					&& this.chargeEnseignementTheorique().nbGroupes() == null));
	}

	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeAP(editingContext());
	}

	@Override
	public BigDecimal valeurEnHeures(Boolean dureeMax) {
		IChargeEnseignement chargeEnseignement = chargeEnseignementTheorique();
		
		if (chargeEnseignement != null) {
			return chargeEnseignement.valeurHeures();
		}
		
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public EOAP copy(Integer anneeVersion, Integer numeroMajeur) {
		EOAP ap;
		try {
			ap = EOComposantFactory.createComposant(EOAP.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(ap);
			return ap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return IAP : le composant suivant associe au composant si il existe
	 */
	public IAP getComposantAvecOrigine() {
		if(super.getComposantSuivant() != null) {
			return ((IAP) (super.getComposantSuivant()));
		}
		
		return null;
	}
	
	/**
	 * @return IAP : le composant d'origine
	 */
	public IAP getComposantOrigine() {
		return (IAP) composantOrigine();
	}

	protected void assignTo(EOAP newComposant) {
		super.assignTo(newComposant);
		newComposant.setSeuil(this.seuil());
		newComposant.setValeurTempsEtudiant(this.valeurTempsEtudiant());
		newComposant.setModeRelationship(this.mode());
		newComposant.setTypeAPRelationship(this.typeAP());
		
		// Charges d'enseignement
		for (EOChargeEnseignement chargeEnseignement : chargeEnseignements()) {
			chargeEnseignement.copySurAP(newComposant);
		}
	}

	/**
	 * Retourne la charge d'enseignement pour cette AP et ce type.
	 * 
	 * @param typeChargeEnseignement un type de charge d'enseignement (voir constantes dans {@link EOTypeChargeEnseignement})
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
	public IChargeEnseignement chargeEnseignement(String typeChargeEnseignement) {
		List<EOChargeEnseignement> listeChargeEnseignement = chargeEnseignements(EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT.dot(EOTypeChargeEnseignement.CODE).eq(typeChargeEnseignement));
		
		IChargeEnseignement chargeEnseignement = null;
		
		if (!listeChargeEnseignement.isEmpty()) {
			chargeEnseignement = listeChargeEnseignement.get(0);
		}
		
		return chargeEnseignement;
	}

	/**
	 * Retourne la charge d'enseignement prévisionnelle (saisie surcharge Peche) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementPrevisionnelle() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_PREVISIONNELLE);
    }
    
	/**
	 * Retourne la charge d'enseignement théorique (saisie Girofle) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementTheorique() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_THEORIQUE);
    }

	/**
	 * Retourne la charge d'enseignement pédagogique (saisie Paprika) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementPedagogique() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_PEDAGOGIQUE);
    }

    /**
	 * Retourne la charge d'enseignement planifiée (saisie SuperPlan) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement chargeEnseignementPlanifieee() {
    	return chargeEnseignement(ITypeChargeEnseignement.CODE_TYPE_PLANIFIEE);
    }
    
	/**
	 * Retourne la charge d'enseignement prévisionnelle (saisie surcharge Peche) pour cette AP.
	 * <p>
	 * Attention : s'il n'y a pas de charge d'enseignement pour l'AP, un objet {@link EOChargeEnseignement} est créé.
	 * 
	 * @return la charge d'enseignement (jamais <code>null</code>)
	 */
    public IChargeEnseignement chargeEnseignementPrevisionnelleJamaisNull() {
    	IChargeEnseignement chargeEnseignement = chargeEnseignementPrevisionnelle();
    	
    	if (chargeEnseignement == null) {
    		//chargeEnseignement = EOChargeEnseignement.creerChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typePrevisionnelle(editingContext()), persIdCreation());
    		chargeEnseignement = EOComposantFactory.createChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typePrevisionnelle(editingContext()), persIdCreation());
    	}
    	
    	return chargeEnseignement;
    }
    

    /** 
     * {@inheritDoc}
     */
    public IChargeEnseignement chargeEnseignementTheoriqueJamaisNull() {
    	IChargeEnseignement chargeEnseignement = chargeEnseignementTheorique();

    	if (chargeEnseignement == null) {
    		//chargeEnseignement = EOChargeEnseignement.creerChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typeTheorique(editingContext()), persIdCreation());
    		chargeEnseignement = EOComposantFactory.createChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typeTheorique(editingContext()), persIdCreation());
    	}
    	
    	return chargeEnseignement;
    }

	/**
	 * Retourne la charge d'enseignement pédagogique (saisie Paprika) pour cette AP.
	 * <p>
	 * Attention : s'il n'y a pas de charge d'enseignement pour l'AP, un objet {@link EOChargeEnseignement} est créé.
	 * 
	 * @return la charge d'enseignement (jamais <code>null</code>)
	 */
    public IChargeEnseignement chargeEnseignementPedagogiqueJamaisNull() {
    	IChargeEnseignement chargeEnseignement = chargeEnseignementPedagogique();

    	if (chargeEnseignement == null) {
    		//chargeEnseignement = EOChargeEnseignement.creerChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typeTheorique(editingContext()), persIdCreation());
    		chargeEnseignement = EOComposantFactory.createChargeEnseignementSurAP(this, EOTypeChargeEnseignement.typePedagogique(editingContext()), persIdCreation());
    	}
    	
    	return chargeEnseignement;
    }

    /**
     * Valide typeAP
     * 
     * @param typeAP
     * @throws ValidationException
     */
    public Object validateTypeAP(EOTypeAP typeAP) throws ValidationException {
    	if (typeAP == null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_AP.typeAP.NullPropertyException");
    	}
		
		return typeAP;
    }
    
    /**
     * TODO : ne fonctionne pas pour l'instant car on essaye de supprimer une charge d'enseignement a inserer et donc marque comme insert
     * a voir plus tard
     * 
     * Valide chargeEnseignements
     * 
     * @param chargeEnseignements
     * @throws ValidationException
     */
    /*public Object validateChargeEnseignements(NSArray<EOChargeEnseignement> chargeEnseignements) throws ValidationException {
    	NSMutableArray<EOChargeEnseignement> chargeEnseignementsTemp = chargeEnseignements.mutableClone();
    	
    	for(EOChargeEnseignement ce : chargeEnseignementsTemp) {
    		boolean supprime = ((IChargeEnseignement) ce).supprimerSiSansValeurs();

    		if(supprime) {
    			chargeEnseignements.remove(ce);
    		}
    	}

		return chargeEnseignements;
    }*/
    
    @Override
    public String libelleTreeTable() {
    	return typeAP().code() + " - " + libelle();
    }

}
