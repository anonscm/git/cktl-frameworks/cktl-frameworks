package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette inteface représente un AE.
 * 
 * @author Pascal MACOUIN
 */
public interface IAE extends IComposant {
	/**
	 * @return base de la note (ex: sur 20, sur 10)
	 */
	Integer baseNote();

	/**
	 * @param value base de la note (ex: sur 20, sur 10)
	 */
	void setBaseNote(Integer value);

	/**
	 * @return Défini si l'évaluation de cette AE se fait sans note
	 */
	Integer sansNote();
	
	/**
	 * @return Défini si l'évaluation de cette AE comprend une épreuve
	 */
	Integer epreuve();
	
	/**
	 * @return Le type d'AE
	 */
	ITypeAE typeAE();

	/**
	 * @return La durée minimale de l'épreuve (en minutes)
	 */
	Integer dureeMin();

	/**
	 * @param duree Une durée minimale de l'épreuve (en minutes)
	 */
	void setDureeMin(Integer duree);

	/**
	 * @return La durée maximale de l'épreuve (en minutes)
	 */
	Integer dureeMax();

	/**
	 * @param duree Une durée maximale de l'épreuve (en minutes)
	 */
	void setDureeMax(Integer duree);
}
