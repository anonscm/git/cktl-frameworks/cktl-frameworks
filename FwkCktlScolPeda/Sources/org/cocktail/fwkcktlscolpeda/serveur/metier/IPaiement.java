package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.List;

import com.webobjects.foundation.NSTimestamp;

/**
 * Super interface pour un paiement qu'il soit SCO_SCOLARITE ou SCO_PRE_SCOLARITE.
 * 
 * @author Pascal MACOUIN
 */
public interface IPaiement {

    /**
     * @return l'id technique
     */
	Integer id();

	/**
	 * @return infos de l'année pour l'étudiant qui est à l'origine du paiement
	 */
	IEtudiantAnnee toEtudiantAnnee();

	/**
	 * @param unEtudiantAnnee L'étudiant qui est à l'origine du paiement
	 */
	void setToEtudiantAnneeRelationship(IEtudiantAnnee unEtudiantAnnee);
	
	/**
	 * @return Le numéro d'ordre du paiement pour cet étudiant année (de 1 à n).
	 */
	Integer ordre();

	/**
	 * @param value Un numéro d'ordre du paiement pour cet étudiant année (de 1 à n).
	 */
	void setOrdre(Integer value);
	
	/**
	 * @return true si le paiement est valide, false sinon
	 */
	Boolean paiementValide();

	/**
	 * @param value true si le paiement est valide, false sinon
	 */
	void setPaiementValide(Boolean value);

	/**
	 * @return date du paiement
	 */
	NSTimestamp datePaiement();

	/**
	 * @param value date du paiement
	 */
	void setDatePaiement(NSTimestamp value);
	
	/**
	 * @return le mode de paiement, peut-être simple ou combinés (chèque + prel)
	 */
	IModePaiement modePaiement();
	
	/**
	 * @return le type de paiement
	 */
	TypePaiement typePaiement();
	
	/**
	 * @return vrai si un des moyen de paiement est le prélèvement
	 */
	boolean hasPaiementMoyenPrelevement();
	
	/**
	 * @return vrai si plusieurs moyens de paiements son utilisés
	 */
	boolean hasPlusieursPaiementMoyens();
	
	/**
	 * @return le montant total du paiement
	 */
	BigDecimal montant();
	
	/**
	 * @param value le montant total du paiement
	 */
	void setMontant(BigDecimal value);
	
	/**
	 * @return Le montant total dû (le "à payer" moins le "à rembourser")
	 */
	BigDecimal montantTotalDu();
	
	/**
	 * @return la liste des détails du paiement
	 */
	List<? extends IPaiementDetail> toPaiementDetails();

	/**
	 * @return les articles détail du paiement triés avec le tri par défaut
	 */
	List<? extends IPaiementDetail> toPaiementDetailsTries();
	
	/**
	 * @return détail du paiement relatifs aux formations
	 */
	List<? extends IPaiementDetail> toPaiementDetailFormations();

	/**
	 * @return détail du paiement relatifs aux diplomes
	 */
	List<? extends IPaiementDetail> toPaiementDetailDiplomes();
	
	/**
	 * @return détail du paiement relatifs aux articles complémentaires
	 */
	List<? extends IPaiementDetail> toPaiementDetailArticlesComplementaires();
	
	/**
	 * @return détail du paiement relatifs aux articles générés automatiquement
	 */
	List<? extends IPaiementDetail> toPaiementDetailArticlesAuto();
	
	/**
	 * @return détail du paiement relatifs aux articles générés manuellement
	 */
	List<? extends IPaiementDetail> toPaiementDetailArticlesManuel();
	
	/**
	 * @return moyens de payement 
	 */
	List<? extends IPaiementMoyen> toPaiementMoyens();	
	
	/**
	 * @return moyens de payement triés par ordre d'affichage des modes de paiement
	 */
	List<? extends IPaiementMoyen> toPaiementMoyensTries();
	
	/**
	 * @return Le moyen de paiement de prélèvement
	 */
	IPaiementMoyen toPaiementMoyenPrelevement();
	
	/**
	 * @return Le moyen de paiement de la première échéance
	 */
	IPaiementMoyen toPaiementMoyenPremiereEcheance();

	/**
	 * Retourne <code>true</code> si cet étudiant a des données le concernant dans la GFC.
	 * <p>
	 * C'est le cas si le paiement à un bordereau ou si un des moyen de paiement a un échéancier SEPA.
	 * 
	 * @return <code>true</code> si cet étudiant a des données le concernant dans la GFC
	 */
	boolean aUnLienAvecGfc();
	
    /**
     * @return Le bordereau associé à ce paiement
     */
	IBordereau toBordereau();
	
	/**
	 * Retourne <code>true</code> si c'est le paiement initial (le premier).
	 * 
	 * @return <code>true</code> si c'est le paiement initial
	 */
	boolean isInitial();
	
	/**
	 * Retourne <code>true</code> si c'est le dernier paiement (par ordre).
	 * 
	 * @return <code>true</code> si c'est le dernier paiement
	 */
	boolean isDernier();
	
	// ***************
	// Données d'audit
	// ***************
	/**
	 * @return date de creation de l'enregistrement
	 */
	NSTimestamp dCreation();

	/**
	 * @param value date de creation de l'enregistrement
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return date de modification du paiement
	 */
	NSTimestamp dModification();

	/**
	 * @param value date de modification du paiement
	 */
	void setDModification(NSTimestamp value);

	/**
	 * @return personne qui a créé l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return personne qui a modifié l'enregistrement
	 */
	Integer persIdModification();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdModification(Integer value);
}
