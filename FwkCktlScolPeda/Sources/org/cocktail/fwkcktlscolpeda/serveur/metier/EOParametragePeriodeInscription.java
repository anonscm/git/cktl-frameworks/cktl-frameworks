package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Parametrage des periodes d'ouverture et de fermeture des campagnes d'inscription
 */
public class EOParametragePeriodeInscription extends _EOParametragePeriodeInscription implements IParametragePeriodeInscription {
  private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParametragePeriodeInscription.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public static final String CODE_ETUDIANT = "ETU";
	public static final String CODE_GESTIONNAIRE = "GEST";

	
	/**
	 * 
	 * @param editingContext
	 * @param profil
	 * @return parametragePeriodeInscription
	 */
	public static EOParametragePeriodeInscription creerParametragePeriodeInscription(EOEditingContext editingContext, String profil) {
		EOParametragePeriodeInscription parametragePeriodeInscription = (EOParametragePeriodeInscription) EOUtilities.createAndInsertInstance(editingContext, _EOParametragePeriodeInscription.ENTITY_NAME);
		parametragePeriodeInscription.setProfil(profil);
		
		return parametragePeriodeInscription;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setDateFermeture(Date value) {
		super.setDateFermeture((NSTimestamp) value);

	}

	/**
	 * {@inheritDoc}
	 */
	public void setDateOuverture(Date value) {
		super.setDateOuverture((NSTimestamp) value);

	}
	/**
	 * {@inheritDoc}
	 */
	public void setTypeFormation1(ITypeFormation typeFormation) {
		setTypeFormationRelationship((EOTypeFormation) typeFormation);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**L
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}
}
