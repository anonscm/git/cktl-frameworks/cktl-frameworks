package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * @author isabelle
 *
 */
public class EOInscription extends _EOInscription implements IScoInscription {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOInscription.class);
	
	
	/**
	 * 
	 * @param edc l'editing context
	 * @param persId du createur
	 * @param annee l'annee universitaire
	 * @return une inscription
	 */
    public static EOInscription creer(EOEditingContext edc, Integer persId, Integer annee) {
    	EOInscription inscription = (EOInscription)EOUtilities.createAndInsertInstance(edc, EOInscription.ENTITY_NAME);
    	inscription.setPersIdCreation(persId);
    	inscription.setPersIdModification(persId);
    	inscription.setDCreation(new NSTimestamp());
    	inscription.setDModification(new NSTimestamp());
    	inscription.setAnnee(annee);
    	inscription.setRedoublement(false);
    	inscription.setReorientation(false);
    	inscription.setPassageConditionnel(false);
    	inscription.setCycleAmenage(false);
    	return inscription;
    }
    
    /**
	 * 
	 * @param regimeInscription le regime d'inscription
	 */
	public void setToRegimeInscriptionRelationship(IRegimeInscription regimeInscription) {
		super.setToRegimeInscriptionRelationship((EORegimeInscription) regimeInscription);
	}

	/**
	 * relation avec l'IEtudiantAnne
	 * @param etudiantAnne l'étudiantAnnee
	 */
	public void setToEtudiantAnneeRelationship(IEtudiantAnnee etudiantAnne) {
		super.setToEtudiantAnneeRelationship((EOEtudiantAnnee) etudiantAnne); 
	}
	
	/**
	 * relation avec l'Etudiant
	 * @param etudiant l'Etudiant
	 */
	public void setToEtudiantRelationship(IEtudiant etudiant) {
		super.setToEtudiantRelationship((EOEtudiant) etudiant);
	}
	
	/**
	 * lien vers le diplome
	 * @param diplome le diplome sur lequel est l'inscription
	 */
	public void setToDiplomeRelationship(IDiplome diplome) {
		super.setToDiplomeRelationship((EODiplome) diplome);
	}
	
	/**
	 * 
	 * @param gradeUniversitaire le grade universitaire sur lequel est l'inscription
	 */
	public void setToGradeUniversitaireRelationship(IGradeUniversitaire gradeUniversitaire) {
		super.setToGradeUniversitaireRelationship((EOGradeUniversitaire) gradeUniversitaire);
	}
	
	/**
	 * 
	 * @param typeInscription le typeInscription sur lequel est l'inscription
	 */

	public void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation typeInscription) {
		super.setToTypeInscriptionFormationRelationship((EOTypeInscriptionFormation) typeInscription);
	}

	/**
	 * {@inheritDoc}
	 */
	public IVersionDiplome versionDiplome() {
		return toDiplome().getVersionDiplome(this.annee());
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer id() {
		return Integer.valueOf(primaryKey());
	}

	public boolean isDetteOuAnticipation() {
		return 
			StringUtils.equals(toTypeInscriptionFormation().codeInscription(), ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_DETTE)
			||
			StringUtils.equals(toTypeInscriptionFormation().codeInscription(), ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ANTICIPATION);
	}
}
