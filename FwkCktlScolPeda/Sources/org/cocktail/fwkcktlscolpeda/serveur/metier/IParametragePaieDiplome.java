package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un article de paiement pour un diplôme.
 * 
 * @author Pascal MACOUIN
 */
public interface IParametragePaieDiplome extends IParametragePaie {

	void setBoursier(Boolean value);

	void setComptabilite(String value);

	void setMontant(java.math.BigDecimal value);

	void setRemboursement(Boolean value);
	
	Integer niveau();

	void setNiveau(Integer value);

	EOTypeMontant toTypeMontant();
	
	void setToTypeMontantRelationship(EOTypeMontant value);
	
	IDiplome toDiplome();

	void setToDiplomeRelationship(IDiplome unDiplome);

	IParcours toParcours();

	void setToParcoursRelationship(IParcours unParcoursAnnee);

}