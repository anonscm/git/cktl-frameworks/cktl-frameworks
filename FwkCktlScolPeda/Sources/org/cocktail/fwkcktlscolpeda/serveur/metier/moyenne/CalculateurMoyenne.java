package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.parboiled.errors.ParsingException;

/**
 * Définit le calcul des moyennes
 */
public interface CalculateurMoyenne {


	/**
	 * calcule la moyenne pour le composant
	 * @param valeurNotes : valeur des notes
	 * @param coefficients : valeur des coefficients
	 * @param baseNotes : valeur des bases notes
	 * @param baseNoteComposant la base note du composant pourlequel on calcule la moyenne
	 * @return BigDecimal : le résultat de la formule
	 * @throws CalculateurMoyenneException exception levée en cas d'erreur dans le calcul de la formule	 * 
	 * @throws ParsingException exception levée en cas d'erreur de parsong de la formule
	 */
	BigDecimal calculerMoyenne(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, Map<String, BigDecimal> baseNotes,
	    BigDecimal baseNoteComposant) throws CalculateurMoyenneException, ParsingException;

	/**
	 * Calculer le résultat d'une formule d'admission en fournissant une liste de valeurs pour les notes, les coefficients, et la moyenne
	 * @param valeurNotes : valeur des notes
	 * @param coefficients : valeur des coefficients
	 * @param moyenne : valeur de la moyenne
	 * @return BigDecimal : le résultat de la formule
	 * @throws CalculateurMoyenneException exception levée en cas d'erreur dans le calcul de la formule
	 * @throws ParsingException exception levée en cas d'erreur de parsong de la formule
	 */
	Boolean calculerResultat(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, BigDecimal moyenne) throws CalculateurMoyenneException,
	    ParsingException;
}