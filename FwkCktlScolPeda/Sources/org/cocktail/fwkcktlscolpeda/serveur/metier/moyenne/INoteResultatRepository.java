package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;

/**
 * repository des types de resultats (nomenclature)
 *
 */
public interface INoteResultatRepository {

	/**
	 * @param code code de la note résultat (admis, dette, ajourné, admis par compensation, jury)
	 * @return la note résultat correpondant à ce code
	 */
	INoteResultat getNoteResultat(String code);
	
}
