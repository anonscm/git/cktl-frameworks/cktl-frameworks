package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlscolpeda.serveur.formules.services.CalculFormuleService;
import org.cocktail.fwkcktlscolpeda.serveur.formules.services.CalculFormuleServiceParboiled;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.EONoteService;
import org.parboiled.errors.ParsingException;

/**
 * Calcul d'un moyenne avec formule de type (c1*n1 + c2*n2 )
 */
public class CalculateurMoyenneFormule extends CalculateurMoyenneAbstract {


	// Private fields
	private String formuleInput;
	private List<String> notesLitterals = null;
	private List<String> coefsLitterals = null;

	private CalculateurFormuleHelper calculateurFormuleHelper;

	// Constructor
	/**
	 * @param formuleInput : formule donnee en entree
	 */
	public CalculateurMoyenneFormule(String formuleInput) {
		setFormuleInput(traduit(formuleInput));
		setCalculateurFormuleHelper(new CalculateurFormuleHelper(new EONoteService()));
	}

	// Properties
	public CalculateurFormuleHelper getCalculateurFormuleHelper() {
		return calculateurFormuleHelper;
	}

	public void setCalculateurFormuleHelper(CalculateurFormuleHelper calculateurFormuleHelper) {
		this.calculateurFormuleHelper = calculateurFormuleHelper;
	}

	public String getFormuleInput() {
		return formuleInput;
	}

	/**
	 * @param formuleInput texte de la formule
	 */
	public void setFormuleInput(String formuleInput) {
		this.formuleInput = formuleInput;
		extractNotesLitterals();
		extractCoefsLitterals();
	}

	public List<String> getNotesLitterals() {
		return notesLitterals;
	}

	public List<String> getCoefsLitterals() {
		return coefsLitterals;
	}


	/**
	 * {@inheritDoc}
	 */
	public BigDecimal calculerMoyenne(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, Map<String, BigDecimal> baseNotes,
	    BigDecimal baseNoteComposant) throws CalculateurMoyenneException,
	    ParsingException {
		CalculFormuleService calculFormuleService = new CalculFormuleServiceParboiled();
		BigDecimal valeur = calculFormuleService.calculer(traduireFormuleValeur(valeurNotes, coefficients, null));
		return valeur;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean calculerResultat(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, BigDecimal moyenne)
	    throws CalculateurMoyenneException, ParsingException {
		CalculFormuleService calculFormuleService = new CalculFormuleServiceParboiled();
		BigDecimal valeur = calculFormuleService.calculer(traduireFormuleValeur(valeurNotes, coefficients, moyenne));
		Boolean resultat = null;
		if (BigDecimal.ONE.compareTo(valeur) == 0) {
			resultat = Boolean.TRUE;
		} else if (BigDecimal.ZERO.compareTo(valeur) == 0) {
			resultat = Boolean.FALSE;
		}
		return resultat;
	}

	/**
	 * Calculer la moyenne
	 * @param notes : les notes
	 * @param coefficients : les coefficients
	 * @param noteMoyenne : la note moyenne
	 * @return BigDecimal : le résultat du calcul
	 * @throws Exception
	 */
	BigDecimal calculerMoyenne(Map<String, INote> notes, Map<String, BigDecimal> coefficients, INote noteMoyenne) throws CalculateurMoyenneException,
	    ParsingException {
		CalculFormuleService calculFormuleService = new CalculFormuleServiceParboiled();
		BigDecimal moyenne = calculFormuleService.calculer(traduireFormule(notes, coefficients, noteMoyenne));
		return moyenne.setScale(2, RoundingMode.CEILING);
	}

	// Private Methods
	private String traduit(String formuleInput) {
		formuleInput = formuleInput.replaceAll("(?i)min", "MIN");
		formuleInput = formuleInput.replaceAll("(?i)max", "MAX");
		formuleInput = formuleInput.replaceAll("(?i)et", "&");
		formuleInput = formuleInput.replaceAll("(?i)ou", "|");
		formuleInput = formuleInput.replaceAll("(?i)>=", "S");
		formuleInput = formuleInput.replaceAll("(?i)<=", "I");
		formuleInput = formuleInput.replaceAll("(?i)!=", "!");
		return formuleInput;
	}

	private String traduireFormule(Map<String, INote> notes, Map<String, BigDecimal> coefficients, INote noteMoyenne) throws CalculateurMoyenneException {
		Map<String, BigDecimal> valeurNotes = new HashMap<String, BigDecimal>();
		for (Entry<String, INote> note : notes.entrySet()) {
			valeurNotes.put(note.getKey(), note.getValue().noteInitiale());
		}

		BigDecimal valeurNoteMoyenne = null;
		if (noteMoyenne != null) {
			valeurNoteMoyenne = noteMoyenne.noteInitiale();
		}

		return traduireFormuleValeur(valeurNotes, coefficients, valeurNoteMoyenne);
	}

	private String traduireFormuleValeur(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, BigDecimal moyenne)
	    throws CalculateurMoyenneException {
		checkMoyennePresentes(moyenne);
		checkValeursPresentes(getNotesLitterals(), valeurNotes, CalculateurMoyenneException.NOTE_VALEUR_ABSENTE_EXCEPTION);
		checkValeursPresentes(getCoefsLitterals(), coefficients, CalculateurMoyenneException.COEF_VALEUR_ABSENTE_EXCEPTION);

		String formule = formuleInput;

		if (CalculateurFormuleHelper.formuleContientOperandeMoyenne(formule)) {
			formule = formule.replace(CalculateurFormuleHelper.PREFIX_MOYENNE_DEFAULT, moyenne.toString());
		}

		for (String noteLitteral : getNotesLitterals()) {
			BigDecimal note = valeurNotes.get(noteLitteral);
			formule = formule.replace(noteLitteral, note.toString());
		}

		for (String coefLitteral : getCoefsLitterals()) {
			BigDecimal coef = coefficients.get(coefLitteral);
			formule = formule.replace(coefLitteral, coef.toString());
		}

		return formule;

	}

	private void checkMoyennePresentes(BigDecimal moyenne) throws CalculateurMoyenneException {
		if (CalculateurFormuleHelper.formuleContientOperandeMoyenne(getFormuleInput()) && moyenne == null) {
			String message = String.format(CalculateurMoyenneException.MOYENNE_VALEUR_ABSENTE_EXCEPTION, getFormuleInput());
			throw new CalculateurMoyenneException(message);
		}
	}

	private void checkValeursPresentes(List<String> listeLitterals, Map<String, BigDecimal> listeValeurs, String templateMessage)
	    throws CalculateurMoyenneException {
		if (listeLitterals != null) {
			for (String litteral : listeLitterals) {
				if (!listeValeurs.containsKey(litteral)) {
					String message = String.format(templateMessage, litteral, getFormuleInput());
					throw new CalculateurMoyenneException(message);
				}
			}
		}
	}

	private void extractNotesLitterals() {
		notesLitterals = new ArrayList<String>();
		if (formuleInput != null) {
			Matcher matcher = Pattern.compile("n[0-9]+").matcher(formuleInput);
			while (matcher.find()) {
				notesLitterals.add(matcher.group());
			}
		}

		notesLitterals = removeDuplicates(notesLitterals);
	}

	private void extractCoefsLitterals() {
		coefsLitterals = new ArrayList<String>();
		if (formuleInput != null) {
			Matcher matcher = Pattern.compile("c[0-9]+").matcher(formuleInput);
			while (matcher.find()) {
				coefsLitterals.add(matcher.group());
			}
		}

		coefsLitterals = removeDuplicates(coefsLitterals);
	}
}
