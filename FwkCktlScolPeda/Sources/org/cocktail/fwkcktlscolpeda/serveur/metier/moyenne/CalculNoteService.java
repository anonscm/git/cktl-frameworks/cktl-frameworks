package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.EONoteService;
import org.parboiled.errors.ParsingException;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Calcule des notes et des moyennes sur un composant
 */
public class CalculNoteService {

	private EOEditingContext editingContext;
	private CalculateurFormuleHelper calculateurFormuleHelper;
	private CalculResultatService calculResultatService;

	public static final Logger LOG = Logger.getLogger(CalculNoteService.class);

	/**
	 * @param editingContext contexte d'édition
	 */
	public CalculNoteService(EOEditingContext editingContext) {
		this.editingContext = editingContext;
		setCalculResultatService(new CalculResultatService(new NoteResultatRepositoryEOF(editingContext)));
		setCalculateurFormuleHelper(new CalculateurFormuleHelper(new EONoteService()));
	}

	CalculateurFormuleHelper getCalculateurFormuleHelper() {
		return calculateurFormuleHelper;
	}

	void setCalculateurFormuleHelper(CalculateurFormuleHelper calculateurFormuleHelper) {
		this.calculateurFormuleHelper = calculateurFormuleHelper;
	}

	CalculResultatService getCalculResultatService() {
		return calculResultatService;
	}

	void setCalculResultatService(CalculResultatService calculResultatService) {
		this.calculResultatService = calculResultatService;
	}

	/**
	 * Retourne le calculateur de formule necessaire au composant child du lien
	 * @param lien : le lien pour lequel on va rechercher un calculateur
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @return CalculateurMoyenneAbstract : un calculateur de moyenne
	 */
	public CalculateurMoyenneAbstract getCalculateur(ILien lien, Integer numeroSession) {
		IComposant composant = lien.child();
		IFormule formule = getCalculateurFormuleHelper().getFormule(numeroSession, composant, ITypeFormule.TYPEMOYENNE_LBL);
		if (formule != null && formule.moyennePonderee() != null && formule.moyennePonderee() == 0 && formule.formule() != null) {
			return new CalculateurMoyenneFormule(formule.formule());
		}

		return new CalculateurMoyennePonderee();
	}

	/**
	 * Calcule la moyenne pour une inscription, pour un lien et sur une session
	 * @param inscription : inscription administrative
	 * @param lien : lien pour le child duquel on veut calculer la moyenne
	 * @param numeroSession : le numéro de la session (peut être null)
	 */
	public void calculerNotesComposantEtFils(IScoInscription inscription, ILien lien, Integer numeroSession) {
		for (ILien lienfils : lien.child().liensParentsWithoutDuplicate()) {
			if (!(lienfils.child() instanceof IAP) && !(lienfils.child() instanceof IAE)) {
				calculerNotesComposantEtFils(inscription, lienfils, numeroSession);
			}
		}

		try {
			calculer(inscription, lien, numeroSession);
		} catch (ParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CalculateurMoyenneException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Calcule la moyenne pour une inscription, pour un lien et sur une session
	 * @param inscription : inscription administrative
	 * @param lien : lien pour le child duquel on veut calculer la moyenne
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @throws CalculateurMoyenneException
	 * @throws ParsingException
	 */
	private void calculer(IScoInscription inscription, ILien lien, Integer numeroSession) throws ParsingException, CalculateurMoyenneException {
		CalculateurMoyenneAbstract calculateur = getCalculateur(lien, numeroSession);
		INote note = calculateur.getNote(inscription, lien, numeroSession, ITypeNote.NOTE_CALCULEE);
		IInscriptionPedagogiqueElement inscriptionPeda = calculateur.getInscriptionPedagogiqueElement(inscription, lien);

		// Vérifier que au moins un des composants childs a une note sur cette session
		Boolean hasToBeCalculated = false;
		for (ILien lienFils : lien.child().liensParentsWithoutDuplicate()) {
			if (calculateur.getNoteInitiale(inscription, lienFils, numeroSession) != null) {
				hasToBeCalculated = true;
			}
		}

		if (hasToBeCalculated) {
			Map<String, BigDecimal> notes = getCalculateurFormuleHelper().getNotesValeurs(inscription, lien, numeroSession);
			Map<String, BigDecimal> coefficients = getCalculateurFormuleHelper().getCoefficients(lien.child());
			Map<String, BigDecimal> baseNotes = getCalculateurFormuleHelper().getBaseNotes(lien.child());
			BigDecimal baseNoteComposant = getCalculateurFormuleHelper().getBaseNote(lien.child());
			BigDecimal moyenne = calculateur.calculerMoyenne(notes, coefficients, baseNotes, baseNoteComposant);

			if (moyenne != null) {
				if (note == null) {
					note = EONote.creerNote(editingContext, (EOInscriptionPedagogiqueElement) inscriptionPeda, EOTypeNote.typeCalculee(editingContext), numeroSession);
				}
				note.setNoteInitiale(moyenne);
				INoteResultat resultat;
				resultat = getCalculResultatService().calculerResultat(notes, coefficients, inscription, lien, moyenne, numeroSession);
				note.setToNoteResultatRelationship(resultat);
			} else {
				if (note != null) {
					inscriptionPeda.removeFromToNotesRelationship(note);
					if (note instanceof EONote) {
						editingContext.deleteObject((EONote) note);
					}
				}
			}
		}
		else {
			if (note != null) {
				inscriptionPeda.removeFromToNotesRelationship(note);
				if (note instanceof EONote) {
					editingContext.deleteObject((EONote) note);
				}
			}
		}

	}
}
