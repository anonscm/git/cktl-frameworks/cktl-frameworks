package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

/**
 * Exception remontees par le caclulateur de moyenne
 *
 */
public class CalculateurMoyenneException extends Exception {
  private static final long serialVersionUID = 1L;
  public static final String NOTE_NULLE_EXCEPTION = "Note avec valeur nulle.";
  public static final String ERRORS_FORMULE_EXCEPTION = "Erreur dans la formule.";
  public static final String DIVISION_ZERO_EXCEPTION = "Division par zéro interdite.";
  public static final String MOYENNE_VALEUR_ABSENTE_EXCEPTION = "La moyenne n'a pas pu être calculée pour la formule '%s'.";
  public static final String NOTE_VALEUR_ABSENTE_EXCEPTION = "La valeur pour la note '%s' n'est pas renseignée pour la formule '%s'.";
  public static final String COEF_VALEUR_ABSENTE_EXCEPTION = "La valeur pour le coefficient '%s' n'est pas renseignée pour la formule '%s'.";
  public static final String AUTRE_ERREUR = "Autre erreur dans le calcul de la formule"; 
  /**
   * @param message message renvoye en cas d'erreur
   */
  public CalculateurMoyenneException(String message) {
     super(message);
  }

}
