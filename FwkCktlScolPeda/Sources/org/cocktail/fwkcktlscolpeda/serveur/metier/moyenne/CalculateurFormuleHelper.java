package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.EONoteService;

/**
 * Helper pour les calculateurs de formules utilisés dans {@link CalculNoteService} et {@link CalculResultatService}
 */
public class CalculateurFormuleHelper {
	public static final Integer BASE_NOTE_DEFAUT = 20;
	static final BigDecimal COEFFICIENT_DEFAULT = new BigDecimal(1);
	public static final String PREFIX_NOTE_DEFAULT = "n";
	public static final String PREFIX_COEFF_DEFAULT = "c";
	public static final String PREFIX_BASE_DEFAULT = "b";
	public static final String PREFIX_MOYENNE_DEFAULT = "moyenne";

	private EONoteService noteService;

	/**
	 * @param ordre numéro de la clé correspond à l'ordre du composant
	 * @return la clé associé à la note
	 */
	public static String getCleNote(Integer ordre) {
		String cleNote = PREFIX_NOTE_DEFAULT + ordre;
		return cleNote;
	}

	/**
	 * @param ordre numéro de la clé correspond à l'ordre du composant
	 * @return la clé associé au coefficient
	 */
	public static String getCleCoefficient(Integer ordre) {
		String cleCoef = PREFIX_COEFF_DEFAULT + ordre;
		return cleCoef;
	}

	/**
	 * @param ordre numéro de la clé correspond à l'ordre du composant
	 * @return la clé associé à la base note
	 */
	public static String getCleBaseNote(Integer ordre) {
		String cleCoef = PREFIX_BASE_DEFAULT + ordre;
		return cleCoef;
	}

	/**
	 * @param formule formule analysée
	 * @return <code>true</code> si la formule contient l'opérande moyenne
	 */
	public static boolean formuleContientOperandeMoyenne(String formule) {
		return formule == null ? false : formule.contains(PREFIX_MOYENNE_DEFAULT);
	}

	/**
	 * @param noteService service de recuperation des notes
	 */
	public CalculateurFormuleHelper(EONoteService noteService) {
		this.noteService = noteService;
	}

	Map<String, BigDecimal> getNotesValeurs(IScoInscription inscription, ILien lien, Integer noSession) {
		Map<String, BigDecimal> noteValeurs = new HashMap<String, BigDecimal>();
		Map<String, INote> notes = getNotes(inscription, lien, noSession);
		for (String cle : notes.keySet()) {
			BigDecimal noteInitiale = null;
			if (notes.get(cle) != null) {
				noteInitiale = notes.get(cle).noteInitiale();
			}
			noteValeurs.put(cle, noteInitiale);
		}
		return noteValeurs;
	}

	Map<String, INote> getNotes(IScoInscription inscription, ILien lien, Integer noSession) {
		Map<String, INote> notes = new HashMap<String, INote>();
		IComposant composant = lien.child();
		for (ILienComposer lienComposer : composant.liensComposerAvecLesComposantsEnfants()) {
			INote note = noteService.getDerniereNoteInitialeExistante(inscription, lienComposer, noSession);
			notes.put(getCleNote(lienComposer.ordre()), note);
		}
		return notes;
	}

	Map<String, BigDecimal> getCoefficients(IComposant composant) {
		Map<String, BigDecimal> coefficients = new HashMap<String, BigDecimal>();
		for (ILienComposer lienComposer : getLiensEnfants(composant)) {
			coefficients.put(getCleCoefficient(lienComposer.ordre()), getCoefficient(lienComposer));
		}
		return coefficients;
	}

	private BigDecimal getCoefficient(ILienComposer lienComposer) {
		if (lienComposer.coefficient() != null) {
			return lienComposer.coefficient();
		}
		return COEFFICIENT_DEFAULT;
	}

	Map<String, BigDecimal> getBaseNotes(IComposant composant) {
		Map<String, BigDecimal> baseNotes = new HashMap<String, BigDecimal>();
		for (ILienComposer lienComposer : getLiensEnfants(composant)) {
			baseNotes.put(getCleBaseNote(lienComposer.ordre()), getBaseNote(composant));
		}
		return baseNotes;
	}

	IFormule getFormule(Integer numeroSession, IComposant composant, String typeFormule) {
		ISessionComposant session = composant.getSession(numeroSession);
		if (session != null) {
			return session.getFormule(typeFormule);
		}
		return null;
	}

	private List<ILienComposer> getLiensEnfants(IComposant composant) {
		List<ILienComposer> liens = new ArrayList<ILienComposer>();
		for (ILienComposer lien : composant.liensComposerAvecLesComposantsEnfants()) {
			if (!(lien.child() instanceof IAP)) {
				liens.add(lien);
			}
		}
		return liens;
	}

	/**
	 * @param composant composant (UE, EC, ...)
	 * @return la base note du composant
	 */
	public BigDecimal getBaseNote(IComposant composant) {

		if (composant instanceof IAE && ((IAE) composant).baseNote() != null) {
			return new BigDecimal(((IAE) composant).baseNote());
		}
		if (composant.creditable() != null && composant.creditable().baseNote() != null) {
			return new BigDecimal(composant.creditable().baseNote());
		}

		return new BigDecimal(BASE_NOTE_DEFAUT);

	}

}