package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.EONoteService;
import org.parboiled.errors.ParsingException;

import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;


/**
 * Classe de base des calculs de moyennes
 */
public abstract class CalculateurMoyenneAbstract implements CalculateurMoyenne {	
	private EONoteService noteService;
	
	
	/**
	 * @return  Service de manipulation des notes
	 */
	public EONoteService getNoteService() {
		if (noteService == null) {
			noteService = new EONoteService();
		}
		
		return noteService;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public  abstract Boolean calculerResultat(Map<String, BigDecimal> notes, Map<String, BigDecimal> coefficients, BigDecimal moyenne) throws CalculateurMoyenneException, ParsingException;;
	
	protected INote getNote(IScoInscription inscription, ILien lien, Integer numeroSession, String typeNoteCode) {
		return getNoteService().getNote(inscription, lien, numeroSession, typeNoteCode);
	}
	
	protected INote getNoteInitiale(IScoInscription inscription, ILien lien, Integer numeroSession) {
		return getNoteService().getNoteInitiale(inscription, lien, numeroSession);
	}

	protected BigDecimal getNoteInitialeValue(IScoInscription inscription, ILien lien, Integer numeroSession) {
		return getNoteService().getNoteInitialeValue(inscription, lien, numeroSession);
	}
	
	protected INote getDerniereNoteInitialeExistante(IScoInscription inscription, ILien lien, Integer numeroSession) {
		return getNoteService().getDerniereNoteInitialeExistante(inscription, lien, numeroSession);
	}

	protected BigDecimal getDerniereNoteInitialeExistanteValue(IScoInscription inscription, ILien lien, Integer numeroSession) {
		return getNoteService().getDerniereNoteInitialeExistanteValue(inscription, lien, numeroSession);
	}

	protected IInscriptionPedagogiqueElement getInscriptionPedagogiqueElement(IScoInscription inscription, ILien lien) {
		return getNoteService().getInscriptionPedagogiqueElement(inscription, lien);
	}
	
	protected boolean isComposantSansNote(IComposant composant) {
		if (composant.creditable() != null) {
			return composant.creditable().sansNote() != null && composant.creditable().sansNote().booleanValue();
		}
		return false;
	}

	protected boolean canBeAdded(IScoInscription scoInscription, Integer numeroSession, ILien lienEnfant) {
		return !(lienEnfant.child() instanceof IAP) 
		    && !(lienEnfant.child().creditable() != null && lienEnfant.child().creditable().sansNote() != null && lienEnfant.child().creditable().sansNote().booleanValue());
	}

	
	protected List<ILien> getSortedLiens(IComposant composant) {
		List<ILien> liens = new ArrayList<ILien>();
		for (ILien lien : composant.liensParentsWithoutDuplicate()) {
			if (!(lien.child() instanceof IAP)) {
				liens.add(lien);
			}
		}
		Collections.sort(liens, new Comparator<ILien>() {
			public int compare(ILien i1, ILien i2) {
				return (i1.ordre() - i2.ordre());
			}
		});

		return liens;
	}
	
	protected List<String> removeDuplicates(List<String> liste) {
		NSMutableArray<String> result = new NSMutableArray<String>();
        NSMutableSet<String> already = new NSMutableSet<String>();
        for(String object : liste) {
            if(!already.containsObject(object)){
                already.addObject(object);
                result.addObject(object);
            }
        }
        return result;
	}

	

}
