package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.parboiled.errors.ParsingException;

/**
 * Calcul de base de la moyenne (moyenne pondérée)
 */
public class CalculateurMoyennePonderee extends CalculateurMoyenneAbstract {

	public static final Logger LOG = Logger.getLogger(CalculateurMoyennePonderee.class);

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal calculerMoyenne(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, Map<String, BigDecimal> baseNotes,
	    BigDecimal baseNoteComposant) throws CalculateurMoyenneException, ParsingException {
		BigDecimal moyenne = calculer(valeurNotes, coefficients, baseNotes, baseNoteComposant);
		if (moyenne != null) {
			return moyenne.setScale(2, RoundingMode.CEILING);
		} else {
			return null;
		}
	}

	boolean peutCalculerMemeSiManqueUneNote() {
		return FwkCktlScolPeda.getParamManager().getCalculMoyennePondereeSiManqueNote();
	}

	private BigDecimal calculer(Map<String, BigDecimal> valeurNotes, Map<String, BigDecimal> coefficients, Map<String, BigDecimal> basesNotes,
	    BigDecimal baseNoteComposant) throws CalculateurMoyenneException {
		BigDecimal moyenneSomme = new BigDecimal(0);
		BigDecimal baseNoteSomme = new BigDecimal(0);

		for (Integer i = 0; i < valeurNotes.size(); i++) {
			BigDecimal note = valeurNotes.get(CalculateurFormuleHelper.getCleNote(i + 1));
			
			BigDecimal coefficient = getCoefficient(coefficients, i);
			BigDecimal baseNote = getbaseNote(basesNotes, i);

			if (note == null && !peutCalculerMemeSiManqueUneNote()) {
				throw new CalculateurMoyenneException(CalculateurMoyenneException.NOTE_NULLE_EXCEPTION);
			}

			if (note != null && coefficient != null) {
				moyenneSomme = moyenneSomme.add(coefficient.multiply(note));
				baseNoteSomme = baseNoteSomme.add(coefficient.multiply(baseNote));
			}

		}

		if (baseNoteSomme.intValue() > 0) {
			moyenneSomme = moyenneSomme.divide(baseNoteSomme, 5, RoundingMode.CEILING);
			moyenneSomme = moyenneSomme.multiply(baseNoteComposant);
			return moyenneSomme.setScale(2, RoundingMode.CEILING);
		}

		return null;
	}

	private BigDecimal getbaseNote(Map<String, BigDecimal> basesNotes, Integer ordre) {
	  BigDecimal baseNote = basesNotes.get(CalculateurFormuleHelper.getCleBaseNote(ordre + 1));
	  if (baseNote == null) {
	  	baseNote = new BigDecimal(CalculateurFormuleHelper.BASE_NOTE_DEFAUT);
	  }
	  return baseNote;
  }

	private BigDecimal getCoefficient(Map<String, BigDecimal> coefficients, Integer ordre) {
		BigDecimal coefficient = coefficients.get(CalculateurFormuleHelper.getCleCoefficient(ordre + 1));
		if (coefficient == null) {
			coefficient = CalculateurFormuleHelper.COEFFICIENT_DEFAULT;
		}
		return coefficient;
	}

	@Override
	public Boolean calculerResultat(Map<String, BigDecimal> notes, Map<String, BigDecimal> coefficients, BigDecimal moyenne) throws CalculateurMoyenneException,
	    ParsingException {
		return null;
	}
}
