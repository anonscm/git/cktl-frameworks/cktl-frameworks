package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * {@inheritDoc}
 */
public class NoteResultatRepositoryEOF implements INoteResultatRepository {

	private static ERXEnterpriseObjectCache<EONoteResultat> noteResultatCache;
	private EOEditingContext editingContext;
	
	/**
	 * @param editingContext contexte d'édition
	 */
	public NoteResultatRepositoryEOF(EOEditingContext editingContext) {
		super();
		this.editingContext = editingContext;
	}

	/**
	 * @return le cache des {@link EONoteResultat} par leur code
	 */
	private static ERXEnterpriseObjectCache<EONoteResultat> getNoteResultatCache() {
		if (noteResultatCache == null) {
			noteResultatCache = new ERXEnterpriseObjectCache<EONoteResultat>(EONoteResultat.class, EONoteResultat.CODE_KEY);
		}
		return noteResultatCache;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public INoteResultat getNoteResultat(String code) {
		EONoteResultat noteResultat = getNoteResultatCache().objectForKey(editingContext, code);
		return noteResultat;
	}

}
