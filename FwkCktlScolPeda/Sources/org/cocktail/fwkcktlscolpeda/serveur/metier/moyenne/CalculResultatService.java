package org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INoteResultat;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.note.services.EONoteService;
import org.parboiled.errors.ParsingException;

/**
 * Service de calcul du résultat
 */
public class CalculResultatService {

	public static final Logger LOG = Logger.getLogger(CalculNoteService.class);

	private INoteResultatRepository noteResultatRepository;
	private CalculateurFormuleHelper calculateurFormuleHelper;

	/**
	 * @param noteResultatRepository repository contenant les types de résultats
	 */
	public CalculResultatService(INoteResultatRepository noteResultatRepository) {
		this.noteResultatRepository = noteResultatRepository;
		setCalculateurFormuleHelper(new CalculateurFormuleHelper(new EONoteService()));
	}

	CalculateurFormuleHelper getCalculateurFormuleHelper() {
		return calculateurFormuleHelper;
	}

	void setCalculateurFormuleHelper(CalculateurFormuleHelper helper) {
		this.calculateurFormuleHelper = helper;
	}

	/**
	 * @param notes les notes pour le composant dont on calcul le résultat
	 * @param coefficients les coefficient du composant
	 * @param inscription une inscription
	 * @param lien le lien surlequel on veut calculer le résultat
	 * @param moyenne la moyenne déjà calculée (pour y faire référence dans la formule)
	 * @param noSession le numéro de session
	 * @return un {@link INoteResultat} correspondant au résultat du calcul de la formule d'admission
	 * @throws ParsingException
	 * @throws CalculateurMoyenneException
	 */
	public INoteResultat calculerResultat(Map<String, BigDecimal> notes, Map<String, BigDecimal> coefficients, IScoInscription inscription, ILien lien,
	    BigDecimal moyenne, Integer noSession) {
		IComposant composant = lien.child();

		IFormule formule = null;
		try {
			formule = getCalculateurFormuleHelper().getFormule(noSession, composant, ITypeFormule.TYPEADMISSION_LBL);
			Boolean admission = calculerResultatFormule(formule, notes, coefficients, moyenne);
			if (admission) {
				return noteResultatRepository.getNoteResultat(INoteResultat.ADMIS);
			}

			formule = getCalculateurFormuleHelper().getFormule(noSession, composant, ITypeFormule.TYPECOMPENSATION_LBL);
			Boolean compensation = calculerResultatFormule(formule, notes, coefficients, moyenne);
			if (compensation) {
				return noteResultatRepository.getNoteResultat(INoteResultat.ADMIS_COMPENSATION);
			}

			formule = getCalculateurFormuleHelper().getFormule(noSession, composant, ITypeFormule.TYPEDETTES_LBL);
			Boolean dette = calculerResultatFormule(formule, notes, coefficients, moyenne);
			if (dette) {
				return noteResultatRepository.getNoteResultat(INoteResultat.DETTE);
			}

			return noteResultatRepository.getNoteResultat(INoteResultat.AJOURNE);

		} catch (ParsingException e) {
			logWarningFormule(noSession, composant, formule);
			return null;
		} catch (CalculateurMoyenneException e) {
			logWarningCalcul(inscription, lien, noSession, e);
			return null;
		}

	}

	private Boolean calculerResultatFormule(IFormule formule, Map<String, BigDecimal> notes,
	    Map<String, BigDecimal> coefficients, BigDecimal moyenne) throws ParsingException, CalculateurMoyenneException {
		// TODO fix en attendant d'avoir les calculateurs par defaut pour l'admission, compensation et dette
		String texteFormule = "moyenne > 10";
		if (formule != null && formule.formule() != null) {
			texteFormule = formule.formule();
		}
		CalculateurMoyenneAbstract calculateurMoyenneFormule = new CalculateurMoyenneFormule(texteFormule);
		Boolean result = calculateurMoyenneFormule.calculerResultat(notes, coefficients, moyenne);
		if (result == null) {
			throw new CalculateurMoyenneException(CalculateurMoyenneException.AUTRE_ERREUR);
		}
		return result;
	}

	private void logWarningCalcul(IScoInscription inscription, ILien lien,
	    Integer noSession, Exception e) {
		String message = String.format("Une erreur est survenue lors du calcul du résultat pour inscription=%s, lien=%s, noSession=%s", inscription, lien,
		    noSession);
		LOG.warn(message, e);
	}

	private void logWarningFormule(Integer noSession, IComposant composant,
	    IFormule formuleAdmission) {
		String message = String.format("Calcul de l'admission impossible pour le composant %s, la session %s, la formule %s ",
		    composant, noSession, formuleAdmission);
		LOG.warn(message);
	}

}
