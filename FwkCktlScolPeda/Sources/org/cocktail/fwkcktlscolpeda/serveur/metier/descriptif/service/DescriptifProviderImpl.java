package org.cocktail.fwkcktlscolpeda.serveur.metier.descriptif.service;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeDescriptifDestination;

import com.webobjects.foundation.NSArray;

/**
 * {@inheritDoc}
 */
public class DescriptifProviderImpl implements DescriptifProvider {

	/**
	 * {@inheritDoc}
	 */
	public IDescriptif getDescriptif(IComposant composant, Boolean isHabilitation) {
		String destination = ITypeDescriptifDestination.TYPEPUBLICATION_LBL;
		if (isHabilitation) {
			destination = ITypeDescriptifDestination.TYPEHABILITATION_LBL;
		}
		NSArray<EODescriptif> descriptifs = ((EOComposant) composant).descriptifs(EODescriptif.DESTINATION.dot(EOTypeDescriptifDestination.TYPE).eq(destination));
		if (descriptifs.count() > 0) {
			return descriptifs.get(0);
		}
		return null;
	}

}
