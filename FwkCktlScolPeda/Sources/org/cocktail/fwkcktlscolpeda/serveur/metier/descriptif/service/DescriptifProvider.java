package org.cocktail.fwkcktlscolpeda.serveur.metier.descriptif.service;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;

/**
 *  service de fourniture d'un descriptif (syllabus)
 */
public interface DescriptifProvider {

	/**
	 * @param composant composant pour lequel on cherche un le descriptif
	 * @param isHabilitation <code> true </code> si l'on recherche le descriptif d'habilitation,  <code> false </code>  si l'on recherche le descriptif de publication
	 * @return le descriptif (encore appelé Syllabus) attaché au composant
	 */
	IDescriptif getDescriptif(IComposant composant, Boolean isHabilitation);
}
