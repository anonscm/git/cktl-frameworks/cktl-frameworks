package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEnterpriseObjectCache;
import er.extensions.eof.ERXQ;

/**
 * La classe EOTypeComposant décrit et maintient un type de composant.
 */
@SuppressWarnings("serial")
public class EOTypeComposant extends _EOTypeComposant implements ITypeComposant {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeComposant.class);

	private static ERXEnterpriseObjectCache<EOTypeComposant> typeComposantCache;

	/**
	 * @return le cache des {@link EOTypeComposant} par leur nom
	 */
	private static ERXEnterpriseObjectCache<EOTypeComposant> getTypeComposantCache() {
		if (typeComposantCache == null) {
			typeComposantCache = new ERXEnterpriseObjectCache<EOTypeComposant>(EOTypeComposant.class, NOM_KEY);
		}
		return typeComposantCache;
	}

	/**
	 * @param typeComposantCache
	 *            le cache a setter
	 */
	public static void setTypeComposantCache(ERXEnterpriseObjectCache<EOTypeComposant> typeComposantCache) {
		EOTypeComposant.typeComposantCache = typeComposantCache;
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeDiplome
	 */
	public static EOTypeComposant typeDiplome(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEDIPLOME_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeVersionDiplome
	 */
	public static EOTypeComposant typeVersionDiplome(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEVERSIONDIPLOME_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typePeriode
	 */
	public static EOTypeComposant typePeriode(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEPERIODE_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeParcours
	 */
	public static EOTypeComposant typeParcours(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEPARCOURS_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeRegroupement.
	 */
	public static EOTypeComposant typeRegroupement(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEREGROUPEMENT_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeEC (Element constitutif).
	 */
	public static EOTypeComposant typeEC(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEEC_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeUE (Unité d'enseignement).
	 */
	public static EOTypeComposant typeUE(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEUE_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeAP (Atome pédagogique).
	 */
	public static EOTypeComposant typeAP(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEAP_NOM);
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type typeAE (Atome d'évaluation).
	 */
	public static EOTypeComposant typeAE(EOEditingContext edc) {
		return getTypeComposantCache().objectForKey(edc, ITypeComposant.TYPEAE_NOM);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see er.extensions.eof.ERXGenericRecord#toString()
	 */
	@Override
	public String toString() {
		return this.nom();
	}

	/**
	 * @param editingContext
	 *            :contexte d'édition.
	 * @param typelien
	 *            : type de lien pour lequel on cherche les types de composants enfant autorisés.
	 * @param typeComposant
	 *            : type de composant pour lequel on cherche les types de composants enfant autorisé.
	 * @return liste des types de composants parents autorisé pour un type de composant et type de lien donné.
	 */
	public static NSArray<EOTypeComposant> typesComposantEnfantAutorise(EOEditingContext editingContext, EOTypeComposant typeComposant, EOTypeLien typelien) {
		NSArray<EOTypeComposant> types = new NSMutableArray<EOTypeComposant>();

		EOQualifier qualifierAllowedParent = ERXQ.equals(EORegleLiaison.ALLOWED_PARENT_KEY, typeComposant);
		EOQualifier qualifierTypeLien = ERXQ.equals(EORegleLiaison.TYPE_LIEN_KEY, typelien);
		EOQualifier qualifierForbidden = ERXQ.equals(EORegleLiaison.FORBIDDEN_KEY, 0);
		EOQualifier qualifierVisible = ERXQ.equals(EORegleLiaison.VISIBLE_KEY, 1);
		EOQualifier qualifierAllowed = ERXQ.equals(EORegleLiaison.ALLOWED_KEY, 1);
		EOQualifier rechercherQualifier = ERXQ.and(qualifierAllowedParent, qualifierTypeLien, qualifierForbidden, qualifierVisible, qualifierAllowed);

		for (EORegleLiaison regleLiaison : EORegleLiaison.fetchSco_RegleLiaisons(editingContext, rechercherQualifier, null)) {
			types.add(regleLiaison.allowedChild());
		}

		return types;
	}

	/**
	 * @param editingContext
	 *            :contexte d'édition.
	 * @param typelien
	 *            : type de lien pour lequel on cherche les types de composants enfant autorisés.
	 * @param typeComposant
	 *            : type de composant pour lequel on cherche les types de composants enfant autorisé.
	 * @return liste des types de composants parents autorisé pour un type de composant et type de lien donné.
	 */
	public static NSArray<EOTypeComposant> typesComposantParentAutorise(EOEditingContext editingContext, EOTypeComposant typeComposant, EOTypeLien typelien) {
		NSArray<EOTypeComposant> types = new NSMutableArray<EOTypeComposant>();

		EOQualifier qualifierAllowedChild = ERXQ.equals(EORegleLiaison.ALLOWED_CHILD_KEY, typeComposant);
		EOQualifier qualifierTypeLien = ERXQ.equals(EORegleLiaison.TYPE_LIEN_KEY, typelien);
		EOQualifier qualifierForbidden = ERXQ.equals(EORegleLiaison.FORBIDDEN_KEY, 0);
		EOQualifier qualifierVisible = ERXQ.equals(EORegleLiaison.VISIBLE_KEY, 1);
		EOQualifier rechercherQualifier = ERXQ.and(qualifierAllowedChild, qualifierTypeLien, qualifierForbidden, qualifierVisible);

		for (EORegleLiaison regleLiaison : EORegleLiaison.fetchSco_RegleLiaisons(editingContext, rechercherQualifier, null)) {
			types.add(regleLiaison.allowedParent());
		}

		return types;
	}

	public String srcImage() {
		return "images/Matrice/" + this.nom() + ".png";
	}

}
