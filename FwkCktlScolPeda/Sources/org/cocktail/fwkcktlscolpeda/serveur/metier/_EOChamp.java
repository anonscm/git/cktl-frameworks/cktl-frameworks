// DO NOT EDIT.  Make changes to EOChamp.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOChamp extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Champ";

  // Attribute Keys
  public static final ERXKey<String> APPLICATION = new ERXKey<String>("application");
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> ID_PERE = new ERXKey<Integer>("idPere");
  public static final ERXKey<Boolean> OBLIGATOIRE = new ERXKey<Boolean>("obligatoire");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> TO_CHILDREN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp>("toChildren");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> TO_PARAMETRAGE_CHAMPS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>("toParametrageChamps");

  // Attributes
  public static final String APPLICATION_KEY = APPLICATION.key();
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String ID_PERE_KEY = ID_PERE.key();
  public static final String OBLIGATOIRE_KEY = OBLIGATOIRE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships
  public static final String TO_CHILDREN_KEY = TO_CHILDREN.key();
  public static final String TO_PARAMETRAGE_CHAMPS_KEY = TO_PARAMETRAGE_CHAMPS.key();

  private static Logger LOG = Logger.getLogger(_EOChamp.class);

  public EOChamp localInstanceIn(EOEditingContext editingContext) {
    EOChamp localInstance = (EOChamp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String application() {
    return (String) storedValueForKey(_EOChamp.APPLICATION_KEY);
  }

  public void setApplication(String value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating application from " + application() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.APPLICATION_KEY);
  }

  public String code() {
    return (String) storedValueForKey(_EOChamp.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOChamp.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOChamp.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.D_MODIFICATION_KEY);
  }

  public Integer idPere() {
    return (Integer) storedValueForKey(_EOChamp.ID_PERE_KEY);
  }

  public void setIdPere(Integer value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating idPere from " + idPere() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.ID_PERE_KEY);
  }

  public Boolean obligatoire() {
    return (Boolean) storedValueForKey(_EOChamp.OBLIGATOIRE_KEY);
  }

  public void setObligatoire(Boolean value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating obligatoire from " + obligatoire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.OBLIGATOIRE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOChamp.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.ORDRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOChamp.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOChamp.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOChamp.LOG.isDebugEnabled()) {
    	_EOChamp.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChamp.PERS_ID_MODIFCATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> toChildren() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp>)storedValueForKey(_EOChamp.TO_CHILDREN_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> toChildren(EOQualifier qualifier) {
    return toChildren(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> toChildren(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> results;
      results = toChildren();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToChildren(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp object) {
    includeObjectIntoPropertyWithKey(object, _EOChamp.TO_CHILDREN_KEY);
  }

  public void removeFromToChildren(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp object) {
    excludeObjectFromPropertyWithKey(object, _EOChamp.TO_CHILDREN_KEY);
  }

  public void addToToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp object) {
    if (_EOChamp.LOG.isDebugEnabled()) {
      _EOChamp.LOG.debug("adding " + object + " to toChildren relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToChildren(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOChamp.TO_CHILDREN_KEY);
    }
  }

  public void removeFromToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp object) {
    if (_EOChamp.LOG.isDebugEnabled()) {
      _EOChamp.LOG.debug("removing " + object + " from toChildren relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToChildren(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOChamp.TO_CHILDREN_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp createToChildrenRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOChamp.TO_CHILDREN_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp) eo;
  }

  public void deleteToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOChamp.TO_CHILDREN_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToChildrenRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> objects = toChildren().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToChildrenRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)storedValueForKey(_EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier) {
    return toParametrageChamps(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier, boolean fetch) {
    return toParametrageChamps(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.TO_CHAMP_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.fetchSco_ParametrageChamps(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toParametrageChamps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToParametrageChamps(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    includeObjectIntoPropertyWithKey(object, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public void removeFromToParametrageChamps(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    excludeObjectFromPropertyWithKey(object, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public void addToToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    if (_EOChamp.LOG.isDebugEnabled()) {
      _EOChamp.LOG.debug("adding " + object + " to toParametrageChamps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToParametrageChamps(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
    }
  }

  public void removeFromToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    if (_EOChamp.LOG.isDebugEnabled()) {
      _EOChamp.LOG.debug("removing " + object + " from toParametrageChamps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToParametrageChamps(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp createToParametrageChampsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp) eo;
  }

  public void deleteToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOChamp.TO_PARAMETRAGE_CHAMPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToParametrageChampsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> objects = toParametrageChamps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToParametrageChampsRelationship(objects.nextElement());
    }
  }


  public static EOChamp createSco_Champ(EOEditingContext editingContext, String application
, String code
, NSTimestamp dCreation
, NSTimestamp dModification
, Boolean obligatoire
, Integer ordre
, Integer persIdCreation
, Integer persIdModifcation
) {
    EOChamp eo = (EOChamp) EOUtilities.createAndInsertInstance(editingContext, _EOChamp.ENTITY_NAME);    
		eo.setApplication(application);
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setObligatoire(obligatoire);
		eo.setOrdre(ordre);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModifcation(persIdModifcation);
    return eo;
  }

  public static ERXFetchSpecification<EOChamp> fetchSpec() {
    return new ERXFetchSpecification<EOChamp>(_EOChamp.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOChamp> fetchAllSco_Champs(EOEditingContext editingContext) {
    return _EOChamp.fetchAllSco_Champs(editingContext, null);
  }

  public static NSArray<EOChamp> fetchAllSco_Champs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChamp.fetchSco_Champs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChamp> fetchSco_Champs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOChamp> fetchSpec = new ERXFetchSpecification<EOChamp>(_EOChamp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChamp> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOChamp fetchSco_Champ(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChamp.fetchSco_Champ(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChamp fetchSco_Champ(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChamp> eoObjects = _EOChamp.fetchSco_Champs(editingContext, qualifier, null);
    EOChamp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Champ that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChamp fetchRequiredSco_Champ(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChamp.fetchRequiredSco_Champ(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChamp fetchRequiredSco_Champ(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChamp eoObject = _EOChamp.fetchSco_Champ(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Champ that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChamp localInstanceIn(EOEditingContext editingContext, EOChamp eo) {
    EOChamp localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
