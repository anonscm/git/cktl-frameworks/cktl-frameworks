// DO NOT EDIT.  Make changes to EOParametragePaieFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametragePaieFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametragePaieFormation";

  // Attribute Keys
  public static final ERXKey<Boolean> BOURSIER = new ERXKey<Boolean>("boursier");
  public static final ERXKey<String> COMPTABILITE = new ERXKey<String>("comptabilite");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> REMBOURSEMENT = new ERXKey<Boolean>("remboursement");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TO_TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("toTypeFormation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant> TO_TYPE_MONTANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant>("toTypeMontant");

  // Attributes
  public static final String BOURSIER_KEY = BOURSIER.key();
  public static final String COMPTABILITE_KEY = COMPTABILITE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String REMBOURSEMENT_KEY = REMBOURSEMENT.key();
  // Relationships
  public static final String TO_GRADE_UNIVERSITAIRE_KEY = TO_GRADE_UNIVERSITAIRE.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();
  public static final String TO_TYPE_FORMATION_KEY = TO_TYPE_FORMATION.key();
  public static final String TO_TYPE_MONTANT_KEY = TO_TYPE_MONTANT.key();

  private static Logger LOG = Logger.getLogger(_EOParametragePaieFormation.class);

  public EOParametragePaieFormation localInstanceIn(EOEditingContext editingContext) {
    EOParametragePaieFormation localInstance = (EOParametragePaieFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean boursier() {
    return (Boolean) storedValueForKey(_EOParametragePaieFormation.BOURSIER_KEY);
  }

  public void setBoursier(Boolean value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating boursier from " + boursier() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.BOURSIER_KEY);
  }

  public String comptabilite() {
    return (String) storedValueForKey(_EOParametragePaieFormation.COMPTABILITE_KEY);
  }

  public void setComptabilite(String value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating comptabilite from " + comptabilite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.COMPTABILITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieFormation.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieFormation.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(_EOParametragePaieFormation.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.MONTANT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametragePaieFormation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOParametragePaieFormation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean remboursement() {
    return (Boolean) storedValueForKey(_EOParametragePaieFormation.REMBOURSEMENT_KEY);
  }

  public void setRemboursement(Boolean value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
    	_EOParametragePaieFormation.LOG.debug( "updating remboursement from " + remboursement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieFormation.REMBOURSEMENT_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setToGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
      _EOParametragePaieFormation.LOG.debug("updating toGradeUniversitaire from " + toGradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EOParametragePaieFormation.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EOParametragePaieFormation.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
      _EOParametragePaieFormation.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieFormation.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieFormation.TO_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation toTypeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(_EOParametragePaieFormation.TO_TYPE_FORMATION_KEY);
  }
  
  public void setToTypeFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    takeStoredValueForKey(value, _EOParametragePaieFormation.TO_TYPE_FORMATION_KEY);
  }

  public void setToTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
      _EOParametragePaieFormation.LOG.debug("updating toTypeFormation from " + toTypeFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = toTypeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieFormation.TO_TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieFormation.TO_TYPE_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant toTypeMontant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant)storedValueForKey(_EOParametragePaieFormation.TO_TYPE_MONTANT_KEY);
  }
  
  public void setToTypeMontant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant value) {
    takeStoredValueForKey(value, _EOParametragePaieFormation.TO_TYPE_MONTANT_KEY);
  }

  public void setToTypeMontantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant value) {
    if (_EOParametragePaieFormation.LOG.isDebugEnabled()) {
      _EOParametragePaieFormation.LOG.debug("updating toTypeMontant from " + toTypeMontant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeMontant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant oldValue = toTypeMontant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieFormation.TO_TYPE_MONTANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieFormation.TO_TYPE_MONTANT_KEY);
    }
  }
  

  public static EOParametragePaieFormation createSco_ParametragePaieFormation(EOEditingContext editingContext, Boolean boursier
, NSTimestamp dCreation
, java.math.BigDecimal montant
, Boolean remboursement
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation toTypeFormation, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant toTypeMontant) {
    EOParametragePaieFormation eo = (EOParametragePaieFormation) EOUtilities.createAndInsertInstance(editingContext, _EOParametragePaieFormation.ENTITY_NAME);    
		eo.setBoursier(boursier);
		eo.setDCreation(dCreation);
		eo.setMontant(montant);
		eo.setRemboursement(remboursement);
    eo.setToStructureRelationship(toStructure);
    eo.setToTypeFormationRelationship(toTypeFormation);
    eo.setToTypeMontantRelationship(toTypeMontant);
    return eo;
  }

  public static ERXFetchSpecification<EOParametragePaieFormation> fetchSpec() {
    return new ERXFetchSpecification<EOParametragePaieFormation>(_EOParametragePaieFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametragePaieFormation> fetchAllSco_ParametragePaieFormations(EOEditingContext editingContext) {
    return _EOParametragePaieFormation.fetchAllSco_ParametragePaieFormations(editingContext, null);
  }

  public static NSArray<EOParametragePaieFormation> fetchAllSco_ParametragePaieFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametragePaieFormation.fetchSco_ParametragePaieFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametragePaieFormation> fetchSco_ParametragePaieFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametragePaieFormation> fetchSpec = new ERXFetchSpecification<EOParametragePaieFormation>(_EOParametragePaieFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametragePaieFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametragePaieFormation fetchSco_ParametragePaieFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieFormation.fetchSco_ParametragePaieFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieFormation fetchSco_ParametragePaieFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametragePaieFormation> eoObjects = _EOParametragePaieFormation.fetchSco_ParametragePaieFormations(editingContext, qualifier, null);
    EOParametragePaieFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametragePaieFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieFormation fetchRequiredSco_ParametragePaieFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieFormation.fetchRequiredSco_ParametragePaieFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieFormation fetchRequiredSco_ParametragePaieFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametragePaieFormation eoObject = _EOParametragePaieFormation.fetchSco_ParametragePaieFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametragePaieFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieFormation localInstanceIn(EOEditingContext editingContext, EOParametragePaieFormation eo) {
    EOParametragePaieFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
