// DO NOT EDIT.  Make changes to EOCreditable.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOCreditable extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Creditable";

  // Attribute Keys
  public static final ERXKey<Integer> BASE_NOTE = new ERXKey<Integer>("baseNote");
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<java.math.BigDecimal> CREDIT_ECTS = new ERXKey<java.math.BigDecimal>("creditECTS");
  public static final ERXKey<Integer> LIEN_ID = new ERXKey<Integer>("lienId");
  public static final ERXKey<Boolean> SANS_NOTE = new ERXKey<Boolean>("sansNote");
  public static final ERXKey<java.math.BigDecimal> TRAVAIL_ETUDIANT = new ERXKey<java.math.BigDecimal>("travailEtudiant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("lien");

  // Attributes
  public static final String BASE_NOTE_KEY = BASE_NOTE.key();
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String CREDIT_ECTS_KEY = CREDIT_ECTS.key();
  public static final String LIEN_ID_KEY = LIEN_ID.key();
  public static final String SANS_NOTE_KEY = SANS_NOTE.key();
  public static final String TRAVAIL_ETUDIANT_KEY = TRAVAIL_ETUDIANT.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String LIEN_KEY = LIEN.key();

  private static Logger LOG = Logger.getLogger(_EOCreditable.class);

  public EOCreditable localInstanceIn(EOEditingContext editingContext) {
    EOCreditable localInstance = (EOCreditable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer baseNote() {
    return (Integer) storedValueForKey(_EOCreditable.BASE_NOTE_KEY);
  }

  public void setBaseNote(Integer value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating baseNote from " + baseNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.BASE_NOTE_KEY);
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOCreditable.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.COMPOSANT_ID_KEY);
  }

  public java.math.BigDecimal creditECTS() {
    return (java.math.BigDecimal) storedValueForKey(_EOCreditable.CREDIT_ECTS_KEY);
  }

  public void setCreditECTS(java.math.BigDecimal value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating creditECTS from " + creditECTS() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.CREDIT_ECTS_KEY);
  }

  public Integer lienId() {
    return (Integer) storedValueForKey(_EOCreditable.LIEN_ID_KEY);
  }

  public void setLienId(Integer value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating lienId from " + lienId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.LIEN_ID_KEY);
  }

  public Boolean sansNote() {
    return (Boolean) storedValueForKey(_EOCreditable.SANS_NOTE_KEY);
  }

  public void setSansNote(Boolean value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating sansNote from " + sansNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.SANS_NOTE_KEY);
  }

  public java.math.BigDecimal travailEtudiant() {
    return (java.math.BigDecimal) storedValueForKey(_EOCreditable.TRAVAIL_ETUDIANT_KEY);
  }

  public void setTravailEtudiant(java.math.BigDecimal value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
    	_EOCreditable.LOG.debug( "updating travailEtudiant from " + travailEtudiant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCreditable.TRAVAIL_ETUDIANT_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOCreditable.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOCreditable.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
      _EOCreditable.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCreditable.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCreditable.COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien lien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOCreditable.LIEN_KEY);
  }
  
  public void setLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOCreditable.LIEN_KEY);
  }

  public void setLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOCreditable.LOG.isDebugEnabled()) {
      _EOCreditable.LOG.debug("updating lien from " + lien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = lien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCreditable.LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCreditable.LIEN_KEY);
    }
  }
  

  public static EOCreditable createSco_Creditable(EOEditingContext editingContext, Boolean sansNote
) {
    EOCreditable eo = (EOCreditable) EOUtilities.createAndInsertInstance(editingContext, _EOCreditable.ENTITY_NAME);    
		eo.setSansNote(sansNote);
    return eo;
  }

  public static ERXFetchSpecification<EOCreditable> fetchSpec() {
    return new ERXFetchSpecification<EOCreditable>(_EOCreditable.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOCreditable> fetchAllSco_Creditables(EOEditingContext editingContext) {
    return _EOCreditable.fetchAllSco_Creditables(editingContext, null);
  }

  public static NSArray<EOCreditable> fetchAllSco_Creditables(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCreditable.fetchSco_Creditables(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCreditable> fetchSco_Creditables(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOCreditable> fetchSpec = new ERXFetchSpecification<EOCreditable>(_EOCreditable.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCreditable> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOCreditable fetchSco_Creditable(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCreditable.fetchSco_Creditable(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCreditable fetchSco_Creditable(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCreditable> eoObjects = _EOCreditable.fetchSco_Creditables(editingContext, qualifier, null);
    EOCreditable eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Creditable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCreditable fetchRequiredSco_Creditable(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCreditable.fetchRequiredSco_Creditable(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCreditable fetchRequiredSco_Creditable(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCreditable eoObject = _EOCreditable.fetchSco_Creditable(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Creditable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCreditable localInstanceIn(EOEditingContext editingContext, EOCreditable eo) {
    EOCreditable localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
