package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * {@inheritDoc}
 */
public class EOVocation extends _EOVocation implements IVocation {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOVocation.class);
}
