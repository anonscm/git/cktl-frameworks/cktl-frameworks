package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOMention;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAcademie;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IBac;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Informations relatives à un étudiant
 */
public interface IInfosEtudiant {
	
	public EOEditingContext editingContext();
	
	Date dNaissance();

	CodeInsee getCodeInsee();

	/**
	 * @return civilite (Mr, Mme, Mlle)
	 */
	String civilite();


	Integer cleInsee();

	String codeIne();

	Integer etudNumero();
	
	String deptNaissanceLibelle();


	Boolean enfantsCharge();

	Boolean ineProvisoire();

	Boolean inseeProvisoire();

	String noInsee();

	String nomPatronymique();

	String nomUsuel();

	String prenom();

	String prenom2();

	
	/**
	 * @return <code> true </code> si l'étudiant fait du sport à haut niveau
	 */
	Boolean sportHN();
	
	/**
	 * @param value <code> true </code> si l'étudiant fait du sport à haut niveau
	 */
	void setSportHN(Boolean value);

	String villeNaissance();

	ICivilite toCivilite();
	
	IPays toPaysNaissance();

	IPays toPaysNationalite();


	ISituationFamiliale toSituationFamiliale();




	IIndividu toIndividuEtudiant();


	void setCleInsee(Integer value);

	void setCodeIne(String value);


	void setDNaissance(Date value);

	void setEnfantsCharge(Boolean value);

	void setIneProvisoire(Boolean value);

	void setInseeProvisoire(Boolean value);

	void setNoInsee(String value);

	void setNomPatronymique(String value);

	void setNomUsuel(String value);

	void setPrenom(String value);

	void setPrenom2(String value);



	void setToSituationFamilialeRelationship(ISituationFamiliale value);
	/**
	 * @return departement de naissance de l'étudiant
	 */
	IDepartement toDepartement();
	
	void setToDepartementRelationship(IDepartement departement);
	
	IIndividu asIndividu();

	List<IAdresse> toAdresses();

	IAdresse adresseEtudiant();

	IAdresse adresseParent();

	void supprimerAdresse(IAdresse currentAdresse, ITypeAdresse type);

	/**
	 * @return le cursus de l'étudiant
	 */
	List<? extends ICursus> toCursus();
	
	List<ITelephone> toTelephones();
	ITelephone toTelephone(ITypeTel typeTel, ITypeNoTel typeNoTel);
	
	void setToCiviliteRelationship(ICivilite value);
	
	void setToPaysNaissanceRelationship(IPays value);
	
	void setToPaysNationaliteRelationship(IPays value);
	
	void setVilleNaissance(String value);
	
	/**
	 * @return profession (du parent)
	 */
	IProfession toProfession1();
	
	/**
	 * @param value profession (du parent)
	 */
	void setToProfession1Relationship(EOProfession value);
	
	/**
	 * @return profession (du deuxième parent)
	 */
	IProfession toProfession2();
	
	/**
	 * @param value profession (du deuxième parent)
	 */
	void setToProfession2Relationship(EOProfession value);

	/**
	 * @return inscription principale
	 */
	IInscription getInscriptionPrincipale();
	
	/**
	 * @return liste de toutes les inscriptions de l'étudiant
	 */
	List<? extends IInscription> toInscriptions();
	
	/**
	 * @param qualifier définit les critères de sélection
	 * @return liste de toutes les inscriptions de l'étudiant
	 */
	List<? extends IInscription> toInscriptions(EOQualifier qualifier);
	
	/**
	 * @return liste de toutes les bourses de l'étudiant
	 */
	List<? extends IBourses> toBourses();
	
	/**
	 * @param qualifier définit les critères de sélection
	 * @return  liste des bourses correspondant aux critères  
	 */
	List<? extends IBourses> toBourses(EOQualifier qualifier);
	
	/**
	 * @return bac obtenu par l'étudiant
	 */
	IBac toBac();
	
	 /**
	 * @return année d'obtention du bac
	 */
	Long etudAnbac();
	  /**
	 * @return l'académie dans lequel l'étudiant a passé le bac
	 */
	IAcademie toAcademie();

	/**
	 * @return première année d'inscription dans l'établissement
	 */
	Long etudAnnee1inscEtab();
	
	/**
	 * @return première année d'inscription dans l'enseignement supérieur
	 */
	Long etudAnnee1inscSup();
	 
	/**
	 * @return première année d'inscription à l'universite
	 */
	Long etudAnnee1inscUniv();

	/**
	 * @param uneAnnee L'année de première inscription dans l'établissement
	 */
	void setEtudAnnee1InscEtab(Long uneAnnee);
	
	/**
	 * @param uneAnnee L'année de première inscription dans l'enseignement supérieur
	 */
	void setEtudAnnee1InscSup(Long uneAnnee);
	
	/**
	 * @param uneAnnee L'année de première inscription à l'université
	 */
	void setEtudAnnee1InscUniv(Long uneAnnee);
	
	 /**
	 * @return département d'obtention du bacalauréat
	 */
	IDepartement toDepartementEtabBac();

	void setToBacRelationship(EOBac bac);

	void setToMentionRelationship(EOMention mentionBacSelected);
	
	IRne toRneEtabBac();

	void setToRneEtabBacRelationship(IRne etablissementBac);

	void setVilleBac(String villeBac);

	String villeBac();

	IPays toPaysEtabBac();

	void setToPaysEtabBacRelationship(IPays selectedPays);

	EOMention toMention();

	void setToRneEtabSupRelationship(IRne rneEtabSup);
	
	void setEtudReimmatriculation(String value);
	
	 public String etudReimmatriculation();

}
