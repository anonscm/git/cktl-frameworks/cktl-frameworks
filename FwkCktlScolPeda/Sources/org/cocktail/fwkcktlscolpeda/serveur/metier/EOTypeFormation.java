package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Types de formation
 */
public class EOTypeFormation extends _EOTypeFormation implements ITypeFormation {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeFormation.class);
    private static ERXEnterpriseObjectCache<EOTypeFormation> typeFormationCache;
    
    /**
     * @return le cache des {@link EOTypeFormation} par leur type
     */
    public static ERXEnterpriseObjectCache<EOTypeFormation> getTypeFormationCache() {
    	if (typeFormationCache == null) {
    		typeFormationCache = new ERXEnterpriseObjectCache<EOTypeFormation>(EOTypeFormation.class, CODE_KEY);
    	}
    	return typeFormationCache;
    }
    
    /**
     * @param typeFormationCache le cache a setter
     */
    public static void setTypeFormationCache(ERXEnterpriseObjectCache<EOTypeFormation> typeFormationCache) {
    	EOTypeFormation.typeFormationCache = typeFormationCache;
    }
    
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormation} "Licence"
	 */
	public static EOTypeFormation typeLicence(EOEditingContext edc) {
		return getTypeFormationCache().objectForKey(edc, TF_LICENCE);
	}
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormation} "LicencePro"
	 */
	public static EOTypeFormation typeLicencePro(EOEditingContext edc) {
		return getTypeFormationCache().objectForKey(edc, TF_LICENCE_PRO);
	}
	
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormation} "Master"
	 */
	public static EOTypeFormation typeMaster(EOEditingContext edc) {
		return getTypeFormationCache().objectForKey(edc, TF_MASTER);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormation} "Doctorat"
	 */
	public static EOTypeFormation typeDoctorat(EOEditingContext edc) {
		return getTypeFormationCache().objectForKey(edc, TF_DOCTORAT);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormation} "DUT"
	 */
	public static EOTypeFormation typeDUT(EOEditingContext edc) {
		return getTypeFormationCache().objectForKey(edc, TF_DUT);
	}
	
	
	/**
	 * @return Boolean : indique si le type de formation est habilitable
	 */
	public Boolean isHabilitable() {
		return (habilitable() == 1);
	}

	@Override
	public String toString() {
		return this.libelle();
	}

}
