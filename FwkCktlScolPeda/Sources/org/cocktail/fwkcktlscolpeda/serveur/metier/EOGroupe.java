package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Cette classe représente un groupe pour un composant.
 * 
 * @author Pascal MACOUIN
 */
public class EOGroupe extends _EOGroupe implements IGroupe {
	private static final long serialVersionUID = -5088791368756985849L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOGroupe.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToComposantRelationship(IComposant unComposant) {
		super.setToComposantRelationship((EOComposant) unComposant);
	}

	/**
     * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}	
}
