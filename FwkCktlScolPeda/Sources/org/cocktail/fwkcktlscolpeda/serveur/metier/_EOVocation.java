// DO NOT EDIT.  Make changes to EOVocation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOVocation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Vocation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOVocation.class);

  public EOVocation localInstanceIn(EOEditingContext editingContext) {
    EOVocation localInstance = (EOVocation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOVocation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOVocation.LOG.isDebugEnabled()) {
    	_EOVocation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVocation.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOVocation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOVocation.LOG.isDebugEnabled()) {
    	_EOVocation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVocation.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOVocation.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOVocation.LOG.isDebugEnabled()) {
    	_EOVocation.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVocation.ORDRE_KEY);
  }


  public static EOVocation createSco_Vocation(EOEditingContext editingContext, String code
, String libelle
, Integer ordre
) {
    EOVocation eo = (EOVocation) EOUtilities.createAndInsertInstance(editingContext, _EOVocation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
    return eo;
  }

  public static ERXFetchSpecification<EOVocation> fetchSpec() {
    return new ERXFetchSpecification<EOVocation>(_EOVocation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOVocation> fetchAllSco_Vocations(EOEditingContext editingContext) {
    return _EOVocation.fetchAllSco_Vocations(editingContext, null);
  }

  public static NSArray<EOVocation> fetchAllSco_Vocations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVocation.fetchSco_Vocations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVocation> fetchSco_Vocations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOVocation> fetchSpec = new ERXFetchSpecification<EOVocation>(_EOVocation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVocation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOVocation fetchSco_Vocation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVocation.fetchSco_Vocation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVocation fetchSco_Vocation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVocation> eoObjects = _EOVocation.fetchSco_Vocations(editingContext, qualifier, null);
    EOVocation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Vocation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVocation fetchRequiredSco_Vocation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVocation.fetchRequiredSco_Vocation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVocation fetchRequiredSco_Vocation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVocation eoObject = _EOVocation.fetchSco_Vocation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Vocation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVocation localInstanceIn(EOEditingContext editingContext, EOVocation eo) {
    EOVocation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
