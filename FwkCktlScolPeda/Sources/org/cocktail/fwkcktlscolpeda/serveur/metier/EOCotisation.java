package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EOCotisation extends _EOCotisation {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOCotisation.class);
	
	/** The Constant COTI_CODE_NON. */
	public static final String COTI_CODE_NON = "0";
	
	/** The Constant COTI_CODE_OUI. */
	public static final String COTI_CODE_OUI = "-1";
	
	/** The Constant COTI_CODE_OUI_MAIS_EXONERE. */
	public static final String COTI_CODE_OUI_MAIS_EXONERE = "-2";
	
	/** The Constant COTI_CODE_PARENT_REGIME_PARTICULIER. */
	public static final String COTI_CODE_PARENT_REGIME_PARTICULIER = "1";

	/** The Constant COTI_CODE_PLUS_DE_28_ANS. */
	public static final String COTI_CODE_PLUS_DE_28_ANS = "2";

	/** The Constant COTI_CODE_SALARIE. */
	public static final String COTI_CODE_SALARIE = "3";

	/** The Constant COTI_CODE_CONJOINT. */
	public static final String COTI_CODE_CONJOINT = "4";

	/** The Constant COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE. */
	public static final String COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE = "5";

	/** The Constant COTI_CODE_DEJA_AFFILIE. */
	public static final String COTI_CODE_DEJA_AFFILIE = "6";
	
	/** The Constant COTI_CODE_DEMANDEUR_D_EMPLOI. */
	public static final String COTI_CODE_DEMANDEUR_D_EMPLOI = "7";

	/** The Constant COTI_CODE_AUTRES_CAS. */
	public static final String COTI_CODE_AUTRES_CAS = "8";

	/** The Constant COTI_CODE_CARTE_EUROPEENNE. */
	public static final String COTI_CODE_CARTE_EUROPEENNE = "9";

	/** The Constant COTI_CODE_ETUDIANT_ETRANGER. */
	public static final String COTI_CODE_ETUDIANT_ETRANGER = "10";
	
	/** The Constant COTI_CODE_ENFANT_SALARIE. */
	public static final String COTI_CODE_ENFANT_SALARIE = "11";
	
	/** The Constant COTI_CODE_ENFANT_SALARIE. */
	public static final String COTI_CODE_ETUDIANT_INDEPENDANT = "12";
}
