package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * type de lien
 */
public interface ITypeLien {
	
	/**
	 * @return id du lien
	 */
	Integer id();
}