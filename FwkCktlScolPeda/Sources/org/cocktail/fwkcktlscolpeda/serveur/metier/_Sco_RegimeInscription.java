// DO NOT EDIT.  Make changes to Sco_RegimeInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _Sco_RegimeInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_RegimeInscription";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Sco_RegimeInscription.class);

  public Sco_RegimeInscription localInstanceIn(EOEditingContext editingContext) {
    Sco_RegimeInscription localInstance = (Sco_RegimeInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_Sco_RegimeInscription.CODE_KEY);
  }

  public void setCode(String value) {
    if (_Sco_RegimeInscription.LOG.isDebugEnabled()) {
    	_Sco_RegimeInscription.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_RegimeInscription.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_Sco_RegimeInscription.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_Sco_RegimeInscription.LOG.isDebugEnabled()) {
    	_Sco_RegimeInscription.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_RegimeInscription.LIBELLE_KEY);
  }


  public static Sco_RegimeInscription createSco_RegimeInscription(EOEditingContext editingContext, String code
, String libelle
) {
    Sco_RegimeInscription eo = (Sco_RegimeInscription) EOUtilities.createAndInsertInstance(editingContext, _Sco_RegimeInscription.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<Sco_RegimeInscription> fetchSpec() {
    return new ERXFetchSpecification<Sco_RegimeInscription>(_Sco_RegimeInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Sco_RegimeInscription> fetchAllSco_RegimeInscriptions(EOEditingContext editingContext) {
    return _Sco_RegimeInscription.fetchAllSco_RegimeInscriptions(editingContext, null);
  }

  public static NSArray<Sco_RegimeInscription> fetchAllSco_RegimeInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Sco_RegimeInscription.fetchSco_RegimeInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<Sco_RegimeInscription> fetchSco_RegimeInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Sco_RegimeInscription> fetchSpec = new ERXFetchSpecification<Sco_RegimeInscription>(_Sco_RegimeInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Sco_RegimeInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Sco_RegimeInscription fetchSco_RegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_RegimeInscription.fetchSco_RegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_RegimeInscription fetchSco_RegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Sco_RegimeInscription> eoObjects = _Sco_RegimeInscription.fetchSco_RegimeInscriptions(editingContext, qualifier, null);
    Sco_RegimeInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_RegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_RegimeInscription fetchRequiredSco_RegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_RegimeInscription.fetchRequiredSco_RegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_RegimeInscription fetchRequiredSco_RegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    Sco_RegimeInscription eoObject = _Sco_RegimeInscription.fetchSco_RegimeInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_RegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_RegimeInscription localInstanceIn(EOEditingContext editingContext, Sco_RegimeInscription eo) {
    Sco_RegimeInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
