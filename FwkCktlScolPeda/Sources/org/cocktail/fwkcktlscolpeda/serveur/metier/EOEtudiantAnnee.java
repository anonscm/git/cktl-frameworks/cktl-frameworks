package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXKey;

/**
 * Informations de l'étudiant relatives à une année
 */
public class EOEtudiantAnnee extends _EOEtudiantAnnee implements IScoEtudiantAnnee {
	private static final long serialVersionUID = 6843134239583533246L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOEtudiantAnnee.class);

	public static final ERXKey<Boolean> ID_ETUDIANT_ANNEE = new ERXKey<Boolean>("idEtudiantAnnee");
	public static final String BOURSIER_KEY = "boursier";
	public static final ERXKey<Boolean> BOURSIER = new ERXKey<Boolean>(BOURSIER_KEY);

	/**
	 * {@inheritDoc}
	 */
	public IInfosEtudiant toInfosEtudiant() {
		return new ScoEtudiant(super.toEtudiant());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantRelationship(IEtudiant value) {
		super.setToEtudiantRelationship((EOEtudiant) value);
	}

	

	public IScoInscription getInscriptionPrincipale() {
		EOQualifier qualifier = EOInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).eq(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		NSArray<EOInscription> listeInscription = toInscriptions(qualifier);
		if (listeInscription != null && listeInscription.size() > 0) {
			return listeInscription.lastObject();
		} else {
			qualifier = EOInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).eq(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL);
			listeInscription = toInscriptions(qualifier);
			if (listeInscription != null && listeInscription.size() > 0) {
				return listeInscription.lastObject();
			}
		}
		return null;
	
    }
	
	public List<IScoInscription> getInscriptionsComplementaires() {
		EOQualifier qualifier = EOInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).ne(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		return new ArrayList<IScoInscription>(toInscriptions(qualifier, EOInscription.D_CREATION.ascs(), false));
	}

	public IScoInscription getInscriptionComplementaire(int index) {
		if (getInscriptionsComplementaires().size() > index) {
			return getInscriptionsComplementaires().get(index);
		} else {
			return null;
		}
	}
	
	public IScoInscription getInscriptionSecondaire() {
		return getInscriptionComplementaire(0);
	}
	
	public IScoInscription getInscriptionTertiaire() {
		return getInscriptionComplementaire(1);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IScoBourses> toBourses() {
		EOQualifier qualifier = EOBourses.TO_ETUDIANT.eq(toEtudiant()).and(EOBourses.ANNEE.eq(annee()));
		return new ArrayList<IScoBourses>(EOBourses.fetchSco_Bourseses(editingContext(), qualifier, null));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToRegimeParticulierRelationship(IRegimeParticulier unRegimeParticulier) {
		super.setToRegimeParticulierRelationship((EORegimeParticulier) unRegimeParticulier);
	}
	
    /**
     * {@inheritDoc}
     */
    public Integer id() {
        return Integer.valueOf(primaryKey());
    }

    /**
     * {@inheritDoc}
     */
    public boolean aUnLienAvecGfc() {
    	for (IScoPaiement paiement : toPaiements()) {
			if (paiement.aUnLienAvecGfc()) {
				return true;
			}
		}
    	
    	return false;
    }

    /**
     * {@inheritDoc}
     */
	public List<IScoPaiementMoyen> moyensPaiementPrelevementSansEcheancierSepa() {
    	// Il faut qu'il y ai au moins un paiement avec un moyen de paiement par prélèvement
    	List<IScoPaiementMoyen> moyens = new ArrayList<IScoPaiementMoyen>();
    	for (IScoPaiement paiement : toPaiements()) {
    		for (IScoPaiementMoyen moyen : paiement.toScoPaiementMoyens()) {
    			if (moyen.isDiffere() && moyen.toEcheancierSepaSdd() == null) {
    				if (!moyen.toPaiementEcheances().isEmpty()) {
    					moyens.add(moyen);
    				}
    			}
    		}
    	}
    	return moyens;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean boursier() {
		return (isStatutBourseCNOUSEchelonValide() || toBourses().size() > 0);
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isStatutBourseCNOUSEchelonValide() {
		// TODO à completer si besoin (échelon 0Bis)
		if (statutBoursierCnous() != null) {
			return StringUtils.isNumeric(statutBoursierCnous());
		}

		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IPaiement toPaiementInitial() {
		return toScoPaiementInitial();
	}

	/**
	 * {@inheritDoc}
	 */
	public IScoPaiement toScoPaiementInitial() {
		return super.toPaiements(EOPaiement.ORDRE.eq(1)).lastObject();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IScoPaiement> toScoPaiements() {
		return super.toPaiements();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToSituationFamilialeRelationship(ISituationFamiliale value) {
		super.setToSituationFamilialeRelationship((EOSituationFamiliale) value);
	}

}
