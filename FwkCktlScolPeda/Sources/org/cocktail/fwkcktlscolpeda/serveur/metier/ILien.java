package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * Interface représentant un lien
 */
public interface ILien {
	/**
	 * @return le composant enfant relie au lien
	 */
	IComposant child();

	/**
	 * @return le composant parent relie au lien
	 */
	IComposant parent();

	/**
	 * @return la somme des credits ECTS calcules pour un composant donne
	 */
	BigDecimal getCreditECTSCalcule();

	/**
	 * met à jour la valeur du creditECTS calcule
	 * @param creditECTSCalcule nouvelle valeur du credit ECTS
	 */
	void setCreditECTSCalcule(BigDecimal creditECTSCalcule);

	/**
	 * Compare le nombre d'ECTS calculé par rapport aux regles
	 * @return si le nombre est valide ou pas
	 */
	Boolean isNbCreditsEctsOk();

	/**
	 * @return la sommes des minutes enseignees pour un composant donne
	 */
	Integer getSommeMinutesEnseigneesCalculee();

	/**
	 * met à jour la valeur du sommeMinutesEnseigneesCalculee
	 * @param sommeMinutesEnseigneesCalculee nouvelle valeur des minutes enseignees
	 */
	void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee);

	/**
	 * @param typeAP typeAP pour lequel on veut avoir la somme des minutes
	 * @return un dictionnaire de sommes de minutes enseignées par type de composant
	 */
	Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP);

	/**
	 * met a jour la somme des minutes enseignees calculees pour un typeAP donne
	 * @param sommeMinutesEnseignesCalculees somme des minute enseignees
	 * @param typeAP typeAP pour lequel on veut mettre à jour la somme des minutes
	 */
	void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeMinutesEnseignesCalculees, EOTypeAP typeAP);

	/**
	 * @return somme des minutes des APs dont le type AP est un type TD pour tous les descendants du compsant child du lien
	 */
	Integer sommeMinutesEnseigneesAPTD();
	
	/**
	 * @param valeurMinutes la somme des minutes enseignées de type TD
	 */
	void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes);

	/**
	 * @return somme des minutes des APs dont le type AP est un type TP pour tous les descendants du compsant child du lien
	 */
	Integer sommeMinutesEnseigneesAPTP();

	/**
	 * @param valeurMinutes la somme des minutes enseignées de type TP
	 */
	void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes);
	
	/**
	 * @return somme des minutes des APs dont le type AP est un type Cours (CM) pour tous les descendants du compsant child du lien
	 */
	Integer sommeMinutesEnseigneesAPCours();
	
	/**
	 * @param valeurMinutes la somme des minutes enseignées de type cours
	 */
	void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes);

	/**
	 * @return l'id du lien
	 */
	Integer id();

	/**
	 * @return true si le child du lien est sous un semestre
	 */
	Boolean isParentSemestre();

	/**
	 * @return l'id du composant enfant
	 */
	Integer childId();

	/**
	 * @return l'id du composant parent
	 */
	Integer parentId();

	/**
	 * @return ordre de la relation parent/enfant dans la liste des relations parent/enfants
	 */
	Integer ordre();

	/**
	 * @param value ordre de la relation parent/enfant dans la liste des relations parent/enfants
	 */
	void setOrdre(Integer value);
	
	/**
	 * @return le creditable associe au lien si il existe
	 */
	ICreditable creditable();

	/**
	 * @param nouveauLien nouveau lien auquel on assigne les valeurs du lien courant
	 */
	public void assignTo(ILien nouveauLien);
	
	/**
	 * Null Object pour envoyer un résultat non null.
	 */
	ILien NULL = new ILien() {

		public Integer sommeMinutesEnseigneesAPTP() {
			return null;
		}

		public Integer sommeMinutesEnseigneesAPTD() {
			return null;
		}

		public Integer sommeMinutesEnseigneesAPCours() {
			return null;
		}

		public BigDecimal sommeCreditECTSEnfants() {
			return null;
		}

		public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeMinutesEnseignesCalculees, EOTypeAP typeAP) {
		}

		public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
		}

		public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
		}

		public IComposant parent() {
			return null;
		}

		public Boolean isParentSemestre() {
			return null;
		}

		public Integer id() {
			return null;
		}

		public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
			return null;
		}

		public Integer getSommeMinutesEnseigneesCalculee() {
			return null;
		}

		public BigDecimal getCreditECTSCalcule() {
			return null;
		}

		public IComposant child() {
			return null;
		}

		public Boolean isNbCreditsEctsOk() {
			return null;
		}

		public Integer childId() {
			return null;
		}

		public Integer parentId() {
			return null;
		}

		public Integer ordre() {
	    return null;
    }

		public void setOrdre(Integer value) {	    
    }

		public ICreditable creditable() {
			return null;
		}

		public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
			
		}

		public void assignTo(ILien newLien) {
	    // TODO Auto-generated method stub
	    
    }
	};

}
