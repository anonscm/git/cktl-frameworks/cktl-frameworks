package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXUtilities;

/**
 * La classe EOVersionDiplome décrit et maintient une version de diplôme..
 */
public class EOVersionDiplome extends _EOVersionDiplome implements IVersionDiplome {
	private static final long serialVersionUID = 1L;
	// Private fields
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOVersionDiplome.class);

	public static final String DIPLOME_KEY = "diplome";
	public static final ERXKey<EODiplome> DIPLOME = new ERXKey<EODiplome>(DIPLOME_KEY);

	// Constructor

	// Properties
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeVersionDiplome(editingContext());
	}

	/**
	 * @param diplome : diplome rattaché
	 * @param annee : année rattachée
	 */
	public void initialise(EODiplome diplome, Integer annee) {
		setCode("v " + annee + " - " + diplome.code());
		setLibelle("La version " + annee + " du diplôme " + diplome.code());
		setAnnee(annee);
	}

	// Public methods
	/**
	 * @return le diplome associé à la version de diplome si il existe
	 */
	public EODiplome getDiplome() {
		EODiplome diplome = null;

		if (parents(EOTypeLien.typeVersionner(editingContext()), EOTypeComposant.typeDiplome(editingContext()), null).size() > 0) {
			diplome = (EODiplome) parents(EOTypeLien.typeVersionner(editingContext()), EOTypeComposant.typeDiplome(editingContext()), null).get(0);
		}

		return diplome;
	}

	/**
	 * Cree les niveaux de type Periode sous une version de diplome
	 * @param nbNiveaux : le nombre de niveaux
	 */
	public void creerNiveaux(int nbNiveaux) {
		for (int i = 0; i < nbNiveaux; i++) {
			// créer le grade : un composant de type période et le lien qui le rattache au diplôme
			EOPeriode nouvellePeriode = creerPeriode(String.format("%s %d du diplôme %s ", getDiplome().gradeUniversitaire().libelle(), i + 1, code()),
			    String.format("%s %d", getDiplome().gradeUniversitaire().libelle(), i + 1), EOTypePeriode.typePeriodeAnnee(editingContext()), i + 1);

			// lier la période au diplome
			EOLienComposer lien = EOComposantFactory.createLienComposer(this, nouvellePeriode);
			this.addToLiensParentsRelationship(lien);
			nouvellePeriode.addToLiensChildsRelationship(lien);
		}
	}

	/**
	 * Cree les niveaux et les périodes de type Periode sous une version de diplome
	 * @param nbNiveaux : le nombre de niveaux
	 * @param nbPeriodes : le nombre de periodes
	 */
	public void creerNiveauxEtPeriodes(int nbNiveaux, int nbPeriodes) {
		int compteurPeriode = 1;

		for (int i = 0; i < nbNiveaux; i++) {
			// créer le grade : un composant de type période et le lien qui le rattache au diplôme
			EOPeriode nouvellePeriode = creerPeriode(String.format("%s %d du diplôme %s", getDiplome().gradeUniversitaire().libelle(), i + 1, code()),
			    String.format("%s %d", getDiplome().gradeUniversitaire().libelle(), i + 1), EOTypePeriode.typePeriodeAnnee(editingContext()), i + 1);

			// lier la période au diplome
			EOLienComposer lien = EOComposantFactory.createLienComposer(this, nouvellePeriode);
			this.addToLiensParentsRelationship(lien);
			nouvellePeriode.addToLiensChildsRelationship(lien);

			// puis créer les n périodes sous le niveau
			for (int j = 0; j < nbPeriodes; j++) {
				// créer le grade : un composant de type période et le lien qui le rattache au diplôme
				EOPeriode nouvellePeriode2 = creerPeriode(
				    String.format("%s %d du niveau %s", getDiplome().typeOrganisation().libelle(), compteurPeriode, nouvellePeriode.code()),
				    String.format("%s %d", getDiplome().typeOrganisation().libelle(), compteurPeriode), EOTypePeriode.typePeriodeSemestre(editingContext()),
				    compteurPeriode);

				// lier la période au diplome
				EOLienComposer lien2 = EOComposantFactory.createLienComposer(nouvellePeriode, nouvellePeriode2);
				nouvellePeriode.addToLiensParentsRelationship(lien2);
				nouvellePeriode2.addToLiensChildsRelationship(lien2);

				compteurPeriode++;
			}
		}
	}

	private EOPeriode creerPeriode(String code, String libelle, EOTypePeriode typePeriode, Integer niveau) {
		try {
			EOPeriode nouvellePeriode = EOComposantFactory.createComposant(EOPeriode.class, this.persIdCreation(), editingContext(), composantInfoVersion().annee(), composantInfoVersion().versionMajeure());
			nouvellePeriode.setCode(code);
			nouvellePeriode.setLibelle(libelle);
			nouvellePeriode.setToTypePeriodeRelationship(typePeriode);
			nouvellePeriode.setOrdre(niveau);
			return nouvellePeriode;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String libelleTreeTable() {
		return this.getDiplome().libelle();
	}

	/**
	 * {@inheritDoc}
	 */
	public EOVersionDiplome copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			// TODO : récupérer le persId par injection du user
			EOVersionDiplome versionDiplome = EOComposantFactory.createComposant(EOVersionDiplome.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(versionDiplome);
			return versionDiplome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return IVersionDiplome : le composant suivant associe au composant si il existe
	 */
	public IVersionDiplome getComposantAvecOrigine() {
		if (super.getComposantSuivant() != null) {
			return ((IVersionDiplome) (super.getComposantSuivant()));
		}

		return null;
	}

	/**
	 * @return IVersionDiplome : le composant d'origine
	 */
	public IVersionDiplome getComposantOrigine() {
		return (IVersionDiplome) composantOrigine();
	}

	private void assignTo(EOVersionDiplome versionDiplome) {
		super.assignTo(versionDiplome);
		versionDiplome.setAnnee(this.annee());
	}

	@Override
	public String toString() {
		return this.getDiplome().toString();
	}

	/**
	 * Supprime les AP d'un EC s'ils n'ont pas de valeur
	 * @param edc editingContext
	 * @throws Exception exception lors de la suppression
	 */
	public void purgerAPnonUtilisees(EOEditingContext edc) throws Exception {
		
		NSArray<EOComposant> lesAPs = this.allSuccessors();
		lesAPs = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(lesAPs, EOComposant.TYPE_COMPOSANT.eq(EOTypeComposant.typeAP(edc)));
		List<IComposant> composantsParents = new ArrayList<IComposant>();
		for (IComposant ap : lesAPs) {
			if (ap instanceof IAP) {
				IComposant composantParent = purgeChargeEnseignementVideAP(edc, (IAP) ap);
				if (composantParent != null) {
					composantsParents.add(composantParent);
				}
			}
		}
		
		for (IComposant composant : composantsParents) {
			composant.renumeroterLienComposerEnfants(EOTypeComposant.typeAP(edc));
		}
			
	}

	private IComposant purgeChargeEnseignementVideAP(EOEditingContext edc, IAP ap) throws Exception {
		
		IComposant parent = null;
		
		if (chargeEnseignementVideOuNull(ap)) {
			
			if (ap.liensChildsWithoutDuplicate().size() == 1) {
				ILien lien = ap.liensChildsWithoutDuplicate().get(0);
				parent = lien.parent();
				((EOAP) ap).supprimer();
				((EOLien) lien).delete();
			}
		}
		
		return parent;
		
	}

	protected boolean chargeEnseignementVideOuNull(IAP ap) {
		return (ap.chargeEnseignementTheorique() == null 
				|| (ap.chargeEnseignementTheorique() != null && ap.chargeEnseignementTheorique().valeurMinutes() == null
					&& ap.chargeEnseignementTheorique().nbGroupes() == null));
	}
	
	/**
	 * Supprime les AE d'un EC s'ils n'ont pas de valeur
	 * @param edc editingContext
	 * @throws Exception exception lors de la suppression
	 */
	public void purgerAEnonUtilisees(EOEditingContext edc) throws Exception {
		
		NSArray<EOComposant> lesAEs = this.allSuccessors();
		lesAEs = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(lesAEs, EOComposant.TYPE_COMPOSANT.eq(EOTypeComposant.typeAE(edc)));
		List<IComposant> composantsParents = new ArrayList<IComposant>();
		for (IComposant ae : lesAEs) {
			if (ae instanceof IAE) {
				IComposant composantParent = purgeCoefficientEtDureeVideAE(edc, (IAE) ae);
				if (composantParent != null) {
					composantsParents.add(composantParent);
				}
			}
		}
		
		for (IComposant composant : composantsParents) {
			composant.renumeroterLienComposerEnfants(EOTypeComposant.typeAE(edc));
		}
			
	}
	
	private IComposant purgeCoefficientEtDureeVideAE(EOEditingContext edc, IAE ae) throws Exception {
		
		IComposant parent = null;
		
		if (ae.liensChildsWithoutDuplicate().size() == 1) {
			ILien lien = ae.liensChildsWithoutDuplicate().get(0);
			if (lien instanceof ILienComposer) {
				ILienComposer lienComposer = (ILienComposer) lien;
				if (lienComposer.coefficient() == null && dureeMaxAENulle(ae, lienComposer)) {
					((EOAE) ae).supprimer();
					parent = lienComposer.parent();
					((EOLienComposer) lienComposer).delete();
				}
			}
		}
		
		return parent;
	}

	protected boolean dureeMaxAENulle(IAE ae, ILienComposer lienComposer) {
		return (((EOLienComposer) lienComposer).dureeMax() == null) && ae.dureeMax() == null;
	}
	
}
