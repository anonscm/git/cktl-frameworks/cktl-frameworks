package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

/**
 * 
 * @author isabelle
 *
 */
public interface IJuryConvocation {

	Date dateConvocation();

	void setDateConvocation(Date value);

	Date dCreation();

	void setDCreation(Date value);

	Date dModification();

	void setDModification(Date value);

	Integer persIdCreation();

	void setPersIdCreation(Integer value);

	Integer persIdModification();

	void setPersIdModification(Integer value);

	String salleConvocation();

	void setSalleConvocation(String value);

	IJury toJury();

	void setToJuryRelationship(IJury value);
	
	Boolean valide();

	void setValide(Boolean value);


}