package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


/**
 * {@inheritDoc}
 */
public class EOCursus extends _EOCursus implements IScoCursus {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOCursus.class);


	/**
	 * @param edc editing context dans lequel on crée le pre-cursus
	 * @param persId identifiant de l'utilisateur qu crée le pre-cursus
	 * @return un pre-cursus
	 */
	public static EOCursus creer(EOEditingContext edc, Integer persId) {
		EOCursus cursus = (EOCursus) EOUtilities.createAndInsertInstance(edc, EOCursus.ENTITY_NAME);
		cursus.setPersIdCreation(persId);
		cursus.setPersIdMofication(persId);
		cursus.setDCreation(new NSTimestamp());
		cursus.setDModifcation(new NSTimestamp());
		cursus.setInterruptionEtud(false);
		return cursus;
	}

	
	
	/**
	 * {@inheritDoc}
	 */
	public void setToAccesTitreGradeUniversitaireRelationship(IGradeUniversitaire value) {
		super.setToAccesTitreGradeUniversitaireRelationship((EOGradeUniversitaire) value);
	}

	
	public void setToEtudiantRelationship(IInfosEtudiant value) {
		super.setToEtudiantRelationship((EOEtudiant) (((ScoEtudiant) value).getEtudiant()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantRelationship(IEtudiant value) {
		super.setToEtudiantRelationship((EOEtudiant) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToGradeUniversitaireRelationship(IGradeUniversitaire value) {
		super.setToGradeUniversitaireRelationship((EOGradeUniversitaire) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToTypeFormationRelationship(ITypeFormation value) {
	  super.setToTypeFormationRelationship((EOTypeFormation) value);
	  
  }

	/**
	 * {@inheritDoc}
	 */
	public void setToCRneRelationship(IRne value) {
	  super.setToCRneRelationship((EORne) value);	  
  }

	public void setToPaysRelationship(IPays value) {
		super.setToPaysRelationship((EOPays) value);
		
	}

	public void calculerTypeInscription(String codeRneEtablissement) {
		if (toCRne() == null) {
			setTypeInscription(null);
		} else if (toCRne().cRne().equals(codeRneEtablissement)) {
			setTypeInscription(ITypeInscription.CODE_LOCAL);
		} else if (toCRne().isUneUniversite()) {
			setTypeInscription(ITypeInscription.CODE_UNIV_FRANCAISE);
		} else {
			setTypeInscription(ITypeInscription.CODE_ENS_SUP);
		}
		
	}

		/**
		 * {@inheritDoc}
		 */
		public boolean supprimerSiSansValeurs() {
			if (toAccesTitreGradeUniversitaire() == null && anneeDebut() == null && anneeFin() == null && toCRne() == null && !interruptionEtud()) {
				setToEtudiantRelationship((EOEtudiant)null);
				delete();
				return true;
			}

			return false;
		}

}
