/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

/**
 * Element d'inscription pédagogique, attaché à un lien sur un élément pédagogique (UE, EC, Parcours, Regroupement, VersionDiplome)
 * @author isabelle
 */
public interface IInscriptionPedagogiqueElement {

	/**
	 * @return lien auquel est attaché l'element d'inscription pédagogique
	 */
	ILien toLien();

	/**
	 * @return nature de l'élement d'inscription pédagogique (exempté, déjà acquis, standard)
	 */
	ITypeInscriptionPeda toTypeInscriptionPeda();

	/**
	 * @return lien vers l'inscription pédagogique rattachée
	 */
	IInscriptionPedagogique toInscriptionPedagogique();

	/**
	 * @param note note que l'on enlève
	 */
	void removeFromToNotesRelationship(INote note);

	/**
	 * @param value inscription pédagogique rattachée
	 */
	void setToInscriptionPedagogiqueRelationship(IInscriptionPedagogique value);

	/**
	 * @param value lien auquel est attaché l'element d'inscription pédagogique
	 */
	void setToLienRelationship(ILien value);

	/**
	 * @param value nature de l'élement d'inscription pédagogique (exempté, déjà acquis, standard)
	 */
	void setToTypeInscriptionPedaRelationship(ITypeInscriptionPeda value);

	/**
	 * @return notes attachées à l'élement d'insscription pédagogique
	 */
	List<? extends INote> toNotes();

	/**
	 * 
	 * @return L'id du lien correspondant à l'élément
	 */
	Integer idLien();
	
	/**
	 * @return la dernière note renseignée (note de la session 1, 2 ou 3)
	 */
	INote noteFinale();
	
	/**
	 * @return la dernière note résultat renseignée (note de la session 1, 2 ou 3)
	 */
	INote noteResultatFinale();
	
	/**
	 * 
	 * @param numSession le numero de session
	 * @return la note associée à la session
	 */
	INote noteSession(Integer numSession);
	
	/**
	 * 
	 * @return Boolean : indique si l'ip element a des notes saisies ou non
	 */
	Boolean hasNotes();
	
	/**
	 * 
	 * @return l'id lien contexte 
	 */
	String idLienContexte();

	/**
	 * @return l'id de l'ip élément
	 */
	Integer id();
	
}
