package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * decrit un creditable
 */
public interface ICreditable {

	/**
	 * @return credit ECTS
	 */
	BigDecimal creditECTS();

	/**
	 * @return travaile personnel de l'etudiant
	 */
	BigDecimal travailEtudiant();

	/**
	 * @return base de la note
	 */
	Integer baseNote();

	/**
	 * @return 1 si on est sans note 0 sinon
	 */
	Boolean sansNote();
	
	/**
	 * @return l'id du composant correspondant
	 */
	Integer composantId();
	
	/**
	 * @return l'id du lien correspondant
	 */
	Integer lienId();

	/**
	 * @param ectsSurcharge credits ects
	 */
	void setCreditECTS(BigDecimal ectsSurcharge);

}
