// DO NOT EDIT.  Make changes to EOTypeInscriptionPeda.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeInscriptionPeda extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeInscriptionPeda";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeInscriptionPeda.class);

  public EOTypeInscriptionPeda localInstanceIn(EOEditingContext editingContext) {
    EOTypeInscriptionPeda localInstance = (EOTypeInscriptionPeda)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeInscriptionPeda.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeInscriptionPeda.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionPeda.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionPeda.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeInscriptionPeda.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeInscriptionPeda.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionPeda.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionPeda.LIBELLE_KEY);
  }


  public static EOTypeInscriptionPeda createSco_TypeInscriptionPeda(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeInscriptionPeda eo = (EOTypeInscriptionPeda) EOUtilities.createAndInsertInstance(editingContext, _EOTypeInscriptionPeda.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeInscriptionPeda> fetchSpec() {
    return new ERXFetchSpecification<EOTypeInscriptionPeda>(_EOTypeInscriptionPeda.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeInscriptionPeda> fetchAllSco_TypeInscriptionPedas(EOEditingContext editingContext) {
    return _EOTypeInscriptionPeda.fetchAllSco_TypeInscriptionPedas(editingContext, null);
  }

  public static NSArray<EOTypeInscriptionPeda> fetchAllSco_TypeInscriptionPedas(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeInscriptionPeda.fetchSco_TypeInscriptionPedas(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeInscriptionPeda> fetchSco_TypeInscriptionPedas(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeInscriptionPeda> fetchSpec = new ERXFetchSpecification<EOTypeInscriptionPeda>(_EOTypeInscriptionPeda.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeInscriptionPeda> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeInscriptionPeda fetchSco_TypeInscriptionPeda(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscriptionPeda.fetchSco_TypeInscriptionPeda(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscriptionPeda fetchSco_TypeInscriptionPeda(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeInscriptionPeda> eoObjects = _EOTypeInscriptionPeda.fetchSco_TypeInscriptionPedas(editingContext, qualifier, null);
    EOTypeInscriptionPeda eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeInscriptionPeda that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscriptionPeda fetchRequiredSco_TypeInscriptionPeda(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscriptionPeda.fetchRequiredSco_TypeInscriptionPeda(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscriptionPeda fetchRequiredSco_TypeInscriptionPeda(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeInscriptionPeda eoObject = _EOTypeInscriptionPeda.fetchSco_TypeInscriptionPeda(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeInscriptionPeda that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscriptionPeda localInstanceIn(EOEditingContext editingContext, EOTypeInscriptionPeda eo) {
    EOTypeInscriptionPeda localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
