package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * 
 * @author isabelle
 *
 */
public class EOJuryPersonnes extends _EOJuryPersonnes implements IJuryPersonnes {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOJuryPersonnes.class);


	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	public void setDCreation(Date value) {
		super.setDCreation((NSTimestamp) value);
	}

	public void setDModification(Date value) {
		super.setDModification((NSTimestamp) value);
	}

	public void setToIndividuRelationship(IIndividu value) {
		super.setToIndividuRelationship((EOIndividu) value);
	}

	public void setToJuryRelationship(IJury value) {
		super.setToJuryRelationship((EOJury) value);
	}

	public void setToJuryRoleRelationship(IJuryRole value) {
		super.setToJuryRoleRelationship((EOJuryRole) value);
	}
	
	/**
     * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
