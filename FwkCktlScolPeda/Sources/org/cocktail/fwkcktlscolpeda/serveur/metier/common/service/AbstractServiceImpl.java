package org.cocktail.fwkcktlscolpeda.serveur.metier.common.service;

import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.server.injection.CktlCurrentFlowProvider;
import org.cocktail.fwkcktlwebapp.server.injection.CktlFlow;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;


public class AbstractServiceImpl {

	protected EOEditingContext editingContext;
	
	@Inject
	private CktlCurrentFlowProvider flowProvider;
	
	@Inject
	protected UserInfo userInfo;

	public AbstractServiceImpl() {

	}
	
	public AbstractServiceImpl(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public void setFlowProvider(CktlCurrentFlowProvider flowProvider) {
		this.flowProvider = flowProvider;
	}

	public CktlFlow getFlow() {
		if (flowProvider != null) {
			return flowProvider.get();
		} else {
			return null;
		}
	}

	protected EOEditingContext editingContext() {
		try {
			CktlFlow flow = getFlow();
			if (flow != null) {
				return flow.getEditingContext();
			} 
		} catch (IllegalStateException e) {
			
		}
		
		return editingContext;

	}

	public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
	
}