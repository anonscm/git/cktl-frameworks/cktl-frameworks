package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Définit un perimetre de donnees pour l'application Girofle
 */
public class EOGdPerimetreGirofle extends _EOGdPerimetreGirofle {
	// Private fields
	private static final long serialVersionUID = 1L;

	@Inject
	@Nullable
	private UserInfo userInfo;

	// Constructor	
	// Properties

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public UserInfo getUserInfo() {
	    return userInfo;
    }
	
	// Public Methods	
	/**
	 * @param editingContext : contexte d'édition. Necessaire pour reveiller le mecanisme d'injection du user
	 * @return Un EOGdPerimetreGirofle cree et insere dans le contexte d'edition
	 */
	public static EOGdPerimetreGirofle create(EOEditingContext editingContext) {
		EOGdPerimetreGirofle eo = (EOGdPerimetreGirofle) createAndInsertInstance(editingContext, EOGdPerimetreGirofle.ENTITY_NAME);
		return eo;
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		actualiseInfoEnregistrement();
	}

	@Override
	public void validateBeforeTransactionSave() throws ValidationException {
		super.validateBeforeTransactionSave();
		actualiseInfoEnregistrement();
	}

	// Protected Methods
	// Private methods	
	private void actualiseInfoEnregistrement() {
		if ((userInfo != null) && (userInfo.persId() != null)) {
			if (persIdCreation() == null) {
				setPersIdCreation(userInfo.persId().intValue());
				setDateCreation(new NSTimestamp());
			}
			setPersIdModification(userInfo.persId().intValue());
			setDateModification(new NSTimestamp());
		}
	}

	/**
	 * @return EOGdPerimetreGirofle : le perimetre parent girofle
	 */
	public EOGdPerimetreGirofle perimetreGirofleParent() {
		EOGdPerimetreGirofle perimetreGirofleParent = null;

		if (perimetreParent() != null) {
			EOGdPerimetreGirofle.fetchRequiredSco_GdPerimetreGirofle(editingContext(), ERXQ.equals(EOGdPerimetreGirofle.ID_KEY, perimetreParentId()));
		}

		return perimetreGirofleParent;
	}


	/**
	 * @return la donnée du périmetre dépendant de la stratégie - utilisé pour affichage
	 */
	public String getLibelleDonnee() {
		String libelleDonnee = "";

		if (strategie().equals(EOGdStrategie.strategieAccesFormation(this.editingContext()))) {
			libelleDonnee = composant().libelle();
		}

		if (strategie().equals(EOGdStrategie.strategieAccesElementsFormation(this.editingContext()))) {
			libelleDonnee = composant().libelle();
		}

		if (strategie().equals(EOGdStrategie.strategieAccesElement(this.editingContext()))) {
			libelleDonnee = composant().libelle();
		}

		if (strategie().equals(EOGdStrategie.strategieAccesFormationsComposante(this.editingContext()))) {
			libelleDonnee = structure().libelle();
		}

		if (strategie().equals(EOGdStrategie.strategieAccesGrade(this.editingContext()))) {
			libelleDonnee = gradeUniversitaire().libelle();
		}

		return libelleDonnee;
	}
}
