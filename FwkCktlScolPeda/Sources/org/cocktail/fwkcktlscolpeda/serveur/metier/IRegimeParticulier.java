package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un régime particulier.
 * 
 * @author Pascal MACOUIN
 */
public interface IRegimeParticulier {

	Boolean affiliationSsEtudiante();

	void setAffiliationSsEtudiante(Boolean value);

	String code();

	void setCode(String value);

	String libelle();

	void setLibelle(String value);

	Integer ordre();

	void setOrdre(Integer value);

}