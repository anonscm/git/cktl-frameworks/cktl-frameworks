package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;

/**
 * @param <T> type de composant pour lequel on cherche les enfants
 */
public class ListeEnfants<T extends EOComposant> implements IListeComposantEnfantsStrategy<T> {

	private EOTypeComposant typeComposant;

	// Constructor
	@SuppressWarnings("unused")
	private ListeEnfants() {
	}

	/**
	 * @param typeComposant : de composants pour lequel on veut la liste des enfants de type EOAP
	 */
	public ListeEnfants(EOTypeComposant typeComposant) {
		this.typeComposant = typeComposant;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IListeComposantEnfantsStrategy#getListEnfants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)
	 */
	public List<T> getListEnfants(EOComposant composant) {
		List<T> enfants = new ArrayList<T>();

		EOTypeLien liencomposer = EOTypeLien.typeComposer(composant.editingContext());

		for (IComposant enfant : composant.childs(liencomposer, typeComposant, EOComposant.CODE.ascs())) {
			enfants.add((T) enfant);
		}
		return enfants;
	}

}
