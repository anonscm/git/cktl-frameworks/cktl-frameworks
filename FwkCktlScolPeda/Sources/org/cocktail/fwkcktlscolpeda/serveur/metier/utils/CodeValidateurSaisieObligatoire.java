package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.localization.ERXLocalizer;

/**
 * Validation du code interdisant un code null
 */
public class CodeValidateurSaisieObligatoire implements Validateur {

	/**
	 * {@inheritDoc}
	 */
	public String validateCode(String code, EOEditingContext editingContext) {

		if (code == null) {
			throw new ValidationException(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Composant.code.NullPropertyException"));
		}

		return code;
	}

}
