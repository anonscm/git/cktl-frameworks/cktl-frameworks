package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.Comparator;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteData;

/**
 * Un comparateur qui permet de voir la précédence
 * des périodes par rapport à leur ordre 
 */
public class PeriodeComparator implements Comparator<ILien> {

	private MaquetteData maquetteData;

	public PeriodeComparator(MaquetteData maquetteData) {
		this.maquetteData = maquetteData;
	}

	public int compare(ILien l1, ILien l2) {

		IComposant c1 = maquetteData.getComposant(l1.childId());
		IComposant c2 = maquetteData.getComposant(l2.childId());
		if (c1 instanceof EOPeriode && c2 instanceof EOPeriode) {
			IPeriode p1 = (IPeriode) c1;
			IPeriode p2 = (IPeriode) c2;
			if (p1.toTypePeriode().equals(p2.toTypePeriode())) {
				return p1.ordre().compareTo(p2.ordre());
			}
		}
		return 0;
	}

}