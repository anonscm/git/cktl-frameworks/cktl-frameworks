package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.Comparator;

/**
 * Cette classe rerésente des utilitaires pour écrire des {@link Comparator}.
 * 
 * @author Pascal MACOUIN
 */
public class ComparatorUtils {
	
	/**
	 * Un compareTo "Null Safe". Le <code>null</code> sont à la fin.
	 * @param o1 La première chaine
	 * @param o2 La seconde chaine
	 * @return {@link Comparator#compare(Object, Object)}
	 */
	public static int compareToNullSafe(String o1, String o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else if (o1 == null) {
			return -1;
		} else if (o2 == null) {
			return 1;
		} else {
			return o1.compareTo(o2);
		}
	}
	
	/**
	 * Un compareTo "Null Safe". Le <code>null</code> sont à la fin.
	 * @param o1 Le premier long
	 * @param o2 Le second long
	 * @return {@link Comparator#compare(Object, Object)}
	 */
	public static int compareToNullSafe(Long o1, Long o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else if (o1 == null) {
			return -1;
		} else if (o2 == null) {
			return 1;
		} else {
			return o1.compareTo(o2);
		}
	}

}
