package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Validation des codes de composants
 */
public interface Validateur {

	/**
	 * @param code code a valider
	 * @param editingContext contexte d'edition pour lequel on souhaite valider les code
	 * @return code valide
	 */
	String validateCode(String code, EOEditingContext editingContext);
}
