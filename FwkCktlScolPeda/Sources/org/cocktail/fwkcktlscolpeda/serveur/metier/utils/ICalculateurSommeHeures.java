package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

/**
 * Définition du calcul de la somme des heures
 */
public interface ICalculateurSommeHeures {
		
	
	
	/**
	 * Calcule récursivement la somme des heures qui doivent être effectuées par les enseignants 
	 * avant addition, les heures sont multipliées par le nombre de groupe et les coefficients "équivalents TD"
	 * @param lien  lien pour lequel on calcule la somme des heures enseignées
	 */
	void calculerSommeHeuresEnseignants(ILien lien);
	
	/**
	 * Calcule récursivement la somme des heures qui sont reçues par un étudiant
	 * les heures sont additionnées simplement
	 * @param lien  lien pour lequel on calcule la somme des heures enseignées
	 */
	void calculerSommeHeuresEtudiant(ILien lien);
	
	/**
	 * @param typeAPs : liste des types d'AP pour lesquels on veut calculer les heures
	 */
	void setTypeAPs(List<EOTypeAP> typeAPs);
	
}
