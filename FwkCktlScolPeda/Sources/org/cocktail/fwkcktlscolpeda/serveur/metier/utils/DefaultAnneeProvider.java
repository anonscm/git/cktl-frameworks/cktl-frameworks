package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;


/**
 * Classe d'implementation de l'annee de l'exercice
 */
public class DefaultAnneeProvider implements IAnneeProvider {

	private Integer anneeExercice;
	
	public Integer getAnneeExercice() {
		return this.anneeExercice;
	}

	public void setAnneeExercice(Integer annee) {
		this.anneeExercice = annee;
	}

}
