package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp;

/**
 * 
 * Classe contenant tous les codes des champs affichables dans l'interface.
 * 
 * @see EOChamp
 * 
 * @author Alexis Tual
 * 
 */
public class ParametragesConstantes {

	/** Paramètres champs cadreA */
	private static final String CODE_CADRE_A = "cadreA";
	private static final String CODE_CODE_INE = "codeIne";
	private static final String CODE_NUM_ETUD = "numEtud";
	private static final String CODE_INE_PROVISOIRE = "ineProvisoire";
	private static final String CODE_CIVILITE = "civilite";
	private static final String CODE_PHOTO = "photo";
	private static final String CODE_NOM_FAMILLE = "nomFamille";
	private static final String CODE_NOM_USAGE = "nomUsage";
	private static final String CODE_PRENOM = "prenom";
	private static final String CODE_PRENOM2 = "prenom2";
	private static final String CODE_DATE_NAISSANCE = "dateNaissance";
	private static final String CODE__VILLE_NAISSANCE = "VilleNaissance";
	private static final String CODE_DEPARTEMENT = "departement";
	private static final String CODE_PAYS_NAISSANCE = "paysNaissance";
	private static final String CODE_NATIONALITE = "nationalite";
	private static final String CODE_CODE_INSEE = "codeInsee";
	private static final String CODE_INSEE_PROVISOIRE = "inseeProvisoire";
	private static final String CODE_SIT_FAMILLIALE = "sitFamilliale";
	private static final String CODE_SIT_PROF = "sitProf";
	private static final String CODE_ETABLISSEMENT = "etablissement";
	private static final String CODE_PRECISION_PROF = "precisionProf";
	private static final String CODE_ENFANT_CHARGE = "enfantCharge";
	private static final String CODE_NOMBRE_ENFANT_CHARGE = "nbEnfantCharge";

	/** Paramètres champs cadreB */
	private static final String CODE_CADRE_B = "cadreB";
	private static final String CODE_TYPE_ADRESSE = "typeAdresse";
	private static final String CODE_ADRESSE = "adresse";
	private static final String CODE_COMPLEMENT = "complement";
	private static final String CODE_BOITE_POSTALE = "boitePostale";
	private static final String CODE_CODE_POSTAL = "codePostal";
	private static final String CODE_VILLE = "ville";
	private static final String CODE_PAYS = "pays";
	private static final String CODE_MAIL = "mail";
	private static final String CODE_MAIL_ETUDIANT = "mailEtudiant";
	private static final String CODE_CADRE_TEL = "cadreTel";
	private static final String CODE_TYPE_TEL = "typeTel";
	private static final String CODE_TYPE_NO_TEL = "typeNoTel";
	private static final String CODE_INDICATIF_PAYS = "indicatifPays";
	private static final String CODE_NUMERO_TEL = "numeroTel";

	/** Paramètres champs cadreC */
	private static final String CODE_CADRE_C = "cadreC";
	private static final String CODE__TYPE_INSCRIPTION = "TypeInscription";
	private static final String CODE_REGIME_INSCRIPTION = "regimeInscription";
	private static final String CODE_GRADE = "grade";
	private static final String CODE_NIVEAU = "niveau";
	private static final String CODE_PARCOURS = "parcours";
	private static final String CODE_SPECIALITE = "specialite";
	private static final String CODE_DIPLOME = "diplome";
	private static final String CODE_DATE_INSC = "dateInsc";
	private static final String CODE_ECH_INTERNATIONAL = "echInternational";
	private static final String CODE_REDOUBLEMENT = "redoublement";
	private static final String CODE_REORIENTATION = "reorientation";
	private static final String CODE_CONDITIONNEL = "conditionnel";
	private static final String CODE_CYCLE_AMENAGE = "cycleAmenage";
	private static final String CODE_ETABLISSEMENT_CUMUL = "EtablissementCumul";

	/** Paramètres champs cadreD */
	private static final String CODE_CADRE_D = "cadreD";
	private static final String CODE_CADRE_TITRE_ACCES_BAC = "CadreTitreAccessBac";
	private static final String CODE_ANNEE_OBTENTION = "anneeObtention";
	private static final String CODE_TITRE_ACCES = "titreAcces";
	private static final String CODE_MENTION_ACCES = "mentionAcces";
	private static final String CODE_ETABLISSEMENT_ACCES = "etablissementAcces";
	private static final String CODE_VILLE_ETABLISSEMENT = "villeEtablissement";
	private static final String CODE_DEPARTEMENT_ETABLISSEMENT = "departementEtablissement";
	private static final String CODE_PAYS_ETABLISSEMENT = "paysEtablissement";
	private static final String CODE_CADRE_TITRE_ACCES_FORMATION = "CadreTitreAccessFormation";
	private static final String CODE_ANNEE_OBTENTION_FORMATION = "anneeObtentionFormation";
	private static final String CODE_TITRE_ACCES_FORMATION = "titreAccesFormation";
	private static final String CODE_MENTION_ACCES_FORMATION = "mentionAccesFormation";
	private static final String CODE_ETABLISSEMENT_ACCES_FORMATION = "etabAccesFormation";
	private static final String CODE_VILLE_ETABLISSEMENT_FORMATION = "villeEtabFormation";
	private static final String CODE_DEPARTEMENT_ETABLISSEMENT_FORMATION = "departementEtabFormation";
	private static final String CODE_PAYS_ETABLISSEMENT_FORMATION = "paysEtabFormation";

	/** Paramètres champs cadreEFG */
	private static final String CODE_CADRE_E_F_G = "cadreEFG";
	private static final String CODE_ANNEE_DEBUT_PERIODE = "anneeDebutPeriode";
	private static final String CODE_ANNEEFIN_PERIODE = "anneefinPeriode";
	private static final String CODE_INTERRUPTION = "interruption";
	private static final String CODE_ETABLISSEMENT_CURSUS = "etablissementCursus";
	private static final String CODE_VILLE_CURSUS = "villeCursus";
	private static final String CODE_DEPARTEMENT_CURSUS = "departementCursus";
	private static final String CODE_PAYS_CURSUS = "paysCursus";
	private static final String CODE_TYPEFORMATION_CURSUS = "typeformationCursus";
	private static final String CODE_NIVEAU_CURSUS = "niveauCursus";
	private static final String CODE_INTITULEFORMATION_CURSUS = "intituleformationCursus";
	private static final String CODE_DIPLOME_CURSUS = "diplomeCursus";
	private static final String CODE_GRADE_FORMATION_CURSUS = "gradeFormationCursus";
	private static final String CODE_INTITULE_DIPLOME_CURSUS = "intituleDiplomeCursus";
	/** Paramètres champs cadre HIJ */
	private static final String CODE_CADRE_HIJ = "cadreHIJ";
	private static final String CODE_CADRE_BOURSES = "cadreBourses";
	private static final String CODE_EXONERATION = "exoneration";
	private static final String CODE_EXONERATION_OUI = "exonerationOui";
	private static final String CODE_ORGANISME_BOURSE = "organismeBourse";
	private static final String CODE_NUMEROALLOCATAIRE = "numeroallocataire";
	private static final String CODE_ECHELLON_BOURSE = "echellonBourse";
	private static final String CODE_FORMATION_BOURSE = "formationBourse";
	private static final String CODE_CAMPUS_FRANCE = "campusFrance";
	private static final String CODE_TYPE_REMBOURSEMENT = "typeRemboursement";
	
	private static final String CODE_CADRE_SIT_SOCIALE = "cadreSitSociale";
	private static final String CODE_EXONERATION_NON = "exonerationNon";
	private static final String CODE_INSEE_PROV_SIT_SOCIALE = "inseeProvSitSociale";
	private static final String CODE_ORIGINE_RESSOURCES = "origineRessources";
	private static final String CODE_TUTEUR1 = "tuteur1";
	private static final String CODE_TUTEUR2 = "tuteur2";
	private static final String CODE_PROFEESION_TUTEUR1 = "profeesionTuteur1";
	private static final String CODE_PROFEESION_TUTEUR2 = "profeesionTuteur2";
	private static final String CODE_RESPONSABLE_LEGAL1 = "responsableLegal1";
	private static final String CODE_RESPONSABLE_LEGAL2 = "responsableLegal2";
	
	private static final String CODE_CADRE_EXONERATION = "cadreExonerationRessource";
	private static final String CODE_CADRE_MUTUELLE = "cadreMutuelle";
	private static final String CODE_CHOIX_SIT_SOCIALE = "choixSituSociale";
	private static final String CODE_REGIME_PARTICULIER_PARENT = "regimeParticulierParent";
	private static final String CODE_CADRE_INFO_SIT_SOCIALE = "CadreInfoSituSociale";
	private static final String CODE_AFFILIATION_SECU = "affiliationSecu";
	private static final String CODE_MUTUELLE_ETUDIANT = "mutuelleEtudiant";
	private static final String CODE_ORGANISME_SECU = "organismeSecu";
	private static final String CODE_COTISATION_SECU = "cotisationSecu";
	private static final String CODE_NON_AFFILIE_SECU = "nonAffiliationSecu";
	private static final String CODE_ORGANISME_MUTUELLE = "OrganismeMutuelle";
	// gestionnaire
	private static final String CODE_SITUATION_PRINCIPALE = "situationPrincipale";
	private static final String CODE_SITUATION_EXONERATION = "situationExoneration";
	private static final String CODE_SECU_ETUDIANTE = "SecuSocialeEtudiante";
	private static final String CODE_MUTUELLE_ETUDIANTE = "MutuelleEtudiante";
	private static final String CODE_RESPONSABILITE_CIVILE = "responsabiliteCivile";
	private static final String CODE_NOM_TUTEUR1 = "nomTuteur1";
	private static final String CODE_PCS_TUTEUR1 = "PCSTuteur1";
	private static final String CODE_NOM_TUTEUR2 = "nomTuteur2";
	private static final String CODE_PCS_TUTEUR2 = "PCSTuteur2";

	

	/** Paramètres champs cadreK */
	private static final String CODE_CADRE_K = "cadreK";
	private static final String CODE_RENSEIGNEMENTS_DIVERS = "renseignementsDivers";
	private static final String CODE_INTERRUPTION_ETUDE = "interruptionEtude";
	private static final String CODE_DEBUT_INTERRUPTION_ETUDE = "debutInterruptionEtude";
	private static final String CODE_FIN_INTERRUPTION_ETUDE = "finInterruptionEtude";
	private static final String CODE_DEMANDEUR_EMPLOI = "demandeurEmploi";
	private static final String CODE_FORMATION_FINANCEE = "formationFinancee";
	private static final String CODE_SPORTIF_H_N = "sportifHN";
	private static final String CODE_NIVEAU_SPORTIF ="niveauSportif";
	private static final String CODE_SPORT_PRATIQUE ="sportPratique";
	private static final String CODE_HANDICAP = "handicap";
	private static final String CODE_TYPE_HANDICAP = "typeHandicap";
	private static final String CODE_ASSURANCE_RESP_CIVILE = "assuranceRespCivile";
	private static final String CODE_COMPAGNIE_ASSURANCE = "compagnieAssurance";
	private static final String CODE_NUMERO_ASSURANCE = "numeroAssurance";
	private static final String CODE_EMAIL_PARENT = "emailParent";
	private static final String CODE_COMMENTAIRE = "commentaires";
	private static final String CODE_CADRE_SERVICE_NATIONAL ="CadreServiceNational";
	private static final String CODE_ATTESTAIONRECENSEMENT = "attestaionrecensement";
	private static final String CODE_CERTIF_PARTICIP_APPEL = "certifParticipAppel";

	/** Paramètres champs cadreL */
	private static final String CODE_CADRE_L = "cadreL";
	private static final String CODE_CHARTE = "charte";
	private static final String CODE_ANNUAIRE = "annuaire";
	private static final String CODE_PHOTO_AUTRES = "photoAutres";
	private static final String CODE_LOGIN = "login";
	private static final String CODE_MELAUTRES = "melautres";
	private static final String CODE_MOT_DE_PASSE = "motDePasse";
	private static final String CODE_MOT_DE_PASSE2 = "motDePasse2";
	private static final String CODE_OBSERVATIONS_AUTRES = "observationsAutres";
	
	public String getCodeIne() {
		return CODE_CODE_INE;
	}

	public String getCodeNumEtud() {
		return CODE_NUM_ETUD;
	}
	public String getCodeCadreA() {
		return CODE_CADRE_A;
	}

	public String getCodeCodeIne() {
		return CODE_CODE_INE;
	}

	public String getCodeIneProvisoire() {
		return CODE_INE_PROVISOIRE;
	}

	public String getCodeCivilite() {
		return CODE_CIVILITE;
	}

	public String getCodePhoto() {
		return CODE_PHOTO;
	}

	public String getCodeNomFamille() {
		return CODE_NOM_FAMILLE;
	}

	public String getCodeNomUsage() {
		return CODE_NOM_USAGE;
	}

	public String getCodePrenom() {
		return CODE_PRENOM;
	}

	public String getCodePrenom2() {
		return CODE_PRENOM2;
	}

	public String getCodeDateNaissance() {
		return CODE_DATE_NAISSANCE;
	}

	public String getCodeVilleNaissance() {
		return CODE__VILLE_NAISSANCE;
	}

	public String getCodeDepartement() {
		return CODE_DEPARTEMENT;
	}

	public String getCodePaysNaissance() {
		return CODE_PAYS_NAISSANCE;
	}

	public String getCodeNationalite() {
		return CODE_NATIONALITE;
	}

	public String getCodeCodeInsee() {
		return CODE_CODE_INSEE;
	}

	public String getCodeInseeProvisoire() {
		return CODE_INSEE_PROVISOIRE;
	}

	public String getCodeSitFamilliale() {
		return CODE_SIT_FAMILLIALE;
	}

	public String getCodeSitProf() {
		return CODE_SIT_PROF;
	}

	public String getCodeEtablissement() {
		return CODE_ETABLISSEMENT;
	}

	public String getCodePrecisionProf() {
		return CODE_PRECISION_PROF;
	}

	public String getCodeEnfantCharge() {
		return CODE_ENFANT_CHARGE;
	}
	
	public String getCodeNombreEnfantCharge() {
		return CODE_NOMBRE_ENFANT_CHARGE;
	}
	
	public String getCodeCadreB() {
		return CODE_CADRE_B;
	}

	public String getCodeTypeAdresse() {
		return CODE_TYPE_ADRESSE;
	}

	public String getCodeAdresse() {
		return CODE_ADRESSE;
	}

	public String getCodeComplement() {
		return CODE_COMPLEMENT;
	}

	public String getCodeBoitePostale() {
		return CODE_BOITE_POSTALE;
	}

	public String getCodeCodePostal() {
		return CODE_CODE_POSTAL;
	}

	public String getCodeVille() {
		return CODE_VILLE;
	}

	public String getCodePays() {
		return CODE_PAYS;
	}

	public String getCodeMail() {
		return CODE_MAIL;
	}

	public String getCodeMailEtudiant() {
		return CODE_MAIL_ETUDIANT;
	}
	
	public String getCodeCadreTel() {
		return CODE_CADRE_TEL;
	}

	public String getCodeTypeTel() {
		return CODE_TYPE_TEL;
	}

	public String getCodeTypeNoTel() {
		return CODE_TYPE_NO_TEL;
	}

	public String getCodeIndicatifPays() {
		return CODE_INDICATIF_PAYS;
	}

	public String getCodeNumeroTel() {
		return CODE_NUMERO_TEL;
	}

	public String getCodeCadreC() {
		return CODE_CADRE_C;
	}

	public String getCodeTypeInscription() {
		return CODE__TYPE_INSCRIPTION;
	}

	public String getCodeRegimeInscription() {
		return CODE_REGIME_INSCRIPTION;
	}
	
	public String getCodeGrade() {
		return CODE_GRADE;
	}

	public String getCodeNiveau() {
		return CODE_NIVEAU;
	}

	public String getCodeDiplome() {
		return CODE_DIPLOME;
	}

	public String getCodeParcours() {
		return CODE_PARCOURS;
	}
	
	public String getCodeSpecialite() {
		return CODE_SPECIALITE;
	}
	
	public String getCodeDateInsc() {
		return CODE_DATE_INSC;
	}

	public String getCodeEchInternational() {
		return CODE_ECH_INTERNATIONAL;
	}

	public String getCodeRedoublement() {
		return CODE_REDOUBLEMENT;
	}

	public String getCodeReorientation() {
		return CODE_REORIENTATION;
	}

	public String getCodeConditionnel() {
		return CODE_CONDITIONNEL;
	}

	public String getCodeCycleAmenage() {
		return CODE_CYCLE_AMENAGE;
	}

	public String getCodeEtablissementCumul() {
		return CODE_ETABLISSEMENT_CUMUL;
	}
	
	public String getCodeCadreD() {
		return CODE_CADRE_D;
	}
	public String getCadreTitreAccessBac(){
		return CODE_CADRE_TITRE_ACCES_BAC;
	}
	
	public String getCodeAnneeObtention() {
		return CODE_ANNEE_OBTENTION;
	}

	public String getCodeTitreAcces() {
		return CODE_TITRE_ACCES;
	}

	public String getCodeMentionAcces() {
		return CODE_MENTION_ACCES;
	}

	public String getCodeEtablissementAcces() {
		return CODE_ETABLISSEMENT_ACCES;
	}

	public String getCodeVilleEtablissement() {
		return CODE_VILLE_ETABLISSEMENT;
	}

	public String getCodeDepartementEtablissement() {
		return CODE_DEPARTEMENT_ETABLISSEMENT;
	}

	public String getCodePaysEtablissement() {
		return CODE_PAYS_ETABLISSEMENT;
	}
	
	public String getCadreTitreAccessFormation(){
		return CODE_CADRE_TITRE_ACCES_FORMATION;
	}
	
	public String getCodeAnneeObtentionFormation() {
		return CODE_ANNEE_OBTENTION_FORMATION;
	}

	public String getCodeTitreAccesFormation() {
		return CODE_TITRE_ACCES_FORMATION;
	}

	public String getCodeMentionAccesFormation() {
		return CODE_MENTION_ACCES_FORMATION;
	}

	public String getCodeEtabAccesFormation() {
		return CODE_ETABLISSEMENT_ACCES_FORMATION;
	}

	public String getCodeVilleEtabFormation() {
		return CODE_VILLE_ETABLISSEMENT_FORMATION;
	}

	public String getCodeDepartementEtabFormation() {
		return CODE_DEPARTEMENT_ETABLISSEMENT_FORMATION;
	}

	public String getCodePaysEtabFormation() {
		return CODE_PAYS_ETABLISSEMENT_FORMATION;
	}


	public String getCodeCadreEFG() {
		return CODE_CADRE_E_F_G;
	}

	public String getCodeAnneeDebutPeriode() {
		return CODE_ANNEE_DEBUT_PERIODE;
	}

	public String getCodeAnneefinPeriode() {
		return CODE_ANNEEFIN_PERIODE;
	}

	public String getCodeInterruption() {
		return CODE_INTERRUPTION;
	}

	public String getCodeEtablissementCursus() {
		return CODE_ETABLISSEMENT_CURSUS;
	}

	public String getCodeVilleCursus() {
		return CODE_VILLE_CURSUS;
	}

	public String getCodeDepartementCursus() {
		return CODE_DEPARTEMENT_CURSUS;
	}

	public String getCodePaysCursus() {
		return CODE_PAYS_CURSUS;
	}

	public String getCodeTypeformationCursus() {
		return CODE_TYPEFORMATION_CURSUS;
	}

	public String getCodeNiveauCursus() {
		return CODE_NIVEAU_CURSUS;
	}

	public String getCodeIntituleformationCursus() {
		return CODE_INTITULEFORMATION_CURSUS;
	}

	public String getCodeDiplomeCursus() {
		return CODE_DIPLOME_CURSUS;
	}

	public String getCodeGradeFromationCursus() {
		return CODE_GRADE_FORMATION_CURSUS;
	}

	public String getCodeIntituleDiplomeCursus() {
		return CODE_INTITULE_DIPLOME_CURSUS;
	}

	public String getCodeCadreHIJ() {
		return CODE_CADRE_HIJ;
	}

	public String getCodeCadreBourses() {
		return CODE_CADRE_BOURSES;
	}

	public String getCodeExoneration() {
		return CODE_EXONERATION;
	}

	public String getCodeExonerationOui() {
		return CODE_EXONERATION_OUI;
	}

	public String getCodeOrganismeBourse() {
		return CODE_ORGANISME_BOURSE;
	}

	public String getCodeNumeroallocataire() {
		return CODE_NUMEROALLOCATAIRE;
	}

	public String getCodeEchellonBourse() {
		return CODE_ECHELLON_BOURSE;
	}

	public String getCodeFormationBourse() {
		return CODE_FORMATION_BOURSE;
	}
	public String getCodeCampusFrance() {
		return CODE_CAMPUS_FRANCE;
	}
	public String getCodeTypeRemboursement() {
		return CODE_TYPE_REMBOURSEMENT;
	}

	public String getCodeCadreSitSociale() {
		return CODE_CADRE_SIT_SOCIALE;
	}

	public String getCodeExonerationNon() {
		return CODE_EXONERATION_NON;
	}

	public String getCodeInseeProvSitSociale() {
		return CODE_INSEE_PROV_SIT_SOCIALE;
	}

	public String getCodeOrigineRessources() {
		return CODE_ORIGINE_RESSOURCES;
	}

	public String getCodeTuteur1() {
		return CODE_TUTEUR1;
	}

	public String getCodeTuteur2() {
		return CODE_TUTEUR2;
	}

	public String getCodeProfeesionTuteur1() {
		return CODE_PROFEESION_TUTEUR1;
	}

	public String getCodeProfeesionTuteur2() {
		return CODE_PROFEESION_TUTEUR2;
	}

	public String getCodeCadreExonerationRessource() {
		return CODE_CADRE_EXONERATION;
	}

	public String getCodeCadreMutuelle() {
		return CODE_CADRE_MUTUELLE;
	}

	public String getCodeChoixSituSociale() {
		return CODE_CHOIX_SIT_SOCIALE;
	}

	public String getCodeRegimeParticulierParent() {
		return CODE_REGIME_PARTICULIER_PARENT;
	}
	
//	public String getCodeCadresecu() {
//		return CODE_CADRESECU;
//	}

	public String getCodeAffiliationSecu() {
		return CODE_AFFILIATION_SECU;
	}

	public String getCodeMutuelleEtudiant() {
		return CODE_MUTUELLE_ETUDIANT;
	}

	public String getCodeOrganismeSecu() {
		return CODE_ORGANISME_SECU;
	}

	public String getCodeCotisationSecu() {
		return CODE_COTISATION_SECU;
	}

	public String getCodeNonAffiliationSecu() {
		return CODE_NON_AFFILIE_SECU;
	}

//	public String getCodeCadreComplement() {
//		return CODE_CADRE_COMPLEMENT;
//	}


	public String getCodeOrganismeMutuelle() {
		return CODE_ORGANISME_MUTUELLE;
	}

	public String getCodeCadreK() {
		return CODE_CADRE_K;
	}

	public String getRenseignementsDivers() {
		return CODE_RENSEIGNEMENTS_DIVERS;
	}
	
	public String getCodeInterruptionEtude() {
		return CODE_INTERRUPTION_ETUDE;
	}

	public String getCodeDebutInterruptionEtude() {
		return CODE_DEBUT_INTERRUPTION_ETUDE;
	}

	public String getCodeFinInterruptionEtude() {
		return CODE_FIN_INTERRUPTION_ETUDE;
	}

	public String getCodeDemandeurEmploi() {
		return CODE_DEMANDEUR_EMPLOI;
	}

	public String getCodeFormationFinancee() {
		return CODE_FORMATION_FINANCEE;
	}

	public String getCodeSportifHN() {
		return CODE_SPORTIF_H_N;
	}

	public String getCodeNiveauSportif() {
		return CODE_NIVEAU_SPORTIF;
	}

	public String getCodeSportPratique() {
		return CODE_SPORT_PRATIQUE;
	}

	public String getCodeHandicap() {
		return CODE_HANDICAP;
	}

	public String getCodeTypeHandicap() {
		return CODE_TYPE_HANDICAP;
	}

	public String getCodeAssuranceRespCivile() {
		return CODE_ASSURANCE_RESP_CIVILE;
	}

	public String getCodeCompagnieAssurance() {
		return CODE_COMPAGNIE_ASSURANCE;
	}

	public String getCodeNumeroAssurance() {
		return CODE_NUMERO_ASSURANCE;
	}

	public String getCodeEmailParent() {
		return CODE_EMAIL_PARENT;
	}
	public String getCodeCommentaires() {
		return CODE_COMMENTAIRE;
	}

	public String getCodeCadreServiceNational() {
		return CODE_CADRE_SERVICE_NATIONAL;
	}
	
	public String getCodeAttestaionrecensement() {
		return CODE_ATTESTAIONRECENSEMENT;
	}

	public String getCodeCertifParticipAppel() {
		return CODE_CERTIF_PARTICIP_APPEL;
	}

	public String getCodeCadreL() {
		return CODE_CADRE_L;
	}

	public String getCodeCharte() {
		return CODE_CHARTE;
	}

	public String getCodeAnnuaire() {
		return CODE_ANNUAIRE;
	}

	public String getCodePhotoAutres() {
		return CODE_PHOTO_AUTRES;
	}

	public String getCodeLogin() {
		return CODE_LOGIN;
	}

	public String getCodeMelautres() {
		return CODE_MELAUTRES;
	}

	public String getCodeMotDePasse() {
		return CODE_MOT_DE_PASSE;
	}

	public String getCodeMotDePasse2() {
		return CODE_MOT_DE_PASSE2;
	}

	public String getCodeObservationsAutres() {
		return CODE_OBSERVATIONS_AUTRES;
	}

	public String getCodeCadreInfoSituSociale() {
		return CODE_CADRE_INFO_SIT_SOCIALE;
	}
	
	public String getCodeResponsableLegal1() {
		return CODE_RESPONSABLE_LEGAL1;
	}
	
	public String getCodeResponsableLegal2() {
		return CODE_RESPONSABLE_LEGAL2;
	}

	public String getCodeSituationPrincipale() {
		return CODE_SITUATION_PRINCIPALE;
	}

	public String getCodeSituationExoneration() {
		return CODE_SITUATION_EXONERATION;
	}

	public String getCodeSecuEtudiante() {
		return CODE_SECU_ETUDIANTE;
	}

	public String getCodeMutuelleEtudiante() {
		return CODE_MUTUELLE_ETUDIANTE;
	}

	public String getCodeResponsabiliteCivile() {
		return CODE_RESPONSABILITE_CIVILE;
	}

	public String getCodeNomTuteur1() {
		return CODE_NOM_TUTEUR1;
	}

	public String getCodePcsTuteur1() {
		return CODE_PCS_TUTEUR1;
	}

	public String getCodeNomTuteur2() {
		return CODE_NOM_TUTEUR2;
	}

	public String getCodePcsTuteur2() {
		return CODE_PCS_TUTEUR2;
	}
}
