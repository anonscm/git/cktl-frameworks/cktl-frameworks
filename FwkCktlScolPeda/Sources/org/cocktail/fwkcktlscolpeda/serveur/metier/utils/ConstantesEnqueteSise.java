package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenant les constantes pour l'enquete SISE
 * @author isabelle
 *
 */
public class ConstantesEnqueteSise {
	
	// constantes pour remplir la variable SITUPRE : situation de l'anne precedente
	
	public static String getBacCodeSiseEnseignementSecondaireEtranger() {
		return "0031";
	}
	
	/** 
	 * liste des code rne  correspondant aux établissement pour lequels SITUPRE= E
	 * @return listeRneE
	 */
	public static List<String> getListeRneSitupreE() {
		List<String> listeRneE = new ArrayList<String>();
		listeRneE.add("0900424X");
		listeRneE.add("0601223D");
		listeRneE.add("0101060Y");
		listeRneE.add("0541564G");
		listeRneE.add("0311381H");
		listeRneE.add("0381912X");
		listeRneE.add("0410981U");
		return listeRneE;
	}
	
	public static String getTypeFormationPourSitupreD() {
		return "CPGE";
	}
	
	public static List<String> getTypeFormationPourSitupreC() {
		List<String> liste = new ArrayList<String>();
		liste.add("DUT");
		liste.add("LIP");
		return liste;
	}
	
	public static List<String> getTypeFormationPourSitupreE() {
		List<String> liste = new ArrayList<String>();
		liste.add("DI");
		liste.add("DIS");
		return liste;
		
	}
	
	public static String getTypeFormationPourSitupreR() {
		return "EFEE";
	}
	
	public static List<String> getTypeFormationPourSitupreH() {
		List<String> liste = new ArrayList<String>();
		liste.add("LIC");
		liste.add("MAS");
		liste.add("DOC");
		return liste;
	}
	
	public static List<String> getListeRneSitupreH() {
		List<String> listeRneE = new ArrayList<String>();
		listeRneE.add("0750736T");

		return listeRneE;
	}
	
	public static List<String> getListeRneSitupreK() {
		List<String> listeRneE = new ArrayList<String>();
		listeRneE.add("0940607Z");
		listeRneE.add("069047XK");
		listeRneE.add("0590344D");
		listeRneE.add("0690195M");
		listeRneE.add("0490811R");
		listeRneE.add("0753541S");
		listeRneE.add("0310155A");
		listeRneE.add("0753494R");
		listeRneE.add("0753742K");
		listeRneE.add("0753486G");
		listeRneE.add("0753478Y");
		listeRneE.add("0692459Y");
		listeRneE.add("0851415B");
		listeRneE.add("0753488J");
		listeRneE.add("0753428U");
		listeRneE.add("0753431X");
		listeRneE.add("0753496T");
		listeRneE.add("0690193K");
		listeRneE.add("0940608A");
		listeRneE.add("0310153Y");
		listeRneE.add("0783043J");

		return listeRneE;
	}

	// constantes pour remplir la variable DIPDER : type du dernier diplome obtenu

	
	public static List<String> getTypeFormationPourDipderDUT() {
		List<String> liste = new ArrayList<String>();
		liste.add("DUT");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderCPGE() {
		List<String> liste = new ArrayList<String>();
		liste.add("CPGE");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderDAEU() {
		List<String> liste = new ArrayList<String>();
		liste.add("DAEU");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderDEUG() {
		List<String> liste = new ArrayList<String>();
		liste.add("DEUG");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderLicences() {
		List<String> liste = new ArrayList<String>();
		liste.add("LIC");
		liste.add("LIP");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderMaitrise() {
		List<String> liste = new ArrayList<String>();
		liste.add("MAI");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderMaster() {
		List<String> liste = new ArrayList<String>();
		liste.add("MAS");
		return liste;
	}
	public static List<String> getTypeFormationPourDipderIngenieur() {
		List<String> liste = new ArrayList<String>();
		liste.add("DI");
		liste.add("DIS");
		return liste;
	}
	
	public static List<String> getTypeFormationPourDipderCertificat() {
		List<String> liste = new ArrayList<String>();
		liste.add("CC");
		return liste;
	}
	
	
	// Constante pour remplir la variable CURPAR
	
	public static List<String> getTypeEtabBTS() {
		List<String> liste = new ArrayList<String>();
		liste.add("LA");
		liste.add("LGT");
		liste.add("LPO");
		liste.add("LP");
		liste.add("LTD");
		liste.add("EFE");
		return liste;		
	}
	
	public static List<String> getTypeEtabEcoleIngenieur() {
		List<String> liste = new ArrayList<String>();
		liste.add("EI");
		return liste;		
	}
	
	public static List<String> getTypeEtabEtablissementPrive() {
		List<String> liste = new ArrayList<String>();
		liste.add("ICA");
		return liste;		
	}
	
	public static List<String> getTypeEtabEtablissementEtranger() {
		List<String> liste = new ArrayList<String>();
		liste.add("EET");
		return liste;		
	}
	
	public static List<String> getTypeEtabEcoleNormale() {
		List<String> liste = new ArrayList<String>();
		liste.add("ENS");
		return liste;		
	}
	
	public static List<String> getTypeEtabAutreEcole() {
		List<String> liste = new ArrayList<String>();
		liste.add("IUT");
		liste.add("AE");
		liste.add("EMA");
		return liste;		
	}
	
	public static List<String> getTypeEtabUniversite() {
		List<String> liste = new ArrayList<String>();
		liste.add("UNI");
		return liste;		
	}
}
