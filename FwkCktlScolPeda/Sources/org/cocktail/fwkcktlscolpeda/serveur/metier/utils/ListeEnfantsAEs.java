package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;

/**
 * Implémente la stratégie pour renvoyer une liste des enfants APs
 */
public class ListeEnfantsAEs implements IListeComposantEnfantsStrategy<EOAE> {
	// Private fields
	private EOTypeAE typeAE = null;

	// Constructor
	@SuppressWarnings("unused")
	private ListeEnfantsAEs() {
	}

	/**
	 * @param typeAE : type d'AP pour lequel on veut la liste des enfants de type EOAP
	 */
	public ListeEnfantsAEs(EOTypeAE typeAE) {
		this.typeAE = typeAE;
	}

	// Public Methods
	/*
	 * (non-Javadoc)
	 * @see
	 * org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IListComposantEnfantsStrategy#getListEnfants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)
	 */
	public List<EOAE> getListEnfants(EOComposant composant) {
		if (typeAE == null) {
			return null;
		}
		List<EOAE> aps = new ArrayList<EOAE>();

		EOTypeLien liencomposer= EOTypeLien.typeComposer(composant.editingContext());
		EOTypeComposant typeComposant = EOTypeComposant.typeAE(composant.editingContext());
		
		for (IComposant enfant : composant.childs(liencomposer, typeComposant, EOComposant.CODE.ascs())) {
			if (((EOAE) enfant).typeAE().equals(typeAE)) {
				aps.add((EOAE) enfant);
			}
		}
		return aps;
	}

}
