package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Classe renvoyant l'annee de l'exercice
 */
public interface IAnneeProvider {
	
	/**
	 * @return Integer : annee de l'exercice
	 */
	Integer getAnneeExercice();
	
	/**
	 * Set l'annee de l'exercice
	 * @param annee : annee de selection de l'exercice
	 */
	void setAnneeExercice(Integer annee);
	
}
