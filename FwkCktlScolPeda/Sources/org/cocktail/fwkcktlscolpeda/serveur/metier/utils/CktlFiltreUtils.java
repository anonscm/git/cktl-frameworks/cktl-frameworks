package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
/**
 * Classe utilitaire pour les filtres des CktlAjaxSimpleTableView
 * @author isabelle
 *
 */
public class CktlFiltreUtils {
	
	/**
	 * constructeur
	 */
	public CktlFiltreUtils() {
		super();
	}
	
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	public EOQualifier getContainsQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.contains(nomColonne, filtre);
		}
		return qualifier;
	}
	
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	public EOQualifier getLikeQualifier(String nomColonne, String filtre) {
		filtre = StringCtrl.replace(filtre, "%", "*");
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.likeInsensitive(nomColonne, filtre);
		}
		return qualifier;
	}
	
	/**
	 * construit la condition egal sur une colonne booléenne.
	 * @param nomColonne nom de la colonne sur laquelle on fait la recherche.
	 * @param filtre vide, '0', 'n' ou 'non' vallent <code>false</code>, tout le reste vaut <code>true</code>
	 * @return EOQualifier un qualifier egal (<code>null</code> si le filtre est <code>null</code>)
	 */
	public EOQualifier getEqualsBooleanQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			Boolean valeurFiltre = Boolean.TRUE;

			if (StringCtrl.isEmpty(filtre) || "0".equals(filtre) || "n".equalsIgnoreCase(filtre) || "non".equalsIgnoreCase(filtre)) {
				valeurFiltre = Boolean.FALSE;
			}

			qualifier = ERXQ.equals(nomColonne, valeurFiltre);
		}

		return qualifier;
	}
	
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	public EOQualifier getEqualsQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.equals(nomColonne, filtre);
		}
		return qualifier;
	}
	
	/**
	 * Indique si la valeur isLike le filtre
	 * @param valeur : la valeur a analyser
	 * @param filtre : le filtre recherche
	 * @return Boolean : indique si oui si la valeur est like le filtre
	 */
	public Boolean isLikePattern(String valeur, String filtre) {
		filtre = StringCtrl.replace(filtre, "%", "*");
		if (StringCtrl.like(valeur, filtre)) {
			return true;
		}
		
		return false;
	}
	
}
