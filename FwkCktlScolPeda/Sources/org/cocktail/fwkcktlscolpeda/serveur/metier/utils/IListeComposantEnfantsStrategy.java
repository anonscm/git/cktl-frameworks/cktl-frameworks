package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import java.util.List;

/**
 * Définition d'une stratégie de récupération d'une liste d'éléments
 * @param <T> : classe des enfants (dérive de EOComposant)
 */
public interface IListeComposantEnfantsStrategy<T extends EOComposant> {

	/**
	 * @param composant : composant pour lequel on recherche les enfants
	 * @return : liste des enfants d'un composant suivant une stratégie donnée.
	 * exemple liste des enfants d'un composant de type AP et de typaAP Cours	 
	 */
	List<T> getListEnfants(EOComposant composant);
	
}
