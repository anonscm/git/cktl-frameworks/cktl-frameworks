package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteData;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteElementProvider;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Implémentation du calcul des sommes d'heures
 */
public class CalculateurSommeHeure implements ICalculateurSommeHeures {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CalculateurSommeHeure.class);

	// Private fields
	private List<EOTypeAP> typeAPs;
	private EOEditingContext editingContext;
	private MaquetteElementProvider maquetteElementProvider;

	/**
	 * @param editingContext : contexte d'edition necessaire çà la récupération des types d'AP par défaut
	 * @param maquetteData maquetteData préchargée
	 */
	public CalculateurSommeHeure(EOEditingContext editingContext, MaquetteData maquetteData) {
		setEditingContext(editingContext);
		maquetteElementProvider = new MaquetteElementProvider(maquetteData);
		setTypeAPs(getDefaultTypesAP());
	}

	// Properties
	public List<EOTypeAP> getTypeAPs() {
		return typeAPs;
	}

	public void setTypeAPs(List<EOTypeAP> typeAPs) {
		this.typeAPs = typeAPs;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	// Public Methods
	/**
	 * {@inheritDoc}
	 */
	public void calculerSommeHeuresEnseignants(ILien lien) {
		calculerSommeHeuresEnseigneesEnfants(lien, false);

	}

	/**
	 * {@inheritDoc}
	 */
	public void calculerSommeHeuresEtudiant(ILien lien) {
		calculerSommeHeuresEnseigneesEnfants(lien, true);
	}

	// Private methods
	/**
	 * Calcule récursivement la somme des heures enseignees pour un lien et tous ses enfants
	 * @param lien lien pour lequel on calcule la somme des heures enseignées
	 * @param calculHeureEtudiant <code> true </code> si l'on fait le calcul de la somme des heures pour un étudiant, <code> false </code> si l'on calcul la somme
	 *          des heures qui doivent être effectuées par les enseignants
	 */
	private void calculerSommeHeuresEnseigneesEnfants(ILien lien, Boolean calculHeureEtudiant) {
		lien.setSommeMinutesEnseigneesCalculee(null);
		
		for (ILien l : maquetteElementProvider.getLiensAvecComposantParent(maquetteElementProvider.getChildAvecLien(lien))) {
			calculerSommeHeuresEnseigneesEnfants(l, calculHeureEtudiant);
		}

		List<Integer> sommePourTypeAP = new ArrayList<Integer>();
		for (EOTypeAP typeAP : getTypeAPs()) {
			Integer sommetypeAP = sommeHeuresEnseigneesEnfants(lien, typeAP, calculHeureEtudiant);
			if (sommetypeAP.doubleValue() > 0) {
				lien.setSommeMinutesEnseigneesCalculeePourTypeAP(sommetypeAP, typeAP);
				sommePourTypeAP.add(sommetypeAP);
			}
		}

		Integer somme = sommeMinutes(sommePourTypeAP);
		if (somme > 0) {
			lien.setSommeMinutesEnseigneesCalculee(somme);
		}

	}

	private Integer sommeHeuresEnseigneesEnfants(ILien lienParent, EOTypeAP typeAP, Boolean calculHeureEtudiant) {
		List<Integer> listeMinutes = new ArrayList<Integer>();

		for (ILien lien : maquetteElementProvider.getLiensAvecComposantParent(maquetteElementProvider.getChildAvecLien(lienParent))) {
			if (lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAP) != null) {
				listeMinutes.add(lien.getSommeMinutesEnseigneesCalculeePourTypeAP(typeAP));
			} else {
				if (lienPrisEnCompte(lien)) {
					Integer sommeMinutes = getMinutesEnseignees(maquetteElementProvider.getChildAvecLien(lien), typeAP, calculHeureEtudiant);
					if (sommeMinutes != null) {
						listeMinutes.add(sommeMinutes);
					}
				}
			}
		}
		return sommeMinutes(listeMinutes);
	}

	private boolean lienPrisEnCompte(ILien lien) {
		if (maquetteElementProvider.getChildAvecLien(lien) instanceof IAP) {
			return true;
		} else {
			return (lien instanceof ILienComposer && ((ILienComposer) lien).obligatoire() != null && ((ILienComposer) lien).obligatoire());
		}
	}

	private Integer getMinutesEnseignees(IComposant composant, EOTypeAP typeAP, Boolean calculHeureEtudiant) {
		if (composant instanceof EOAP) {
			EOAP ap = (EOAP) composant;
			if (typeAP.code().equals(ap.typeAP().code())) {
				return getminutes(ap, calculHeureEtudiant);
			}
		}
		return null;
	}

	private Integer getminutes(EOAP ap, Boolean calculHeureEtudiant) {
		IChargeEnseignement chargeEnseignementTheorique = maquetteElementProvider.getChargeEnseignement(ap);
		Integer valeurMinutes = null;
		if (chargeEnseignementTheorique != null) {
			valeurMinutes = chargeEnseignementTheorique.valeurMinutes();
			if (!calculHeureEtudiant) {
				Integer nbGroupes = chargeEnseignementTheorique.nbGroupes();
				valeurMinutes = multiplierMinute(valeurMinutes, nbGroupes, ap.typeAP().heureEquivalent());
			}
		}
		return valeurMinutes;
	}

	private Integer sommeMinutes(List<Integer> listeValeurMinutes) {
		Integer dureeEnMinutes = 0;

		for (Integer valeurMinutes : listeValeurMinutes) {
			dureeEnMinutes += valeurMinutes;
		}

		return dureeEnMinutes;
	}

	private Integer multiplierMinute(Integer minutes, Integer nb, BigDecimal heureEquivalent) {
		if (nb == null || minutes == null || heureEquivalent == null) {
			return null;
		}

		Integer dureeEnMinutes = minutes * nb;
		dureeEnMinutes = heureEquivalent.multiply(BigDecimal.valueOf((long) dureeEnMinutes)).setScale(0, RoundingMode.HALF_UP).intValue();

		return dureeEnMinutes;
	}

	private List<EOTypeAP> getDefaultTypesAP() {
		List<EOTypeAP> defaultTypesAP = new ArrayList<EOTypeAP>();
		if (editingContext != null) {
			defaultTypesAP.add(EOTypeAP.typeCours(editingContext));
			defaultTypesAP.add(EOTypeAP.typeTD(editingContext));
			defaultTypesAP.add(EOTypeAP.typeTP(editingContext));
		}
		return defaultTypesAP;
	}

}
