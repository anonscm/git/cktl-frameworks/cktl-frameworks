package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Regoupe des définitions communes aux applications scolarité
 */
public final class EOScolpedaCommonDefinitions {
	
	/**
	 * constructeur privé pour évité l'instantation de la classe 
	 */
	private EOScolpedaCommonDefinitions() {
	}

	/**
	 * renvoie le sens de tri par défaut.
	 * @param key : clé sur laquelle se fait le tri
	 * @return le tableau du tri
	 */
	public static NSArray<EOSortOrdering> defaultsortOrderings(String key) {
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(key, EOSortOrdering.CompareCaseInsensitiveAscending));
		return sortOrderings;
	}
}
