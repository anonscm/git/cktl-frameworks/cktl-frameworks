package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Cette classe permet de formater des minutes pour les afficher au format "heures,centième".
 */
public class DureeMinutesEnHeuresCentiemesFormat extends Format {

	/** Numéro de série. */
	private static final long serialVersionUID = 6884787189752447864L;
	/** Le nombre de minutes dans une heure. */
	private static final int NB_MINUTES_DANS_UNE_HEURE = 60;
	/** Le nombre de minutes dans une heure (version BigDecimal). */
	private static final BigDecimal NB_MINUTES_DANS_UNE_HEURE_BIGDEC = new BigDecimal(NB_MINUTES_DANS_UNE_HEURE);
	/** Les formats acceptés. */
	private Pattern pattern = Pattern.compile("([0-9]*)[.,]?([0-9]*)");
	
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj == null) {
			return toAppendTo;
		}
		
		Integer minutes = (Integer) obj;
		
		BigDecimal centiemes = BigDecimal.valueOf(minutes).divide(NB_MINUTES_DANS_UNE_HEURE_BIGDEC, 2, RoundingMode.HALF_UP);
		
		int heure = centiemes.intValue();
		int minute = centiemes.subtract(BigDecimal.valueOf(heure)).movePointRight(2).intValue();
		
		return toAppendTo.append(String.format("%d,%02d", heure, minute));
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		if (source == null) {
			pos.setIndex(-1);
			return null;
		}
		
		Matcher matcher = pattern.matcher(source);
		boolean trouve = matcher.matches();
		
		if (!trouve) {
			return null;
		}
		
		String heure = matcher.group(1);
		String centieme = matcher.group(2);

		int minutes = 0;
				
		if (centieme != null && centieme.length() != 0) {
			if (centieme.length() == 1) {
				// Si qu'une position pour les centiemes, on ajoute un zéro à droite (1,1 = 1,10)
				centieme += "0";
			} else if (centieme.length() > 2) {
				// Plus de deux positions décimales => erreur
				pos.setErrorIndex(matcher.start(2));
				return null;
			}
			
			minutes = new BigDecimal(Integer.parseInt(centieme)).movePointLeft(2).multiply(NB_MINUTES_DANS_UNE_HEURE_BIGDEC).setScale(0, RoundingMode.HALF_UP).intValue();
		}
		
		if (heure != null && heure.length() != 0) {
			minutes += Integer.parseInt(heure) * NB_MINUTES_DANS_UNE_HEURE;
		}
			
		pos.setIndex(-1);
		return minutes;
	}
}
