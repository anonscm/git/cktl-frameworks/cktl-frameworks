package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;

/**
 * Cette classe représente le formateur utilisé pour les zones de saisi de type montant.
 */
public class MontantSaisiFormat extends DecimalFormat {
	
	/** Numéro de série. */
	private static final long serialVersionUID = -4308485224900760896L;

	/**
	 * Constructeur.
	 */
	public MontantSaisiFormat() {
		setParseBigDecimal(true);
	}
	
	@Override
	public Number parse(String text, ParsePosition pos) {
		// Si le séparateur de millier n'est pas utilisé ou que ce n'est pas le point
		// et que le séparateur décimal est une virgule
		// alors on autorise également le point comme séparateur décimal
		// cas de la saisie française
		DecimalFormatSymbols decimalFormatSymbols = getDecimalFormatSymbols();
		if ((!isGroupingUsed() || decimalFormatSymbols.getGroupingSeparator() != '.') 
				&& decimalFormatSymbols.getDecimalSeparator() == ',') {
			text = text.replace('.', ',');
		}
		
		BigDecimal number = (BigDecimal) super.parse(text, pos);
		
		// Erreur, toute la chaine de caractères doit être analysée
		if (pos.getIndex() != text.length()) {
			pos.setIndex(0);
		} else if (number != null && number.scale() > getMaximumFractionDigits()) {
			// On ne peut pas avoir plus du nombre de décimale indiqué
			pos.setIndex(0);
		}
		
		return number;
	}
}
