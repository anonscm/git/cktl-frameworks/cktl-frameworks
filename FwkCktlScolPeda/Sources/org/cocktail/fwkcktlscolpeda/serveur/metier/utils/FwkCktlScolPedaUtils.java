package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe utilitaire fournissant les liste d'objets de type EOComposant
 */
public class FwkCktlScolPedaUtils {
	// Private fields
	private Integer anneeDebutHabilitation;
	private Integer dureeHabilitation;


	public Integer getAnneeDebutHabilitation() {
		return anneeDebutHabilitation;
	}

	public void setAnneeDebutHabilitation(Integer anneeDebutHabilitation) {
		this.anneeDebutHabilitation = anneeDebutHabilitation;
	}

	public Integer getDureeHabilitation() {
		return dureeHabilitation;
	}

	public void setDureeHabilitation(Integer dureeHabilitation) {
		this.dureeHabilitation = dureeHabilitation;
	}

	private Integer getAnneeFinHabilitation() {
		return getAnneeDebutHabilitation() + getDureeHabilitation();
	}

	// Public Methods
	/**
	 * @param edc : contexte d'edition
	 * @param annee : annee pour laquelle on cherche la liste des diplomes
	 * @return liste des diplomes
	 */
	private NSArray<EODiplome> getDiplomes(EOEditingContext edc, Integer annee) {
		NSArray<EODiplome> diplomes = new NSArray<EODiplome>();
		NSArray<EODiplome> diplomesTemp = new NSMutableArray<EODiplome>();

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODiplome.CODE_KEY, EOSortOrdering.CompareAscending));

		EOQualifier rechercheQualifier = null;
		EOQualifier qualifierDiplomeAnnee = ERXQ.equals(EODiplome.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE).key(), annee);//ERXQ.equals(EODiplome.ANNEES_KEY, annee);

		rechercheQualifier = null;// ERXQ.and(qualifierDiplomeAnnee);

		diplomes = EODiplome.fetchSco_Diplomes(edc, rechercheQualifier, sortOrderings);

		for (EODiplome diplome : diplomes) {
			if (diplome.annees().containsObject(annee)) {
				diplomesTemp.add(diplome);
			}
		}

		return ERXArrayUtilities.arrayWithoutDuplicates(diplomesTemp);
	}

	/**
	 * @param edc : contexte d'edition dans leqel sont recherches les objet
	 * @param annee : annee pour laquelle on cherche les composant
	 * @param typeComposant : type de composant pour lequel on fait la recherche
	 * @return liste des composants pour l'année et le type de composants
	 */
	/*public NSArray<EOComposant> getComposants(EOEditingContext edc, Integer annee, EOTypeComposant typeComposant) {
		NSArray<EOComposant> composants = new NSArray<EOComposant>();
		NSArray<EOComposant> composantsTemp = new NSMutableArray<EOComposant>();

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		EOQualifier rechercheQualifier = null;

		EOQualifier qualifierComposantAnnee = null;
		if (annee != null) {
			qualifierComposantAnnee = ERXQ.containsObject(EORegroupement.ANNEES_KEY, annee);
		}

		EOQualifier qualifierTypeComposant = null;
		if (typeComposant != null) {
			qualifierTypeComposant = ERXQ.equals(EOComposant.TYPE_COMPOSANT_KEY, typeComposant);
		}

		rechercheQualifier = ERXQ.and(qualifierTypeComposant);// ERXQ.and(qualifierComposantAnnee, qualifierTypeComposant);

		composants = EOComposant.fetchSco_Composants(edc, rechercheQualifier, sortOrderings);

		for (EOComposant composant : composants) {
			if (composant.annees().count() == 0) {
				composantsTemp.add(composant);
			} else if (composant.annees().containsObject(annee)) {
				composantsTemp.add(composant);
			}
		}

		return ERXArrayUtilities.arrayWithoutDuplicates(composantsTemp);
	}*/

	/**
	 * Renvoie la liste des diplomes a versionner pour une annee de depart vers une annee cible.
	 * @param edc contexte d'edition
	 * @param anneeDepart annee dont on part pour le versionning
	 * @param anneeCible annee cible des nouvelles versions
	 * @return liste de diplomes à versionner
	 */
	public NSArray<EODiplome> getDiplomesAVersionner(EOEditingContext edc, Integer anneeDepart, Integer anneeCible) {
		NSArray<EODiplome> diplomes = new NSMutableArray<EODiplome>();

		// Ajout des diplomes en production...
		NSArray<EODiplome> diplomesEnProduction = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(getDiplomes(edc, anneeDepart),
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_EP_CODE));
		// ... et non habilitables  
		NSArray<EODiplome> diplomesEnProductionNonHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnProduction,
		        EODiplome.HABILITATION_REQUISE.ne(1));
		diplomes.addAll(diplomesEnProductionNonHabilitables);
		// ... et habilitables et pour lesquels l'annee est dans la periode d'habilitation
		NSArray<EODiplome> diplomesEnProductionHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnProduction,
		        EODiplome.HABILITATION_REQUISE.eq(1));
		for (EODiplome diplome : diplomesEnProductionHabilitables) {
			if (diplome.anneeDansPeriodeHabilitation(anneeCible, getAnneeDebutHabilitation(), getAnneeFinHabilitation())) {
				diplomes.add(diplome);
			}
		}
		
		// Ajout des diplomes en cours de construction...
		NSArray<EODiplome> diplomesEnConstruction = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(getDiplomes(edc, anneeDepart),
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_ECC_CODE));
		// ... et non habilitables  
		NSArray<EODiplome> diplomesEnConstructionNonHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnConstruction,
		        EODiplome.HABILITATION_REQUISE.ne(1));
		diplomes.addAll(diplomesEnConstructionNonHabilitables);
		// ... et habilitables et pour lesquels l'annee est dans la periode d'habilitation
		NSArray<EODiplome> diplomesEnConstructionHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnConstruction,
		        EODiplome.HABILITATION_REQUISE.eq(1));
		for (EODiplome diplome : diplomesEnConstructionHabilitables) {
			if (diplome.anneeDansPeriodeHabilitation(anneeCible, getAnneeDebutHabilitation(), getAnneeFinHabilitation())) {
				diplomes.add(diplome);
			}
		}
		
		return diplomes;
	}
	
	/**
	 * @param edc contexte d'edition
	 * @param anneeReference annee de reference 
	 * @param anneeCible annee cible pour laquelle on souhaite habiliter
	 * @return liste des diplomes à habiliter
	 */
	public NSArray<EODiplome> getDiplomesAHabiliter(EOEditingContext edc, Integer anneeReference, Integer anneeCible) {
		NSArray<EODiplome> diplomes = new NSMutableArray<EODiplome>();
		
		// Retourner les diplomes en production et habilitable
		NSArray<EODiplome> diplomesProductionEtHabilitable = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(getDiplomes(edc, anneeReference),
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_EP_CODE).and(EODiplome.HABILITATION_REQUISE.eq(1)));
		diplomes.addAll(diplomesProductionEtHabilitable);
		
		return diplomes;
	}
	
}
