package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Cette classe permet de formater des minutes pour les afficher au format "heures[h]minutes".
 */
public class DureeMinutesEnHeuresMinutesFormat extends Format {

	/** Numéro de série. */
	private static final long serialVersionUID = 6884787189752447864L;
	/** Le nombre de minutes dans une heure. */
	private static final int NB_MINUTES_DANS_UNE_HEURE = 60;
	/** Les formats acceptés. */
	private Pattern pattern = Pattern.compile("([0-9]*)[h.,:]?([0-9]*)");
	
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj == null) {
			return toAppendTo;
		}
		
		Integer minutes = (Integer) obj;
		
		int heure = minutes / NB_MINUTES_DANS_UNE_HEURE;
		int minute = minutes % NB_MINUTES_DANS_UNE_HEURE;
		
		return toAppendTo.append(String.format("%dh%02d", heure, minute));
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		if (source == null) {
			pos.setIndex(-1);
			return null;
		}
		
		Matcher matcher = pattern.matcher(source);
		boolean trouve = matcher.matches();
		
		if (!trouve) {
			return null;
		}
		
		String heure = matcher.group(1);
		String minute = matcher.group(2);

		int minutes = 0;
		
		if (heure != null && heure.length() != 0) {
			minutes = Integer.parseInt(heure) * NB_MINUTES_DANS_UNE_HEURE;
		}
		
		if (minute != null && minute.length() != 0) {
			// Si qu'une position pour les minutes, on ajoute un zéro à droite (1h1 = 1h10)
			if (minute.length() == 1) {
				minute += "0";
			}
			
			int minuteInt = Integer.parseInt(minute);
			
			if (minuteInt >= NB_MINUTES_DANS_UNE_HEURE) {
				pos.setErrorIndex(matcher.start(2));
				return null;
			}
			
			minutes += Integer.parseInt(minute);
		}
			
		pos.setIndex(-1);
		return minutes;
	}

}
