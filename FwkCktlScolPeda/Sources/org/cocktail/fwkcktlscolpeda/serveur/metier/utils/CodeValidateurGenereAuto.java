package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.jdbc.ERXSQLHelper;
import er.extensions.localization.ERXLocalizer;

/**
 * Valide le code composant et le genère automatquement si il est null
 */
public class CodeValidateurGenereAuto implements Validateur {

	private static final String SYS_CODE_COMPOSANT_SEQ = "SCO_SCOLARITE.SYS_CODE_COMPOSANT_SEQ";

	/**
	 * {@inheritDoc}
	 */
	public String validateCode(String code, EOEditingContext editingContext) {
		if (code == null) {
			code = ERXLocalizer.defaultLocalizer().localizedStringForKey("CodeValidateurGenerateAuto.codePrefix")
			        + getNumeroSuivantCodeComposant(editingContext);
		}
		return code;
	}

	/**
	 * @param editingContext un editing context
	 * @return Le numéro suivant tiré de la séquence {@value #SYS_CODE_COMPOSANT_SEQ}
	 */
	private long getNumeroSuivantCodeComposant(EOEditingContext editingContext) {
		ERXSQLHelper sqlHelper = ERXSQLHelper.newSQLHelper(editingContext, FwkCktlScolPeda.MODEL_NAME);
		return sqlHelper.getNextValFromSequenceNamed(editingContext, FwkCktlScolPeda.MODEL_NAME, SYS_CODE_COMPOSANT_SEQ).longValue();
	}

}
