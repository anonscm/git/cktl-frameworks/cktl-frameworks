package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import org.cocktail.fwkcktljefyadmin.common.exception.IllegalArgumentException;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSKeyValueCodingAdditions;

import er.extensions.localization.ERXLocalizer;

/**
 * 
 * Classe à utiliser pour déterminer si un champ est visible ou non modifiable.
 * Une instance de cette classe doit être créée par type de connexion :
 * <pre>
 * - GESTIONNAIRE
 * - INSCRIPTION 
 * - REINSCRIPTION
 * </pre>
 * @author Alexis Tual
 *
 */
public class ParametragesAffichage implements NSKeyValueCodingAdditions {

	private static final String CHAMPS_PREFIX = "Champs.";
	private EOEditingContext ec;
	private ERXLocalizer localizer;
	private String typeConnexion;
	private String parametrage;
	private static String isVisible = "isVisible";
	private static String isNotModifiable = "isNotModifiable";
	private static String libelle = "libelle";
	private Boolean isEnLecture;
	
	/**
	 * @param ec un editingContext, vraisemblablement celui de session
	 * @param typeConnexion le type de connexion, @see {@link EOParametrageChamp}
	 * @param parametrage le parametrage enregistré 
	 */
	public ParametragesAffichage(EOEditingContext ec, ERXLocalizer localizer, String typeConnexion, String parametrage, Boolean lectureSeule) {
		this.ec = ec;
		this.typeConnexion = typeConnexion;
		this.parametrage = parametrage;
		this.isEnLecture = lectureSeule;
		this.localizer = localizer;
		if (localizer == null) {
			throw new IllegalArgumentException("Le localizer ne doit pas etre null");
		}
	}
	
	/**
	 * Ce constructeur n'est à utiliser qu'avec parcimonie (ie méthode libelleForChamp)
	 * @param localizer le localizer
	 */
	public ParametragesAffichage(ERXLocalizer localizer) {
		this(null, localizer, null, null, null);
	}

	/**
	 * @param codeChamp un code 
	 * @return le libelle du champ correspondant au code
	 */
	public String libelleForChamp(String codeChamp) {
		String unLibelle = ERXLocalizer.currentLocalizer().localizedStringForKey(CHAMPS_PREFIX+codeChamp);
		return unLibelle;
	}
	
	/**
	 * 
	 * @param codeChamp le code du champ
	 * @return true si le champ de code <code>codeChamp</code> est visible, false si le champ est inconnu
	 * 		   ou s'il n'est pas visible
	 */
	public Boolean isChampVisible(String codeChamp) {
		EOParametrageChamp paramChamp = EOParametrageChamp.parametrageChampFor(ec, codeChamp, parametrage);
		Boolean result = Boolean.FALSE;
		if (paramChamp != null) {
			result = !paramChamp.isInvisible(typeConnexion);
		}
		return result; 
	}
	
	
	/**
	 * 
	 * @param codeChamp le code du champ
	 * @return true si le champ de code <code>codeChamp</code> est obligatoire, false si le champ est inconnu
	 * 		   ou s'il n'est pas obligatoire
	 */
	public Boolean isChampObligatoire(String codeChamp) {
		EOChamp champ = EOChamp.champForCode(ec, codeChamp);
		Boolean result = Boolean.FALSE;
		if (champ != null) {		
			result = champ.obligatoire();	
		}
		return result; 
	}
	
	/**
	 * @param codeChamp le code du champ
	 * @return false si le champ de code <code>codeChamp</code> est modifiable
	 */
	public Boolean isChampNotModifiable(String codeChamp) {
		EOParametrageChamp paramChamp = EOParametrageChamp.parametrageChampFor(ec, codeChamp, parametrage);
		Boolean result = Boolean.FALSE;
		if (paramChamp != null) {
			if (isEnLecture) {
				result = true;
			} else {
				result = !paramChamp.isModifiable(typeConnexion);
			}
		}
		return result;
	}

	//@Override
	public void takeValueForKeyPath(Object value, String keyPath) {
		throw new UnsupportedOperationException("Non disponible");
	}

	/**
	 * Permet d'éviter de créer autant de méthodes que de champs...
	 * @param key la clef, exemple de clef : @cadreA.isVisible, @charte.isModifiable
	 * @return le resultat de {@link #isChampNotModifiable(String)} ou {@link #isChampNotModifiable(String)} 
	 * 		   selon la <code>key</code> passée en paramètre. La key doit être de la forme 
	 * 		   <code>@codeChamp.isVisible</code> ou <code>@codeChamp.isNotModifiable</code>
	 * @throws NSKeyValueCoding.UnknownKeyException si la <code>key</code> n'a pas le bon format
	 * @see com.webobjects.foundation.NSKeyValueCoding#valueForKey(java.lang.String)
	 */
	//@Override
	public Object valueForKeyPath(String key) throws UnknownKeyException {
		Object result = null;
		if (key != null && key.charAt(0) == '@') {
			int indexPoint = key.indexOf(".");
			if (indexPoint > 1) {
				String codeChamp = key.substring(1, indexPoint);
				String method = key.substring(indexPoint + 1);
				if (isVisible.equals(method)) {
					result = isChampVisible(codeChamp);
				} else if (isNotModifiable.equals(method)) {
					result = isChampNotModifiable(codeChamp);
				} else if (libelle.equals(method)) {
					result = libelleForChamp(codeChamp);
				}
			}
		}
		if (result == null) {
			throw new UnknownKeyException(
					"Le keypath doit avoir le format @codeChamp.isVisible ou @codeChamp.isNotModifiable", this, key);
		}
		return result;
	}

	//@Override
	public void takeValueForKey(Object value, String key) {
		takeValueForKeyPath(value, key);
	}

	//@Override
	public Object valueForKey(String key) {
		return valueForKeyPath(key);
	}

	/**
	 * @return the typeConnexion
	 */
	public String getTypeConnexion() {
		return typeConnexion;
	}

	/**
	 * @param typeConnexion the typeConnexion to set
	 */
	public void setTypeConnexion(String typeConnexion) {
		this.typeConnexion = typeConnexion;
	}
	
	
	
}
