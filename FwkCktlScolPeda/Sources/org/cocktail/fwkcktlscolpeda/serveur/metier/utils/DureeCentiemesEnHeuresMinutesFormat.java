package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Cette classe permet de formater des centièmes d'heures pour les afficher au format "heures[h]minutes".
 */
public class DureeCentiemesEnHeuresMinutesFormat extends Format {

	/** Numéro de série. */
	private static final long serialVersionUID = 6884787189752447864L;
	/** Le nombre de minutes dans une heure. */
	private static final int NB_MINUTES_DANS_UNE_HEURE = 60;
	/** Le nombre de minutes dans une heure (version BigDecimal). */
	private static final BigDecimal NB_MINUTES_DANS_UNE_HEURE_BIGDEC = new BigDecimal(NB_MINUTES_DANS_UNE_HEURE);
	/** Les formats acceptés. */
	private Pattern pattern = Pattern.compile("([0-9]*)[h.,:]?([0-9]*)");
	
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj == null) {
			return toAppendTo;
		}
		
		BigDecimal heuresCentiemes = (BigDecimal) obj;
		
		int heure = heuresCentiemes.intValue();
		int minute = heuresCentiemes.subtract(BigDecimal.valueOf(heure)).multiply(NB_MINUTES_DANS_UNE_HEURE_BIGDEC).setScale(0, RoundingMode.HALF_UP).intValue();
		
		return toAppendTo.append(String.format("%dh%02d", heure, minute));
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		if (source == null) {
			pos.setIndex(-1);
			return null;
		}
		
		Matcher matcher = pattern.matcher(source);
		boolean trouve = matcher.matches();
		
		if (!trouve) {
			return null;
		}
		
		String heure = matcher.group(1);
		String minute = matcher.group(2);
		
		// Rien dans heure => 0 heure
		if (heure == null || heure.length() == 0) {
			heure = "0";
		}

		BigDecimal heureCentieme = null;
				
		if (minute != null && minute.length() != 0) {
			if (minute.length() == 1) {
				// Si qu'une position pour les minutes, on ajoute un zéro à droite (1,1 = 1,10)
				minute += "0";
			} else if (minute.length() > 2) {
				// Plus de deux positions décimales => erreur
				pos.setErrorIndex(matcher.start(2));
				return null;
			}
			
			heureCentieme = new BigDecimal(Integer.parseInt(minute)).divide(NB_MINUTES_DANS_UNE_HEURE_BIGDEC, 2, RoundingMode.HALF_UP);
		}
		
		if (heureCentieme == null) {
			heureCentieme = new BigDecimal(heure).setScale(2);
		} else {
			heureCentieme = heureCentieme.add(new BigDecimal(heure));
		}
		
		pos.setIndex(-1);
		return heureCentieme;
	}
}
