package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;

/**
 * Implémente la stratégie pour renvoyer une liste des enfants APs
 */
public class ListeEnfantsAPs implements IListeComposantEnfantsStrategy<EOAP> {
	// Private fields
	private EOTypeAP typeAP = null;

	// Constructor
	@SuppressWarnings("unused")
	private ListeEnfantsAPs() {
	}

	/**
	 * @param typeAP : type d'AP pour lequel on veut la liste des enfants de type EOAP
	 */
	public ListeEnfantsAPs(EOTypeAP typeAP) {
		this.typeAP = typeAP;
	}

	// Public Methods
	/*
	 * (non-Javadoc)
	 * @see
	 * org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IListComposantEnfantsStrategy#getListEnfants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)
	 */
	public List<EOAP> getListEnfants(EOComposant composant) {
		if (typeAP == null) {
			return null;
		}
		List<EOAP> aps = new ArrayList<EOAP>();

		EOTypeLien liencomposer= EOTypeLien.typeComposer(composant.editingContext());
		EOTypeComposant typeComposant = EOTypeComposant.typeAP(composant.editingContext());
		
		for (IComposant enfant : composant.childs(liencomposer, typeComposant, EOLienComposer.ORDRE.ascs())) {
			if (((EOAP) enfant).typeAP().equals(typeAP)) {
				aps.add((EOAP) enfant);
			}
		}

		return aps;
	}

}
