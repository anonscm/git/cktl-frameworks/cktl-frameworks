package org.cocktail.fwkcktlscolpeda.serveur.metier.utils;

import java.math.BigDecimal;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Cette classe permet de formater des centièmes d'heures pour les afficher au format "heures[,]centièmes".
 */
public class DureeCentiemesEnHeuresCentiemesFormat extends Format {

	/** Numéro de série. */
	private static final long serialVersionUID = 6884787189752447864L;
	/** Les formats acceptés. */
	private Pattern pattern = Pattern.compile("([0-9]*)[.,]?([0-9]*)");
	
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj == null) {
			return toAppendTo;
		}
		
		BigDecimal heuresCentiemes = (BigDecimal) obj;
		
		int heure = heuresCentiemes.intValue();
		int minute = heuresCentiemes.subtract(BigDecimal.valueOf(heure)).movePointRight(2).intValue();
		
		return toAppendTo.append(String.format("%d,%02d", heure, minute));
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		if (source == null) {
			pos.setIndex(-1);
			return null;
		}
		
		Matcher matcher = pattern.matcher(source);
		boolean trouve = matcher.matches();
		
		if (!trouve) {
			return null;
		}
		
		String heure = matcher.group(1);
		String centieme = matcher.group(2);

		// Rien dans heure => 0 heure
		if (heure == null || heure.length() == 0) {
			heure = "0";
		}
		
		BigDecimal heureCentieme = null;
				
		if (centieme != null && centieme.length() != 0) {
			if (centieme.length() == 1) {
				// Si qu'une position pour les minutes, on ajoute un zéro à droite (1,1 = 1,10)
				centieme += "0";
			} else if (centieme.length() > 2) {
				// Plus de deux positions décimales => erreur
				pos.setErrorIndex(matcher.start(2));
				return null;
			}
			
			heureCentieme = new BigDecimal(Integer.parseInt(centieme)).movePointLeft(2);
		}
		
		if (heureCentieme == null) {
			heureCentieme = new BigDecimal(heure).setScale(2);
		} else {
			heureCentieme = heureCentieme.add(new BigDecimal(heure));
		}
			
		pos.setIndex(-1);
		return heureCentieme;
	}
}
