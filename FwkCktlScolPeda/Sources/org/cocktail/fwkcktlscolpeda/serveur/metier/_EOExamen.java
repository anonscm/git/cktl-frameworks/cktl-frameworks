// DO NOT EDIT.  Make changes to EOExamen.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOExamen extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Examen";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFCATION = new ERXKey<NSTimestamp>("dModifcation");
  public static final ERXKey<Integer> ID_COMPOSANT_AE = new ERXKey<Integer>("idComposantAE");
  public static final ERXKey<Integer> ID_EXAMEN = new ERXKey<Integer>("idExamen");
  public static final ERXKey<Integer> ID_SESSION_PERIODE = new ERXKey<Integer>("idSessionPeriode");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE> TO_COMPOSANT_AE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE>("toComposantAE");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat> TO_EXAMEN_ETAT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat>("toExamenEtat");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> TO_EXAMEN_ORGANISATIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation>("toExamenOrganisations");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode> TO_SESSION_PERIODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode>("toSessionPeriode");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFCATION_KEY = D_MODIFCATION.key();
  public static final String ID_COMPOSANT_AE_KEY = ID_COMPOSANT_AE.key();
  public static final String ID_EXAMEN_KEY = ID_EXAMEN.key();
  public static final String ID_SESSION_PERIODE_KEY = ID_SESSION_PERIODE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_COMPOSANT_AE_KEY = TO_COMPOSANT_AE.key();
  public static final String TO_EXAMEN_ETAT_KEY = TO_EXAMEN_ETAT.key();
  public static final String TO_EXAMEN_ORGANISATIONS_KEY = TO_EXAMEN_ORGANISATIONS.key();
  public static final String TO_SESSION_PERIODE_KEY = TO_SESSION_PERIODE.key();

  private static Logger LOG = Logger.getLogger(_EOExamen.class);

  public EOExamen localInstanceIn(EOEditingContext editingContext) {
    EOExamen localInstance = (EOExamen)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOExamen.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.ANNEE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOExamen.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.D_CREATION_KEY);
  }

  public NSTimestamp dModifcation() {
    return (NSTimestamp) storedValueForKey(_EOExamen.D_MODIFCATION_KEY);
  }

  public void setDModifcation(NSTimestamp value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating dModifcation from " + dModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.D_MODIFCATION_KEY);
  }

  public Integer idComposantAE() {
    return (Integer) storedValueForKey(_EOExamen.ID_COMPOSANT_AE_KEY);
  }

  public void setIdComposantAE(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating idComposantAE from " + idComposantAE() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.ID_COMPOSANT_AE_KEY);
  }

  public Integer idExamen() {
    return (Integer) storedValueForKey(_EOExamen.ID_EXAMEN_KEY);
  }

  public void setIdExamen(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating idExamen from " + idExamen() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.ID_EXAMEN_KEY);
  }

  public Integer idSessionPeriode() {
    return (Integer) storedValueForKey(_EOExamen.ID_SESSION_PERIODE_KEY);
  }

  public void setIdSessionPeriode(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating idSessionPeriode from " + idSessionPeriode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.ID_SESSION_PERIODE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOExamen.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOExamen.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
    	_EOExamen.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamen.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE toComposantAE() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE)storedValueForKey(_EOExamen.TO_COMPOSANT_AE_KEY);
  }
  
  public void setToComposantAE(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE value) {
    takeStoredValueForKey(value, _EOExamen.TO_COMPOSANT_AE_KEY);
  }

  public void setToComposantAERelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
      _EOExamen.LOG.debug("updating toComposantAE from " + toComposantAE() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToComposantAE(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE oldValue = toComposantAE();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamen.TO_COMPOSANT_AE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamen.TO_COMPOSANT_AE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat toExamenEtat() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat)storedValueForKey(_EOExamen.TO_EXAMEN_ETAT_KEY);
  }
  
  public void setToExamenEtat(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat value) {
    takeStoredValueForKey(value, _EOExamen.TO_EXAMEN_ETAT_KEY);
  }

  public void setToExamenEtatRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
      _EOExamen.LOG.debug("updating toExamenEtat from " + toExamenEtat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToExamenEtat(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat oldValue = toExamenEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamen.TO_EXAMEN_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamen.TO_EXAMEN_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode toSessionPeriode() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode)storedValueForKey(_EOExamen.TO_SESSION_PERIODE_KEY);
  }
  
  public void setToSessionPeriode(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode value) {
    takeStoredValueForKey(value, _EOExamen.TO_SESSION_PERIODE_KEY);
  }

  public void setToSessionPeriodeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode value) {
    if (_EOExamen.LOG.isDebugEnabled()) {
      _EOExamen.LOG.debug("updating toSessionPeriode from " + toSessionPeriode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToSessionPeriode(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode oldValue = toSessionPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamen.TO_SESSION_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamen.TO_SESSION_PERIODE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> toExamenOrganisations() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation>)storedValueForKey(_EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> toExamenOrganisations(EOQualifier qualifier) {
    return toExamenOrganisations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> toExamenOrganisations(EOQualifier qualifier, boolean fetch) {
    return toExamenOrganisations(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> toExamenOrganisations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation.TO_EXAMEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation.fetchSco_ExamenOrganisations(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toExamenOrganisations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExamenOrganisations(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation object) {
    includeObjectIntoPropertyWithKey(object, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
  }

  public void removeFromToExamenOrganisations(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation object) {
    excludeObjectFromPropertyWithKey(object, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
  }

  public void addToToExamenOrganisationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation object) {
    if (_EOExamen.LOG.isDebugEnabled()) {
      _EOExamen.LOG.debug("adding " + object + " to toExamenOrganisations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToExamenOrganisations(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
    }
  }

  public void removeFromToExamenOrganisationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation object) {
    if (_EOExamen.LOG.isDebugEnabled()) {
      _EOExamen.LOG.debug("removing " + object + " from toExamenOrganisations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToExamenOrganisations(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation createToExamenOrganisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation) eo;
  }

  public void deleteToExamenOrganisationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOExamen.TO_EXAMEN_ORGANISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExamenOrganisationsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> objects = toExamenOrganisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExamenOrganisationsRelationship(objects.nextElement());
    }
  }


  public static EOExamen createSco_Examen(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, Integer idComposantAE
, Integer idExamen
, Integer idSessionPeriode
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE toComposantAE, org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtat toExamenEtat, org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode toSessionPeriode) {
    EOExamen eo = (EOExamen) EOUtilities.createAndInsertInstance(editingContext, _EOExamen.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setIdComposantAE(idComposantAE);
		eo.setIdExamen(idExamen);
		eo.setIdSessionPeriode(idSessionPeriode);
		eo.setPersIdCreation(persIdCreation);
    eo.setToComposantAERelationship(toComposantAE);
    eo.setToExamenEtatRelationship(toExamenEtat);
    eo.setToSessionPeriodeRelationship(toSessionPeriode);
    return eo;
  }

  public static ERXFetchSpecification<EOExamen> fetchSpec() {
    return new ERXFetchSpecification<EOExamen>(_EOExamen.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOExamen> fetchAllSco_Examens(EOEditingContext editingContext) {
    return _EOExamen.fetchAllSco_Examens(editingContext, null);
  }

  public static NSArray<EOExamen> fetchAllSco_Examens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOExamen.fetchSco_Examens(editingContext, null, sortOrderings);
  }

  public static NSArray<EOExamen> fetchSco_Examens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOExamen> fetchSpec = new ERXFetchSpecification<EOExamen>(_EOExamen.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOExamen> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOExamen fetchSco_Examen(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamen.fetchSco_Examen(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamen fetchSco_Examen(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOExamen> eoObjects = _EOExamen.fetchSco_Examens(editingContext, qualifier, null);
    EOExamen eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Examen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamen fetchRequiredSco_Examen(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamen.fetchRequiredSco_Examen(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamen fetchRequiredSco_Examen(EOEditingContext editingContext, EOQualifier qualifier) {
    EOExamen eoObject = _EOExamen.fetchSco_Examen(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Examen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamen localInstanceIn(EOEditingContext editingContext, EOExamen eo) {
    EOExamen localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
