package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

/**
 *  Décrit une session pour une composant donné (modalités, formule) 
 */
public interface ISessionComposant {

	/**
	 * @return Le libelle de la session
	 */
	String libelle();

	/**
	 * @param unLibelle Un libellé pour la session
	 */
	void setLibelle(String unLibelle);

	/**
	 * @return Le composant associé
	 */
	IComposant composant();

	/**
	 * @param unComposant Le composant associé
	 */
	void setComposantRelationship(IComposant unComposant);
	
	/**
	 * @return Le numéro de la session (de 1 à n)
	 */
	Integer numeroSession();

	/**
	 * @param unNumeroSession Un numéro de session (de 1 à n)
	 */
	void setNumeroSession(Integer unNumeroSession);
	
	/**
	 * @param typeFormuleLbl type de formule visé
	 * @return la formule active qui correspond au type de formule souhaité
	 */
	IFormule getFormule(String typeFormuleLbl);

	/**
	 * @return 1 si la session est active, 0 sinon
	 */
	Integer active();
	
	/**
	 * @param estActive 1 si la session est active, 0 sinon
	 */
	void setActive(Integer estActive);

	/**
	 * @return la liste des formules
	 */
	List<? extends IFormule> formules();

}
