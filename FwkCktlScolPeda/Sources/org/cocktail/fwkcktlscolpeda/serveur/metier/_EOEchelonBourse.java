// DO NOT EDIT.  Make changes to EOEchelonBourse.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEchelonBourse extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_EchelonBourse";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOEchelonBourse.class);

  public EOEchelonBourse localInstanceIn(EOEditingContext editingContext) {
    EOEchelonBourse localInstance = (EOEchelonBourse)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOEchelonBourse.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOEchelonBourse.LOG.isDebugEnabled()) {
    	_EOEchelonBourse.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEchelonBourse.CODE_KEY);
  }


  public static EOEchelonBourse createSco_EchelonBourse(EOEditingContext editingContext, String code
) {
    EOEchelonBourse eo = (EOEchelonBourse) EOUtilities.createAndInsertInstance(editingContext, _EOEchelonBourse.ENTITY_NAME);    
		eo.setCode(code);
    return eo;
  }

  public static ERXFetchSpecification<EOEchelonBourse> fetchSpec() {
    return new ERXFetchSpecification<EOEchelonBourse>(_EOEchelonBourse.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEchelonBourse> fetchAllSco_EchelonBourses(EOEditingContext editingContext) {
    return _EOEchelonBourse.fetchAllSco_EchelonBourses(editingContext, null);
  }

  public static NSArray<EOEchelonBourse> fetchAllSco_EchelonBourses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEchelonBourse.fetchSco_EchelonBourses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEchelonBourse> fetchSco_EchelonBourses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEchelonBourse> fetchSpec = new ERXFetchSpecification<EOEchelonBourse>(_EOEchelonBourse.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEchelonBourse> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEchelonBourse fetchSco_EchelonBourse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEchelonBourse.fetchSco_EchelonBourse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEchelonBourse fetchSco_EchelonBourse(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEchelonBourse> eoObjects = _EOEchelonBourse.fetchSco_EchelonBourses(editingContext, qualifier, null);
    EOEchelonBourse eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_EchelonBourse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEchelonBourse fetchRequiredSco_EchelonBourse(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEchelonBourse.fetchRequiredSco_EchelonBourse(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEchelonBourse fetchRequiredSco_EchelonBourse(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEchelonBourse eoObject = _EOEchelonBourse.fetchSco_EchelonBourse(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_EchelonBourse that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEchelonBourse localInstanceIn(EOEditingContext editingContext, EOEchelonBourse eo) {
    EOEchelonBourse localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
