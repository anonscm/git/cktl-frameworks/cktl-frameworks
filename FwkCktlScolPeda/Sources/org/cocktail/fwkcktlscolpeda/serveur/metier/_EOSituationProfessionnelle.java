// DO NOT EDIT.  Make changes to EOSituationProfessionnelle.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSituationProfessionnelle extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SituationProfessionnelle";

  // Attribute Keys
  public static final ERXKey<String> CODE_SIT_PROF = new ERXKey<String>("codeSitProf");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String CODE_SIT_PROF_KEY = CODE_SIT_PROF.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSituationProfessionnelle.class);

  public EOSituationProfessionnelle localInstanceIn(EOEditingContext editingContext) {
    EOSituationProfessionnelle localInstance = (EOSituationProfessionnelle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeSitProf() {
    return (String) storedValueForKey(_EOSituationProfessionnelle.CODE_SIT_PROF_KEY);
  }

  public void setCodeSitProf(String value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating codeSitProf from " + codeSitProf() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.CODE_SIT_PROF_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOSituationProfessionnelle.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOSituationProfessionnelle.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSituationProfessionnelle.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOSituationProfessionnelle.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOSituationProfessionnelle.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOSituationProfessionnelle.LOG.isDebugEnabled()) {
    	_EOSituationProfessionnelle.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSituationProfessionnelle.PERS_ID_MODIFICATION_KEY);
  }


  public static EOSituationProfessionnelle createSco_SituationProfessionnelle(EOEditingContext editingContext, String codeSitProf
, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
) {
    EOSituationProfessionnelle eo = (EOSituationProfessionnelle) EOUtilities.createAndInsertInstance(editingContext, _EOSituationProfessionnelle.ENTITY_NAME);    
		eo.setCodeSitProf(codeSitProf);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOSituationProfessionnelle> fetchSpec() {
    return new ERXFetchSpecification<EOSituationProfessionnelle>(_EOSituationProfessionnelle.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSituationProfessionnelle> fetchAllSco_SituationProfessionnelles(EOEditingContext editingContext) {
    return _EOSituationProfessionnelle.fetchAllSco_SituationProfessionnelles(editingContext, null);
  }

  public static NSArray<EOSituationProfessionnelle> fetchAllSco_SituationProfessionnelles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSituationProfessionnelle.fetchSco_SituationProfessionnelles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSituationProfessionnelle> fetchSco_SituationProfessionnelles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSituationProfessionnelle> fetchSpec = new ERXFetchSpecification<EOSituationProfessionnelle>(_EOSituationProfessionnelle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSituationProfessionnelle> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSituationProfessionnelle fetchSco_SituationProfessionnelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationProfessionnelle.fetchSco_SituationProfessionnelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationProfessionnelle fetchSco_SituationProfessionnelle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSituationProfessionnelle> eoObjects = _EOSituationProfessionnelle.fetchSco_SituationProfessionnelles(editingContext, qualifier, null);
    EOSituationProfessionnelle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SituationProfessionnelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationProfessionnelle fetchRequiredSco_SituationProfessionnelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSituationProfessionnelle.fetchRequiredSco_SituationProfessionnelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSituationProfessionnelle fetchRequiredSco_SituationProfessionnelle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSituationProfessionnelle eoObject = _EOSituationProfessionnelle.fetchSco_SituationProfessionnelle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SituationProfessionnelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSituationProfessionnelle localInstanceIn(EOEditingContext editingContext, EOSituationProfessionnelle eo) {
    EOSituationProfessionnelle localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
