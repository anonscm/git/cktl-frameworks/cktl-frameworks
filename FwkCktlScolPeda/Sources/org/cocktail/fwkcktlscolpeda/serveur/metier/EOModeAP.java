package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;


public class EOModeAP extends _EOModeAP implements IModeAP {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOModeAP.class);
	
	public static final String MODE_PRESENTIEL_CODE = "PRES";
	public static final String MODE_ADISTANCE_CODE = "ADIST";
	public static final String MODE_AUTOFORMATION_CODE = "AUTOFOR";
	
	/**
     * @param edc : contexte d'édition
     * @return le mode PRESENTIEL
     */
    public static EOModeAP modePresentiel(EOEditingContext edc) {
    	EOQualifier qualifier = ERXQ.equals(CODE_KEY, MODE_PRESENTIEL_CODE);
    	return EOModeAP.fetchSco_ModeAP(edc, qualifier);
    }
    
    /**
     * @param edc : contexte d'édition
     * @return le mode A DISTANCE
     */
    public static EOModeAP modeADistance(EOEditingContext edc) {
    	EOQualifier qualifier = ERXQ.equals(CODE_KEY, MODE_ADISTANCE_CODE);
    	return EOModeAP.fetchSco_ModeAP(edc, qualifier);
    }
    
    /**
     * @param edc : contexte d'édition
     * @return le mode AUTO-FORMATION
     */
    public static EOModeAP modeAutoFormation(EOEditingContext edc) {
    	EOQualifier qualifier = ERXQ.equals(CODE_KEY, MODE_AUTOFORMATION_CODE);
    	return EOModeAP.fetchSco_ModeAP(edc, qualifier);
    }
    
    public String toString() {
    	return this.libelle();
    }
	    
}
