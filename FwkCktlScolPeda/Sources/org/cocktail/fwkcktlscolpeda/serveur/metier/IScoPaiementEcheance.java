package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 * Cette classe représente une échanece de paiement.
 *  
 * @author Pascal MACOUIN
 */
public interface IScoPaiementEcheance extends IPaiementEcheance {

//	/**
//	 * @return le moyen de paiement associé à cette échéance
//	 */
//	IScoPaiementMoyen toPaiementMoyen();
//
//	/**
//	 * Affecte le moyen de paiement associé à cette échéance.
//	 * @param value un moyen de paiement
//	 */
//	void setToPaiementMoyenRelationship(IScoPaiementMoyen value);
//
//	/**
//	 * Le moyen de paiement ayant servi à payer cette échéance.
//	 * <p>
//	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
//	 * a été payé avec un chèque.
//	 * @return Le moyen de paiement ayant servi à payer cette échéance
//	 */
//	IScoPaiementMoyen toAutrePaiementMoyen();
//
//	/**
//	 * Affecte le moyen de paiement ayant servi à payer cette échéance.
//	 * <p>
//	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
//	 * a été payé avec un chèque.
//	 * 
//	 * @param unAutreMoyenDePaiement un autre moyen le paiement
//	 */
//	void setToAutrePaiementMoyenRelationship(IScoPaiementMoyen unAutreMoyenDePaiement);
}