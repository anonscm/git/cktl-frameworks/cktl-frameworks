package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

/**
 * Dedinit les regles de calcul du credit ECTS
 */
public interface RegleCalculCreditECTS {

	/**
	 * @param lienAvecEnfant lien avec l'enfant que l'on prend en compte ou pas dans la regle de calcul
	 * @return vrai si l'on doit prendre en compte ce lien pour le calcul
	 */
	boolean selectionnable(ILien lienAvecEnfant);

	/**
	 * @param lien lien sur lequl on fait le test
	 * @return vrai si la somme des enfant est necessaire pour un lien donne
	 */
	boolean sommeSurEnfantsNecessaire(ILien lien);

	/**
	 * Est-ce que le calcul doit tenir compte des minimums théoriques.
	 * <p>
	 * C'est le cas en particulier sur les regroupements où il peut y avoir
	 * un nombre d'ECTS minimum forcément acquis ou un nombre d'éléments enfants forcément choisi.
	 * 
	 * @return vrai si le calcul doit tenir compte des minimums théoriques
	 */
	boolean calculAvecMinimumTheorique();
}
