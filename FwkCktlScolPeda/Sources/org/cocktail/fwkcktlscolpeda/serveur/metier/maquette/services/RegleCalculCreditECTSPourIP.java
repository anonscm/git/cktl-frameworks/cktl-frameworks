package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;

/**
 * Implemente les regles de calcul du credit ECTS pour les IP
 */
public class RegleCalculCreditECTSPourIP implements RegleCalculCreditECTS {
	private IInscriptionPedagogique inscriptionPedagogique;
	private List<ILien> liensInscriptionPedagogique = new ArrayList<ILien>();
	private MaquetteData maquetteData;
	private MaquetteElementProvider maquetteElementProvider;
	private CreditableService creditableService = new CreditableServiceImpl();

	public RegleCalculCreditECTSPourIP() {
	}

	/**
	 * @param ip : inscription pedagogique
	 */
	public RegleCalculCreditECTSPourIP(IInscriptionPedagogique ip) {
		this.inscriptionPedagogique = ip;
		List<ILien> liens = new ArrayList<ILien>();
		for (IInscriptionPedagogiqueElement ipElement : inscriptionPedagogique.toInscriptionPedagogiqueElements()) {
			liens.add(ipElement.toLien());
		}
		setLiensInscriptionPedagogique(liens);
	}

	public void setLiensInscriptionPedagogique(List<ILien> liensInscriptionPedagogique) {
		this.liensInscriptionPedagogique = liensInscriptionPedagogique;
	}


	/**
	 * {@inheritDoc}
	 */
	public boolean selectionnable(ILien lienAvecEnfant) {
		IComposant child = maquetteElementProvider.getChildAvecLien(lienAvecEnfant);

		if (child instanceof IAE || child instanceof IAP) {
			return false;
		}
		if (child instanceof IUE || child instanceof IEC || child instanceof IRegroupement) {
			return liensInscriptionPedagogique.contains(lienAvecEnfant);
		}
		if (child instanceof IParcours) {
			if (lienAvecEnfant instanceof ILienComposer) {
				// si le parent est un semestre
				IComposant parent = maquetteElementProvider.getParentAvecLien(lienAvecEnfant);
				if (parent instanceof IPeriode && ((IPeriode) parent).isSemestre()) {
					return liensInscriptionPedagogique.contains(lienAvecEnfant);
				}
			}
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean sommeSurEnfantsNecessaire(ILien lien) {
		List<ILien> lienParents = null;
		IComposant composant = maquetteElementProvider.getChildAvecLien(lien);
		// HORREUR FIX ME !!!
		if (lien instanceof ILienConsultation) {
			ILienConsultation lienConsultation = (ILienConsultation) lien;
			lienParents = new ArrayList<ILien>(lienConsultation.getChildren());
		} else {
			lienParents = maquetteElementProvider.getLiensAvecComposantParent(composant);
		}

		// Il a pas d'enfants donc on fait pas la somme
		if (lienParents.size() == 0) {
			return false;
		}
		
		if (composant instanceof IUE) {
			// Si le composant a un credit ECTS et qu'au moin un des enfant n'en a pas  
			if (!hasCreditECTSNull(lien) && auMoinsUnLienSelectionnableSansCreditable(lienParents)) {
				return false;
			}
		}
		
		return true;
	}

	private boolean auMoinsUnLienSelectionnableSansCreditable(List<ILien> liens) {
		// la somme sur les enfants n'est pas nécessaire si au moins un des enfants selectionnables n'a pas de creditECTS
		for (ILien lien : liens) {
			if (selectionnable(lien)) {
				if (hasCreditECTSNull(lien)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean hasCreditECTSNull(ILien lien) {
		IComposant child = maquetteElementProvider.getChildAvecLien(lien);
		ICreditable creditableChild = maquetteElementProvider.getCreditablePourComposant(child);
		ICreditable creditableLien = maquetteElementProvider.getCreditablePourLien(lien);
		
		return creditableService.creditsEcts(creditableChild, creditableLien) == null;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean calculAvecMinimumTheorique() {
		return false;
	}

	public MaquetteData getMaquetteData() {
		return maquetteData;
	}

	public void setMaquetteData(MaquetteData maquetteData) {
		this.maquetteData = maquetteData;
		maquetteElementProvider = new MaquetteElementProvider(maquetteData);
	}
}
