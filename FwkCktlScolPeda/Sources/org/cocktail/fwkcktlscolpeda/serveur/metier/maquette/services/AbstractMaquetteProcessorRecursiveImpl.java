package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Version récursive. A utiliser si on souhaite conserver l'ordre de parcours de l'arbre (de la feuille vers la racine).
 * 
 * @author Pascal MACOUIN
 */
public abstract class AbstractMaquetteProcessorRecursiveImpl extends AbstractMaquetteProcessorImpl {

	private static final Logger LOG = Logger.getLogger(AbstractMaquetteProcessorRecursiveImpl.class);

	/** On peut arrêter prématurément le parcours de l'arbre si une cible est atteinte. */
	private boolean targetReach = false;
	
	/**
	 * @param editingContext nécessaire pour fetcher les fils 
	 */
	public AbstractMaquetteProcessorRecursiveImpl(EOEditingContext editingContext) {
		super(editingContext);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void proceeed() {
		if (getRoot() == null) {
			LOG.warn("La root est nulle. Arret du parcours de la maquette.");
			return;
		}
		
		boolean reach = processChildren(getRoot());
		performActionsOn(getRoot());
	}

	/**
	 * 
	 * @param lien
	 * @return <code>true</code> si le composant cible est atteint
	 */
	protected boolean processChildren(ILien lien) {
	    if (shouldProcessChildrenOf(lien)) {
	        for (ILien lienFils : getFils(lien)) {
	        	boolean reach = processChildren(lienFils);
	        	if (reach) {
	        		break;
	        	}
			}
	        
        	performPopActionsOn(lien);
	    }
	    
	    performActionsOn(lien);
	    
	    return isTargetReach();
	}
	
	protected void performPopActionsOn(ILien lien) {
		// Par défaut ne fait rien
	}
	
	protected boolean isTargetReach() {
		return targetReach;
	}

	protected void setTargetReach(boolean targetReach) {
		this.targetReach = targetReach;
	}
}
