package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlwebapp.eof.CktlFetchSpecification;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.qualifiers.ERXOrQualifier;
import er.extensions.qualifiers.ERXTrueQualifier;

/**
 * Construction des données de la maquette
 */
public final class MaquetteDataBuilder {

	private EOEditingContext edc;

	private Integer composantRacineId;
	private Integer profondeurMax;
	private ILien lienRacine;
	private List<ILien> liens;
	private List<ILienConsultation> lienConsultations;
	private List<Integer> lienChildIds;
	private List<Integer> lienParentIds;
	private List<Integer> lienParentAutreIds = Collections.emptyList();
	private List<Integer> lienIds;
	private List<IComposant> composants;
	private List<IChargeEnseignement> chargeEnseignements;
	private List<ICreditable> creditables;
	private List<IResponsableComposant> responsablesComposant;

	private MaquetteData data = new MaquetteData();

	/**
	 * @return instance de MaquetteDataBuilder
	 */
	public static MaquetteDataBuilder get() {
		return new MaquetteDataBuilder();
	}

	private MaquetteDataBuilder() {
	}

	/**
	 * @param edc contexte d'édition
	 * @return instance avec les types de composants chargé
	 */
	public MaquetteDataBuilder editingContext(EOEditingContext edc) {
		this.edc = edc;
		// une fois qu'on a l'editingContext, on peut charger des nomenclatures
		initTypeComposants();

		return this;
	}

	public MaquetteDataBuilder composantRacineId(Integer id) {
		this.composantRacineId = id;
		return this;
	}

	/**
	 * @param profondeurMax la profondeur à laquelle on explore l'arbre 
	 * @return  this
	 */
	public MaquetteDataBuilder profondeurMax(Integer profondeurMax) {
		this.profondeurMax = profondeurMax;
		return this;
	}

	/**
	 * charge et indexe les liens de la maquette
	 * @return  l'ensemble des liens de la maquette
	 */
	public MaquetteDataBuilder loadLiens() {
		CktlFetchSpecification<EOLienComposer> fs = new CktlFetchSpecification<EOLienComposer>(EOLienComposer.ENTITY_NAME, new ERXTrueQualifier(), null);
		fs.setConnectBy(EOLien.CHILD_ID, EOLien.PARENT_ID);
		fs.setStartWithQualifier(EOLien.CHILD_ID.eq(composantRacineId));
		fs.setMaxLevel(this.profondeurMax);
		fs.setSiblingsSortOrderings(EOLien.ORDRE.ascs());
		Set<ILien> liensUniques = new HashSet<ILien>(fs.fetchObjects(edc));
		if (lienRacine != null) {
			liensUniques.add(lienRacine);
		}
		liens = new ArrayList<ILien>(liensUniques);
		lienChildIds = new ArrayList<Integer>();
		lienParentIds = new ArrayList<Integer>();
		lienIds = new ArrayList<Integer>();
		for (ILien l : liens) {
			lienChildIds.add(l.childId());
			lienParentIds.add(l.parentId());
			lienIds.add(l.id());
		}
		indexLiens();
		return this;
	}

	public MaquetteDataBuilder loadLiensHorsMaquette() {
		NSArray<EOLien> liensParentAutre = fetchLiensParentsAutres();
		liens.addAll(liensParentAutre);
		lienParentAutreIds = new ArrayList<Integer>();
		for (ILien lien : liensParentAutre) {
			Integer parentId = lien.parentId();
			lienParentAutreIds.add(parentId);
		}
		indexLiens();
		return this;
	}
	
	public MaquetteDataBuilder initialiserAvec(List<ILienConsultation> liens) {
		lienConsultations = liens;
		extractLienIds();
		recreerArborescence();
		indexLienConsultations();
		return this;
	}

	private void extractLienIds() {
		lienChildIds = Lists.transform(lienConsultations, new Function<ILienConsultation, Integer>() {
			public Integer apply(ILienConsultation input) {
				return input.childId();
			}
		});
		lienParentIds = Lists.transform(lienConsultations, new Function<ILienConsultation, Integer>() {
			public Integer apply(ILienConsultation input) {
				return input.parentId();
			}
		});
		lienIds = Lists.transform(lienConsultations, new Function<ILienConsultation, Integer>() {
			public Integer apply(ILienConsultation input) {
				return input.id();
			}
		});
	}
	
	
	private void recreerArborescence() {
		for (ILienConsultation lienConsultation : lienConsultations) {
			final ILienConsultation lc = lienConsultation;
			// Remplissage des enfants
			Collection<ILienConsultation> children = Collections2.filter(lienConsultations, new Predicate<ILienConsultation>() {
				public boolean apply(ILienConsultation lien) {
					return StringUtils.equals(lc.idLienContexte(), lien.idLienContexteParent());
				}
			});
			lc.setChildren(Lists.newArrayList(children));
			// Remplissage du parent
			ILienConsultation parent = Iterators.find(lienConsultations.iterator(), new Predicate<ILienConsultation>() {
				public boolean apply(ILienConsultation input) {
					return StringUtils.equals(lc.idLienContexteParent(), input.idLienContexte());
				}
			}, null);
			lc.setParent(parent);
		}
	}

	/**
	 * @return les liens parents autres que ceux "normaux" ie ceux fetchés à partir de la racine avec la requête hiérarchique.
	 */
	private NSArray<EOLien> fetchLiensParentsAutres() {
		EOQualifier qualifier = EOLien.CHILD_ID.in(new NSArray<Integer>(lienChildIds)).and(ERXQ.not(EOLien.ID.in(new NSArray<Integer>(lienIds))));
		NSArray<EOLien> liensAvecDoublons = EOLien.fetchSco_Liens(edc, qualifier, null);
		NSArray<EOLien> liensParentsAutres = ERXArrayUtilities.arrayWithoutDuplicates(liensAvecDoublons);
		return liensParentsAutres;
	}

	/**
	 * ajoute un lien et indexe les liens
	 * @param lien lien ajouté
	 * @return this
	 */
	public MaquetteDataBuilder addLien(ILien lien) {
		if (!liens.contains(lien)) {
			liens.add(lien);
			lienChildIds.add(lien.childId());
			indexLiens();
		}
		return this;
	}

	public List<ILien> getLiens() {
		return ImmutableList.copyOf(liens);
	}

	/**
	 * charge les composants et les indexe
	 * @return this
	 */
	public MaquetteDataBuilder loadComposants() {

		NSArray<Integer> idsComposant = new NSMutableArray<Integer>();
		idsComposant.addAll(lienChildIds);
		idsComposant.addAll(lienParentIds);
		//idsComposant.addAll(lienParentAutreIds);
		idsComposant = ERXArrayUtilities.arrayWithoutDuplicates(idsComposant);

		composants = new ArrayList<IComposant>(EOComposant.fetchSco_Composants(edc, EOComposant.ID.in(idsComposant), null));

		indexComposants();

		return this;
	}

	private void indexComposants() {
		indexComposantsByTypeComposantId();
		indexComposantsById();
	}

	public List<IComposant> getComposants() {
		return ImmutableList.copyOf(composants);
	}

	/**
	 * @return charge les créditables
	 */
	public MaquetteDataBuilder loadCreditables() {
		ERXOrQualifier qualifier = EOCreditable.COMPOSANT_ID.in(new NSArray<Integer>(lienChildIds)).or(EOCreditable.LIEN_ID.in(new NSArray<Integer>(lienIds)));
		NSArray<EOCreditable> creditablesTmp = EOCreditable.fetchSco_Creditables(edc, qualifier, null);
		creditables = new ArrayList<ICreditable>(creditablesTmp);

		indexCreditablesByComposantId();
		indexCreditablesByLienId();

		return this;

	}

	/**
	 * @return charge les charges d'enseignements
	 */
	public MaquetteDataBuilder loadChargeEnseignements() {
		chargeEnseignements = new ArrayList<IChargeEnseignement>(EOChargeEnseignement.fetchSco_ChargeEnseignements(
		    edc,
		    EOChargeEnseignement.AP_ID.in(new NSArray<Integer>(lienChildIds)).and(
		        EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT.eq((EOTypeChargeEnseignement) EOTypeChargeEnseignement.typeTheorique(edc))), null));
		indexChargeEnseignementByApId();

		return this;
	}

	/**
	 * @return charge les responsables de composants
	 */
	public MaquetteDataBuilder loadResponsablesComposants() {
		
		responsablesComposant = new ArrayList<IResponsableComposant>(EOResponsableComposant.fetchSco_ResponsableComposants(edc,
		    EOResponsableComposant.COMPOSANT.dot(EOComposant.ID).in(new NSArray<Integer>(lienChildIds)), null));
		indexResponsablesComposantByComposantId();
		return this;
	}

	private MaquetteDataBuilder indexResponsablesComposantByComposantId() {
		data.responsablesByComposantId = Multimaps.index(responsablesComposant, new Function<IResponsableComposant, Integer>() {
			public Integer apply(IResponsableComposant responsableComposant) {
				return responsableComposant.composantId();
			}
		});
		return this;
  }

	/**
	 * @param lien enlève un branche de l'arbre en partant du lien
	 * @return la maquetteData sans la branche
	 */
	public MaquetteDataBuilder removeBranche(ILien lien) {
		List<ILien> emptyList = Collections.emptyList();
		return removeBrancheSauf(lien, emptyList);
	}
	
	/**
	 * @param lien enlève un branche de l'arbre en partant du lien
	 * @return la maquetteData sans la branche
	 */
	public MaquetteDataBuilder removeBrancheSauf(ILien lien, List<ILien> liensAnePasSupprimer) {
		List<ILien> lienAndChildren = lienAndChildren(lien);
		lienAndChildren.removeAll(liensAnePasSupprimer);
		liens.removeAll(lienAndChildren);
		return this;
	}
	

	/**
	 * @return les données de la maquette
	 */
	public MaquetteData getData() {
		data.racine = lienRacine;
		return data;
	}

	private void initTypeComposants() {
		Map<String, ITypeComposant> typeComposants = new HashMap<String, ITypeComposant>();
		typeComposants.put(IDiplome.class.getSimpleName(), EOTypeComposant.typeDiplome(edc));
		typeComposants.put(IVersionDiplome.class.getSimpleName(), EOTypeComposant.typeVersionDiplome(edc));
		typeComposants.put(IParcours.class.getSimpleName(), EOTypeComposant.typeParcours(edc));
		typeComposants.put(IPeriode.class.getSimpleName(), EOTypeComposant.typePeriode(edc));
		typeComposants.put(IRegroupement.class.getSimpleName(), EOTypeComposant.typeRegroupement(edc));
		typeComposants.put(IUE.class.getSimpleName(), EOTypeComposant.typeUE(edc));
		typeComposants.put(IEC.class.getSimpleName(), EOTypeComposant.typeEC(edc));
		typeComposants.put(IAP.class.getSimpleName(), EOTypeComposant.typeAP(edc));
		typeComposants.put(IAE.class.getSimpleName(), EOTypeComposant.typeAE(edc));
		data.typeComposants = typeComposants;
	}

	public MaquetteDataBuilder indexChargeEnseignementByApId() {
		data.chargeEnseignementByApId = Maps.uniqueIndex(chargeEnseignements, new Function<IChargeEnseignement, Integer>() {
			public Integer apply(IChargeEnseignement charge) {
				return charge.apId();
			}
		});
		return this;
	}

	public MaquetteDataBuilder indexCreditablesByLienId() {
		List<ICreditable> creditableLiens = new ArrayList<ICreditable>();
		for (ICreditable c : creditables) {
			if (c.lienId() != null) {
				creditableLiens.add(c);
			}
		}
		data.creditablesByLienId = Maps.uniqueIndex(creditableLiens, new Function<ICreditable, Integer>() {
			public Integer apply(ICreditable creditable) {
				return creditable.lienId();
			}
		});
		return this;

	}

	public MaquetteDataBuilder indexCreditablesByComposantId() {
		List<ICreditable> creditableComposants = new ArrayList<ICreditable>();
		for (ICreditable c : creditables) {
			if (c.composantId() != null) {
				creditableComposants.add(c);
			}
		}
		data.creditablesByComposantId = Maps.uniqueIndex(creditableComposants, new Function<ICreditable, Integer>() {
			public Integer apply(ICreditable creditable) {
				return creditable.composantId();
			}
		});
		return this;

	}

	public MaquetteDataBuilder indexLiensById() {
		data.liensById = Maps.uniqueIndex(liens, new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.id();
			}
		});
		return this;

	}

	public MaquetteDataBuilder indexComposantsById() {
		data.composantsById = Maps.uniqueIndex(composants, new Function<IComposant, Integer>() {
			public Integer apply(IComposant composant) {
				return composant.id();
			}
		});
		return this;

	}

	/**
	 * @return maquetteData avec les liens indexés par parentId
	 */
	public MaquetteDataBuilder indexLiensByParentId() {
		data.liensByParentId = Multimaps.index(liens, new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.parentId();
			}
		});
		return this;

	}

	/**
	 * @return maquetteData avec les liens indexés par childId
	 */
	public MaquetteDataBuilder indexLiensByChildId() {
		data.liensByChildId = Multimaps.index(liens, new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.childId();
			}
		});
		return this;

	}
	
	/**
	 * @return maquetteData avec les liens indexés par parentId
	 */
	public MaquetteDataBuilder indexLienConsultationsByParentId() {
		data.liensByParentId = Multimaps.index(new ArrayList<ILien>(lienConsultations), new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.parentId();
			}
		});
		return this;

	}

	/**
	 * @return maquetteData avec les liens indexés par childId
	 */
	public MaquetteDataBuilder indexLienConsultationsByChildId() {
		data.liensByChildId = Multimaps.index(new ArrayList<ILien>(lienConsultations), new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.childId();
			}
		});
		return this;

	}

	public MaquetteDataBuilder indexLienConsultationsById() {
		liens = new ArrayList<ILien>(ERXArrayUtilities.arrayWithoutDuplicates(EOLien.fetchSco_Liens(edc, EOLien.ID.in(new NSArray<Integer>(lienIds)), null)));

		data.liensById = Maps.uniqueIndex(liens, new Function<ILien, Integer>() {
			public Integer apply(ILien lien) {
				return lien.id();
			}
		});
		return this;
	}
	
	public MaquetteDataBuilder indexLienConsultationsByIdLienContexte() {
		data.lienConsultationsById = Maps.uniqueIndex(lienConsultations, new Function<ILienConsultation, String>() {
			public String apply(ILienConsultation lien) {
				return lien.idLienContexte();
			}
		});
		return this;
	}
	
	/**
	 * @return maquetteData avec les liens indexés par type de composant
	 */
	public MaquetteDataBuilder indexComposantsByTypeComposantId() {
		data.composantsByTypeComposantId = Multimaps.index(composants, new Function<IComposant, Integer>() {
			public Integer apply(IComposant composant) {
				return composant.typeComposantId();
			}
		});
		return this;

	}

	private List<ILien> children(ILien lien) {
		List<ILien> children = new ArrayList<ILien>();
		for (ILien l : liens) {
			if (lien.childId().equals(l.parentId())) {
				children.add(l);
				children.addAll(children(l));
			}
		}
		return children;
	}

	private List<ILien> lienAndChildren(ILien lien) {
		List<ILien> lesLiens = new ArrayList<ILien>();
		lesLiens.add(lien);
		lesLiens.addAll(children(lien));
		return lesLiens;
	}

	/**
	 * @param parcours
	 * @param version
	 * @return
	 */
	public MaquetteDataBuilder filtrerParcoursDiplomePourVersion(final IParcours parcours, final IVersionDiplome version) {
		if (parcours == null) {
			return this;
		}

		Collection<ILien> lesLiens = Collections2.filter(getLiens(), new Predicate<ILien>() {
			public boolean apply(ILien lien) {
				return version.id().equals(lien.parentId()) && !parcours.id().equals(lien.childId());
			}
		});

		for (ILien l : lesLiens) {
			removeBranche(l);
		}

		indexLiens();
		return this;
	}

	/**
	 * Supprime les liens correspondant aux parcours autre que celui passé en paramètre.
	 * On évite de supprimer les liens fils (mutualisé donc) du parcours passé.
	 * 
	 * @param parcours le parcours année
	 * @return le builder
	 */
	public MaquetteDataBuilder filtrerParcoursAnnee(final IParcours parcours) {
		if (parcours == null) {
			return this;
		}
		ILien lienParcoursAnnee = findLienParcoursAnnee(parcours);
		List<ILien> liensAnePasSupprimer = lienAndChildren(lienParcoursAnnee);
		Collection<ILien> lesLiens = filtrerLiensNeCorrespondantPasAuParcours(parcours);
		for (ILien l : lesLiens) {
			removeBrancheSauf(l, liensAnePasSupprimer);
		}
		indexLiens();
		return this;
	}

	private Collection<ILien> filtrerLiensNeCorrespondantPasAuParcours(
			final IParcours parcours) {
		Collection<ILien> lesLiens = Collections2.filter(getLiens(), new Predicate<ILien>() {

			public boolean apply(ILien lien) {
				IComposant c = data.getComposant(lien.parentId());
				if (c instanceof IPeriode) {
					IPeriode p = (IPeriode) c;
					if (p.isAnnee()) {
						return !parcours.id().equals(lien.childId());
					}
				}
				return false;
			}

		});
		return lesLiens;
	}

	private ILien findLienParcoursAnnee(final IParcours parcours) {
		ILien lienParcoursAnnee = Iterators.find(getLiens().iterator(), new Predicate<ILien>() {
			
			public boolean apply(ILien lien) {
				return parcours.id().equals(lien.childId());
			}
			
		});
		return lienParcoursAnnee;
	}

	public MaquetteDataBuilder filtrerAnneesAutresQue(final Integer annee) {
		if (annee == null) {
			return this;
		}
		Collection<IComposant> periodes = getPeriodes();
		Collection<IComposant> annees = getAnneesAutreQue(annee, periodes);
		IComposant composantAnnee = getAnnee(annee, periodes);
		ILien lienAnnee = getLienAnnee(composantAnnee);
		List<ILien> liensAnePasSupprimer = lienAndChildren(lienAnnee);

		for (final IComposant a : annees) {
			Collection<ILien> liens = Collections2.filter(getLiens(), new Predicate<ILien>() {
				public boolean apply(ILien lien) {
					return lien.childId().equals(a.id());
				}
			});
			for (ILien l : liens) {
				removeBrancheSauf(l, liensAnePasSupprimer);
			}
		}

		indexLiens();

		return this;
	}

	private ILien getLienAnnee(final IComposant composantAnnee) {
		ILien lienAnnee = Iterators.find(getLiens().iterator(), new Predicate<ILien>() {
			public boolean apply(ILien l) {
				return l.childId().equals(composantAnnee.id());
			}
		});
		return lienAnnee;
	}

	private IComposant getAnnee(final Integer annee, Collection<IComposant> periodes) {
		final IComposant composantAnnee = Iterators.find(periodes.iterator(), new Predicate<IComposant>() {
			public boolean apply(IComposant c) {
				IPeriode p = (IPeriode) c;
				return p.isAnnee() && annee.equals(p.ordre());
			}
		});
		return composantAnnee;
	}

	private Collection<IComposant> getPeriodes() {
		Collection<IComposant> periodes = Collections2.filter(getComposants(), new Predicate<IComposant>() {
			public boolean apply(IComposant c) {
				return c.typeComposantId().equals(EOTypeComposant.typePeriode(edc).id());
			}
		});
		return periodes;
	}

	private Collection<IComposant> getAnneesAutreQue(final Integer annee, Collection<IComposant> periodes) {
		Collection<IComposant> annees = Collections2.filter(periodes, new Predicate<IComposant>() {
			public boolean apply(IComposant c) {
				IPeriode p = (IPeriode) c;
				return p.isAnnee() && !annee.equals(p.ordre());
			}
		});
		return annees;
	}

	private void indexLiens() {
		indexLiensByParentId();
		indexLiensByChildId();
		indexLiensById();
	}

	private void indexLienConsultations() {
		indexLienConsultationsByParentId();
		indexLienConsultationsByChildId();
		indexLienConsultationsById();
		indexLienConsultationsByIdLienContexte();
	}
	
	public MaquetteDataBuilder lienRacine(ILien lienRacine) {
		this.lienRacine = lienRacine;
		this.composantRacineId = lienRacine.childId();
		return this;
	}

}
