package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;

/**
 * Interface qui définit le service permettant de récupérer le creditable d'un composant ou d'un lien composer
 */
public interface CreditableService {

	
	/**
	 * Renvoie le crédit ECTS connaissant les créditables associés
	 * @param creditableComposant creditable associé au composant
	 * @param creditableLien créditable associé au lien
	 * @return creditECTS du composant si il existe et du lien composer si il a été surchargé dans ce dernier
	 */
	BigDecimal creditsEcts(ICreditable creditableComposant, ICreditable creditableLien);

	
	
	/**
	 * Renvoie le crédit ECTS connaissant le lien composer et le composant
	 * @param composant composant dont on souhaite connaitre le credit ETCS
	 * @param lienComposer lien composer dont on souhaite connaitre le credit ETCS 
	 * @return creditECTS du composant si il existe et du lien composer si il a été surchargé dans ce dernier
	 */
	BigDecimal creditsEcts(IComposant composant, ILienComposer lienComposer);
	
	
	/**
	 * Renvoie le crédit ECTS connaissant le lien composer
	 * @param lienComposer lien composer dont on souhaite connaitre le credit ETCS
	 * @return creditECTS de l'enfant du lien composer si il existe et du lien composer si il a été surchargé dans ce dernier
	 */
	BigDecimal creditsEcts(ILienComposer lienComposer);
	

	
	
}
