package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;

/**
*  Services d'extraction d'information d'une maquetteData
 */
public interface MaquetteDataConsultation {

	/**
	 * Pour l'instant cette méthode renvoie le premier lien entre le composant racine et le composant recherche
	 * TODO à remplacer progressivement par des lienconsultation partout
	 * @param versionDiplome la version de diplome dans laquelle on fait la recherche
	 * @param composantRecherche composant recherche
	 * @return le lienconsultation du composant recherche
	 */
	ILienConsultation getLien(IVersionDiplome versionDiplome, IComposant composantRecherche);

	/**
	 * @param composantPere composant pere
	 * @param typeComposant type de composants
	 * @return liste des lienConsultation dont les composants sont de type typeComposant
	 */
	List<ILienConsultation> getLiensConsultationsSous(ILienConsultation composantPere, ITypeComposant typeComposant);

	/**
	 * @param lienConsultation lienconsulatation
	 * @return le composant (fils) attaché à ce lien
	 */
	IComposant getComposant(ILienConsultation lienConsultation);

	/**
	 * @param composantPere le composant pere
	 * @param composantFils le composant fils 
	 * @return le lien de consultation direct entre le pere et le fils
	 */
	ILienConsultation getLienDirect(IComposant composantPere, IComposant composantFils);
	
}
