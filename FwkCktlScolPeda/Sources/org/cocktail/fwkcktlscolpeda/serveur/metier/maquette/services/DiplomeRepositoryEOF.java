package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;

public class DiplomeRepositoryEOF implements DiplomeRepository {

	private EOEditingContext editingContext;
	
	public DiplomeRepositoryEOF(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<IDiplome> getListeDiplomes(Integer annee, boolean isEnModelisation) {
		NSArray<EOLienConsultation> versions = EOLienConsultation.fetchSco_LienConsultations(
						editingContext, 
						EOLienConsultation.V_DIP_ANNEE.eq(annee), null);
		NSArray<Integer> idsDiplomes = (NSArray<Integer>) versions.valueForKey(EOLienConsultation.ID_DIPLOME_KEY);
		ERXFetchSpecification<EODiplome> fspec = new ERXFetchSpecification<EODiplome>(EODiplome.ENTITY_NAME);
		fspec.setQualifier(EODiplome.ID.in(idsDiplomes).and(qualifierHabilitation(isEnModelisation)));
		fspec.setSortOrderings(EODiplome.CODE.ascs());
		fspec.setUsesDistinct(true);
		NSArray<EODiplome> eos = fspec.fetchObjects(editingContext);
		return new ArrayList<IDiplome>(eos);
	}

	private EOQualifier qualifierHabilitation(boolean isEnModelisation) {
		EOQualifier qualifier = null;
		if (isEnModelisation) {
			qualifier = EODiplome.HABILITATION_REQUISE.eq(1);
		} else {
			EOStatutFormation statutEnProduction = EOStatutFormation.statutEnProduction(editingContext);
			EOStatutFormation statutFictive = EOStatutFormation.statutFictive(editingContext);
			qualifier = EODiplome.STATUT_FORMATION.inObjects(statutEnProduction, statutFictive).or(EODiplome.HABILITATION_REQUISE.eq(0));
		}
		return qualifier;
	}

}
