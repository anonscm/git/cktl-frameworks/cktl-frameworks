package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.common.service.AbstractServiceImpl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Service offrant des API qui permettent 
 * de récupérer des éléments spécifiques 
 * de la maquette
 * 
 * @author jlafourc
 *
 */
public class ExplorationMaquetteService extends AbstractServiceImpl {

	private static final int CACHE_ENTRY_EXPIRATION = 24;
	private static final Logger LOG = Logger.getLogger(ExplorationMaquetteService.class);
	
	private static Cache<String, List<IParcours>> specialitesCache =
			CacheBuilder.newBuilder().expireAfterWrite(CACHE_ENTRY_EXPIRATION, TimeUnit.HOURS).recordStats().build();
	private static Cache<String, List<IParcours>> specialitesEtNiveauCache =
			CacheBuilder.newBuilder().expireAfterWrite(CACHE_ENTRY_EXPIRATION, TimeUnit.HOURS).recordStats().build();
	private static Cache<String, List<IParcours>> parcoursPourDiplomeEtAnneeCache =
			CacheBuilder.newBuilder().expireAfterWrite(CACHE_ENTRY_EXPIRATION, TimeUnit.HOURS).recordStats().build();
	
	private boolean useCache;
	
	/**
	 * Il est recommandé d'utiliser ce constructeur 
	 * si l'application n'utilise pas l'injection de Flows
	 * @param editingContext l'editingContext de travail
	 * @param useCache true si on met en cache les résultats
	 */
	public ExplorationMaquetteService(EOEditingContext editingContext, boolean useCache) {
		super(editingContext);
		this.useCache = useCache;
	}
	
	/**
	 * @param diplome le diplome sur lequel on travaille
	 * @param annee l'année pour laquelle on veut les spécialités
	 * @return la liste des spécialités
	 */
	public List<IParcours> getSpecialitesDeVersionDiplome(final IDiplome diplome, final Integer annee) {
		List<IParcours> parcours = null;
		if (useCache) {
			parcours = new NSMutableArray<IParcours>();
			for (IParcours parcoursTmp : getSpecialitesDeVersionDiplomeFromCache(diplome, annee)) {
				parcours.add(ERXEOControlUtilities.localInstanceOfObject(editingContext, (EOParcours) parcoursTmp));
			}
		} else {
			parcours = fetchSpecialitesDeVersionDiplome(diplome, annee);
		}
		return parcours;
	}

	private List<IParcours> getSpecialitesDeVersionDiplomeFromCache(
			final IDiplome diplome, final Integer annee) {
		final String key = String.format("%s_%s", diplome.id(), annee);
		Callable<List<IParcours>> fetcher = new Callable<List<IParcours>>() {
			public List<IParcours> call() throws Exception {
				LOG.debug("Cache miss : récupération dans le cache des spécialités correspondant à la clef " + key);
				return fetchSpecialitesDeVersionDiplome(diplome, annee);
			}
		};
		return getParcoursFromCache(key, specialitesCache, fetcher);
	}
	
	private List<IParcours> getParcoursFromCache(String key, Cache<String, List<IParcours>> cache, Callable<List<IParcours>> fetcher) {
		try {
			LOG.debug("Récupération dans le cache des parcours correspondant à la clef " + key);
			return cache.get(key, fetcher);
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	private List<IParcours> fetchSpecialitesDeVersionDiplome(final IDiplome diplome, final Integer annee) {
		IVersionDiplome versionDiplome = diplome.getVersionDiplome(annee);
		return getSpecialitesDeVersionDiplome(versionDiplome);
	}
	
	/**
	 * @param specialite la spécialité
	 * @param niveau le niveau
	 * @return la liste des parcours en dessous de la période du niveau
	 */
	public List<IParcours> getParcoursPourSpecialiteEtNiveau(IParcours specialite, Integer niveau) {
		List<IParcours> parcours = null;
		if (useCache) {
			parcours = new NSMutableArray<IParcours>();
			for (IParcours parcoursTmp : getParcoursPourSpecialiteEtNiveauFromCache(specialite, niveau)) {
				parcours.add(ERXEOControlUtilities.localInstanceOfObject(editingContext, (EOParcours) parcoursTmp));
			}
		} else {
			parcours = fetchParcoursPourSpecialiteEtNiveau(specialite, niveau);
		}
		return parcours;
	}
	
	private List<IParcours> getParcoursPourSpecialiteEtNiveauFromCache(
			final IParcours specialite, final Integer niveau) {
		final String key = String.format("%s_%s", specialite.id(), niveau);
		Callable<List<IParcours>> fetcher = new Callable<List<IParcours>>() {
			public List<IParcours> call() throws Exception {
				LOG.debug("Cache miss : récupération dans le cache des parcours correspondant à la clef " + key);
				return fetchParcoursPourSpecialiteEtNiveau(specialite, niveau);
			}
		};
		return getParcoursFromCache(key, specialitesEtNiveauCache, fetcher);
	}

	private List<IParcours> fetchParcoursPourSpecialiteEtNiveau(
			IParcours specialite, Integer niveau) {
		EOQualifier qualifier =
 				ERXQ.and(
 					EOLien._PARENT_PARCOURS.eq((EOParcours) specialite),
 					EOLien._CHILD_PERIODE.dot(EOPeriode.ORDRE).eq(niveau),
 					EOLien._CHILD_PERIODE.dot(EOPeriode.TO_TYPE_PERIODE).eq(EOTypePeriode.typePeriodeAnnee(editingContext()))
 				);
	
		EOLien lienAnnee = ERXArrayUtilities.firstObject(EOLien.fetchSco_Liens(editingContext, qualifier, null));
		return getParcoursPourUneAnnee(lienAnnee);
	}

	/**
	 * L'appel à cette méthode implique que le diplome n'a pas 
	 * de spécialité sinon elle ne renverra rien. 
	 * @param diplome le diplome
	 * @param annee l'annnée
	 * @param niveau le niveau dans le diplome
	 * @return renvoie la liste des parcours
	 */
	public List<IParcours> getParcoursPourDiplomeEtAnnee(IDiplome diplome, Integer annee, Integer niveau) {
		
		List<IParcours> parcours = null;
		if (useCache) {
			parcours = new NSMutableArray<IParcours>();
			for (IParcours parcoursTmp : getParcoursPourDiplomeEtAnneeFromCache(diplome, annee, niveau)) {
				parcours.add(ERXEOControlUtilities.localInstanceOfObject(editingContext, (EOParcours) parcoursTmp));
			}
		} else {
			parcours = fetchParcoursPourDiplomeEtAnnee(diplome, annee, niveau);
		}
		return parcours;
	}
	
	private List<IParcours> getParcoursPourDiplomeEtAnneeFromCache(final IDiplome diplome, final Integer annee, final Integer niveau) {
		final String key = String.format("%s_%s_%s", diplome.id(), annee.toString(), niveau);
		
		Callable<List<IParcours>> fetcher = new Callable<List<IParcours>>() {
			public List<IParcours> call() throws Exception {
				LOG.debug("Cache miss : récupération dans le cache des parcours correspondant à la clef " + key);
				return fetchParcoursPourDiplomeEtAnnee(diplome, annee, niveau);
			}
		};
		return getParcoursFromCache(key, parcoursPourDiplomeEtAnneeCache, fetcher);
	
	}

	private List<IParcours> fetchParcoursPourDiplomeEtAnnee(IDiplome diplome,
			Integer annee, Integer niveau) {
		IVersionDiplome versionDiplome = diplome.getVersionDiplome(annee);
		EOQualifier qualifier =
			ERXQ.and(
				EOLien._PARENT_VERSION_DIPLOME.eq((EOVersionDiplome) versionDiplome),
				EOLien._CHILD_PERIODE.dot(EOPeriode.ORDRE).eq(niveau),
				EOLien._CHILD_PERIODE.dot(EOPeriode.TO_TYPE_PERIODE).eq(EOTypePeriode.typePeriodeAnnee(editingContext()))
			);
 		EOLien lienAnnee = ERXArrayUtilities.firstObject(EOLien.fetchSco_Liens(editingContext(), qualifier, null));
 		return getParcoursPourUneAnnee(lienAnnee);
	}

	/**
	 * @param versionDiplome la version de diplome sur laquelle
	 * on travaille
	 * @return la liste des spécialités se trouvant sous la version de diplome
	 */
	public List<IParcours> getSpecialitesDeVersionDiplome(IVersionDiplome versionDiplome) {
		
		final List<IParcours> specialites = new ArrayList<IParcours>();
		
		EOQualifier qualifier = 
			EOLien.CHILD_ID.eq(versionDiplome.id())
				.and(EOLien.TYPE_LIEN.eq(EOTypeLien.typeVersionner(editingContext)));
		
		ILien lienVersionner = EOLien.fetchSco_Lien(editingContext, qualifier);
		
		MaquetteProcessor processor = new AbstractMaquetteProcessorIterativeImpl(editingContext) {
			
			public Boolean shouldProcessChildrenOf(ILien lien) {
				IComposant c = lien.child();
				if (c instanceof IPeriode) {
					return false;
				}
				return true;
			}
			
			public void performActionsOn(ILien lien) {
				if (lien.child() instanceof IParcours) {
					specialites.add((IParcours) lien.child());
				}
			}
		};
		
		processor.setRoot(lienVersionner);
		processor.proceeed();
		
		return specialites;
		
	}
	
	/**
	 * @param lienAnnee Un lien qui à l'année en question comme enfant
	 * @return la liste des parcours placés directement sous l'année
	 */
	public List<IParcours> getParcoursPourUneAnnee(ILien lienAnnee) {
		final List<IParcours> parcours  = new ArrayList<IParcours>();
		
		MaquetteProcessor processor = new AbstractMaquetteProcessorIterativeImpl(editingContext) {
			
			public Boolean shouldProcessChildrenOf(ILien lien) {
				IComposant c = lien.child();
				if (c instanceof IPeriode) {
					IPeriode p = (IPeriode) c;
					return p.isAnnee();
				}
				return true;
			}
			
			public void performActionsOn(ILien lien) {
				if (lien.child() instanceof IParcours) {
					parcours.add((IParcours) lien.child());
				}
			}
		};
		
		processor.setRoot(lienAnnee);
		processor.proceeed();
		
		return parcours;
	}
	
	/**
	 * Retourne tous les parcours (ce qui est entre crochet) entre le niveau et le parcours (années) pour un diplôme/année/spécialité/niveau/[p1/p2/parcours].
	 * <p>
	 * L'ordre de retour est toujours du niveau le plus fin (la feuille) vers le niveau parent.
	 * 
	 * @param diplome un diplôme
	 * @param annee une année
	 * @param parcoursDiplome une spécialité (un parcours diplôme)
	 * @param niveau un niveau
	 * @param parcoursAnnee un parcours année
	 * @return la liste des parcours année
	 */
	public List<IParcours> getParcoursPourSpecialiteNiveauEtParcours(IDiplome diplome, Integer annee, IParcours parcoursDiplome, Integer niveau, IParcours parcoursAnnee) {
		final List<IParcours> parcours  = new ArrayList<IParcours>();

		AbstractMaquetteProcessorRecursiveAvecContraintesImpl processor = new AbstractMaquetteProcessorRecursiveAvecContraintesImpl(editingContext) {
			
			public void performActionsOn(ILien lien) {
				if (isTargetReach() && isNiveauEstPasse() && lien.child() instanceof IParcours) {
					parcours.add((IParcours) lien.child());
				}
			}
		};
		
		processor.setDiplome(diplome);
		processor.setAnnee(annee);
		processor.setParcoursDiplome(parcoursDiplome);
		processor.setNiveau(niveau);
		processor.setParcoursAnnee(parcoursAnnee);
		processor.setComposantCible(parcoursAnnee);
		
		processor.proceeed();
		
		return parcours;
	}
	
	
	/**
	 * @param versionDiplome la version de diplome sur laquelle
	 * on travaille
	 * @return la liste des spécialités se trouvant sous la version de diplome
	 */
	public List<IPeriode> getPeriodeAnneDeVersionDiplome(IVersionDiplome versionDiplome) {
		
		final List<IPeriode> periodes = new ArrayList<IPeriode>();
		
		EOQualifier qualifier = 
			EOLien.CHILD_ID.eq(versionDiplome.id())
				.and(EOLien.TYPE_LIEN.eq(EOTypeLien.typeVersionner(editingContext)));
		
		ILien lienVersionner = EOLien.fetchSco_Lien(editingContext, qualifier);
		
		MaquetteProcessor processor = new AbstractMaquetteProcessorIterativeImpl(editingContext) {
			
			public Boolean shouldProcessChildrenOf(ILien lien) {
				IComposant c = lien.child();
				if (c instanceof IParcours) {
					return false;
				}
				return true;
			}
			
			public void performActionsOn(ILien lien) {
				if (lien.child() instanceof IPeriode) {
					if (((IPeriode)lien.child()).isAnnee()) {
						periodes.add((IPeriode) lien.child());
					}
				}
			}
		};
		
		processor.setRoot(lienVersionner);
		processor.proceeed();
		
		return periodes;
		
	}
	
	/**
	 * @param lienParcours Un lien qui à le parcours en question comme enfant
	 * @return la liste des parcours placés directement sous le parcours
	 */
	public List<IPeriode> getPeriodeAnneeDeParcours(ILien lienParcours) {
	
		final List<IPeriode> periodes = new ArrayList<IPeriode>();
		MaquetteProcessor processor = new AbstractMaquetteProcessorIterativeImpl(editingContext) {
		
		public Boolean shouldProcessChildrenOf(ILien lien) {
			IComposant c = lien.child();
			if (c instanceof IParcours) {
				IPeriode p = (IPeriode) c;
				return p.isAnnee();
			}
			return true;
		}
		
		public void performActionsOn(ILien lien) {
			if (lien.child() instanceof IPeriode) {
				if (((IPeriode)lien.child()).isSemestre()) {
					periodes.add((IPeriode) lien.child());
				}
			}
		}
		};
		
		processor.setRoot(lienParcours);
		processor.proceeed();
		
		return periodes;
		
	}
	
	/**
	 * @param lienParcours Un lien qui à le parcours en question comme enfant
	 * @return la liste des parcours placés directement sous le parcours
	 */
	public List<IPeriode> getPeriodeSemestreDeParcours(ILien lienParcours) {
	
		final List<IPeriode> periodes = new ArrayList<IPeriode>();
		MaquetteProcessor processor = new AbstractMaquetteProcessorIterativeImpl(editingContext) {
		
		public Boolean shouldProcessChildrenOf(ILien lien) {
			IComposant c = lien.child();
			if (c instanceof IPeriode) {
				IPeriode p = (IPeriode) c;
				return p.isAnnee();
			}
			return true;
		}
		
		public void performActionsOn(ILien lien) {
			if (lien.child() instanceof IPeriode) {
				if (((IPeriode)lien.child()).isSemestre()) {
					periodes.add((IPeriode) lien.child());
				}
			}
		}
		};
		
		processor.setRoot(lienParcours);
		processor.proceeed();
		
		return periodes;
		
	}

}
