package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

import com.google.common.collect.Lists;

public class ComposantIterator implements Iterator<ILien> {

    private ILien current;
    private List<ILien> liensArbre;
    private Iterator<ILien> brancheIterator;
    
    private ComposantIterator(List<ILien> liensArbre, ILien leaf) {
        this.liensArbre = liensArbre;
        this.brancheIterator = etablirBranche(leaf).iterator();
    }

    public static ComposantIterator branchIterator(List<ILien> tousLiensArbre, ILien leaf) {
        return new ComposantIterator(tousLiensArbre, leaf);
    }

    private List<ILien> etablirBranche(ILien leaf) {
        List<ILien> branche = Lists.newArrayList();
        current = leaf;
        while (current != null) {
            List<? extends ILien> parents = current.parent().liensChildsWithoutDuplicate();
            current = parent(parents);
            if (current != null) {
                branche.add(current);
            }
        }
        return branche;
    }

    private ILien parent(List<? extends ILien> parents) {
        ILien res = null;
        for (ILien parent : parents) {
            if (liensArbre.contains(parent)) {
                res = parent;
                break;
            }
        }
        return res;
    }
    
    public boolean hasNext() {
        return brancheIterator.hasNext();
    }
    
    public ILien next() {
        return brancheIterator.next();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

}
