package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Version iterative.
 * 
 * @author jlafourc
 */
public abstract class AbstractMaquetteProcessorIterativeImpl extends AbstractMaquetteProcessorImpl {

	private static final Logger LOG = Logger.getLogger(AbstractMaquetteProcessorIterativeImpl.class);

	/**
	 * @param editingContext nécessaire pour fetcher les fils 
	 */
	public AbstractMaquetteProcessorIterativeImpl(EOEditingContext editingContext) {
		super(editingContext);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void proceeed() {
		if (getRoot() == null) {
			LOG.warn("La root est nulle. Arret du parcours de la maquette.");
			return;
		}
		
		LinkedList<ILien> liens = new LinkedList<ILien>();
		liens.add(getRoot());
		
		while (!liens.isEmpty()) {
		    ILien lien = liens.remove();
		    if (shouldProcessChildrenOf(lien)) {
		        liens.addAll(getFils(lien));
		    }
		    performActionsOn(lien);
		}
	}
}
