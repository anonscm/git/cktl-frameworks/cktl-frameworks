package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * 
 * Repository pour récupérer ce qui concerne l'arbo des composants, les liens...
 * 
 * 
 * @author Alexis Tual
 *
 */
public interface ComposantRepository {
    
    /**
     * @param rootComposant le composant racine 
     * @return le composant racine et le graphe descendant 
     */
    IComposant initialiserGrapheComposant(IComposant rootComposant);
    

	/**
	 * @param typeComposant  type de composant que l'on souhaite pre-fetcher
	 * @param annee  annee pour laquelle on cherche les composants de ce type
	 * @return liste des composants d'un certain type et qui sont relies à une annee donnee et qui sont visible en modelisation
	 */
	List<IComposant> getListeComposantsModelisation(ITypeComposant typeComposant, Integer annee);
		
	/**
	 * @param typeComposant  type de composant que l'on souhaite pre-fetcher
	 * @param annee  annee pour laquelle on cherche les composants de ce type
	 * @return liste des composants d'un certain type et qui sont relies à une annee donnee 
	 */
	List<IComposant> getListeComposants(ITypeComposant typeComposant, Integer annee);
	
	/**
	 * @param typeComposant  type de composant que l'on souhaite pre-fetcher
	 * @param annee  annee pour laquelle on cherche les composants de ce type
	 * @return liste des composants d'un certain type et qui sont relies à une annee donnee et qui n'appartiennet à aucun diplome 
	 */
	List<IComposant> getListeComposantsHorsDiplome(ITypeComposant typeComposant, Integer annee);
	
	/**
	 * @param typeComposant  type de composant que l'on souhaite pre-fetcher
	 * @param annee  annee pour laquelle on cherche les composants de ce type
	 * @return liste des composants d'un certain type et qui sont relies à une annee donnee et qui sont visible en maquettage
	 */
	List<IComposant> getListeComposantsMaquettage(ITypeComposant typeComposant, Integer annee);
	
	/**
	 * @param typesComposant les types de composant que l'on souhaite récupérer
	 * @param annee  annee pour laquelle on cherche les composants de ce type
	 * @return liste des composants des types passés et qui sont relies à une annee donnee et qui sont visible en maquettage
	 */
	List<IComposant> getListeComposantsPlusieursTypesMaquettage(Integer annee, ITypeComposant...typesComposant);
	
	/**
	 * @param editingContext : l'editing context
	 * @param anneeDebutHabilitation : annee de debut de l'habilitation en cours
	 * @param anneeFinHabilitation : annee de fin de l'habilitation en cours
	 * @param anneeDepart : annee de depart pour laquelle on recherche les diplomes
	 * @param anneeCible : annee cible sur laquelle on souhaite les versionner
	 * @return List<IComposant> : liste des diplomes a versionner d'une annee de depart sur une annee cible
	 */
	List<IComposant> getListeDiplomesAVersionner(EOEditingContext editingContext, Integer anneeDebutHabilitation, Integer anneeFinHabilitation, Integer anneeDepart, Integer anneeCible);
	
	/**
	 * @param editingContext : l'editing context
	 * @param anneeReference : annee de reference pour laquelle on recherche les diplomes
	 * @param anneeCible : annee cible pour laquelle on souhaite habiliter
	 * @return List<IComposant> : liste des diplomes a habiliter 
	 */
	List<IComposant> getListeDiplomesAHabiliter(EOEditingContext editingContext, Integer anneeReference, Integer anneeCible);
	
	/**
	 * 
	 * @param editingContext the editing context
	 * @param typeComposant le type de composant fils que les ue et les ec doivent avoir
	 * @param annee  annee universitaire
	 * @return la liste des UE et Ec ayant des inscription péda pour l'année universitaire et étant parents du type de composant passé
	 */
	List< IComposant> getListeComposantAvecEnfantsEtIP(EOEditingContext editingContext, ITypeComposant typeComposant, Integer annee);
	
	/**
	 * @param editingContext the editing context
	 * @param annee annee universitaire
	 * @return liste des diplomes qui ont une version pour l'année universitaire
	 */
	List<IDiplome> getListDiplomes(EOEditingContext editingContext, Integer annee);
	
	/**
	 * renvoie la liste des composants visibles pour une année donnée 
	 * associés à une liste de structures
	 * @param typeComposant : type de composant
	 * @param annee : annee a laquelle appartient la version de diplome du composant
	 * @param structuresId : liste des structures concernées
	 * @return liste de composant
	 */
	public List<IComposant> getListeComposantsDansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId);
	
	/**
	 * renvoie la liste des composants visibles pour une année donnée 
	 * associés à une liste de structures ET 
	 * sans association à une structure
	 * @param typeComposant : type de composant
	 * @param annee : annee a laquelle appartient la version de diplome du composant
	 * @param structuresId : liste des structures concernées
	 * @return liste de composant
	 */
	public List<IComposant> getListeComposantsDansStructuresEtSansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId);
		
}
