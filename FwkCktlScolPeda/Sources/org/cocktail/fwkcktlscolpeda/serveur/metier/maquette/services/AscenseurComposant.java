package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

/**
 * Ascenseur de composant: permet de monter et descendre un composant dans une relation parent/enfant
 */
public class AscenseurComposant {

	/**
	 * @param lien lien entre le parent (fixe) et l'enfant qu'il faut monter dans la relation l'enfant est remonté de 1 (lorsque c'est possible)
	 */
	public void monter(ILien lien) {
		if (lien.ordre() == 1) {
			return;
		}
		List<ILien> liens = ordonneParOrdre(lien);

		Integer index = liens.indexOf(lien);
		Collections.swap(liens, index, index - 1);

		renumeroteOrdre(liens);

	}

	/**
	 * @param lien lien entre le parent (fixe) et l'enfant qu'il faut descendre dans la relation l'enfant est descendu de 1 (lorsque c'est possible)
	 */
	public void descendre(ILien lien) {
		if (lien.ordre() == lien.parent().liensParents().size()) {
			return;
		}

		List<ILien> liens = ordonneParOrdre(lien);

		Integer index = liens.indexOf(lien);
		Collections.swap(liens, index, index + 1);

		renumeroteOrdre(liens);
	}

	@SuppressWarnings("unchecked")
	private List<ILien> ordonneParOrdre(ILien lien) {
		IComposant parent = lien.parent();
		// pour supprimer les éléments dupliqués
		Set<ILien> setLiens = new HashSet<ILien>((List<ILien>) parent.liensParents());
		List<ILien> liens = new ArrayList<ILien>(setLiens);

		Collections.sort(liens, new Comparator<ILien>() {
			public int compare(ILien lien1, ILien lien2) {
				return (lien1.ordre() - lien2.ordre());
			}
		});
		return liens;
	}

	private void renumeroteOrdre(List<ILien> liens) {
		Integer ordre = 1;
		for (ILien iLien : liens) {
			iLien.setOrdre(ordre++);
		}
	}

}
