package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOCnu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICnu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class SectionCnuRepository {

    public List<? extends ICnu> fetchSectionsCnu(EOEditingContext editingContext, boolean sousSections) {
        EOQualifier qualifier = EOCnu.TEM_VALIDE.eq("O");
        // Si on n'affiche pas les sous-sections
        if (!sousSections) {
            qualifier = ERXQ.and(qualifier, EOCnu.TEM_COMPOSEE.eq("O").or(EOCnu.C_SOUS_SECTION_CNU.isNull()).or(EOCnu.C_SOUS_SECTION_CNU.eq("00").or(EOCnu.C_SOUS_SECTION_CNU.eq("000"))));
        }
        return EOCnu.fetchAll(editingContext, qualifier, EOCnu.C_SECTION_CNU.asc().then(EOCnu.C_SOUS_SECTION_CNU.asc()));
    }
    
}
