package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

/**
 * Calcule la somme des creditables pour un ensemble de composants donnés
 */
public interface CalculateurSommeCreditECTS {
	
	
	/**
	 * calcule recursivement la somme des credit ECTS pour le composant et ses enfants 
	 * @param lien : composant racine à partir duquel on refait le calcul des credit ECTS 
	 */
	void calcule(ILien lien);
		
}
