package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;

/**
 * Classe de service permettant de récupérer le creditable d'un composant ou d'un lien composer
 */
public class CreditableServiceImpl implements CreditableService {

	private final static Logger LOG = Logger.getLogger(CreditableServiceImpl.class);
	
	/**
	 *  {@inheritDoc}
	 */
	public BigDecimal creditsEcts(ICreditable creditableComposant, ICreditable creditableLien) {

		BigDecimal ects = null;

		if (creditableComposant != null && creditableComposant.creditECTS() != null) {
			ects = creditableComposant.creditECTS();
			LOG.debug("Ects du creditable du composant [" + creditableComposant.composantId() + "] : " + ects);
		}

		//si le lien composer existe alors on surcharge avec le creditable du lien si ce dernier est non null
		if (creditableLien != null && creditableLien.creditECTS() != null) {
			ects = creditableLien.creditECTS();
			LOG.debug("Ects du creditable du lien [" + creditableLien.lienId() + "] : " + ects);
		}

		return ects;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public BigDecimal creditsEcts(IComposant composant, ILienComposer lienComposer) {
		ICreditable creditableComposant = composant.creditable();
		ICreditable creditableLien = null;
		if (lienComposer != null) {
			creditableLien = lienComposer.creditable();
		}

		return creditsEcts(creditableComposant, creditableLien);
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal creditsEcts(ILienComposer lienComposer) {
		return creditsEcts(lienComposer.child(), lienComposer);
	}


}
