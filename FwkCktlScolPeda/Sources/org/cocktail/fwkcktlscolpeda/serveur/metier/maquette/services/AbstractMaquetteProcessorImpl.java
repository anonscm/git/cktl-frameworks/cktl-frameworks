package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

import com.google.common.collect.Lists;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.foundation.ERXArrayUtilities;

/**
 * Version permettant de parcourir dans le sens père vers fils.
 *  
 * @author jlafourc, pascal
 */
public abstract class AbstractMaquetteProcessorImpl implements MaquetteProcessor {

	private ILien root;
	private EOEditingContext editingContext;
	
	private static final Logger LOG = Logger.getLogger(AbstractMaquetteProcessorImpl.class);
    private boolean shouldFetch;
	
	/**
	 * @param editingContext nécessaire pour fetcher les fils 
	 */
	public AbstractMaquetteProcessorImpl(EOEditingContext editingContext) {
		this.editingContext = editingContext;
		this.shouldFetch = true;
	}
	
	/**
	 * Retourne les liens fils de ce lien.
	 * @param lien un lien
	 * @return les liens fils
	 */
	protected List<ILien> getFils(ILien lien) {
		List<ILien> fils = Collections.emptyList();
		
		if (isShouldFetch()) {
		    fils = fetchLiensFils(lien);
		} else {
		    fils = Lists.newArrayList(lien.child().liensParents());
		}
		
		return fils;
	}

    private List<ILien> fetchLiensFils(ILien lien) {
        List<ILien> fils;
        EOQualifier qualifier = EOLien.PARENT.eq((EOComposant) lien.child());
        fils = new ArrayList<ILien>(
                ERXArrayUtilities.arrayWithoutDuplicates(
                        EOLien.fetchSco_Liens(editingContext, qualifier, null)
                        )
                );
        return fils;
    }

	protected EOEditingContext getEditingContext() {
		return editingContext;
	}

	protected void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	protected ILien getRoot() {
		return root;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setRoot(ILien root) {
		this.root = root;
	}
	
	protected boolean isShouldFetch() {
		return shouldFetch;
	}

	public void setShouldFetch(boolean shouldFetch) {
        this.shouldFetch = shouldFetch;
    }
}
