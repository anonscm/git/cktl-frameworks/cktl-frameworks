package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXBatchFetchUtilities;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Implémentation EOF du Repository.
 * @author Alexis Tual
 */
public class ComposantRepositoryEOF implements ComposantRepository {

	// Public Methods
	/**
	 * {@inheritDoc}
	 * <p>
	 * Cette implémentation récupère les ids des composants impliqués dans le graphe, à partir de l'id du composant racine (en fait le version diplome). <br/>
	 * Ensuite un second fetch récupère des EOComposant à partir de ces ids. <br/>
	 * Enfin un batchfetch de toutes les relations nécessaires est effectuée (environ 1 fetch par relation et par sous entité). Ce procédé permet de limiter le
	 * nombre de fetch et de le rendre indépendant du nombre de composants.
	 * </p>
	 */
	public IComposant initialiserGrapheComposant(IComposant rootComposant) {
		EOEditingContext editingContext = ((EOComposant) rootComposant).editingContext();
		NSArray<Integer> idComposants = fetchIdsComposantChildrenOfId(editingContext, rootComposant.id());
		NSArray<EOComposant> composants = fetchComposants(editingContext, idComposants, null);
		batchFetchRelationships(composants);
		return rootComposant;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeComposantsModelisation(ITypeComposant typeComposant, Integer annee) {
		return fetchListeComposants(typeComposant, annee, true);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeComposantsMaquettage(ITypeComposant typeComposant, Integer annee) {
		return fetchListeComposantsDerniereVersion(annee, typeComposant);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeComposantsPlusieursTypesMaquettage(Integer annee, ITypeComposant...typesComposant) {
		return fetchListeComposantsDerniereVersion(annee, typesComposant);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeComposants(ITypeComposant typeComposant, Integer annee) {
		return fetchListeComposants(typeComposant, annee);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeComposantsHorsDiplome(ITypeComposant typeComposant, Integer annee) {
		// TODO Auto-generated method stub
		return null;
	}

	// Private Methods
	/**
	 * renvoie la liste des composants visibles pour une année donnée soit en modélisation soit en maquettage, concaténée à la liste des composants qui ne sont
	 * reliés à aucun composant
	 * @param typeComposant : type de composant
	 * @param annee annee a laquelle appartien la version de diplome du composant
	 * @param visibleEnModelisation si true alors liste des composants visible en modelisation, sinon ceux visible en maquettage
	 * @return liste de composant
	 */
	private List<IComposant> fetchListeComposants(ITypeComposant typeComposant, Integer annee, Boolean visibleEnModelisation) {
		EOEditingContext editingContext = ((EOTypeComposant) typeComposant).editingContext();
		NSArray<Integer> idComposants = fetchIdsComposantOfType(editingContext, (EOTypeComposant) typeComposant, annee, visibleEnModelisation);
		// Si on est en maquettage il ne faut afficher que la liste des derniers composants
		// Filtrer la liste des composants en ne recuperant que les dernieres version d'un composant
		if (!visibleEnModelisation) {
			NSArray<Integer> idComposantsFiltres = new NSMutableArray<Integer>();
			for(EOComposant composant : fetchComposants(editingContext, idComposants, null)) {
				if (composant.isLastVersion(annee)) {
					idComposantsFiltres.add(composant.id());
				}
			}
			
			idComposants = idComposantsFiltres;
		}
		
		// Recuperer tous les composants libre qui ne sont donc pas rattaches a un diplome
		idComposants.addAll(fetchIdsComposantsHorsDiplome(editingContext, (EOTypeComposant) typeComposant, annee));
		
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		List<IComposant> composants = new ArrayList<IComposant>(fetchComposants(editingContext, idComposants, sortOrderings));

		return composants;
	}

	@SuppressWarnings("unchecked")
	private List<IComposant> fetchListeComposantsDerniereVersion(Integer annee, ITypeComposant...typesComposant) {
		NSArray<ITypeComposant> types = new NSArray<ITypeComposant>(typesComposant);
		NSArray<Integer> idsTypes = (NSArray<Integer>) types.valueForKey(EOTypeComposant.ID.key());
		EOEditingContext editingContext = ((EOTypeComposant) types.lastObject()).editingContext();
		String sql = "select distinct c.id_composant from "
					+ "sco_scolarite.composant c "
					+ "left join sco_scolarite.composant_info_version civ on c.info_version_id = civ.id_info_version "
					+ "left join sco_scolarite.v_lien l on c.id_composant = l.child_id "
					+ "left join sco_scolarite.composant c2 on l.id_version_diplome = c2.id_composant "
					+ "left join sco_scolarite.composant_info_version civ2 on c2.info_version_id = civ2.id_info_version "
					+ "left join sco_scolarite.composant c3 on l.id_diplome = c3.id_composant "
					+ "left join sco_scolarite.v_diplome_version_max vmax on l.id_diplome = vmax.id_diplome and vmax.annee = $annee "
					+ "where  "
						+ "c.type_composant_id in ($typesComposant) and "
						+ "civ.annee = $annee and "
						+ "(civ2.id_info_version is null or (civ2.annee = $annee and (civ2.version_majeure is null or (civ2.version_majeure = vmax.version_majeure)))) and "
						+ "(c3.id_composant is null or (c3.dip_statut_formation_id in ($statutProd, $statutFictif) or c3.dip_habilitation_requise=0))";
		sql = sql.replace("$annee", annee.toString())
		   .replace("$typesComposant", idsTypes.componentsJoinedByString(","))
		   .replace("$statutProd", EOStatutFormation.statutEnProduction(editingContext).id().toString())
		   .replace("$statutFictif", EOStatutFormation.statutFictive(editingContext).id().toString());

		NSArray<Integer> idComposants = fetchIdsComposantsWithSql(editingContext, sql);
		List<IComposant> composants = new ArrayList<IComposant>(fetchComposants(editingContext, idComposants, EOComposant.CODE.ascs()));
		return composants;
	}
	
	/**
	 * renvoie la liste des composants visibles pour une année donnée
	 * @param typeComposant : type de composant
	 * @param annee annee a laquelle appartient la version de diplome du composant
	 * @return liste de composant
	 */
	private List<IComposant> fetchListeComposants(ITypeComposant typeComposant, Integer annee) {
		EOEditingContext editingContext = ((EOTypeComposant) typeComposant).editingContext();
		NSArray<Integer> idComposants = fetchIdsComposantOfType(editingContext, (EOTypeComposant) typeComposant, annee, null);
		idComposants.addAll(fetchIdsComposantsHorsDiplome(editingContext, (EOTypeComposant) typeComposant, annee));

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		List<IComposant> composants = new ArrayList<IComposant>(fetchComposants(editingContext, idComposants, sortOrderings));

		return composants;
	}

	private NSArray<Integer> fetchIdsComposantsHorsDiplome(EOEditingContext editingContext, EOTypeComposant typeComposant, Integer annee) {
		String sql = "select distinct id_composant from sco_scolarite.composant comp, sco_scolarite.composant_info_version v where " + "comp.type_composant_id = "
		    + typeComposant.id() + " and comp.info_version_id = v.id_info_version and v.annee = " + annee + "\n" + "and comp.id_composant not in( "
		    + "select distinct l.child_id as ID_COMPOSANT from sco_scolarite.lien l, sco_scolarite.composant c " + "where c.type_composant_id = "
		    + typeComposant.id() + "\n" + "and c.id_composant = l.child_id " + "start with child_id in "
		    + "(select c1.id_composant from sco_scolarite.composant c1 where c1.type_composant_id="
		    + EOTypeComposant.typeVersionDiplome(editingContext).id() + ") " + "connect by prior child_id = parent_id)\n";

		return fetchIdsComposantsWithSql(editingContext, sql);
	}

	private void batchFetchRelationships(NSArray<EOComposant> composants) {
		ERXBatchFetchUtilities.batchFetch(composants,
		    new NSArray<String>(EOComposant.LIENS_CHILDS_KEY, EOComposant.LIENS_PARENTS_KEY, EOComposant.CREDITABLES_KEY, EOAP.CHARGE_ENSEIGNEMENTS_KEY,
		        EOComposant.RESPONSABLES_COMPOSANT_KEY, EOComposant.LIENS_CHILDS.dot(EOLien.CREDITABLES).key(), EOComposant.LIENS_PARENTS.dot(EOLien.CREDITABLES).key(),
		        EOComposant.LIENS_CHILDS.dot(EOLien.CHARGE_ENSEIGNEMENTS).key(), EOComposant.LIENS_PARENTS.dot(EOLien.CHARGE_ENSEIGNEMENTS).key()));
	}

	private NSArray<EOComposant> fetchComposants(EOEditingContext editingContext, NSArray<Integer> idComposants, NSArray<EOSortOrdering> sortOrderings) {
		return EOComposant.fetchSco_Composants(editingContext, EOComposant.ID.in(idComposants), sortOrderings);
	}

	@SuppressWarnings("unchecked")
	private NSArray<Integer> fetchIdsComposantsWithSql(EOEditingContext editingContext, String sql) {
		EOEntity entity = EOUtilities.entityForClass(editingContext, EOComposant.class);

		NSArray<?> rows = EOUtilities.rawRowsForSQL(editingContext, entity.model().name(), sql, null);
		NSArray<Integer> resultat = (NSArray<Integer>) rows.valueForKey("ID_COMPOSANT");
		return ERXArrayUtilities.arrayWithoutDuplicates(resultat);
	}

	private NSArray<Integer> fetchIdsComposantChildrenOfId(EOEditingContext editingContext, Integer id) {
		String sql = "select distinct l.child_id ID_COMPOSANT \n" + "from sco_scolarite.lien l \n" + "start with child_id = ? \n"
		    + "connect by prior child_id = parent_id";
		sql = sql.replace("?", id.toString());
		return fetchIdsComposantsWithSql(editingContext, sql);
	}

	private NSArray<Integer> fetchIdsComposantOfType(EOEditingContext editingContext, EOTypeComposant typeComposant, Integer annee, Boolean visibleEnModelisation) {
		String sql = "select distinct l.child_id ID_COMPOSANT from sco_scolarite.lien l, sco_scolarite.composant c \n" + "where c.id_composant = l.child_id \n"
		    + "and c.type_composant_id=%d \n" + "start with child_id in \n" + "(" + getSqlVersionDiplome(editingContext, annee, visibleEnModelisation) + ")"
		    + "connect by prior child_id = parent_id";
		sql = String.format(sql, typeComposant.id(), EOTypeComposant.typeVersionDiplome(editingContext).id(), annee);
		return fetchIdsComposantsWithSql(editingContext, sql);
	}

	private String getSqlVersionDiplome(EOEditingContext editingContext, Integer annee, Boolean visibleEnModelisation) {
		String sql = "select distinct c1.id_composant from sco_scolarite.composant c1, SCO_SCOLARITE.lien l1, sco_scolarite.composant c2, sco_scolarite.composant_info_version civ1 \n"
			    + "where c1.type_composant_id=" + EOTypeComposant.typeVersionDiplome(editingContext).id() + " and c1.info_version_id=civ1.id_info_version and civ1.annee= " + annee.toString() + "\n"
			    + "and l1.child_id = c1.id_composant and l1.parent_id= c2.id_composant \n";

		if (visibleEnModelisation == null) {
			return sql;
		} else {
			if (visibleEnModelisation) {
				sql += " and (c2.dip_habilitation_requise=1) \n";
			} else {
				sql += "and (c2.dip_statut_formation_id in (" + EOStatutFormation.statutEnProduction(editingContext).id() + ","
				    + EOStatutFormation.statutFictive(editingContext).id() + ")" + " or c2.dip_habilitation_requise=0) \n";
			}
		}
		return sql;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeDiplomesAVersionner(EOEditingContext editingContext, Integer anneeDebutHabilitation, Integer anneeFinHabilitation, Integer anneeDepart, Integer anneeCible) {
		// Recuperer les diplomes sur l'annee de depart
		List<IComposant> versionDiplomes = getListeComposants(EOTypeComposant.typeVersionDiplome(editingContext), anneeDepart);
		NSArray<EODiplome> diplomesRecuperes = getDiplomesFromVersions(versionDiplomes);
		
		// Filtrer les diplomes a versionner
		NSArray<EODiplome> diplomes = new NSMutableArray<EODiplome>();

		// Ajout des diplomes en production...
		NSArray<EODiplome> diplomesEnProduction = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesRecuperes,
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_EP_CODE));
		// ... et non habilitables  
		NSArray<EODiplome> diplomesEnProductionNonHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnProduction,
		        EODiplome.HABILITATION_REQUISE.ne(1));
		diplomes.addAll(diplomesEnProductionNonHabilitables);
		// ... et habilitables et pour lesquels l'annee est dans la periode d'habilitation
		NSArray<EODiplome> diplomesEnProductionHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnProduction,
		        EODiplome.HABILITATION_REQUISE.eq(1));
		for (EODiplome diplome : diplomesEnProductionHabilitables) {
			if (diplome.anneeDansPeriodeHabilitation(anneeCible, anneeDebutHabilitation, anneeFinHabilitation)) {
				diplomes.add(diplome);
			}
		}
		
		// Ajout des diplomes en cours de construction...
		NSArray<EODiplome> diplomesEnConstruction = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesRecuperes,
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_ECC_CODE));
		// ... et non habilitables  
		NSArray<EODiplome> diplomesEnConstructionNonHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnConstruction,
		        EODiplome.HABILITATION_REQUISE.ne(1));
		diplomes.addAll(diplomesEnConstructionNonHabilitables);
		// ... et habilitables et pour lesquels l'annee est dans la periode d'habilitation
		NSArray<EODiplome> diplomesEnConstructionHabilitables = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesEnConstruction,
		        EODiplome.HABILITATION_REQUISE.eq(1));
		for (EODiplome diplome : diplomesEnConstructionHabilitables) {
			if (diplome.anneeDansPeriodeHabilitation(anneeCible, anneeDebutHabilitation, anneeFinHabilitation)) {
				diplomes.add(diplome);
			}
		}

		List<IComposant> composants = new ArrayList<IComposant>(diplomes);
		return composants;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> getListeDiplomesAHabiliter(EOEditingContext editingContext, Integer anneeReference, Integer anneeCible) {
		// Recuperer les diplomes sur l'annee de depart
		List<IComposant> versionDiplomes = getListeComposants(EOTypeComposant.typeVersionDiplome(editingContext), anneeReference);
		NSArray<EODiplome> diplomesRecuperes = getDiplomesFromVersions(versionDiplomes);
		
		// Filtrer les diplomes a versionner
		NSArray<EODiplome> diplomes = new NSMutableArray<EODiplome>();
		
		// Retourner les diplomes en production et habilitable
		NSArray<EODiplome> diplomesProductionEtHabilitable = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(diplomesRecuperes,
		        EODiplome.STATUT_FORMATION.dot(EOStatutFormation.CODE).eq(EOStatutFormation.STATUT_EP_CODE).and(EODiplome.HABILITATION_REQUISE.eq(1)));
		diplomes.addAll(diplomesProductionEtHabilitable);
		
		List<IComposant> composants = new ArrayList<IComposant>(diplomes);
		return composants;
	}

	
	/**
	 * 
	 * @param editingContext the editing context
	 * @param typeComposant le type de composant fils que les ue et les ec doivent avoir
	 * @param annee  annee universitaire
	 * @return la liste des UE et Ec ayant des inscription péda pour l'année universitaire et étant parents du type de composant passé
	 */
	public List< IComposant> getListeComposantAvecEnfantsEtIP(EOEditingContext editingContext, ITypeComposant typeComposant, Integer annee) {
		
		List<EOComposant> listeComposants = null;
		EOQualifier qualifierLiens = new ERXQualifierInSubquery(EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE.dot(EOInscriptionPedagogique.TO_INSCRIPTION).dot(EOInscription.ANNEE).eq(annee), EOInscriptionPedagogiqueElement.ENTITY_NAME, EOLien.ID_KEY, EOInscriptionPedagogiqueElement.ID_LIEN_KEY);
		NSArray<Integer> idLiens = (NSArray<Integer>) ERXEOControlUtilities.primaryKeysMatchingQualifier(editingContext, EOLien.ENTITY_NAME, qualifierLiens, null).valueForKey(EOLien.ID_KEY);
		
		EOQualifier qualifier = EOComposant.TYPE_COMPOSANT.eq(EOTypeComposant.typeUE(editingContext)).or(EOComposant.TYPE_COMPOSANT.eq(EOTypeComposant.typeEC(editingContext)))
		.and(EOComposant.LIENS_CHILDS.dot(EOLien.ID).in(idLiens)
		.and(EOComposant.LIENS_PARENTS.dot(EOLien.CHILD).dot(EOComposant.TYPE_COMPOSANT).eq((EOTypeComposant) typeComposant)));
	
		ERXFetchSpecification<EOComposant> fetchSpec = EOComposant.fetchSpec();
		fetchSpec.setQualifier(qualifier);
		fetchSpec.setUsesDistinct(true);
		
		listeComposants = fetchSpec.fetchObjects(editingContext);
		
		return new ArrayList<IComposant>(listeComposants);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IDiplome> getListDiplomes(EOEditingContext editingContext, Integer annee) {
			List<IComposant> versionDiplomes = getListeComposants(EOTypeComposant.typeVersionDiplome(editingContext), annee);			
			
			List<IDiplome> composants = new ArrayList<IDiplome>(getDiplomesFromVersions(versionDiplomes));
			return composants;

  }

	private NSArray<EODiplome> getDiplomesFromVersions(List<IComposant> versionDiplomes) {
	  NSArray<EODiplome> diplomesRecuperes = new NSMutableArray<EODiplome>();
	  for (IComposant diplome : versionDiplomes) {
	  	diplomesRecuperes.add(((EOVersionDiplome) diplome).getDiplome());
	  }
	  diplomesRecuperes = ERXArrayUtilities.arrayWithoutDuplicates(diplomesRecuperes);
	  return diplomesRecuperes;
  }
	 

	/**
	 * renvoie la liste des composants visibles pour une année donnée 
	 * associés à une liste de structures
	 * @param typeComposant : type de composant
	 * @param annee : annee a laquelle appartient la version de diplome du composant
	 * @param structuresId : liste des structures concernées
	 * @return liste de composant
	 */
	public List<IComposant> getListeComposantsDansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId) {
		return fetchListeComposantsDansStructures(typeComposant, annee, structuresId);
	}

	/**
	 * renvoie la liste des composants visibles pour une année donnée 
	 * associés à une liste de structures ET 
	 * sans association à une structure
	 * @param typeComposant : type de composant
	 * @param annee : annee a laquelle appartient la version de diplome du composant
	 * @param structuresId : liste des structures concernées
	 * @return liste de composant
	 */
	public List<IComposant> getListeComposantsDansStructuresEtSansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId) {
		return fetchListeComposantsDansStructuresEtSansStructures(typeComposant, annee, structuresId);
	}

	/**
	 * renvoie la liste des ID des composants de type AP visibles pour une année donnée 
	 * associés à des UE/EC associés eux mêmes à une liste de structures ET 
	 * sans association à une structure.<br>
	 * Ces AP ont une charge d'enseignement en minutes > 0 et un nombre de groupes > 0
	 * Ces AP sont ensuite triés par ordre alpha du code du parent (UE/EC)
	 * @param annee : annee a laquelle appartient la version de diplome du composant
	 * @param structuresId : liste des structures concernées
	 * @return liste de Id de composant de type AP
	 */
	public NSArray<Integer> getListeIdAPsAvecChargesDansStructuresEtSansStructures(EOEditingContext editingContext, Integer annee, NSArray<Integer> structuresId) {
		return fetchIdsAPsAvecChargesDansStructuresEtSansStructures(editingContext, annee, null, structuresId);
	}

	private List<IComposant> fetchListeComposantsDansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId) {
		EOEditingContext editingContext = ((EOTypeComposant) typeComposant).editingContext();
		NSArray<Integer> idComposants = fetchIdsComposantOfTypeDansStructures(editingContext, (EOTypeComposant) typeComposant, annee, null, structuresId);

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		List<IComposant> composants = new ArrayList<IComposant>(fetchComposants(editingContext, idComposants, sortOrderings));
		
		return composants;
	}
	
	private NSArray<Integer> fetchIdsComposantOfTypeDansStructures(EOEditingContext editingContext, EOTypeComposant typeComposant, Integer annee, Boolean visibleEnModelisation, NSArray<Integer> structuresId) {
		StringBuilder sql = new StringBuilder("select distinct c.id_composant , s.c_structure from  sco_scolarite.lien l, sco_scolarite.composant c "); 
		sql.append(" left join  sco_scolarite.structures_composant s ON c.id_composant = s.composant_id  where c.id_composant = l.child_id ");
		sql.append(" and  s.c_structure in ( ");
		sql.append(structuresId.componentsJoinedByString(","));
		sql.append(" )");
		sql.append("and c.type_composant_id=%d and c.sys_archive is null start with child_id in ( ");
		sql.append(getSqlVersionDiplome(editingContext, annee, visibleEnModelisation) + ")" + "connect by prior child_id = parent_id");

		return fetchIdsComposantsWithSql(editingContext, String.format(sql.toString(), typeComposant.id(), EOTypeComposant.typeVersionDiplome(editingContext).id(), annee));
	}
	
	
	private List<IComposant> fetchListeComposantsDansStructuresEtSansStructures(ITypeComposant typeComposant, Integer annee, NSArray<Integer> structuresId) {
		EOEditingContext editingContext = ((EOTypeComposant) typeComposant).editingContext();
		NSArray<Integer> idComposants = fetchIdsComposantOfTypeDansStructuresEtSansStructures(editingContext, (EOTypeComposant) typeComposant, annee, null, structuresId);

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		List<IComposant> composants = new ArrayList<IComposant>(fetchComposants(editingContext, idComposants, sortOrderings));
		
		return composants;
	}
	
	private NSArray<Integer> fetchIdsComposantOfTypeDansStructuresEtSansStructures(EOEditingContext editingContext, EOTypeComposant typeComposant, Integer annee, Boolean visibleEnModelisation, NSArray<Integer> structuresId) {
		StringBuilder sql = new StringBuilder("select distinct c.id_composant , s.c_structure from  sco_scolarite.lien l, sco_scolarite.composant c "); 
		sql.append(" left join  sco_scolarite.structures_composant s ON c.id_composant = s.composant_id  where c.id_composant = l.child_id ");
		sql.append(" and (s.c_structure IS NULL or s.c_structure in ( ");
		sql.append(structuresId.componentsJoinedByString(","));
		sql.append(" ))");
		sql.append("and c.type_composant_id=%d and c.sys_archive is null start with child_id in ( ");
		sql.append(getSqlVersionDiplome(editingContext, annee, visibleEnModelisation) + ")" + "connect by prior child_id = parent_id");
		
		return fetchIdsComposantsWithSql(editingContext, String.format(sql.toString(), typeComposant.id(), EOTypeComposant.typeVersionDiplome(editingContext).id(), annee));
	}
	
	private NSArray<Integer> fetchIdsAPsAvecChargesDansStructuresEtSansStructures(EOEditingContext editingContext, Integer annee, Boolean visibleEnModelisation, NSArray<Integer> structuresId) {
		StringBuilder sql = new StringBuilder("select l0.child_id as ID_COMPOSANT from sco_scolarite.lien l0, sco_scolarite.composant c0 where l0.child_id in (");
		sql.append("select distinct c.id_composant from  sco_scolarite.lien l, sco_scolarite.composant c ");		
		sql.append(" inner join sco_scolarite.charge_enseignement ce on c.id_composant = ce.ap_id and ce.valeur_minutes > 0 and ce.nb_groupes is not null");
		sql.append(" left join  sco_scolarite.structures_composant s ON c.id_composant = s.composant_id  where c.id_composant = l.child_id ");  
		sql.append(" and c.type_composant_id=%d and c.sys_archive is null  ");
		sql.append(" and parent_id in ( ");
		sql.append(" select distinct c.id_composant from  sco_scolarite.lien l, sco_scolarite.composant c ");  
		sql.append(" left join  sco_scolarite.structures_composant s ON c.id_composant = s.composant_id  where c.id_composant = l.child_id ");  
		sql.append(" and (s.c_structure IS NULL or s.c_structure in ( ");
		sql.append(structuresId.componentsJoinedByString(","));
		sql.append(" ))");
		sql.append("and (c.type_composant_id=%d or c.type_composant_id=%d) and c.sys_archive is null start with child_id in ( ");
		sql.append(getSqlVersionDiplome(editingContext, annee, visibleEnModelisation) + ")" + "connect by prior child_id = parent_id)");
		sql.append(") and l0.parent_id=c0.id_composant order by c0.code asc");
		
		return fetchIdsComposantsWithSql(editingContext, 
				String.format(sql.toString(),
						EOTypeComposant.typeAP(editingContext).id(),
						EOTypeComposant.typeUE(editingContext).id(),
						EOTypeComposant.typeEC(editingContext).id()));
	}
			
}
