package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;

/**
 * 
 * Récupération des diplômes
 *
 */
public interface DiplomeRepository {

	/**
	 * @param annee l'année pour laquelle on veut récupérer les diplômes
	 * @param isEnModelisation true pour récupérer les diplomes visibles en modélisation
	 * @return la liste des diplôme correspondant à l'année
	 */
	List<IDiplome> getListeDiplomes(Integer annee, boolean isEnModelisation);
	
}
