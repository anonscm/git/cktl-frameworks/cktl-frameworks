package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * {@inheritDoc}
 */
public class MaquetteDataConsultationImpl implements MaquetteDataConsultation {
	private EOEditingContext editingContext;
	

	/**
	 * @param editingContext contexte d'edition
	 */
	public MaquetteDataConsultationImpl(EOEditingContext editingContext) {
		setEditingContext(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public ILienConsultation getLien(IVersionDiplome versionDiplome, IComposant composantRecherche) {
		// TODO attention aux élements mutualisés, on renvoie le premier de la liste
		
		List<EOLienConsultation> listeLiens = EOLienConsultation.fetchSco_LienConsultations(getEditingContext(),
			    EOLienConsultation.ID_VERSION_DIPLOME.eq(versionDiplome.id()).and(EOLienConsultation.CHILD_ID.eq(composantRecherche.id())), null);
		if (listeLiens != null && listeLiens.size() > 0) {
			return listeLiens.get(0);
		}
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ILienConsultation getLienDirect(IComposant composantPere, IComposant composantFils) {
		return EOLienConsultation.fetchSco_LienConsultation(getEditingContext(),
		    EOLienConsultation.PARENT_ID.eq(composantPere.id()).and(EOLienConsultation.CHILD_ID.eq(composantFils.id())));
  }

	/**
	 * {@inheritDoc}
	 */
	public List<ILienConsultation> getLiensConsultationsSous(ILienConsultation composantPere, ITypeComposant typeComposant) {
		return new ArrayList<ILienConsultation>(
		    EOLienConsultation.fetchSco_LienConsultations(
		        getEditingContext(),
		        EOLienConsultation.ID_LIEN_CONTEXTE.startsWith(composantPere.idLienContexte()).and(
		            EOLienConsultation.TYPE_COMPOSANT_NOM_CHILD.eq(typeComposant.nom())), null));

	}

	/**
	 * {@inheritDoc}
	 */
	public IComposant getComposant(ILienConsultation lienConsultation) {
		return EOComposant.fetchSco_Composant(getEditingContext(), EOComposant.ID.eq(lienConsultation.childId()));
	}
}
