package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

/**
 * contient les composants d'une maquette (d'un diplome) et les données s'y rapportant
 */
public class MaquetteData {
	ImmutableListMultimap<Integer, ILien> liensByParentId;
	ImmutableListMultimap<Integer, ILien> liensByChildId;
	ImmutableMap<Integer, ILien> liensById;
	ImmutableListMultimap<Integer, ILienConsultation> lienConsultationsByParentId;
	ImmutableListMultimap<Integer, ILienConsultation> lienConsultationsByChildId;
	ImmutableMap<String, ILienConsultation> lienConsultationsById;
	ImmutableMap<Integer, IComposant> composantsById;
	ImmutableListMultimap<Integer, IComposant> composantsByTypeComposantId;
	ImmutableMap<Integer, ICreditable> creditablesByComposantId;
	ImmutableMap<Integer, ICreditable> creditablesByLienId;
	ImmutableMap<Integer, IChargeEnseignement> chargeEnseignementByApId;
	ImmutableListMultimap<Integer, IResponsableComposant> responsablesByComposantId;
	
	ILien racine;
	Map<String, ITypeComposant> typeComposants;
	
	MaquetteData() {
	}
	
	/**
	 * @param id id du composant
	 * @return composant correspondant à l'ID
	 */
	public IComposant getComposant(Integer id) {
		return composantsById.get(id);
	}
	
	/**
	 * @param apId id de l'AP
	 * @return  charge d'enseignement positionnée sur un AP
	 */
	public IChargeEnseignement getChargeEnseignement(Integer apId) {
		return chargeEnseignementByApId.get(apId);
	}
	
	/**
	 * @param id id du composant
	 * @return creditable positionné sur un composant
	 */
	public ICreditable getCreditableByComposantId(Integer id) {
		return creditablesByComposantId.get(id);
	}
	
	public ICreditable getCreditableByLienId(Integer id) {
		return creditablesByLienId.get(id);
	}
	
	public List<ILien> getLiensByParentId(Integer id) {
		return liensByParentId.get(id);
	}

	public List<ILien> getLiensByChildId(Integer id) {
		return liensByChildId.get(id);
	}
	
	public ILien getLienById(Integer id) {
		return liensById.get(id);
	}
	
	public ILienConsultation getLienConsultationById(String id) {
		return lienConsultationsById.get(id);
	}
	
	public List<IDiplome> getDiplomes() {
		return getComposantsByTypeComposantClass(IDiplome.class);
	}

	public List<IVersionDiplome> getVersionDiplomes() {
		return getComposantsByTypeComposantClass(IVersionDiplome.class);
	}
	
	public List<IParcours> getParcours() {
		return getComposantsByTypeComposantClass(IParcours.class);
	}
	
	public List<IPeriode> getPeriodes() {
		return getComposantsByTypeComposantClass(IPeriode.class);
	}
	
	public List<IRegroupement> getRegroupements() {
		return getComposantsByTypeComposantClass(IRegroupement.class);
	}
	
	public List<IUE> getUEs() {
		return getComposantsByTypeComposantClass(IUE.class);
	}
	
	public List<IEC> getECs() {
		return getComposantsByTypeComposantClass(IEC.class);
	}
	
	public List<IAP> getAPs() {
		return getComposantsByTypeComposantClass(IAP.class);
	}
	
	public List<IAE> getAEs() {
		return getComposantsByTypeComposantClass(IAE.class);
	}
	
	private <T extends IComposant> List<T> getComposantsByTypeComposantClass(Class<T> clazz) {
		return new ArrayList(getComposantsByTypeComposant(typeComposants.get(clazz.getSimpleName())));
	}
	
	/**
	 * @param composantId id du composant
	 * @return  liste des responsables pour un composant donné
	 */
	public List<IResponsableComposant> getResponsablesByComposantId(Integer composantId) {
	  return responsablesByComposantId.get(composantId);
  }
	
	public List<IComposant> getComposantsByTypeComposant(ITypeComposant typeComposant) {
		return getComposantsByTypeComposantId(typeComposant.id());
	}
	
	public List<IComposant> getComposantsByTypeComposantId(Integer id) {
		return composantsByTypeComposantId.get(id);
	}
	
	public List<ILien> getLienEtEnfants(ILien lien) {
		List<ILien> liens = Lists.newArrayList();
		liens.add(lien);
		liens.addAll(getEnfants(lien));
		return liens;
	}
	
	public List<ILien> getEnfants(ILien lien) {
		List<ILien> liens = Lists.newArrayList();
		for (ILien l : getLiensByParentId(lien.childId())) {
			liens.add(l);
			liens.addAll(getEnfants(l));
		}
		return liens;
	}
	
	public List<ILienConsultation> getEnfantConsultations(final ILienConsultation lien,final boolean inclureLien) {
		return FluentIterable.from(lienConsultationsById.entrySet()).filter(new Predicate<Entry<String, ILienConsultation>>() {
			public boolean apply(Entry<String, ILienConsultation> input) {
				String prefix = lien.idLienContexte();
				if (!inclureLien) {
					prefix += ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR;
				}
				return StringUtils.startsWith(input.getKey(), prefix);
			}
		}).transform(new Function<Entry<String, ILienConsultation>, ILienConsultation>() {
			public ILienConsultation apply(Entry<String, ILienConsultation> input) {
				return input.getValue();
			}
		}).toList();
	}
	
	public List<ILien> getParents(ILien lien) {
		List<ILien> liens = Lists.newArrayList();
		for (ILien l : getLiensByChildId(lien.parentId())) {
			liens.add(l);
			liens.addAll(getParents(l));
		}
		return liens;
	}
	
	public Collection<IComposant> getComposantsParents(ILien lien) {
		List<ILien> liensParent = getLiensByChildId(lien.childId());
		List<IComposant> parents = new ArrayList<IComposant>();
		for (ILien lienParent : liensParent) {
			IComposant composant = getComposant(lienParent.parentId());
			if (composant != null) {
				parents.add(composant);
			}
		}
		return parents;
	}
	
	public Boolean childDeType(ILien lien, ITypeComposant type) {
		IComposant composant = getComposant(lien.childId());
		return getComposantsByTypeComposantId(type.id()).contains(composant);
	}

	public Collection<Integer> getTypeComposantIds(ITypeComposant... types) {
		return Collections2.transform(ImmutableList.copyOf(types), new Function<ITypeComposant, Integer>() {
			public Integer apply(ITypeComposant type) {
				return type.id();
			}
		});	
	}
	
	public List<ILien> getPremiersEnfantsDeTypes(ILien lien, Boolean obligatoire, ITypeComposant... types) {
		return getPremiersEnfantsDeTypes(lien, obligatoire, null, types);
	}
	
	public List<ILien> getPremiersEnfantsDeTypes(ILien lien, Boolean obligatoire, List<IComposant> exclus, ITypeComposant... types) {
		return getPremiersEnfantsDeTypes(lien, getTypeComposantIds(types), obligatoire, exclus);
	}
	
	
	private List<ILien> getPremiersEnfantsDeTypes(ILien lien, Collection<Integer> typeIds, Boolean obligatoire, List<IComposant> exclus) {
		List<ILien> liens = Lists.newArrayList();
		for (ILien l : getLiensByParentId(lien.childId())) {
			IComposant c = getComposant(l.childId());
			// Le composant est du bon type mais il faut vérifier si il est exclu
			if (typeIds.contains(c.typeComposantId()) && (exclus == null || !exclus.contains(c))) {
				if (!obligatoire || isLienObligatoire(l)) {
					liens.add(l);
				}
			} else {
				liens.addAll(getPremiersEnfantsDeTypes(l, typeIds, obligatoire, exclus));
			}
		}
		return liens;	
	}
	
	public List<ILien> getPremiersEnfantsDirectDeTypes(ILien lien, Boolean obligatoire, ITypeComposant... types) {
		return getPremiersEnfantsDirectDeTypes(lien, obligatoire, null, types);
	}
	
	public List<ILien> getPremiersEnfantsDirectDeTypes(ILien lien, Boolean obligatoire, List<IComposant> exclus, ITypeComposant... types) {
		return getPremiersEnfantsDirectDeTypes(lien, getTypeComposantIds(types), obligatoire, exclus);
	}
	
	
	private List<ILien> getPremiersEnfantsDirectDeTypes(ILien lien, Collection<Integer> typeIds, Boolean obligatoire, List<IComposant> exclus) {
		List<ILien> liens = Lists.newArrayList();
		for (ILien l : getLiensByParentId(lien.childId())) {
			IComposant c = getComposant(l.childId());
			// Le composant est du bon type mais il faut vérifier si il est exclu
			if (typeIds.contains(c.typeComposantId()) && (exclus == null || !exclus.contains(c))) {
				if (!obligatoire || isLienObligatoire(l)) {
					liens.add(l);
				}
			} 
		}
		return liens;	
	}
	
	
	public ILien getRacine() {
		return racine;
	}
	
	public Boolean isLienObligatoire(ILien l) {
		return l instanceof ILienComposer && ((ILienComposer) l).obligatoire() != null && ((ILienComposer) l).obligatoire();
	}
	
	public List<ILien> getLiensWhereChildrenIn(List<? extends IComposant> children) {
		List<ILien> liens = new ArrayList<ILien>();
		for (IComposant c : children) {
			liens.addAll(getLiensByChildId(c.id()));
		}
		return liens;
	}
	
	public List<ILien> getListeJusquaLaRacine(ILien feuille) {
		List<ILien> liens = Lists.newArrayList(feuille);
		for (ILien l : getLiensByChildId(feuille.parentId())) {
			liens.addAll(getListeJusquaLaRacine(l));
		}
		return liens;
	}
	
	public List<ILienConsultation> getEnfantDirectConsultationsDeTypeAEouAP(final ILienConsultation lien) {
		return FluentIterable.from(lienConsultationsById.entrySet()).filter(new Predicate<Entry<String, ILienConsultation>>() {
			public boolean apply(Entry<String, ILienConsultation> input) {
				String idLienContexte = input.getKey();
				ILienConsultation lienConsultation = input.getValue();
				// Lien fils de niveau plus 1
				return StringUtils.startsWith(idLienContexte, lien.idLienContexte()) 
						&& lienConsultation.niveau() == lien.niveau() + 1 
						&& (lienConsultation.isChildAE() || lienConsultation.isChildAP());
			}
		}).transform(new Function<Entry<String, ILienConsultation>, ILienConsultation>() {
			public ILienConsultation apply(Entry<String, ILienConsultation> input) {
				return input.getValue();
			}
		}).toList();
	}

	public ITypeComposant getTypeVersionDiplome() {
		return typeComposants.get(IVersionDiplome.class.getSimpleName());
	}
	
	public ITypeComposant getTypeAE() {
		return typeComposants.get(IAE.class.getSimpleName());
	}
	
	public ITypeComposant getTypeAP() {
		return typeComposants.get(IAP.class.getSimpleName());
	}
	
	public ITypeComposant getTypeUE() {
		return typeComposants.get(IUE.class.getSimpleName());
	}
	
	public ITypeComposant getTypeEC() {
		return typeComposants.get(IEC.class.getSimpleName());
	}
	
	public ITypeComposant getTypeParcours() {
		return typeComposants.get(IParcours.class.getSimpleName());
	}
	
	public ITypeComposant getTypeRegroupement() {
		return typeComposants.get(IRegroupement.class.getSimpleName());
	}
	
	public ITypeComposant getTypePeriode() {
		return typeComposants.get(IPeriode.class.getSimpleName());
	}


}
