package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;

/**
 * 
 * @author jlafourc
 *
 */
public interface MaquetteProcessor {
	/**
	 * lance le traitement sur l'arborescence de la maquette 
	 */
	void proceeed();
	
	/**
	 * @param root le lien à partir duquel on lance le traitement
	 */
	void setRoot(ILien root);
	
	/**
	 * Détermine si on doit élaguer <i>(to prune)</i> 
	 * ce qui se trouve en dessous du lien
	 * 
	 * @param lien Le lien à traiter
	 * @return Doit-on élaguer ou pas ?
	 */
	Boolean shouldProcessChildrenOf(ILien lien);
	
	/**
	 * Les action à réaliser sur le lien
	 * @param lien Le lien à traiter
	 */
	void performActionsOn(ILien lien);
	
}
