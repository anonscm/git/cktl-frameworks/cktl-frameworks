package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Version récursive avec des contraintes (facultatives) de parcours de composant parcoursDiplome, niveau et parcoursAnnee.
 * On peut arrête le parcours de l'arbre lorsqu'un composant cible est atteint.
 * 
 * Si l'élément racine (root) n'est précisé, il sera déduit par le couple diplôme/année si ceux si sont renseignés.
 * 
 * @author Pascal MACOUIN
 */
public abstract class AbstractMaquetteProcessorRecursiveAvecContraintesImpl extends AbstractMaquetteProcessorRecursiveImpl {

	private static final Logger LOG = Logger.getLogger(AbstractMaquetteProcessorRecursiveAvecContraintesImpl.class);
	
	private IDiplome diplome;
	private Integer annee;
	private IParcours parcoursDiplome;
	private Integer niveau;
	private IParcours parcoursAnnee;
	private IComposant composantCible;
	
	private boolean parcoursDiplomeEstPasse = false;
	private IPeriode niveauPasse = null;
	private boolean parcoursAnneeEstPasse = false;
	
	/**
	 * @param editingContext nécessaire pour fetcher les fils 
	 */
	public AbstractMaquetteProcessorRecursiveAvecContraintesImpl(EOEditingContext editingContext) {
		super(editingContext);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void proceeed() {
		// Si root est null, on le recherche avec diplome/annee
		if (getRoot() == null && getDiplome() != null && getAnnee() != null) {
	 		IVersionDiplome versionDiplome = getDiplome().getVersionDiplome(getAnnee());
			EOQualifier qualifier = 
					EOLien.CHILD_ID.eq(versionDiplome.id())
						.and(EOLien.TYPE_LIEN.eq(EOTypeLien.typeVersionner(getEditingContext())));
				
			ILien lienVersionner = EOLien.fetchSco_Lien(getEditingContext(), qualifier);
			setRoot(lienVersionner);
		}
		
		super.proceeed();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Boolean shouldProcessChildrenOf(ILien lien) {
		boolean shouldProcess = true;
		
		IComposant c = lien.child();
		
		if (c instanceof IParcours) {
			shouldProcess = shouldProcessChildrenOfParcours((IParcours) c);
		} else if (c instanceof IPeriode) {
			shouldProcess = shouldProcessChildrenOfPeriode((IPeriode) c);
		}
	
		if (shouldProcess) {
			calculerSiCibleAtteinte(lien);
			
			if (isTargetReach()) {
				shouldProcess = false;
			}
		}		
		
		return shouldProcess;
	}
	
	private boolean shouldProcessChildrenOfParcours(IParcours parcours) {
		if (!isNiveauEstPasse() && parcours.estIdentique(getParcoursDiplome())) {
			setParcoursDiplomeEstPasse(true);
			return true;
		} else if (isNiveauEstPasse()) {
			if (parcours.estIdentique(getParcoursAnnee())) {
				setParcoursAnneeEstPasse(true);
			}
			
			return true;
		}
	
		return false;
	}
	
	private boolean shouldProcessChildrenOfPeriode(IPeriode periode) {
		if (!periode.isAnnee()) {
			return true;
		} else if (periode.isAnnee() && getNiveau() != null && periode.ordre().equals(getNiveau())) {
			setNiveauPasse(periode);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Si on retraverse dans l'autre sens une contrainte déjà traversée, on l'a détope.
	 */
	protected void performPopActionsOn(ILien lien) {
		IComposant c = lien.child();
		
		if (c instanceof IParcours) {
			IParcours parcours = (IParcours) c;
			
			if (parcours.estIdentique(getParcoursDiplome())) {
				setParcoursDiplomeEstPasse(false);
			} else if (parcours.estIdentique(getParcoursAnnee())) {
				setParcoursAnneeEstPasse(false);
			}
		} else if (c instanceof IPeriode) {
			IPeriode periode = (IPeriode) c;
			
			if (periode.estIdentique(getNiveauPasse())) {
				setNiveauEstPasse(false);
			}
		}
	}
	
	private void calculerSiCibleAtteinte(ILien lien) {
		// Si un parcours diplôme est défini comme contraint, il faut qu'il est été traversé
		// Ainsi que les autres contraintes
		if ((getParcoursDiplome() == null || isParcoursDiplomeEstPasse())
				&& (getNiveau() == null || isNiveauEstPasse())
				&& (getParcoursAnnee() == null || isParcoursAnneeEstPasse())
				&& lien.child().estIdentique(getComposantCible())) {
			setTargetReach(true);
		}
	}
	
	public IDiplome getDiplome() {
		return diplome;
	}

	public void setDiplome(IDiplome diplome) {
		this.diplome = diplome;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public IParcours getParcoursAnnee() {
		return parcoursAnnee;
	}

	public void setParcoursAnnee(IParcours parcoursAnnee) {
		this.parcoursAnnee = parcoursAnnee;
	}

	public Integer getNiveau() {
		return niveau;
	}

	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}

	public IParcours getParcoursDiplome() {
		return parcoursDiplome;
	}

	public void setParcoursDiplome(IParcours parcoursDiplome) {
		this.parcoursDiplome = parcoursDiplome;
	}

	protected IComposant getComposantCible() {
		return composantCible;
	}

	protected void setComposantCible(IComposant composantCible) {
		this.composantCible = composantCible;
	}

	protected boolean isParcoursDiplomeEstPasse() {
		return parcoursDiplomeEstPasse;
	}

	protected void setParcoursDiplomeEstPasse(boolean parcoursDiplomeEstPasse) {
		this.parcoursDiplomeEstPasse = parcoursDiplomeEstPasse;
	}

	protected boolean isNiveauEstPasse() {
		return niveauPasse != null;
	}

	protected void setNiveauEstPasse(boolean niveauEstPasse) {
		if (niveauEstPasse) {
			throw new IllegalArgumentException("Le paramètre 'niveauEstPasse' ne peut être qu'à false. Utiliser la méthode 'setNiveauPasse(IPeriode niveauPasse)'.");
		}
		
		niveauPasse = null;
	}

	protected IPeriode getNiveauPasse() {
		return niveauPasse;
	}
	
	protected void setNiveauPasse(IPeriode niveauPasse) {
		if (this.niveauPasse == null) {
			this.niveauPasse = niveauPasse;
		}
	}

	protected boolean isParcoursAnneeEstPasse() {
		return parcoursAnneeEstPasse;
	}

	protected void setParcoursAnneeEstPasse(boolean parcoursAnneeEstPasse) {
		this.parcoursAnneeEstPasse = parcoursAnneeEstPasse;
	}
}
