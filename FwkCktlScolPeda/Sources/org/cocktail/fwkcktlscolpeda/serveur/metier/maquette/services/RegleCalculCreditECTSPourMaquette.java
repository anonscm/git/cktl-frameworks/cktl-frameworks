package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;

/**
 * Implemente les regles de calcul du credit ECTS pour la maquette de formation
 */
public class RegleCalculCreditECTSPourMaquette implements RegleCalculCreditECTS {

	/**
	 * {@inheritDoc}
	 */
	public boolean selectionnable(ILien lienAvecEnfant) {
		if (lienAvecEnfant instanceof ILienComposer) {
			IComposant child = lienAvecEnfant.child();
			if (child instanceof IAP || child instanceof IAE) {
				return false;
			}
			if (child instanceof IUE || child instanceof IEC || child instanceof IRegroupement || child instanceof IParcours) {
				return ((ILienComposer) lienAvecEnfant).obligatoire() != null && ((ILienComposer) lienAvecEnfant).obligatoire();
			}
		}
		return true;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public boolean sommeSurEnfantsNecessaire(ILien lien) {
		IComposant composant = lien.child();
		if (composant instanceof IUE) {
			for (ILien lienChild : composant.liensParentsWithoutDuplicate()) {
				if (lienChild instanceof ILienComposer) {
					if (((ILienComposer) lienChild).obligatoire() == null || !((ILienComposer) lienChild).obligatoire()) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean calculAvecMinimumTheorique() {
		return true;
	}
}
