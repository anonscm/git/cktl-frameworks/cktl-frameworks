package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;

/**
 * Fournit des éléments à partir d'élément d'une maquette de formation soit à partir des informations préchargées dans MaquetteData, soit directement à partir
 * de l'élément lui-même
 */
public class MaquetteElementProvider {
	private MaquetteData maquetteData;

	/**
	 * @param maquetteData structure contenant les éléments préchargés d'une maquette de formation
	 */
	public MaquetteElementProvider(MaquetteData maquetteData) {
		this.maquetteData = maquetteData;
	}

	/**
	 * @param lien lien pour lequel on cherche le composant enfant
	 * @return composant enfant positionné sur le lien
	 */
	public IComposant getChildAvecLien(ILien lien) {
		if (maquetteData == null) {
			return lien.child();
		} else {
			return maquetteData.getComposant(lien.childId());
		}
	}

	/**
	 * @param lien lien pour lequel on cherche le composant parent
	 * @return composant parent positionné sur le lien
	 */
	public IComposant getParentAvecLien(ILien lien) {
		if (maquetteData == null) {
			return lien.parent();
		} else {
			return maquetteData.getComposant(lien.parentId());
		}
	}

	/**
	 * @param composant composant
	 * @return creditable associé au composant si il existe
	 */
	public ICreditable getCreditablePourComposant(IComposant composant) {
		if (maquetteData == null) {
			return composant.creditable();
		} else {
			return maquetteData.getCreditableByComposantId(composant.id());
		}
	}

	/**
	 * @param lien lien
	 * @return creditable associé au lien si il existe, surcharge le creditable du composant enfant
	 */
	public ICreditable getCreditablePourLien(ILien lien) {
		if (maquetteData == null) {
			if (lien instanceof ILienComposer) {
				return ((ILienComposer) lien).creditable();
			} else {
				return null;
			}
		} else {
			return maquetteData.getCreditableByLienId(lien.id());
		}
	}

	/**
	 * @param composant composant
	 * @return liens pour lesquel le composant est un parent
	 */
	@SuppressWarnings("unchecked")
	public List<ILien> getLiensAvecComposantParent(IComposant composant) {
		if (maquetteData == null) {
			return (List<ILien>) composant.liensParentsWithoutDuplicate();
		} else {
			return maquetteData.getLiensByParentId(composant.id());
		}
	}
	
	public IComposant getChildDeLien(ILien lien) {
		if (maquetteData == null) {
			return lien.child();
		} else {
			return maquetteData.getComposant(lien.childId());
		}

	}

	/**
	 * @param ap composant de type atome pédagogique
	 * @return la charge d'enseignement associée au composant
	 */
	public IChargeEnseignement getChargeEnseignement(IAP ap) {
		if (maquetteData == null) {
			return ap.chargeEnseignementTheorique();
		} else {
			return maquetteData.getChargeEnseignement(ap.id());
		}
	}

	/**
	 * @param composant composant
	 * @return la liste des responsables pour ce composant
	 */
	@SuppressWarnings("unchecked")
  public List<IResponsableComposant> getResponsablesComposant(IComposant composant) {
		if (maquetteData == null) {
			return (List<IResponsableComposant>) composant.responsablesComposant();
		} else {
			return maquetteData.getResponsablesByComposantId(composant.id());
		}
	}
}
