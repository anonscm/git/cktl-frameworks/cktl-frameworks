package org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICreditable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;

/**
 * Implémentation simple du calcul de somme de creditECTS
 */
public class CalculateurSommeCreditECTSBase implements CalculateurSommeCreditECTS {
	
	private CreditableService creditableService;
	private RegleCalculCreditECTS regleCalculService;

	// List des composants qu'on a deja compté dans le calcul
	private List<Integer> composantCompteIds = new ArrayList<Integer>();
	
	private MaquetteElementProvider maquetteElementProvider;
	private static final Logger LOG = Logger.getLogger(CalculateurSommeCreditECTSBase.class);
		
	/**
	 * @param regleCalculService definit la regle de calcul qui doit etre utilisee pour le creditECTS
	 */
	public CalculateurSommeCreditECTSBase(RegleCalculCreditECTS regleCalculService) {
		this(regleCalculService, null);
	}
	
	/**
	 * @param regleCalculService definit la regle de calcul qui doit etre utilisee pour le creditECTS
	 * @param maquetteData données pré-chargées de la maquette de formation 
	 */
	public CalculateurSommeCreditECTSBase(RegleCalculCreditECTS regleCalculService, MaquetteData maquetteData) {
		creditableService = new CreditableServiceImpl();
		this.regleCalculService = regleCalculService;
		maquetteElementProvider = new MaquetteElementProvider(maquetteData);
	}

	
	/**
	 * {@inheritDoc}
	 */
	public void calcule(ILien lien) {
		composantCompteIds = new ArrayList<Integer>();
		faireLeCalcul(lien);
	}
	
	private void faireLeCalcul(ILien lien) {
		if (lien == null) {
			return;
		}

		lien.setCreditECTSCalcule(null);	
		
		IComposant child = maquetteElementProvider.getChildAvecLien(lien);
		for (ILien lienChild : maquetteElementProvider.getLiensAvecComposantParent(child)) {
			faireLeCalcul(lienChild);
		}

		BigDecimal somme = getSommeCreditECTSPourCalcul(lien);
		if (somme != null && somme.signum() == 1) {
			lien.setCreditECTSCalcule(somme);
		}
	}

	/**
	 * @param lien sur lequel on cherche le credit ECTS pour l'utiliser ensuite dans le calcul de la somme du credit ECTS du parent. Le composant pour lequel on
	 *          fait le calcul est bien le child() du lien.
	 * @return somme des credits ECTS pour un lien
	 */
	public BigDecimal getSommeCreditECTSPourCalcul(ILien lien) {
		if (regleCalculService.sommeSurEnfantsNecessaire(lien)) {
			BigDecimal sommeCreditECTSEnfants = getSommeCreditECTSEnfants(lien);
			LOG.debug("Calcul des ECTS enfants pour lien [" + lien + "] : " + sommeCreditECTSEnfants);
			return sommeCreditECTSEnfants;
		}
		if (lien instanceof ILienComposer) {
			if (regleCalculService.selectionnable(lien)) {
				IComposant child = maquetteElementProvider.getChildAvecLien(lien);
				ICreditable creditableChild = maquetteElementProvider.getCreditablePourComposant(child);
				ICreditable creditableLien = maquetteElementProvider.getCreditablePourLien(lien);
				BigDecimal creditsEcts = creditableService.creditsEcts(creditableChild, creditableLien);
				LOG.debug("Calcul des ECTS pour lien [" + lien + "] : " + creditsEcts);
				return creditsEcts;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	 BigDecimal getSommeCreditECTSEnfants(ILien lien) {
		IComposant composant = maquetteElementProvider.getChildAvecLien(lien);
		BigDecimal somme = BigDecimal.ZERO;
		Set<ILien> liensAvecEnfant = null;
		// HORREUR FIX ME !!!
		if (lien instanceof ILienConsultation) {
			ILienConsultation lienConsultation = (ILienConsultation) lien;
			liensAvecEnfant = new HashSet<ILien>(lienConsultation.getChildren());
		} else {
			liensAvecEnfant = new HashSet<ILien>(maquetteElementProvider.getLiensAvecComposantParent(composant));
		}
		
		List<BigDecimal> listeCreditsEctsSelectionnes = new ArrayList<BigDecimal>();
		List<BigDecimal> listeCreditsEctsNonSelectionnes = new ArrayList<BigDecimal>();
		
		for (ILien lienAvecEnfant : liensAvecEnfant) {
			BigDecimal creditECTS = getCreditECTS(lienAvecEnfant);
			if (!composantCompteIds.contains(lienAvecEnfant.childId()) && creditECTS != null) {
				if (regleCalculService.selectionnable(lienAvecEnfant)) {
					somme = somme.add(creditECTS);
					listeCreditsEctsSelectionnes.add(creditECTS);
				} else {
					listeCreditsEctsNonSelectionnes.add(creditECTS);
				}
				composantCompteIds.add(lienAvecEnfant.childId());
			}
		}
		
		if (regleCalculService.calculAvecMinimumTheorique()) {
			// On retourne ce minimum théorique s'il est supérieur au calculé
			if (lien instanceof ILienComposer && composant instanceof IRegroupement) {
				// On calcule le nombre d'ects minimum qu'aura ce regroupement
				BigDecimal ectsMinimum = getSommeCreditECTSMinimumRegroupement((ILienComposer) lien, listeCreditsEctsSelectionnes, somme, listeCreditsEctsNonSelectionnes);
				
				if (ectsMinimum.compareTo(somme) > 0) {
					somme = ectsMinimum;
				}
			}
		}
		
		return somme;
	}

	/**
	 * Calcul le nombre d'ECTS minimum qu'aura obligatoirement ce regroupement.
	 * @param lien le lien composé vers le regroupement (le regroupement est le fils)
	 * @param listeCreditsEctsSelectionnes la liste des crédits ECTS sélectionnés par le calcul (obligatoires)
	 * @param sommeCreditsEctsSelectionnes la somme de la liste listeCreditsEctsSelectionnes
	 * @param listeCreditsEctsNonSelectionnes la liste des crédits ECTS NON sélectionnés par le calcul (non obligatoires)
	 * @return le nombre minimum d'ECTS qu'aura ce regroupement
	 */
	private BigDecimal getSommeCreditECTSMinimumRegroupement(ILienComposer lien, List<BigDecimal> listeCreditsEctsSelectionnes, BigDecimal sommeCreditsEctsSelectionnes, List<BigDecimal> listeCreditsEctsNonSelectionnes) {
		IRegroupement regroupement = (IRegroupement) maquetteElementProvider.getChildAvecLien(lien);
		BigDecimal ectsMinimum = BigDecimal.ZERO;
		
		if (regroupement.choixECTS() != null) {
			// Un nombre d'ECTS est indiqué sur le regroupement, on prend cette valeur si la somme des enfants sélectionnés est zéro
			if (BigDecimal.ZERO.compareTo(sommeCreditsEctsSelectionnes) == 0) {
				ectsMinimum = new BigDecimal(regroupement.choixECTS());
			}
		} else if (regroupement.choixMin() != null) {
			// Un nombre d'éléments enfants minimum obligatoire est indiqué, on fait le total des plus petites valeurs
			int nombreCreditsEctsSelectionnes = listeCreditsEctsSelectionnes.size();
			
			if (regroupement.choixMin().compareTo(nombreCreditsEctsSelectionnes) > 0) {
				ectsMinimum = sommeCreditsEctsSelectionnes;
				// On trie la liste des crédits ECTS par ordre croissant
				// pour faire la somme des "choixMin()" manquant des premiers plus petits éléments non sélectionnés
				Collections.sort(listeCreditsEctsNonSelectionnes);
				
				// Dans la limite de la taille du tableau
				int nb = Math.min(regroupement.choixMin() - nombreCreditsEctsSelectionnes, listeCreditsEctsNonSelectionnes.size());
				for (int i = 0; i < nb; i++) {
					ectsMinimum = ectsMinimum.add(listeCreditsEctsNonSelectionnes.get(i));
				}
			}
		}
		
		return ectsMinimum;
	}
	
	private BigDecimal getCreditECTS(ILien lien) {
		if (lien.getCreditECTSCalcule() != null) {
			return lien.getCreditECTSCalcule();
		}

		if (lien instanceof ILienComposer) {
			IComposant child = maquetteElementProvider.getChildAvecLien(lien);
			ICreditable creditableChild = maquetteElementProvider.getCreditablePourComposant(child);
			ICreditable creditableLien = maquetteElementProvider.getCreditablePourLien(lien);
			return creditableService.creditsEcts(creditableChild, creditableLien);
		}

		return new BigDecimal(0);
	}

}
