package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * Définition d'un atome pédagogique (cours, TD, P, etc..)
 */
public interface IAP extends IComposant {

	/**
	 * @return Le mode de l'AP
	 */
	IModeAP mode();
	
	/**
	 * @return nombre maximum d'étudiant pour cet AP
	 */
	Integer seuil();

	/**
	 * @return type de l'atome pédagogique
	 */
	ITypeAP typeAP();

	/**
	 * @return Nombre d'heures de travail étudiant de l'AP
	 */
	BigDecimal valeurTempsEtudiant();
	
	/**
	 * Retourne la charge d'enseignement théorique (saisie Girofle) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
	IChargeEnseignement chargeEnseignementTheorique();

	/**
	 * Retourne la charge d'enseignement prévisionnelle (saisie surcharge Peche) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
	IChargeEnseignement chargeEnseignementPrevisionnelle();

	/**
	 * Retourne la charge d'enseignement pédagogique (saisie Paprika) pour cette AP.
	 * 
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
	IChargeEnseignement chargeEnseignementPedagogique();

	/**
	 * Retourne la charge d'enseignement théorique (saisie Girofle) pour cette AP.
	 * <p>
	 * Attention : s'il n'y a pas de charge d'enseignement pour l'AP, un objet {@link EOChargeEnseignement} est créé.
	 * 
	 * @return la charge d'enseignement (jamais <code>null</code>)
	 */
	IChargeEnseignement chargeEnseignementTheoriqueJamaisNull();
}
