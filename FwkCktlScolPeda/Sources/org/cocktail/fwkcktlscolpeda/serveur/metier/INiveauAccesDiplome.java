package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Niveau d'accès minimum à un diplome
 */
public interface INiveauAccesDiplome {
	Integer NIVEAU_ACCES_BAC_0 = 0;
	Integer NIVEAU_ACCES_BAC_1 = 1;
	Integer NIVEAU_ACCES_BAC_2 = 2;
	Integer NIVEAU_ACCES_BAC_3 = 3;
	Integer NIVEAU_ACCES_BAC_4 = 4;
	Integer NIVEAU_ACCES_BAC_5 = 5;
	Integer NIVEAU_ACCES_BAC_6 = 6;
	
	/**
	 * @return libelle du niveau d'accès
	 */
	String libelle();

	/**
	 * @param value libelle du niveau d'accès
	 */
	void setLibelle(String value);

	/**
	 * @return niveau d'accès au diplome
	 */
	Integer niveau();

	/**
	 * @param value niveau d'accès au diplome
	 */
	void setNiveau(Integer value);
}
