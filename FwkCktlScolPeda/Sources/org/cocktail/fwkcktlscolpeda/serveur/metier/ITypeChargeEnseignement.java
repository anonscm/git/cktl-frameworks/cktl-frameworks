package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 * Cette interface représente un type de charge d'enseignement (table TYPE_CHARGE_ENSEIGNEMENT).
 */
public interface ITypeChargeEnseignement {

	/** Charge d'enseignement théorique (saisie dans Girofle). */
	public static final String CODE_TYPE_THEORIQUE = "THEORIQUE";
	/** Charge d'enseignement prévisionnelle (saisie dans Peche). */
	public static final String CODE_TYPE_PREVISIONNELLE = "PREVISIONNELLE";
	/** Charge d'enseignement pédagogique (saisie dans Paprika). */
	public static final String CODE_TYPE_PEDAGOGIQUE = "PEDAGOGIQUE";
	/** Charge d'enseignement planifiée (saisie dans SuperPlan). */
	public static final String CODE_TYPE_PLANIFIEE = "PLANIFIEE";

}