package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Les différents types de paiements.
 * 
 * @author Pascal MACOUIN
 */
public enum TypePaiement {
	PAIEMENT_INITIAL,
	PAIEMENT,
	REMBOURSEMENT,
	DEMISSION
}
