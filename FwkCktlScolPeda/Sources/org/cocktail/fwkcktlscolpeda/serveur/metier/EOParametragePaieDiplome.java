package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Cette classe représente un article de paiement pour un diplôme.
 * 
 * @author Pascal MACOUIN
 */
public class EOParametragePaieDiplome extends _EOParametragePaieDiplome implements IParametragePaieDiplome {
	private static final long serialVersionUID = 4383458688293696711L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParametragePaieDiplome.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	/**
	 * @return le libellé complet de l'article de paiement (libellé diplome + libellé parcours diplôme + libellé parcours année)
	 */
	public String libelleComplet() {
		String libelleComplet = toDiplome().libelle();
		
		if (toParcours() != null) {
			libelleComplet += " - " + toParcours().libelle();
		}
		
		return libelleComplet;
	}

	public void setToDiplomeRelationship(IDiplome unDiplome) {
		super.setToDiplomeRelationship((EODiplome) unDiplome);
	}

	public void setToParcoursRelationship(IParcours unParcours) {
		super.setToParcoursRelationship((EOParcours) unParcours);
	}

	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
