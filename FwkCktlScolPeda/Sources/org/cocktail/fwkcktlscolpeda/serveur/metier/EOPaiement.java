package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Paiment pour un étudiant donné
 */
public class EOPaiement extends _EOPaiement implements IScoPaiement {
	
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPaiement.class);

	@Inject
	@Nullable
	private UserInfo userInfo;
	
	/** 
     * {@inheritDoc}
     */
    public Integer id() {
    	return primaryKey() == null ? null : Integer.valueOf(primaryKey());
    }

    public IScoEtudiantAnnee toScoEtudiantAnnee() {
    	return super.toEtudiantAnnee();
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantAnneeRelationship(IEtudiantAnnee unEtudiantAnnee) {
		super.setToEtudiantAnneeRelationship((EOEtudiantAnnee) unEtudiantAnnee);
	}
	
	/**
	 * {@inheritDoc}
	 */
    public List<? extends IScoPaiementDetail> toScoPaiementDetails() {
    	return super.toPaiementDetails();
    }
    
	/**
	 * {@inheritDoc}
	 */
	public List<EOPaiementDetail> toPaiementDetailsTries() {
		List<EOPaiementDetail> listeDetailPaiement = toPaiementDetailFormations();
		listeDetailPaiement.addAll(toPaiementDetailDiplomes());
		listeDetailPaiement.addAll(toPaiementDetailArticlesComplementaires());
		
		return listeDetailPaiement;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<EOPaiementDetail> toPaiementDetailFormations() {
		return toPaiementDetails(EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.isNotNull(),
				EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.dot(EOParametragePaieFormation.TO_STRUCTURE).dot(EOStructure.LC_STRUCTURE).asc()
				.then(EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.dot(EOParametragePaieFormation.TO_TYPE_FORMATION).dot(EOTypeFormation.LIBELLE).asc()),
				false);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EOPaiementDetail> toPaiementDetailDiplomes() {
		return toPaiementDetails(EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.isNotNull(),
				EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.dot(EOParametragePaieDiplome.TO_DIPLOME).dot(EODiplome.LIBELLE).asc()
				.then(EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.dot(EOParametragePaieDiplome.TO_PARCOURS).dot(EOParcours.LIBELLE).asc()),
				false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<EOPaiementDetail> toPaiementDetailArticlesComplementaires() {
		return toPaiementDetails(EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.isNotNull(),
				EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.dot(EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE).dot(EOTypeArticleComplementaire.LIBELLE).asc()
				.then(EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.dot(EOParametragePaieArticleComplementaire.LIBELLE).asc()),
				false);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailArticlesAuto() {
		return toPaiementDetails(EOPaiementDetail.AUTO.isTrue(), null, false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailArticlesManuel() {
		return toPaiementDetails(EOPaiementDetail.AUTO.isFalse(), null, false);
	}

	/** 
     * {@inheritDoc}
     */
    public void setToBordereauRelationship(IBordereau unBordereau) {
        super.setToBordereauRelationship((EOBordereau) unBordereau);
    }

    /**
     * {@inheritDoc}
     */
    public boolean aUnLienAvecGfc() {
    	if (toBordereau() != null) {
    		return true;
    	}
    	
    	for (IScoPaiementMoyen moyen : toPaiementMoyens()) {
			if (moyen.toEcheancierSepaSdd() != null) {
				return true;
			}
		}
    	
    	return false;
    }

	/**
	 * {@inheritDoc}
	 */
    public boolean isInitial() {
    	return ordre() == 1;
    }
    
	/**
	 * {@inheritDoc}
	 */
	public boolean isDernier() {
		return this.equals(toEtudiantAnnee().toPaiements(null, EOPaiement.ORDRE.ascs(), false).lastObject());
	}

	/**
	 * {@inheritDoc}
	 */
	public TypePaiement typePaiement() {
		if (isInitial()) {
			return TypePaiement.PAIEMENT_INITIAL;
		} else if (montant().compareTo(BigDecimal.ZERO) >= 0) {
			return TypePaiement.PAIEMENT;
		} else if (!toEtudiantAnnee().valide() && isDernier()) {
			return TypePaiement.DEMISSION;
		} else {
			return TypePaiement.REMBOURSEMENT;
		}
	}
	
	/**
	 * @return le mode de paiement, peut-être simple ou combinés (chèque + prel)
	 */
	public IModePaiement modePaiement() {
		IModePaiement modePaiement = null;
		if (hasPlusieursPaiementMoyens()) {
			modePaiement = modePaiementCombine();
		} else if (hasUnMoyenPaiement()) {
			modePaiement = modePaiementSimple();
		}
		return modePaiement;
	}
	
	private IModePaiement modePaiementSimple() {
		return toPaiementMoyens().lastObject().toModePaiement();
	}

	public IModePaiement modePaiementCombine() {
		IModePaiement modePaiement = null;
		NSArray<String> codesModePaiement = codesModePaiement();
		if (codesModePaiement.contains(IModePaiement.CODE_MODE_PRELEVEMENT)) {
			if (codesModePaiement.contains(IModePaiement.CODE_MODE_CHEQUE)) {
				modePaiement = ModePaiementChequePrelevement.instance();
			} else if (codesModePaiement.contains(IModePaiement.CODE_MODE_CB)) {
				modePaiement = ModePaiementCBPrelevement.instance();
			} else if (codesModePaiement.contains(IModePaiement.CODE_MODE_VIREMENT)) {
				modePaiement = ModePaiementVirementPrelevement.instance();
			}
		} 
		return modePaiement;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<? extends IScoPaiementMoyen> toScoPaiementMoyens() {
		return super.toPaiementMoyens();
	}
	
	private boolean hasUnMoyenPaiement() {
		return toPaiementMoyens().count() == 1;
	}
	
	public boolean hasPlusieursPaiementMoyens() {
		return toPaiementMoyens().count() > 1;
	}

	public boolean hasPaiementMoyenPrelevement() {
		return codesModePaiement().contains(IModePaiement.CODE_MODE_PRELEVEMENT);
	}
	
	@SuppressWarnings("unchecked")
	private NSArray<String> codesModePaiement() {
		NSArray<String> codesModePaiement = 
				(NSArray<String>) toPaiementMoyens().valueForKey(EOPaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).key());
		return codesModePaiement;
	}

	/** 
     * {@inheritDoc}
     */
	public BigDecimal montantTotalDu() {
		BigDecimal montantTotalDu = BigDecimal.ZERO;
		
		for (IPaiementDetail detail : toPaiementDetails()) {
			if (detail.montantAPayer() != null) {
				montantTotalDu = montantTotalDu.add(detail.montantAPayer());
			}
			if (detail.montantARembourser() != null) {
				montantTotalDu = montantTotalDu.subtract(detail.montantARembourser());
			}
		}

		return montantTotalDu;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementMoyen> toPaiementMoyensTries() {
		return EOPaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.ORDRE).desc().sorted(super.toPaiementMoyens());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IPaiementMoyen toPaiementMoyenPrelevement() {
		return toScoPaiementMoyenPrelevement();
	}

	/**
	 * {@inheritDoc}
	 */
	public IScoPaiementMoyen toScoPaiementMoyenPrelevement() {
		return toPaiementMoyens(EOPaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).eq(IModePaiement.CODE_MODE_PRELEVEMENT)).lastObject();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IPaiementMoyen toPaiementMoyenPremiereEcheance() {
		NSArray<EOPaiementMoyen> preMoyenPaiements = toPaiementMoyens(EOPaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).ne(IModePaiement.CODE_MODE_PRELEVEMENT));
		return preMoyenPaiements.lastObject();
	}
    
	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de paiement
	 */
	@Override
	public void setDatePaiement(NSTimestamp date) {
		if (date == null) {
			super.setDatePaiement(null);
		} else {
			super.setDatePaiement(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
