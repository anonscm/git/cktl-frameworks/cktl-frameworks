// DO NOT EDIT.  Make changes to EOTypePaiement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypePaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypePaiement";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypePaiement.class);

  public EOTypePaiement localInstanceIn(EOEditingContext editingContext) {
    EOTypePaiement localInstance = (EOTypePaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypePaiement.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypePaiement.LOG.isDebugEnabled()) {
    	_EOTypePaiement.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePaiement.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypePaiement.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypePaiement.LOG.isDebugEnabled()) {
    	_EOTypePaiement.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePaiement.LIBELLE_KEY);
  }


  public static EOTypePaiement createSco_TypePaiement(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypePaiement eo = (EOTypePaiement) EOUtilities.createAndInsertInstance(editingContext, _EOTypePaiement.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypePaiement> fetchSpec() {
    return new ERXFetchSpecification<EOTypePaiement>(_EOTypePaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypePaiement> fetchAllSco_TypePaiements(EOEditingContext editingContext) {
    return _EOTypePaiement.fetchAllSco_TypePaiements(editingContext, null);
  }

  public static NSArray<EOTypePaiement> fetchAllSco_TypePaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypePaiement.fetchSco_TypePaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypePaiement> fetchSco_TypePaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypePaiement> fetchSpec = new ERXFetchSpecification<EOTypePaiement>(_EOTypePaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypePaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypePaiement fetchSco_TypePaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePaiement.fetchSco_TypePaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePaiement fetchSco_TypePaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypePaiement> eoObjects = _EOTypePaiement.fetchSco_TypePaiements(editingContext, qualifier, null);
    EOTypePaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypePaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePaiement fetchRequiredSco_TypePaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePaiement.fetchRequiredSco_TypePaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePaiement fetchRequiredSco_TypePaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypePaiement eoObject = _EOTypePaiement.fetchSco_TypePaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypePaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePaiement localInstanceIn(EOEditingContext editingContext, EOTypePaiement eo) {
    EOTypePaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
