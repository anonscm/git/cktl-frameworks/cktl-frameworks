// DO NOT EDIT.  Make changes to EOSysNumeroEtudiantOffset.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSysNumeroEtudiantOffset extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SysNumeroEtudiantOffset";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_UNIVERSITAIRE = new ERXKey<Integer>("anneeUniversitaire");
  public static final ERXKey<Integer> OFFSET = new ERXKey<Integer>("offset");
  // Relationship Keys

  // Attributes
  public static final String ANNEE_UNIVERSITAIRE_KEY = ANNEE_UNIVERSITAIRE.key();
  public static final String OFFSET_KEY = OFFSET.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSysNumeroEtudiantOffset.class);

  public EOSysNumeroEtudiantOffset localInstanceIn(EOEditingContext editingContext) {
    EOSysNumeroEtudiantOffset localInstance = (EOSysNumeroEtudiantOffset)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneeUniversitaire() {
    return (Integer) storedValueForKey(_EOSysNumeroEtudiantOffset.ANNEE_UNIVERSITAIRE_KEY);
  }

  public void setAnneeUniversitaire(Integer value) {
    if (_EOSysNumeroEtudiantOffset.LOG.isDebugEnabled()) {
    	_EOSysNumeroEtudiantOffset.LOG.debug( "updating anneeUniversitaire from " + anneeUniversitaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSysNumeroEtudiantOffset.ANNEE_UNIVERSITAIRE_KEY);
  }

  public Integer offset() {
    return (Integer) storedValueForKey(_EOSysNumeroEtudiantOffset.OFFSET_KEY);
  }

  public void setOffset(Integer value) {
    if (_EOSysNumeroEtudiantOffset.LOG.isDebugEnabled()) {
    	_EOSysNumeroEtudiantOffset.LOG.debug( "updating offset from " + offset() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSysNumeroEtudiantOffset.OFFSET_KEY);
  }


  public static EOSysNumeroEtudiantOffset createSco_SysNumeroEtudiantOffset(EOEditingContext editingContext, Integer anneeUniversitaire
, Integer offset
) {
    EOSysNumeroEtudiantOffset eo = (EOSysNumeroEtudiantOffset) EOUtilities.createAndInsertInstance(editingContext, _EOSysNumeroEtudiantOffset.ENTITY_NAME);    
		eo.setAnneeUniversitaire(anneeUniversitaire);
		eo.setOffset(offset);
    return eo;
  }

  public static ERXFetchSpecification<EOSysNumeroEtudiantOffset> fetchSpec() {
    return new ERXFetchSpecification<EOSysNumeroEtudiantOffset>(_EOSysNumeroEtudiantOffset.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSysNumeroEtudiantOffset> fetchAllSco_SysNumeroEtudiantOffsets(EOEditingContext editingContext) {
    return _EOSysNumeroEtudiantOffset.fetchAllSco_SysNumeroEtudiantOffsets(editingContext, null);
  }

  public static NSArray<EOSysNumeroEtudiantOffset> fetchAllSco_SysNumeroEtudiantOffsets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSysNumeroEtudiantOffset.fetchSco_SysNumeroEtudiantOffsets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSysNumeroEtudiantOffset> fetchSco_SysNumeroEtudiantOffsets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSysNumeroEtudiantOffset> fetchSpec = new ERXFetchSpecification<EOSysNumeroEtudiantOffset>(_EOSysNumeroEtudiantOffset.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSysNumeroEtudiantOffset> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSysNumeroEtudiantOffset fetchSco_SysNumeroEtudiantOffset(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSysNumeroEtudiantOffset.fetchSco_SysNumeroEtudiantOffset(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSysNumeroEtudiantOffset fetchSco_SysNumeroEtudiantOffset(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSysNumeroEtudiantOffset> eoObjects = _EOSysNumeroEtudiantOffset.fetchSco_SysNumeroEtudiantOffsets(editingContext, qualifier, null);
    EOSysNumeroEtudiantOffset eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SysNumeroEtudiantOffset that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSysNumeroEtudiantOffset fetchRequiredSco_SysNumeroEtudiantOffset(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSysNumeroEtudiantOffset.fetchRequiredSco_SysNumeroEtudiantOffset(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSysNumeroEtudiantOffset fetchRequiredSco_SysNumeroEtudiantOffset(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSysNumeroEtudiantOffset eoObject = _EOSysNumeroEtudiantOffset.fetchSco_SysNumeroEtudiantOffset(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SysNumeroEtudiantOffset that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSysNumeroEtudiantOffset localInstanceIn(EOEditingContext editingContext, EOSysNumeroEtudiantOffset eo) {
    EOSysNumeroEtudiantOffset localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
