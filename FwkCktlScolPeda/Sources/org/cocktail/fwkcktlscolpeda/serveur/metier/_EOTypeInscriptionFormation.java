// DO NOT EDIT.  Make changes to EOTypeInscriptionFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeInscriptionFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeInscriptionFormation";

  // Attribute Keys
  public static final ERXKey<String> CODE_INSCRIPTION = new ERXKey<String>("codeInscription");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Boolean> IS_COMPLEMENTAIRE = new ERXKey<Boolean>("isComplementaire");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String CODE_INSCRIPTION_KEY = CODE_INSCRIPTION.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String IS_COMPLEMENTAIRE_KEY = IS_COMPLEMENTAIRE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeInscriptionFormation.class);

  public EOTypeInscriptionFormation localInstanceIn(EOEditingContext editingContext) {
    EOTypeInscriptionFormation localInstance = (EOTypeInscriptionFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeInscription() {
    return (String) storedValueForKey(_EOTypeInscriptionFormation.CODE_INSCRIPTION_KEY);
  }

  public void setCodeInscription(String value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating codeInscription from " + codeInscription() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.CODE_INSCRIPTION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeInscriptionFormation.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOTypeInscriptionFormation.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.D_MODIFICATION_KEY);
  }

  public Boolean isComplementaire() {
    return (Boolean) storedValueForKey(_EOTypeInscriptionFormation.IS_COMPLEMENTAIRE_KEY);
  }

  public void setIsComplementaire(Boolean value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating isComplementaire from " + isComplementaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.IS_COMPLEMENTAIRE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeInscriptionFormation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOTypeInscriptionFormation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOTypeInscriptionFormation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOTypeInscriptionFormation.LOG.isDebugEnabled()) {
    	_EOTypeInscriptionFormation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscriptionFormation.PERS_ID_MODIFICATION_KEY);
  }


  public static EOTypeInscriptionFormation createSco_TypeInscriptionFormation(EOEditingContext editingContext, String codeInscription
, NSTimestamp dCreation
, Boolean isComplementaire
, String libelle
, Integer persIdCreation
, Integer persIdModification
) {
    EOTypeInscriptionFormation eo = (EOTypeInscriptionFormation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeInscriptionFormation.ENTITY_NAME);    
		eo.setCodeInscription(codeInscription);
		eo.setDCreation(dCreation);
		eo.setIsComplementaire(isComplementaire);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeInscriptionFormation> fetchSpec() {
    return new ERXFetchSpecification<EOTypeInscriptionFormation>(_EOTypeInscriptionFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeInscriptionFormation> fetchAllSco_TypeInscriptionFormations(EOEditingContext editingContext) {
    return _EOTypeInscriptionFormation.fetchAllSco_TypeInscriptionFormations(editingContext, null);
  }

  public static NSArray<EOTypeInscriptionFormation> fetchAllSco_TypeInscriptionFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeInscriptionFormation.fetchSco_TypeInscriptionFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeInscriptionFormation> fetchSco_TypeInscriptionFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeInscriptionFormation> fetchSpec = new ERXFetchSpecification<EOTypeInscriptionFormation>(_EOTypeInscriptionFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeInscriptionFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeInscriptionFormation fetchSco_TypeInscriptionFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscriptionFormation.fetchSco_TypeInscriptionFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscriptionFormation fetchSco_TypeInscriptionFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeInscriptionFormation> eoObjects = _EOTypeInscriptionFormation.fetchSco_TypeInscriptionFormations(editingContext, qualifier, null);
    EOTypeInscriptionFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeInscriptionFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscriptionFormation fetchRequiredSco_TypeInscriptionFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscriptionFormation.fetchRequiredSco_TypeInscriptionFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscriptionFormation fetchRequiredSco_TypeInscriptionFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeInscriptionFormation eoObject = _EOTypeInscriptionFormation.fetchSco_TypeInscriptionFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeInscriptionFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscriptionFormation localInstanceIn(EOEditingContext editingContext, EOTypeInscriptionFormation eo) {
    EOTypeInscriptionFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
