// DO NOT EDIT.  Make changes to EOBordereau.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOBordereau extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Bordereau";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_UNIVERSITAIRE = new ERXKey<Integer>("anneeUniversitaire");
  public static final ERXKey<String> CODE_GESTION = new ERXKey<String>("codeGestion");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_ENVOI_COMPTA = new ERXKey<NSTimestamp>("dateEnvoiCompta");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<NSTimestamp> DATE_PAIEMENT = new ERXKey<NSTimestamp>("datePaiement");
  public static final ERXKey<Integer> NUMERO_BORDEREAU = new ERXKey<Integer>("numeroBordereau");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> TO_PAIEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>("toPaiements");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau> TO_TYPE_BORDEREAU = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau>("toTypeBordereau");

  // Attributes
  public static final String ANNEE_UNIVERSITAIRE_KEY = ANNEE_UNIVERSITAIRE.key();
  public static final String CODE_GESTION_KEY = CODE_GESTION.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_ENVOI_COMPTA_KEY = DATE_ENVOI_COMPTA.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DATE_PAIEMENT_KEY = DATE_PAIEMENT.key();
  public static final String NUMERO_BORDEREAU_KEY = NUMERO_BORDEREAU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_PAIEMENTS_KEY = TO_PAIEMENTS.key();
  public static final String TO_TYPE_BORDEREAU_KEY = TO_TYPE_BORDEREAU.key();

  private static Logger LOG = Logger.getLogger(_EOBordereau.class);

  public EOBordereau localInstanceIn(EOEditingContext editingContext) {
    EOBordereau localInstance = (EOBordereau)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneeUniversitaire() {
    return (Integer) storedValueForKey(_EOBordereau.ANNEE_UNIVERSITAIRE_KEY);
  }

  public void setAnneeUniversitaire(Integer value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating anneeUniversitaire from " + anneeUniversitaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.ANNEE_UNIVERSITAIRE_KEY);
  }

  public String codeGestion() {
    return (String) storedValueForKey(_EOBordereau.CODE_GESTION_KEY);
  }

  public void setCodeGestion(String value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating codeGestion from " + codeGestion() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.CODE_GESTION_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOBordereau.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.DATE_CREATION_KEY);
  }

  public NSTimestamp dateEnvoiCompta() {
    return (NSTimestamp) storedValueForKey(_EOBordereau.DATE_ENVOI_COMPTA_KEY);
  }

  public void setDateEnvoiCompta(NSTimestamp value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating dateEnvoiCompta from " + dateEnvoiCompta() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.DATE_ENVOI_COMPTA_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOBordereau.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.DATE_MODIFICATION_KEY);
  }

  public NSTimestamp datePaiement() {
    return (NSTimestamp) storedValueForKey(_EOBordereau.DATE_PAIEMENT_KEY);
  }

  public void setDatePaiement(NSTimestamp value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating datePaiement from " + datePaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.DATE_PAIEMENT_KEY);
  }

  public Integer numeroBordereau() {
    return (Integer) storedValueForKey(_EOBordereau.NUMERO_BORDEREAU_KEY);
  }

  public void setNumeroBordereau(Integer value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating numeroBordereau from " + numeroBordereau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.NUMERO_BORDEREAU_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOBordereau.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOBordereau.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
    	_EOBordereau.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBordereau.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau toTypeBordereau() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau)storedValueForKey(_EOBordereau.TO_TYPE_BORDEREAU_KEY);
  }
  
  public void setToTypeBordereau(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau value) {
    takeStoredValueForKey(value, _EOBordereau.TO_TYPE_BORDEREAU_KEY);
  }

  public void setToTypeBordereauRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau value) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
      _EOBordereau.LOG.debug("updating toTypeBordereau from " + toTypeBordereau() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeBordereau(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau oldValue = toTypeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOBordereau.TO_TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOBordereau.TO_TYPE_BORDEREAU_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)storedValueForKey(_EOBordereau.TO_PAIEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier) {
    return toPaiements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier, boolean fetch) {
    return toPaiements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.fetchSco_Paiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPaiements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPaiements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOBordereau.TO_PAIEMENTS_KEY);
  }

  public void removeFromToPaiements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOBordereau.TO_PAIEMENTS_KEY);
  }

  public void addToToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
      _EOBordereau.LOG.debug("adding " + object + " to toPaiements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPaiements(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOBordereau.TO_PAIEMENTS_KEY);
    }
  }

  public void removeFromToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    if (_EOBordereau.LOG.isDebugEnabled()) {
      _EOBordereau.LOG.debug("removing " + object + " from toPaiements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPaiements(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOBordereau.TO_PAIEMENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement createToPaiementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOBordereau.TO_PAIEMENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement) eo;
  }

  public void deleteToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOBordereau.TO_PAIEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPaiementsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> objects = toPaiements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPaiementsRelationship(objects.nextElement());
    }
  }


  public static EOBordereau createSco_Bordereau(EOEditingContext editingContext, Integer anneeUniversitaire
, String codeGestion
, NSTimestamp dateCreation
, NSTimestamp dateEnvoiCompta
, NSTimestamp datePaiement
, Integer numeroBordereau
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau toTypeBordereau) {
    EOBordereau eo = (EOBordereau) EOUtilities.createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);    
		eo.setAnneeUniversitaire(anneeUniversitaire);
		eo.setCodeGestion(codeGestion);
		eo.setDateCreation(dateCreation);
		eo.setDateEnvoiCompta(dateEnvoiCompta);
		eo.setDatePaiement(datePaiement);
		eo.setNumeroBordereau(numeroBordereau);
		eo.setPersIdCreation(persIdCreation);
    eo.setToTypeBordereauRelationship(toTypeBordereau);
    return eo;
  }

  public static ERXFetchSpecification<EOBordereau> fetchSpec() {
    return new ERXFetchSpecification<EOBordereau>(_EOBordereau.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOBordereau> fetchAllSco_Bordereaus(EOEditingContext editingContext) {
    return _EOBordereau.fetchAllSco_Bordereaus(editingContext, null);
  }

  public static NSArray<EOBordereau> fetchAllSco_Bordereaus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBordereau.fetchSco_Bordereaus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBordereau> fetchSco_Bordereaus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOBordereau> fetchSpec = new ERXFetchSpecification<EOBordereau>(_EOBordereau.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBordereau> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOBordereau fetchSco_Bordereau(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBordereau.fetchSco_Bordereau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBordereau fetchSco_Bordereau(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBordereau> eoObjects = _EOBordereau.fetchSco_Bordereaus(editingContext, qualifier, null);
    EOBordereau eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Bordereau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBordereau fetchRequiredSco_Bordereau(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBordereau.fetchRequiredSco_Bordereau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBordereau fetchRequiredSco_Bordereau(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBordereau eoObject = _EOBordereau.fetchSco_Bordereau(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Bordereau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBordereau localInstanceIn(EOEditingContext editingContext, EOBordereau eo) {
    EOBordereau localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
