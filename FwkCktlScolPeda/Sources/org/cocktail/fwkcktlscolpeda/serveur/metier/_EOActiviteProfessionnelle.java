// DO NOT EDIT.  Make changes to EOActiviteProfessionnelle.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOActiviteProfessionnelle extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ActiviteProfessionnelle";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> DMODIFICATION = new ERXKey<NSTimestamp>("dmodification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> VALIDITE = new ERXKey<String>("validite");
  // Relationship Keys

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String DMODIFICATION_KEY = DMODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String VALIDITE_KEY = VALIDITE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOActiviteProfessionnelle.class);

  public EOActiviteProfessionnelle localInstanceIn(EOEditingContext editingContext) {
    EOActiviteProfessionnelle localInstance = (EOActiviteProfessionnelle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOActiviteProfessionnelle.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.D_CREATION_KEY);
  }

  public NSTimestamp dmodification() {
    return (NSTimestamp) storedValueForKey(_EOActiviteProfessionnelle.DMODIFICATION_KEY);
  }

  public void setDmodification(NSTimestamp value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating dmodification from " + dmodification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.DMODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOActiviteProfessionnelle.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOActiviteProfessionnelle.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOActiviteProfessionnelle.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.PERS_ID_MODIFICATION_KEY);
  }

  public String validite() {
    return (String) storedValueForKey(_EOActiviteProfessionnelle.VALIDITE_KEY);
  }

  public void setValidite(String value) {
    if (_EOActiviteProfessionnelle.LOG.isDebugEnabled()) {
    	_EOActiviteProfessionnelle.LOG.debug( "updating validite from " + validite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActiviteProfessionnelle.VALIDITE_KEY);
  }


  public static EOActiviteProfessionnelle createSco_ActiviteProfessionnelle(EOEditingContext editingContext, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
, String validite
) {
    EOActiviteProfessionnelle eo = (EOActiviteProfessionnelle) EOUtilities.createAndInsertInstance(editingContext, _EOActiviteProfessionnelle.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setValidite(validite);
    return eo;
  }

  public static ERXFetchSpecification<EOActiviteProfessionnelle> fetchSpec() {
    return new ERXFetchSpecification<EOActiviteProfessionnelle>(_EOActiviteProfessionnelle.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOActiviteProfessionnelle> fetchAllSco_ActiviteProfessionnelles(EOEditingContext editingContext) {
    return _EOActiviteProfessionnelle.fetchAllSco_ActiviteProfessionnelles(editingContext, null);
  }

  public static NSArray<EOActiviteProfessionnelle> fetchAllSco_ActiviteProfessionnelles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOActiviteProfessionnelle.fetchSco_ActiviteProfessionnelles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOActiviteProfessionnelle> fetchSco_ActiviteProfessionnelles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOActiviteProfessionnelle> fetchSpec = new ERXFetchSpecification<EOActiviteProfessionnelle>(_EOActiviteProfessionnelle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOActiviteProfessionnelle> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOActiviteProfessionnelle fetchSco_ActiviteProfessionnelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOActiviteProfessionnelle.fetchSco_ActiviteProfessionnelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOActiviteProfessionnelle fetchSco_ActiviteProfessionnelle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOActiviteProfessionnelle> eoObjects = _EOActiviteProfessionnelle.fetchSco_ActiviteProfessionnelles(editingContext, qualifier, null);
    EOActiviteProfessionnelle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ActiviteProfessionnelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOActiviteProfessionnelle fetchRequiredSco_ActiviteProfessionnelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOActiviteProfessionnelle.fetchRequiredSco_ActiviteProfessionnelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOActiviteProfessionnelle fetchRequiredSco_ActiviteProfessionnelle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOActiviteProfessionnelle eoObject = _EOActiviteProfessionnelle.fetchSco_ActiviteProfessionnelle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ActiviteProfessionnelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOActiviteProfessionnelle localInstanceIn(EOEditingContext editingContext, EOActiviteProfessionnelle eo) {
    EOActiviteProfessionnelle localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
