// DO NOT EDIT.  Make changes to EOCotisation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOCotisation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Cotisation";

  // Attribute Keys
  public static final ERXKey<String> COTI_CODE = new ERXKey<String>("cotiCode");
  public static final ERXKey<Integer> COTI_CODE_NUMBER = new ERXKey<Integer>("cotiCodeNumber");
  public static final ERXKey<Boolean> COTISATION = new ERXKey<Boolean>("cotisation");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<Integer> DEBUT_VALIDITE = new ERXKey<Integer>("debutValidite");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> FIN_VALIDITE = new ERXKey<Integer>("finValidite");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LL_COTISATION = new ERXKey<String>("llCotisation");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> VALIDITE = new ERXKey<String>("validite");
  // Relationship Keys

  // Attributes
  public static final String COTI_CODE_KEY = COTI_CODE.key();
  public static final String COTI_CODE_NUMBER_KEY = COTI_CODE_NUMBER.key();
  public static final String COTISATION_KEY = COTISATION.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String DEBUT_VALIDITE_KEY = DEBUT_VALIDITE.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String FIN_VALIDITE_KEY = FIN_VALIDITE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LL_COTISATION_KEY = LL_COTISATION.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String VALIDITE_KEY = VALIDITE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOCotisation.class);

  public EOCotisation localInstanceIn(EOEditingContext editingContext) {
    EOCotisation localInstance = (EOCotisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cotiCode() {
    return (String) storedValueForKey(_EOCotisation.COTI_CODE_KEY);
  }

  public void setCotiCode(String value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating cotiCode from " + cotiCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.COTI_CODE_KEY);
  }

  public Integer cotiCodeNumber() {
    return (Integer) storedValueForKey(_EOCotisation.COTI_CODE_NUMBER_KEY);
  }

  public void setCotiCodeNumber(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating cotiCodeNumber from " + cotiCodeNumber() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.COTI_CODE_NUMBER_KEY);
  }

  public Boolean cotisation() {
    return (Boolean) storedValueForKey(_EOCotisation.COTISATION_KEY);
  }

  public void setCotisation(Boolean value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating cotisation from " + cotisation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.COTISATION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOCotisation.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.D_CREATION_KEY);
  }

  public Integer debutValidite() {
    return (Integer) storedValueForKey(_EOCotisation.DEBUT_VALIDITE_KEY);
  }

  public void setDebutValidite(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating debutValidite from " + debutValidite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.DEBUT_VALIDITE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOCotisation.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.D_MODIFICATION_KEY);
  }

  public Integer finValidite() {
    return (Integer) storedValueForKey(_EOCotisation.FIN_VALIDITE_KEY);
  }

  public void setFinValidite(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating finValidite from " + finValidite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.FIN_VALIDITE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOCotisation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.LIBELLE_KEY);
  }

  public String llCotisation() {
    return (String) storedValueForKey(_EOCotisation.LL_COTISATION_KEY);
  }

  public void setLlCotisation(String value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating llCotisation from " + llCotisation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.LL_COTISATION_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOCotisation.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.ORDRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOCotisation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOCotisation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.PERS_ID_MODIFICATION_KEY);
  }

  public String validite() {
    return (String) storedValueForKey(_EOCotisation.VALIDITE_KEY);
  }

  public void setValidite(String value) {
    if (_EOCotisation.LOG.isDebugEnabled()) {
    	_EOCotisation.LOG.debug( "updating validite from " + validite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCotisation.VALIDITE_KEY);
  }


  public static EOCotisation createSco_Cotisation(EOEditingContext editingContext, String cotiCode
, Integer cotiCodeNumber
, Boolean cotisation
, NSTimestamp dCreation
, Integer debutValidite
, String libelle
, Integer ordre
, Integer persIdCreation
, String validite
) {
    EOCotisation eo = (EOCotisation) EOUtilities.createAndInsertInstance(editingContext, _EOCotisation.ENTITY_NAME);    
		eo.setCotiCode(cotiCode);
		eo.setCotiCodeNumber(cotiCodeNumber);
		eo.setCotisation(cotisation);
		eo.setDCreation(dCreation);
		eo.setDebutValidite(debutValidite);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
		eo.setPersIdCreation(persIdCreation);
		eo.setValidite(validite);
    return eo;
  }

  public static ERXFetchSpecification<EOCotisation> fetchSpec() {
    return new ERXFetchSpecification<EOCotisation>(_EOCotisation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOCotisation> fetchAllSco_Cotisations(EOEditingContext editingContext) {
    return _EOCotisation.fetchAllSco_Cotisations(editingContext, null);
  }

  public static NSArray<EOCotisation> fetchAllSco_Cotisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCotisation.fetchSco_Cotisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCotisation> fetchSco_Cotisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOCotisation> fetchSpec = new ERXFetchSpecification<EOCotisation>(_EOCotisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCotisation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOCotisation fetchSco_Cotisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCotisation.fetchSco_Cotisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCotisation fetchSco_Cotisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCotisation> eoObjects = _EOCotisation.fetchSco_Cotisations(editingContext, qualifier, null);
    EOCotisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Cotisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCotisation fetchRequiredSco_Cotisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCotisation.fetchRequiredSco_Cotisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCotisation fetchRequiredSco_Cotisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCotisation eoObject = _EOCotisation.fetchSco_Cotisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Cotisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCotisation localInstanceIn(EOEditingContext editingContext, EOCotisation eo) {
    EOCotisation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
