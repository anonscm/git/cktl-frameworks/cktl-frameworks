package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un mode d'AP.
 * 
 * @author Pascal MACOUIN
 */
public interface IModeAP {

	/**
	 * @return Le code du mode
	 */
	String code();

	/**
	 * @param unCode Un code pour ce mode
	 */
	void setCode(String unCode);

	/**
	 * @return Le libellé
	 */
	String libelle();

	/**
	 * @param unLibelle Un libellé
	 */
	void setLibelle(String unLibelle);

	/**
	 * @return <code>"oui"</code> si ce mode est en lecture seule
	 */
	Integer readOnly();

	/**
	 * @param enLectureSeule En lecture seule ?
	 */
	void setReadOnly(Integer enLectureSeule);

}