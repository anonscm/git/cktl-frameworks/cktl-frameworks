// DO NOT EDIT.  Make changes to EOGroupeEtudiant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOGroupeEtudiant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_GroupeEtudiant";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee> TO_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee>("toEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe> TO_GROUPE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe>("toGroupe");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_ETUDIANT_ANNEE_KEY = TO_ETUDIANT_ANNEE.key();
  public static final String TO_GROUPE_KEY = TO_GROUPE.key();

  private static Logger LOG = Logger.getLogger(_EOGroupeEtudiant.class);

  public EOGroupeEtudiant localInstanceIn(EOEditingContext editingContext) {
    EOGroupeEtudiant localInstance = (EOGroupeEtudiant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOGroupeEtudiant.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
    	_EOGroupeEtudiant.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupeEtudiant.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOGroupeEtudiant.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
    	_EOGroupeEtudiant.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupeEtudiant.DATE_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOGroupeEtudiant.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
    	_EOGroupeEtudiant.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupeEtudiant.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOGroupeEtudiant.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
    	_EOGroupeEtudiant.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupeEtudiant.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee)storedValueForKey(_EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY);
  }
  
  public void setToEtudiantAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    takeStoredValueForKey(value, _EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY);
  }

  public void setToEtudiantAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
      _EOGroupeEtudiant.LOG.debug("updating toEtudiantAnnee from " + toEtudiantAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiantAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee oldValue = toEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe toGroupe() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe)storedValueForKey(_EOGroupeEtudiant.TO_GROUPE_KEY);
  }
  
  public void setToGroupe(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe value) {
    takeStoredValueForKey(value, _EOGroupeEtudiant.TO_GROUPE_KEY);
  }

  public void setToGroupeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe value) {
    if (_EOGroupeEtudiant.LOG.isDebugEnabled()) {
      _EOGroupeEtudiant.LOG.debug("updating toGroupe from " + toGroupe() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToGroupe(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe oldValue = toGroupe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGroupeEtudiant.TO_GROUPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGroupeEtudiant.TO_GROUPE_KEY);
    }
  }
  

  public static EOGroupeEtudiant createSco_GroupeEtudiant(EOEditingContext editingContext, NSTimestamp dateCreation
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee, org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe toGroupe) {
    EOGroupeEtudiant eo = (EOGroupeEtudiant) EOUtilities.createAndInsertInstance(editingContext, _EOGroupeEtudiant.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToEtudiantAnneeRelationship(toEtudiantAnnee);
    eo.setToGroupeRelationship(toGroupe);
    return eo;
  }

  public static ERXFetchSpecification<EOGroupeEtudiant> fetchSpec() {
    return new ERXFetchSpecification<EOGroupeEtudiant>(_EOGroupeEtudiant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOGroupeEtudiant> fetchAllSco_GroupeEtudiants(EOEditingContext editingContext) {
    return _EOGroupeEtudiant.fetchAllSco_GroupeEtudiants(editingContext, null);
  }

  public static NSArray<EOGroupeEtudiant> fetchAllSco_GroupeEtudiants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGroupeEtudiant.fetchSco_GroupeEtudiants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGroupeEtudiant> fetchSco_GroupeEtudiants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOGroupeEtudiant> fetchSpec = new ERXFetchSpecification<EOGroupeEtudiant>(_EOGroupeEtudiant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGroupeEtudiant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOGroupeEtudiant fetchSco_GroupeEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGroupeEtudiant.fetchSco_GroupeEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGroupeEtudiant fetchSco_GroupeEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGroupeEtudiant> eoObjects = _EOGroupeEtudiant.fetchSco_GroupeEtudiants(editingContext, qualifier, null);
    EOGroupeEtudiant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_GroupeEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGroupeEtudiant fetchRequiredSco_GroupeEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGroupeEtudiant.fetchRequiredSco_GroupeEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGroupeEtudiant fetchRequiredSco_GroupeEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGroupeEtudiant eoObject = _EOGroupeEtudiant.fetchSco_GroupeEtudiant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_GroupeEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGroupeEtudiant localInstanceIn(EOEditingContext editingContext, EOGroupeEtudiant eo) {
    EOGroupeEtudiant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
