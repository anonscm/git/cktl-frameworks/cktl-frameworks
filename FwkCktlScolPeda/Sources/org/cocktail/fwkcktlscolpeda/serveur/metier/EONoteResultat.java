package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EONoteResultat extends _EONoteResultat implements INoteResultat {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EONoteResultat.class);
	
	public static final String CODE_RESULTAT_NON_ACQUIS = "D";
	
}
