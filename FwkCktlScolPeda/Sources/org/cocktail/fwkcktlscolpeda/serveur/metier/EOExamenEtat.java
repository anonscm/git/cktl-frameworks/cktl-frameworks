package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * {@inheritDoc}
 */
public class EOExamenEtat extends _EOExamenEtat implements IExamenEtat {
	private static final long serialVersionUID = 1L;
	private static ERXEnterpriseObjectCache<EOExamenEtat> examenEtatCache;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOExamenEtat.class);

	/**
	 * @return le cache des {@link EOExamenEtat} par leur type
	 */
	public static ERXEnterpriseObjectCache<EOExamenEtat> getExamenEtatCache() {
		if (examenEtatCache == null) {
			examenEtatCache = new ERXEnterpriseObjectCache<EOExamenEtat>(EOExamenEtat.class, CODE_KEY);
		}
		return examenEtatCache;
	}

	/**
	 * @param examenEtatCache le cache a setter
	 */
	public static void setExamenEtatCache(ERXEnterpriseObjectCache<EOExamenEtat> examenEtatCache) {
		EOExamenEtat.examenEtatCache = examenEtatCache;
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOExamenEtat} "Programmé"
	 */
	public static EOExamenEtat examenEtatProgramme(EOEditingContext edc) {
		return getExamenEtatCache().objectForKey(edc, EX_PROGRAMME);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOExamenEtat} "Convoqué"
	 */
	public static EOExamenEtat examenEtatCovoque(EOEditingContext edc) {
		return getExamenEtatCache().objectForKey(edc, EX_CONVOQUE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOExamenEtat} "Liste établie"
	 */
	public static EOExamenEtat examenEtatListeEtablie(EOEditingContext edc) {
		return getExamenEtatCache().objectForKey(edc, EX_LISTE_ETABLIE);
	}
}
