// DO NOT EDIT.  Make changes to EOResponsableComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOResponsableComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ResponsableComposant";

  // Attribute Keys
  public static final ERXKey<Integer> RESPONSABLE_PERS_ID = new ERXKey<Integer>("responsablePersId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> ROLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole>("roles");

  // Attributes
  public static final String RESPONSABLE_PERS_ID_KEY = RESPONSABLE_PERS_ID.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String ROLES_KEY = ROLES.key();

  private static Logger LOG = Logger.getLogger(_EOResponsableComposant.class);

  public EOResponsableComposant localInstanceIn(EOEditingContext editingContext) {
    EOResponsableComposant localInstance = (EOResponsableComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer responsablePersId() {
    return (Integer) storedValueForKey(_EOResponsableComposant.RESPONSABLE_PERS_ID_KEY);
  }

  public void setResponsablePersId(Integer value) {
    if (_EOResponsableComposant.LOG.isDebugEnabled()) {
    	_EOResponsableComposant.LOG.debug( "updating responsablePersId from " + responsablePersId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOResponsableComposant.RESPONSABLE_PERS_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOResponsableComposant.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOResponsableComposant.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOResponsableComposant.LOG.isDebugEnabled()) {
      _EOResponsableComposant.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOResponsableComposant.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOResponsableComposant.COMPOSANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> roles() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole>)storedValueForKey(_EOResponsableComposant.ROLES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> roles(EOQualifier qualifier) {
    return roles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> roles(EOQualifier qualifier, boolean fetch) {
    return roles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> roles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole.RESPONSABLE_COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole.fetchSco_ResponsableComposantRoles(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = roles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRoles(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole object) {
    includeObjectIntoPropertyWithKey(object, _EOResponsableComposant.ROLES_KEY);
  }

  public void removeFromRoles(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole object) {
    excludeObjectFromPropertyWithKey(object, _EOResponsableComposant.ROLES_KEY);
  }

  public void addToRolesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole object) {
    if (_EOResponsableComposant.LOG.isDebugEnabled()) {
      _EOResponsableComposant.LOG.debug("adding " + object + " to roles relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToRoles(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOResponsableComposant.ROLES_KEY);
    }
  }

  public void removeFromRolesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole object) {
    if (_EOResponsableComposant.LOG.isDebugEnabled()) {
      _EOResponsableComposant.LOG.debug("removing " + object + " from roles relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromRoles(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOResponsableComposant.ROLES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole createRolesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOResponsableComposant.ROLES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole) eo;
  }

  public void deleteRolesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOResponsableComposant.ROLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRolesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposantRole> objects = roles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRolesRelationship(objects.nextElement());
    }
  }


  public static EOResponsableComposant createSco_ResponsableComposant(EOEditingContext editingContext, Integer responsablePersId
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant) {
    EOResponsableComposant eo = (EOResponsableComposant) EOUtilities.createAndInsertInstance(editingContext, _EOResponsableComposant.ENTITY_NAME);    
		eo.setResponsablePersId(responsablePersId);
    eo.setComposantRelationship(composant);
    return eo;
  }

  public static ERXFetchSpecification<EOResponsableComposant> fetchSpec() {
    return new ERXFetchSpecification<EOResponsableComposant>(_EOResponsableComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOResponsableComposant> fetchAllSco_ResponsableComposants(EOEditingContext editingContext) {
    return _EOResponsableComposant.fetchAllSco_ResponsableComposants(editingContext, null);
  }

  public static NSArray<EOResponsableComposant> fetchAllSco_ResponsableComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOResponsableComposant.fetchSco_ResponsableComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOResponsableComposant> fetchSco_ResponsableComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOResponsableComposant> fetchSpec = new ERXFetchSpecification<EOResponsableComposant>(_EOResponsableComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOResponsableComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOResponsableComposant fetchSco_ResponsableComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResponsableComposant.fetchSco_ResponsableComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResponsableComposant fetchSco_ResponsableComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOResponsableComposant> eoObjects = _EOResponsableComposant.fetchSco_ResponsableComposants(editingContext, qualifier, null);
    EOResponsableComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ResponsableComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResponsableComposant fetchRequiredSco_ResponsableComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResponsableComposant.fetchRequiredSco_ResponsableComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResponsableComposant fetchRequiredSco_ResponsableComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOResponsableComposant eoObject = _EOResponsableComposant.fetchSco_ResponsableComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ResponsableComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResponsableComposant localInstanceIn(EOEditingContext editingContext, EOResponsableComposant eo) {
    EOResponsableComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
