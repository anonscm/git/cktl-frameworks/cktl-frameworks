package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * 
 * @author isabelle
 *
 */
public interface IJuryEtat {
	
	String JE_ENCOURS = "EC";
	String JE_CONVOQUE = "CONV";
	String JE_CLOS = "CLOS";

	/**
	 * @return code
	 */
	String code();

	/**
	 * @return libelle 
	 */
	String libelle();

}