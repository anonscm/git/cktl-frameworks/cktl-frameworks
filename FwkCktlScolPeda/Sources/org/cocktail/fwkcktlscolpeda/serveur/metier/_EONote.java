// DO NOT EDIT.  Make changes to EONote.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONote extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Note";

  // Attribute Keys
  public static final ERXKey<Boolean> ABSENCE = new ERXKey<Boolean>("absence");
  public static final ERXKey<String> CODE_ABSENCE = new ERXKey<String>("codeAbsence");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> ID_NOTE_RESULTAT = new ERXKey<Integer>("idNoteResultat");
  public static final ERXKey<Integer> NO_SESSION = new ERXKey<Integer>("noSession");
  public static final ERXKey<java.math.BigDecimal> NOTE_INITIALE = new ERXKey<java.math.BigDecimal>("noteInitiale");
  public static final ERXKey<java.math.BigDecimal> NOTE_SURCHARGEE = new ERXKey<java.math.BigDecimal>("noteSurchargee");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement>("toInscriptionPedagogiqueElement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat> TO_NOTE_RESULTAT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat>("toNoteResultat");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote> TO_TYPE_NOTE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote>("toTypeNote");

  // Attributes
  public static final String ABSENCE_KEY = ABSENCE.key();
  public static final String CODE_ABSENCE_KEY = CODE_ABSENCE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String ID_NOTE_RESULTAT_KEY = ID_NOTE_RESULTAT.key();
  public static final String NO_SESSION_KEY = NO_SESSION.key();
  public static final String NOTE_INITIALE_KEY = NOTE_INITIALE.key();
  public static final String NOTE_SURCHARGEE_KEY = NOTE_SURCHARGEE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY = TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT.key();
  public static final String TO_NOTE_RESULTAT_KEY = TO_NOTE_RESULTAT.key();
  public static final String TO_TYPE_NOTE_KEY = TO_TYPE_NOTE.key();

  private static Logger LOG = Logger.getLogger(_EONote.class);

  public EONote localInstanceIn(EOEditingContext editingContext) {
    EONote localInstance = (EONote)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean absence() {
    return (Boolean) storedValueForKey(_EONote.ABSENCE_KEY);
  }

  public void setAbsence(Boolean value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating absence from " + absence() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.ABSENCE_KEY);
  }

  public String codeAbsence() {
    return (String) storedValueForKey(_EONote.CODE_ABSENCE_KEY);
  }

  public void setCodeAbsence(String value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating codeAbsence from " + codeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.CODE_ABSENCE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EONote.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EONote.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.D_MODIFICATION_KEY);
  }

  public Integer idNoteResultat() {
    return (Integer) storedValueForKey(_EONote.ID_NOTE_RESULTAT_KEY);
  }

  public void setIdNoteResultat(Integer value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating idNoteResultat from " + idNoteResultat() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.ID_NOTE_RESULTAT_KEY);
  }

  public Integer noSession() {
    return (Integer) storedValueForKey(_EONote.NO_SESSION_KEY);
  }

  public void setNoSession(Integer value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating noSession from " + noSession() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.NO_SESSION_KEY);
  }

  public java.math.BigDecimal noteInitiale() {
    return (java.math.BigDecimal) storedValueForKey(_EONote.NOTE_INITIALE_KEY);
  }

  public void setNoteInitiale(java.math.BigDecimal value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating noteInitiale from " + noteInitiale() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.NOTE_INITIALE_KEY);
  }

  public java.math.BigDecimal noteSurchargee() {
    return (java.math.BigDecimal) storedValueForKey(_EONote.NOTE_SURCHARGEE_KEY);
  }

  public void setNoteSurchargee(java.math.BigDecimal value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating noteSurchargee from " + noteSurchargee() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.NOTE_SURCHARGEE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EONote.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EONote.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EONote.LOG.isDebugEnabled()) {
    	_EONote.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONote.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement toInscriptionPedagogiqueElement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement)storedValueForKey(_EONote.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY);
  }
  
  public void setToInscriptionPedagogiqueElement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement value) {
    takeStoredValueForKey(value, _EONote.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY);
  }

  public void setToInscriptionPedagogiqueElementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement value) {
    if (_EONote.LOG.isDebugEnabled()) {
      _EONote.LOG.debug("updating toInscriptionPedagogiqueElement from " + toInscriptionPedagogiqueElement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToInscriptionPedagogiqueElement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement oldValue = toInscriptionPedagogiqueElement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EONote.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EONote.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat toNoteResultat() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat)storedValueForKey(_EONote.TO_NOTE_RESULTAT_KEY);
  }
  
  public void setToNoteResultat(org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat value) {
    takeStoredValueForKey(value, _EONote.TO_NOTE_RESULTAT_KEY);
  }

  public void setToNoteResultatRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat value) {
    if (_EONote.LOG.isDebugEnabled()) {
      _EONote.LOG.debug("updating toNoteResultat from " + toNoteResultat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToNoteResultat(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EONoteResultat oldValue = toNoteResultat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EONote.TO_NOTE_RESULTAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EONote.TO_NOTE_RESULTAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote toTypeNote() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote)storedValueForKey(_EONote.TO_TYPE_NOTE_KEY);
  }
  
  public void setToTypeNote(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote value) {
    takeStoredValueForKey(value, _EONote.TO_TYPE_NOTE_KEY);
  }

  public void setToTypeNoteRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote value) {
    if (_EONote.LOG.isDebugEnabled()) {
      _EONote.LOG.debug("updating toTypeNote from " + toTypeNote() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeNote(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote oldValue = toTypeNote();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EONote.TO_TYPE_NOTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EONote.TO_TYPE_NOTE_KEY);
    }
  }
  

  public static EONote createSco_Note(EOEditingContext editingContext, Boolean absence
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement toInscriptionPedagogiqueElement, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote toTypeNote) {
    EONote eo = (EONote) EOUtilities.createAndInsertInstance(editingContext, _EONote.ENTITY_NAME);    
		eo.setAbsence(absence);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToInscriptionPedagogiqueElementRelationship(toInscriptionPedagogiqueElement);
    eo.setToTypeNoteRelationship(toTypeNote);
    return eo;
  }

  public static ERXFetchSpecification<EONote> fetchSpec() {
    return new ERXFetchSpecification<EONote>(_EONote.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONote> fetchAllSco_Notes(EOEditingContext editingContext) {
    return _EONote.fetchAllSco_Notes(editingContext, null);
  }

  public static NSArray<EONote> fetchAllSco_Notes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONote.fetchSco_Notes(editingContext, null, sortOrderings);
  }

  public static NSArray<EONote> fetchSco_Notes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONote> fetchSpec = new ERXFetchSpecification<EONote>(_EONote.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONote> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONote fetchSco_Note(EOEditingContext editingContext, String keyName, Object value) {
    return _EONote.fetchSco_Note(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONote fetchSco_Note(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONote> eoObjects = _EONote.fetchSco_Notes(editingContext, qualifier, null);
    EONote eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Note that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONote fetchRequiredSco_Note(EOEditingContext editingContext, String keyName, Object value) {
    return _EONote.fetchRequiredSco_Note(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONote fetchRequiredSco_Note(EOEditingContext editingContext, EOQualifier qualifier) {
    EONote eoObject = _EONote.fetchSco_Note(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Note that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONote localInstanceIn(EOEditingContext editingContext, EONote eo) {
    EONote localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
