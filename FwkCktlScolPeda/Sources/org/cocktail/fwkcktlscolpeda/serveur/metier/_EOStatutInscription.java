// DO NOT EDIT.  Make changes to EOStatutInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOStatutInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_StatutInscription";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID_STATUT_INSCRIPTION = new ERXKey<Integer>("idStatutInscription");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_STATUT_INSCRIPTION_KEY = ID_STATUT_INSCRIPTION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOStatutInscription.class);

  public EOStatutInscription localInstanceIn(EOEditingContext editingContext) {
    EOStatutInscription localInstance = (EOStatutInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOStatutInscription.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOStatutInscription.LOG.isDebugEnabled()) {
    	_EOStatutInscription.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutInscription.CODE_KEY);
  }

  public Integer idStatutInscription() {
    return (Integer) storedValueForKey(_EOStatutInscription.ID_STATUT_INSCRIPTION_KEY);
  }

  public void setIdStatutInscription(Integer value) {
    if (_EOStatutInscription.LOG.isDebugEnabled()) {
    	_EOStatutInscription.LOG.debug( "updating idStatutInscription from " + idStatutInscription() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutInscription.ID_STATUT_INSCRIPTION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOStatutInscription.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOStatutInscription.LOG.isDebugEnabled()) {
    	_EOStatutInscription.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutInscription.LIBELLE_KEY);
  }


  public static EOStatutInscription createSco_StatutInscription(EOEditingContext editingContext, String code
, Integer idStatutInscription
, String libelle
) {
    EOStatutInscription eo = (EOStatutInscription) EOUtilities.createAndInsertInstance(editingContext, _EOStatutInscription.ENTITY_NAME);    
		eo.setCode(code);
		eo.setIdStatutInscription(idStatutInscription);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOStatutInscription> fetchSpec() {
    return new ERXFetchSpecification<EOStatutInscription>(_EOStatutInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOStatutInscription> fetchAllSco_StatutInscriptions(EOEditingContext editingContext) {
    return _EOStatutInscription.fetchAllSco_StatutInscriptions(editingContext, null);
  }

  public static NSArray<EOStatutInscription> fetchAllSco_StatutInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStatutInscription.fetchSco_StatutInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStatutInscription> fetchSco_StatutInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOStatutInscription> fetchSpec = new ERXFetchSpecification<EOStatutInscription>(_EOStatutInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStatutInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOStatutInscription fetchSco_StatutInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutInscription.fetchSco_StatutInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutInscription fetchSco_StatutInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStatutInscription> eoObjects = _EOStatutInscription.fetchSco_StatutInscriptions(editingContext, qualifier, null);
    EOStatutInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_StatutInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutInscription fetchRequiredSco_StatutInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutInscription.fetchRequiredSco_StatutInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutInscription fetchRequiredSco_StatutInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStatutInscription eoObject = _EOStatutInscription.fetchSco_StatutInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_StatutInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutInscription localInstanceIn(EOEditingContext editingContext, EOStatutInscription eo) {
    EOStatutInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
