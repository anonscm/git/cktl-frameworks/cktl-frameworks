package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOParametragePaieArticleComplementaire extends _EOParametragePaieArticleComplementaire implements IParametragePaieArticleComplementaire {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParametragePaieArticleComplementaire.class);

	public void setToTypeArticleComplementaireRelationship(ITypeArticleComplementaire value) {
		super.setToTypeArticleComplementaireRelationship((EOTypeArticleComplementaire) value);
	}

	@Inject
	@Nullable
	private UserInfo userInfo;

	/**
	 * @return le libellé complet de l'article de paiement (libellé type + libellé)
	 */
	public String libelleComplet() {
		return toTypeArticleComplementaire().libelle() + " - " + libelle();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
