package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import com.webobjects.eocontrol.EOEditingContext;
import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Classe statut d'inscription
 */
public class EOStatutInscription extends _EOStatutInscription {
	
    private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOStatutInscription.class);

	private static final String STATUT_INSCRIPTION_VALIDEE = "VALIDEE";
	private static final String STATUT_INSCRIPTION_REFUSEE = "REFUSEE";
	
	private static ERXEnterpriseObjectCache<EOStatutInscription> statutInscriptionCache;

	/**
	 * @return les status d'inscription en cache
	 */
	public static ERXEnterpriseObjectCache<EOStatutInscription> getStatutInscriptionCache() {
		if (statutInscriptionCache == null) {
			statutInscriptionCache = new ERXEnterpriseObjectCache<EOStatutInscription>(EOStatutInscription.class, CODE_KEY);
		}
	    return statutInscriptionCache;
    }

	public static void setStatutInscriptionCache(ERXEnterpriseObjectCache<EOStatutInscription> statutInscriptionCache) {
	    EOStatutInscription.statutInscriptionCache = statutInscriptionCache;
    }
	
	/**
	 * @param edc editingContext
	 * @return le statut d'inscription validee
	 */
	public static EOStatutInscription statutInscriptionValidee(EOEditingContext edc) {
		return getStatutInscriptionCache().objectForKey(edc, STATUT_INSCRIPTION_VALIDEE);
	}
	
	/**
	 * @param edc editingContext
	 * @return le statut d'inscription refusée
	 */
	public static EOStatutInscription statutInscriptionRefusee(EOEditingContext edc) {
		return getStatutInscriptionCache().objectForKey(edc, STATUT_INSCRIPTION_REFUSEE);
	}
}
