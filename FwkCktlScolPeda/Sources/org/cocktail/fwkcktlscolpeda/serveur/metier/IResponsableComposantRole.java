package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 *  Role d'un reponsable
 */
public interface IResponsableComposantRole {

	/**
	 * @return definition du role
	 */
	IRoleResponsable role();
	
	/**
	 * @param responsablecomposant 
	 * @return copie du role
	 */
	EOResponsableComposantRole copy(IResponsableComposant responsablecomposant);

}
