package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author isabelle
 * 
 */
public interface IJury {

	/**
	 * @param value date de creation
	 */
	void setDCreation(Date value);

	/**
	 * @return date de modification
	 */
	Date dModification();

	/**
	 * @param value date de modification
	 */
	void setDModification(Date value);

	/**
	 * @return perdId de l'utilisateur qui a créé le jury
	 */
	Integer persIdCreation();

	/**
	 * @param value perdId de l'utilisateur qui a créé le jury
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return perdId de l'utilisateur qui a modifié le jury
	 */
	Integer persIdModification();

	/**
	 * @param value perdId de l'utilisateur qui a modifié le jury
	 */
	void setPersIdModification(Integer value);

	/**
	 * @return etat du jury (en cours, convoqué, clos...)
	 */
	IJuryEtat toJuryEtat();

	/**
	 * @param value etat du jury (en cours, convoqué, clos...)
	 */
	void setToJuryEtatRelationship(IJuryEtat value);

	/**
	 * @return llien entre le diplome et la version de diplome
	 */
	ILien toLienFormation();

	/**
	 * @param value lien entre le diplome et la version de diplome
	 */
	void setToLienFormationRelationship(ILien value);

	/**
	 * @return lien qui permet de recupérer l'année
	 */
	ILien toLienNiveauAnnee();

	/**
	 * @param value lien qui permet de recupérer l'année
	 */
	void setToLienNiveauAnneeRelationship(ILien value);

	/**
	 * @return lien entre le parcours et le semestre
	 */
	ILien toLienNiveauSemestre();

	/**
	 * @param value lien entre le parcours et le semestre
	 */
	void setToLienNiveauSemestreRelationship(ILien value);

	/**
	 * @return lien entre l'année et le parcours
	 */
	ILien toLienParcoursAnnee();

	/**
	 * @param value lien entre l'année et le parcours
	 */
	void setToLienParcoursAnneeRelationship(ILien value);

	/**
	 * @return lien entre la version de diplome et le parcours
	 */
	ILien toLienParcoursFormation();

	/**
	 * @param value lien entre la version de diplome et le parcours
	 */
	void setToLienParcoursFormationRelationship(ILien value);

	/**
	 * @return liste de convocations
	 */
	List<? extends IJuryConvocation> toJuryConvocations();

	/**
	 * @param object convocation ajoutée à la liste des convocations
	 */
	void addToToJuryConvocationsRelationship(IJuryConvocation object);

	/**
	 * @return liste des personnes du jury
	 */
	List<? extends IJuryPersonnes> toJuryPersonneses();

	/**
	 * @param object personne ajoutée au jury
	 */
	void addToToJuryPersonnesesRelationship(IJuryPersonnes object);

	/**
	 * @return annee unversitaire sur laquelle est positionné le jury
	 */
	Integer annee();

	/**
	 * @param value annee unversitaire sur laquelle est positionné le jury
	 */
	void setAnnee(Integer value);

	/**
	 * @return le commentaire général du jury sur la session
	 */
	String commentaireGeneral();

	/**
	 * @param value le commentaire général du jury sur la session
	 */
	void setCommentaireGeneral(String value);

	/**
	 * @return les modalités de rattrapage
	 */
	String modalitesRattrapage();

	/**
	 * @param value les modalités de rattrapage
	 */
	void setModalitesRattrapage(String value);

	/**
	 * @return les modalités de redoublement
	 */
	String modalitesRedoublement();

	/**
	 * @param value les modalités de redoublement
	 */
	void setModalitesRedoublement(String value);

	/**
	 * @return les évolutions envisagées
	 */
	String evolutionsEnvisagees();

	/**
	 * @param value les évolutions envisagées
	 */
	void setEvolutionsEnvisagees(String value);

	/**
	 * @return points de jury accordés à l'ensemble des étudiants sur la session
	 */
	java.math.BigDecimal pointsJury();

	/**
	 * @param value points de jury accordés à l'ensemble des étudiants sur la session
	 */
	void setPointsJury(java.math.BigDecimal value);

	/**
	 * @return commentaire sur les points de jurys accordés
	 */
	String pointsJuryCommentaire();

	/**
	 * @param value commentaire sur les points de jurys accordés
	 */
	void setPointsJuryCommentaire(String value);

	/**
	 * @return identifiant du jury
	 */
	Integer idJury();

	/**
	 * @param value identifiant du jury
	 */
	void setIdJury(Integer value);
	
	ISessionPeriode toSessionPeriode();
		  

	void setToSessionPeriodeRelationship(ISessionPeriode value);


}