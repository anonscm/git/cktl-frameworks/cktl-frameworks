package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.foundation.NSTimestamp;

/**
 * Interface correspondant l'organisation d'un examen
 * @author isabelle
 *
 */
public interface IExamenOrganisation {

	/**
	 * 
	 * @return la cle primaire
	 */
	Integer idExamenOrganisation();
	
	/**
	 * 
	 * @return la date de l'examen
	 */
	NSTimestamp dateExamen();

	/**
	 * 
	 * @param value la date de l'examen
	 */
	void setDateExamen(NSTimestamp value);

	/**
	 * 
	 * @return la salle de l'examen
	 */
	String salleExamen();

	/**
	 * 
	 * @param value la salle de l'examen
	 */
	void setSalleExamen(String value);

	/**
	 * 
	 * @return l'examen auquel il est relié
	 */
	IExamen toExamen();

	/**
	 * 
	 * @param value l'examen auquel il est relié
	 */
	void setToExamenRelationship(IExamen value);

}