// DO NOT EDIT.  Make changes to EOCursus.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOCursus extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Cursus";

  // Attribute Keys
  public static final ERXKey<Integer> ACCES_ANNEE = new ERXKey<Integer>("accesAnnee");
  public static final ERXKey<Integer> ACCES_MENTION = new ERXKey<Integer>("accesMention");
  public static final ERXKey<Integer> ACCES_TITRE = new ERXKey<Integer>("accesTitre");
  public static final ERXKey<Integer> ANNEE_DEBUT = new ERXKey<Integer>("anneeDebut");
  public static final ERXKey<Integer> ANNEE_FIN = new ERXKey<Integer>("anneeFin");
  public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
  public static final ERXKey<String> C_RNE = new ERXKey<String>("cRne");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<String> DIPLOME = new ERXKey<String>("diplome");
  public static final ERXKey<NSTimestamp> D_MODIFCATION = new ERXKey<NSTimestamp>("dModifcation");
  public static final ERXKey<String> ETAB_ETRANGER = new ERXKey<String>("etabEtranger");
  public static final ERXKey<String> FORMATION = new ERXKey<String>("formation");
  public static final ERXKey<Integer> GRADE = new ERXKey<Integer>("grade");
  public static final ERXKey<Boolean> INTERRUPTION_ETUD = new ERXKey<Boolean>("interruptionEtud");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MOFICATION = new ERXKey<Integer>("persIdMofication");
  public static final ERXKey<Integer> TYPE = new ERXKey<Integer>("type");
  public static final ERXKey<String> TYPE_INSCRIPTION = new ERXKey<String>("typeInscription");
  public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention> TO_ACCES_MENTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention>("toAccesMention");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_ACCES_TITRE_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toAccesTitreGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_C_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toCRne");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPays");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TO_TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("toTypeFormation");

  // Attributes
  public static final String ACCES_ANNEE_KEY = ACCES_ANNEE.key();
  public static final String ACCES_MENTION_KEY = ACCES_MENTION.key();
  public static final String ACCES_TITRE_KEY = ACCES_TITRE.key();
  public static final String ANNEE_DEBUT_KEY = ANNEE_DEBUT.key();
  public static final String ANNEE_FIN_KEY = ANNEE_FIN.key();
  public static final String C_PAYS_KEY = C_PAYS.key();
  public static final String C_RNE_KEY = C_RNE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String DIPLOME_KEY = DIPLOME.key();
  public static final String D_MODIFCATION_KEY = D_MODIFCATION.key();
  public static final String ETAB_ETRANGER_KEY = ETAB_ETRANGER.key();
  public static final String FORMATION_KEY = FORMATION.key();
  public static final String GRADE_KEY = GRADE.key();
  public static final String INTERRUPTION_ETUD_KEY = INTERRUPTION_ETUD.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MOFICATION_KEY = PERS_ID_MOFICATION.key();
  public static final String TYPE_KEY = TYPE.key();
  public static final String TYPE_INSCRIPTION_KEY = TYPE_INSCRIPTION.key();
  public static final String VILLE_KEY = VILLE.key();
  // Relationships
  public static final String TO_ACCES_MENTION_KEY = TO_ACCES_MENTION.key();
  public static final String TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY = TO_ACCES_TITRE_GRADE_UNIVERSITAIRE.key();
  public static final String TO_C_RNE_KEY = TO_C_RNE.key();
  public static final String TO_ETUDIANT_KEY = TO_ETUDIANT.key();
  public static final String TO_GRADE_UNIVERSITAIRE_KEY = TO_GRADE_UNIVERSITAIRE.key();
  public static final String TO_PAYS_KEY = TO_PAYS.key();
  public static final String TO_TYPE_FORMATION_KEY = TO_TYPE_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOCursus.class);

  public EOCursus localInstanceIn(EOEditingContext editingContext) {
    EOCursus localInstance = (EOCursus)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer accesAnnee() {
    return (Integer) storedValueForKey(_EOCursus.ACCES_ANNEE_KEY);
  }

  public void setAccesAnnee(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating accesAnnee from " + accesAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ACCES_ANNEE_KEY);
  }

  public Integer accesMention() {
    return (Integer) storedValueForKey(_EOCursus.ACCES_MENTION_KEY);
  }

  public void setAccesMention(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating accesMention from " + accesMention() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ACCES_MENTION_KEY);
  }

  public Integer accesTitre() {
    return (Integer) storedValueForKey(_EOCursus.ACCES_TITRE_KEY);
  }

  public void setAccesTitre(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating accesTitre from " + accesTitre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ACCES_TITRE_KEY);
  }

  public Integer anneeDebut() {
    return (Integer) storedValueForKey(_EOCursus.ANNEE_DEBUT_KEY);
  }

  public void setAnneeDebut(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating anneeDebut from " + anneeDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ANNEE_DEBUT_KEY);
  }

  public Integer anneeFin() {
    return (Integer) storedValueForKey(_EOCursus.ANNEE_FIN_KEY);
  }

  public void setAnneeFin(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating anneeFin from " + anneeFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ANNEE_FIN_KEY);
  }

  public String cPays() {
    return (String) storedValueForKey(_EOCursus.C_PAYS_KEY);
  }

  public void setCPays(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating cPays from " + cPays() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.C_PAYS_KEY);
  }

  public String cRne() {
    return (String) storedValueForKey(_EOCursus.C_RNE_KEY);
  }

  public void setCRne(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.C_RNE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOCursus.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.D_CREATION_KEY);
  }

  public String diplome() {
    return (String) storedValueForKey(_EOCursus.DIPLOME_KEY);
  }

  public void setDiplome(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating diplome from " + diplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.DIPLOME_KEY);
  }

  public NSTimestamp dModifcation() {
    return (NSTimestamp) storedValueForKey(_EOCursus.D_MODIFCATION_KEY);
  }

  public void setDModifcation(NSTimestamp value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating dModifcation from " + dModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.D_MODIFCATION_KEY);
  }

  public String etabEtranger() {
    return (String) storedValueForKey(_EOCursus.ETAB_ETRANGER_KEY);
  }

  public void setEtabEtranger(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating etabEtranger from " + etabEtranger() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.ETAB_ETRANGER_KEY);
  }

  public String formation() {
    return (String) storedValueForKey(_EOCursus.FORMATION_KEY);
  }

  public void setFormation(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating formation from " + formation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.FORMATION_KEY);
  }

  public Integer grade() {
    return (Integer) storedValueForKey(_EOCursus.GRADE_KEY);
  }

  public void setGrade(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating grade from " + grade() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.GRADE_KEY);
  }

  public Boolean interruptionEtud() {
    return (Boolean) storedValueForKey(_EOCursus.INTERRUPTION_ETUD_KEY);
  }

  public void setInterruptionEtud(Boolean value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating interruptionEtud from " + interruptionEtud() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.INTERRUPTION_ETUD_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EOCursus.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.NIVEAU_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOCursus.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.PERS_ID_CREATION_KEY);
  }

  public Integer persIdMofication() {
    return (Integer) storedValueForKey(_EOCursus.PERS_ID_MOFICATION_KEY);
  }

  public void setPersIdMofication(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating persIdMofication from " + persIdMofication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.PERS_ID_MOFICATION_KEY);
  }

  public Integer type() {
    return (Integer) storedValueForKey(_EOCursus.TYPE_KEY);
  }

  public void setType(Integer value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.TYPE_KEY);
  }

  public String typeInscription() {
    return (String) storedValueForKey(_EOCursus.TYPE_INSCRIPTION_KEY);
  }

  public void setTypeInscription(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating typeInscription from " + typeInscription() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.TYPE_INSCRIPTION_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(_EOCursus.VILLE_KEY);
  }

  public void setVille(String value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
    	_EOCursus.LOG.debug( "updating ville from " + ville() + " to " + value);
    }
    takeStoredValueForKey(value, _EOCursus.VILLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention toAccesMention() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention)storedValueForKey(_EOCursus.TO_ACCES_MENTION_KEY);
  }
  
  public void setToAccesMention(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention value) {
    takeStoredValueForKey(value, _EOCursus.TO_ACCES_MENTION_KEY);
  }

  public void setToAccesMentionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toAccesMention from " + toAccesMention() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAccesMention(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention oldValue = toAccesMention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_ACCES_MENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_ACCES_MENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toAccesTitreGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EOCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setToAccesTitreGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EOCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToAccesTitreGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toAccesTitreGradeUniversitaire from " + toAccesTitreGradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAccesTitreGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toAccesTitreGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toCRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(_EOCursus.TO_C_RNE_KEY);
  }
  
  public void setToCRne(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    takeStoredValueForKey(value, _EOCursus.TO_C_RNE_KEY);
  }

  public void setToCRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toCRne from " + toCRne() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToCRne(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toCRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_C_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_C_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant)storedValueForKey(_EOCursus.TO_ETUDIANT_KEY);
  }
  
  public void setToEtudiant(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    takeStoredValueForKey(value, _EOCursus.TO_ETUDIANT_KEY);
  }

  public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toEtudiant from " + toEtudiant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EOCursus.TO_GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setToGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EOCursus.TO_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toGradeUniversitaire from " + toGradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(_EOCursus.TO_PAYS_KEY);
  }
  
  public void setToPays(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    takeStoredValueForKey(value, _EOCursus.TO_PAYS_KEY);
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toPays from " + toPays() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPays(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_PAYS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation toTypeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(_EOCursus.TO_TYPE_FORMATION_KEY);
  }
  
  public void setToTypeFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    takeStoredValueForKey(value, _EOCursus.TO_TYPE_FORMATION_KEY);
  }

  public void setToTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (_EOCursus.LOG.isDebugEnabled()) {
      _EOCursus.LOG.debug("updating toTypeFormation from " + toTypeFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = toTypeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOCursus.TO_TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOCursus.TO_TYPE_FORMATION_KEY);
    }
  }
  

  public static EOCursus createSco_Cursus(EOEditingContext editingContext, Integer anneeDebut
, NSTimestamp dCreation
, Boolean interruptionEtud
, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant) {
    EOCursus eo = (EOCursus) EOUtilities.createAndInsertInstance(editingContext, _EOCursus.ENTITY_NAME);    
		eo.setAnneeDebut(anneeDebut);
		eo.setDCreation(dCreation);
		eo.setInterruptionEtud(interruptionEtud);
    eo.setToEtudiantRelationship(toEtudiant);
    return eo;
  }

  public static ERXFetchSpecification<EOCursus> fetchSpec() {
    return new ERXFetchSpecification<EOCursus>(_EOCursus.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOCursus> fetchAllSco_Cursuses(EOEditingContext editingContext) {
    return _EOCursus.fetchAllSco_Cursuses(editingContext, null);
  }

  public static NSArray<EOCursus> fetchAllSco_Cursuses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOCursus.fetchSco_Cursuses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOCursus> fetchSco_Cursuses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOCursus> fetchSpec = new ERXFetchSpecification<EOCursus>(_EOCursus.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOCursus> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOCursus fetchSco_Cursus(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCursus.fetchSco_Cursus(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCursus fetchSco_Cursus(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOCursus> eoObjects = _EOCursus.fetchSco_Cursuses(editingContext, qualifier, null);
    EOCursus eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Cursus that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCursus fetchRequiredSco_Cursus(EOEditingContext editingContext, String keyName, Object value) {
    return _EOCursus.fetchRequiredSco_Cursus(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOCursus fetchRequiredSco_Cursus(EOEditingContext editingContext, EOQualifier qualifier) {
    EOCursus eoObject = _EOCursus.fetchSco_Cursus(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Cursus that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOCursus localInstanceIn(EOEditingContext editingContext, EOCursus eo) {
    EOCursus localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
