// DO NOT EDIT.  Make changes to EOExamenEtat.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOExamenEtat extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ExamenEtat";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOExamenEtat.class);

  public EOExamenEtat localInstanceIn(EOEditingContext editingContext) {
    EOExamenEtat localInstance = (EOExamenEtat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOExamenEtat.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOExamenEtat.LOG.isDebugEnabled()) {
    	_EOExamenEtat.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtat.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOExamenEtat.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOExamenEtat.LOG.isDebugEnabled()) {
    	_EOExamenEtat.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtat.LIBELLE_KEY);
  }


  public static EOExamenEtat createSco_ExamenEtat(EOEditingContext editingContext, String code
, String libelle
) {
    EOExamenEtat eo = (EOExamenEtat) EOUtilities.createAndInsertInstance(editingContext, _EOExamenEtat.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOExamenEtat> fetchSpec() {
    return new ERXFetchSpecification<EOExamenEtat>(_EOExamenEtat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOExamenEtat> fetchAllSco_ExamenEtats(EOEditingContext editingContext) {
    return _EOExamenEtat.fetchAllSco_ExamenEtats(editingContext, null);
  }

  public static NSArray<EOExamenEtat> fetchAllSco_ExamenEtats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOExamenEtat.fetchSco_ExamenEtats(editingContext, null, sortOrderings);
  }

  public static NSArray<EOExamenEtat> fetchSco_ExamenEtats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOExamenEtat> fetchSpec = new ERXFetchSpecification<EOExamenEtat>(_EOExamenEtat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOExamenEtat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOExamenEtat fetchSco_ExamenEtat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenEtat.fetchSco_ExamenEtat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenEtat fetchSco_ExamenEtat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOExamenEtat> eoObjects = _EOExamenEtat.fetchSco_ExamenEtats(editingContext, qualifier, null);
    EOExamenEtat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ExamenEtat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenEtat fetchRequiredSco_ExamenEtat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenEtat.fetchRequiredSco_ExamenEtat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenEtat fetchRequiredSco_ExamenEtat(EOEditingContext editingContext, EOQualifier qualifier) {
    EOExamenEtat eoObject = _EOExamenEtat.fetchSco_ExamenEtat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ExamenEtat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenEtat localInstanceIn(EOEditingContext editingContext, EOExamenEtat eo) {
    EOExamenEtat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
