package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Cette classe représente une échanece de paiement.
 *  
 * @author Pascal MACOUIN
 */
public class EOPaiementEcheance extends _EOPaiementEcheance implements IScoPaiementEcheance {
	private static final long serialVersionUID = 6127714086380538445L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPaiementEcheance.class);

	@Inject
	@Nullable
	private UserInfo userInfo;
	
	/**
	 * {@inheritDoc}
	 */
	public void setToPaiementMoyenRelationship(IPaiementMoyen value) {
		super.setToPaiementMoyenRelationship((EOPaiementMoyen) value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToAutrePaiementMoyenRelationship(IPaiementMoyen value) {
		super.setToAutrePaiementMoyenRelationship((EOPaiementMoyen) value);
	}
	
	/**
	 * @param date la date d'échéance
	 */
	@Override
	public void setDateEcheance(NSTimestamp date) {
		// On ne veut que la date (sans les heure, minutes, secondes, ...).
		super.setDateEcheance(new NSTimestamp(DateCtrl.getDateOnly(date)));
	}

	/**
	 * @param date la date d'échéance
	 */
	public void setDateEcheance(Date date) {
		// On ne veut que la date (sans les heure, minutes, secondes, ...).
		super.setDateEcheance(new NSTimestamp(DateCtrl.getDateOnly(date)));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
