// DO NOT EDIT.  Make changes to EOSessionComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSessionComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SessionComposant";

  // Attribute Keys
  public static final ERXKey<Integer> ACTIVE = new ERXKey<Integer>("active");
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NUMERO_SESSION = new ERXKey<Integer>("numeroSession");
  public static final ERXKey<Integer> SEUIL = new ERXKey<Integer>("seuil");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> FORMULES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule>("formules");

  // Attributes
  public static final String ACTIVE_KEY = ACTIVE.key();
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NUMERO_SESSION_KEY = NUMERO_SESSION.key();
  public static final String SEUIL_KEY = SEUIL.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String FORMULES_KEY = FORMULES.key();

  private static Logger LOG = Logger.getLogger(_EOSessionComposant.class);

  public EOSessionComposant localInstanceIn(EOEditingContext editingContext) {
    EOSessionComposant localInstance = (EOSessionComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer active() {
    return (Integer) storedValueForKey(_EOSessionComposant.ACTIVE_KEY);
  }

  public void setActive(Integer value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating active from " + active() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.ACTIVE_KEY);
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOSessionComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.COMPOSANT_ID_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOSessionComposant.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSessionComposant.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.LIBELLE_KEY);
  }

  public Integer numeroSession() {
    return (Integer) storedValueForKey(_EOSessionComposant.NUMERO_SESSION_KEY);
  }

  public void setNumeroSession(Integer value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating numeroSession from " + numeroSession() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.NUMERO_SESSION_KEY);
  }

  public Integer seuil() {
    return (Integer) storedValueForKey(_EOSessionComposant.SEUIL_KEY);
  }

  public void setSeuil(Integer value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
    	_EOSessionComposant.LOG.debug( "updating seuil from " + seuil() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionComposant.SEUIL_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOSessionComposant.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOSessionComposant.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
      _EOSessionComposant.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOSessionComposant.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOSessionComposant.COMPOSANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> formules() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule>)storedValueForKey(_EOSessionComposant.FORMULES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> formules(EOQualifier qualifier) {
    return formules(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> formules(EOQualifier qualifier, boolean fetch) {
    return formules(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> formules(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule.SESSION_COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule.fetchSco_Formules(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = formules();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFormules(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule object) {
    includeObjectIntoPropertyWithKey(object, _EOSessionComposant.FORMULES_KEY);
  }

  public void removeFromFormules(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule object) {
    excludeObjectFromPropertyWithKey(object, _EOSessionComposant.FORMULES_KEY);
  }

  public void addToFormulesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule object) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
      _EOSessionComposant.LOG.debug("adding " + object + " to formules relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToFormules(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOSessionComposant.FORMULES_KEY);
    }
  }

  public void removeFromFormulesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule object) {
    if (_EOSessionComposant.LOG.isDebugEnabled()) {
      _EOSessionComposant.LOG.debug("removing " + object + " from formules relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromFormules(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOSessionComposant.FORMULES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule createFormulesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOSessionComposant.FORMULES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule) eo;
  }

  public void deleteFormulesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOSessionComposant.FORMULES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFormulesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule> objects = formules().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFormulesRelationship(objects.nextElement());
    }
  }


  public static EOSessionComposant createSco_SessionComposant(EOEditingContext editingContext, Integer id
, Integer numeroSession
) {
    EOSessionComposant eo = (EOSessionComposant) EOUtilities.createAndInsertInstance(editingContext, _EOSessionComposant.ENTITY_NAME);    
		eo.setId(id);
		eo.setNumeroSession(numeroSession);
    return eo;
  }

  public static ERXFetchSpecification<EOSessionComposant> fetchSpec() {
    return new ERXFetchSpecification<EOSessionComposant>(_EOSessionComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSessionComposant> fetchAllSco_SessionComposants(EOEditingContext editingContext) {
    return _EOSessionComposant.fetchAllSco_SessionComposants(editingContext, null);
  }

  public static NSArray<EOSessionComposant> fetchAllSco_SessionComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSessionComposant.fetchSco_SessionComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSessionComposant> fetchSco_SessionComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSessionComposant> fetchSpec = new ERXFetchSpecification<EOSessionComposant>(_EOSessionComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSessionComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSessionComposant fetchSco_SessionComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSessionComposant.fetchSco_SessionComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSessionComposant fetchSco_SessionComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSessionComposant> eoObjects = _EOSessionComposant.fetchSco_SessionComposants(editingContext, qualifier, null);
    EOSessionComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SessionComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSessionComposant fetchRequiredSco_SessionComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSessionComposant.fetchRequiredSco_SessionComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSessionComposant fetchRequiredSco_SessionComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSessionComposant eoObject = _EOSessionComposant.fetchSco_SessionComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SessionComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSessionComposant localInstanceIn(EOEditingContext editingContext, EOSessionComposant eo) {
    EOSessionComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
