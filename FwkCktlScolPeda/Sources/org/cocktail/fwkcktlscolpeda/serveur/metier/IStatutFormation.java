package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 *  statut de la formation
 */
public interface IStatutFormation {
	static final String STATUT_ECC_CODE = "ECC";
	static final String STATUT_EP_CODE = "EP";
	static final String STATUT_F_CODE = "F";
}