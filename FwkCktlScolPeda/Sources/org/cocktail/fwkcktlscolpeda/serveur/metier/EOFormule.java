package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * {@inheritDoc}
 */
public class EOFormule extends _EOFormule implements IFormule {
	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public void setTypeFormuleRelationship(ITypeFormule unTypeFormule) {
		super.setTypeFormuleRelationship((EOTypeFormule) unTypeFormule);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setSessionComposantRelationship(ISessionComposant uneSessionComposant) {
		super.setSessionComposantRelationship((EOSessionComposant) uneSessionComposant);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return "Formule " + id() + "de type " + this.typeFormule() + " de la session " + sessionComposant();
	}

	/**
	 * @return copie de la formule
	 */
	public EOFormule copy() {
		EOFormule formule= new EOFormule();
		editingContext().insertObject(formule);
		formule.setTypeFormuleRelationship(this.typeFormule());
		formule.setFormule(this.formule());
		return formule;
	}
	
	public Boolean isMoyennePonderee() {
		return ((this.moyennePonderee()!=null) && (this.moyennePonderee()==1));
	}
	
}
