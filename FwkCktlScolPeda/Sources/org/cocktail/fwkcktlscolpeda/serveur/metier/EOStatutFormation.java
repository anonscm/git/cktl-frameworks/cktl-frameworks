package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOStatutFormation extends _EOStatutFormation implements IStatutFormation {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOStatutFormation.class);
	private static ERXEnterpriseObjectCache<EOStatutFormation> statutFormationCache;
	
	
	
	
	/**
     * @return le cache des {@link EOStatutFormation} par leur code
     */
    public static ERXEnterpriseObjectCache<EOStatutFormation> getStatutFormationCache() {
    	if (statutFormationCache == null) {
    		statutFormationCache = new ERXEnterpriseObjectCache<EOStatutFormation>(EOStatutFormation.class, CODE_KEY);
    	}
    	return statutFormationCache;
    }
    
    /**
     * @param statutFormationCache le cache a setter
     */
    public static void setStatutFormationCache(ERXEnterpriseObjectCache<EOStatutFormation> statutFormationCache) {
    	EOStatutFormation.statutFormationCache = statutFormationCache;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOStatutFormation} "STATUT_ECC_CODE"
	 */
	public static EOStatutFormation statutEnCoursConstruction(EOEditingContext edc) {
		return getStatutFormationCache().objectForKey(edc, STATUT_ECC_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOStatutFormation} "STATUT_EP_CODE"
	 */
	public static EOStatutFormation statutEnProduction(EOEditingContext edc) {
		return getStatutFormationCache().objectForKey(edc, STATUT_EP_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOStatutFormation} "STATUT_F_CODE"
	 */
	public static EOStatutFormation statutFictive(EOEditingContext edc) {
		return getStatutFormationCache().objectForKey(edc, STATUT_F_CODE);
	}
	
	@Override
	public String toString() {
		return this.libelle();
	}
	
}
