package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;


public class EORoleResponsable extends _EORoleResponsable implements IRoleResponsable{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORoleResponsable.class);
	private static ERXEnterpriseObjectCache<EORoleResponsable> roleResponsableCache;
	
	public static final String ROLE_SECRETAIRE_CODE = "SEC";
	public static final String ROLE_ENSEIGNANT_CODE = "ENS";
	public static final String ROLE_RESPONSABLE_CODE = "RESP";

    /**
     * @return le cache des {@link EORoleResponsable} par leur code
     */
    public static ERXEnterpriseObjectCache<EORoleResponsable> getRoleResponsableCache() {
    	if (roleResponsableCache == null) {
    		roleResponsableCache = new ERXEnterpriseObjectCache<EORoleResponsable>(EORoleResponsable.class, CODE_KEY);
    	}
    	return roleResponsableCache;
    }
    
    /**
     * @param roleResponsableCache le cache a setter
     */
    public static void setRoleResponsableCache(ERXEnterpriseObjectCache<EORoleResponsable> roleResponsableCache) {
    	EORoleResponsable.roleResponsableCache = roleResponsableCache;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EORoleResponsable} "SECRETAIRE"
	 */
	public static EORoleResponsable roleSecretaire(EOEditingContext edc) {
		return getRoleResponsableCache().objectForKey(edc, ROLE_SECRETAIRE_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EORoleResponsable} "ENSEIGNANT"
	 */
	public static EORoleResponsable roleEnseignant(EOEditingContext edc) {
		return getRoleResponsableCache().objectForKey(edc, ROLE_ENSEIGNANT_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EORoleResponsable} "RESPONSABLE"
	 */
	public static EORoleResponsable roleResponsable(EOEditingContext edc) {
		return getRoleResponsableCache().objectForKey(edc, ROLE_RESPONSABLE_CODE);
	}
	
	@Override
	public String toString() {
		return this.libelleLong();
	}
    
}
