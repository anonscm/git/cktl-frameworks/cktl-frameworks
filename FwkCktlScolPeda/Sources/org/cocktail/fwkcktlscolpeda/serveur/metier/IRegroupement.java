package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un regroupement.
 */
public interface IRegroupement extends IComposant {
	/**
	 * @return Le nombre d'ECTS de ce regroupement si le résultat du calcul d'ECTS des éléments enfants est <code>null</code> ou zéro.
	 */
	Integer choixECTS();
	
	/**
	 * @return Le nombre d'éléments enfants directs minimum à sélectionner, si ce regroupement est sélectionné
	 */
	Integer choixMax();
	
	/**
	 * @return Le nombre d'éléments enfants directs maximum à sélectionner, si ce regroupement est sélectionné
	 */
	Integer choixMin();

	/**
	 * @param choixMin le nombre d'éléments enfants directs minimum à sélectionner, si ce regroupement est sélectionné
	 */
	void setChoixMin(Integer choixMin);
	
	/**
	 * @param choixMax le nombre d'éléments enfants directs minimum à sélectionner, si ce regroupement est sélectionné
	 */
	void setChoixMax(Integer choixMax);
	
	/**
	 * @param nbEcts le nombre d'ECTS de ce regroupement si le résultat du calcul d'ECTS des éléments enfants est <code>null</code> ou zéro.
	 */
	void setChoixECTS(Integer nbEcts);
	
	/**
	 * @return true si c'est un regroupement type choix min max
	 */
	Boolean isChoixMinMax();
	
	/**
	 * @return true si c'est un regroupement type ects
	 */
	Boolean isChoixEcts();
	
	
}