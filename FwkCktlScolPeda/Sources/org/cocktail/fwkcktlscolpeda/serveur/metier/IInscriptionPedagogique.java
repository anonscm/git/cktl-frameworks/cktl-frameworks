package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Represente une inscription pedagogique
 * @author isabelle
 */
public interface IInscriptionPedagogique {

	public EOEditingContext editingContext();

	/**
	 * @return inscription administrative à laquelle est reliee cette inscription pedagogique
	 */
	IScoInscription toInscription();

	/**
	 * @return liste des elements d'inscriptions pedagogiques auxquels est reliee cette inscription pedagogique
	 */
	List<? extends IInscriptionPedagogiqueElement> toInscriptionPedagogiqueElements();

	/**
	 * @param value inscription liée à l'inscription pédagogique
	 */
	void setToInscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription value);

	/**
	 * @return demande de workflow associée à l'IP
	 */
	IDemande toDemande();

	/**
	 * @param value demande de workflow associée à l'IP
	 */
	void setToDemandeRelationship(IDemande value);

	/**
	 * @return Le libellé de l'état de cet IP
	 */
	String libelleEtat();

	/**
	 * @return l'identifiant de l'inscription pédagogique
	 */
	String id();

	/**
	 * @param inscription Une inscription
	 */
	void setToInscriptionRelationship(IInscription inscription);

	/**
	 * @param typeEtudiant Un type d'étudiant
	 */
	void setToTypeEtudiantRelationship(ITypeEtudiant typeEtudiant);

	/**
	 * @return true si cela concerne l'étudiant de persId <code>persIdEtudiant</code>
	 */
	boolean concerneEtudiant(int persIdEtudiant);

	public ILienConsultation toLienConsultation();

	public void setToLienConsultationRelationship(ILienConsultation value);

	/**
	 * @return <code>true</code> si l'inscription est principale (sur la version de diplome)
	 */
	Boolean principale();

	/**
	 * @param value <code>true</code> si l'inscription est principale (sur la version de diplome)
	 */
	void setPrincipale(Boolean value);

}
