package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * 
 * @author isabelle
 *
 */
public interface IExamenEtudiant {

	/**
	 * 
	 * @return si l'etudiant est convoque
	 */
	Boolean convoque();

	/**
	 * 
	 * @param value etudiant est convoque
	 */
	void setConvoque(Boolean value);

	/**
	 * 
	 * @return lien vers l'étudiant annee
	 */
	IEtudiantAnnee toEtudiantAnnee();

	/**
	 * 
	 * @param value l'étudiant annee
	 */
	void setToEtudiantAnneeRelationship(IEtudiantAnnee value);

	/**
	 * 
	 * @return l'examen
	 */
	IExamenOrganisation toExamenOrganisation();

	/**
	 * 
	 * @param value l'examen
	 */
	void setToExamenOrganistaionRelationship(IExamenOrganisation value);

}