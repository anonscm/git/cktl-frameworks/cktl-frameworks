// DO NOT EDIT.  Make changes to EODescriptifChampContenu.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODescriptifChampContenu extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DescriptifChampContenu";

  // Attribute Keys
  public static final ERXKey<Integer> DESCRIPTIF_CHAMP_TITRE_ID = new ERXKey<Integer>("descriptifChampTitreId");
  public static final ERXKey<Integer> DESCRIPTIF_ID = new ERXKey<Integer>("descriptifId");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptif");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> DESCRIPTIF_CHAMP_TITRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre>("descriptifChampTitre");

  // Attributes
  public static final String DESCRIPTIF_CHAMP_TITRE_ID_KEY = DESCRIPTIF_CHAMP_TITRE_ID.key();
  public static final String DESCRIPTIF_ID_KEY = DESCRIPTIF_ID.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String DESCRIPTIF_KEY = DESCRIPTIF.key();
  public static final String DESCRIPTIF_CHAMP_TITRE_KEY = DESCRIPTIF_CHAMP_TITRE.key();

  private static Logger LOG = Logger.getLogger(_EODescriptifChampContenu.class);

  public EODescriptifChampContenu localInstanceIn(EOEditingContext editingContext) {
    EODescriptifChampContenu localInstance = (EODescriptifChampContenu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer descriptifChampTitreId() {
    return (Integer) storedValueForKey(_EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_ID_KEY);
  }

  public void setDescriptifChampTitreId(Integer value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
    	_EODescriptifChampContenu.LOG.debug( "updating descriptifChampTitreId from " + descriptifChampTitreId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_ID_KEY);
  }

  public Integer descriptifId() {
    return (Integer) storedValueForKey(_EODescriptifChampContenu.DESCRIPTIF_ID_KEY);
  }

  public void setDescriptifId(Integer value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
    	_EODescriptifChampContenu.LOG.debug( "updating descriptifId from " + descriptifId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampContenu.DESCRIPTIF_ID_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODescriptifChampContenu.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
    	_EODescriptifChampContenu.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampContenu.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODescriptifChampContenu.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
    	_EODescriptifChampContenu.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampContenu.LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif descriptif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif)storedValueForKey(_EODescriptifChampContenu.DESCRIPTIF_KEY);
  }
  
  public void setDescriptif(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    takeStoredValueForKey(value, _EODescriptifChampContenu.DESCRIPTIF_KEY);
  }

  public void setDescriptifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
      _EODescriptifChampContenu.LOG.debug("updating descriptif from " + descriptif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDescriptif(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif oldValue = descriptif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptifChampContenu.DESCRIPTIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptifChampContenu.DESCRIPTIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre descriptifChampTitre() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre)storedValueForKey(_EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_KEY);
  }
  
  public void setDescriptifChampTitre(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre value) {
    takeStoredValueForKey(value, _EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_KEY);
  }

  public void setDescriptifChampTitreRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre value) {
    if (_EODescriptifChampContenu.LOG.isDebugEnabled()) {
      _EODescriptifChampContenu.LOG.debug("updating descriptifChampTitre from " + descriptifChampTitre() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDescriptifChampTitre(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre oldValue = descriptifChampTitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE_KEY);
    }
  }
  

  public static EODescriptifChampContenu createSco_DescriptifChampContenu(EOEditingContext editingContext, Integer id
) {
    EODescriptifChampContenu eo = (EODescriptifChampContenu) EOUtilities.createAndInsertInstance(editingContext, _EODescriptifChampContenu.ENTITY_NAME);    
		eo.setId(id);
    return eo;
  }

  public static ERXFetchSpecification<EODescriptifChampContenu> fetchSpec() {
    return new ERXFetchSpecification<EODescriptifChampContenu>(_EODescriptifChampContenu.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODescriptifChampContenu> fetchAllSco_DescriptifChampContenus(EOEditingContext editingContext) {
    return _EODescriptifChampContenu.fetchAllSco_DescriptifChampContenus(editingContext, null);
  }

  public static NSArray<EODescriptifChampContenu> fetchAllSco_DescriptifChampContenus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODescriptifChampContenu.fetchSco_DescriptifChampContenus(editingContext, null, sortOrderings);
  }

  public static NSArray<EODescriptifChampContenu> fetchSco_DescriptifChampContenus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODescriptifChampContenu> fetchSpec = new ERXFetchSpecification<EODescriptifChampContenu>(_EODescriptifChampContenu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODescriptifChampContenu> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODescriptifChampContenu fetchSco_DescriptifChampContenu(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifChampContenu.fetchSco_DescriptifChampContenu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifChampContenu fetchSco_DescriptifChampContenu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODescriptifChampContenu> eoObjects = _EODescriptifChampContenu.fetchSco_DescriptifChampContenus(editingContext, qualifier, null);
    EODescriptifChampContenu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DescriptifChampContenu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifChampContenu fetchRequiredSco_DescriptifChampContenu(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifChampContenu.fetchRequiredSco_DescriptifChampContenu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifChampContenu fetchRequiredSco_DescriptifChampContenu(EOEditingContext editingContext, EOQualifier qualifier) {
    EODescriptifChampContenu eoObject = _EODescriptifChampContenu.fetchSco_DescriptifChampContenu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DescriptifChampContenu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifChampContenu localInstanceIn(EOEditingContext editingContext, EODescriptifChampContenu eo) {
    EODescriptifChampContenu localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
