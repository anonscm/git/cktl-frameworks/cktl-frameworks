package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Type d'atome d'évaluation (Examen Oral, Controle Continu, etc..)
 */
public interface ITypeAE {

	/**
	 * @return libelle
	 */
	String libelle();

	/**
	 * @return code
	 */
	String code();
}
