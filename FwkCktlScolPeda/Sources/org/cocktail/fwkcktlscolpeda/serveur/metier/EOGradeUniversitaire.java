package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Représentation d'un grade universitaire
 */
@SuppressWarnings("serial")
public class EOGradeUniversitaire extends _EOGradeUniversitaire implements IGradeUniversitaire {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(EOGradeUniversitaire.class);
    private static ERXEnterpriseObjectCache<EOGradeUniversitaire> typeGradeUniversitaireCache;
    
    public static final String GRADE_BAC = "BAC";
    public static final String GRADE_LICENCE = "L";
    public static final String GRADE_MASTER = "M";
    public static final String GRADE_DOCTORAT = "D";
    public static final String GRADE_SANS = "SANS";
    
    /**
     * @return le cache des {@link EOGradeUniversitaire} par leur type
     */
    public static ERXEnterpriseObjectCache<EOGradeUniversitaire> getTypeGradeUniversitaireCache() {
    	if (typeGradeUniversitaireCache == null) {
    		typeGradeUniversitaireCache = new ERXEnterpriseObjectCache<EOGradeUniversitaire>(EOGradeUniversitaire.class, TYPE_KEY);
    	}
    	return typeGradeUniversitaireCache;
    }
    
    /**
     * @param typeGradeUniversitaire le cache a setter
     */
    public static void setTypeGradeUniversitaireCache(ERXEnterpriseObjectCache<EOGradeUniversitaire> typeGradeUniversitaire) {
    	EOGradeUniversitaire.typeGradeUniversitaireCache = typeGradeUniversitaire;
    }
    
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGradeUniversitaire} "Baccalauréat"
	 */
	public static EOGradeUniversitaire gradeBaccalaureat(EOEditingContext edc) {
		return getTypeGradeUniversitaireCache().objectForKey(edc, GRADE_BAC);
	}
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOGradeUniversitaire} "Licence"
	 */
	public static EOGradeUniversitaire gradeLicence(EOEditingContext edc) {
		return getTypeGradeUniversitaireCache().objectForKey(edc, GRADE_LICENCE);
	}
	
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGradeUniversitaire} "Master"
	 */
	public static EOGradeUniversitaire gradeMaster(EOEditingContext edc) {
		return getTypeGradeUniversitaireCache().objectForKey(edc, GRADE_MASTER);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGradeUniversitaire} "Doctorat"
	 */
	public static EOGradeUniversitaire gradeDoctorat(EOEditingContext edc) {
		return getTypeGradeUniversitaireCache().objectForKey(edc, GRADE_DOCTORAT);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGradeUniversitaire} "Sans"
	 */
	public static EOGradeUniversitaire gradeSans(EOEditingContext edc) {
		return getTypeGradeUniversitaireCache().objectForKey(edc, GRADE_SANS);
	}
	
    /**
	 * {@inheritDoc}
	 */
    public String toString() {
    	return this.type();
    }
    
    /**
	 * {@inheritDoc}
	 */
    public boolean isLMD() {
    	return Arrays.asList(GRADE_LICENCE, GRADE_MASTER, GRADE_DOCTORAT).contains(type());
    }

    /**
     * {@inheritDoc}
     */
	public boolean isLicence() {
		return GRADE_LICENCE.equals(type());
	}
    
    /**
     * {@inheritDoc}
     */
	public boolean isDoctorat() {
		return GRADE_DOCTORAT.equals(type());
	}
	
    /**
     * {@inheritDoc}
     */
	public boolean isBac() {
		return GRADE_BAC.equals(type());
	}
	
}
