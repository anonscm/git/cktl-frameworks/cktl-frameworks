package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Comparator;

/**
 * Définition d'une formule employé pour le calcul de la moyenne
 */
public interface IFormule {

	static Comparator<IFormule> FORMULE_ASC = new Comparator<IFormule>() {
		public int compare(IFormule formule1, IFormule formule2) {
			return ITypeFormule.TYPE_ASC.compare(
					formule1.typeFormule(),
					formule2.typeFormule());
		}
	};
	
	/**
	 * @return type de la formule
	 */
	ITypeFormule typeFormule();

	/**
	 * @param unTypeFormule Un type de formule
	 */
	void setTypeFormuleRelationship(ITypeFormule unTypeFormule);
	
	/**
	 * @return 1 si l'on doit faire une moyenne pondérée, 0 sinon
	 */
	Integer moyennePonderee();
	
	/**
	 * @param isMoyennePonderee 1 si on doit faire une moyenne pondérée, 0 sinon
	 */
	void setMoyennePonderee(Integer isMoyennePonderee);
	
	/**
	 * @return expression de la formule
	 */
	String formule();
	
	/**
	 * @param uneFormule L'expression de la formule
	 */
	void setFormule(String uneFormule);
	
	/**
	 * @return La session composant associée à cette formule
	 */
	ISessionComposant sessionComposant();
	
	/**
	 * @param uneSessionComposant Une session composant associée à cette formule
	 */
	void setSessionComposantRelationship(ISessionComposant uneSessionComposant);
}
