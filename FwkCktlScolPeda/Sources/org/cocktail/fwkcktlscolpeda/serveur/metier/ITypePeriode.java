package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Type de la période (année, semestre)
 */
public interface ITypePeriode {
	String CODE_ANNEE = "ANNEE";
	String CODE_SEMESTRE = "SEMESTRE";

	/**
	 * @return code de la période
	 */
	String code();

	/**
	 * @return libellé de la période
	 */
	String libelle();
	
	
	/**
	 * @return  id du type de periode
	 */
	Integer idTypePeriode();

}
