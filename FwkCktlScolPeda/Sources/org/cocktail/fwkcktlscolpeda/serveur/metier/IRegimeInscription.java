package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 *  régime d'inscription
 */
public interface IRegimeInscription {

	/**
	 * @return code
	 */
	String code();

	/**
	 * @param value code
	 */
	void setCode(String value);

	/**
	 * @return libellé
	 */
	String libelle();

	/**
	 * @param value libellé
	 */
	void setLibelle(String value);

}