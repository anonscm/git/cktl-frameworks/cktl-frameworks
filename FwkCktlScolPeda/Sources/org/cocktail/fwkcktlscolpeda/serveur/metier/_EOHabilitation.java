// DO NOT EDIT.  Make changes to EOHabilitation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOHabilitation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Habilitation";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_DEBUT = new ERXKey<Integer>("anneeDebut");
  public static final ERXKey<Integer> ANNEE_FIN = new ERXKey<Integer>("anneeFin");
  public static final ERXKey<String> NUMERO_HABILITATION = new ERXKey<String>("numeroHabilitation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> DIPLOMES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("diplomes");

  // Attributes
  public static final String ANNEE_DEBUT_KEY = ANNEE_DEBUT.key();
  public static final String ANNEE_FIN_KEY = ANNEE_FIN.key();
  public static final String NUMERO_HABILITATION_KEY = NUMERO_HABILITATION.key();
  // Relationships
  public static final String DIPLOMES_KEY = DIPLOMES.key();

  private static Logger LOG = Logger.getLogger(_EOHabilitation.class);

  public EOHabilitation localInstanceIn(EOEditingContext editingContext) {
    EOHabilitation localInstance = (EOHabilitation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneeDebut() {
    return (Integer) storedValueForKey(_EOHabilitation.ANNEE_DEBUT_KEY);
  }

  public void setAnneeDebut(Integer value) {
    if (_EOHabilitation.LOG.isDebugEnabled()) {
    	_EOHabilitation.LOG.debug( "updating anneeDebut from " + anneeDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOHabilitation.ANNEE_DEBUT_KEY);
  }

  public Integer anneeFin() {
    return (Integer) storedValueForKey(_EOHabilitation.ANNEE_FIN_KEY);
  }

  public void setAnneeFin(Integer value) {
    if (_EOHabilitation.LOG.isDebugEnabled()) {
    	_EOHabilitation.LOG.debug( "updating anneeFin from " + anneeFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOHabilitation.ANNEE_FIN_KEY);
  }

  public String numeroHabilitation() {
    return (String) storedValueForKey(_EOHabilitation.NUMERO_HABILITATION_KEY);
  }

  public void setNumeroHabilitation(String value) {
    if (_EOHabilitation.LOG.isDebugEnabled()) {
    	_EOHabilitation.LOG.debug( "updating numeroHabilitation from " + numeroHabilitation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOHabilitation.NUMERO_HABILITATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> diplomes() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)storedValueForKey(_EOHabilitation.DIPLOMES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> diplomes(EOQualifier qualifier) {
    return diplomes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> diplomes(EOQualifier qualifier, boolean fetch) {
    return diplomes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> diplomes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.HABILITATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.fetchSco_Diplomes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = diplomes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDiplomes(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    includeObjectIntoPropertyWithKey(object, _EOHabilitation.DIPLOMES_KEY);
  }

  public void removeFromDiplomes(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    excludeObjectFromPropertyWithKey(object, _EOHabilitation.DIPLOMES_KEY);
  }

  public void addToDiplomesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    if (_EOHabilitation.LOG.isDebugEnabled()) {
      _EOHabilitation.LOG.debug("adding " + object + " to diplomes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDiplomes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOHabilitation.DIPLOMES_KEY);
    }
  }

  public void removeFromDiplomesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    if (_EOHabilitation.LOG.isDebugEnabled()) {
      _EOHabilitation.LOG.debug("removing " + object + " from diplomes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDiplomes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOHabilitation.DIPLOMES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome createDiplomesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOHabilitation.DIPLOMES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome) eo;
  }

  public void deleteDiplomesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOHabilitation.DIPLOMES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDiplomesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> objects = diplomes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDiplomesRelationship(objects.nextElement());
    }
  }


  public static EOHabilitation createSco_Habilitation(EOEditingContext editingContext, Integer anneeDebut
, Integer anneeFin
) {
    EOHabilitation eo = (EOHabilitation) EOUtilities.createAndInsertInstance(editingContext, _EOHabilitation.ENTITY_NAME);    
		eo.setAnneeDebut(anneeDebut);
		eo.setAnneeFin(anneeFin);
    return eo;
  }

  public static ERXFetchSpecification<EOHabilitation> fetchSpec() {
    return new ERXFetchSpecification<EOHabilitation>(_EOHabilitation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOHabilitation> fetchAllSco_Habilitations(EOEditingContext editingContext) {
    return _EOHabilitation.fetchAllSco_Habilitations(editingContext, null);
  }

  public static NSArray<EOHabilitation> fetchAllSco_Habilitations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOHabilitation.fetchSco_Habilitations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOHabilitation> fetchSco_Habilitations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOHabilitation> fetchSpec = new ERXFetchSpecification<EOHabilitation>(_EOHabilitation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOHabilitation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOHabilitation fetchSco_Habilitation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHabilitation.fetchSco_Habilitation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHabilitation fetchSco_Habilitation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOHabilitation> eoObjects = _EOHabilitation.fetchSco_Habilitations(editingContext, qualifier, null);
    EOHabilitation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Habilitation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHabilitation fetchRequiredSco_Habilitation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOHabilitation.fetchRequiredSco_Habilitation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOHabilitation fetchRequiredSco_Habilitation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOHabilitation eoObject = _EOHabilitation.fetchSco_Habilitation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Habilitation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOHabilitation localInstanceIn(EOEditingContext editingContext, EOHabilitation eo) {
    EOHabilitation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
