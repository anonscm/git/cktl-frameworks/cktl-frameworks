package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * Cette interface représente un article de paiement.
 * 
 * @author Pascal MACOUIN
 */
public interface IParametragePaie {

	/**
	 * @return le libellé complet de l'article de paiement (libellé type + libellé)
	 */
	String libelleComplet();
	
	/**
	 * @return le montant de l'article de paiement
	 */
	BigDecimal montant();
	
	/**
	 * @return le plan comptable de l'article de paiement
	 */
	String comptabilite();
	
	/**
	 * @return <code>true</code> si l'article de paiement est éxonéré pour les boursiers
	 */
	Boolean boursier();
	
	/**
	 * @return <code>true</code> si l'article de paiement est remboursable
	 */
	Boolean remboursement();
}
