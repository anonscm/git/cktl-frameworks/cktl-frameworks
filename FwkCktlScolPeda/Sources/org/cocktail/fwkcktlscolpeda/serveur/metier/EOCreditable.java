package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

/**
 * Represente un "créditable"
 */
public class EOCreditable extends _EOCreditable implements ICreditable {
	private static final long serialVersionUID = 1L;
	// Private fields
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOCreditable.class);
	private final Integer intValeurDefaut = 0;
	private final BigDecimal bgValeurDefaut = new BigDecimal(0);

	/**
	 * Constructeur avec valeur par défaut pour les colonnes non nullable.
	 */
	public EOCreditable() {
		setSansNote(false);
	}
	
	// Getters and Setters

	// Public methods
	/**
	 * Suppression d'un creditable.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		try {
			if (composant() != null) {
				composant().removeFromCreditablesRelationship(this);
				setComposantRelationship(null);
			}
			if (lien() != null) {
				lien().removeFromCreditablesRelationship(this);
				setLienRelationship(null);
			}
			
			this.delete();
		} catch (Exception e) {
			System.err.println("Erreur suppression creditable : " + e.getMessage());
			throw e;
		}
	}
	
	/**
	 * @return vrai si le creditable est valide
	 */
	public boolean estValide() {
		boolean valid = valeurValideBigDecimal(creditECTS()) || valeurValideBigDecimal(travailEtudiant()) || valeurValide(baseNote());

		return valid;
	}

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		if (composant() != null) {
			return "Creditable " + primaryKey() + " du composant " + composant();
		}

		if (lien() != null) {
			return "Creditable " + primaryKey() + " du lien " + lien();
		}

		return "Creditable " + primaryKey();
	}

	/**
	 * @return copie du creditable courant
	 */
	public EOCreditable copy() {
		EOCreditable creditable = new EOCreditable();
		creditable.setBaseNote(this.baseNote());
		creditable.setCreditECTS(this.creditECTS());
		creditable.setSansNote(this.sansNote());
		creditable.setTravailEtudiant(this.travailEtudiant());
		editingContext().insertObject(creditable);
		return creditable;
	}

	// Protected methods
	// Private methods

	private boolean valeurValide(Integer value) {
		if (value == null) {
			return false;
		} else {
			return !(value == intValeurDefaut);
		}
	}

	private boolean valeurValideBigDecimal(BigDecimal value) {
		if (value == null) {
			return false;
		} else {
			return !(value == bgValeurDefaut);
		}
	}

}
