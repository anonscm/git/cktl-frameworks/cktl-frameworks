package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Cette classe représente un type de charge d'enseignement (table TYPE_CHARGE_ENSEIGNEMENT).
 */
public class EOTypeChargeEnseignement extends _EOTypeChargeEnseignement implements ITypeChargeEnseignement {
	/** Numéro de série. */
	private static final long serialVersionUID = -9153240766816161862L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeChargeEnseignement.class);
	
	/** Le cache des types de charge d'enseignement. */
    private static ERXEnterpriseObjectCache<EOTypeChargeEnseignement> cacheTypeChargeEnseignement;

	/**
     * @return le cache des {@link EOTypeChargeEnseignement} par leur code
     */
    private static ERXEnterpriseObjectCache<EOTypeChargeEnseignement> getTypeChargeEnseignementCache() {
    	if (cacheTypeChargeEnseignement == null) {
    		cacheTypeChargeEnseignement = new ERXEnterpriseObjectCache<EOTypeChargeEnseignement>(EOTypeChargeEnseignement.class, CODE_KEY);
    	}
    	return cacheTypeChargeEnseignement;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeChargeEnseignement} de type théorique ({@link EOTypeChargeEnseignement#CODE_TYPE_THEORIQUE}
	 */
	public static ITypeChargeEnseignement typeTheorique(EOEditingContext edc) {
		return getTypeChargeEnseignementCache().objectForKey(edc, ITypeChargeEnseignement.CODE_TYPE_THEORIQUE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeChargeEnseignement} de type théorique ({@link EOTypeChargeEnseignement#CODE_TYPE_PREVISIONNELLE}
	 */
	public static ITypeChargeEnseignement typePrevisionnelle(EOEditingContext edc) {
		return getTypeChargeEnseignementCache().objectForKey(edc, ITypeChargeEnseignement.CODE_TYPE_PREVISIONNELLE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeChargeEnseignement} de type pédagogique ({@link EOTypeChargeEnseignement#CODE_TYPE_PEDAGOGIQUE}
	 */
	public static ITypeChargeEnseignement typePedagogique(EOEditingContext edc) {
		return getTypeChargeEnseignementCache().objectForKey(edc, ITypeChargeEnseignement.CODE_TYPE_PEDAGOGIQUE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeChargeEnseignement} de type planifiée ({@link EOTypeChargeEnseignement#CODE_TYPE_PLANIFIEE}
	 */
	public static ITypeChargeEnseignement typePlanifiee(EOEditingContext edc) {
		return getTypeChargeEnseignementCache().objectForKey(edc, ITypeChargeEnseignement.CODE_TYPE_PLANIFIEE);
	}
}