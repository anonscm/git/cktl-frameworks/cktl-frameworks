package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.List;

/**
 *  Période d'enseignement (année ou semestre)
 */
public interface IPeriode extends IComposant {

	/**
	 * @return  <code> true </code>  si la période est une année
	 */
	Boolean isAnnee();

	/**
	 * @return  <code> true </code>  si la période est une année
	 */
	Boolean isSemestre();
	
	/**
	 * @return ordre de la période
	 */
	Integer ordre();
	
	/**
	 * @param niveau le niveau à tester
	 * @return true si la période est une année du niveau spécifié
	 */
	Boolean isAnneeDeNiveau(Integer niveau);

	/**
	 * @return le type de période
	 */
	ITypePeriode toTypePeriode();
	
	/**
	 * @return BigDecimal : le nombre de credits ECTS pour la periode
	 */
	BigDecimal getSeuilNbCreditsECTS();
	
	List<IRegimeInscription> getRegimesInscription();
	
}