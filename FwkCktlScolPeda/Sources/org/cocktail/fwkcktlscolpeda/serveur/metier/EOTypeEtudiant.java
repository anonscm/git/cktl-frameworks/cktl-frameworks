package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOTypeEtudiant extends _EOTypeEtudiant implements ITypeEtudiant {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeEtudiant.class);
	private static ERXEnterpriseObjectCache<EOTypeEtudiant> typeEtudiantCache;

	/**
	 * @return le cache des {@link EOTypeEtatIP} par leur code
	 */
	public static ERXEnterpriseObjectCache<EOTypeEtudiant> getTypeEtudiantCache() {
		if (typeEtudiantCache == null) {
			typeEtudiantCache = new ERXEnterpriseObjectCache<EOTypeEtudiant>(
					EOTypeEtudiant.class, CODE_KEY);
		}
		return typeEtudiantCache;
	}

	public static void setTypeEtudiantCache(
			ERXEnterpriseObjectCache<EOTypeEtudiant> typeEtudiantCache) {
		EOTypeEtudiant.typeEtudiantCache = typeEtudiantCache;
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "CLASSIQUE"
	 */
	public static EOTypeEtudiant typeClassique(EOEditingContext edc) {
		return getTypeEtudiantCache().objectForKey(edc, CODE_CLASSIQUE);
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "FORMATION_CONTINUE"
	 */
	public static EOTypeEtudiant typeFormationContinue(EOEditingContext edc) {
		return getTypeEtudiantCache()
				.objectForKey(edc, CODE_FORMATION_CONTINUE);
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "AUDITEUR_LIBRE"
	 */
	public static EOTypeEtudiant typeAuditeurLibre(EOEditingContext edc) {
		return getTypeEtudiantCache().objectForKey(edc, CODE_AUDITEUR_LIBRE);
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "REDOUBLANT"
	 */
	public static EOTypeEtudiant typeRedoublant(EOEditingContext edc) {
		return getTypeEtudiantCache().objectForKey(edc, CODE_REDOUBLANT);
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "CHANGEMENT_ORIENTATION"
	 */
	public static EOTypeEtudiant typeChangementOrientation(EOEditingContext edc) {
		return getTypeEtudiantCache()
				.objectForKey(edc, CODE_CHANGEMENT_ORIENTATION);
	}

	/**
	 * @param edc
	 *            l'editingContext
	 * @return le {@link EOTypeEtudiant} "ECHANGE_INTERNATIONNAL"
	 */
	public static EOTypeEtudiant typeEchangeInternational(EOEditingContext edc) {
		return getTypeEtudiantCache().objectForKey(edc, CODE_ECHANGE_INTERNATIONNAL);
	}

}
