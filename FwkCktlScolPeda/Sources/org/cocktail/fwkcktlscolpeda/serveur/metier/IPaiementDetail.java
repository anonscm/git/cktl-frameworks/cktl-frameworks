package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSTimestamp;

/**
 * Super interface pour un détail paiement qu'il soit SCO_SCOLARITE ou SCO_PRE_SCOLARITE.
 * 
 * @author Pascal MACOUIN
 */
public interface IPaiementDetail {

	/**
	 * @return le libellé complet du détail (récupéré différemment en fonction du type d'article de paiement)
	 */
	String getLibelle();
	
	/**
	 * @return date du paiement
	 */
	NSTimestamp datePaye();

	/**
	 * @param value date du paiement
	 */
	void setDatePaye(NSTimestamp value);

	/**
	 * @return date du remboursement
	 */
	NSTimestamp dateRembourse();

	/**
	 * @param value date du remboursement
	 */
	void setDateRembourse(NSTimestamp value);

	/**
	 * @return montant à payer
	 */
	BigDecimal montantAPayer();

	/**
	 * @param value montant à payer
	 */
	void setMontantAPayer(BigDecimal value);

	/**
	 * @return montant à rembourser
	 */
	BigDecimal montantARembourser();

	/**
	 * @param value montant à rembourser
	 */
	void setMontantARembourser(BigDecimal value);

	/**
	 * @return montant payé
	 */
	BigDecimal montantPaye();

	/**
	 * @param value montant payé
	 */
	void setMontantPaye(BigDecimal value);

	/**
	 * @return montant remboursé
	 */
	BigDecimal montantRembourse();

	/**
	 * @param value montant remboursé
	 */
	void setMontantRembourse(BigDecimal value);

	/**
	 * @return  paiement relatif à ce détail de paiement
	 */
	IPaiement toPaiement();

	/**
	 * @return lien vers le parametrage de paiement de l'article
	 */
	IParametragePaie toParametragePaie();
	
	/**
	 * @param value paiement relatif à ce détail de paiement
	 */
	void setToPaiementRelationship(IPaiement value);

	/**
	 * @return paramétrage d'un paiement d'article complémentaire
	 */
	IParametragePaieArticleComplementaire toParametragePaieArticleComplementaire();

	/**
	 * @param value  paramétrage d'un paiement d'article complémentaire
	 */
	void setToParametragePaieArticleComplementaireRelationship(IParametragePaieArticleComplementaire value);

	/**
	 * @return  paramétrage d'un paiement d'une formation
	 */
	IParametragePaieFormation toParametragePaieFormation();

	/**
	 * @param value  paramétrage d'un paiement d'une formation
	 */
	void setToParametragePaieFormationRelationship(IParametragePaieFormation value);

	/**
	 * @return  paramétrage d'un paiement d'un diplôme/parcours
	 */
	IParametragePaieDiplome toParametragePaieDiplome();

	/**
	 * @param value  paramétrage d'un paiement d'un diplôme/parcours
	 */
	void setToParametragePaieDiplomeRelationship(IParametragePaieDiplome value);

	/**
	 * @return Indique si le détail de paiement a été inséré automatiquement ou  manuellement
	 */
	Boolean auto();

	/**
	 * @param value Indique si le détail de paiement a été inséré automatiquement ou  manuellement 
	 */
	void setAuto(Boolean value);

	// ***************
	// Données d'audit
	// ***************
	/**
	 * @return date de creation de l'enregistrement
	 */
	NSTimestamp dCreation();

	/**
	 * @param value date de creation de l'enregistrement
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return date de modification du paiement
	 */
	NSTimestamp dModification();

	/**
	 * @param value date de modification du paiement
	 */
	void setDModification(NSTimestamp value);

	/**
	 * @return personne qui a créé l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return personne qui a modifié l'enregistrement
	 */
	Integer persIdModification();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdModification(Integer value);
}
