// DO NOT EDIT.  Make changes to EORegleLiaison.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORegleLiaison extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_RegleLiaison";

  // Attribute Keys
  public static final ERXKey<Integer> ALLOWED = new ERXKey<Integer>("allowed");
  public static final ERXKey<Integer> FORBIDDEN = new ERXKey<Integer>("forbidden");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> VISIBLE = new ERXKey<Integer>("visible");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> ALLOWED_CHILD = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("allowedChild");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> ALLOWED_PARENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("allowedParent");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien> TYPE_LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien>("typeLien");

  // Attributes
  public static final String ALLOWED_KEY = ALLOWED.key();
  public static final String FORBIDDEN_KEY = FORBIDDEN.key();
  public static final String ID_KEY = ID.key();
  public static final String VISIBLE_KEY = VISIBLE.key();
  // Relationships
  public static final String ALLOWED_CHILD_KEY = ALLOWED_CHILD.key();
  public static final String ALLOWED_PARENT_KEY = ALLOWED_PARENT.key();
  public static final String TYPE_LIEN_KEY = TYPE_LIEN.key();

  private static Logger LOG = Logger.getLogger(_EORegleLiaison.class);

  public EORegleLiaison localInstanceIn(EOEditingContext editingContext) {
    EORegleLiaison localInstance = (EORegleLiaison)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer allowed() {
    return (Integer) storedValueForKey(_EORegleLiaison.ALLOWED_KEY);
  }

  public void setAllowed(Integer value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
    	_EORegleLiaison.LOG.debug( "updating allowed from " + allowed() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegleLiaison.ALLOWED_KEY);
  }

  public Integer forbidden() {
    return (Integer) storedValueForKey(_EORegleLiaison.FORBIDDEN_KEY);
  }

  public void setForbidden(Integer value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
    	_EORegleLiaison.LOG.debug( "updating forbidden from " + forbidden() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegleLiaison.FORBIDDEN_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EORegleLiaison.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
    	_EORegleLiaison.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegleLiaison.ID_KEY);
  }

  public Integer visible() {
    return (Integer) storedValueForKey(_EORegleLiaison.VISIBLE_KEY);
  }

  public void setVisible(Integer value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
    	_EORegleLiaison.LOG.debug( "updating visible from " + visible() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegleLiaison.VISIBLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant allowedChild() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant)storedValueForKey(_EORegleLiaison.ALLOWED_CHILD_KEY);
  }
  
  public void setAllowedChild(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    takeStoredValueForKey(value, _EORegleLiaison.ALLOWED_CHILD_KEY);
  }

  public void setAllowedChildRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
      _EORegleLiaison.LOG.debug("updating allowedChild from " + allowedChild() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setAllowedChild(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant oldValue = allowedChild();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORegleLiaison.ALLOWED_CHILD_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORegleLiaison.ALLOWED_CHILD_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant allowedParent() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant)storedValueForKey(_EORegleLiaison.ALLOWED_PARENT_KEY);
  }
  
  public void setAllowedParent(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    takeStoredValueForKey(value, _EORegleLiaison.ALLOWED_PARENT_KEY);
  }

  public void setAllowedParentRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
      _EORegleLiaison.LOG.debug("updating allowedParent from " + allowedParent() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setAllowedParent(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant oldValue = allowedParent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORegleLiaison.ALLOWED_PARENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORegleLiaison.ALLOWED_PARENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien typeLien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien)storedValueForKey(_EORegleLiaison.TYPE_LIEN_KEY);
  }
  
  public void setTypeLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien value) {
    takeStoredValueForKey(value, _EORegleLiaison.TYPE_LIEN_KEY);
  }

  public void setTypeLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien value) {
    if (_EORegleLiaison.LOG.isDebugEnabled()) {
      _EORegleLiaison.LOG.debug("updating typeLien from " + typeLien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien oldValue = typeLien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORegleLiaison.TYPE_LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORegleLiaison.TYPE_LIEN_KEY);
    }
  }
  

  public static EORegleLiaison createSco_RegleLiaison(EOEditingContext editingContext, Integer allowed
, Integer forbidden
, Integer id
, Integer visible
) {
    EORegleLiaison eo = (EORegleLiaison) EOUtilities.createAndInsertInstance(editingContext, _EORegleLiaison.ENTITY_NAME);    
		eo.setAllowed(allowed);
		eo.setForbidden(forbidden);
		eo.setId(id);
		eo.setVisible(visible);
    return eo;
  }

  public static ERXFetchSpecification<EORegleLiaison> fetchSpec() {
    return new ERXFetchSpecification<EORegleLiaison>(_EORegleLiaison.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORegleLiaison> fetchAllSco_RegleLiaisons(EOEditingContext editingContext) {
    return _EORegleLiaison.fetchAllSco_RegleLiaisons(editingContext, null);
  }

  public static NSArray<EORegleLiaison> fetchAllSco_RegleLiaisons(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORegleLiaison.fetchSco_RegleLiaisons(editingContext, null, sortOrderings);
  }

  public static NSArray<EORegleLiaison> fetchSco_RegleLiaisons(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORegleLiaison> fetchSpec = new ERXFetchSpecification<EORegleLiaison>(_EORegleLiaison.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORegleLiaison> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORegleLiaison fetchSco_RegleLiaison(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegleLiaison.fetchSco_RegleLiaison(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegleLiaison fetchSco_RegleLiaison(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORegleLiaison> eoObjects = _EORegleLiaison.fetchSco_RegleLiaisons(editingContext, qualifier, null);
    EORegleLiaison eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_RegleLiaison that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegleLiaison fetchRequiredSco_RegleLiaison(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegleLiaison.fetchRequiredSco_RegleLiaison(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegleLiaison fetchRequiredSco_RegleLiaison(EOEditingContext editingContext, EOQualifier qualifier) {
    EORegleLiaison eoObject = _EORegleLiaison.fetchSco_RegleLiaison(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_RegleLiaison that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegleLiaison localInstanceIn(EOEditingContext editingContext, EORegleLiaison eo) {
    EORegleLiaison localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
