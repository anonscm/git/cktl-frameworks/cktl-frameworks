package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Cette classe représente un champ de l'interface graphique Aujourd'hui seuls les champs de Coriandre sont geres
 */
public class EOChamp extends _EOChamp {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOChamp.class);

	private static ERXEnterpriseObjectCache<EOChamp> champCache;

	/**
	 * @return un champ cache
	 */
	public static ERXEnterpriseObjectCache<EOChamp> getChampCache() {
		if (champCache == null) {
			champCache = new ERXEnterpriseObjectCache<EOChamp>(EOChamp.class, CODE_KEY);
		}
		return champCache;
	}

	/**
	 * @param champCache : le champ cache à mettre à jour
	 */
	public static void setChampCache(ERXEnterpriseObjectCache<EOChamp> champCache) {
		EOChamp.champCache = champCache;
	}

	/**
	 * @param ec : contexte d'edition
	 * @param codeChamp le code du champ recherche
	 * @return le champ recherche
	 */
	public static EOChamp champForCode(EOEditingContext ec, String codeChamp) {
		return getChampCache().objectForKey(ec, codeChamp);
	}
}
