// DO NOT EDIT.  Make changes to EOModeAP.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOModeAP extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ModeAP";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOModeAP.class);

  public EOModeAP localInstanceIn(EOEditingContext editingContext) {
    EOModeAP localInstance = (EOModeAP)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOModeAP.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOModeAP.LOG.isDebugEnabled()) {
    	_EOModeAP.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModeAP.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOModeAP.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOModeAP.LOG.isDebugEnabled()) {
    	_EOModeAP.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModeAP.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOModeAP.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOModeAP.LOG.isDebugEnabled()) {
    	_EOModeAP.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModeAP.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOModeAP.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOModeAP.LOG.isDebugEnabled()) {
    	_EOModeAP.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModeAP.READ_ONLY_KEY);
  }


  public static EOModeAP createSco_ModeAP(EOEditingContext editingContext, String code
, Integer id
, String libelle
, Integer readOnly
) {
    EOModeAP eo = (EOModeAP) EOUtilities.createAndInsertInstance(editingContext, _EOModeAP.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EOModeAP> fetchSpec() {
    return new ERXFetchSpecification<EOModeAP>(_EOModeAP.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOModeAP> fetchAllSco_ModeAPs(EOEditingContext editingContext) {
    return _EOModeAP.fetchAllSco_ModeAPs(editingContext, null);
  }

  public static NSArray<EOModeAP> fetchAllSco_ModeAPs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOModeAP.fetchSco_ModeAPs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOModeAP> fetchSco_ModeAPs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOModeAP> fetchSpec = new ERXFetchSpecification<EOModeAP>(_EOModeAP.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOModeAP> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOModeAP fetchSco_ModeAP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModeAP.fetchSco_ModeAP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOModeAP fetchSco_ModeAP(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOModeAP> eoObjects = _EOModeAP.fetchSco_ModeAPs(editingContext, qualifier, null);
    EOModeAP eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ModeAP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOModeAP fetchRequiredSco_ModeAP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModeAP.fetchRequiredSco_ModeAP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOModeAP fetchRequiredSco_ModeAP(EOEditingContext editingContext, EOQualifier qualifier) {
    EOModeAP eoObject = _EOModeAP.fetchSco_ModeAP(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ModeAP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOModeAP localInstanceIn(EOEditingContext editingContext, EOModeAP eo) {
    EOModeAP localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
