// DO NOT EDIT.  Make changes to EODomaine.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODomaine extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Domaine";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORIGINE_CREATION = new ERXKey<Integer>("origineCreation");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORIGINE_CREATION_KEY = ORIGINE_CREATION.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EODomaine.class);

  public EODomaine localInstanceIn(EOEditingContext editingContext) {
    EODomaine localInstance = (EODomaine)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EODomaine.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EODomaine.LOG.isDebugEnabled()) {
    	_EODomaine.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaine.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODomaine.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODomaine.LOG.isDebugEnabled()) {
    	_EODomaine.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaine.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODomaine.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODomaine.LOG.isDebugEnabled()) {
    	_EODomaine.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaine.LIBELLE_KEY);
  }

  public Integer origineCreation() {
    return (Integer) storedValueForKey(_EODomaine.ORIGINE_CREATION_KEY);
  }

  public void setOrigineCreation(Integer value) {
    if (_EODomaine.LOG.isDebugEnabled()) {
    	_EODomaine.LOG.debug( "updating origineCreation from " + origineCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaine.ORIGINE_CREATION_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EODomaine.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EODomaine.LOG.isDebugEnabled()) {
    	_EODomaine.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaine.READ_ONLY_KEY);
  }


  public static EODomaine createSco_Domaine(EOEditingContext editingContext, String code
, Integer id
, String libelle
, Integer readOnly
) {
    EODomaine eo = (EODomaine) EOUtilities.createAndInsertInstance(editingContext, _EODomaine.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EODomaine> fetchSpec() {
    return new ERXFetchSpecification<EODomaine>(_EODomaine.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODomaine> fetchAllSco_Domaines(EOEditingContext editingContext) {
    return _EODomaine.fetchAllSco_Domaines(editingContext, null);
  }

  public static NSArray<EODomaine> fetchAllSco_Domaines(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODomaine.fetchSco_Domaines(editingContext, null, sortOrderings);
  }

  public static NSArray<EODomaine> fetchSco_Domaines(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODomaine> fetchSpec = new ERXFetchSpecification<EODomaine>(_EODomaine.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODomaine> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODomaine fetchSco_Domaine(EOEditingContext editingContext, String keyName, Object value) {
    return _EODomaine.fetchSco_Domaine(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODomaine fetchSco_Domaine(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODomaine> eoObjects = _EODomaine.fetchSco_Domaines(editingContext, qualifier, null);
    EODomaine eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Domaine that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODomaine fetchRequiredSco_Domaine(EOEditingContext editingContext, String keyName, Object value) {
    return _EODomaine.fetchRequiredSco_Domaine(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODomaine fetchRequiredSco_Domaine(EOEditingContext editingContext, EOQualifier qualifier) {
    EODomaine eoObject = _EODomaine.fetchSco_Domaine(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Domaine that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODomaine localInstanceIn(EOEditingContext editingContext, EODomaine eo) {
    EODomaine localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
