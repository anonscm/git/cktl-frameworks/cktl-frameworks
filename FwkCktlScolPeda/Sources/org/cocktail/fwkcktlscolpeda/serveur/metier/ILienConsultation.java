package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

/**
 *	Lien qui étend un lien composer uniquement pour la consultation
 */
public interface ILienConsultation extends ILienComposer {
	
	String ID_LIEN_CONTEXTE_SEPERATOR = "/";
	
	/**
	 * @return le diplome auquel appartient le lien
	 */
	Integer idDiplome();
	/**
	 * @return l'id du lien
	 */
	Integer idLien();
	/**
	 * @return un id de type /25/45/42/89
	 */
	String idLienContexte();
	/**
	 * @return un id de type /25/45/42
	 */
	String idLienContexteParent();
	/**
	 * @return l'id du lien parent
	 */
	Integer idLienParent();
	/**
	 * @return l'id du lien diplome / version diplome
	 */
	Integer idLienRoot();
	/**
	 * @return l'id de la version de diplome
	 */
	Integer idVersionDiplome();
	/**
	 * @return le libelle du composant fils
	 */
	String llComposantChild();
	/**
	 * @return le libelle du composant parent
	 */
	String llComposantParent();
	/**
	 * @return le libelle child ou parent selon les cas (surtout pour les versions de diplome)
	 */
	String libelle();
	/**
	 * @return le niveau dans l'arboresence
	 */
	Integer niveau();
	/**
	 * @return l'id du type du composant child
	 */
	String typeComposantNomChild();
	/**
	 * @return l'id du type du composant parent
	 */
	String typeComposantNomParent();
	/**
	 * @return la liste des enfants
	 */
	List<ILienConsultation> getChildren();
	/**
	 * @param children la nouvelle liste des enfants
	 */
	void setChildren(List<ILienConsultation> children);
	/**
	 * @return renvoi le lien parent
	 */
	ILienConsultation getParent();
	/**
	 * @param parent le nouveau parent
	 */
	void setParent(ILienConsultation parent);
	
	/**
	 * @return le composant enfant est un parcours
	 */
	Boolean isChildParcours();
	/**
	 * @return le composant enfant est une UE
	 */
	Boolean isChildUE();
	/**
	 * @return le composant enfant est un EC
	 */
	Boolean isChildEC();
	/**
	 * @return le composant enfant est un regroupement
	 */
	Boolean isChildRegroupement();
	/**
	 * @return le composant enfant est un AE
	 */
	Boolean isChildAE();
	/**
	 * @return le composant enfant est un AP
	 */
	Boolean isChildAP();
	/**
	 * @return le composant enfant est une Période
	 */
	Boolean isChildPeriode();
	/**
	* @return id du type de période
	 */
	Integer typePeriodeId();
	/**
	 * @return le regroupement est de choix min max
	 */
	Boolean isChoixMinMax();
	/**
	 * @return le regroupement est de choix ects
	 */
	Boolean isChoixEcts();
	/**
	 * @return le choix max
	 */
	Integer choixMax();
	/**
	 * @return le choix min
	 */
	Integer choixMin();
	
}
