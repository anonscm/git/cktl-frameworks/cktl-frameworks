package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOLangue;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.EOScolpedaCommonDefinitions;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IAnneeProvider;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.Validateur;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationFactory;

/**
 * La classe EOComposant decrit et maintient un composant.
 * @author lmo
 */
@SuppressWarnings("serial")
public abstract class EOComposant extends _EOComposant implements ISupprimable, IComposant, IEnregistrementInfo {
	// Private fields
	@Inject
	private Validateur validateur;

	@Inject
	private CreditableService creditableService;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOComposant.class);

	public static final String PARENTS_KEY = "parents";
	public static final ERXKey<NSArray<EOComposant>> PARENTS = new ERXKey<NSArray<EOComposant>>(PARENTS_KEY);

	public static final String CHILDS_KEY = "childs";
	public static final ERXKey<NSArray<EOComposant>> CHILDS = new ERXKey<NSArray<EOComposant>>(CHILDS_KEY);

	public static final String ALLANCESTORS_KEY = "allAncestors";
	public static final ERXKey<NSArray<EOComposant>> ALLANCESTORS = new ERXKey<NSArray<EOComposant>>(ALLANCESTORS_KEY);

	public static final String ALLSUCCESSORS_KEY = "allSuccessors";
	public static final ERXKey<NSArray<EOComposant>> ALLSUCCESSORS = new ERXKey<NSArray<EOComposant>>(ALLSUCCESSORS_KEY);

	public static final String ALLLIENSANCESTORS_KEY = "allLiensAncestors";
	public static final ERXKey<NSArray<EOLien>> ALLLIENSANCESTORS = new ERXKey<NSArray<EOLien>>(ALLLIENSANCESTORS_KEY);

	public static final String ALLLIENSSUCCESSORS_KEY = "allLiensSuccessors";
	public static final ERXKey<NSArray<EOLien>> ALLLIENSSUCCESSORS = new ERXKey<NSArray<EOLien>>(ALLLIENSSUCCESSORS_KEY);

	public static final String CREDITABLE_KEY = "creditable";
	public static final ERXKey<EOCreditable> CREDITABLE = new ERXKey<EOCreditable>(CREDITABLE_KEY);

	public static final String VERSIONSDIPLOMES_KEY = "versionsDiplomes";
	public static final ERXKey<NSArray<EOVersionDiplome>> VERSIONSDIPLOMES = new ERXKey<NSArray<EOVersionDiplome>>(VERSIONSDIPLOMES_KEY);

	public static final String ANNEES_KEY = "annees";
	public static final ERXKey<NSArray<Integer>> ANNEES = new ERXKey<NSArray<Integer>>(ANNEES_KEY);

	public static final String IMPLANTATION_GEO_KEY = "implantationGeo";
	public static final ERXKey<EOImplantationGeo> IMPLANTATION_GEO = new ERXKey<EOImplantationGeo>("implantationGeo");
	
	public static final String STRUCTURE_KEY = "structure";
	public static final ERXKey<EOStructure> STRUCTURE = new ERXKey<EOStructure>(STRUCTURE_KEY);
	
	public static final String DISCIPLINE_KEY = "discipline";
	public static final ERXKey<EODiscipline> DISCIPLINE = new ERXKey<EODiscipline>(DISCIPLINE_KEY);
	
	@Inject
	@Nullable
	private IAnneeProvider anneeProvider;

	// Properties
	public Integer getAnneeExercice() {
		Integer annee = null;
		if (anneeProvider != null) {
			annee = anneeProvider.getAnneeExercice();
		}
		return annee;
	}

	public void setAnneeExercice(Integer annee) {
		if (anneeProvider != null) {
			this.anneeProvider.setAnneeExercice(annee);
		}
	}

	public void setValidateur(Validateur validateur) {
		this.validateur = validateur;
	}

	private Validateur getValidateur() {
		return validateur;
	}

	public void setCreditableService(CreditableService creditableService) {
		this.creditableService = creditableService;
	}

	public CreditableService getCreditableService() {
		return creditableService;
	}

	/**
	 * @return le type de composant
	 */
	public abstract EOTypeComposant getTypeComposant();

	/**
	 * @return true si le composant a des parents.
	 */
	public boolean haveParents() {
		return liensChilds().size() > 0;
	}

	/**
	 * @return true si le composant a des enfants.
	 */
	public boolean haveChilds() {
		return liensParents().size() > 0;
	}

	@Override
	public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensParents(EOQualifier qualifier) {
		return ERXArrayUtilities.arrayWithoutDuplicates(super.liensParents(qualifier, null, false));
	}
	
/**
 * {@inheritDoc}
 */
	public NSArray<EOLien> liensChildsWithoutDuplicate() {
		return ERXArrayUtilities.arrayWithoutDuplicates(super.liensChilds());
	}

/**
 * {@inheritDoc}
 */
	public NSArray<EOLien> liensParentsWithoutDuplicate() {
		return ERXArrayUtilities.arrayWithoutDuplicates(super.liensParents());
	}
	// Public Methods
	/**
	 * {@inheritDoc}
	 */
	public void initialise() {
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal valeurEnHeures(Boolean dureeMax) {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public void actualiseInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isVisibleEnConstruction(Integer annee) {
		// pour pouvoir prendre en compte les composants qui ne sont pas encore dans un diplome
		if (versionsDiplomes().count() == 0) {
			return true;
		}

		Boolean enConstruction = false;
		for (IVersionDiplome versionDiplome : versionsDiplomes()) {
			if (versionDiplome.annee().equals(annee)) {
				if (versionDiplome.getDiplome().statutFormation() != null
				    && versionDiplome.getDiplome().statutFormation().equals(EOStatutFormation.statutEnCoursConstruction(editingContext()))) {
					enConstruction = true;
					break;
				}
			}

		}

		return enConstruction;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isVisibleEnProduction(Integer annee) {
		if (versionsDiplomes().count() == 0) {
			return true;
		}

		Boolean enProduction = false;

		for (IVersionDiplome versionDiplome : versionsDiplomes()) {
			if (versionDiplome.annee().equals(annee)) {
				if (!versionDiplome.getDiplome().isHabilitationRequise()) {
					enProduction = true;
					break;
				} else {
					if ((versionDiplome.getDiplome().statutFormation() != null)
					    && (versionDiplome.getDiplome().statutFormation().equals(EOStatutFormation.statutEnProduction(editingContext())))) {
						enProduction = true;
						break;
					}

				}
			}
		}

		return enProduction;
	}

	@Override
	public String toString() {
		return this.code() + " - " + this.libelle();
	}

	/**
	 * @return displayed string for treeView
	 */
	public String displayedString() {
		return String.format("%s", this.libelle());
	}

	/**
	 * {@inheritDoc}
	 */
	public NSArray<IVersionDiplome> versionsDiplomes() {
		NSArray<EOComposant> composants = allAncestors();
		NSArray<IVersionDiplome> versionsDiplomes = new NSMutableArray<IVersionDiplome>();

		for (EOComposant composant : composants) {
			if (composant.typeComposant().equals(EOTypeComposant.typeVersionDiplome(editingContext()))) {
				versionsDiplomes.add((EOVersionDiplome) composant);
			}
		}

		return versionsDiplomes;
	}

	/**
	 * {@inheritDoc}
	 */
	public IVersionDiplome getVersionDiplome(Integer annee){
		return null;
	}
	/**
	 * {@inheritDoc}
	 */
	public NSArray<Integer> annees() {
		NSArray<Integer> annees = new NSMutableArray<Integer>();

		for (IVersionDiplome versionDiplome : versionsDiplomes()) {
			annees.add(versionDiplome.annee());
		}

		return ERXArrayUtilities.arrayWithoutDuplicates(annees);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> parents() {
		return parents(null, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> parents(EOTypeLien typeLien) {
		return parents(typeLien, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> parents(ITypeComposant typeComposant) {
		return parents(null, typeComposant, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> parents(ITypeLien typeLien, ITypeComposant typeComposant, ERXSortOrderings sortOrderings) {
		EOQualifier qualifier = null;
		if (typeLien != null) {
			qualifier = ERXQ.and(qualifier, ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLien));
		}
		if (typeComposant != null) {
			qualifier = ERXQ.and(qualifier, EOLien.PARENT.dot(EOComposant.TYPE_COMPOSANT).eq((EOTypeComposant) typeComposant));
		}
		@SuppressWarnings("unchecked")
		NSArray<EOComposant> parents = ERXArrayUtilities.arrayWithoutDuplicates((NSArray<EOComposant>) liensChilds(qualifier, sortOrderings, false).valueForKey(
		    EOLien.PARENT_KEY));

		return new NSArray<IComposant>(parents);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> childs() {
		return childs(null, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> childs(EOTypeLien typeLien) {
		return childs(typeLien, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> childs(ITypeComposant typeComposant) {
		return childs(null, typeComposant, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IComposant> childs(ITypeLien typeLien, ITypeComposant typeComposant, ERXSortOrderings sortOrderings) {
		EOQualifier qualifier = null;
		if (typeLien != null) {
			qualifier = ERXQ.and(qualifier, ERXQ.equals(EOLien.TYPE_LIEN_KEY, typeLien));
		}
		if (typeComposant != null) {
			qualifier = ERXQ.and(qualifier, EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq((EOTypeComposant) typeComposant));
		}
		@SuppressWarnings("unchecked")
		NSArray<EOComposant> childs = ERXArrayUtilities.arrayWithoutDuplicates((NSArray<EOComposant>) liensParents(qualifier, sortOrderings, false).valueForKey(
		    EOLien.CHILD_KEY));

		return new NSArray<IComposant>(childs);
	}

	/**
	 * Renvoie les enfants pour un type de lien.
	 * @param typeLien : type de lien recherche.
	 * @return un tableau de lien.
	 */
	public NSArray<EOLien> liensAvecLesComposantsEnfants(EOTypeLien typeLien) {

		// ne pas oublier de filter les doublons
		// liensParents: liens pour lesquels le composant est un parent.
		return NSArrayCtrl.removeDuplicate(liensParents(EOLien.TYPE_LIEN.eq(typeLien), EOScolpedaCommonDefinitions.defaultsortOrderings(EOLien.ORDRE_KEY), false));
	}

	/**
	 * Renvoie les enfants pour un type de composant.
	 * @param typeComposant : type de composant recherche.
	 * @return un tableau de lien.
	 */
	public NSArray<EOLien> liensAvecLesComposantsEnfants(EOTypeComposant typeComposant) {

		// ne pas oublier de filter les doublons
		// liensParents: liens pour lesquels le composant est un parent.
		return NSArrayCtrl.removeDuplicate(liensParents(EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq(typeComposant),
		    EOScolpedaCommonDefinitions.defaultsortOrderings(EOLien.ID_KEY), false));
	}

	/**
	 * renvoie les enfants pour un type de composant.
	 * @param typeLien : type de lien
	 * @param typeComposant : type de composant recherche.
	 * @return un tableau de lien.
	 */
	public NSArray<EOLien> liensAvecLesComposantsEnfants(EOTypeLien typeLien, EOTypeComposant typeComposant) {

		// ne pas oublier de filter les doublons
		// liensParents: liens pour lesquels le composant est un parent.
		return NSArrayCtrl.removeDuplicate(liensParents(
		    EOLien.TYPE_LIEN.eq(EOTypeLien.typeComposer(editingContext())).and(EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq(typeComposant)),
		    EOScolpedaCommonDefinitions.defaultsortOrderings(EOLien.ID_KEY), false));
	}

	public NSArray<EOLien> allLiens() {
		NSArray<EOLien> liens = new NSMutableArray<EOLien>();

		liens(this, liens);

		return liens;
	}

	/**
	 * Renvoi tous les enfants pour un type de composant et un type de lien donne
	 * @param typeLien : type de lien
	 * @param typeComposant : type de composant
	 * @return : tableaux de liens
	 */
	public NSArray<EOLien> allLiens(EOTypeLien typeLien, EOTypeComposant typeComposant) {
		NSArray<EOLien> liens = new NSMutableArray<EOLien>();

		liens(this, liens, typeLien, typeComposant);

		return liens;
	}

	private void liens(EOComposant composant, NSArray<EOLien> liens, EOTypeLien typeLien, EOTypeComposant typeComposant) {
		for (EOLien lien : composant.liensAvecLesComposantsEnfants(typeLien)) {
			if (lien.child().typeComposant().equals(typeComposant)) {
				liens.add(lien);
			}

			composant.liens(lien.child(), liens, typeLien, typeComposant);
		}
	}

	private void liens(EOComposant composant, NSArray<EOLien> liens) {
		List<EOLien> liensAvecFils = composant.liensParents();
		for (EOLien lien : liensAvecFils) {
			liens.add(lien);
			composant.liens(lien.child(), liens);
		}
	}

	/**
	 * Renvoie les liens de type "composer"
	 * @return un tableau de lien
	 */
	public NSArray<EOLienComposer> liensComposerAvecLesComposantsEnfants() {
		NSArray<EOLienComposer> liensComposerChilds = new NSMutableArray<EOLienComposer>();

		for (EOLien lien : liensAvecLesComposantsEnfants(EOTypeLien.typeComposer(editingContext()))) {
			if (lien.getClass().equals(EOLienComposer.class)) {
				EOLienComposer lienComposer = (EOLienComposer) lien;
				liensComposerChilds.add(lienComposer);
			}
		}
		return ERXArrayUtilities.arrayWithoutDuplicates(liensComposerChilds);
	}

	/**
	 * Renvoie les liens de type "composer" pour un type de composant donne
	 * @param typeComposant type de composant
	 * @return un tableau de lien
	 */
	public NSArray<EOLienComposer> liensComposerAvecLesComposantsEnfants(EOTypeComposant typeComposant) {
		NSArray<EOLienComposer> liensComposerChilds = new NSMutableArray<EOLienComposer>();

		NSArray<EOLien> liens = NSArrayCtrl.removeDuplicate(liensParents(
		    EOLien.TYPE_LIEN.eq(EOTypeLien.typeComposer(editingContext())).and(EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq(typeComposant)),
		    EOScolpedaCommonDefinitions.defaultsortOrderings(EOLien.ID_KEY), false));

		for (EOLien lien : liens) {
			EOLienComposer lienComposer = (EOLienComposer) lien;
			if (lienComposer != null) {
				liensComposerChilds.add(lienComposer);
			}
		}

		return liensComposerChilds;
	}

	/**
	 * @param edc l'edc
	 * @param id l'id du composant à fetcher
	 * @return le composant correspondant à l'id
	 */
	public static IComposant fetchById(EOEditingContext edc, Integer id) {
		return fetchSco_Composant(edc, ID.eq(id));
	}
	
	/**
	 * Renvoie l'ensemble des composants d'un type ne descendant pas deja du composant parent.
	 * @param editingContext : contexte d'edition.
	 * @param composantParent : composant parent.
	 * @param typeComposant : type de composant que l'on souhaite selectionner.
	 * @return liste des composant sans lien de parente avec le composant parent.
	 */
	public static NSArray<EOComposant> nonDescendantdeParent(EOEditingContext editingContext, EOComposant composantParent, EOTypeComposant typeComposant) {
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOComposant.CODE_KEY, EOSortOrdering.CompareAscending));

		NSArray<EOComposant> composants = (NSMutableArray<EOComposant>) EOComposant.fetchSco_Composants(editingContext,
		    ERXQ.equals(EOComposant.TYPE_COMPOSANT_KEY, typeComposant), sortOrderings);
		composants.remove(composantParent);

		List<IComposant> childs = composantParent.childs(typeComposant);

		for (IComposant composant : childs) {
			composants.remove((EOComposant) composant);
		}

		return composants;
	}

	/**
	 * @return tous les ascendants du composant.
	 */
	public NSArray<EOComposant> allAncestors() {
		NSMutableArray<EOComposant> allAncestors = new NSMutableArray<EOComposant>();

		ancestors(this, allAncestors);

		return NSArrayCtrl.removeDuplicate(allAncestors);
	}

	/**
	 * @return tous les successeurs du composant.
	 */
	public NSArray<EOComposant> allSuccessors() {
		NSMutableArray<EOComposant> allSuccessors = new NSMutableArray<EOComposant>();

		successors(this, allSuccessors);

		return NSArrayCtrl.removeDuplicate(allSuccessors);
	}

	/**
	 * remplace le parent par un nouveau parent.
	 * @param edc : contexte d'edition
	 * @param parent : parent actuel
	 * @param nouveauParent : nouveau parent
	 */
	public void remplacerParent(EOEditingContext edc, EOComposant parent, EOComposant nouveauParent) {
		if ((parent != null) && (nouveauParent != null)) {
			EOLien lien = EOLien.fetchSco_Lien(edc, EOLien.CHILD.eq(this).and(EOLien.PARENT.eq(parent)));
			EOTypeLien typeLien = lien.typeLien();
			edc.deleteObject(lien);
			EOComposantFactory.createLien(nouveauParent, this, typeLien);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean peutEtresupprime() throws Exception {
		if (composantsSuivants()!=null && composantsSuivants().size() > 0) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Composant.nePeutEtreSupprime.DejaVersionne");
		}

		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Boolean avantSuppression() throws Exception {
		return true;
	}
	
	/**
	 * Suppression d'un composant.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		if (!peutEtresupprime()) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Composant.nePeutEtreSupprime");
		}
		try {
			if (avantSuppression()) {
				supprimerInfoVersion();
				supprimerComposantOrigine();
				supprimerComposantsSuivants();
				supprimerCreditables();
				supprimerDescriptifs();
				supprimerDisciplines();
				supprimerDomaines();
				supprimerImplantationsGeo();
				supprimerLangues();
				supprimerLiensEnfants();
				supprimerLiensParents();
				supprimerResponsables();
				supprimerSessions();
				supprimerStructures();
				supprimerCnu();

				this.delete();
			}

		} catch (Exception e) {
			System.err.println("Erreur suppression composant " + this.id() + " : " + e.getMessage());
			throw e;
		}
	}

	protected void supprimerInfoVersion() throws Exception {
		if (this.composantInfoVersion() != null) {
			this.composantInfoVersion().supprimer();
			setComposantInfoVersionRelationship(null);
		}
	}

	protected void supprimerComposantOrigine() throws Exception {
		if (this.getComposantAvecOrigine()!=null) {
			((EOComposant) this.getComposantAvecOrigine()).removeFromComposantsSuivantsRelationship(this);
			this.setComposantOrigineRelationship(null);
		}
	}
	
	protected void supprimerComposantsSuivants() throws Exception {
		Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> objects = composantsSuivants().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	EOComposant composant = objects.nextElement();
	    	removeFromComposantsSuivantsRelationship(composant);
	    }
	}
	
	protected void supprimerCreditables() throws Exception {
		if (creditables() == null) {
			return;
		}
		
		Enumeration<EOCreditable> objects = creditables().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOCreditable creditable = objects.nextElement();
			creditable.supprimer();
		}
	}
	
	protected void supprimerDescriptifs() throws Exception {
		if (descriptifs() == null) {
			return;
		}
		
		Enumeration<EODescriptif> objects = descriptifs().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EODescriptif descriptif = objects.nextElement();
			descriptif.supprimer();
		}
	}
	
	protected void supprimerDisciplines() throws Exception {
		if (disciplines() == null) {
			return;
		}
		
		Enumeration<EODiscipline> objects = disciplines().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EODiscipline discipline = objects.nextElement();
			removeFromDisciplinesRelationship(discipline);
		}
	}
	
	protected void supprimerDomaines() throws Exception {
		if (domaines() == null) {
			return;
		}
		
		Enumeration<EODomaine> objects = domaines().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EODomaine domaine = objects.nextElement();
			removeFromDomainesRelationship(domaine);
		}
	}
	
	protected void supprimerImplantationsGeo() throws Exception {
		if (implantationsGeos() == null) {
			return;
		}

		Enumeration<EOImplantationGeo> objects = implantationsGeos().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOImplantationGeo implGeo = objects.nextElement();
			removeFromImplantationsGeosRelationship(implGeo);
		}
	}
	
	protected void supprimerLangues() {
		if (langues() == null) {
			return;
		}

		Enumeration<EOLangue> objects = langues().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOLangue langue =  objects.nextElement();
			removeFromLanguesRelationship(langue);
		}
	}
	
	protected void supprimerLiensEnfants() throws Exception {
		if (liensParents() != null) {
			for (EOLien lien : NSArrayCtrl.removeDuplicate(liensParents())) {
				lien.supprimer();
			}
		}
	}
	
	protected void supprimerLiensParents() throws Exception {
		if (liensChilds() != null) {
			for (EOLien lien : NSArrayCtrl.removeDuplicate(liensChilds())) {
				lien.supprimer();
			}
		}
	}

	protected void supprimerResponsables() throws Exception {
		if (responsablesComposant() == null) {
			return;
		}
		
		Enumeration<EOResponsableComposant> objects = responsablesComposant().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOResponsableComposant respComp = objects.nextElement();
			respComp.supprimer();
		}
	}
	
	protected void supprimerSessions() throws Exception {
		if (sessions() == null) {
			return;
		}
		
		Enumeration<EOSessionComposant> objects = sessions().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOSessionComposant session = objects.nextElement();
			session.supprimer();
		}
	}
	
	protected void supprimerStructures() throws Exception {
		if (structures() == null) {
			return;
		}

		Enumeration<EOStructure> objects = structures().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EOStructure structure = objects.nextElement();
			removeFromStructuresRelationship(structure);
		}
	}
	
	protected void supprimerCnu() throws Exception {
		if (this.toCnu() != null) {
			setToCnuRelationship(null);
		}
	}

	/**
	 * associe une langue au composant.
	 * @param langue : langue associee.
	 * @return true si tout s'est bien passe, false sinon
	 */
	public Boolean associerLangue(EOLangue langue) {
		try {
			this.addToLanguesRelationship(langue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * associe le creditable du composant.
	 * @param creditable : le creditable a associer.
	 */
	public void associerCreditable(EOCreditable creditable) {
		if (creditable != null) {
			creditable.setComposantRelationship(this);
		}
	}

	/**
	 * a cause d'une limitation de WebObject, on est oblige d'implementer une relation many to one et de manipuler un tableau de creditable pour conserver un seul
	 * lien
	 * @return le creditable associe au composant si il existe
	 */
	public ICreditable creditable() {
		if ((creditables() != null) && (creditables().count() == 1)) {
			return creditables().get(0);
		}

		return null;
	}

	/**
	 * a cause d'une limitation de WebObject, on est oblige d'implementer une relation many to one et de manipuler un tableau de composant suivant pour conserver
	 * un seul composant suivant
	 * @return le composant suivant associe au composant si il existe
	 */
	public IComposant getComposantSuivant() {
		if ((composantsSuivants() != null) && (composantsSuivants().count() == 1)) {
			return ((IComposant) composantsSuivants().get(0));
		}

		return null;
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une discipline.
	 * @return discipline associee au composant.
	 */
	public EODiscipline getDiscipline() {
		if ((disciplines() != null) && (disciplines().count() == 1)) {
			return disciplines().get(0);
		}

		return null;
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une discipline.
	 * @param discipline : discipline.
	 */
	public void setDiscipline(EODiscipline discipline) {
		for (EODiscipline disciplineExistante : disciplines().immutableClone()) {
			removeFromDisciplinesRelationship(disciplineExistante);
		}

		if (discipline != null) {
			this.addToDisciplinesRelationship(discipline);
		}
	}

	/**
	 * associe un domaine au composant.
	 * @param domaine : domaine associe
	 */
	public void associerDomaine(EODomaine domaine) {
		this.addToDomainesRelationship(domaine);
	}

	/**
	 * @param edc : contexte d'edition
	 * @param composantParent : composant parentchilds
	 * @param typeLien : type de lien
	 * @return : le tableau des type de composants qui peuvent etre rattache a un composant.
	 */
	public NSArray<EOTypeComposant> typesComposantsPouvantEtreRattaches(EOEditingContext edc, EOTypeComposant composantParent, EOTypeLien typeLien) {
		NSMutableArray<EOTypeComposant> typesComposants = new NSMutableArray<EOTypeComposant>();

		EOQualifier allowedParentQualifier = ERXQ.equals(EORegleLiaison.ALLOWED_PARENT_KEY, composantParent);
		EOQualifier typeLienQualifier = ERXQ.equals(EORegleLiaison.TYPE_LIEN_KEY, typeLien);
		EOQualifier forbiddenQualifier = ERXQ.equals(EORegleLiaison.FORBIDDEN_KEY, 0);
		EOQualifier visibleQualifier = ERXQ.equals(EORegleLiaison.VISIBLE_KEY, 1);
		EOQualifier rechercheQualifier = ERXQ.and(allowedParentQualifier, typeLienQualifier, forbiddenQualifier, visibleQualifier);

		if (EORegleLiaison.fetchSco_RegleLiaisons(edc, rechercheQualifier, null).size() > 0) {
			for (EORegleLiaison regleLiaison : EORegleLiaison.fetchSco_RegleLiaisons(edc, rechercheQualifier, null)) {
				typesComposants.add(regleLiaison.allowedChild());
			}

			return typesComposants;
		} else {
			return null;
		}
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une implementation geographique.
	 * @return implantation geographique
	 */
	public EOImplantationGeo getImplantationGeo() {
		EOImplantationGeo implantationGeo = null;
		if ((implantationsGeos() != null) && (implantationsGeos().count() > 0)) {
			implantationGeo = implantationsGeos().get(0);
		}
		return implantationGeo;
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une implementation geographique.
	 * @param implantationGeo : implantation geographique.
	 * @throws Exception 
	 */
	public void setImplantationGeo(EOImplantationGeo implantationGeo) throws Exception {
		supprimerImplantationsGeo();

		if (implantationGeo != null) {
			associerImplantationsGeos(implantationGeo);
		}
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une structure
	 * @return la structure de l'element
	 */
	public IStructure getStructure() {
		IStructure structure = null;
		if ((structures() != null) && (structures().count() > 0)) {
			structure = structures().get(0);
		}
		return structure;
	}

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une structure
	 * @param structure : structure a associer
	 * @throws Exception 
	 */
	public void setStructure(EOStructure structure) throws Exception {
		supprimerStructures();

		if (structure != null) {
			associerStructure(structure);
		}
	}

	/**
	 * renvoie les composants d'un certain type sans lien avec le composant present
	 * @param typeComposant : type de composant
	 * @return tableau de composant
	 */
	public NSArray<EOComposant> composantsSansLienAvec(EOTypeComposant typeComposant) {
		NSMutableArray<EOComposant> relatives = new NSMutableArray<EOComposant>();
		relatives.addAll(allAncestors());
		relatives.addAll(allSuccessors());
		relatives.add(this);

		NSArray<EOComposant> composants = EOComposant.fetchSco_Composants(editingContext(), EOComposant.TYPE_COMPOSANT.eq(typeComposant), null);
		NSMutableArray<EOComposant> compsSansLien = new NSMutableArray<EOComposant>();
		for (EOComposant comp : composants) {
			if (!relatives.contains(comp)) {
				compsSansLien.add(comp);
			}
		}

		return ERXArrayUtilities.arrayWithoutDuplicates(compsSansLien);
	}

	public Boolean isMutualise() {
		return (parents().size() > 1);
	}

	/**
	 * renvoie une chaine concatenee renvoyant la liste des parents du composant a l'exclusion du parent exclu
	 * @param parentExclu : parent exlu de la liste
	 * @return codes des parents concatenes
	 */
	public String listeCodesAutresParents(EOComposant parentExclu) {
		return StringUtils.join(tableauCodesAutresParents(parentExclu), ", ");
	}

	/**
	 * renvoie un tableau renvoyant les parents du composant a l'exclusion du parent exclu
	 * @param parentExclu : parent exlu de la liste
	 * @return tableau de titres (code + libelle) des parents concatenes
	 */
	public NSArray<String> tableauTitreAutresParents(EOComposant parentExclu) {
		NSArray<String> codes = new NSMutableArray<String>();
		for (IComposant composant : parents()) {
			if (composant != parentExclu) {
				codes.add(composant.toString());
			}
		}
		return codes;
	}

	/**
	 * renvoie un tableau renvoyant les parents du composant a l'exclusion du parent exclu
	 * @param parentExclu : parent exlu de la liste
	 * @return tableau de codes des parents concatenes
	 */
	private NSArray<String> tableauCodesAutresParents(EOComposant parentExclu) {
		NSArray<String> codes = new NSMutableArray<String>();
		for (IComposant composant : parents()) {
			if (composant != parentExclu) {
				codes.add(composant.code());
			}
		}
		return codes;
	}

	/**
	 * @return descriptif courant du composant
	 */
	public EODescriptif descriptifCourant() {
		if (descriptifs().count() == 0) {
			EODescriptif descriptif = EOComposantFactory.createDescriptif(this);
			return descriptif;
		} else {
			return descriptifs(null, EOScolpedaCommonDefinitions.defaultsortOrderings(EODescriptif.ID_KEY), false).get(0);
		}
	}

	/**
	 * Indique si ce composant est la première version pour l'annee passee en parametre
	 * @param annee : annee de l'exercice
	 * @return Boolean : vrai si oui, non dans l'autre cas
	 */
	public Boolean isFirstVersion(Integer annee) {
		return (this.composantInfoVersion().annee().equals(annee))
		    && (this.composantOrigine() == null || !this.composantOrigine().composantInfoVersion().annee().equals(annee));
	}

	/**
	 * Indique si ce composant est la dernière version pour l'annee passee en parametre
	 * @param annee : annee de l'exercice
	 * @return Boolean : vrai si oui, non dans l'autre cas
	 */
	public Boolean isLastVersion(Integer annee) {
		return (this.composantsSuivants(ERXQ.equals(EOComposant.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE).key(), annee)).size() == 0);
	}

	// Private methods
	/**
	 * renvoie recursivement les suivants d'un composant.
	 * @param composant : composant pour lequel on cherche les successeurs
	 * @param successors : tableau de successeurs rempli recursivement.
	 */
	private void successors(EOComposant composant, NSMutableArray<EOComposant> successors) {

		if (composant.liensParents() == null) {
			return;
		}

		for (EOLien lien : composant.liensParents()) {
			successors.add(lien.child());

			if (lien.parent().liensParents().size() > 0) {
				successors(lien.child(), successors);
			}
		}
	}

	/**
	 * renvoie recursivement les suivants d'un composant.
	 * @param composant : composant pour lequel on cherche les successeurs
	 * @param ancestors : tableau d'ancetres rempli recursivement.
	 */
	private void ancestors(EOComposant composant, NSMutableArray<EOComposant> ancestors) {
		/*
		 * for (EOLien lien : composant.liensChilds()) { ancestors.add(lien.parent());
		 * 
		 * if (lien.parent().liensChilds().size() > 0) { ancestors(lien.parent(), ancestors); } }
		 */

		if (composant.liensChilds() == null) {
			return;
		}

		for (EOLien lien : composant.liensChilds()) {
			ancestors.add(lien.parent());

			if (lien.parent().liensChilds().size() > 0) {
				ancestors(lien.parent(), ancestors);
			}
		}
	}

	private void associerStructure(EOStructure structure) {
		if (structure != null) {
			this.addToStructuresRelationship(structure);
		}
	}

	private void associerImplantationsGeos(EOImplantationGeo implantationGeo) {
		if (implantationGeo != null) {
			this.addToImplantationsGeosRelationship(implantationGeo);
		}
	}

	/**
	 * @param child : le composant enfant
	 * @param typeLien : le type de lien
	 * @return vrai si on peut faire un lien entre le composant courant et le composant child avec le type de lien typeLien
	 */
	public boolean lienPossible(EOComposant child, EOTypeLien typeLien) {
		// le composant parent et child ne doivent pas etre le meme
		if (this.equals(child)) {
			throw new IllegalArgumentException("Liaison invalide : Le composant parent " + this + " et le composant child " + child
			    + " ne doivent pas etre identiques");
		}

		if (childs().contains(child)) {
			throw new IllegalArgumentException("Liaison invalide : il y a déjà un lien entre Le composant parent " + this + " et le composant child " + child);
		}

		// le composant child ne doit pas deja avoir le composant parent dans la liste de ses successors
		if (child.allSuccessors().contains(this)) {
			throw new IllegalArgumentException("Liaison invalide : Le composant child " + child + " ne doit pas deja avoir le composant parent " + this
			    + " dans la liste de ses composants successeurs");
		}

		return EORegleLiaison.isValid(this.editingContext(), this.typeComposant(), child.typeComposant(), typeLien);
	}

	/********************************************************************************************************/
	/****************************** FONCTION CALCUL POUR TREE TABLE *****************************************/
	/********************************************************************************************************/

	/** COMPOSANT DE TYPE EC **/

	/**
	 * {@inheritDoc}
	 */
	public EOAP getAPCours() {
		return getUniqueAPTypeDonneNullSinon(EOTypeAP.typeCours(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOAP> getAPsCours() {
		return getAPs(EOTypeAP.typeCours(editingContext()));
	}

	/**
	 * @return premier element de type AP et de typeAP typeTD
	 */
	public EOAP getAPTD() {
		return getUniqueAPTypeDonneNullSinon(EOTypeAP.typeTD(editingContext()));
	}

	/**
	 * @return la liste de APs de type TD
	 */
	public NSArray<EOAP> getAPsTD() {
		return getAPs(EOTypeAP.typeTD(editingContext()));
	}

	/**
	 * @return premier element de type AP et de typeAP typeTP
	 */
	public EOAP getAPTP() {
		return getUniqueAPTypeDonneNullSinon(EOTypeAP.typeTP(editingContext()));
	}

	/**
	 * @return la liste de APs de type TP
	 */
	public NSArray<EOAP> getAPsTP() {
		return getAPs(EOTypeAP.typeTP(editingContext()));
	}

	/* COLONNE MODALITES DE CONTROLES pour les types d'AE */
	// TYPE AE CC Oral
	public NSArray<EOAE> getAEsCCOral() {
		return getAEs(EOTypeAE.typeCCOral(editingContext()));
	}

	/**
	 * @return unique AE de type typeCCOral si il existe
	 */
	public EOAE getAECCOral() {
		if (getAEsCCOral().size() == 1) {
			return getAEsCCOral().get(0);
		} else {
			return null;
		}
	}

	/**
	 * @return lien de type lienComposant entre le composant courant et son AE de type CCOral si il existe
	 */
	public EOLienComposer lienComposerAvecAECCOral() {
		return lienComposerParentsAvecEnfant(getAECCOral());
	}

	/**
	 * @return somme des coefficients des AE de type typeCCOral
	 */
	public BigDecimal sommeCoefficientAECCOral() {
		return sommeCoefficientAE(EOTypeAE.typeCCOral(editingContext()));
	}

	private BigDecimal sommeCoefficientAE(EOTypeAE typeAE) {
		BigDecimal somme = new BigDecimal("0");
		for (EOLienComposer lienComposer : liensComposerAvecLesComposantsEnfants(EOTypeComposant.typeAE(editingContext()))) {
			EOAE ae = (EOAE) lienComposer.child();
			if (ae.typeAE().equals(typeAE)) {
				if (lienComposer.coefficient() != null) {
					somme = somme.add(lienComposer.coefficient());
				}
			}
		}

		return somme;
	}

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOAE> getAEsCCEcrit() {
		return getAEs(EOTypeAE.typeCCEcrit(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOAE getAECCEcrit() {
		return getUniqueAETypeDonneNullSinon(EOTypeAE.typeCCEcrit(editingContext()));
	}

	/**
	 * @return lien de type composer entre le composant courant et le composant de type typeAE et avec le typeCCEcrit
	 */
	public EOLienComposer lienComposerAvecAECCEcrit() {
		return lienComposerParentsAvecEnfant(getAECCEcrit());
	}

	private EOLienComposer lienComposerParentsAvecEnfant(EOComposant child) {
		NSArray<EOLienComposer> liensComposer = liensComposerAvecLesComposantsEnfants();
		EOLienComposer result = null;
		for (EOLienComposer lienComposer : liensComposer) {
			if (lienComposer.child().equals(child)) {
				result = lienComposer;
				break;
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal sommeCoefficientAECCEcrit() {
		return sommeCoefficientAE(EOTypeAE.typeCCEcrit(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOAE> getAEsEXOral() {
		return getAEs(EOTypeAE.typeEXOral(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOAE getAEEXOral() {
		return getUniqueAETypeDonneNullSinon(EOTypeAE.typeEXOral(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOLienComposer lienComposerAvecAEEXOral() {
		return lienComposerParentsAvecEnfant(getAEEXOral());
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal sommeCoefficientAEEXOral() {
		return sommeCoefficientAE(EOTypeAE.typeEXOral(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOAE> getAEsEXEcrit() {
		return getAEs(EOTypeAE.typeEXEcrit(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOAE getAEEXEcrit() {
		return getUniqueAETypeDonneNullSinon(EOTypeAE.typeEXEcrit(editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOLienComposer lienComposerAvecAEEXEcrit() {
		return lienComposerParentsAvecEnfant(getAEEXEcrit());
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal sommeCoefficientAEEXEcrit() {
		return sommeCoefficientAE(EOTypeAE.typeEXEcrit(editingContext()));
	}

	/** COMPOSANT DE TYPE UE/REGROUPEMENT/PARCOURS/PERIODE/VERSION_DIPLOME **/
	/* COLONNE NOMBRES D'HEURES */

	/**
	 * Permet de retirer des types de composants supplementaires par rapport a ceux definis dans la base
	 * @param composantsInterditsSupplementaires types de composants a retirer
	 * @return les types de composants autorises
	 */
	public NSArray<EOTypeComposant> typesComposantEnfantAutorise(NSArray<EOTypeComposant> composantsInterditsSupplementaires) {

		NSArray<EOTypeComposant> composantsAutorises = EOTypeComposant.typesComposantEnfantAutorise(editingContext(), typeComposant(),
		    EOTypeLien.typeComposer(editingContext()));
		if (composantsInterditsSupplementaires != null) {
			composantsAutorises.removeAll(composantsInterditsSupplementaires);
		}
		return composantsAutorises;

	}

	/**
	 * @return un libelle pour l'affichage dans un treetable
	 */
	public String libelleTreeTable() {
		return libelle();
	}

	public String libelleVersion() {
		return "v " + this.composantInfoVersion().versionMajeure();
	}

	private NSArray<EOAP> getAPs(EOTypeAP typeAP) {
		NSArray<EOAP> aps = new NSMutableArray<EOAP>();

		for (IComposant composant : childs(EOTypeLien.typeComposer(editingContext()), EOTypeComposant.typeAP(editingContext()), EOLien.ORDRE.ascs())) {
			if (((EOAP) composant).typeAP().equals(typeAP)) {
				aps.add((EOAP) composant);
			}
		}

		return aps;
	}

	private NSArray<EOAE> getAEs(EOTypeAE typeAE) {
		NSArray<EOAE> aes = new NSMutableArray<EOAE>();

		for (IComposant composant : childs(EOTypeLien.typeComposer(editingContext()), EOTypeComposant.typeAE(editingContext()), EOLien.ORDRE.ascs())) {
			EOTypeAE composantTypeAE = ((EOAE) composant).typeAE();
			if (composantTypeAE.equals(typeAE)) {
				aes.add((EOAE) composant);
			}
		}

		return aes;
	}

	/**
	 * @param typeAP : type AP
	 * @return unique type A d'un type donne, null sinon
	 */
	private EOAP getUniqueAPTypeDonneNullSinon(EOTypeAP typeAP) {
		NSArray<EOAP> aps = getAPs(typeAP);
		if (aps.size() == 1) {
			return aps.get(0);
		} else {
			return null;
		}
	}

	/**
	 * @param typeAE : type AE
	 * @return unique type A Ed'un type donne, null sinon
	 */
	private EOAE getUniqueAETypeDonneNullSinon(EOTypeAE typeAE) {
		NSArray<EOAE> aes = getAEs(typeAE);
		if (aes.size() == 1) {
			return aes.get(0);
		} else {
			return null;
		}
	}

	/**
	 * @param listeHeureChilds
	 * @return liste des heures des enfants (se construit recursivement
	 */
	private void remplitListHeureTravailEtudiantChilds(List<BigDecimal> listeHeureChilds) {
		for (IComposant composant : childs(EOTypeLien.typeComposer(editingContext()))) {
			if (((EOComposant) composant).getTypeComposant().equals(EOTypeComposant.typeEC(editingContext()))) {
				if ((((EOEC) composant).creditable() != null) && (((EOEC) composant).creditable().travailEtudiant() != null)) {
					listeHeureChilds.add(new BigDecimal(((EOEC) composant).creditable().travailEtudiant().toString()));
				}
			} else {
				((EOComposant) composant).remplitListHeureTravailEtudiantChilds(listeHeureChilds);
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public abstract EOComposant copy(Integer anneeVersion, Integer numeroMajeur);

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public <T extends IComposant> T dupliquer(Integer anneeVersion, Integer numeroMajeur) {

		T copieComposant = getComposantAvecOrigine();

		if (copieComposant == null) {
			copieComposant = (T) this.copy(anneeVersion, numeroMajeur);

			for (ILien lien : ERXArrayUtilities.arrayWithoutDuplicates(liensParents())) {
				T composantChildCopie = (T) lien.child().dupliquer(anneeVersion, numeroMajeur);
				ILienComposer lienComposer = EOComposantFactory.createLienComposer(copieComposant, composantChildCopie);
				lien.assignTo(lienComposer);
			}
		}

		return copieComposant;
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract <T extends IComposant> T getComposantAvecOrigine();

	protected void assignTo(EOComposant newComposant) {

		// properties
		newComposant.setCode(this.code());
		newComposant.setLibelle(this.libelle());
		newComposant.setTagApplication(this.tagApplication());

		newComposant.setComposantOrigineRelationship(this);

		newComposant.setTypeComposantRelationship(this.typeComposant());

		
		for (IResponsableComposant responsableComposant : this.responsablesComposant()) {
			newComposant.associerResponsable(responsableComposant);
		}

		for (EOImplantationGeo implant : this.implantationsGeos()) {
			newComposant.associerImplantationsGeos(implant);
		}

		for (EOStructure structure : this.structures()) {
			newComposant.associerStructure(structure);
		}
		
		for (EODomaine domaine : this.domaines()) {
			newComposant.associerDomaine(domaine);
		}

		// relations pas toujours présentes dans le composant dérivé
		conditionalAssignTo(newComposant);
	}

	private void associerResponsable(IResponsableComposant responsableComposant) {
	  IResponsableComposant newResponsableComposant = responsableComposant.copyToComposant(this);
	  addToResponsablesComposantRelationship((EOResponsableComposant) newResponsableComposant);	  
  }

	/**
	 * @param newComposant : composant auquel on assigne les valeurs du composant courant
	 */
	private void conditionalAssignTo(EOComposant newComposant) {
		if (this.allPropertyKeys().contains(LIBELLE_COURT_KEY)) {
			newComposant.setLibelleCourt(this.libelleCourt());
		}

		if (this.allPropertyKeys().contains(DISCIPLINES_KEY)) {
			for (EODiscipline discipline : this.disciplines()) {
				newComposant.addToDisciplinesRelationship(discipline);
			}
		}

		if (this.allPropertyKeys().contains(LANGUES_KEY)) {
			for (EOLangue langue : this.langues()) {
				newComposant.associerLangue(langue);
			}
		}

		if (this.allPropertyKeys().contains(SESSIONS_KEY)) {
			for (EOSessionComposant session : sessions()) {
				newComposant.addToSessionsRelationship(session.copy());
			}
		}

		if (this.allPropertyKeys().contains(CREDITABLES_KEY)) {
			for (EOCreditable creditable : creditables()) {
				newComposant.associerCreditable(creditable.copy());
			}
		}

		if (this.allPropertyKeys().contains(DOMAINES_KEY)) {
			for (EODomaine domaine : this.domaines()) {
				newComposant.addToDomainesRelationship(domaine);
			}
		}
	}

	/**
	 * valide le code et le remplit automatiquement si il est vide
	 * @param code le code à valider
	 * @return le code après validation
	 */
	public Object validateCode(String code) {
		return getValidateur().validateCode(code, this.editingContext());
	}

	/**
	 * @return vrai si le composant a un creditable
	 */
	public boolean hasCreditable() {
		try {
			return creditable() != null;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean estIdentique(IComposant composant) {
		return ERXEOControlUtilities.eoEquals(this, (EOComposant) composant);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ILien> getLiensAnnees(Integer niveau) {
		if (niveau == null) {
			return null;
		}
		List<ILien> liensAnnees = new ArrayList<ILien>();
		chercheLiensAnnees(this, niveau, liensAnnees);
		Set<ILien> set = new HashSet<ILien>(liensAnnees);
		return new ArrayList<ILien>(set);
	}

	private void chercheLiensAnnees(IComposant composant, Integer niveau, List<ILien> liensAnnees) {
		for (ILien lien : composant.liensParents()) {
			if (lien.child() instanceof IPeriode) {
				IPeriode periode = ((IPeriode) lien.child());
				if (periode.isAnnee() && niveau.equals(periode.ordre())) {
					liensAnnees.add(lien);
				}
			}
		}
		for (ILien lien : composant.liensParents()) {
			chercheLiensAnnees(lien.child(), niveau, liensAnnees);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isTypeOf(Class<? extends IComposant> type) {
		return type.isAssignableFrom(this.getClass());
	}

	/**
	 * @return the currentCnu
	 */
	public String getCurrentCnu() {
		if (toCnu() != null) {
			if (toCnu().cSousSectionCnu() != null) {
				return (toCnu().cSectionCnu() + " " + toCnu().cSousSectionCnu());
			} else {
				return toCnu().cSectionCnu() + "    ";
			}
		}
		return null;
	}

	public Boolean isPeriode() {
		return this instanceof IPeriode;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String cnu() {
		return getCurrentCnu();
	}
	
	public void renumeroterLienComposerEnfants(ITypeComposant typeComposant) {
		NSArray<EOLienComposer> listeLiensComposer = this.liensComposerAvecLesComposantsEnfants((EOTypeComposant) typeComposant);
		for (int i = 0; i < listeLiensComposer.count(); i++) {
			EOLienComposer lienComposer = listeLiensComposer.get(i);
			lienComposer.setOrdre(i + 1);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ISessionComposant getSession(Integer numeroSession) {
		List<? extends ISessionComposant> sessions = sessions();
		ISessionComposant resultat = null;
		for (ISessionComposant sessionComposant : sessions) {
			if (numeroSession != null && numeroSession.equals(sessionComposant.numeroSession())) {
				resultat = sessionComposant;
				break;
			}
		}
		return resultat;
	}
	
}
