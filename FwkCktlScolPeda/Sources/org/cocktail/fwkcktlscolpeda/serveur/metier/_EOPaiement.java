// DO NOT EDIT.  Make changes to EOPaiement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Paiement";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_PAIEMENT = new ERXKey<NSTimestamp>("datePaiement");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Boolean> PAIEMENT_VALIDE = new ERXKey<Boolean>("paiementValide");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau> TO_BORDEREAU = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau>("toBordereau");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee> TO_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee>("toEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> TO_PAIEMENT_DETAILS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail>("toPaiementDetails");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> TO_PAIEMENT_MOYENS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>("toPaiementMoyens");

  // Attributes
  public static final String DATE_PAIEMENT_KEY = DATE_PAIEMENT.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PAIEMENT_VALIDE_KEY = PAIEMENT_VALIDE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_BORDEREAU_KEY = TO_BORDEREAU.key();
  public static final String TO_ETUDIANT_ANNEE_KEY = TO_ETUDIANT_ANNEE.key();
  public static final String TO_PAIEMENT_DETAILS_KEY = TO_PAIEMENT_DETAILS.key();
  public static final String TO_PAIEMENT_MOYENS_KEY = TO_PAIEMENT_MOYENS.key();

  private static Logger LOG = Logger.getLogger(_EOPaiement.class);

  public EOPaiement localInstanceIn(EOEditingContext editingContext) {
    EOPaiement localInstance = (EOPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp datePaiement() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_PAIEMENT_KEY);
  }

  public void setDatePaiement(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating datePaiement from " + datePaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_PAIEMENT_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiement.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.MONTANT_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOPaiement.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.ORDRE_KEY);
  }

  public Boolean paiementValide() {
    return (Boolean) storedValueForKey(_EOPaiement.PAIEMENT_VALIDE_KEY);
  }

  public void setPaiementValide(Boolean value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating paiementValide from " + paiementValide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.PAIEMENT_VALIDE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPaiement.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPaiement.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau toBordereau() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau)storedValueForKey(_EOPaiement.TO_BORDEREAU_KEY);
  }
  
  public void setToBordereau(org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau value) {
    takeStoredValueForKey(value, _EOPaiement.TO_BORDEREAU_KEY);
  }

  public void setToBordereauRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("updating toBordereau from " + toBordereau() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToBordereau(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau oldValue = toBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiement.TO_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiement.TO_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee)storedValueForKey(_EOPaiement.TO_ETUDIANT_ANNEE_KEY);
  }
  
  public void setToEtudiantAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    takeStoredValueForKey(value, _EOPaiement.TO_ETUDIANT_ANNEE_KEY);
  }

  public void setToEtudiantAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("updating toEtudiantAnnee from " + toEtudiantAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiantAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee oldValue = toEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiement.TO_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiement.TO_ETUDIANT_ANNEE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> toPaiementDetails() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail>)storedValueForKey(_EOPaiement.TO_PAIEMENT_DETAILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> toPaiementDetails(EOQualifier qualifier) {
    return toPaiementDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> toPaiementDetails(EOQualifier qualifier, boolean fetch) {
    return toPaiementDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> toPaiementDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail.fetchSco_PaiementDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPaiementDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPaiementDetails(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail object) {
    includeObjectIntoPropertyWithKey(object, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
  }

  public void removeFromToPaiementDetails(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail object) {
    excludeObjectFromPropertyWithKey(object, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
  }

  public void addToToPaiementDetailsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("adding " + object + " to toPaiementDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPaiementDetails(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
    }
  }

  public void removeFromToPaiementDetailsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("removing " + object + " from toPaiementDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPaiementDetails(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail createToPaiementDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail) eo;
  }

  public void deleteToPaiementDetailsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPaiementDetailsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail> objects = toPaiementDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPaiementDetailsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> toPaiementMoyens() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>)storedValueForKey(_EOPaiement.TO_PAIEMENT_MOYENS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> toPaiementMoyens(EOQualifier qualifier) {
    return toPaiementMoyens(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> toPaiementMoyens(EOQualifier qualifier, boolean fetch) {
    return toPaiementMoyens(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> toPaiementMoyens(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen.fetchSco_PaiementMoyens(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPaiementMoyens();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPaiementMoyens(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen object) {
    includeObjectIntoPropertyWithKey(object, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
  }

  public void removeFromToPaiementMoyens(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen object) {
    excludeObjectFromPropertyWithKey(object, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
  }

  public void addToToPaiementMoyensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("adding " + object + " to toPaiementMoyens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPaiementMoyens(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
    }
  }

  public void removeFromToPaiementMoyensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("removing " + object + " from toPaiementMoyens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPaiementMoyens(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen createToPaiementMoyensRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen) eo;
  }

  public void deleteToPaiementMoyensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_PAIEMENT_MOYENS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPaiementMoyensRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> objects = toPaiementMoyens().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPaiementMoyensRelationship(objects.nextElement());
    }
  }


  public static EOPaiement createSco_Paiement(EOEditingContext editingContext, NSTimestamp dCreation
, java.math.BigDecimal montant
, Integer ordre
, Boolean paiementValide
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee) {
    EOPaiement eo = (EOPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setMontant(montant);
		eo.setOrdre(ordre);
		eo.setPaiementValide(paiementValide);
		eo.setPersIdCreation(persIdCreation);
    eo.setToEtudiantAnneeRelationship(toEtudiantAnnee);
    return eo;
  }

  public static ERXFetchSpecification<EOPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOPaiement>(_EOPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPaiement> fetchAllSco_Paiements(EOEditingContext editingContext) {
    return _EOPaiement.fetchAllSco_Paiements(editingContext, null);
  }

  public static NSArray<EOPaiement> fetchAllSco_Paiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPaiement.fetchSco_Paiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPaiement> fetchSco_Paiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPaiement> fetchSpec = new ERXFetchSpecification<EOPaiement>(_EOPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPaiement fetchSco_Paiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiement.fetchSco_Paiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiement fetchSco_Paiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPaiement> eoObjects = _EOPaiement.fetchSco_Paiements(editingContext, qualifier, null);
    EOPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Paiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiement fetchRequiredSco_Paiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiement.fetchRequiredSco_Paiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiement fetchRequiredSco_Paiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPaiement eoObject = _EOPaiement.fetchSco_Paiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Paiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiement localInstanceIn(EOEditingContext editingContext, EOPaiement eo) {
    EOPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
