// DO NOT EDIT.  Make changes to EOSelectionLienDescriptif.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSelectionLienDescriptif extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SelectionLienDescriptif";

  // Attribute Keys
  public static final ERXKey<Integer> DESCRIPTIF_ID = new ERXKey<Integer>("descriptifId");
  public static final ERXKey<Integer> LIEN_ID = new ERXKey<Integer>("lienId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptif");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("lien");

  // Attributes
  public static final String DESCRIPTIF_ID_KEY = DESCRIPTIF_ID.key();
  public static final String LIEN_ID_KEY = LIEN_ID.key();
  // Relationships
  public static final String DESCRIPTIF_KEY = DESCRIPTIF.key();
  public static final String LIEN_KEY = LIEN.key();

  private static Logger LOG = Logger.getLogger(_EOSelectionLienDescriptif.class);

  public EOSelectionLienDescriptif localInstanceIn(EOEditingContext editingContext) {
    EOSelectionLienDescriptif localInstance = (EOSelectionLienDescriptif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer descriptifId() {
    return (Integer) storedValueForKey(_EOSelectionLienDescriptif.DESCRIPTIF_ID_KEY);
  }

  public void setDescriptifId(Integer value) {
    if (_EOSelectionLienDescriptif.LOG.isDebugEnabled()) {
    	_EOSelectionLienDescriptif.LOG.debug( "updating descriptifId from " + descriptifId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSelectionLienDescriptif.DESCRIPTIF_ID_KEY);
  }

  public Integer lienId() {
    return (Integer) storedValueForKey(_EOSelectionLienDescriptif.LIEN_ID_KEY);
  }

  public void setLienId(Integer value) {
    if (_EOSelectionLienDescriptif.LOG.isDebugEnabled()) {
    	_EOSelectionLienDescriptif.LOG.debug( "updating lienId from " + lienId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSelectionLienDescriptif.LIEN_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif descriptif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif)storedValueForKey(_EOSelectionLienDescriptif.DESCRIPTIF_KEY);
  }
  
  public void setDescriptif(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    takeStoredValueForKey(value, _EOSelectionLienDescriptif.DESCRIPTIF_KEY);
  }

  public void setDescriptifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    if (_EOSelectionLienDescriptif.LOG.isDebugEnabled()) {
      _EOSelectionLienDescriptif.LOG.debug("updating descriptif from " + descriptif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDescriptif(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif oldValue = descriptif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOSelectionLienDescriptif.DESCRIPTIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOSelectionLienDescriptif.DESCRIPTIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien lien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOSelectionLienDescriptif.LIEN_KEY);
  }
  
  public void setLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOSelectionLienDescriptif.LIEN_KEY);
  }

  public void setLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOSelectionLienDescriptif.LOG.isDebugEnabled()) {
      _EOSelectionLienDescriptif.LOG.debug("updating lien from " + lien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = lien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOSelectionLienDescriptif.LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOSelectionLienDescriptif.LIEN_KEY);
    }
  }
  

  public static EOSelectionLienDescriptif createSco_SelectionLienDescriptif(EOEditingContext editingContext, Integer descriptifId
, Integer lienId
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif descriptif, org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien lien) {
    EOSelectionLienDescriptif eo = (EOSelectionLienDescriptif) EOUtilities.createAndInsertInstance(editingContext, _EOSelectionLienDescriptif.ENTITY_NAME);    
		eo.setDescriptifId(descriptifId);
		eo.setLienId(lienId);
    eo.setDescriptifRelationship(descriptif);
    eo.setLienRelationship(lien);
    return eo;
  }

  public static ERXFetchSpecification<EOSelectionLienDescriptif> fetchSpec() {
    return new ERXFetchSpecification<EOSelectionLienDescriptif>(_EOSelectionLienDescriptif.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSelectionLienDescriptif> fetchAllSco_SelectionLienDescriptifs(EOEditingContext editingContext) {
    return _EOSelectionLienDescriptif.fetchAllSco_SelectionLienDescriptifs(editingContext, null);
  }

  public static NSArray<EOSelectionLienDescriptif> fetchAllSco_SelectionLienDescriptifs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptifs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSelectionLienDescriptif> fetchSco_SelectionLienDescriptifs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSelectionLienDescriptif> fetchSpec = new ERXFetchSpecification<EOSelectionLienDescriptif>(_EOSelectionLienDescriptif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSelectionLienDescriptif> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSelectionLienDescriptif fetchSco_SelectionLienDescriptif(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSelectionLienDescriptif fetchSco_SelectionLienDescriptif(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSelectionLienDescriptif> eoObjects = _EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptifs(editingContext, qualifier, null);
    EOSelectionLienDescriptif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SelectionLienDescriptif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSelectionLienDescriptif fetchRequiredSco_SelectionLienDescriptif(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSelectionLienDescriptif.fetchRequiredSco_SelectionLienDescriptif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSelectionLienDescriptif fetchRequiredSco_SelectionLienDescriptif(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSelectionLienDescriptif eoObject = _EOSelectionLienDescriptif.fetchSco_SelectionLienDescriptif(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SelectionLienDescriptif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSelectionLienDescriptif localInstanceIn(EOEditingContext editingContext, EOSelectionLienDescriptif eo) {
    EOSelectionLienDescriptif localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
