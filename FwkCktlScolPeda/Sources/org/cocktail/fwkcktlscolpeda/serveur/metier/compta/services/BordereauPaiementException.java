package org.cocktail.fwkcktlscolpeda.serveur.metier.compta.services;

/**
 * Exception pouvant être levée par la classe de service {@link BordereauPaiementService}.
 * 
 * @author Pascal MACOUIN
 */
public class BordereauPaiementException extends Exception {

	private static final long serialVersionUID = -9035091188162373070L;

	/**
	 * Construit une exception avec ce message en detail.
	 * @param message un message
	 */
	public BordereauPaiementException(String message) {
		super(message);
	}
	
	/**
	 * Construit l'exception à partir d'un autre exception.
	 * @param e une exception
	 */
	public BordereauPaiementException(Throwable e) {
		super(e.getMessage(), e);
	}
}
