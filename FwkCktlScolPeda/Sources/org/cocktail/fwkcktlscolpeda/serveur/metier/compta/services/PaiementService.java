package org.cocktail.fwkcktlscolpeda.serveur.metier.compta.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaie;

/**
 * Classe de service permettant de manipuler les paiements.
 * (méthodes transverses aux framework FwkCktlPreScolarite et FwkCktlScolPeda)
 * 
 * @author Pascal MACOUIN
 */
public class PaiementService {
	
	/**
	 * Trie des paiements par numéro d'ordre décroissant.
	 */
	private final Comparator<IPaiement> comparatorPaiementParOrdreDecroissant = new Comparator<IPaiement>() {
		public int compare(IPaiement p1, IPaiement p2) {
			return p2.ordre().compareTo(p1.ordre()); // Tri par ordre décroissant
		}
	};

	/**
	 * @return Le comparateur {@link #comparatorPaiementParOrdreDecroissant}
	 */
	public Comparator<IPaiement> getComparatorPaiementParOrdreDecroissant() {
		return comparatorPaiementParOrdreDecroissant;
	}
	
	/**
	 * Trie des détails paiement par type de paramétrage d'article ("Formation" puis "Diplôme" puis "Complémentaire")
	 * et par libellé croissant.
	 */
	private final Comparator<IPaiementDetail> comparatorDetailPaiementParParametrageLibelleAsc = new Comparator<IPaiementDetail>() {
		public int compare(IPaiementDetail d1, IPaiementDetail d2) {
			int compareTo = getOrdreTypeParametrageArticle(d1) - getOrdreTypeParametrageArticle(d2);
			
			if (compareTo == 0) {
				return d1.getLibelle().compareTo(d2.getLibelle());
			}
			
			return compareTo;
		}
		
		/**
		 * @param d un détail paiement
		 * @return le numéro d'ordre du type de paramétrage (en premier les articles Formation, puis Diplôme, puis Complémentaire)
		 */
		private int getOrdreTypeParametrageArticle(IPaiementDetail d) {
			if (d.toParametragePaieFormation() != null) {
				return 1;
			} else if (d.toParametragePaieDiplome() != null) {
				return 2;
			} else {
				return 3;
			}
		}
	};
	
	/**
	 * @return Le comparateur {@link #comparatorDetailPaiementParParametrageLibelleAsc}
	 */
	public Comparator<IPaiementDetail> getComparatorDetailPaiementParParametrageLibelleAsc() {
		return comparatorDetailPaiementParParametrageLibelleAsc;
	}
	
	/**
	 * Calcule la synthèse des articles de paiement d'un étudiant.
	 * <p>
	 * On parcours les paiements pour rechercher les articles et leur état (payer, à payer, rembourser, à rembourser).
	 * 
	 * @param etudiantAnnee Un étudiant année
	 * @return La liste des articles de paiement
	 */
	public List<IPaiementDetail> calculerSyntheseDetailsPaiement(IEtudiantAnnee etudiantAnnee) {
		return calculerSyntheseDetailsPaiement(etudiantAnnee, false);
	}
	
	/**
	 * Calcule la synthèses de articles de paiement d'un étudiant.
	 * <p>
	 * On parcours les paiements pour rechercher les articles et leur état (payer, à payer, rembourser, à rembourser).
	 * 
	 * @param etudiantAnnee Un étudiant année
	 * @param payeUniquement Recchercher les articles payés uniquement
	 * @return La liste des articles de paiement
	 */
	private List<IPaiementDetail> calculerSyntheseDetailsPaiement(IEtudiantAnnee etudiantAnnee, boolean payeUniquement) {
		List<IParametragePaie> listeArticlesPaiement = new ArrayList<IParametragePaie>();
		List<IPaiementDetail> listeDetailsPaiements = new ArrayList<IPaiementDetail>();
		
		List<? extends IPaiement> listePaiements = etudiantAnnee.toPaiements();
		Collections.sort(listePaiements, getComparatorPaiementParOrdreDecroissant());
		
		for (IPaiement paiement : listePaiements) {
			for (IPaiementDetail paiementDetail : paiement.toPaiementDetails()) {
				if (!payeUniquement || paiementDetail.montantPaye() != null) {
					IParametragePaie parametragePaie = paiementDetail.toParametragePaie();
					if (!listeArticlesPaiement.contains(parametragePaie)) {
						listeArticlesPaiement.add(parametragePaie);
						listeDetailsPaiements.add(paiementDetail);
					}
				}
			}
		}
		
		return listeDetailsPaiements;
	}
	
	/**
	 * Recherche des détail de paiement remboursables pour l'étudiant.
	 * @param etudiantAnnee un étudiant année
	 * @return la liste des détail de paiement remboursables
	 */
	public List<IPaiementDetail> rechercherDetailsPaiementRemboursables(IEtudiantAnnee etudiantAnnee) {
		ArrayList<IPaiementDetail> listeDetailsPaiementRemboursables = new ArrayList<IPaiementDetail>();
		
		for (IPaiementDetail detail : calculerSyntheseDetailsPaiement(etudiantAnnee)) {
			if (detail.montantPaye() != null && detail.toParametragePaie().remboursement()) {
				listeDetailsPaiementRemboursables.add(detail);
			}
		}
		
		return listeDetailsPaiementRemboursables;
	}
	
	/**
	 * Recherche des détails de paiement payés pour l'étudiant.
	 * <p>
	 * Tous les articles payés au moins une fois sont retournés (même s'ils ont été remboursés).
	 * @param etudiantAnnee un étudiant année
	 * @return la liste des détail de paiement payés
	 */
	public List<IPaiementDetail> rechercherDetailsPaiementPayes(IEtudiantAnnee etudiantAnnee) {
		return calculerSyntheseDetailsPaiement(etudiantAnnee, true);
	}
}
