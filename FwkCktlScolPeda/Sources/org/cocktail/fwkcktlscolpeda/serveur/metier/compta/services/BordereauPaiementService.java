package org.cocktail.fwkcktlscolpeda.serveur.metier.compta.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.scolarite.Article;
import org.cocktail.fwkcktlcompta.server.scolarite.Paiement;
import org.cocktail.fwkcktlcompta.server.scolarite.PaiementException;
import org.cocktail.fwkcktlcompta.server.scolarite.PanierPaiement;
import org.cocktail.fwkcktlcompta.server.scolarite.PanierRemboursement;
import org.cocktail.fwkcktlcompta.server.scolarite.PrelevementSepaService;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOBordereau;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPlancoGestion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBordereau;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBordereau;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.TypeBordereauRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.TypeBordereauRepositoryEOF;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Cette classe de service permet de générer les bordereaux de paiement des étudiants dans la GFC.
 * 
 * @author Pascal MACOUIN
 */
public class BordereauPaiementService {

	private static final Logger LOG = Logger.getLogger(BordereauPaiementService.class);
	private EOEditingContext editingContext;
	private UserInfo userInfo;
	private TypeBordereauRepository typeBordereauRepository;
	private PaiementService paiementService;
	private org.cocktail.fwkcktlcompta.server.scolarite.PaiementService paiementServiceGfc;
	private PrelevementSepaService prelevementSepaService;
	
	/**
	 * Le constructeur.
	 * 
	 * @param editingContext Un editing context
	 * @param userInfo la personne effectuant la génération du bordereau
	 */
	public BordereauPaiementService(EOEditingContext editingContext, UserInfo userInfo) {
		this.editingContext = editingContext;
		this.userInfo = userInfo;
	}
	

	/**
	 * @param paiement paiement à valider
	 */
	public void validerPaiement(IPaiement paiement) {
		NSTimestamp now = new NSTimestamp();

		for (IPaiementDetail preDetail : paiement.toPaiementDetails()) {
			if (preDetail.montantAPayer() != null) {
				preDetail.setMontantPaye(preDetail.montantAPayer());
				preDetail.setDatePaye(now);
				preDetail.setMontantAPayer(null);
			}
			if (preDetail.montantARembourser() != null) {
				preDetail.setMontantRembourse(preDetail.montantARembourser());
				preDetail.setDateRembourse(now);
				preDetail.setMontantARembourser(null);
			}
		}

		paiement.setPaiementValide(true);
		paiement.setDatePaiement(now);
	}

	/**
	 * @param paiement paiement à annuler
	 */
	public void annulerPaiement(IPaiement paiement) {
		for (IPaiementDetail preDetail : paiement.toPaiementDetails()) {
			if (preDetail.montantPaye() != null) {
				preDetail.setMontantAPayer(preDetail.montantPaye());
				preDetail.setDatePaye(null);
				preDetail.setMontantPaye(null);
			}
			if (preDetail.montantRembourse() != null) {
				preDetail.setMontantARembourser(preDetail.montantRembourse());
				preDetail.setDateRembourse(null);
				preDetail.setMontantRembourse(null);
			}
		}

		paiement.setPaiementValide(false);
		paiement.setDatePaiement(null);
	}

	/**
	 * Genère un bordereau de paiement pour tous les paiements d'une journée.
	 * 
	 * @param codeRne le code RNE de l'établissement
	 * @param codeGestionEtablissement le code gestion de l'établissement
	 * @param anneeUniversitaire l'année universitaire en question 
	 * @param datePaiement la date de paiement
	 * @return le bordereau généré ou <code>null</code> si aucun paiement traité pour cette journée
	 * @throws BordereauPaiementException si une erreur survient pendant la génération du bordereau
	 */
	public IBordereau genererBordereauPaiement(String codeRne, String codeGestionEtablissement, Integer anneeUniversitaire, NSTimestamp datePaiement) throws BordereauPaiementException {

		LOG.info(String.format("Début de génération d'un bordereau de paiement étudiant pour codeRne=%s, codeGestionEtablissement=%s, datePaiement=%tF", codeRne, codeGestionEtablissement, datePaiement));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(datePaiement);
		int exercice = calendar.get(Calendar.YEAR);
		
		// On boucle sur tous les paiements positifs de la journée
		EOQualifier qualifierPaiement = EOPaiement.PAIEMENT_VALIDE.eq(true).and(EOPaiement.MONTANT.greaterThanOrEqualTo(BigDecimal.ZERO)).and(EOPaiement.DATE_PAIEMENT.eq(datePaiement));
		NSArray<EOPaiement> listePaiements = EOPaiement.fetchSco_Paiements(editingContext, qualifierPaiement, null);
		List<IScoPaiement> listePaiementsTraites = new ArrayList<IScoPaiement>(listePaiements.size());
		
		LOG.info(String.format("%s paiement(s) trouvé(s) pour la date du %tF", listePaiements.size(), datePaiement));

		PanierPaiement panier = new PanierPaiement(codeRne, codeGestionEtablissement, datePaiement);
		
		for (EOPaiement paiement : listePaiements) {
			// Gestion des articles
			List<Article> listeArticles = new ArrayList<Article>();
			BigDecimal montantArticles = BigDecimal.ZERO;

			for (EOPaiementDetail paiementDetail : paiement.toPaiementDetails()) {
				// On n'ajoute que les articles dont le montant payé/remboursé est différent de zéro
				BigDecimal montant = null;
				
				if (paiementDetail.montantPaye() != null) {
					montant = paiementDetail.montantPaye();
				} else if (paiementDetail.montantRembourse() != null) {
					montant = paiementDetail.montantRembourse().negate();
				}
				
				if (montant == null) {
					String message = String.format("Un des articles du paiement id %s n'a ni de montant payé ni de montant remboursé. Impossible de générer le bordereau.", paiement.id());
					LOG.info(message);
					throw new BordereauPaiementException(message);
				} else if (montant.compareTo(BigDecimal.ZERO) != 0) {
					montantArticles = montantArticles.add(montant);
					
					String planComptableArticle = getPlanComptable(paiementDetail);
					String codeGestionArticle = getCodeGestionPlanComptable(codeRne, planComptableArticle);
					Article article = new Article(montant, planComptableArticle, codeGestionArticle);
					
					listeArticles.add(article);
				}
			}
			
			// Gestion des moyens de paiement
			List<Paiement> listeMoyenPaiements = new ArrayList<Paiement>();
			BigDecimal montantPayeMoyenPaiements = BigDecimal.ZERO;
			
			for (IScoPaiementMoyen moyenPaiement : paiement.toPaiementMoyens()) {
				montantPayeMoyenPaiements = montantPayeMoyenPaiements.add(moyenPaiement.montantPaye());
				
				String planComptablePaiement = getPlanComptablePaiement(moyenPaiement.toModePaiement(), exercice);
				String codeGestionPaiement = getCodeGestionPlanComptable(codeRne, planComptablePaiement);
				
				Paiement paiementEtudiant;
				
				if (moyenPaiement.isComptant()) {
					paiementEtudiant = new Paiement(moyenPaiement.montantPaye(), planComptablePaiement, codeGestionPaiement);
				} else {
					// Si le paiement différé n'a pas d'échéancier ==problème=> On ne peut pas générer le bordereau
					if (moyenPaiement.toEcheancierSepaSdd() == null) {
						EOEtudiant etudiant = paiement.toEtudiantAnnee().toEtudiant();
						String message = String.format("Le paiement id %s de l'étudiant n° %s (%s %s) n'a pas d'échéancier. Impossible de générer le bordereau.",
								paiement.primaryKey(), etudiant.etudNumero(), etudiant.toIndividu().nomAffichage(), etudiant.toIndividu().prenomAffichage());
						LOG.info(message);
						throw new BordereauPaiementException(message);
					}
					paiementEtudiant = new Paiement(moyenPaiement.toEcheancierSepaSdd(), planComptablePaiement, codeGestionPaiement);
				}
				
				listeMoyenPaiements.add(paiementEtudiant);
			}
			
			// Si le total du paiement, le total des articles payés et le total des moyens de paiement sont différents => Erreur
			if (montantArticles.compareTo(montantPayeMoyenPaiements) != 0 || montantArticles.compareTo(paiement.montant()) != 0) {
				EOEtudiant etudiant = paiement.toEtudiantAnnee().toEtudiant();
				String message = String.format("Le total du paiement (%s), des articles payés (%s) et des moyens de paiement (%s) du paiement id %s sont différents. Impossible de générer le bordereau.",
						paiement.montant(), montantArticles, montantPayeMoyenPaiements, paiement.primaryKey(), etudiant.etudNumero(), etudiant.toIndividu().nomAffichage(), etudiant.toIndividu().prenomAffichage());
				LOG.info(message);
				throw new BordereauPaiementException(message);
			}
			
			// On ajoute dans le bordereau que des paiements avec des articles différents de zéro
			if (listeArticles.size() != 0) {
				try {
					panier.ajouter(listeArticles, listeMoyenPaiements);
				} catch (PaiementException e) {
					throw new BordereauPaiementException(e);
				}
			}
			
			listePaiementsTraites.add(paiement);
		}
		
		LOG.info(String.format("%s paiement(s) traité(s)", listePaiementsTraites.size()));
		
		IBordereau bordereau = null;
		
		// S'il y a quelque chose dans le panier, on génère le bordereau
		if (listePaiementsTraites.size() == 0) {
			LOG.info("Aucun paiement à traiter pour cette journée. Aucun bordereau généré.");
		} else {
			LOG.info("Création du bordereau de paiement étudiant...");
			org.cocktail.fwkcktlcompta.server.scolarite.PaiementService service = getPaiementServiceGfc();
			try {
				Integer numeroBordereau;
				if (panier.isExistePlancoAvecMontantDifferentDeZero()) {
					org.cocktail.fwkcktlcompta.common.entities.IBordereau bordereauPaiement = service.enregistrerPaiement(panier);
					numeroBordereau = bordereauPaiement.borNum();
					LOG.info(String.format("Ok - Bordereau de paiement GFC n° %s créé", numeroBordereau));
				} else {
					numeroBordereau = service.getNumeroBordereau(panier.getDatePaiement(), panier.getCodeRne());
					LOG.info("Tout les montants des plans comptable sont à zéros (aucun mouvement) - Pas de bordereau GFC généré");
				}
				
				bordereau = creerBordereauScolarite(getTypeBordereauRepository().getTypeBordereauPaiement(), codeGestionEtablissement, anneeUniversitaire, datePaiement, listePaiementsTraites, numeroBordereau);
				LOG.info(String.format("Ok - Bordereau de paiement scolarité n° %s créé", numeroBordereau));
			} catch (PaiementException e) {
				LOG.info(String.format("La création du bordereau de paiement a échoué : %s", e.getMessage()));
				throw new BordereauPaiementException(e);
			}
		}
		
		return bordereau;
	}

	/**
	 * Genère un bordereau de remboursement pour tous les remboursements en attente.
	 * 
	 * @param codeRne le code RNE de l'établissement
	 * @param codeGestionEtablissement le code gestion de l'établissement
	 * @param anneeUniversitaire l'année universitaire en question 
	 * @return le bordereau généré ou <code>null</code> si aucun paiement de remboursement traité
	 * @throws BordereauPaiementException si une erreur survient pendant la génération du bordereau
	 */
	public IBordereau genererBordereauRemboursement(String codeRne, String codeGestionEtablissement, Integer anneeUniversitaire) throws BordereauPaiementException {

		LOG.info(String.format("Début de génération d'un bordereau de remboursement étudiant pour codeRne=%s, codeGestionEtablissement=%s", codeRne, codeGestionEtablissement));
		org.cocktail.fwkcktlcompta.server.scolarite.PaiementService service = getPaiementServiceGfc();
		
		Calendar calendar = Calendar.getInstance();
		Date dateJour = DateCtrl.getDateOnly(calendar.getTime());
		int exercice = calendar.get(Calendar.YEAR);
		
		// Ensuite on boucle sur tous les paiements négatifs non validés
		EOQualifier qualifierRemboursement = EOPaiement.PAIEMENT_VALIDE.eq(false).and(EOPaiement.MONTANT.lessThan(BigDecimal.ZERO));
		List<IScoPaiement> listeRemboursements = new ArrayList<IScoPaiement>(EOPaiement.fetchSco_Paiements(editingContext, qualifierRemboursement, null));
		
		// Plus tous ceux d'un bordereau potentiel déjà existant
		IBordereau bordereau = EOBordereau.fetchSco_Bordereau(editingContext, getTypeBordereauRepository().getTypeBordereauRemboursement(), service.getNumeroBordereau(dateJour, codeRne), codeGestionEtablissement);
		if (bordereau != null) {
			// Si un bordereau existe déjà, on annule tous les paiements de ce bordereau
			for (IPaiement paiement : bordereau.toPaiements()) {
				annulerPaiement(paiement);
			}
			listeRemboursements.addAll(bordereau.toPaiements());
		}
		
		LOG.info(String.format("%s remboursement(s) trouvé(s)", listeRemboursements.size()));

		List<IScoPaiement> listeRemboursementsTraites = new ArrayList<IScoPaiement>(listeRemboursements.size());
		PanierRemboursement panier = new PanierRemboursement(codeRne, codeGestionEtablissement, dateJour);
		
		for (IScoPaiement paiement : listeRemboursements) {
			if (isRemboursementTardif(paiement, dateJour)) {
				continue;
			}
			
			// Gestion des articles
			List<Article> listeArticles = new ArrayList<Article>();
			BigDecimal montantArticles = BigDecimal.ZERO;

			for (IPaiementDetail paiementDetail : paiement.toPaiementDetails()) {
				// On n'ajoute que les articles dont le montant à rembourser/à payer est différent de zéro
				BigDecimal montant = null;
				
				if (paiementDetail.montantARembourser() != null) {
					montant = paiementDetail.montantARembourser().negate();
				} else if (paiementDetail.montantAPayer() != null) {
					montant = paiementDetail.montantAPayer();
				}
				
				if (montant == null) {
					String message = String.format("Un des articles du paiement id %s n'a ni de montant à rembourser ni de montant à payer. Impossible de générer le bordereau.", paiement.id());
					LOG.info(message);
					throw new BordereauPaiementException(message);
				} else if (montant.compareTo(BigDecimal.ZERO) != 0) {
					montantArticles = montantArticles.add(montant);
					
					String planComptableArticle = getPlanComptable(paiementDetail);
					String codeGestionArticle = getCodeGestionPlanComptable(codeRne, planComptableArticle);
					Article article = new Article(montant, planComptableArticle, codeGestionArticle);
					
					listeArticles.add(article);
				}
			}
			
			// Gestion des moyens de paiement
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			// Il doit y avoir un et un seul moyen de paiement pour un remboursement sinon erreur
			if (paiement.toScoPaiementMoyens().size() != 1) {
				String message = String.format("Il doit y avoir un et un seul moyen de paiement pour le paiement id %s. Impossible de générer le bordereau.", paiement.id());
				LOG.info(message);
				throw new BordereauPaiementException(message);
			}
			
			IScoPaiementMoyen moyenPaiement = paiement.toScoPaiementMoyens().get(0);
			String planComptablePaiement = getPlanComptableRemboursement(moyenPaiement.toModePaiement(), exercice);
			String codeGestionPaiement = getCodeGestionPlanComptable(codeRne, planComptablePaiement);
			
			Paiement paiementEtudiant = new Paiement(moyenPaiement.montantPaye(), planComptablePaiement, codeGestionPaiement);
			
			IFournis fournisseur = moyenPaiement.toFournisseurTitulaire();
			IRib ribFournisseur = moyenPaiement.toRibTitulaire();
			IAdresse adresseFournisseur = moyenPaiement.toAdresseTitulaire();
			String modeCodeRemboursement = moyenPaiement.toModePaiement().modCodeRemboursement();
			
			// Si le total du paiement, le total des articles à rembourser et le total des moyens de paiement sont différents => Erreur
			if (montantArticles.compareTo(moyenPaiement.montantPaye()) != 0 || montantArticles.compareTo(paiement.montant()) != 0) {
				IEtudiant etudiant = paiement.toScoEtudiantAnnee().toEtudiant();
				String message = String.format("Le total du paiement (%s), des articles à rembourser (%s) et des moyens de paiement (%s) du paiement id %s sont différents. Impossible de générer le bordereau.",
						paiement.montant(), montantArticles, moyenPaiement.montantPaye(), paiement.id(), etudiant.etudNumero(), etudiant.toIndividu().nomAffichage(), etudiant.toIndividu().prenomAffichage());
				LOG.info(message);
				throw new BordereauPaiementException(message);
			}
			
			// Il faut un fournisseur
			if (fournisseur == null || ribFournisseur == null | adresseFournisseur == null) {
				String message = String.format("Aucun fournisseur (avec son RIB et son adresse) trouvé sur un des moyens de paiement du paiement id %s. Impossible de générer le bordereau.",
						paiement.id());
				LOG.info(message);
				throw new BordereauPaiementException(message);
			}
			
			try {
				panier.ajouter(paiement.id(), fournisseur, ribFournisseur, adresseFournisseur, modeCodeRemboursement, listeArticles, paiementEtudiant);
				listeRemboursementsTraites.add(paiement);
			} catch (PaiementException e) {
				throw new BordereauPaiementException(e);
			}
			
			// On valide le paiement
			validerPaiement(paiement);
		}
		
		LOG.info(String.format("%s remboursement(s) traité(s)", listeRemboursementsTraites.size()));
		
		IBordereau nouveauBordereau = null;
		
		// S'il y a quelque chose dans le panier, on génère le bordereau
		if (listeRemboursementsTraites.size() == 0) {
			LOG.info("Aucun remboursement à traiter. Aucun bordereau généré.");
		} else {
			LOG.info("Création du bordereau de remboursement étudiant...");
			try {
				org.cocktail.fwkcktlcompta.common.entities.IBordereau bordereauRemboursement = service.enregistrerRemboursement(panier);
				LOG.info(String.format("Ok - Bordereau de remboursement GFC n° %s créé", bordereauRemboursement.borNum()));
				
				nouveauBordereau = creerBordereauScolarite(getTypeBordereauRepository().getTypeBordereauRemboursement(), codeGestionEtablissement, anneeUniversitaire, dateJour, listeRemboursementsTraites, bordereauRemboursement.borNum());
				LOG.info(String.format("Ok - Bordereau de remboursement scolarité n° %s créé", bordereauRemboursement.borNum()));
			} catch (PaiementException e) {
				LOG.info(String.format("La création du bordereau de remboursement a échoué : %s", e.getMessage()));
				throw new BordereauPaiementException(e);
			}
		}
		
		return nouveauBordereau;
	}

	/**
	 * Annuler le bordereau si non visé.
	 * 
	 * @param bordereau le bordereau à annuler
	 * @throws BordereauPaiementException si une erreur survient pendant la génération du bordereau
	 */
	public void annulerBordereau(IBordereau bordereau) throws BordereauPaiementException {
		
		LOG.info(String.format("Début annulation d'un bordereau, type=%s, numéro bordereau=%s, codeGestionEtablissement=%s, dateEnvoi=%tF",
				bordereau.toTypeBordereau().libelleCourt(), bordereau.numeroBordereau() , bordereau.codeGestion(), bordereau.dateEnvoiCompta()));

		// On va supprimer le bordereau dans la gfc
		LOG.info("Suppression du bordereau dans la GFC...");
		try {
			org.cocktail.fwkcktlcompta.common.entities.IBordereau bordereauGfc = getBordereauGfc(bordereau);
			if (bordereauGfc != null) {
				getPaiementServiceGfc().supprimerBordereau(bordereauGfc);
				LOG.info(String.format("Ok - Bordereau GFC n° %s supprimé", bordereauGfc.borNum()));
			} else {
				LOG.info("Pas de bordereau GFC à supprimer");
			}
			
			supprimerBordereauScolarite(bordereau);
			LOG.info(String.format("Ok - Bordereau scolarité n° %s supprimé", bordereau.numeroBordereau()));
		} catch (PaiementException e) {
			LOG.info(String.format("La suppression du bordereau a échoué : %s", e.getMessage()));
			throw new BordereauPaiementException(e);
		}
	}
	
	/**
	 * Création ou mise à jour du bordereau de paiement de la scolarité.
	 * 
	 * @param codeGestionEtablissement le code de gestion pour l'établissement
	 * @param anneeUniversitaire l'année universitaire
	 * @param datePaiement la date de paiement
	 * @param listePaiements la liste des paiements contenues dans ce bordereau
	 * @param bordereauPaiement le bordereau de paiement GFC
	 * @param typeBordereau le type de bordereau
	 * @return un bordereau scolarité
	 */
	private IBordereau creerBordereauScolarite(ITypeBordereau typeBordereau, String codeGestionEtablissement, Integer anneeUniversitaire, Date datePaiement, List<IScoPaiement> listePaiements, Integer numeroBordereau) {
		
		// On recherche le bordereau scol (ou on le crée s'il n'existe pas), et on rattache tous les paiements traités à ce bordereau
		IBordereau bordereau = EOBordereau.fetchSco_Bordereau(editingContext, typeBordereau, numeroBordereau, codeGestionEtablissement);
		
		if (bordereau != null) {
			// On détache tous les paiements de ce bordereau (en théorie, on rattachera les mêmes plus de nouveaux)
			for (IScoPaiement paiement : new ArrayList<IScoPaiement>(bordereau.toPaiements())) {
				paiement.setToBordereauRelationship(null);
			}
		} else {
			bordereau = EOBordereau.createAndInsertInstance(editingContext);
			bordereau.setToTypeBordereauRelationship(typeBordereau);
			bordereau.setAnneeUniversitaire(anneeUniversitaire);
			bordereau.setCodeGestion(codeGestionEtablissement);
			bordereau.setNumeroBordereau(numeroBordereau);
			bordereau.setDatePaiement(new NSTimestamp(datePaiement));
		}
		
		bordereau.setDateEnvoiCompta(new NSTimestamp());
		
		for (IScoPaiement paiementTraite : listePaiements) {
			paiementTraite.setToBordereauRelationship(bordereau);
		}
		
		return bordereau;
	}

	/**
	 * Retourne le plan comptable de paiement pour ce mode de paiement et cet exercice.
	 * 
	 * @param modePaiement un mode de paiement
	 * @param exercice un exercice
	 * @return le plan comptable
	 * @throws BordereauPaiementException si aucun plan comptable n'est trouvé
	 */
	private String getPlanComptablePaiement(IModePaiement modePaiement, int exercice) throws BordereauPaiementException {
		String planComptable = modePaiement.getPlanComptablePaiementAUtilisePourExercice(exercice);
		
		if (planComptable == null) {
			String message = String.format("Aucun plan comptable de paiement trouvé pour le mode de paiement \"%s (%s)\" et l'exercice \"%s\"", modePaiement.code(), modePaiement.libelle(), exercice);
			LOG.warn(message);
			throw new BordereauPaiementException(message);
		}
		
		return planComptable;
	}
	
	/**
	 * Retourne le plan comptable de remboursement pour ce mode de paiement et cet exercice.
	 * 
	 * @param modePaiement un mode de paiement
	 * @param exercice un exercice
	 * @return le plan comptable
	 * @throws BordereauPaiementException si aucun plan comptable n'est trouvé
	 */
	private String getPlanComptableRemboursement(IModePaiement modePaiement, int exercice) throws BordereauPaiementException {
		String planComptable = modePaiement.getPlanComptableRemboursementAUtilisePourExercice(exercice);
		
		if (planComptable == null) {
			String message = String.format("Aucun plan comptable de remboursement trouvé pour le mode de paiement \"%s (%s)\" et l'exercice \"%s\"", modePaiement.code(), modePaiement.libelle(), exercice);
			LOG.warn(message);
			throw new BordereauPaiementException(message);
		}
		
		return planComptable;
	}
	
	/**
	 * Retourne le plan comptable du détail paiement (de l'article de paiement).
	 * @param paiementDetail le détail
	 * @return un plan comptable
	 * @throws BordereauPaiementException si aucun plan comptable n'est trouvé
	 */
	private String getPlanComptable(IPaiementDetail paiementDetail) throws BordereauPaiementException {
		IParametragePaie parametragePaiement;
		String planComptable;
		
		if (paiementDetail.toParametragePaieFormation() != null) {
			parametragePaiement = paiementDetail.toParametragePaieFormation();
		} else if (paiementDetail.toParametragePaieDiplome() != null) {
			parametragePaiement = paiementDetail.toParametragePaieDiplome();
		} else {
			parametragePaiement = paiementDetail.toParametragePaieArticleComplementaire();
		}
		
		planComptable = parametragePaiement.comptabilite();
		
		if (planComptable == null) {
			String message = String.format("Aucun plan comptable renseigné pour l'article de paiement \"%s\"", parametragePaiement.libelleComplet());
			LOG.warn(message);
			throw new BordereauPaiementException(message);
		}
		
		return planComptable;
	}
	
	/**
	 * Retourne le code de gestion du plan comptable pour un établissement.
	 * 
	 * @param codeRne le code RNE de l'établissement
	 * @param pcoNum le plan comptable
	 * @return le code gestion
	 * @throws BordereauPaiementException si aucun code gestion n'est trouvé
	 */
	private String getCodeGestionPlanComptable(String codeRne, String pcoNum) throws BordereauPaiementException {
		ERXKeyValueQualifier qualifierEtablissement = EOPlancoGestion.TO_RNE.dot(EORne.C_RNE).eq(codeRne);
		ERXKeyValueQualifier qualifierPlancomptable = EOPlancoGestion.TO_PLAN_COMPTABLE.dot(EOPlanComptable.PCO_NUM).eq(pcoNum);
		
		EOPlancoGestion planComptable = EOPlancoGestion.fetchSco_PlancoGestion(editingContext, ERXQ.and(qualifierEtablissement, qualifierPlancomptable));
		
		if (planComptable == null) {
			planComptable = EOPlancoGestion.fetchSco_PlancoGestion(editingContext, qualifierPlancomptable.and(EOPlancoGestion.TO_RNE.isNull()));
		}
		
		if (planComptable == null) {
			String message = String.format("Aucun code gestion trouvé pour le codeRne \"%s\" et pcoNum \"%s\" (ou pcoNum \"%s\" seul sans RNE) dans la table SCO_SCOLARITE.PLANCO_GESTION", codeRne, pcoNum, pcoNum);
			LOG.warn(message);
			throw new BordereauPaiementException(message);
		}
		
		return planComptable.gesCode(); 
	}
	
	/**
	 * Retourne le code état d'un bordereau de paiement GFC correspondant à ce bordereau (ou <code>null</code> si le bordereau n'existe pas).
	 * 
	 * @param bordereau un bordereau
	 * @return le code état du bordereau GFC correspondant
	 */
	public String getCodeEtatBordereauGfc(IBordereau bordereau) {
		try {
			org.cocktail.fwkcktlcompta.common.entities.IBordereau bordereauCompta;
			
			bordereauCompta = getBordereauGfc(bordereau);
			
			String codeEtat = null;
			if (bordereauCompta != null) {
				codeEtat = bordereauCompta.borEtat();
			}
			
			return codeEtat;
		} catch (PaiementException e) {
			LOG.error(e.getMessage());
			return "ERREUR";
		}
	}

	/**
	 * Si le bordereau GFC existe, il doit être visé.
	 * 
	 * @param bordereau Un borderau Scol
	 * @return <code>true</code> si le bordereau GFC n'existe pas ou qu'il est visé
	 */
	public boolean isBordereauGfcViseSilExiste(IBordereau bordereau) {
		String codeEtatBordereauGfc = getCodeEtatBordereauGfc(bordereau);
		
		return codeEtatBordereauGfc == null || org.cocktail.fwkcktlcompta.server.metier.EOBordereau.BOR_ETAT_VISE.equals(codeEtatBordereauGfc);
	}
	
	/**
	 * @param paiement Un paiement de type remboursement
	 * @param date Une date de génération du bordereau de remboursement
	 * @return <code>true</code> si le remboursement ne peut pas encore être pris en compte à cette date
	 */
	private boolean isRemboursementTardif(IScoPaiement paiement, Date date) {
		LocalDate dateReference = new LocalDate(date);
		boolean isRemboursementTardif = false;
		
		List<IPaiementDetail> listeDetailsPaiementPayes = getPaiementService().rechercherDetailsPaiementPayes(paiement.toEtudiantAnnee());
		
		for (IScoPaiementDetail detail : paiement.toScoPaiementDetails()) {
			if (detail.montantARembourser() != null) {
				for (IPaiementDetail paiementDetailPaye : listeDetailsPaiementPayes) {
					if (paiementDetailPaye.toParametragePaie().equals(detail.toParametragePaie())) {
						ISepaSddEcheance derniereEcheance = rechercherDerniereEcheance((IScoPaiement) paiementDetailPaye.toPaiement());
						
						if (isEcheanceApres(derniereEcheance, dateReference)) {
							isRemboursementTardif = true;
							break;
						}
					}
				}
				
			}
		}

		return isRemboursementTardif;
	}
	
	/**
	 * @param paiement Un paiement
	 * @return La dernière échéance du paiement
	 */
	public ISepaSddEcheance rechercherDerniereEcheance(IScoPaiement paiement) {
		if (paiement == null) {
			return null;
		}
		
		IScoPaiementMoyen paiementMoyenPrelevement = paiement.toScoPaiementMoyenPrelevement();
		ISepaSddEcheance echeance = null;
		
		if (paiementMoyenPrelevement != null) {
			echeance = getPrelevementSepaService(null).rechercherDerniereEcheance(paiementMoyenPrelevement.toEcheancierSepaSdd());
		}
		
		return echeance;
	}
	
	/**
	 * @param echeance Une échéance
	 * @param dateReference Une date de référence
	 * @return <code>true</code> si la date de l'échéanceest après la date de référence
	 */
	public boolean isEcheanceApres(ISepaSddEcheance echeance, LocalDate dateReference) {
		boolean isEcheanceApres = false;
		
		if (echeance != null) {
			LocalDate dateEcheance = SepaSddEcheanceHelper.getSharedInstance().getDateEcheance(echeance);
			if (dateReference.isBefore(dateEcheance)) {
				isEcheanceApres = true;
			}
		}
		
		return isEcheanceApres;
	}
	
	private PrelevementSepaService getPrelevementSepaService(Integer idSepaParam) {
		if (prelevementSepaService == null) {
			prelevementSepaService = new PrelevementSepaService(userInfo.persId().intValue(), idSepaParam, editingContext);
		}
		
		return prelevementSepaService;
	}
	
	/**
	 * @param servicePaiement Le service compta
	 * @param bordereau Le bordereau
	 * @return Le bordereau GFC si trouvé
	 * @throws PaiementException
	 */
	private org.cocktail.fwkcktlcompta.common.entities.IBordereau getBordereauGfc(IBordereau bordereau) throws PaiementException {
		if (bordereau == null) {
			return null;
		}
		
		org.cocktail.fwkcktlcompta.common.entities.IBordereau bordereauGfc;
		
		if (bordereau.toTypeBordereau().isPaiement()) {
			bordereauGfc = getPaiementServiceGfc().getBordereauPaiement(bordereau.numeroBordereau(), bordereau.codeGestion());
		} else {
			bordereauGfc = getPaiementServiceGfc().getBordereauRemboursement(bordereau.numeroBordereau(), bordereau.codeGestion());
		}
		
		return bordereauGfc;
	}
	
	private void supprimerBordereauScolarite(IBordereau bordereau) {
		// Pour un bordereau de remboursement, on annule les paiements attachés à ce bordereau
		if (bordereau.toTypeBordereau().isRemboursement()) {
			for (IPaiement paiement : bordereau.toPaiements()) {
				annulerPaiement(paiement);
			}
		}
		
		editingContext.deleteObject((EOEnterpriseObject) bordereau);
	}
	
	private TypeBordereauRepository getTypeBordereauRepository() {
		if (typeBordereauRepository == null) {
			typeBordereauRepository = new TypeBordereauRepositoryEOF(editingContext);
		}
		return typeBordereauRepository;
	}

	private PaiementService getPaiementService() {
		if (paiementService == null) {
			paiementService = new PaiementService();
		}
		
		return paiementService;
	}
	
	private org.cocktail.fwkcktlcompta.server.scolarite.PaiementService getPaiementServiceGfc() {
		if (paiementServiceGfc == null) {
			paiementServiceGfc = new org.cocktail.fwkcktlcompta.server.scolarite.PaiementService(userInfo, editingContext);
		}
		
		return paiementServiceGfc;
	}
}
