// DO NOT EDIT.  Make changes to EOGdStrategie.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOGdStrategie extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_GdStrategie";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOGdStrategie.class);

  public EOGdStrategie localInstanceIn(EOEditingContext editingContext) {
    EOGdStrategie localInstance = (EOGdStrategie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOGdStrategie.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOGdStrategie.LOG.isDebugEnabled()) {
    	_EOGdStrategie.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdStrategie.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOGdStrategie.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOGdStrategie.LOG.isDebugEnabled()) {
    	_EOGdStrategie.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdStrategie.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOGdStrategie.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOGdStrategie.LOG.isDebugEnabled()) {
    	_EOGdStrategie.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdStrategie.LIBELLE_KEY);
  }


  public static EOGdStrategie createSco_GdStrategie(EOEditingContext editingContext, String code
, Integer id
, String libelle
) {
    EOGdStrategie eo = (EOGdStrategie) EOUtilities.createAndInsertInstance(editingContext, _EOGdStrategie.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOGdStrategie> fetchSpec() {
    return new ERXFetchSpecification<EOGdStrategie>(_EOGdStrategie.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOGdStrategie> fetchAllSco_GdStrategies(EOEditingContext editingContext) {
    return _EOGdStrategie.fetchAllSco_GdStrategies(editingContext, null);
  }

  public static NSArray<EOGdStrategie> fetchAllSco_GdStrategies(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGdStrategie.fetchSco_GdStrategies(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGdStrategie> fetchSco_GdStrategies(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOGdStrategie> fetchSpec = new ERXFetchSpecification<EOGdStrategie>(_EOGdStrategie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGdStrategie> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOGdStrategie fetchSco_GdStrategie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGdStrategie.fetchSco_GdStrategie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGdStrategie fetchSco_GdStrategie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGdStrategie> eoObjects = _EOGdStrategie.fetchSco_GdStrategies(editingContext, qualifier, null);
    EOGdStrategie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_GdStrategie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGdStrategie fetchRequiredSco_GdStrategie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGdStrategie.fetchRequiredSco_GdStrategie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGdStrategie fetchRequiredSco_GdStrategie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGdStrategie eoObject = _EOGdStrategie.fetchSco_GdStrategie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_GdStrategie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGdStrategie localInstanceIn(EOEditingContext editingContext, EOGdStrategie eo) {
    EOGdStrategie localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
