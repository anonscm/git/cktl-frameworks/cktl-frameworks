package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * {@inheritDoc}
 */
public class EOTypePeriode extends _EOTypePeriode implements ITypePeriode {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypePeriode.class);

	// Private fields
	private static final long serialVersionUID = 1L;
	private static ERXEnterpriseObjectCache<EOTypePeriode> typePeriodeCache;

	// Public fields
	// Properties
	/**
	 * @return le cache des {@link EOTypePeriode} par leur nom
	 */
	public static ERXEnterpriseObjectCache<EOTypePeriode> getTypePeriodeCache() {
		if (typePeriodeCache == null) {
			typePeriodeCache = new ERXEnterpriseObjectCache<EOTypePeriode>(EOTypePeriode.class, CODE_KEY);
		}
		return typePeriodeCache;
	}

	/**
	 * @param typePeriodeCache le cache a setter
	 */
	public static void setTypePeriodeCache(ERXEnterpriseObjectCache<EOTypePeriode> typePeriodeCache) {
		EOTypePeriode.typePeriodeCache = typePeriodeCache;
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypePeriode} "annee"
	 */
	public static EOTypePeriode typePeriodeAnnee(EOEditingContext edc) {
		return getTypePeriodeCache().objectForKey(edc, CODE_ANNEE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypePeriode} "semestre"
	 */
	public static EOTypePeriode typePeriodeSemestre(EOEditingContext edc) {
		return getTypePeriodeCache().objectForKey(edc, CODE_SEMESTRE);
	}

}
