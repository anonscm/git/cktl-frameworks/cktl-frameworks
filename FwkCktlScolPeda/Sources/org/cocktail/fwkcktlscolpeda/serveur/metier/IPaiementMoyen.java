package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;

import com.webobjects.foundation.NSTimestamp;

/**
 * Super interface pour un moyen de paiement qu'il soit SCO_SCOLARITE ou SCO_PRE_SCOLARITE.
 * 
 * @author Pascal MACOUIN
 */
public interface IPaiementMoyen {

	/**
	 * @return paiement
	 */
	IPaiement toPaiement();

	/**
	 * @param value paiement
	 */
	void setToPaiementRelationship(IPaiement value);
	
	/**
	 * @return la liste des échéances du paiement
	 */
	List<? extends IPaiementEcheance> toPaiementEcheances();

	/**
	 * @return la liste des échéances triées par date d'échéances
	 */
	List<? extends IPaiementEcheance> toPaiementEcheancesTriees();

	/**
	 * @return mode de paiement
	 */
	IModePaiement toModePaiement();

	/**
	 * @param value mode de paiement
	 */
	void setToModePaiementRelationship(IModePaiement value);

	/**
	 * @return type de paiement
	 */
	ITypePaiement toTypePaiement();

	/**
	 * @param value type de paiement
	 */
	void setToTypePaiementRelationship(ITypePaiement value);
	
    /**
     * @return <code>true</code> s'il s'agit d'un prepaiement différé
     */
    boolean isDiffere();
    
    /**
     * @return <code>true</code> s'il s'agit d'un prepaiement comptant
     */
    boolean isComptant();
    
    /**
     * @return <code>true</code> s'il s'agit d'un paiement par chèque
     */
    boolean isCheque();
    
    /**
     * @return <code>true</code> s'il s'agit d'un paiement par virement
     */
    boolean isVirement();
    
    /**
     * @return <code>true</code> s'il s'agit d'un paiement par prélevement
     */
    boolean isPrelevement();

    /**
     * @return <code>true</code> s'il s'agit d'un paiement
     */
    boolean isPaiement();
    
    /**
     * @return <code>true</code> s'il s'agit d'un remboursement
     */
    boolean isRemboursement();
    
	/**
	 * @return le montant payé avec ce moyen de paiement
	 */
	BigDecimal montantPaye();

	/**
	 * Affecte le montant payé avec ce moyen de paiement
	 * @param unMontant un montant
	 */
	void setMontantPaye(BigDecimal unMontant);

    /**
     * @return l'adresse existante du titulaire du rib
     */
    IAdresse toAdresseTitulaire();
    
    /**
     * Copie les informations du titulaire du compte.
     * @param moyenPaiementSource Le moyen de paiement depuis lequel on récupère les informations
     */
    void copierInformationsTitulaireCompteDepuis(IPaiementMoyen moyenPaiementSource);
    
	// *********************************************
	// Données du chéque pour un paiement par chèque
	// *********************************************
	/**
	 * @return code de la banque sur laquelle est tirée le paiement
	 */
	String codeBanque();

	/**
	 * @param value code de la banque sur laquelle est tirée le paiement
	 */
	void setCodeBanque(String value);

	/**
	 * @return nom de la personne qui a fait le chèque
	 */
	String nomTireur();

	/**
	 * @param value nom de la personne qui a fait le chèque
	 */
	void setNomTireur(String value);

	/**
	 * @return prénom de la personne qui a fait le chèque
	 */
	String prenomTireur();

	/**
	 * @param value prénom de la personne qui a fait le chèque
	 */
	void setPrenomTireur(String value);

	/**
	 * @return numéro du chèque
	 */
	String numeroCheque();

	/**
	 * @param value numéro du chèque
	 */
	void setNumeroCheque(String value);

	/**
	 * @return numéro du compte relatif au chèque émis
	 */
	String numeroCompte();

	/**
	 * @param value numéro du compte relatif au chèque émis
	 */
	void setNumeroCompte(String value);

	// ************************************************************
	// Données du compte à débiter pour un paiement par prélèvement
	// ************************************************************
    /**
     * @return l'iban correspondant au rib si paiement différé
     */
    String iban();
    
    /**
     * @param iban l'iban à mettre à jour
     */
    void setIban(String iban);
    
    /**
     * @return le bic correspondant au rib si paiement différé
     */
    String bic();
    
    /**
     * @param bic le bic à mettre à jour
     */
    void setBic(String bic);
    
    /**
     * @return true si l'étudiant est le titulaire du rib
     */
    Boolean etudiantTitulaire();
    
    /**
     * @param value le flag
     */
    void setEtudiantTitulaire(Boolean value);
    
    /**
     * @return true si les infos minimales nécessaires au rib sont présentes
     */
    boolean verifierInfosMinimalesRib();

	// ***************
	// Données d'audit
	// ***************
	/**
	 * @return date de creation de l'enregistrement
	 */
	NSTimestamp dCreation();

	/**
	 * @param value date de creation de l'enregistrement
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return date de modification du paiement
	 */
	NSTimestamp dModification();

	/**
	 * @param value date de modification du paiement
	 */
	void setDModification(NSTimestamp value);

	/**
	 * @return personne qui a créé l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return personne qui a modifié l'enregistrement
	 */
	Integer persIdModification();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdModification(Integer value);
}
