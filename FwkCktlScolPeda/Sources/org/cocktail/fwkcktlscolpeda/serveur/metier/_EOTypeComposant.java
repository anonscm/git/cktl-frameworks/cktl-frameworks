// DO NOT EDIT.  Make changes to EOTypeComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeComposant";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> NOM = new ERXKey<String>("nom");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> DESCRIPTIF_THEMES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme>("descriptifThemes");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> REGLES_LIAISONS_ALLOWED_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>("reglesLiaisonsAllowedChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> REGLES_LIAISONS_ALLOWED_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>("reglesLiaisonsAllowedParents");

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NOM_KEY = NOM.key();
  // Relationships
  public static final String DESCRIPTIF_THEMES_KEY = DESCRIPTIF_THEMES.key();
  public static final String REGLES_LIAISONS_ALLOWED_CHILDS_KEY = REGLES_LIAISONS_ALLOWED_CHILDS.key();
  public static final String REGLES_LIAISONS_ALLOWED_PARENTS_KEY = REGLES_LIAISONS_ALLOWED_PARENTS.key();

  private static Logger LOG = Logger.getLogger(_EOTypeComposant.class);

  public EOTypeComposant localInstanceIn(EOEditingContext editingContext) {
    EOTypeComposant localInstance = (EOTypeComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeComposant.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
    	_EOTypeComposant.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeComposant.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeComposant.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
    	_EOTypeComposant.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeComposant.LIBELLE_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(_EOTypeComposant.NOM_KEY);
  }

  public void setNom(String value) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
    	_EOTypeComposant.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeComposant.NOM_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> descriptifThemes() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme>)storedValueForKey(_EOTypeComposant.DESCRIPTIF_THEMES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> descriptifThemes(EOQualifier qualifier) {
    return descriptifThemes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> descriptifThemes(EOQualifier qualifier, boolean fetch) {
    return descriptifThemes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> descriptifThemes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme.TYPE_COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme.fetchSco_DescriptifThemes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = descriptifThemes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDescriptifThemes(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
  }

  public void removeFromDescriptifThemes(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
  }

  public void addToDescriptifThemesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("adding " + object + " to descriptifThemes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDescriptifThemes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
    }
  }

  public void removeFromDescriptifThemesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("removing " + object + " from descriptifThemes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDescriptifThemes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme createDescriptifThemesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme) eo;
  }

  public void deleteDescriptifThemesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.DESCRIPTIF_THEMES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDescriptifThemesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifTheme> objects = descriptifThemes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDescriptifThemesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedChilds() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)storedValueForKey(_EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedChilds(EOQualifier qualifier) {
    return reglesLiaisonsAllowedChilds(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedChilds(EOQualifier qualifier, boolean fetch) {
    return reglesLiaisonsAllowedChilds(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedChilds(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.ALLOWED_CHILD_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.fetchSco_RegleLiaisons(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reglesLiaisonsAllowedChilds();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReglesLiaisonsAllowedChilds(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
  }

  public void removeFromReglesLiaisonsAllowedChilds(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
  }

  public void addToReglesLiaisonsAllowedChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("adding " + object + " to reglesLiaisonsAllowedChilds relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToReglesLiaisonsAllowedChilds(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
    }
  }

  public void removeFromReglesLiaisonsAllowedChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("removing " + object + " from reglesLiaisonsAllowedChilds relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromReglesLiaisonsAllowedChilds(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison createReglesLiaisonsAllowedChildsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison) eo;
  }

  public void deleteReglesLiaisonsAllowedChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_CHILDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReglesLiaisonsAllowedChildsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> objects = reglesLiaisonsAllowedChilds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReglesLiaisonsAllowedChildsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedParents() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)storedValueForKey(_EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedParents(EOQualifier qualifier) {
    return reglesLiaisonsAllowedParents(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedParents(EOQualifier qualifier, boolean fetch) {
    return reglesLiaisonsAllowedParents(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisonsAllowedParents(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.ALLOWED_PARENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.fetchSco_RegleLiaisons(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reglesLiaisonsAllowedParents();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReglesLiaisonsAllowedParents(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
  }

  public void removeFromReglesLiaisonsAllowedParents(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
  }

  public void addToReglesLiaisonsAllowedParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("adding " + object + " to reglesLiaisonsAllowedParents relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToReglesLiaisonsAllowedParents(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
    }
  }

  public void removeFromReglesLiaisonsAllowedParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeComposant.LOG.isDebugEnabled()) {
      _EOTypeComposant.LOG.debug("removing " + object + " from reglesLiaisonsAllowedParents relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromReglesLiaisonsAllowedParents(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison createReglesLiaisonsAllowedParentsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison) eo;
  }

  public void deleteReglesLiaisonsAllowedParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeComposant.REGLES_LIAISONS_ALLOWED_PARENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReglesLiaisonsAllowedParentsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> objects = reglesLiaisonsAllowedParents().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReglesLiaisonsAllowedParentsRelationship(objects.nextElement());
    }
  }


  public static EOTypeComposant createSco_TypeComposant(EOEditingContext editingContext, Integer id
, String libelle
, String nom
) {
    EOTypeComposant eo = (EOTypeComposant) EOUtilities.createAndInsertInstance(editingContext, _EOTypeComposant.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setNom(nom);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeComposant> fetchSpec() {
    return new ERXFetchSpecification<EOTypeComposant>(_EOTypeComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeComposant> fetchAllSco_TypeComposants(EOEditingContext editingContext) {
    return _EOTypeComposant.fetchAllSco_TypeComposants(editingContext, null);
  }

  public static NSArray<EOTypeComposant> fetchAllSco_TypeComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeComposant.fetchSco_TypeComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeComposant> fetchSco_TypeComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeComposant> fetchSpec = new ERXFetchSpecification<EOTypeComposant>(_EOTypeComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeComposant fetchSco_TypeComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeComposant.fetchSco_TypeComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeComposant fetchSco_TypeComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeComposant> eoObjects = _EOTypeComposant.fetchSco_TypeComposants(editingContext, qualifier, null);
    EOTypeComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeComposant fetchRequiredSco_TypeComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeComposant.fetchRequiredSco_TypeComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeComposant fetchRequiredSco_TypeComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeComposant eoObject = _EOTypeComposant.fetchSco_TypeComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeComposant localInstanceIn(EOEditingContext editingContext, EOTypeComposant eo) {
    EOTypeComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
