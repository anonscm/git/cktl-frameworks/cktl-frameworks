package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPaiementDetail extends _EOPaiementDetail implements IScoPaiementDetail {
	private static final long serialVersionUID = -3844928289141743389L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPaiementDetail.class);
	
	@Inject
	@Nullable
	private UserInfo userInfo;
	
	/**
	 * {@inheritDoc}
	 */
	public void setToPaiementRelationship(IPaiement unPaiement) {
		super.setToPaiementRelationship((EOPaiement) unPaiement);
	}

	/**
	 * {@inheritDoc}
	 */
	public IParametragePaie toParametragePaie() {
		if (toParametragePaieFormation() != null) {
			return toParametragePaieFormation();
		} else if (toParametragePaieDiplome() != null) {
			return toParametragePaieDiplome();
		} else if (toParametragePaieArticleComplementaire() != null) {
			return toParametragePaieArticleComplementaire();
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToParametragePaieArticleComplementaireRelationship(IParametragePaieArticleComplementaire unParametragePaieArticleComplementaire) {
		super.setToParametragePaieArticleComplementaireRelationship((EOParametragePaieArticleComplementaire) unParametragePaieArticleComplementaire);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToParametragePaieFormationRelationship(IParametragePaieFormation unParametragePaieFormation) {
		super.setToParametragePaieFormationRelationship((EOParametragePaieFormation) unParametragePaieFormation);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToParametragePaieDiplomeRelationship(IParametragePaieDiplome unParametragePaieDiplome) {
		super.setToParametragePaieDiplomeRelationship((EOParametragePaieDiplome) unParametragePaieDiplome);
	}

	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de paiement
	 */
	@Override
	public void setDatePaye(NSTimestamp date) {
		if (date == null) {
			super.setDatePaye(null);
		} else {
			super.setDatePaye(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}

	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de remboursement
	 */
	@Override
	public void setDateRembourse(NSTimestamp date) {
		if (date == null) {
			super.setDateRembourse(null);
		} else {
			super.setDateRembourse(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}

	/**
	 * @return le libelle "complet" (une concaténation de libellés) de l'article de paiement
	 */
	public String getLibelle() {
		String libelle = "";
		
		if (toParametragePaieFormation() != null) {
			libelle = toParametragePaieFormation().libelleComplet();
		} else if (toParametragePaieDiplome() != null) {
			libelle = toParametragePaieDiplome().libelleComplet();
		} else if (toParametragePaieArticleComplementaire() != null) {
			libelle = toParametragePaieArticleComplementaire().libelleComplet();
		}
		
		return libelle;
	}

	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
