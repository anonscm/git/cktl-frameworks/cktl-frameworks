// DO NOT EDIT.  Make changes to EOTypeFormationAglae.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeFormationAglae extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeFormationAglae";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeFormationAglae.class);

  public EOTypeFormationAglae localInstanceIn(EOEditingContext editingContext) {
    EOTypeFormationAglae localInstance = (EOTypeFormationAglae)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeFormationAglae.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeFormationAglae.LOG.isDebugEnabled()) {
    	_EOTypeFormationAglae.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormationAglae.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeFormationAglae.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeFormationAglae.LOG.isDebugEnabled()) {
    	_EOTypeFormationAglae.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormationAglae.LIBELLE_KEY);
  }


  public static EOTypeFormationAglae createSco_TypeFormationAglae(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeFormationAglae eo = (EOTypeFormationAglae) EOUtilities.createAndInsertInstance(editingContext, _EOTypeFormationAglae.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeFormationAglae> fetchSpec() {
    return new ERXFetchSpecification<EOTypeFormationAglae>(_EOTypeFormationAglae.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeFormationAglae> fetchAllSco_TypeFormationAglaes(EOEditingContext editingContext) {
    return _EOTypeFormationAglae.fetchAllSco_TypeFormationAglaes(editingContext, null);
  }

  public static NSArray<EOTypeFormationAglae> fetchAllSco_TypeFormationAglaes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeFormationAglae.fetchSco_TypeFormationAglaes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeFormationAglae> fetchSco_TypeFormationAglaes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeFormationAglae> fetchSpec = new ERXFetchSpecification<EOTypeFormationAglae>(_EOTypeFormationAglae.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeFormationAglae> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeFormationAglae fetchSco_TypeFormationAglae(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormationAglae.fetchSco_TypeFormationAglae(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormationAglae fetchSco_TypeFormationAglae(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeFormationAglae> eoObjects = _EOTypeFormationAglae.fetchSco_TypeFormationAglaes(editingContext, qualifier, null);
    EOTypeFormationAglae eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeFormationAglae that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormationAglae fetchRequiredSco_TypeFormationAglae(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormationAglae.fetchRequiredSco_TypeFormationAglae(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormationAglae fetchRequiredSco_TypeFormationAglae(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeFormationAglae eoObject = _EOTypeFormationAglae.fetchSco_TypeFormationAglae(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeFormationAglae that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormationAglae localInstanceIn(EOEditingContext editingContext, EOTypeFormationAglae eo) {
    EOTypeFormationAglae localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
