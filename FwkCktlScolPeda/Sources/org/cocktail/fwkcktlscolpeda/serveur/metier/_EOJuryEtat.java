// DO NOT EDIT.  Make changes to EOJuryEtat.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOJuryEtat extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_JuryEtat";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOJuryEtat.class);

  public EOJuryEtat localInstanceIn(EOEditingContext editingContext) {
    EOJuryEtat localInstance = (EOJuryEtat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOJuryEtat.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOJuryEtat.LOG.isDebugEnabled()) {
    	_EOJuryEtat.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryEtat.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOJuryEtat.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOJuryEtat.LOG.isDebugEnabled()) {
    	_EOJuryEtat.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryEtat.LIBELLE_KEY);
  }


  public static EOJuryEtat createSco_JuryEtat(EOEditingContext editingContext, String code
, String libelle
) {
    EOJuryEtat eo = (EOJuryEtat) EOUtilities.createAndInsertInstance(editingContext, _EOJuryEtat.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOJuryEtat> fetchSpec() {
    return new ERXFetchSpecification<EOJuryEtat>(_EOJuryEtat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOJuryEtat> fetchAllSco_JuryEtats(EOEditingContext editingContext) {
    return _EOJuryEtat.fetchAllSco_JuryEtats(editingContext, null);
  }

  public static NSArray<EOJuryEtat> fetchAllSco_JuryEtats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJuryEtat.fetchSco_JuryEtats(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJuryEtat> fetchSco_JuryEtats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOJuryEtat> fetchSpec = new ERXFetchSpecification<EOJuryEtat>(_EOJuryEtat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJuryEtat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOJuryEtat fetchSco_JuryEtat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryEtat.fetchSco_JuryEtat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryEtat fetchSco_JuryEtat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJuryEtat> eoObjects = _EOJuryEtat.fetchSco_JuryEtats(editingContext, qualifier, null);
    EOJuryEtat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_JuryEtat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryEtat fetchRequiredSco_JuryEtat(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryEtat.fetchRequiredSco_JuryEtat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryEtat fetchRequiredSco_JuryEtat(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJuryEtat eoObject = _EOJuryEtat.fetchSco_JuryEtat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_JuryEtat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryEtat localInstanceIn(EOEditingContext editingContext, EOJuryEtat eo) {
    EOJuryEtat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
