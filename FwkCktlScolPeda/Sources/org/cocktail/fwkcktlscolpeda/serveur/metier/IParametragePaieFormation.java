package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.foundation.NSTimestamp;

public interface IParametragePaieFormation extends IParametragePaie {

	public Boolean boursier();

	public void setBoursier(Boolean value);

	public String comptabilite();

	public void setComptabilite(String value);

	public NSTimestamp dCreation();

	public void setDCreation(NSTimestamp value);

	public NSTimestamp dModification();

	public void setDModification(NSTimestamp value);

	public String libelleComplet();

	public java.math.BigDecimal montant();

	public void setMontant(java.math.BigDecimal value);

	public Integer persIdCreation();

	public void setPersIdCreation(Integer value);

	public Integer persIdModification();

	public void setPersIdModification(Integer value);

	public Boolean remboursement();

	public void setRemboursement(Boolean value);

	public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire();

	public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value);

	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure();

	public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value);

	public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation toTypeFormation();

	public void setToTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value);

	public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant toTypeMontant();

	public void setToTypeMontantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant value);

}