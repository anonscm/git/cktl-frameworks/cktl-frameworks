package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface ITypePaiement {
	String CODE_TYPE_COMPTANT = "CPT";
	String CODE_TYPE_DIFFERE = "DIF";

	/**
     * @return libelle du type de paiement
     */
    String libelle();
    
    /**
     * @return code du type de paiement
     */
    String code();

    /**
     * @return true s'il s'agit du type differe
     */
    boolean isDiffere();
    
    /**
     * @return true s'il s'agit du type comptant
     */
    boolean isComptant();
    
}
