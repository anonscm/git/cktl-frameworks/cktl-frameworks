// DO NOT EDIT.  Make changes to EOTypeFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeFormation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> HABILITABLE = new ERXKey<Integer>("habilitable");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae> TYPE_FORMATION_AGLAE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae>("typeFormationAglae");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String HABILITABLE_KEY = HABILITABLE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String TYPE_FORMATION_AGLAE_KEY = TYPE_FORMATION_AGLAE.key();

  private static Logger LOG = Logger.getLogger(_EOTypeFormation.class);

  public EOTypeFormation localInstanceIn(EOEditingContext editingContext) {
    EOTypeFormation localInstance = (EOTypeFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeFormation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeFormation.LOG.isDebugEnabled()) {
    	_EOTypeFormation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormation.CODE_KEY);
  }

  public Integer habilitable() {
    return (Integer) storedValueForKey(_EOTypeFormation.HABILITABLE_KEY);
  }

  public void setHabilitable(Integer value) {
    if (_EOTypeFormation.LOG.isDebugEnabled()) {
    	_EOTypeFormation.LOG.debug( "updating habilitable from " + habilitable() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormation.HABILITABLE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeFormation.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeFormation.LOG.isDebugEnabled()) {
    	_EOTypeFormation.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormation.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeFormation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeFormation.LOG.isDebugEnabled()) {
    	_EOTypeFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormation.LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae typeFormationAglae() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae)storedValueForKey(_EOTypeFormation.TYPE_FORMATION_AGLAE_KEY);
  }
  
  public void setTypeFormationAglae(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae value) {
    takeStoredValueForKey(value, _EOTypeFormation.TYPE_FORMATION_AGLAE_KEY);
  }

  public void setTypeFormationAglaeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae value) {
    if (_EOTypeFormation.LOG.isDebugEnabled()) {
      _EOTypeFormation.LOG.debug("updating typeFormationAglae from " + typeFormationAglae() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeFormationAglae(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormationAglae oldValue = typeFormationAglae();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOTypeFormation.TYPE_FORMATION_AGLAE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOTypeFormation.TYPE_FORMATION_AGLAE_KEY);
    }
  }
  

  public static EOTypeFormation createSco_TypeFormation(EOEditingContext editingContext, String code
, Integer id
, String libelle
) {
    EOTypeFormation eo = (EOTypeFormation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeFormation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeFormation> fetchSpec() {
    return new ERXFetchSpecification<EOTypeFormation>(_EOTypeFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeFormation> fetchAllSco_TypeFormations(EOEditingContext editingContext) {
    return _EOTypeFormation.fetchAllSco_TypeFormations(editingContext, null);
  }

  public static NSArray<EOTypeFormation> fetchAllSco_TypeFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeFormation.fetchSco_TypeFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeFormation> fetchSco_TypeFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeFormation> fetchSpec = new ERXFetchSpecification<EOTypeFormation>(_EOTypeFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeFormation fetchSco_TypeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormation.fetchSco_TypeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormation fetchSco_TypeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeFormation> eoObjects = _EOTypeFormation.fetchSco_TypeFormations(editingContext, qualifier, null);
    EOTypeFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormation fetchRequiredSco_TypeFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormation.fetchRequiredSco_TypeFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormation fetchRequiredSco_TypeFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeFormation eoObject = _EOTypeFormation.fetchSco_TypeFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormation localInstanceIn(EOEditingContext editingContext, EOTypeFormation eo) {
    EOTypeFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
