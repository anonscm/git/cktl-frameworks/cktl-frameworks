package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Cette classe représente un bordereau.
 * 
 * @author Pascal MACOUIN
 */
public class EOBordereau extends _EOBordereau implements IBordereau {
	private static final long serialVersionUID = 5730325122825857774L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOBordereau.class);
	
	@Inject
	@Nullable
	private UserInfo userInfo;

	/**
	 * Crée et insère une instance "vide" dans l'editing context.
	 * 
	 * @param editingContext un editing context
	 * @return un bordereau
	 */
	public static EOBordereau createAndInsertInstance(EOEditingContext editingContext) {
		EOBordereau eo = (EOBordereau) EOUtilities.createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);
		return eo;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public BigDecimal getTotalPaye(IModePaiement unModePaiement) {
		EOQualifier qualifier = EOPaiementMoyen.TO_PAIEMENT.dot(EOPaiement.TO_BORDEREAU).eq(this)
				.and(EOPaiementMoyen.TO_MODE_PAIEMENT.eq((EOModePaiement) unModePaiement));
		Object total = ERXEOControlUtilities._aggregateFunctionWithQualifier(this.editingContext(), EOPaiementMoyen.ENTITY_NAME, EOPaiementDetail.MONTANT_PAYE_KEY, "SUM", Number.class, "B", qualifier);
		
    	if (total instanceof BigDecimal) {
    		return (BigDecimal) total;
    	}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public BigDecimal getTotalPaye(ITypePaiement unTypePaiement) {
		EOQualifier qualifier = EOPaiementMoyen.TO_PAIEMENT.dot(EOPaiement.TO_BORDEREAU).eq(this)
				.and(EOPaiementMoyen.TO_TYPE_PAIEMENT.eq((EOTypePaiement) unTypePaiement));
		Object total = ERXEOControlUtilities._aggregateFunctionWithQualifier(this.editingContext(), EOPaiementMoyen.ENTITY_NAME, EOPaiementDetail.MONTANT_PAYE_KEY, "SUM", Number.class, "B", qualifier);
		
    	if (total instanceof BigDecimal) {
    		return (BigDecimal) total;
    	}
		
		return null;
	}
	
	/**
	 * Recherche du bordereau par sa clé fonctionnelle.
	 * @param editingContext un editing context
	 * @param typeBordereau un type de bordereau
	 * @param numeroBordereau un numéro de bordereau
	 * @param codeGestion un code de gestion
	 * @return le bordereau trouvé (ou <code>null</code> si non trouvé)
	 */
	public static EOBordereau fetchSco_Bordereau(EOEditingContext editingContext, ITypeBordereau typeBordereau, Integer numeroBordereau, String codeGestion) {
		EOQualifier qualifier = EOBordereau.TO_TYPE_BORDEREAU.eq((EOTypeBordereau) typeBordereau).and(EOBordereau.NUMERO_BORDEREAU.eq(numeroBordereau)).and(EOBordereau.CODE_GESTION.eq(codeGestion));
		return EOBordereau.fetchSco_Bordereau(editingContext, qualifier);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToTypeBordereauRelationship(ITypeBordereau typeBordereau) {
		super.setToTypeBordereauRelationship((EOTypeBordereau) typeBordereau);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}
}
