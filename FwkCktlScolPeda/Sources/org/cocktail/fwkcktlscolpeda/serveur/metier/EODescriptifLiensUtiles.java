package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Liens utiles associés à un descriptif (syllabus)
 */
public class EODescriptifLiensUtiles extends _EODescriptifLiensUtiles {
    private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODescriptifLiensUtiles.class);
	
	
	/**
	 * supprime le lien
	 * @param edc : contexte d'édition
	 */
	public void supprimer(EOEditingContext edc) {
		// avant de supprimer un lien recalculer l'ordre de chacun des élements
		recalculerOrdre();

		// supprimer l'objet
		edc.deleteObject(this);

	}
	
	private void recalculerOrdre() {
		for(int i=this.ordre(); i<this.descriptif().liensUtiles().size(); i++) {
			// récupérer le lien dont l'ordre est i et le set à i
			this.descriptif().liensUtiles().get(i).setOrdre(i);
		}
	}
	
}
