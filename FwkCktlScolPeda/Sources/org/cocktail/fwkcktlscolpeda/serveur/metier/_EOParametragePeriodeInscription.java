// DO NOT EDIT.  Make changes to EOParametragePeriodeInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametragePeriodeInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametragePeriodeInscription";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PROFIL = new ERXKey<String>("profil");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("typeFormation");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_FERMETURE_KEY = DATE_FERMETURE.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DATE_OUVERTURE_KEY = DATE_OUVERTURE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String PROFIL_KEY = PROFIL.key();
  // Relationships
  public static final String TYPE_FORMATION_KEY = TYPE_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOParametragePeriodeInscription.class);

  public EOParametragePeriodeInscription localInstanceIn(EOEditingContext editingContext) {
    EOParametragePeriodeInscription localInstance = (EOParametragePeriodeInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametragePeriodeInscription.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.DATE_CREATION_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(_EOParametragePeriodeInscription.DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOParametragePeriodeInscription.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.DATE_MODIFICATION_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(_EOParametragePeriodeInscription.DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.DATE_OUVERTURE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametragePeriodeInscription.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOParametragePeriodeInscription.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.PERS_ID_MODIFICATION_KEY);
  }

  public String profil() {
    return (String) storedValueForKey(_EOParametragePeriodeInscription.PROFIL_KEY);
  }

  public void setProfil(String value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
    	_EOParametragePeriodeInscription.LOG.debug( "updating profil from " + profil() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.PROFIL_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation typeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(_EOParametragePeriodeInscription.TYPE_FORMATION_KEY);
  }
  
  public void setTypeFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    takeStoredValueForKey(value, _EOParametragePeriodeInscription.TYPE_FORMATION_KEY);
  }

  public void setTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (_EOParametragePeriodeInscription.LOG.isDebugEnabled()) {
      _EOParametragePeriodeInscription.LOG.debug("updating typeFormation from " + typeFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = typeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePeriodeInscription.TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePeriodeInscription.TYPE_FORMATION_KEY);
    }
  }
  

  public static EOParametragePeriodeInscription createSco_ParametragePeriodeInscription(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateFermeture
, NSTimestamp dateOuverture
, Integer persIdCreation
, String profil
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation typeFormation) {
    EOParametragePeriodeInscription eo = (EOParametragePeriodeInscription) EOUtilities.createAndInsertInstance(editingContext, _EOParametragePeriodeInscription.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateFermeture(dateFermeture);
		eo.setDateOuverture(dateOuverture);
		eo.setPersIdCreation(persIdCreation);
		eo.setProfil(profil);
    eo.setTypeFormationRelationship(typeFormation);
    return eo;
  }

  public static ERXFetchSpecification<EOParametragePeriodeInscription> fetchSpec() {
    return new ERXFetchSpecification<EOParametragePeriodeInscription>(_EOParametragePeriodeInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametragePeriodeInscription> fetchAllSco_ParametragePeriodeInscriptions(EOEditingContext editingContext) {
    return _EOParametragePeriodeInscription.fetchAllSco_ParametragePeriodeInscriptions(editingContext, null);
  }

  public static NSArray<EOParametragePeriodeInscription> fetchAllSco_ParametragePeriodeInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametragePeriodeInscription.fetchSco_ParametragePeriodeInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametragePeriodeInscription> fetchSco_ParametragePeriodeInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametragePeriodeInscription> fetchSpec = new ERXFetchSpecification<EOParametragePeriodeInscription>(_EOParametragePeriodeInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametragePeriodeInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametragePeriodeInscription fetchSco_ParametragePeriodeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePeriodeInscription.fetchSco_ParametragePeriodeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePeriodeInscription fetchSco_ParametragePeriodeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametragePeriodeInscription> eoObjects = _EOParametragePeriodeInscription.fetchSco_ParametragePeriodeInscriptions(editingContext, qualifier, null);
    EOParametragePeriodeInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametragePeriodeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePeriodeInscription fetchRequiredSco_ParametragePeriodeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePeriodeInscription.fetchRequiredSco_ParametragePeriodeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePeriodeInscription fetchRequiredSco_ParametragePeriodeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametragePeriodeInscription eoObject = _EOParametragePeriodeInscription.fetchSco_ParametragePeriodeInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametragePeriodeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePeriodeInscription localInstanceIn(EOEditingContext editingContext, EOParametragePeriodeInscription eo) {
    EOParametragePeriodeInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
