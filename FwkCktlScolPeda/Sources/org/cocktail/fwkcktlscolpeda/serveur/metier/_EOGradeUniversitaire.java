// DO NOT EDIT.  Make changes to EOGradeUniversitaire.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOGradeUniversitaire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_GradeUniversitaire";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NB_UNITES_TEMPS = new ERXKey<Integer>("nbUnitesTemps");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NB_UNITES_TEMPS_KEY = NB_UNITES_TEMPS.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOGradeUniversitaire.class);

  public EOGradeUniversitaire localInstanceIn(EOEditingContext editingContext) {
    EOGradeUniversitaire localInstance = (EOGradeUniversitaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOGradeUniversitaire.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOGradeUniversitaire.LOG.isDebugEnabled()) {
    	_EOGradeUniversitaire.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGradeUniversitaire.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOGradeUniversitaire.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOGradeUniversitaire.LOG.isDebugEnabled()) {
    	_EOGradeUniversitaire.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGradeUniversitaire.LIBELLE_KEY);
  }

  public Integer nbUnitesTemps() {
    return (Integer) storedValueForKey(_EOGradeUniversitaire.NB_UNITES_TEMPS_KEY);
  }

  public void setNbUnitesTemps(Integer value) {
    if (_EOGradeUniversitaire.LOG.isDebugEnabled()) {
    	_EOGradeUniversitaire.LOG.debug( "updating nbUnitesTemps from " + nbUnitesTemps() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGradeUniversitaire.NB_UNITES_TEMPS_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOGradeUniversitaire.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOGradeUniversitaire.LOG.isDebugEnabled()) {
    	_EOGradeUniversitaire.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGradeUniversitaire.TYPE_KEY);
  }


  public static EOGradeUniversitaire createSco_GradeUniversitaire(EOEditingContext editingContext, Integer id
, String libelle
, Integer nbUnitesTemps
, String type
) {
    EOGradeUniversitaire eo = (EOGradeUniversitaire) EOUtilities.createAndInsertInstance(editingContext, _EOGradeUniversitaire.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setNbUnitesTemps(nbUnitesTemps);
		eo.setType(type);
    return eo;
  }

  public static ERXFetchSpecification<EOGradeUniversitaire> fetchSpec() {
    return new ERXFetchSpecification<EOGradeUniversitaire>(_EOGradeUniversitaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOGradeUniversitaire> fetchAllSco_GradeUniversitaires(EOEditingContext editingContext) {
    return _EOGradeUniversitaire.fetchAllSco_GradeUniversitaires(editingContext, null);
  }

  public static NSArray<EOGradeUniversitaire> fetchAllSco_GradeUniversitaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGradeUniversitaire.fetchSco_GradeUniversitaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGradeUniversitaire> fetchSco_GradeUniversitaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOGradeUniversitaire> fetchSpec = new ERXFetchSpecification<EOGradeUniversitaire>(_EOGradeUniversitaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGradeUniversitaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOGradeUniversitaire fetchSco_GradeUniversitaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGradeUniversitaire.fetchSco_GradeUniversitaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGradeUniversitaire fetchSco_GradeUniversitaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGradeUniversitaire> eoObjects = _EOGradeUniversitaire.fetchSco_GradeUniversitaires(editingContext, qualifier, null);
    EOGradeUniversitaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_GradeUniversitaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGradeUniversitaire fetchRequiredSco_GradeUniversitaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGradeUniversitaire.fetchRequiredSco_GradeUniversitaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGradeUniversitaire fetchRequiredSco_GradeUniversitaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGradeUniversitaire eoObject = _EOGradeUniversitaire.fetchSco_GradeUniversitaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_GradeUniversitaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGradeUniversitaire localInstanceIn(EOEditingContext editingContext, EOGradeUniversitaire eo) {
    EOGradeUniversitaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
