// DO NOT EDIT.  Make changes to EOJuryConvocation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOJuryConvocation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_JuryConvocation";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CONVOCATION = new ERXKey<NSTimestamp>("dateConvocation");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> SALLE_CONVOCATION = new ERXKey<String>("salleConvocation");
  public static final ERXKey<Boolean> VALIDE = new ERXKey<Boolean>("valide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury> TO_JURY = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury>("toJury");

  // Attributes
  public static final String DATE_CONVOCATION_KEY = DATE_CONVOCATION.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SALLE_CONVOCATION_KEY = SALLE_CONVOCATION.key();
  public static final String VALIDE_KEY = VALIDE.key();
  // Relationships
  public static final String TO_JURY_KEY = TO_JURY.key();

  private static Logger LOG = Logger.getLogger(_EOJuryConvocation.class);

  public EOJuryConvocation localInstanceIn(EOEditingContext editingContext) {
    EOJuryConvocation localInstance = (EOJuryConvocation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateConvocation() {
    return (NSTimestamp) storedValueForKey(_EOJuryConvocation.DATE_CONVOCATION_KEY);
  }

  public void setDateConvocation(NSTimestamp value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating dateConvocation from " + dateConvocation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.DATE_CONVOCATION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOJuryConvocation.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOJuryConvocation.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOJuryConvocation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOJuryConvocation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.PERS_ID_MODIFICATION_KEY);
  }

  public String salleConvocation() {
    return (String) storedValueForKey(_EOJuryConvocation.SALLE_CONVOCATION_KEY);
  }

  public void setSalleConvocation(String value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating salleConvocation from " + salleConvocation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.SALLE_CONVOCATION_KEY);
  }

  public Boolean valide() {
    return (Boolean) storedValueForKey(_EOJuryConvocation.VALIDE_KEY);
  }

  public void setValide(Boolean value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
    	_EOJuryConvocation.LOG.debug( "updating valide from " + valide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryConvocation.VALIDE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury toJury() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury)storedValueForKey(_EOJuryConvocation.TO_JURY_KEY);
  }
  
  public void setToJury(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury value) {
    takeStoredValueForKey(value, _EOJuryConvocation.TO_JURY_KEY);
  }

  public void setToJuryRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury value) {
    if (_EOJuryConvocation.LOG.isDebugEnabled()) {
      _EOJuryConvocation.LOG.debug("updating toJury from " + toJury() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToJury(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury oldValue = toJury();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJuryConvocation.TO_JURY_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJuryConvocation.TO_JURY_KEY);
    }
  }
  

  public static EOJuryConvocation createSco_JuryConvocation(EOEditingContext editingContext, NSTimestamp dCreation
, Integer persIdCreation
, Boolean valide
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury toJury) {
    EOJuryConvocation eo = (EOJuryConvocation) EOUtilities.createAndInsertInstance(editingContext, _EOJuryConvocation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
		eo.setValide(valide);
    eo.setToJuryRelationship(toJury);
    return eo;
  }

  public static ERXFetchSpecification<EOJuryConvocation> fetchSpec() {
    return new ERXFetchSpecification<EOJuryConvocation>(_EOJuryConvocation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOJuryConvocation> fetchAllSco_JuryConvocations(EOEditingContext editingContext) {
    return _EOJuryConvocation.fetchAllSco_JuryConvocations(editingContext, null);
  }

  public static NSArray<EOJuryConvocation> fetchAllSco_JuryConvocations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJuryConvocation.fetchSco_JuryConvocations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJuryConvocation> fetchSco_JuryConvocations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOJuryConvocation> fetchSpec = new ERXFetchSpecification<EOJuryConvocation>(_EOJuryConvocation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJuryConvocation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOJuryConvocation fetchSco_JuryConvocation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryConvocation.fetchSco_JuryConvocation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryConvocation fetchSco_JuryConvocation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJuryConvocation> eoObjects = _EOJuryConvocation.fetchSco_JuryConvocations(editingContext, qualifier, null);
    EOJuryConvocation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_JuryConvocation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryConvocation fetchRequiredSco_JuryConvocation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryConvocation.fetchRequiredSco_JuryConvocation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryConvocation fetchRequiredSco_JuryConvocation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJuryConvocation eoObject = _EOJuryConvocation.fetchSco_JuryConvocation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_JuryConvocation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryConvocation localInstanceIn(EOEditingContext editingContext, EOJuryConvocation eo) {
    EOJuryConvocation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
