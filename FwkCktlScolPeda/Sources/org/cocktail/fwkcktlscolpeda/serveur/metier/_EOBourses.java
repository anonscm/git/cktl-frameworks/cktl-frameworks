// DO NOT EDIT.  Make changes to EOBourses.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOBourses extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Bourses";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MOFICATION = new ERXKey<NSTimestamp>("dMofication");
  public static final ERXKey<Boolean> IS_CAMPUS_FRANCE = new ERXKey<Boolean>("isCampusFrance");
  public static final ERXKey<String> NUM_ALLOCATAIRE = new ERXKey<String>("numAllocataire");
  public static final ERXKey<String> ORGANISME = new ERXKey<String>("organisme");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse> TO_ECHELON_BOURSE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse>("toEchelonBourse");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF> TO_TYPE_BOURSES_CF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF>("toTypeBoursesCF");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation> TO_TYPE_INSCRIPTION_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation>("toTypeInscriptionFormation");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MOFICATION_KEY = D_MOFICATION.key();
  public static final String IS_CAMPUS_FRANCE_KEY = IS_CAMPUS_FRANCE.key();
  public static final String NUM_ALLOCATAIRE_KEY = NUM_ALLOCATAIRE.key();
  public static final String ORGANISME_KEY = ORGANISME.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_ECHELON_BOURSE_KEY = TO_ECHELON_BOURSE.key();
  public static final String TO_ETUDIANT_KEY = TO_ETUDIANT.key();
  public static final String TO_TYPE_BOURSES_CF_KEY = TO_TYPE_BOURSES_CF.key();
  public static final String TO_TYPE_INSCRIPTION_FORMATION_KEY = TO_TYPE_INSCRIPTION_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOBourses.class);

  public EOBourses localInstanceIn(EOEditingContext editingContext) {
    EOBourses localInstance = (EOBourses)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOBourses.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.ANNEE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOBourses.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.D_CREATION_KEY);
  }

  public NSTimestamp dMofication() {
    return (NSTimestamp) storedValueForKey(_EOBourses.D_MOFICATION_KEY);
  }

  public void setDMofication(NSTimestamp value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating dMofication from " + dMofication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.D_MOFICATION_KEY);
  }

  public Boolean isCampusFrance() {
    return (Boolean) storedValueForKey(_EOBourses.IS_CAMPUS_FRANCE_KEY);
  }

  public void setIsCampusFrance(Boolean value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating isCampusFrance from " + isCampusFrance() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.IS_CAMPUS_FRANCE_KEY);
  }

  public String numAllocataire() {
    return (String) storedValueForKey(_EOBourses.NUM_ALLOCATAIRE_KEY);
  }

  public void setNumAllocataire(String value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating numAllocataire from " + numAllocataire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.NUM_ALLOCATAIRE_KEY);
  }

  public String organisme() {
    return (String) storedValueForKey(_EOBourses.ORGANISME_KEY);
  }

  public void setOrganisme(String value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating organisme from " + organisme() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.ORGANISME_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOBourses.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOBourses.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
    	_EOBourses.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOBourses.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse toEchelonBourse() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse)storedValueForKey(_EOBourses.TO_ECHELON_BOURSE_KEY);
  }
  
  public void setToEchelonBourse(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse value) {
    takeStoredValueForKey(value, _EOBourses.TO_ECHELON_BOURSE_KEY);
  }

  public void setToEchelonBourseRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
      _EOBourses.LOG.debug("updating toEchelonBourse from " + toEchelonBourse() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEchelonBourse(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEchelonBourse oldValue = toEchelonBourse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOBourses.TO_ECHELON_BOURSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOBourses.TO_ECHELON_BOURSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant)storedValueForKey(_EOBourses.TO_ETUDIANT_KEY);
  }
  
  public void setToEtudiant(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    takeStoredValueForKey(value, _EOBourses.TO_ETUDIANT_KEY);
  }

  public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
      _EOBourses.LOG.debug("updating toEtudiant from " + toEtudiant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOBourses.TO_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOBourses.TO_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF toTypeBoursesCF() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF)storedValueForKey(_EOBourses.TO_TYPE_BOURSES_CF_KEY);
  }
  
  public void setToTypeBoursesCF(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF value) {
    takeStoredValueForKey(value, _EOBourses.TO_TYPE_BOURSES_CF_KEY);
  }

  public void setToTypeBoursesCFRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
      _EOBourses.LOG.debug("updating toTypeBoursesCF from " + toTypeBoursesCF() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeBoursesCF(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBoursesCF oldValue = toTypeBoursesCF();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOBourses.TO_TYPE_BOURSES_CF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOBourses.TO_TYPE_BOURSES_CF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation toTypeInscriptionFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation)storedValueForKey(_EOBourses.TO_TYPE_INSCRIPTION_FORMATION_KEY);
  }
  
  public void setToTypeInscriptionFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation value) {
    takeStoredValueForKey(value, _EOBourses.TO_TYPE_INSCRIPTION_FORMATION_KEY);
  }

  public void setToTypeInscriptionFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation value) {
    if (_EOBourses.LOG.isDebugEnabled()) {
      _EOBourses.LOG.debug("updating toTypeInscriptionFormation from " + toTypeInscriptionFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeInscriptionFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation oldValue = toTypeInscriptionFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOBourses.TO_TYPE_INSCRIPTION_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOBourses.TO_TYPE_INSCRIPTION_FORMATION_KEY);
    }
  }
  

  public static EOBourses createSco_Bourses(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant) {
    EOBourses eo = (EOBourses) EOUtilities.createAndInsertInstance(editingContext, _EOBourses.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
    eo.setToEtudiantRelationship(toEtudiant);
    return eo;
  }

  public static ERXFetchSpecification<EOBourses> fetchSpec() {
    return new ERXFetchSpecification<EOBourses>(_EOBourses.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOBourses> fetchAllSco_Bourseses(EOEditingContext editingContext) {
    return _EOBourses.fetchAllSco_Bourseses(editingContext, null);
  }

  public static NSArray<EOBourses> fetchAllSco_Bourseses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOBourses.fetchSco_Bourseses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOBourses> fetchSco_Bourseses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOBourses> fetchSpec = new ERXFetchSpecification<EOBourses>(_EOBourses.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOBourses> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOBourses fetchSco_Bourses(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBourses.fetchSco_Bourses(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBourses fetchSco_Bourses(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOBourses> eoObjects = _EOBourses.fetchSco_Bourseses(editingContext, qualifier, null);
    EOBourses eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Bourses that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBourses fetchRequiredSco_Bourses(EOEditingContext editingContext, String keyName, Object value) {
    return _EOBourses.fetchRequiredSco_Bourses(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOBourses fetchRequiredSco_Bourses(EOEditingContext editingContext, EOQualifier qualifier) {
    EOBourses eoObject = _EOBourses.fetchSco_Bourses(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Bourses that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOBourses localInstanceIn(EOEditingContext editingContext, EOBourses eo) {
    EOBourses localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
