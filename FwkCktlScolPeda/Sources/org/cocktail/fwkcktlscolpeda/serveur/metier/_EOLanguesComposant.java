// DO NOT EDIT.  Make changes to EOLanguesComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOLanguesComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_LanguesComposant";

  // Attribute Keys
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  // Relationship Keys

  // Attributes
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOLanguesComposant.class);

  public EOLanguesComposant localInstanceIn(EOEditingContext editingContext) {
    EOLanguesComposant localInstance = (EOLanguesComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOLanguesComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOLanguesComposant.LOG.isDebugEnabled()) {
    	_EOLanguesComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLanguesComposant.COMPOSANT_ID_KEY);
  }


  public static EOLanguesComposant createSco_LanguesComposant(EOEditingContext editingContext, Integer composantId
) {
    EOLanguesComposant eo = (EOLanguesComposant) EOUtilities.createAndInsertInstance(editingContext, _EOLanguesComposant.ENTITY_NAME);    
		eo.setComposantId(composantId);
    return eo;
  }

  public static ERXFetchSpecification<EOLanguesComposant> fetchSpec() {
    return new ERXFetchSpecification<EOLanguesComposant>(_EOLanguesComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOLanguesComposant> fetchAllSco_LanguesComposants(EOEditingContext editingContext) {
    return _EOLanguesComposant.fetchAllSco_LanguesComposants(editingContext, null);
  }

  public static NSArray<EOLanguesComposant> fetchAllSco_LanguesComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLanguesComposant.fetchSco_LanguesComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLanguesComposant> fetchSco_LanguesComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOLanguesComposant> fetchSpec = new ERXFetchSpecification<EOLanguesComposant>(_EOLanguesComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLanguesComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOLanguesComposant fetchSco_LanguesComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLanguesComposant.fetchSco_LanguesComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLanguesComposant fetchSco_LanguesComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLanguesComposant> eoObjects = _EOLanguesComposant.fetchSco_LanguesComposants(editingContext, qualifier, null);
    EOLanguesComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_LanguesComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLanguesComposant fetchRequiredSco_LanguesComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLanguesComposant.fetchRequiredSco_LanguesComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLanguesComposant fetchRequiredSco_LanguesComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLanguesComposant eoObject = _EOLanguesComposant.fetchSco_LanguesComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_LanguesComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLanguesComposant localInstanceIn(EOEditingContext editingContext, EOLanguesComposant eo) {
    EOLanguesComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
