// DO NOT EDIT.  Make changes to EOParcours.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParcours extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant {
  public static final String ENTITY_NAME = "Sco_Parcours";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Boolean> PARCOURS_TYPE = new ERXKey<Boolean>("parcoursType");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> SISE = new ERXKey<String>("sise");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> COMPOSANT_SUIVANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("composantSuivant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PARCOURS_TYPE_KEY = PARCOURS_TYPE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SISE_KEY = SISE.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  // Relationships
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String COMPOSANT_SUIVANT_KEY = COMPOSANT_SUIVANT.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EOParcours.class);

  public EOParcours localInstanceIn(EOEditingContext editingContext) {
    EOParcours localInstance = (EOParcours)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean parcoursType() {
    return (Boolean) storedValueForKey(_EOParcours.PARCOURS_TYPE_KEY);
  }

  public void setParcoursType(Boolean value) {
    if (_EOParcours.LOG.isDebugEnabled()) {
    	_EOParcours.LOG.debug( "updating parcoursType from " + parcoursType() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParcours.PARCOURS_TYPE_KEY);
  }

  public String sise() {
    return (String) storedValueForKey(_EOParcours.SISE_KEY);
  }

  public void setSise(String value) {
    if (_EOParcours.LOG.isDebugEnabled()) {
    	_EOParcours.LOG.debug( "updating sise from " + sise() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParcours.SISE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> composantSuivant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>)storedValueForKey(_EOParcours.COMPOSANT_SUIVANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> composantSuivant(EOQualifier qualifier) {
    return composantSuivant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> composantSuivant(EOQualifier qualifier, boolean fetch) {
    return composantSuivant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> composantSuivant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours.fetchSco_Parcourses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantSuivant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours object) {
    includeObjectIntoPropertyWithKey(object, _EOParcours.COMPOSANT_SUIVANT_KEY);
  }

  public void removeFromComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours object) {
    excludeObjectFromPropertyWithKey(object, _EOParcours.COMPOSANT_SUIVANT_KEY);
  }

  public void addToComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours object) {
    if (_EOParcours.LOG.isDebugEnabled()) {
      _EOParcours.LOG.debug("adding " + object + " to composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantSuivant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOParcours.COMPOSANT_SUIVANT_KEY);
    }
  }

  public void removeFromComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours object) {
    if (_EOParcours.LOG.isDebugEnabled()) {
      _EOParcours.LOG.debug("removing " + object + " from composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantSuivant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParcours.COMPOSANT_SUIVANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours createComposantSuivantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOParcours.COMPOSANT_SUIVANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours) eo;
  }

  public void deleteComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParcours.COMPOSANT_SUIVANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantSuivantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> objects = composantSuivant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantSuivantRelationship(objects.nextElement());
    }
  }


  public static EOParcours createSco_Parcours(EOEditingContext editingContext, Integer id
, String libelle
, Boolean parcoursType
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion composantInfoVersion) {
    EOParcours eo = (EOParcours) EOUtilities.createAndInsertInstance(editingContext, _EOParcours.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setParcoursType(parcoursType);
    eo.setComposantInfoVersionRelationship(composantInfoVersion);
    return eo;
  }

  public static ERXFetchSpecification<EOParcours> fetchSpecForSco_Parcours() {
    return new ERXFetchSpecification<EOParcours>(_EOParcours.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParcours> fetchAllSco_Parcourses(EOEditingContext editingContext) {
    return _EOParcours.fetchAllSco_Parcourses(editingContext, null);
  }

  public static NSArray<EOParcours> fetchAllSco_Parcourses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParcours.fetchSco_Parcourses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParcours> fetchSco_Parcourses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParcours> fetchSpec = new ERXFetchSpecification<EOParcours>(_EOParcours.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParcours> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParcours fetchSco_Parcours(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParcours.fetchSco_Parcours(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParcours fetchSco_Parcours(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParcours> eoObjects = _EOParcours.fetchSco_Parcourses(editingContext, qualifier, null);
    EOParcours eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Parcours that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParcours fetchRequiredSco_Parcours(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParcours.fetchRequiredSco_Parcours(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParcours fetchRequiredSco_Parcours(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParcours eoObject = _EOParcours.fetchSco_Parcours(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Parcours that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParcours localInstanceIn(EOEditingContext editingContext, EOParcours eo) {
    EOParcours localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
