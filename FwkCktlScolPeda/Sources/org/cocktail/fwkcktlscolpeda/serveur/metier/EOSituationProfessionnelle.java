package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOSituationProfessionnelle extends _EOSituationProfessionnelle {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOSituationProfessionnelle.class);
	
	private static ERXEnterpriseObjectCache<EOSituationProfessionnelle> situationProfessionnelleCache;
	
	/** The Constant CODE_SITPROF_SALARIE. */
	public static final String CODE_SITPROF_SALARIE = "1";
	
	/** The Constant CODE_SITPROF_FONCTIONNAIRE. */
	public static final String CODE_SITPROF_FONCTIONNAIRE = "2";
	
	/** The Constant CODE_SITPROF_MILITAIRE. */
	public static final String CODE_SITPROF_MILITAIRE = "3";
	
	/** The Constant CODE_SITPROF_AUTRE. */
	public static final String CODE_SITPROF_AUTRE = "4";
	
	/** The Constant CODE_SITPROF_SANS. */
	public static final String CODE_SITPROF_SANS = "S";
	
	
	/**
     * @return le cache des {@link EOSituationProfessionnelle} par leur code
     */
    public static ERXEnterpriseObjectCache<EOSituationProfessionnelle> getSituationProfessionnelleCache() {
    	if (situationProfessionnelleCache == null) {
    		situationProfessionnelleCache = new ERXEnterpriseObjectCache<EOSituationProfessionnelle>(EOSituationProfessionnelle.class, CODE_SIT_PROF_KEY);
    	}
    	return situationProfessionnelleCache;
    }
    
    /**
     * @param situationProfessionnelleCache le cache a setter
     */
    public static void setSituationProfessionnelleCache(ERXEnterpriseObjectCache<EOSituationProfessionnelle> situationProfessionnelleCache) {
    	EOSituationProfessionnelle.situationProfessionnelleCache = situationProfessionnelleCache;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOSituationProfessionnelle} "SALARIE"
	 */
	public static EOSituationProfessionnelle situationProfessionnelleSalarie(EOEditingContext edc) {
		return getSituationProfessionnelleCache().objectForKey(edc, CODE_SITPROF_SALARIE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOSituationProfessionnelle} "FONCTIONNAIRE"
	 */
	public static EOSituationProfessionnelle situationProfessionnelleFonctionnaire(EOEditingContext edc) {
		return getSituationProfessionnelleCache().objectForKey(edc, CODE_SITPROF_FONCTIONNAIRE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOSituationProfessionnelle} "MILITAIRE"
	 */
	public static EOSituationProfessionnelle situationProfessionnelleMilitaire(EOEditingContext edc) {
		return getSituationProfessionnelleCache().objectForKey(edc, CODE_SITPROF_MILITAIRE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOSituationProfessionnelle} "AUTRE"
	 */
	public static EOSituationProfessionnelle situationProfessionnelleAutre(EOEditingContext edc) {
		return getSituationProfessionnelleCache().objectForKey(edc, CODE_SITPROF_AUTRE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOSituationProfessionnelle} "SANS"
	 */
	public static EOSituationProfessionnelle situationProfessionnelleSans(EOEditingContext edc) {
		return getSituationProfessionnelleCache().objectForKey(edc, CODE_SITPROF_SANS);
	}
	
	@Override
	public String toString() {
		return this.libelle();
	}
	
}
