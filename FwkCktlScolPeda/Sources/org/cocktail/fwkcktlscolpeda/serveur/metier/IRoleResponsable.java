package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * roles d'un responsable spécifique à la scol
 */
public interface IRoleResponsable {

	
	/**
	 * @return libellé long du rôle
	 */
	String libelleLong();
}
