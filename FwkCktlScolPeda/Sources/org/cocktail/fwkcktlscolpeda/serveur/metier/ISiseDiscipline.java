package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * discipline SISE associée au diplome
 */
public interface ISiseDiscipline {
	
	/**
	 * @return code de la discipline
	 */
	String code();

	/**
	 * @param value code de la discipline
	 */
	void setCode(String value);

	/**
	 * @return libelle de la discipline
	 */
	String libelle();

	/**
	 * @param value libelle de la discipline
	 */
	void setLibelle(String value);
}
