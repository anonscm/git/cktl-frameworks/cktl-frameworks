package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Revoi les lien avec des infos sur le contexte
 * i.e la position précise du lien dans l'arboresence
 */
public class EOLienConsultation extends _EOLienConsultation implements ILienConsultation {
	
	private static final long serialVersionUID = -2229448047889248996L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOLienConsultation.class);

	private BigDecimal creditECTSCalcule;
	private Integer sommeMinutesEnseigneesCalculee;
	private Map<EOTypeAP, Integer> sommeMinutesEnseigneesCalculeePourTypeAP = new HashMap<EOTypeAP, Integer>();

	private List<ILienConsultation> children;

	private ILienConsultation parent;

	/**
	 * Retourne le lien consultation correspondant à cet id.
	 * @param edc un editing contexte
	 * @param idLien Un id lien consultation
	 * @return Le lien consultation trouvé
	 */
	public static ILienConsultation fetchById(EOEditingContext edc, int idLien) {
		return EOLienConsultation.fetchSco_LienConsultation(edc, EOLienConsultation.ID_LIEN.eq(idLien));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IComposant child() {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IComposant parent() {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	public BigDecimal getCreditECTSCalcule() {
		return creditECTSCalcule;
	}

	public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
		this.creditECTSCalcule = creditECTSCalcule;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isNbCreditsEctsOk() {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	public Integer getSommeMinutesEnseigneesCalculee() {
		return sommeMinutesEnseigneesCalculee;
	}

	public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
		this.sommeMinutesEnseigneesCalculee = sommeMinutesEnseigneesCalculee;
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(typeAP);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeHeuresEnseignesCalculees, EOTypeAP typeAP) {
		sommeMinutesEnseigneesCalculeePourTypeAP.put(typeAP, sommeHeuresEnseignesCalculees);
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPTD() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeTD(this.editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeTD(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPTP() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeTP(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeTP(this.editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer sommeMinutesEnseigneesAPCours() {
		return sommeMinutesEnseigneesCalculeePourTypeAP.get(EOTypeAP.typeCours(this.editingContext()));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
		setSommeMinutesEnseigneesCalculeePourTypeAP(valeurMinutes, EOTypeAP.typeCours(this.editingContext()));
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer id() {
		return idLien();
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isParentSemestre() {
		throw new UnsupportedOperationException("Not yet implemented");
	}


	/**
	 * {@inheritDoc}
	 */
	public void assignTo(ILien nouveauLien) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * {@inheritDoc}
	 */
	public ICreditable creditable() {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	public List<ILienConsultation> getChildren() {
		return children;
	}

	public void setChildren(List<ILienConsultation> children) {
		this.children = children;
	}

	public ILienConsultation getParent() {
		return parent;
	}

	public void setParent(ILienConsultation parent) {
			this.parent = parent;
	}

	/**
	 * {@inheritDoc}
	 */
	public String libelle() {
		if (StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeVersionDiplome(editingContext()).nom())) {
			return llComposantParent();
		} else {
			return llComposantChild();
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildParcours() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeParcours(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildUE() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeUE(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildEC() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeEC(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildRegroupement() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeRegroupement(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildAE() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeAE(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildAP() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typeAP(editingContext()).nom());
	}
	/**
	 * {@inheritDoc}
	 */
	public Boolean isChildPeriode() {
		return StringUtils.equals(typeComposantNomChild(), EOTypeComposant.typePeriode(editingContext()).nom());
	}

	@Override
	public String toString() {
		return idLienContexte() + " - " + llComposantChild();
	}
	
	/**
	 *  {@inheritDoc}
	 */
	public Boolean isChoixMinMax() {
		return this.choixMin() != null && this.choixMax() != null;
	}
	
	/**
	 *  {@inheritDoc}
	 */
	public Boolean isChoixEcts() {
		return this.choixECTS() != null;
	}
	
	
}
