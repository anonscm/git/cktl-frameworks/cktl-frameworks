package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.foundation.NSTimestamp;

/**
 *  Session sur une période (exemple session de fin de semestre 1)
 */
public interface ISessionPeriode {
	/**
	 * @return  annee universitaire
	 */
	Integer annee();
	
	/**
	 * @param value  annee universitaire
	 */
	void setAnnee(Integer value);
	
	/**
	 * @return numéro de la session
	 */
	Integer numeroSession();
	
	/**
	 * @param value numéro de la session
	 */
	void setNumeroSession(Integer value);
	
	/**
	 * @return libellé de la session
	 */
	String libelle();

	/**
	 * @param value libellé de la session
	 */
	void setLibelle(String value);
	
	/**
	 * @return date d'ouverture de la session
	 */
	NSTimestamp dateOuverture();
	
	/**
	 * @param value date d'ouverture de la session
	 */
	void setDateOuverture(NSTimestamp value);
	
	/**
	 * @return  date de fermeture de la session
	 */
	NSTimestamp dateFermeture();
	
	/**
	 * @param value date de fermeture de la session
	 */
	void setDateFermeture(NSTimestamp value);
	
	/**
	 * @return id de la session
	 */
	Integer id();
}
