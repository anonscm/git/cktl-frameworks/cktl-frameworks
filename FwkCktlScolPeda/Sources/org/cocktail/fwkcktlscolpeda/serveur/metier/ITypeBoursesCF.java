package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un type de bourse Campus France (table
 * TYPE_BOURSES_CF)
 * 
 * @author isabelle
 * 
 */
public interface ITypeBoursesCF {

	String CODE_SANS = "SANS";
	String CODE_INSCRIPTION = "INSCRIPTION";
	String CODE_SECU = "SECU";
	String CODE_INSCRIPTION_SECU = "INSCRIPTION_SECU";

	/**
	 * 
	 * @return code
	 */
	String code();

	/**
	 * 
	 * @param value the code
	 */
	void setCode(String value);
	
	/**
	 * 
	 * @return libelle
	 */
	String libelle();

	/**
	 * 
	 * @param value the libelle
	 */
	void setLibelle(String value);
}