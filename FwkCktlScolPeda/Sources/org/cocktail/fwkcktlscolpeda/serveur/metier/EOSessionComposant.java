package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 * Cette classe represente une session
 */
public class EOSessionComposant extends _EOSessionComposant implements ISessionComposant {
	// Private fields
	private static final long serialVersionUID = 1L;

	// Public Methods
	
	/**
	 * {@inheritDoc}
	 */
	public void setComposantRelationship(IComposant unComposant) {
		super.setComposantRelationship((EOComposant) unComposant);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.libelle();
	}

	/**
	 * @return copie de la session
	 */
	public EOSessionComposant copy() {
		EOSessionComposant session = new EOSessionComposant();
		session.setActive(this.active());
		session.setNumeroSession(this.numeroSession());
		session.setLibelle(this.libelle());
		session.setSeuil(this.seuil());
		for (EOFormule formule : formules()) {
			EOFormule copyFormule = formule.copy();
			copyFormule.setSessionComposantRelationship(session);
		}
		editingContext().insertObject(session);
		return session;
	}
	
	/**
	 * Suppression d'une session.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		try {
			// Supprimer les formules
			deleteAllFormulesRelationships();
			
			// Supprimer la relation avec le composant
			removeObjectFromBothSidesOfRelationshipWithKey(composant(), _EOSessionComposant.COMPOSANT_KEY);
			
			this.delete();
		} catch (Exception e) {
			System.err.println("Erreur suppression session composant : " + e.getMessage());
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public IFormule getFormule(String typeFormuleLbl) {
		for (IFormule formule : formules()) {
			if (typeFormuleLbl.equals(formule.typeFormule().type())) {
				return formule;
			}
		}
		return null;
	}
}
