package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 *  Spécialité rattaché à un diplome
 */
public interface ISpecialite {
	
	/**
	 * @return code de la spécialité
	 */
	String code();
	
	/**
	 * @return libellé de la spécialité
	 */
	String libelle();

}
