// DO NOT EDIT.  Make changes to EOEC.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEC extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant {
  public static final String ENTITY_NAME = "Sco_EC";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> LIBRE = new ERXKey<Integer>("libre");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> COMPOSANT_SUIVANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>("composantSuivant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String LIBRE_KEY = LIBRE.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  // Relationships
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String COMPOSANT_SUIVANT_KEY = COMPOSANT_SUIVANT.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EOEC.class);

  public EOEC localInstanceIn(EOEditingContext editingContext) {
    EOEC localInstance = (EOEC)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer libre() {
    return (Integer) storedValueForKey(_EOEC.LIBRE_KEY);
  }

  public void setLibre(Integer value) {
    if (_EOEC.LOG.isDebugEnabled()) {
    	_EOEC.LOG.debug( "updating libre from " + libre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEC.LIBRE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> composantSuivant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>)storedValueForKey(_EOEC.COMPOSANT_SUIVANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> composantSuivant(EOQualifier qualifier) {
    return composantSuivant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> composantSuivant(EOQualifier qualifier, boolean fetch) {
    return composantSuivant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> composantSuivant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC.fetchSco_ECs(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantSuivant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC object) {
    includeObjectIntoPropertyWithKey(object, _EOEC.COMPOSANT_SUIVANT_KEY);
  }

  public void removeFromComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC object) {
    excludeObjectFromPropertyWithKey(object, _EOEC.COMPOSANT_SUIVANT_KEY);
  }

  public void addToComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC object) {
    if (_EOEC.LOG.isDebugEnabled()) {
      _EOEC.LOG.debug("adding " + object + " to composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantSuivant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEC.COMPOSANT_SUIVANT_KEY);
    }
  }

  public void removeFromComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC object) {
    if (_EOEC.LOG.isDebugEnabled()) {
      _EOEC.LOG.debug("removing " + object + " from composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantSuivant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEC.COMPOSANT_SUIVANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC createComposantSuivantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEC.COMPOSANT_SUIVANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC) eo;
  }

  public void deleteComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEC.COMPOSANT_SUIVANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantSuivantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> objects = composantSuivant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantSuivantRelationship(objects.nextElement());
    }
  }


  public static EOEC createSco_EC(EOEditingContext editingContext, Integer id
, String libelle
, Integer libre
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion composantInfoVersion) {
    EOEC eo = (EOEC) EOUtilities.createAndInsertInstance(editingContext, _EOEC.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setLibre(libre);
    eo.setComposantInfoVersionRelationship(composantInfoVersion);
    return eo;
  }

  public static ERXFetchSpecification<EOEC> fetchSpecForSco_EC() {
    return new ERXFetchSpecification<EOEC>(_EOEC.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEC> fetchAllSco_ECs(EOEditingContext editingContext) {
    return _EOEC.fetchAllSco_ECs(editingContext, null);
  }

  public static NSArray<EOEC> fetchAllSco_ECs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEC.fetchSco_ECs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEC> fetchSco_ECs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEC> fetchSpec = new ERXFetchSpecification<EOEC>(_EOEC.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEC> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEC fetchSco_EC(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEC.fetchSco_EC(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEC fetchSco_EC(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEC> eoObjects = _EOEC.fetchSco_ECs(editingContext, qualifier, null);
    EOEC eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_EC that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEC fetchRequiredSco_EC(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEC.fetchRequiredSco_EC(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEC fetchRequiredSco_EC(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEC eoObject = _EOEC.fetchSco_EC(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_EC that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEC localInstanceIn(EOEditingContext editingContext, EOEC eo) {
    EOEC localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
