package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * {@inheritDoc}
 */
public class EOTypeDescriptifDestination extends _EOTypeDescriptifDestination implements ITypeDescriptifDestination {
    private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeDescriptifDestination.class);
	

	
	
	/**
	 * @param edc contexte d'ediion
	 * @return descriptif à destination d'habilitation
	 */
	public static EOTypeDescriptifDestination typeHabilitation(EOEditingContext edc) {
		EOQualifier qualifier = ERXQ.equals(TYPE_KEY, TYPEHABILITATION_LBL);
		return EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestination(edc, qualifier);
	}
	
	/**
	 * @param edc contexte d'ediion
	 * @return descriptif à destination de publication
	 */
	public static EOTypeDescriptifDestination typePublication(EOEditingContext edc) {
		EOQualifier qualifier = ERXQ.equals(TYPE_KEY, TYPEPUBLICATION_LBL);
		return EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestination(edc, qualifier);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.libelle();
	}
	
}
