package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

/**
 * 
 * @author isabelle Represente une personne assistant à un jury avec role
 */
public interface IJuryPersonnes {
	Boolean STATUT_DEFAUT_PRESENT = false;

	/**
	 * @param value date de creation
	 */
	void setDCreation(Date value);

	/**
	 * @param value date de modification
	 */
	void setDModification(Date value);

	/**
	 * @return perdId de l'utilisateur qui a créé le membre du jury
	 */
	Integer persIdCreation();

	/**
	 * @param value de l'utilisateur qui a créé le membre du jury
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return perdId de l'utilisateur qui a modifié le membre du jury
	 */
	Integer persIdModification();

	/**
	 * @param value de l'utilisateur qui a modifié le membre du jury
	 */
	void setPersIdModification(Integer value);

	/**
	 * @return individu qui prend part au jury
	 */
	IIndividu toIndividu();

	/**
	 * @param value individu qui prend part au jury
	 */
	void setToIndividuRelationship(IIndividu value);

	/**
	 * @return jury concerné
	 */
	IJury toJury();

	/**
	 * @param value jury concerné
	 */
	void setToJuryRelationship(IJury value);

	/**
	 * @return role tenu par l'individu dans ce jury
	 */
	IJuryRole toJuryRole();

	/**
	 * @param value role tenu par l'individu dans ce jury
	 */
	void setToJuryRoleRelationship(IJuryRole value);

	/**
	 * @return <code>true </code> si l'individu sera bien présent lors du jury
	 */
	Boolean present();

	/**
	 * @param value <code>true </code> si l'individu sera bien présent lors du jury
	 */
	void setPresent(Boolean value);

}