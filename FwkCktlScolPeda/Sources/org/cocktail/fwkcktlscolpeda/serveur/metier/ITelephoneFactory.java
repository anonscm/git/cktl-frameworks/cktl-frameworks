package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

/**
 * 
 * Factory pour créer les téléphones
 * 
 * @author Alexis Tual
 *
 */
public interface ITelephoneFactory {

    /**
     * @param preEtudiant un preEtudiant
     * @param typeTelephone un type de téléphone
     * @return un preTelephone de type tel fixe
     */
	ITelephone creerTelephoneFixe(IInfosEtudiant etudiant, ITypeTel typeTelephone);
    
    /**
     * @param preEtudiant un preEtudiant
     * @param typeTelephone un type de téléphone
     * @return un preTelephone de type tel GSM
     */
	ITelephone creerTelephoneGsm(IInfosEtudiant etudiant, ITypeTel typeTelephone);
	
    /**
     * @param preEtudiant un preEtudiant
     * @return un preTelephone de type tel fixe
     */
	ITelephone creerTelephoneFixeEtudiant(IInfosEtudiant etudiant);
    
    /**
     * @param preEtudiant un preEtudiant
     * @return un preTelephone de type tel fixe
     */
	ITelephone creerTelephoneFixeParent(IInfosEtudiant etudiant);
    
    /**
     * @param preEtudiant un preEtudiant
     * @return un preTelephone de type tel GSM
     */
	ITelephone creerTelephoneGsmEtudiant(IInfosEtudiant etudiant);
    
    /**
     * @param preEtudiant un preEtudiant
     * @return un preTelephone de type tel GSM
     */
	ITelephone creerTelephoneGsmParent(IInfosEtudiant etudiant);
    
}
