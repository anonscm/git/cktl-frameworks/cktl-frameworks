package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 *  Vocation du diplome
 */
public interface IVocation {

	
	/**
	 * @return code de la vocation
	 */
	String code();
	
	/**
	 * @return libelle de la vocation
	 */
	String libelle();
	
	/**
	 * @return ordre dans le quel apparait la vocations
	 */
	Integer ordre();
}
