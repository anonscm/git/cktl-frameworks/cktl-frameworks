package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;


/**
 * Classe correspondant à l'objet note
 * @author isabelle
 *
 */
public class EONote extends _EONote implements INote {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EONote.class);
	
	public static final ERXSortOrderings SORT_NOTE_RELEVE_NOTES = EONote.TO_TYPE_NOTE.dot(EOTypeNote.CODE).ascs().then(EONote.NO_SESSION.asc());
	
	 @Inject
		@Nullable
		private UserInfo userInfo;
	/**
	 * constructeur.
	 */
	public EONote() {
		super();
	}

	public void setNoteSurchargee(BigDecimal value) {
		super.setNoteSurchargee(value);
		
	}

	public void setToInscriptionPedagogiqueElementRelationship(
			IInscriptionPedagogiqueElement value) {
		super.setToInscriptionPedagogiqueElementRelationship((EOInscriptionPedagogiqueElement) value);
		
	}

	public void setToTypeNoteRelationship(ITypeNote value) {
		super.setToTypeNoteRelationship((EOTypeNote) value);
		
	}
	
	@Override
	public void validateForInsert() throws ValidationException {
		this.updateRecordMetaData(getUserInfo().persId().intValue());
		super.validateForInsert();
	}
	
	@Override
	public void validateForSave() throws ValidationException {
		if (absence() == null) {
			setAbsence(Boolean.FALSE);
		}
		super.validateForSave();
	}
	
	public void updateRecordMetaData(Integer utilisateurPersId) {
		if(storedValueForKey(PERS_ID_CREATION_KEY) == null) {
			this.takeStoredValueForKey(utilisateurPersId, PERS_ID_CREATION_KEY);
		}
		this.takeStoredValueForKey(utilisateurPersId, PERS_ID_MODIFICATION_KEY);
		if(storedValueForKey(D_CREATION_KEY) == null) {
			this.takeStoredValueForKey(new NSTimestamp(), D_CREATION_KEY);
		}
		this.takeStoredValueForKey(new NSTimestamp(), D_MODIFICATION_KEY);
	}
	
	 public void setUserInfo(UserInfo userInfo) {
			this.userInfo = userInfo;
		}

		public UserInfo getUserInfo() {
		    return userInfo;
	    }
 
	
	public static EONote creerNote (EOEditingContext editingContext, EOInscriptionPedagogiqueElement inscripPead, EOTypeNote typeNote, Integer noSession) {
		EONote note = (EONote) EOUtilities.createAndInsertInstance(editingContext, EONote.ENTITY_NAME);
		note.setToInscriptionPedagogiqueElementRelationship(inscripPead);
		note.setToTypeNoteRelationship(typeNote);
		note.setNoSession(noSession);	
		note.setNoteSurchargee(null);
		return note;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSession(int noSession) {
		return noSession() != null && noSession().equals(noSession);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isCalculee() {
		return toTypeNote().isCalculee();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSaisie() {
		return toTypeNote().isSaisie();
	}

	public void setToNoteResultatRelationship(INoteResultat value) {
		super.setToNoteResultatRelationship((EONoteResultat)value);
		
	}

	public EONoteResultat toNoteResultat() {
		return super.toNoteResultat();
	}

	@Override
	public void setNoteInitiale(BigDecimal value) {
		super.setNoteInitiale(value);
		if (value != null) {
			setAbsence(null);
			setCodeAbsence(null);
		}
	}
	
}
