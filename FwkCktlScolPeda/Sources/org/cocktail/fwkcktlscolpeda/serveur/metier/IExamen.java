package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;


/**
 * 
 * @author isabelle
 *
 */
public interface IExamen {
	
	/**
	 * 
	 * @return la clé primaire
	 */
	Long idExamenLong();

	/**
	 * 
	 * @return La clé primaire du composant AE
	 */
	Long idComposantAELong();
	
	/**
	 * 
	 * @return annee universitaire
	 */
	Integer annee();

	/**
	 * 
	 * @param value the annee universitaire
	 */
	void setAnnee(Integer value);


	/**
	 * 
	 * @return état de l'examen
	 */
	IExamenEtat toExamenEtat();

	/**
	 * 
	 * @param value the état de l'examen
	 */
	void setToExamenEtatRelationship(IExamenEtat value);

	/**
	 * 
	 * @return le lien de l'ae correspondant à l'examen
	 */
	IAE toComposantAE();

	/**
	 * 
	 * @param value the le lien de l'ae correspondant à l'examen
	 */
	void setToComposantAERelationship(IAE value);
	  
	  /**
	   * 
	   * @return la session periode
	   */
	ISessionPeriode toSessionPeriode();
		  
	  /**
	   * 
	   * @param value the la session periode
	   */
	void setToSessionPeriodeRelationship(ISessionPeriode value);
	
	/**
	 * @return La liste des organisations de cet examen
	 */
	List<? extends IExamenOrganisation> toExamenOrganisations();

}