// DO NOT EDIT.  Make changes to EOMention.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOMention extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Mention";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("typeFormation");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships
  public static final String TYPE_FORMATION_KEY = TYPE_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOMention.class);

  public EOMention localInstanceIn(EOEditingContext editingContext) {
    EOMention localInstance = (EOMention)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOMention.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOMention.LOG.isDebugEnabled()) {
    	_EOMention.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMention.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOMention.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOMention.LOG.isDebugEnabled()) {
    	_EOMention.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMention.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOMention.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOMention.LOG.isDebugEnabled()) {
    	_EOMention.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMention.READ_ONLY_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation typeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(_EOMention.TYPE_FORMATION_KEY);
  }
  
  public void setTypeFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    takeStoredValueForKey(value, _EOMention.TYPE_FORMATION_KEY);
  }

  public void setTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (_EOMention.LOG.isDebugEnabled()) {
      _EOMention.LOG.debug("updating typeFormation from " + typeFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = typeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMention.TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMention.TYPE_FORMATION_KEY);
    }
  }
  

  public static EOMention createSco_Mention(EOEditingContext editingContext, String code
, String libelle
, Integer readOnly
) {
    EOMention eo = (EOMention) EOUtilities.createAndInsertInstance(editingContext, _EOMention.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EOMention> fetchSpec() {
    return new ERXFetchSpecification<EOMention>(_EOMention.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOMention> fetchAllSco_Mentions(EOEditingContext editingContext) {
    return _EOMention.fetchAllSco_Mentions(editingContext, null);
  }

  public static NSArray<EOMention> fetchAllSco_Mentions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMention.fetchSco_Mentions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMention> fetchSco_Mentions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOMention> fetchSpec = new ERXFetchSpecification<EOMention>(_EOMention.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMention> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOMention fetchSco_Mention(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMention.fetchSco_Mention(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMention fetchSco_Mention(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMention> eoObjects = _EOMention.fetchSco_Mentions(editingContext, qualifier, null);
    EOMention eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Mention that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMention fetchRequiredSco_Mention(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMention.fetchRequiredSco_Mention(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMention fetchRequiredSco_Mention(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMention eoObject = _EOMention.fetchSco_Mention(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Mention that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMention localInstanceIn(EOEditingContext editingContext, EOMention eo) {
    EOMention localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
