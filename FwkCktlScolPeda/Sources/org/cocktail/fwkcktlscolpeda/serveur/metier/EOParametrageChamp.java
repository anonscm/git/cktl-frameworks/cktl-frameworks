package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOParametrageChamp extends _EOParametrageChamp {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParametrageChamp.class);
	
	public static final String TYPE_GESTIONNAIRE = "gestionnaire";
	public static final String TYPE_REINSCRITPTION = "reinscription";
	public static final String TYPE_PREINSCRIPTION = "preinscription";

	/**
	 * On stocke un cache par paramétrage (Initial...)
	 */
	private static NSMutableDictionary<String, ERXEnterpriseObjectCache<EOParametrageChamp>> caches =
			new NSMutableDictionary<String, ERXEnterpriseObjectCache<EOParametrageChamp>>();
	
	/**
	 * Met un cache dans le cache des caches -_-
	 * @param parametrageChamp le cache des paramétrages
	 * @param parametrage le paramétrage ('Initial'...)
	 */
	public static void setChampCacheForParametrage(ERXEnterpriseObjectCache<EOParametrageChamp> parametrageChamp, String parametrage) {
		caches.setObjectForKey(parametrageChamp, parametrage);
	}
	
	/**
	 * @param parametrage le paramétrage ('Initial'...)
	 * @return le cache des paramétrages de champ en fonction du paramétrage donné
	 */
	public static ERXEnterpriseObjectCache<EOParametrageChamp> getChampCacheForParametrage(String parametrage) {
		ERXEnterpriseObjectCache<EOParametrageChamp> cache = caches.objectForKey(parametrage);
		if (cache == null) {
			EOQualifier qual = TO_PARAMETRAGE_INSCRIPTION.dot(EOParametrageInscription.LIBELLE_KEY).eq(parametrage);
			cache = new ERXEnterpriseObjectCache<EOParametrageChamp>(ENTITY_NAME, TO_CHAMP.dot(EOChamp.CODE_KEY).key(), qual, 1000 * 60 * 5, true, false);
			setChampCacheForParametrage(cache, parametrage);
		}
		return cache;
	}
	
	/**
	 * @param ec un ec
	 * @param codeChamp le code champ (ex: codeInsee)
	 * @param parametrage le paramétrage ('Initial'...) 
	 * @return le paramétrage champ correspondant
	 */
	public static EOParametrageChamp parametrageChampFor(EOEditingContext ec, String codeChamp, String parametrage) {
		EOParametrageChamp parametrageChamp =
				getChampCacheForParametrage(parametrage).objectForKey(ec, codeChamp);
		return parametrageChamp;
	}

	private void checkTypeParametreAffichage(String typeParametreAffichage) {
		List<String> typesConnus =  Arrays.asList(EOTypeParametreAffichage.INVISIBLE, 
				EOTypeParametreAffichage.LECTURE, EOTypeParametreAffichage.MODIFIABLE);
		if (!typesConnus.contains(typeParametreAffichage)) {
			throw new IllegalArgumentException("Le typeParametrageAffichage " + typeParametreAffichage + " est inconnu ");
		}
	}
	
	private boolean aTypeParametreAffichage(String typeConnexion, String typeParametreAffichage) {
		boolean result = false;
		checkTypeParametreAffichage(typeParametreAffichage);
		checkTypeParametreAffichage(toTypeGestionnaire().code());
		if (TYPE_GESTIONNAIRE.equals(typeConnexion)) {
			result = typeParametreAffichage.equals(toTypeGestionnaire().code());
		} else if (TYPE_REINSCRITPTION.equals(typeConnexion)) {
			result = typeParametreAffichage.equals(toTypeReinscription().code());
		} else if (TYPE_PREINSCRIPTION.equals(typeConnexion)) {
			result = typeParametreAffichage.equals(toTypePreinscription().code());
		} 
		return result;
	}
	
	/**
	 * @param typeConnexion le type de connexion (inscription, reinscription, admission)
	 * @return true si le champ est modifiable pour le type de connexion
	 */
	public boolean isModifiable(String typeConnexion) {
		return aTypeParametreAffichage(typeConnexion, EOTypeParametreAffichage.MODIFIABLE);
	}
	
	/**
	 * @param typeConnexion le type de connexion (inscription, reinscription, admission)
	 * @return true si le champ est lisible pour le type de connexion
	 */
	public boolean isLisible(String typeConnexion) {
		return aTypeParametreAffichage(typeConnexion, EOTypeParametreAffichage.LECTURE);
	}
	
	/**
	 * @param typeConnexion le type de connexion (inscription, reinscription, admission)
	 * @return true si le champ est invisible pour le type de connexion
	 */
	public boolean isInvisible(String typeConnexion) {
		return aTypeParametreAffichage(typeConnexion, EOTypeParametreAffichage.INVISIBLE);
	}
	
}
