package org.cocktail.fwkcktlscolpeda.serveur.metier;

public class ModePaiementVirementPrelevement implements IModePaiement {

	private static ModePaiementVirementPrelevement modePaiementVirementPrelevement;
	
	public String code() {
		return IModePaiement.CODE_MODE_VIREMENT_PRELEVEMENT;
	}

	public String libelle() {
		return "Virement et prélèvement";
	}

	public String modCodePaiement() {
		return IModePaiement.CODE_MODE_VIREMENT_PRELEVEMENT;
	}

	public String modCodeRemboursement() {
		return IModePaiement.CODE_MODE_VIREMENT_PRELEVEMENT;
	}
	
	public Integer ordre() {
		return 12;
	}

	public String getPlanComptablePaiementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}

	public String getPlanComptableRemboursementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}
	
	public static IModePaiement instance() {
		if (modePaiementVirementPrelevement == null) {
			modePaiementVirementPrelevement = new ModePaiementVirementPrelevement();
		}
		return modePaiementVirementPrelevement;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSimple() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isPrelevement() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isCheque() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
    public boolean isVirement() {
    	return false;
    }
}
