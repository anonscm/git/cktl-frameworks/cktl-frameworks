// DO NOT EDIT.  Make changes to EOTypePeriode.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypePeriode extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypePeriode";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID_TYPE_PERIODE = new ERXKey<Integer>("idTypePeriode");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_TYPE_PERIODE_KEY = ID_TYPE_PERIODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypePeriode.class);

  public EOTypePeriode localInstanceIn(EOEditingContext editingContext) {
    EOTypePeriode localInstance = (EOTypePeriode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypePeriode.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypePeriode.LOG.isDebugEnabled()) {
    	_EOTypePeriode.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePeriode.CODE_KEY);
  }

  public Integer idTypePeriode() {
    return (Integer) storedValueForKey(_EOTypePeriode.ID_TYPE_PERIODE_KEY);
  }

  public void setIdTypePeriode(Integer value) {
    if (_EOTypePeriode.LOG.isDebugEnabled()) {
    	_EOTypePeriode.LOG.debug( "updating idTypePeriode from " + idTypePeriode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePeriode.ID_TYPE_PERIODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypePeriode.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypePeriode.LOG.isDebugEnabled()) {
    	_EOTypePeriode.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePeriode.LIBELLE_KEY);
  }


  public static EOTypePeriode createSco_TypePeriode(EOEditingContext editingContext, String code
, Integer idTypePeriode
, String libelle
) {
    EOTypePeriode eo = (EOTypePeriode) EOUtilities.createAndInsertInstance(editingContext, _EOTypePeriode.ENTITY_NAME);    
		eo.setCode(code);
		eo.setIdTypePeriode(idTypePeriode);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypePeriode> fetchSpec() {
    return new ERXFetchSpecification<EOTypePeriode>(_EOTypePeriode.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypePeriode> fetchAllSco_TypePeriodes(EOEditingContext editingContext) {
    return _EOTypePeriode.fetchAllSco_TypePeriodes(editingContext, null);
  }

  public static NSArray<EOTypePeriode> fetchAllSco_TypePeriodes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypePeriode.fetchSco_TypePeriodes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypePeriode> fetchSco_TypePeriodes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypePeriode> fetchSpec = new ERXFetchSpecification<EOTypePeriode>(_EOTypePeriode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypePeriode> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypePeriode fetchSco_TypePeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePeriode.fetchSco_TypePeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePeriode fetchSco_TypePeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypePeriode> eoObjects = _EOTypePeriode.fetchSco_TypePeriodes(editingContext, qualifier, null);
    EOTypePeriode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypePeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePeriode fetchRequiredSco_TypePeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePeriode.fetchRequiredSco_TypePeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePeriode fetchRequiredSco_TypePeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypePeriode eoObject = _EOTypePeriode.fetchSco_TypePeriode(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypePeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePeriode localInstanceIn(EOEditingContext editingContext, EOTypePeriode eo) {
    EOTypePeriode localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
