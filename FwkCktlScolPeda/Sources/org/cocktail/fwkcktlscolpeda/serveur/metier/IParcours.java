package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un parcours.
 * 
 * @author Pascal MACOUIN
 */
public interface IParcours extends IComposant {
	/**
	 * @return <code>true</code> si le parcours est un parcours type
	 */
	Boolean parcoursType();
	
	/**
	 * 
	 * @param unParcoursType <code>true</code> si le parcours est un parcours type
	 */
	void setParcoursType(Boolean unParcoursType);
	
	/**
	 * @return Le code SISE du parcours
	 */
	String sise();
	
	/**
	 * @param unCodeSise Un code SISE pour le parcours
	 */
	void setSise(String unCodeSise);
}
