package org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces;

public interface ITypeEtudiant {
	String CODE_CLASSIQUE = "CL";
	String CODE_FORMATION_CONTINUE = "FO";
	String CODE_AUDITEUR_LIBRE= "AL";
	String CODE_REDOUBLANT = "RD";
	String CODE_CHANGEMENT_ORIENTATION = "CH";
	String CODE_ECHANGE_INTERNATIONNAL ="EI";

}
