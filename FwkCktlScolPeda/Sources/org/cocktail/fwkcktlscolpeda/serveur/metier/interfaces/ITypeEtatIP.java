package org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces;

/**
 * Interface poour la table SCO_SCOLARITE.TYPE_ETAT_IP
 */
public interface ITypeEtatIP {

	String CODE_VALIDE = "VALIDE";
	String CODE_EN_COURS = "EN_COURS";
	String CODE_ATTENTE_VALIDATION = "ATTENTE_VALIDATION";
	
}
