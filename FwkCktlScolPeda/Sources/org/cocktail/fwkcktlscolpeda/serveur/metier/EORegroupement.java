package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXKey;
import er.extensions.validation.ERXValidationFactory;


/**
 * Cette classe gère les regroupements.
 * @author lmo
 */
public class EORegroupement extends _EORegroupement implements IRegroupement {
	// Private fields
	private static final long serialVersionUID = 1L;
	
	public static final String ANNEES_KEY = "annees";
	public static final ERXKey<NSArray<Integer>> ANNEES = new ERXKey<NSArray<Integer>>(ANNEES_KEY);

	// Properties
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeRegroupement(editingContext());
	}
		
	
	@Override
	public void initialise() {

	}
		
	// Public methods	
	/**
	 * {@inheritDoc}
	 */
	public EORegroupement copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			//TODO : récupérer le persId par injection du user
			EORegroupement regroupement = EOComposantFactory.createComposant(EORegroupement.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(regroupement);
			return regroupement;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @return IRegroupement : le composant suivant associe au composant si il existe
	 */
	public IRegroupement getComposantAvecOrigine() {
		if(super.getComposantSuivant() != null) {
			return ((IRegroupement) (super.getComposantSuivant()));
		}
		
		return null;
	}
	
	/**
	 * @return IRegroupement : le composant d'origine
	 */
	public IRegroupement getComposantOrigine() {
		return (IRegroupement) composantOrigine();
	}
	
	/**
     * Valide choixMax
     * 
     * @param choixMax : le choix max
     * @return Object : le choix max
     * @throws ValidationException : exception
     */
	public Object validateChoixMax(Object choixMax) throws ValidationException {
    	if (choixMax == null && this.choixMin() != null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Regroupement.choixMax.NullAlorsQueChoixMinNonNull");
    	}
    	
    	if (choixMax != null && this.choixMin() == null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Regroupement.choixMax.NonNullAlorsQueChoixMinNull");
    	}
    	
    	/*System.err.println("choixMax.getClass().getName() : "+choixMax.getClass().getName());
    	if (choixMax != null && this.choixMin() != null) {
    		if ((new BigDecimal(choixMax)).compareTo(new BigDecimal(this.choixMin())) < 0) {
    			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Regroupement.choixMax.InferieurAChoixMin");
    		}
    	}*/
		
		return choixMax;
    }
	
	 //Private methods
	private void assignTo(EORegroupement regroupement) {
		super.assignTo(regroupement);
		regroupement.setChoixECTS(this.choixECTS());
		regroupement.setChoixMax(this.choixMax());
		regroupement.setChoixMin(this.choixMin());
	}   
	

	/**
	 *  {@inheritDoc}
	 */
	public Boolean isChoixMinMax() {
		return this.choixMin() != null && this.choixMax() != null;
	}
	
	/**
	 *  {@inheritDoc}
	 */
	public Boolean isChoixEcts() {
		return this.choixECTS() != null;
	}
	
	
	
}
