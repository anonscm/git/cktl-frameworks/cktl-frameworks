package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe represente une habilitation.
 */
public class EOHabilitation extends _EOHabilitation {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOHabilitation.class);


	/**
	 * @param edc contexte d'edition
	 * @param anneeDebut debut d'habilitation
	 * @param anneeFin annee de fin d'habilitation
	 * @return habilitation creee et inseree dans le contexte d'edition
	 */
	public static EOHabilitation creer(EOEditingContext edc, Integer anneeDebut, Integer anneeFin) {
		EOHabilitation habilitation = new EOHabilitation();
		habilitation.setAnneeDebut(anneeDebut);
		habilitation.setAnneeFin(anneeFin);
		edc.insertObject(habilitation);

		return habilitation;
	}
	
	/**
	 * Suppression d'une habilitation.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		try {
			Enumeration<EODiplome> objects = diplomes().immutableClone().objectEnumerator();
		    while (objects.hasMoreElements()) {
		    	EODiplome diplome = objects.nextElement();
		    	removeFromDiplomesRelationship(diplome);
		    }

			this.delete();
		} catch (Exception e) {
			System.err.println("Erreur suppression habilitation : " + e.getMessage());
			throw e;
		}
	}

	public EODiplome getDiplome() {
		if (diplomes()!=null && diplomes().size()>0) {
			return diplomes().get(0);
		}
		
		return null;
	}
	
	/**
	 * Valide anneeFin
	 * @param anneeFin : annee que l'on souhaite valider
	 * @throws ValidationException : exception lancee lorsque l'annee de fin n'est pas valide
	 * @return l'annee validee si elle est correcte
	 */
	public Object validateAnneeFin(Integer anneeFin) throws ValidationException {
		if (anneeDebut() != null) {
			if (anneeFin < this.anneeDebut()) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Habilitation.DateFinAvantDateDebut");
			}
		}

		return anneeFin;
	}

}
