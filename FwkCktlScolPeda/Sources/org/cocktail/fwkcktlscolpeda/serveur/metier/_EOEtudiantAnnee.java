// DO NOT EDIT.  Make changes to EOEtudiantAnnee.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEtudiantAnnee extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_EtudiantAnnee";

  // Attribute Keys
  public static final ERXKey<Boolean> ACCORD_ANNUAIRE = new ERXKey<Boolean>("accordAnnuaire");
  public static final ERXKey<Boolean> ACCORD_PHOTO = new ERXKey<Boolean>("accordPhoto");
  public static final ERXKey<Boolean> ADH_MUTUELLE = new ERXKey<Boolean>("adhMutuelle");
  public static final ERXKey<Boolean> AFF_SS = new ERXKey<Boolean>("affSs");
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Boolean> ASSURANCE_CIVILE = new ERXKey<Boolean>("assuranceCivile");
  public static final ERXKey<String> ASSURANCE_CIVILE_CIE = new ERXKey<String>("assuranceCivileCie");
  public static final ERXKey<String> ASSURANCE_CIVILE_NUM = new ERXKey<String>("assuranceCivileNum");
  public static final ERXKey<String> AUTRE_CAS_EXO = new ERXKey<String>("autreCasExo");
  public static final ERXKey<Integer> AYANT_DROIT = new ERXKey<Integer>("ayantDroit");
  public static final ERXKey<Integer> CERTIFICAT_PART_APPEL = new ERXKey<Integer>("certificatPartAppel");
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<Boolean> DEMANDEUR_EMPLOI = new ERXKey<Boolean>("demandeurEmploi");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> DUREE_INTERRUPT = new ERXKey<Integer>("dureeInterrupt");
  public static final ERXKey<String> EMAIL_PARENT = new ERXKey<String>("emailParent");
  public static final ERXKey<Boolean> ENFANTS_CHARGE = new ERXKey<Boolean>("enfantsCharge");
  public static final ERXKey<String> ETAB_BAC_ETRANGER = new ERXKey<String>("etabBacEtranger");
  public static final ERXKey<Boolean> ETUD_HANDICAP = new ERXKey<Boolean>("etudHandicap");
  public static final ERXKey<String> ETUD_NO_ORIGINE = new ERXKey<String>("etudNoOrigine");
  public static final ERXKey<Boolean> ETUD_SPORT_HN = new ERXKey<Boolean>("etudSportHN");
  public static final ERXKey<Boolean> FORMATION_FINANCEE = new ERXKey<Boolean>("formationFinancee");
  public static final ERXKey<Boolean> INTERRUPT_ETUDE = new ERXKey<Boolean>("interruptEtude");
  public static final ERXKey<Integer> NB_ENFANTS_CHARGE = new ERXKey<Integer>("nbEnfantsCharge");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> RECENSEMENT = new ERXKey<Integer>("recensement");
  public static final ERXKey<Boolean> RESPECT_CHARTE = new ERXKey<Boolean>("respectCharte");
  public static final ERXKey<String> SAL_CRNE = new ERXKey<String>("salCrne");
  public static final ERXKey<String> SAL_LIBELLE = new ERXKey<String>("salLibelle");
  public static final ERXKey<String> SPORT_PRATIQUE = new ERXKey<String>("sportPratique");
  public static final ERXKey<Boolean> SPORT_UNIV = new ERXKey<Boolean>("sportUniv");
  public static final ERXKey<String> STATUT_BOURSIER_CNOUS = new ERXKey<String>("statutBoursierCnous");
  public static final ERXKey<String> THAN_CODE = new ERXKey<String>("thanCode");
  public static final ERXKey<String> TUTEUR1 = new ERXKey<String>("tuteur1");
  public static final ERXKey<String> TUTEUR2 = new ERXKey<String>("tuteur2");
  public static final ERXKey<String> TYPE_REGIME = new ERXKey<String>("typeRegime");
  public static final ERXKey<Boolean> VALIDE = new ERXKey<Boolean>("valide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle> TO_ACTIVITE_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle>("toActiviteProfessionnelle");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation> TO_COTISATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation>("toCotisation");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> TO_GROUPE_ETUDIANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>("toGroupeEtudiants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> TO_INSCRIPTIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>("toInscriptions");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle> TO_MUTUELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle>("toMutuelle");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif> TO_NIVEAU_SPORTIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif>("toNiveauSportif");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources> TO_ORIGINE_RESSOURCES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources>("toOrigineRessources");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> TO_PAIEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>("toPaiements");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION1 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession1");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION2 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession2");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail> TO_QUOTITE_TRAVAIL = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail>("toQuotiteTravail");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier> TO_REGIME_PARTICULIER = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier>("toRegimeParticulier");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale> TO_SITUATION_FAMILIALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale>("toSituationFamiliale");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle> TO_SITUATION_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle>("toSituationProfessionnelle");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap> TO_TYPE_HANDICAP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap>("toTypeHandicap");

  // Attributes
  public static final String ACCORD_ANNUAIRE_KEY = ACCORD_ANNUAIRE.key();
  public static final String ACCORD_PHOTO_KEY = ACCORD_PHOTO.key();
  public static final String ADH_MUTUELLE_KEY = ADH_MUTUELLE.key();
  public static final String AFF_SS_KEY = AFF_SS.key();
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String ASSURANCE_CIVILE_KEY = ASSURANCE_CIVILE.key();
  public static final String ASSURANCE_CIVILE_CIE_KEY = ASSURANCE_CIVILE_CIE.key();
  public static final String ASSURANCE_CIVILE_NUM_KEY = ASSURANCE_CIVILE_NUM.key();
  public static final String AUTRE_CAS_EXO_KEY = AUTRE_CAS_EXO.key();
  public static final String AYANT_DROIT_KEY = AYANT_DROIT.key();
  public static final String CERTIFICAT_PART_APPEL_KEY = CERTIFICAT_PART_APPEL.key();
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String DEMANDEUR_EMPLOI_KEY = DEMANDEUR_EMPLOI.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String DUREE_INTERRUPT_KEY = DUREE_INTERRUPT.key();
  public static final String EMAIL_PARENT_KEY = EMAIL_PARENT.key();
  public static final String ENFANTS_CHARGE_KEY = ENFANTS_CHARGE.key();
  public static final String ETAB_BAC_ETRANGER_KEY = ETAB_BAC_ETRANGER.key();
  public static final String ETUD_HANDICAP_KEY = ETUD_HANDICAP.key();
  public static final String ETUD_NO_ORIGINE_KEY = ETUD_NO_ORIGINE.key();
  public static final String ETUD_SPORT_HN_KEY = ETUD_SPORT_HN.key();
  public static final String FORMATION_FINANCEE_KEY = FORMATION_FINANCEE.key();
  public static final String INTERRUPT_ETUDE_KEY = INTERRUPT_ETUDE.key();
  public static final String NB_ENFANTS_CHARGE_KEY = NB_ENFANTS_CHARGE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String RECENSEMENT_KEY = RECENSEMENT.key();
  public static final String RESPECT_CHARTE_KEY = RESPECT_CHARTE.key();
  public static final String SAL_CRNE_KEY = SAL_CRNE.key();
  public static final String SAL_LIBELLE_KEY = SAL_LIBELLE.key();
  public static final String SPORT_PRATIQUE_KEY = SPORT_PRATIQUE.key();
  public static final String SPORT_UNIV_KEY = SPORT_UNIV.key();
  public static final String STATUT_BOURSIER_CNOUS_KEY = STATUT_BOURSIER_CNOUS.key();
  public static final String THAN_CODE_KEY = THAN_CODE.key();
  public static final String TUTEUR1_KEY = TUTEUR1.key();
  public static final String TUTEUR2_KEY = TUTEUR2.key();
  public static final String TYPE_REGIME_KEY = TYPE_REGIME.key();
  public static final String VALIDE_KEY = VALIDE.key();
  // Relationships
  public static final String TO_ACTIVITE_PROFESSIONNELLE_KEY = TO_ACTIVITE_PROFESSIONNELLE.key();
  public static final String TO_COTISATION_KEY = TO_COTISATION.key();
  public static final String TO_ETUDIANT_KEY = TO_ETUDIANT.key();
  public static final String TO_GROUPE_ETUDIANTS_KEY = TO_GROUPE_ETUDIANTS.key();
  public static final String TO_INSCRIPTIONS_KEY = TO_INSCRIPTIONS.key();
  public static final String TO_MUTUELLE_KEY = TO_MUTUELLE.key();
  public static final String TO_NIVEAU_SPORTIF_KEY = TO_NIVEAU_SPORTIF.key();
  public static final String TO_ORIGINE_RESSOURCES_KEY = TO_ORIGINE_RESSOURCES.key();
  public static final String TO_PAIEMENTS_KEY = TO_PAIEMENTS.key();
  public static final String TO_PROFESSION1_KEY = TO_PROFESSION1.key();
  public static final String TO_PROFESSION2_KEY = TO_PROFESSION2.key();
  public static final String TO_QUOTITE_TRAVAIL_KEY = TO_QUOTITE_TRAVAIL.key();
  public static final String TO_REGIME_PARTICULIER_KEY = TO_REGIME_PARTICULIER.key();
  public static final String TO_RNE_KEY = TO_RNE.key();
  public static final String TO_SITUATION_FAMILIALE_KEY = TO_SITUATION_FAMILIALE.key();
  public static final String TO_SITUATION_PROFESSIONNELLE_KEY = TO_SITUATION_PROFESSIONNELLE.key();
  public static final String TO_TYPE_HANDICAP_KEY = TO_TYPE_HANDICAP.key();

  private static Logger LOG = Logger.getLogger(_EOEtudiantAnnee.class);

  public EOEtudiantAnnee localInstanceIn(EOEditingContext editingContext) {
    EOEtudiantAnnee localInstance = (EOEtudiantAnnee)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean accordAnnuaire() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ACCORD_ANNUAIRE_KEY);
  }

  public void setAccordAnnuaire(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating accordAnnuaire from " + accordAnnuaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ACCORD_ANNUAIRE_KEY);
  }

  public Boolean accordPhoto() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ACCORD_PHOTO_KEY);
  }

  public void setAccordPhoto(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating accordPhoto from " + accordPhoto() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ACCORD_PHOTO_KEY);
  }

  public Boolean adhMutuelle() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ADH_MUTUELLE_KEY);
  }

  public void setAdhMutuelle(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating adhMutuelle from " + adhMutuelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ADH_MUTUELLE_KEY);
  }

  public Boolean affSs() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.AFF_SS_KEY);
  }

  public void setAffSs(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating affSs from " + affSs() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.AFF_SS_KEY);
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ANNEE_KEY);
  }

  public Boolean assuranceCivile() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ASSURANCE_CIVILE_KEY);
  }

  public void setAssuranceCivile(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating assuranceCivile from " + assuranceCivile() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ASSURANCE_CIVILE_KEY);
  }

  public String assuranceCivileCie() {
    return (String) storedValueForKey(_EOEtudiantAnnee.ASSURANCE_CIVILE_CIE_KEY);
  }

  public void setAssuranceCivileCie(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating assuranceCivileCie from " + assuranceCivileCie() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ASSURANCE_CIVILE_CIE_KEY);
  }

  public String assuranceCivileNum() {
    return (String) storedValueForKey(_EOEtudiantAnnee.ASSURANCE_CIVILE_NUM_KEY);
  }

  public void setAssuranceCivileNum(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating assuranceCivileNum from " + assuranceCivileNum() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ASSURANCE_CIVILE_NUM_KEY);
  }

  public String autreCasExo() {
    return (String) storedValueForKey(_EOEtudiantAnnee.AUTRE_CAS_EXO_KEY);
  }

  public void setAutreCasExo(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating autreCasExo from " + autreCasExo() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.AUTRE_CAS_EXO_KEY);
  }

  public Integer ayantDroit() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.AYANT_DROIT_KEY);
  }

  public void setAyantDroit(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating ayantDroit from " + ayantDroit() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.AYANT_DROIT_KEY);
  }

  public Integer certificatPartAppel() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.CERTIFICAT_PART_APPEL_KEY);
  }

  public void setCertificatPartAppel(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating certificatPartAppel from " + certificatPartAppel() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.CERTIFICAT_PART_APPEL_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOEtudiantAnnee.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.COMMENTAIRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOEtudiantAnnee.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.D_CREATION_KEY);
  }

  public Boolean demandeurEmploi() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.DEMANDEUR_EMPLOI_KEY);
  }

  public void setDemandeurEmploi(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating demandeurEmploi from " + demandeurEmploi() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.DEMANDEUR_EMPLOI_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOEtudiantAnnee.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.D_MODIFICATION_KEY);
  }

  public Integer dureeInterrupt() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.DUREE_INTERRUPT_KEY);
  }

  public void setDureeInterrupt(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating dureeInterrupt from " + dureeInterrupt() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.DUREE_INTERRUPT_KEY);
  }

  public String emailParent() {
    return (String) storedValueForKey(_EOEtudiantAnnee.EMAIL_PARENT_KEY);
  }

  public void setEmailParent(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating emailParent from " + emailParent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.EMAIL_PARENT_KEY);
  }

  public Boolean enfantsCharge() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ENFANTS_CHARGE_KEY);
  }

  public void setEnfantsCharge(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating enfantsCharge from " + enfantsCharge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ENFANTS_CHARGE_KEY);
  }

  public String etabBacEtranger() {
    return (String) storedValueForKey(_EOEtudiantAnnee.ETAB_BAC_ETRANGER_KEY);
  }

  public void setEtabBacEtranger(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating etabBacEtranger from " + etabBacEtranger() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ETAB_BAC_ETRANGER_KEY);
  }

  public Boolean etudHandicap() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ETUD_HANDICAP_KEY);
  }

  public void setEtudHandicap(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating etudHandicap from " + etudHandicap() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ETUD_HANDICAP_KEY);
  }

  public String etudNoOrigine() {
    return (String) storedValueForKey(_EOEtudiantAnnee.ETUD_NO_ORIGINE_KEY);
  }

  public void setEtudNoOrigine(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating etudNoOrigine from " + etudNoOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ETUD_NO_ORIGINE_KEY);
  }

  public Boolean etudSportHN() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.ETUD_SPORT_HN_KEY);
  }

  public void setEtudSportHN(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating etudSportHN from " + etudSportHN() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.ETUD_SPORT_HN_KEY);
  }

  public Boolean formationFinancee() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.FORMATION_FINANCEE_KEY);
  }

  public void setFormationFinancee(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating formationFinancee from " + formationFinancee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.FORMATION_FINANCEE_KEY);
  }

  public Boolean interruptEtude() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.INTERRUPT_ETUDE_KEY);
  }

  public void setInterruptEtude(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating interruptEtude from " + interruptEtude() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.INTERRUPT_ETUDE_KEY);
  }

  public Integer nbEnfantsCharge() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.NB_ENFANTS_CHARGE_KEY);
  }

  public void setNbEnfantsCharge(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating nbEnfantsCharge from " + nbEnfantsCharge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.NB_ENFANTS_CHARGE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.PERS_ID_MODIFICATION_KEY);
  }

  public Integer recensement() {
    return (Integer) storedValueForKey(_EOEtudiantAnnee.RECENSEMENT_KEY);
  }

  public void setRecensement(Integer value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating recensement from " + recensement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.RECENSEMENT_KEY);
  }

  public Boolean respectCharte() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.RESPECT_CHARTE_KEY);
  }

  public void setRespectCharte(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating respectCharte from " + respectCharte() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.RESPECT_CHARTE_KEY);
  }

  public String salCrne() {
    return (String) storedValueForKey(_EOEtudiantAnnee.SAL_CRNE_KEY);
  }

  public void setSalCrne(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating salCrne from " + salCrne() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.SAL_CRNE_KEY);
  }

  public String salLibelle() {
    return (String) storedValueForKey(_EOEtudiantAnnee.SAL_LIBELLE_KEY);
  }

  public void setSalLibelle(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating salLibelle from " + salLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.SAL_LIBELLE_KEY);
  }

  public String sportPratique() {
    return (String) storedValueForKey(_EOEtudiantAnnee.SPORT_PRATIQUE_KEY);
  }

  public void setSportPratique(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating sportPratique from " + sportPratique() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.SPORT_PRATIQUE_KEY);
  }

  public Boolean sportUniv() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.SPORT_UNIV_KEY);
  }

  public void setSportUniv(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating sportUniv from " + sportUniv() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.SPORT_UNIV_KEY);
  }

  public String statutBoursierCnous() {
    return (String) storedValueForKey(_EOEtudiantAnnee.STATUT_BOURSIER_CNOUS_KEY);
  }

  public void setStatutBoursierCnous(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating statutBoursierCnous from " + statutBoursierCnous() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.STATUT_BOURSIER_CNOUS_KEY);
  }

  public String thanCode() {
    return (String) storedValueForKey(_EOEtudiantAnnee.THAN_CODE_KEY);
  }

  public void setThanCode(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating thanCode from " + thanCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.THAN_CODE_KEY);
  }

  public String tuteur1() {
    return (String) storedValueForKey(_EOEtudiantAnnee.TUTEUR1_KEY);
  }

  public void setTuteur1(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating tuteur1 from " + tuteur1() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.TUTEUR1_KEY);
  }

  public String tuteur2() {
    return (String) storedValueForKey(_EOEtudiantAnnee.TUTEUR2_KEY);
  }

  public void setTuteur2(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating tuteur2 from " + tuteur2() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.TUTEUR2_KEY);
  }

  public String typeRegime() {
    return (String) storedValueForKey(_EOEtudiantAnnee.TYPE_REGIME_KEY);
  }

  public void setTypeRegime(String value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating typeRegime from " + typeRegime() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.TYPE_REGIME_KEY);
  }

  public Boolean valide() {
    return (Boolean) storedValueForKey(_EOEtudiantAnnee.VALIDE_KEY);
  }

  public void setValide(Boolean value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
    	_EOEtudiantAnnee.LOG.debug( "updating valide from " + valide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtudiantAnnee.VALIDE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle toActiviteProfessionnelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle)storedValueForKey(_EOEtudiantAnnee.TO_ACTIVITE_PROFESSIONNELLE_KEY);
  }
  
  public void setToActiviteProfessionnelle(org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_ACTIVITE_PROFESSIONNELLE_KEY);
  }

  public void setToActiviteProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toActiviteProfessionnelle from " + toActiviteProfessionnelle() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToActiviteProfessionnelle(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle oldValue = toActiviteProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_ACTIVITE_PROFESSIONNELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_ACTIVITE_PROFESSIONNELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation toCotisation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation)storedValueForKey(_EOEtudiantAnnee.TO_COTISATION_KEY);
  }
  
  public void setToCotisation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_COTISATION_KEY);
  }

  public void setToCotisationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toCotisation from " + toCotisation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToCotisation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation oldValue = toCotisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_COTISATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_COTISATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant)storedValueForKey(_EOEtudiantAnnee.TO_ETUDIANT_KEY);
  }
  
  public void setToEtudiant(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_ETUDIANT_KEY);
  }

  public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toEtudiant from " + toEtudiant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle toMutuelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle)storedValueForKey(_EOEtudiantAnnee.TO_MUTUELLE_KEY);
  }
  
  public void setToMutuelle(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_MUTUELLE_KEY);
  }

  public void setToMutuelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toMutuelle from " + toMutuelle() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToMutuelle(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle oldValue = toMutuelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_MUTUELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_MUTUELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif toNiveauSportif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif)storedValueForKey(_EOEtudiantAnnee.TO_NIVEAU_SPORTIF_KEY);
  }
  
  public void setToNiveauSportif(org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_NIVEAU_SPORTIF_KEY);
  }

  public void setToNiveauSportifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toNiveauSportif from " + toNiveauSportif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToNiveauSportif(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif oldValue = toNiveauSportif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_NIVEAU_SPORTIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_NIVEAU_SPORTIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources toOrigineRessources() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources)storedValueForKey(_EOEtudiantAnnee.TO_ORIGINE_RESSOURCES_KEY);
  }
  
  public void setToOrigineRessources(org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_ORIGINE_RESSOURCES_KEY);
  }

  public void setToOrigineRessourcesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toOrigineRessources from " + toOrigineRessources() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToOrigineRessources(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources oldValue = toOrigineRessources();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_ORIGINE_RESSOURCES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_ORIGINE_RESSOURCES_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession1() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(_EOEtudiantAnnee.TO_PROFESSION1_KEY);
  }
  
  public void setToProfession1(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_PROFESSION1_KEY);
  }

  public void setToProfession1Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toProfession1 from " + toProfession1() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToProfession1(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession1();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_PROFESSION1_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_PROFESSION1_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession2() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(_EOEtudiantAnnee.TO_PROFESSION2_KEY);
  }
  
  public void setToProfession2(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_PROFESSION2_KEY);
  }

  public void setToProfession2Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toProfession2 from " + toProfession2() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToProfession2(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_PROFESSION2_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_PROFESSION2_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail toQuotiteTravail() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail)storedValueForKey(_EOEtudiantAnnee.TO_QUOTITE_TRAVAIL_KEY);
  }
  
  public void setToQuotiteTravail(org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_QUOTITE_TRAVAIL_KEY);
  }

  public void setToQuotiteTravailRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toQuotiteTravail from " + toQuotiteTravail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToQuotiteTravail(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail oldValue = toQuotiteTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_QUOTITE_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_QUOTITE_TRAVAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier toRegimeParticulier() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier)storedValueForKey(_EOEtudiantAnnee.TO_REGIME_PARTICULIER_KEY);
  }
  
  public void setToRegimeParticulier(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_REGIME_PARTICULIER_KEY);
  }

  public void setToRegimeParticulierRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toRegimeParticulier from " + toRegimeParticulier() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRegimeParticulier(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier oldValue = toRegimeParticulier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_REGIME_PARTICULIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_REGIME_PARTICULIER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(_EOEtudiantAnnee.TO_RNE_KEY);
  }
  
  public void setToRne(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRne(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toSituationFamiliale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale)storedValueForKey(_EOEtudiantAnnee.TO_SITUATION_FAMILIALE_KEY);
  }
  
  public void setToSituationFamiliale(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_SITUATION_FAMILIALE_KEY);
  }

  public void setToSituationFamilialeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toSituationFamiliale from " + toSituationFamiliale() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToSituationFamiliale(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale oldValue = toSituationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_SITUATION_FAMILIALE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle toSituationProfessionnelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle)storedValueForKey(_EOEtudiantAnnee.TO_SITUATION_PROFESSIONNELLE_KEY);
  }
  
  public void setToSituationProfessionnelle(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_SITUATION_PROFESSIONNELLE_KEY);
  }

  public void setToSituationProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toSituationProfessionnelle from " + toSituationProfessionnelle() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToSituationProfessionnelle(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle oldValue = toSituationProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_SITUATION_PROFESSIONNELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_SITUATION_PROFESSIONNELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap toTypeHandicap() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap)storedValueForKey(_EOEtudiantAnnee.TO_TYPE_HANDICAP_KEY);
  }
  
  public void setToTypeHandicap(org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap value) {
    takeStoredValueForKey(value, _EOEtudiantAnnee.TO_TYPE_HANDICAP_KEY);
  }

  public void setToTypeHandicapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap value) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("updating toTypeHandicap from " + toTypeHandicap() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeHandicap(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap oldValue = toTypeHandicap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtudiantAnnee.TO_TYPE_HANDICAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtudiantAnnee.TO_TYPE_HANDICAP_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)storedValueForKey(_EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier) {
    return toGroupeEtudiants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier, boolean fetch) {
    return toGroupeEtudiants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.fetchSco_GroupeEtudiants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGroupeEtudiants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGroupeEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    includeObjectIntoPropertyWithKey(object, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
  }

  public void removeFromToGroupeEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    excludeObjectFromPropertyWithKey(object, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
  }

  public void addToToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("adding " + object + " to toGroupeEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToGroupeEtudiants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
    }
  }

  public void removeFromToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("removing " + object + " from toGroupeEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToGroupeEtudiants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant createToGroupeEtudiantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant) eo;
  }

  public void deleteToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_GROUPE_ETUDIANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGroupeEtudiantsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> objects = toGroupeEtudiants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGroupeEtudiantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> toInscriptions() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)storedValueForKey(_EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> toInscriptions(EOQualifier qualifier) {
    return toInscriptions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> toInscriptions(EOQualifier qualifier, boolean fetch) {
    return toInscriptions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> toInscriptions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.TO_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.fetchSco_Inscriptions(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toInscriptions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToInscriptions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    includeObjectIntoPropertyWithKey(object, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
  }

  public void removeFromToInscriptions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    excludeObjectFromPropertyWithKey(object, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
  }

  public void addToToInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("adding " + object + " to toInscriptions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToInscriptions(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
    }
  }

  public void removeFromToInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("removing " + object + " from toInscriptions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToInscriptions(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription createToInscriptionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription) eo;
  }

  public void deleteToInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_INSCRIPTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToInscriptionsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> objects = toInscriptions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToInscriptionsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)storedValueForKey(_EOEtudiantAnnee.TO_PAIEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier) {
    return toPaiements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier, boolean fetch) {
    return toPaiements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> toPaiements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.TO_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.fetchSco_Paiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPaiements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPaiements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
  }

  public void removeFromToPaiements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
  }

  public void addToToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("adding " + object + " to toPaiements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPaiements(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
    }
  }

  public void removeFromToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    if (_EOEtudiantAnnee.LOG.isDebugEnabled()) {
      _EOEtudiantAnnee.LOG.debug("removing " + object + " from toPaiements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPaiements(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement createToPaiementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement) eo;
  }

  public void deleteToPaiementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOEtudiantAnnee.TO_PAIEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPaiementsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> objects = toPaiements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPaiementsRelationship(objects.nextElement());
    }
  }


  public static EOEtudiantAnnee createSco_EtudiantAnnee(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, Integer nbEnfantsCharge
, Integer persIdCreation
, Integer persIdModification
, Boolean valide
, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant) {
    EOEtudiantAnnee eo = (EOEtudiantAnnee) EOUtilities.createAndInsertInstance(editingContext, _EOEtudiantAnnee.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setNbEnfantsCharge(nbEnfantsCharge);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setValide(valide);
    eo.setToEtudiantRelationship(toEtudiant);
    return eo;
  }

  public static ERXFetchSpecification<EOEtudiantAnnee> fetchSpec() {
    return new ERXFetchSpecification<EOEtudiantAnnee>(_EOEtudiantAnnee.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEtudiantAnnee> fetchAllSco_EtudiantAnnees(EOEditingContext editingContext) {
    return _EOEtudiantAnnee.fetchAllSco_EtudiantAnnees(editingContext, null);
  }

  public static NSArray<EOEtudiantAnnee> fetchAllSco_EtudiantAnnees(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEtudiantAnnee.fetchSco_EtudiantAnnees(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEtudiantAnnee> fetchSco_EtudiantAnnees(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEtudiantAnnee> fetchSpec = new ERXFetchSpecification<EOEtudiantAnnee>(_EOEtudiantAnnee.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEtudiantAnnee> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEtudiantAnnee fetchSco_EtudiantAnnee(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtudiantAnnee.fetchSco_EtudiantAnnee(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtudiantAnnee fetchSco_EtudiantAnnee(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEtudiantAnnee> eoObjects = _EOEtudiantAnnee.fetchSco_EtudiantAnnees(editingContext, qualifier, null);
    EOEtudiantAnnee eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_EtudiantAnnee that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtudiantAnnee fetchRequiredSco_EtudiantAnnee(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtudiantAnnee.fetchRequiredSco_EtudiantAnnee(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtudiantAnnee fetchRequiredSco_EtudiantAnnee(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEtudiantAnnee eoObject = _EOEtudiantAnnee.fetchSco_EtudiantAnnee(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_EtudiantAnnee that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtudiantAnnee localInstanceIn(EOEditingContext editingContext, EOEtudiantAnnee eo) {
    EOEtudiantAnnee localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
