// DO NOT EDIT.  Make changes to EOTypeAE.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeAE extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeAE";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAE.class);

  public EOTypeAE localInstanceIn(EOEditingContext editingContext) {
    EOTypeAE localInstance = (EOTypeAE)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeAE.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeAE.LOG.isDebugEnabled()) {
    	_EOTypeAE.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAE.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeAE.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeAE.LOG.isDebugEnabled()) {
    	_EOTypeAE.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAE.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeAE.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeAE.LOG.isDebugEnabled()) {
    	_EOTypeAE.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAE.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOTypeAE.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOTypeAE.LOG.isDebugEnabled()) {
    	_EOTypeAE.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAE.READ_ONLY_KEY);
  }


  public static EOTypeAE createSco_TypeAE(EOEditingContext editingContext, String code
, Integer id
, String libelle
, Integer readOnly
) {
    EOTypeAE eo = (EOTypeAE) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAE.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeAE> fetchSpec() {
    return new ERXFetchSpecification<EOTypeAE>(_EOTypeAE.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeAE> fetchAllSco_TypeAEs(EOEditingContext editingContext) {
    return _EOTypeAE.fetchAllSco_TypeAEs(editingContext, null);
  }

  public static NSArray<EOTypeAE> fetchAllSco_TypeAEs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAE.fetchSco_TypeAEs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAE> fetchSco_TypeAEs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeAE> fetchSpec = new ERXFetchSpecification<EOTypeAE>(_EOTypeAE.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAE> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeAE fetchSco_TypeAE(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAE.fetchSco_TypeAE(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAE fetchSco_TypeAE(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAE> eoObjects = _EOTypeAE.fetchSco_TypeAEs(editingContext, qualifier, null);
    EOTypeAE eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeAE that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAE fetchRequiredSco_TypeAE(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAE.fetchRequiredSco_TypeAE(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAE fetchRequiredSco_TypeAE(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAE eoObject = _EOTypeAE.fetchSco_TypeAE(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeAE that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAE localInstanceIn(EOEditingContext editingContext, EOTypeAE eo) {
    EOTypeAE localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
