// DO NOT EDIT.  Make changes to EOGroupe.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOGroupe extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Groupe";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> TO_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("toComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> TO_GROUPE_ETUDIANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>("toGroupeEtudiants");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_COMPOSANT_KEY = TO_COMPOSANT.key();
  public static final String TO_GROUPE_ETUDIANTS_KEY = TO_GROUPE_ETUDIANTS.key();

  private static Logger LOG = Logger.getLogger(_EOGroupe.class);

  public EOGroupe localInstanceIn(EOEditingContext editingContext) {
    EOGroupe localInstance = (EOGroupe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOGroupe.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOGroupe.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.DATE_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOGroupe.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOGroupe.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.ORDRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOGroupe.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOGroupe.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
    	_EOGroupe.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGroupe.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant toComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOGroupe.TO_COMPOSANT_KEY);
  }
  
  public void setToComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOGroupe.TO_COMPOSANT_KEY);
  }

  public void setToComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
      _EOGroupe.LOG.debug("updating toComposant from " + toComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = toComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGroupe.TO_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGroupe.TO_COMPOSANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)storedValueForKey(_EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier) {
    return toGroupeEtudiants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier, boolean fetch) {
    return toGroupeEtudiants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> toGroupeEtudiants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.TO_GROUPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.fetchSco_GroupeEtudiants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGroupeEtudiants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGroupeEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    includeObjectIntoPropertyWithKey(object, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
  }

  public void removeFromToGroupeEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    excludeObjectFromPropertyWithKey(object, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
  }

  public void addToToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
      _EOGroupe.LOG.debug("adding " + object + " to toGroupeEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToGroupeEtudiants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
    }
  }

  public void removeFromToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    if (_EOGroupe.LOG.isDebugEnabled()) {
      _EOGroupe.LOG.debug("removing " + object + " from toGroupeEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToGroupeEtudiants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant createToGroupeEtudiantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant) eo;
  }

  public void deleteToGroupeEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOGroupe.TO_GROUPE_ETUDIANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGroupeEtudiantsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant> objects = toGroupeEtudiants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGroupeEtudiantsRelationship(objects.nextElement());
    }
  }


  public static EOGroupe createSco_Groupe(EOEditingContext editingContext, NSTimestamp dateCreation
, String libelle
, Integer ordre
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant toComposant) {
    EOGroupe eo = (EOGroupe) EOUtilities.createAndInsertInstance(editingContext, _EOGroupe.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
		eo.setPersIdCreation(persIdCreation);
    eo.setToComposantRelationship(toComposant);
    return eo;
  }

  public static ERXFetchSpecification<EOGroupe> fetchSpec() {
    return new ERXFetchSpecification<EOGroupe>(_EOGroupe.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOGroupe> fetchAllSco_Groupes(EOEditingContext editingContext) {
    return _EOGroupe.fetchAllSco_Groupes(editingContext, null);
  }

  public static NSArray<EOGroupe> fetchAllSco_Groupes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGroupe.fetchSco_Groupes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGroupe> fetchSco_Groupes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOGroupe> fetchSpec = new ERXFetchSpecification<EOGroupe>(_EOGroupe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGroupe> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOGroupe fetchSco_Groupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGroupe.fetchSco_Groupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGroupe fetchSco_Groupe(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGroupe> eoObjects = _EOGroupe.fetchSco_Groupes(editingContext, qualifier, null);
    EOGroupe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Groupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGroupe fetchRequiredSco_Groupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGroupe.fetchRequiredSco_Groupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGroupe fetchRequiredSco_Groupe(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGroupe eoObject = _EOGroupe.fetchSco_Groupe(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Groupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGroupe localInstanceIn(EOEditingContext editingContext, EOGroupe eo) {
    EOGroupe localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
