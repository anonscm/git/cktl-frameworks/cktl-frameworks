package org.cocktail.fwkcktlscolpeda.serveur.metier.administration;

import java.util.Map;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEC;

/**
 * Classe qui gère l'appel à la procédure d'import dans PreEtudiant
 */
public class ProcedureImportDonneesPreEtudiants {

	public static final String IMPORT_DONNEES_PRE_ETUDIANTS = "ImportLauncher";
	
	/**
	 * Lance la procédure
	 * @param params les paramètres à passer lors de l'execution de la procédure
	 * @throws Exception en cas d'erreur
	 */
	public void execute(Map<String, Object> params) throws Exception {
		try {
			EOUtilities.executeStoredProcedureNamed(ERXEC.newEditingContext(), IMPORT_DONNEES_PRE_ETUDIANTS, new NSDictionary<String, Object>(params));
		} catch (Exception e) {
			throw e;
		}
	}
	
}
