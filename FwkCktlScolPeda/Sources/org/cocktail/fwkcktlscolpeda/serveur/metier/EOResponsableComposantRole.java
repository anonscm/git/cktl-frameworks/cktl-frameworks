package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;

/**
 * {@inheritDoc}
 */
public class EOResponsableComposantRole extends _EOResponsableComposantRole implements IResponsableComposantRole {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOResponsableComposantRole.class);

	/**
	 * {@inheritDoc}
	 */
	public EOResponsableComposantRole copy(IResponsableComposant responsablecomposant) {
		EOResponsableComposantRole role = (EOResponsableComposantRole) EOUtilities
		    .createAndInsertInstance(editingContext(), EOResponsableComposantRole.ENTITY_NAME);
		role.setResponsableComposant((EOResponsableComposant) responsablecomposant);
		role.setRoleRelationship(this.role());
		return role;
	}
}
