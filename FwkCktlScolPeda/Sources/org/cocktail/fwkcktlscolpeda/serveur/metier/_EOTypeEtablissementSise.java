// DO NOT EDIT.  Make changes to EOTypeEtablissementSise.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeEtablissementSise extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeEtablissementSise";

  // Attribute Keys
  public static final ERXKey<String> CODE_SISE = new ERXKey<String>("codeSise");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_SISE_KEY = CODE_SISE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeEtablissementSise.class);

  public EOTypeEtablissementSise localInstanceIn(EOEditingContext editingContext) {
    EOTypeEtablissementSise localInstance = (EOTypeEtablissementSise)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeSise() {
    return (String) storedValueForKey(_EOTypeEtablissementSise.CODE_SISE_KEY);
  }

  public void setCodeSise(String value) {
    if (_EOTypeEtablissementSise.LOG.isDebugEnabled()) {
    	_EOTypeEtablissementSise.LOG.debug( "updating codeSise from " + codeSise() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEtablissementSise.CODE_SISE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeEtablissementSise.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeEtablissementSise.LOG.isDebugEnabled()) {
    	_EOTypeEtablissementSise.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEtablissementSise.LIBELLE_KEY);
  }


  public static EOTypeEtablissementSise createSco_TypeEtablissementSise(EOEditingContext editingContext, String codeSise
, String libelle
) {
    EOTypeEtablissementSise eo = (EOTypeEtablissementSise) EOUtilities.createAndInsertInstance(editingContext, _EOTypeEtablissementSise.ENTITY_NAME);    
		eo.setCodeSise(codeSise);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeEtablissementSise> fetchSpec() {
    return new ERXFetchSpecification<EOTypeEtablissementSise>(_EOTypeEtablissementSise.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeEtablissementSise> fetchAllSco_TypeEtablissementSises(EOEditingContext editingContext) {
    return _EOTypeEtablissementSise.fetchAllSco_TypeEtablissementSises(editingContext, null);
  }

  public static NSArray<EOTypeEtablissementSise> fetchAllSco_TypeEtablissementSises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeEtablissementSise.fetchSco_TypeEtablissementSises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeEtablissementSise> fetchSco_TypeEtablissementSises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeEtablissementSise> fetchSpec = new ERXFetchSpecification<EOTypeEtablissementSise>(_EOTypeEtablissementSise.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeEtablissementSise> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeEtablissementSise fetchSco_TypeEtablissementSise(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtablissementSise.fetchSco_TypeEtablissementSise(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtablissementSise fetchSco_TypeEtablissementSise(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeEtablissementSise> eoObjects = _EOTypeEtablissementSise.fetchSco_TypeEtablissementSises(editingContext, qualifier, null);
    EOTypeEtablissementSise eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeEtablissementSise that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtablissementSise fetchRequiredSco_TypeEtablissementSise(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtablissementSise.fetchRequiredSco_TypeEtablissementSise(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtablissementSise fetchRequiredSco_TypeEtablissementSise(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeEtablissementSise eoObject = _EOTypeEtablissementSise.fetchSco_TypeEtablissementSise(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeEtablissementSise that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtablissementSise localInstanceIn(EOEditingContext editingContext, EOTypeEtablissementSise eo) {
    EOTypeEtablissementSise localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
