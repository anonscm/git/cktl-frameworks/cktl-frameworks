package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EORegimeInscription extends _EORegimeInscription implements IRegimeInscription {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORegimeInscription.class);
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.libelle();
	}
	
}
