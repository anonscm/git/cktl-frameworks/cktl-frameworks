package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOTypeInscriptionFormation extends _EOTypeInscriptionFormation implements ITypeInscriptionFormation {
	private static ERXEnterpriseObjectCache<EOTypeInscriptionFormation> typeInscFormCache;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeInscriptionFormation.class);
	
	 /**
	    * @return le cache des {@link EOTypeInscriptionPeda} par leur code
	    */
	   public static ERXEnterpriseObjectCache<EOTypeInscriptionFormation> getTypeInscFormCache() {
	   	if (typeInscFormCache == null) {
	   		typeInscFormCache = new ERXEnterpriseObjectCache<EOTypeInscriptionFormation>(EOTypeInscriptionFormation.class, CODE_INSCRIPTION_KEY);
	   	}
	   	return typeInscFormCache;
	   }
	   
	   /**
		 * @param edc l'editingContext
		 * @return le {@link EOTypeInscriptionPeda} "PRINCIPAL"
		 */
		public static EOTypeInscriptionFormation typePrincipal(EOEditingContext edc) {
			return getTypeInscFormCache().objectForKey(edc, ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		}
		
		public static void setTypeInscFormCache(ERXEnterpriseObjectCache<EOTypeInscriptionFormation> typeInscFormCache) {
			EOTypeInscriptionFormation.typeInscFormCache = typeInscFormCache;
		}
	   
}
