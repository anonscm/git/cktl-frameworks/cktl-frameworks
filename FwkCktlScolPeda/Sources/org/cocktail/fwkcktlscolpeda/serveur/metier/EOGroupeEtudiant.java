package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Cette classe représente l'appartenance d'un étudiant à un groupe.
 * 
 * @author Pascal MACOUIN
 */
public class EOGroupeEtudiant extends _EOGroupeEtudiant implements IGroupeEtudiant {
	private static final long serialVersionUID = 8022161768222547845L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOGroupeEtudiant.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantAnneeRelationship(IScoEtudiantAnnee unEtudiantAnnee) {
		super.setToEtudiantAnneeRelationship((EOEtudiantAnnee) unEtudiantAnnee);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToGroupeRelationship(IGroupe unGroupe) {
		super.setToGroupeRelationship((EOGroupe) unGroupe);
	}

	/**
     * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}
}
