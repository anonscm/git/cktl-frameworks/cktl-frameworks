// DO NOT EDIT.  Make changes to EODomaineComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODomaineComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DomaineComposant";

  // Attribute Keys
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<Integer> DOMAINE_ID = new ERXKey<Integer>("domaineId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaine");

  // Attributes
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String DOMAINE_ID_KEY = DOMAINE_ID.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String DOMAINE_KEY = DOMAINE.key();

  private static Logger LOG = Logger.getLogger(_EODomaineComposant.class);

  public EODomaineComposant localInstanceIn(EOEditingContext editingContext) {
    EODomaineComposant localInstance = (EODomaineComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EODomaineComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EODomaineComposant.LOG.isDebugEnabled()) {
    	_EODomaineComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaineComposant.COMPOSANT_ID_KEY);
  }

  public Integer domaineId() {
    return (Integer) storedValueForKey(_EODomaineComposant.DOMAINE_ID_KEY);
  }

  public void setDomaineId(Integer value) {
    if (_EODomaineComposant.LOG.isDebugEnabled()) {
    	_EODomaineComposant.LOG.debug( "updating domaineId from " + domaineId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODomaineComposant.DOMAINE_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EODomaineComposant.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EODomaineComposant.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EODomaineComposant.LOG.isDebugEnabled()) {
      _EODomaineComposant.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODomaineComposant.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODomaineComposant.COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine domaine() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine)storedValueForKey(_EODomaineComposant.DOMAINE_KEY);
  }
  
  public void setDomaine(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine value) {
    takeStoredValueForKey(value, _EODomaineComposant.DOMAINE_KEY);
  }

  public void setDomaineRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine value) {
    if (_EODomaineComposant.LOG.isDebugEnabled()) {
      _EODomaineComposant.LOG.debug("updating domaine from " + domaine() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDomaine(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine oldValue = domaine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODomaineComposant.DOMAINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODomaineComposant.DOMAINE_KEY);
    }
  }
  

  public static EODomaineComposant createSco_DomaineComposant(EOEditingContext editingContext, Integer composantId
, Integer domaineId
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant, org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine domaine) {
    EODomaineComposant eo = (EODomaineComposant) EOUtilities.createAndInsertInstance(editingContext, _EODomaineComposant.ENTITY_NAME);    
		eo.setComposantId(composantId);
		eo.setDomaineId(domaineId);
    eo.setComposantRelationship(composant);
    eo.setDomaineRelationship(domaine);
    return eo;
  }

  public static ERXFetchSpecification<EODomaineComposant> fetchSpec() {
    return new ERXFetchSpecification<EODomaineComposant>(_EODomaineComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODomaineComposant> fetchAllSco_DomaineComposants(EOEditingContext editingContext) {
    return _EODomaineComposant.fetchAllSco_DomaineComposants(editingContext, null);
  }

  public static NSArray<EODomaineComposant> fetchAllSco_DomaineComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODomaineComposant.fetchSco_DomaineComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EODomaineComposant> fetchSco_DomaineComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODomaineComposant> fetchSpec = new ERXFetchSpecification<EODomaineComposant>(_EODomaineComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODomaineComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODomaineComposant fetchSco_DomaineComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EODomaineComposant.fetchSco_DomaineComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODomaineComposant fetchSco_DomaineComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODomaineComposant> eoObjects = _EODomaineComposant.fetchSco_DomaineComposants(editingContext, qualifier, null);
    EODomaineComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DomaineComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODomaineComposant fetchRequiredSco_DomaineComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EODomaineComposant.fetchRequiredSco_DomaineComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODomaineComposant fetchRequiredSco_DomaineComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EODomaineComposant eoObject = _EODomaineComposant.fetchSco_DomaineComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DomaineComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODomaineComposant localInstanceIn(EOEditingContext editingContext, EODomaineComposant eo) {
    EODomaineComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
