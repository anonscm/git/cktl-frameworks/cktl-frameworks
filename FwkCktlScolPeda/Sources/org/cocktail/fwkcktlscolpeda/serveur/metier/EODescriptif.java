package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Enumeration;

import com.webobjects.foundation.NSTimestamp;

import er.extensions.localization.ERXLocalizer;

/**
 * Descriptif d'un composant
 */
public class EODescriptif extends _EODescriptif implements IDescriptif {
	// Private fields
	private static final long serialVersionUID = 1L;

	// Public Methods
	/**
	 * intialisation du descriptif avec des valeurs par défaut
	 */
	public void initialise() {
		setDestinationRelationship(EOTypeDescriptifDestination.typePublication(editingContext()));
		setDestinationId(EOTypeDescriptifDestination.typePublication(editingContext()).id());
		setLibelle(ERXLocalizer.defaultLocalizer().localizedStringForKey("EODescriptif.libelle"));
		setVersion(ERXLocalizer.defaultLocalizer().localizedStringForKey("EODescriptif.version"));

		setAnnee(new NSTimestamp());
	}

	/**
	 * Suppression d'un descriptif.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		try {
			// Supprimer tous les champs contenus
			deleteAllDescriptifChampContenusRelationships();
	
			// Supprimer tous les selection_lien_descriptif
			Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> objects = liens().immutableClone().objectEnumerator();
			while (objects.hasMoreElements()) {
				EOLien lien = objects.nextElement();
				removeObjectFromBothSidesOfRelationshipWithKey(lien, _EODescriptif.LIENS_KEY);
			}
			
			// Supprimer tous les liens utiles
			deleteAllLiensUtilesRelationships();
	
			// Supprimer la relation avec le composant
			removeObjectFromBothSidesOfRelationshipWithKey(composant(), _EODescriptif.COMPOSANT_KEY);
			
			// Supprimer l'objet
			delete();
		} catch (Exception e) {
			System.err.println("Erreur suppression descriptif : " + e.getMessage());
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public String getContenuChamp(String titreChamp) {
		EODescriptifChampContenu champContenu = EODescriptifChampContenu.fetchSco_DescriptifChampContenu(editingContext(),
		    EODescriptifChampContenu.DESCRIPTIF_CHAMP_TITRE.dot(EODescriptifChampTitre.CODE).eq(titreChamp).and(EODescriptifChampContenu.DESCRIPTIF.eq(this)));
		if (champContenu != null) {
			return champContenu.libelle();
		}
		return null;
	}
	
	@Override
	public String toString() {
		return this.libelle() + " - " + this.destination() + " - " + this.annee() + " - " + this.version();
	}
	
}
