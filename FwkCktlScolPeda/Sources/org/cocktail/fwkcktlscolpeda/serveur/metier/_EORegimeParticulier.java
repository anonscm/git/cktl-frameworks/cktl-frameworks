// DO NOT EDIT.  Make changes to EORegimeParticulier.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORegimeParticulier extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_RegimeParticulier";

  // Attribute Keys
  public static final ERXKey<Boolean> AFFILIATION_SS_ETUDIANTE = new ERXKey<Boolean>("affiliationSsEtudiante");
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String AFFILIATION_SS_ETUDIANTE_KEY = AFFILIATION_SS_ETUDIANTE.key();
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EORegimeParticulier.class);

  public EORegimeParticulier localInstanceIn(EOEditingContext editingContext) {
    EORegimeParticulier localInstance = (EORegimeParticulier)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean affiliationSsEtudiante() {
    return (Boolean) storedValueForKey(_EORegimeParticulier.AFFILIATION_SS_ETUDIANTE_KEY);
  }

  public void setAffiliationSsEtudiante(Boolean value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating affiliationSsEtudiante from " + affiliationSsEtudiante() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.AFFILIATION_SS_ETUDIANTE_KEY);
  }

  public String code() {
    return (String) storedValueForKey(_EORegimeParticulier.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EORegimeParticulier.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EORegimeParticulier.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EORegimeParticulier.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EORegimeParticulier.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.ORDRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EORegimeParticulier.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EORegimeParticulier.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EORegimeParticulier.LOG.isDebugEnabled()) {
    	_EORegimeParticulier.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeParticulier.PERS_ID_MODIFICATION_KEY);
  }


  public static EORegimeParticulier createSco_RegimeParticulier(EOEditingContext editingContext, Boolean affiliationSsEtudiante
, String code
, NSTimestamp dCreation
, String libelle
, Integer ordre
, Integer persIdCreation
) {
    EORegimeParticulier eo = (EORegimeParticulier) EOUtilities.createAndInsertInstance(editingContext, _EORegimeParticulier.ENTITY_NAME);    
		eo.setAffiliationSsEtudiante(affiliationSsEtudiante);
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EORegimeParticulier> fetchSpec() {
    return new ERXFetchSpecification<EORegimeParticulier>(_EORegimeParticulier.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORegimeParticulier> fetchAllSco_RegimeParticuliers(EOEditingContext editingContext) {
    return _EORegimeParticulier.fetchAllSco_RegimeParticuliers(editingContext, null);
  }

  public static NSArray<EORegimeParticulier> fetchAllSco_RegimeParticuliers(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORegimeParticulier.fetchSco_RegimeParticuliers(editingContext, null, sortOrderings);
  }

  public static NSArray<EORegimeParticulier> fetchSco_RegimeParticuliers(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORegimeParticulier> fetchSpec = new ERXFetchSpecification<EORegimeParticulier>(_EORegimeParticulier.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORegimeParticulier> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORegimeParticulier fetchSco_RegimeParticulier(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegimeParticulier.fetchSco_RegimeParticulier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegimeParticulier fetchSco_RegimeParticulier(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORegimeParticulier> eoObjects = _EORegimeParticulier.fetchSco_RegimeParticuliers(editingContext, qualifier, null);
    EORegimeParticulier eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_RegimeParticulier that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegimeParticulier fetchRequiredSco_RegimeParticulier(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegimeParticulier.fetchRequiredSco_RegimeParticulier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegimeParticulier fetchRequiredSco_RegimeParticulier(EOEditingContext editingContext, EOQualifier qualifier) {
    EORegimeParticulier eoObject = _EORegimeParticulier.fetchSco_RegimeParticulier(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_RegimeParticulier that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegimeParticulier localInstanceIn(EOEditingContext editingContext, EORegimeParticulier eo) {
    EORegimeParticulier localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
