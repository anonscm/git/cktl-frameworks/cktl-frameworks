// DO NOT EDIT.  Make changes to EOQuotiteTravail.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOQuotiteTravail extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_QuotiteTravail";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> QTRA_CODE = new ERXKey<String>("qtraCode");
  // Relationship Keys

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String QTRA_CODE_KEY = QTRA_CODE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOQuotiteTravail.class);

  public EOQuotiteTravail localInstanceIn(EOEditingContext editingContext) {
    EOQuotiteTravail localInstance = (EOQuotiteTravail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOQuotiteTravail.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOQuotiteTravail.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOQuotiteTravail.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOQuotiteTravail.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOQuotiteTravail.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.PERS_ID_MODIFICATION_KEY);
  }

  public String qtraCode() {
    return (String) storedValueForKey(_EOQuotiteTravail.QTRA_CODE_KEY);
  }

  public void setQtraCode(String value) {
    if (_EOQuotiteTravail.LOG.isDebugEnabled()) {
    	_EOQuotiteTravail.LOG.debug( "updating qtraCode from " + qtraCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOQuotiteTravail.QTRA_CODE_KEY);
  }


  public static EOQuotiteTravail createSco_QuotiteTravail(EOEditingContext editingContext, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
, String qtraCode
) {
    EOQuotiteTravail eo = (EOQuotiteTravail) EOUtilities.createAndInsertInstance(editingContext, _EOQuotiteTravail.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setQtraCode(qtraCode);
    return eo;
  }

  public static ERXFetchSpecification<EOQuotiteTravail> fetchSpec() {
    return new ERXFetchSpecification<EOQuotiteTravail>(_EOQuotiteTravail.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOQuotiteTravail> fetchAllSco_QuotiteTravails(EOEditingContext editingContext) {
    return _EOQuotiteTravail.fetchAllSco_QuotiteTravails(editingContext, null);
  }

  public static NSArray<EOQuotiteTravail> fetchAllSco_QuotiteTravails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOQuotiteTravail.fetchSco_QuotiteTravails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOQuotiteTravail> fetchSco_QuotiteTravails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOQuotiteTravail> fetchSpec = new ERXFetchSpecification<EOQuotiteTravail>(_EOQuotiteTravail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOQuotiteTravail> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOQuotiteTravail fetchSco_QuotiteTravail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQuotiteTravail.fetchSco_QuotiteTravail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQuotiteTravail fetchSco_QuotiteTravail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOQuotiteTravail> eoObjects = _EOQuotiteTravail.fetchSco_QuotiteTravails(editingContext, qualifier, null);
    EOQuotiteTravail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_QuotiteTravail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQuotiteTravail fetchRequiredSco_QuotiteTravail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOQuotiteTravail.fetchRequiredSco_QuotiteTravail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOQuotiteTravail fetchRequiredSco_QuotiteTravail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOQuotiteTravail eoObject = _EOQuotiteTravail.fetchSco_QuotiteTravail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_QuotiteTravail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOQuotiteTravail localInstanceIn(EOEditingContext editingContext, EOQuotiteTravail eo) {
    EOQuotiteTravail localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
