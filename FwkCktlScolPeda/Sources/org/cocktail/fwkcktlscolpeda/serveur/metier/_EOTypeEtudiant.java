// DO NOT EDIT.  Make changes to EOTypeEtudiant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeEtudiant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeEtudiant";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeEtudiant.class);

  public EOTypeEtudiant localInstanceIn(EOEditingContext editingContext) {
    EOTypeEtudiant localInstance = (EOTypeEtudiant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeEtudiant.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeEtudiant.LOG.isDebugEnabled()) {
    	_EOTypeEtudiant.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEtudiant.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeEtudiant.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeEtudiant.LOG.isDebugEnabled()) {
    	_EOTypeEtudiant.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEtudiant.LIBELLE_KEY);
  }


  public static EOTypeEtudiant createSco_TypeEtudiant(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeEtudiant eo = (EOTypeEtudiant) EOUtilities.createAndInsertInstance(editingContext, _EOTypeEtudiant.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeEtudiant> fetchSpec() {
    return new ERXFetchSpecification<EOTypeEtudiant>(_EOTypeEtudiant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeEtudiant> fetchAllSco_TypeEtudiants(EOEditingContext editingContext) {
    return _EOTypeEtudiant.fetchAllSco_TypeEtudiants(editingContext, null);
  }

  public static NSArray<EOTypeEtudiant> fetchAllSco_TypeEtudiants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeEtudiant.fetchSco_TypeEtudiants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeEtudiant> fetchSco_TypeEtudiants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeEtudiant> fetchSpec = new ERXFetchSpecification<EOTypeEtudiant>(_EOTypeEtudiant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeEtudiant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeEtudiant fetchSco_TypeEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtudiant.fetchSco_TypeEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtudiant fetchSco_TypeEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeEtudiant> eoObjects = _EOTypeEtudiant.fetchSco_TypeEtudiants(editingContext, qualifier, null);
    EOTypeEtudiant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtudiant fetchRequiredSco_TypeEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEtudiant.fetchRequiredSco_TypeEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEtudiant fetchRequiredSco_TypeEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeEtudiant eoObject = _EOTypeEtudiant.fetchSco_TypeEtudiant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEtudiant localInstanceIn(EOEditingContext editingContext, EOTypeEtudiant eo) {
    EOTypeEtudiant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
