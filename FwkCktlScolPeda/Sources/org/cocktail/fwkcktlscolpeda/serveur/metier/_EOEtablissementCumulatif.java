// DO NOT EDIT.  Make changes to EOEtablissementCumulatif.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEtablissementCumulatif extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_EtablissementCumulatif";

  // Attribute Keys
  public static final ERXKey<Boolean> VALIDE = new ERXKey<Boolean>("valide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");

  // Attributes
  public static final String VALIDE_KEY = VALIDE.key();
  // Relationships
  public static final String TO_RNE_KEY = TO_RNE.key();

  private static Logger LOG = Logger.getLogger(_EOEtablissementCumulatif.class);

  public EOEtablissementCumulatif localInstanceIn(EOEditingContext editingContext) {
    EOEtablissementCumulatif localInstance = (EOEtablissementCumulatif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean valide() {
    return (Boolean) storedValueForKey(_EOEtablissementCumulatif.VALIDE_KEY);
  }

  public void setValide(Boolean value) {
    if (_EOEtablissementCumulatif.LOG.isDebugEnabled()) {
    	_EOEtablissementCumulatif.LOG.debug( "updating valide from " + valide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEtablissementCumulatif.VALIDE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(_EOEtablissementCumulatif.TO_RNE_KEY);
  }
  
  public void setToRne(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    takeStoredValueForKey(value, _EOEtablissementCumulatif.TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOEtablissementCumulatif.LOG.isDebugEnabled()) {
      _EOEtablissementCumulatif.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRne(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEtablissementCumulatif.TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEtablissementCumulatif.TO_RNE_KEY);
    }
  }
  

  public static EOEtablissementCumulatif createSco_EtablissementCumulatif(EOEditingContext editingContext, Boolean valide
, org.cocktail.fwkcktlpersonne.common.metier.EORne toRne) {
    EOEtablissementCumulatif eo = (EOEtablissementCumulatif) EOUtilities.createAndInsertInstance(editingContext, _EOEtablissementCumulatif.ENTITY_NAME);    
		eo.setValide(valide);
    eo.setToRneRelationship(toRne);
    return eo;
  }

  public static ERXFetchSpecification<EOEtablissementCumulatif> fetchSpec() {
    return new ERXFetchSpecification<EOEtablissementCumulatif>(_EOEtablissementCumulatif.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEtablissementCumulatif> fetchAllSco_EtablissementCumulatifs(EOEditingContext editingContext) {
    return _EOEtablissementCumulatif.fetchAllSco_EtablissementCumulatifs(editingContext, null);
  }

  public static NSArray<EOEtablissementCumulatif> fetchAllSco_EtablissementCumulatifs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEtablissementCumulatif.fetchSco_EtablissementCumulatifs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEtablissementCumulatif> fetchSco_EtablissementCumulatifs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEtablissementCumulatif> fetchSpec = new ERXFetchSpecification<EOEtablissementCumulatif>(_EOEtablissementCumulatif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEtablissementCumulatif> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEtablissementCumulatif fetchSco_EtablissementCumulatif(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtablissementCumulatif.fetchSco_EtablissementCumulatif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtablissementCumulatif fetchSco_EtablissementCumulatif(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEtablissementCumulatif> eoObjects = _EOEtablissementCumulatif.fetchSco_EtablissementCumulatifs(editingContext, qualifier, null);
    EOEtablissementCumulatif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_EtablissementCumulatif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtablissementCumulatif fetchRequiredSco_EtablissementCumulatif(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEtablissementCumulatif.fetchRequiredSco_EtablissementCumulatif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEtablissementCumulatif fetchRequiredSco_EtablissementCumulatif(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEtablissementCumulatif eoObject = _EOEtablissementCumulatif.fetchSco_EtablissementCumulatif(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_EtablissementCumulatif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEtablissementCumulatif localInstanceIn(EOEditingContext editingContext, EOEtablissementCumulatif eo) {
    EOEtablissementCumulatif localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
