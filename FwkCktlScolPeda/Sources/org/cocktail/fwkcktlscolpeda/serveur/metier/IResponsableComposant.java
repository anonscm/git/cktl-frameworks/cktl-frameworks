package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

/**
 * responsable d'un composant
 */
public interface IResponsableComposant {

	/**
	 * @return personne (individu ou structure)  responsable
	 */
	IPersonne responsable();

	/**
	 * @return roles du responsable
	 */
	List<? extends IResponsableComposantRole> roles();

	/**
	 * @return composant sur lequel est positionné le responsable
	 */
	IComposant composant();

	/**
	 * @param autreComposant le composant sur lequel on copie le responsable et ses roles
	 * @return le responsable sur le nouveau composant
	 */
	IResponsableComposant copyToComposant(IComposant autreComposant);
	
	/**
	 * @return l'ID du composant
	 */
	Integer composantId();
}
