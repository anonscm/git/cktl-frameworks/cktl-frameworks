// DO NOT EDIT.  Make changes to EODescriptifTheme.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODescriptifTheme extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DescriptifTheme";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> DESCRIPTIF_CHAMP_TITRES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre>("descriptifChampTitres");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  // Relationships
  public static final String DESCRIPTIF_CHAMP_TITRES_KEY = DESCRIPTIF_CHAMP_TITRES.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EODescriptifTheme.class);

  public EODescriptifTheme localInstanceIn(EOEditingContext editingContext) {
    EODescriptifTheme localInstance = (EODescriptifTheme)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODescriptifTheme.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
    	_EODescriptifTheme.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifTheme.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODescriptifTheme.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
    	_EODescriptifTheme.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifTheme.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EODescriptifTheme.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
    	_EODescriptifTheme.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifTheme.ORDRE_KEY);
  }

  public Integer typeComposantId() {
    return (Integer) storedValueForKey(_EODescriptifTheme.TYPE_COMPOSANT_ID_KEY);
  }

  public void setTypeComposantId(Integer value) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
    	_EODescriptifTheme.LOG.debug( "updating typeComposantId from " + typeComposantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifTheme.TYPE_COMPOSANT_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant typeComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant)storedValueForKey(_EODescriptifTheme.TYPE_COMPOSANT_KEY);
  }
  
  public void setTypeComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    takeStoredValueForKey(value, _EODescriptifTheme.TYPE_COMPOSANT_KEY);
  }

  public void setTypeComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
      _EODescriptifTheme.LOG.debug("updating typeComposant from " + typeComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant oldValue = typeComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptifTheme.TYPE_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptifTheme.TYPE_COMPOSANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> descriptifChampTitres() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre>)storedValueForKey(_EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> descriptifChampTitres(EOQualifier qualifier) {
    return descriptifChampTitres(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> descriptifChampTitres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> results;
      results = descriptifChampTitres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDescriptifChampTitres(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre object) {
    includeObjectIntoPropertyWithKey(object, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
  }

  public void removeFromDescriptifChampTitres(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre object) {
    excludeObjectFromPropertyWithKey(object, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
  }

  public void addToDescriptifChampTitresRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre object) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
      _EODescriptifTheme.LOG.debug("adding " + object + " to descriptifChampTitres relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDescriptifChampTitres(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
    }
  }

  public void removeFromDescriptifChampTitresRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre object) {
    if (_EODescriptifTheme.LOG.isDebugEnabled()) {
      _EODescriptifTheme.LOG.debug("removing " + object + " from descriptifChampTitres relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDescriptifChampTitres(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre createDescriptifChampTitresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre) eo;
  }

  public void deleteDescriptifChampTitresRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptifTheme.DESCRIPTIF_CHAMP_TITRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDescriptifChampTitresRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampTitre> objects = descriptifChampTitres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDescriptifChampTitresRelationship(objects.nextElement());
    }
  }


  public static EODescriptifTheme createSco_DescriptifTheme(EOEditingContext editingContext, Integer id
, String libelle
, Integer typeComposantId
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant typeComposant) {
    EODescriptifTheme eo = (EODescriptifTheme) EOUtilities.createAndInsertInstance(editingContext, _EODescriptifTheme.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setTypeComposantId(typeComposantId);
    eo.setTypeComposantRelationship(typeComposant);
    return eo;
  }

  public static ERXFetchSpecification<EODescriptifTheme> fetchSpec() {
    return new ERXFetchSpecification<EODescriptifTheme>(_EODescriptifTheme.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODescriptifTheme> fetchAllSco_DescriptifThemes(EOEditingContext editingContext) {
    return _EODescriptifTheme.fetchAllSco_DescriptifThemes(editingContext, null);
  }

  public static NSArray<EODescriptifTheme> fetchAllSco_DescriptifThemes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODescriptifTheme.fetchSco_DescriptifThemes(editingContext, null, sortOrderings);
  }

  public static NSArray<EODescriptifTheme> fetchSco_DescriptifThemes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODescriptifTheme> fetchSpec = new ERXFetchSpecification<EODescriptifTheme>(_EODescriptifTheme.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODescriptifTheme> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODescriptifTheme fetchSco_DescriptifTheme(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifTheme.fetchSco_DescriptifTheme(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifTheme fetchSco_DescriptifTheme(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODescriptifTheme> eoObjects = _EODescriptifTheme.fetchSco_DescriptifThemes(editingContext, qualifier, null);
    EODescriptifTheme eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DescriptifTheme that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifTheme fetchRequiredSco_DescriptifTheme(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifTheme.fetchRequiredSco_DescriptifTheme(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifTheme fetchRequiredSco_DescriptifTheme(EOEditingContext editingContext, EOQualifier qualifier) {
    EODescriptifTheme eoObject = _EODescriptifTheme.fetchSco_DescriptifTheme(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DescriptifTheme that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifTheme localInstanceIn(EOEditingContext editingContext, EODescriptifTheme eo) {
    EODescriptifTheme localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
