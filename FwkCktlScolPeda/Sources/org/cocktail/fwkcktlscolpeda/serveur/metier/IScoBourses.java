package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

public interface IScoBourses extends IBourses {

	IEtudiant toEtudiant();

	void setToEtudiantRelationship(IEtudiant value);

//	public Integer annee();
//
//	public void setAnnee(Integer value);
//
//	public NSTimestamp dCreation();
//
//	public void setDCreation(NSTimestamp value);
//
//	public NSTimestamp dMofication();
//
//	public void setDMofication(NSTimestamp value);
//
//	public IEchelonBourse toEchelonBourse();
//
//	public void setToEchelonBourseRelationship(IEchelonBourse value);
//
//	public String numAllocataire();
//
//	public void setNumAllocataire(String value);
//
//	public String organisme();
//
//	public void setOrganisme(String value);
//
//	public void setPersIdCreation(Integer value);
//
//	public void setPersIdModification(Integer value);
//
//	public IEtudiant toEtudiant();
//
//	public void setToEtudiantRelationship(IEtudiant value);
//	public void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation value);
//	public ITypeInscriptionFormation toTypeInscriptionFormation();
//	
//	public Boolean isCampusFrance();
//
//	public void setIsCampusFrance(Boolean value);
//
//	public ITypeBoursesCF toTypeBoursesCF();
//
//	public void setToTypeBoursesCFRelationship(ITypeBoursesCF value);
}
