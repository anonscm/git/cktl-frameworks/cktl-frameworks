// DO NOT EDIT.  Make changes to EOSiseDiscipline.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSiseDiscipline extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SiseDiscipline";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> SISE_SECTEURS_DISCIPLINAIRES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire>("siseSecteursDisciplinaires");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String SISE_SECTEURS_DISCIPLINAIRES_KEY = SISE_SECTEURS_DISCIPLINAIRES.key();

  private static Logger LOG = Logger.getLogger(_EOSiseDiscipline.class);

  public EOSiseDiscipline localInstanceIn(EOEditingContext editingContext) {
    EOSiseDiscipline localInstance = (EOSiseDiscipline)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOSiseDiscipline.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOSiseDiscipline.LOG.isDebugEnabled()) {
    	_EOSiseDiscipline.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSiseDiscipline.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSiseDiscipline.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSiseDiscipline.LOG.isDebugEnabled()) {
    	_EOSiseDiscipline.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSiseDiscipline.LIBELLE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> siseSecteursDisciplinaires() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire>)storedValueForKey(_EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> siseSecteursDisciplinaires(EOQualifier qualifier) {
    return siseSecteursDisciplinaires(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> siseSecteursDisciplinaires(EOQualifier qualifier, boolean fetch) {
    return siseSecteursDisciplinaires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> siseSecteursDisciplinaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire.SISE_DISCIPLINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire.fetchSco_SiseSecteurDisciplinaires(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = siseSecteursDisciplinaires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSiseSecteursDisciplinaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire object) {
    includeObjectIntoPropertyWithKey(object, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
  }

  public void removeFromSiseSecteursDisciplinaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire object) {
    excludeObjectFromPropertyWithKey(object, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
  }

  public void addToSiseSecteursDisciplinairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire object) {
    if (_EOSiseDiscipline.LOG.isDebugEnabled()) {
      _EOSiseDiscipline.LOG.debug("adding " + object + " to siseSecteursDisciplinaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToSiseSecteursDisciplinaires(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
    }
  }

  public void removeFromSiseSecteursDisciplinairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire object) {
    if (_EOSiseDiscipline.LOG.isDebugEnabled()) {
      _EOSiseDiscipline.LOG.debug("removing " + object + " from siseSecteursDisciplinaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromSiseSecteursDisciplinaires(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire createSiseSecteursDisciplinairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire) eo;
  }

  public void deleteSiseSecteursDisciplinairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOSiseDiscipline.SISE_SECTEURS_DISCIPLINAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSiseSecteursDisciplinairesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> objects = siseSecteursDisciplinaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSiseSecteursDisciplinairesRelationship(objects.nextElement());
    }
  }


  public static EOSiseDiscipline createSco_SiseDiscipline(EOEditingContext editingContext, String code
, String libelle
) {
    EOSiseDiscipline eo = (EOSiseDiscipline) EOUtilities.createAndInsertInstance(editingContext, _EOSiseDiscipline.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOSiseDiscipline> fetchSpec() {
    return new ERXFetchSpecification<EOSiseDiscipline>(_EOSiseDiscipline.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSiseDiscipline> fetchAllSco_SiseDisciplines(EOEditingContext editingContext) {
    return _EOSiseDiscipline.fetchAllSco_SiseDisciplines(editingContext, null);
  }

  public static NSArray<EOSiseDiscipline> fetchAllSco_SiseDisciplines(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSiseDiscipline.fetchSco_SiseDisciplines(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSiseDiscipline> fetchSco_SiseDisciplines(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSiseDiscipline> fetchSpec = new ERXFetchSpecification<EOSiseDiscipline>(_EOSiseDiscipline.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSiseDiscipline> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSiseDiscipline fetchSco_SiseDiscipline(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiseDiscipline.fetchSco_SiseDiscipline(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiseDiscipline fetchSco_SiseDiscipline(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSiseDiscipline> eoObjects = _EOSiseDiscipline.fetchSco_SiseDisciplines(editingContext, qualifier, null);
    EOSiseDiscipline eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SiseDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiseDiscipline fetchRequiredSco_SiseDiscipline(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiseDiscipline.fetchRequiredSco_SiseDiscipline(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiseDiscipline fetchRequiredSco_SiseDiscipline(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSiseDiscipline eoObject = _EOSiseDiscipline.fetchSco_SiseDiscipline(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SiseDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiseDiscipline localInstanceIn(EOEditingContext editingContext, EOSiseDiscipline eo) {
    EOSiseDiscipline localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
