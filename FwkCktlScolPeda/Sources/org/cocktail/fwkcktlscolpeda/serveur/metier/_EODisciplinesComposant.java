// DO NOT EDIT.  Make changes to EODisciplinesComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODisciplinesComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DisciplinesComposant";

  // Attribute Keys
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<Integer> DISCIPLINE_ID = new ERXKey<Integer>("disciplineId");
  // Relationship Keys

  // Attributes
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String DISCIPLINE_ID_KEY = DISCIPLINE_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EODisciplinesComposant.class);

  public EODisciplinesComposant localInstanceIn(EOEditingContext editingContext) {
    EODisciplinesComposant localInstance = (EODisciplinesComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EODisciplinesComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EODisciplinesComposant.LOG.isDebugEnabled()) {
    	_EODisciplinesComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODisciplinesComposant.COMPOSANT_ID_KEY);
  }

  public Integer disciplineId() {
    return (Integer) storedValueForKey(_EODisciplinesComposant.DISCIPLINE_ID_KEY);
  }

  public void setDisciplineId(Integer value) {
    if (_EODisciplinesComposant.LOG.isDebugEnabled()) {
    	_EODisciplinesComposant.LOG.debug( "updating disciplineId from " + disciplineId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODisciplinesComposant.DISCIPLINE_ID_KEY);
  }


  public static EODisciplinesComposant createSco_DisciplinesComposant(EOEditingContext editingContext, Integer composantId
, Integer disciplineId
) {
    EODisciplinesComposant eo = (EODisciplinesComposant) EOUtilities.createAndInsertInstance(editingContext, _EODisciplinesComposant.ENTITY_NAME);    
		eo.setComposantId(composantId);
		eo.setDisciplineId(disciplineId);
    return eo;
  }

  public static ERXFetchSpecification<EODisciplinesComposant> fetchSpec() {
    return new ERXFetchSpecification<EODisciplinesComposant>(_EODisciplinesComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODisciplinesComposant> fetchAllSco_DisciplinesComposants(EOEditingContext editingContext) {
    return _EODisciplinesComposant.fetchAllSco_DisciplinesComposants(editingContext, null);
  }

  public static NSArray<EODisciplinesComposant> fetchAllSco_DisciplinesComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODisciplinesComposant.fetchSco_DisciplinesComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EODisciplinesComposant> fetchSco_DisciplinesComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODisciplinesComposant> fetchSpec = new ERXFetchSpecification<EODisciplinesComposant>(_EODisciplinesComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODisciplinesComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODisciplinesComposant fetchSco_DisciplinesComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EODisciplinesComposant.fetchSco_DisciplinesComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODisciplinesComposant fetchSco_DisciplinesComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODisciplinesComposant> eoObjects = _EODisciplinesComposant.fetchSco_DisciplinesComposants(editingContext, qualifier, null);
    EODisciplinesComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DisciplinesComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODisciplinesComposant fetchRequiredSco_DisciplinesComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EODisciplinesComposant.fetchRequiredSco_DisciplinesComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODisciplinesComposant fetchRequiredSco_DisciplinesComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EODisciplinesComposant eoObject = _EODisciplinesComposant.fetchSco_DisciplinesComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DisciplinesComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODisciplinesComposant localInstanceIn(EOEditingContext editingContext, EODisciplinesComposant eo) {
    EODisciplinesComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
