package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.ReportFicheInscriptionPedagogiqueData;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Générateur de données pour les impression des fiche d'inscription pédagogique.
 * 
 * @author Pascal MACOUIN
 */
public class ReportFicheInscriptionPedagogiqueDataGenerator implements IReportFicheInscriptionPedagogiqueDataGenerator {

	/**
	 * {@inheritDoc}
	 */
	public ReportImpressionData generateFicheInscriptionPedagogique(EOEditingContext editingContext, String mainLogoUrl, IInscriptionPedagogique inscriptionPedagogique) {
		ReportFicheInscriptionPedagogiqueData ficheInscriptionPedagogiqueData = new ReportFicheInscriptionPedagogiqueData();
		
		IScoInscription inscriptionAdministrative = inscriptionPedagogique.toInscription();

		ficheInscriptionPedagogiqueData.setIdentiteEtablissement(new IdentiteEtablissementDataTransformer().getIdentiteEtablissementData(editingContext, mainLogoUrl));
		ficheInscriptionPedagogiqueData.setIdentiteEtudiant(new IdentiteEtudiantDataTransformer().getIdentiteEtudiantData(inscriptionAdministrative.toEtudiantAnnee().toInfosEtudiant()));
		ficheInscriptionPedagogiqueData.setNumeroEtudiant(inscriptionAdministrative.toEtudiant().etudNumero());
		ficheInscriptionPedagogiqueData.setInscriptionPedagogique(new InscriptionPedagogiqueTransformer().getInscriptionPedagogiquePourFicheInscriptionPedagogique(editingContext, inscriptionPedagogique));
		
		return ficheInscriptionPedagogiqueData;
	}

}
