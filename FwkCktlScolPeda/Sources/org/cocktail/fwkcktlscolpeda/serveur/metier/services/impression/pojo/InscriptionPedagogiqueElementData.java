package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import java.math.BigDecimal;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

public class InscriptionPedagogiqueElementData implements ReportImpressionData {
	private String typeComposant;
	private String codeComposant;
	private String libelle;
	private String libelleCourt;
	private boolean obligatoire;
	private BigDecimal nombreCreditEctsCalcule;
	
	public String getTypeComposant() {
		return typeComposant;
	}

	public void setTypeComposant(String typeComposant) {
		this.typeComposant = typeComposant;
	}

	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public String getLibelleCourt() {
		return libelleCourt;
	}

	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}

	public boolean isObligatoire() {
		return obligatoire;
	}
	
	public void setObligatoire(boolean obligatoire) {
		this.obligatoire = obligatoire;
	}
	
	public BigDecimal getNombreCreditEctsCalcule() {
		return nombreCreditEctsCalcule;
	}
	
	public void setNombreCreditEctsCalcule(BigDecimal nombreCreditEctsCalcule) {
		this.nombreCreditEctsCalcule = nombreCreditEctsCalcule;
	}
	
	public String getCodeComposant() {
		return codeComposant;
	}
	
	public void setCodeComposant(String codeComposant) {
		this.codeComposant = codeComposant;
	}
	
}
