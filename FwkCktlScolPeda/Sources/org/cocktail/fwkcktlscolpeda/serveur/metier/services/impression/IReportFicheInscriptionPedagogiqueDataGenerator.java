package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Générateur de données pour les impression des fiche d'inscription pédagogique.
 * 
 * @author Pascal MACOUIN
 */
public interface IReportFicheInscriptionPedagogiqueDataGenerator {

	/**
	 * @param editingContext un editing context
	 * @param mainLogoUrl l'url du logo de l'établissement
	 * @param inscriptionPedagogique L'inscription pédagogique à imprimer
	 * @return Les données pour l'impression d'une fiche d'inscription pédagogique
	 */
	ReportImpressionData generateFicheInscriptionPedagogique(EOEditingContext editingContext, String mainLogoUrl, IInscriptionPedagogique inscriptionPedagogique);

}
