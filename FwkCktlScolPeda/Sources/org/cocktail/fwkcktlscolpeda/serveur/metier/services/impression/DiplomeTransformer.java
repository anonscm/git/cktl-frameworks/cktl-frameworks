package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.DiplomeData;

public class DiplomeTransformer {
	private String libelleLong;

	public String getLibelleLong() {
		return libelleLong;
	}

	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}

	public DiplomeData getDiplome(IDiplome diplome) {
		DiplomeData diplomeData = new DiplomeData();
		
		diplomeData.setCode(diplome.code());
		diplomeData.setLibelle(diplome.libelle());
		diplomeData.setLibelleCourt(diplome.libelleCourt());

		return diplomeData;
	}
}
