package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.InscriptionPedagogiqueElementData;

public class InscriptionPedagogiqueElementTransformer {

	private static final Integer INSCRIPTION_PEDAGOGIQUE_ELEMENT_OBLIGATOIRE = Integer.valueOf(1);

	public InscriptionPedagogiqueElementData getInscriptionPedagogiqueElement(ILien lienInscriptionPedagogiqueElement) {
		InscriptionPedagogiqueElementData inscriptionPedagogiqueElementData = new InscriptionPedagogiqueElementData();
		
		ILienComposer lienComposer = null;
		if (lienInscriptionPedagogiqueElement instanceof ILienComposer) {
			lienComposer = (ILienComposer) lienInscriptionPedagogiqueElement;
		}
		IComposant composant = lienInscriptionPedagogiqueElement.child();
		
		inscriptionPedagogiqueElementData.setTypeComposant(composant.typeComposant().nom());
		inscriptionPedagogiqueElementData.setLibelle(composant.libelle());
		inscriptionPedagogiqueElementData.setLibelleCourt(composant.libelleCourt());
		inscriptionPedagogiqueElementData.setCodeComposant(composant.code());
		if (lienComposer != null) {
			inscriptionPedagogiqueElementData.setObligatoire(INSCRIPTION_PEDAGOGIQUE_ELEMENT_OBLIGATOIRE.equals(lienComposer.obligatoire()));
		}
		
		inscriptionPedagogiqueElementData.setNombreCreditEctsCalcule(lienInscriptionPedagogiqueElement.getCreditECTSCalcule());
		
		return inscriptionPedagogiqueElementData;
	}

}
