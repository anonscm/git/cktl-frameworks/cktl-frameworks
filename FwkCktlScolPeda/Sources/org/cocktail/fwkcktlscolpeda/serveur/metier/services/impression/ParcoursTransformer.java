package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.ParcoursData;

public class ParcoursTransformer {

	public ParcoursData getParcours(IParcours parcours) {
		if (parcours == null) {
			return null;
		}
		
		ParcoursData parcoursData = new ParcoursData();
		
		parcoursData.setCode(parcours.code());
		parcoursData.setLibelle(parcours.libelle());
		parcoursData.setLibelleCourt(parcours.libelleCourt());
		
		return parcoursData;
	}

}
