package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

public class InscriptionPedagogiqueData implements ReportImpressionData {
	private DiplomeData diplome;
	private ParcoursData parcoursDiplome;
	private ParcoursData parcoursAnnee;
	private Integer niveau;
	private Integer anneeUniversitaire;
	private String codeEtape;
	private String libelleEtape;
	private BigDecimal nombreCreditEcts;
	private List<InscriptionPedagogiqueElementData> listeInscriptionPedagogiqueElement;
	
	public DiplomeData getDiplome() {
		return diplome;
	}
	
	public void setDiplome(DiplomeData diplome) {
		this.diplome = diplome;
	}
	
	public ParcoursData getParcoursDiplome() {
		return parcoursDiplome;
	}

	public void setParcoursDiplome(ParcoursData parcoursDiplome) {
		this.parcoursDiplome = parcoursDiplome;
	}

	public ParcoursData getParcoursAnnee() {
		return parcoursAnnee;
	}

	public void setParcoursAnnee(ParcoursData parcoursAnnee) {
		this.parcoursAnnee = parcoursAnnee;
	}

	public Integer getNiveau() {
		return niveau;
	}

	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}
	
	public Integer getAnneeUniversitaire() {
		return anneeUniversitaire;
	}
	
	public void setAnneeUniversitaire(Integer anneeUniversitaire) {
		this.anneeUniversitaire = anneeUniversitaire;
	}
	
	public String getCodeEtape() {
		return codeEtape;
	}

	public void setCodeEtape(String codeEtape) {
		this.codeEtape = codeEtape;
	}

	public String getLibelleEtape() {
		return libelleEtape;
	}
	
	public void setLibelleEtape(String libelleEtape) {
		this.libelleEtape = libelleEtape;
	}
	
	public BigDecimal getNombreCreditEcts() {
		return nombreCreditEcts;
	}
	
	public void setNombreCreditEcts(BigDecimal nombreCreditEcts) {
		this.nombreCreditEcts = nombreCreditEcts;
	}
	
	public List<InscriptionPedagogiqueElementData> getListeInscriptionPedagogiqueElement() {
		return listeInscriptionPedagogiqueElement;
	}
	
	public void setListeInscriptionPedagogiqueElement(List<InscriptionPedagogiqueElementData> listeInscriptionPedagogiqueElement) {
		this.listeInscriptionPedagogiqueElement = listeInscriptionPedagogiqueElement;
	}
	
}
