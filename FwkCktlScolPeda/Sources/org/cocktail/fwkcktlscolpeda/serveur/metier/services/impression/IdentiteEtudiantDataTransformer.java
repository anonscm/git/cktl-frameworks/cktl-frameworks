package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.IdentiteEtudiantData;

public class IdentiteEtudiantDataTransformer {
	/**
	 * @param etudiant L'étudiant dont on récupère les informations.
	 * @return informations de l'étudiant sous forme de Pojo necessaires pour la génération du rapport
	 */
	public IdentiteEtudiantData getIdentiteEtudiantData(IInfosEtudiant etudiant) {

		IdentiteEtudiantData identiteEtudiantData = new IdentiteEtudiantData();
		
		identiteEtudiantData.setCivilite(etudiant.toCivilite().getCode());
		identiteEtudiantData.setSexe(etudiant.toCivilite().getSexe());
		identiteEtudiantData.setNomUsuel(etudiant.nomUsuel());
		identiteEtudiantData.setPrenomPrincipal(etudiant.prenom());
		identiteEtudiantData.setNumeroInsee(etudiant.noInsee());
		identiteEtudiantData.setDateNaissance(etudiant.dNaissance());
		if (etudiant.toDepartement() != null) {
			identiteEtudiantData.setDeptNaissance(etudiant.toDepartement().llDepartement());
			identiteEtudiantData.setCodeDeptNaissance(etudiant.toDepartement().getCode());
		}
		identiteEtudiantData.setVilleNaissance(etudiant.villeNaissance());
		if (etudiant.toPaysNaissance() != null) {
			identiteEtudiantData.setPaysNaissance(etudiant.toPaysNaissance().llPays());
			identiteEtudiantData.setCodePaysNaissance(etudiant.toPaysNaissance().getCode());
		}
		if (etudiant.toPaysNationalite() != null) {
			identiteEtudiantData.setNationalite(etudiant.toPaysNationalite().lNationalite());
		}
		if (etudiant.toSituationFamiliale() != null) {
			identiteEtudiantData.setSituationFamiliale(etudiant.toSituationFamiliale().lSituationFamille());
		}

		return identiteEtudiantData;
	}

}
