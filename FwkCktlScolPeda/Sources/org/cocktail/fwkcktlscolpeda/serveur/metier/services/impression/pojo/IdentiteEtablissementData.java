package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

/**
 * Identité de l'établissement.
 * 
 * @author Pascal MACOUIN
 */
public class IdentiteEtablissementData implements ReportImpressionData {
	private String mainLogoUrl;
	
	private String libelleLongEtablissement;
	private String libelleCourtEtablissement;
	private String adresseCompleteEtablissement;
	private String numerosTelephone;
	private String numeroFax;
	private String villeEtablissement;
	
	private String nomDirecteur;
	private String prenomDirecteur;
	
	public String getMainLogoUrl() {
		return mainLogoUrl;
	}
	
	public void setMainLogoUrl(String mainLogoUrl) {
		this.mainLogoUrl = mainLogoUrl;
	}
	
	public String getLibelleLongEtablissement() {
		return libelleLongEtablissement;
	}
	
	public void setLibelleLongEtablissement(String libelleLongEtablissement) {
		this.libelleLongEtablissement = libelleLongEtablissement;
	}
	
	public String getLibelleCourtEtablissement() {
		return libelleCourtEtablissement;
	}
	
	public void setLibelleCourtEtablissement(String libelleCourtEtablissement) {
		this.libelleCourtEtablissement = libelleCourtEtablissement;
	}
	
	public String getAdresseCompleteEtablissement() {
		return adresseCompleteEtablissement;
	}
	
	public void setAdresseCompleteEtablissement(String adresseCompleteEtablissement) {
		this.adresseCompleteEtablissement = adresseCompleteEtablissement;
	}
	
	public String getNumerosTelephone() {
		return numerosTelephone;
	}
	
	public void setNumerosTelephone(String numerosTelephone) {
		this.numerosTelephone = numerosTelephone;
	}
	
	public String getNumeroFax() {
		return numeroFax;
	}
	
	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}
	
	public String getVilleEtablissement() {
		return villeEtablissement;
	}
	
	public void setVilleEtablissement(String villeEtablissement) {
		this.villeEtablissement = villeEtablissement;
	}
	
	public String getNomDirecteur() {
		return nomDirecteur;
	}
	
	public void setNomDirecteur(String nomDirecteur) {
		this.nomDirecteur = nomDirecteur;
	}
	
	public String getPrenomDirecteur() {
		return prenomDirecteur;
	}
	
	public void setPrenomDirecteur(String prenomDirecteur) {
		this.prenomDirecteur = prenomDirecteur;
	}
	
}
