package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

public class ReportFicheInscriptionPedagogiqueData implements ReportImpressionData {
	private IdentiteEtablissementData identiteEtablissement;
	private IdentiteEtudiantData identiteEtudiant;
	private Integer numeroEtudiant;
	private InscriptionPedagogiqueData inscriptionPedagogique;

	public IdentiteEtablissementData getIdentiteEtablissement() {
		return identiteEtablissement;
	}

	public void setIdentiteEtablissement(IdentiteEtablissementData identiteEtablissement) {
		this.identiteEtablissement = identiteEtablissement;
	}

	public IdentiteEtudiantData getIdentiteEtudiant() {
		return identiteEtudiant;
	}

	public void setIdentiteEtudiant(IdentiteEtudiantData identiteEtudiant) {
		this.identiteEtudiant = identiteEtudiant;
	}

	public Integer getNumeroEtudiant() {
		return numeroEtudiant;
	}

	public void setNumeroEtudiant(Integer numeroEtudiant) {
		this.numeroEtudiant = numeroEtudiant;
	}

	public InscriptionPedagogiqueData getInscriptionPedagogique() {
		return inscriptionPedagogique;
	}

	public void setInscriptionPedagogique(InscriptionPedagogiqueData inscriptionPedagogique) {
		this.inscriptionPedagogique = inscriptionPedagogique;
	}
}
