package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import java.util.Date;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

/**
 * Pojo contenant les données d'identite de l'étudiant extraites pour l'impression.
 */
public class IdentiteEtudiantData implements ReportImpressionData {
	private String civilite;
	private String sexe;
	private String nomUsuel;
	private String prenomPrincipal;
	private Date dateNaissance;
	private String numeroInsee;
	private String codeDeptNaissance;
	private String deptNaissance;
	private String villeNaissance;
	private String paysNaissance;
	private String codePaysNaissance;
	private String nationalite;
	private String autresPrenoms;
	private String situationFamiliale;

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getNomUsuel() {
		return nomUsuel;
	}

	public void setNomUsuel(String nomUsuel) {
		this.nomUsuel = nomUsuel;
	}

	public String getPrenomPrincipal() {
		return prenomPrincipal;
	}

	public void setPrenomPrincipal(String prenomPrincipal) {
		this.prenomPrincipal = prenomPrincipal;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getNumeroInsee() {
		return numeroInsee;
	}

	public void setNumeroInsee(String numeroInsee) {
		this.numeroInsee = numeroInsee;
	}

	public String getDeptNaissance() {
		return deptNaissance;
	}

	public void setDeptNaissance(String deptNaissance) {
		this.deptNaissance = deptNaissance;
	}

	public String getVilleNaissance() {
		return villeNaissance;
	}

	public void setVilleNaissance(String villeNaissance) {
		this.villeNaissance = villeNaissance;
	}

	public String getPaysNaissance() {
		return paysNaissance;
	}

	public void setPaysNaissance(String paysNaissance) {
		this.paysNaissance = paysNaissance;
	}

	public String getCodePaysNaissance() {
		return codePaysNaissance;
	}

	public void setCodePaysNaissance(String codePaysNaissance) {
		this.codePaysNaissance = codePaysNaissance;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public String getCodeDeptNaissance() {
		return codeDeptNaissance;
	}

	public void setCodeDeptNaissance(String codeDeptNaissance) {
		this.codeDeptNaissance = codeDeptNaissance;
	}

	public String getAutresPrenoms() {
		return autresPrenoms;
	}

	public void setAutresPrenoms(String autresPrenoms) {
		this.autresPrenoms = autresPrenoms;
	}

	public String getSituationFamiliale() {
		return situationFamiliale;
	}

	public void setSituationFamiliale(String situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}

}
