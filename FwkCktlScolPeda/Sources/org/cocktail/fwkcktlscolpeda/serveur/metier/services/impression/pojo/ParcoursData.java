package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;

public class ParcoursData implements ReportImpressionData {
	private String code;
	private String libelle;
	private String libelleCourt;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getLibelleCourt() {
		return libelleCourt;
	}

	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}
}
