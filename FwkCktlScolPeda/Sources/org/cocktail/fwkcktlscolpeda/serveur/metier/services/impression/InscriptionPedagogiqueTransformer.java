package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CalculateurSommeCreditECTSBase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteData;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteDataBuilder;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.RegleCalculCreditECTSPourIP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.InscriptionPedagogiqueData;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.InscriptionPedagogiqueElementData;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class InscriptionPedagogiqueTransformer {

	private CreditableService creditableService = new CreditableServiceImpl();
	private List<IComposant> composantExclus = new ArrayList<IComposant>();
	private List<ILien> lienExclus = new ArrayList<ILien>();
	
	public InscriptionPedagogiqueData getInscriptionPedagogiquePourFicheInscriptionPedagogique(EOEditingContext editingContext, IInscriptionPedagogique inscriptionPedagogique) {
		InscriptionPedagogiqueData inscriptionPedagogiqueData = new InscriptionPedagogiqueData();
		
		// Lecture de la maquette
		IInscription inscription = inscriptionPedagogique.toInscription();
		IDiplome diplome = inscription.toDiplome();
		IVersionDiplome versionDiplome = diplome.getVersionDiplome(inscription.annee());
		EOLien lienVersionDiplome = EOLien.fetchSco_Lien(editingContext, EOLien.PARENT.eq((EOComposant) diplome).and(EOLien.CHILD.eq((EOComposant) versionDiplome)));
		
		MaquetteDataBuilder maquetteDataBuilder = getMaquetteDataBuilder(editingContext, inscription, versionDiplome, lienVersionDiplome);
		MaquetteData maquetteData = 
			maquetteDataBuilder
				.loadCreditables()
				.indexLiensByParentId()
				.indexComposantsById()					
				.getData();

		List<ILien> liensIp = getLienElementIps((EOInscriptionPedagogique) inscriptionPedagogique, maquetteDataBuilder);
		initialiserCreditsECTS(lienVersionDiplome, maquetteData, liensIp);
		
		// Remplissage
		inscriptionPedagogiqueData.setAnneeUniversitaire(inscriptionPedagogique.toInscription().annee());
		inscriptionPedagogiqueData.setNiveau(inscriptionPedagogique.toInscription().niveau());
		inscriptionPedagogiqueData.setCodeEtape(getCodeEtape(inscriptionPedagogique.toDemande()));
		inscriptionPedagogiqueData.setLibelleEtape(getLibelleEtape(inscriptionPedagogique.toDemande()));
		inscriptionPedagogiqueData.setDiplome(new DiplomeTransformer().getDiplome(inscriptionPedagogique.toInscription().toDiplome()));
		IParcours parcours = inscriptionPedagogique.toInscription().toParcoursDiplome();
		if (parcours != null) {
			inscriptionPedagogiqueData.setParcoursDiplome(new ParcoursTransformer().getParcours(parcours));
			composantExclus.add(parcours);
		}
		parcours = inscriptionPedagogique.toInscription().toParcoursAnnee();
		if (parcours != null) {
			inscriptionPedagogiqueData.setParcoursAnnee(new ParcoursTransformer().getParcours(parcours));
			composantExclus.add(parcours);
		}
		
		List<InscriptionPedagogiqueElementData> listeInscriptionPedagogiqueElement = new ArrayList<InscriptionPedagogiqueElementData>();
		InscriptionPedagogiqueElementTransformer inscriptionPedagogiqueElementTransformer = new InscriptionPedagogiqueElementTransformer();
		
		// On complète le calcul
		for (ILien lienInscriptionPedagogiqueElement : liensIp) {
			if (lienInscriptionPedagogiqueElement.getCreditECTSCalcule() == null && lienInscriptionPedagogiqueElement instanceof ILienComposer) {
				lienInscriptionPedagogiqueElement.setCreditECTSCalcule(creditableService.creditsEcts((ILienComposer) lienInscriptionPedagogiqueElement));
			}
		}
		
		// On ajoute dans le flux et on fait le total
		BigDecimal totalCreditECTSCalcule = BigDecimal.ZERO;
		for (ILien lienInscriptionPedagogiqueElement : liensIp) {
			if (isDansFluxFicheInscriptionPedagogique(lienInscriptionPedagogiqueElement, maquetteData)) {
				listeInscriptionPedagogiqueElement.add(inscriptionPedagogiqueElementTransformer.getInscriptionPedagogiqueElement(lienInscriptionPedagogiqueElement));
				totalCreditECTSCalcule = additionnerUeEtEc(totalCreditECTSCalcule, lienInscriptionPedagogiqueElement);
			}
		}
		inscriptionPedagogiqueData.setListeInscriptionPedagogiqueElement(listeInscriptionPedagogiqueElement);
		
		// Normalement "lienVersionDiplome.getCreditECTSCalcule()" devrait retourner le bon résultat
		inscriptionPedagogiqueData.setNombreCreditEcts(totalCreditECTSCalcule);
		
		return inscriptionPedagogiqueData;
	}

	private BigDecimal additionnerUeEtEc(BigDecimal totalCreditECTSCalcule, ILien lienInscriptionPedagogiqueElement) {
		BigDecimal creditECTSCalcule = lienInscriptionPedagogiqueElement.getCreditECTSCalcule();
		String typeComposant = lienInscriptionPedagogiqueElement.child().typeComposant().nom();
		
		if (creditECTSCalcule != null
				&& (ITypeComposant.TYPEUE_NOM.equals(typeComposant) || ITypeComposant.TYPEEC_NOM.equals(typeComposant))) {
			return totalCreditECTSCalcule.add(creditECTSCalcule);
		}
		
		return totalCreditECTSCalcule;
	}

	/**
	 * Est-ce que cet IP est dans le flux pour l'impression de la fiche d'inscription pédagogique.
	 * @param lienInscriptionPedagogiqueElement l'IP
	 * @return <code>true</code> si cet IP est dans le flux
	 */
	private boolean isDansFluxFicheInscriptionPedagogique(ILien lienInscriptionPedagogiqueElement, final MaquetteData maquetteData) {
		IComposant composant = lienInscriptionPedagogiqueElement.child();
		
		if (maquetteData.childDeType(lienInscriptionPedagogiqueElement, maquetteData.getTypeUE())) {
			List<ILien> enfants = maquetteData.getEnfants(lienInscriptionPedagogiqueElement);
			
			Collection<ILien> ecs = Collections2.filter(enfants, new Predicate<ILien>() {
				public boolean apply(ILien lien) {
					return maquetteData.childDeType(lien, maquetteData.getTypeEC());
				}
			});
			
			boolean hasEnfantDeTypeEc = ecs.size() > 0;
			
			Collection<ILien> ecsAvecCredits = Collections2.filter(ecs, new Predicate<ILien>() {
				public boolean apply(ILien lien) {
					return lien.getCreditECTSCalcule() != null;
				}
			});
			
			boolean hasEnfantSansEcts = ecsAvecCredits.size() == 0;

			
			boolean estDansFlux = hasEnfantSansEcts || !hasEnfantDeTypeEc;
			
			if (estDansFlux) {
				// On exclut les enfants du flux
				lienExclus.addAll(maquetteData.getEnfants(lienInscriptionPedagogiqueElement));
			}
			
			return estDansFlux;
		} else if (maquetteData.childDeType(lienInscriptionPedagogiqueElement, maquetteData.getTypeEC())) {
			return !lienExclus.contains(lienInscriptionPedagogiqueElement);
		} else if (maquetteData.childDeType(lienInscriptionPedagogiqueElement, maquetteData.getTypePeriode())) {
			// Semestres seulements
			return Boolean.TRUE.equals(((IPeriode) composant).isSemestre());
		} else if (maquetteData.childDeType(lienInscriptionPedagogiqueElement, maquetteData.getTypeParcours())) {
			// Sauf les parcours année et diplôme
			return !composantExclus.contains(composant);
		}
		
		return false;
	}
	
	/**
	 * Est présent dans le flux si un de ces enfants n'a pas de crédit ECTS.
	 */
	private boolean isDansFluxFicheInscriptionPedagogique(IUE ue, MaquetteData maquetteData) {
		boolean hasEnfantDeTypeEc = false;
		boolean hasEnfantSansEcts = false;
		for (ILien lien : ue.liensParents()) {
			String typeComposant = lien.child().typeComposant().nom();
			if (ITypeComposant.TYPEEC_NOM.equals(typeComposant)) {
				hasEnfantDeTypeEc = true;
				
				if (lien.getCreditECTSCalcule() == null) {
					hasEnfantSansEcts = true;
					break;
				}
			}
		}
		
		return hasEnfantSansEcts || !hasEnfantDeTypeEc;
	}
	
	private MaquetteDataBuilder getMaquetteDataBuilder(EOEditingContext editingContext, IInscription inscription, IVersionDiplome versionDiplome, EOLien lienVersionDiplome) {
		MaquetteDataBuilder maquetteDataBuilder = 
			MaquetteDataBuilder.get()
				.editingContext(editingContext)
				.composantRacineId(versionDiplome.id())
				.loadLiens()
				.addLien(lienVersionDiplome);

		maquetteDataBuilder.loadComposants();

		return maquetteDataBuilder;
	}

	private List<ILien> getLienElementIps(EOInscriptionPedagogique ip, MaquetteDataBuilder maquetteDataBuilder) {
		List<ILien> liens = maquetteDataBuilder.getLiens();
		List<ILien> lienElementIps = new ArrayList<ILien>();
		NSArray<EOInscriptionPedagogiqueElement> ipElements = ip.toInscriptionPedagogiqueElements();

		NSArray<Integer> lienIpIds = (NSArray<Integer>) ipElements.valueForKey(EOInscriptionPedagogiqueElement.ID_LIEN_KEY);
		for (ILien l : liens) {
			if (lienIpIds.contains(l.id())) {
				lienElementIps.add((EOLien) l);
			}
		}
		return lienElementIps;
	}

	private void initialiserCreditsECTS(EOLien lienVersionDiplome, MaquetteData maquetteData, List<ILien> liensIp) {
		RegleCalculCreditECTSPourIP regle = new RegleCalculCreditECTSPourIP();
		regle.setLiensInscriptionPedagogique(new ArrayList<ILien>(liensIp));
		regle.setMaquetteData(maquetteData);

		CalculateurSommeCreditECTSBase calculateurEcts = new CalculateurSommeCreditECTSBase(regle, maquetteData);
		calculateurEcts.calcule(lienVersionDiplome);
	}

	private String getCodeEtape(IDemande demande) {
		String code = null;
		
		if (demande != null) {
			code = demande.toEtape().codeEtape();
		}
		
		return code;
	}

	private String getLibelleEtape(IDemande demande) {
		String libelle = null;
		
		if (demande != null) {
			libelle = demande.toEtape().libelleEtape();
		}
		
		return libelle;
	}
}
