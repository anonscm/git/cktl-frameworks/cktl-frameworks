package org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.services.impression.pojo.IdentiteEtablissementData;

import com.google.common.base.Joiner;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class IdentiteEtablissementDataTransformer {
	public IdentiteEtablissementData getIdentiteEtablissementData(EOEditingContext editingContext, String mainLogoUrl) {

		IdentiteEtablissementData identiteEtablissementData = new IdentiteEtablissementData();
		
		EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext);
		
		identiteEtablissementData.setLibelleLongEtablissement(etablissement.llStructure());
		identiteEtablissementData.setLibelleCourtEtablissement(etablissement.lcStructure());
		if (etablissement.getAdressePrincipale() != null) {
			identiteEtablissementData.setVilleEtablissement(etablissement.getAdressePrincipale().ville());
			identiteEtablissementData.setAdresseCompleteEtablissement(etablissement.getAdressePrincipale().toDisplayString());
		}		
		identiteEtablissementData.setNumerosTelephone(getNumerosTelephoneEtablissement(etablissement));
		identiteEtablissementData.setNumeroFax(getNumeroFax(etablissement));
		if (etablissement.toResponsable() != null) {
			identiteEtablissementData.setNomDirecteur(etablissement.toResponsable().nomAffichage());
			identiteEtablissementData.setPrenomDirecteur(etablissement.toResponsable().prenomAffichage());
		}		
		identiteEtablissementData.setMainLogoUrl(mainLogoUrl);
		
		return identiteEtablissementData;
	}

	private String getNumerosTelephoneEtablissement(EOStructure etablissement) {
		List<String> numerosTelephone = new ArrayList<String>();
		NSArray<EOPersonneTelephone> tels = 
			etablissement.toPersonneTelephones(
				ERXQ.and(
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_NON_FAX,
					EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_PRF_OK
				),
				ERXS.chain(EOPersonneTelephone.SORT_TEL_PRINCIPAL_DESC),
				true
			);
		for (EOPersonneTelephone tel : tels) {
			numerosTelephone.add(tel.getTelephoneFormateAvecIndicatif());
		}
		return Joiner.on(", ").join(numerosTelephone);
	}

	private String getNumeroFax(EOStructure etablissement) {
		NSArray<EOPersonneTelephone> numerosFax = etablissement.toPersonneTelephones(EOPersonneTelephone.QUAL_PERSONNE_TELEPHONE_FAX);
		EOPersonneTelephone numeroFax = ERXArrayUtilities.firstObject(numerosFax);
		if (numeroFax != null) {
			return numeroFax.getTelephoneFormateAvecIndicatif();
		} else {
			return "";
		}
	}
}
