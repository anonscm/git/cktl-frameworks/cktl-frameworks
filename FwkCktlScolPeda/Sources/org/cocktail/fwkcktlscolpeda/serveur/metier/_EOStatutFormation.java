// DO NOT EDIT.  Make changes to EOStatutFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOStatutFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_StatutFormation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOStatutFormation.class);

  public EOStatutFormation localInstanceIn(EOEditingContext editingContext) {
    EOStatutFormation localInstance = (EOStatutFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOStatutFormation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOStatutFormation.LOG.isDebugEnabled()) {
    	_EOStatutFormation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutFormation.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOStatutFormation.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOStatutFormation.LOG.isDebugEnabled()) {
    	_EOStatutFormation.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutFormation.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOStatutFormation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOStatutFormation.LOG.isDebugEnabled()) {
    	_EOStatutFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStatutFormation.LIBELLE_KEY);
  }


  public static EOStatutFormation createSco_StatutFormation(EOEditingContext editingContext, String code
, Integer id
, String libelle
) {
    EOStatutFormation eo = (EOStatutFormation) EOUtilities.createAndInsertInstance(editingContext, _EOStatutFormation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOStatutFormation> fetchSpec() {
    return new ERXFetchSpecification<EOStatutFormation>(_EOStatutFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOStatutFormation> fetchAllSco_StatutFormations(EOEditingContext editingContext) {
    return _EOStatutFormation.fetchAllSco_StatutFormations(editingContext, null);
  }

  public static NSArray<EOStatutFormation> fetchAllSco_StatutFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStatutFormation.fetchSco_StatutFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStatutFormation> fetchSco_StatutFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOStatutFormation> fetchSpec = new ERXFetchSpecification<EOStatutFormation>(_EOStatutFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStatutFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOStatutFormation fetchSco_StatutFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutFormation.fetchSco_StatutFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutFormation fetchSco_StatutFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStatutFormation> eoObjects = _EOStatutFormation.fetchSco_StatutFormations(editingContext, qualifier, null);
    EOStatutFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_StatutFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutFormation fetchRequiredSco_StatutFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStatutFormation.fetchRequiredSco_StatutFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStatutFormation fetchRequiredSco_StatutFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStatutFormation eoObject = _EOStatutFormation.fetchSco_StatutFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_StatutFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStatutFormation localInstanceIn(EOEditingContext editingContext, EOStatutFormation eo) {
    EOStatutFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
