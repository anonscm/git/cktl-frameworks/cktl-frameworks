package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;
import er.extensions.validation.ERXValidationFactory;


/**
 * Cette classe represente un type d'AP
 */
@SuppressWarnings("serial")
public class EOTypeAP extends _EOTypeAP implements ITypeAP{
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(EOTypeAP.class);
    private static ERXEnterpriseObjectCache<EOTypeAP> typeAPCache;
    
    public static final String TYPECOURS_CODE = "CM";
    public static final String TYPETD_CODE = "TD";
    public static final String TYPETP_CODE = "TP";
    public static final String TYPEMEMOIRE_CODE = "MM";
    public static final String TYPESTAGE_CODE = "ST";
    public static final String TYPEIP_CODE = "IP";

    /**
     * @return le cache des {@link EOTypeAP} par leur code
     */
    public static ERXEnterpriseObjectCache<EOTypeAP> getTypeAPCache() {
    	if (typeAPCache == null) {
    		typeAPCache = new ERXEnterpriseObjectCache<EOTypeAP>(EOTypeAP.class, CODE_KEY);
    	}
    	return typeAPCache;
    }
    
    /**
     * @param typeAPCache le cache a setter
     */
    public static void setTypeAPCache(ERXEnterpriseObjectCache<EOTypeAP> typeAPCache) {
    	EOTypeAP.typeAPCache = typeAPCache;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "CM"
	 */
	public static EOTypeAP typeCours(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPECOURS_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "TD"
	 */
	public static EOTypeAP typeTD(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPETD_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "TP"
	 */
	public static EOTypeAP typeTP(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPETP_CODE);
	}
    
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "MM"
	 */
	public static EOTypeAP typeMemoire(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPEMEMOIRE_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "ST"
	 */
	public static EOTypeAP typeStage(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPESTAGE_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAP} "IP"
	 */
	public static EOTypeAP typeIP(EOEditingContext edc) {
		return getTypeAPCache().objectForKey(edc, TYPEIP_CODE);
	}
    

    /**
     * Valide que le code est correct
     * 
     * @param code code du type A à valider
     * @throws ValidationException exception lancee si la validation a echoue
     * @return le code valide si il est correct
     */
    public Object validateCode(String code) throws ValidationException {
		if (!EOTypeAP.fetchSco_TypeAPs(editingContext(), EOTypeAP.CODE.eq(code).and(EOTypeAP.ID.ne(this.id())), null).isEmpty()) {	    
		    throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_TypeAP.codeExistant");
		}
		
		return code;
    }
    
}
