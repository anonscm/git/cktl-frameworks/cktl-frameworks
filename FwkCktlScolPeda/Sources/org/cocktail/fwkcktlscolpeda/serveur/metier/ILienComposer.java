package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * Interface representant un lien de type composer
 */
public interface ILienComposer extends ILien {

	/**
	 * @return le nombre de creditECTS que l'étudiant doit choisir
	 */
	Integer choixECTS();

	/**
	 * @return le creditable du lien
	 */
	ICreditable creditable();

	/**
	 * un lien est obligatoire si le composant child doit obligatoirement être sélectionné par l'étudiant
	 * @return 1 si le lien est obligatoire
	 */
	Boolean obligatoire();

	/**
	 * @param value lien obligatoire
	 */
	void setObligatoire(Boolean value);
	
	/**
	 * @return coefficient (utilisé dans le calcul des formules) 
	 */
	BigDecimal coefficient();

	/**
	 * @param coeff set le coefficient
	 */
	void setCoefficient(BigDecimal coeff);
	
    /**
     * Null Object pour envoyer un résultat non null.
     */
	ILienComposer NULL = new ILienComposer() {
        
        public Integer sommeMinutesEnseigneesAPTP() {
            return null;
        }
        
        public Integer sommeMinutesEnseigneesAPTD() {
            return null;
        }
        
        public Integer sommeMinutesEnseigneesAPCours() {
            return null;
        }
        
        public BigDecimal sommeCreditECTSEnfants() {
            return null;
        }
        
        public void setSommeMinutesEnseigneesCalculeePourTypeAP(Integer sommeHeuresEnseignesCalculees, EOTypeAP typeAP) {
            
        }
        
        public void setSommeMinutesEnseigneesCalculee(Integer sommeMinutesEnseigneesCalculee) {
            
        }
        
        public void setCreditECTSCalcule(BigDecimal creditECTSCalcule) {
            
        }
        
        public IComposant parent() {
            return null;
        }
        
        public Boolean isParentSemestre() {
            return null;
        }
        
        public Integer id() {
            return null;
        }
        
        public Integer getSommeMinutesEnseigneesCalculeePourTypeAP(EOTypeAP typeAP) {
            return null;
        }
        
        public Integer getSommeMinutesEnseigneesCalculee() {
            return null;
        }
        
        public BigDecimal getCreditECTSCalcule() {
            return null;
        }
        
        public IComposant child() {
            return null;
        }
        
        public void setObligatoire(Boolean value) {
            
        }
        
        public Boolean obligatoire() {
            return null;
        }
        
        public ICreditable creditable() {
            return null;
        }
        
        public Integer choixECTS() {
            return null;
        }

		public Boolean isNbCreditsEctsOk() {
			return null;
		}

		public Integer childId() {
			return null;
		}

		public Integer parentId() {
			return null;
		}

		public Integer ordre() {
			return null;
		}

		public void setOrdre(Integer value) {

		}

		public BigDecimal coefficient() {
			return null;
		}

		public void setSommeMinutesEnseigneesAPCours(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTD(Integer valeurMinutes) {
			
		}

		public void setSommeMinutesEnseigneesAPTP(Integer valeurMinutes) {
			
		}

		public void setCoefficient(BigDecimal coeff) {
			
		}

		public void assignTo(ILien nouveauLien) {
	    // TODO Auto-generated method stub
	    
    }
    };

}