// DO NOT EDIT.  Make changes to EOSessionPeriode.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSessionPeriode extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SessionPeriode";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_FERMETURE = new ERXKey<NSTimestamp>("dateFermeture");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<NSTimestamp> DATE_OUVERTURE = new ERXKey<NSTimestamp>("dateOuverture");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NUMERO_SESSION = new ERXKey<Integer>("numeroSession");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_FERMETURE_KEY = DATE_FERMETURE.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DATE_OUVERTURE_KEY = DATE_OUVERTURE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NUMERO_SESSION_KEY = NUMERO_SESSION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSessionPeriode.class);

  public EOSessionPeriode localInstanceIn(EOEditingContext editingContext) {
    EOSessionPeriode localInstance = (EOSessionPeriode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOSessionPeriode.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.ANNEE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOSessionPeriode.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.DATE_CREATION_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(_EOSessionPeriode.DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOSessionPeriode.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.DATE_MODIFICATION_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(_EOSessionPeriode.DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.DATE_OUVERTURE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOSessionPeriode.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSessionPeriode.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.LIBELLE_KEY);
  }

  public Integer numeroSession() {
    return (Integer) storedValueForKey(_EOSessionPeriode.NUMERO_SESSION_KEY);
  }

  public void setNumeroSession(Integer value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating numeroSession from " + numeroSession() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.NUMERO_SESSION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOSessionPeriode.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOSessionPeriode.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOSessionPeriode.LOG.isDebugEnabled()) {
    	_EOSessionPeriode.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSessionPeriode.PERS_ID_MODIFICATION_KEY);
  }


  public static EOSessionPeriode createSco_SessionPeriode(EOEditingContext editingContext, Integer annee
, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer id
, String libelle
, Integer numeroSession
, Integer persIdCreation
, Integer persIdModification
) {
    EOSessionPeriode eo = (EOSessionPeriode) EOUtilities.createAndInsertInstance(editingContext, _EOSessionPeriode.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setNumeroSession(numeroSession);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    return eo;
  }

  public static ERXFetchSpecification<EOSessionPeriode> fetchSpec() {
    return new ERXFetchSpecification<EOSessionPeriode>(_EOSessionPeriode.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSessionPeriode> fetchAllSco_SessionPeriodes(EOEditingContext editingContext) {
    return _EOSessionPeriode.fetchAllSco_SessionPeriodes(editingContext, null);
  }

  public static NSArray<EOSessionPeriode> fetchAllSco_SessionPeriodes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSessionPeriode.fetchSco_SessionPeriodes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSessionPeriode> fetchSco_SessionPeriodes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSessionPeriode> fetchSpec = new ERXFetchSpecification<EOSessionPeriode>(_EOSessionPeriode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSessionPeriode> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSessionPeriode fetchSco_SessionPeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSessionPeriode.fetchSco_SessionPeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSessionPeriode fetchSco_SessionPeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSessionPeriode> eoObjects = _EOSessionPeriode.fetchSco_SessionPeriodes(editingContext, qualifier, null);
    EOSessionPeriode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SessionPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSessionPeriode fetchRequiredSco_SessionPeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSessionPeriode.fetchRequiredSco_SessionPeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSessionPeriode fetchRequiredSco_SessionPeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSessionPeriode eoObject = _EOSessionPeriode.fetchSco_SessionPeriode(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SessionPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSessionPeriode localInstanceIn(EOEditingContext editingContext, EOSessionPeriode eo) {
    EOSessionPeriode localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
