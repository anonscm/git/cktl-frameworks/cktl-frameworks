package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Cette classe représente un moyen de paiement pour un paiement.
 */
public class EOPaiementMoyen extends _EOPaiementMoyen implements IScoPaiementMoyen {
	private static final long serialVersionUID = 3104062615510906443L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPaiementMoyen.class);
	
	@Inject
	@Nullable
	private UserInfo userInfo;
	
    @Override
    public void awakeFromInsertion(EOEditingContext editingContext) {
    	super.awakeFromInsertion(editingContext);
    	setEtudiantTitulaire(Boolean.TRUE);
    }
    
    /**
     * {@inheritDoc}
     */
	public IScoPaiement toScoPaiement() {
		return super.toPaiement();
	}
	
    /**
     * {@inheritDoc}
     */
    public void setToPaiementRelationship(IPaiement paiement) {
    	super.setToPaiementRelationship((EOPaiement) paiement);
    }
    
    /** 
     * {@inheritDoc}
     */
    public void setToModePaiementRelationship(IModePaiement modePaiement) {
    	super.setToModePaiementRelationship((EOModePaiement) modePaiement);
    }

    /** 
     * {@inheritDoc}
     */
    public void setToTypePaiementRelationship(ITypePaiement typePaiement) {
    	super.setToTypePaiementRelationship((EOTypePaiement) typePaiement);
    }

    /**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementEcheance> toPaiementEcheancesTriees() {
		return super.toPaiementEcheances(null, EOPaiementEcheance.DATE_ECHEANCE.ascs(), false);
	}
	
    /** 
     * {@inheritDoc}
     */
    public boolean isDiffere() {
        return toTypePaiement().isDiffere();
    }

    /** 
     * {@inheritDoc}
     */
    public boolean isComptant() {
        return toTypePaiement().isComptant();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isCheque() {
    	return toModePaiement().isCheque();
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isVirement() {
    	return toModePaiement().isVirement();
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isPrelevement() {
    	return toModePaiement().isPrelevement();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isPaiement() {
    	return montantPaye().signum() >= 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isRemboursement() {
    	return montantPaye().signum() < 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean verifierInfosMinimalesRib() {
    	return iban() != null && bic() != null && nomTireur() != null && prenomTireur() != null;
    }
    
    /** 
     * {@inheritDoc}
     */
    public void setToEcheancierRelationship(ISepaSddEcheancier echeancier) {
        setToEcheancierSepaSddRelationship((EOSepaSddEcheancier) echeancier);
    }

    /**
     * {@inheritDoc}
     */
    public void setToAdresseTitulaireRelationship(IAdresse adresse) {
        setToAdresseTitulaireRelationship((EOAdresse) adresse);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToFournisseurTitulaireRelationship(IFournis fournis) {
		super.setToFournisseurTitulaireRelationship((EOFournis) fournis);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToRibTitulaireRelationship(IRib rib) {
		super.setToRibTitulaireRelationship((EORib) rib);
	}

	/**
	 * {@inheritDoc}
	 */
    public void copierInformationsTitulaireCompteDepuis(IPaiementMoyen moyenPaiementSource) {
    	if (moyenPaiementSource instanceof IScoPaiementMoyen) {
    		setNomTireur(moyenPaiementSource.nomTireur());
    		setPrenomTireur(moyenPaiementSource.prenomTireur());
    		setIban(moyenPaiementSource.iban());
    		setBic(moyenPaiementSource.bic());
    		setEtudiantTitulaire(moyenPaiementSource.etudiantTitulaire());
    		setToFournisseurTitulaireRelationship(((IScoPaiementMoyen) moyenPaiementSource).toFournisseurTitulaire());
    		setToAdresseTitulaireRelationship(moyenPaiementSource.toAdresseTitulaire());
    		setToRibTitulaireRelationship(((IScoPaiementMoyen) moyenPaiementSource).toRibTitulaire());
    	} else {
    		throw new UnsupportedOperationException("On ne peut copier les informations du titulaire du compte que depuis/vers un EOPaiementMoyen");
    	}
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
