package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;


/**
 * Paiment pour un étudiant donné
 */
public interface IScoPaiement extends IPaiement {

	/**
	 * @return infos de l'année pour l'étudiant qui est à l'origine du paiement
	 */
	IScoEtudiantAnnee toScoEtudiantAnnee();
	
	/**
	 * @return la liste des détails du paiement
	 */
	List<? extends IScoPaiementDetail> toScoPaiementDetails();
	
	/**
	 * @return la liste des moyens de paiement du paiement
	 */
	List<? extends IScoPaiementMoyen> toScoPaiementMoyens();
	
	/**
	 * @return Le moyen de paiement prélèvement
	 */
	IScoPaiementMoyen toScoPaiementMoyenPrelevement();

	/**
	 * Affecte un bordereau à ce paiement.
	 * @param unBordereau un bordereau
	 */
    void setToBordereauRelationship(IBordereau unBordereau);

}
