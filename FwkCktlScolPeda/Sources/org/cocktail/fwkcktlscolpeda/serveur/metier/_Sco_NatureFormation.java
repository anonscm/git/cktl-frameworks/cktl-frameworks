// DO NOT EDIT.  Make changes to Sco_NatureFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _Sco_NatureFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_NatureFormation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Sco_NatureFormation.class);

  public Sco_NatureFormation localInstanceIn(EOEditingContext editingContext) {
    Sco_NatureFormation localInstance = (Sco_NatureFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_Sco_NatureFormation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_Sco_NatureFormation.LOG.isDebugEnabled()) {
    	_Sco_NatureFormation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_NatureFormation.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_Sco_NatureFormation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_Sco_NatureFormation.LOG.isDebugEnabled()) {
    	_Sco_NatureFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_NatureFormation.LIBELLE_KEY);
  }


  public static Sco_NatureFormation createSco_NatureFormation(EOEditingContext editingContext, String code
, String libelle
) {
    Sco_NatureFormation eo = (Sco_NatureFormation) EOUtilities.createAndInsertInstance(editingContext, _Sco_NatureFormation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<Sco_NatureFormation> fetchSpec() {
    return new ERXFetchSpecification<Sco_NatureFormation>(_Sco_NatureFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Sco_NatureFormation> fetchAllSco_NatureFormations(EOEditingContext editingContext) {
    return _Sco_NatureFormation.fetchAllSco_NatureFormations(editingContext, null);
  }

  public static NSArray<Sco_NatureFormation> fetchAllSco_NatureFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Sco_NatureFormation.fetchSco_NatureFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<Sco_NatureFormation> fetchSco_NatureFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Sco_NatureFormation> fetchSpec = new ERXFetchSpecification<Sco_NatureFormation>(_Sco_NatureFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Sco_NatureFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Sco_NatureFormation fetchSco_NatureFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_NatureFormation.fetchSco_NatureFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_NatureFormation fetchSco_NatureFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Sco_NatureFormation> eoObjects = _Sco_NatureFormation.fetchSco_NatureFormations(editingContext, qualifier, null);
    Sco_NatureFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_NatureFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_NatureFormation fetchRequiredSco_NatureFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_NatureFormation.fetchRequiredSco_NatureFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_NatureFormation fetchRequiredSco_NatureFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    Sco_NatureFormation eoObject = _Sco_NatureFormation.fetchSco_NatureFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_NatureFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_NatureFormation localInstanceIn(EOEditingContext editingContext, Sco_NatureFormation eo) {
    Sco_NatureFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
