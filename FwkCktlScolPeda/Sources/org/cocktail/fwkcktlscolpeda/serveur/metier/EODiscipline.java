package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Enumeration;

import org.apache.log4j.Logger;


public class EODiscipline extends _EODiscipline implements IDiscipline {
    private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODiscipline.class);
	
	
	/**
	 * Suppression d'une discipline
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		// Supprimer toutes les relations avec les disciplines childs
		Enumeration<EODiscipline> objects = toChildren().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EODiscipline discipline = objects.nextElement();
			discipline.supprimer();
		}

		// Supprimer l'objet
		delete();
	}
	
	@Override
	public String toString() {
		return this.libelleLong();
	}
	
}
