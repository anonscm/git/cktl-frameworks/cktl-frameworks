package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 * mention attachée à un diplome
 */
public interface IMention {
	/**
	 * @return code de la mention
	 */
	String code();
	
	/**
	 * @return libellé de la mention
	 */
	String libelle();
}
