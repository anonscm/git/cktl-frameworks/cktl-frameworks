package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;

import com.webobjects.foundation.NSArray;


/**
 * La classe EOPeriode décrit et maintient une période.
 */
public class EOPeriode extends _EOPeriode implements IPeriode {
	private static final long serialVersionUID = 1L;

	// Private fields

	// Properties
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typePeriode(editingContext());
	}
	
	@Override
	public void initialise() {
		super.initialise();
		setAlternance(false);
	}

	// Public methods
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean avantSuppression() throws Exception {
		try {
			supprimerRegimesInscription();		
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	protected void supprimerRegimesInscription() throws Exception {
		if (regimesInscription() == null) {
			return;
		}

		Enumeration<EORegimeInscription> objects = regimesInscription().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			EORegimeInscription regime = objects.nextElement();
			removeFromRegimesInscriptionRelationship(regime);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public EOPeriode copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			// TODO : récupérer le persId par injection du user
			EOPeriode periode = EOComposantFactory.createComposant(EOPeriode.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(periode);
			return periode;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return IPeriode : le composant suivant associe au composant si il existe
	 */
	public IPeriode getComposantAvecOrigine() {
		if (super.getComposantSuivant() != null) {
			return ((IPeriode) (super.getComposantSuivant()));
		}

		return null;
	}

	/**
	 * @return IPeriode : le composant d'origine
	 */
	public IPeriode getComposantOrigine() {
		return (IPeriode) composantOrigine();
	}

	// Private methods
	private void assignTo(EOPeriode periode) {
		super.assignTo(periode);
		periode.setOrdre(ordre());
		periode.setToTypePeriodeRelationship(toTypePeriode());
	}

	public Boolean isAnnee() {
		return (toTypePeriode() != null && StringUtils.equals(toTypePeriode().code(), EOTypePeriode.CODE_ANNEE));
	}

	public Boolean isSemestre() {
		return (toTypePeriode() != null && StringUtils.equals(toTypePeriode().code(), EOTypePeriode.CODE_SEMESTRE));
	}


	public Boolean isAnneeDeNiveau(Integer niveau) {
		return this.isAnnee() && ordre() != null && ordre().equals(niveau);
	}

	public BigDecimal getSeuilNbCreditsECTS() {
		BigDecimal seuil = BigDecimal.ZERO;
		
		BigDecimal seuilSemestre = BigDecimal.ZERO;
		Integer nbCreditsECTSSemestre = FwkCktlScolPeda.getParamManager().getGirofleNbMinEctsSemestre();
		if (nbCreditsECTSSemestre != null) {
			seuilSemestre = new BigDecimal(nbCreditsECTSSemestre);
		}
		
		if(this.isAnnee()) {
			BigDecimal nbSemestres = new BigDecimal(this.liensParentsWithoutDuplicate().size());
			// TODO, a corriger car si on a 4 parcours pb voir geode 7792 
			seuil = seuilSemestre.multiply(new BigDecimal(2));
		}
		else if(this.isPeriode()) {
			seuil = seuilSemestre;
		}
		
		return seuil;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IRegimeInscription> getRegimesInscription(){
			return new NSArray<IRegimeInscription>(super.regimesInscription());
	}
	
}
