package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * {@inheritDoc}
 */
public interface IDomaine {

	/**
	 * @return code du domaine
	 */
	String getCode();
	
	/**
	 * @return libelle du domaine
	 */
	String getLibelle();
	
}
