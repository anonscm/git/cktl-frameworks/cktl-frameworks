package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

import com.webobjects.foundation.NSTimestamp;


/**
 * Super interface pour une inscription qu'elle  soit SCO_SCOLARITE ou SCO_PRE_SCOLARITE.

 * @author isabelle
 *
 */
public interface IInscription {
	
	/**
	 * @return l'identifiant
	 */
	Integer id();
	
	/**
	 * @return le diplome de l'inscription
	 */
	IDiplome toDiplome();
	
	/**
	 * @return le grade universitaire
	 */
	IGradeUniversitaire toGradeUniversitaire();
	
	/**
	 * @return le niveau dans le diplome
	 */
	Integer niveau();
	
	/**
	 * @param value le niveau dans le diplome
	 */
	void setNiveau(Integer value);
	/**
	 * @return le type d'inscription
	 */
	ITypeInscriptionFormation toTypeInscriptionFormation();
	
	/**
	 * @return le parcours eventuellment choisi 
	 * en dessous de l'année
	 */
	IParcours toParcoursAnnee();
	
	/**
	 * @return le parcours eventuellement choisi
	 * dans le diplome
	 */
	IParcours toParcoursDiplome();
	
	/**
	 * @return l'année de l'inscription
	 */
	Integer annee();
	
	/**
	 * 
	 * @return le régime d'inscription
	 */
	IRegimeInscription toRegimeInscription();
	
	/**
	 * 
	 * @param regimeInscription le regime d'inscription
	 */
	void setToRegimeInscriptionRelationship(IRegimeInscription regimeInscription);

	/**
	 * 
	 * @return l'établissement cumulatif
	 */
	EOEtablissementCumulatif toEtablissementCumulatif();

	/**
	 * 
	 * @return vrai ou faux
	 */
	Boolean cycleAmenage();
	
	/**
	 * 
	 * @return vrai ou faux
	 */
	Boolean passageConditionnel();

	/**
	 * 
	 * @return vrai ou faux
	 */
	Boolean redoublement();

	/**
	 * 
	 * @return vrai ou faux
	 */
	Boolean reorientation();

	/**
	 * 
	 * @return le type d'échange internationnal
	 */
	ITypeEchange toTypeEchange();

	/**
	 * 
	 * @return la date de création
	 */
	NSTimestamp dCreation();
	
	/**
	 * 
	 * @return IEtudiant
	 */
	IEtudiant toEtudiant();
	
	/**
	 * Relation avec l'IEtudiant
	 * @param etudiant l'étudiant
	 */
	void setToEtudiantRelationship(IEtudiant etudiant);

	/**
	 * 
	 * @return IEtudiantAnnee
	 */
	IEtudiantAnnee toEtudiantAnnee();
	
	/**
	 * Relation avec l'IEtudiantAnnee
	 * @param etudiantAnnee l'étudiantAnnee
	 */
	void setToEtudiantAnneeRelationship(IEtudiantAnnee etudiantAnnee);

	/**
	 * lien vers le diplome
	 * @param diplome le diplome sur lequel est l'inscription
	 */
	void setToDiplomeRelationship(IDiplome diplome);

	/**
	 * 
	 * @param gradeUniversitaire le grade universitaire sur lequel est l'inscription
	 */
	void setToGradeUniversitaireRelationship(IGradeUniversitaire gradeUniversitaire);

	/**
	 * 
	 * @param typeInscription le typeInscription sur lequel est l'inscription
	 */
	void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation typeInscription);

	/**
	 * 
	 * @param parcours le parcours sur lequel est l'inscription
	 */
	void setToParcoursDiplomeRelationship(EOParcours parcours);

	/**
	 * 
	 * @param selectedParcours le parcours sur lequel est l'inscription
	 */
	void setToParcoursAnneeRelationship(EOParcours selectedParcours);
	
	/**
	 * @return retourne la version de diplome
	 */
	IVersionDiplome versionDiplome();
	

	/**
	 * @return true si l'inscription est de type dette ou anticipation 
	 */
	boolean isDetteOuAnticipation();

	/**
	 * @return id du lien consultation de l'année d'inscription
	 */
	String idLienConsultationAnnee();

	/**
	 * @param value id du lien consultation de l'année d'inscription
	 */
	void setIdLienConsultationAnnee(String value);

	/**
	 * @return id du lien consultation de la version de diplome 
	 */
	String idLienConsultationDiplome();

	/**
	 * @param value id du lien consultation de la version de diplome 
	 */
	void setIdLienConsultationDiplome(String value);

	/**
	 * @return  id du lien consultation du parcours sous l'année
	 */
	String idLienConsultationParcoursAnnee();

	/**
	 * @param value id du lien consultation du parcours sous l'année
	 */
	void setIdLienConsultationParcoursAnnee(String value);

	/**
	 * @return id du lien consultation du parcours sous la version de diplome
	 */
	String idLienConsultationParcoursDiplome();

	/**
	 * @param value  id du lien consultation du parcours sous la version de diplome
	 */
	void setIdLienConsultationParcoursDiplome(String value);
	
}
