package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface IModePaiement {
	String CODE_MODE_CB = "CB";
	String CODE_MODE_CHEQUE = "CHE";
	String CODE_MODE_PRELEVEMENT = "PRE";
	String CODE_MODE_VIREMENT = "VIR";
	String CODE_MODE_CHEQUE_PRELEVEMENT = "CHE_PRE";
	String CODE_MODE_CB_PRELEVEMENT = "CB_PRE";
	String CODE_MODE_VIREMENT_PRELEVEMENT = "VIR_PRE";

    String code();

    String libelle();

    String modCodePaiement();

    String modCodeRemboursement();
    
    Integer ordre();
    
    /**
     * Retourne le plan comptable de paiement pour ce mode de paiement et cet exercice.
     * 
     * @param exercice l'exercice (l'année)
     * @return le plan comptable pour ce mode de paiement et cet exercice
     */
    String getPlanComptablePaiementAUtilisePourExercice(int exercice);

    /**
     * Retourne le plan comptable de remboursement pour ce mode de paiement et cet exercice.
     * 
     * @param exercice l'exercice (l'année)
     * @return le plan comptable pour ce mode de paiement et cet exercice
     */
    String getPlanComptableRemboursementAUtilisePourExercice(int exercice);
    
    /**
     * @return true si moyen de paiement simple (CB, Cheque,...), false si moyen
     * 			de paiement combiné (CB + Prelevement...)
     */
    boolean isSimple();
    
    /**
     * @return true si c'est un mode de paiement "Prelevement"
     */
    boolean isPrelevement();
    
    /**
     * @return true si c'est un mode de paiement "Cheque"
     */
    boolean isCheque();
    
    /**
     * @return true si c'est un mode de paiement "Virement"
     */
    boolean isVirement();
}