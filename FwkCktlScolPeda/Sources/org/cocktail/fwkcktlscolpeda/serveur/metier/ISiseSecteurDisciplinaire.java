package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Secteur disciplinaire SISE associé à un composant (de type diplome)
 */
public interface ISiseSecteurDisciplinaire {
	/**
	 * @return code du secteur disciplinaire
	 */
	String code();

	/**
	 * @param value code du secteur disciplinaire
	 */
	void setCode(String value);

	/**
	 * @return libelle du secteur disciplinaire
	 */
	String libelle();

	/**
	 * @param value libelle du secteur disciplinaire
	 */
	void setLibelle(String value);
}
