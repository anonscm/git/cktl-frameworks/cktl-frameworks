package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette classe représente une pièce justificative que l'étudiant doit fournir à l'établissement.
 * 
 * @author Pascal MACOUIN
 */
public interface IPieceJustificative {

	/**
	 * @return le code de la pièce justificative
	 */
	String code();

	/**
	 * @param code un code
	 */
	void setCode(String code);

	/**
	 * @return le libellé de la pièce justificative
	 */
	String libelle();

	/**
	 * @param libelle un libellé
	 */
	void setLibelle(String libelle);

	/**
	 * @return le numéro d'ordre d'affichage de la pièce justificative
	 */
	Integer ordre();

	/**
	 * @param ordre un numéro d'ordre
	 */
	void setOrdre(Integer ordre);

}