package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 *  Resultat obtenu suite au calcul de moyenne
 */
public interface INoteResultat {

	String AJOURNE = "AJOURNE";
	String ADMIS = "ADMIS";
	String ADMIS_COMPENSATION = "ADMIS_COMP";
	String DETTE = "DETTE";

	/**
	 * @return  code du résultat
	 */
	String code();

	/**
	 * @param value  code du résultat
	 */
	void setCode(String value);

	/**
	 * @return  libellé du résultat
	 */
	String libelle();

	/**
	 * @param value  libellé du résultat
	 */
	void setLibelle(String value);

}