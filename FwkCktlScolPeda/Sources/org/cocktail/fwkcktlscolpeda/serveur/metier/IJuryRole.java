package org.cocktail.fwkcktlscolpeda.serveur.metier;



/**
 * 
 * @author isabelle
 *
 */
public interface IJuryRole {
	
	String JR_MEMBRE = "MEMBRE";
	String JR_RESPONSABLE = "RESPONSABLE";

	String code();

	String libelle();
	
}