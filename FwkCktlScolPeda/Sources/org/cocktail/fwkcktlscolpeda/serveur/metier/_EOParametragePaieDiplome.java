// DO NOT EDIT.  Make changes to EOParametragePaieDiplome.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametragePaieDiplome extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametragePaieDiplome";

  // Attribute Keys
  public static final ERXKey<Boolean> BOURSIER = new ERXKey<Boolean>("boursier");
  public static final ERXKey<String> COMPTABILITE = new ERXKey<String>("comptabilite");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> REMBOURSEMENT = new ERXKey<Boolean>("remboursement");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> TO_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("toDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> TO_PARCOURS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("toParcours");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant> TO_TYPE_MONTANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant>("toTypeMontant");

  // Attributes
  public static final String BOURSIER_KEY = BOURSIER.key();
  public static final String COMPTABILITE_KEY = COMPTABILITE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String REMBOURSEMENT_KEY = REMBOURSEMENT.key();
  // Relationships
  public static final String TO_DIPLOME_KEY = TO_DIPLOME.key();
  public static final String TO_PARCOURS_KEY = TO_PARCOURS.key();
  public static final String TO_TYPE_MONTANT_KEY = TO_TYPE_MONTANT.key();

  private static Logger LOG = Logger.getLogger(_EOParametragePaieDiplome.class);

  public EOParametragePaieDiplome localInstanceIn(EOEditingContext editingContext) {
    EOParametragePaieDiplome localInstance = (EOParametragePaieDiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean boursier() {
    return (Boolean) storedValueForKey(_EOParametragePaieDiplome.BOURSIER_KEY);
  }

  public void setBoursier(Boolean value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating boursier from " + boursier() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.BOURSIER_KEY);
  }

  public String comptabilite() {
    return (String) storedValueForKey(_EOParametragePaieDiplome.COMPTABILITE_KEY);
  }

  public void setComptabilite(String value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating comptabilite from " + comptabilite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.COMPTABILITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieDiplome.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieDiplome.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(_EOParametragePaieDiplome.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.MONTANT_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EOParametragePaieDiplome.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.NIVEAU_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametragePaieDiplome.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOParametragePaieDiplome.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean remboursement() {
    return (Boolean) storedValueForKey(_EOParametragePaieDiplome.REMBOURSEMENT_KEY);
  }

  public void setRemboursement(Boolean value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
    	_EOParametragePaieDiplome.LOG.debug( "updating remboursement from " + remboursement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieDiplome.REMBOURSEMENT_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome)storedValueForKey(_EOParametragePaieDiplome.TO_DIPLOME_KEY);
  }
  
  public void setToDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    takeStoredValueForKey(value, _EOParametragePaieDiplome.TO_DIPLOME_KEY);
  }

  public void setToDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
      _EOParametragePaieDiplome.LOG.debug("updating toDiplome from " + toDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome oldValue = toDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieDiplome.TO_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieDiplome.TO_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours toParcours() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(_EOParametragePaieDiplome.TO_PARCOURS_KEY);
  }
  
  public void setToParcours(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    takeStoredValueForKey(value, _EOParametragePaieDiplome.TO_PARCOURS_KEY);
  }

  public void setToParcoursRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
      _EOParametragePaieDiplome.LOG.debug("updating toParcours from " + toParcours() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParcours(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = toParcours();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieDiplome.TO_PARCOURS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieDiplome.TO_PARCOURS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant toTypeMontant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant)storedValueForKey(_EOParametragePaieDiplome.TO_TYPE_MONTANT_KEY);
  }
  
  public void setToTypeMontant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant value) {
    takeStoredValueForKey(value, _EOParametragePaieDiplome.TO_TYPE_MONTANT_KEY);
  }

  public void setToTypeMontantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant value) {
    if (_EOParametragePaieDiplome.LOG.isDebugEnabled()) {
      _EOParametragePaieDiplome.LOG.debug("updating toTypeMontant from " + toTypeMontant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeMontant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant oldValue = toTypeMontant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieDiplome.TO_TYPE_MONTANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieDiplome.TO_TYPE_MONTANT_KEY);
    }
  }
  

  public static EOParametragePaieDiplome createSco_ParametragePaieDiplome(EOEditingContext editingContext, Boolean boursier
, NSTimestamp dCreation
, java.math.BigDecimal montant
, Integer persIdCreation
, Boolean remboursement
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeMontant toTypeMontant) {
    EOParametragePaieDiplome eo = (EOParametragePaieDiplome) EOUtilities.createAndInsertInstance(editingContext, _EOParametragePaieDiplome.ENTITY_NAME);    
		eo.setBoursier(boursier);
		eo.setDCreation(dCreation);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setRemboursement(remboursement);
    eo.setToDiplomeRelationship(toDiplome);
    eo.setToTypeMontantRelationship(toTypeMontant);
    return eo;
  }

  public static ERXFetchSpecification<EOParametragePaieDiplome> fetchSpec() {
    return new ERXFetchSpecification<EOParametragePaieDiplome>(_EOParametragePaieDiplome.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametragePaieDiplome> fetchAllSco_ParametragePaieDiplomes(EOEditingContext editingContext) {
    return _EOParametragePaieDiplome.fetchAllSco_ParametragePaieDiplomes(editingContext, null);
  }

  public static NSArray<EOParametragePaieDiplome> fetchAllSco_ParametragePaieDiplomes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametragePaieDiplome> fetchSco_ParametragePaieDiplomes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametragePaieDiplome> fetchSpec = new ERXFetchSpecification<EOParametragePaieDiplome>(_EOParametragePaieDiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametragePaieDiplome> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametragePaieDiplome fetchSco_ParametragePaieDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieDiplome.fetchSco_ParametragePaieDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieDiplome fetchSco_ParametragePaieDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametragePaieDiplome> eoObjects = _EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
    EOParametragePaieDiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametragePaieDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieDiplome fetchRequiredSco_ParametragePaieDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieDiplome.fetchRequiredSco_ParametragePaieDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieDiplome fetchRequiredSco_ParametragePaieDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametragePaieDiplome eoObject = _EOParametragePaieDiplome.fetchSco_ParametragePaieDiplome(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametragePaieDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieDiplome localInstanceIn(EOEditingContext editingContext, EOParametragePaieDiplome eo) {
    EOParametragePaieDiplome localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
