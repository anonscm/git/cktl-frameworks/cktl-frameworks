// DO NOT EDIT.  Make changes to EOParametrageChamp.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametrageChamp extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametrageChamp";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp> TO_CHAMP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp>("toChamp");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription> TO_PARAMETRAGE_INSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription>("toParametrageInscription");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage> TO_TYPE_ADMISSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage>("toTypeAdmission");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage> TO_TYPE_GESTIONNAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage>("toTypeGestionnaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage> TO_TYPE_PREINSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage>("toTypePreinscription");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage> TO_TYPE_REINSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage>("toTypeReinscription");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships
  public static final String TO_CHAMP_KEY = TO_CHAMP.key();
  public static final String TO_PARAMETRAGE_INSCRIPTION_KEY = TO_PARAMETRAGE_INSCRIPTION.key();
  public static final String TO_TYPE_ADMISSION_KEY = TO_TYPE_ADMISSION.key();
  public static final String TO_TYPE_GESTIONNAIRE_KEY = TO_TYPE_GESTIONNAIRE.key();
  public static final String TO_TYPE_PREINSCRIPTION_KEY = TO_TYPE_PREINSCRIPTION.key();
  public static final String TO_TYPE_REINSCRIPTION_KEY = TO_TYPE_REINSCRIPTION.key();

  private static Logger LOG = Logger.getLogger(_EOParametrageChamp.class);

  public EOParametrageChamp localInstanceIn(EOEditingContext editingContext) {
    EOParametrageChamp localInstance = (EOParametrageChamp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametrageChamp.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
    	_EOParametrageChamp.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageChamp.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOParametrageChamp.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
    	_EOParametrageChamp.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageChamp.D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametrageChamp.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
    	_EOParametrageChamp.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageChamp.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOParametrageChamp.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
    	_EOParametrageChamp.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageChamp.PERS_ID_MODIFCATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp toChamp() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp)storedValueForKey(_EOParametrageChamp.TO_CHAMP_KEY);
  }
  
  public void setToChamp(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_CHAMP_KEY);
  }

  public void setToChampRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toChamp from " + toChamp() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToChamp(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp oldValue = toChamp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_CHAMP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_CHAMP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription toParametrageInscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription)storedValueForKey(_EOParametrageChamp.TO_PARAMETRAGE_INSCRIPTION_KEY);
  }
  
  public void setToParametrageInscription(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_PARAMETRAGE_INSCRIPTION_KEY);
  }

  public void setToParametrageInscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toParametrageInscription from " + toParametrageInscription() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParametrageInscription(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription oldValue = toParametrageInscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_PARAMETRAGE_INSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_PARAMETRAGE_INSCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeAdmission() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage)storedValueForKey(_EOParametrageChamp.TO_TYPE_ADMISSION_KEY);
  }
  
  public void setToTypeAdmission(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_TYPE_ADMISSION_KEY);
  }

  public void setToTypeAdmissionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toTypeAdmission from " + toTypeAdmission() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeAdmission(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage oldValue = toTypeAdmission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_TYPE_ADMISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_TYPE_ADMISSION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeGestionnaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage)storedValueForKey(_EOParametrageChamp.TO_TYPE_GESTIONNAIRE_KEY);
  }
  
  public void setToTypeGestionnaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_TYPE_GESTIONNAIRE_KEY);
  }

  public void setToTypeGestionnaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toTypeGestionnaire from " + toTypeGestionnaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeGestionnaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage oldValue = toTypeGestionnaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_TYPE_GESTIONNAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_TYPE_GESTIONNAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypePreinscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage)storedValueForKey(_EOParametrageChamp.TO_TYPE_PREINSCRIPTION_KEY);
  }
  
  public void setToTypePreinscription(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_TYPE_PREINSCRIPTION_KEY);
  }

  public void setToTypePreinscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toTypePreinscription from " + toTypePreinscription() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypePreinscription(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage oldValue = toTypePreinscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_TYPE_PREINSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_TYPE_PREINSCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeReinscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage)storedValueForKey(_EOParametrageChamp.TO_TYPE_REINSCRIPTION_KEY);
  }
  
  public void setToTypeReinscription(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    takeStoredValueForKey(value, _EOParametrageChamp.TO_TYPE_REINSCRIPTION_KEY);
  }

  public void setToTypeReinscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage value) {
    if (_EOParametrageChamp.LOG.isDebugEnabled()) {
      _EOParametrageChamp.LOG.debug("updating toTypeReinscription from " + toTypeReinscription() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeReinscription(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage oldValue = toTypeReinscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametrageChamp.TO_TYPE_REINSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametrageChamp.TO_TYPE_REINSCRIPTION_KEY);
    }
  }
  

  public static EOParametrageChamp createSco_ParametrageChamp(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCreation
, Integer persIdModifcation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOChamp toChamp, org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription toParametrageInscription, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeAdmission, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeGestionnaire, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypePreinscription, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeParametreAffichage toTypeReinscription) {
    EOParametrageChamp eo = (EOParametrageChamp) EOUtilities.createAndInsertInstance(editingContext, _EOParametrageChamp.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModifcation(persIdModifcation);
    eo.setToChampRelationship(toChamp);
    eo.setToParametrageInscriptionRelationship(toParametrageInscription);
    eo.setToTypeAdmissionRelationship(toTypeAdmission);
    eo.setToTypeGestionnaireRelationship(toTypeGestionnaire);
    eo.setToTypePreinscriptionRelationship(toTypePreinscription);
    eo.setToTypeReinscriptionRelationship(toTypeReinscription);
    return eo;
  }

  public static ERXFetchSpecification<EOParametrageChamp> fetchSpec() {
    return new ERXFetchSpecification<EOParametrageChamp>(_EOParametrageChamp.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametrageChamp> fetchAllSco_ParametrageChamps(EOEditingContext editingContext) {
    return _EOParametrageChamp.fetchAllSco_ParametrageChamps(editingContext, null);
  }

  public static NSArray<EOParametrageChamp> fetchAllSco_ParametrageChamps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametrageChamp.fetchSco_ParametrageChamps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametrageChamp> fetchSco_ParametrageChamps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametrageChamp> fetchSpec = new ERXFetchSpecification<EOParametrageChamp>(_EOParametrageChamp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametrageChamp> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametrageChamp fetchSco_ParametrageChamp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametrageChamp.fetchSco_ParametrageChamp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametrageChamp fetchSco_ParametrageChamp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametrageChamp> eoObjects = _EOParametrageChamp.fetchSco_ParametrageChamps(editingContext, qualifier, null);
    EOParametrageChamp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametrageChamp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametrageChamp fetchRequiredSco_ParametrageChamp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametrageChamp.fetchRequiredSco_ParametrageChamp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametrageChamp fetchRequiredSco_ParametrageChamp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametrageChamp eoObject = _EOParametrageChamp.fetchSco_ParametrageChamp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametrageChamp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametrageChamp localInstanceIn(EOEditingContext editingContext, EOParametrageChamp eo) {
    EOParametrageChamp localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
