// DO NOT EDIT.  Make changes to EOPlancoGestion.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPlancoGestion extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PlancoGestion";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> GES_CODE = new ERXKey<String>("gesCode");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> TO_PLAN_COMPTABLE = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("toPlanComptable");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String GES_CODE_KEY = GES_CODE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_PLAN_COMPTABLE_KEY = TO_PLAN_COMPTABLE.key();
  public static final String TO_RNE_KEY = TO_RNE.key();

  private static Logger LOG = Logger.getLogger(_EOPlancoGestion.class);

  public EOPlancoGestion localInstanceIn(EOEditingContext editingContext) {
    EOPlancoGestion localInstance = (EOPlancoGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPlancoGestion.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPlancoGestion.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPlancoGestion.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPlancoGestion.D_MODIFICATION_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(_EOPlancoGestion.GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating gesCode from " + gesCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPlancoGestion.GES_CODE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPlancoGestion.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPlancoGestion.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPlancoGestion.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
    	_EOPlancoGestion.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPlancoGestion.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptable() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(_EOPlancoGestion.TO_PLAN_COMPTABLE_KEY);
  }
  
  public void setToPlanComptable(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, _EOPlancoGestion.TO_PLAN_COMPTABLE_KEY);
  }

  public void setToPlanComptableRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
      _EOPlancoGestion.LOG.debug("updating toPlanComptable from " + toPlanComptable() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPlanComptable(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = toPlanComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPlancoGestion.TO_PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPlancoGestion.TO_PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(_EOPlancoGestion.TO_RNE_KEY);
  }
  
  public void setToRne(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    takeStoredValueForKey(value, _EOPlancoGestion.TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (_EOPlancoGestion.LOG.isDebugEnabled()) {
      _EOPlancoGestion.LOG.debug("updating toRne from " + toRne() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRne(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPlancoGestion.TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPlancoGestion.TO_RNE_KEY);
    }
  }
  

  public static EOPlancoGestion createSco_PlancoGestion(EOEditingContext editingContext, NSTimestamp dCreation
, String gesCode
, Integer persIdCreation
, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptable) {
    EOPlancoGestion eo = (EOPlancoGestion) EOUtilities.createAndInsertInstance(editingContext, _EOPlancoGestion.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setGesCode(gesCode);
		eo.setPersIdCreation(persIdCreation);
    eo.setToPlanComptableRelationship(toPlanComptable);
    return eo;
  }

  public static ERXFetchSpecification<EOPlancoGestion> fetchSpec() {
    return new ERXFetchSpecification<EOPlancoGestion>(_EOPlancoGestion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPlancoGestion> fetchAllSco_PlancoGestions(EOEditingContext editingContext) {
    return _EOPlancoGestion.fetchAllSco_PlancoGestions(editingContext, null);
  }

  public static NSArray<EOPlancoGestion> fetchAllSco_PlancoGestions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPlancoGestion.fetchSco_PlancoGestions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPlancoGestion> fetchSco_PlancoGestions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPlancoGestion> fetchSpec = new ERXFetchSpecification<EOPlancoGestion>(_EOPlancoGestion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPlancoGestion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPlancoGestion fetchSco_PlancoGestion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPlancoGestion.fetchSco_PlancoGestion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPlancoGestion fetchSco_PlancoGestion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPlancoGestion> eoObjects = _EOPlancoGestion.fetchSco_PlancoGestions(editingContext, qualifier, null);
    EOPlancoGestion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PlancoGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPlancoGestion fetchRequiredSco_PlancoGestion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPlancoGestion.fetchRequiredSco_PlancoGestion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPlancoGestion fetchRequiredSco_PlancoGestion(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPlancoGestion eoObject = _EOPlancoGestion.fetchSco_PlancoGestion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PlancoGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPlancoGestion localInstanceIn(EOEditingContext editingContext, EOPlancoGestion eo) {
    EOPlancoGestion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
