// DO NOT EDIT.  Make changes to EOPaiementDetail.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPaiementDetail extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PaiementDetail";

  // Attribute Keys
  public static final ERXKey<Boolean> AUTO = new ERXKey<Boolean>("auto");
  public static final ERXKey<NSTimestamp> DATE_PAYE = new ERXKey<NSTimestamp>("datePaye");
  public static final ERXKey<NSTimestamp> DATE_REMBOURSE = new ERXKey<NSTimestamp>("dateRembourse");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT_A_PAYER = new ERXKey<java.math.BigDecimal>("montantAPayer");
  public static final ERXKey<java.math.BigDecimal> MONTANT_A_REMBOURSER = new ERXKey<java.math.BigDecimal>("montantARembourser");
  public static final ERXKey<java.math.BigDecimal> MONTANT_PAYE = new ERXKey<java.math.BigDecimal>("montantPaye");
  public static final ERXKey<java.math.BigDecimal> MONTANT_REMBOURSE = new ERXKey<java.math.BigDecimal>("montantRembourse");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>("toPaiement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>("toParametragePaieArticleComplementaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome> TO_PARAMETRAGE_PAIE_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome>("toParametragePaieDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation> TO_PARAMETRAGE_PAIE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation>("toParametragePaieFormation");

  // Attributes
  public static final String AUTO_KEY = AUTO.key();
  public static final String DATE_PAYE_KEY = DATE_PAYE.key();
  public static final String DATE_REMBOURSE_KEY = DATE_REMBOURSE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String MONTANT_A_PAYER_KEY = MONTANT_A_PAYER.key();
  public static final String MONTANT_A_REMBOURSER_KEY = MONTANT_A_REMBOURSER.key();
  public static final String MONTANT_PAYE_KEY = MONTANT_PAYE.key();
  public static final String MONTANT_REMBOURSE_KEY = MONTANT_REMBOURSE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_PAIEMENT_KEY = TO_PAIEMENT.key();
  public static final String TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY = TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.key();
  public static final String TO_PARAMETRAGE_PAIE_DIPLOME_KEY = TO_PARAMETRAGE_PAIE_DIPLOME.key();
  public static final String TO_PARAMETRAGE_PAIE_FORMATION_KEY = TO_PARAMETRAGE_PAIE_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOPaiementDetail.class);

  public EOPaiementDetail localInstanceIn(EOEditingContext editingContext) {
    EOPaiementDetail localInstance = (EOPaiementDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean auto() {
    return (Boolean) storedValueForKey(_EOPaiementDetail.AUTO_KEY);
  }

  public void setAuto(Boolean value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating auto from " + auto() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.AUTO_KEY);
  }

  public NSTimestamp datePaye() {
    return (NSTimestamp) storedValueForKey(_EOPaiementDetail.DATE_PAYE_KEY);
  }

  public void setDatePaye(NSTimestamp value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating datePaye from " + datePaye() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.DATE_PAYE_KEY);
  }

  public NSTimestamp dateRembourse() {
    return (NSTimestamp) storedValueForKey(_EOPaiementDetail.DATE_REMBOURSE_KEY);
  }

  public void setDateRembourse(NSTimestamp value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating dateRembourse from " + dateRembourse() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.DATE_REMBOURSE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPaiementDetail.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPaiementDetail.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montantAPayer() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementDetail.MONTANT_A_PAYER_KEY);
  }

  public void setMontantAPayer(java.math.BigDecimal value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating montantAPayer from " + montantAPayer() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.MONTANT_A_PAYER_KEY);
  }

  public java.math.BigDecimal montantARembourser() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementDetail.MONTANT_A_REMBOURSER_KEY);
  }

  public void setMontantARembourser(java.math.BigDecimal value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating montantARembourser from " + montantARembourser() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.MONTANT_A_REMBOURSER_KEY);
  }

  public java.math.BigDecimal montantPaye() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementDetail.MONTANT_PAYE_KEY);
  }

  public void setMontantPaye(java.math.BigDecimal value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating montantPaye from " + montantPaye() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.MONTANT_PAYE_KEY);
  }

  public java.math.BigDecimal montantRembourse() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementDetail.MONTANT_REMBOURSE_KEY);
  }

  public void setMontantRembourse(java.math.BigDecimal value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating montantRembourse from " + montantRembourse() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.MONTANT_REMBOURSE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPaiementDetail.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPaiementDetail.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
    	_EOPaiementDetail.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementDetail.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement toPaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement)storedValueForKey(_EOPaiementDetail.TO_PAIEMENT_KEY);
  }
  
  public void setToPaiement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement value) {
    takeStoredValueForKey(value, _EOPaiementDetail.TO_PAIEMENT_KEY);
  }

  public void setToPaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
      _EOPaiementDetail.LOG.debug("updating toPaiement from " + toPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement oldValue = toPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementDetail.TO_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementDetail.TO_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire toParametragePaieArticleComplementaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire)storedValueForKey(_EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
  }
  
  public void setToParametragePaieArticleComplementaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire value) {
    takeStoredValueForKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
  }

  public void setToParametragePaieArticleComplementaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
      _EOPaiementDetail.LOG.debug("updating toParametragePaieArticleComplementaire from " + toParametragePaieArticleComplementaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParametragePaieArticleComplementaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire oldValue = toParametragePaieArticleComplementaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome toParametragePaieDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome)storedValueForKey(_EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
  }
  
  public void setToParametragePaieDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome value) {
    takeStoredValueForKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
  }

  public void setToParametragePaieDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
      _EOPaiementDetail.LOG.debug("updating toParametragePaieDiplome from " + toParametragePaieDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParametragePaieDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome oldValue = toParametragePaieDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation toParametragePaieFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation)storedValueForKey(_EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION_KEY);
  }
  
  public void setToParametragePaieFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation value) {
    takeStoredValueForKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION_KEY);
  }

  public void setToParametragePaieFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation value) {
    if (_EOPaiementDetail.LOG.isDebugEnabled()) {
      _EOPaiementDetail.LOG.debug("updating toParametragePaieFormation from " + toParametragePaieFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParametragePaieFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation oldValue = toParametragePaieFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION_KEY);
    }
  }
  

  public static EOPaiementDetail createSco_PaiementDetail(EOEditingContext editingContext, Boolean auto
, NSTimestamp dCreation
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement toPaiement) {
    EOPaiementDetail eo = (EOPaiementDetail) EOUtilities.createAndInsertInstance(editingContext, _EOPaiementDetail.ENTITY_NAME);    
		eo.setAuto(auto);
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToPaiementRelationship(toPaiement);
    return eo;
  }

  public static ERXFetchSpecification<EOPaiementDetail> fetchSpec() {
    return new ERXFetchSpecification<EOPaiementDetail>(_EOPaiementDetail.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPaiementDetail> fetchAllSco_PaiementDetails(EOEditingContext editingContext) {
    return _EOPaiementDetail.fetchAllSco_PaiementDetails(editingContext, null);
  }

  public static NSArray<EOPaiementDetail> fetchAllSco_PaiementDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPaiementDetail.fetchSco_PaiementDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPaiementDetail> fetchSco_PaiementDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPaiementDetail> fetchSpec = new ERXFetchSpecification<EOPaiementDetail>(_EOPaiementDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPaiementDetail> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPaiementDetail fetchSco_PaiementDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementDetail.fetchSco_PaiementDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementDetail fetchSco_PaiementDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPaiementDetail> eoObjects = _EOPaiementDetail.fetchSco_PaiementDetails(editingContext, qualifier, null);
    EOPaiementDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PaiementDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementDetail fetchRequiredSco_PaiementDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementDetail.fetchRequiredSco_PaiementDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementDetail fetchRequiredSco_PaiementDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPaiementDetail eoObject = _EOPaiementDetail.fetchSco_PaiementDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PaiementDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementDetail localInstanceIn(EOEditingContext editingContext, EOPaiementDetail eo) {
    EOPaiementDetail localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
