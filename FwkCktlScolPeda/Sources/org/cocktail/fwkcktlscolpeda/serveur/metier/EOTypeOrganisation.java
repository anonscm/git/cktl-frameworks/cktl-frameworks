package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * Représentation d'un type d'organisation (annuelle, semestrielle, continue...).
 */
@SuppressWarnings("serial")
public class EOTypeOrganisation extends _EOTypeOrganisation {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(EOTypeOrganisation.class);

    @Override
    public String toString() {
    	return this.type();
    }
}
