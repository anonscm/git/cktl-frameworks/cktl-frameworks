package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSDictionary;

/**
 * Domaine 
 */
public class EODomaine extends _EODomaine implements IDomaine {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODomaine.class);
	

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.libelle();
	}

	/**
	 * {@inheritDoc}
	 */

	public String getCode() {
		return code();
	}

	/**
	 * {@inheritDoc}
	 */
	public String getLibelle() {
		return libelle();
	}
	
}
