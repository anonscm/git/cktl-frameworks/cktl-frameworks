// DO NOT EDIT.  Make changes to EOTypeMontant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeMontant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeMontant";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeMontant.class);

  public EOTypeMontant localInstanceIn(EOEditingContext editingContext) {
    EOTypeMontant localInstance = (EOTypeMontant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeMontant.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeMontant.LOG.isDebugEnabled()) {
    	_EOTypeMontant.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeMontant.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeMontant.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeMontant.LOG.isDebugEnabled()) {
    	_EOTypeMontant.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeMontant.LIBELLE_KEY);
  }


  public static EOTypeMontant createSco_TypeMontant(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeMontant eo = (EOTypeMontant) EOUtilities.createAndInsertInstance(editingContext, _EOTypeMontant.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeMontant> fetchSpec() {
    return new ERXFetchSpecification<EOTypeMontant>(_EOTypeMontant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeMontant> fetchAllSco_TypeMontants(EOEditingContext editingContext) {
    return _EOTypeMontant.fetchAllSco_TypeMontants(editingContext, null);
  }

  public static NSArray<EOTypeMontant> fetchAllSco_TypeMontants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeMontant.fetchSco_TypeMontants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeMontant> fetchSco_TypeMontants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeMontant> fetchSpec = new ERXFetchSpecification<EOTypeMontant>(_EOTypeMontant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeMontant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeMontant fetchSco_TypeMontant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMontant.fetchSco_TypeMontant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMontant fetchSco_TypeMontant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeMontant> eoObjects = _EOTypeMontant.fetchSco_TypeMontants(editingContext, qualifier, null);
    EOTypeMontant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeMontant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMontant fetchRequiredSco_TypeMontant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMontant.fetchRequiredSco_TypeMontant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMontant fetchRequiredSco_TypeMontant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeMontant eoObject = _EOTypeMontant.fetchSco_TypeMontant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeMontant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMontant localInstanceIn(EOEditingContext editingContext, EOTypeMontant eo) {
    EOTypeMontant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
