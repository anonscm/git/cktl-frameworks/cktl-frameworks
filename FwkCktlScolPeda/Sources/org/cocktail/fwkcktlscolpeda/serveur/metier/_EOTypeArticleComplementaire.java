// DO NOT EDIT.  Make changes to EOTypeArticleComplementaire.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeArticleComplementaire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeArticleComplementaire";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>("toParametragePaieArticlesComplementaires");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  // Relationships
  public static final String TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY = TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES.key();

  private static Logger LOG = Logger.getLogger(_EOTypeArticleComplementaire.class);

  public EOTypeArticleComplementaire localInstanceIn(EOEditingContext editingContext) {
    EOTypeArticleComplementaire localInstance = (EOTypeArticleComplementaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeArticleComplementaire.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOTypeArticleComplementaire.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeArticleComplementaire.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeArticleComplementaire.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOTypeArticleComplementaire.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeArticleComplementaire.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOTypeArticleComplementaire.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOTypeArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOTypeArticleComplementaire.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeArticleComplementaire.ORDRE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> toParametragePaieArticlesComplementaires() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>)storedValueForKey(_EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> toParametragePaieArticlesComplementaires(EOQualifier qualifier) {
    return toParametragePaieArticlesComplementaires(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> toParametragePaieArticlesComplementaires(EOQualifier qualifier, boolean fetch) {
    return toParametragePaieArticlesComplementaires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> toParametragePaieArticlesComplementaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaires(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toParametragePaieArticlesComplementaires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToParametragePaieArticlesComplementaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
  }

  public void removeFromToParametragePaieArticlesComplementaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
  }

  public void addToToParametragePaieArticlesComplementairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire object) {
    if (_EOTypeArticleComplementaire.LOG.isDebugEnabled()) {
      _EOTypeArticleComplementaire.LOG.debug("adding " + object + " to toParametragePaieArticlesComplementaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToParametragePaieArticlesComplementaires(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
    }
  }

  public void removeFromToParametragePaieArticlesComplementairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire object) {
    if (_EOTypeArticleComplementaire.LOG.isDebugEnabled()) {
      _EOTypeArticleComplementaire.LOG.debug("removing " + object + " from toParametragePaieArticlesComplementaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToParametragePaieArticlesComplementaires(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire createToParametragePaieArticlesComplementairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire) eo;
  }

  public void deleteToParametragePaieArticlesComplementairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeArticleComplementaire.TO_PARAMETRAGE_PAIE_ARTICLES_COMPLEMENTAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToParametragePaieArticlesComplementairesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> objects = toParametragePaieArticlesComplementaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToParametragePaieArticlesComplementairesRelationship(objects.nextElement());
    }
  }


  public static EOTypeArticleComplementaire createSco_TypeArticleComplementaire(EOEditingContext editingContext, String code
, String libelle
, Integer ordre
) {
    EOTypeArticleComplementaire eo = (EOTypeArticleComplementaire) EOUtilities.createAndInsertInstance(editingContext, _EOTypeArticleComplementaire.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeArticleComplementaire> fetchSpec() {
    return new ERXFetchSpecification<EOTypeArticleComplementaire>(_EOTypeArticleComplementaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeArticleComplementaire> fetchAllSco_TypeArticleComplementaires(EOEditingContext editingContext) {
    return _EOTypeArticleComplementaire.fetchAllSco_TypeArticleComplementaires(editingContext, null);
  }

  public static NSArray<EOTypeArticleComplementaire> fetchAllSco_TypeArticleComplementaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeArticleComplementaire.fetchSco_TypeArticleComplementaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeArticleComplementaire> fetchSco_TypeArticleComplementaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeArticleComplementaire> fetchSpec = new ERXFetchSpecification<EOTypeArticleComplementaire>(_EOTypeArticleComplementaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeArticleComplementaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeArticleComplementaire fetchSco_TypeArticleComplementaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeArticleComplementaire.fetchSco_TypeArticleComplementaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeArticleComplementaire fetchSco_TypeArticleComplementaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeArticleComplementaire> eoObjects = _EOTypeArticleComplementaire.fetchSco_TypeArticleComplementaires(editingContext, qualifier, null);
    EOTypeArticleComplementaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeArticleComplementaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeArticleComplementaire fetchRequiredSco_TypeArticleComplementaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeArticleComplementaire.fetchRequiredSco_TypeArticleComplementaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeArticleComplementaire fetchRequiredSco_TypeArticleComplementaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeArticleComplementaire eoObject = _EOTypeArticleComplementaire.fetchSco_TypeArticleComplementaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeArticleComplementaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeArticleComplementaire localInstanceIn(EOEditingContext editingContext, EOTypeArticleComplementaire eo) {
    EOTypeArticleComplementaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
