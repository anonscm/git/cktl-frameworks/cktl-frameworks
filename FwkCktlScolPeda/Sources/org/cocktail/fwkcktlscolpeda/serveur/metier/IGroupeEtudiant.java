package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente l'appartenance d'un étudiant à un groupe.
 * 
 * @author Pascal MACOUIN
 */
public interface IGroupeEtudiant {

	/**
	 * @return L'étudiant année
	 */
	IScoEtudiantAnnee toEtudiantAnnee();

	/**
	 * @param unEtudiantAnnee Un étudiant année
	 */
	void setToEtudiantAnneeRelationship(IScoEtudiantAnnee unEtudiantAnnee);

	/**
	 * @return Le groupe
	 */
	IGroupe toGroupe();

	/**
	 * @param unGroupe Un groupe
	 */
	void setToGroupeRelationship(IGroupe unGroupe);
}