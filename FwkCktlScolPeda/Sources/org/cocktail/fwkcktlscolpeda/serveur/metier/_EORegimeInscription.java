// DO NOT EDIT.  Make changes to EORegimeInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORegimeInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_RegimeInscription";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EORegimeInscription.class);

  public EORegimeInscription localInstanceIn(EOEditingContext editingContext) {
    EORegimeInscription localInstance = (EORegimeInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EORegimeInscription.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EORegimeInscription.LOG.isDebugEnabled()) {
    	_EORegimeInscription.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeInscription.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EORegimeInscription.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EORegimeInscription.LOG.isDebugEnabled()) {
    	_EORegimeInscription.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EORegimeInscription.LIBELLE_KEY);
  }


  public static EORegimeInscription createSco_RegimeInscription(EOEditingContext editingContext, String code
, String libelle
) {
    EORegimeInscription eo = (EORegimeInscription) EOUtilities.createAndInsertInstance(editingContext, _EORegimeInscription.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EORegimeInscription> fetchSpec() {
    return new ERXFetchSpecification<EORegimeInscription>(_EORegimeInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORegimeInscription> fetchAllSco_RegimeInscriptions(EOEditingContext editingContext) {
    return _EORegimeInscription.fetchAllSco_RegimeInscriptions(editingContext, null);
  }

  public static NSArray<EORegimeInscription> fetchAllSco_RegimeInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORegimeInscription.fetchSco_RegimeInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EORegimeInscription> fetchSco_RegimeInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORegimeInscription> fetchSpec = new ERXFetchSpecification<EORegimeInscription>(_EORegimeInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORegimeInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORegimeInscription fetchSco_RegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegimeInscription.fetchSco_RegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegimeInscription fetchSco_RegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORegimeInscription> eoObjects = _EORegimeInscription.fetchSco_RegimeInscriptions(editingContext, qualifier, null);
    EORegimeInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_RegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegimeInscription fetchRequiredSco_RegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EORegimeInscription.fetchRequiredSco_RegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORegimeInscription fetchRequiredSco_RegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EORegimeInscription eoObject = _EORegimeInscription.fetchSco_RegimeInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_RegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORegimeInscription localInstanceIn(EOEditingContext editingContext, EORegimeInscription eo) {
    EORegimeInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
