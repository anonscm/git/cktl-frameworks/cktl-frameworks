package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

/**
 * Informations de l'etudiant pour une annee donnee
 */
public interface IScoEtudiantAnnee extends IEtudiantAnnee {

	/**
	 * @return l'identifiant
	 */
	Integer id();
	
	/**
	 * Information calculee a partir de affSs et toCotisation ;
	 * @return 1 : Etudiant affilie et non cotisant ; -1 : Etudiant affilie et cotisant ; -2 : Etudiant affilie, cotisant mais exonere.
	 */
	Integer ayantDroit();

	/**
	 * @param value 1 : Etudiant affilie et non cotisant ; -1 : Etudiant affilie et cotisant ; -2 : Etudiant affilie, cotisant mais exonere.
	 */
	void setAyantDroit(Integer value);
	
	/**
	 * @return la liste des bourses correspondantes à l'étudiant et à l'année
	 */
	List<IScoBourses> toBourses();
	
	/**
	 * Retourne <code>true</code> si cet étudiant a des données le concernant dans la GFC.
	 * <p>
	 * C'est le cas si un des paiements a un lien avec la GFC.
	 * 
	 * @return <code>true</code> si cet étudiant a des données le concernant dans la GFC
	 */
	boolean aUnLienAvecGfc();

	/**
	 * @return Le paiement initial (le premier paiement)
	 */
	IScoPaiement toScoPaiementInitial();
	
	/**
	 * @return les paiements de l'étudiant pour cet année.
	 */
	List<? extends IScoPaiement> toScoPaiements();
	
	/**
	 * @return groupe etudiant
	 */
	List<? extends IGroupeEtudiant> toGroupeEtudiants();
	
	/**
	 * @return lien vers l'etudiant
	 */
	IEtudiant toEtudiant();

	/**
	 * @param value etudiant
	 */
	void setToEtudiantRelationship(IEtudiant value);
	
	/**
	 * @return la liste des moyens de paiement par prélèvement
	 */
	List<IScoPaiementMoyen> moyensPaiementPrelevementSansEcheancierSepa();
	
	/**
	 * @param value Est-ce que l'étudiant année est valide
	 */
	void setValide(Boolean value);

}
