package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOAcademie;
import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOMention;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Informations de l'étudiant susceptible d'être modifiées dans coriandre
 * 
 */
public class ScoEtudiant implements IInfosEtudiant {
	private IEtudiant etudiant;
	private boolean ineProvisoire;


	public EOEditingContext editingContext() {
		return getEtudiant().editingContext();
	}
	
	/**
	 * @param etudiant etudiant à partir duquel on extrait les information
	 */
	public ScoEtudiant(IEtudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	public IEtudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(IEtudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Date getDtNaissance() {
		return getEtudiant().toIndividu().dNaissance();
	}

	public Integer getCleInsee() {
		return asIndividu().getCleInsee();
	}

	public Integer getCleInseeProv() {
		return null;
	}

	public String nomAffichage() {
		return getEtudiant().toIndividu().nomAffichage();
	}

	public String villeDeNaissance() {
		return asIndividu().villeDeNaissance();
	}


	public IAdresse adresseFacturation() {
		return asIndividu().adresseFacturation();
	}

	public IAdresse adresseEtudiant() {
		return asIndividu().adresseEtudiant();
	}

	public IAdresse adresseParent() {
		return asIndividu().adresseParent();
	}


	public Date dNaissance() {
		return asIndividu().dNaissance();
	}

	public CodeInsee getCodeInsee() {
		return asIndividu().getCodeInsee();
	}

	public String civilite() {
		return toCivilite().cCivilite();
	}




	public Integer cleInsee() {
		return asIndividu().getCleInsee();
	}

	public String codeIne() {
		return getEtudiant().etudCodeIne();
	}


	public IDepartement departementNaissance() {
		return asIndividu().toDepartement();
	}

	public String deptNaissanceLibelle() {
		IDepartement dept = departementNaissance();
		return dept != null ? dept.llDepartement() : null;
	}


	public Boolean enfantsCharge() {
		return getEtudiant().hasEnfants();
	}

	public Boolean ineProvisoire() {
		return ineProvisoire;
	}

	public Boolean inseeProvisoire() {
		return asIndividu().hasNoInseeProvisoire();
	}

	public String noInsee() {
		return asIndividu().indNoInsee();
	}

	public String nomPatronymique() {
		return asIndividu().nomPatronymique();
	}

	public String nomUsuel() {
		return asIndividu().nomUsuel();
	}


	public String prenom() {
		return asIndividu().prenom();
	}
	
	public void setPrenom(String value) {
		asIndividu().setPrenom(value);
	}
	
	public String prenom2() {
		return asIndividu().prenom2();
	}



	/**
	 * {@inheritDoc}
	 */
	public Boolean sportHN() {
		return (getEtudiant().etudSportHn().intValue() == 1);
	}

	public String villeNaissance() {
		return asIndividu().villeDeNaissance();
	}

	public EOCivilite toCivilite() {
		return (EOCivilite) asIndividu().getCivilite();
	}

	public IPays toPaysNaissance() {
		return asIndividu().toPaysNaissance();
	}

	public IPays toPaysNationalite() {
		return asIndividu().toPaysNationalite();
	}


	public ISituationFamiliale toSituationFamiliale() {
		return asIndividu().toSituationFamiliale();
	}


	public List<? extends ICursus> toCursus() {
		EOEtudiant monetudiant = (EOEtudiant) getEtudiant();
		
		return (List<? extends ICursus>) EOCursus.fetchSco_Cursuses(monetudiant.editingContext(), EOCursus.TO_ETUDIANT.eq(monetudiant), null);
	}
	
	


	public IIndividu toIndividuEtudiant() {
		return getEtudiant().toIndividu();
	}


	public void setCleInsee(Integer value) {
		asIndividu().setCleInsee(value);
	}

	public void setCodeIne(String value) {
		getEtudiant().setEtudCodeIne(value);
	}


	public void setDeptNaissance(IDepartement departement) {
		asIndividu().setToDepartementRelationship(departement);
	}


	public void setDNaissance(Date value) {
		asIndividu().setDNaissance(value);
	}


	public void setEnfantsCharge(Boolean value) {
		getEtudiant().setSituationFamilialeEtudiante(value);
	}

	public void setIneProvisoire(Boolean value) {
		if (value != null) {
			this.ineProvisoire = value;
		}
	}

	public void setInseeProvisoire(Boolean inseeProvisoire) {
		asIndividu().setHasNoInseeProvisoire(inseeProvisoire);
	}

	public void setNoInsee(String value) {
		asIndividu().setIndNoInsee(value);
	}

	public void setNomPatronymique(String value) {
		asIndividu().setNomPatronymique(value);
		asIndividu().setNomPatronymiqueAffichage(value);
	}

	public void setNomUsuel(String value) {
		asIndividu().setNomUsuel(value);
	}


	public void setPrenom2(String value) {
		asIndividu().setPrenom2(value);
	}


	/**
	 * {@inheritDoc}
	 */
	public void setSportHN(Boolean value) {
		EOEtudiant etudiant = (EOEtudiant) getEtudiant();
		if (value) {
			getEtudiant().setEtudSportHn(1L);
		} else {
			getEtudiant().setEtudSportHn(0L);
		}

	}




	public void setToSituationFamilialeRelationship(ISituationFamiliale value) {
		getEtudiant().setSituationFamilialeEtudiante(enfantsCharge());
		asIndividu().setToSituationFamilialeRelationship(value);
	}

	public IDepartement toDepartement() {
		return asIndividu().getDepartement();
	}

	/**
	 * {@inheritDoc}
	 */
	public IIndividu asIndividu() {
		return getEtudiant().toIndividu();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IAdresse> toAdresses() {
		return asIndividu().toAdresses();
	}

	public void setToDepartementRelationship(IDepartement departement) {
		asIndividu().setToDepartementRelationship(departement);
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimerAdresse(IAdresse currentAdresse, ITypeAdresse type) {
		asIndividu().supprimerAdresse(currentAdresse, type);
	}

	public List<ITelephone> toTelephones() {
		return asIndividu().toTelephones();
	}

	public ITelephone toTelephone(ITypeTel typeTel, ITypeNoTel typeNoTel) {
		return asIndividu().telephone(typeNoTel.cTypeNoTel(), typeTel.cTypeTel());
	}

	public void setToCiviliteRelationship(ICivilite value) {
		asIndividu().setToCiviliteRelationship(value);
		
	}

	public void setToPaysNaissanceRelationship(IPays value) {
		asIndividu().setToPaysNaissanceRelationship(value);
		
	}

	public void setToPaysNationaliteRelationship(IPays value) {
		asIndividu().setToPaysNationaliteRelationship(value);
	}

	public void setVilleNaissance(String value) {
		asIndividu().setVilleDeNaissance(value);
	}

	public Integer etudNumero() {
		return getEtudiant().etudNumero();
	}
	
	public EOProfession toProfession1() {
		EOProfession profession = null;
		if (getEtudiant().proCode()!=null) {
			profession = EOProfession.fetchByQualifier(getEtudiant().editingContext(), EOProfession.PRO_CODE.eq(getEtudiant().proCode()));
		}
		return profession;
	}
	
	public void setToProfession1Relationship(EOProfession value) {
		getEtudiant().setProCode(value.proCode());
	}

	public EOProfession toProfession2() {
		EOProfession profession = null;
		if (getEtudiant().proCode2()!=null) {
			profession = EOProfession.fetchByQualifier(getEtudiant().editingContext(), EOProfession.PRO_CODE.eq(getEtudiant().proCode2()));
		}
		return profession;
	}

	public void setToProfession2Relationship(EOProfession value) {
		getEtudiant().setProCode2(value.proCode());
	}

	public IInscription getInscriptionPrincipale() {
		return null;
	}

	public List<? extends IInscription> toInscriptions() {
		return new ArrayList<IInscription>(EOInscription.fetchSco_Inscriptions(getEtudiant().editingContext(), EOInscription.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()), null));
	}
	
	public List<? extends IInscription> toInscriptions(EOQualifier qualifier) {
		return new ArrayList<IInscription>(EOInscription.fetchSco_Inscriptions(getEtudiant().editingContext(), EOInscription.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()).and(qualifier), null));
	}

	public List<? extends IBourses> toBourses() {
		return new ArrayList<IBourses>(EOBourses.fetchSco_Bourseses(getEtudiant().editingContext(), EOBourses.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()), null));
	}
	
	public List<? extends IBourses> toBourses(EOQualifier qualifier) {
		return new ArrayList<IBourses>(EOBourses.fetchSco_Bourseses(getEtudiant().editingContext(), EOBourses.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()).and(qualifier), null));
	}

	public EOBac toBac() {
		return getEtudiant().toBac();
	}

	public Long etudAnbac() {
		
		return getEtudiant().etudAnbac();
	}
	public void setEtudAnbac(Long anBac) {
		
		 getEtudiant().setEtudAnbac(anBac);
	}
	
	public EOAcademie toAcademie() {
		return getEtudiant().toAcademie();
	}

	public Long etudAnnee1inscEtab() {
		return getEtudiant().etudAnnee1inscUlr();
	}
	
	public Long etudAnnee1inscSup() {
		return getEtudiant().etudAnnee1inscSup();
	}

	public Long etudAnnee1inscUniv() {
		return getEtudiant().etudAnnee1inscUniv();
	}
	
	public void setEtudAnnee1InscEtab(Long uneAnnee) {
		getEtudiant().setEtudAnnee1inscUlr(uneAnnee);
	}
	
	public void setEtudAnnee1InscSup(Long uneAnnee) {
		getEtudiant().setEtudAnnee1inscSup(uneAnnee);
	}
	
	public void setEtudAnnee1InscUniv(Long uneAnnee) {
		getEtudiant().setEtudAnnee1inscUniv(uneAnnee);
	}
	
	public EODepartement toDepartementEtabBac(){
		return getEtudiant().toDepartementEtabBac(); 
	}

	public void setToBacRelationship(EOBac bac) {
		getEtudiant().setToBacRelationship(bac);
	}
	
	public void setToMentionRelationship(EOMention mentionBacSelected) {
		getEtudiant().setToMentionRelationship((EOMention) mentionBacSelected);
	}

	public IRne toRneEtabBac() {
		return getEtudiant().toRneCodeBac();
	}
	
	public void setToRneEtabBacRelationship(IRne etablissementBac){
		getEtudiant().setToRneCodeBacRelationship((EORne) etablissementBac);
	}

	public void setVilleBac(String villeBac) {
		getEtudiant().setEtudVilleBac(villeBac);
		
	}

	public String villeBac() {
		return getEtudiant().etudVilleBac();
	}

	public IPays toPaysEtabBac() {
		return getEtudiant().toPaysEtabBac();
	}

	public void setToPaysEtabBacRelationship(IPays selectedPays) {
		getEtudiant().setToPaysEtabBacRelationship((EOPays) selectedPays);
	}

	public EOMention toMention() {
		return getEtudiant().toMention();
	}

	public void setToRneEtabSupRelationship(IRne rneEtabSup) {
		getEtudiant().setToRneCodeSupRelationship((EORne) rneEtabSup);
	}

	public void setEtudReimmatriculation(String value) {
		getEtudiant().setEtudReimmatriculation(value);
		
	}

	public String etudReimmatriculation() {
		return getEtudiant().etudReimmatriculation();
	}

	
	
}
