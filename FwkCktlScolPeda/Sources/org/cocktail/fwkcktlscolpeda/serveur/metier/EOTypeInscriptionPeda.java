package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOTypeInscriptionPeda extends _EOTypeInscriptionPeda  implements ITypeInscriptionPeda {
	private static ERXEnterpriseObjectCache<EOTypeInscriptionPeda> typeIpCache;
	
	public static final String CODE_STANDARD = "STANDARD";
	
	 /**
    * @return le cache des {@link EOTypeInscriptionPeda} par leur code
    */
   public static ERXEnterpriseObjectCache<EOTypeInscriptionPeda> getTypeIpCache() {
   	if (typeIpCache == null) {
   		typeIpCache = new ERXEnterpriseObjectCache<EOTypeInscriptionPeda>(EOTypeInscriptionPeda.class, CODE_KEY);
   	}
   	return typeIpCache;
   }
   
   /**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeInscriptionPeda} "STANDARD"
	 */
	public static EOTypeInscriptionPeda typeStandard(EOEditingContext edc) {
		return getTypeIpCache().objectForKey(edc, CODE_STANDARD);
	}
	
	public static void setTypeIpCache(ERXEnterpriseObjectCache<EOTypeInscriptionPeda> typeIpCache) {
		EOTypeInscriptionPeda.typeIpCache = typeIpCache;
	}
   
}
