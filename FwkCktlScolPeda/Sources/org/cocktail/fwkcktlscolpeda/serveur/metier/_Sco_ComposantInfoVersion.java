// DO NOT EDIT.  Make changes to Sco_ComposantInfoVersion.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _Sco_ComposantInfoVersion extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ComposantInfoVersion";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<Integer> VERSION_MAJEURE = new ERXKey<Integer>("versionMajeure");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String VERSION_MAJEURE_KEY = VERSION_MAJEURE.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_Sco_ComposantInfoVersion.class);

  public Sco_ComposantInfoVersion localInstanceIn(EOEditingContext editingContext) {
    Sco_ComposantInfoVersion localInstance = (Sco_ComposantInfoVersion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_Sco_ComposantInfoVersion.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_Sco_ComposantInfoVersion.LOG.isDebugEnabled()) {
    	_Sco_ComposantInfoVersion.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_ComposantInfoVersion.ANNEE_KEY);
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_Sco_ComposantInfoVersion.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_Sco_ComposantInfoVersion.LOG.isDebugEnabled()) {
    	_Sco_ComposantInfoVersion.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_ComposantInfoVersion.COMPOSANT_ID_KEY);
  }

  public Integer versionMajeure() {
    return (Integer) storedValueForKey(_Sco_ComposantInfoVersion.VERSION_MAJEURE_KEY);
  }

  public void setVersionMajeure(Integer value) {
    if (_Sco_ComposantInfoVersion.LOG.isDebugEnabled()) {
    	_Sco_ComposantInfoVersion.LOG.debug( "updating versionMajeure from " + versionMajeure() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_ComposantInfoVersion.VERSION_MAJEURE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_Sco_ComposantInfoVersion.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _Sco_ComposantInfoVersion.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_Sco_ComposantInfoVersion.LOG.isDebugEnabled()) {
      _Sco_ComposantInfoVersion.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _Sco_ComposantInfoVersion.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _Sco_ComposantInfoVersion.COMPOSANT_KEY);
    }
  }
  

  public static Sco_ComposantInfoVersion createSco_ComposantInfoVersion(EOEditingContext editingContext, Integer annee
, Integer composantId
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant) {
    Sco_ComposantInfoVersion eo = (Sco_ComposantInfoVersion) EOUtilities.createAndInsertInstance(editingContext, _Sco_ComposantInfoVersion.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setComposantId(composantId);
    eo.setComposantRelationship(composant);
    return eo;
  }

  public static ERXFetchSpecification<Sco_ComposantInfoVersion> fetchSpec() {
    return new ERXFetchSpecification<Sco_ComposantInfoVersion>(_Sco_ComposantInfoVersion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Sco_ComposantInfoVersion> fetchAllSco_ComposantInfoVersions(EOEditingContext editingContext) {
    return _Sco_ComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext, null);
  }

  public static NSArray<Sco_ComposantInfoVersion> fetchAllSco_ComposantInfoVersions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Sco_ComposantInfoVersion.fetchSco_ComposantInfoVersions(editingContext, null, sortOrderings);
  }

  public static NSArray<Sco_ComposantInfoVersion> fetchSco_ComposantInfoVersions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Sco_ComposantInfoVersion> fetchSpec = new ERXFetchSpecification<Sco_ComposantInfoVersion>(_Sco_ComposantInfoVersion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Sco_ComposantInfoVersion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Sco_ComposantInfoVersion fetchSco_ComposantInfoVersion(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_ComposantInfoVersion.fetchSco_ComposantInfoVersion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_ComposantInfoVersion fetchSco_ComposantInfoVersion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Sco_ComposantInfoVersion> eoObjects = _Sco_ComposantInfoVersion.fetchSco_ComposantInfoVersions(editingContext, qualifier, null);
    Sco_ComposantInfoVersion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ComposantInfoVersion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_ComposantInfoVersion fetchRequiredSco_ComposantInfoVersion(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_ComposantInfoVersion.fetchRequiredSco_ComposantInfoVersion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_ComposantInfoVersion fetchRequiredSco_ComposantInfoVersion(EOEditingContext editingContext, EOQualifier qualifier) {
    Sco_ComposantInfoVersion eoObject = _Sco_ComposantInfoVersion.fetchSco_ComposantInfoVersion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ComposantInfoVersion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_ComposantInfoVersion localInstanceIn(EOEditingContext editingContext, Sco_ComposantInfoVersion eo) {
    Sco_ComposantInfoVersion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
