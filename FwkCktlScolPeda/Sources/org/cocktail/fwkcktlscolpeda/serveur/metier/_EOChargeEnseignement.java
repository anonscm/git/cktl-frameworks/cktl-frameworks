// DO NOT EDIT.  Make changes to EOChargeEnseignement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOChargeEnseignement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ChargeEnseignement";

  // Attribute Keys
  public static final ERXKey<Integer> AP_ID = new ERXKey<Integer>("apId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> NB_GROUPES = new ERXKey<Integer>("nbGroupes");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<java.math.BigDecimal> VALEUR_MINUTES_BDD = new ERXKey<java.math.BigDecimal>("valeurMinutesBdd");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> TO_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("toAP");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLien");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement> TO_TYPE_CHARGE_ENSEIGNEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement>("toTypeChargeEnseignement");

  // Attributes
  public static final String AP_ID_KEY = AP_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String NB_GROUPES_KEY = NB_GROUPES.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String VALEUR_MINUTES_BDD_KEY = VALEUR_MINUTES_BDD.key();
  // Relationships
  public static final String TO_AP_KEY = TO_AP.key();
  public static final String TO_LIEN_KEY = TO_LIEN.key();
  public static final String TO_TYPE_CHARGE_ENSEIGNEMENT_KEY = TO_TYPE_CHARGE_ENSEIGNEMENT.key();

  private static Logger LOG = Logger.getLogger(_EOChargeEnseignement.class);

  public EOChargeEnseignement localInstanceIn(EOEditingContext editingContext) {
    EOChargeEnseignement localInstance = (EOChargeEnseignement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer apId() {
    return (Integer) storedValueForKey(_EOChargeEnseignement.AP_ID_KEY);
  }

  public void setApId(Integer value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating apId from " + apId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.AP_ID_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOChargeEnseignement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOChargeEnseignement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.DATE_MODIFICATION_KEY);
  }

  public Integer nbGroupes() {
    return (Integer) storedValueForKey(_EOChargeEnseignement.NB_GROUPES_KEY);
  }

  public void setNbGroupes(Integer value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating nbGroupes from " + nbGroupes() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.NB_GROUPES_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOChargeEnseignement.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOChargeEnseignement.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.PERS_ID_MODIFICATION_KEY);
  }

  public java.math.BigDecimal valeurMinutesBdd() {
    return (java.math.BigDecimal) storedValueForKey(_EOChargeEnseignement.VALEUR_MINUTES_BDD_KEY);
  }

  public void setValeurMinutesBdd(java.math.BigDecimal value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOChargeEnseignement.LOG.debug( "updating valeurMinutesBdd from " + valeurMinutesBdd() + " to " + value);
    }
    takeStoredValueForKey(value, _EOChargeEnseignement.VALEUR_MINUTES_BDD_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP toAP() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP)storedValueForKey(_EOChargeEnseignement.TO_AP_KEY);
  }
  
  public void setToAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    takeStoredValueForKey(value, _EOChargeEnseignement.TO_AP_KEY);
  }

  public void setToAPRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
      _EOChargeEnseignement.LOG.debug("updating toAP from " + toAP() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAP(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP oldValue = toAP();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOChargeEnseignement.TO_AP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOChargeEnseignement.TO_AP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOChargeEnseignement.TO_LIEN_KEY);
  }
  
  public void setToLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOChargeEnseignement.TO_LIEN_KEY);
  }

  public void setToLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
      _EOChargeEnseignement.LOG.debug("updating toLien from " + toLien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOChargeEnseignement.TO_LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOChargeEnseignement.TO_LIEN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement toTypeChargeEnseignement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement)storedValueForKey(_EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT_KEY);
  }
  
  public void setToTypeChargeEnseignement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement value) {
    takeStoredValueForKey(value, _EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT_KEY);
  }

  public void setToTypeChargeEnseignementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement value) {
    if (_EOChargeEnseignement.LOG.isDebugEnabled()) {
      _EOChargeEnseignement.LOG.debug("updating toTypeChargeEnseignement from " + toTypeChargeEnseignement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeChargeEnseignement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement oldValue = toTypeChargeEnseignement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOChargeEnseignement.TO_TYPE_CHARGE_ENSEIGNEMENT_KEY);
    }
  }
  

  public static EOChargeEnseignement createSco_ChargeEnseignement(EOEditingContext editingContext, NSTimestamp dateCreation
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement toTypeChargeEnseignement) {
    EOChargeEnseignement eo = (EOChargeEnseignement) EOUtilities.createAndInsertInstance(editingContext, _EOChargeEnseignement.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToTypeChargeEnseignementRelationship(toTypeChargeEnseignement);
    return eo;
  }

  public static ERXFetchSpecification<EOChargeEnseignement> fetchSpec() {
    return new ERXFetchSpecification<EOChargeEnseignement>(_EOChargeEnseignement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOChargeEnseignement> fetchAllSco_ChargeEnseignements(EOEditingContext editingContext) {
    return _EOChargeEnseignement.fetchAllSco_ChargeEnseignements(editingContext, null);
  }

  public static NSArray<EOChargeEnseignement> fetchAllSco_ChargeEnseignements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOChargeEnseignement.fetchSco_ChargeEnseignements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOChargeEnseignement> fetchSco_ChargeEnseignements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOChargeEnseignement> fetchSpec = new ERXFetchSpecification<EOChargeEnseignement>(_EOChargeEnseignement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOChargeEnseignement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOChargeEnseignement fetchSco_ChargeEnseignement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChargeEnseignement.fetchSco_ChargeEnseignement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChargeEnseignement fetchSco_ChargeEnseignement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOChargeEnseignement> eoObjects = _EOChargeEnseignement.fetchSco_ChargeEnseignements(editingContext, qualifier, null);
    EOChargeEnseignement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ChargeEnseignement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChargeEnseignement fetchRequiredSco_ChargeEnseignement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOChargeEnseignement.fetchRequiredSco_ChargeEnseignement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOChargeEnseignement fetchRequiredSco_ChargeEnseignement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOChargeEnseignement eoObject = _EOChargeEnseignement.fetchSco_ChargeEnseignement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ChargeEnseignement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOChargeEnseignement localInstanceIn(EOEditingContext editingContext, EOChargeEnseignement eo) {
    EOChargeEnseignement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
