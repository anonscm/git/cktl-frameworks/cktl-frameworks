// DO NOT EDIT.  Make changes to EOTypeLien.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeLien extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeLien";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> NOM = new ERXKey<String>("nom");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> REGLES_LIAISONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>("reglesLiaisons");

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String NOM_KEY = NOM.key();
  // Relationships
  public static final String REGLES_LIAISONS_KEY = REGLES_LIAISONS.key();

  private static Logger LOG = Logger.getLogger(_EOTypeLien.class);

  public EOTypeLien localInstanceIn(EOEditingContext editingContext) {
    EOTypeLien localInstance = (EOTypeLien)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeLien.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeLien.LOG.isDebugEnabled()) {
    	_EOTypeLien.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeLien.ID_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(_EOTypeLien.NOM_KEY);
  }

  public void setNom(String value) {
    if (_EOTypeLien.LOG.isDebugEnabled()) {
    	_EOTypeLien.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeLien.NOM_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisons() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)storedValueForKey(_EOTypeLien.REGLES_LIAISONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisons(EOQualifier qualifier) {
    return reglesLiaisons(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisons(EOQualifier qualifier, boolean fetch) {
    return reglesLiaisons(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> reglesLiaisons(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.TYPE_LIEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.fetchSco_RegleLiaisons(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reglesLiaisons();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReglesLiaisons(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeLien.REGLES_LIAISONS_KEY);
  }

  public void removeFromReglesLiaisons(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeLien.REGLES_LIAISONS_KEY);
  }

  public void addToReglesLiaisonsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeLien.LOG.isDebugEnabled()) {
      _EOTypeLien.LOG.debug("adding " + object + " to reglesLiaisons relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToReglesLiaisons(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeLien.REGLES_LIAISONS_KEY);
    }
  }

  public void removeFromReglesLiaisonsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    if (_EOTypeLien.LOG.isDebugEnabled()) {
      _EOTypeLien.LOG.debug("removing " + object + " from reglesLiaisons relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromReglesLiaisons(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeLien.REGLES_LIAISONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison createReglesLiaisonsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeLien.REGLES_LIAISONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison) eo;
  }

  public void deleteReglesLiaisonsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeLien.REGLES_LIAISONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReglesLiaisonsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegleLiaison> objects = reglesLiaisons().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReglesLiaisonsRelationship(objects.nextElement());
    }
  }


  public static EOTypeLien createSco_TypeLien(EOEditingContext editingContext, Integer id
, String nom
) {
    EOTypeLien eo = (EOTypeLien) EOUtilities.createAndInsertInstance(editingContext, _EOTypeLien.ENTITY_NAME);    
		eo.setId(id);
		eo.setNom(nom);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeLien> fetchSpec() {
    return new ERXFetchSpecification<EOTypeLien>(_EOTypeLien.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeLien> fetchAllSco_TypeLiens(EOEditingContext editingContext) {
    return _EOTypeLien.fetchAllSco_TypeLiens(editingContext, null);
  }

  public static NSArray<EOTypeLien> fetchAllSco_TypeLiens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeLien.fetchSco_TypeLiens(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeLien> fetchSco_TypeLiens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeLien> fetchSpec = new ERXFetchSpecification<EOTypeLien>(_EOTypeLien.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeLien> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeLien fetchSco_TypeLien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeLien.fetchSco_TypeLien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeLien fetchSco_TypeLien(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeLien> eoObjects = _EOTypeLien.fetchSco_TypeLiens(editingContext, qualifier, null);
    EOTypeLien eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeLien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeLien fetchRequiredSco_TypeLien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeLien.fetchRequiredSco_TypeLien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeLien fetchRequiredSco_TypeLien(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeLien eoObject = _EOTypeLien.fetchSco_TypeLien(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeLien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeLien localInstanceIn(EOEditingContext editingContext, EOTypeLien eo) {
    EOTypeLien localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
