package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Enumeration;

import org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEcoleDoctorale;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;

/**
 * Classe metier d'un diplome
 */
public class EODiplome extends _EODiplome implements IDiplome {
	// Private fields
	private static final long serialVersionUID = 1L;

	// Public fields
	public static final String VERSIONDIPLOME_KEY = "versionDiplome";
	public static final ERXKey<EOVersionDiplome> VERSIONDIPLOME = new ERXKey<EOVersionDiplome>(VERSIONDIPLOME_KEY);

	public static final String SISE_STR_KEY = "siseStr";
	public static final ERXKey<String> SISE_STR = new ERXKey<String>(SISE_STR_KEY);

	public static final String SISE_ETAB_STR_KEY = "siseEtablissementStr";
	public static final ERXKey<String> SISE_ETAB_STR = new ERXKey<String>(SISE_ETAB_STR_KEY);

	// Properties
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeDiplome(editingContext());
	}

	/**
	 * @return version texte du code sise
	 */
	public String getSiseStr() {
		if (sise() == null) {
			return "";
		} else {
			return sise().toString();
		}
	}

	/**
	 * @return version texte du code sise etablissement
	 */
	public String getSiseEtablissementStr() {
		if (siseEtablissement() == null) {
			return "";
		} else {
			return siseEtablissement().toString();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void setEcoleDoctoraleRelationship(IEcoleDoctorale uneEcoleDoctorale) {
		super.setEcoleDoctoraleRelationship((EOEcoleDoctorale) uneEcoleDoctorale);
	}
	
	/**
	 * @return true si ce diplome a doit être habilite
	 */
	public Boolean isHabilitationRequise() {
		if (habilitationRequise() == null) {
			return false;
		}
		return (habilitationRequise() == 1);
	}

	/**
	 * {@inheritDoc} Indique si le diplome est utilisable en construction (donc dans l'onglet Modelisation) Il est en construction si il possede au moins a une
	 * version de diplome sur l'annee universitaire passe en parametre et que son statut de formation = "En cours de création" et s'il est en habilitation requise
	 * = 1
	 */
	@Override
	public Boolean isVisibleEnConstruction(Integer annee) {
		if (getVersionDiplome(annee) == null) {
			return false;
		}

		if (statutFormation() != null && statutFormation().equals(EOStatutFormation.statutEnCoursConstruction(editingContext())) && isHabilitationRequise()) {
			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc} Indique si le diplome est utilisable en production (donc dans l'onglet Maquettage) Il est en production s'il possede au moins une version de
	 * diplome sur l'annee universitaire passe en parametre et -> que soit son statut de formation = "En cours de création" et que s'il est en habilitation
	 * requise = 0 -> que soit son statut de formation = "Fictif" (son habilitation requise = 0/1) -> que soit son statut de formation = "En production" (son
	 * habilitation requise = 0/1)
	 */
	@Override
	public Boolean isVisibleEnProduction(Integer annee) {
		if (getVersionDiplome(annee) == null) {
			return false;
		}

		if (statutFormation() != null && statutFormation().equals(EOStatutFormation.statutEnCoursConstruction(editingContext())) && !isHabilitationRequise()) {
			return true;
		}

		if (statutFormation() != null && statutFormation().equals(EOStatutFormation.statutFictive(editingContext()))) {
			return true;
		}

		if (statutFormation() != null && statutFormation().equals(EOStatutFormation.statutEnProduction(editingContext()))) {
			return true;
		}

		return false;

	}

	// Public Methods
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean peutEtresupprime() throws Exception {
		if (inscriptions() != null && inscriptions().size() > 0) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Composant.nePeutEtreSupprime.InscriptionsExistantes");
		}

		if (statutFormation() != null && statutFormation().equals(EOStatutFormation.statutEnProduction(editingContext()))) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Composant.nePeutEtreSupprime.DiplomeEnproduction");
		}
		
		return true;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean avantSuppression() throws Exception {
		try {
			supprimerLesDiplomesIntermediaires();
			supprimerEcoleDoctorale();
			supprimerGradeUniversitaire();
			supprimerHabilitation();
			supprimerMention();
			supprimerNatureFormation();
			supprimerNiveauAccesDiplome();
			supprimerSecteurDisciplinaireSise();
			supprimerSpecialite();
			supprimerStatutFormation();
			supprimerTypeFormation();
			supprimerTypeOrganisation();
			supprimerTypeValidation();
			supprimerVocation();
			supprimerLesPeriodes();
			supprimerLesVersions();

			return true;
				
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * enregistre un diplome et initialise les relations, la version de diplome, les niveaux, les périodes et l'habilitation.
	 * @param annee : annee sur laquelle il faut creer la version du diplome
	 * @return true si l'enregistrement s'est bien passe
	 */
	public boolean enregistrer(Integer annee, Integer numeroMajeur) {
		// créer la version du diplome
		EOVersionDiplome versionDiplome;
		try {
			versionDiplome = EOComposantFactory.createComposant(EOVersionDiplome.class, persIdCreation(), this.editingContext(), annee, numeroMajeur);
			versionDiplome.initialise(this, annee);
			// lier la version du diplome au diplome
			lierVersionAuDiplome(versionDiplome);

			// Inserer le nombre de niveaux et de periodes si besoin
			if (gradeUniversitaire() != null && typeOrganisation() == null) {
				versionDiplome.creerNiveaux(gradeUniversitaire().nbUnitesTemps());
			} else if (gradeUniversitaire() != null && typeOrganisation() != null) { // Insérer le nombre de périodes
				versionDiplome.creerNiveauxEtPeriodes(gradeUniversitaire().nbUnitesTemps(), typeOrganisation().nbPeriodes());
			}

			// créer l'habilitation si nécessaire
			if ((habilitationRequise() != null) && (habilitationRequise() == 1)) {
				// Calendar calendrier = Calendar.getInstance();
				// int annee = calendrier.get(Calendar.YEAR);
				EOHabilitation habilitation = EOHabilitation.creer(this.editingContext(), annee, annee);
				setHabilitationRelationship(habilitation);
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public EODiplome copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			// TODO : récupérer le persId par injection du user
			EODiplome newDiplome = EOComposantFactory.createComposant(EODiplome.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(newDiplome);
			return newDiplome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * cree une nouvelle version de diplome
	 * @param anneeDepart : annee de la version de diplome qui est prise comme référénce
	 * @param anneeCible : annee de la nouvelle version de diplome
	 * @return la nouvelle version de diplome pour l'annee cible
	 */
	public IVersionDiplome versionner(Integer anneeDepart, Integer anneeCible) {
		IVersionDiplome versionDepart = getVersionDiplome(anneeDepart);
		if (versionDepart == null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.impossibleVersionner"));

			return null;
		}

		if (versionDepart.getComposantSuivant() != null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.dejaVersionne"));
			return null;
		}

		EOVersionDiplome nouvelleVersion = versionDepart.dupliquer(anneeCible, versionDepart.composantInfoVersion().versionMajeure());
		nouvelleVersion.initialise(this, anneeCible);
		lierVersionAuDiplome(nouvelleVersion);

		return nouvelleVersion;
	}

	/**
	 * @return IDiplome : le composant suivant associe au composant si il existe
	 */
	public IDiplome getComposantAvecOrigine() {
		if (super.getComposantSuivant() != null) {
			return ((IDiplome) (super.getComposantSuivant()));
		}

		return null;
	}

	/**
	 * @return IDiplome : le composant d'origine
	 */
	public IDiplome getComposantOrigine() {
		return (IDiplome) composantOrigine();
	}

	/**
	 * cree un nouveau diplome et une nouvelle version pour ce dernier diplome
	 * @param anneeReference : annee de la version de diplome qui est prise comme référénce
	 * @param anneeCible : annee de la nouvelle version de diplome
	 * @return nouveau diplome pour habilitation
	 */
	public IDiplome habiliter(Integer anneeReference, Integer anneeCible) {
		// verifier si le diplome dans l'annee cible n'existe pas deja
		if (getComposantAvecOrigine() != null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.dejaHabilite"));

			return null;
		}

		if (getComposantAvecOrigine() != null && getComposantAvecOrigine().getVersionDiplome(anneeCible) != null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.dejaHabilitePourAnneeCible"));

			return null;
		}

		// si le diplome n'a pas deja ete habilite on l'habilite pour l'annee cible
		// -> copie le diplome de l'annee de reference
		// -> on versionne le diplome cree depuis l'annee de reference
		EODiplome nouveauDiplome = this.copy(anneeCible, null);
		nouveauDiplome.setStatutFormationRelationship(EOStatutFormation.statutEnCoursConstruction(editingContext()));
		EOVersionDiplome nouvelleVersion = (this.getVersionDiplome(anneeReference)).dupliquer(anneeCible, null);
		nouvelleVersion.initialise(this, anneeCible);
		nouveauDiplome.lierVersionAuDiplome(nouvelleVersion);

		return nouveauDiplome;

	}

	/**
	 * Passe en production une version de diplome
	 * @param versionDiplomeAHabiliterEnProduction la version en modélisation du diplome que l'on passe en production
	 * @return la version de diplome qui est en production
	 */
	public EOVersionDiplome habiliterPassageEnProduction(EOVersionDiplome versionDiplomeAHabiliterEnProduction) {
		if (versionDiplomeAHabiliterEnProduction == null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.impossibleVersionner"));

			return null;
		}

		if (versionDiplomeAHabiliterEnProduction.getComposantSuivant() != null) {
			System.err.println(ERXLocalizer.defaultLocalizer().localizedStringForKey("Sco_Diplome.dejaVersionne"));
			return null;
		}

		if (versionDiplomeAHabiliterEnProduction.composantInfoVersion().versionMajeure() == null) {
			versionDiplomeAHabiliterEnProduction.composantInfoVersion().setVersionMajeure(0);
		}

		EOVersionDiplome nouvelleVersion = versionDiplomeAHabiliterEnProduction.dupliquer(versionDiplomeAHabiliterEnProduction.composantInfoVersion().annee(),
		    versionDiplomeAHabiliterEnProduction.composantInfoVersion().versionMajeure() + 1);
		nouvelleVersion.initialise(this, versionDiplomeAHabiliterEnProduction.composantInfoVersion().annee());
		lierVersionAuDiplome(nouvelleVersion);

		// Le diplôme passe dans le statut en production
		setStatutFormationRelationship(EOStatutFormation.statutEnProduction(editingContext()));

		return nouvelleVersion;
	}

	/**
	 * redefinit la fonction versionsDiplomes
	 * @return la liste des versions de diplomes dont le diplome est le parent
	 */
	@Override
	public NSArray<IVersionDiplome> versionsDiplomes() {
		NSArray<IVersionDiplome> versionsDiplomes = new NSMutableArray<IVersionDiplome>();

		for (IComposant composant : childs(EOTypeLien.typeVersionner(editingContext()), EOTypeComposant.typeVersionDiplome(editingContext()), null)) {
			EOVersionDiplome vd = (EOVersionDiplome) composant;
			versionsDiplomes.add(vd);
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(versionsDiplomes, EOComposant.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.VERSION_MAJEURE_KEY)
		    .ascs());
	}

	/**
	 * @param annee : annee pour laquelle on veut la version de diplome
	 * @return version de diplome
	 */
	public NSArray<IVersionDiplome> getVersionsDiplome(Integer annee) {
		NSArray<IVersionDiplome> versionsDiplome = new NSMutableArray<IVersionDiplome>();
		for (IVersionDiplome versionDiplome : versionsDiplomes()) {
			if (versionDiplome.composantInfoVersion().annee().equals(annee)) {
				versionsDiplome.add(versionDiplome);
			}
		}

		return versionsDiplome;
	}

	/**
	 * {@inheritDoc}
	 */
	public IVersionDiplome getVersionDiplome(Integer annee) {
		if (getVersionsDiplome(annee).size() > 0) {
			return getVersionsDiplome(annee).lastObject();
		}

		return null;
	}

	/**
	 * @param annee annee pour laquelle on cherche une version de diplome
	 * @return la version de diplome en modelisation pour une annee donnee
	 */
	public IVersionDiplome getVersionDiplomeEnModelisation(Integer annee) {
		for (IVersionDiplome versionDiplome : versionsDiplomes()) {
			if (versionDiplome.composantInfoVersion().versionMajeure() == 0) {
				return versionDiplome;
			}
		}

		return null;
	}

	/**
	 * @param annee pour laquelle on veut la version de diplome
	 * @return le lien parent de la version diplome corresopndant à l'année
	 */
	public ILien getVersionDiplomeAsLien(Integer annee) {
		EOVersionDiplome versionDiplome = (EOVersionDiplome) getVersionDiplome(annee);
		NSArray<EOLien> liens = versionDiplome.liensChilds(EOLien.PARENT.eq(this));
		return !liens.isEmpty() ? liens.lastObject() : null;
	}

	/**
	 * @param annee : année testée
	 * @param anneeDebutHabilitation : année de début d'habilitation
	 * @param anneeFinHabilitation : année de fin d'habilitation
	 * @return vrai si l'année est bien dans la période d'habilitation en tenant compte de la durée du diplome
	 */
	public boolean anneeDansPeriodeHabilitation(Integer annee, Integer anneeDebutHabilitation, Integer anneeFinHabilitation) {
		if (gradeUniversitaire() == null) {
			return false;
		}

		if ((annee >= anneeDebutHabilitation) && (annee < anneeFinHabilitation + gradeUniversitaire().nbUnitesTemps())) {
			return true;
		}
		return false;
	}

	// Private methods
	/**
	 * @param versionDiplome
	 */
	private void lierVersionAuDiplome(EOVersionDiplome versionDiplome) {
		EOLien lien = EOComposantFactory.createLien(this, versionDiplome, EOTypeLien.typeVersionner(this.editingContext()));
		addToLiensParentsRelationship(lien);
		versionDiplome.addToLiensChildsRelationship(lien);
	}

	private void supprimerLesDiplomesIntermediaires() throws Exception {
		this.deleteAllDiplomesIntermediairesRelationships();
	}
	
	private void supprimerEcoleDoctorale() throws Exception {
		if (this.ecoleDoctorale() != null) {
			setEcoleDoctoraleRelationship(null);
		}
	}
	
	private void supprimerGradeUniversitaire() throws Exception {
		if (this.gradeUniversitaire() != null) {
			setGradeUniversitaireRelationship(null);
		}
	}
	
	private void supprimerMention() throws Exception {
		if (this.mention() != null) {
			setMentionRelationship(null);
		}
	}
	
	private void supprimerNatureFormation() throws Exception {
		if (this.natureFormation() != null) {
			setNatureFormationRelationship(null);
		}
	}
	
	private void supprimerNiveauAccesDiplome() throws Exception {
		if (this.niveauAccesDiplome() != null) {
			setNiveauAccesDiplomeRelationship(null);
		}
	}
	
	private void supprimerSecteurDisciplinaireSise() throws Exception {
		if (this.secteurDisciplinaireSise() != null) {
			setSecteurDisciplinaireSiseRelationship(null);
		}
	}
	
	private void supprimerSpecialite() throws Exception {
		if (this.specialite() != null) {
			setSpecialiteRelationship(null);
		}
	}
	
	private void supprimerStatutFormation() throws Exception {
		if (this.statutFormation() != null) {
			setStatutFormationRelationship(null);
		}
	}
	
	private void supprimerTypeFormation() throws Exception {
		if (this.typeFormation() != null) {
			setTypeFormationRelationship(null);
		}
	}
	
	private void supprimerTypeOrganisation() throws Exception {
		if (this.typeOrganisation() != null) {
			setTypeOrganisationRelationship(null);
		}
	}
	
	private void supprimerTypeValidation() throws Exception {
		if (this.typeValidation() != null) {
			setTypeValidationRelationship(null);
		}
	}
	
	private void supprimerVocation() throws Exception {
		if (this.vocation() != null) {
			setVocationRelationship(null);
		}
	}
	
	private void supprimerHabilitation() throws Exception {
		if (this.habilitation() != null) {
			this.habilitation().supprimer();
		}
	}
	
	private void supprimerLesVersions() throws Exception {
		try {
			for (IComposant version : childs(EOTypeComposant.typeVersionDiplome(editingContext()))) {
				((EOVersionDiplome) version).supprimer();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void supprimerLesPeriodes() throws Exception {
		NSArray<EOComposant> periodes = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(allSuccessors(),
		    EOComposant.TYPE_COMPOSANT.eq(EOTypeComposant.typePeriode(editingContext())));
		try {
			for (IComposant periode : periodes) {
				((EOPeriode) periode).supprimer();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void assignTo(EODiplome newDiplome) {
		super.assignTo(newDiplome);
		newDiplome.setDeliberationSemestrielle(this.deliberationSemestrielle());
		newDiplome.setSise(this.sise());
		newDiplome.setSiseEtablissement(this.siseEtablissement());

		newDiplome.setGradeUniversitaireRelationship(this.gradeUniversitaire());
		newDiplome.setHabilitationRelationship(this.habilitation());
		newDiplome.setSpecialiteRelationship(this.specialite());
		newDiplome.setStatutFormationRelationship(this.statutFormation());
		newDiplome.setTypeFormationRelationship(this.typeFormation());
		newDiplome.setTypeOrganisationRelationship(this.typeOrganisation());
		newDiplome.setMentionRelationship(this.mention());
		newDiplome.setSecteurDisciplinaireSiseRelationship(this.secteurDisciplinaireSise());
		newDiplome.setNiveauAccesDiplomeRelationship(this.niveauAccesDiplome());

		newDiplome.setHabilitationRequise(this.habilitationRequise());
	}

	/**
	 * Valide statutFormation
	 * @param statutFormation statut de formation du diplome
	 * @throws ValidationException exception lancée lorsque le diplome n'est pas valide
	 * @return le statut de formation si il a été valide
	 */
	public Object validateStatutFormation(EOStatutFormation statutFormation) throws ValidationException {
		if (statutFormation == null) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Diplome.statutFormation.NullPropertyException");
		}

		return statutFormation;
	}

	/**
	 * Valide gradeUniversitaire
	 * @param gradeUniversitaire : grade universitaire du diplome
	 * @throws ValidationException : exception lancée lorsque le diplome n'est pas valide
	 * @return le grade universitaire si il a été valide
	 */
	public Object validateGradeUniversitaire(EOGradeUniversitaire gradeUniversitaire) throws ValidationException {
		if (gradeUniversitaire == null) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Diplome.gradeUniversitaire.NullPropertyException");
		}

		return gradeUniversitaire;
	}

	/**
	 * Valide typeFormation
	 * @param typeFormation : type de formation du diplome
	 * @throws ValidationException : exception lancée lorsque le diplome n'est pas valide
	 * @return le type de formation si il a été valide
	 */
	public Object validateTypeFormation(EOTypeFormation typeFormation) throws ValidationException {
		if (typeFormation == null) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Diplome.typeFormation.NullPropertyException");
		}
		return typeFormation;
	}

	/**
	 * Valide le secteur disciplinaire Sise
	 * @param siseSecteurDisciplinaire : secteur disciplinaire Sise
	 * @throws ValidationException : exception lancée lorsque le diplome n'est pas valide
	 * @return le secteur disciplinaire Sise si il a été valide
	 */
	public Object validateSecteurDisciplinaireSise(EOSiseSecteurDisciplinaire siseSecteurDisciplinaire) throws ValidationException {
		if (isHabilitationRequise() && siseSecteurDisciplinaire == null) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Diplome.siseSecteurDisciplinaire.NullPropertyException");
		}
		return siseSecteurDisciplinaire;
	}

	/**
	 * Valide le secteur disciplinaire Sise
	 * @param niveauAccesDiplome : secteur disciplinaire Sise
	 * @throws ValidationException : exception lancée lorsque le diplome n'est pas valide
	 * @return le secteur disciplinaire Sise si il a été valide
	 */
	public Object validateNiveauAccesDiplome(EONiveauAccesDiplome niveauAccesDiplome) throws ValidationException {

		if (isHabilitationRequise() && niveauAccesDiplome == null) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Diplome.niveauAccesDiplome.NullPropertyException");
		}
		return niveauAccesDiplome;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isLMD() {
		return gradeUniversitaire() != null && gradeUniversitaire().isLMD();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isLicence() {
		return gradeUniversitaire() != null && gradeUniversitaire().isLicence();
	}

}
