package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;

import com.google.common.collect.ImmutableList;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Gestion des inscriptions pedagogiques
 */
public interface InscriptionPedagogiqueService {

	List<Class<? extends IComposant>> COMPOSANTS_SELECTIONNABLES_CLASSES = ImmutableList.<Class<? extends IComposant>> of(IUE.class, IEC.class, IParcours.class);

	/**
	 * cree les IPs pour une IA
	 * @param editingContext contexte d'edition
	 * @param inscription inscription administrative
	 * @param persId persId de l'utilisateur qui crée lIP
	 * @param typeEtudiant type d'étudiant
	 */
	void creerIpsPourIa(EOEditingContext editingContext, IInscription inscription, Integer persId, ITypeEtudiant typeEtudiant);

	/**
	 * Cree un IPElement pour une IP
	 * @param editingContext contexte d'edition
	 * @param ip inscription pedagogique
	 * @param lien lien surlequel on crée l'élement d'inscription pédagogique
	 * @param type type d'inscription
	 * @param persId persId de l'utilisateur qui crée lIP
	 * @return un élément d'inscription pédagogique
	 */
	IInscriptionPedagogiqueElement creerIpElement(EOEditingContext editingContext, IInscriptionPedagogique ip, ILienConsultation lien, ITypeInscriptionPeda type, Integer persId);

	/**
	 * @param element element d'inscription pedagogique à supprimer
	 */
	void supprimerIpElement(IInscriptionPedagogiqueElement element);
}
