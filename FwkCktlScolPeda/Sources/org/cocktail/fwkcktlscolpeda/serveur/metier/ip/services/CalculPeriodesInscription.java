package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;

/**
 *  Permet de savoir sur quels composants (périodes, version de diplomes) une inscription porte 
 *  à partir des infos de l'inscription administrative
 */
public interface CalculPeriodesInscription {

	/**
	 * @param inscription inscription administratives
	 * @param liensInscription map contenant liens pre-chargés pour éviter de refetcher dans la base
	 * @return les périodes sur lesquelles on doit faire une inscription pédagogique
	 */
	List<ILienConsultation> getPeriodesSemestre(IInscription inscription, Map<String, ILienConsultation> liensInscription);
	
	/**
	 * @param inscription inscription administratives
	 * @param liensInscription map contenant liens pre-chargés pour éviter de refetcher dans la base
	 * @return les périodes sur lesquelles on doit faire une inscription pédagogique
	 */
	List<ILienConsultation> getLienPourIpSecondaires(IInscription inscription, Map<String, ILienConsultation> liensInscription);
	
}
