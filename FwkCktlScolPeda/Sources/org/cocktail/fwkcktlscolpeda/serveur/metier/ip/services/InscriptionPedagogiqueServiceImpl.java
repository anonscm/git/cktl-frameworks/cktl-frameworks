package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.common.service.AbstractServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.AbstractMaquetteProcessorIterativeImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantIterator;

import com.google.common.collect.Lists;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * IInscriptionPedagogique ip
 */
public class InscriptionPedagogiqueServiceImpl extends AbstractServiceImpl implements InscriptionPedagogiqueService {

	private static Logger LOG = Logger.getLogger(InscriptionPedagogiqueServiceImpl.class);

	/**
	 * @param editingContext un ec
	 */
	public InscriptionPedagogiqueServiceImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	/**
     * 
     */
	public InscriptionPedagogiqueServiceImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public void creerIpsPourIa(EOEditingContext editingContext, IInscription inscription, Integer persId, ITypeEtudiant typeEtudiant) {
		Map<String, ILienConsultation> liensInscription = getLiensConsultationsInscription(editingContext, inscription);

		// Création de l'IP principale
		ILienConsultation lienIPPrincipale = liensInscription.get(inscription.idLienConsultationDiplome());
		IInscriptionPedagogique ipPrincipale = creerIP(editingContext, inscription, typeEtudiant, lienIPPrincipale, true);

		// Création des IPs secondaires
		CalculPeriodesInscription calculLiensConsultationInscription =  new CalculPeriodesInscriptionImpl(editingContext);
		List<ILienConsultation> listeLienPourIpSecondaires = calculLiensConsultationInscription.getLienPourIpSecondaires(inscription, liensInscription);
		List<IInscriptionPedagogique> ipSecondaires = new ArrayList<IInscriptionPedagogique>();
		for (ILienConsultation lienConsultation : listeLienPourIpSecondaires) {
			ipSecondaires.add(creerIP(editingContext, inscription, typeEtudiant, lienConsultation, false));
		}

		// Création des IPs elements pour l'IP principale
		for (ILienConsultation lienConsultation : liensInscription.values()) {
			//l'IP élément n'est pas créé sur l'année si il n'y a pas de semestres en dessous
			if (lienConsultation.idLienContexte().equals(inscription.idLienConsultationDiplome()) || !listeLienPourIpSecondaires.contains(lienConsultation)) {
				creerIpElement(editingContext, ipPrincipale, lienConsultation, EOTypeInscriptionPeda.typeStandard(editingContext), persId);
			}
		}

	}

	private Map<String, ILienConsultation> getLiensConsultationsInscription(EOEditingContext editingContext, IInscription inscription) {
		Map<String, ILienConsultation> map = new HashMap<String, ILienConsultation>();

		EOQualifier qualifier = null;
		if (inscription.idLienConsultationDiplome() != null) {
			qualifier = ERXQ.or(qualifier, EOLienConsultation.ID_LIEN_CONTEXTE.eq(inscription.idLienConsultationDiplome()));
		}
		if (inscription.idLienConsultationParcoursDiplome() != null) {
			qualifier = ERXQ.or(qualifier, EOLienConsultation.ID_LIEN_CONTEXTE.eq(inscription.idLienConsultationParcoursDiplome()));
		}
		if (inscription.idLienConsultationAnnee() != null) {
			qualifier = ERXQ.or(qualifier, EOLienConsultation.ID_LIEN_CONTEXTE.eq(inscription.idLienConsultationAnnee()));
		}
		if (inscription.idLienConsultationParcoursAnnee() != null) {
			qualifier = ERXQ.or(qualifier, EOLienConsultation.ID_LIEN_CONTEXTE.eq(inscription.idLienConsultationParcoursAnnee()));
		}

		if (qualifier != null) {
			NSArray<EOLienConsultation> liensConsultation = EOLienConsultation.fetchSco_LienConsultations(editingContext, qualifier, null);
			for (EOLienConsultation lienConsultation : liensConsultation) {
				map.put(lienConsultation.idLienContexte(), lienConsultation);
			}
		}

		return map;
	}

	private IInscriptionPedagogique creerIP(EOEditingContext editingContext, IInscription inscription, ITypeEtudiant typeEtudiant,
	    ILienConsultation lienConsultation, Boolean isPrincipale) {
		IInscriptionPedagogique ip = (IInscriptionPedagogique) EOUtilities.createAndInsertInstance(editingContext, EOInscriptionPedagogique.ENTITY_NAME);
		ip.setToInscriptionRelationship(inscription);
		ip.setToTypeEtudiantRelationship(typeEtudiant);
		ip.setToLienConsultationRelationship(lienConsultation);
		ip.setPrincipale(isPrincipale);
		return ip;
	}

	/**
	 * {@inheritDoc}
	 */
	public IInscriptionPedagogiqueElement creerIpElement(EOEditingContext editingContext, IInscriptionPedagogique ip, ILienConsultation lien, ITypeInscriptionPeda type, Integer persId) {
		EOInscriptionPedagogiqueElement element = (EOInscriptionPedagogiqueElement) ERXEOControlUtilities.createAndInsertObject(editingContext,
		    EOInscriptionPedagogiqueElement.ENTITY_NAME);

		element.setToInscriptionPedagogiqueRelationship((EOInscriptionPedagogique) ip);
		element.setIdLienContexte(lien.idLienContexte());
		element.setToLienConsultationRelationship((EOLienConsultation) lien);
		element.setIdLien(lien.id());
		element.setToTypeInscriptionPedaRelationship((EOTypeInscriptionPeda) type);

		element.setDCreation(new NSTimestamp());
		element.setDModification(new NSTimestamp());

		element.setPersIdCreation(persId);
		element.setPersIdModification(persId);

		((EOInscriptionPedagogique) ip).addToToInscriptionPedagogiqueElementsRelationship(element);

		return element;
	}

	private void parcourirElementPeresIp(IInscriptionPedagogique ip, ILien lien, ILienIpHandler handler) {
		IDiplome diplome = ip.toInscription().toDiplome();
		IVersionDiplome versionDiplome = diplome.getVersionDiplome(ip.toInscription().annee());
		ILien lienVersionDiplome = diplome.getVersionDiplomeAsLien(ip.toInscription().annee());
		List<ILien> tousLiensArbre = Lists.newArrayList();
		tousLiensArbre.addAll(((EOComposant) versionDiplome).allLiens());
		tousLiensArbre.add(lienVersionDiplome);
		ComposantIterator branchIterator = ComposantIterator.branchIterator(tousLiensArbre, lien);
		while (branchIterator.hasNext()) {
			handler.handle(ip, branchIterator.next());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimerIpElement(IInscriptionPedagogiqueElement element) {
		supprimerIpSurElementsPeres(element);
		supprimerIpSurElementsFils(element);
		supprimerIpElementSimple(element);
	}

	private void supprimerIpSurElementsFils(final IInscriptionPedagogiqueElement element) {
		AbstractMaquetteProcessorIterativeImpl processor = new AbstractMaquetteProcessorIterativeImpl(editingContext()) {

			public Boolean shouldProcessChildrenOf(ILien lien) {
				return true;
			}

			public void performActionsOn(ILien lien) {
				IInscriptionPedagogiqueElement elementPere = element;
				if (!lien.equals(element.toLien())) {
					IInscriptionPedagogiqueElement elementFils = recupererIpElement(elementPere.toInscriptionPedagogique(), lien);
					supprimerIpElementSimple(elementFils);
				}
			}
		};
		processor.setRoot(element.toLien());
		processor.setShouldFetch(false);
		processor.proceeed();
	}

	private void supprimerIpSurElementsPeres(IInscriptionPedagogiqueElement element) {
		if (aucuneAutreIpAuMemeNiveau(element)) {
			LOG.debug("Aucun autre Ip au même niveau : supressions des éléments IP ancêtres correspondant au lien " + element.toLien());
			parcourirElementPeresIp(element.toInscriptionPedagogique(), element.toLien(), new ILienIpHandler() {
				public void handle(IInscriptionPedagogique ip, ILien lien) {
					IInscriptionPedagogiqueElement ipElement = recupererIpElement(ip, lien);
					supprimerIpElementSimple(ipElement);
				}
			});
		}
	}

	private boolean aucuneAutreIpAuMemeNiveau(IInscriptionPedagogiqueElement element) {
		boolean resultat = true;
		IComposant parent = element.toLien().parent();
		IInscriptionPedagogique ip = element.toInscriptionPedagogique();
		for (IInscriptionPedagogiqueElement elt : ip.toInscriptionPedagogiqueElements()) {
			if (!elt.equals(element) && elt.toLien().parent().equals(parent)) {
				resultat = false;
				break;
			}
		}
		return resultat;
	}

	private IInscriptionPedagogiqueElement recupererIpElement(IInscriptionPedagogique ip, ILien lien) {
		IInscriptionPedagogiqueElement resultat = null;
		for (IInscriptionPedagogiqueElement element : ip.toInscriptionPedagogiqueElements()) {
			if (element.toLien().equals(lien)) {
				resultat = element;
				break;
			}
		}
		return resultat;
	}

	protected void supprimerIpElementSimple(IInscriptionPedagogiqueElement element) {
		if (element != null) {
			LOG.debug("Suppression de l'IP Element sur le lien " + element.toLien());
			EOInscriptionPedagogiqueElement e = (EOInscriptionPedagogiqueElement) element;

			EOInscriptionPedagogique ip = (EOInscriptionPedagogique) element.toInscriptionPedagogique();

			e.setToInscriptionPedagogiqueRelationship(null);
			ip.removeFromToInscriptionPedagogiqueElementsRelationship(e);

			e.setToLienRelationship(null);
			e.setToTypeInscriptionPedaRelationship(null);

			editingContext().deleteObject(e);
		}
	}

	/**
	 * Simple handler pour traiter les liens d'une IP
	 *
	 */
	public interface ILienIpHandler {
		/**
		 * @param ip l'ip en cours
		 * @param lien le lien de l'ip en cours
		 */
		void handle(IInscriptionPedagogique ip, ILien lien);
	}
}
