package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * {@inheritDoc}
 */
public class CalculPeriodesInscriptionImpl implements CalculPeriodesInscription {
	private EOEditingContext editingContext;

	/**
	 * @param editingContext contexte d'edition
	 */
	public CalculPeriodesInscriptionImpl(EOEditingContext editingContext) {
		setEditingContext(editingContext);
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ILienConsultation> getPeriodesSemestre(IInscription inscription, Map<String, ILienConsultation> liensInscription) {
		ILienConsultation lienAuDessusSemestres = getLienAnneeOuParcoursAnnee(inscription, liensInscription);
		List<ILienConsultation> liens = new ArrayList<ILienConsultation>();
		if (lienAuDessusSemestres != null) {
			liens = getLiensSous(lienAuDessusSemestres, ITypeComposant.TYPEPERIODE_NOM);
		}
		return liens;
	}

	/**
	 * @param inscription inscription
	 * @return le lien juste au dessus des semestres, correspond au parcours année si il existe ou sinon au lien sur l'année
	 */
	private ILienConsultation getLienAnneeOuParcoursAnnee(IInscription inscription, Map<String, ILienConsultation> liensInscription) {
		ILienConsultation lienAuDessusSemestres = liensInscription.get(inscription.idLienConsultationParcoursAnnee());
		if (lienAuDessusSemestres == null) {
			lienAuDessusSemestres = liensInscription.get(inscription.idLienConsultationAnnee());
		}
		return lienAuDessusSemestres;
	}

	/**
	 * @param lienConsultation le lien consultationpour lequel on cherche des élements d'un type de composant donnée
	 * @param typeComposant le type de composant recherché
	 * @return liste des liens consultations qui sont dans l'aroborescence sous lienconsultation et qui ont le type de composant typeComposant
	 */
	List<ILienConsultation> getLiensSous(ILienConsultation lienConsultation, String typeComposant) {
		NSArray<EOLienConsultation> liens = (EOLienConsultation.fetchSco_LienConsultations(getEditingContext(),
		    EOLienConsultation.ID_LIEN_CONTEXTE.startsWith(lienConsultation.idLienContexte() + "/")
		        .and(EOLienConsultation.TYPE_COMPOSANT_NOM_CHILD.eq(typeComposant)), null));

		return new ArrayList<ILienConsultation>(liens);

	}

	/**
	 * {@inheritDoc}
	 */
	public List<ILienConsultation> getLienPourIpSecondaires(IInscription inscription, Map<String, ILienConsultation> liensInscription) {
		List<ILienConsultation> periodesSemestre = getPeriodesSemestre(inscription, liensInscription);
		if (!periodesSemestre.isEmpty()) {
			return periodesSemestre;
		}
		
		ILienConsultation lienAnneeOuParcoursAnnee = getLienAnneeOuParcoursAnnee(inscription, liensInscription);
		if (lienAnneeOuParcoursAnnee != null) {
			return Arrays.asList(lienAnneeOuParcoursAnnee);
		}
		
		ILienConsultation lienParcoursDiplome = liensInscription.get(inscription.idLienConsultationParcoursDiplome());
		if (lienParcoursDiplome != null) {
			return Arrays.asList(lienParcoursDiplome);
		}

		return Arrays.asList(liensInscription.get(inscription.idLienConsultationDiplome()));
	}
}
