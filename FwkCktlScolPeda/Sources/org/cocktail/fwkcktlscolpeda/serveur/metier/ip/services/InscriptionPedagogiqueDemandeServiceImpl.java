package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.common.service.AbstractServiceImpl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXQ;

/**
 * {@inheritDoc}
 */
public class InscriptionPedagogiqueDemandeServiceImpl extends AbstractServiceImpl implements InscriptionPedagogiqueDemandeService {

	/**
	 * @param editingContext contexte d'édition
	 */
	public InscriptionPedagogiqueDemandeServiceImpl(EOEditingContext editingContext) {
		super(editingContext);
	}

	/**
	 * 
	 */
	public InscriptionPedagogiqueDemandeServiceImpl() {
		super();
	}

//	/**
//	 * {@inheritDoc}
//	 */
//	public IDemande getDemandeEtudiant(IEtudiant etudiant) {
//		List<IDemandeWorkflowEtudiant> demandesWorkflowEtudiant = fetchDemandesEtudiant(etudiant);
//		if (demandesWorkflowEtudiant.size() > 0) {
//			IDemandeWorkflowEtudiant demandeWorkflowEtudiant = demandesWorkflowEtudiant.get(0);
//			return demandeWorkflowEtudiant.demande();
//		}
//		return null;
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	public void associerDemandeEtudiant(IEtudiant etudiant, IDemande demande) {
//		EODemandeWorkflowEtudiant demandeWorkflowEtudiant = (EODemandeWorkflowEtudiant) EOUtilities.createAndInsertInstance(editingContext,
//		    EODemandeWorkflowEtudiant.ENTITY_NAME);
//		demandeWorkflowEtudiant.setDemandeRelationship((EODemande) demande);
//		demandeWorkflowEtudiant.setEtudiantRelationship((EOEtudiant) etudiant);
//		//demandeWorkflowEtudiant.setDemandeId(Integer.valueOf(((EODemande) demande).primaryKey()));
//		//demandeWorkflowEtudiant.setEtudiantNumero(((EOEtudiant) etudiant).etudNumero());		
//	}
//
//	List<IDemandeWorkflowEtudiant> fetchDemandesEtudiant(IEtudiant etudiant) {
//		List<IDemandeWorkflowEtudiant> demandesWorkflowEtudiant = new ArrayList<IDemandeWorkflowEtudiant>(
//		    EODemandeWorkflowEtudiant.fetchSco_DemandeWorkflowEtudiants(editingContext,
//		        ERXQ.equals(EODemandeWorkflowEtudiant.ETUDIANT_KEY, etudiant), null));
//		return demandesWorkflowEtudiant;
//	}

}
