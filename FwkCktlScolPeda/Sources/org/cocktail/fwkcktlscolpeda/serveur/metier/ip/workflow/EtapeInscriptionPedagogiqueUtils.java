package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;

import er.extensions.localization.ERXLocalizer;

/**
 * Cette classe représente les utilitaires pour manipuler les étapes des inscriptions pédagogiques.
 * 
 * @author Pascal MACOUIN
 */
public class EtapeInscriptionPedagogiqueUtils {
	/**
	 * @param ip Une inscription pédagogique
	 * @return Le libellé de l'étape affiché à l'utilisateur
	 */
	public static String getLibelleEtape(IInscriptionPedagogique ip) {
		return getLibelleEtape(getCodeEtape(ip));
	}
	
	/**
	 * @param codeEtape Un code étape
	 * @return Le libellé de l'étape affiché à l'utilisateur
	 */
	public static String getLibelleEtape(String codeEtape) {
		if (codeEtape == null) {
			return ERXLocalizer.currentLocalizer().localizedStringForKey("IP.EtatDemande.NonConnecte");
		}
		if (EtapeInscriptionPedagogique.VALID_ETUDIANT.getCodeEtape().equals(codeEtape)) {
			return ERXLocalizer.currentLocalizer().localizedStringForKey("IP.EtatDemande.AttenteValidEtudiant");
		}
		if (EtapeInscriptionPedagogique.VALID_GESTIONNAIRE.getCodeEtape().equals(codeEtape)) {
			return ERXLocalizer.currentLocalizer().localizedStringForKey("IP.EtatDemande.AttenteValidGestionnaire");
		}
		if (EtapeInscriptionPedagogique.VALIDEE.getCodeEtape().equals(codeEtape)) {
			return ERXLocalizer.currentLocalizer().localizedStringForKey("IP.EtatDemande.IPValidee");
		}
		
		return "";
	}
	
	/**
	 * @param ip Une inscription pédagogique
	 * @return La classe CSS pour l'affichage à l'écran
	 */
	public static String getClasseCssColonneEtat(IInscriptionPedagogique ip) {
		return getClasseCssColonneEtat(getCodeEtape(ip));
	}

	/**
	 * @param codeEtape Un code étape
	 * @return La classe CSS pour l'affichage à l'écran
	 */
	public static String getClasseCssColonneEtat(String codeEtape) {
		if (codeEtape == null) {
			return "cktlajaxtableviewCaseEtatNonConnecte";
		}
		if (EtapeInscriptionPedagogique.VALID_ETUDIANT.getCodeEtape().equals(codeEtape)) {
			return "cktlajaxtableviewCaseEtatEnAttenteEtudiant";
		}
		if (EtapeInscriptionPedagogique.VALID_GESTIONNAIRE.getCodeEtape().equals(codeEtape)) {
			return "cktlajaxtableviewCaseEtatEnAttenteGestionnaire";
		}
		if (EtapeInscriptionPedagogique.VALIDEE.getCodeEtape().equals(codeEtape)) {
			return "cktlajaxtableviewEtatValide";
		}
		
		return "";
	}

	private static String getCodeEtape(IInscriptionPedagogique ip) {
		String codeEtape = null;
		
		if (ip != null && ip.toDemande() != null) {
			codeEtape = ip.toDemande().toEtape().codeEtape();
		}
		return codeEtape;
	}
}
