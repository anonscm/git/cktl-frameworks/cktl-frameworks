package org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

/**
 * Gère les demandes au sens Workflow relatives à l'inscription pédagogique d'un étudiant
 */
public interface InscriptionPedagogiqueDemandeService {
  
//	/**
//	 * @param inscriptionPedagogique inscription pedagogique pour lequel on recherche la demande
//	 * @return demande (au sens workflow) associée à l'inscription pedagogique
//	 */
//	IDemande getDemande(IInscriptionPedagogique inscriptionPedagogique);
//
//	/**
//	 * @param etudiant étudiant auquel on associe une demande
//	 * @param demande demande associée à l'étudiant
//	 */
//	void associerDemandeEtudiant(IEtudiant etudiant, IDemande demande);
	
}
