// DO NOT EDIT.  Make changes to EOTypeEchange.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeEchange extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeEchange";

  // Attribute Keys
  public static final ERXKey<String> CODE_ECHANGE = new ERXKey<String>("codeEchange");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> SENS = new ERXKey<String>("sens");
  // Relationship Keys

  // Attributes
  public static final String CODE_ECHANGE_KEY = CODE_ECHANGE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SENS_KEY = SENS.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeEchange.class);

  public EOTypeEchange localInstanceIn(EOEditingContext editingContext) {
    EOTypeEchange localInstance = (EOTypeEchange)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeEchange() {
    return (String) storedValueForKey(_EOTypeEchange.CODE_ECHANGE_KEY);
  }

  public void setCodeEchange(String value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating codeEchange from " + codeEchange() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.CODE_ECHANGE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeEchange.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOTypeEchange.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeEchange.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOTypeEchange.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOTypeEchange.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.PERS_ID_MODIFICATION_KEY);
  }

  public String sens() {
    return (String) storedValueForKey(_EOTypeEchange.SENS_KEY);
  }

  public void setSens(String value) {
    if (_EOTypeEchange.LOG.isDebugEnabled()) {
    	_EOTypeEchange.LOG.debug( "updating sens from " + sens() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeEchange.SENS_KEY);
  }


  public static EOTypeEchange createSco_TypeEchange(EOEditingContext editingContext, String codeEchange
, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
, Integer persIdModification
, String sens
) {
    EOTypeEchange eo = (EOTypeEchange) EOUtilities.createAndInsertInstance(editingContext, _EOTypeEchange.ENTITY_NAME);    
		eo.setCodeEchange(codeEchange);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setSens(sens);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeEchange> fetchSpec() {
    return new ERXFetchSpecification<EOTypeEchange>(_EOTypeEchange.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeEchange> fetchAllSco_TypeEchanges(EOEditingContext editingContext) {
    return _EOTypeEchange.fetchAllSco_TypeEchanges(editingContext, null);
  }

  public static NSArray<EOTypeEchange> fetchAllSco_TypeEchanges(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeEchange.fetchSco_TypeEchanges(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeEchange> fetchSco_TypeEchanges(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeEchange> fetchSpec = new ERXFetchSpecification<EOTypeEchange>(_EOTypeEchange.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeEchange> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeEchange fetchSco_TypeEchange(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEchange.fetchSco_TypeEchange(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEchange fetchSco_TypeEchange(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeEchange> eoObjects = _EOTypeEchange.fetchSco_TypeEchanges(editingContext, qualifier, null);
    EOTypeEchange eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeEchange that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEchange fetchRequiredSco_TypeEchange(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeEchange.fetchRequiredSco_TypeEchange(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeEchange fetchRequiredSco_TypeEchange(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeEchange eoObject = _EOTypeEchange.fetchSco_TypeEchange(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeEchange that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeEchange localInstanceIn(EOEditingContext editingContext, EOTypeEchange eo) {
    EOTypeEchange localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
