package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public interface IBourses {
	
	public EOEditingContext editingContext();

	public void delete();

	Integer annee();

	void setAnnee(Integer value);

	NSTimestamp dCreation();

	void setDCreation(NSTimestamp value);

	NSTimestamp dMofication();

	void setDMofication(NSTimestamp value);

	IEchelonBourse toEchelonBourse();

	void setToEchelonBourseRelationship(IEchelonBourse value);

	String numAllocataire();

	void setNumAllocataire(String value);

	String organisme();

	void setOrganisme(String value);

	Integer persIdCreation();

	void setPersIdCreation(Integer value);

	Integer persIdModification();

	void setPersIdModification(Integer value);

	void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation value);

	ITypeInscriptionFormation toTypeInscriptionFormation();

	Boolean isCampusFrance();

	void setIsCampusFrance(Boolean value);

	ITypeBoursesCF toTypeBoursesCF();

	void setToTypeBoursesCFRelationship(ITypeBoursesCF value);
	
	void setToEtudiantRelationship(IInfosEtudiant value);

}
