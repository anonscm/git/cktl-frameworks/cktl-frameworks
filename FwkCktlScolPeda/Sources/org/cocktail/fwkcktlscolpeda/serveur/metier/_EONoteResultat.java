// DO NOT EDIT.  Make changes to EONoteResultat.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONoteResultat extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_NoteResultat";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONoteResultat.class);

  public EONoteResultat localInstanceIn(EOEditingContext editingContext) {
    EONoteResultat localInstance = (EONoteResultat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EONoteResultat.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EONoteResultat.LOG.isDebugEnabled()) {
    	_EONoteResultat.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EONoteResultat.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EONoteResultat.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EONoteResultat.LOG.isDebugEnabled()) {
    	_EONoteResultat.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EONoteResultat.LIBELLE_KEY);
  }


  public static EONoteResultat createSco_NoteResultat(EOEditingContext editingContext, String code
, String libelle
) {
    EONoteResultat eo = (EONoteResultat) EOUtilities.createAndInsertInstance(editingContext, _EONoteResultat.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EONoteResultat> fetchSpec() {
    return new ERXFetchSpecification<EONoteResultat>(_EONoteResultat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONoteResultat> fetchAllSco_NoteResultats(EOEditingContext editingContext) {
    return _EONoteResultat.fetchAllSco_NoteResultats(editingContext, null);
  }

  public static NSArray<EONoteResultat> fetchAllSco_NoteResultats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONoteResultat.fetchSco_NoteResultats(editingContext, null, sortOrderings);
  }

  public static NSArray<EONoteResultat> fetchSco_NoteResultats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONoteResultat> fetchSpec = new ERXFetchSpecification<EONoteResultat>(_EONoteResultat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONoteResultat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONoteResultat fetchSco_NoteResultat(EOEditingContext editingContext, String keyName, Object value) {
    return _EONoteResultat.fetchSco_NoteResultat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONoteResultat fetchSco_NoteResultat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONoteResultat> eoObjects = _EONoteResultat.fetchSco_NoteResultats(editingContext, qualifier, null);
    EONoteResultat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_NoteResultat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONoteResultat fetchRequiredSco_NoteResultat(EOEditingContext editingContext, String keyName, Object value) {
    return _EONoteResultat.fetchRequiredSco_NoteResultat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONoteResultat fetchRequiredSco_NoteResultat(EOEditingContext editingContext, EOQualifier qualifier) {
    EONoteResultat eoObject = _EONoteResultat.fetchSco_NoteResultat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_NoteResultat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONoteResultat localInstanceIn(EOEditingContext editingContext, EONoteResultat eo) {
    EONoteResultat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
