package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * {@inheritDoc}
 */
public class EONiveauAccesDiplome extends _EONiveauAccesDiplome implements INiveauAccesDiplome {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EONiveauAccesDiplome.class);
	private static ERXEnterpriseObjectCache<EONiveauAccesDiplome> niveauAccesDiplomeCache;

	/**
	 * @return le cache des {@link EONiveauAccesDiplome} par leur type
	 */
	public static ERXEnterpriseObjectCache<EONiveauAccesDiplome> getNiveauAccesDiplomeCache() {
		if (niveauAccesDiplomeCache == null) {
			niveauAccesDiplomeCache = new ERXEnterpriseObjectCache<EONiveauAccesDiplome>(EONiveauAccesDiplome.class, NIVEAU_KEY);
		}
		return niveauAccesDiplomeCache;
	}

	/**
	 * @param niveauAccesDiplomeCache le cache des niveaux d'accès au diplome
	 */
	public static void setTypeFormationCache(ERXEnterpriseObjectCache<EONiveauAccesDiplome> niveauAccesDiplomeCache) {
		EONiveauAccesDiplome.niveauAccesDiplomeCache = niveauAccesDiplomeCache;
	}

	/**
	 * @param edc l'editingContext
	 * @param niveau la valeur du niveau 
	 * @return le {@link EONiveauAccesDiplome} correpondant au niveau
	 */
	public static EONiveauAccesDiplome getNiveauAccesDiplome(EOEditingContext edc, Integer niveau) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, niveau);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} inférieur ou égal au bac
	 */
	public static EONiveauAccesDiplome niveauBac0(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_0);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+1
	 */
	public static EONiveauAccesDiplome niveauBac1(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_1);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+2
	 */
	public static EONiveauAccesDiplome niveauBac2(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_2);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+3
	 */
	public static EONiveauAccesDiplome niveauBac3(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_3);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+4
	 */
	public static EONiveauAccesDiplome niveauBac4(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_4);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+5
	 */
	public static EONiveauAccesDiplome niveauBac5(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_5);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EONiveauAccesDiplome} bac+6 et plus
	 */
	public static EONiveauAccesDiplome niveauBac6(EOEditingContext edc) {
		return getNiveauAccesDiplomeCache().objectForKey(edc, NIVEAU_ACCES_BAC_6);
	}
}
