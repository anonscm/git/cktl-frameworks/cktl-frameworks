package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * l'organisation d'un examen
 * @author isabelle
 *
 */
public class EOExamenOrganisation extends _EOExamenOrganisation implements IExamenOrganisation {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOExamenOrganisation.class);
	
	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToExamenRelationship(IExamen value) {
		super.setToExamenRelationship((EOExamen) value);
		
	}
	
	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId utilisateur qui met à jour l'enregistrement
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
