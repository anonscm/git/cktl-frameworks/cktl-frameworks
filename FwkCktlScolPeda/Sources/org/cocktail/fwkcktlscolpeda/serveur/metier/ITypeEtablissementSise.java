package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface ITypeEtablissementSise {
	
	
	String CODE_SANS = "00";
	String CODE_BTS = "01";
	String CODE_CPGE = "02";
	String CODE_ECOLE_COMMERCE = "03";
	String CODE_ECOLE_INGENIEUR = "04";
	String CODE_ETABLISSEMENT_PRIVE = "05";
	String CODE_ARTISTIQUE = "06";
	String CODE_ETABLISSEMENT_ETRANGER = "10";
	String CODE_ECOLE_NORMALE = "11";
	String CODE_ECOLE_ARCHITECTURE = "13";
	String CODE_AUTRES = "15";
	String CODE_UNIVERSITE = "16";
	String CODE_CORRESPONDANCE = "17";
	String CODE_PARAMEDICAL = "18";
	
	
	
	/**
	 * @return libelle du type de formation
	 */
	String libelle();

	/**
	 * @return code du type de formation
	 */
	String codeSise();


}