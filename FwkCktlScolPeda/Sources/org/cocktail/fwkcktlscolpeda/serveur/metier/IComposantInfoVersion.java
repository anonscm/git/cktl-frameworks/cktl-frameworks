package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Informations de version liées à un composant
 */
public interface IComposantInfoVersion {
	
	 /**
	 * @return annee de la version
	 */
	Integer annee();

	 /**
	 * @param value annee de la version
	 */
	void setAnnee(Integer value);

	 /**
	 * @return numéro majeur de la verion
	 */
	Integer versionMajeure();

	 /**
	 * @param value numéro majeur de la verion
	 */
	void setVersionMajeure(Integer value);


}
