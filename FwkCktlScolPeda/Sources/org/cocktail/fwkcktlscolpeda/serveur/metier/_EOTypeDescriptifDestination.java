// DO NOT EDIT.  Make changes to EOTypeDescriptifDestination.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeDescriptifDestination extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeDescriptifDestination";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeDescriptifDestination.class);

  public EOTypeDescriptifDestination localInstanceIn(EOEditingContext editingContext) {
    EOTypeDescriptifDestination localInstance = (EOTypeDescriptifDestination)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeDescriptifDestination.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeDescriptifDestination.LOG.isDebugEnabled()) {
    	_EOTypeDescriptifDestination.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeDescriptifDestination.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeDescriptifDestination.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeDescriptifDestination.LOG.isDebugEnabled()) {
    	_EOTypeDescriptifDestination.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeDescriptifDestination.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOTypeDescriptifDestination.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOTypeDescriptifDestination.LOG.isDebugEnabled()) {
    	_EOTypeDescriptifDestination.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeDescriptifDestination.READ_ONLY_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOTypeDescriptifDestination.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOTypeDescriptifDestination.LOG.isDebugEnabled()) {
    	_EOTypeDescriptifDestination.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeDescriptifDestination.TYPE_KEY);
  }


  public static EOTypeDescriptifDestination createSco_TypeDescriptifDestination(EOEditingContext editingContext, Integer id
, String libelle
, Integer readOnly
, String type
) {
    EOTypeDescriptifDestination eo = (EOTypeDescriptifDestination) EOUtilities.createAndInsertInstance(editingContext, _EOTypeDescriptifDestination.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
		eo.setType(type);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeDescriptifDestination> fetchSpec() {
    return new ERXFetchSpecification<EOTypeDescriptifDestination>(_EOTypeDescriptifDestination.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeDescriptifDestination> fetchAllSco_TypeDescriptifDestinations(EOEditingContext editingContext) {
    return _EOTypeDescriptifDestination.fetchAllSco_TypeDescriptifDestinations(editingContext, null);
  }

  public static NSArray<EOTypeDescriptifDestination> fetchAllSco_TypeDescriptifDestinations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestinations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeDescriptifDestination> fetchSco_TypeDescriptifDestinations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeDescriptifDestination> fetchSpec = new ERXFetchSpecification<EOTypeDescriptifDestination>(_EOTypeDescriptifDestination.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeDescriptifDestination> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeDescriptifDestination fetchSco_TypeDescriptifDestination(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestination(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeDescriptifDestination fetchSco_TypeDescriptifDestination(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeDescriptifDestination> eoObjects = _EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestinations(editingContext, qualifier, null);
    EOTypeDescriptifDestination eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeDescriptifDestination that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeDescriptifDestination fetchRequiredSco_TypeDescriptifDestination(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeDescriptifDestination.fetchRequiredSco_TypeDescriptifDestination(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeDescriptifDestination fetchRequiredSco_TypeDescriptifDestination(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeDescriptifDestination eoObject = _EOTypeDescriptifDestination.fetchSco_TypeDescriptifDestination(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeDescriptifDestination that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeDescriptifDestination localInstanceIn(EOEditingContext editingContext, EOTypeDescriptifDestination eo) {
    EOTypeDescriptifDestination localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
