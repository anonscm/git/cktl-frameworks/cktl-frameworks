package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * 
 * @author isabelle
 *
 */
public interface IEchelonBourse {
	
	/**
	 * 
	 * @return code
	 */
	String code();

}
