// DO NOT EDIT.  Make changes to EOTypeOrganisation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeOrganisation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeOrganisation";

  // Attribute Keys
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NB_PERIODES = new ERXKey<Integer>("nbPeriodes");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys

  // Attributes
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NB_PERIODES_KEY = NB_PERIODES.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeOrganisation.class);

  public EOTypeOrganisation localInstanceIn(EOEditingContext editingContext) {
    EOTypeOrganisation localInstance = (EOTypeOrganisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeOrganisation.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeOrganisation.LOG.isDebugEnabled()) {
    	_EOTypeOrganisation.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeOrganisation.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeOrganisation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeOrganisation.LOG.isDebugEnabled()) {
    	_EOTypeOrganisation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeOrganisation.LIBELLE_KEY);
  }

  public Integer nbPeriodes() {
    return (Integer) storedValueForKey(_EOTypeOrganisation.NB_PERIODES_KEY);
  }

  public void setNbPeriodes(Integer value) {
    if (_EOTypeOrganisation.LOG.isDebugEnabled()) {
    	_EOTypeOrganisation.LOG.debug( "updating nbPeriodes from " + nbPeriodes() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeOrganisation.NB_PERIODES_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOTypeOrganisation.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOTypeOrganisation.LOG.isDebugEnabled()) {
    	_EOTypeOrganisation.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeOrganisation.TYPE_KEY);
  }


  public static EOTypeOrganisation createSco_TypeOrganisation(EOEditingContext editingContext, Integer id
, String libelle
, Integer nbPeriodes
, String type
) {
    EOTypeOrganisation eo = (EOTypeOrganisation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeOrganisation.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setNbPeriodes(nbPeriodes);
		eo.setType(type);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeOrganisation> fetchSpec() {
    return new ERXFetchSpecification<EOTypeOrganisation>(_EOTypeOrganisation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeOrganisation> fetchAllSco_TypeOrganisations(EOEditingContext editingContext) {
    return _EOTypeOrganisation.fetchAllSco_TypeOrganisations(editingContext, null);
  }

  public static NSArray<EOTypeOrganisation> fetchAllSco_TypeOrganisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeOrganisation.fetchSco_TypeOrganisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeOrganisation> fetchSco_TypeOrganisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeOrganisation> fetchSpec = new ERXFetchSpecification<EOTypeOrganisation>(_EOTypeOrganisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeOrganisation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeOrganisation fetchSco_TypeOrganisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOrganisation.fetchSco_TypeOrganisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOrganisation fetchSco_TypeOrganisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeOrganisation> eoObjects = _EOTypeOrganisation.fetchSco_TypeOrganisations(editingContext, qualifier, null);
    EOTypeOrganisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeOrganisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOrganisation fetchRequiredSco_TypeOrganisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOrganisation.fetchRequiredSco_TypeOrganisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOrganisation fetchRequiredSco_TypeOrganisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeOrganisation eoObject = _EOTypeOrganisation.fetchSco_TypeOrganisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeOrganisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOrganisation localInstanceIn(EOEditingContext editingContext, EOTypeOrganisation eo) {
    EOTypeOrganisation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
