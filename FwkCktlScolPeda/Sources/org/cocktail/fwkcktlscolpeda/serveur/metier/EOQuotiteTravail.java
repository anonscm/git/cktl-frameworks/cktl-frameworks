package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EOQuotiteTravail extends _EOQuotiteTravail {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOQuotiteTravail.class);
	
	/** The Constant QTRA_CODE_SANS_OBJET. */
	public static final String QTRA_CODE_SANS_OBJET = "0";
	
	/** The Constant QTRA_CODE_TEMPS_COMPLET. */
	public static final String QTRA_CODE_TEMPS_COMPLET = "1";
	
	/** The Constant QTRA_CODE_TEMPS_PARTIEL_SUPERIEUR_MI_TEMPS. */
	public static final String QTRA_CODE_TEMPS_PARTIEL_SUPERIEUR_MI_TEMPS = "2";
	
	/** The Constant QTRA_CODE_TEMPS_PARTIEL_INFERIEUR_OU_EGAL_MI_TEMPS. */
	public static final String QTRA_CODE_TEMPS_PARTIEL_INFERIEUR_OU_EGAL_MI_TEMPS = "3";
	
	/** The Constant QTRA_CODE_ACTIVITE_SALARIEE_SUPERIEURE_OU_EGALE_60H. */
	public static final String QTRA_CODE_ACTIVITE_SALARIEE_SUPERIEURE_OU_EGALE_60H = "4";
	
	/** The Constant QTRA_CODE_ACTIVITE_SALARIEE_INFERIEURE_60H. */
	public static final String QTRA_CODE_ACTIVITE_SALARIEE_INFERIEURE_60H = "5";
	
}
