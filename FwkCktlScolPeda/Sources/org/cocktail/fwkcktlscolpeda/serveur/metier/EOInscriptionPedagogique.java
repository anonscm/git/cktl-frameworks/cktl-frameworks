package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogiqueUtils;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;

/**
 * Represente une Inscription pedagogiqe
 */
public class EOInscriptionPedagogique extends _EOInscriptionPedagogique implements IInscriptionPedagogique {
	public static final ERXKey<Integer> ID_INSCRIPTION_PEDA = new ERXKey<Integer>("idInscriptionPeda");
	public static final String ID_INSCRIPTION_PEDA_KEY = ID_INSCRIPTION_PEDA.key();
	
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOInscriptionPedagogique.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public static final ERXKey<String> LIBELLE_ETAT = new ERXKey<String>("libelleEtat");
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToDemandeRelationship(IDemande value) {
		super.setToDemandeRelationship((EODemande) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToInscriptionRelationship(IInscription inscription) {
		super.setToInscriptionRelationship((EOInscription) inscription);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToTypeEtudiantRelationship(ITypeEtudiant typeEtudiant) {
		super.setToTypeEtudiantRelationship((EOTypeEtudiant) typeEtudiant);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String id() {
		return primaryKey();
	}

	public static EOInscriptionPedagogique fetchById(EOEditingContext edc, Integer id) {
		return EOInscriptionPedagogique.fetchSco_InscriptionPedagogique(edc, ID_INSCRIPTION_PEDA.eq(id));
	}
	
	/**
	 * @return the libelleEtat
	 */
	public String libelleEtat() {
		return EtapeInscriptionPedagogiqueUtils.getLibelleEtape(this);
	}

    @Override
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId Utilisateur faisant la création/modification
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean concerneEtudiant(int persIdEtudiant) {
		boolean result = false;
		if (toInscription() != null && toInscription().toEtudiant() != null && toInscription().toEtudiant().toIndividu() != null) {
			result = Integer.valueOf(persIdEtudiant).equals(toInscription().toEtudiant().toIndividu().persId());
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToLienConsultationRelationship(ILienConsultation value) {
		super.setToLienConsultationRelationship((EOLienConsultation) value);
	}

	
}
