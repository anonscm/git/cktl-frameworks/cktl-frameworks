package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ILangue;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CreditableService;
import org.cocktail.fwkgspot.serveur.metier.eof.IImplantationGeo;

/**
 * definit le contrat d'un composant
 */
public interface IComposant {

	
	/**
	 * operations d'initialisation suite à la creation du composant
	 */
	void initialise();

	/**
	 * @param dureeMax vrai si on veut la duree max (lorsu'elle existe)
	 * @return la valeur en heure du composant lorsqu'elle est définie
	 */
	BigDecimal valeurEnHeures(Boolean dureeMax);

	/**
	 * @param <T> : type du composant
	 * @param anneeVersion annee de version du composant copie
	 * @param numeroMajeur numero majeur de version du coposant copie
	 * @return un nouveau composant copie a partir des valeurs du composant
	 */
	<T extends IComposant> T copy(Integer anneeVersion, Integer numeroMajeur);

	/**
	 * @param <T> : type du composant
	 * @param anneeVersion annee de version du composant copie
	 * @param numeroMajeur numero majeur de version du coposant copie
	 * @return duplique le composant et les composants qu sont dessous
	 */
	<T extends IComposant> T dupliquer(Integer anneeVersion, Integer numeroMajeur);

	/**
	 * @param <T> : type du composant
	 * @return le composant dont le composant d'orgineId est egal à id, null sinon
	 */
	<T extends IComposant> T getComposantAvecOrigine();

	/**
	 * @return renvoie la liste des versions de diplomes auxquelles est rattache le composant
	 */
	List<IVersionDiplome> versionsDiplomes();

	/**
	 * Teste si le composant est en construction pour une annee donnee si un composant n'est relié à aucune version de diplome il est en visible en construction
	 * sinon, un composant est en construction si au moins une des version de diplomes auquelles il est lié est rattachée à un diplome qui un statut de formation
	 * en construction.
	 * @param annee : annee sur laquelle le test est effectue.
	 * @return true si le composant est en construction
	 */
	Boolean isVisibleEnConstruction(Integer annee);

	/**
	 * Teste si le composant est en production pour une annee donnee si un composant n'est relié à aucune version de diplome alors il est visible en production Il
	 * est en production si il appartient au moins a une version de diplome sur l'annee universitaire passe en parametre et que cette version de diplome : ne soit
	 * pas habilitable ou si habilitationRequise alors la statut de la formation est "En Production"
	 * @param annee : annee sur lequel le test est effectue.
	 * @return true si le composant est en production
	 */
	Boolean isVisibleEnProduction(Integer annee);

	/**
	 * @return liste des annees pour lesquelles il y a une version de diplomes
	 */
	List<Integer> annees();

	/**
	 * @return le libelle du composant
	 */
	String libelle();
	
	/**
	 * @return le libelle du composant dans un arbre
	 */
	String libelleTreeTable();

	/**
	 * @return le creditable associe au composant
	 */
	ICreditable creditable();

	/**
	 * Renvoie les enfants pour un type de lien et un type de composant donnes.
	 * @param typeLien type de lien recherche.
	 * @param typeComposant type de composant recherche.
	 * @param sortOrderings liste des tris
	 * @return un tableau de composant.
	 */

	/**
	 * @param typeComposant type de composant surlequel on filtre les enfants
	 * @return liste des enfants d'un composant d'un type de composant donné
	 */
	List<IComposant> childs(ITypeComposant typeComposant);

	/**
	 * Renvoie les enfants pour un type de lien donne.
	 * @param typeLien : type de lien recherche.
	 * @return un tableau de composant.
	 */
	List<IComposant> childs(EOTypeLien typeLien);

	/**
	 * renvoie la liste des enfants du composant.
	 * @return liste de composants.
	 */
	List<IComposant> childs();

	/**
	 * Renvoie les parents pour un type de lien et un type de composant donnes.
	 * @param typeLien type de lien recherche.
	 * @param typeComposant type de composant recherche.
	 * @param sortOrderings liste des tris
	 * @return un tableau de composant.
	 */

	/**
	 * @param typeComposant type de composant surlequel on filtre les parents
	 * @return liste des parents d'un composant d'un type de composant donné
	 */
	List<IComposant> parents(ITypeComposant typeComposant);

	/**
	 * Renvoie les parents pour un type de lien donne.
	 * @param typeLien : type de lien recherche.
	 * @return un tableau de composant.
	 */
	List<IComposant> parents(EOTypeLien typeLien);

	/**
	 * renvoie la liste des parents du composant.
	 * @return liste de composants.
	 */
	List<IComposant> parents();

	/**
	 * Les liens pour lequels le composant est enfant
	 * @return liste des liens pour lequels le composant est enfant sans duplication
	 */
	List<? extends ILien> liensChildsWithoutDuplicate();

	/**
	 * Les liens pour lequels le composant est parent
	 * @return liste des liens our lequels le composant est parent sans duplication
	 */
	List<? extends ILien> liensParentsWithoutDuplicate();

	/**
	 * @return liens de type composé pour lesquels le composant est parent
	 */
	List<? extends ILienComposer> liensComposerAvecLesComposantsEnfants();

	
	/**
	 * @return creditable service
	 */
	CreditableService getCreditableService();

	/**
	 * @return type du composant
	 */
	ITypeComposant typeComposant();
	
	/**
	 * @return l'id du type de composant
	 */
	Integer typeComposantId();

	/**
	 * @return code du composant
	 */
	String code();

	/**
	 * @return libelle court du composant
	 */
	String libelleCourt();

	/**
	 * @return nom de l'application (autre que Girofle) qui gere le composant
	 */
	String tagApplication();

	/**
	 * @return liste des structures du composant
	 */
	List<? extends IStructure> structures();

	/**
	 * @param composant à tester
	 * @return true lien le composant est le meme
	 */
	Boolean estIdentique(IComposant composant);

	/**
	 * @return la liste des liens parents
	 */
	List<? extends ILien> liensParents();

	/**
	 * @return l'identifiant technique du composant
	 */
	Integer id();

	/**
	 * renvoie la liste des liens dont le composant enfant est une annee d'un niveau donnee
	 * @param niveau niveau pour lequel on veut récuperer la liste des liens "annees"
	 * @return liste des liens annees d'un niveau donne
	 */
	List<ILien> getLiensAnnees(Integer niveau);

	/**
	 * @param type le type de composant (sous type de IComposant)
	 * @return true si le composant est de type passé en paramètre
	 */
	boolean isTypeOf(Class<? extends IComposant> type);

	/**
	 * @return boolean : indique si le composant est mutualise
	 */
	Boolean isMutualise();

	/**
	 * @return information de version du composant
	 */
	IComposantInfoVersion composantInfoVersion();

	/**
	 * @return Si le composant a été versionné, renvoie sur le nouvelle version du composant
	 */
	IComposant getComposantSuivant();

	/**
	 * Indique si ce composant est la première version pour l'annee passee en parametre
	 * @param annee : annee de l'exercice
	 * @return Boolean : vrai si oui, non dans l'autre cas
	 */
	Boolean isFirstVersion(Integer annee);

	/**
	 * Indique si ce composant est la dernière version pour l'annee passee en parametre
	 * @param annee : annee de l'exercice
	 * @return Boolean : vrai si oui, non dans l'autre cas
	 */
	Boolean isLastVersion(Integer annee);

	/**
	 * @return true si le composant est un période
	 */
	Boolean isPeriode();

	/**
	 * cet accesseur est utilise pour les composants qui n'ont qu'une structure
	 * @return la structure de l'element
	 */
	IStructure getStructure();

	/**
	 * @return sessions attachées au composant
	 */
	List<? extends ISessionComposant> sessions();

	/**
	 * @param noSession le numéro de la session
	 * @return l'objet correspondant à noSession
	 */
	ISessionComposant getSession(Integer noSession);
	
	/**
	 * @param annee l'année pour laquelle on cherche la version de diplome
	 * @return version de diplome pour une annee
	 */
	IVersionDiplome getVersionDiplome(Integer annee);

	/**
	 * @return responsables d'un composant
	 */
	List<? extends IResponsableComposant> responsablesComposant();

	/**
	 * @return descriptif (syllabus) du composant
	 */
	List<? extends IDescriptif> descriptifs();

	/**
	 * @return domaine du composant
	 */
	List<? extends IDomaine> domaines();
	
	
	/**
	 * @return liste des localisations du composant 
	 */
	List<? extends IImplantationGeo>  implantationsGeos();
	
	/**
	 * @return localisation du composant (premier et seul élément de la liste) 
	 */
	IImplantationGeo getImplantationGeo();
	
  /**
   * @return discipline associée au composant
   */
  IDiscipline getDiscipline();
  
  
	/**
	 * @return langue d'enseignement du composant
	 */
	List<? extends ILangue> langues();
	
	/**
	 * @return la section Cnu
	 */
	String cnu();
	
	/**
	 * @return premier element de type AP et de typeAP typeCours
	 */
	IAP getAPCours();
	
	/**
	 * @return la liste de APs de type Cours
	 */
	List<? extends IAP> getAPsCours();
	
	/**
	 * @return premier element de type AP et de typeAP TP
	 */
	IAP getAPTP();
	
	/**
	 * @return la liste de APs de type TP
	 */
	List<? extends IAP> getAPsTP();
	
	/**
	 * @return premier element de type AP et de typeAP TD
	 */
	IAP getAPTD();
	
	/**
	 * @return la liste de APs de type TD
	 */
	List<? extends IAP> getAPsTD();
	
	/**
	 * @return unique AE de type typeCCEcrit si il existe
	 */
	IAE getAECCEcrit();
	
	/**
	 * @return la liste des AEs de type CCEcrit
	 */
	List<? extends IAE> getAEsCCEcrit();
	
	/**
	 * @return le lien composer avec AECCEcrit
	 */
	ILienComposer lienComposerAvecAECCEcrit();
	
	/**
	 * @return somme des coefficients des AE de type typeCCEcrit
	 */
	BigDecimal sommeCoefficientAECCEcrit();
	
	/**
	 * @return unique AE de type typeCCEcrit si il existe
	 */
	IAE getAECCOral();
	
	/**
	 * @return la liste des AEs de type CCOral
	 */
	List<? extends IAE> getAEsCCOral();
	
	/**
	 * @return le lien composer avec AECCOral
	 */
	ILienComposer lienComposerAvecAECCOral();
	
	/**
	 * @return somme des coefficients des AE de type typeCCOral
	 */
	BigDecimal sommeCoefficientAECCOral();
	
	/**
	 * @return unique AE de type typeEXEcrit si il existe
	 */
	IAE getAEEXEcrit();
	
	/**
	 * @return la liste des AEs de type EXEcrit
	 */
	List<? extends IAE> getAEsEXEcrit();
	
	/**
	 * @return lien de type composer entre le composant courant de type typeAE et le composant de type typCCEcrit
	 */
	ILienComposer lienComposerAvecAEEXEcrit();
	
	/**
	 * @return somme des coefficients pour les AEs de type typeEXOral
	 */
	BigDecimal sommeCoefficientAEEXEcrit();
	
	/**
     * @return unique AE de type typeEXOral si il existe
	 */
	IAE getAEEXOral();
	
	/**
	 * @return la liste des AEs de type EXOral
	 */
	List<? extends IAE> getAEsEXOral();
	
	/**
	 * @return lien de type composer entre le composant courant de type typeAE et le composant de type typCCEcrit
	 */
	ILienComposer lienComposerAvecAEEXOral();
	
	/**
     * @return somme des coefficients pour les AEs de type typeEXOral
	 */
	BigDecimal sommeCoefficientAEEXOral();
	
	/**
	 * Renumérote la liste des liens des composants enfants d'un type de composant donné
	 * @param typeComposant type des composants enfants que l'on numérote
	 */
	void renumeroterLienComposerEnfants(ITypeComposant typeComposant);
	
	/**
	 * @return true le composant est archivé
	 */
	Boolean sysArchive();
	
}
