// DO NOT EDIT.  Make changes to EOComposantInfoVersion.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOComposantInfoVersion extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ComposantInfoVersion";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Integer> VERSION_MAJEURE = new ERXKey<Integer>("versionMajeure");
  // Relationship Keys

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String VERSION_MAJEURE_KEY = VERSION_MAJEURE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOComposantInfoVersion.class);

  public EOComposantInfoVersion localInstanceIn(EOEditingContext editingContext) {
    EOComposantInfoVersion localInstance = (EOComposantInfoVersion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOComposantInfoVersion.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOComposantInfoVersion.LOG.isDebugEnabled()) {
    	_EOComposantInfoVersion.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposantInfoVersion.ANNEE_KEY);
  }

  public Integer versionMajeure() {
    return (Integer) storedValueForKey(_EOComposantInfoVersion.VERSION_MAJEURE_KEY);
  }

  public void setVersionMajeure(Integer value) {
    if (_EOComposantInfoVersion.LOG.isDebugEnabled()) {
    	_EOComposantInfoVersion.LOG.debug( "updating versionMajeure from " + versionMajeure() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposantInfoVersion.VERSION_MAJEURE_KEY);
  }


  public static EOComposantInfoVersion createSco_ComposantInfoVersion(EOEditingContext editingContext, Integer annee
) {
    EOComposantInfoVersion eo = (EOComposantInfoVersion) EOUtilities.createAndInsertInstance(editingContext, _EOComposantInfoVersion.ENTITY_NAME);    
		eo.setAnnee(annee);
    return eo;
  }

  public static ERXFetchSpecification<EOComposantInfoVersion> fetchSpec() {
    return new ERXFetchSpecification<EOComposantInfoVersion>(_EOComposantInfoVersion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOComposantInfoVersion> fetchAllSco_ComposantInfoVersions(EOEditingContext editingContext) {
    return _EOComposantInfoVersion.fetchAllSco_ComposantInfoVersions(editingContext, null);
  }

  public static NSArray<EOComposantInfoVersion> fetchAllSco_ComposantInfoVersions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOComposantInfoVersion.fetchSco_ComposantInfoVersions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOComposantInfoVersion> fetchSco_ComposantInfoVersions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOComposantInfoVersion> fetchSpec = new ERXFetchSpecification<EOComposantInfoVersion>(_EOComposantInfoVersion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOComposantInfoVersion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOComposantInfoVersion fetchSco_ComposantInfoVersion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOComposantInfoVersion.fetchSco_ComposantInfoVersion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOComposantInfoVersion fetchSco_ComposantInfoVersion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOComposantInfoVersion> eoObjects = _EOComposantInfoVersion.fetchSco_ComposantInfoVersions(editingContext, qualifier, null);
    EOComposantInfoVersion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ComposantInfoVersion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOComposantInfoVersion fetchRequiredSco_ComposantInfoVersion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOComposantInfoVersion.fetchRequiredSco_ComposantInfoVersion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOComposantInfoVersion fetchRequiredSco_ComposantInfoVersion(EOEditingContext editingContext, EOQualifier qualifier) {
    EOComposantInfoVersion eoObject = _EOComposantInfoVersion.fetchSco_ComposantInfoVersion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ComposantInfoVersion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOComposantInfoVersion localInstanceIn(EOEditingContext editingContext, EOComposantInfoVersion eo) {
    EOComposantInfoVersion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
