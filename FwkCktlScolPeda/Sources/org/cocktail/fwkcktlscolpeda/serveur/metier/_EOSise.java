// DO NOT EDIT.  Make changes to EOSise.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSise extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Sise";

  // Attribute Keys
  public static final ERXKey<String> ACABAC = new ERXKey<String>("acabac");
  public static final ERXKey<String> ACTIVI = new ERXKey<String>("activi");
  public static final ERXKey<String> AMENA = new ERXKey<String>("amena");
  public static final ERXKey<String> ANBAC = new ERXKey<String>("anbac");
  public static final ERXKey<String> ANETAB = new ERXKey<String>("anetab");
  public static final ERXKey<String> ANINSC = new ERXKey<String>("aninsc");
  public static final ERXKey<String> ANNAIS = new ERXKey<String>("annais");
  public static final ERXKey<String> ANSUP = new ERXKey<String>("ansup");
  public static final ERXKey<String> BAC = new ERXKey<String>("bac");
  public static final ERXKey<String> CGE = new ERXKey<String>("cge");
  public static final ERXKey<String> COMETU = new ERXKey<String>("cometu");
  public static final ERXKey<String> COMPOS = new ERXKey<String>("compos");
  public static final ERXKey<String> COMREF = new ERXKey<String>("comref");
  public static final ERXKey<String> CONV = new ERXKey<String>("conv");
  public static final ERXKey<String> CP_ETU = new ERXKey<String>("cpEtu");
  public static final ERXKey<String> CP_PAR = new ERXKey<String>("cpPar");
  public static final ERXKey<String> CURPAR = new ERXKey<String>("curpar");
  public static final ERXKey<String> DEPBAC = new ERXKey<String>("depbac");
  public static final ERXKey<String> DIPDER = new ERXKey<String>("dipder");
  public static final ERXKey<String> DIPLOM = new ERXKey<String>("diplom");
  public static final ERXKey<String> DISPENS = new ERXKey<String>("dispens");
  public static final ERXKey<String> DOM1 = new ERXKey<String>("dom1");
  public static final ERXKey<String> DOM2 = new ERXKey<String>("dom2");
  public static final ERXKey<String> DOM3 = new ERXKey<String>("dom3");
  public static final ERXKey<String> DOM4 = new ERXKey<String>("dom4");
  public static final ERXKey<String> DOM5 = new ERXKey<String>("dom5");
  public static final ERXKey<String> ECHANG = new ERXKey<String>("echang");
  public static final ERXKey<String> FONCTIO = new ERXKey<String>("fonctio");
  public static final ERXKey<String> IDETU = new ERXKey<String>("idetu");
  public static final ERXKey<String> INSPR = new ERXKey<String>("inspr");
  public static final ERXKey<String> JONAIS = new ERXKey<String>("jonais");
  public static final ERXKey<String> LCOM_ETU = new ERXKey<String>("lcomEtu");
  public static final ERXKey<String> L_COM_REF = new ERXKey<String>("lComRef");
  public static final ERXKey<String> MONAIS = new ERXKey<String>("monais");
  public static final ERXKey<String> NATION = new ERXKey<String>("nation");
  public static final ERXKey<String> NIVEAU = new ERXKey<String>("niveau");
  public static final ERXKey<String> NUMED = new ERXKey<String>("numed");
  public static final ERXKey<String> NUMINS = new ERXKey<String>("numins");
  public static final ERXKey<String> PARIPA = new ERXKey<String>("paripa");
  public static final ERXKey<String> PAR_TYPE = new ERXKey<String>("parType");
  public static final ERXKey<String> PAYPAR = new ERXKey<String>("paypar");
  public static final ERXKey<String> PCSPAR = new ERXKey<String>("pcspar");
  public static final ERXKey<String> PCSPAR2 = new ERXKey<String>("pcspar2");
  public static final ERXKey<String> REGIME = new ERXKey<String>("regime");
  public static final ERXKey<String> REIMMA = new ERXKey<String>("reimma");
  public static final ERXKey<String> SEXE = new ERXKey<String>("sexe");
  public static final ERXKey<String> SITUPRE = new ERXKey<String>("situpre");
  public static final ERXKey<String> SPECI_A = new ERXKey<String>("speciA");
  public static final ERXKey<String> SPECI_B = new ERXKey<String>("speciB");
  public static final ERXKey<String> SPECI_C = new ERXKey<String>("speciC");
  public static final ERXKey<String> TELENS = new ERXKey<String>("telens");
  public static final ERXKey<String> TYPREPA = new ERXKey<String>("typrepa");
  // Relationship Keys

  // Attributes
  public static final String ACABAC_KEY = ACABAC.key();
  public static final String ACTIVI_KEY = ACTIVI.key();
  public static final String AMENA_KEY = AMENA.key();
  public static final String ANBAC_KEY = ANBAC.key();
  public static final String ANETAB_KEY = ANETAB.key();
  public static final String ANINSC_KEY = ANINSC.key();
  public static final String ANNAIS_KEY = ANNAIS.key();
  public static final String ANSUP_KEY = ANSUP.key();
  public static final String BAC_KEY = BAC.key();
  public static final String CGE_KEY = CGE.key();
  public static final String COMETU_KEY = COMETU.key();
  public static final String COMPOS_KEY = COMPOS.key();
  public static final String COMREF_KEY = COMREF.key();
  public static final String CONV_KEY = CONV.key();
  public static final String CP_ETU_KEY = CP_ETU.key();
  public static final String CP_PAR_KEY = CP_PAR.key();
  public static final String CURPAR_KEY = CURPAR.key();
  public static final String DEPBAC_KEY = DEPBAC.key();
  public static final String DIPDER_KEY = DIPDER.key();
  public static final String DIPLOM_KEY = DIPLOM.key();
  public static final String DISPENS_KEY = DISPENS.key();
  public static final String DOM1_KEY = DOM1.key();
  public static final String DOM2_KEY = DOM2.key();
  public static final String DOM3_KEY = DOM3.key();
  public static final String DOM4_KEY = DOM4.key();
  public static final String DOM5_KEY = DOM5.key();
  public static final String ECHANG_KEY = ECHANG.key();
  public static final String FONCTIO_KEY = FONCTIO.key();
  public static final String IDETU_KEY = IDETU.key();
  public static final String INSPR_KEY = INSPR.key();
  public static final String JONAIS_KEY = JONAIS.key();
  public static final String LCOM_ETU_KEY = LCOM_ETU.key();
  public static final String L_COM_REF_KEY = L_COM_REF.key();
  public static final String MONAIS_KEY = MONAIS.key();
  public static final String NATION_KEY = NATION.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String NUMED_KEY = NUMED.key();
  public static final String NUMINS_KEY = NUMINS.key();
  public static final String PARIPA_KEY = PARIPA.key();
  public static final String PAR_TYPE_KEY = PAR_TYPE.key();
  public static final String PAYPAR_KEY = PAYPAR.key();
  public static final String PCSPAR_KEY = PCSPAR.key();
  public static final String PCSPAR2_KEY = PCSPAR2.key();
  public static final String REGIME_KEY = REGIME.key();
  public static final String REIMMA_KEY = REIMMA.key();
  public static final String SEXE_KEY = SEXE.key();
  public static final String SITUPRE_KEY = SITUPRE.key();
  public static final String SPECI_A_KEY = SPECI_A.key();
  public static final String SPECI_B_KEY = SPECI_B.key();
  public static final String SPECI_C_KEY = SPECI_C.key();
  public static final String TELENS_KEY = TELENS.key();
  public static final String TYPREPA_KEY = TYPREPA.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSise.class);

  public EOSise localInstanceIn(EOEditingContext editingContext) {
    EOSise localInstance = (EOSise)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String acabac() {
    return (String) storedValueForKey(_EOSise.ACABAC_KEY);
  }

  public void setAcabac(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating acabac from " + acabac() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ACABAC_KEY);
  }

  public String activi() {
    return (String) storedValueForKey(_EOSise.ACTIVI_KEY);
  }

  public void setActivi(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating activi from " + activi() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ACTIVI_KEY);
  }

  public String amena() {
    return (String) storedValueForKey(_EOSise.AMENA_KEY);
  }

  public void setAmena(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating amena from " + amena() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.AMENA_KEY);
  }

  public String anbac() {
    return (String) storedValueForKey(_EOSise.ANBAC_KEY);
  }

  public void setAnbac(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating anbac from " + anbac() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ANBAC_KEY);
  }

  public String anetab() {
    return (String) storedValueForKey(_EOSise.ANETAB_KEY);
  }

  public void setAnetab(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating anetab from " + anetab() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ANETAB_KEY);
  }

  public String aninsc() {
    return (String) storedValueForKey(_EOSise.ANINSC_KEY);
  }

  public void setAninsc(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating aninsc from " + aninsc() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ANINSC_KEY);
  }

  public String annais() {
    return (String) storedValueForKey(_EOSise.ANNAIS_KEY);
  }

  public void setAnnais(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating annais from " + annais() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ANNAIS_KEY);
  }

  public String ansup() {
    return (String) storedValueForKey(_EOSise.ANSUP_KEY);
  }

  public void setAnsup(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating ansup from " + ansup() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ANSUP_KEY);
  }

  public String bac() {
    return (String) storedValueForKey(_EOSise.BAC_KEY);
  }

  public void setBac(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating bac from " + bac() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.BAC_KEY);
  }

  public String cge() {
    return (String) storedValueForKey(_EOSise.CGE_KEY);
  }

  public void setCge(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating cge from " + cge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.CGE_KEY);
  }

  public String cometu() {
    return (String) storedValueForKey(_EOSise.COMETU_KEY);
  }

  public void setCometu(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating cometu from " + cometu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.COMETU_KEY);
  }

  public String compos() {
    return (String) storedValueForKey(_EOSise.COMPOS_KEY);
  }

  public void setCompos(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating compos from " + compos() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.COMPOS_KEY);
  }

  public String comref() {
    return (String) storedValueForKey(_EOSise.COMREF_KEY);
  }

  public void setComref(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating comref from " + comref() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.COMREF_KEY);
  }

  public String conv() {
    return (String) storedValueForKey(_EOSise.CONV_KEY);
  }

  public void setConv(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating conv from " + conv() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.CONV_KEY);
  }

  public String cpEtu() {
    return (String) storedValueForKey(_EOSise.CP_ETU_KEY);
  }

  public void setCpEtu(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating cpEtu from " + cpEtu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.CP_ETU_KEY);
  }

  public String cpPar() {
    return (String) storedValueForKey(_EOSise.CP_PAR_KEY);
  }

  public void setCpPar(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating cpPar from " + cpPar() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.CP_PAR_KEY);
  }

  public String curpar() {
    return (String) storedValueForKey(_EOSise.CURPAR_KEY);
  }

  public void setCurpar(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating curpar from " + curpar() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.CURPAR_KEY);
  }

  public String depbac() {
    return (String) storedValueForKey(_EOSise.DEPBAC_KEY);
  }

  public void setDepbac(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating depbac from " + depbac() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DEPBAC_KEY);
  }

  public String dipder() {
    return (String) storedValueForKey(_EOSise.DIPDER_KEY);
  }

  public void setDipder(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dipder from " + dipder() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DIPDER_KEY);
  }

  public String diplom() {
    return (String) storedValueForKey(_EOSise.DIPLOM_KEY);
  }

  public void setDiplom(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating diplom from " + diplom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DIPLOM_KEY);
  }

  public String dispens() {
    return (String) storedValueForKey(_EOSise.DISPENS_KEY);
  }

  public void setDispens(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dispens from " + dispens() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DISPENS_KEY);
  }

  public String dom1() {
    return (String) storedValueForKey(_EOSise.DOM1_KEY);
  }

  public void setDom1(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dom1 from " + dom1() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DOM1_KEY);
  }

  public String dom2() {
    return (String) storedValueForKey(_EOSise.DOM2_KEY);
  }

  public void setDom2(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dom2 from " + dom2() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DOM2_KEY);
  }

  public String dom3() {
    return (String) storedValueForKey(_EOSise.DOM3_KEY);
  }

  public void setDom3(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dom3 from " + dom3() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DOM3_KEY);
  }

  public String dom4() {
    return (String) storedValueForKey(_EOSise.DOM4_KEY);
  }

  public void setDom4(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dom4 from " + dom4() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DOM4_KEY);
  }

  public String dom5() {
    return (String) storedValueForKey(_EOSise.DOM5_KEY);
  }

  public void setDom5(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating dom5 from " + dom5() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.DOM5_KEY);
  }

  public String echang() {
    return (String) storedValueForKey(_EOSise.ECHANG_KEY);
  }

  public void setEchang(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating echang from " + echang() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.ECHANG_KEY);
  }

  public String fonctio() {
    return (String) storedValueForKey(_EOSise.FONCTIO_KEY);
  }

  public void setFonctio(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating fonctio from " + fonctio() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.FONCTIO_KEY);
  }

  public String idetu() {
    return (String) storedValueForKey(_EOSise.IDETU_KEY);
  }

  public void setIdetu(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating idetu from " + idetu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.IDETU_KEY);
  }

  public String inspr() {
    return (String) storedValueForKey(_EOSise.INSPR_KEY);
  }

  public void setInspr(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating inspr from " + inspr() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.INSPR_KEY);
  }

  public String jonais() {
    return (String) storedValueForKey(_EOSise.JONAIS_KEY);
  }

  public void setJonais(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating jonais from " + jonais() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.JONAIS_KEY);
  }

  public String lcomEtu() {
    return (String) storedValueForKey(_EOSise.LCOM_ETU_KEY);
  }

  public void setLcomEtu(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating lcomEtu from " + lcomEtu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.LCOM_ETU_KEY);
  }

  public String lComRef() {
    return (String) storedValueForKey(_EOSise.L_COM_REF_KEY);
  }

  public void setLComRef(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating lComRef from " + lComRef() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.L_COM_REF_KEY);
  }

  public String monais() {
    return (String) storedValueForKey(_EOSise.MONAIS_KEY);
  }

  public void setMonais(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating monais from " + monais() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.MONAIS_KEY);
  }

  public String nation() {
    return (String) storedValueForKey(_EOSise.NATION_KEY);
  }

  public void setNation(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating nation from " + nation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.NATION_KEY);
  }

  public String niveau() {
    return (String) storedValueForKey(_EOSise.NIVEAU_KEY);
  }

  public void setNiveau(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.NIVEAU_KEY);
  }

  public String numed() {
    return (String) storedValueForKey(_EOSise.NUMED_KEY);
  }

  public void setNumed(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating numed from " + numed() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.NUMED_KEY);
  }

  public String numins() {
    return (String) storedValueForKey(_EOSise.NUMINS_KEY);
  }

  public void setNumins(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating numins from " + numins() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.NUMINS_KEY);
  }

  public String paripa() {
    return (String) storedValueForKey(_EOSise.PARIPA_KEY);
  }

  public void setParipa(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating paripa from " + paripa() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.PARIPA_KEY);
  }

  public String parType() {
    return (String) storedValueForKey(_EOSise.PAR_TYPE_KEY);
  }

  public void setParType(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating parType from " + parType() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.PAR_TYPE_KEY);
  }

  public String paypar() {
    return (String) storedValueForKey(_EOSise.PAYPAR_KEY);
  }

  public void setPaypar(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating paypar from " + paypar() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.PAYPAR_KEY);
  }

  public String pcspar() {
    return (String) storedValueForKey(_EOSise.PCSPAR_KEY);
  }

  public void setPcspar(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating pcspar from " + pcspar() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.PCSPAR_KEY);
  }

  public String pcspar2() {
    return (String) storedValueForKey(_EOSise.PCSPAR2_KEY);
  }

  public void setPcspar2(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating pcspar2 from " + pcspar2() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.PCSPAR2_KEY);
  }

  public String regime() {
    return (String) storedValueForKey(_EOSise.REGIME_KEY);
  }

  public void setRegime(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating regime from " + regime() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.REGIME_KEY);
  }

  public String reimma() {
    return (String) storedValueForKey(_EOSise.REIMMA_KEY);
  }

  public void setReimma(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating reimma from " + reimma() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.REIMMA_KEY);
  }

  public String sexe() {
    return (String) storedValueForKey(_EOSise.SEXE_KEY);
  }

  public void setSexe(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating sexe from " + sexe() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.SEXE_KEY);
  }

  public String situpre() {
    return (String) storedValueForKey(_EOSise.SITUPRE_KEY);
  }

  public void setSitupre(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating situpre from " + situpre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.SITUPRE_KEY);
  }

  public String speciA() {
    return (String) storedValueForKey(_EOSise.SPECI_A_KEY);
  }

  public void setSpeciA(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating speciA from " + speciA() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.SPECI_A_KEY);
  }

  public String speciB() {
    return (String) storedValueForKey(_EOSise.SPECI_B_KEY);
  }

  public void setSpeciB(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating speciB from " + speciB() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.SPECI_B_KEY);
  }

  public String speciC() {
    return (String) storedValueForKey(_EOSise.SPECI_C_KEY);
  }

  public void setSpeciC(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating speciC from " + speciC() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.SPECI_C_KEY);
  }

  public String telens() {
    return (String) storedValueForKey(_EOSise.TELENS_KEY);
  }

  public void setTelens(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating telens from " + telens() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.TELENS_KEY);
  }

  public String typrepa() {
    return (String) storedValueForKey(_EOSise.TYPREPA_KEY);
  }

  public void setTyprepa(String value) {
    if (_EOSise.LOG.isDebugEnabled()) {
    	_EOSise.LOG.debug( "updating typrepa from " + typrepa() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSise.TYPREPA_KEY);
  }


  public static EOSise createSco_Sise(EOEditingContext editingContext) {
    EOSise eo = (EOSise) EOUtilities.createAndInsertInstance(editingContext, _EOSise.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<EOSise> fetchSpec() {
    return new ERXFetchSpecification<EOSise>(_EOSise.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSise> fetchAllSco_Sises(EOEditingContext editingContext) {
    return _EOSise.fetchAllSco_Sises(editingContext, null);
  }

  public static NSArray<EOSise> fetchAllSco_Sises(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSise.fetchSco_Sises(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSise> fetchSco_Sises(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSise> fetchSpec = new ERXFetchSpecification<EOSise>(_EOSise.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSise> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSise fetchSco_Sise(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSise.fetchSco_Sise(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSise fetchSco_Sise(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSise> eoObjects = _EOSise.fetchSco_Sises(editingContext, qualifier, null);
    EOSise eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Sise that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSise fetchRequiredSco_Sise(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSise.fetchRequiredSco_Sise(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSise fetchRequiredSco_Sise(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSise eoObject = _EOSise.fetchSco_Sise(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Sise that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSise localInstanceIn(EOEditingContext editingContext, EOSise eo) {
    EOSise localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
