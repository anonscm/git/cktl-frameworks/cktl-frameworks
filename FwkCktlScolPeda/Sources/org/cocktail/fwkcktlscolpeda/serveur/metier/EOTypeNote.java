package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * 
 * @author isabelle
 *
 */
public class EOTypeNote extends _EOTypeNote implements ITypeNote {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeNote.class);
	
		
	private static ERXEnterpriseObjectCache<EOTypeNote> typeNoteCache;

	/**
	 * @return le cache des {@link EOTypeNote} par leur code
	 */
	private static ERXEnterpriseObjectCache<EOTypeNote> getTypeNoteCache() {
		if (typeNoteCache == null) {
			typeNoteCache = new ERXEnterpriseObjectCache<EOTypeNote>(EOTypeNote.class, CODE_KEY);
		}
		return typeNoteCache;
	}

	/**
	 * @param typeNoteCache
	 *            le cache a setter
	 */
	public static void setTypeNoteCache(ERXEnterpriseObjectCache<EOTypeNote> typeNoteCache) {
		EOTypeNote.typeNoteCache = typeNoteCache;
	}

	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type noteSaisie
	 */
	public static EOTypeNote typeSaisie(EOEditingContext edc) {
		return getTypeNoteCache().objectForKey(edc, NOTE_SAISIE);
	}
	
	/**
	 * @param edc
	 *            : contexte d'édition
	 * @return le type noteSaisie
	 */
	public static EOTypeNote typeCalculee(EOEditingContext edc) {
		return getTypeNoteCache().objectForKey(edc, NOTE_CALCULEE);
	}

	public boolean isCalculee() {
		return NOTE_CALCULEE.equals(code());
	}

	public boolean isSaisie() {
		return NOTE_SAISIE.equals(code());
	}

}
