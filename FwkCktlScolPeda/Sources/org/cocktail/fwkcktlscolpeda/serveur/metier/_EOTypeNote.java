// DO NOT EDIT.  Make changes to EOTypeNote.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeNote extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeNote";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeNote.class);

  public EOTypeNote localInstanceIn(EOEditingContext editingContext) {
    EOTypeNote localInstance = (EOTypeNote)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeNote.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeNote.LOG.isDebugEnabled()) {
    	_EOTypeNote.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeNote.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeNote.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeNote.LOG.isDebugEnabled()) {
    	_EOTypeNote.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeNote.LIBELLE_KEY);
  }


  public static EOTypeNote createSco_TypeNote(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeNote eo = (EOTypeNote) EOUtilities.createAndInsertInstance(editingContext, _EOTypeNote.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeNote> fetchSpec() {
    return new ERXFetchSpecification<EOTypeNote>(_EOTypeNote.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeNote> fetchAllSco_TypeNotes(EOEditingContext editingContext) {
    return _EOTypeNote.fetchAllSco_TypeNotes(editingContext, null);
  }

  public static NSArray<EOTypeNote> fetchAllSco_TypeNotes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeNote.fetchSco_TypeNotes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeNote> fetchSco_TypeNotes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeNote> fetchSpec = new ERXFetchSpecification<EOTypeNote>(_EOTypeNote.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeNote> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeNote fetchSco_TypeNote(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeNote.fetchSco_TypeNote(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeNote fetchSco_TypeNote(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeNote> eoObjects = _EOTypeNote.fetchSco_TypeNotes(editingContext, qualifier, null);
    EOTypeNote eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeNote that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeNote fetchRequiredSco_TypeNote(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeNote.fetchRequiredSco_TypeNote(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeNote fetchRequiredSco_TypeNote(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeNote eoObject = _EOTypeNote.fetchSco_TypeNote(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeNote that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeNote localInstanceIn(EOEditingContext editingContext, EOTypeNote eo) {
    EOTypeNote localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
