package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * {@inheritDoc}
 */
public class EOSpecialite extends _EOSpecialite implements ISpecialite {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOSpecialite.class);
	
	@Override
	public String toString() {
		return this.libelle();
	}
	
}
