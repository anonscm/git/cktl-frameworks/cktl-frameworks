package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;


/**
 * Cette strategie definit un perimetre sur les composants d'une formation dont l'utilisateur est responsable.
 */
public class PerimetreStrategieAccesElementsFormationUserResp implements IPerimetreStrategie {
	
	/**
	 * Constructeur de la strategie d'acces au composant dont l'utilisateur est responsable.
	 */
	public PerimetreStrategieAccesElementsFormationUserResp() {

	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}
	
}
