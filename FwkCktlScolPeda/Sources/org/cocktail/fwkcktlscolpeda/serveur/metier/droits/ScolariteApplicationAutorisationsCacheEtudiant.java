package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

/**
 * Définition des autorisations pour les applications de la sphère scolarité avec des droits par défaut en tant qu'étudiant.
 */
public class ScolariteApplicationAutorisationsCacheEtudiant extends ScolariteApplicationAutorisationsCache {

	/**
	 * Constructeur.
	 * @param appStrId identifiant de l'application
	 * @param persId identifiant de l'utilisateur
	 */
	public ScolariteApplicationAutorisationsCacheEtudiant(String appStrId, Integer persId) {
		super(appStrId, persId);
	}

	/*****************************/
	/********* CORIANDRE *********/
	/*****************************/
	
	/**
	 * Droit d'utiliser Coriandre.
	 * @return true si l'utilisateur a le droit d'utiliser Coriandre
	 */
	public boolean hasDroitUtilisationCoriandre() {
		return true;
	}
	
	/**
	 * Droit de connaitre Coriandre.
	 * @return true si l'utilisateur a le droit de connaitre Coriandre
	 */
	public boolean hasDroitConnaissanceCoriandre() {
		return true;
	}

	/**
	 * Droit d'utiliser la page IA Web/Re-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la re-inscription administrative par le web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemReInscrip() {
		return true;
	}

	/**
	 * Droit de connaitre la page IA Web/Re-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la Re-inscription administrative par le web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemReInscrip() {
		return true;
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Rapport d'inscription.
	 * @return true si l'utilisateur a le droit d'utiliser le rapport d'inscription
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemRapportInscrip() {
		return true;
	}

	/**
	 * Droit de connaitre la page IA Web/Rapport d'inscription.
	 * @return true si l'utilisateur a le droit de connaitre le rapport d'inscription
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemRapportInscrip() {
		return true;
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Prendre RDV.
	 * @return true si l'utilisateur a le droit d'utiliser la prise de rendez-vous
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemPrendreRDV() {
		return true;
	}

	/**
	 * Droit de connaitre la page IA Web/Prendre RDV.
	 * @return true si l'utilisateur a le droit de connaitre la prise de rendez-vous
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemPrendreRDV() {
		return true;
	}
	
	/**
	 * Droit d'utiliser la page IA Web/IA Validees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des IAs Validees par le web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemIaValidees() {
		return true;
	}

	/**
	 * Droit de connaitre la page IA Web/IA Validees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des IAs Validees par le web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemIaValidees() {
		return true;
	}
}
