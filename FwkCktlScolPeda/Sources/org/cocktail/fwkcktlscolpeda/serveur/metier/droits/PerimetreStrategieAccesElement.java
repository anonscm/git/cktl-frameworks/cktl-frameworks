package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.scol.maquette.ComposantRead;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;
import com.webobjects.eocontrol.EOQualifier;


/**
 * Cette strategie definit un perimetre de donnees sur un composant de la maquette de formation.
 */
public class PerimetreStrategieAccesElement implements IPerimetreStrategie {

	private EOComposant composant;

	private QComposant qComposant = QComposant.composant;

	/**
	 * Constructeur de la strategie auquel on passe un composant.
	 * @param composant element de la maquette sur lequel le perimetre est defini
	 */
	public PerimetreStrategieAccesElement(EOComposant composant) {
		this.composant = composant;
	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		return EOComposant.ID.eq(composant.id());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		SQLQuery query = template.newSqlQuery()
				.from(qComposant)
				.where(getPredicate())
				.orderBy(qComposant.code.asc())
				.distinct();
		ConstructorExpression<ComposantRead> mapping = ConstructorExpression.create(ComposantRead.class, qComposant.idComposant);
		
		List<ComposantRead> res = template.query(query, mapping);
		
		return res;

	}
	
	/**
	 * {@inheritDoc}
	 */
	private Predicate getPredicate() {
		Predicate predicat = qComposant.idComposant.eq(Long.valueOf(composant.id()));
		
		return predicat;
	}

}
