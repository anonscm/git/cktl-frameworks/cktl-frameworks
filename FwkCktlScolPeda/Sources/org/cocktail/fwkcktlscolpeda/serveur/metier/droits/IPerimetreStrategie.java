package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Cette interface definit une strategie de construction des perimetres.
 */
public interface IPerimetreStrategie {

	/**
	 * Qualifier correspondant à la strategie.
	 * @return un qualifier construit avec la strategie courante
	 */
	EOQualifier getQualifier();
	
	/**
	 * Liste des composants correspondant à la strategie.
	 * @param template modèle du schéma
	 * @return une liste List<ComposantRead> construit avec la strategie courante
	 */
	List<ComposantRead> getComposants(QueryDslJdbcTemplate template);
	
}
