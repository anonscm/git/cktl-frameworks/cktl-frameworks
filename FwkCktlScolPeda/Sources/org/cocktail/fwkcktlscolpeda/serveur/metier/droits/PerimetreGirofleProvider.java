package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Cette classe fournit des perimetres de donnees pour l'application Girofle.
 */
public class PerimetreGirofleProvider implements IPerimetreProvider<EOGdPerimetreGirofle> {
	private List<EOGdPerimetreGirofle> perimetres;

	// Private fields

	// Properties
	private IPerimetreStrategie getPerimetreStrategie(EOGdPerimetreGirofle perimetre) {
		return PerimetreStrategieFactory.createStrategy(perimetre);
	}

	public List<EOGdPerimetreGirofle> getPerimetres() {
		return perimetres;
	}

	/**
	 * Renseigne la liste des perimetres fournis au PerimetreGirofleProvider
	 * @param perimetres perimetres fournis au PerimetreGirofleProvider
	 */
	public void setPerimetres(List<EOGdPerimetreGirofle> perimetres) {
		this.perimetres = perimetres;
	}

	// Public Method

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getAllQualifiers() {
		return getAllQualifiers(null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getAllQualifiers(NSArray<EOGdStrategie> strategies, UserInfo userInfo) {
		EOQualifier allQualifiers = null;

		for (EOGdPerimetreGirofle perimetre : getPerimetres()) {
			if ((strategies == null) || (strategies.contains(perimetre.strategie()))) {
				perimetre.setUserInfo(userInfo);
				IPerimetreStrategie perimetreStrategie = getPerimetreStrategie(perimetre);
				if (perimetreStrategie.getQualifier() != null) {
					allQualifiers = ERXQ.or(allQualifiers, perimetreStrategie.getQualifier());
				}
			}
		}
		return allQualifiers;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getAllComposants(QueryDslJdbcTemplate template, List<EOGdStrategie> strategies, UserInfo userInfo) {
		List<ComposantRead> allComposants = new ArrayList<ComposantRead>();
		for (EOGdPerimetre perimetre : perimetres) {
			EOGdPerimetreGirofle perimetreGirofle = (EOGdPerimetreGirofle) perimetre;
			if ((strategies == null) || (strategies.contains(perimetreGirofle.strategie()))) {
				perimetreGirofle.setUserInfo(userInfo);
				IPerimetreStrategie perimetreStrategie = getPerimetreStrategie(perimetreGirofle);
				if (perimetreStrategie.getComposants(template) != null) {
					allComposants.addAll(perimetreStrategie.getComposants(template));
				}
			}
		}
		
		return allComposants;
	}

	// Protected Methods
	// Private methods

}
