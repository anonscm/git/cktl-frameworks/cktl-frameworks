package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.scol.maquette.ComposantRead;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;
import com.webobjects.eocontrol.EOQualifier;


/**
 * Cette strategie definit un perimetre sur l'acces a un grade universitaire.
 */
public class PerimetreStrategieAccesGrade implements IPerimetreStrategie {
	
	private EOGradeUniversitaire gradeUniversitaire;

	private QComposant qComposant = QComposant.composant;

	/**
	 * Constructeur auquel on passe le grade universitaire.
	 * @param gradeUniversitaire grade sur lequel est defini la strategie. 
	 */
	public PerimetreStrategieAccesGrade(EOGradeUniversitaire gradeUniversitaire) {
		this.gradeUniversitaire = gradeUniversitaire;
	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		return EODiplome.GRADE_UNIVERSITAIRE.eq(gradeUniversitaire);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		SQLQuery query = template.newSqlQuery()
				.from(qComposant)
				.where(getPredicate())
				.orderBy(qComposant.code.asc())
				.distinct();
		ConstructorExpression<ComposantRead> mapping = ConstructorExpression.create(ComposantRead.class, qComposant.idComposant);
		
		List<ComposantRead> res = template.query(query, mapping);
		
		return res;

	}
	
	/**
	 * {@inheritDoc}
	 */
	private Predicate getPredicate() {
		Predicate predicat = qComposant.dipGradeUniversitaireId.eq(Long.valueOf(gradeUniversitaire.id()));
		
		return predicat;
	}

}
