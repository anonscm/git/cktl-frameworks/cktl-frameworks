package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Cette strategie definit un perimetre sur la composante de l'utilisateur (structure).
 */
public class PerimetreStrategieAccesFormationsComposanteUser implements IPerimetreStrategie {

	private EOGdPerimetreGirofle perimetre;

	// Constructor
	/**
	 * Constructeur auquel on passe le perimetre de la strategie.
	 * @param perimetre perimetre associe pour la stratégie, utilisé pour récupérer le editing context
	 */
	public PerimetreStrategieAccesFormationsComposanteUser(EOGdPerimetreGirofle perimetre) {
		this.perimetre = perimetre;
	}

	// Properties

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		EOQualifier qualifierStructures = null;
		for (EOStructure structure : structuresUser()) {
			qualifierStructures = ERXQ.or(qualifierStructures, EOComposant.STRUCTURES.containsObject(structure));
		}
		return qualifierStructures;
	}

	/**
	 * @return NSArray<EOStructure> : la liste des structures de l'utilisateur connecté
	 */
	private NSArray<EOStructure> structuresUser() {
		NSArray<EOStructure> structures = new NSMutableArray<EOStructure>();

		EOIndividu individuUser = (EOIndividu) EOIndividu.individuWithPersId(perimetre.editingContext(),
		        Integer.valueOf(perimetre.getUserInfo().persId().intValue()));

		@SuppressWarnings("unchecked")
		NSArray<EORepartStructure> repartStructures = ((NSArray<EORepartStructure>) individuUser.getRepartStructuresAffectes(Integer.valueOf(perimetre
		        .getUserInfo().persId().intValue())));
		for (EORepartStructure repartStructuresElt : repartStructures) {
			EOStructure structure = repartStructuresElt.toStructureGroupe();
			if (structure.cTypeStructure().equals(EOTypeStructure.TYPE_STRUCTURE_C)) {
				structures.add(structure);
			}
		}
		return structures;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}

}
