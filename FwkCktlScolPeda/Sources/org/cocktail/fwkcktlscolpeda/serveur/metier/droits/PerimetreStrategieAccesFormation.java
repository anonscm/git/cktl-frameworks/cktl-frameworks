package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Cette strategie definit un perimetre sur une formation (un diplome).
 */
public class PerimetreStrategieAccesFormation implements IPerimetreStrategie {

	private EOComposant diplome;

	/**
	 * Constructeur auquel on passe le diplome avec lequel est defini la strategie.
	 * @param  diplome diplome sur lequel est defini le perimetre de donnee
	 */
	public PerimetreStrategieAccesFormation(EOComposant diplome) {
	    this.diplome = diplome;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {		
		return EOComposant.ID.eq(diplome.id()).or(EOComposant.ALLANCESTORS.containsObject(diplome));			
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}

}