package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;


/**
 * Définition des autorisations pour les applications de la sphère scolarité.
 */
public class ScolariteApplicationAutorisationsCache extends AutorisationsCache {
	// Private fields
	
	/*****************************/
	/********** GIROFLE **********/
	/*****************************/
	public static final String APP_STRING_ID_GIROFLE = "GIROFLE";
	
	public static final String APP_STRING_ID_MODELISATION_MODEL = "MODELISATION_MODEL";
	public static final String APP_STRING_ID_MODELISATION_SYL_DIP = "MODELISATION_SYL_DIP";
	public static final String APP_STRING_ID_MODELISATION_SYL_CMP = "MODELISATION_SYL_CMP";
	public static final String APP_STRING_ID_MODELISATION_EXPORT_CDMFR_HAB = "MODELISATION_CDMFR_H";
	public static final String APP_STRING_ID_MODELISATION_EXPORT_CDMFR_PUB = "MODELISATION_CDMFR_P";
	
	public static final String APP_STRING_ID_EXPLOITATION_EC = "EXPLOITATION_EC";
	public static final String APP_STRING_ID_EXPLOITATION_UE = "EXPLOITATION_UE";
	public static final String APP_STRING_ID_EXPLOITATION_RGPT = "EXPLOITATION_RGPT";
	public static final String APP_STRING_ID_EXPLOITATION_PARCOURS = "EXPLOITATION_PARCOURS";
	public static final String APP_STRING_ID_EXPLOITATION_DIPLOME = "EXPLOITATION_DIPLOME";
	public static final String APP_STRING_ID_EXPLOITATION_MAQUETTAGE = "EXPLOITATION_MAQUETE";

	public static final String APP_STRING_ID_ADMINISTRATION_CHOIX_EXERCICE = "ADMIN_CHOIX_EXERCICE";
	public static final String APP_STRING_ID_ADMINISTRATION_AUTORISER_ASSO = "ADMIN_AUTORISER_ASSO";
	public static final String APP_STRING_ID_ADMINISTRATION_INTERDIRE_ASSO = "ADMIN_INTERDIRE_ASSO";
	public static final String APP_STRING_ID_ADMINISTRATION_TYPE_AP = "ADMIN_TYPE_AP";
	public static final String APP_STRING_ID_ADMINISTRATION_TYPE_AE = "ADMIN_TYPE_AE";
	public static final String APP_STRING_ID_ADMINISTRATION_NOUVELLE_ANNEE = "ADMIN_NOUVELLE_ANNEE";
	public static final String APP_STRING_ID_ADMINISTRATION_NOUVELLE_HABILITATION = "ADMIN_NOUVELLE_HAB";
	
	
	/*****************************/
	/********** CORIANDRE **********/
	/*****************************/
	public static final String APP_STRING_ID_CORIANDRE = "CORIANDRE";
	
	public static final String APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_PREINSCRIPTION = "CO_IAWEB_PREINSCRIP";
	public static final String APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_REINSCRIPTION = "CO_IAWEB_REINSCRIP";
	public static final String APP_STRING_ID_CORIANDRE_IAWEB_RAPPORT_RAPPORTINSCRIP = "CO_IAWEB_RAPPORT";
	public static final String APP_STRING_ID_CORIANDRE_IAWEB_RDV_PRENDRERDV = "CO_IAWEB_RDV";
	public static final String APP_STRING_ID_CORIANDRE_IAWEB_VALIDATION_IAVALIDEES = "CO_IAWEB_IAVALIDEES";
	
	public static final String APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_PREINSCRIPTION = "CO_IA_PREINSCRIPTION";
	public static final String APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_REINSCRIPTION = "CO_IA_REINSCRIPTION";
	public static final String APP_STRING_ID_CORIANDRE_IA_RDV_PLANNING = "CO_IA_RDVPLANNING";
	public static final String APP_STRING_ID_CORIANDRE_IA_DOSSIER_MODIFIA = "CO_IA_DOSSIERMODIFIA";
	public static final String APP_STRING_ID_CORIANDRE_IA_PAIEMENTS_ENATTENTE = "CO_IA_PAIESATTENTE";
	public static final String APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOSSIER = "CO_IA_IMPRESDOSSIER";
	public static final String APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOCETUD = "CO_IA_IMPRESDOCETUD";
	public static final String APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_CARTEETUD = "CO_IA_IMPRESCARTETUD";
	public static final String APP_STRING_ID_CORIANDRE_IA_LISTEETUDIANTS_IATERMINEES = "CO_IA_LISTEETIATERM";
	
	public static final String APP_STRING_ID_CORIANDRE_FINANCIER_PAIEMENTS_ATTENTE = "CO_FINAN_PAIE";
	public static final String APP_STRING_ID_CORIANDRE_FINANCIER_RECAP_JOUR = "CO_FINAN_RECAP";
	public static final String APP_STRING_ID_CORIANDRE_FINANCIER_ENVOISGFC_DEVERSEMENT = "CO_FINAN_GFC";
	
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_PREINSCRIPTION = "CO_PIL_PREINSCRIPTION";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_REINSCRIPTION = "CO_PIL_REINSCRIPTION";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAATTENTE = "CO_PIL_LISTEIAATTENTE";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAVALIDEES = "CO_PIL_LISTEIAPREVALIDEE";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTS_IAVALIDEES = "CO_PIL_LISTEETIAVALID";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTSBOURSIERS = "CO_PIL_LISTEETBOURSIERS";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_EDITIONS_RECAPIA = "CO_PIL_EDITIONSRECAP";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SISE = "CO_PIL_EXPORTSSISE";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_CNOUS = "CO_PIL_EXPORTSCNOUS";
	public static final String APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SS = "CO_PIL_EXPORTSSS";
	
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_PAIEMENT_PARAMETRAGE = "CO_ADMIN_PAIEMENTPAR";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_AFFICHAGE_DEFINITION = "CO_ADMIN_AFFDEF";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_RDV_DEFINITION = "CO_ADMIN_RDVDEF";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW = "CO_ADMIN_WORFLOW";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_POSTBAC = "CO_ADMIN_IMPORTPOBAC";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_RESULTATS = "CO_ADMIN_IMPORTRESUL";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_GESTIONNAIRES = "CO_ADMIN_PERIODESGES";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_ETUDIANTS = "CO_ADMIN_PERIODESETU";
	public static final String APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_ETUDIANTS = "CO_ADMIN_IMPORTETUDS";
	
	
	/*****************************/
	/********** PAPRIKA **********/
	/*****************************/
	public static final String APP_STRING_ID_PAPRIKA = "PAPRIKA";
	
	public static final String APP_STRING_ID_PAPRIKA_IPWEB_INSCRIPTION_IPWEB = "PA_IPWEB_INSCRIPWEB";
	public static final String APP_STRING_ID_PAPRIKA_IPWEB_DOSSIER_CONTRATPEDA = "PA_IPWEB_CONTRATPEDA";
	public static final String APP_STRING_ID_PAPRIKA_IPWEB_VALIDATION_IPVALIDEES = "PA_IPWEB_IPVALIDEES";
	
	public static final String APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPATTENTE = "PA_IP_LISTEIPATTENTE";
	public static final String APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPVALIDEES = "PA_IP_LISTEIPVALIDEE";
	public static final String APP_STRING_ID_PAPRIKA_IP_IPMASSE_MODIFIERIP = "PA_IP_MODIFIERIP";
	public static final String APP_STRING_ID_PAPRIKA_IP_IPMASSE_IPMASSE = "PA_IP_IPMASSE";
	public static final String APP_STRING_ID_PAPRIKA_IP_DOSSIER_CONTRATPEDA = "PA_IP_CONTRATPEDA";
	
	public static final String APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_CONTROLE = "PA_VERIF_CONTROLE";
	public static final String APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_VALIDITE = "PA_VERIF_VALIDITE";
	public static final String APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPDIPLOME = "PA_VERIF_IPDIPLOME";
	public static final String APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPPARCOURS = "PA_VERIF_IPPARCOURS";
	public static final String APP_STRING_ID_PAPRIKA_VERIFICATION_GROUPE_GROUPEETUD = "PA_VERIF_GROUPEETUD";
	
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_ANNEE_CHOIX = "PA_ADMIN_CHOIXANNEE";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_DIPLOME = "PA_ADMIN_OFDIPLOME";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_PARCOURS = "PA_ADMIN_OFPARCOURS";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_MAIL_CONFIG = "PA_ADMIN_MAILCONFIG";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW = "PA_ADMIN_WORFLOW";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_PARAMETRAGE_PARAMETRES = "PA_ADMIN_PARAMETRES";
	public static final String APP_STRING_ID_PAPRIKA_ADMINISTRATION_PARAMETRAGE_INITGROUPES = "PA_ADMIN_INITGROUPES";
	

	// Constructor
	/**
	 * Constructeur.
	 * @param appStrId identifiant de l'application
	 * @param persId identifiant de l'utilisateur
	 */
	public ScolariteApplicationAutorisationsCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}

	// Properties

	// Public Methods
	
	/*****************************/
	/********** GIROFLE **********/
	/*****************************/
	
	/**
	 * Droit d'utiliser Girofle.
	 * @return true si l'utilisateur a le droit d'utiliser Girofle
	 */
	public boolean hasDroitUtilisationGirofle() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_GIROFLE);
	}

	/**
	 * Droit de connaitre Girofle.
	 * @return true si l'utilisateur a le droit de connaitre Girofle
	 */
	public boolean hasDroitConnaissanceGirofle() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_GIROFLE);
	}
	
	/** ONGLET MODELISATION **/
	/**
	 * Droit d'utiliser la partie Modelisation.
	 * @return true si l'utilisateur a le droit d'utiliser la modelisation
	 */
	public boolean hasDroitUtilisationModelisation() {
		return hasDroitUtilisationModelisationModel() ||
			   hasDroitUtilisationModelisationSyllabusDiplome() ||
			   hasDroitUtilisationModelisationSyllabusComposant() ||
			   hasDroitUtilisationModelisationExportCdmfrHabilitation() ||
			   hasDroitUtilisationModelisationExportCdmfrPublication();
	}

	/**
	 * Droit de connaitre la partie Modelisation.
	 * @return true si l'utilisateur a le droit de connaitre la modelisation
	 */
	public boolean hasDroitConnaissanceModelisation() {
		return hasDroitConnaissanceModelisationModel() ||
			   hasDroitConnaissanceModelisationSyllabusDiplome() ||
			   hasDroitConnaissanceModelisationSyllabusComposant() ||
			   hasDroitConnaissanceModelisationExportCdmfrHabilitation() ||
			   hasDroitConnaissanceModelisationExportCdmfrPublication();
	}

	/**
	 * Droit d'utiliser la page Modelisation/Modelisation d'une formation. 
	 * @return true si l'utilisateur a le droit d'utiliser la modelisation d'une formation
	 */
	public boolean hasDroitUtilisationModelisationModel() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_MODELISATION_MODEL);
	}

	/**
	 * Droit de connaitre la page Modelisation/Modelisation d'une formation.
	 * @return true si l'utilisateur a le droit de connaitre la modelisation d'une formation
	 */
	public boolean hasDroitConnaissanceModelisationModel() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_MODELISATION_MODEL);
	}

	/**
	 * Droit d'utiliser la page Modelisation/Syllabus d'un diplome.
	 * @return true si l'utilisateur a le droit d'utiliser le syllabus d'un diplome
	 */
	public boolean hasDroitUtilisationModelisationSyllabusDiplome() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_MODELISATION_SYL_DIP);
	}

	/**
	 * Droit de connaitre la page Modelisation/Syllabus d'un diplome.
	 * @return true si l'utilisateur a le droit de connaitre le syllabus d'un diplome
	 */
	public boolean hasDroitConnaissanceModelisationSyllabusDiplome() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_MODELISATION_SYL_DIP);
	}

	/**
	 * Droit d'utiliser la page Modelisation/Syllabus d'un composant.
	 * @return true si l'utilisateur a le droit d'utiliser le syllabus d'un composant
	 */
	public boolean hasDroitUtilisationModelisationSyllabusComposant() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_MODELISATION_SYL_CMP);
	}

	/**
	 * Droit de connaitre la page Modelisation/Syllabus d'un composant.
	 * @return true si l'utilisateur a le droit de connaitre le syllabus d'un composant
	 */
	public boolean hasDroitConnaissanceModelisationSyllabusComposant() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_MODELISATION_SYL_CMP);
	}
	
	/**
	 * Droit d'utiliser la page modelisation/Export Cdmfr en habilitation.
	 * @return true si l'utilisateur a le droit d'utiliser l'export Cdmfr en habilitation
	 */
	public boolean hasDroitUtilisationModelisationExportCdmfrHabilitation() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_MODELISATION_EXPORT_CDMFR_HAB);
	}
	
	/**
	 * Droit de connaitre la page Modelisation/Export Cdmfr en habilitation.
	 * @return true si l'utilisateur a le droit de connaitre l'export Cdmfr en habilitation
	 */
	public boolean hasDroitConnaissanceModelisationExportCdmfrHabilitation() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_MODELISATION_EXPORT_CDMFR_HAB);
	}
	
	/**
	 * Droit d'utiliser la page Modelisation/Export Cdmfr en habilitation.
	 * @return true si l'utilisateur a le droit d'utiliser l'export Cdmfr en habilitation
	 */
	public boolean hasDroitUtilisationModelisationExportCdmfrPublication() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_MODELISATION_EXPORT_CDMFR_PUB);
	}
	
	/**
	 * Droit de connaitre la page Modelisation/Export Cdmfr en publication.
	 * @return true si l'utilisateur a le droit de connaitre l'export Cdmfr en publication
	 */
	public boolean hasDroitConnaissanceModelisationExportCdmfrPublication() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_MODELISATION_EXPORT_CDMFR_PUB);
	}

	/** ONGLET EXPLOITATION **/
	/**
	 * Droit d'utiliser la partie Exploitation.
	 * @return true si l'utilisateur a le droit d'utiliser la partie Exploitation
	 */
	public boolean hasDroitUtilisationExploitation() {
		return hasDroitUtilisationExploitationEC() ||
			   hasDroitUtilisationExploitationUE() ||
			   hasDroitUtilisationExploitationRGPT() ||
			   hasDroitUtilisationExploitationDiplome() ||
			   hasDroitUtilisationExploitationMaquettage();
	}

	/**
	 * Droit de connaitre la partie Exploitation.
	 * @return true si l'utilisateur a le droit de connaitre la partie Exploitation
	 */
	public boolean hasDroitConnaissanceExploitation() {
		return hasDroitConnaissanceExploitationEC() ||
			   hasDroitConnaissanceExploitationUE() ||
			   hasDroitConnaissanceExploitationRGPT() ||
			   hasDroitConnaissanceExploitationDiplome() ||
			   hasDroitConnaissanceExploitationMaquettage();
	}

	/**
	 * Droit d'utiliser la page Exploitation/ECs.
	 * @return true si l'utilisateur a le droit d'utiliser les ECs
	 */
	public boolean hasDroitUtilisationExploitationEC() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_EC);
	}

	/**
	 * Droit de connaitre la page Exploitation/ECs.
	 * @return true si l'utilisateur a le droit de connaitre les ECs
	 */
	public boolean hasDroitConnaissanceExploitationEC() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_EC);
	}

	/**
	 * Droit d'utiliser la page Exploitation/UEs.
	 * @return true si l'utilisateur a le droit d'utiliser les UEs
	 */
	public boolean hasDroitUtilisationExploitationUE() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_UE);
	}

	/**
	 * Droit de connaitre la page Exploitation/UEs.
	 * @return true si l'utilisateur a le droit de connaitre les UEs
	 */
	public boolean hasDroitConnaissanceExploitationUE() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_UE);
	}

	/**
	 * Droit d'utiliser la page Exploitation/Regroupements.
	 * @return true si l'utilisateur a le droit d'utiliser les Regroupements
	 */
	public boolean hasDroitUtilisationExploitationRGPT() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_RGPT);
	}

	/**
	 * Droit de connaitre la page Exploitation/Regroupements.
	 * @return true si l'utilisateur a le droit de connaitre les Regroupements
	 */
	public boolean hasDroitConnaissanceExploitationRGPT() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_RGPT);
	}

	/**
	 * Droit d'utiliser la page Exploitation/Parcours.
	 * @return true si l'utilisateur a le droit d'utiliser les Parcours
	 */
	public boolean hasDroitUtilisationExploitationParcours() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_PARCOURS);
	}

	/**
	 * Droit de connaitre la page Exploitation/Parcours.
	 * @return true si l'utilisateur a le droit de connaitre les Parcours
	 */
	public boolean hasDroitConnaissanceExploitationParcours() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_PARCOURS);
	}

	/**
	 * Droit d'utiliser la page Exploitation/Diplomes.
	 * @return true si l'utilisateur a le droit d'utiliser les Diplomes
	 */
	public boolean hasDroitUtilisationExploitationDiplome() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_DIPLOME);
	}

	/**
	 * Droit de connaitre la page Exploitation/Diplomes.
	 * @return true si l'utilisateur a le droit de connaitre les Diplomes
	 */
	public boolean hasDroitConnaissanceExploitationDiplome() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_DIPLOME);
	}

	/**
	 * Droit d'utiliser la page Exploitation/Maquettage.
	 * @return true si l'utilisateur a le droit d'utiliser la maquette de formation
	 */
	public boolean hasDroitUtilisationExploitationMaquettage() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_EXPLOITATION_MAQUETTAGE);
	}

	/**
	 * Droit de connaitre la page Exploitation/Maquettage.
	 * @return true si l'utilisateur a le droit de connaitre la maquette de formation
	 */
	public boolean hasDroitConnaissanceExploitationMaquettage() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_EXPLOITATION_MAQUETTAGE);
	}

	/** ONGLET PILOTAGE **/
	/**
	 * Droit d'utiliser la partie Pilotage.
	 * @return true si l'utilisateur a le droit d'utiliser le pilotage
	 */
	public boolean hasDroitUtilisationPilotage() {
		return false;
	}

	/**
	 * Droit de connaitre la partie Pilotage.
	 * @return true si l'utilisateur a le droit de connaitre le pilotage
	 */
	public boolean hasDroitConnaissancePilotage() {
		return true;
	}
	
	/** ONGLET ADMINISTRATION **/
	/**
	 * Droit d'utiliser la partie Administration.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration
	 */
	public boolean hasDroitUtilisationAdministration() {
		return hasDroitUtilisationAdministrationChoixExercice() ||
			   hasDroitUtilisationAdministrationAutorisationAssociation() ||
			   hasDroitUtilisationAdministrationInterdictionAssociation() ||
			   hasDroitUtilisationAdministrationTypeAP() ||
			   hasDroitUtilisationAdministrationTypeAE() ||
			   hasDroitUtilisationAdministrationNouvelleAnnee() ||
			   hasDroitUtilisationAdministrationNouvelleHabilitation();
	}

	/**
	 * Droit de connaitre la partie Administration.
	 * @return true si l'utilisateur a le droit de connaitre l'administration
	 */
	public boolean hasDroitConnaissanceAdministration() {
		return hasDroitConnaissanceAdministrationChoixExercice() ||
			   hasDroitConnaissanceAdministrationAutorisationAssociation() ||
			   hasDroitConnaissanceAdministrationInterdictionAssociation() ||
			   hasDroitConnaissanceAdministrationTypeAP() ||
			   hasDroitConnaissanceAdministrationTypeAE() ||
			   hasDroitConnaissanceAdministrationNouvelleAnnee() ||
			   hasDroitConnaissanceAdministrationNouvelleHabilitation();
	}

	/**
	 * Droit d'utiliser la page Administration/Choix de l'exercice. 
	 * @return true si l'utilisateur a le droit d'utiliser la selection du choix de l'exercice
	 */
	public boolean hasDroitUtilisationAdministrationChoixExercice() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_CHOIX_EXERCICE);
	}

	/**
	 * Droit de connaitre la page Administration/Choix de l'exercice.
	 * @return true si l'utilisateur a le droit de connaitre la selection du choix de l'exercice
	 */
	public boolean hasDroitConnaissanceAdministrationChoixExercice() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_CHOIX_EXERCICE);
	}

	/**
	 * Droit d'utiliser la page Administration/Autorisation des associations.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration pour l'autorisation des associations
	 */
	public boolean hasDroitUtilisationAdministrationAutorisationAssociation() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_AUTORISER_ASSO);
	}

	/**
	 * Droit de connaitre la page Administration/Autorisation des associations.
	 * @return true si l'utilisateur a le droit de connaitre l'administration pour l'autorisation des associations
	 */
	public boolean hasDroitConnaissanceAdministrationAutorisationAssociation() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_AUTORISER_ASSO);
	}

	/**
	 * Droit d'utiliser la page Administration/Interdiction des associations.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration pour l'interdiction des associations
	 */
	public boolean hasDroitUtilisationAdministrationInterdictionAssociation() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_INTERDIRE_ASSO);
	}

	/**
	 * Droit de connaitre la page Administration/interdiction des associations.
	 * @return true si l'utilisateur a le droit de connaitre l'administration pour l'interdiction des associations
	 */
	public boolean hasDroitConnaissanceAdministrationInterdictionAssociation() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_INTERDIRE_ASSO);
	}

	/**
	 * Droit d'utiliser la page Administration/Types APs.
	 * @return true si l'utilisateur a le droit d'utiliser les types APs
	 */
	public boolean hasDroitUtilisationAdministrationTypeAP() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_TYPE_AP);
	}

	/**
	 * Droit de connaitre la page Administration/Types APs.
	 * @return true si l'utilisateur a le droit de connaitre les types APs
	 */
	public boolean hasDroitConnaissanceAdministrationTypeAP() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_TYPE_AP);
	}

	/**
	 * Droit d'utiliser la page Administration/Types AEs.
	 * @return true si l'utilisateur a le droit d'utiliser les types AEs
	 */
	public boolean hasDroitUtilisationAdministrationTypeAE() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_TYPE_AE);
	}

	/**
	 * Droit de connaitre la page Administration/Types AEs.
	 * @return true si l'utilisateur a le droit de connaitre les types AEs
	 */
	public boolean hasDroitConnaissanceAdministrationTypeAE() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_TYPE_AE);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Nouvelle annee.
	 * @return true si l'utilisateur a le droit d'utiliser la nouvelle année
	 */
	public boolean hasDroitUtilisationAdministrationNouvelleAnnee() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_NOUVELLE_ANNEE);
	}

	/**
	 * Droit de connaitre la page Administration/Nouvelle annee.
	 * @return true si l'utilisateur a le droit de connaitre la nouvelle annee
	 */
	public boolean hasDroitConnaissanceAdministrationNouvelleAnnee() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_NOUVELLE_ANNEE);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Nouvelle habilitation.
	 * @return true si l'utilisateur a le droit d'utiliser la nouvelle habilitation
	 */
	public boolean hasDroitUtilisationAdministrationNouvelleHabilitation() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_ADMINISTRATION_NOUVELLE_HABILITATION);
	}

	/**
	 * Droit de connaitre la page Administration/Nouvelle habilitation.
	 * @return true si l'utilisateur a le droit de connaitre la nouvelle habilitation
	 */
	public boolean hasDroitConnaissanceAdministrationNouvelleHabilitation() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_ADMINISTRATION_NOUVELLE_HABILITATION);
	}
	
	
	/*****************************/
	/********* CORIANDRE *********/
	/*****************************/
	
	/**
	 * Droit d'utiliser Coriandre.
	 * @return true si l'utilisateur a le droit d'utiliser Coriandre
	 */
	public boolean hasDroitUtilisationCoriandre() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE);
	}
	
	/**
	 * Droit de connaitre Coriandre.
	 * @return true si l'utilisateur a le droit de connaitre Coriandre
	 */
	public boolean hasDroitConnaissanceCoriandre() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE);
	}
	
	/** ONGLET IA WEB **/
	/**
	 * Droit d'utiliser la partie IA Web.
	 * @return true si l'utilisateur a le droit d'utiliser l'IA Web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWeb() {
		return hasDroitUtilisationCoriandreOngletIaWebItemPreInscrip() ||
			   hasDroitUtilisationCoriandreOngletIaWebItemReInscrip() ||
			   hasDroitUtilisationCoriandreOngletIaWebItemRapportInscrip() ||
			   hasDroitUtilisationCoriandreOngletIaWebItemPrendreRDV() ||
			   hasDroitUtilisationCoriandreOngletIaWebItemIaValidees();
	}

	/**
	 * Droit de connaitre la partie IA Web.
	 * @return true si l'utilisateur a le droit de connaitre l'IA Web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIpWeb() {
		return hasDroitConnaissanceCoriandreOngletIaWebItemPreInscrip() ||
			   hasDroitConnaissanceCoriandreOngletIaWebItemReInscrip() ||
			   hasDroitConnaissanceCoriandreOngletIaWebItemRapportInscrip() ||
			   hasDroitConnaissanceCoriandreOngletIaWebItemPrendreRDV() ||
			   hasDroitConnaissanceCoriandreOngletIaWebItemIaValidees();
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Pre-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la pre-inscription administrative par le web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemPreInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_PREINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page IA Web/Pre-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la pre-inscription administrative par le web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemPreInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_PREINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Re-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la re-inscription administrative par le web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemReInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_REINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page IA Web/Re-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la Re-inscription administrative par le web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemReInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_INSCRIPTION_REINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Rapport d'inscription.
	 * @return true si l'utilisateur a le droit d'utiliser le rapport d'inscription
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemRapportInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_RAPPORT_RAPPORTINSCRIP);
	}

	/**
	 * Droit de connaitre la page IA Web/Rapport d'inscription.
	 * @return true si l'utilisateur a le droit de connaitre le rapport d'inscription
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemRapportInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_RAPPORT_RAPPORTINSCRIP);
	}
	
	/**
	 * Droit d'utiliser la page IA Web/Prendre RDV.
	 * @return true si l'utilisateur a le droit d'utiliser la prise de rendez-vous
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemPrendreRDV() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_RDV_PRENDRERDV);
	}

	/**
	 * Droit de connaitre la page IA Web/Prendre RDV.
	 * @return true si l'utilisateur a le droit de connaitre la prise de rendez-vous
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemPrendreRDV() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_RDV_PRENDRERDV);
	}
	
	/**
	 * Droit d'utiliser la page IA Web/IA Validees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des IAs Validees par le web
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaWebItemIaValidees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_VALIDATION_IAVALIDEES);
	}

	/**
	 * Droit de connaitre la page IA Web/IA Validees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des IAs Validees par le web
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaWebItemIaValidees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IAWEB_VALIDATION_IAVALIDEES);
	}

	/** ONGLET IA **/
	/**
	 * Droit d'utiliser la partie IA.
	 * @return true si l'utilisateur a le droit d'utiliser l'IA
	 */
	public boolean hasDroitUtilisationCoriandreOngletIa() {
		return hasDroitUtilisationCoriandreOngletIaItemListePreInscrip() ||
			   hasDroitUtilisationCoriandreOngletIaItemListeReInscrip() ||
			   hasDroitUtilisationCoriandreOngletIaItemRDVPlanning() ||
			   hasDroitUtilisationCoriandreOngletIaItemDossierModification() ||
			   hasDroitUtilisationCoriandreOngletIaItemPaiementsAttente() ||
			   hasDroitUtilisationCoriandreOngletIaItemImpressionDossier() ||
			   hasDroitUtilisationCoriandreOngletIaItemImpressionDocEtudiant() ||
			   hasDroitUtilisationCoriandreOngletIaItemImpressionCarteEtudiant() ||
			   hasDroitUtilisationCoriandreOngletIaItemListeEtudiantsIaTerminees();
	}

	/**
	 * Droit de connaitre la partie IA.
	 * @return true si l'utilisateur a le droit de connaitre l'IA
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIa() {
		return hasDroitConnaissanceCoriandreOngletIaItemListePreInscrip() ||
			   hasDroitConnaissanceCoriandreOngletIaItemListeReInscrip() ||
			   hasDroitConnaissanceCoriandreOngletIaItemRDVPlanning() ||
			   hasDroitConnaissanceCoriandreOngletIaItemDossierModification() ||
			   hasDroitConnaissanceCoriandreOngletIaItemPaiementsAttente() ||
			   hasDroitConnaissanceCoriandreOngletIaItemImpressionDossier() ||
			   hasDroitConnaissanceCoriandreOngletIaItemImpressionDocEtudiant() ||
			   hasDroitConnaissanceCoriandreOngletIaItemImpressionCarteEtudiant() ||
			   hasDroitConnaissanceCoriandreOngletIaItemListeEtudiantsIaTerminees();
	}
	
	/**
	 * Droit d'utiliser la page IA/Pre-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des pre-inscriptions
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemListePreInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_PREINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page IA/Pre-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la liste des pre-inscriptions
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemListePreInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_PREINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page IA/Re-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des re-inscriptions
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemListeReInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_REINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page IA/Re-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la liste des re-inscriptions
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemListeReInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTECANDIDATS_REINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page IA/RDV Planning.
	 * @return true si l'utilisateur a le droit d'utiliser les RDV planning
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemRDVPlanning() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_RDV_PLANNING);
	}

	/**
	 * Droit de connaitre la page IA/RDV Planning.
	 * @return true si l'utilisateur a le droit de connaitre les RDV planning
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemRDVPlanning() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_RDV_PLANNING);
	}
	
	/**
	 * Droit d'utiliser la page IA/Paiements en attente.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des paiements en attente
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemPaiementsAttente() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_PAIEMENTS_ENATTENTE);
	}

	/**
	 * Droit de connaitre la page IA/Paiements en attente.
	 * @return true si l'utilisateur a le droit de connaitre la liste des paiements en attente
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemPaiementsAttente() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_PAIEMENTS_ENATTENTE);
	}
	
	/**
	 * Droit d'utiliser la page IA/Impression dossier.
	 * @return true si l'utilisateur a le droit d'utiliser l'impression d'un dossier administratif
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemImpressionDossier() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOSSIER);
	}

	/**
	 * Droit de connaitre la page IA/Impression dossier.
	 * @return true si l'utilisateur a le droit de connaitre l'impression d'un dossier administratif
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemImpressionDossier() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOSSIER);
	}
	
	/**
	 * Droit d'utiliser la page IA/Impression document etudiant.
	 * @return true si l'utilisateur a le droit d'utiliser l'impression d'un doc etudiant
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemImpressionDocEtudiant() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOCETUD);
	}

	/**
	 * Droit de connaitre la page IA/Impression document etudiant.
	 * @return true si l'utilisateur a le droit de connaitre l'impression d'un doc etudiant
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemImpressionDocEtudiant() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_DOCETUD);
	}
	
	/**
	 * Droit d'utiliser la page IA/Impression carte etudiant.
	 * @return true si l'utilisateur a le droit d'utiliser l'impression d'une carte etudiant
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemImpressionCarteEtudiant() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_CARTEETUD);
	}

	/**
	 * Droit de connaitre la page IA/Impression carte etudiant.
	 * @return true si l'utilisateur a le droit de connaitre l'impression d'une carte etudiant
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemImpressionCarteEtudiant() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_IMPRESSIONS_CARTEETUD);
	}
	
	/**
	 * Droit d'utiliser la page IA/Modifier dossier administratif.
	 * @return true si l'utilisateur a le droit d'utiliser la modification d'un dossier administratif
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemDossierModification() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_DOSSIER_MODIFIA);
	}

	/**
	 * Droit de connaitre la page IA/Modifier dossier administratif.
	 * @return true si l'utilisateur a le droit de connaitre la modification d'un dossier administratif
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemDossierModification() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_DOSSIER_MODIFIA);
	}
		
	/**
	 * Droit d'utiliser la page IA/Liste Etudiants IA terminees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions administratives etudiantes terminees
	 */
	public boolean hasDroitUtilisationCoriandreOngletIaItemListeEtudiantsIaTerminees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTEETUDIANTS_IATERMINEES);
	}

	/**
	 * Droit de connaitre la page IA/Liste Etudiants IA terminees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions administratives etudiantes terminees
	 */
	public boolean hasDroitConnaissanceCoriandreOngletIaItemListeEtudiantsIaTerminees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_IA_LISTEETUDIANTS_IATERMINEES);
	}
		
	/** ONGLET Financier **/
	/**
	 * Droit d'utiliser la partie Financier.
	 * @return true si l'utilisateur a le droit d'utiliser le Financier
	 */
	public boolean hasDroitUtilisationCoriandreOngletFinancier() {
		return hasDroitUtilisationCoriandreOngletFinancierItemPaiementAttente()||
			   hasDroitUtilisationCoriandreOngletFinancierItemRecapJour() ||
			   hasDroitUtilisationCoriandreOngletFinancierItemEnvoisGFCDeversement();
	}

	/**
	 * Droit de connaitre la partie Financier.
	 * @return true si l'utilisateur a le droit de connaitre le Financier
	 */
	public boolean hasDroitConnaissanceCoriandreOngletFinancier() {
		return hasDroitConnaissanceCoriandreOngletFinancierItemPaiementAttente() ||
			   hasDroitConnaissanceCoriandreOngletFinancierItemRecapJour() ||
			   hasDroitConnaissanceCoriandreOngletFinancierItemEnvoisGFCDeversement();
	}	
	
	/**
	 * Droit d'utiliser la page Financier/ paiement en attente.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des  Financier/ paiement en attente
	 */
	public boolean hasDroitUtilisationCoriandreOngletFinancierItemPaiementAttente() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_PAIEMENTS_ATTENTE);
	}

	/**
	 * Droit de connaitre la page Financier/ paiement en attente.
	 * @return true si l'utilisateur a le droit de connaitre lla liste des  Financier/ paiement en attente
	 */
	public boolean hasDroitConnaissanceCoriandreOngletFinancierItemPaiementAttente() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_PAIEMENTS_ATTENTE);
	}
	
	/**
	 * Droit d'utiliser la page  Financier/Recapituamtif jour.
	 * @return true si l'utilisateur a le droit d'utiliser le Recapituamtif jour
	 */
	public boolean hasDroitUtilisationCoriandreOngletFinancierItemRecapJour() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_RECAP_JOUR);
	}

	/**
	 * Droit de connaitre la page IFinancier/Recapituamtif jour.
	 * @return true si l'utilisateur a le droit de connaitre le Recapituamtif jour
	 */
	public boolean hasDroitConnaissanceCoriandreOngletFinancierItemRecapJour() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_RECAP_JOUR);
	}
	
	/**
	 * Droit d'utiliser la page IA/Deversement GFC.
	 * @return true si l'utilisateur a le droit d'utiliser l'envoi des deversement dans la GFC
	 */
	public boolean hasDroitUtilisationCoriandreOngletFinancierItemEnvoisGFCDeversement() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_ENVOISGFC_DEVERSEMENT);
	}

	/**
	 * Droit de connaitre la page IA/Deversement GFC.
	 * @return true si l'utilisateur a le droit de connaitre l'envoi des deversement dans la GFC
	 */
	public boolean hasDroitConnaissanceCoriandreOngletFinancierItemEnvoisGFCDeversement() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_FINANCIER_ENVOISGFC_DEVERSEMENT);
	}
	
	/** ONGLET PILOTAGE **/
	/**
	 * Droit d'utiliser la partie Pilotage.
	 * @return true si l'utilisateur a le droit d'utiliser le pilotage
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotage() {
		return hasDroitUtilisationCoriandreOngletPilotageItemListePreInscrip() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemListeReInscrip() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemListeIaAttente() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemListeIaValidees() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemListeEtudiantsIAValidees() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemListeEtudiantsBoursiers() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemEditionsRecapIa() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemExportsSISE() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemExportsCnous() ||
			   hasDroitUtilisationCoriandreOngletPilotageItemExportsSS();
	}

	/**
	 * Droit de connaitre la partie Pilotage.
	 * @return true si l'utilisateur a le droit de connaitre le pilotage
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotage() {
		return hasDroitConnaissanceCoriandreOngletPilotageItemListePreInscrip() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemListeReInscrip() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemListeIaAttente() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemListeIaValidees() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemListeEtudiantsIAValidees() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemListeEtudiantsBoursiers() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemEditionsRecapIa() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemExportsSISE() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemExportsCnous() ||
			   hasDroitConnaissanceCoriandreOngletPilotageItemExportsSS();
	}

	/**
	 * Droit d'utiliser la page Pilotage/Pre-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des pre-inscriptions
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListePreInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_PREINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page Pilotage/Pre-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la liste des pre-inscriptions
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListePreInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_PREINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Re-inscription.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des re-inscriptions
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListeReInscrip() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_REINSCRIPTION);
	}

	/**
	 * Droit de connaitre la page Pilotage/Re-inscription.
	 * @return true si l'utilisateur a le droit de connaitre la liste des re-inscriptions
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListeReInscrip() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_REINSCRIPTION);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/IA en attente.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions administratives en attente
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListeIaAttente() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAATTENTE);
	}

	/**
	 * Droit de connaitre la page Pilotage/IA en attente.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions administratives en attente
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListeIaAttente() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAATTENTE);
	}

	/**
	 * Droit d'utiliser la page Pilotage/IA validees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions administratives validees
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListeIaValidees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAVALIDEES);
	}

	/**
	 * Droit de connaitre la page Pilotage/IA validees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions administratives validees
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListeIaValidees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTECANDIDATS_IAVALIDEES);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Liste Etudiants IA validees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions administratives etudiantes validees
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListeEtudiantsIAValidees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTS_IAVALIDEES);
	}

	/**
	 * Droit de connaitre la page Pilotage/Liste Etudiants IA validees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions administratives etudiantes validees
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListeEtudiantsIAValidees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTS_IAVALIDEES);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Boursiers.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des editions recapitulatives des IAs
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemListeEtudiantsBoursiers() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTSBOURSIERS);
	}

	/**
	 * Droit de connaitre la page Pilotage/Boursiers.
	 * @return true si l'utilisateur a le droit de connaitre la liste des editions recapitulatives des IAs
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemListeEtudiantsBoursiers() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_LISTEETUDIANTSBOURSIERS);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Recap IA.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des editions recapitulatives des IAs
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemEditionsRecapIa() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EDITIONS_RECAPIA);
	}

	/**
	 * Droit de connaitre la page Pilotage/Recap IA.
	 * @return true si l'utilisateur a le droit de connaitre la liste des editions recapitulatives des IAs
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemEditionsRecapIa() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EDITIONS_RECAPIA);
	}

	/**
	 * Droit d'utiliser la page Pilotage/Exports SISE.
	 * @return true si l'utilisateur a le droit d'utiliser l'export SISE
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemExportsSISE() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SISE);
	}

	/**
	 * Droit de connaitre la page Pilotage/Exports SISE.
	 * @return true si l'utilisateur a le droit de connaitre l'export SISE
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemExportsSISE() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SISE);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Exports CROUS.
	 * @return true si l'utilisateur a le droit d'utiliser l'export CROUS
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemExportsCnous() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_CNOUS);
	}

	/**
	 * Droit de connaitre la page Pilotage/Exports CROUS.
	 * @return true si l'utilisateur a le droit de connaitre l'export CROUS
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemExportsCnous() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_CNOUS);
	}
	
	/**
	 * Droit d'utiliser la page Pilotage/Exports SS.
	 * @return true si l'utilisateur a le droit d'utiliser l'export SS
	 */
	public boolean hasDroitUtilisationCoriandreOngletPilotageItemExportsSS() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SS);
	}

	/**
	 * Droit de connaitre la page Pilotage/Exports SS.
	 * @return true si l'utilisateur a le droit de connaitre l'export SS
	 */
	public boolean hasDroitConnaissanceCoriandreOngletPilotageItemExportsSS() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_PILOTAGE_EXPORTS_SS);
	}
	
	/** ONGLET ADMINISTRATION **/
	/**
	 * Droit d'utiliser la partie Administration.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministration() {
		return hasDroitUtilisationCoriandreOngletAdministrationItemParametragePaiement() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemDefinitionAffichage() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemDefinirRDV() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemWorkflow() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemImportPostBac() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemImportResultats() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemPeriodesGestionnaires() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemImportEtudiants() ||
			   hasDroitUtilisationCoriandreOngletAdministrationItemPeriodesEtudiants();
	}

	/**
	 * Droit de connaitre la partie Administration.
	 * @return true si l'utilisateur a le droit de connaitre l'dministration
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministration() {
		return hasDroitConnaissanceCoriandreOngletAdministrationItemParametragePaiement() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemDefinitionAffichage() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemDefinirRDV() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemWorkflow() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemImportPostBac() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemImportResultats() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemPeriodesGestionnaires() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemPeriodesEtudiants() ||
			   hasDroitConnaissanceCoriandreOngletAdministrationItemImportEtudiants();
	}
	
	/**
	 * Droit d'utiliser la page Administration/Parametrage Paiement.
	 * @return true si l'utilisateur a le droit d'utiliser le parametrage du paiement
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemParametragePaiement() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PAIEMENT_PARAMETRAGE);
	}

	/**
	 * Droit de connaitre la page Administration/Parametrage Paiement.
	 * @return true si l'utilisateur a le droit d'utiliser le parametrage du paiement
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemParametragePaiement() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PAIEMENT_PARAMETRAGE);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Definition affichage.
	 * @return true si l'utilisateur a le droit d'utiliser la definition de l'affichage
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemDefinitionAffichage() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_AFFICHAGE_DEFINITION);
	}

	/**
	 * Droit de connaitre la page Administration/Definition affichage.
	 * @return true si l'utilisateur a le droit d'utiliser la definition de l'affichage
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemDefinitionAffichage() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_AFFICHAGE_DEFINITION);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Definir RDV.
	 * @return true si l'utilisateur a le droit d'utiliser la definition des rendez-vous
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemDefinirRDV() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_RDV_DEFINITION);
	}

	/**
	 * Droit de connaitre la page Administration/Definir RDV.
	 * @return true si l'utilisateur a le droit d'utiliser la definition des rendez-vous
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemDefinirRDV() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_RDV_DEFINITION);
	}

	/**
	 * Droit d'utiliser la page Administration/Workflow.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration des circuits de validation
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemWorkflow() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW);
	}

	/**
	 * Droit de connaitre la page Administration/Workflow.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration des circuits de validation
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemWorkflow() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Import POST BAC.
	 * @return true si l'utilisateur a le droit d'utiliser l'import POST BAC
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemImportPostBac() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_POSTBAC);
	}

	/**
	 * Droit de connaitre la page Administration/Import POST BAC.
	 * @return true si l'utilisateur a le droit de connaitre l'import POST BAC
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemImportPostBac() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_POSTBAC);
	}

	/**
	 * Droit d'utiliser la page Administration/Import Resultats.
	 * @return true si l'utilisateur a le droit d'utiliser l'import des resultats
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemImportResultats() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_RESULTATS);
	}

	/**
	 * Droit de connaitre la page Administration/Import Resultats.
	 * @return true si l'utilisateur a le droit de connaitre l'import des resultats
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemImportResultats() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_RESULTATS);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Periodes Gestionnaires.
	 * @return true si l'utilisateur a le droit d'utiliser les periodes gestionnaires
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemPeriodesGestionnaires() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_GESTIONNAIRES);
	}

	/**
	 * Droit de connaitre la page Administration/Periodes Gestionnaires.
	 * @return true si l'utilisateur a le droit de connaitre les periodes gestionnaires
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemPeriodesGestionnaires() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_GESTIONNAIRES);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Periodes Etudiants.
	 * @return true si l'utilisateur a le droit d'utiliser les periodes etudiants
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemPeriodesEtudiants() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_ETUDIANTS);
	}

	/**
	 * Droit de connaitre la page Administration/Periodes Etudiants.
	 * @return true si l'utilisateur a le droit de connaitre les periodes etudiants
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemPeriodesEtudiants() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_PERIODES_ETUDIANTS);
	}
		
	/**
	 * Droit d'utiliser la page Administration/Import Etudiants.
	 * @return true si l'utilisateur a le droit d'utiliser l'import des etudiants
	 */
	public boolean hasDroitUtilisationCoriandreOngletAdministrationItemImportEtudiants() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_ETUDIANTS);
	}
	
	/**
	 * Droit de connaitre la page Administration/Import Etudiants.
	 * @return true si l'utilisateur a le droit de connaitre l'import des etudiants
	 */
	public boolean hasDroitConnaissanceCoriandreOngletAdministrationItemImportEtudiants() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_CORIANDRE_ADMINISTRATION_IMPORT_ETUDIANTS);
	}
	
		
	/*****************************/
	/********** PAPRIKA **********/
	/*****************************/
	
	/**
	 * Droit d'utiliser Paprika.
	 * @return true si l'utilisateur a le droit d'utiliser Paprika
	 */
	public boolean hasDroitUtilisationPaprika() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA);
	}
	
	/**
	 * Droit de connaitre Paprika.
	 * @return true si l'utilisateur a le droit de connaitre Paprika
	 */
	public boolean hasDroitConnaissancePaprika() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA);
	}
	
	/** ONGLET IP WEB **/
	/**
	 * Droit d'utiliser la partie IP Web.
	 * @return true si l'utilisateur a le droit d'utiliser l'IP Web
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpWeb() {
		return hasDroitUtilisationPaprikaOngletIpWebItemIpWeb() ||
			   hasDroitUtilisationPaprikaOngletIpWebItemContratPeda() ||
			   hasDroitUtilisationPaprikaOngletIpWebItemIpValidees();
	}

	/**
	 * Droit de connaitre la partie IP Web.
	 * @return true si l'utilisateur a le droit de connaitre l'IP Web
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpWeb() {
		return hasDroitConnaissancePaprikaOngletIpWebItemIpWeb() ||
			   hasDroitConnaissancePaprikaOngletIpWebItemContratPeda() ||
			   hasDroitConnaissancePaprikaOngletIpWebItemIpValidees();
	}

	/**
	 * Droit d'utiliser la page IP Web/IP par le Web.
	 * @return true si l'utilisateur a le droit d'utiliser l'inscription pedagogique par le web
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpWebItemIpWeb() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_INSCRIPTION_IPWEB);
	}

	/**
	 * Droit de connaitre la page IP Web/IP par le Web.
	 * @return true si l'utilisateur a le droit de connaitre l'inscription pedagogique par le web
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpWebItemIpWeb() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_INSCRIPTION_IPWEB);
	}
	
	/**
	 * Droit d'utiliser la page IP Web/Contrat Peda.
	 * @return true si l'utilisateur a le droit d'utiliser l'edition du dossier du contrat pedagogique par le web
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpWebItemContratPeda() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_DOSSIER_CONTRATPEDA);
	}

	/**
	 * Droit de connaitre la page IP Web/Contrat Peda.
	 * @return true si l'utilisateur a le droit de connaitre l'edition du dossier du contrat pedagogique par le web
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpWebItemContratPeda() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_DOSSIER_CONTRATPEDA);
	}

	/**
	 * Droit d'utiliser la page IP Web/IP Validées.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des IPs Validees par le web
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpWebItemIpValidees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_VALIDATION_IPVALIDEES);
	}

	/**
	 * Droit de connaitre la page IP Web/IP Validées.
	 * @return true si l'utilisateur a le droit de connaitre la liste des IPs Validees  par le web
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpWebItemIpValidees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IPWEB_VALIDATION_IPVALIDEES);
	}
	
	/** ONGLET IP **/
	/**
	 * Droit d'utiliser la partie IP.
	 * @return true si l'utilisateur a le droit d'utiliser l'IP
	 */
	public boolean hasDroitUtilisationPaprikaOngletIp() {
		return hasDroitUtilisationPaprikaOngletIpItemIpAttente() ||
			   hasDroitUtilisationPaprikaOngletIpItemIpValidees() ||
			   hasDroitUtilisationPaprikaOngletIpItemModifier() ||
			   hasDroitUtilisationPaprikaOngletIpItemIpMasse() ||
			   hasDroitUtilisationPaprikaOngletIpItemContratPeda();
	}

	/**
	 * Droit de connaitre la partie IP.
	 * @return true si l'utilisateur a le droit de connaitre l'IP
	 */
	public boolean hasDroitConnaissancePaprikaOngletIp() {
		return hasDroitConnaissancePaprikaOngletIpItemIpAttente() ||
			   hasDroitConnaissancePaprikaOngletIpItemIpValidees() ||
			   hasDroitConnaissancePaprikaOngletIpItemModifier() ||
			   hasDroitConnaissancePaprikaOngletIpItemIpMasse() ||
			   hasDroitConnaissancePaprikaOngletIpItemContratPeda();
	}
	
	/**
	 * Droit d'utiliser la page IP/IP en attente.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions pedagogiques en attente
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpItemIpAttente() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPATTENTE);
	}

	/**
	 * Droit de connaitre la page IP/IP en attente.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions pedagogiques en attente
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpItemIpAttente() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPATTENTE);
	}
	
	/**
	 * Droit d'utiliser la page IP/IP validees.
	 * @return true si l'utilisateur a le droit d'utiliser la liste des inscriptions pedagogiques validees
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpItemIpValidees() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPVALIDEES);
	}

	/**
	 * Droit de connaitre la page IP/IP validees.
	 * @return true si l'utilisateur a le droit de connaitre la liste des inscriptions pedagogiques validees
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpItemIpValidees() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IP_INSCRIPTION_IPVALIDEES);
	}
	
	/**
	 * Droit d'utiliser la page IP/Modifier IP.
	 * @return true si l'utilisateur a le droit d'utiliser la modification d'une IP
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpItemModifier() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IP_IPMASSE_MODIFIERIP);
	}

	/**
	 * Droit de connaitre la page IP/Modifier IP.
	 * @return true si l'utilisateur a le droit de connaitre la modification d'une IP
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpItemModifier() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IP_IPMASSE_MODIFIERIP);
	}
	
	/**
	 * Droit d'utiliser la page IP/IP de masse.
	 * @return true si l'utilisateur a le droit d'utiliser les IP de masse
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpItemIpMasse() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IP_IPMASSE_IPMASSE);
	}

	/**
	 * Droit de connaitre la page IP/IP de masse.
	 * @return true si l'utilisateur a le droit de connaitre les IP de masse
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpItemIpMasse() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IP_IPMASSE_IPMASSE);
	}
	
	/**
	 * Droit d'utiliser la page IP/Contrat Peda.
	 * @return true si l'utilisateur a le droit d'utiliser l'edition du dossier du contrat pedagogique par le web
	 */
	public boolean hasDroitUtilisationPaprikaOngletIpItemContratPeda() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_IP_DOSSIER_CONTRATPEDA);
	}

	/**
	 * Droit de connaitre la page IP/Contrat Peda.
	 * @return true si l'utilisateur a le droit de connaitre l'edition du dossier du contrat pedagogique par le web
	 */
	public boolean hasDroitConnaissancePaprikaOngletIpItemContratPeda() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_IP_DOSSIER_CONTRATPEDA);
	}

	/** ONGLET VERIFICATION **/
	/**
	 * Droit d'utiliser la partie Verification.
	 * @return true si l'utilisateur a le droit d'utiliser la verification
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerification() {
		return hasDroitUtilisationPaprikaOngletVerificationItemControle() ||
			   hasDroitUtilisationPaprikaOngletVerificationItemValidite() ||
			   hasDroitUtilisationPaprikaOngletVerificationItemIpDiplome() ||
			   hasDroitUtilisationPaprikaOngletVerificationItemIpParcours() ||
			   hasDroitUtilisationPaprikaOngletVerificationItemGroupeEtud();
	}

	/**
	 * Droit de connaitre la partie Verification.
	 * @return true si l'utilisateur a le droit de connaitre la verification
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerification() {
		return hasDroitConnaissancePaprikaOngletVerificationItemControle() ||
			   hasDroitConnaissancePaprikaOngletVerificationItemValidite() ||
			   hasDroitConnaissancePaprikaOngletVerificationItemIpDiplome() ||
			   hasDroitConnaissancePaprikaOngletVerificationItemIpParcours() ||
			   hasDroitConnaissancePaprikaOngletVerificationItemGroupeEtud();
	}
	
	/**
	 * Droit d'utiliser la page Verification/Controle IAIP.
	 * @return true si l'utilisateur a le droit d'utiliser le controle des IAIP
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerificationItemControle() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_CONTROLE);
	}

	/**
	 * Droit de connaitre la page Verification/Controle IAIP.
	 * @return true si l'utilisateur a le droit de connaitre le controle des IAIP
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerificationItemControle() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_CONTROLE);
	}
	
	/**
	 * Droit d'utiliser la page Verification/Validite IP.
	 * @return true si l'utilisateur a le droit d'utiliser la verification de la validite des IP
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerificationItemValidite() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_VALIDITE);
	}

	/**
	 * Droit de connaitre la page Verification/Validite IP.
	 * @return true si l'utilisateur a le droit de connaitre la verification de la validite des IP
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerificationItemValidite() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_VERIFICATION_VALIDITE);
	}

	/**
	 * Droit d'utiliser la page Verification/IP par Diplome.
	 * @return true si l'utilisateur a le droit d'utiliser la verification des IP par diplome
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerificationItemIpDiplome() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPDIPLOME);
	}

	/**
	 * Droit de connaitre la page Verification/IP par Diplome.
	 * @return true si l'utilisateur a le droit de connaitre la verification des IP par diplome
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerificationItemIpDiplome() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPDIPLOME);
	}
	
	/**
	 * Droit d'utiliser la page Verification/IP par Parcours.
	 * @return true si l'utilisateur a le droit d'utiliser la verification des IP par parcours
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerificationItemIpParcours() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPPARCOURS);
	}

	/**
	 * Droit de connaitre la page Verification/IP par Parcours.
	 * @return true si l'utilisateur a le droit de connaitre la verification des IP par parcours
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerificationItemIpParcours() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_LISTEETUDIANTS_IPPARCOURS);
	}
	
	/**
	 * Droit d'utiliser la page Verification/Groupe Etud.
	 * @return true si l'utilisateur a le droit d'utiliser la verification des IP par parcours
	 */
	public boolean hasDroitUtilisationPaprikaOngletVerificationItemGroupeEtud() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_GROUPE_GROUPEETUD);
	}

	/**
	 * Droit de connaitre la page Verification/Groupe Etud.
	 * @return true si l'utilisateur a le droit de connaitre la verification des IP par parcours
	 */
	public boolean hasDroitConnaissancePaprikaOngletVerificationItemGroupeEtud() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_VERIFICATION_GROUPE_GROUPEETUD);
	}
	
	/** ONGLET ADMINISTRATION **/
	/**
	 * Droit d'utiliser la partie Administration.
	 * @return true si l'utilisateur a le droit d'utiliser l'dministration
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministration() {
		return hasDroitUtilisationPaprikaOngletAdministrationItemChoixAnnee() ||
			   hasDroitUtilisationPaprikaOngletAdministrationItemOFDiplome() ||
			   hasDroitUtilisationPaprikaOngletAdministrationItemOFParcours() ||
			   hasDroitUtilisationPaprikaOngletAdministrationItemMailConfig() ||
			   hasDroitUtilisationPaprikaOngletAdministrationItemWorkflow();
	}

	/**
	 * Droit de connaitre la partie Administration.
	 * @return true si l'utilisateur a le droit de connaitre l'dministration
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministration() {
		return hasDroitConnaissancePaprikaOngletAdministrationItemChoixAnnee() ||
			   hasDroitConnaissancePaprikaOngletAdministrationItemOFDiplome() ||
			   hasDroitConnaissancePaprikaOngletAdministrationItemOFParcours() ||
			   hasDroitConnaissancePaprikaOngletAdministrationItemMailConfig() ||
			   hasDroitConnaissancePaprikaOngletAdministrationItemWorkflow();
	}
	
	/**
	 * Droit d'utiliser la page Administration/Choix Annee.
	 * @return true si l'utilisateur a le droit d'utiliser le choix de l'annee de visualisation
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministrationItemChoixAnnee() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_ANNEE_CHOIX);
	}

	/**
	 * Droit de connaitre la page Administration/Choix Annee.
	 * @return true si l'utilisateur a le droit de connaitre le choix de l'annee de visualisation
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministrationItemChoixAnnee() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_ANNEE_CHOIX);
	}
	
	/**
	 * Droit d'utiliser la page Administration/OF Diplome.
	 * @return true si l'utilisateur a le droit d'utiliser l'OF des diplomes
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministrationItemOFDiplome() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_DIPLOME);
	}

	/**
	 * Droit de connaitre la page Administration/OF Diplome.
	 * @return true si l'utilisateur a le droit de connaitre l'OF des diplomes
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministrationItemOFDiplome() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_DIPLOME);
	}
	
	/**
	 * Droit d'utiliser la page Administration/OF Parcours.
	 * @return true si l'utilisateur a le droit d'utiliser l'OF des parcours
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministrationItemOFParcours() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_PARCOURS);
	}

	/**
	 * Droit de connaitre la page Administration/OF Parcours.
	 * @return true si l'utilisateur a le droit de connaitre l'OF des parcours
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministrationItemOFParcours() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_OF_PARCOURS);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Configurer Mail.
	 * @return true si l'utilisateur a le droit d'utiliser la configuration des mails
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministrationItemMailConfig() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_MAIL_CONFIG);
	}

	/**
	 * Droit de connaitre la page Administration/Configurer Mail.
	 * @return true si l'utilisateur a le droit de connaitre la configuration des mails
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministrationItemMailConfig() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_MAIL_CONFIG);
	}
	
	/**
	 * Droit d'utiliser la page Administration/Workflow.
	 * @return true si l'utilisateur a le droit d'utiliser l'administration des circuits de validation
	 */
	public boolean hasDroitUtilisationPaprikaOngletAdministrationItemWorkflow() {
		return hasDroitUtilisationOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW);
	}

	/**
	 * Droit de connaitre la page Administration/Workflow.
	 * @return true si l'utilisateur a le droit de connaitre l'administration des circuits de validation
	 */
	public boolean hasDroitConnaissancePaprikaOngletAdministrationItemWorkflow() {
		return hasDroitConnaissanceOnFonction(APP_STRING_ID_PAPRIKA_ADMINISTRATION_CIRCUITVALIDATION_WORKFLOW);
	}
	
	// Protected Methods
	
	
	// Private methods

	
}
