package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie;

/**
 * Cette classe permet de construire une strategie de construction de perimetre pour un perimetre de donnees.
 */
public final class PerimetreStrategieFactory {

	private PerimetreStrategieFactory() {
	}

	/**
	 * Creation d'une strategie à partir d'un perimetre de donnees.
	 * @param perimetre perimetre de donnees pour lequel est definie la strategie.
	 * @return IPerimetreStrategie strategie
	 */
	public static IPerimetreStrategie createStrategy(EOGdPerimetreGirofle perimetre) {
		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESFORMATION_CODE)) {
			return new PerimetreStrategieAccesFormation(perimetre.composant());
		}
		
		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESELEMENTSFORMATION_CODE)) {
			if (perimetre.perimetreParent() == null) {
				return new PerimetreStrategieAccesElementsFormation(perimetre);
			}
		}
		
		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESELEMENT_CODE)) {
			return new PerimetreStrategieAccesElement(perimetre.composant());
		}

		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESFORMATIONSCOMPOSANTE_CODE)) {
			return new PerimetreStrategieAccesFormationsComposante(perimetre.structure());
		}

		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESFORMATIONSCOMPOSANTEUSER_CODE)) {
			return new PerimetreStrategieAccesFormationsComposanteUser(perimetre);
		}
		
		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESELEMENTSFORMATIONUSERRESP_CODE)) {
			return new PerimetreStrategieDefault();
		}
		
		if (perimetre.strategie().code().equals(EOGdStrategie.STRATEGIE_ACCESGRADE_CODE)) {
			return new PerimetreStrategieAccesGrade(perimetre.gradeUniversitaire());
		}

		return new PerimetreStrategieDefault();
	}
}
