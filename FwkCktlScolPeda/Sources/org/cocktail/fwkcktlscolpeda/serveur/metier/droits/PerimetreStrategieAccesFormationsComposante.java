package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Cette strategie definit un perimetre sur une structure (composante d'un etablissement).
 */
public class PerimetreStrategieAccesFormationsComposante implements IPerimetreStrategie {

	private EOStructure structure;

	/**
	 * Constructeur auquel on passe la structure.
	 * @param structure structure pour laquelle on definit un perimetre de donnee
	 */
	public PerimetreStrategieAccesFormationsComposante(EOStructure structure) {
		this.structure = structure;
	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		EOQualifier structuresQualifier = null;

		if (structure != null) {
			structuresQualifier = EOComposant.STRUCTURES.containsObject(structure);

			for (EOComposant composant : EOComposant.fetchSco_Composants(structure.editingContext(), EOComposant.STRUCTURES.containsObject(structure), null)) {
				structuresQualifier = ERXQ.or(structuresQualifier, EOComposant.ALLANCESTORS.containsObject(composant));
			}
		}
		return structuresQualifier;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}

}
