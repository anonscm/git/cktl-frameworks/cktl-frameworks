package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Strategie par défaut.
 */
public class PerimetreStrategieDefault implements IPerimetreStrategie {

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}

}
