package org.cocktail.fwkcktlscolpeda.serveur.metier.droits;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdPerimetreGirofle;
import org.cocktail.scol.maquette.ComposantRead;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;


/**
 * Cette strategie definit un perimetre sur un ensemble de composants appartenant à une formation (un diplome).
 */
public class PerimetreStrategieAccesElementsFormation implements IPerimetreStrategie {

	private EOGdPerimetreGirofle perimetre;


	/**
	 * Constructeur de la strategie auquel on passe le perimetre de donnees racine.
	 * @param  perimetre  perimetre de donnees racine defini sur une formation
	 */
	public PerimetreStrategieAccesElementsFormation(EOGdPerimetreGirofle perimetre) {
	    this.perimetre = perimetre;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		EOQualifier allQualifiers = null;
		
		// Ajouter le droit en acces sur le composant en entree
		allQualifiers = EOComposant.ID.eq(perimetre.composant().id());
		
		// puis ajouter les droits en acces sur l'ensemble des composants des perimetres enfants
		for (EOGdPerimetre perimetreEnfant : perimetre.perimetresEnfants()) {
			EOGdPerimetreGirofle perimetreGirofleEnfant = (EOGdPerimetreGirofle) perimetreEnfant;
						
			allQualifiers = ERXQ.or(allQualifiers,
					EOComposant.ID.eq(perimetreGirofleEnfant.composant().id()).and(EOComposant.ALLANCESTORS.containsObject(perimetre.composant())));
			
			// pour chaque perimetre enfant il faut ajouter les droits en acces sur l'ensemble des successeurs
			for (EOComposant composant : perimetreGirofleEnfant.composant().allSuccessors()) {
				allQualifiers = ERXQ.or(allQualifiers,
						EOComposant.ID.eq(composant.id()).and(EOComposant.ALLANCESTORS.containsObject(perimetreGirofleEnfant.composant())));
			}
		
		}

		return allQualifiers;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ComposantRead> getComposants(QueryDslJdbcTemplate template) {
		return null;
	}

	
}
