package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Destinaton du descriptif (pour habilitation, pour publication)
 */
public interface ITypeDescriptifDestination {
	String TYPEHABILITATION_LBL = "HABILITATION";
	String TYPEPUBLICATION_LBL = "PUBLICATION";

	/**
	 * @return type de destination du descriptif (habilitation ou publication)
	 */
	String type();
}
