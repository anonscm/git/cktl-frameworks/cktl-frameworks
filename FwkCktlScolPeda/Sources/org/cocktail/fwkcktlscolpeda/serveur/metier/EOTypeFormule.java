package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * {@inheritDoc}
 */
public class EOTypeFormule extends _EOTypeFormule implements ITypeFormule {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeFormule.class);
	private static ERXEnterpriseObjectCache<EOTypeFormule> typeFormuleCache;

	/**
	 * @return le cache des {@link EOTypeFormule} par leur code
	 */
	public static ERXEnterpriseObjectCache<EOTypeFormule> getTypeFormuleCache() {
		if (typeFormuleCache == null) {
			typeFormuleCache = new ERXEnterpriseObjectCache<EOTypeFormule>(EOTypeFormule.class, TYPE_KEY);
		}
		return typeFormuleCache;
	}

	/**
	 * @param typeFormuleCache le cache a setter
	 */
	public static void setTypeFormuleCache(ERXEnterpriseObjectCache<EOTypeFormule> typeFormuleCache) {
		EOTypeFormule.typeFormuleCache = typeFormuleCache;
	}

	/**
	 * @param edc l'editing context
	 * @param libelle le libelle du type de formule
	 * @return le {@link EOTypeFormule}  qui correspond au libellé
	 */
	public static EOTypeFormule getFormuleDeType(EOEditingContext edc, String libelle) {
		return getTypeFormuleCache().objectForKey(edc, libelle);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormule} "MOYENNE"
	 */
	public static EOTypeFormule typeMoyenne(EOEditingContext edc) {
		return getTypeFormuleCache().objectForKey(edc, TYPEMOYENNE_LBL);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormule} "ADMISSION"
	 */
	public static EOTypeFormule typeAdmission(EOEditingContext edc) {
		return getTypeFormuleCache().objectForKey(edc, TYPEADMISSION_LBL);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormule} "COMPENSATION"
	 */
	public static EOTypeFormule typeCompensation(EOEditingContext edc) {
		return getTypeFormuleCache().objectForKey(edc, TYPECOMPENSATION_LBL);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeFormule} "COMPENSATION"
	 */
	public static EOTypeFormule typeDette(EOEditingContext edc) {
		return getTypeFormuleCache().objectForKey(edc, TYPEDETTES_LBL);
	}

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.type();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isMoyenne() {
		return TYPEMOYENNE_LBL.equals(type());
	}

}
