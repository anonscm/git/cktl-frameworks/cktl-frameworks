// DO NOT EDIT.  Make changes to EOTypeInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeInscription";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MOFIFICATION = new ERXKey<NSTimestamp>("dMofification");
  public static final ERXKey<String> LC_TYPE_INSCRIPTION = new ERXKey<String>("lcTypeInscription");
  public static final ERXKey<String> LL_TYPE_INSCRIPTION = new ERXKey<String>("llTypeInscription");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODICATION = new ERXKey<Integer>("persIdModication");
  // Relationship Keys

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MOFIFICATION_KEY = D_MOFIFICATION.key();
  public static final String LC_TYPE_INSCRIPTION_KEY = LC_TYPE_INSCRIPTION.key();
  public static final String LL_TYPE_INSCRIPTION_KEY = LL_TYPE_INSCRIPTION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODICATION_KEY = PERS_ID_MODICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeInscription.class);

  public EOTypeInscription localInstanceIn(EOEditingContext editingContext) {
    EOTypeInscription localInstance = (EOTypeInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeInscription.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.D_CREATION_KEY);
  }

  public NSTimestamp dMofification() {
    return (NSTimestamp) storedValueForKey(_EOTypeInscription.D_MOFIFICATION_KEY);
  }

  public void setDMofification(NSTimestamp value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating dMofification from " + dMofification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.D_MOFIFICATION_KEY);
  }

  public String lcTypeInscription() {
    return (String) storedValueForKey(_EOTypeInscription.LC_TYPE_INSCRIPTION_KEY);
  }

  public void setLcTypeInscription(String value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating lcTypeInscription from " + lcTypeInscription() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.LC_TYPE_INSCRIPTION_KEY);
  }

  public String llTypeInscription() {
    return (String) storedValueForKey(_EOTypeInscription.LL_TYPE_INSCRIPTION_KEY);
  }

  public void setLlTypeInscription(String value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating llTypeInscription from " + llTypeInscription() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.LL_TYPE_INSCRIPTION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOTypeInscription.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModication() {
    return (Integer) storedValueForKey(_EOTypeInscription.PERS_ID_MODICATION_KEY);
  }

  public void setPersIdModication(Integer value) {
    if (_EOTypeInscription.LOG.isDebugEnabled()) {
    	_EOTypeInscription.LOG.debug( "updating persIdModication from " + persIdModication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeInscription.PERS_ID_MODICATION_KEY);
  }


  public static EOTypeInscription createSco_TypeInscription(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dMofification
, String lcTypeInscription
, String llTypeInscription
, Integer persIdCreation
, Integer persIdModication
) {
    EOTypeInscription eo = (EOTypeInscription) EOUtilities.createAndInsertInstance(editingContext, _EOTypeInscription.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDMofification(dMofification);
		eo.setLcTypeInscription(lcTypeInscription);
		eo.setLlTypeInscription(llTypeInscription);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModication(persIdModication);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeInscription> fetchSpec() {
    return new ERXFetchSpecification<EOTypeInscription>(_EOTypeInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeInscription> fetchAllSco_TypeInscriptions(EOEditingContext editingContext) {
    return _EOTypeInscription.fetchAllSco_TypeInscriptions(editingContext, null);
  }

  public static NSArray<EOTypeInscription> fetchAllSco_TypeInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeInscription.fetchSco_TypeInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeInscription> fetchSco_TypeInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeInscription> fetchSpec = new ERXFetchSpecification<EOTypeInscription>(_EOTypeInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeInscription fetchSco_TypeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscription.fetchSco_TypeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscription fetchSco_TypeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeInscription> eoObjects = _EOTypeInscription.fetchSco_TypeInscriptions(editingContext, qualifier, null);
    EOTypeInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscription fetchRequiredSco_TypeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeInscription.fetchRequiredSco_TypeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeInscription fetchRequiredSco_TypeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeInscription eoObject = _EOTypeInscription.fetchSco_TypeInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeInscription localInstanceIn(EOEditingContext editingContext, EOTypeInscription eo) {
    EOTypeInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
