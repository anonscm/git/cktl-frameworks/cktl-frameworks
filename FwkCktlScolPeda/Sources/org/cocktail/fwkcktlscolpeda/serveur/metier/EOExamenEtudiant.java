package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * 
 * @author isabelle
 *
 */
public class EOExamenEtudiant extends _EOExamenEtudiant implements IExamenEtudiant {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOExamenEtudiant.class);

	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantAnneeRelationship(IEtudiantAnnee value) {
		super.setToEtudiantAnneeRelationship((EOEtudiantAnnee) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToExamenOrganistaionRelationship(IExamenOrganisation value) {
		super.setToExamenOrganisationRelationship((EOExamenOrganisation) value);
	}

}
