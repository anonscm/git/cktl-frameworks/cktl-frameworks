// DO NOT EDIT.  Make changes to EOSiseSecteurDisciplinaire.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSiseSecteurDisciplinaire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SiseSecteurDisciplinaire";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline> SISE_DISCIPLINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline>("siseDiscipline");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String SISE_DISCIPLINE_KEY = SISE_DISCIPLINE.key();

  private static Logger LOG = Logger.getLogger(_EOSiseSecteurDisciplinaire.class);

  public EOSiseSecteurDisciplinaire localInstanceIn(EOEditingContext editingContext) {
    EOSiseSecteurDisciplinaire localInstance = (EOSiseSecteurDisciplinaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOSiseSecteurDisciplinaire.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOSiseSecteurDisciplinaire.LOG.isDebugEnabled()) {
    	_EOSiseSecteurDisciplinaire.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSiseSecteurDisciplinaire.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSiseSecteurDisciplinaire.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSiseSecteurDisciplinaire.LOG.isDebugEnabled()) {
    	_EOSiseSecteurDisciplinaire.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSiseSecteurDisciplinaire.LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline siseDiscipline() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline)storedValueForKey(_EOSiseSecteurDisciplinaire.SISE_DISCIPLINE_KEY);
  }
  
  public void setSiseDiscipline(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline value) {
    takeStoredValueForKey(value, _EOSiseSecteurDisciplinaire.SISE_DISCIPLINE_KEY);
  }

  public void setSiseDisciplineRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline value) {
    if (_EOSiseSecteurDisciplinaire.LOG.isDebugEnabled()) {
      _EOSiseSecteurDisciplinaire.LOG.debug("updating siseDiscipline from " + siseDiscipline() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setSiseDiscipline(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline oldValue = siseDiscipline();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOSiseSecteurDisciplinaire.SISE_DISCIPLINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOSiseSecteurDisciplinaire.SISE_DISCIPLINE_KEY);
    }
  }
  

  public static EOSiseSecteurDisciplinaire createSco_SiseSecteurDisciplinaire(EOEditingContext editingContext, String code
, String libelle
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseDiscipline siseDiscipline) {
    EOSiseSecteurDisciplinaire eo = (EOSiseSecteurDisciplinaire) EOUtilities.createAndInsertInstance(editingContext, _EOSiseSecteurDisciplinaire.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    eo.setSiseDisciplineRelationship(siseDiscipline);
    return eo;
  }

  public static ERXFetchSpecification<EOSiseSecteurDisciplinaire> fetchSpec() {
    return new ERXFetchSpecification<EOSiseSecteurDisciplinaire>(_EOSiseSecteurDisciplinaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSiseSecteurDisciplinaire> fetchAllSco_SiseSecteurDisciplinaires(EOEditingContext editingContext) {
    return _EOSiseSecteurDisciplinaire.fetchAllSco_SiseSecteurDisciplinaires(editingContext, null);
  }

  public static NSArray<EOSiseSecteurDisciplinaire> fetchAllSco_SiseSecteurDisciplinaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSiseSecteurDisciplinaire.fetchSco_SiseSecteurDisciplinaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSiseSecteurDisciplinaire> fetchSco_SiseSecteurDisciplinaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSiseSecteurDisciplinaire> fetchSpec = new ERXFetchSpecification<EOSiseSecteurDisciplinaire>(_EOSiseSecteurDisciplinaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSiseSecteurDisciplinaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSiseSecteurDisciplinaire fetchSco_SiseSecteurDisciplinaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiseSecteurDisciplinaire.fetchSco_SiseSecteurDisciplinaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiseSecteurDisciplinaire fetchSco_SiseSecteurDisciplinaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSiseSecteurDisciplinaire> eoObjects = _EOSiseSecteurDisciplinaire.fetchSco_SiseSecteurDisciplinaires(editingContext, qualifier, null);
    EOSiseSecteurDisciplinaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SiseSecteurDisciplinaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiseSecteurDisciplinaire fetchRequiredSco_SiseSecteurDisciplinaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSiseSecteurDisciplinaire.fetchRequiredSco_SiseSecteurDisciplinaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSiseSecteurDisciplinaire fetchRequiredSco_SiseSecteurDisciplinaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSiseSecteurDisciplinaire eoObject = _EOSiseSecteurDisciplinaire.fetchSco_SiseSecteurDisciplinaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SiseSecteurDisciplinaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSiseSecteurDisciplinaire localInstanceIn(EOEditingContext editingContext, EOSiseSecteurDisciplinaire eo) {
    EOSiseSecteurDisciplinaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
