// DO NOT EDIT.  Make changes to EOLien.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOLien extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Lien";

  // Attribute Keys
  public static final ERXKey<Integer> CHILD_ID = new ERXKey<Integer>("childId");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PARENT_ID = new ERXKey<Integer>("parentId");
  public static final ERXKey<Integer> TYPE_LIEN_ID = new ERXKey<Integer>("typeLienId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> _CHILD_PARCOURS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("_childParcours");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode> _CHILD_PERIODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode>("_childPeriode");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> _CHILD_VERSION_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("_childVersionDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> _PARENT_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("_parentDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> _PARENT_PARCOURS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("_parentParcours");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> _PARENT_VERSION_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("_parentVersionDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> CHARGE_ENSEIGNEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>("chargeEnseignements");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> CHILD = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("child");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> PARENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("parent");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien> TYPE_LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien>("typeLien");

  // Attributes
  public static final String CHILD_ID_KEY = CHILD_ID.key();
  public static final String ID_KEY = ID.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PARENT_ID_KEY = PARENT_ID.key();
  public static final String TYPE_LIEN_ID_KEY = TYPE_LIEN_ID.key();
  // Relationships
  public static final String _CHILD_PARCOURS_KEY = _CHILD_PARCOURS.key();
  public static final String _CHILD_PERIODE_KEY = _CHILD_PERIODE.key();
  public static final String _CHILD_VERSION_DIPLOME_KEY = _CHILD_VERSION_DIPLOME.key();
  public static final String _PARENT_DIPLOME_KEY = _PARENT_DIPLOME.key();
  public static final String _PARENT_PARCOURS_KEY = _PARENT_PARCOURS.key();
  public static final String _PARENT_VERSION_DIPLOME_KEY = _PARENT_VERSION_DIPLOME.key();
  public static final String CHARGE_ENSEIGNEMENTS_KEY = CHARGE_ENSEIGNEMENTS.key();
  public static final String CHILD_KEY = CHILD.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String PARENT_KEY = PARENT.key();
  public static final String TYPE_LIEN_KEY = TYPE_LIEN.key();

  private static Logger LOG = Logger.getLogger(_EOLien.class);

  public EOLien localInstanceIn(EOEditingContext editingContext) {
    EOLien localInstance = (EOLien)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer childId() {
    return (Integer) storedValueForKey(_EOLien.CHILD_ID_KEY);
  }

  public void setChildId(Integer value) {
    if (_EOLien.LOG.isDebugEnabled()) {
    	_EOLien.LOG.debug( "updating childId from " + childId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLien.CHILD_ID_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOLien.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOLien.LOG.isDebugEnabled()) {
    	_EOLien.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLien.ID_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOLien.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOLien.LOG.isDebugEnabled()) {
    	_EOLien.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLien.ORDRE_KEY);
  }

  public Integer parentId() {
    return (Integer) storedValueForKey(_EOLien.PARENT_ID_KEY);
  }

  public void setParentId(Integer value) {
    if (_EOLien.LOG.isDebugEnabled()) {
    	_EOLien.LOG.debug( "updating parentId from " + parentId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLien.PARENT_ID_KEY);
  }

  public Integer typeLienId() {
    return (Integer) storedValueForKey(_EOLien.TYPE_LIEN_ID_KEY);
  }

  public void setTypeLienId(Integer value) {
    if (_EOLien.LOG.isDebugEnabled()) {
    	_EOLien.LOG.debug( "updating typeLienId from " + typeLienId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLien.TYPE_LIEN_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours _childParcours() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(_EOLien._CHILD_PARCOURS_KEY);
  }
  
  public void set_ChildParcours(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    takeStoredValueForKey(value, _EOLien._CHILD_PARCOURS_KEY);
  }

  public void set_ChildParcoursRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _childParcours from " + _childParcours() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ChildParcours(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = _childParcours();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._CHILD_PARCOURS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._CHILD_PARCOURS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode _childPeriode() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode)storedValueForKey(_EOLien._CHILD_PERIODE_KEY);
  }
  
  public void set_ChildPeriode(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode value) {
    takeStoredValueForKey(value, _EOLien._CHILD_PERIODE_KEY);
  }

  public void set_ChildPeriodeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _childPeriode from " + _childPeriode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ChildPeriode(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode oldValue = _childPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._CHILD_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._CHILD_PERIODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome _childVersionDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome)storedValueForKey(_EOLien._CHILD_VERSION_DIPLOME_KEY);
  }
  
  public void set_ChildVersionDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome value) {
    takeStoredValueForKey(value, _EOLien._CHILD_VERSION_DIPLOME_KEY);
  }

  public void set_ChildVersionDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _childVersionDiplome from " + _childVersionDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ChildVersionDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome oldValue = _childVersionDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._CHILD_VERSION_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._CHILD_VERSION_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome _parentDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome)storedValueForKey(_EOLien._PARENT_DIPLOME_KEY);
  }
  
  public void set_ParentDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    takeStoredValueForKey(value, _EOLien._PARENT_DIPLOME_KEY);
  }

  public void set_ParentDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _parentDiplome from " + _parentDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ParentDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome oldValue = _parentDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._PARENT_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._PARENT_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours _parentParcours() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(_EOLien._PARENT_PARCOURS_KEY);
  }
  
  public void set_ParentParcours(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    takeStoredValueForKey(value, _EOLien._PARENT_PARCOURS_KEY);
  }

  public void set_ParentParcoursRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _parentParcours from " + _parentParcours() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ParentParcours(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = _parentParcours();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._PARENT_PARCOURS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._PARENT_PARCOURS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome _parentVersionDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome)storedValueForKey(_EOLien._PARENT_VERSION_DIPLOME_KEY);
  }
  
  public void set_ParentVersionDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome value) {
    takeStoredValueForKey(value, _EOLien._PARENT_VERSION_DIPLOME_KEY);
  }

  public void set_ParentVersionDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating _parentVersionDiplome from " + _parentVersionDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	set_ParentVersionDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome oldValue = _parentVersionDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien._PARENT_VERSION_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien._PARENT_VERSION_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant child() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOLien.CHILD_KEY);
  }
  
  public void setChild(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOLien.CHILD_KEY);
  }

  public void setChildRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating child from " + child() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setChild(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = child();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien.CHILD_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien.CHILD_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant parent() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOLien.PARENT_KEY);
  }
  
  public void setParent(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOLien.PARENT_KEY);
  }

  public void setParentRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating parent from " + parent() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setParent(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = parent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien.PARENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien.PARENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien typeLien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien)storedValueForKey(_EOLien.TYPE_LIEN_KEY);
  }
  
  public void setTypeLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien value) {
    takeStoredValueForKey(value, _EOLien.TYPE_LIEN_KEY);
  }

  public void setTypeLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien value) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("updating typeLien from " + typeLien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien oldValue = typeLien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOLien.TYPE_LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOLien.TYPE_LIEN_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)storedValueForKey(_EOLien.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier) {
    return chargeEnseignements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier, boolean fetch) {
    return chargeEnseignements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.TO_LIEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.fetchSco_ChargeEnseignements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = chargeEnseignements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToChargeEnseignements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    includeObjectIntoPropertyWithKey(object, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public void removeFromChargeEnseignements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    excludeObjectFromPropertyWithKey(object, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public void addToChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("adding " + object + " to chargeEnseignements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToChargeEnseignements(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
    }
  }

  public void removeFromChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("removing " + object + " from chargeEnseignements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromChargeEnseignements(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement createChargeEnseignementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement) eo;
  }

  public void deleteChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.CHARGE_ENSEIGNEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllChargeEnseignementsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> objects = chargeEnseignements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteChargeEnseignementsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)storedValueForKey(_EOLien.CREDITABLES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier) {
    return creditables(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier, boolean fetch) {
    return creditables(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.LIEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.fetchSco_Creditables(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = creditables();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCreditables(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    includeObjectIntoPropertyWithKey(object, _EOLien.CREDITABLES_KEY);
  }

  public void removeFromCreditables(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    excludeObjectFromPropertyWithKey(object, _EOLien.CREDITABLES_KEY);
  }

  public void addToCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("adding " + object + " to creditables relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToCreditables(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOLien.CREDITABLES_KEY);
    }
  }

  public void removeFromCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("removing " + object + " from creditables relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromCreditables(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.CREDITABLES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable createCreditablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOLien.CREDITABLES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable) eo;
  }

  public void deleteCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.CREDITABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCreditablesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> objects = creditables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCreditablesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)storedValueForKey(_EOLien.DESCRIPTIFS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs(EOQualifier qualifier) {
    return descriptifs(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> results;
      results = descriptifs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDescriptifs(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    includeObjectIntoPropertyWithKey(object, _EOLien.DESCRIPTIFS_KEY);
  }

  public void removeFromDescriptifs(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    excludeObjectFromPropertyWithKey(object, _EOLien.DESCRIPTIFS_KEY);
  }

  public void addToDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("adding " + object + " to descriptifs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDescriptifs(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOLien.DESCRIPTIFS_KEY);
    }
  }

  public void removeFromDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    if (_EOLien.LOG.isDebugEnabled()) {
      _EOLien.LOG.debug("removing " + object + " from descriptifs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDescriptifs(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.DESCRIPTIFS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif createDescriptifsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOLien.DESCRIPTIFS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif) eo;
  }

  public void deleteDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOLien.DESCRIPTIFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDescriptifsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> objects = descriptifs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDescriptifsRelationship(objects.nextElement());
    }
  }


  public static EOLien createSco_Lien(EOEditingContext editingContext, Integer id
) {
    EOLien eo = (EOLien) EOUtilities.createAndInsertInstance(editingContext, _EOLien.ENTITY_NAME);    
		eo.setId(id);
    return eo;
  }

  public static ERXFetchSpecification<EOLien> fetchSpec() {
    return new ERXFetchSpecification<EOLien>(_EOLien.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOLien> fetchAllSco_Liens(EOEditingContext editingContext) {
    return _EOLien.fetchAllSco_Liens(editingContext, null);
  }

  public static NSArray<EOLien> fetchAllSco_Liens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLien.fetchSco_Liens(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLien> fetchSco_Liens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOLien> fetchSpec = new ERXFetchSpecification<EOLien>(_EOLien.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLien> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOLien fetchSco_Lien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLien.fetchSco_Lien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLien fetchSco_Lien(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLien> eoObjects = _EOLien.fetchSco_Liens(editingContext, qualifier, null);
    EOLien eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Lien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLien fetchRequiredSco_Lien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLien.fetchRequiredSco_Lien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLien fetchRequiredSco_Lien(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLien eoObject = _EOLien.fetchSco_Lien(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Lien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLien localInstanceIn(EOEditingContext editingContext, EOLien eo) {
    EOLien localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
