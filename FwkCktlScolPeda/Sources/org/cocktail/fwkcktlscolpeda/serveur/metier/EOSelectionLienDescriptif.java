package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EOSelectionLienDescriptif extends _EOSelectionLienDescriptif {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOSelectionLienDescriptif.class);
	
	public String toString() {
		return this.lien() + " sélectionne le descriptif " + descriptif();
	}
	
}
