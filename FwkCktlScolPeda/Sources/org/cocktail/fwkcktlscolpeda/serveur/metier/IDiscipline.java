package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 *  Discipline du composant
 */
public interface IDiscipline {
	 /**
	 * @return  validaté de la discipline
	 */
	//TODO redondant avec annee début et année fin
	Boolean validite();
	
	/**
	 * @return début de la période de validité
	 */
	Integer annee_debut();
	
	/**
	 * @return fin de la période de validité
	 */
	Integer annee_fin();
	
	/**
	 * @return code de la discipline
	 */
	String code();
	
	/**
	 * @return index primaire
	 */
	Integer id();
	
	/**
	 * @return libelle long
	 */
	String libelleLong();
	
	/**
	 * @return libelle court
	 */
	String libelleCourt();
	
}
