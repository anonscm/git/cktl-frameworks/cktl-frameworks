package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Représente un grade universitaire
 */
public interface IGradeUniversitaire {
	String GRADE_BAC = "BAC";
	String GRADE_LICENCE = "L";
	String GRADE_MASTER = "M";
	String GRADE_DOCTORAT = "D";
	String GRADE_SANS = "SANS";

	/**
	 * @return le type de grade universitaire
	 */
	String type();

	/**
	 * @return libelle du grade universitaire
	 */
	String libelle();

	/**
	 * @return le nombre d'unités de temps
	 */
	Integer nbUnitesTemps();

	/**
	 * @return true si c'est un grade L, M ou D
	 */
	boolean isLMD();
	
	/**
	 * @return true si c'est le type licence
	 */
	boolean isLicence();
	
	/**
	 * @return true si c'est le type doctorat
	 */
	boolean isDoctorat();
	
	/**
	 * @return true si c'est le type bac
	 */
	boolean isBac();	
}