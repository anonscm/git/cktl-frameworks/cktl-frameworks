package org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Cette classe représente le dépôt des {@link IStructure} utilisées conjointement par la scolarité et par Peche.
 * <p>
 * Un cache est géré par instance.
 * Vous pouvez garder une instance en mémoire, le ré-appel d'une méthode pour une instance ne coûte rien.
 * 
 * @author Pascal MACOUIN
 */
public class StructureRepositoryEOF implements StructureRepository {

	private EOEditingContext editingContext;
	private List<IStructure> listeStructuresEnseignement;
	private List<IStructure> listeComposantesScolarites;
	private List<IStructure> listeDepartementsEnseignements;
	private List<IStructure> listeDepartementsEnseignementsDesComposantesScolarites;
	private Map<String, List<IStructure>> mapListeComposantesScolarites = new HashMap<String, List<IStructure>>();
	private Map<String, List<IStructure>> mapListeDepartementsEnseignements = new HashMap<String, List<IStructure>>();
	private Map<String, List<IStructure>> mapListeStructuresEnseignement = new HashMap<String, List<IStructure>>();
	
	private static Comparator<IStructure> comparateurLibelleCourtAsc = new Comparator<IStructure>() {
		public int compare(IStructure s1, IStructure s2) {
			return compareTo(s1.lcStructure(), s2.lcStructure());
		}
	};
	private static Comparator<IStructure> comparateurLibelleLongAsc = new Comparator<IStructure>() {
		public int compare(IStructure s1, IStructure s2) {
			return compareTo(s1.llStructure(), s2.llStructure());
		}
	};
	
	/**
	 * Constructeur.
	 * 
	 * @param editingContext Un editing context
	 */
	public StructureRepositoryEOF(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getStructuresAffectablesAuxComposants() {
		return getComposantesScolarites();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getStructuresAffectablesAuxComposants(IStructure structuresPere) {
		return getComposantesScolarites(structuresPere);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getComposantesScolarites() {
		if (listeComposantesScolarites == null) {
			listeComposantesScolarites = new ArrayList<IStructure>(EOStructureForGroupeSpec.rechercherGroupes(editingContext, getQualifierComposanteScolarite(), 0, true));
		}
		
		return listeComposantesScolarites;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getComposantesScolarites(IStructure structuresPere) {
		if (!mapListeComposantesScolarites.containsKey(structuresPere.cStructure())) {
			List<IStructure> listeComposantesScolaritesDescendantes = new ArrayList<IStructure>();
			
			if (structuresPere.isRacine()) {
				listeComposantesScolaritesDescendantes.addAll(getComposantesScolarites());
			} else {
				listeComposantesScolaritesDescendantes.addAll(fetchStructureAvecEnfantsWithSql(structuresPere, EOTypeGroupe.TGRP_CODE_CS));
			}
			
			mapListeComposantesScolarites.put(structuresPere.cStructure(), listeComposantesScolaritesDescendantes);
		}
		
		return mapListeComposantesScolarites.get(structuresPere.cStructure());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getDepartementsEnseignementsDesComposantesScolarites() {
		if (listeDepartementsEnseignementsDesComposantesScolarites == null) {
			Set<IStructure> setDepartementsEnseignements = new HashSet<IStructure>();
			for (IStructure structure : getComposantesScolarites()) {
				setDepartementsEnseignements.addAll(getDepartementsEnseignements(structure));
			}
			listeDepartementsEnseignementsDesComposantesScolarites = new ArrayList<IStructure>(setDepartementsEnseignements);
		}
		
		return listeDepartementsEnseignementsDesComposantesScolarites;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getDepartementsEnseignements() {
		if (listeDepartementsEnseignements == null) {
			listeDepartementsEnseignements = new ArrayList<IStructure>(EOStructureForGroupeSpec.rechercherGroupes(editingContext, getQualifierGroupeDepartementEnseignement(), 0, true));
		}
		
		return listeDepartementsEnseignements;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getDepartementsEnseignements(IStructure structuresPere) {
		if (!mapListeDepartementsEnseignements.containsKey(structuresPere.cStructure())) {
			List<IStructure> listeDepartementsEnseignementsDescendants = new ArrayList<IStructure>();
			
			if (structuresPere.isRacine()) {
				listeDepartementsEnseignementsDescendants.addAll(getDepartementsEnseignements());
			} else {
				listeDepartementsEnseignementsDescendants.addAll(fetchStructureAvecEnfantsWithSql(structuresPere, EOTypeGroupe.TGRP_CODE_DE));
			}
			
			mapListeDepartementsEnseignements.put(structuresPere.cStructure(), listeDepartementsEnseignementsDescendants);
		}
	
		return mapListeDepartementsEnseignements.get(structuresPere.cStructure());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getStructuresEnseignement() {
		if (listeStructuresEnseignement == null) {
			listeStructuresEnseignement = new ArrayList<IStructure>(EOStructureForGroupeSpec.rechercherGroupes(editingContext, getQualifierComposanteScolarite(), 0, true));
			listeStructuresEnseignement.addAll(new ArrayList<IStructure>(EOStructureForGroupeSpec.rechercherGroupes(editingContext, getQualifierGroupeDepartementEnseignement(), 0, true)));
		}
		
		return listeStructuresEnseignement;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IStructure> getStructuresEnseignement(IStructure structuresPere) {
		if (!mapListeStructuresEnseignement.containsKey(structuresPere.cStructure())) {
			List<IStructure> listeComposantesScolaritesDescendantes = new ArrayList<IStructure>();
			listeComposantesScolaritesDescendantes.addAll(getComposantesScolarites(structuresPere));
			listeComposantesScolaritesDescendantes.addAll(getDepartementsEnseignements(structuresPere));
			mapListeStructuresEnseignement.put(structuresPere.cStructure(), listeComposantesScolaritesDescendantes);
		}
		
		return mapListeStructuresEnseignement.get(structuresPere.cStructure());
	}

	/**
	 * {@inheritDoc}
	 */
	public Comparator<IStructure> getComparateurLibelleCourtAsc() {
		return comparateurLibelleCourtAsc;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Comparator<IStructure> getComparateurLibelleLongAsc() {
		return comparateurLibelleLongAsc;
	}
	
	/**
	 * @return Un qualifier pour la recherche des composantes scolarité
	 */
	private EOQualifier getQualifierComposanteScolarite() {
		EOQualifier qualifier = 
				ERXQ.and(
						EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).containsObject(EOTypeGroupe.TGRP_CODE_CS),
						EOStructure.QUAL_STRUCTURES_VALIDE);
		
		return qualifier;
	}
	
	/**
	 * @return Un qualifier pour la recherche des départements d'enseignements
	 */
	private EOQualifier getQualifierGroupeDepartementEnseignement() {
		EOQualifier qualifier = 
				ERXQ.and(
						EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).containsObject(EOTypeGroupe.TGRP_CODE_DE),
						EOStructure.QUAL_STRUCTURES_VALIDE);
		
		return qualifier;
	}

	/**
	 * @param listeStructures Une liste de structures
	 * @param typeGroupe Un type de groupe
	 * @return Les structures ayant ce type de groupe
	 */
	public List<IStructure> retournerStructuresDeTypeGroupe(List<IStructure> listeStructures, String typeGroupe) {
		ArrayList<IStructure> listeStructuresDeTypeGroupe = new ArrayList<IStructure>();
		
		for (IStructure structure : listeStructures) {
			if (isStructureDeTypeGroupe(structure, typeGroupe)) {
				listeStructuresDeTypeGroupe.add(structure);
			}
		}
		
		return listeStructuresDeTypeGroupe;
	}
	
	/**
	 * @param structure Une structure
	 * @param typeGroupe Un type de groupe
	 * @return <code>true</code> si la structure est de type groupe spécifié
	 */
	private boolean isStructureDeTypeGroupe(IStructure structure, String typeGroupe) {
		if (structure != null) {
			return ((EOStructure) structure).toRepartTypeGroupes(EOTypeGroupe.TGRP_CODE.eq(typeGroupe)).size() > 0;
		}
		return false;
	}
	
	/**
	 *  Recherche de la liste de toutes les structures filles avec le type de groupe demandé. 
	 *  	=> Utilisation du rawRow pour améliorer les performances
	 * @param structuresPere La structure racine à partir de laquelle chercher (cette structure fait partie de la recherche)
	 * @return liste de structure avec le type de groupe passé en paramètre
	 */
	@SuppressWarnings("unchecked")
	private List<IStructure> fetchStructureAvecEnfantsWithSql(IStructure structurePere, String typeGroupe) {
		List<IStructure> liste = new ArrayList<IStructure>();
		
		String sql = "SELECT distinct (s.C_STRUCTURE) FROM STRUCTURE_ULR s, REPART_TYPE_GROUPE rtg"
						+ " WHERE s.C_STRUCTURE = rtg.C_STRUCTURE"
						+ " AND rtg.TGRP_CODE = '" + typeGroupe + "'"
						+ " AND s.TEM_VALIDE = 'O'"
						+ " START WITH s.C_STRUCTURE_PERE = " + structurePere.cStructure() + " OR s.C_STRUCTURE = " + structurePere.cStructure()
						+ " CONNECT BY s.C_STRUCTURE_PERE = PRIOR s.C_STRUCTURE \n";
		
		EOEntity entity = EOUtilities.entityForClass(editingContext, EOStructure.class);
		NSArray<?> rows = EOUtilities.rawRowsForSQL(editingContext, entity.model().name(), sql, null);
		
		if (!NSArrayCtrl.isEmpty(rows)) {
			NSArray<String> resultat = (NSArray<String>) rows.valueForKey((EOStructure.C_STRUCTURE_COLKEY));
			NSArray<EOStructure> listeStruc = EOStructure.fetchAll(editingContext, ERXQ.in(EOStructure.C_STRUCTURE_KEY, resultat));
			liste.addAll(listeStruc);
		}
		return liste;
	}

	/**
	 * Un {@link String#compareTo(String)} "null safe".
	 * @param s1 Première chaîne
	 * @param s2 Seconde chaîne
	 * @return idem {@link String#compareTo(String)}
	 */
	private static int compareTo(String s1, String s2) {
		if (s1 == null && s2 == null) {
			return 0;
		} else if (s1 == null) {
			return -1;
		} else if (s2 == null) {
			return 1;
		}
		
		return s1.compareTo(s2);
	};
}
