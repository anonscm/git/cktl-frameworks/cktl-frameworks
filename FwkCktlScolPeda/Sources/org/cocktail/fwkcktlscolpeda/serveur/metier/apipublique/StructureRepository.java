package org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique;

import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

/**
 * Cette interface représente le dépôt des {@link IStructure} utilisées conjointement par la scolarité et par Peche.
 * <p>
 * Ne pas présupposer de tri sur les retours d'aucune de ces méthodes. Utiliser les comparateurs (getComparateur...()) pour trier les résultats.
 * 
 * @author Pascal MACOUIN
 */
public interface StructureRepository {
	/**
	 * Retourne la liste des structures affectables aux composants (Diplôme, UE, EC) d'une maquette de la scolarité.
	 * @return Une liste de structures affectables aux composants d'une maquette
	 */
	List<IStructure> getStructuresAffectablesAuxComposants();
	
	/**
	 * Retourne la liste des structures affectables aux composants (Diplôme, UE, EC) d'une maquette de la scolarité.
	 * @param structuresPere La structure racine à partir de laquelle chercher (cette structure fait partie de la recherche)
	 * @return Une liste de structure affectable aux composants d'une maquette
	 */
	List<IStructure> getStructuresAffectablesAuxComposants(IStructure structuresPere);
	
	/**
	 * Retourne la liste des départements d'enseignements ayant comme ascendants au moins une composante scolarité (structures ayant un type groupe "Département d'Enseignement")
	 * @return Une liste de département d'enseignement
	 */
	List<IStructure> getDepartementsEnseignementsDesComposantesScolarites();
	
	/**
	 * Retourne la liste des composantes scolarités (structures ayant un type de groupe "Composante Scolarité").
	 * @return Une liste de composante scolarité
	 */
	List<IStructure> getComposantesScolarites();
	
	/**
	 * Retourne la liste des composantes scolarités (structures ayant un type de groupe "Composante Scolarité").
	 * @param structuresPere La structure racine à partir de laquelle chercher (cette structure fait partie de la recherche)
	 * @return Une liste de composante scolarité
	 */
	List<IStructure> getComposantesScolarites(IStructure structuresPere);
	
	/**
	 * Retourne la liste des départements d'enseignements (structures ayant un type de groupe "Département d'Enseignement").
	 * @return Une liste de département d'enseignement
	 */
	List<IStructure> getDepartementsEnseignements();
	
	/**
	 * Retourne la liste des départements d'enseignements (structures ayant un type de groupe "Département d'Enseignement").
	 * @param structuresPere La structure racine à partir de laquelle chercher (cette structure fait partie de la recherche)
	 * @return Une liste de département d'enseignement
	 */
	List<IStructure> getDepartementsEnseignements(IStructure structuresPere);
	
	/**
	 * Retourne la liste des structures d'enseignement (structures ayant un type de groupe "Département d'Enseignement" ou  "Composante Scolarité").
	 * @return Une liste des structures d'enseignement (Composante de scolarité et Département d'enseignement)
	 */
	List<IStructure> getStructuresEnseignement();
	
	/**
	 * Retourne la liste des structures d'enseignement (structures ayant un type de groupe "Département d'Enseignement" ou  "Composante Scolarité").
	 * @param structuresPere La structure racine à partir de laquelle chercher (cette structure fait partie de la recherche)
	 * @return Une liste des structures d'enseignement (Composante de scolarité et Département d'enseignement)
	 */
	List<IStructure> getStructuresEnseignement(IStructure structuresPere);
	
	/**
	 * Retourne un comparateur pour trier les structures sur le libellé court ascendant.
	 * @return Un comparateur pour trier les structures sur le libellé court ascendant
	 */
	Comparator<IStructure> getComparateurLibelleCourtAsc();
	
	/**
	 * Retourne un comparateur pour trier les structures sur le libellé long ascendant.
	 * @return Un comparateur pour trier les structures sur le libellé long ascendant
	 */
	Comparator<IStructure> getComparateurLibelleLongAsc();
	
	/**
	 * @param listeStructures Une liste de structures
	 * @param typeGroupe Un type de groupe
	 * @return Les structures ayant ce type de groupe
	 */
	List<IStructure> retournerStructuresDeTypeGroupe(List<IStructure> listeStructures, String typeGroupe);
}
