package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEcoleDoctorale;

import com.webobjects.foundation.NSArray;

/**
 * Diplome
 */
public interface IDiplome extends IComposant {
	/**
	 * @return grade universitaires
	 */
	IGradeUniversitaire gradeUniversitaire();

	/**
	 * @param annee annee pour laquelle on cherche les versions de diplomes
	 * @return les versions de diplomes pour une annee donnee
	 */
	NSArray<IVersionDiplome> getVersionsDiplome(Integer annee);

	/**
	 * {@inheritDoc}
	 */
	IVersionDiplome getVersionDiplome(Integer annee);

	/**
	 * @param annee annee pour laquelle on recher la veriosn de diplome
	 * @return lien qui rattache le diplome à sa version principale
	 */
	ILien getVersionDiplomeAsLien(Integer annee);

	/**
	 * @return statut de la formation
	 */
	IStatutFormation statutFormation();

	/**
	 * @return <code>true</code> si une habilitation est requise pour ce diplome
	 */
	Boolean isHabilitationRequise();

	/**
	 * @return type de la formation
	 */
	ITypeFormation typeFormation();

	/**
	 * @return vocation du diplome
	 */
	IVocation vocation();

	/**
	 * @return specialite du diplome
	 */
	ISpecialite specialite();

	/**
	 * @return mention d'un diplome
	 */
	IMention mention();

	/**
	 * @return true si c'est un diplome LMD
	 */
	boolean isLMD();

	/**
	 * @return true si le diplome est une licence
	 */
	boolean isLicence();

	/**
	 * 
	 * @return le niveauAccesDiplome
	 */
	INiveauAccesDiplome niveauAccesDiplome();

	/**
	 * 
	 * @return le secteur disciplinaire sise
	 */
	ISiseSecteurDisciplinaire secteurDisciplinaireSise();

	/**
	 * @return version texte du code sise
	 */
	public String getSiseStr();
	
	/**
	 * @return L'école doctorale si le grade est "doctorat"
	 */
	IEcoleDoctorale ecoleDoctorale();
	
	/**
	 * @param uneEcoleDoctorale Une école doctorale si le grade est "doctorat"
	 */
	void setEcoleDoctoraleRelationship(IEcoleDoctorale uneEcoleDoctorale);
}
