package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * Cette classe représente une pièce justificative que l'étudiant doit fournir à l'établissement.
 * 
 * @author Pascal MACOUIN
 */
public class EOPieceJustificative extends _EOPieceJustificative implements IPieceJustificative {
	private static final long serialVersionUID = -6065837336094066001L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPieceJustificative.class);
}
