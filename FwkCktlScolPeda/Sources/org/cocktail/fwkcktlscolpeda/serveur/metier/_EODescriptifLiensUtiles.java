// DO NOT EDIT.  Make changes to EODescriptifLiensUtiles.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODescriptifLiensUtiles extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DescriptifLiensUtiles";

  // Attribute Keys
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<String> TITRE = new ERXKey<String>("titre");
  public static final ERXKey<String> URL = new ERXKey<String>("URL");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptif");

  // Attributes
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String TITRE_KEY = TITRE.key();
  public static final String URL_KEY = URL.key();
  // Relationships
  public static final String DESCRIPTIF_KEY = DESCRIPTIF.key();

  private static Logger LOG = Logger.getLogger(_EODescriptifLiensUtiles.class);

  public EODescriptifLiensUtiles localInstanceIn(EOEditingContext editingContext) {
    EODescriptifLiensUtiles localInstance = (EODescriptifLiensUtiles)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EODescriptifLiensUtiles.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EODescriptifLiensUtiles.LOG.isDebugEnabled()) {
    	_EODescriptifLiensUtiles.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifLiensUtiles.ORDRE_KEY);
  }

  public String titre() {
    return (String) storedValueForKey(_EODescriptifLiensUtiles.TITRE_KEY);
  }

  public void setTitre(String value) {
    if (_EODescriptifLiensUtiles.LOG.isDebugEnabled()) {
    	_EODescriptifLiensUtiles.LOG.debug( "updating titre from " + titre() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifLiensUtiles.TITRE_KEY);
  }

  public String URL() {
    return (String) storedValueForKey(_EODescriptifLiensUtiles.URL_KEY);
  }

  public void setURL(String value) {
    if (_EODescriptifLiensUtiles.LOG.isDebugEnabled()) {
    	_EODescriptifLiensUtiles.LOG.debug( "updating URL from " + URL() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifLiensUtiles.URL_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif descriptif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif)storedValueForKey(_EODescriptifLiensUtiles.DESCRIPTIF_KEY);
  }
  
  public void setDescriptif(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    takeStoredValueForKey(value, _EODescriptifLiensUtiles.DESCRIPTIF_KEY);
  }

  public void setDescriptifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif value) {
    if (_EODescriptifLiensUtiles.LOG.isDebugEnabled()) {
      _EODescriptifLiensUtiles.LOG.debug("updating descriptif from " + descriptif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDescriptif(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif oldValue = descriptif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptifLiensUtiles.DESCRIPTIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptifLiensUtiles.DESCRIPTIF_KEY);
    }
  }
  

  public static EODescriptifLiensUtiles createSco_DescriptifLiensUtiles(EOEditingContext editingContext, String titre
, String URL
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif descriptif) {
    EODescriptifLiensUtiles eo = (EODescriptifLiensUtiles) EOUtilities.createAndInsertInstance(editingContext, _EODescriptifLiensUtiles.ENTITY_NAME);    
		eo.setTitre(titre);
		eo.setURL(URL);
    eo.setDescriptifRelationship(descriptif);
    return eo;
  }

  public static ERXFetchSpecification<EODescriptifLiensUtiles> fetchSpec() {
    return new ERXFetchSpecification<EODescriptifLiensUtiles>(_EODescriptifLiensUtiles.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODescriptifLiensUtiles> fetchAllSco_DescriptifLiensUtileses(EOEditingContext editingContext) {
    return _EODescriptifLiensUtiles.fetchAllSco_DescriptifLiensUtileses(editingContext, null);
  }

  public static NSArray<EODescriptifLiensUtiles> fetchAllSco_DescriptifLiensUtileses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODescriptifLiensUtiles.fetchSco_DescriptifLiensUtileses(editingContext, null, sortOrderings);
  }

  public static NSArray<EODescriptifLiensUtiles> fetchSco_DescriptifLiensUtileses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODescriptifLiensUtiles> fetchSpec = new ERXFetchSpecification<EODescriptifLiensUtiles>(_EODescriptifLiensUtiles.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODescriptifLiensUtiles> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODescriptifLiensUtiles fetchSco_DescriptifLiensUtiles(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifLiensUtiles.fetchSco_DescriptifLiensUtiles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifLiensUtiles fetchSco_DescriptifLiensUtiles(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODescriptifLiensUtiles> eoObjects = _EODescriptifLiensUtiles.fetchSco_DescriptifLiensUtileses(editingContext, qualifier, null);
    EODescriptifLiensUtiles eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DescriptifLiensUtiles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifLiensUtiles fetchRequiredSco_DescriptifLiensUtiles(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifLiensUtiles.fetchRequiredSco_DescriptifLiensUtiles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifLiensUtiles fetchRequiredSco_DescriptifLiensUtiles(EOEditingContext editingContext, EOQualifier qualifier) {
    EODescriptifLiensUtiles eoObject = _EODescriptifLiensUtiles.fetchSco_DescriptifLiensUtiles(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DescriptifLiensUtiles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifLiensUtiles localInstanceIn(EOEditingContext editingContext, EODescriptifLiensUtiles eo) {
    EODescriptifLiensUtiles localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
