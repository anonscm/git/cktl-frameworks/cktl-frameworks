package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * 
 * Etat de l'examen : programmé, convoqué, liste établie
 */
public interface IExamenEtat {
	String EX_PROGRAMME = "PROG";
	String EX_CONVOQUE = "CONV";
	String EX_LISTE_ETABLIE = "LE";

	/**
	 * @return code
	 */
	String code();

	/**
	 * @return libelle 
	 */
	String libelle();

}