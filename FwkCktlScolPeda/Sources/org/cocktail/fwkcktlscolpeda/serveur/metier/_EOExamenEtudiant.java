// DO NOT EDIT.  Make changes to EOExamenEtudiant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOExamenEtudiant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ExamenEtudiant";

  // Attribute Keys
  public static final ERXKey<Boolean> CONVOQUE = new ERXKey<Boolean>("convoque");
  public static final ERXKey<Integer> D_CREATION = new ERXKey<Integer>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee> TO_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee>("toEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation> TO_EXAMEN_ORGANISATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation>("toExamenOrganisation");

  // Attributes
  public static final String CONVOQUE_KEY = CONVOQUE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships
  public static final String TO_ETUDIANT_ANNEE_KEY = TO_ETUDIANT_ANNEE.key();
  public static final String TO_EXAMEN_ORGANISATION_KEY = TO_EXAMEN_ORGANISATION.key();

  private static Logger LOG = Logger.getLogger(_EOExamenEtudiant.class);

  public EOExamenEtudiant localInstanceIn(EOEditingContext editingContext) {
    EOExamenEtudiant localInstance = (EOExamenEtudiant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean convoque() {
    return (Boolean) storedValueForKey(_EOExamenEtudiant.CONVOQUE_KEY);
  }

  public void setConvoque(Boolean value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
    	_EOExamenEtudiant.LOG.debug( "updating convoque from " + convoque() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtudiant.CONVOQUE_KEY);
  }

  public Integer dCreation() {
    return (Integer) storedValueForKey(_EOExamenEtudiant.D_CREATION_KEY);
  }

  public void setDCreation(Integer value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
    	_EOExamenEtudiant.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtudiant.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOExamenEtudiant.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
    	_EOExamenEtudiant.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtudiant.D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOExamenEtudiant.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
    	_EOExamenEtudiant.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtudiant.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOExamenEtudiant.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
    	_EOExamenEtudiant.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenEtudiant.PERS_ID_MODIFCATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee)storedValueForKey(_EOExamenEtudiant.TO_ETUDIANT_ANNEE_KEY);
  }
  
  public void setToEtudiantAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    takeStoredValueForKey(value, _EOExamenEtudiant.TO_ETUDIANT_ANNEE_KEY);
  }

  public void setToEtudiantAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
      _EOExamenEtudiant.LOG.debug("updating toEtudiantAnnee from " + toEtudiantAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiantAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee oldValue = toEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamenEtudiant.TO_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamenEtudiant.TO_ETUDIANT_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation toExamenOrganisation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation)storedValueForKey(_EOExamenEtudiant.TO_EXAMEN_ORGANISATION_KEY);
  }
  
  public void setToExamenOrganisation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation value) {
    takeStoredValueForKey(value, _EOExamenEtudiant.TO_EXAMEN_ORGANISATION_KEY);
  }

  public void setToExamenOrganisationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation value) {
    if (_EOExamenEtudiant.LOG.isDebugEnabled()) {
      _EOExamenEtudiant.LOG.debug("updating toExamenOrganisation from " + toExamenOrganisation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToExamenOrganisation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation oldValue = toExamenOrganisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamenEtudiant.TO_EXAMEN_ORGANISATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamenEtudiant.TO_EXAMEN_ORGANISATION_KEY);
    }
  }
  

  public static EOExamenEtudiant createSco_ExamenEtudiant(EOEditingContext editingContext, Boolean convoque
, Integer dCreation
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee, org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenOrganisation toExamenOrganisation) {
    EOExamenEtudiant eo = (EOExamenEtudiant) EOUtilities.createAndInsertInstance(editingContext, _EOExamenEtudiant.ENTITY_NAME);    
		eo.setConvoque(convoque);
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToEtudiantAnneeRelationship(toEtudiantAnnee);
    eo.setToExamenOrganisationRelationship(toExamenOrganisation);
    return eo;
  }

  public static ERXFetchSpecification<EOExamenEtudiant> fetchSpec() {
    return new ERXFetchSpecification<EOExamenEtudiant>(_EOExamenEtudiant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOExamenEtudiant> fetchAllSco_ExamenEtudiants(EOEditingContext editingContext) {
    return _EOExamenEtudiant.fetchAllSco_ExamenEtudiants(editingContext, null);
  }

  public static NSArray<EOExamenEtudiant> fetchAllSco_ExamenEtudiants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOExamenEtudiant.fetchSco_ExamenEtudiants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOExamenEtudiant> fetchSco_ExamenEtudiants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOExamenEtudiant> fetchSpec = new ERXFetchSpecification<EOExamenEtudiant>(_EOExamenEtudiant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOExamenEtudiant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOExamenEtudiant fetchSco_ExamenEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenEtudiant.fetchSco_ExamenEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenEtudiant fetchSco_ExamenEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOExamenEtudiant> eoObjects = _EOExamenEtudiant.fetchSco_ExamenEtudiants(editingContext, qualifier, null);
    EOExamenEtudiant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ExamenEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenEtudiant fetchRequiredSco_ExamenEtudiant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenEtudiant.fetchRequiredSco_ExamenEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenEtudiant fetchRequiredSco_ExamenEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOExamenEtudiant eoObject = _EOExamenEtudiant.fetchSco_ExamenEtudiant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ExamenEtudiant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenEtudiant localInstanceIn(EOEditingContext editingContext, EOExamenEtudiant eo) {
    EOExamenEtudiant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
