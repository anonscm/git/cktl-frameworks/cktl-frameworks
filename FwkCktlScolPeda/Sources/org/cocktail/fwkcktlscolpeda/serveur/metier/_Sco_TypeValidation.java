// DO NOT EDIT.  Make changes to Sco_TypeValidation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _Sco_TypeValidation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeValidation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_Sco_TypeValidation.class);

  public Sco_TypeValidation localInstanceIn(EOEditingContext editingContext) {
    Sco_TypeValidation localInstance = (Sco_TypeValidation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_Sco_TypeValidation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_Sco_TypeValidation.LOG.isDebugEnabled()) {
    	_Sco_TypeValidation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_TypeValidation.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_Sco_TypeValidation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_Sco_TypeValidation.LOG.isDebugEnabled()) {
    	_Sco_TypeValidation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _Sco_TypeValidation.LIBELLE_KEY);
  }


  public static Sco_TypeValidation createSco_TypeValidation(EOEditingContext editingContext, String code
, String libelle
) {
    Sco_TypeValidation eo = (Sco_TypeValidation) EOUtilities.createAndInsertInstance(editingContext, _Sco_TypeValidation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<Sco_TypeValidation> fetchSpec() {
    return new ERXFetchSpecification<Sco_TypeValidation>(_Sco_TypeValidation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Sco_TypeValidation> fetchAllSco_TypeValidations(EOEditingContext editingContext) {
    return _Sco_TypeValidation.fetchAllSco_TypeValidations(editingContext, null);
  }

  public static NSArray<Sco_TypeValidation> fetchAllSco_TypeValidations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _Sco_TypeValidation.fetchSco_TypeValidations(editingContext, null, sortOrderings);
  }

  public static NSArray<Sco_TypeValidation> fetchSco_TypeValidations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Sco_TypeValidation> fetchSpec = new ERXFetchSpecification<Sco_TypeValidation>(_Sco_TypeValidation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Sco_TypeValidation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Sco_TypeValidation fetchSco_TypeValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_TypeValidation.fetchSco_TypeValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_TypeValidation fetchSco_TypeValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Sco_TypeValidation> eoObjects = _Sco_TypeValidation.fetchSco_TypeValidations(editingContext, qualifier, null);
    Sco_TypeValidation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_TypeValidation fetchRequiredSco_TypeValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _Sco_TypeValidation.fetchRequiredSco_TypeValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Sco_TypeValidation fetchRequiredSco_TypeValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    Sco_TypeValidation eoObject = _Sco_TypeValidation.fetchSco_TypeValidation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Sco_TypeValidation localInstanceIn(EOEditingContext editingContext, Sco_TypeValidation eo) {
    Sco_TypeValidation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
