package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Le type de formation Aglae.
 * 
 * @author Alexis Tual
 *
 */
public interface ITypeFormationAglae {

    /**
     * @return libelle du type de formation
     */
    String libelle();
    
    /**
     * @return code du type de formation
     */
    String code();
    
}
