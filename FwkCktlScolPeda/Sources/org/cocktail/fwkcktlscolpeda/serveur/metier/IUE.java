package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * représentation d'un UE
 */
public interface IUE extends IComposant {

	/**
	 * @return <code> true </code> si l'UE est de type EC : n'a aucun enfant de type EC, Regroupement ou UE l'UE peut avoir cependant des enfants de type AP
	 */
	Integer sansComposant();
}