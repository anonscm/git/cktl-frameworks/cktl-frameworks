// DO NOT EDIT.  Make changes to EOSysIneEtudiantOffset.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSysIneEtudiantOffset extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_SysIneEtudiantOffset";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_UNIVERSITAIRE = new ERXKey<Integer>("anneeUniversitaire");
  public static final ERXKey<Integer> OFFSET = new ERXKey<Integer>("offset");
  // Relationship Keys

  // Attributes
  public static final String ANNEE_UNIVERSITAIRE_KEY = ANNEE_UNIVERSITAIRE.key();
  public static final String OFFSET_KEY = OFFSET.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSysIneEtudiantOffset.class);

  public EOSysIneEtudiantOffset localInstanceIn(EOEditingContext editingContext) {
    EOSysIneEtudiantOffset localInstance = (EOSysIneEtudiantOffset)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneeUniversitaire() {
    return (Integer) storedValueForKey(_EOSysIneEtudiantOffset.ANNEE_UNIVERSITAIRE_KEY);
  }

  public void setAnneeUniversitaire(Integer value) {
    if (_EOSysIneEtudiantOffset.LOG.isDebugEnabled()) {
    	_EOSysIneEtudiantOffset.LOG.debug( "updating anneeUniversitaire from " + anneeUniversitaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSysIneEtudiantOffset.ANNEE_UNIVERSITAIRE_KEY);
  }

  public Integer offset() {
    return (Integer) storedValueForKey(_EOSysIneEtudiantOffset.OFFSET_KEY);
  }

  public void setOffset(Integer value) {
    if (_EOSysIneEtudiantOffset.LOG.isDebugEnabled()) {
    	_EOSysIneEtudiantOffset.LOG.debug( "updating offset from " + offset() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSysIneEtudiantOffset.OFFSET_KEY);
  }


  public static EOSysIneEtudiantOffset createSco_SysIneEtudiantOffset(EOEditingContext editingContext, Integer anneeUniversitaire
, Integer offset
) {
    EOSysIneEtudiantOffset eo = (EOSysIneEtudiantOffset) EOUtilities.createAndInsertInstance(editingContext, _EOSysIneEtudiantOffset.ENTITY_NAME);    
		eo.setAnneeUniversitaire(anneeUniversitaire);
		eo.setOffset(offset);
    return eo;
  }

  public static ERXFetchSpecification<EOSysIneEtudiantOffset> fetchSpec() {
    return new ERXFetchSpecification<EOSysIneEtudiantOffset>(_EOSysIneEtudiantOffset.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSysIneEtudiantOffset> fetchAllSco_SysIneEtudiantOffsets(EOEditingContext editingContext) {
    return _EOSysIneEtudiantOffset.fetchAllSco_SysIneEtudiantOffsets(editingContext, null);
  }

  public static NSArray<EOSysIneEtudiantOffset> fetchAllSco_SysIneEtudiantOffsets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSysIneEtudiantOffset.fetchSco_SysIneEtudiantOffsets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSysIneEtudiantOffset> fetchSco_SysIneEtudiantOffsets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSysIneEtudiantOffset> fetchSpec = new ERXFetchSpecification<EOSysIneEtudiantOffset>(_EOSysIneEtudiantOffset.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSysIneEtudiantOffset> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSysIneEtudiantOffset fetchSco_SysIneEtudiantOffset(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSysIneEtudiantOffset.fetchSco_SysIneEtudiantOffset(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSysIneEtudiantOffset fetchSco_SysIneEtudiantOffset(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSysIneEtudiantOffset> eoObjects = _EOSysIneEtudiantOffset.fetchSco_SysIneEtudiantOffsets(editingContext, qualifier, null);
    EOSysIneEtudiantOffset eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_SysIneEtudiantOffset that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSysIneEtudiantOffset fetchRequiredSco_SysIneEtudiantOffset(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSysIneEtudiantOffset.fetchRequiredSco_SysIneEtudiantOffset(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSysIneEtudiantOffset fetchRequiredSco_SysIneEtudiantOffset(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSysIneEtudiantOffset eoObject = _EOSysIneEtudiantOffset.fetchSco_SysIneEtudiantOffset(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_SysIneEtudiantOffset that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSysIneEtudiantOffset localInstanceIn(EOEditingContext editingContext, EOSysIneEtudiantOffset eo) {
    EOSysIneEtudiantOffset localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
