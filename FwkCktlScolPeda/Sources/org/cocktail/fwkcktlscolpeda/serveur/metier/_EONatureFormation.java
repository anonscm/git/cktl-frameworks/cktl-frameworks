// DO NOT EDIT.  Make changes to EONatureFormation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONatureFormation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_NatureFormation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONatureFormation.class);

  public EONatureFormation localInstanceIn(EOEditingContext editingContext) {
    EONatureFormation localInstance = (EONatureFormation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EONatureFormation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EONatureFormation.LOG.isDebugEnabled()) {
    	_EONatureFormation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EONatureFormation.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EONatureFormation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EONatureFormation.LOG.isDebugEnabled()) {
    	_EONatureFormation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EONatureFormation.LIBELLE_KEY);
  }


  public static EONatureFormation createSco_NatureFormation(EOEditingContext editingContext, String code
, String libelle
) {
    EONatureFormation eo = (EONatureFormation) EOUtilities.createAndInsertInstance(editingContext, _EONatureFormation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EONatureFormation> fetchSpec() {
    return new ERXFetchSpecification<EONatureFormation>(_EONatureFormation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONatureFormation> fetchAllSco_NatureFormations(EOEditingContext editingContext) {
    return _EONatureFormation.fetchAllSco_NatureFormations(editingContext, null);
  }

  public static NSArray<EONatureFormation> fetchAllSco_NatureFormations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONatureFormation.fetchSco_NatureFormations(editingContext, null, sortOrderings);
  }

  public static NSArray<EONatureFormation> fetchSco_NatureFormations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONatureFormation> fetchSpec = new ERXFetchSpecification<EONatureFormation>(_EONatureFormation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONatureFormation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONatureFormation fetchSco_NatureFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureFormation.fetchSco_NatureFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureFormation fetchSco_NatureFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONatureFormation> eoObjects = _EONatureFormation.fetchSco_NatureFormations(editingContext, qualifier, null);
    EONatureFormation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_NatureFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureFormation fetchRequiredSco_NatureFormation(EOEditingContext editingContext, String keyName, Object value) {
    return _EONatureFormation.fetchRequiredSco_NatureFormation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONatureFormation fetchRequiredSco_NatureFormation(EOEditingContext editingContext, EOQualifier qualifier) {
    EONatureFormation eoObject = _EONatureFormation.fetchSco_NatureFormation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_NatureFormation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONatureFormation localInstanceIn(EOEditingContext editingContext, EONatureFormation eo) {
    EONatureFormation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
