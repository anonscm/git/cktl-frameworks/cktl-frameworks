// DO NOT EDIT.  Make changes to EODiplome.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODiplome extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant {
  public static final String ENTITY_NAME = "Sco_Diplome";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> DELIBERATION_SEMESTRIELLE = new ERXKey<Integer>("deliberationSemestrielle");
  public static final ERXKey<Integer> HABILITATION_ID = new ERXKey<Integer>("habilitationId");
  public static final ERXKey<Integer> HABILITATION_REQUISE = new ERXKey<Integer>("habilitationRequise");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> NIVEAU_ACCES_ID = new ERXKey<Integer>("niveauAccesId");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> SECTEUR_SISE_ID = new ERXKey<Integer>("secteurSiseId");
  public static final ERXKey<String> SISE = new ERXKey<String>("sise");
  public static final ERXKey<String> SISE_ETABLISSEMENT = new ERXKey<String>("siseEtablissement");
  public static final ERXKey<Integer> SPECIALITE_ID = new ERXKey<Integer>("specialiteId");
  public static final ERXKey<Integer> STATUT_FORMATION_ID = new ERXKey<Integer>("statutFormationId");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  public static final ERXKey<Integer> TYPE_FORMATION_ID = new ERXKey<Integer>("typeFormationId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> COMPOSANT_SUIVANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("composantSuivant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> DIPLOMES_INTERMEDIAIRES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire>("diplomesIntermediaires");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale> ECOLE_DOCTORALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale>("ecoleDoctorale");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("gradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation> HABILITATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation>("habilitation");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> INSCRIPTIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>("inscriptions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention> MENTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention>("mention");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation> NATURE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation>("natureFormation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome> NIVEAU_ACCES_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome>("niveauAccesDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire> SECTEUR_DISCIPLINAIRE_SISE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire>("secteurDisciplinaireSise");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite> SPECIALITE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite>("specialite");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation> STATUT_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation>("statutFormation");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("typeFormation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation> TYPE_ORGANISATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation>("typeOrganisation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation> TYPE_VALIDATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation>("typeValidation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation> VOCATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation>("vocation");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DELIBERATION_SEMESTRIELLE_KEY = DELIBERATION_SEMESTRIELLE.key();
  public static final String HABILITATION_ID_KEY = HABILITATION_ID.key();
  public static final String HABILITATION_REQUISE_KEY = HABILITATION_REQUISE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String NIVEAU_ACCES_ID_KEY = NIVEAU_ACCES_ID.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SECTEUR_SISE_ID_KEY = SECTEUR_SISE_ID.key();
  public static final String SISE_KEY = SISE.key();
  public static final String SISE_ETABLISSEMENT_KEY = SISE_ETABLISSEMENT.key();
  public static final String SPECIALITE_ID_KEY = SPECIALITE_ID.key();
  public static final String STATUT_FORMATION_ID_KEY = STATUT_FORMATION_ID.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  public static final String TYPE_FORMATION_ID_KEY = TYPE_FORMATION_ID.key();
  // Relationships
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String COMPOSANT_SUIVANT_KEY = COMPOSANT_SUIVANT.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DIPLOMES_INTERMEDIAIRES_KEY = DIPLOMES_INTERMEDIAIRES.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String ECOLE_DOCTORALE_KEY = ECOLE_DOCTORALE.key();
  public static final String GRADE_UNIVERSITAIRE_KEY = GRADE_UNIVERSITAIRE.key();
  public static final String HABILITATION_KEY = HABILITATION.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String INSCRIPTIONS_KEY = INSCRIPTIONS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String MENTION_KEY = MENTION.key();
  public static final String NATURE_FORMATION_KEY = NATURE_FORMATION.key();
  public static final String NIVEAU_ACCES_DIPLOME_KEY = NIVEAU_ACCES_DIPLOME.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SECTEUR_DISCIPLINAIRE_SISE_KEY = SECTEUR_DISCIPLINAIRE_SISE.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String SPECIALITE_KEY = SPECIALITE.key();
  public static final String STATUT_FORMATION_KEY = STATUT_FORMATION.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();
  public static final String TYPE_FORMATION_KEY = TYPE_FORMATION.key();
  public static final String TYPE_ORGANISATION_KEY = TYPE_ORGANISATION.key();
  public static final String TYPE_VALIDATION_KEY = TYPE_VALIDATION.key();
  public static final String VOCATION_KEY = VOCATION.key();

  private static Logger LOG = Logger.getLogger(_EODiplome.class);

  public EODiplome localInstanceIn(EOEditingContext editingContext) {
    EODiplome localInstance = (EODiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer deliberationSemestrielle() {
    return (Integer) storedValueForKey(_EODiplome.DELIBERATION_SEMESTRIELLE_KEY);
  }

  public void setDeliberationSemestrielle(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating deliberationSemestrielle from " + deliberationSemestrielle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.DELIBERATION_SEMESTRIELLE_KEY);
  }

  public Integer habilitationId() {
    return (Integer) storedValueForKey(_EODiplome.HABILITATION_ID_KEY);
  }

  public void setHabilitationId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating habilitationId from " + habilitationId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.HABILITATION_ID_KEY);
  }

  public Integer habilitationRequise() {
    return (Integer) storedValueForKey(_EODiplome.HABILITATION_REQUISE_KEY);
  }

  public void setHabilitationRequise(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating habilitationRequise from " + habilitationRequise() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.HABILITATION_REQUISE_KEY);
  }

  public Integer niveauAccesId() {
    return (Integer) storedValueForKey(_EODiplome.NIVEAU_ACCES_ID_KEY);
  }

  public void setNiveauAccesId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating niveauAccesId from " + niveauAccesId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.NIVEAU_ACCES_ID_KEY);
  }

  public Integer secteurSiseId() {
    return (Integer) storedValueForKey(_EODiplome.SECTEUR_SISE_ID_KEY);
  }

  public void setSecteurSiseId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating secteurSiseId from " + secteurSiseId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.SECTEUR_SISE_ID_KEY);
  }

  public String sise() {
    return (String) storedValueForKey(_EODiplome.SISE_KEY);
  }

  public void setSise(String value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating sise from " + sise() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.SISE_KEY);
  }

  public String siseEtablissement() {
    return (String) storedValueForKey(_EODiplome.SISE_ETABLISSEMENT_KEY);
  }

  public void setSiseEtablissement(String value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating siseEtablissement from " + siseEtablissement() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.SISE_ETABLISSEMENT_KEY);
  }

  public Integer specialiteId() {
    return (Integer) storedValueForKey(_EODiplome.SPECIALITE_ID_KEY);
  }

  public void setSpecialiteId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating specialiteId from " + specialiteId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.SPECIALITE_ID_KEY);
  }

  public Integer statutFormationId() {
    return (Integer) storedValueForKey(_EODiplome.STATUT_FORMATION_ID_KEY);
  }

  public void setStatutFormationId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating statutFormationId from " + statutFormationId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.STATUT_FORMATION_ID_KEY);
  }

  public Integer typeFormationId() {
    return (Integer) storedValueForKey(_EODiplome.TYPE_FORMATION_ID_KEY);
  }

  public void setTypeFormationId(Integer value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
    	_EODiplome.LOG.debug( "updating typeFormationId from " + typeFormationId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplome.TYPE_FORMATION_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale ecoleDoctorale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale)storedValueForKey(_EODiplome.ECOLE_DOCTORALE_KEY);
  }
  
  public void setEcoleDoctorale(org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale value) {
    takeStoredValueForKey(value, _EODiplome.ECOLE_DOCTORALE_KEY);
  }

  public void setEcoleDoctoraleRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating ecoleDoctorale from " + ecoleDoctorale() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setEcoleDoctorale(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEcoleDoctorale oldValue = ecoleDoctorale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.ECOLE_DOCTORALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.ECOLE_DOCTORALE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire gradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EODiplome.GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EODiplome.GRADE_UNIVERSITAIRE_KEY);
  }

  public void setGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating gradeUniversitaire from " + gradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = gradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation habilitation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation)storedValueForKey(_EODiplome.HABILITATION_KEY);
  }
  
  public void setHabilitation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation value) {
    takeStoredValueForKey(value, _EODiplome.HABILITATION_KEY);
  }

  public void setHabilitationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating habilitation from " + habilitation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setHabilitation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOHabilitation oldValue = habilitation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.HABILITATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.HABILITATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention mention() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention)storedValueForKey(_EODiplome.MENTION_KEY);
  }
  
  public void setMention(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention value) {
    takeStoredValueForKey(value, _EODiplome.MENTION_KEY);
  }

  public void setMentionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating mention from " + mention() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setMention(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention oldValue = mention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.MENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.MENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation natureFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation)storedValueForKey(_EODiplome.NATURE_FORMATION_KEY);
  }
  
  public void setNatureFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation value) {
    takeStoredValueForKey(value, _EODiplome.NATURE_FORMATION_KEY);
  }

  public void setNatureFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating natureFormation from " + natureFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setNatureFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EONatureFormation oldValue = natureFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.NATURE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.NATURE_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome niveauAccesDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome)storedValueForKey(_EODiplome.NIVEAU_ACCES_DIPLOME_KEY);
  }
  
  public void setNiveauAccesDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome value) {
    takeStoredValueForKey(value, _EODiplome.NIVEAU_ACCES_DIPLOME_KEY);
  }

  public void setNiveauAccesDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating niveauAccesDiplome from " + niveauAccesDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setNiveauAccesDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauAccesDiplome oldValue = niveauAccesDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.NIVEAU_ACCES_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.NIVEAU_ACCES_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire secteurDisciplinaireSise() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire)storedValueForKey(_EODiplome.SECTEUR_DISCIPLINAIRE_SISE_KEY);
  }
  
  public void setSecteurDisciplinaireSise(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire value) {
    takeStoredValueForKey(value, _EODiplome.SECTEUR_DISCIPLINAIRE_SISE_KEY);
  }

  public void setSecteurDisciplinaireSiseRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating secteurDisciplinaireSise from " + secteurDisciplinaireSise() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setSecteurDisciplinaireSise(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSiseSecteurDisciplinaire oldValue = secteurDisciplinaireSise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.SECTEUR_DISCIPLINAIRE_SISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.SECTEUR_DISCIPLINAIRE_SISE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite specialite() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite)storedValueForKey(_EODiplome.SPECIALITE_KEY);
  }
  
  public void setSpecialite(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite value) {
    takeStoredValueForKey(value, _EODiplome.SPECIALITE_KEY);
  }

  public void setSpecialiteRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating specialite from " + specialite() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setSpecialite(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSpecialite oldValue = specialite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.SPECIALITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.SPECIALITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation statutFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation)storedValueForKey(_EODiplome.STATUT_FORMATION_KEY);
  }
  
  public void setStatutFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation value) {
    takeStoredValueForKey(value, _EODiplome.STATUT_FORMATION_KEY);
  }

  public void setStatutFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating statutFormation from " + statutFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setStatutFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation oldValue = statutFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.STATUT_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.STATUT_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation typeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(_EODiplome.TYPE_FORMATION_KEY);
  }
  
  public void setTypeFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    takeStoredValueForKey(value, _EODiplome.TYPE_FORMATION_KEY);
  }

  public void setTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating typeFormation from " + typeFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = typeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.TYPE_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation typeOrganisation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation)storedValueForKey(_EODiplome.TYPE_ORGANISATION_KEY);
  }
  
  public void setTypeOrganisation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation value) {
    takeStoredValueForKey(value, _EODiplome.TYPE_ORGANISATION_KEY);
  }

  public void setTypeOrganisationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating typeOrganisation from " + typeOrganisation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeOrganisation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeOrganisation oldValue = typeOrganisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.TYPE_ORGANISATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.TYPE_ORGANISATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation typeValidation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation)storedValueForKey(_EODiplome.TYPE_VALIDATION_KEY);
  }
  
  public void setTypeValidation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation value) {
    takeStoredValueForKey(value, _EODiplome.TYPE_VALIDATION_KEY);
  }

  public void setTypeValidationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating typeValidation from " + typeValidation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeValidation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeValidation oldValue = typeValidation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.TYPE_VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.TYPE_VALIDATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation vocation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation)storedValueForKey(_EODiplome.VOCATION_KEY);
  }
  
  public void setVocation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation value) {
    takeStoredValueForKey(value, _EODiplome.VOCATION_KEY);
  }

  public void setVocationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation value) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("updating vocation from " + vocation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setVocation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOVocation oldValue = vocation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplome.VOCATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplome.VOCATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> composantSuivant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)storedValueForKey(_EODiplome.COMPOSANT_SUIVANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> composantSuivant(EOQualifier qualifier) {
    return composantSuivant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> composantSuivant(EOQualifier qualifier, boolean fetch) {
    return composantSuivant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> composantSuivant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.fetchSco_Diplomes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantSuivant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    includeObjectIntoPropertyWithKey(object, _EODiplome.COMPOSANT_SUIVANT_KEY);
  }

  public void removeFromComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    excludeObjectFromPropertyWithKey(object, _EODiplome.COMPOSANT_SUIVANT_KEY);
  }

  public void addToComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("adding " + object + " to composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantSuivant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODiplome.COMPOSANT_SUIVANT_KEY);
    }
  }

  public void removeFromComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("removing " + object + " from composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantSuivant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.COMPOSANT_SUIVANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome createComposantSuivantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODiplome.COMPOSANT_SUIVANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome) eo;
  }

  public void deleteComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.COMPOSANT_SUIVANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantSuivantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> objects = composantSuivant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantSuivantRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> diplomesIntermediaires() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire>)storedValueForKey(_EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> diplomesIntermediaires(EOQualifier qualifier) {
    return diplomesIntermediaires(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> diplomesIntermediaires(EOQualifier qualifier, boolean fetch) {
    return diplomesIntermediaires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> diplomesIntermediaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire.DIPLOME_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire.fetchSco_DiplomeIntermediaires(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = diplomesIntermediaires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDiplomesIntermediaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire object) {
    includeObjectIntoPropertyWithKey(object, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
  }

  public void removeFromDiplomesIntermediaires(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire object) {
    excludeObjectFromPropertyWithKey(object, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
  }

  public void addToDiplomesIntermediairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("adding " + object + " to diplomesIntermediaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDiplomesIntermediaires(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
    }
  }

  public void removeFromDiplomesIntermediairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("removing " + object + " from diplomesIntermediaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDiplomesIntermediaires(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire createDiplomesIntermediairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire) eo;
  }

  public void deleteDiplomesIntermediairesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.DIPLOMES_INTERMEDIAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDiplomesIntermediairesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplomeIntermediaire> objects = diplomesIntermediaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDiplomesIntermediairesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> inscriptions() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)storedValueForKey(_EODiplome.INSCRIPTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> inscriptions(EOQualifier qualifier) {
    return inscriptions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> inscriptions(EOQualifier qualifier, boolean fetch) {
    return inscriptions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> inscriptions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.TO_DIPLOME_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.fetchSco_Inscriptions(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inscriptions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInscriptions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    includeObjectIntoPropertyWithKey(object, _EODiplome.INSCRIPTIONS_KEY);
  }

  public void removeFromInscriptions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    excludeObjectFromPropertyWithKey(object, _EODiplome.INSCRIPTIONS_KEY);
  }

  public void addToInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("adding " + object + " to inscriptions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToInscriptions(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODiplome.INSCRIPTIONS_KEY);
    }
  }

  public void removeFromInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    if (_EODiplome.LOG.isDebugEnabled()) {
      _EODiplome.LOG.debug("removing " + object + " from inscriptions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromInscriptions(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.INSCRIPTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription createInscriptionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODiplome.INSCRIPTIONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription) eo;
  }

  public void deleteInscriptionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiplome.INSCRIPTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInscriptionsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> objects = inscriptions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInscriptionsRelationship(objects.nextElement());
    }
  }


  public static EODiplome createSco_Diplome(EOEditingContext editingContext, Integer id
, String libelle
) {
    EODiplome eo = (EODiplome) EOUtilities.createAndInsertInstance(editingContext, _EODiplome.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EODiplome> fetchSpecForSco_Diplome() {
    return new ERXFetchSpecification<EODiplome>(_EODiplome.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODiplome> fetchAllSco_Diplomes(EOEditingContext editingContext) {
    return _EODiplome.fetchAllSco_Diplomes(editingContext, null);
  }

  public static NSArray<EODiplome> fetchAllSco_Diplomes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODiplome.fetchSco_Diplomes(editingContext, null, sortOrderings);
  }

  public static NSArray<EODiplome> fetchSco_Diplomes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODiplome> fetchSpec = new ERXFetchSpecification<EODiplome>(_EODiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODiplome> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODiplome fetchSco_Diplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplome.fetchSco_Diplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplome fetchSco_Diplome(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODiplome> eoObjects = _EODiplome.fetchSco_Diplomes(editingContext, qualifier, null);
    EODiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Diplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplome fetchRequiredSco_Diplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplome.fetchRequiredSco_Diplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplome fetchRequiredSco_Diplome(EOEditingContext editingContext, EOQualifier qualifier) {
    EODiplome eoObject = _EODiplome.fetchSco_Diplome(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Diplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplome localInstanceIn(EOEditingContext editingContext, EODiplome eo) {
    EODiplome localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
