package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * Cette classe represente une activite professionnelle
 */
public class EOActiviteProfessionnelle extends _EOActiviteProfessionnelle {
  private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOActiviteProfessionnelle.class);
}
