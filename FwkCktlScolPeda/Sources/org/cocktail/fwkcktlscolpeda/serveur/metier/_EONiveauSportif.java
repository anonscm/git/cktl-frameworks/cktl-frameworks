// DO NOT EDIT.  Make changes to EONiveauSportif.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONiveauSportif extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_NiveauSportif";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERSID_MODIFCATION = new ERXKey<Integer>("persidModifcation");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERSID_MODIFCATION_KEY = PERSID_MODIFCATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONiveauSportif.class);

  public EONiveauSportif localInstanceIn(EOEditingContext editingContext) {
    EONiveauSportif localInstance = (EONiveauSportif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EONiveauSportif.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EONiveauSportif.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EONiveauSportif.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EONiveauSportif.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EONiveauSportif.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.PERS_ID_CREATION_KEY);
  }

  public Integer persidModifcation() {
    return (Integer) storedValueForKey(_EONiveauSportif.PERSID_MODIFCATION_KEY);
  }

  public void setPersidModifcation(Integer value) {
    if (_EONiveauSportif.LOG.isDebugEnabled()) {
    	_EONiveauSportif.LOG.debug( "updating persidModifcation from " + persidModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauSportif.PERSID_MODIFCATION_KEY);
  }


  public static EONiveauSportif createSco_NiveauSportif(EOEditingContext editingContext, String code
, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
) {
    EONiveauSportif eo = (EONiveauSportif) EOUtilities.createAndInsertInstance(editingContext, _EONiveauSportif.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EONiveauSportif> fetchSpec() {
    return new ERXFetchSpecification<EONiveauSportif>(_EONiveauSportif.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONiveauSportif> fetchAllSco_NiveauSportifs(EOEditingContext editingContext) {
    return _EONiveauSportif.fetchAllSco_NiveauSportifs(editingContext, null);
  }

  public static NSArray<EONiveauSportif> fetchAllSco_NiveauSportifs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONiveauSportif.fetchSco_NiveauSportifs(editingContext, null, sortOrderings);
  }

  public static NSArray<EONiveauSportif> fetchSco_NiveauSportifs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONiveauSportif> fetchSpec = new ERXFetchSpecification<EONiveauSportif>(_EONiveauSportif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONiveauSportif> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONiveauSportif fetchSco_NiveauSportif(EOEditingContext editingContext, String keyName, Object value) {
    return _EONiveauSportif.fetchSco_NiveauSportif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONiveauSportif fetchSco_NiveauSportif(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONiveauSportif> eoObjects = _EONiveauSportif.fetchSco_NiveauSportifs(editingContext, qualifier, null);
    EONiveauSportif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_NiveauSportif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONiveauSportif fetchRequiredSco_NiveauSportif(EOEditingContext editingContext, String keyName, Object value) {
    return _EONiveauSportif.fetchRequiredSco_NiveauSportif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONiveauSportif fetchRequiredSco_NiveauSportif(EOEditingContext editingContext, EOQualifier qualifier) {
    EONiveauSportif eoObject = _EONiveauSportif.fetchSco_NiveauSportif(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_NiveauSportif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONiveauSportif localInstanceIn(EOEditingContext editingContext, EONiveauSportif eo) {
    EONiveauSportif localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
