package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

/**
 * Cette interface représente un groupe pour un composant.
 * 
 * @author Pascal MACOUIN
 */
public interface IGroupe {

	/**
	 * @return Le libellé du groupe
	 */
	String libelle();

	/**
	 * @param unLibelle Un libellé pour le groupe
	 */
	void setLibelle(String unLibelle);

	/**
	 * @return L'ordre d'affichage du groupe pour ce composant
	 */
	Integer ordre();

	/**
	 * @param unOrdre Un ordre d'affichage de ce groupe pour ce composant
	 */
	void setOrdre(Integer unOrdre);

	/**
	 * @return Le composant lié à ce groupe
	 */
	IComposant toComposant();

	/**
	 * @param unComposant Un composant lié à ce groupe
	 */
	void setToComposantRelationship(IComposant unComposant);
	
	/**
	 * @return La liste des étudiants pour ce groupe
	 */
	List<? extends IGroupeEtudiant> toGroupeEtudiants();

}