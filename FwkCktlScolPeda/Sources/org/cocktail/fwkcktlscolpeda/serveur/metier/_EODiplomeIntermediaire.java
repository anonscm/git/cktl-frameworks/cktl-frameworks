// DO NOT EDIT.  Make changes to EODiplomeIntermediaire.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODiplomeIntermediaire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DiplomeIntermediaire";

  // Attribute Keys
  public static final ERXKey<Integer> DIPLOME_ID = new ERXKey<Integer>("diplomeId");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> SISE = new ERXKey<String>("sise");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("diplome");

  // Attributes
  public static final String DIPLOME_ID_KEY = DIPLOME_ID.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String SISE_KEY = SISE.key();
  // Relationships
  public static final String DIPLOME_KEY = DIPLOME.key();

  private static Logger LOG = Logger.getLogger(_EODiplomeIntermediaire.class);

  public EODiplomeIntermediaire localInstanceIn(EOEditingContext editingContext) {
    EODiplomeIntermediaire localInstance = (EODiplomeIntermediaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer diplomeId() {
    return (Integer) storedValueForKey(_EODiplomeIntermediaire.DIPLOME_ID_KEY);
  }

  public void setDiplomeId(Integer value) {
    if (_EODiplomeIntermediaire.LOG.isDebugEnabled()) {
    	_EODiplomeIntermediaire.LOG.debug( "updating diplomeId from " + diplomeId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplomeIntermediaire.DIPLOME_ID_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODiplomeIntermediaire.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODiplomeIntermediaire.LOG.isDebugEnabled()) {
    	_EODiplomeIntermediaire.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplomeIntermediaire.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODiplomeIntermediaire.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODiplomeIntermediaire.LOG.isDebugEnabled()) {
    	_EODiplomeIntermediaire.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplomeIntermediaire.LIBELLE_KEY);
  }

  public String sise() {
    return (String) storedValueForKey(_EODiplomeIntermediaire.SISE_KEY);
  }

  public void setSise(String value) {
    if (_EODiplomeIntermediaire.LOG.isDebugEnabled()) {
    	_EODiplomeIntermediaire.LOG.debug( "updating sise from " + sise() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiplomeIntermediaire.SISE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome diplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome)storedValueForKey(_EODiplomeIntermediaire.DIPLOME_KEY);
  }
  
  public void setDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    takeStoredValueForKey(value, _EODiplomeIntermediaire.DIPLOME_KEY);
  }

  public void setDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    if (_EODiplomeIntermediaire.LOG.isDebugEnabled()) {
      _EODiplomeIntermediaire.LOG.debug("updating diplome from " + diplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome oldValue = diplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODiplomeIntermediaire.DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODiplomeIntermediaire.DIPLOME_KEY);
    }
  }
  

  public static EODiplomeIntermediaire createSco_DiplomeIntermediaire(EOEditingContext editingContext, Integer diplomeId
, Integer id
, String libelle
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome diplome) {
    EODiplomeIntermediaire eo = (EODiplomeIntermediaire) EOUtilities.createAndInsertInstance(editingContext, _EODiplomeIntermediaire.ENTITY_NAME);    
		eo.setDiplomeId(diplomeId);
		eo.setId(id);
		eo.setLibelle(libelle);
    eo.setDiplomeRelationship(diplome);
    return eo;
  }

  public static ERXFetchSpecification<EODiplomeIntermediaire> fetchSpec() {
    return new ERXFetchSpecification<EODiplomeIntermediaire>(_EODiplomeIntermediaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODiplomeIntermediaire> fetchAllSco_DiplomeIntermediaires(EOEditingContext editingContext) {
    return _EODiplomeIntermediaire.fetchAllSco_DiplomeIntermediaires(editingContext, null);
  }

  public static NSArray<EODiplomeIntermediaire> fetchAllSco_DiplomeIntermediaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODiplomeIntermediaire.fetchSco_DiplomeIntermediaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EODiplomeIntermediaire> fetchSco_DiplomeIntermediaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODiplomeIntermediaire> fetchSpec = new ERXFetchSpecification<EODiplomeIntermediaire>(_EODiplomeIntermediaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODiplomeIntermediaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODiplomeIntermediaire fetchSco_DiplomeIntermediaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplomeIntermediaire.fetchSco_DiplomeIntermediaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplomeIntermediaire fetchSco_DiplomeIntermediaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODiplomeIntermediaire> eoObjects = _EODiplomeIntermediaire.fetchSco_DiplomeIntermediaires(editingContext, qualifier, null);
    EODiplomeIntermediaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DiplomeIntermediaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplomeIntermediaire fetchRequiredSco_DiplomeIntermediaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiplomeIntermediaire.fetchRequiredSco_DiplomeIntermediaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiplomeIntermediaire fetchRequiredSco_DiplomeIntermediaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EODiplomeIntermediaire eoObject = _EODiplomeIntermediaire.fetchSco_DiplomeIntermediaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DiplomeIntermediaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiplomeIntermediaire localInstanceIn(EOEditingContext editingContext, EODiplomeIntermediaire eo) {
    EODiplomeIntermediaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
