// DO NOT EDIT.  Make changes to EOParametragePaieArticleComplementaire.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametragePaieArticleComplementaire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametragePaieArticleComplementaire";

  // Attribute Keys
  public static final ERXKey<Boolean> APPLICATION_SYSTEMATIQUE = new ERXKey<Boolean>("applicationSystematique");
  public static final ERXKey<Boolean> BOURSIER = new ERXKey<Boolean>("boursier");
  public static final ERXKey<String> COMPOSANTE_BUDGETAIRE = new ERXKey<String>("composanteBudgetaire");
  public static final ERXKey<String> COMPTABILITE = new ERXKey<String>("comptabilite");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> REMBOURSEMENT = new ERXKey<Boolean>("remboursement");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire> TO_TYPE_ARTICLE_COMPLEMENTAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire>("toTypeArticleComplementaire");

  // Attributes
  public static final String APPLICATION_SYSTEMATIQUE_KEY = APPLICATION_SYSTEMATIQUE.key();
  public static final String BOURSIER_KEY = BOURSIER.key();
  public static final String COMPOSANTE_BUDGETAIRE_KEY = COMPOSANTE_BUDGETAIRE.key();
  public static final String COMPTABILITE_KEY = COMPTABILITE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String REMBOURSEMENT_KEY = REMBOURSEMENT.key();
  // Relationships
  public static final String TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY = TO_TYPE_ARTICLE_COMPLEMENTAIRE.key();

  private static Logger LOG = Logger.getLogger(_EOParametragePaieArticleComplementaire.class);

  public EOParametragePaieArticleComplementaire localInstanceIn(EOEditingContext editingContext) {
    EOParametragePaieArticleComplementaire localInstance = (EOParametragePaieArticleComplementaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean applicationSystematique() {
    return (Boolean) storedValueForKey(_EOParametragePaieArticleComplementaire.APPLICATION_SYSTEMATIQUE_KEY);
  }

  public void setApplicationSystematique(Boolean value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating applicationSystematique from " + applicationSystematique() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.APPLICATION_SYSTEMATIQUE_KEY);
  }

  public Boolean boursier() {
    return (Boolean) storedValueForKey(_EOParametragePaieArticleComplementaire.BOURSIER_KEY);
  }

  public void setBoursier(Boolean value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating boursier from " + boursier() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.BOURSIER_KEY);
  }

  public String composanteBudgetaire() {
    return (String) storedValueForKey(_EOParametragePaieArticleComplementaire.COMPOSANTE_BUDGETAIRE_KEY);
  }

  public void setComposanteBudgetaire(String value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating composanteBudgetaire from " + composanteBudgetaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.COMPOSANTE_BUDGETAIRE_KEY);
  }

  public String comptabilite() {
    return (String) storedValueForKey(_EOParametragePaieArticleComplementaire.COMPTABILITE_KEY);
  }

  public void setComptabilite(String value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating comptabilite from " + comptabilite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.COMPTABILITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieArticleComplementaire.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOParametragePaieArticleComplementaire.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOParametragePaieArticleComplementaire.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.LIBELLE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(_EOParametragePaieArticleComplementaire.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.MONTANT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametragePaieArticleComplementaire.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOParametragePaieArticleComplementaire.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean remboursement() {
    return (Boolean) storedValueForKey(_EOParametragePaieArticleComplementaire.REMBOURSEMENT_KEY);
  }

  public void setRemboursement(Boolean value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
    	_EOParametragePaieArticleComplementaire.LOG.debug( "updating remboursement from " + remboursement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.REMBOURSEMENT_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire toTypeArticleComplementaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire)storedValueForKey(_EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY);
  }
  
  public void setToTypeArticleComplementaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire value) {
    takeStoredValueForKey(value, _EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY);
  }

  public void setToTypeArticleComplementaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire value) {
    if (_EOParametragePaieArticleComplementaire.LOG.isDebugEnabled()) {
      _EOParametragePaieArticleComplementaire.LOG.debug("updating toTypeArticleComplementaire from " + toTypeArticleComplementaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeArticleComplementaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire oldValue = toTypeArticleComplementaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE_KEY);
    }
  }
  

  public static EOParametragePaieArticleComplementaire createSco_ParametragePaieArticleComplementaire(EOEditingContext editingContext, Boolean applicationSystematique
, Boolean boursier
, NSTimestamp dCreation
, String libelle
, java.math.BigDecimal montant
, Integer persIdCreation
, Integer persIdModification
, Boolean remboursement
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire toTypeArticleComplementaire) {
    EOParametragePaieArticleComplementaire eo = (EOParametragePaieArticleComplementaire) EOUtilities.createAndInsertInstance(editingContext, _EOParametragePaieArticleComplementaire.ENTITY_NAME);    
		eo.setApplicationSystematique(applicationSystematique);
		eo.setBoursier(boursier);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setRemboursement(remboursement);
    eo.setToTypeArticleComplementaireRelationship(toTypeArticleComplementaire);
    return eo;
  }

  public static ERXFetchSpecification<EOParametragePaieArticleComplementaire> fetchSpec() {
    return new ERXFetchSpecification<EOParametragePaieArticleComplementaire>(_EOParametragePaieArticleComplementaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametragePaieArticleComplementaire> fetchAllSco_ParametragePaieArticleComplementaires(EOEditingContext editingContext) {
    return _EOParametragePaieArticleComplementaire.fetchAllSco_ParametragePaieArticleComplementaires(editingContext, null);
  }

  public static NSArray<EOParametragePaieArticleComplementaire> fetchAllSco_ParametragePaieArticleComplementaires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametragePaieArticleComplementaire> fetchSco_ParametragePaieArticleComplementaires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametragePaieArticleComplementaire> fetchSpec = new ERXFetchSpecification<EOParametragePaieArticleComplementaire>(_EOParametragePaieArticleComplementaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametragePaieArticleComplementaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametragePaieArticleComplementaire fetchSco_ParametragePaieArticleComplementaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieArticleComplementaire fetchSco_ParametragePaieArticleComplementaire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametragePaieArticleComplementaire> eoObjects = _EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaires(editingContext, qualifier, null);
    EOParametragePaieArticleComplementaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametragePaieArticleComplementaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieArticleComplementaire fetchRequiredSco_ParametragePaieArticleComplementaire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametragePaieArticleComplementaire.fetchRequiredSco_ParametragePaieArticleComplementaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametragePaieArticleComplementaire fetchRequiredSco_ParametragePaieArticleComplementaire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametragePaieArticleComplementaire eoObject = _EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametragePaieArticleComplementaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametragePaieArticleComplementaire localInstanceIn(EOEditingContext editingContext, EOParametragePaieArticleComplementaire eo) {
    EOParametragePaieArticleComplementaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
