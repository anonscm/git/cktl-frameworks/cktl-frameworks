// DO NOT EDIT.  Make changes to EOSpecialite.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOSpecialite extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Specialite";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOSpecialite.class);

  public EOSpecialite localInstanceIn(EOEditingContext editingContext) {
    EOSpecialite localInstance = (EOSpecialite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOSpecialite.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOSpecialite.LOG.isDebugEnabled()) {
    	_EOSpecialite.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSpecialite.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOSpecialite.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOSpecialite.LOG.isDebugEnabled()) {
    	_EOSpecialite.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSpecialite.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOSpecialite.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOSpecialite.LOG.isDebugEnabled()) {
    	_EOSpecialite.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSpecialite.LIBELLE_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EOSpecialite.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EOSpecialite.LOG.isDebugEnabled()) {
    	_EOSpecialite.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSpecialite.LIBELLE_COURT_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOSpecialite.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOSpecialite.LOG.isDebugEnabled()) {
    	_EOSpecialite.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOSpecialite.READ_ONLY_KEY);
  }


  public static EOSpecialite createSco_Specialite(EOEditingContext editingContext, String code
, Integer id
, String libelle
, Integer readOnly
) {
    EOSpecialite eo = (EOSpecialite) EOUtilities.createAndInsertInstance(editingContext, _EOSpecialite.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EOSpecialite> fetchSpec() {
    return new ERXFetchSpecification<EOSpecialite>(_EOSpecialite.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOSpecialite> fetchAllSco_Specialites(EOEditingContext editingContext) {
    return _EOSpecialite.fetchAllSco_Specialites(editingContext, null);
  }

  public static NSArray<EOSpecialite> fetchAllSco_Specialites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSpecialite.fetchSco_Specialites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSpecialite> fetchSco_Specialites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOSpecialite> fetchSpec = new ERXFetchSpecification<EOSpecialite>(_EOSpecialite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSpecialite> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOSpecialite fetchSco_Specialite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialite.fetchSco_Specialite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialite fetchSco_Specialite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSpecialite> eoObjects = _EOSpecialite.fetchSco_Specialites(editingContext, qualifier, null);
    EOSpecialite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Specialite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialite fetchRequiredSco_Specialite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSpecialite.fetchRequiredSco_Specialite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSpecialite fetchRequiredSco_Specialite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSpecialite eoObject = _EOSpecialite.fetchSco_Specialite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Specialite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSpecialite localInstanceIn(EOEditingContext editingContext, EOSpecialite eo) {
    EOSpecialite localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
