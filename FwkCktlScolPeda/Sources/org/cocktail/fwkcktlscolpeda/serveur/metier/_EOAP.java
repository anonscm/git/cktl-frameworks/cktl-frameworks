// DO NOT EDIT.  Make changes to EOAP.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOAP extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant {
  public static final String ENTITY_NAME = "Sco_AP";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> MODE_AP_ID = new ERXKey<Integer>("modeAPId");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> SEUIL = new ERXKey<Integer>("seuil");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_AP_ID = new ERXKey<Integer>("typeAPId");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  public static final ERXKey<java.math.BigDecimal> VALEUR_TEMPS_ETUDIANT = new ERXKey<java.math.BigDecimal>("valeurTempsEtudiant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> CHARGE_ENSEIGNEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>("chargeEnseignements");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> COMPOSANT_SUIVANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("composantSuivant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP> MODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP>("mode");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP> TYPE_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP>("typeAP");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String MODE_AP_ID_KEY = MODE_AP_ID.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SEUIL_KEY = SEUIL.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_AP_ID_KEY = TYPE_AP_ID.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  public static final String VALEUR_TEMPS_ETUDIANT_KEY = VALEUR_TEMPS_ETUDIANT.key();
  // Relationships
  public static final String CHARGE_ENSEIGNEMENTS_KEY = CHARGE_ENSEIGNEMENTS.key();
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String COMPOSANT_SUIVANT_KEY = COMPOSANT_SUIVANT.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String MODE_KEY = MODE.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_AP_KEY = TYPE_AP.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EOAP.class);

  public EOAP localInstanceIn(EOEditingContext editingContext) {
    EOAP localInstance = (EOAP)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer modeAPId() {
    return (Integer) storedValueForKey(_EOAP.MODE_AP_ID_KEY);
  }

  public void setModeAPId(Integer value) {
    if (_EOAP.LOG.isDebugEnabled()) {
    	_EOAP.LOG.debug( "updating modeAPId from " + modeAPId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOAP.MODE_AP_ID_KEY);
  }

  public Integer seuil() {
    return (Integer) storedValueForKey(_EOAP.SEUIL_KEY);
  }

  public void setSeuil(Integer value) {
    if (_EOAP.LOG.isDebugEnabled()) {
    	_EOAP.LOG.debug( "updating seuil from " + seuil() + " to " + value);
    }
    takeStoredValueForKey(value, _EOAP.SEUIL_KEY);
  }

  public Integer typeAPId() {
    return (Integer) storedValueForKey(_EOAP.TYPE_AP_ID_KEY);
  }

  public void setTypeAPId(Integer value) {
    if (_EOAP.LOG.isDebugEnabled()) {
    	_EOAP.LOG.debug( "updating typeAPId from " + typeAPId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOAP.TYPE_AP_ID_KEY);
  }

  public java.math.BigDecimal valeurTempsEtudiant() {
    return (java.math.BigDecimal) storedValueForKey(_EOAP.VALEUR_TEMPS_ETUDIANT_KEY);
  }

  public void setValeurTempsEtudiant(java.math.BigDecimal value) {
    if (_EOAP.LOG.isDebugEnabled()) {
    	_EOAP.LOG.debug( "updating valeurTempsEtudiant from " + valeurTempsEtudiant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOAP.VALEUR_TEMPS_ETUDIANT_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP mode() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP)storedValueForKey(_EOAP.MODE_KEY);
  }
  
  public void setMode(org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP value) {
    takeStoredValueForKey(value, _EOAP.MODE_KEY);
  }

  public void setModeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP value) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("updating mode from " + mode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setMode(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP oldValue = mode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOAP.MODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOAP.MODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP typeAP() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP)storedValueForKey(_EOAP.TYPE_AP_KEY);
  }
  
  public void setTypeAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP value) {
    takeStoredValueForKey(value, _EOAP.TYPE_AP_KEY);
  }

  public void setTypeAPRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP value) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("updating typeAP from " + typeAP() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeAP(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP oldValue = typeAP();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOAP.TYPE_AP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOAP.TYPE_AP_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)storedValueForKey(_EOAP.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier) {
    return chargeEnseignements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier, boolean fetch) {
    return chargeEnseignements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> chargeEnseignements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.TO_AP_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.fetchSco_ChargeEnseignements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = chargeEnseignements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToChargeEnseignements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    includeObjectIntoPropertyWithKey(object, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public void removeFromChargeEnseignements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    excludeObjectFromPropertyWithKey(object, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
  }

  public void addToChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("adding " + object + " to chargeEnseignements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToChargeEnseignements(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
    }
  }

  public void removeFromChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("removing " + object + " from chargeEnseignements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromChargeEnseignements(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement createChargeEnseignementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement) eo;
  }

  public void deleteChargeEnseignementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOAP.CHARGE_ENSEIGNEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllChargeEnseignementsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> objects = chargeEnseignements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteChargeEnseignementsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> composantSuivant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>)storedValueForKey(_EOAP.COMPOSANT_SUIVANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> composantSuivant(EOQualifier qualifier) {
    return composantSuivant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> composantSuivant(EOQualifier qualifier, boolean fetch) {
    return composantSuivant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> composantSuivant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP.fetchSco_APs(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantSuivant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP object) {
    includeObjectIntoPropertyWithKey(object, _EOAP.COMPOSANT_SUIVANT_KEY);
  }

  public void removeFromComposantSuivant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP object) {
    excludeObjectFromPropertyWithKey(object, _EOAP.COMPOSANT_SUIVANT_KEY);
  }

  public void addToComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP object) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("adding " + object + " to composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantSuivant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOAP.COMPOSANT_SUIVANT_KEY);
    }
  }

  public void removeFromComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP object) {
    if (_EOAP.LOG.isDebugEnabled()) {
      _EOAP.LOG.debug("removing " + object + " from composantSuivant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantSuivant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOAP.COMPOSANT_SUIVANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP createComposantSuivantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOAP.COMPOSANT_SUIVANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP) eo;
  }

  public void deleteComposantSuivantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOAP.COMPOSANT_SUIVANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantSuivantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> objects = composantSuivant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantSuivantRelationship(objects.nextElement());
    }
  }


  public static EOAP createSco_AP(EOEditingContext editingContext, Integer id
, String libelle
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion composantInfoVersion) {
    EOAP eo = (EOAP) EOUtilities.createAndInsertInstance(editingContext, _EOAP.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
    eo.setComposantInfoVersionRelationship(composantInfoVersion);
    return eo;
  }

  public static ERXFetchSpecification<EOAP> fetchSpecForSco_AP() {
    return new ERXFetchSpecification<EOAP>(_EOAP.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOAP> fetchAllSco_APs(EOEditingContext editingContext) {
    return _EOAP.fetchAllSco_APs(editingContext, null);
  }

  public static NSArray<EOAP> fetchAllSco_APs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOAP.fetchSco_APs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOAP> fetchSco_APs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOAP> fetchSpec = new ERXFetchSpecification<EOAP>(_EOAP.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOAP> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOAP fetchSco_AP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAP.fetchSco_AP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAP fetchSco_AP(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOAP> eoObjects = _EOAP.fetchSco_APs(editingContext, qualifier, null);
    EOAP eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_AP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAP fetchRequiredSco_AP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOAP.fetchRequiredSco_AP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOAP fetchRequiredSco_AP(EOEditingContext editingContext, EOQualifier qualifier) {
    EOAP eoObject = _EOAP.fetchSco_AP(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_AP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOAP localInstanceIn(EOEditingContext editingContext, EOAP eo) {
    EOAP localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
