package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJury;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryConvocation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryPersonnes;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryRole;

import com.google.common.collect.Ordering;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class JuryRepositoryEOF implements JuryRepository {

	private EOEditingContext editingContext;
	
	public void ajouterMembre(IJury jury, IIndividu individu, IJuryRole role) {
		EOJuryPersonnes juryPersonnes = (EOJuryPersonnes) EOUtilities.createAndInsertInstance(editingContext, EOJuryPersonnes.ENTITY_NAME);
		juryPersonnes.setToIndividuRelationship((EOIndividu) individu);
		juryPersonnes.setToJuryRelationship((EOJury) jury);
		juryPersonnes.setToJuryRoleRelationship(role);
		juryPersonnes.setPresent(IJuryPersonnes.STATUT_DEFAUT_PRESENT);
	}

	@SuppressWarnings("unchecked")
	public List<IIndividu> fetchMembres(IJury jury) {
		List<IIndividu> individus = (List<IIndividu>) EOJury.TO_JURY_PERSONNESES.dot(EOJuryPersonnes.TO_INDIVIDU).valueInObject(jury);
		Ordering<IIndividu> orderByNom = new Ordering<IIndividu>() {
			@Override
			public int compare(IIndividu left, IIndividu right) {
				return Ordering.natural().compare(left.getNomPrenomAffichage(), right.getNomPrenomAffichage());
			}
		};
		return orderByNom.sortedCopy(individus);
	}

	public void supprimerMembre(IJury jury, IIndividu membre) {
		EOJuryPersonnes juryPersonneASupprimer = null;
		for (IJuryPersonnes juryPersonne : jury.toJuryPersonneses()) {
			if (juryPersonne.toIndividu().equals(membre)) {
				juryPersonneASupprimer = (EOJuryPersonnes) juryPersonne;
				break;
			}
		}
		EOJury eoJury = (EOJury) jury;
		if (juryPersonneASupprimer != null) {
			eoJury.removeFromToJuryPersonnesesRelationship(juryPersonneASupprimer);
			juryPersonneASupprimer.delete();
		}
	}
	
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public IJuryConvocation creerJuryConvocation(IJury jury) {
		IJuryConvocation juryConvocation = (EOJuryConvocation) EOUtilities.createAndInsertInstance(editingContext, EOJuryConvocation.ENTITY_NAME);
		juryConvocation.setToJuryRelationship(jury);
		return juryConvocation;

	}
	
	public IJuryConvocation getJuryConvocationValide(IJury jury) {
		IJuryConvocation juryConvocation = null;
		
		NSArray<EOJuryConvocation> listeJuryConvocation = ((EOJury) jury).toJuryConvocations(EOJuryConvocation.VALIDE.eq(true));
		if (listeJuryConvocation != null && listeJuryConvocation.size() > 0) {
			juryConvocation = listeJuryConvocation.get(0);
		}
		return juryConvocation;
	}
	
	public String juryDejaPresent(IJury jury) {
		String message = null;
		String messageFormation = "Il existe déja un jury pour: " + jury.toLienFormation().child().libelle();
		String messageParcoursFormation ="";
		String messageNiveauAnnee = "";
		String messageParcoursAnnee = "";
		String messageNiveauSemestre  = "";
		EOQualifier qualifierSession = EOJury.TO_SESSION_PERIODE.eq((EOSessionPeriode) jury.toSessionPeriode());
		EOQualifier qualifierFormation = EOJury.TO_LIEN_FORMATION.eq((EOLien) jury.toLienFormation());
		EOQualifier qualifierParcoursFormation = EOJury.TO_LIEN_PARCOURS_FORMATION.isNull();
		EOQualifier qualifierNiveauAnnee = EOJury.TO_LIEN_NIVEAU_ANNEE.isNull();
		EOQualifier qualifierParcoursAnnee = EOJury.TO_LIEN_PARCOURS_ANNEE.isNull();
		EOQualifier qualifierNiveauSemestre = EOJury.TO_LIEN_NIVEAU_SEMESTRE.isNull();
	
		
		if (jury.toLienParcoursFormation() != null) {
			qualifierParcoursFormation = EOJury.TO_LIEN_PARCOURS_FORMATION.eq((EOLien) jury.toLienParcoursFormation());
			messageParcoursFormation =  "/" + jury.toLienParcoursFormation().child().libelle();
		}
		if (jury.toLienNiveauAnnee() != null) {
			qualifierNiveauAnnee = EOJury.TO_LIEN_NIVEAU_ANNEE.eq((EOLien) jury.toLienNiveauAnnee());
			messageNiveauAnnee =  "/" + jury.toLienNiveauAnnee().child().libelle();
		} 
		if (jury.toLienParcoursAnnee() != null) {
			qualifierParcoursAnnee = EOJury.TO_LIEN_PARCOURS_ANNEE.eq((EOLien) jury.toLienParcoursAnnee());
			messageParcoursAnnee =  "/" + jury.toLienParcoursAnnee().child().libelle();
		} 
		if (jury.toLienNiveauSemestre() != null) {
			qualifierNiveauSemestre = EOJury.TO_LIEN_NIVEAU_SEMESTRE.eq((EOLien) jury.toLienNiveauSemestre());
			messageNiveauSemestre = "/" + jury.toLienNiveauSemestre().child().libelle();
		}

		
		EOQualifier qualifier = ERXQ.and(qualifierSession, qualifierFormation, qualifierParcoursFormation, qualifierNiveauAnnee, qualifierParcoursAnnee, qualifierNiveauSemestre);
		EOJury juryExistant = EOJury.fetchSco_Jury(editingContext, qualifier);
		if (juryExistant != null) {
			message = messageFormation + messageParcoursFormation + messageNiveauAnnee + messageParcoursAnnee + messageNiveauSemestre + "pour la session "+ jury.toSessionPeriode().numeroSession() + "-"+jury.toSessionPeriode().libelle();
		}
		return message;
	}
	

}
