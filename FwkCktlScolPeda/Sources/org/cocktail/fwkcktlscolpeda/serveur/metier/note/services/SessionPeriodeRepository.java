package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionPeriode;

/**
 * Cette interface représente tous les accès possibles à la table SessionPeriode.
 */
public interface SessionPeriodeRepository {

	/**
	 * Créer une nouvelle session.
	 * @param uneAnneeUniversitaire Une année universitaire
	 * @param unNumero Un numéro de session
	 * @param unLibelle Un libellé pour la session
	 * @return Une nouvelle session
	 */
	ISessionPeriode creerSessionPeriode(Integer uneAnneeUniversitaire, Integer unNumero, String unLibelle);
	
	/**
	 * @param annee  annee pour laquelle on cherche les sessions ouvertes
	 * @return  liste des sessions ouvertes
	 */
	List<ISessionPeriode> listeSessionPeriodeOuverte(Integer annee);
	
	/**
	 * 
	 * @param uneAnneeUniversitaire
	 * @param unNumero
	 * @return true si la session existe et est ouverte
	 */
	boolean isSessionPeriodeOuverte(Integer uneAnneeUniversitaire, Integer unNumero);
}
