package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier._EOSessionPeriode;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class SessionPeriodeRepositoryEOF implements SessionPeriodeRepository {

	private EOEditingContext editingContext;

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * {@inheritDoc}
	 */
	public ISessionPeriode creerSessionPeriode(Integer uneAnneeUniversitaire, Integer unNumero, String unLibelle) {
		ISessionPeriode nouvelleSession = (EOSessionPeriode) EOUtilities.createAndInsertInstance(editingContext, _EOSessionPeriode.ENTITY_NAME);
		nouvelleSession.setAnnee(uneAnneeUniversitaire);
		nouvelleSession.setNumeroSession(unNumero);
		nouvelleSession.setLibelle(unLibelle);
		return nouvelleSession;
	}

	/**
	 * {@inheritDoc}
	 */

	public List<ISessionPeriode> listeSessionPeriodeOuverte(Integer annee) {
		NSArray<EOSessionPeriode> liste;
		NSTimestamp date = new NSTimestamp();
		EOQualifier qualifier = ERXQ.and(
		    EOSessionPeriode.DATE_OUVERTURE.before(date),
		    (EOSessionPeriode.DATE_FERMETURE.after(date).or(EOSessionPeriode.DATE_FERMETURE.isNull())),
		    EOSessionPeriode.ANNEE.eq(annee));

		liste = EOSessionPeriode.fetchSco_SessionPeriodes(editingContext, qualifier, null);
		return new ArrayList<ISessionPeriode>(liste);
	}

	private ISessionPeriode getSessionPeriode(Integer uneAnneeUniversitaire, Integer unNumero) {
		EOQualifier qualifier = ERXQ.and(EOSessionPeriode.ANNEE.eq(uneAnneeUniversitaire), EOSessionPeriode.NUMERO_SESSION.eq(unNumero));
		return EOSessionPeriode.fetchSco_SessionPeriode(editingContext, qualifier);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSessionPeriodeOuverte(Integer uneAnneeUniversitaire, Integer unNumero) {
		ISessionPeriode sessionPeriode = getSessionPeriode(uneAnneeUniversitaire, unNumero);
		NSTimestamp date = new NSTimestamp();
		if (sessionPeriode != null && sessionPeriode.dateOuverture() != null && sessionPeriode.dateOuverture().compareTo(date) <= 0
		    && (sessionPeriode.dateFermeture() == null || sessionPeriode.dateFermeture().compareTo(date) >= 0)) {
			return true;
		}
		return false;
	}
}
