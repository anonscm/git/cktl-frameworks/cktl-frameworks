package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeNote;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;


/**
 *  Service qui permet de créer supprimer ou récuperer une note ou un ensemble de notes
 */
public class EONoteService {

	/**
	 * Constructeur
	 */
	public EONoteService() {
		super();
	}
	
	/**
	 * Creation d'une note
	 * @param editingContext context ed'édition
	 * @param inscripPeda element d'inscription pédagogique
	 * @param typeNote type de note
	 * @param noSession numéro de session
	 * @return INote : la note créée
	 */
	public INote creerNote(EOEditingContext editingContext,  EOInscriptionPedagogiqueElement inscripPeda, EOTypeNote typeNote, Integer noSession) {
		INote note = (INote) EONote.creerNote(editingContext, inscripPeda, typeNote, noSession);
		note.setNoSession(noSession);	
		note.setNoteSurchargee(null);
		return note; 
	}

	/**
	 * @param note note que l'on supprime
	 * @param edc  contexte d'edition
	 */
	public void supprimer(INote note, EOEditingContext edc) {
		note.toInscriptionPedagogiqueElement().removeFromToNotesRelationship(note);
		note.setToInscriptionPedagogiqueElementRelationship(null);
		edc.deleteObject((EOEnterpriseObject) note);
	}
	
	/**
	 * Retourne la note correspondante à une inscription, pour un lien et sur une session
	 * @param inscription : l'inscription
	 * @param lien : le lien
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @param typeNoteCode : le code type de la note (saisie ou calculée)
	 * @return INote : la note
	 */
	public INote getNote(IScoInscription inscription, ILien lien, Integer numeroSession, String typeNoteCode) {
		IInscriptionPedagogiqueElement inscriptionPedaElt = getInscriptionPedagogiqueElement(inscription, lien);
		if (inscriptionPedaElt != null) {
			for (INote note : inscriptionPedaElt.toNotes()) {
				if (note.toTypeNote() != null && typeNoteCode.equals(note.toTypeNote().code())) {
					if((numeroSession==null && note.noSession() == null) || (numeroSession!=null && numeroSession.equals(note.noSession()))) {
						return note;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Retourne la note initiale correspondante à une inscription, pour un lien et sur une session
	 * @param inscription : l'inscription
	 * @param lien : le lien
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @return INote : la note
	 */
	public INote getNoteInitiale(IScoInscription inscription, ILien lien, Integer numeroSession) {
		INote note = null;
		IInscriptionPedagogiqueElement inscriptionPedaElt = getInscriptionPedagogiqueElement(inscription, lien);
		if (inscriptionPedaElt != null) {
			note = getNote(inscription, lien, numeroSession, ITypeNote.NOTE_SAISIE);
			if (note == null || note.noteInitiale() == null) {
				note = getNote(inscription, lien, numeroSession, ITypeNote.NOTE_CALCULEE);
			}
		}

		return note;
	}
	
	/**
	 * Retourne la valeur de la note initiale correspondante à une inscription, pour un lien et sur une session
	 * @param scoInscription : l'inscription
	 * @param lien : le lien
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @return BigDecimal : la valeur de la note
	 */
	public BigDecimal getNoteInitialeValue(IScoInscription scoInscription, ILien lien, Integer numeroSession) {
		INote note = getNoteInitiale(scoInscription, lien, numeroSession);
		if (note != null) {
			return note.noteInitiale();
		}

		return null;
	}
	
	/**
	 * Retourne la dernière note initiale correspondante à une inscription, pour un lien et sur une session
	 * @param scoInscription : l'inscription
	 * @param lien : le lien
	 * @param numeroSession : le numéro de la session (peut être null)
	 * @return INote : la note
	 */
	public INote getDerniereNoteInitialeExistante(IScoInscription scoInscription, ILien lien, Integer numeroSession) {
		INote note = getNoteInitiale(scoInscription, lien, numeroSession);
		if (note != null) {
			return note;
		}
		
		if (numeroSession != null) {
			Integer numeroSessionTemp = numeroSession;
			
			while (numeroSessionTemp > 0) {
				numeroSessionTemp--;
				
				note = getNoteInitiale(scoInscription, lien, numeroSessionTemp);
				
				if (note != null) {
					return note;
				}
			}
		}

		return null;
	}
	
	/**
	 * Retourne la dernière valeur existante de la note initiale correspondante à une inscription, pour un lien et sur une session
	 * @param scoInscription : l'inscription
	 * @param lien : le lien
	 * @param numeroSession : le numero de la session (peut être null)
	 * @return BigDecimal : la valeur de la note
	 */
	public BigDecimal getDerniereNoteInitialeExistanteValue(IScoInscription scoInscription, ILien lien, Integer numeroSession) {
		INote note = getDerniereNoteInitialeExistante(scoInscription, lien, numeroSession);
		if (note != null) {
			return note.noteInitiale();
		}

		return null;
	}
	
	/**
	 * Retourne l'IP Element pour une inscription et pour un lien
	 * @param inscription : l'inscription
	 * @param lien : le lien
	 * @return IInscriptionPedagogiqueElement : l'IP Element
	 */
	public IInscriptionPedagogiqueElement getInscriptionPedagogiqueElement(IScoInscription inscription, ILien lien) {
		IInscriptionPedagogiqueElement inscriptionPedaElt = null;
		for (IInscriptionPedagogique inscPeda : inscription.toInscriptionsPedagogiques()) {
			for (IInscriptionPedagogiqueElement elt : inscPeda.toInscriptionPedagogiqueElements()) {
				if (elt.toLien().equals(lien)) {
					inscriptionPedaElt = elt;
				}
			}
		}

		return inscriptionPedaElt;
	}
	
}
