package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.MaquetteDataBuilder;

import com.google.common.base.Objects;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXExistsQualifier;

public class VerificateurSaiseCompleteNote {
	
	private EOEditingContext editingContext;

	public VerificateurSaiseCompleteNote(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	public static class InformationSaisieIncomplete {
		
		private ILien lienSaisieIncomplete;
		private IEtudiant etudiant;
		
		public InformationSaisieIncomplete(ILien lien, IEtudiant etudiant) {
			this.lienSaisieIncomplete = lien;
			this.etudiant = etudiant;
		}

		public IEtudiant getEtudiant() {
			return etudiant;
		}
		
		public void setEtudiant(IEtudiant etudiant) {
			this.etudiant = etudiant;
		}
		
		public ILien getLienSaisieIncomplete() {
			return lienSaisieIncomplete;
		}
		
		public void setLienSaisieIncomplete(ILien lienSaisieIncomplete) {
			this.lienSaisieIncomplete = lienSaisieIncomplete;
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(this)
					.add("lien", lienSaisieIncomplete)
					.add("etudiant", etudiant).toString();
		}
		
	}

	public List<InformationSaisieIncomplete> verifierSaisieComplete(
			IComposant diplome,
			IComposant parcoursDiplome, 
			IComposant parcoursAnnee,
			ILien lienComposant) {
		List<ILien> liensComposant = getLiensFils(lienComposant);
		EOQualifier qualifierToutesNotesNull = ERXQ.not(new ERXExistsQualifier(EONote.NOTE_INITIALE.isNotNull(), EOInscriptionPedagogiqueElement.TO_NOTES.key()));
		EOQualifier qualifierIpElement = getQualifierIpElement(diplome, parcoursDiplome, parcoursAnnee, liensComposant);
		
		EOQualifier qualifier = ERXQ.and(
				qualifierToutesNotesNull, 
				qualifierIpElement);
		
		List<EOInscriptionPedagogiqueElement> inscriptionPedagogiqueElements = EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext, qualifier, null);
		
		List<InformationSaisieIncomplete> infos = new ArrayList<VerificateurSaiseCompleteNote.InformationSaisieIncomplete>();
		for (IInscriptionPedagogiqueElement ipElement : inscriptionPedagogiqueElements) {
			ILien lien = ipElement.toLien();
			IEtudiant etudiant = ipElement.toInscriptionPedagogique().toInscription().toEtudiant();
			infos.add(new InformationSaisieIncomplete(lien, etudiant));
		}
		return infos;
	}

	private List<ILien> getLiensFils(ILien lienComposant) {
		MaquetteDataBuilder maquetteDataBuilder = 
				MaquetteDataBuilder.get()
				.editingContext(editingContext)
				.composantRacineId(lienComposant.childId())
				.loadLiens()
				.addLien(lienComposant);
		List<ILien> liens = new ArrayList<ILien>();
		liens.add(lienComposant);
		liens.addAll(maquetteDataBuilder.getData().getEnfants(lienComposant));
		return liens;
	}

	EOQualifier getQualifierIpElement(
			IComposant diplome,
			IComposant parcoursDiplome, 
			IComposant parcoursAnnee,
			List<ILien> liensComposant) {
		
		ERXKey<EOInscription> toInscription = EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE.dot(EOInscriptionPedagogique.TO_INSCRIPTION);
		EOQualifier qualifierIpElementInscriptionsDiplome = toInscription.dot(EOInscription.TO_DIPLOME).eq((EODiplome) diplome);
		EOQualifier qualifierIpElementInscriptionsParcoursDiplome = null;
		if (parcoursDiplome != null) {
			qualifierIpElementInscriptionsParcoursDiplome = toInscription.dot(EOInscription.TO_PARCOURS_DIPLOME).eq((EOParcours) parcoursDiplome);			
		}
		EOQualifier qualifierIpElementInscriptionsParcoursAnnee = null;
		if (parcoursAnnee != null) {
			qualifierIpElementInscriptionsParcoursAnnee = toInscription.dot(EOInscription.TO_PARCOURS_ANNEE).eq((EOParcours) parcoursAnnee);			
		}
		EOLien[] eoliensComposant = liensComposant.toArray(new EOLien[0]);
		EOQualifier qualifierIpElementLienSemestreEtFils = EOInscriptionPedagogiqueElement.TO_LIEN.inObjects(eoliensComposant);
		
		EOQualifier qualifier = ERXQ.and(
				qualifierIpElementInscriptionsDiplome, 
				qualifierIpElementInscriptionsParcoursDiplome, 
				qualifierIpElementInscriptionsParcoursAnnee,
				qualifierIpElementLienSemestreEtFils);
		
		return qualifier;
	}
	
	
}
