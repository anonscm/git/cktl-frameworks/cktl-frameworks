package org.cocktail.fwkcktlscolpeda.serveur.metier.note.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJury;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryConvocation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryRole;

public interface JuryRepository {

	void ajouterMembre(IJury jury, IIndividu individu, IJuryRole role);

	List<IIndividu> fetchMembres(IJury jury);

	void supprimerMembre(IJury jury, IIndividu membre);
	
	IJuryConvocation creerJuryConvocation(IJury jury);
	
	IJuryConvocation getJuryConvocationValide(IJury jury);
	
	public String juryDejaPresent(IJury jury);

}
