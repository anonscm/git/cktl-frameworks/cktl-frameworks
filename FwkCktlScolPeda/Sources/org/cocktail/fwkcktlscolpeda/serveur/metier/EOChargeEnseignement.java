package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.validation.ERXValidationFactory;


/**
 * Cette classe représente une charge d'enseignement (table CHARGE_ENSEIGNEMENT).
 */
public class EOChargeEnseignement extends _EOChargeEnseignement implements IChargeEnseignement {
	/** Numéro de série. */
	private static final long serialVersionUID = -1622520533488013535L;

	@SuppressWarnings("unused")
	private static Logger LOG = Logger.getLogger(EOChargeEnseignement.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * @param valeurHeures : la valeur en heure de l'AP
	 * @return la valeur si son format est correct (hh:mm)
	 * @throws ValidationException exception renvoyé si le format est incorrect
	 */
	public Object validateValeurHeureMinutes(BigDecimal valeurHeures) throws ValidationException {
		if (valeurHeures != null) {
			int minutes = valeurHeures.remainder(BigDecimal.ONE).movePointRight(2).intValue();
			
			if (minutes >= HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_AP.valeurHeures.formatIncorrect");
			}
		}
		
		return valeurHeures;

	}
	
	/**
	 * La colonne de la base de données ne doit pas être accédée directement.
	 * @return le nombre de minutes
	 * @deprecated Ne pas utiliser. En dehors de cette classe, utiliser plutot la méthode {@link #valeurMinutes()}.
	 */
	public BigDecimal valeurMinutesBdd() {
		return super.valeurMinutesBdd();
	}

	/**
	 * La colonne de la base de données ne doit pas être accédée directement.
	 * @param value le nombre de minutes
	 * @deprecated Ne pas utiliser. En dehors de cette classe, utiliser plutot la méthode {@link #setValeurMinutes()}.
	 */
	public void setValeurMinutesBdd(BigDecimal value) {
		super.setValeurMinutesBdd(value);
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer valeurMinutes() {
		return HeuresMinutesUtils.minuteBddEnMinute(valeurMinutesBdd());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setValeurMinutes(Integer value) {
		setValeurMinutesBdd(HeuresMinutesUtils.minuteEnMinuteBdd(value));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public BigDecimal valeurHeures() {
		return HeuresMinutesUtils.minuteBddEnCentieme(valeurMinutesBdd());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setValeurHeures(BigDecimal value) {
		setValeurMinutesBdd(HeuresMinutesUtils.centiemeEnMinuteBdd(value));
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal valeurHeureMinutes() {
		return HeuresMinutesUtils.minuteBddEnHeureMinute(valeurMinutesBdd());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setValeurHeureMinutes(BigDecimal value) {
		setValeurMinutesBdd(HeuresMinutesUtils.heureMinuteEnMinuteBdd(value));
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimer() {
		setToTypeChargeEnseignementRelationship(null);
		setToAP(null);
		setToLienRelationship(null);
		delete();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean supprimerSiSansValeurs() {
		if (valeurMinutesBdd() == null && nbGroupes() == null) {
			supprimer();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Supprimer tous les objects {@link EOChargeEnseignement} (les mettre à l'état "Delete") de l'editing context,
	 * s'ils ne contiennent aucune valeurs significatives.
	 * 
	 * @param edc l'editing context
	 */
	public static void supprimerSiSansValeurs(EOEditingContext edc) {
		// On supprime toutes les charges d'enseignements sans valeurs (ni heures ni groupe)
		for (EOGenericRecord object : (NSArray<EOGenericRecord>) edc.registeredObjects().immutableClone()) {
			if (object instanceof EOChargeEnseignement) {
				((IChargeEnseignement) object).supprimerSiSansValeurs();
			}
		}
	}
	
	public EOChargeEnseignement copySurAP(EOAP ap) {
		EOChargeEnseignement ce;
		try {
			ce = EOComposantFactory.createChargeEnseignementSurAP(ap, this.toTypeChargeEnseignement(), this.persIdCreation());
			assignTo(ce);
			return ce;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public EOChargeEnseignement copySurLien(EOLien lien) {
		EOChargeEnseignement ce;
		try {
			ce = EOComposantFactory.createChargeEnseignementSurLien(lien, this.toTypeChargeEnseignement(), this.persIdCreation());
			assignTo(ce);
			return ce;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	protected void assignTo(EOChargeEnseignement newChargeEnseignement) {
		newChargeEnseignement.setValeurMinutesBdd(valeurMinutesBdd());
		newChargeEnseignement.setNbGroupes(nbGroupes());
	}
	
	/**
     * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}
	
}