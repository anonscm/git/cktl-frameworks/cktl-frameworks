// DO NOT EDIT.  Make changes to EOParametrageInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametrageInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ParametrageInscription";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> TO_PARAMETRAGE_CHAMPS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>("toParametrageChamps");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships
  public static final String TO_PARAMETRAGE_CHAMPS_KEY = TO_PARAMETRAGE_CHAMPS.key();

  private static Logger LOG = Logger.getLogger(_EOParametrageInscription.class);

  public EOParametrageInscription localInstanceIn(EOEditingContext editingContext) {
    EOParametrageInscription localInstance = (EOParametrageInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametrageInscription.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
    	_EOParametrageInscription.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageInscription.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOParametrageInscription.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
    	_EOParametrageInscription.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageInscription.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOParametrageInscription.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
    	_EOParametrageInscription.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageInscription.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOParametrageInscription.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
    	_EOParametrageInscription.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageInscription.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOParametrageInscription.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
    	_EOParametrageInscription.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametrageInscription.PERS_ID_MODIFCATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)storedValueForKey(_EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier) {
    return toParametrageChamps(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier, boolean fetch) {
    return toParametrageChamps(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> toParametrageChamps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.TO_PARAMETRAGE_INSCRIPTION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.fetchSco_ParametrageChamps(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toParametrageChamps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToParametrageChamps(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    includeObjectIntoPropertyWithKey(object, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public void removeFromToParametrageChamps(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    excludeObjectFromPropertyWithKey(object, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
  }

  public void addToToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
      _EOParametrageInscription.LOG.debug("adding " + object + " to toParametrageChamps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToParametrageChamps(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
    }
  }

  public void removeFromToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    if (_EOParametrageInscription.LOG.isDebugEnabled()) {
      _EOParametrageInscription.LOG.debug("removing " + object + " from toParametrageChamps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToParametrageChamps(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp createToParametrageChampsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp) eo;
  }

  public void deleteToParametrageChampsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParametrageInscription.TO_PARAMETRAGE_CHAMPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToParametrageChampsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageChamp> objects = toParametrageChamps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToParametrageChampsRelationship(objects.nextElement());
    }
  }


  public static EOParametrageInscription createSco_ParametrageInscription(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, Integer persIdCreation
, Integer persIdModifcation
) {
    EOParametrageInscription eo = (EOParametrageInscription) EOUtilities.createAndInsertInstance(editingContext, _EOParametrageInscription.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModifcation(persIdModifcation);
    return eo;
  }

  public static ERXFetchSpecification<EOParametrageInscription> fetchSpec() {
    return new ERXFetchSpecification<EOParametrageInscription>(_EOParametrageInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametrageInscription> fetchAllSco_ParametrageInscriptions(EOEditingContext editingContext) {
    return _EOParametrageInscription.fetchAllSco_ParametrageInscriptions(editingContext, null);
  }

  public static NSArray<EOParametrageInscription> fetchAllSco_ParametrageInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametrageInscription.fetchSco_ParametrageInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametrageInscription> fetchSco_ParametrageInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametrageInscription> fetchSpec = new ERXFetchSpecification<EOParametrageInscription>(_EOParametrageInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametrageInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametrageInscription fetchSco_ParametrageInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametrageInscription.fetchSco_ParametrageInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametrageInscription fetchSco_ParametrageInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametrageInscription> eoObjects = _EOParametrageInscription.fetchSco_ParametrageInscriptions(editingContext, qualifier, null);
    EOParametrageInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ParametrageInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametrageInscription fetchRequiredSco_ParametrageInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametrageInscription.fetchRequiredSco_ParametrageInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametrageInscription fetchRequiredSco_ParametrageInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametrageInscription eoObject = _EOParametrageInscription.fetchSco_ParametrageInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ParametrageInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametrageInscription localInstanceIn(EOEditingContext editingContext, EOParametrageInscription eo) {
    EOParametrageInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
