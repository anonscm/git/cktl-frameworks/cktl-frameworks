// DO NOT EDIT.  Make changes to EOPaiementEcheance.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPaiementEcheance extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PaiementEcheance";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_ECHEANCE = new ERXKey<NSTimestamp>("dateEcheance");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> TO_AUTRE_PAIEMENT_MOYEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>("toAutrePaiementMoyen");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen> TO_PAIEMENT_MOYEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen>("toPaiementMoyen");

  // Attributes
  public static final String DATE_ECHEANCE_KEY = DATE_ECHEANCE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships
  public static final String TO_AUTRE_PAIEMENT_MOYEN_KEY = TO_AUTRE_PAIEMENT_MOYEN.key();
  public static final String TO_PAIEMENT_MOYEN_KEY = TO_PAIEMENT_MOYEN.key();

  private static Logger LOG = Logger.getLogger(_EOPaiementEcheance.class);

  public EOPaiementEcheance localInstanceIn(EOEditingContext editingContext) {
    EOPaiementEcheance localInstance = (EOPaiementEcheance)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateEcheance() {
    return (NSTimestamp) storedValueForKey(_EOPaiementEcheance.DATE_ECHEANCE_KEY);
  }

  public void setDateEcheance(NSTimestamp value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating dateEcheance from " + dateEcheance() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.DATE_ECHEANCE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPaiementEcheance.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPaiementEcheance.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOPaiementEcheance.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.LIBELLE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementEcheance.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.MONTANT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPaiementEcheance.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPaiementEcheance.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
    	_EOPaiementEcheance.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementEcheance.PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen toAutrePaiementMoyen() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen)storedValueForKey(_EOPaiementEcheance.TO_AUTRE_PAIEMENT_MOYEN_KEY);
  }
  
  public void setToAutrePaiementMoyen(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen value) {
    takeStoredValueForKey(value, _EOPaiementEcheance.TO_AUTRE_PAIEMENT_MOYEN_KEY);
  }

  public void setToAutrePaiementMoyenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
      _EOPaiementEcheance.LOG.debug("updating toAutrePaiementMoyen from " + toAutrePaiementMoyen() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAutrePaiementMoyen(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen oldValue = toAutrePaiementMoyen();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementEcheance.TO_AUTRE_PAIEMENT_MOYEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementEcheance.TO_AUTRE_PAIEMENT_MOYEN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen toPaiementMoyen() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen)storedValueForKey(_EOPaiementEcheance.TO_PAIEMENT_MOYEN_KEY);
  }
  
  public void setToPaiementMoyen(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen value) {
    takeStoredValueForKey(value, _EOPaiementEcheance.TO_PAIEMENT_MOYEN_KEY);
  }

  public void setToPaiementMoyenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen value) {
    if (_EOPaiementEcheance.LOG.isDebugEnabled()) {
      _EOPaiementEcheance.LOG.debug("updating toPaiementMoyen from " + toPaiementMoyen() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPaiementMoyen(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen oldValue = toPaiementMoyen();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementEcheance.TO_PAIEMENT_MOYEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementEcheance.TO_PAIEMENT_MOYEN_KEY);
    }
  }
  

  public static EOPaiementEcheance createSco_PaiementEcheance(EOEditingContext editingContext, NSTimestamp dateEcheance
, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, java.math.BigDecimal montant
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen toPaiementMoyen) {
    EOPaiementEcheance eo = (EOPaiementEcheance) EOUtilities.createAndInsertInstance(editingContext, _EOPaiementEcheance.ENTITY_NAME);    
		eo.setDateEcheance(dateEcheance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToPaiementMoyenRelationship(toPaiementMoyen);
    return eo;
  }

  public static ERXFetchSpecification<EOPaiementEcheance> fetchSpec() {
    return new ERXFetchSpecification<EOPaiementEcheance>(_EOPaiementEcheance.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPaiementEcheance> fetchAllSco_PaiementEcheances(EOEditingContext editingContext) {
    return _EOPaiementEcheance.fetchAllSco_PaiementEcheances(editingContext, null);
  }

  public static NSArray<EOPaiementEcheance> fetchAllSco_PaiementEcheances(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPaiementEcheance.fetchSco_PaiementEcheances(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPaiementEcheance> fetchSco_PaiementEcheances(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPaiementEcheance> fetchSpec = new ERXFetchSpecification<EOPaiementEcheance>(_EOPaiementEcheance.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPaiementEcheance> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPaiementEcheance fetchSco_PaiementEcheance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementEcheance.fetchSco_PaiementEcheance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementEcheance fetchSco_PaiementEcheance(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPaiementEcheance> eoObjects = _EOPaiementEcheance.fetchSco_PaiementEcheances(editingContext, qualifier, null);
    EOPaiementEcheance eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PaiementEcheance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementEcheance fetchRequiredSco_PaiementEcheance(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementEcheance.fetchRequiredSco_PaiementEcheance(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementEcheance fetchRequiredSco_PaiementEcheance(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPaiementEcheance eoObject = _EOPaiementEcheance.fetchSco_PaiementEcheance(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PaiementEcheance that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementEcheance localInstanceIn(EOEditingContext editingContext, EOPaiementEcheance eo) {
    EOPaiementEcheance localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
