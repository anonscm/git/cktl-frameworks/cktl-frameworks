package org.cocktail.fwkcktlscolpeda.serveur.metier;

public class ModePaiementChequePrelevement implements IModePaiement {

	private static ModePaiementChequePrelevement modePaiementChequePrelevement;
	
	public String code() {
		return IModePaiement.CODE_MODE_CHEQUE_PRELEVEMENT;
	}

	public String libelle() {
		return "Chèque et prélèvement";
	}

	public String modCodePaiement() {
		return IModePaiement.CODE_MODE_CHEQUE_PRELEVEMENT;
	}

	public String modCodeRemboursement() {
		return IModePaiement.CODE_MODE_CHEQUE_PRELEVEMENT;
	}

	public Integer ordre() {
		return 10;
	}

	public String getPlanComptablePaiementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}

	public String getPlanComptableRemboursementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}
	
	public static IModePaiement instance() {
		if (modePaiementChequePrelevement == null) {
			modePaiementChequePrelevement = new ModePaiementChequePrelevement();
		}
		return modePaiementChequePrelevement;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSimple() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isPrelevement() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isCheque() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
    public boolean isVirement() {
    	return false;
    }
}
