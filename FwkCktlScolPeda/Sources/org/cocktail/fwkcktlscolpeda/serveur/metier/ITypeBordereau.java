package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Cette interface représente un type de bordereau.
 * 
 * @author Pascal MACOUIN
 */
public interface ITypeBordereau {

	/** Code pour le type PAIEMENT. */
    String CODE_PAIEMENT = "PAIEMENT";
    /** Code pour le type REMBOURSEMENT. */
    String CODE_REMBOURSEMENT = "REMBOURSEMENT";

	/**
	 * @return Le code du type de bordereau
	 */
	String code();

	/**
	 * @param value Un code type de bordereau
	 */
	void setCode(String value);

	/**
	 * @return Le libellé du type de bordereau
	 */
	String libelle();

	/**
	 * @param value Un libellé pour ce type de bordereau
	 */
	void setLibelle(String value);

	/**
	 * @return Le libellé court du type de bordereau
	 */
	String libelleCourt();

	/**
	 * @param value Un libellé court pour ce type de bordereau
	 */
	void setLibelleCourt(String value);
	
	/**
	 * @return <code>true</code> si c'est un type de bordereau de paiement
	 */
	boolean isPaiement();
	
	/**
	 * @return <code>true</code> si c'est un type de bordereau de remboursement
	 */
	boolean isRemboursement();
}