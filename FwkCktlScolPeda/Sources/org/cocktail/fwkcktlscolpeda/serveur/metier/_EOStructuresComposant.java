// DO NOT EDIT.  Make changes to EOStructuresComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOStructuresComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_StructuresComposant";

  // Attribute Keys
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  // Relationship Keys

  // Attributes
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String C_STRUCTURE_KEY = C_STRUCTURE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOStructuresComposant.class);

  public EOStructuresComposant localInstanceIn(EOEditingContext editingContext) {
    EOStructuresComposant localInstance = (EOStructuresComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOStructuresComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOStructuresComposant.LOG.isDebugEnabled()) {
    	_EOStructuresComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStructuresComposant.COMPOSANT_ID_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(_EOStructuresComposant.C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    if (_EOStructuresComposant.LOG.isDebugEnabled()) {
    	_EOStructuresComposant.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, _EOStructuresComposant.C_STRUCTURE_KEY);
  }


  public static EOStructuresComposant createSco_StructuresComposant(EOEditingContext editingContext, Integer composantId
, String cStructure
) {
    EOStructuresComposant eo = (EOStructuresComposant) EOUtilities.createAndInsertInstance(editingContext, _EOStructuresComposant.ENTITY_NAME);    
		eo.setComposantId(composantId);
		eo.setCStructure(cStructure);
    return eo;
  }

  public static ERXFetchSpecification<EOStructuresComposant> fetchSpec() {
    return new ERXFetchSpecification<EOStructuresComposant>(_EOStructuresComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOStructuresComposant> fetchAllSco_StructuresComposants(EOEditingContext editingContext) {
    return _EOStructuresComposant.fetchAllSco_StructuresComposants(editingContext, null);
  }

  public static NSArray<EOStructuresComposant> fetchAllSco_StructuresComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStructuresComposant.fetchSco_StructuresComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStructuresComposant> fetchSco_StructuresComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOStructuresComposant> fetchSpec = new ERXFetchSpecification<EOStructuresComposant>(_EOStructuresComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStructuresComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOStructuresComposant fetchSco_StructuresComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructuresComposant.fetchSco_StructuresComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructuresComposant fetchSco_StructuresComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStructuresComposant> eoObjects = _EOStructuresComposant.fetchSco_StructuresComposants(editingContext, qualifier, null);
    EOStructuresComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_StructuresComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructuresComposant fetchRequiredSco_StructuresComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructuresComposant.fetchRequiredSco_StructuresComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructuresComposant fetchRequiredSco_StructuresComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStructuresComposant eoObject = _EOStructuresComposant.fetchSco_StructuresComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_StructuresComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructuresComposant localInstanceIn(EOEditingContext editingContext, EOStructuresComposant eo) {
    EOStructuresComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
