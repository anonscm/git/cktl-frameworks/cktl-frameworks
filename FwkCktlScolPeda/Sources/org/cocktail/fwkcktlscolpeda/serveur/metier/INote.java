package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * note pour une inscription pedagoique sur un élément et une session
 */
public interface INote {

	public static final int SESSION_1 = 1;
	public static final int SESSION_2 = 2;
	public static final int SESSION_3 = 3;
	
	/**
	 * @return numéro de la session
	 */
	Integer noSession();
	
	
	/**
	 * @param noSession numéro de la session
	 */
	void setNoSession(Integer noSession);

	/**
	 * @return note initiale
	 */
	BigDecimal noteInitiale();
	
	/**
	 * @param noteInitiale  note initiale
	 */
	void setNoteInitiale(BigDecimal noteInitiale);

	/**
	 * @return note surchargée
	 */
	BigDecimal noteSurchargee();
	
	/**
	 * @param object note surchargée
	 */
	void setNoteSurchargee(BigDecimal object);
	/**
	 * @return lien vers l'élément d'inscription pédagogique
	 */
	IInscriptionPedagogiqueElement toInscriptionPedagogiqueElement();
	
	/**
	 * @param object element dinscription pédagogique
	 */
	void setToInscriptionPedagogiqueElementRelationship(IInscriptionPedagogiqueElement object);
	
	/**
	 * @return lien vers le type de note (saisie, calculée)
	 */
	ITypeNote toTypeNote();

	/**
	 * @param object type de note
	 */
	void setToTypeNoteRelationship(ITypeNote object);
	
	/**
	 * @param noSession le numéro de session
	 * @return true si la note correspond à la session donnée
	 */
	boolean isSession(int noSession);


	/**
	 * @return true si la note est calculée (moyennes)
	 */
	boolean isCalculee();


	/**
	 * @return true si la note est saisie
	 */
	boolean isSaisie();
	
	/**
	 * 
	 * @return object listeResultat
	 */
	INoteResultat toNoteResultat();
	
	/**
	 * 
	 * @param value object listeResultat
	 */
	void setToNoteResultatRelationship(INoteResultat value);
	
	/**
	 * @param absence true si absence
	 */
	void setAbsence(Boolean absence);
	
	/**
	 * @return true s'il y a une absence (= pas de note à saisir mais un motif)
	 */
	Boolean absence();
	
	/**
	 * @param code le code abscence
	 * @see Absence#code
	 */
	void setCodeAbsence(String code);
	
	/**
	 * @return le code de l'abscence
	 * @see Absence#code
	 */
	String codeAbsence();

}
