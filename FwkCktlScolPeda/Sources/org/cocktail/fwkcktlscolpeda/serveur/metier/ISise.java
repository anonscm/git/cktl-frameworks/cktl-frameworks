package org.cocktail.fwkcktlscolpeda.serveur.metier;


public interface ISise {

	public static final String ORACLE_EMPTY_STRING = " ";
	public static final String ECOLE = "ECOLE";
	public static final String UNIV = "UNIV";
	
	/**
	 * @return Profil - cursus aménagé
	 */
	String amena();

	/**
	 * @param value Profil - cursus aménagé
	 */
	void setAmena(String value);

	/**
	 * @return année d'obtention du bac
	 */
	String anbac();

	/**
	 * @param value année d'obtention du bac
	 */
	void setAnbac(String value);

	/**
	 * @return année de première inscription dans l'établissement
	 */
	String anetab();

	/**
	 * @param value année de première inscription dans l'établissement
	 */
	void setAnetab(String value);

	/**
	 * @return année de naissance
	 */
	String annais();

	/**
	 * @param value année de naissance
	 */
	void setAnnais(String value);

	/**
	 * @return année d'entrée dans l'enseignement supérieur
	 */
	String ansup();

	/**
	 * @param value année d'entrée dans l'enseignement supérieur
	 */
	void setAnsup(String value);

	/**
	 * @return Baccalauréat et équivalence
	 */
	String bac();

	/**
	 * @param value Baccalauréat et équivalence
	 */
	void setBac(String value);

	/**
	 * @return unité de rattachement (composante)
	 */
	String compos();

	/**
	 * @param value unité de rattachement (composante)
	 */
	void setCompos(String value);

	/**
	 * @return code postal de la résidence de l'étudiant
	 */
	String cpEtu();

	/**
	 * @param value code postal de la résidence de l'étudiant
	 */
	void setCpEtu(String value);

	/**
	 * @return code postal de la résidence de référence
	 */
	String cpPar();

	/**
	 * @param value code postal de la résidence de référence
	 */
	void setCpPar(String value);

	/**
	 * @return autre établissement fréquenté dans l'année
	 */
	String curpar();

	/**
	 * @param value autre établissement fréquenté dans l'année
	 */
	void setCurpar(String value);

	/**
	 * @return type du dernier diplome obtenu
	 */
	String dipder();

	/**
	 * @param value type du dernier diplome obtenu
	 */
	void setDipder(String value);

	/**
	 * @return diplome
	 */
	String diplom();

	/**
	 * @param value diplome
	 */
	void setDiplom(String value);

	/**
	 * @return programme d'échange international
	 */
	String echang();

	/**
	 * @param value programme d'échange international
	 */
	void setEchang(String value);

	/**
	 * @return fonctionnaire stagiaire ou militaire
	 */
	String fonctio();

	/**
	 * @param value fonctionnaire stagiaire ou militaire
	 */
	void setFonctio(String value);

	/**
	 * @return identifiant national etudiant
	 */
	String idetu();

	/**
	 * @param value identifiant national etudiant
	 */
	void setIdetu(String value);

	/**
	 * @return inscription première
	 */
	String inspr();

	/**
	 * @param value inscription première
	 */
	void setInspr(String value);

	/**
	 * @return commune de résidence de l'étudiant (en clair)
	 */
	String lcomEtu();

	/**
	 * @param value commune de résidence de l'étudiant (en clair)
	 */
	void setLcomEtu(String value);

	/**
	 * @return commune de résidence de référence (en clair)
	 */
	String lComRef();

	/**
	 * @param value commune de résidence de référence (en clair)
	 */
	void setLComRef(String value);

	/**
	 * @return nationalité
	 */
	String nation();

	/**
	 * @param value nationalité
	 */
	void setNation(String value);

	/**
	 * @return niveau dans le diplome (année d'étude)
	 */
	String niveau();

	/**
	 * @param value niveau dans le diplome (année d'étude)
	 */
	void setNiveau(String value);

	/**
	 * @return numéro de l'école doctorale
	 */
	String numed();

	/**
	 * @param value numéro de l'école doctorale
	 */
	void setNumed(String value);

	/**
	 * @return numéro d'inscription
	 */
	String numins();

	/**
	 * @param value numéro d'inscription
	 */
	void setNumins(String value);

	/**
	 * @return pays de résidence de l'étudiant
	 */
	String paripa();

	/**
	 * @param value pays de résidence de l'étudiant
	 */
	void setParipa(String value);

	/**
	 * @return pays de résidence de référence
	 */
	String paypar();

	/**
	 * @param value pays de résidence de référence
	 */
	void setPaypar(String value);

	/**
	 * @return PCS du parent référence
	 */
	String pcspar();

	/**
	 * @param value PCS du parent référence
	 */
	void setPcspar(String value);

	/**
	 * @return PCS du second parent référence
	 */
	String pcspar2();

	/**
	 * @param value PCS du second parent référence
	 */
	void setPcspar2(String value);

	/**
	 * @return régime d'inscription
	 */
	String regime();

	/**
	 * @param value régime d'inscription
	 */
	void setRegime(String value);

	/**
	 * @return reimmatriculation (du numero INSEE)
	 */
	String reimma();

	/**
	 * @param value reimmatriculation (du numero INSEE)
	 */
	void setReimma(String value);

	/**
	 * @return sexe (1: masculin, 2: féminin)
	 */
	String sexe();

	/**
	 * @param value sexe (1: masculin, 2: féminin)
	 */
	void setSexe(String value);

	/**
	 * @return situation l'année précedente
	 */
	String situpre();

	/**
	 * @param value situation l'année précedente
	 */
	void setSitupre(String value);

	/**
	 * @return spécialité 1
	 */
	String speciA();

	/**
	 * @param value spécialité 1
	 */
	void setSpeciA(String value);

	/**
	 * @return spécialité 2
	 */
	String speciB();

	/**
	 * @param value spécialité 2
	 */
	void setSpeciB(String value);

	/**
	 * @return spécialité 3
	 */
	String speciC();

	/**
	 * @param value spécialité 3
	 */
	void setSpeciC(String value);

	/**
	 * @return type de classe préparatoire
	 */
	String typrepa();

	/**
	 * @param value type de classe préparatoire
	 */
	void setTyprepa(String value);
	
	String acabac();

	void setAcabac(String value);

	String activi();

	void setActivi(String value);

	String aninsc();

	void setAninsc(String value);

	String cge();

	void setCge(String value);

	String cometu();

	void setCometu(String value);

	String comref();

	void setComref(String value);

	String conv();

	void setConv(String value);

	String depbac();

	void setDepbac(String value);

	String dispens();

	void setDispens(String value);

	String dom1();

	void setDom1(String value);

	String dom2();

	void setDom2(String value);

	String dom3();

	void setDom3(String value);

	String dom4();

	void setDom4(String value);

	String dom5();

	void setDom5(String value);

	String jonais();

	void setJonais(String value);

	String monais();

	void setMonais(String value);

	String parType();

	void setParType(String value);

	String telens();

	void setTelens(String value);

}