package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.repositories.IPaysIndicatifRepository;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class EOPersonneTelephoneFactory implements ITelephoneFactory {
	
    private IPaysIndicatifRepository indicatifRepository;
    private EOEditingContext editingContext;
    private Integer persIdCreateur;

    /**
     * @param editingContext l'editingcontext
     * @param indicatifRepository le repo des indicatifs
     * @param persIdCreateur le persId du createur
     */
    public EOPersonneTelephoneFactory(
            EOEditingContext editingContext, 
            IPaysIndicatifRepository indicatifRepository,
            Integer persIdCreateur) {
        this.editingContext = editingContext;
        this.persIdCreateur = persIdCreateur;
        this.indicatifRepository = indicatifRepository;
    }
 

	public EOPersonneTelephone creerTelephoneFixe(IInfosEtudiant etudiant,
			ITypeTel typeTelephone) {
		return creerTelephone(etudiant, EOTypeNoTel.typeFixe(editingContext), typeTelephone);
	}

	public EOPersonneTelephone creerTelephoneGsm(IInfosEtudiant etudiant,
			ITypeTel typeTelephone) {
		return creerTelephone(etudiant, EOTypeNoTel.typePortable(editingContext), typeTelephone);
	}

	public EOPersonneTelephone creerTelephoneFixeEtudiant(IInfosEtudiant etudiant) {
		return creerTelephone(etudiant, EOTypeNoTel.typeFixe(editingContext), EOTypeTel.typeEtudiant(editingContext));
	}

	public EOPersonneTelephone creerTelephoneFixeParent(IInfosEtudiant etudiant) {
		return creerTelephone(etudiant, EOTypeNoTel.typeFixe(editingContext), EOTypeTel.typeParent(editingContext));
	}

	public EOPersonneTelephone creerTelephoneGsmEtudiant(IInfosEtudiant etudiant) {
		return creerTelephone(etudiant, EOTypeNoTel.typePortable(editingContext), EOTypeTel.typeEtudiant(editingContext));
	}

	public ITelephone creerTelephoneGsmParent(IInfosEtudiant etudiant) {
		return creerTelephone(etudiant, EOTypeNoTel.typePortable(editingContext), EOTypeTel.typeParent(editingContext));
	}
	
	private EOPersonneTelephone creerTelephone (IInfosEtudiant etudiant, ITypeNoTel typeNoTel, ITypeTel typeTel) {
		// On ne peux pas modifier un n° de téléphone (n° en clé primaire)
		// On supprime tous ces téléphones de ce type et type pour le re-créer avec le nouveau n°
		EOQualifier qualifier = ERXQ.and(EOPersonneTelephone.TO_TYPE_NO_TEL.eq((EOTypeNoTel) typeNoTel),
		    EOPersonneTelephone.TO_TYPE_TEL.eq((EOTypeTel) typeTel));

		NSArray<EOPersonneTelephone> listeTel = ((EOIndividu) etudiant.asIndividu()).toPersonneTelephones(qualifier);

		for (EOPersonneTelephone personneTelephone : listeTel) {
			personneTelephone.delete();
		}

		EOPersonneTelephone personneTelephone = EOPersonneTelephone.creerInstance(editingContext);
		personneTelephone.setToTypeTelRelationship((EOTypeTel) typeTel);
		personneTelephone.setToTypeNoTelRelationship((EOTypeNoTel) typeNoTel);
		
		personneTelephone.setDCreation(new NSTimestamp());
		personneTelephone.setToPersonneRelationship((EOIndividu) etudiant.asIndividu());
		personneTelephone.setIndicatif(defaultPaysIndicatif());
		personneTelephone.setListeRouge("N");
		
		return personneTelephone;
	}

	 private Integer defaultPaysIndicatif() {
	        IPaysIndicatif indicatif = indicatifRepository.defaultPaysIndicatif();
	        return indicatif.indicatif();
	    }
}
