// DO NOT EDIT.  Make changes to EOJuryPersonnes.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOJuryPersonnes extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_JuryPersonnes";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> PRESENT = new ERXKey<Boolean>("present");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury> TO_JURY = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury>("toJury");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole> TO_JURY_ROLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole>("toJuryRole");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String PRESENT_KEY = PRESENT.key();
  // Relationships
  public static final String TO_INDIVIDU_KEY = TO_INDIVIDU.key();
  public static final String TO_JURY_KEY = TO_JURY.key();
  public static final String TO_JURY_ROLE_KEY = TO_JURY_ROLE.key();

  private static Logger LOG = Logger.getLogger(_EOJuryPersonnes.class);

  public EOJuryPersonnes localInstanceIn(EOEditingContext editingContext) {
    EOJuryPersonnes localInstance = (EOJuryPersonnes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOJuryPersonnes.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
    	_EOJuryPersonnes.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryPersonnes.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOJuryPersonnes.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
    	_EOJuryPersonnes.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryPersonnes.D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOJuryPersonnes.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
    	_EOJuryPersonnes.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryPersonnes.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOJuryPersonnes.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
    	_EOJuryPersonnes.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryPersonnes.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean present() {
    return (Boolean) storedValueForKey(_EOJuryPersonnes.PRESENT_KEY);
  }

  public void setPresent(Boolean value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
    	_EOJuryPersonnes.LOG.debug( "updating present from " + present() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryPersonnes.PRESENT_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(_EOJuryPersonnes.TO_INDIVIDU_KEY);
  }
  
  public void setToIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, _EOJuryPersonnes.TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
      _EOJuryPersonnes.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToIndividu(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJuryPersonnes.TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJuryPersonnes.TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury toJury() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury)storedValueForKey(_EOJuryPersonnes.TO_JURY_KEY);
  }
  
  public void setToJury(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury value) {
    takeStoredValueForKey(value, _EOJuryPersonnes.TO_JURY_KEY);
  }

  public void setToJuryRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
      _EOJuryPersonnes.LOG.debug("updating toJury from " + toJury() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToJury(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury oldValue = toJury();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJuryPersonnes.TO_JURY_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJuryPersonnes.TO_JURY_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole toJuryRole() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole)storedValueForKey(_EOJuryPersonnes.TO_JURY_ROLE_KEY);
  }
  
  public void setToJuryRole(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole value) {
    takeStoredValueForKey(value, _EOJuryPersonnes.TO_JURY_ROLE_KEY);
  }

  public void setToJuryRoleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole value) {
    if (_EOJuryPersonnes.LOG.isDebugEnabled()) {
      _EOJuryPersonnes.LOG.debug("updating toJuryRole from " + toJuryRole() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToJuryRole(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole oldValue = toJuryRole();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJuryPersonnes.TO_JURY_ROLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJuryPersonnes.TO_JURY_ROLE_KEY);
    }
  }
  

  public static EOJuryPersonnes createSco_JuryPersonnes(EOEditingContext editingContext, NSTimestamp dCreation
, Integer persIdCreation
, Boolean present
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlscolpeda.serveur.metier.EOJury toJury, org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryRole toJuryRole) {
    EOJuryPersonnes eo = (EOJuryPersonnes) EOUtilities.createAndInsertInstance(editingContext, _EOJuryPersonnes.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
		eo.setPresent(present);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToJuryRelationship(toJury);
    eo.setToJuryRoleRelationship(toJuryRole);
    return eo;
  }

  public static ERXFetchSpecification<EOJuryPersonnes> fetchSpec() {
    return new ERXFetchSpecification<EOJuryPersonnes>(_EOJuryPersonnes.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOJuryPersonnes> fetchAllSco_JuryPersonneses(EOEditingContext editingContext) {
    return _EOJuryPersonnes.fetchAllSco_JuryPersonneses(editingContext, null);
  }

  public static NSArray<EOJuryPersonnes> fetchAllSco_JuryPersonneses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJuryPersonnes.fetchSco_JuryPersonneses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJuryPersonnes> fetchSco_JuryPersonneses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOJuryPersonnes> fetchSpec = new ERXFetchSpecification<EOJuryPersonnes>(_EOJuryPersonnes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJuryPersonnes> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOJuryPersonnes fetchSco_JuryPersonnes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryPersonnes.fetchSco_JuryPersonnes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryPersonnes fetchSco_JuryPersonnes(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJuryPersonnes> eoObjects = _EOJuryPersonnes.fetchSco_JuryPersonneses(editingContext, qualifier, null);
    EOJuryPersonnes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_JuryPersonnes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryPersonnes fetchRequiredSco_JuryPersonnes(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryPersonnes.fetchRequiredSco_JuryPersonnes(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryPersonnes fetchRequiredSco_JuryPersonnes(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJuryPersonnes eoObject = _EOJuryPersonnes.fetchSco_JuryPersonnes(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_JuryPersonnes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryPersonnes localInstanceIn(EOEditingContext editingContext, EOJuryPersonnes eo) {
    EOJuryPersonnes localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
