// DO NOT EDIT.  Make changes to EOTypeAP.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeAP extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeAP";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<java.math.BigDecimal> HEURE_EQUIVALENT = new ERXKey<java.math.BigDecimal>("heureEquivalent");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String HEURE_EQUIVALENT_KEY = HEURE_EQUIVALENT.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeAP.class);

  public EOTypeAP localInstanceIn(EOEditingContext editingContext) {
    EOTypeAP localInstance = (EOTypeAP)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeAP.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeAP.LOG.isDebugEnabled()) {
    	_EOTypeAP.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAP.CODE_KEY);
  }

  public java.math.BigDecimal heureEquivalent() {
    return (java.math.BigDecimal) storedValueForKey(_EOTypeAP.HEURE_EQUIVALENT_KEY);
  }

  public void setHeureEquivalent(java.math.BigDecimal value) {
    if (_EOTypeAP.LOG.isDebugEnabled()) {
    	_EOTypeAP.LOG.debug( "updating heureEquivalent from " + heureEquivalent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAP.HEURE_EQUIVALENT_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeAP.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeAP.LOG.isDebugEnabled()) {
    	_EOTypeAP.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAP.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeAP.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeAP.LOG.isDebugEnabled()) {
    	_EOTypeAP.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAP.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOTypeAP.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOTypeAP.LOG.isDebugEnabled()) {
    	_EOTypeAP.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeAP.READ_ONLY_KEY);
  }


  public static EOTypeAP createSco_TypeAP(EOEditingContext editingContext, String code
, Integer id
, String libelle
, Integer readOnly
) {
    EOTypeAP eo = (EOTypeAP) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAP.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeAP> fetchSpec() {
    return new ERXFetchSpecification<EOTypeAP>(_EOTypeAP.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeAP> fetchAllSco_TypeAPs(EOEditingContext editingContext) {
    return _EOTypeAP.fetchAllSco_TypeAPs(editingContext, null);
  }

  public static NSArray<EOTypeAP> fetchAllSco_TypeAPs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAP.fetchSco_TypeAPs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAP> fetchSco_TypeAPs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeAP> fetchSpec = new ERXFetchSpecification<EOTypeAP>(_EOTypeAP.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAP> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeAP fetchSco_TypeAP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAP.fetchSco_TypeAP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAP fetchSco_TypeAP(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAP> eoObjects = _EOTypeAP.fetchSco_TypeAPs(editingContext, qualifier, null);
    EOTypeAP eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeAP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAP fetchRequiredSco_TypeAP(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAP.fetchRequiredSco_TypeAP(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAP fetchRequiredSco_TypeAP(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAP eoObject = _EOTypeAP.fetchSco_TypeAP(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeAP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAP localInstanceIn(EOEditingContext editingContext, EOTypeAP eo) {
    EOTypeAP localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
