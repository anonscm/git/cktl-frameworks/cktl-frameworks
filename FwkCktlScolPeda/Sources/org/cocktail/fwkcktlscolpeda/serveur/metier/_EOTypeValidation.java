// DO NOT EDIT.  Make changes to EOTypeValidation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeValidation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeValidation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeValidation.class);

  public EOTypeValidation localInstanceIn(EOEditingContext editingContext) {
    EOTypeValidation localInstance = (EOTypeValidation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeValidation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeValidation.LOG.isDebugEnabled()) {
    	_EOTypeValidation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeValidation.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeValidation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeValidation.LOG.isDebugEnabled()) {
    	_EOTypeValidation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeValidation.LIBELLE_KEY);
  }


  public static EOTypeValidation createSco_TypeValidation(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeValidation eo = (EOTypeValidation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeValidation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeValidation> fetchSpec() {
    return new ERXFetchSpecification<EOTypeValidation>(_EOTypeValidation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeValidation> fetchAllSco_TypeValidations(EOEditingContext editingContext) {
    return _EOTypeValidation.fetchAllSco_TypeValidations(editingContext, null);
  }

  public static NSArray<EOTypeValidation> fetchAllSco_TypeValidations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeValidation.fetchSco_TypeValidations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeValidation> fetchSco_TypeValidations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeValidation> fetchSpec = new ERXFetchSpecification<EOTypeValidation>(_EOTypeValidation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeValidation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeValidation fetchSco_TypeValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeValidation.fetchSco_TypeValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeValidation fetchSco_TypeValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeValidation> eoObjects = _EOTypeValidation.fetchSco_TypeValidations(editingContext, qualifier, null);
    EOTypeValidation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeValidation fetchRequiredSco_TypeValidation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeValidation.fetchRequiredSco_TypeValidation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeValidation fetchRequiredSco_TypeValidation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeValidation eoObject = _EOTypeValidation.fetchSco_TypeValidation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeValidation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeValidation localInstanceIn(EOEditingContext editingContext, EOTypeValidation eo) {
    EOTypeValidation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
