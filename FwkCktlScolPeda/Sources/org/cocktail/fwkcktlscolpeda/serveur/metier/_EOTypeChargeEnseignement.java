// DO NOT EDIT.  Make changes to EOTypeChargeEnseignement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeChargeEnseignement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeChargeEnseignement";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeChargeEnseignement.class);

  public EOTypeChargeEnseignement localInstanceIn(EOEditingContext editingContext) {
    EOTypeChargeEnseignement localInstance = (EOTypeChargeEnseignement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeChargeEnseignement.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOTypeChargeEnseignement.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeChargeEnseignement.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeChargeEnseignement.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeChargeEnseignement.LOG.isDebugEnabled()) {
    	_EOTypeChargeEnseignement.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeChargeEnseignement.LIBELLE_KEY);
  }


  public static EOTypeChargeEnseignement createSco_TypeChargeEnseignement(EOEditingContext editingContext, String code
, String libelle
) {
    EOTypeChargeEnseignement eo = (EOTypeChargeEnseignement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeChargeEnseignement.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeChargeEnseignement> fetchSpec() {
    return new ERXFetchSpecification<EOTypeChargeEnseignement>(_EOTypeChargeEnseignement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeChargeEnseignement> fetchAllSco_TypeChargeEnseignements(EOEditingContext editingContext) {
    return _EOTypeChargeEnseignement.fetchAllSco_TypeChargeEnseignements(editingContext, null);
  }

  public static NSArray<EOTypeChargeEnseignement> fetchAllSco_TypeChargeEnseignements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeChargeEnseignement.fetchSco_TypeChargeEnseignements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeChargeEnseignement> fetchSco_TypeChargeEnseignements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeChargeEnseignement> fetchSpec = new ERXFetchSpecification<EOTypeChargeEnseignement>(_EOTypeChargeEnseignement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeChargeEnseignement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeChargeEnseignement fetchSco_TypeChargeEnseignement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeChargeEnseignement.fetchSco_TypeChargeEnseignement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeChargeEnseignement fetchSco_TypeChargeEnseignement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeChargeEnseignement> eoObjects = _EOTypeChargeEnseignement.fetchSco_TypeChargeEnseignements(editingContext, qualifier, null);
    EOTypeChargeEnseignement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeChargeEnseignement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeChargeEnseignement fetchRequiredSco_TypeChargeEnseignement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeChargeEnseignement.fetchRequiredSco_TypeChargeEnseignement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeChargeEnseignement fetchRequiredSco_TypeChargeEnseignement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeChargeEnseignement eoObject = _EOTypeChargeEnseignement.fetchSco_TypeChargeEnseignement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeChargeEnseignement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeChargeEnseignement localInstanceIn(EOEditingContext editingContext, EOTypeChargeEnseignement eo) {
    EOTypeChargeEnseignement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
