// DO NOT EDIT.  Make changes to EOInscriptionPedagogique.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOInscriptionPedagogique extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_InscriptionPedagogique";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> PRINCIPALE = new ERXKey<Boolean>("principale");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemande");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription> TO_INSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription>("toInscription");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement>("toInscriptionPedagogiqueElements");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation> TO_LIEN_CONSULTATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation>("toLienConsultation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant> TO_TYPE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant>("toTypeEtudiant");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String PRINCIPALE_KEY = PRINCIPALE.key();
  // Relationships
  public static final String TO_DEMANDE_KEY = TO_DEMANDE.key();
  public static final String TO_INSCRIPTION_KEY = TO_INSCRIPTION.key();
  public static final String TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY = TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS.key();
  public static final String TO_LIEN_CONSULTATION_KEY = TO_LIEN_CONSULTATION.key();
  public static final String TO_TYPE_ETUDIANT_KEY = TO_TYPE_ETUDIANT.key();

  private static Logger LOG = Logger.getLogger(_EOInscriptionPedagogique.class);

  public EOInscriptionPedagogique localInstanceIn(EOEditingContext editingContext) {
    EOInscriptionPedagogique localInstance = (EOInscriptionPedagogique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOInscriptionPedagogique.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogique.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogique.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOInscriptionPedagogique.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogique.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogique.D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOInscriptionPedagogique.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogique.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogique.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOInscriptionPedagogique.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogique.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogique.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean principale() {
    return (Boolean) storedValueForKey(_EOInscriptionPedagogique.PRINCIPALE_KEY);
  }

  public void setPrincipale(Boolean value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogique.LOG.debug( "updating principale from " + principale() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogique.PRINCIPALE_KEY);
  }

  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(_EOInscriptionPedagogique.TO_DEMANDE_KEY);
  }
  
  public void setToDemande(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogique.TO_DEMANDE_KEY);
  }

  public void setToDemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("updating toDemande from " + toDemande() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDemande(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = toDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogique.TO_DEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogique.TO_DEMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription toInscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription)storedValueForKey(_EOInscriptionPedagogique.TO_INSCRIPTION_KEY);
  }
  
  public void setToInscription(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogique.TO_INSCRIPTION_KEY);
  }

  public void setToInscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("updating toInscription from " + toInscription() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToInscription(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription oldValue = toInscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogique.TO_INSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogique.TO_INSCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation toLienConsultation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation)storedValueForKey(_EOInscriptionPedagogique.TO_LIEN_CONSULTATION_KEY);
  }
  
  public void setToLienConsultation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogique.TO_LIEN_CONSULTATION_KEY);
  }

  public void setToLienConsultationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("updating toLienConsultation from " + toLienConsultation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienConsultation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation oldValue = toLienConsultation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogique.TO_LIEN_CONSULTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogique.TO_LIEN_CONSULTATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant toTypeEtudiant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant)storedValueForKey(_EOInscriptionPedagogique.TO_TYPE_ETUDIANT_KEY);
  }
  
  public void setToTypeEtudiant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogique.TO_TYPE_ETUDIANT_KEY);
  }

  public void setToTypeEtudiantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant value) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("updating toTypeEtudiant from " + toTypeEtudiant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeEtudiant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant oldValue = toTypeEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogique.TO_TYPE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogique.TO_TYPE_ETUDIANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> toInscriptionPedagogiqueElements() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement>)storedValueForKey(_EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> toInscriptionPedagogiqueElements(EOQualifier qualifier) {
    return toInscriptionPedagogiqueElements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> toInscriptionPedagogiqueElements(EOQualifier qualifier, boolean fetch) {
    return toInscriptionPedagogiqueElements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> toInscriptionPedagogiqueElements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toInscriptionPedagogiqueElements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToInscriptionPedagogiqueElements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement object) {
    includeObjectIntoPropertyWithKey(object, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
  }

  public void removeFromToInscriptionPedagogiqueElements(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement object) {
    excludeObjectFromPropertyWithKey(object, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
  }

  public void addToToInscriptionPedagogiqueElementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement object) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("adding " + object + " to toInscriptionPedagogiqueElements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToInscriptionPedagogiqueElements(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
    }
  }

  public void removeFromToInscriptionPedagogiqueElementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement object) {
    if (_EOInscriptionPedagogique.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogique.LOG.debug("removing " + object + " from toInscriptionPedagogiqueElements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToInscriptionPedagogiqueElements(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement createToInscriptionPedagogiqueElementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement) eo;
  }

  public void deleteToInscriptionPedagogiqueElementsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogique.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToInscriptionPedagogiqueElementsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement> objects = toInscriptionPedagogiqueElements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToInscriptionPedagogiqueElementsRelationship(objects.nextElement());
    }
  }


  public static EOInscriptionPedagogique createSco_InscriptionPedagogique(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persIdCreation
, Integer persIdModification
, Boolean principale
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription toInscription, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant toTypeEtudiant) {
    EOInscriptionPedagogique eo = (EOInscriptionPedagogique) EOUtilities.createAndInsertInstance(editingContext, _EOInscriptionPedagogique.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setPrincipale(principale);
    eo.setToInscriptionRelationship(toInscription);
    eo.setToTypeEtudiantRelationship(toTypeEtudiant);
    return eo;
  }

  public static ERXFetchSpecification<EOInscriptionPedagogique> fetchSpec() {
    return new ERXFetchSpecification<EOInscriptionPedagogique>(_EOInscriptionPedagogique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOInscriptionPedagogique> fetchAllSco_InscriptionPedagogiques(EOEditingContext editingContext) {
    return _EOInscriptionPedagogique.fetchAllSco_InscriptionPedagogiques(editingContext, null);
  }

  public static NSArray<EOInscriptionPedagogique> fetchAllSco_InscriptionPedagogiques(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOInscriptionPedagogique.fetchSco_InscriptionPedagogiques(editingContext, null, sortOrderings);
  }

  public static NSArray<EOInscriptionPedagogique> fetchSco_InscriptionPedagogiques(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOInscriptionPedagogique> fetchSpec = new ERXFetchSpecification<EOInscriptionPedagogique>(_EOInscriptionPedagogique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOInscriptionPedagogique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOInscriptionPedagogique fetchSco_InscriptionPedagogique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscriptionPedagogique.fetchSco_InscriptionPedagogique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscriptionPedagogique fetchSco_InscriptionPedagogique(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOInscriptionPedagogique> eoObjects = _EOInscriptionPedagogique.fetchSco_InscriptionPedagogiques(editingContext, qualifier, null);
    EOInscriptionPedagogique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_InscriptionPedagogique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscriptionPedagogique fetchRequiredSco_InscriptionPedagogique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscriptionPedagogique.fetchRequiredSco_InscriptionPedagogique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscriptionPedagogique fetchRequiredSco_InscriptionPedagogique(EOEditingContext editingContext, EOQualifier qualifier) {
    EOInscriptionPedagogique eoObject = _EOInscriptionPedagogique.fetchSco_InscriptionPedagogique(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_InscriptionPedagogique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscriptionPedagogique localInstanceIn(EOEditingContext editingContext, EOInscriptionPedagogique eo) {
    EOInscriptionPedagogique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
