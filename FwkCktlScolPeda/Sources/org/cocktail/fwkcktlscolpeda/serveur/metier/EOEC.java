package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * La classe EOEC décrit et maintient un EC
 */
@SuppressWarnings("serial")
public class EOEC extends _EOEC implements IEC {

	// Properties
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeEC(editingContext());
	}

	// Public methods

	/**
	 * @return true si l'EC est libre
	 */
	public Boolean isLibre() {
		return (libre() == 1);
	}

	// Protected methods
	@Override
	public BigDecimal valeurEnHeures(Boolean dureeMax) {
		if ((creditable() != null) && (creditable().travailEtudiant() != null)) {
			return (new BigDecimal(creditable().travailEtudiant().toString()));
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public EOEC copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			// TODO : récupérer le persId par injection du user
			EOEC ec = EOComposantFactory.createComposant(EOEC.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(ec);
			return ec;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @return IEC : le composant suivant associe au composant si il existe
	 */
	public IEC getComposantAvecOrigine() {
		if (super.getComposantSuivant() != null) {
			return ((IEC) (super.getComposantSuivant()));
		}

		return null;
	}

	/**
	 * @return IEC : le composant d'origine
	 */
	public IEC getComposantOrigine() {
		return (IEC) composantOrigine();
	}

	private void assignTo(EOEC ec) {
		super.assignTo(ec);
		ec.setLibre(this.libre());
		ec.setToCnuRelationship(this.toCnu());
	}

	public EODiscipline getDiscipline() {
		if (disciplines().count() > 0) {
			return disciplines().get(0);
		}
		return null;
	}

}