package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;

/**
 * Cette interface représente un moyen de paiement pour un paiement.
 */
public interface IScoPaiementMoyen extends IPaiementMoyen {

	/**
	 * @return paiement
	 */
	IScoPaiement toScoPaiement();

//	/**
//	 * @param value paiement
//	 */
//	void setToPaiementRelationship(IScoPaiement value);
//	
//	/**
//	 * @return la liste des échéances du paiement
//	 */
//	List<? extends IScoPaiementEcheance> toPaiementEcheances();
	
//	/**
//	 * @return la liste des échéances du paiement triée avec le tri par défaut (par date d'échéances)
//	 */
//	List<? extends IScoPaiementEcheance> toPaiementEcheancesTriees();
	
	/**
	 * @return L'échéancier SEPA SDD si ce paiement est différé
	 */
	ISepaSddEcheancier toEcheancierSepaSdd();
	
    /**
     * @param echeancier l'échéancier
     */
    void setToEcheancierRelationship(ISepaSddEcheancier echeancier);
    
    /**
     * @param adresse une adresse pour le titulaire du compte
     */
    void setToAdresseTitulaireRelationship(IAdresse adresse);
    
    /**
     * @return l'id du fournisseur existant
     */
    IFournis toFournisseurTitulaire();
    
    /**
     * @param fournis le fournisseur
     */
    void setToFournisseurTitulaireRelationship(IFournis fournis);
    
    /**
     * @return le rib du titulaire
     */
    IRib toRibTitulaire();
    
    /**
     * @param rib le rib
     */
    void setToRibTitulaireRelationship(IRib rib);
    
}
