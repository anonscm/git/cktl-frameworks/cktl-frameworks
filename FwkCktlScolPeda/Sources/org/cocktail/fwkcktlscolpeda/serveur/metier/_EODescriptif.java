// DO NOT EDIT.  Make changes to EODescriptif.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODescriptif extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Descriptif";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> ANNEE = new ERXKey<NSTimestamp>("annee");
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<Integer> DESTINATION_ID = new ERXKey<Integer>("destinationId");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> VERSION = new ERXKey<String>("version");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> DESCRIPTIF_CHAMP_CONTENUS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu>("descriptifChampContenus");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination> DESTINATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination>("destination");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liens");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> LIENS_UTILES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles>("liensUtiles");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String DESTINATION_ID_KEY = DESTINATION_ID.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String VERSION_KEY = VERSION.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String DESCRIPTIF_CHAMP_CONTENUS_KEY = DESCRIPTIF_CHAMP_CONTENUS.key();
  public static final String DESTINATION_KEY = DESTINATION.key();
  public static final String LIENS_KEY = LIENS.key();
  public static final String LIENS_UTILES_KEY = LIENS_UTILES.key();

  private static Logger LOG = Logger.getLogger(_EODescriptif.class);

  public EODescriptif localInstanceIn(EOEditingContext editingContext) {
    EODescriptif localInstance = (EODescriptif)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp annee() {
    return (NSTimestamp) storedValueForKey(_EODescriptif.ANNEE_KEY);
  }

  public void setAnnee(NSTimestamp value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.ANNEE_KEY);
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EODescriptif.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.COMPOSANT_ID_KEY);
  }

  public Integer destinationId() {
    return (Integer) storedValueForKey(_EODescriptif.DESTINATION_ID_KEY);
  }

  public void setDestinationId(Integer value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating destinationId from " + destinationId() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.DESTINATION_ID_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODescriptif.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODescriptif.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.LIBELLE_KEY);
  }

  public String version() {
    return (String) storedValueForKey(_EODescriptif.VERSION_KEY);
  }

  public void setVersion(String value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
    	_EODescriptif.LOG.debug( "updating version from " + version() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptif.VERSION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EODescriptif.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EODescriptif.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptif.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptif.COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination destination() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination)storedValueForKey(_EODescriptif.DESTINATION_KEY);
  }
  
  public void setDestination(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination value) {
    takeStoredValueForKey(value, _EODescriptif.DESTINATION_KEY);
  }

  public void setDestinationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination value) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("updating destination from " + destination() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setDestination(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination oldValue = destination();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODescriptif.DESTINATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODescriptif.DESTINATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> descriptifChampContenus() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu>)storedValueForKey(_EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> descriptifChampContenus(EOQualifier qualifier) {
    return descriptifChampContenus(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> descriptifChampContenus(EOQualifier qualifier, boolean fetch) {
    return descriptifChampContenus(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> descriptifChampContenus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu.DESCRIPTIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu.fetchSco_DescriptifChampContenus(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = descriptifChampContenus();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDescriptifChampContenus(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu object) {
    includeObjectIntoPropertyWithKey(object, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
  }

  public void removeFromDescriptifChampContenus(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu object) {
    excludeObjectFromPropertyWithKey(object, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
  }

  public void addToDescriptifChampContenusRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("adding " + object + " to descriptifChampContenus relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDescriptifChampContenus(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
    }
  }

  public void removeFromDescriptifChampContenusRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("removing " + object + " from descriptifChampContenus relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDescriptifChampContenus(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu createDescriptifChampContenusRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu) eo;
  }

  public void deleteDescriptifChampContenusRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.DESCRIPTIF_CHAMP_CONTENUS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDescriptifChampContenusRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifChampContenu> objects = descriptifChampContenus().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDescriptifChampContenusRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liens() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)storedValueForKey(_EODescriptif.LIENS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liens(EOQualifier qualifier) {
    return liens(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liens(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> results;
      results = liens();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToLiens(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    includeObjectIntoPropertyWithKey(object, _EODescriptif.LIENS_KEY);
  }

  public void removeFromLiens(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    excludeObjectFromPropertyWithKey(object, _EODescriptif.LIENS_KEY);
  }

  public void addToLiensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("adding " + object + " to liens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToLiens(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_KEY);
    }
  }

  public void removeFromLiensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("removing " + object + " from liens relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromLiens(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien createLiensRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODescriptif.LIENS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien) eo;
  }

  public void deleteLiensRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLiensRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> objects = liens().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLiensRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> liensUtiles() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles>)storedValueForKey(_EODescriptif.LIENS_UTILES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> liensUtiles(EOQualifier qualifier) {
    return liensUtiles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> liensUtiles(EOQualifier qualifier, boolean fetch) {
    return liensUtiles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> liensUtiles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles.DESCRIPTIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles.fetchSco_DescriptifLiensUtileses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = liensUtiles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToLiensUtiles(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles object) {
    includeObjectIntoPropertyWithKey(object, _EODescriptif.LIENS_UTILES_KEY);
  }

  public void removeFromLiensUtiles(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles object) {
    excludeObjectFromPropertyWithKey(object, _EODescriptif.LIENS_UTILES_KEY);
  }

  public void addToLiensUtilesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("adding " + object + " to liensUtiles relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToLiensUtiles(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_UTILES_KEY);
    }
  }

  public void removeFromLiensUtilesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles object) {
    if (_EODescriptif.LOG.isDebugEnabled()) {
      _EODescriptif.LOG.debug("removing " + object + " from liensUtiles relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromLiensUtiles(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_UTILES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles createLiensUtilesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODescriptif.LIENS_UTILES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles) eo;
  }

  public void deleteLiensUtilesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODescriptif.LIENS_UTILES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLiensUtilesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptifLiensUtiles> objects = liensUtiles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLiensUtilesRelationship(objects.nextElement());
    }
  }


  public static EODescriptif createSco_Descriptif(EOEditingContext editingContext, Integer composantId
, Integer destinationId
, Integer id
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeDescriptifDestination destination) {
    EODescriptif eo = (EODescriptif) EOUtilities.createAndInsertInstance(editingContext, _EODescriptif.ENTITY_NAME);    
		eo.setComposantId(composantId);
		eo.setDestinationId(destinationId);
		eo.setId(id);
    eo.setComposantRelationship(composant);
    eo.setDestinationRelationship(destination);
    return eo;
  }

  public static ERXFetchSpecification<EODescriptif> fetchSpec() {
    return new ERXFetchSpecification<EODescriptif>(_EODescriptif.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODescriptif> fetchAllSco_Descriptifs(EOEditingContext editingContext) {
    return _EODescriptif.fetchAllSco_Descriptifs(editingContext, null);
  }

  public static NSArray<EODescriptif> fetchAllSco_Descriptifs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODescriptif.fetchSco_Descriptifs(editingContext, null, sortOrderings);
  }

  public static NSArray<EODescriptif> fetchSco_Descriptifs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODescriptif> fetchSpec = new ERXFetchSpecification<EODescriptif>(_EODescriptif.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODescriptif> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODescriptif fetchSco_Descriptif(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptif.fetchSco_Descriptif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptif fetchSco_Descriptif(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODescriptif> eoObjects = _EODescriptif.fetchSco_Descriptifs(editingContext, qualifier, null);
    EODescriptif eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Descriptif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptif fetchRequiredSco_Descriptif(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptif.fetchRequiredSco_Descriptif(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptif fetchRequiredSco_Descriptif(EOEditingContext editingContext, EOQualifier qualifier) {
    EODescriptif eoObject = _EODescriptif.fetchSco_Descriptif(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Descriptif that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptif localInstanceIn(EOEditingContext editingContext, EODescriptif eo) {
    EODescriptif localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
