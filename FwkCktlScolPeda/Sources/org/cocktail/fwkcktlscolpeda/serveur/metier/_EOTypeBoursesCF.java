// DO NOT EDIT.  Make changes to EOTypeBoursesCF.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeBoursesCF extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeBoursesCF";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeBoursesCF.class);

  public EOTypeBoursesCF localInstanceIn(EOEditingContext editingContext) {
    EOTypeBoursesCF localInstance = (EOTypeBoursesCF)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeBoursesCF.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeBoursesCF.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOTypeBoursesCF.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeBoursesCF.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOTypeBoursesCF.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOTypeBoursesCF.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOTypeBoursesCF.LOG.isDebugEnabled()) {
    	_EOTypeBoursesCF.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBoursesCF.PERS_ID_MODIFCATION_KEY);
  }


  public static EOTypeBoursesCF createSco_TypeBoursesCF(EOEditingContext editingContext, String code
, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
) {
    EOTypeBoursesCF eo = (EOTypeBoursesCF) EOUtilities.createAndInsertInstance(editingContext, _EOTypeBoursesCF.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeBoursesCF> fetchSpec() {
    return new ERXFetchSpecification<EOTypeBoursesCF>(_EOTypeBoursesCF.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeBoursesCF> fetchAllSco_TypeBoursesCFs(EOEditingContext editingContext) {
    return _EOTypeBoursesCF.fetchAllSco_TypeBoursesCFs(editingContext, null);
  }

  public static NSArray<EOTypeBoursesCF> fetchAllSco_TypeBoursesCFs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeBoursesCF.fetchSco_TypeBoursesCFs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeBoursesCF> fetchSco_TypeBoursesCFs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeBoursesCF> fetchSpec = new ERXFetchSpecification<EOTypeBoursesCF>(_EOTypeBoursesCF.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeBoursesCF> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeBoursesCF fetchSco_TypeBoursesCF(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeBoursesCF.fetchSco_TypeBoursesCF(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeBoursesCF fetchSco_TypeBoursesCF(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeBoursesCF> eoObjects = _EOTypeBoursesCF.fetchSco_TypeBoursesCFs(editingContext, qualifier, null);
    EOTypeBoursesCF eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeBoursesCF that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeBoursesCF fetchRequiredSco_TypeBoursesCF(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeBoursesCF.fetchRequiredSco_TypeBoursesCF(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeBoursesCF fetchRequiredSco_TypeBoursesCF(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeBoursesCF eoObject = _EOTypeBoursesCF.fetchSco_TypeBoursesCF(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeBoursesCF that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeBoursesCF localInstanceIn(EOEditingContext editingContext, EOTypeBoursesCF eo) {
    EOTypeBoursesCF localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
