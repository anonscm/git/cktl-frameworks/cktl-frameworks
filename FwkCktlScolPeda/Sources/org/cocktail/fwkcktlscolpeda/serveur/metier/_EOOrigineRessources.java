// DO NOT EDIT.  Make changes to EOOrigineRessources.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOOrigineRessources extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_OrigineRessources";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOOrigineRessources.class);

  public EOOrigineRessources localInstanceIn(EOEditingContext editingContext) {
    EOOrigineRessources localInstance = (EOOrigineRessources)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOOrigineRessources.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOOrigineRessources.LOG.isDebugEnabled()) {
    	_EOOrigineRessources.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOOrigineRessources.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOOrigineRessources.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOOrigineRessources.LOG.isDebugEnabled()) {
    	_EOOrigineRessources.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOOrigineRessources.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOOrigineRessources.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOOrigineRessources.LOG.isDebugEnabled()) {
    	_EOOrigineRessources.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOOrigineRessources.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOOrigineRessources.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOOrigineRessources.LOG.isDebugEnabled()) {
    	_EOOrigineRessources.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOOrigineRessources.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOOrigineRessources.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOOrigineRessources.LOG.isDebugEnabled()) {
    	_EOOrigineRessources.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOOrigineRessources.PERS_ID_MODIFICATION_KEY);
  }


  public static EOOrigineRessources createSco_OrigineRessources(EOEditingContext editingContext, NSTimestamp dCreation
, String libelle
, Integer persIdCreation
) {
    EOOrigineRessources eo = (EOOrigineRessources) EOUtilities.createAndInsertInstance(editingContext, _EOOrigineRessources.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOOrigineRessources> fetchSpec() {
    return new ERXFetchSpecification<EOOrigineRessources>(_EOOrigineRessources.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOOrigineRessources> fetchAllSco_OrigineRessourceses(EOEditingContext editingContext) {
    return _EOOrigineRessources.fetchAllSco_OrigineRessourceses(editingContext, null);
  }

  public static NSArray<EOOrigineRessources> fetchAllSco_OrigineRessourceses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOOrigineRessources.fetchSco_OrigineRessourceses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOOrigineRessources> fetchSco_OrigineRessourceses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOOrigineRessources> fetchSpec = new ERXFetchSpecification<EOOrigineRessources>(_EOOrigineRessources.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOOrigineRessources> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOOrigineRessources fetchSco_OrigineRessources(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrigineRessources.fetchSco_OrigineRessources(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrigineRessources fetchSco_OrigineRessources(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOOrigineRessources> eoObjects = _EOOrigineRessources.fetchSco_OrigineRessourceses(editingContext, qualifier, null);
    EOOrigineRessources eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_OrigineRessources that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrigineRessources fetchRequiredSco_OrigineRessources(EOEditingContext editingContext, String keyName, Object value) {
    return _EOOrigineRessources.fetchRequiredSco_OrigineRessources(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOOrigineRessources fetchRequiredSco_OrigineRessources(EOEditingContext editingContext, EOQualifier qualifier) {
    EOOrigineRessources eoObject = _EOOrigineRessources.fetchSco_OrigineRessources(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_OrigineRessources that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOOrigineRessources localInstanceIn(EOEditingContext editingContext, EOOrigineRessources eo) {
    EOOrigineRessources localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
