package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * 
 * @author isabelle
 * Description d'un jury
 */
public class EOJury extends _EOJury implements IJury {
  private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOJury.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDCreation(Date value) {
		super.setDCreation((NSTimestamp) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDModification(Date value) {
		super.setDModification((NSTimestamp) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToJuryEtatRelationship(IJuryEtat value) {
		super.setToJuryEtatRelationship((EOJuryEtat) value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToLienFormationRelationship(ILien value) {
		super.setToLienFormationRelationship((EOLien) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToLienNiveauAnneeRelationship(ILien value) {
		super.setToLienNiveauAnneeRelationship((EOLien) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToLienNiveauSemestreRelationship(ILien value) {
		super.setToLienNiveauSemestreRelationship((EOLien) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToLienParcoursAnneeRelationship(ILien value) {
		super.setToLienParcoursAnneeRelationship((EOLien) value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToLienParcoursFormationRelationship(ILien value) {
		super.setToLienParcoursFormationRelationship((EOLien) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void addToToJuryConvocationsRelationship(IJuryConvocation object) {
		super.addToToJuryConvocationsRelationship((EOJuryConvocation) object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void addToToJuryPersonnesesRelationship(IJuryPersonnes object) {
		super.addToToJuryPersonnesesRelationship((EOJuryPersonnes) object);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToSessionPeriodeRelationship(ISessionPeriode value) {
		super.setToSessionPeriodeRelationship((EOSessionPeriode) value);
		
	}
	
	/**
     * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException 
     */
    public void validateForInsert() throws NSValidation.ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
    @Override
    public void validateForUpdate() throws ValidationException {
    	if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
    	super.validateForUpdate();
    }

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId utilisateur qui met à jour l'enregistrement 
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}


	
	
}
