package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Type d'atome pédagogique (Cours magistral, TP, TD, etc..)
 */
public interface ITypeAP {
	/**
	 * @return libelle
	 */
	String libelle();

	/**
	 * @return code
	 */
	String code();
}
