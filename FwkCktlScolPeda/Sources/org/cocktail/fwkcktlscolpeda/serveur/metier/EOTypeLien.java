package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;


/**
 * Représente un type lien
 */
public class EOTypeLien extends _EOTypeLien implements ITypeLien {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static ERXEnterpriseObjectCache<EOTypeLien> typeLienCache;

	// Public fields
	public static final String TYPECOMPOSER_LBL = "composer";
	public static final String TYPEVERSIONNER_LBL = "versionner";
	public static final String TYPELIERDIPLOMEINTERMEDIAIRE_LBL = "lierDiplomeIntermediaire";	

	// Properties
    /**
     * @return le cache des {@link EOTypeLien} par leur nom
     */
    public static ERXEnterpriseObjectCache<EOTypeLien> getTypeLienCache() {
    	if (typeLienCache == null) {
    		typeLienCache = new ERXEnterpriseObjectCache<EOTypeLien>(EOTypeLien.class, NOM_KEY);
    	}
    	return typeLienCache;
    }
    
    /**
     * @param typeLienCache le cache a setter
     */
    public static void setTypeLienCache(ERXEnterpriseObjectCache<EOTypeLien> typeLienCache) {
    	EOTypeLien.typeLienCache = typeLienCache;
    }
    
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeLien} "composer"
	 */
	public static EOTypeLien typeComposer(EOEditingContext edc) {
		return getTypeLienCache().objectForKey(edc, TYPECOMPOSER_LBL);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeLien} "versionner"
	 */
	public static EOTypeLien typeVersionner(EOEditingContext edc) {
		return getTypeLienCache().objectForKey(edc, TYPEVERSIONNER_LBL);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeLien} "lierDiplomeIntermediaire"
	 */
	public static EOTypeLien typeLierDiplomeIntermediaire(EOEditingContext edc) {
		return getTypeLienCache().objectForKey(edc, TYPELIERDIPLOMEINTERMEDIAIRE_LBL);
	}
	
	// Public Methods	
	@Override
	public String toString() {
		return this.nom();
	}
	
	// Protected Methods
    // Private methods
	
}
