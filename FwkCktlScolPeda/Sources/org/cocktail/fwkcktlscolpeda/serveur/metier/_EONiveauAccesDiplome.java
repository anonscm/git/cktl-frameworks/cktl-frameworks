// DO NOT EDIT.  Make changes to EONiveauAccesDiplome.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONiveauAccesDiplome extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_NiveauAccesDiplome";

  // Attribute Keys
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  // Relationship Keys

  // Attributes
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONiveauAccesDiplome.class);

  public EONiveauAccesDiplome localInstanceIn(EOEditingContext editingContext) {
    EONiveauAccesDiplome localInstance = (EONiveauAccesDiplome)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String libelle() {
    return (String) storedValueForKey(_EONiveauAccesDiplome.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EONiveauAccesDiplome.LOG.isDebugEnabled()) {
    	_EONiveauAccesDiplome.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauAccesDiplome.LIBELLE_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EONiveauAccesDiplome.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EONiveauAccesDiplome.LOG.isDebugEnabled()) {
    	_EONiveauAccesDiplome.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EONiveauAccesDiplome.NIVEAU_KEY);
  }


  public static EONiveauAccesDiplome createSco_NiveauAccesDiplome(EOEditingContext editingContext, String libelle
, Integer niveau
) {
    EONiveauAccesDiplome eo = (EONiveauAccesDiplome) EOUtilities.createAndInsertInstance(editingContext, _EONiveauAccesDiplome.ENTITY_NAME);    
		eo.setLibelle(libelle);
		eo.setNiveau(niveau);
    return eo;
  }

  public static ERXFetchSpecification<EONiveauAccesDiplome> fetchSpec() {
    return new ERXFetchSpecification<EONiveauAccesDiplome>(_EONiveauAccesDiplome.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONiveauAccesDiplome> fetchAllSco_NiveauAccesDiplomes(EOEditingContext editingContext) {
    return _EONiveauAccesDiplome.fetchAllSco_NiveauAccesDiplomes(editingContext, null);
  }

  public static NSArray<EONiveauAccesDiplome> fetchAllSco_NiveauAccesDiplomes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONiveauAccesDiplome.fetchSco_NiveauAccesDiplomes(editingContext, null, sortOrderings);
  }

  public static NSArray<EONiveauAccesDiplome> fetchSco_NiveauAccesDiplomes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONiveauAccesDiplome> fetchSpec = new ERXFetchSpecification<EONiveauAccesDiplome>(_EONiveauAccesDiplome.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONiveauAccesDiplome> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONiveauAccesDiplome fetchSco_NiveauAccesDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EONiveauAccesDiplome.fetchSco_NiveauAccesDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONiveauAccesDiplome fetchSco_NiveauAccesDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONiveauAccesDiplome> eoObjects = _EONiveauAccesDiplome.fetchSco_NiveauAccesDiplomes(editingContext, qualifier, null);
    EONiveauAccesDiplome eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_NiveauAccesDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONiveauAccesDiplome fetchRequiredSco_NiveauAccesDiplome(EOEditingContext editingContext, String keyName, Object value) {
    return _EONiveauAccesDiplome.fetchRequiredSco_NiveauAccesDiplome(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONiveauAccesDiplome fetchRequiredSco_NiveauAccesDiplome(EOEditingContext editingContext, EOQualifier qualifier) {
    EONiveauAccesDiplome eoObject = _EONiveauAccesDiplome.fetchSco_NiveauAccesDiplome(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_NiveauAccesDiplome that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONiveauAccesDiplome localInstanceIn(EOEditingContext editingContext, EONiveauAccesDiplome eo) {
    EONiveauAccesDiplome localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
