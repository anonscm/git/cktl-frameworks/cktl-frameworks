package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.util.Date;

import org.cocktail.fwkcktlcompta.server.scolarite.IEcheanceLight;

import com.webobjects.foundation.NSTimestamp;

/**
 * Super interface pour une échéance de paiement qu'il soit SCO_SCOLARITE ou SCO_PRE_SCOLARITE.
 * 
 * @author Pascal MACOUIN
 */
public interface IPaiementEcheance extends IEcheanceLight {

	/**
	 * @return Le libellé de l'échéance
	 */
	String libelle();

	/**
	 * Affecte le libellé de l'échéance
	 * @param unLibelle un libellé pour cette échéance
	 */
	void setLibelle(String unLibelle);

	/**
	 * Affecte la date d'échéance.
	 * @param uneDate une date d'échéance
	 */
	void setDateEcheance(Date uneDate);

	/**
	 * Affecte le montant de l'échéance.
	 * @param unMontant un montant pour cette échéance
	 */
	void setMontant(BigDecimal unMontant);
	
	/**
	 * @return le moyen de paiement associé à cette échéance
	 */
	IPaiementMoyen toPaiementMoyen();

	/**
	 * Affecte le moyen de paiement associé à cette échéance.
	 * @param value un moyen de paiement
	 */
	void setToPaiementMoyenRelationship(IPaiementMoyen value);

	/**
	 * Le moyen de paiement ayant servi à payer cette échéance.
	 * <p>
	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
	 * a été payé avec un chèque.
	 * @return Le moyen de paiement ayant servi à payer cette échéance
	 */
	IPaiementMoyen toAutrePaiementMoyen();

	/**
	 * Affecte le moyen de paiement ayant servi à payer cette échéance.
	 * <p>
	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
	 * a été payé avec un chèque.
	 * 
	 * @param unAutreMoyenDePaiement un autre moyen le paiement
	 */
	void setToAutrePaiementMoyenRelationship(IPaiementMoyen unAutreMoyenDePaiement);
	
	// ***************
	// Données d'audit
	// ***************
	/**
	 * @return date de creation de l'enregistrement
	 */
	NSTimestamp dCreation();

	/**
	 * @param value date de creation de l'enregistrement
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return date de modification du paiement
	 */
	NSTimestamp dModification();

	/**
	 * @param value date de modification du paiement
	 */
	void setDModification(NSTimestamp value);

	/**
	 * @return personne qui a créé l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return personne qui a modifié l'enregistrement
	 */
	Integer persIdModification();

	/**
	 * @param value personne qui a créé l'enregistrement
	 */
	void setPersIdModification(Integer value);
}
