package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe gère les règles de liaison entre composants.
 */
public class EORegleLiaison extends _EORegleLiaison {
	// Private fields

	private static final long serialVersionUID = 1L;

	// Properties

	public Boolean isForbidden() {
		return forbidden() == 1;
	}

	/**
	 * @param isForbidden vrai si la regle de liaison est interdite
	 */
	public void setIsForbidden(Boolean isForbidden) {
		if (isForbidden) {
			setForbidden(1);
		} else {
			setForbidden(0);
		}
	}

	public Boolean isVisible() {
		return visible() == 1;
	}

	/**
	 * @param isVisible vrai si la regle de liaison est visible
	 */
	public void setIsVisible(Boolean isVisible) {
		if (isVisible) {
			setVisible(1);
		} else {
			setVisible(0);
		}
	}

	public Boolean isAllowed() {
		return allowed() == 1;
	}

	/**
	 * @param isAllowed vrai si la regle de liaison est autorisee
	 */
	public void setIsAllowed(Boolean isAllowed) {
		if (isAllowed) {
			setAllowed(1);
		} else {
			setAllowed(0);
		}
	}

	// Public Methods
	/**
	 * une règle de liaison ne peut pas ne plus être autorisée si elle est déjà utilisée une règle de liaison ne peut pas être interdite si elle est autorisée.
	 * @throws ValidationException exception lancee si la regle de liaison n'est pas validee
	 */
	public void validateForSave() throws ValidationException {
		if ((allowed() == 0) && isEnUtilisation()) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "SCO_RegleLiaison.LienExisteDeja");
		}

		if (forbidden() == 1 && (isAllowed())) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "SCO_RegleLiaison.AssociationDejaAutorisee");
		}

		super.validateForSave();
	}

	// Protected Methods
	// Private methods
	private boolean isEnUtilisation() {

		return EOLien.fetchSco_Liens(
		    editingContext(),
		    EOLien.PARENT.dot(EOComposant.TYPE_COMPOSANT).eq(this.allowedParent()).and(EOLien.CHILD.dot(EOComposant.TYPE_COMPOSANT).eq(this.allowedChild()))
		        .and(EOLien.TYPE_LIEN.eq(this.typeLien())), null).count() > 0;
	}

	/**
	 * @param edc contexte d'edition
	 * @param typeParent type du composant parent
	 * @param typeChild type du composant enfnat
	 * @param typeLien type du lien
	 * @return true si la regle de liaison est valide
	 */
	public static boolean isValid(EOEditingContext edc, EOTypeComposant typeParent, EOTypeComposant typeChild, EOTypeLien typeLien) {
		EOQualifier typeLienQualifier = ERXQ.equals(EORegleLiaison.TYPE_LIEN_KEY, typeLien);
		EOQualifier allowedParentQualifier = ERXQ.equals(EORegleLiaison.ALLOWED_PARENT_KEY, typeParent);
		EOQualifier allowedChildQualifier = ERXQ.equals(EORegleLiaison.ALLOWED_CHILD_KEY, typeChild);
		EOQualifier forbiddenQualifier = ERXQ.equals(EORegleLiaison.FORBIDDEN_KEY, 0);
		EOQualifier allowedQualifier = ERXQ.equals(EORegleLiaison.ALLOWED_KEY, 1);
		EOQualifier visibleQualifier = ERXQ.equals(EORegleLiaison.VISIBLE_KEY, 1);
		EOQualifier rechercheQualifier = ERXQ.and(typeLienQualifier, allowedParentQualifier, allowedChildQualifier, forbiddenQualifier, allowedQualifier,
		    visibleQualifier);

		return EORegleLiaison.fetchSco_RegleLiaison(edc, rechercheQualifier) != null;
	}

}
