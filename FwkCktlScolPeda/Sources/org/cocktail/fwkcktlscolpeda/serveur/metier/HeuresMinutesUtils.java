package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Classe utilitaire pour la gestion des heures / minutes.
 * <p>
 * Devrait toujours resté cantonnée à ce package.
 */
final class HeuresMinutesUtils {

	/** Le nombre de minutes dans une heure. */
	public static final int NB_MINUTES_DANS_UNE_HEURE = 60;
	/** Le nombre de minutes dans une heure (version BigDecimal). */
	public static final BigDecimal NB_MINUTES_DANS_UNE_HEURE_BIGDEC = BigDecimal.valueOf(HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE);

	/**
	 * Constructeur privé pour les classes utilitaires.
	 */
	private HeuresMinutesUtils() {}
	
	/**
	 * Convertis les minutes au format stockage base de donnée (Bdd) en minutes gérées par l'application.
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param minuteBdd minutes au format stockage base de données
	 * @return minutes
	 */
	public static Integer minuteBddEnMinute(BigDecimal minuteBdd) {
		if (minuteBdd == null) {
			return null;
		}
		
		return minuteBdd.setScale(0, RoundingMode.HALF_UP).intValue();
	}

	/**
	 * Convertis les minutes gérées par l'aplication en minutes au format de stockage en base de données (Bdd).
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param minute minutes
	 * @return minutes au format stockage base de données
	 */
	public static BigDecimal minuteEnMinuteBdd(Integer minute) {
		if (minute == null) {
			return null;
		}
		
		return new BigDecimal(minute).setScale(1);
	}

	/**
	 * Convertis des centième d'heures (1.5 = 1h30) au format de stockage en base de données (Bdd)
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param centieme des centième d'heures
	 * @return minutes au format stockage base de données
	 */
	public static BigDecimal centiemeEnMinuteBdd(BigDecimal centieme) {
		if (centieme == null) {
			return null;
		}
		
		BigDecimal minuteBdd = centieme.multiply(HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE_BIGDEC).setScale(1, RoundingMode.HALF_UP);
		
		return minuteBdd;
	}

	/**
	 * Convertis les minutes au format stockage base de donnée (Bdd) en centième d'heure (1.5 = 1h30).
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param minuteBdd minutes au format stockage base de données
	 * @return des centième d'heures
	 */
	public static BigDecimal minuteBddEnCentieme(BigDecimal minuteBdd) {
		if (minuteBdd == null) {
			return null;
		}
		
		BigDecimal centieme = minuteBdd.divide(HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE_BIGDEC, 2, RoundingMode.HALF_UP);
		
		return centieme;
	}

	/**
	 * Convertis des heures/minutes (1.5 = 1.50 = 1h50) au format de stockage en base de données (Bdd)
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param heureMinute des heures/minutes
	 * @return minutes au format stockage base de données
	 */
	public static BigDecimal heureMinuteEnMinuteBdd(BigDecimal heureMinute) {
		if (heureMinute == null) {
			return null;
		}
		
		int heure = heureMinute.intValue();
		int minute = heureMinute.subtract(new BigDecimal(heure)).movePointRight(2).intValue();
	
		return new BigDecimal((heure * HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE) + minute).setScale(1);
	}
	
	/**
	 * Convertis les minutes au format stockage base de donnée (Bdd) en heures/minutes (1.5 = 1.50 = 1h50).
	 * <p>
	 * Remarque : Ne devrait pas être utilisée en dehors des classes EOXxxx.
	 * 
	 * @param minuteBdd minutes au format stockage base de données
	 * @return des heures/minutes
	 */
	public static BigDecimal minuteBddEnHeureMinute(BigDecimal minuteBdd) {
		if (minuteBdd == null) {
			return null;
		}
		
		int minute = minuteBddEnMinute(minuteBdd);
		int heure = minute / HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE;
		int minutes = minute % HeuresMinutesUtils.NB_MINUTES_DANS_UNE_HEURE;
		
		return new BigDecimal(minutes).movePointLeft(2).add(new BigDecimal(heure));
	}

}
