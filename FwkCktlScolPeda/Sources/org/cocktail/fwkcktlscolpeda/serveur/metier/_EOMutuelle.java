// DO NOT EDIT.  Make changes to EOMutuelle.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOMutuelle extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Mutuelle";

  // Attribute Keys
  public static final ERXKey<Integer> ADR_ORDRE = new ERXKey<Integer>("adrOrdre");
  public static final ERXKey<NSTimestamp> DCREATION = new ERXKey<NSTimestamp>("dcreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> MUT_CODE = new ERXKey<String>("mutCode");
  public static final ERXKey<String> MUT_TYPE = new ERXKey<String>("mutType");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String ADR_ORDRE_KEY = ADR_ORDRE.key();
  public static final String DCREATION_KEY = DCREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MUT_CODE_KEY = MUT_CODE.key();
  public static final String MUT_TYPE_KEY = MUT_TYPE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOMutuelle.class);

  public EOMutuelle localInstanceIn(EOEditingContext editingContext) {
    EOMutuelle localInstance = (EOMutuelle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer adrOrdre() {
    return (Integer) storedValueForKey(_EOMutuelle.ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Integer value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating adrOrdre from " + adrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.ADR_ORDRE_KEY);
  }

  public NSTimestamp dcreation() {
    return (NSTimestamp) storedValueForKey(_EOMutuelle.DCREATION_KEY);
  }

  public void setDcreation(NSTimestamp value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating dcreation from " + dcreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.DCREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOMutuelle.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOMutuelle.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.LIBELLE_KEY);
  }

  public String mutCode() {
    return (String) storedValueForKey(_EOMutuelle.MUT_CODE_KEY);
  }

  public void setMutCode(String value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating mutCode from " + mutCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.MUT_CODE_KEY);
  }

  public String mutType() {
    return (String) storedValueForKey(_EOMutuelle.MUT_TYPE_KEY);
  }

  public void setMutType(String value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating mutType from " + mutType() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.MUT_TYPE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOMutuelle.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOMutuelle.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOMutuelle.LOG.isDebugEnabled()) {
    	_EOMutuelle.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMutuelle.PERS_ID_MODIFICATION_KEY);
  }


  public static EOMutuelle createSco_Mutuelle(EOEditingContext editingContext, Integer adrOrdre
, NSTimestamp dcreation
, String mutCode
, String mutType
, Integer persIdCreation
) {
    EOMutuelle eo = (EOMutuelle) EOUtilities.createAndInsertInstance(editingContext, _EOMutuelle.ENTITY_NAME);    
		eo.setAdrOrdre(adrOrdre);
		eo.setDcreation(dcreation);
		eo.setMutCode(mutCode);
		eo.setMutType(mutType);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOMutuelle> fetchSpec() {
    return new ERXFetchSpecification<EOMutuelle>(_EOMutuelle.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOMutuelle> fetchAllSco_Mutuelles(EOEditingContext editingContext) {
    return _EOMutuelle.fetchAllSco_Mutuelles(editingContext, null);
  }

  public static NSArray<EOMutuelle> fetchAllSco_Mutuelles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMutuelle.fetchSco_Mutuelles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMutuelle> fetchSco_Mutuelles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOMutuelle> fetchSpec = new ERXFetchSpecification<EOMutuelle>(_EOMutuelle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMutuelle> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOMutuelle fetchSco_Mutuelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMutuelle.fetchSco_Mutuelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMutuelle fetchSco_Mutuelle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMutuelle> eoObjects = _EOMutuelle.fetchSco_Mutuelles(editingContext, qualifier, null);
    EOMutuelle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Mutuelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMutuelle fetchRequiredSco_Mutuelle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMutuelle.fetchRequiredSco_Mutuelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMutuelle fetchRequiredSco_Mutuelle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMutuelle eoObject = _EOMutuelle.fetchSco_Mutuelle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Mutuelle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMutuelle localInstanceIn(EOEditingContext editingContext, EOMutuelle eo) {
    EOMutuelle localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
