package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;


/**
 * Cette classe represente les bourses universitaires
 */
public class EOBourses extends _EOBourses implements IScoBourses {
  private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOBourses.class);

	public void setToEtudiantRelationship(IInfosEtudiant value) {
		super.setToEtudiantRelationship((EOEtudiant) (((ScoEtudiant) value).getEtudiant()));
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantRelationship(IEtudiant value) {
		super.setToEtudiantRelationship((EOEtudiant) value);
	}
	
	public void setToEchelonBourseRelationship(IEchelonBourse value) {
		super.setToEchelonBourseRelationship((EOEchelonBourse) value);
	}

	

	public void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation value) {
		super.setToTypeInscriptionFormationRelationship((EOTypeInscriptionFormation) value);
		
	}
	
	public void setToTypeBoursesCFRelationship(ITypeBoursesCF value) {
		super.setToTypeBoursesCFRelationship((EOTypeBoursesCF) value);
	}
}
