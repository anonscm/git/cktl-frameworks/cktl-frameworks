package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSTimestamp;

public interface IParametragePaieArticleComplementaire extends IParametragePaie {
	public Boolean boursier();
	
	public void setBoursier(Boolean value);

	public String composanteBudgetaire();

	public void setComposanteBudgetaire(String value);

	public String comptabilite();

	public void setComptabilite(String value);

	public NSTimestamp dCreation();

	public void setDCreation(NSTimestamp value);

	public NSTimestamp dModification();

	public void setDModification(NSTimestamp value);

	public String libelle();

	public void setLibelle(String value);

	public String libelleComplet();
	
	public BigDecimal montant();

	public void setMontant(BigDecimal value);

	public Integer persIdCreation();

	public void setPersIdCreation(Integer value);

	public Integer persIdModification();

	public void setPersIdModification(Integer value);

	public Boolean remboursement();

	public void setRemboursement(Boolean value);

	public ITypeArticleComplementaire toTypeArticleComplementaire();

	public void setToTypeArticleComplementaireRelationship(ITypeArticleComplementaire value);
	
	public Boolean applicationSystematique();

	public void setApplicationSystematique(Boolean value);
}