package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface ITypeArticleComplementaire {
	
	String CODE_ACTIVITE = "ACT";
	String CODE_FRAIS_DIVERS = "FRD";
	String CODE_FRAIS_SANTE = "FRS";
	String CODE_SECU = "SS";
	
	public String code();

	public void setCode(String value);

	public String libelle();

	public void setLibelle(String value);

	public Integer ordre();

	public void setOrdre(Integer value);

}