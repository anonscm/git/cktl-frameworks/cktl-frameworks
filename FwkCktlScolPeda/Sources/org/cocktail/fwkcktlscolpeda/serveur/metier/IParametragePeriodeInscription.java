package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Date;

/**
 * Parametrage des periodes d'ouverture et de fermeture des campagnes d'inscription
 */
public interface IParametragePeriodeInscription {

	
	/**
	 * @return date de fermeture de la periode d'inscription
	 */
	Date dateFermeture();

	/**
	 * @return date d'ouverture de la periode d'inscription
	 */
	Date dateOuverture();

	/**
	 * @param value date de fermeture de la periode d'inscription
	 */
	void setDateFermeture(Date value);

	/**
	 * @param value date d'ouverture de la periode d'inscription
	 */
	void setDateOuverture(Date value);

	/**
	 * @return type de formation pour laquelle est definie la periode d'inscription
	 */
	ITypeFormation typeFormation();
	
	/**
	 * 
	 * @param typeFormation type de formation 
	 */
	void setTypeFormation1(ITypeFormation typeFormation);
	
	/**
	 * @return profil pour lequel est definie la periode d'inscription : etudiant ou gestionnaire
	 */
	String profil();
 
	  /**
	   * @param value profil pour lequel est definie la periode d'inscription : etudiant ou gestionnaire
	   */
	  void setProfil(String value);
  
}
