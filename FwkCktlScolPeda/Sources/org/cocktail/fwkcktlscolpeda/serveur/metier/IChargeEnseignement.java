package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

/**
 * Cette interface représente une charge d'enseignement (table CHARGE_ENSEIGNEMENT).
 */
public interface IChargeEnseignement {

	/**
	 * Retourne l'id de l'AP
	 */
	Integer apId();
	
	/**
	 * Retourne le nombre de groupe.
	 * @return le nombre de groupe (peut être <code>null</code>)
	 */
	Integer nbGroupes();

	/**
	 * Affecte le nombre de groupe.
	 * @param value un nombre de groupe (<code>null</code> pour pas de valeur)
	 */
	void setNbGroupes(Integer value);

	/**
	 * Retourne le nombre d'heure (en centième d'heure, 1.5 = 1h30min).
	 * @return le nombre d'heure (peut être <code>null</code>)
	 */
	BigDecimal valeurHeures();

	/**
	 * Affecte le nombre d'heures (en centième d'heure, 1.5 = 1h30min).
	 * @param value un nombre d'heures (<code>null</code> pour pas de valeur)
	 */
	void setValeurHeures(BigDecimal value);
		  
	/**
	 * Retourne le nombre de minutes.
	 * @return le nombre de minutes (peut être <code>null</code>)
	 */
	Integer valeurMinutes();

	/**
	 * Affecte le nombre de minutes.
	 * @param value un nombre de minutes (<code>null</code> pour pas de valeur)
	 */
	void setValeurMinutes(Integer value);
	
	/**
	 * Retourne le nombre d'heure minutes (1.5 = 1.50 = 1h50min, 1.05 = 1h05min).
	 * @return le nombre d'heure minutes (peut être <code>null</code>)
	 */
	BigDecimal valeurHeureMinutes();

	/**
	 * Affecte le nombre d'heure minutes (1.5 = 1.50 = 1h50min, 1.05 = 1h05min).
	 * @param value un nombre d'heure minutes (<code>null</code> pour pas de valeur)
	 */
	void setValeurHeureMinutes(BigDecimal value);
	
	/**
	 * Supprimer la charge d'enseignement.
	 */
	void supprimer();

	/**
	 * Supprimer la charge d'enseignement lorsque la charge d'enseignement n'a aucune valeur à stocker
	 * (donc que l'occurence en base de données n'a aucune utilisée).
	 * @return <code>true</code> s'il y a eu suppression
	 */
	boolean supprimerSiSansValeurs();

}