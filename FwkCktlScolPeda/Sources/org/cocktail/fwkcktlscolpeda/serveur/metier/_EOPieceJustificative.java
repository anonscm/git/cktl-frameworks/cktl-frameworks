// DO NOT EDIT.  Make changes to EOPieceJustificative.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPieceJustificative extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PieceJustificative";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOPieceJustificative.class);

  public EOPieceJustificative localInstanceIn(EOEditingContext editingContext) {
    EOPieceJustificative localInstance = (EOPieceJustificative)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOPieceJustificative.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPieceJustificative.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPieceJustificative.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOPieceJustificative.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOPieceJustificative.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.ORDRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPieceJustificative.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPieceJustificative.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPieceJustificative.LOG.isDebugEnabled()) {
    	_EOPieceJustificative.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPieceJustificative.PERS_ID_MODIFICATION_KEY);
  }


  public static EOPieceJustificative createSco_PieceJustificative(EOEditingContext editingContext, String code
, NSTimestamp dCreation
, String libelle
, Integer ordre
, Integer persIdCreation
) {
    EOPieceJustificative eo = (EOPieceJustificative) EOUtilities.createAndInsertInstance(editingContext, _EOPieceJustificative.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOPieceJustificative> fetchSpec() {
    return new ERXFetchSpecification<EOPieceJustificative>(_EOPieceJustificative.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPieceJustificative> fetchAllSco_PieceJustificatives(EOEditingContext editingContext) {
    return _EOPieceJustificative.fetchAllSco_PieceJustificatives(editingContext, null);
  }

  public static NSArray<EOPieceJustificative> fetchAllSco_PieceJustificatives(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPieceJustificative.fetchSco_PieceJustificatives(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPieceJustificative> fetchSco_PieceJustificatives(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPieceJustificative> fetchSpec = new ERXFetchSpecification<EOPieceJustificative>(_EOPieceJustificative.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPieceJustificative> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPieceJustificative fetchSco_PieceJustificative(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPieceJustificative.fetchSco_PieceJustificative(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPieceJustificative fetchSco_PieceJustificative(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPieceJustificative> eoObjects = _EOPieceJustificative.fetchSco_PieceJustificatives(editingContext, qualifier, null);
    EOPieceJustificative eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PieceJustificative that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPieceJustificative fetchRequiredSco_PieceJustificative(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPieceJustificative.fetchRequiredSco_PieceJustificative(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPieceJustificative fetchRequiredSco_PieceJustificative(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPieceJustificative eoObject = _EOPieceJustificative.fetchSco_PieceJustificative(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PieceJustificative that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPieceJustificative localInstanceIn(EOEditingContext editingContext, EOPieceJustificative eo) {
    EOPieceJustificative localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
