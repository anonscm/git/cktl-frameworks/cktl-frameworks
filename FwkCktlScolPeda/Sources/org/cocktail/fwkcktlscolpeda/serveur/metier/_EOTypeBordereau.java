// DO NOT EDIT.  Make changes to EOTypeBordereau.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeBordereau extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeBordereau";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeBordereau.class);

  public EOTypeBordereau localInstanceIn(EOEditingContext editingContext) {
    EOTypeBordereau localInstance = (EOTypeBordereau)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeBordereau.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeBordereau.LOG.isDebugEnabled()) {
    	_EOTypeBordereau.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBordereau.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeBordereau.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeBordereau.LOG.isDebugEnabled()) {
    	_EOTypeBordereau.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBordereau.LIBELLE_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EOTypeBordereau.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EOTypeBordereau.LOG.isDebugEnabled()) {
    	_EOTypeBordereau.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeBordereau.LIBELLE_COURT_KEY);
  }


  public static EOTypeBordereau createSco_TypeBordereau(EOEditingContext editingContext, String code
, String libelle
, String libelleCourt
) {
    EOTypeBordereau eo = (EOTypeBordereau) EOUtilities.createAndInsertInstance(editingContext, _EOTypeBordereau.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setLibelleCourt(libelleCourt);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeBordereau> fetchSpec() {
    return new ERXFetchSpecification<EOTypeBordereau>(_EOTypeBordereau.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeBordereau> fetchAllSco_TypeBordereaus(EOEditingContext editingContext) {
    return _EOTypeBordereau.fetchAllSco_TypeBordereaus(editingContext, null);
  }

  public static NSArray<EOTypeBordereau> fetchAllSco_TypeBordereaus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeBordereau.fetchSco_TypeBordereaus(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeBordereau> fetchSco_TypeBordereaus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeBordereau> fetchSpec = new ERXFetchSpecification<EOTypeBordereau>(_EOTypeBordereau.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeBordereau> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeBordereau fetchSco_TypeBordereau(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeBordereau.fetchSco_TypeBordereau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeBordereau fetchSco_TypeBordereau(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeBordereau> eoObjects = _EOTypeBordereau.fetchSco_TypeBordereaus(editingContext, qualifier, null);
    EOTypeBordereau eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeBordereau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeBordereau fetchRequiredSco_TypeBordereau(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeBordereau.fetchRequiredSco_TypeBordereau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeBordereau fetchRequiredSco_TypeBordereau(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeBordereau eoObject = _EOTypeBordereau.fetchSco_TypeBordereau(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeBordereau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeBordereau localInstanceIn(EOEditingContext editingContext, EOTypeBordereau eo) {
    EOTypeBordereau localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
