package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.UUID;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe gère les AEs.
 */
public class EOAE extends _EOAE implements IAE {

	private static final long serialVersionUID = 1L;

	@Override
	public void awakeFromInsertion(EOEditingContext editingContext) {
		super.awakeFromInsertion(editingContext);

		if (this.code() == null) {
			this.setCode("code" + UUID.randomUUID().toString());
		}
	}

	@Override
	public Boolean peutEtresupprime() {
		return (parents().size() <= 1);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean avantSuppression() throws Exception {
		try {
			supprimerType();
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	private void supprimerType() throws Exception {
		if (this.typeAE() != null) {
			setTypeAERelationship(null);
		}
	}

	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeAE(editingContext());
	}

	/**
	 * {@inheritDoc}
	 */
	public EOAE copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			//TODO : récupérer le persId par injection du user
			EOAE ae = EOComposantFactory.createComposant(EOAE.class, this.persIdCreation(), this.editingContext(), 
					anneeVersion, numeroMajeur);
			assignTo(ae);
			return ae;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	/**
	 * @return IAE : le composant suivant associe au composant si il existe
	 */
	public IAE getComposantAvecOrigine() {
		if(super.getComposantSuivant() != null) {
			return ((IAE) (super.getComposantSuivant()));
		}
		
		return null;
	}
	
	/**
	 * @return IAE : le composant d'origine
	 */
	public IAE getComposantOrigine() {
		return (IAE) composantOrigine();
	}
	
	private void assignTo(EOAE ae) {
		super.assignTo(ae);
		ae.setBaseNote(this.baseNote());
		ae.setDureeMax(this.dureeMax());
		ae.setDureeMin(this.dureeMin());
		ae.setEpreuve(this.epreuve());
		ae.setSansNote(this.sansNote());
		ae.setTypeAERelationship(this.typeAE());		
	}
	
	/**
     * Valide typeAE
     * 
     * @param typeAE
     * @throws ValidationException
     */
    public Object validateTypeAE(EOTypeAE typeAE) throws ValidationException {
    	if (typeAE == null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_AE.typeAE.NullPropertyException");
    	}
		
		return typeAE;
    }
    
    @Override
    public String libelleTreeTable() {
    	return typeAE().code() + " - " + libelle();
    }
	
}
