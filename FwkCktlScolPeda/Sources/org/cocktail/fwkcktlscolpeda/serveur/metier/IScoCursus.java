package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

public interface IScoCursus extends ICursus {

	IEtudiant toEtudiant();

	void setToEtudiantRelationship(IEtudiant value);

}
