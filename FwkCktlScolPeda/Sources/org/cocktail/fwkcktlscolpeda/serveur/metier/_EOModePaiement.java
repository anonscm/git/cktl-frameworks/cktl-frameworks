// DO NOT EDIT.  Make changes to EOModePaiement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOModePaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ModePaiement";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> MOD_CODE_PAIEMENT = new ERXKey<String>("modCodePaiement");
  public static final ERXKey<String> MOD_CODE_REMBOURSEMENT = new ERXKey<String>("modCodeRemboursement");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> TO_PLAN_COMPTABLE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("toPlanComptablePaiement");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> TO_PLAN_COMPTABLE_REMBOURSEMENT = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("toPlanComptableRemboursement");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MOD_CODE_PAIEMENT_KEY = MOD_CODE_PAIEMENT.key();
  public static final String MOD_CODE_REMBOURSEMENT_KEY = MOD_CODE_REMBOURSEMENT.key();
  public static final String ORDRE_KEY = ORDRE.key();
  // Relationships
  public static final String TO_PLAN_COMPTABLE_PAIEMENT_KEY = TO_PLAN_COMPTABLE_PAIEMENT.key();
  public static final String TO_PLAN_COMPTABLE_REMBOURSEMENT_KEY = TO_PLAN_COMPTABLE_REMBOURSEMENT.key();

  private static Logger LOG = Logger.getLogger(_EOModePaiement.class);

  public EOModePaiement localInstanceIn(EOEditingContext editingContext) {
    EOModePaiement localInstance = (EOModePaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOModePaiement.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
    	_EOModePaiement.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModePaiement.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOModePaiement.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
    	_EOModePaiement.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModePaiement.LIBELLE_KEY);
  }

  public String modCodePaiement() {
    return (String) storedValueForKey(_EOModePaiement.MOD_CODE_PAIEMENT_KEY);
  }

  public void setModCodePaiement(String value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
    	_EOModePaiement.LOG.debug( "updating modCodePaiement from " + modCodePaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModePaiement.MOD_CODE_PAIEMENT_KEY);
  }

  public String modCodeRemboursement() {
    return (String) storedValueForKey(_EOModePaiement.MOD_CODE_REMBOURSEMENT_KEY);
  }

  public void setModCodeRemboursement(String value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
    	_EOModePaiement.LOG.debug( "updating modCodeRemboursement from " + modCodeRemboursement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModePaiement.MOD_CODE_REMBOURSEMENT_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOModePaiement.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
    	_EOModePaiement.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOModePaiement.ORDRE_KEY);
  }

  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptablePaiement() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(_EOModePaiement.TO_PLAN_COMPTABLE_PAIEMENT_KEY);
  }
  
  public void setToPlanComptablePaiement(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, _EOModePaiement.TO_PLAN_COMPTABLE_PAIEMENT_KEY);
  }

  public void setToPlanComptablePaiementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
      _EOModePaiement.LOG.debug("updating toPlanComptablePaiement from " + toPlanComptablePaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPlanComptablePaiement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = toPlanComptablePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOModePaiement.TO_PLAN_COMPTABLE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOModePaiement.TO_PLAN_COMPTABLE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable toPlanComptableRemboursement() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(_EOModePaiement.TO_PLAN_COMPTABLE_REMBOURSEMENT_KEY);
  }
  
  public void setToPlanComptableRemboursement(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, _EOModePaiement.TO_PLAN_COMPTABLE_REMBOURSEMENT_KEY);
  }

  public void setToPlanComptableRemboursementRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    if (_EOModePaiement.LOG.isDebugEnabled()) {
      _EOModePaiement.LOG.debug("updating toPlanComptableRemboursement from " + toPlanComptableRemboursement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPlanComptableRemboursement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = toPlanComptableRemboursement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOModePaiement.TO_PLAN_COMPTABLE_REMBOURSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOModePaiement.TO_PLAN_COMPTABLE_REMBOURSEMENT_KEY);
    }
  }
  

  public static EOModePaiement createSco_ModePaiement(EOEditingContext editingContext, String code
, String libelle
, Integer ordre
) {
    EOModePaiement eo = (EOModePaiement) EOUtilities.createAndInsertInstance(editingContext, _EOModePaiement.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
		eo.setOrdre(ordre);
    return eo;
  }

  public static ERXFetchSpecification<EOModePaiement> fetchSpec() {
    return new ERXFetchSpecification<EOModePaiement>(_EOModePaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOModePaiement> fetchAllSco_ModePaiements(EOEditingContext editingContext) {
    return _EOModePaiement.fetchAllSco_ModePaiements(editingContext, null);
  }

  public static NSArray<EOModePaiement> fetchAllSco_ModePaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOModePaiement.fetchSco_ModePaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOModePaiement> fetchSco_ModePaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOModePaiement> fetchSpec = new ERXFetchSpecification<EOModePaiement>(_EOModePaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOModePaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOModePaiement fetchSco_ModePaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModePaiement.fetchSco_ModePaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOModePaiement fetchSco_ModePaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOModePaiement> eoObjects = _EOModePaiement.fetchSco_ModePaiements(editingContext, qualifier, null);
    EOModePaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ModePaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOModePaiement fetchRequiredSco_ModePaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOModePaiement.fetchRequiredSco_ModePaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOModePaiement fetchRequiredSco_ModePaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOModePaiement eoObject = _EOModePaiement.fetchSco_ModePaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ModePaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOModePaiement localInstanceIn(EOEditingContext editingContext, EOModePaiement eo) {
    EOModePaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
