package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface ITypeInscription {
	String CODE_ENS_SUP = "ES";
	String CODE_UNIV_FRANCAISE = "Fr";
	String CODE_LOCAL = "Local";
	
	String lcTypeInscription();
	String llTypeInscription();

}
