// DO NOT EDIT.  Make changes to EODiscipline.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODiscipline extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Discipline";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_DEBUT = new ERXKey<Integer>("annee_debut");
  public static final ERXKey<Integer> ANNEE_FIN = new ERXKey<Integer>("annee_fin");
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Boolean> VALIDITE = new ERXKey<Boolean>("validite");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> TO_CHILDREN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("toChildren");

  // Attributes
  public static final String ANNEE_DEBUT_KEY = ANNEE_DEBUT.key();
  public static final String ANNEE_FIN_KEY = ANNEE_FIN.key();
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String VALIDITE_KEY = VALIDITE.key();
  // Relationships
  public static final String TO_CHILDREN_KEY = TO_CHILDREN.key();

  private static Logger LOG = Logger.getLogger(_EODiscipline.class);

  public EODiscipline localInstanceIn(EOEditingContext editingContext) {
    EODiscipline localInstance = (EODiscipline)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee_debut() {
    return (Integer) storedValueForKey(_EODiscipline.ANNEE_DEBUT_KEY);
  }

  public void setAnnee_debut(Integer value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating annee_debut from " + annee_debut() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.ANNEE_DEBUT_KEY);
  }

  public Integer annee_fin() {
    return (Integer) storedValueForKey(_EODiscipline.ANNEE_FIN_KEY);
  }

  public void setAnnee_fin(Integer value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating annee_fin from " + annee_fin() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.ANNEE_FIN_KEY);
  }

  public String code() {
    return (String) storedValueForKey(_EODiscipline.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODiscipline.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.ID_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EODiscipline.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.LIBELLE_COURT_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(_EODiscipline.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.LIBELLE_LONG_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EODiscipline.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.NIVEAU_KEY);
  }

  public Boolean validite() {
    return (Boolean) storedValueForKey(_EODiscipline.VALIDITE_KEY);
  }

  public void setValidite(Boolean value) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
    	_EODiscipline.LOG.debug( "updating validite from " + validite() + " to " + value);
    }
    takeStoredValueForKey(value, _EODiscipline.VALIDITE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> toChildren() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)storedValueForKey(_EODiscipline.TO_CHILDREN_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> toChildren(EOQualifier qualifier) {
    return toChildren(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> toChildren(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> results;
      results = toChildren();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToChildren(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    includeObjectIntoPropertyWithKey(object, _EODiscipline.TO_CHILDREN_KEY);
  }

  public void removeFromToChildren(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    excludeObjectFromPropertyWithKey(object, _EODiscipline.TO_CHILDREN_KEY);
  }

  public void addToToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
      _EODiscipline.LOG.debug("adding " + object + " to toChildren relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToChildren(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODiscipline.TO_CHILDREN_KEY);
    }
  }

  public void removeFromToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    if (_EODiscipline.LOG.isDebugEnabled()) {
      _EODiscipline.LOG.debug("removing " + object + " from toChildren relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToChildren(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiscipline.TO_CHILDREN_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline createToChildrenRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODiscipline.TO_CHILDREN_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline) eo;
  }

  public void deleteToChildrenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODiscipline.TO_CHILDREN_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToChildrenRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> objects = toChildren().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToChildrenRelationship(objects.nextElement());
    }
  }


  public static EODiscipline createSco_Discipline(EOEditingContext editingContext, Integer annee_debut
, Integer id
, String libelleLong
, Integer niveau
, Boolean validite
) {
    EODiscipline eo = (EODiscipline) EOUtilities.createAndInsertInstance(editingContext, _EODiscipline.ENTITY_NAME);    
		eo.setAnnee_debut(annee_debut);
		eo.setId(id);
		eo.setLibelleLong(libelleLong);
		eo.setNiveau(niveau);
		eo.setValidite(validite);
    return eo;
  }

  public static ERXFetchSpecification<EODiscipline> fetchSpec() {
    return new ERXFetchSpecification<EODiscipline>(_EODiscipline.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODiscipline> fetchAllSco_Disciplines(EOEditingContext editingContext) {
    return _EODiscipline.fetchAllSco_Disciplines(editingContext, null);
  }

  public static NSArray<EODiscipline> fetchAllSco_Disciplines(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODiscipline.fetchSco_Disciplines(editingContext, null, sortOrderings);
  }

  public static NSArray<EODiscipline> fetchSco_Disciplines(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODiscipline> fetchSpec = new ERXFetchSpecification<EODiscipline>(_EODiscipline.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODiscipline> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODiscipline fetchSco_Discipline(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiscipline.fetchSco_Discipline(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiscipline fetchSco_Discipline(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODiscipline> eoObjects = _EODiscipline.fetchSco_Disciplines(editingContext, qualifier, null);
    EODiscipline eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Discipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiscipline fetchRequiredSco_Discipline(EOEditingContext editingContext, String keyName, Object value) {
    return _EODiscipline.fetchRequiredSco_Discipline(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODiscipline fetchRequiredSco_Discipline(EOEditingContext editingContext, EOQualifier qualifier) {
    EODiscipline eoObject = _EODiscipline.fetchSco_Discipline(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Discipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODiscipline localInstanceIn(EOEditingContext editingContext, EODiscipline eo) {
    EODiscipline localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
