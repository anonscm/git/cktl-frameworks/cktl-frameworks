package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * interface pour le type d'échange 
 * @author isabelle
 *
 */
public interface ITypeEchange {

	String TYPE_ECHANGE_ARRIVEE = "A";
	String TYPE_ECHANGE_DEPART = "D";

	String AUTRES_PGMES_ACCORDS_BI_A = "AUTRES_PGMES_ACCORDS_BI_A";
	String AUTRES_PGMES_ACCORDS_BI_D = "AUTRES_PGMES_ACCORDS_BI_D";
	String AUTRES_PGMES_FINANCES_UE_A = "AUTRES_PGMES_FINANCES_UE_A";
	String AUTRES_PGMES_FINANCES_UE_D = "AUTRES_PGMES_FINANCES_UE_D";
	String SOCRATES_ERASMUS_A = "SOCRATES_ERASMUS_A";
	String SOCRATES_ERASMUS_D = "SOCRATES_ERASMUS_D";
	String TEMPUS_A = "TEMPUS_A";
	String TEMPUS_D = "TEMPUS_D";
	String SOCRATES_COMENIUS_A = "SOCRATES_COMENIUS_A";
	String SOCRATES_COMENIUS_D = "SOCRATES_COMENIUS_D";
	String LEONARDO_DA_VINCI_A = "LEONARDO_DA_VINCI_A";
	String LEONARDO_DA_VINCI_D = "LEONARDO_DA_VINCI_D";

	/**
	 * 
	 * @return le code échange
	 */
	public String codeEchange();

	/**
	 * 
	 * @param value the code echange
	 */
	public void setCodeEchange(String value);

	/**
	 * 
	 * @return le libelle
	 */
	public String libelle();

	/**
	 * 
	 * @param value the libelle
	 */
	public void setLibelle(String value);

	/**
	 * 
	 * @return le sens (arrivée ou départ)
	 */
	public String sens();

	/**
	 * 
	 * @param value the sens
	 */
	public void setSens(String value);

}