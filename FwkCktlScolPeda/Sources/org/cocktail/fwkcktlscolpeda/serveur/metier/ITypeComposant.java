package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * définit le contrat d'un type de composant
 */
public interface ITypeComposant {

	String TYPEDIPLOME_NOM = "DIPLOME";
	String TYPEVERSIONDIPLOME_NOM = "VERSION_DIPLOME";
	String TYPEPERIODE_NOM = "PERIODE";
	String TYPEPARCOURS_NOM = "PARCOURS";
	String TYPEREGROUPEMENT_NOM = "REGROUPEMENT";
	String TYPEEC_NOM = "EC";
	String TYPEUE_NOM = "UE";
	String TYPEAP_NOM = "AP";
	String TYPEAE_NOM = "AE";

	/**
	 * @return l'id du type de composant
	 */
	Integer id();
	
	/**
	 * @return nom du type de composant
	 */
	String nom();

}