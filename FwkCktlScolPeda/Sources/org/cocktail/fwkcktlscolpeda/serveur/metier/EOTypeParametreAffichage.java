package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EOTypeParametreAffichage extends _EOTypeParametreAffichage {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeParametreAffichage.class);
	
	public static final String MODIFIABLE = "MODIFIABLE";
	public static final String LECTURE = "LECTURE_SEULE";
	public static final String INVISIBLE = "INVISIBLE";

}
