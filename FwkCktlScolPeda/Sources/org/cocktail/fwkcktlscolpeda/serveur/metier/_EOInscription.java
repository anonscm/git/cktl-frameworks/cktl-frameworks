// DO NOT EDIT.  Make changes to EOInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Inscription";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Boolean> CYCLE_AMENAGE = new ERXKey<Boolean>("cycleAmenage");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> GRADE = new ERXKey<Integer>("grade");
  public static final ERXKey<Integer> ID_ETABLISSEMENT_CUMULATIF = new ERXKey<Integer>("idEtablissementCumulatif");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_ANNEE = new ERXKey<String>("idLienConsultationAnnee");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_DIPLOME = new ERXKey<String>("idLienConsultationDiplome");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_PARCOURS_ANNEE = new ERXKey<String>("idLienConsultationParcoursAnnee");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_PARCOURS_DIPLOME = new ERXKey<String>("idLienConsultationParcoursDiplome");
  public static final ERXKey<Integer> ID_PARCOURS_ANNEE = new ERXKey<Integer>("idParcoursAnnee");
  public static final ERXKey<Integer> ID_PARCOURS_DIPLOME = new ERXKey<Integer>("idParcoursDiplome");
  public static final ERXKey<Integer> IDTYPE_ECHANGE = new ERXKey<Integer>("idtypeEchange");
  public static final ERXKey<Integer> ID_TYPE_INSCRIPTION_FORMATION = new ERXKey<Integer>("idTypeInscriptionFormation");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Boolean> PASSAGE_CONDITIONNEL = new ERXKey<Boolean>("passageConditionnel");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> REDOUBLEMENT = new ERXKey<Boolean>("redoublement");
  public static final ERXKey<Boolean> REORIENTATION = new ERXKey<Boolean>("reorientation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> TO_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("toDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif> TO_ETABLISSEMENT_CUMULATIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif>("toEtablissementCumulatif");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee> TO_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee>("toEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> TO_INSCRIPTIONS_PEDAGOGIQUES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique>("toInscriptionsPedagogiques");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> TO_PARCOURS_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("toParcoursAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> TO_PARCOURS_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("toParcoursDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription> TO_REGIME_INSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription>("toRegimeInscription");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange> TO_TYPE_ECHANGE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange>("toTypeEchange");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation> TO_TYPE_INSCRIPTION_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation>("toTypeInscriptionFormation");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String CYCLE_AMENAGE_KEY = CYCLE_AMENAGE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String GRADE_KEY = GRADE.key();
  public static final String ID_ETABLISSEMENT_CUMULATIF_KEY = ID_ETABLISSEMENT_CUMULATIF.key();
  public static final String ID_LIEN_CONSULTATION_ANNEE_KEY = ID_LIEN_CONSULTATION_ANNEE.key();
  public static final String ID_LIEN_CONSULTATION_DIPLOME_KEY = ID_LIEN_CONSULTATION_DIPLOME.key();
  public static final String ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY = ID_LIEN_CONSULTATION_PARCOURS_ANNEE.key();
  public static final String ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY = ID_LIEN_CONSULTATION_PARCOURS_DIPLOME.key();
  public static final String ID_PARCOURS_ANNEE_KEY = ID_PARCOURS_ANNEE.key();
  public static final String ID_PARCOURS_DIPLOME_KEY = ID_PARCOURS_DIPLOME.key();
  public static final String IDTYPE_ECHANGE_KEY = IDTYPE_ECHANGE.key();
  public static final String ID_TYPE_INSCRIPTION_FORMATION_KEY = ID_TYPE_INSCRIPTION_FORMATION.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String PASSAGE_CONDITIONNEL_KEY = PASSAGE_CONDITIONNEL.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String REDOUBLEMENT_KEY = REDOUBLEMENT.key();
  public static final String REORIENTATION_KEY = REORIENTATION.key();
  // Relationships
  public static final String TO_DIPLOME_KEY = TO_DIPLOME.key();
  public static final String TO_ETABLISSEMENT_CUMULATIF_KEY = TO_ETABLISSEMENT_CUMULATIF.key();
  public static final String TO_ETUDIANT_KEY = TO_ETUDIANT.key();
  public static final String TO_ETUDIANT_ANNEE_KEY = TO_ETUDIANT_ANNEE.key();
  public static final String TO_GRADE_UNIVERSITAIRE_KEY = TO_GRADE_UNIVERSITAIRE.key();
  public static final String TO_INSCRIPTIONS_PEDAGOGIQUES_KEY = TO_INSCRIPTIONS_PEDAGOGIQUES.key();
  public static final String TO_PARCOURS_ANNEE_KEY = TO_PARCOURS_ANNEE.key();
  public static final String TO_PARCOURS_DIPLOME_KEY = TO_PARCOURS_DIPLOME.key();
  public static final String TO_REGIME_INSCRIPTION_KEY = TO_REGIME_INSCRIPTION.key();
  public static final String TO_TYPE_ECHANGE_KEY = TO_TYPE_ECHANGE.key();
  public static final String TO_TYPE_INSCRIPTION_FORMATION_KEY = TO_TYPE_INSCRIPTION_FORMATION.key();

  private static Logger LOG = Logger.getLogger(_EOInscription.class);

  public EOInscription localInstanceIn(EOEditingContext editingContext) {
    EOInscription localInstance = (EOInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOInscription.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ANNEE_KEY);
  }

  public Boolean cycleAmenage() {
    return (Boolean) storedValueForKey(_EOInscription.CYCLE_AMENAGE_KEY);
  }

  public void setCycleAmenage(Boolean value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating cycleAmenage from " + cycleAmenage() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.CYCLE_AMENAGE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOInscription.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOInscription.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.D_MODIFICATION_KEY);
  }

  public Integer grade() {
    return (Integer) storedValueForKey(_EOInscription.GRADE_KEY);
  }

  public void setGrade(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating grade from " + grade() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.GRADE_KEY);
  }

  public Integer idEtablissementCumulatif() {
    return (Integer) storedValueForKey(_EOInscription.ID_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public void setIdEtablissementCumulatif(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idEtablissementCumulatif from " + idEtablissementCumulatif() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public String idLienConsultationAnnee() {
    return (String) storedValueForKey(_EOInscription.ID_LIEN_CONSULTATION_ANNEE_KEY);
  }

  public void setIdLienConsultationAnnee(String value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idLienConsultationAnnee from " + idLienConsultationAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_LIEN_CONSULTATION_ANNEE_KEY);
  }

  public String idLienConsultationDiplome() {
    return (String) storedValueForKey(_EOInscription.ID_LIEN_CONSULTATION_DIPLOME_KEY);
  }

  public void setIdLienConsultationDiplome(String value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idLienConsultationDiplome from " + idLienConsultationDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_LIEN_CONSULTATION_DIPLOME_KEY);
  }

  public String idLienConsultationParcoursAnnee() {
    return (String) storedValueForKey(_EOInscription.ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY);
  }

  public void setIdLienConsultationParcoursAnnee(String value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idLienConsultationParcoursAnnee from " + idLienConsultationParcoursAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY);
  }

  public String idLienConsultationParcoursDiplome() {
    return (String) storedValueForKey(_EOInscription.ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY);
  }

  public void setIdLienConsultationParcoursDiplome(String value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idLienConsultationParcoursDiplome from " + idLienConsultationParcoursDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY);
  }

  public Integer idParcoursAnnee() {
    return (Integer) storedValueForKey(_EOInscription.ID_PARCOURS_ANNEE_KEY);
  }

  public void setIdParcoursAnnee(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idParcoursAnnee from " + idParcoursAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_PARCOURS_ANNEE_KEY);
  }

  public Integer idParcoursDiplome() {
    return (Integer) storedValueForKey(_EOInscription.ID_PARCOURS_DIPLOME_KEY);
  }

  public void setIdParcoursDiplome(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idParcoursDiplome from " + idParcoursDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_PARCOURS_DIPLOME_KEY);
  }

  public Integer idtypeEchange() {
    return (Integer) storedValueForKey(_EOInscription.IDTYPE_ECHANGE_KEY);
  }

  public void setIdtypeEchange(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idtypeEchange from " + idtypeEchange() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.IDTYPE_ECHANGE_KEY);
  }

  public Integer idTypeInscriptionFormation() {
    return (Integer) storedValueForKey(_EOInscription.ID_TYPE_INSCRIPTION_FORMATION_KEY);
  }

  public void setIdTypeInscriptionFormation(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating idTypeInscriptionFormation from " + idTypeInscriptionFormation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.ID_TYPE_INSCRIPTION_FORMATION_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EOInscription.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.NIVEAU_KEY);
  }

  public Boolean passageConditionnel() {
    return (Boolean) storedValueForKey(_EOInscription.PASSAGE_CONDITIONNEL_KEY);
  }

  public void setPassageConditionnel(Boolean value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating passageConditionnel from " + passageConditionnel() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.PASSAGE_CONDITIONNEL_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOInscription.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOInscription.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean redoublement() {
    return (Boolean) storedValueForKey(_EOInscription.REDOUBLEMENT_KEY);
  }

  public void setRedoublement(Boolean value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating redoublement from " + redoublement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.REDOUBLEMENT_KEY);
  }

  public Boolean reorientation() {
    return (Boolean) storedValueForKey(_EOInscription.REORIENTATION_KEY);
  }

  public void setReorientation(Boolean value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
    	_EOInscription.LOG.debug( "updating reorientation from " + reorientation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscription.REORIENTATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome)storedValueForKey(_EOInscription.TO_DIPLOME_KEY);
  }
  
  public void setToDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    takeStoredValueForKey(value, _EOInscription.TO_DIPLOME_KEY);
  }

  public void setToDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toDiplome from " + toDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome oldValue = toDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif toEtablissementCumulatif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif)storedValueForKey(_EOInscription.TO_ETABLISSEMENT_CUMULATIF_KEY);
  }
  
  public void setToEtablissementCumulatif(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif value) {
    takeStoredValueForKey(value, _EOInscription.TO_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public void setToEtablissementCumulatifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toEtablissementCumulatif from " + toEtablissementCumulatif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtablissementCumulatif(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif oldValue = toEtablissementCumulatif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_ETABLISSEMENT_CUMULATIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_ETABLISSEMENT_CUMULATIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant)storedValueForKey(_EOInscription.TO_ETUDIANT_KEY);
  }
  
  public void setToEtudiant(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    takeStoredValueForKey(value, _EOInscription.TO_ETUDIANT_KEY);
  }

  public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toEtudiant from " + toEtudiant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee)storedValueForKey(_EOInscription.TO_ETUDIANT_ANNEE_KEY);
  }
  
  public void setToEtudiantAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    takeStoredValueForKey(value, _EOInscription.TO_ETUDIANT_ANNEE_KEY);
  }

  public void setToEtudiantAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toEtudiantAnnee from " + toEtudiantAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEtudiantAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee oldValue = toEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_ETUDIANT_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EOInscription.TO_GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setToGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EOInscription.TO_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toGradeUniversitaire from " + toGradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours toParcoursAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(_EOInscription.TO_PARCOURS_ANNEE_KEY);
  }
  
  public void setToParcoursAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    takeStoredValueForKey(value, _EOInscription.TO_PARCOURS_ANNEE_KEY);
  }

  public void setToParcoursAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toParcoursAnnee from " + toParcoursAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParcoursAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = toParcoursAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_PARCOURS_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_PARCOURS_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours toParcoursDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(_EOInscription.TO_PARCOURS_DIPLOME_KEY);
  }
  
  public void setToParcoursDiplome(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    takeStoredValueForKey(value, _EOInscription.TO_PARCOURS_DIPLOME_KEY);
  }

  public void setToParcoursDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toParcoursDiplome from " + toParcoursDiplome() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParcoursDiplome(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = toParcoursDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_PARCOURS_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_PARCOURS_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription toRegimeInscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription)storedValueForKey(_EOInscription.TO_REGIME_INSCRIPTION_KEY);
  }
  
  public void setToRegimeInscription(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription value) {
    takeStoredValueForKey(value, _EOInscription.TO_REGIME_INSCRIPTION_KEY);
  }

  public void setToRegimeInscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toRegimeInscription from " + toRegimeInscription() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRegimeInscription(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription oldValue = toRegimeInscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_REGIME_INSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_REGIME_INSCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange toTypeEchange() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange)storedValueForKey(_EOInscription.TO_TYPE_ECHANGE_KEY);
  }
  
  public void setToTypeEchange(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange value) {
    takeStoredValueForKey(value, _EOInscription.TO_TYPE_ECHANGE_KEY);
  }

  public void setToTypeEchangeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toTypeEchange from " + toTypeEchange() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeEchange(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange oldValue = toTypeEchange();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_TYPE_ECHANGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_TYPE_ECHANGE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation toTypeInscriptionFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation)storedValueForKey(_EOInscription.TO_TYPE_INSCRIPTION_FORMATION_KEY);
  }
  
  public void setToTypeInscriptionFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation value) {
    takeStoredValueForKey(value, _EOInscription.TO_TYPE_INSCRIPTION_FORMATION_KEY);
  }

  public void setToTypeInscriptionFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation value) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("updating toTypeInscriptionFormation from " + toTypeInscriptionFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeInscriptionFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation oldValue = toTypeInscriptionFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscription.TO_TYPE_INSCRIPTION_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscription.TO_TYPE_INSCRIPTION_FORMATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> toInscriptionsPedagogiques() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique>)storedValueForKey(_EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> toInscriptionsPedagogiques(EOQualifier qualifier) {
    return toInscriptionsPedagogiques(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> toInscriptionsPedagogiques(EOQualifier qualifier, boolean fetch) {
    return toInscriptionsPedagogiques(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> toInscriptionsPedagogiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique.TO_INSCRIPTION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique.fetchSco_InscriptionPedagogiques(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toInscriptionsPedagogiques();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToInscriptionsPedagogiques(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique object) {
    includeObjectIntoPropertyWithKey(object, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
  }

  public void removeFromToInscriptionsPedagogiques(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique object) {
    excludeObjectFromPropertyWithKey(object, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
  }

  public void addToToInscriptionsPedagogiquesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique object) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("adding " + object + " to toInscriptionsPedagogiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToInscriptionsPedagogiques(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
    }
  }

  public void removeFromToInscriptionsPedagogiquesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique object) {
    if (_EOInscription.LOG.isDebugEnabled()) {
      _EOInscription.LOG.debug("removing " + object + " from toInscriptionsPedagogiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToInscriptionsPedagogiques(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique createToInscriptionsPedagogiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique) eo;
  }

  public void deleteToInscriptionsPedagogiquesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToInscriptionsPedagogiquesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> objects = toInscriptionsPedagogiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToInscriptionsPedagogiquesRelationship(objects.nextElement());
    }
  }


  public static EOInscription createSco_Inscription(EOEditingContext editingContext, Integer annee
, Boolean cycleAmenage
, NSTimestamp dCreation
, Boolean passageConditionnel
, Boolean redoublement
, Boolean reorientation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant, org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee toEtudiantAnnee) {
    EOInscription eo = (EOInscription) EOUtilities.createAndInsertInstance(editingContext, _EOInscription.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setCycleAmenage(cycleAmenage);
		eo.setDCreation(dCreation);
		eo.setPassageConditionnel(passageConditionnel);
		eo.setRedoublement(redoublement);
		eo.setReorientation(reorientation);
    eo.setToDiplomeRelationship(toDiplome);
    eo.setToEtudiantRelationship(toEtudiant);
    eo.setToEtudiantAnneeRelationship(toEtudiantAnnee);
    return eo;
  }

  public static ERXFetchSpecification<EOInscription> fetchSpec() {
    return new ERXFetchSpecification<EOInscription>(_EOInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOInscription> fetchAllSco_Inscriptions(EOEditingContext editingContext) {
    return _EOInscription.fetchAllSco_Inscriptions(editingContext, null);
  }

  public static NSArray<EOInscription> fetchAllSco_Inscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOInscription.fetchSco_Inscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOInscription> fetchSco_Inscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOInscription> fetchSpec = new ERXFetchSpecification<EOInscription>(_EOInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOInscription fetchSco_Inscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscription.fetchSco_Inscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscription fetchSco_Inscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOInscription> eoObjects = _EOInscription.fetchSco_Inscriptions(editingContext, qualifier, null);
    EOInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Inscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscription fetchRequiredSco_Inscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscription.fetchRequiredSco_Inscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscription fetchRequiredSco_Inscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOInscription eoObject = _EOInscription.fetchSco_Inscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Inscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscription localInstanceIn(EOEditingContext editingContext, EOInscription eo) {
    EOInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
