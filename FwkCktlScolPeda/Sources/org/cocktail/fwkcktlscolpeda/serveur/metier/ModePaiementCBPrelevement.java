package org.cocktail.fwkcktlscolpeda.serveur.metier;

public class ModePaiementCBPrelevement implements IModePaiement {

	private static ModePaiementCBPrelevement modePaiementCBPrelevement;
	
	public String code() {
		return IModePaiement.CODE_MODE_CB_PRELEVEMENT;
	}

	public String libelle() {
		return "CB et Prélèvement";
	}

	public String modCodePaiement() {
		return IModePaiement.CODE_MODE_CB_PRELEVEMENT;
	}

	public String modCodeRemboursement() {
		return IModePaiement.CODE_MODE_CB_PRELEVEMENT;
	}
	
	public Integer ordre() {
		return 11;
	}

	public String getPlanComptablePaiementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}
	
	public String getPlanComptableRemboursementAUtilisePourExercice(int exercice) {
		throw new UnsupportedOperationException("Objet existant en mémoire uniquement");
	}
	
	public static IModePaiement instance() {
		if (modePaiementCBPrelevement == null) {
			modePaiementCBPrelevement = new ModePaiementCBPrelevement();
		}
		return modePaiementCBPrelevement;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSimple() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isPrelevement() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isCheque() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
    public boolean isVirement() {
    	return false;
    }
}
