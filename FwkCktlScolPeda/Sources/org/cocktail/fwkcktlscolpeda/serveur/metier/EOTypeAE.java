package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;
import er.extensions.validation.ERXValidationFactory;

/**
 * Cette classe represente un type d'AE
 */
@SuppressWarnings("serial")
public class EOTypeAE extends _EOTypeAE implements ITypeAE{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeAE.class);
	private static ERXEnterpriseObjectCache<EOTypeAE> typeAECache;

	public static final String TYPE_CC_ORAL_CODE = "CC_ORAL";
	public static final String TYPE_CC_ECRIT_CODE = "CC_ECRIT";
	public static final String TYPE_EX_ORAL_CODE = "EX_ORAL";
	public static final String TYPE_EX_ECRIT_CODE = "EX_ECRIT";
	public static final String TYPE_MEMOIRE_CODE = "MM";
	public static final String TYPE_TD_CODE = "TD";
	public static final String TYPE_TP_CODE = "TP";
	public static final String TYPE_STAGE_CODE = "STA";
	public static final String TYPE_TOEIC_CODE = "TOEIC";
	public static final String TYPE_TOEFL_CODE = "TOEFL";

	/**
	 * @return le cache des {@link EOTypeAE} par leur code
	 */
	public static ERXEnterpriseObjectCache<EOTypeAE> getTypeAECache() {
		if (typeAECache == null) {
			typeAECache = new ERXEnterpriseObjectCache<EOTypeAE>(EOTypeAE.class, CODE_KEY);
		}
		return typeAECache;
	}

	/**
	 * @param typeAECache le cache a setter
	 */
	public static void setTypeAECache(ERXEnterpriseObjectCache<EOTypeAE> typeAECache) {
		EOTypeAE.typeAECache = typeAECache;
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "CC_ORAL"
	 */
	public static EOTypeAE typeCCOral(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_CC_ORAL_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "CC_ECRIT"
	 */
	public static EOTypeAE typeCCEcrit(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_CC_ECRIT_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "EX_ORAL"
	 */
	public static EOTypeAE typeEXOral(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_EX_ORAL_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "EX_ECRIT"
	 */
	public static EOTypeAE typeEXEcrit(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_EX_ECRIT_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "MM"
	 */
	public static EOTypeAE typeMemoire(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_MEMOIRE_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "TD"
	 */
	public static EOTypeAE typeTD(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_TD_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "TP"
	 */
	public static EOTypeAE typeTP(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_TP_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "STA"
	 */
	public static EOTypeAE typeStage(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_STAGE_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "TOEIC"
	 */
	public static EOTypeAE typeTOEIC(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_TOEIC_CODE);
	}

	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypeAE} "TOEFL"
	 */
	public static EOTypeAE typeTOEFL(EOEditingContext edc) {
		return getTypeAECache().objectForKey(edc, TYPE_TOEFL_CODE);
	}

	/**
	 * Valide que le code ajoute est correct.
	 * @param code le code a verifier
	 * @throws ValidationException l'exception lancee si le code n'est pas validee
	 * @return le code si il est valide
	 */
	public Object validateCode(String code) throws ValidationException {
		if (!EOTypeAE.fetchSco_TypeAEs(editingContext(), EOTypeAE.CODE.eq(code).and(EOTypeAE.ID.ne(this.id())), null).isEmpty()) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "ScolPeda_TypeAE.codeExistant");
		}
		return code;
	}

	@Override
	public String toString() {
		return this.code() + " - " + this.libelle();
	}

}
