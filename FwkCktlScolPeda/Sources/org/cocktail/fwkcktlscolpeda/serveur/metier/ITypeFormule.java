package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Type de formule (pour la moyenne, l'admission, ou la compensation, la dette)
 */
public interface ITypeFormule {

	String TYPEMOYENNE_LBL = "MOYENNE";
	String TYPEADMISSION_LBL = "ADMISSION";
	String TYPECOMPENSATION_LBL = "COMPENSATION";
	String TYPEDETTES_LBL = "DETTE";

	List<String> ORDRE_TYPES = Arrays.asList(TYPEMOYENNE_LBL, TYPEADMISSION_LBL, TYPECOMPENSATION_LBL, TYPEDETTES_LBL);

	Comparator<ITypeFormule> TYPE_ASC = new Comparator<ITypeFormule>() {
		public int compare(ITypeFormule typeFormule1, ITypeFormule typeFormule2) {
			return ORDRE_TYPES.indexOf(typeFormule1.type()) - ORDRE_TYPES.indexOf(typeFormule2.type());
		}
	};

	/**
	 * @return type de la formule
	 */
	String type();

	/**
	 * @return le libellé de la formule
	 */
	String libelle();

	/**
	 * @return true si c'est une formule moyenne
	 */
	boolean isMoyenne();

}
