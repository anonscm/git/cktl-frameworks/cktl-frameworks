// DO NOT EDIT.  Make changes to EOTypeParametreAffichage.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeParametreAffichage extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeParametreAffichage";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFCATION_KEY = PERS_ID_MODIFCATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeParametreAffichage.class);

  public EOTypeParametreAffichage localInstanceIn(EOEditingContext editingContext) {
    EOTypeParametreAffichage localInstance = (EOTypeParametreAffichage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypeParametreAffichage.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.CODE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeParametreAffichage.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOTypeParametreAffichage.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeParametreAffichage.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOTypeParametreAffichage.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(_EOTypeParametreAffichage.PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    if (_EOTypeParametreAffichage.LOG.isDebugEnabled()) {
    	_EOTypeParametreAffichage.LOG.debug( "updating persIdModifcation from " + persIdModifcation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeParametreAffichage.PERS_ID_MODIFCATION_KEY);
  }


  public static EOTypeParametreAffichage createSco_TypeParametreAffichage(EOEditingContext editingContext, String code
, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, Integer persIdCreation
, Integer persIdModifcation
) {
    EOTypeParametreAffichage eo = (EOTypeParametreAffichage) EOUtilities.createAndInsertInstance(editingContext, _EOTypeParametreAffichage.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModifcation(persIdModifcation);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeParametreAffichage> fetchSpec() {
    return new ERXFetchSpecification<EOTypeParametreAffichage>(_EOTypeParametreAffichage.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeParametreAffichage> fetchAllSco_TypeParametreAffichages(EOEditingContext editingContext) {
    return _EOTypeParametreAffichage.fetchAllSco_TypeParametreAffichages(editingContext, null);
  }

  public static NSArray<EOTypeParametreAffichage> fetchAllSco_TypeParametreAffichages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeParametreAffichage.fetchSco_TypeParametreAffichages(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeParametreAffichage> fetchSco_TypeParametreAffichages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeParametreAffichage> fetchSpec = new ERXFetchSpecification<EOTypeParametreAffichage>(_EOTypeParametreAffichage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeParametreAffichage> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeParametreAffichage fetchSco_TypeParametreAffichage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeParametreAffichage.fetchSco_TypeParametreAffichage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeParametreAffichage fetchSco_TypeParametreAffichage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeParametreAffichage> eoObjects = _EOTypeParametreAffichage.fetchSco_TypeParametreAffichages(editingContext, qualifier, null);
    EOTypeParametreAffichage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeParametreAffichage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeParametreAffichage fetchRequiredSco_TypeParametreAffichage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeParametreAffichage.fetchRequiredSco_TypeParametreAffichage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeParametreAffichage fetchRequiredSco_TypeParametreAffichage(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeParametreAffichage eoObject = _EOTypeParametreAffichage.fetchSco_TypeParametreAffichage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeParametreAffichage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeParametreAffichage localInstanceIn(EOEditingContext editingContext, EOTypeParametreAffichage eo) {
    EOTypeParametreAffichage localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
