package org.cocktail.fwkcktlscolpeda.serveur.metier;

/**
 * Type de la formation
 * 
 */
public interface ITypeFormation {
	String TF_LICENCE = "LIC";
	String TF_MASTER = "MAS";
	String TF_DOCTORAT = "DOC";
	String TF_LICENCE_PRO = "LIP";
	String TF_DUT = "DUT";	
	String TF_PRECENM = "   PRECENM ";
	String TF_PRECENS = " PRECENS";
	String TF_PREAGR = "  PREAGR ";
	String TF_PRECDIV = " PRECDIV";

	/**
	 * @return libelle du type de formation
	 */
	String libelle();

	/**
	 * @return code du type de formation
	 */
	String code();

	/**
	 * @return le type de formation Aglae
	 */
	ITypeFormationAglae typeFormationAglae();

}
