// DO NOT EDIT.  Make changes to EOPaiementMoyen.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPaiementMoyen extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PaiementMoyen";

  // Attribute Keys
  public static final ERXKey<String> BIC = new ERXKey<String>("bic");
  public static final ERXKey<String> CODE_BANQUE = new ERXKey<String>("codeBanque");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Boolean> ETUDIANT_TITULAIRE = new ERXKey<Boolean>("etudiantTitulaire");
  public static final ERXKey<String> IBAN = new ERXKey<String>("iban");
  public static final ERXKey<java.math.BigDecimal> MONTANT_PAYE = new ERXKey<java.math.BigDecimal>("montantPaye");
  public static final ERXKey<String> NOM_TIREUR = new ERXKey<String>("nomTireur");
  public static final ERXKey<String> NUMERO_CHEQUE = new ERXKey<String>("numeroCheque");
  public static final ERXKey<String> NUMERO_COMPTE = new ERXKey<String>("numeroCompte");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PRENOM_TIREUR = new ERXKey<String>("prenomTireur");
  public static final ERXKey<Integer> RIB_TITULAIRE_ID = new ERXKey<Integer>("ribTitulaireId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse> TO_ADRESSE_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOAdresse>("toAdresseTitulaire");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier> TO_ECHEANCIER_SEPA_SDD = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier>("toEcheancierSepaSdd");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis> TO_FOURNISSEUR_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOFournis>("toFournisseurTitulaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement> TO_MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement>("toModePaiement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement>("toPaiement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> TO_PAIEMENT_ECHEANCES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance>("toPaiementEcheances");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib> TO_RIB_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORib>("toRibTitulaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement> TO_TYPE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement>("toTypePaiement");

  // Attributes
  public static final String BIC_KEY = BIC.key();
  public static final String CODE_BANQUE_KEY = CODE_BANQUE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String ETUDIANT_TITULAIRE_KEY = ETUDIANT_TITULAIRE.key();
  public static final String IBAN_KEY = IBAN.key();
  public static final String MONTANT_PAYE_KEY = MONTANT_PAYE.key();
  public static final String NOM_TIREUR_KEY = NOM_TIREUR.key();
  public static final String NUMERO_CHEQUE_KEY = NUMERO_CHEQUE.key();
  public static final String NUMERO_COMPTE_KEY = NUMERO_COMPTE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String PRENOM_TIREUR_KEY = PRENOM_TIREUR.key();
  public static final String RIB_TITULAIRE_ID_KEY = RIB_TITULAIRE_ID.key();
  // Relationships
  public static final String TO_ADRESSE_TITULAIRE_KEY = TO_ADRESSE_TITULAIRE.key();
  public static final String TO_ECHEANCIER_SEPA_SDD_KEY = TO_ECHEANCIER_SEPA_SDD.key();
  public static final String TO_FOURNISSEUR_TITULAIRE_KEY = TO_FOURNISSEUR_TITULAIRE.key();
  public static final String TO_MODE_PAIEMENT_KEY = TO_MODE_PAIEMENT.key();
  public static final String TO_PAIEMENT_KEY = TO_PAIEMENT.key();
  public static final String TO_PAIEMENT_ECHEANCES_KEY = TO_PAIEMENT_ECHEANCES.key();
  public static final String TO_RIB_TITULAIRE_KEY = TO_RIB_TITULAIRE.key();
  public static final String TO_TYPE_PAIEMENT_KEY = TO_TYPE_PAIEMENT.key();

  private static Logger LOG = Logger.getLogger(_EOPaiementMoyen.class);

  public EOPaiementMoyen localInstanceIn(EOEditingContext editingContext) {
    EOPaiementMoyen localInstance = (EOPaiementMoyen)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String bic() {
    return (String) storedValueForKey(_EOPaiementMoyen.BIC_KEY);
  }

  public void setBic(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating bic from " + bic() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.BIC_KEY);
  }

  public String codeBanque() {
    return (String) storedValueForKey(_EOPaiementMoyen.CODE_BANQUE_KEY);
  }

  public void setCodeBanque(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating codeBanque from " + codeBanque() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.CODE_BANQUE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOPaiementMoyen.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOPaiementMoyen.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.D_MODIFICATION_KEY);
  }

  public Boolean etudiantTitulaire() {
    return (Boolean) storedValueForKey(_EOPaiementMoyen.ETUDIANT_TITULAIRE_KEY);
  }

  public void setEtudiantTitulaire(Boolean value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating etudiantTitulaire from " + etudiantTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.ETUDIANT_TITULAIRE_KEY);
  }

  public String iban() {
    return (String) storedValueForKey(_EOPaiementMoyen.IBAN_KEY);
  }

  public void setIban(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating iban from " + iban() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.IBAN_KEY);
  }

  public java.math.BigDecimal montantPaye() {
    return (java.math.BigDecimal) storedValueForKey(_EOPaiementMoyen.MONTANT_PAYE_KEY);
  }

  public void setMontantPaye(java.math.BigDecimal value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating montantPaye from " + montantPaye() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.MONTANT_PAYE_KEY);
  }

  public String nomTireur() {
    return (String) storedValueForKey(_EOPaiementMoyen.NOM_TIREUR_KEY);
  }

  public void setNomTireur(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating nomTireur from " + nomTireur() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.NOM_TIREUR_KEY);
  }

  public String numeroCheque() {
    return (String) storedValueForKey(_EOPaiementMoyen.NUMERO_CHEQUE_KEY);
  }

  public void setNumeroCheque(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating numeroCheque from " + numeroCheque() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.NUMERO_CHEQUE_KEY);
  }

  public String numeroCompte() {
    return (String) storedValueForKey(_EOPaiementMoyen.NUMERO_COMPTE_KEY);
  }

  public void setNumeroCompte(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating numeroCompte from " + numeroCompte() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.NUMERO_COMPTE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOPaiementMoyen.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOPaiementMoyen.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.PERS_ID_MODIFICATION_KEY);
  }

  public String prenomTireur() {
    return (String) storedValueForKey(_EOPaiementMoyen.PRENOM_TIREUR_KEY);
  }

  public void setPrenomTireur(String value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating prenomTireur from " + prenomTireur() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.PRENOM_TIREUR_KEY);
  }

  public Integer ribTitulaireId() {
    return (Integer) storedValueForKey(_EOPaiementMoyen.RIB_TITULAIRE_ID_KEY);
  }

  public void setRibTitulaireId(Integer value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
    	_EOPaiementMoyen.LOG.debug( "updating ribTitulaireId from " + ribTitulaireId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiementMoyen.RIB_TITULAIRE_ID_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toAdresseTitulaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey(_EOPaiementMoyen.TO_ADRESSE_TITULAIRE_KEY);
  }
  
  public void setToAdresseTitulaire(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_ADRESSE_TITULAIRE_KEY);
  }

  public void setToAdresseTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toAdresseTitulaire from " + toAdresseTitulaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAdresseTitulaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toAdresseTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_ADRESSE_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_ADRESSE_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier toEcheancierSepaSdd() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier)storedValueForKey(_EOPaiementMoyen.TO_ECHEANCIER_SEPA_SDD_KEY);
  }
  
  public void setToEcheancierSepaSdd(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_ECHEANCIER_SEPA_SDD_KEY);
  }

  public void setToEcheancierSepaSddRelationship(org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toEcheancierSepaSdd from " + toEcheancierSepaSdd() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEcheancierSepaSdd(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier oldValue = toEcheancierSepaSdd();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_ECHEANCIER_SEPA_SDD_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_ECHEANCIER_SEPA_SDD_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOFournis toFournisseurTitulaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOFournis)storedValueForKey(_EOPaiementMoyen.TO_FOURNISSEUR_TITULAIRE_KEY);
  }
  
  public void setToFournisseurTitulaire(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_FOURNISSEUR_TITULAIRE_KEY);
  }

  public void setToFournisseurTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOFournis value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toFournisseurTitulaire from " + toFournisseurTitulaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToFournisseurTitulaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOFournis oldValue = toFournisseurTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_FOURNISSEUR_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_FOURNISSEUR_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement toModePaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement)storedValueForKey(_EOPaiementMoyen.TO_MODE_PAIEMENT_KEY);
  }
  
  public void setToModePaiement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_MODE_PAIEMENT_KEY);
  }

  public void setToModePaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toModePaiement from " + toModePaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToModePaiement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement oldValue = toModePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement toPaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement)storedValueForKey(_EOPaiementMoyen.TO_PAIEMENT_KEY);
  }
  
  public void setToPaiement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_PAIEMENT_KEY);
  }

  public void setToPaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toPaiement from " + toPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement oldValue = toPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORib toRibTitulaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORib)storedValueForKey(_EOPaiementMoyen.TO_RIB_TITULAIRE_KEY);
  }
  
  public void setToRibTitulaire(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_RIB_TITULAIRE_KEY);
  }

  public void setToRibTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORib value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toRibTitulaire from " + toRibTitulaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRibTitulaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORib oldValue = toRibTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_RIB_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_RIB_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement toTypePaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement)storedValueForKey(_EOPaiementMoyen.TO_TYPE_PAIEMENT_KEY);
  }
  
  public void setToTypePaiement(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement value) {
    takeStoredValueForKey(value, _EOPaiementMoyen.TO_TYPE_PAIEMENT_KEY);
  }

  public void setToTypePaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement value) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("updating toTypePaiement from " + toTypePaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypePaiement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement oldValue = toTypePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiementMoyen.TO_TYPE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiementMoyen.TO_TYPE_PAIEMENT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> toPaiementEcheances() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance>)storedValueForKey(_EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> toPaiementEcheances(EOQualifier qualifier) {
    return toPaiementEcheances(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> toPaiementEcheances(EOQualifier qualifier, boolean fetch) {
    return toPaiementEcheances(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> toPaiementEcheances(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance.TO_PAIEMENT_MOYEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance.fetchSco_PaiementEcheances(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPaiementEcheances();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPaiementEcheances(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance object) {
    includeObjectIntoPropertyWithKey(object, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
  }

  public void removeFromToPaiementEcheances(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance object) {
    excludeObjectFromPropertyWithKey(object, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
  }

  public void addToToPaiementEcheancesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance object) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("adding " + object + " to toPaiementEcheances relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPaiementEcheances(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
    }
  }

  public void removeFromToPaiementEcheancesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance object) {
    if (_EOPaiementMoyen.LOG.isDebugEnabled()) {
      _EOPaiementMoyen.LOG.debug("removing " + object + " from toPaiementEcheances relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPaiementEcheances(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance createToPaiementEcheancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance) eo;
  }

  public void deleteToPaiementEcheancesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiementMoyen.TO_PAIEMENT_ECHEANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPaiementEcheancesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance> objects = toPaiementEcheances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPaiementEcheancesRelationship(objects.nextElement());
    }
  }


  public static EOPaiementMoyen createSco_PaiementMoyen(EOEditingContext editingContext, NSTimestamp dCreation
, Boolean etudiantTitulaire
, java.math.BigDecimal montantPaye
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement toModePaiement, org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement toPaiement, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement toTypePaiement) {
    EOPaiementMoyen eo = (EOPaiementMoyen) EOUtilities.createAndInsertInstance(editingContext, _EOPaiementMoyen.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setEtudiantTitulaire(etudiantTitulaire);
		eo.setMontantPaye(montantPaye);
		eo.setPersIdCreation(persIdCreation);
    eo.setToModePaiementRelationship(toModePaiement);
    eo.setToPaiementRelationship(toPaiement);
    eo.setToTypePaiementRelationship(toTypePaiement);
    return eo;
  }

  public static ERXFetchSpecification<EOPaiementMoyen> fetchSpec() {
    return new ERXFetchSpecification<EOPaiementMoyen>(_EOPaiementMoyen.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPaiementMoyen> fetchAllSco_PaiementMoyens(EOEditingContext editingContext) {
    return _EOPaiementMoyen.fetchAllSco_PaiementMoyens(editingContext, null);
  }

  public static NSArray<EOPaiementMoyen> fetchAllSco_PaiementMoyens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPaiementMoyen.fetchSco_PaiementMoyens(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPaiementMoyen> fetchSco_PaiementMoyens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPaiementMoyen> fetchSpec = new ERXFetchSpecification<EOPaiementMoyen>(_EOPaiementMoyen.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPaiementMoyen> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPaiementMoyen fetchSco_PaiementMoyen(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementMoyen.fetchSco_PaiementMoyen(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementMoyen fetchSco_PaiementMoyen(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPaiementMoyen> eoObjects = _EOPaiementMoyen.fetchSco_PaiementMoyens(editingContext, qualifier, null);
    EOPaiementMoyen eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PaiementMoyen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementMoyen fetchRequiredSco_PaiementMoyen(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiementMoyen.fetchRequiredSco_PaiementMoyen(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiementMoyen fetchRequiredSco_PaiementMoyen(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPaiementMoyen eoObject = _EOPaiementMoyen.fetchSco_PaiementMoyen(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PaiementMoyen that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiementMoyen localInstanceIn(EOEditingContext editingContext, EOPaiementMoyen eo) {
    EOPaiementMoyen localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
