// DO NOT EDIT.  Make changes to EOExamenOrganisation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOExamenOrganisation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ExamenOrganisation";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_EXAMEN = new ERXKey<NSTimestamp>("dateExamen");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> ID_EXAMEN_ORGANISATION = new ERXKey<Integer>("idExamenOrganisation");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> SALLE_EXAMEN = new ERXKey<String>("salleExamen");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen> TO_EXAMEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen>("toExamen");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> TO_EXAMEN_ETUDIANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant>("toExamenEtudiants");

  // Attributes
  public static final String DATE_EXAMEN_KEY = DATE_EXAMEN.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String ID_EXAMEN_ORGANISATION_KEY = ID_EXAMEN_ORGANISATION.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SALLE_EXAMEN_KEY = SALLE_EXAMEN.key();
  // Relationships
  public static final String TO_EXAMEN_KEY = TO_EXAMEN.key();
  public static final String TO_EXAMEN_ETUDIANTS_KEY = TO_EXAMEN_ETUDIANTS.key();

  private static Logger LOG = Logger.getLogger(_EOExamenOrganisation.class);

  public EOExamenOrganisation localInstanceIn(EOEditingContext editingContext) {
    EOExamenOrganisation localInstance = (EOExamenOrganisation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateExamen() {
    return (NSTimestamp) storedValueForKey(_EOExamenOrganisation.DATE_EXAMEN_KEY);
  }

  public void setDateExamen(NSTimestamp value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating dateExamen from " + dateExamen() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.DATE_EXAMEN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOExamenOrganisation.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOExamenOrganisation.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.D_MODIFICATION_KEY);
  }

  public Integer idExamenOrganisation() {
    return (Integer) storedValueForKey(_EOExamenOrganisation.ID_EXAMEN_ORGANISATION_KEY);
  }

  public void setIdExamenOrganisation(Integer value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating idExamenOrganisation from " + idExamenOrganisation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.ID_EXAMEN_ORGANISATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOExamenOrganisation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOExamenOrganisation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.PERS_ID_MODIFICATION_KEY);
  }

  public String salleExamen() {
    return (String) storedValueForKey(_EOExamenOrganisation.SALLE_EXAMEN_KEY);
  }

  public void setSalleExamen(String value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
    	_EOExamenOrganisation.LOG.debug( "updating salleExamen from " + salleExamen() + " to " + value);
    }
    takeStoredValueForKey(value, _EOExamenOrganisation.SALLE_EXAMEN_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen toExamen() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen)storedValueForKey(_EOExamenOrganisation.TO_EXAMEN_KEY);
  }
  
  public void setToExamen(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen value) {
    takeStoredValueForKey(value, _EOExamenOrganisation.TO_EXAMEN_KEY);
  }

  public void setToExamenRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen value) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
      _EOExamenOrganisation.LOG.debug("updating toExamen from " + toExamen() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToExamen(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen oldValue = toExamen();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOExamenOrganisation.TO_EXAMEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOExamenOrganisation.TO_EXAMEN_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> toExamenEtudiants() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant>)storedValueForKey(_EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> toExamenEtudiants(EOQualifier qualifier) {
    return toExamenEtudiants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> toExamenEtudiants(EOQualifier qualifier, boolean fetch) {
    return toExamenEtudiants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> toExamenEtudiants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant.TO_EXAMEN_ORGANISATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant.fetchSco_ExamenEtudiants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toExamenEtudiants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExamenEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant object) {
    includeObjectIntoPropertyWithKey(object, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
  }

  public void removeFromToExamenEtudiants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant object) {
    excludeObjectFromPropertyWithKey(object, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
  }

  public void addToToExamenEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant object) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
      _EOExamenOrganisation.LOG.debug("adding " + object + " to toExamenEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToExamenEtudiants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
    }
  }

  public void removeFromToExamenEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant object) {
    if (_EOExamenOrganisation.LOG.isDebugEnabled()) {
      _EOExamenOrganisation.LOG.debug("removing " + object + " from toExamenEtudiants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToExamenEtudiants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant createToExamenEtudiantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant) eo;
  }

  public void deleteToExamenEtudiantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOExamenOrganisation.TO_EXAMEN_ETUDIANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExamenEtudiantsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamenEtudiant> objects = toExamenEtudiants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExamenEtudiantsRelationship(objects.nextElement());
    }
  }


  public static EOExamenOrganisation createSco_ExamenOrganisation(EOEditingContext editingContext, NSTimestamp dCreation
, Integer idExamenOrganisation
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOExamen toExamen) {
    EOExamenOrganisation eo = (EOExamenOrganisation) EOUtilities.createAndInsertInstance(editingContext, _EOExamenOrganisation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setIdExamenOrganisation(idExamenOrganisation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToExamenRelationship(toExamen);
    return eo;
  }

  public static ERXFetchSpecification<EOExamenOrganisation> fetchSpec() {
    return new ERXFetchSpecification<EOExamenOrganisation>(_EOExamenOrganisation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOExamenOrganisation> fetchAllSco_ExamenOrganisations(EOEditingContext editingContext) {
    return _EOExamenOrganisation.fetchAllSco_ExamenOrganisations(editingContext, null);
  }

  public static NSArray<EOExamenOrganisation> fetchAllSco_ExamenOrganisations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOExamenOrganisation.fetchSco_ExamenOrganisations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOExamenOrganisation> fetchSco_ExamenOrganisations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOExamenOrganisation> fetchSpec = new ERXFetchSpecification<EOExamenOrganisation>(_EOExamenOrganisation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOExamenOrganisation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOExamenOrganisation fetchSco_ExamenOrganisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenOrganisation.fetchSco_ExamenOrganisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenOrganisation fetchSco_ExamenOrganisation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOExamenOrganisation> eoObjects = _EOExamenOrganisation.fetchSco_ExamenOrganisations(editingContext, qualifier, null);
    EOExamenOrganisation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ExamenOrganisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenOrganisation fetchRequiredSco_ExamenOrganisation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOExamenOrganisation.fetchRequiredSco_ExamenOrganisation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOExamenOrganisation fetchRequiredSco_ExamenOrganisation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOExamenOrganisation eoObject = _EOExamenOrganisation.fetchSco_ExamenOrganisation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ExamenOrganisation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOExamenOrganisation localInstanceIn(EOEditingContext editingContext, EOExamenOrganisation eo) {
    EOExamenOrganisation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
