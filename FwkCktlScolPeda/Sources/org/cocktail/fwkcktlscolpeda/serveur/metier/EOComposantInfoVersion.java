package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Informations de version liées à un composant
 */
public class EOComposantInfoVersion extends _EOComposantInfoVersion implements IComposantInfoVersion {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOComposantInfoVersion.class);

	/**
	 * @param editingContext contexte d'édition
	 * @param anneeVersion annee de la version
	 * @param numeroVersionMajeur numero majeur de la version
	 * @return les informations de versions d'un composant
	 */
	public static EOComposantInfoVersion create(EOEditingContext editingContext, Integer anneeVersion, Integer numeroVersionMajeur) {
		EOComposantInfoVersion composantInfoVersion = (EOComposantInfoVersion) EOUtilities.createAndInsertInstance(editingContext,
		    EOComposantInfoVersion.ENTITY_NAME);
		composantInfoVersion.setAnnee(anneeVersion);
		composantInfoVersion.setVersionMajeure(numeroVersionMajeur);
		return composantInfoVersion;
	}
	
	/**
	 * Suppression de l'info version d'un composant.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		try {
			this.delete();
		} catch (Exception e) {
			System.err.println("Erreur suppression info version : " + e.getMessage());
			throw e;
		}
	}

}
