// DO NOT EDIT.  Make changes to EOComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Composant";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> COMPOSANT_ORIGINE_ID = new ERXKey<Integer>("composantOrigineId");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<Integer> NO_CNU = new ERXKey<Integer>("noCnu");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> SYS_ARCHIVE = new ERXKey<Boolean>("sysArchive");
  public static final ERXKey<String> TAG_APPLICATION = new ERXKey<String>("tagApplication");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion> COMPOSANT_INFO_VERSION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion>("composantInfoVersion");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT_ORIGINE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantOrigine");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANTS_SUIVANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composantsSuivants");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> DISCIPLINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>("disciplines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> DOMAINES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>("domaines");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> DOMAINES_COMPOSANTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>("domainesComposants");
  public static final ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> IMPLANTATIONS_GEOS = new ERXKey<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>("implantationsGeos");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> LANGUES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>("langues");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_CHILDS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensChilds");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> LIENS_PARENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("liensParents");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLES_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsablesComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structures");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu> TO_CNU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCnu>("toCnu");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String COMPOSANT_ORIGINE_ID_KEY = COMPOSANT_ORIGINE_ID.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String NO_CNU_KEY = NO_CNU.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String SYS_ARCHIVE_KEY = SYS_ARCHIVE.key();
  public static final String TAG_APPLICATION_KEY = TAG_APPLICATION.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  // Relationships
  public static final String COMPOSANT_INFO_VERSION_KEY = COMPOSANT_INFO_VERSION.key();
  public static final String COMPOSANT_ORIGINE_KEY = COMPOSANT_ORIGINE.key();
  public static final String COMPOSANTS_SUIVANTS_KEY = COMPOSANTS_SUIVANTS.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String DISCIPLINES_KEY = DISCIPLINES.key();
  public static final String DOMAINES_KEY = DOMAINES.key();
  public static final String DOMAINES_COMPOSANTS_KEY = DOMAINES_COMPOSANTS.key();
  public static final String IMPLANTATIONS_GEOS_KEY = IMPLANTATIONS_GEOS.key();
  public static final String LANGUES_KEY = LANGUES.key();
  public static final String LIENS_CHILDS_KEY = LIENS_CHILDS.key();
  public static final String LIENS_PARENTS_KEY = LIENS_PARENTS.key();
  public static final String RESPONSABLES_COMPOSANT_KEY = RESPONSABLES_COMPOSANT.key();
  public static final String SESSIONS_KEY = SESSIONS.key();
  public static final String STRUCTURES_KEY = STRUCTURES.key();
  public static final String TO_CNU_KEY = TO_CNU.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();

  private static Logger LOG = Logger.getLogger(_EOComposant.class);

  public EOComposant localInstanceIn(EOEditingContext editingContext) {
    EOComposant localInstance = (EOComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOComposant.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.CODE_KEY);
  }

  public Integer composantOrigineId() {
    return (Integer) storedValueForKey(_EOComposant.COMPOSANT_ORIGINE_ID_KEY);
  }

  public void setComposantOrigineId(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating composantOrigineId from " + composantOrigineId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.COMPOSANT_ORIGINE_ID_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOComposant.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOComposant.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.DATE_MODIFICATION_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOComposant.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOComposant.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.LIBELLE_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EOComposant.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.LIBELLE_COURT_KEY);
  }

  public Integer noCnu() {
    return (Integer) storedValueForKey(_EOComposant.NO_CNU_KEY);
  }

  public void setNoCnu(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating noCnu from " + noCnu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.NO_CNU_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOComposant.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOComposant.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.PERS_ID_MODIFICATION_KEY);
  }

  public Boolean sysArchive() {
    return (Boolean) storedValueForKey(_EOComposant.SYS_ARCHIVE_KEY);
  }

  public void setSysArchive(Boolean value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating sysArchive from " + sysArchive() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.SYS_ARCHIVE_KEY);
  }

  public String tagApplication() {
    return (String) storedValueForKey(_EOComposant.TAG_APPLICATION_KEY);
  }

  public void setTagApplication(String value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating tagApplication from " + tagApplication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.TAG_APPLICATION_KEY);
  }

  public Integer typeComposantId() {
    return (Integer) storedValueForKey(_EOComposant.TYPE_COMPOSANT_ID_KEY);
  }

  public void setTypeComposantId(Integer value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
    	_EOComposant.LOG.debug( "updating typeComposantId from " + typeComposantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOComposant.TYPE_COMPOSANT_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion composantInfoVersion() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion)storedValueForKey(_EOComposant.COMPOSANT_INFO_VERSION_KEY);
  }
  
  public void setComposantInfoVersion(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion value) {
    takeStoredValueForKey(value, _EOComposant.COMPOSANT_INFO_VERSION_KEY);
  }

  public void setComposantInfoVersionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("updating composantInfoVersion from " + composantInfoVersion() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposantInfoVersion(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion oldValue = composantInfoVersion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOComposant.COMPOSANT_INFO_VERSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOComposant.COMPOSANT_INFO_VERSION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composantOrigine() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOComposant.COMPOSANT_ORIGINE_KEY);
  }
  
  public void setComposantOrigine(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOComposant.COMPOSANT_ORIGINE_KEY);
  }

  public void setComposantOrigineRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("updating composantOrigine from " + composantOrigine() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposantOrigine(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composantOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOComposant.COMPOSANT_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOComposant.COMPOSANT_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCnu toCnu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCnu)storedValueForKey(_EOComposant.TO_CNU_KEY);
  }
  
  public void setToCnu(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    takeStoredValueForKey(value, _EOComposant.TO_CNU_KEY);
  }

  public void setToCnuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCnu value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("updating toCnu from " + toCnu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToCnu(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOComposant.TO_CNU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOComposant.TO_CNU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant typeComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant)storedValueForKey(_EOComposant.TYPE_COMPOSANT_KEY);
  }
  
  public void setTypeComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    takeStoredValueForKey(value, _EOComposant.TYPE_COMPOSANT_KEY);
  }

  public void setTypeComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("updating typeComposant from " + typeComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant oldValue = typeComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOComposant.TYPE_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOComposant.TYPE_COMPOSANT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> composantsSuivants() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>)storedValueForKey(_EOComposant.COMPOSANTS_SUIVANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> composantsSuivants(EOQualifier qualifier) {
    return composantsSuivants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> composantsSuivants(EOQualifier qualifier, boolean fetch) {
    return composantsSuivants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> composantsSuivants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant.COMPOSANT_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant.fetchSco_Composants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = composantsSuivants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToComposantsSuivants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
  }

  public void removeFromComposantsSuivants(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
  }

  public void addToComposantsSuivantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to composantsSuivants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToComposantsSuivants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
    }
  }

  public void removeFromComposantsSuivantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from composantsSuivants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromComposantsSuivants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant createComposantsSuivantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant) eo;
  }

  public void deleteComposantsSuivantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.COMPOSANTS_SUIVANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllComposantsSuivantsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> objects = composantsSuivants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteComposantsSuivantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)storedValueForKey(_EOComposant.CREDITABLES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier) {
    return creditables(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier, boolean fetch) {
    return creditables(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> creditables(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.fetchSco_Creditables(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = creditables();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCreditables(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.CREDITABLES_KEY);
  }

  public void removeFromCreditables(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.CREDITABLES_KEY);
  }

  public void addToCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to creditables relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToCreditables(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.CREDITABLES_KEY);
    }
  }

  public void removeFromCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from creditables relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromCreditables(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.CREDITABLES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable createCreditablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.CREDITABLES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable) eo;
  }

  public void deleteCreditablesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.CREDITABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCreditablesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> objects = creditables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCreditablesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)storedValueForKey(_EOComposant.DESCRIPTIFS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs(EOQualifier qualifier) {
    return descriptifs(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs(EOQualifier qualifier, boolean fetch) {
    return descriptifs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> descriptifs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif.COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif.fetchSco_Descriptifs(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = descriptifs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDescriptifs(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.DESCRIPTIFS_KEY);
  }

  public void removeFromDescriptifs(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.DESCRIPTIFS_KEY);
  }

  public void addToDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to descriptifs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDescriptifs(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.DESCRIPTIFS_KEY);
    }
  }

  public void removeFromDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from descriptifs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDescriptifs(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DESCRIPTIFS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif createDescriptifsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.DESCRIPTIFS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif) eo;
  }

  public void deleteDescriptifsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DESCRIPTIFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDescriptifsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> objects = descriptifs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDescriptifsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> disciplines() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)storedValueForKey(_EOComposant.DISCIPLINES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> disciplines(EOQualifier qualifier) {
    return disciplines(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> disciplines(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> results;
      results = disciplines();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDisciplines(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.DISCIPLINES_KEY);
  }

  public void removeFromDisciplines(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.DISCIPLINES_KEY);
  }

  public void addToDisciplinesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to disciplines relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDisciplines(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.DISCIPLINES_KEY);
    }
  }

  public void removeFromDisciplinesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from disciplines relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDisciplines(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DISCIPLINES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline createDisciplinesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.DISCIPLINES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline) eo;
  }

  public void deleteDisciplinesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DISCIPLINES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDisciplinesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiscipline> objects = disciplines().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDisciplinesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> domaines() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>)storedValueForKey(_EOComposant.DOMAINES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> domaines(EOQualifier qualifier) {
    return domaines(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> domaines(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> results;
      results = domaines();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDomaines(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.DOMAINES_KEY);
  }

  public void removeFromDomaines(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.DOMAINES_KEY);
  }

  public void addToDomainesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to domaines relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDomaines(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_KEY);
    }
  }

  public void removeFromDomainesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from domaines relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDomaines(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine createDomainesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.DOMAINES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine) eo;
  }

  public void deleteDomainesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDomainesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaine> objects = domaines().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDomainesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> domainesComposants() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>)storedValueForKey(_EOComposant.DOMAINES_COMPOSANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> domainesComposants(EOQualifier qualifier) {
    return domainesComposants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> domainesComposants(EOQualifier qualifier, boolean fetch) {
    return domainesComposants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> domainesComposants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant.COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant.fetchSco_DomaineComposants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = domainesComposants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDomainesComposants(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.DOMAINES_COMPOSANTS_KEY);
  }

  public void removeFromDomainesComposants(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.DOMAINES_COMPOSANTS_KEY);
  }

  public void addToDomainesComposantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to domainesComposants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToDomainesComposants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_COMPOSANTS_KEY);
    }
  }

  public void removeFromDomainesComposantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from domainesComposants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromDomainesComposants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_COMPOSANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant createDomainesComposantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.DOMAINES_COMPOSANTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant) eo;
  }

  public void deleteDomainesComposantsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.DOMAINES_COMPOSANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDomainesComposantsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EODomaineComposant> objects = domainesComposants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDomainesComposantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> implantationsGeos() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>)storedValueForKey(_EOComposant.IMPLANTATIONS_GEOS_KEY);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> implantationsGeos(EOQualifier qualifier) {
    return implantationsGeos(qualifier, null);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> implantationsGeos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> results;
      results = implantationsGeos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToImplantationsGeos(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.IMPLANTATIONS_GEOS_KEY);
  }

  public void removeFromImplantationsGeos(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.IMPLANTATIONS_GEOS_KEY);
  }

  public void addToImplantationsGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to implantationsGeos relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToImplantationsGeos(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.IMPLANTATIONS_GEOS_KEY);
    }
  }

  public void removeFromImplantationsGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from implantationsGeos relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromImplantationsGeos(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.IMPLANTATIONS_GEOS_KEY);
    }
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo createImplantationsGeosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.IMPLANTATIONS_GEOS_KEY);
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo) eo;
  }

  public void deleteImplantationsGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.IMPLANTATIONS_GEOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllImplantationsGeosRelationships() {
    Enumeration<org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo> objects = implantationsGeos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteImplantationsGeosRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> langues() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>)storedValueForKey(_EOComposant.LANGUES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> langues(EOQualifier qualifier) {
    return langues(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> langues(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> results;
      results = langues();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOLangue>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToLangues(org.cocktail.fwkcktlpersonne.common.metier.EOLangue object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.LANGUES_KEY);
  }

  public void removeFromLangues(org.cocktail.fwkcktlpersonne.common.metier.EOLangue object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.LANGUES_KEY);
  }

  public void addToLanguesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOLangue object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to langues relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToLangues(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.LANGUES_KEY);
    }
  }

  public void removeFromLanguesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOLangue object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from langues relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromLangues(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LANGUES_KEY);
    }
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOLangue createLanguesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlpersonne.common.metier.EOLangue.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.LANGUES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOLangue) eo;
  }

  public void deleteLanguesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOLangue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LANGUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLanguesRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOLangue> objects = langues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLanguesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensChilds() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)storedValueForKey(_EOComposant.LIENS_CHILDS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensChilds(EOQualifier qualifier) {
    return liensChilds(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensChilds(EOQualifier qualifier, boolean fetch) {
    return liensChilds(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensChilds(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.CHILD_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.fetchSco_Liens(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = liensChilds();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToLiensChilds(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.LIENS_CHILDS_KEY);
  }

  public void removeFromLiensChilds(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.LIENS_CHILDS_KEY);
  }

  public void addToLiensChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to liensChilds relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToLiensChilds(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_CHILDS_KEY);
    }
  }

  public void removeFromLiensChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from liensChilds relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromLiensChilds(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_CHILDS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien createLiensChildsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.LIENS_CHILDS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien) eo;
  }

  public void deleteLiensChildsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_CHILDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLiensChildsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> objects = liensChilds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLiensChildsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensParents() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)storedValueForKey(_EOComposant.LIENS_PARENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensParents(EOQualifier qualifier) {
    return liensParents(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensParents(EOQualifier qualifier, boolean fetch) {
    return liensParents(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> liensParents(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.PARENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.fetchSco_Liens(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = liensParents();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToLiensParents(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.LIENS_PARENTS_KEY);
  }

  public void removeFromLiensParents(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.LIENS_PARENTS_KEY);
  }

  public void addToLiensParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to liensParents relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToLiensParents(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_PARENTS_KEY);
    }
  }

  public void removeFromLiensParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from liensParents relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromLiensParents(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_PARENTS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien createLiensParentsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.LIENS_PARENTS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien) eo;
  }

  public void deleteLiensParentsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.LIENS_PARENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLiensParentsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> objects = liensParents().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLiensParentsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> responsablesComposant() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>)storedValueForKey(_EOComposant.RESPONSABLES_COMPOSANT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> responsablesComposant(EOQualifier qualifier) {
    return responsablesComposant(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> responsablesComposant(EOQualifier qualifier, boolean fetch) {
    return responsablesComposant(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> responsablesComposant(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant.COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant.fetchSco_ResponsableComposants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = responsablesComposant();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToResponsablesComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
  }

  public void removeFromResponsablesComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
  }

  public void addToResponsablesComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to responsablesComposant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToResponsablesComposant(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
    }
  }

  public void removeFromResponsablesComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from responsablesComposant relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromResponsablesComposant(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant createResponsablesComposantRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant) eo;
  }

  public void deleteResponsablesComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.RESPONSABLES_COMPOSANT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllResponsablesComposantRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> objects = responsablesComposant().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteResponsablesComposantRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> sessions() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>)storedValueForKey(_EOComposant.SESSIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> sessions(EOQualifier qualifier) {
    return sessions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> sessions(EOQualifier qualifier, boolean fetch) {
    return sessions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> sessions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant.COMPOSANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant.fetchSco_SessionComposants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sessions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSessions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.SESSIONS_KEY);
  }

  public void removeFromSessions(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.SESSIONS_KEY);
  }

  public void addToSessionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to sessions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToSessions(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.SESSIONS_KEY);
    }
  }

  public void removeFromSessionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from sessions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromSessions(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.SESSIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant createSessionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.SESSIONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant) eo;
  }

  public void deleteSessionsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.SESSIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSessionsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> objects = sessions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSessionsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> structures() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)storedValueForKey(_EOComposant.STRUCTURES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> structures(EOQualifier qualifier) {
    return structures(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> structures(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> results;
      results = structures();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToStructures(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    includeObjectIntoPropertyWithKey(object, _EOComposant.STRUCTURES_KEY);
  }

  public void removeFromStructures(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    excludeObjectFromPropertyWithKey(object, _EOComposant.STRUCTURES_KEY);
  }

  public void addToStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("adding " + object + " to structures relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToStructures(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOComposant.STRUCTURES_KEY);
    }
  }

  public void removeFromStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    if (_EOComposant.LOG.isDebugEnabled()) {
      _EOComposant.LOG.debug("removing " + object + " from structures relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromStructures(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.STRUCTURES_KEY);
    }
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure createStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlpersonne.common.metier.EOStructure.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOComposant.STRUCTURES_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) eo;
  }

  public void deleteStructuresRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOComposant.STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllStructuresRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> objects = structures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteStructuresRelationship(objects.nextElement());
    }
  }


  public static EOComposant createSco_Composant(EOEditingContext editingContext, Integer id
, String libelle
) {
    EOComposant eo = (EOComposant) EOUtilities.createAndInsertInstance(editingContext, _EOComposant.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOComposant> fetchSpec() {
    return new ERXFetchSpecification<EOComposant>(_EOComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOComposant> fetchAllSco_Composants(EOEditingContext editingContext) {
    return _EOComposant.fetchAllSco_Composants(editingContext, null);
  }

  public static NSArray<EOComposant> fetchAllSco_Composants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOComposant.fetchSco_Composants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOComposant> fetchSco_Composants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOComposant> fetchSpec = new ERXFetchSpecification<EOComposant>(_EOComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOComposant fetchSco_Composant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOComposant.fetchSco_Composant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOComposant fetchSco_Composant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOComposant> eoObjects = _EOComposant.fetchSco_Composants(editingContext, qualifier, null);
    EOComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Composant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOComposant fetchRequiredSco_Composant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOComposant.fetchRequiredSco_Composant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOComposant fetchRequiredSco_Composant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOComposant eoObject = _EOComposant.fetchSco_Composant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Composant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOComposant localInstanceIn(EOEditingContext editingContext, EOComposant eo) {
    EOComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
