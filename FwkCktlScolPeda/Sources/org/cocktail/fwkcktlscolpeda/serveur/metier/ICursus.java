package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;

import com.webobjects.foundation.NSTimestamp;

/**
 *  Element d'un cursus d'études
 */
public interface ICursus {
	/**
	 * @return année de début de l'élément de cursus
	 */
	Integer anneeDebut();

	/**
	 * @param value année de début de l'élément de cursus
	 */
	void setAnneeDebut(Integer value);

	/**
	 * @return année de fin de l'élément de cursus
	 */
	Integer anneeFin();

	/**
	 * @param value année de début de l'élément de cursus
	 */
	void setAnneeFin(Integer value);

	/**
	 * @return date de création
	 */
	NSTimestamp dCreation();

	/**
	 * @param value date de création
	 */
	void setDCreation(NSTimestamp value);

	/**
	 * @return intitulé du diplome
	 */
	String diplome();

	/**
	 * @param value intitulé du diplome
	 */
	void setDiplome(String value);

	/**
	 * @return date de modification
	 */
	NSTimestamp dModifcation();

	/**
	 * @param value date de modification
	 */
	void setDModifcation(NSTimestamp value);

	/**
	 * @return titre de la formation
	 */
	String formation();

	/**
	 * @param value titre de la formation
	 */
	void setFormation(String value);

	/**
	 * @return vrai si il y a eu interruption d'études pendant cet élément de cursus
	 */
	Boolean interruptionEtud();

	/**
	 * @param value vrai si il y a eu interruption d'études pendant cet élément de cursus
	 */
	void setInterruptionEtud(Boolean value);

	/**
	 * @return niveau de l'élément de cursus
	 */
	Integer niveau();

	/**
	 * @param value niveau de l'élément de cursus
	 */
	void setNiveau(Integer value);

	/**
	 * @return persid de la personne qui a créé l'enregistrement
	 */
	Integer persIdCreation();

	/**
	 * @param value persid de la personne qui a créé l'enregistrement
	 */
	void setPersIdCreation(Integer value);

	/**
	 * @return persid de la personne qui a mis à jour l'enregistrement
	 */
	Integer persIdMofication();

	/**
	 * @param value persid de la personne qui a mis à jour l'enregistrement
	 */
	void setPersIdMofication(Integer value);

	/**
	 * @return type d'inscription
	 */
	String typeInscription();

	/**
	 * @param value type d'inscription
	 */
	void setTypeInscription(String value);

	/**
	 * @return mention du titre d'accès
	 */
	EOMention toAccesMention();

	/**
	 * @param value mention du titre d'accès
	 */
	void setToAccesMentionRelationship(EOMention value);

	/**
	 * @return grade universitaire du titre d'accès
	 */
	IGradeUniversitaire toAccesTitreGradeUniversitaire();

	/**
	 * @param value grade universitaire du titre d'accès
	 */
	void setToAccesTitreGradeUniversitaireRelationship(IGradeUniversitaire value);

	/**
	 * @return code RNE de l'établissement
	 */
	IRne toCRne();

	/**
	 * @param value code RNE de l'établissement
	 */
	void setToCRneRelationship(IRne value);

	/**
	 * @return grade universitaire
	 */
	IGradeUniversitaire toGradeUniversitaire();

	/**
	 * @param value grade universitaire
	 */
	void setToGradeUniversitaireRelationship(IGradeUniversitaire value);

	/**
	 * @return type de formation
	 */
	ITypeFormation toTypeFormation();

	/**
	 * @param value type de formation
	 */
	void setToTypeFormationRelationship(ITypeFormation value);
	
	/**
	 * 
	 * @return nom de l'étblissement si etranger
	 */
	String etabEtranger();

	/**
	 * 
	 * @param value nom de l'étblissement si etranger
	 */
	void setEtabEtranger(String value); 

	/**
	 * 
	 * @return le pays
	 */
	IPays toPays();
	
	/**
	 * 
	 * @param value le pays
	 */
	void setToPaysRelationship(IPays value);
	
	/**
	 * 
	 * @return la ville
	 */
	String ville();

	/**
	 * 
	 * @param value la ville 
	 */
	void setVille(String value);

	/**
	 * 
	 * @param codeRneEtablissement l'établissement
	 */
	void calculerTypeInscription(String codeRneEtablissement);
	
	/**
	 * 
	 * @param value lien vers l'étudiant
	 */
	void setToEtudiantRelationship(IInfosEtudiant value);
	
	/**
	 * 
	 * @return année universitaire pour laquelle le titre d''accès a été défini
	 */
	Integer accesAnnee();

	/**
	 * 
	 * @param value annee
	 */
	void setAccesAnnee(Integer value);

	/**
	 * @return supprime le pre-cusus si aucune des valeurs de ce dernier n'est remplie
	 */
	boolean supprimerSiSansValeurs();
		  
}
