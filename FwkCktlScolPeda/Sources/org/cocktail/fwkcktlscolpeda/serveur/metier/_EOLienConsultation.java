// DO NOT EDIT.  Make changes to EOLienConsultation.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOLienConsultation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_LienConsultation";

  // Attribute Keys
  public static final ERXKey<Integer> BASE_NOTE = new ERXKey<Integer>("baseNote");
  public static final ERXKey<Integer> CHILD_ID = new ERXKey<Integer>("childId");
  public static final ERXKey<Integer> CHOIX_ECTS = new ERXKey<Integer>("choixECTS");
  public static final ERXKey<Integer> CHOIX_MAX = new ERXKey<Integer>("choixMax");
  public static final ERXKey<Integer> CHOIX_MIN = new ERXKey<Integer>("choixMin");
  public static final ERXKey<java.math.BigDecimal> COEFFICIENT = new ERXKey<java.math.BigDecimal>("coefficient");
  public static final ERXKey<java.math.BigDecimal> DUREE_MAX = new ERXKey<java.math.BigDecimal>("dureeMax");
  public static final ERXKey<java.math.BigDecimal> DUREE_MIN = new ERXKey<java.math.BigDecimal>("dureeMin");
  public static final ERXKey<Integer> ID_DIPLOME = new ERXKey<Integer>("idDiplome");
  public static final ERXKey<Integer> ID_LIEN = new ERXKey<Integer>("idLien");
  public static final ERXKey<String> ID_LIEN_CONTEXTE = new ERXKey<String>("idLienContexte");
  public static final ERXKey<String> ID_LIEN_CONTEXTE_PARENT = new ERXKey<String>("idLienContexteParent");
  public static final ERXKey<Integer> ID_LIEN_PARENT = new ERXKey<Integer>("idLienParent");
  public static final ERXKey<Integer> ID_LIEN_ROOT = new ERXKey<Integer>("idLienRoot");
  public static final ERXKey<Integer> ID_VERSION_DIPLOME = new ERXKey<Integer>("idVersionDiplome");
  public static final ERXKey<String> LL_COMPOSANT_CHILD = new ERXKey<String>("llComposantChild");
  public static final ERXKey<String> LL_COMPOSANT_PARENT = new ERXKey<String>("llComposantParent");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Boolean> OBLIGATOIRE = new ERXKey<Boolean>("obligatoire");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PARENT_ID = new ERXKey<Integer>("parentId");
  public static final ERXKey<Integer> SANS_NOTE = new ERXKey<Integer>("sansNote");
  public static final ERXKey<String> TYPE_COMPOSANT_NOM_CHILD = new ERXKey<String>("typeComposantNomChild");
  public static final ERXKey<String> TYPE_COMPOSANT_NOM_PARENT = new ERXKey<String>("typeComposantNomParent");
  public static final ERXKey<Integer> TYPE_PERIODE_ID = new ERXKey<Integer>("typePeriodeId");
  public static final ERXKey<Integer> V_DIP_ANNEE = new ERXKey<Integer>("vDipAnnee");
  // Relationship Keys

  // Attributes
  public static final String BASE_NOTE_KEY = BASE_NOTE.key();
  public static final String CHILD_ID_KEY = CHILD_ID.key();
  public static final String CHOIX_ECTS_KEY = CHOIX_ECTS.key();
  public static final String CHOIX_MAX_KEY = CHOIX_MAX.key();
  public static final String CHOIX_MIN_KEY = CHOIX_MIN.key();
  public static final String COEFFICIENT_KEY = COEFFICIENT.key();
  public static final String DUREE_MAX_KEY = DUREE_MAX.key();
  public static final String DUREE_MIN_KEY = DUREE_MIN.key();
  public static final String ID_DIPLOME_KEY = ID_DIPLOME.key();
  public static final String ID_LIEN_KEY = ID_LIEN.key();
  public static final String ID_LIEN_CONTEXTE_KEY = ID_LIEN_CONTEXTE.key();
  public static final String ID_LIEN_CONTEXTE_PARENT_KEY = ID_LIEN_CONTEXTE_PARENT.key();
  public static final String ID_LIEN_PARENT_KEY = ID_LIEN_PARENT.key();
  public static final String ID_LIEN_ROOT_KEY = ID_LIEN_ROOT.key();
  public static final String ID_VERSION_DIPLOME_KEY = ID_VERSION_DIPLOME.key();
  public static final String LL_COMPOSANT_CHILD_KEY = LL_COMPOSANT_CHILD.key();
  public static final String LL_COMPOSANT_PARENT_KEY = LL_COMPOSANT_PARENT.key();
  public static final String NIVEAU_KEY = NIVEAU.key();
  public static final String OBLIGATOIRE_KEY = OBLIGATOIRE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PARENT_ID_KEY = PARENT_ID.key();
  public static final String SANS_NOTE_KEY = SANS_NOTE.key();
  public static final String TYPE_COMPOSANT_NOM_CHILD_KEY = TYPE_COMPOSANT_NOM_CHILD.key();
  public static final String TYPE_COMPOSANT_NOM_PARENT_KEY = TYPE_COMPOSANT_NOM_PARENT.key();
  public static final String TYPE_PERIODE_ID_KEY = TYPE_PERIODE_ID.key();
  public static final String V_DIP_ANNEE_KEY = V_DIP_ANNEE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOLienConsultation.class);

  public EOLienConsultation localInstanceIn(EOEditingContext editingContext) {
    EOLienConsultation localInstance = (EOLienConsultation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer baseNote() {
    return (Integer) storedValueForKey(_EOLienConsultation.BASE_NOTE_KEY);
  }

  public void setBaseNote(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating baseNote from " + baseNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.BASE_NOTE_KEY);
  }

  public Integer childId() {
    return (Integer) storedValueForKey(_EOLienConsultation.CHILD_ID_KEY);
  }

  public void setChildId(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating childId from " + childId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.CHILD_ID_KEY);
  }

  public Integer choixECTS() {
    return (Integer) storedValueForKey(_EOLienConsultation.CHOIX_ECTS_KEY);
  }

  public void setChoixECTS(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating choixECTS from " + choixECTS() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.CHOIX_ECTS_KEY);
  }

  public Integer choixMax() {
    return (Integer) storedValueForKey(_EOLienConsultation.CHOIX_MAX_KEY);
  }

  public void setChoixMax(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating choixMax from " + choixMax() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.CHOIX_MAX_KEY);
  }

  public Integer choixMin() {
    return (Integer) storedValueForKey(_EOLienConsultation.CHOIX_MIN_KEY);
  }

  public void setChoixMin(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating choixMin from " + choixMin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.CHOIX_MIN_KEY);
  }

  public java.math.BigDecimal coefficient() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienConsultation.COEFFICIENT_KEY);
  }

  public void setCoefficient(java.math.BigDecimal value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating coefficient from " + coefficient() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.COEFFICIENT_KEY);
  }

  public java.math.BigDecimal dureeMax() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienConsultation.DUREE_MAX_KEY);
  }

  public void setDureeMax(java.math.BigDecimal value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating dureeMax from " + dureeMax() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.DUREE_MAX_KEY);
  }

  public java.math.BigDecimal dureeMin() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienConsultation.DUREE_MIN_KEY);
  }

  public void setDureeMin(java.math.BigDecimal value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating dureeMin from " + dureeMin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.DUREE_MIN_KEY);
  }

  public Integer idDiplome() {
    return (Integer) storedValueForKey(_EOLienConsultation.ID_DIPLOME_KEY);
  }

  public void setIdDiplome(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idDiplome from " + idDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_DIPLOME_KEY);
  }

  public Integer idLien() {
    return (Integer) storedValueForKey(_EOLienConsultation.ID_LIEN_KEY);
  }

  public void setIdLien(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idLien from " + idLien() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_LIEN_KEY);
  }

  public String idLienContexte() {
    return (String) storedValueForKey(_EOLienConsultation.ID_LIEN_CONTEXTE_KEY);
  }

  public void setIdLienContexte(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idLienContexte from " + idLienContexte() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_LIEN_CONTEXTE_KEY);
  }

  public String idLienContexteParent() {
    return (String) storedValueForKey(_EOLienConsultation.ID_LIEN_CONTEXTE_PARENT_KEY);
  }

  public void setIdLienContexteParent(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idLienContexteParent from " + idLienContexteParent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_LIEN_CONTEXTE_PARENT_KEY);
  }

  public Integer idLienParent() {
    return (Integer) storedValueForKey(_EOLienConsultation.ID_LIEN_PARENT_KEY);
  }

  public void setIdLienParent(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idLienParent from " + idLienParent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_LIEN_PARENT_KEY);
  }

  public Integer idLienRoot() {
    return (Integer) storedValueForKey(_EOLienConsultation.ID_LIEN_ROOT_KEY);
  }

  public void setIdLienRoot(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idLienRoot from " + idLienRoot() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_LIEN_ROOT_KEY);
  }

  public Integer idVersionDiplome() {
    return (Integer) storedValueForKey(_EOLienConsultation.ID_VERSION_DIPLOME_KEY);
  }

  public void setIdVersionDiplome(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating idVersionDiplome from " + idVersionDiplome() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ID_VERSION_DIPLOME_KEY);
  }

  public String llComposantChild() {
    return (String) storedValueForKey(_EOLienConsultation.LL_COMPOSANT_CHILD_KEY);
  }

  public void setLlComposantChild(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating llComposantChild from " + llComposantChild() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.LL_COMPOSANT_CHILD_KEY);
  }

  public String llComposantParent() {
    return (String) storedValueForKey(_EOLienConsultation.LL_COMPOSANT_PARENT_KEY);
  }

  public void setLlComposantParent(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating llComposantParent from " + llComposantParent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.LL_COMPOSANT_PARENT_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(_EOLienConsultation.NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating niveau from " + niveau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.NIVEAU_KEY);
  }

  public Boolean obligatoire() {
    return (Boolean) storedValueForKey(_EOLienConsultation.OBLIGATOIRE_KEY);
  }

  public void setObligatoire(Boolean value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating obligatoire from " + obligatoire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.OBLIGATOIRE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EOLienConsultation.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.ORDRE_KEY);
  }

  public Integer parentId() {
    return (Integer) storedValueForKey(_EOLienConsultation.PARENT_ID_KEY);
  }

  public void setParentId(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating parentId from " + parentId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.PARENT_ID_KEY);
  }

  public Integer sansNote() {
    return (Integer) storedValueForKey(_EOLienConsultation.SANS_NOTE_KEY);
  }

  public void setSansNote(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating sansNote from " + sansNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.SANS_NOTE_KEY);
  }

  public String typeComposantNomChild() {
    return (String) storedValueForKey(_EOLienConsultation.TYPE_COMPOSANT_NOM_CHILD_KEY);
  }

  public void setTypeComposantNomChild(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating typeComposantNomChild from " + typeComposantNomChild() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.TYPE_COMPOSANT_NOM_CHILD_KEY);
  }

  public String typeComposantNomParent() {
    return (String) storedValueForKey(_EOLienConsultation.TYPE_COMPOSANT_NOM_PARENT_KEY);
  }

  public void setTypeComposantNomParent(String value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating typeComposantNomParent from " + typeComposantNomParent() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.TYPE_COMPOSANT_NOM_PARENT_KEY);
  }

  public Integer typePeriodeId() {
    return (Integer) storedValueForKey(_EOLienConsultation.TYPE_PERIODE_ID_KEY);
  }

  public void setTypePeriodeId(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating typePeriodeId from " + typePeriodeId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.TYPE_PERIODE_ID_KEY);
  }

  public Integer vDipAnnee() {
    return (Integer) storedValueForKey(_EOLienConsultation.V_DIP_ANNEE_KEY);
  }

  public void setVDipAnnee(Integer value) {
    if (_EOLienConsultation.LOG.isDebugEnabled()) {
    	_EOLienConsultation.LOG.debug( "updating vDipAnnee from " + vDipAnnee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienConsultation.V_DIP_ANNEE_KEY);
  }


  public static EOLienConsultation createSco_LienConsultation(EOEditingContext editingContext, Integer idDiplome
, Integer idLien
, String idLienContexte
, Integer idLienRoot
, Integer idVersionDiplome
, String llComposantChild
, String llComposantParent
, Integer niveau
, Boolean obligatoire
, Integer ordre
, String typeComposantNomChild
, String typeComposantNomParent
) {
    EOLienConsultation eo = (EOLienConsultation) EOUtilities.createAndInsertInstance(editingContext, _EOLienConsultation.ENTITY_NAME);    
		eo.setIdDiplome(idDiplome);
		eo.setIdLien(idLien);
		eo.setIdLienContexte(idLienContexte);
		eo.setIdLienRoot(idLienRoot);
		eo.setIdVersionDiplome(idVersionDiplome);
		eo.setLlComposantChild(llComposantChild);
		eo.setLlComposantParent(llComposantParent);
		eo.setNiveau(niveau);
		eo.setObligatoire(obligatoire);
		eo.setOrdre(ordre);
		eo.setTypeComposantNomChild(typeComposantNomChild);
		eo.setTypeComposantNomParent(typeComposantNomParent);
    return eo;
  }

  public static ERXFetchSpecification<EOLienConsultation> fetchSpec() {
    return new ERXFetchSpecification<EOLienConsultation>(_EOLienConsultation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOLienConsultation> fetchAllSco_LienConsultations(EOEditingContext editingContext) {
    return _EOLienConsultation.fetchAllSco_LienConsultations(editingContext, null);
  }

  public static NSArray<EOLienConsultation> fetchAllSco_LienConsultations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLienConsultation.fetchSco_LienConsultations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLienConsultation> fetchSco_LienConsultations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOLienConsultation> fetchSpec = new ERXFetchSpecification<EOLienConsultation>(_EOLienConsultation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLienConsultation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOLienConsultation fetchSco_LienConsultation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienConsultation.fetchSco_LienConsultation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienConsultation fetchSco_LienConsultation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLienConsultation> eoObjects = _EOLienConsultation.fetchSco_LienConsultations(editingContext, qualifier, null);
    EOLienConsultation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_LienConsultation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienConsultation fetchRequiredSco_LienConsultation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienConsultation.fetchRequiredSco_LienConsultation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienConsultation fetchRequiredSco_LienConsultation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLienConsultation eoObject = _EOLienConsultation.fetchSco_LienConsultation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_LienConsultation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienConsultation localInstanceIn(EOEditingContext editingContext, EOLienConsultation eo) {
    EOLienConsultation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
