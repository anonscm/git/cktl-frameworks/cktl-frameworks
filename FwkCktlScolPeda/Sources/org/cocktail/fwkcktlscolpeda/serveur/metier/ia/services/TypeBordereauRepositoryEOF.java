package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeBordereau;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBordereau;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Implémentation EOF du repository type de bordereau.
 * 
 * @author Pascal MACOUIN
 */
public class TypeBordereauRepositoryEOF implements TypeBordereauRepository {
	private EOEditingContext editingContext;

	/**
	 * @param editingContext Un editing context
	 */
	public TypeBordereauRepositoryEOF(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ITypeBordereau getTypeBordereauPaiement() {
		return getTypeBordereau(ITypeBordereau.CODE_PAIEMENT);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ITypeBordereau getTypeBordereauRemboursement() {
		return getTypeBordereau(ITypeBordereau.CODE_REMBOURSEMENT);
	}
	
	private ITypeBordereau getTypeBordereau(String code) {
		return EOTypeBordereau.fetchRequiredSco_TypeBordereau(editingContext, EOTypeBordereau.CODE.eq(code));
	}
}
