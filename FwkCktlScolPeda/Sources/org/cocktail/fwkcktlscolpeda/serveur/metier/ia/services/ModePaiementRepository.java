package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;

/**
 * 
 * Fetch des modes de paiement
 * 
 * @author alexistual
 *
 */
public interface ModePaiementRepository {

	/**
	 * @return la liste de tous les modes de paiement, y compris ceux combinés qui n'ont pas 
	 * 		  de correspondance en base
	 */
	List<IModePaiement> fetchModePaiements();

	/**
	 * @return la liste de tous les modes de remboursements
	 */
	List<IModePaiement> fetchModeRemboursements();
}
