package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementCBPrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementChequePrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementVirementPrelevement;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Implémentation EOF.
 * 
 * @author alexistual
 *
 */
public class ModePaiementRepositoryEOF implements ModePaiementRepository {

	private EOEditingContext editingContext;
	
	/**
	 * @param editingContext l'ec
	 */
	public ModePaiementRepositoryEOF(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IModePaiement> fetchModePaiements() {
		List<IModePaiement> modesPaiement = new ArrayList<IModePaiement>(
				EOModePaiement.fetchAllSco_ModePaiements(editingContext, EOModePaiement.ORDRE.ascs()));
		modesPaiement.add(ModePaiementCBPrelevement.instance());
		modesPaiement.add(ModePaiementChequePrelevement.instance());
		modesPaiement.add(ModePaiementVirementPrelevement.instance());
		return modesPaiement;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IModePaiement> fetchModePaiementsComptant() {
		List<IModePaiement> modesPaiement = new ArrayList<IModePaiement>(
				EOModePaiement.fetchSco_ModePaiements(editingContext, EOModePaiement.CODE.ne(IModePaiement.CODE_MODE_PRELEVEMENT), EOModePaiement.ORDRE.ascs()));
		return modesPaiement;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IModePaiement> fetchModeRemboursements() {
		List<IModePaiement> modesPaiement = new ArrayList<IModePaiement>(
				EOModePaiement.fetchSco_ModePaiements(editingContext, EOModePaiement.CODE.eq(EOModePaiement.CODE_MODE_VIREMENT), EOModePaiement.ORDRE.ascs()));
		return modesPaiement;
	}
}
