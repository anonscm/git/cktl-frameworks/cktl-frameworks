package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBordereau;

/**
 * Repository des type de bordereau.
 * 
 * @author Pascal MACOUIN
 */
public interface TypeBordereauRepository {

	/**
	 * @return Le type de bordereau de paiement
	 */
	ITypeBordereau getTypeBordereauPaiement();

	/**
	 * @return Le type de bordereau de remboursement
	 */
	ITypeBordereau getTypeBordereauRemboursement();

}