package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePeriodeInscription;

/**
 * Source des parametrages de periode d'inscription
 */
public interface PeriodeInscriptionRepository {

    /**
     * @param codeProfil un code profil
     * @return liste des parametrages de période d'inscription
     */
    List<IParametragePeriodeInscription> getListeParametragePeriodeInscription(String codeProfil);
    
    /**
     * @return liste des parametrages de période d'inscription
     */
    List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionEtudiant();
    
    /**
     * @return liste des parametrages de période d'inscription
     */
    List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionGestionnaire();

    /**
     * 
     * @param codeProfil un code profil
     * @return nouvelle période d'inscription qui depend du profil
     */
    IParametragePeriodeInscription newParametragePeriodeInscription(String codeProfil);
}
