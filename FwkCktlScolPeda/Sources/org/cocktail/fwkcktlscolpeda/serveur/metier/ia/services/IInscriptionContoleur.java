package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;

public interface IInscriptionContoleur {
	
	
		/**
		 * @return the displayGroupInscription
		 */
		public ERXDisplayGroup<IInscription> getDisplayGroupInscription();
	
		/**
		 * @param tokentypeformation the tokentypeformation
		 * @param tokenDiplome the tokenDiplome
		 * @param tokenGrade the tokenGrade
		 * @param tokenNiveau the tokenNiveau
		 * @param tokenRedoublement the tokenRedoublement
		 * @param tokenReorientation the tokenReorientation
		 * @param tokenPassageConditionnel the tokenPassageConditionnel
		 * @param tokenCycleAmenage the tokenCycleAmenage
		 * @param tokenDate the tokenDate
		 * @return le qualifier pour les filtres
		 */
		public EOQualifier getDisplayGroupInscriptionQualifier(String tokentypeformation, String tokenDiplome, String tokenGrade, String tokenNiveau, String tokenRedoublement, String tokenReorientation,
				String tokenPassageConditionnel, String tokenCycleAmenage, String tokenDate);

}
