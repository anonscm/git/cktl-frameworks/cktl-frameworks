package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISise;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Définition de l'export SISE
 */
public interface ExportSiseService {

	/**
	 * Initialisation des variables
	 * @param editingContext the editingContext
	 * @param etudiantAnnee the etudiantAnnee
	 * @param annee the annee
	 * @param codeEtablissement the codeEtablissement
	 */
	void init(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee, Integer annee, String codeEtablissement);

	/**
	 * import des données dans la table tampon Sise
	 * @return les données sise de type école
	 */
	ISise lancerImportTableSise();

}
