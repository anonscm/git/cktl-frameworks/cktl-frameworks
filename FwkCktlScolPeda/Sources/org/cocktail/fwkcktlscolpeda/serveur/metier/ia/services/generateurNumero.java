package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISysEtudiantOffset;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.jdbc.ERXSQLHelper;

public abstract class generateurNumero {
	
	private EOEditingContext editingContext;
	private Integer anneeUniversitaire;
	private Boolean premierNumeroAnnee = false;
	private String sequenceName;

	public Integer getAnneeUniversitaire() {
		return anneeUniversitaire;
	}

	public void setAnneeUniversitaire(Integer anneeUniversitaire) {
		this.anneeUniversitaire = anneeUniversitaire;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public Boolean getPremierNumeroAnnee() {
		return premierNumeroAnnee;
	}

	public void setPremierNumeroAnnee(Boolean premierNumeroAnnee) {
		this.premierNumeroAnnee = premierNumeroAnnee;
	}

	
	/**
	 * @return the sequenceName
	 */
	public String getSequenceName() {
		return sequenceName;
	}

	/**
	 * @param sequenceName the sequenceName to set
	 */
	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}

	/**
	 * 
	 * @return le nouveau numéro
	 */
	public abstract Integer getNouveauNumero() throws Exception;
	
	Integer getOffsetValue() {
		ISysEtudiantOffset numeroEtudiantOffset = getNumeroEtudiantOffset();
		Integer numero = numeroEtudiantOffset.offset();
		if (getPremierNumeroAnnee()) {
			numero = numero - 1;
		}
		return numero;
	}
	
	Integer getSequenceNextValue() {
		ERXSQLHelper sqlHelper = ERXSQLHelper.newSQLHelper(editingContext, FwkCktlScolPeda.MODEL_NAME);
		Number seqValue = sqlHelper.getNextValFromSequenceNamed(editingContext, FwkCktlScolPeda.MODEL_NAME, sequenceName);
		return seqValue.intValue();
	}

	protected abstract ISysEtudiantOffset getNumeroEtudiantOffset();
}
