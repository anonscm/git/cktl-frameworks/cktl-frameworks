package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametrageInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePeriodeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePeriodeInscription;

import com.google.inject.Inject;

/**
 * Service concernant les périodes d'inscription
 */
public class PeriodeInscriptionServiceImpl implements PeriodeInscriptionService {

	private PeriodeInscriptionRepository periodeInscriptionRepository;

	/**
	 * constructeur avec injection
	 */
	public PeriodeInscriptionServiceImpl() {
		super();
	}

	/**
	 * @param periodeInscriptionRepository : source des parametres de périodes d'inscription
	 */
	@Inject
	public PeriodeInscriptionServiceImpl(PeriodeInscriptionRepository periodeInscriptionRepository) {
		super();
		this.periodeInscriptionRepository = periodeInscriptionRepository;
	}

	// Public Methods
	/**
	 * {@inheritDoc}
	 */
	public Boolean isInscriptionOuverteEtudiant(Date date) {

		for (IParametragePeriodeInscription parametragePeriodeInscription : getListeParametragePeriodeInscriptionEtudiant()) {
			if (isDateDansPeriodeInscription(date, parametragePeriodeInscription)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean isInscriptionOuverteGestionnaire(Date date) {
		List<IParametragePeriodeInscription> parametragesPeriodesInscriptions = getListeParametragePeriodeInscriptionGestionnaire();
		
		if (parametragesPeriodesInscriptions.size() == 0) {
			return true;
		}
		
		for (IParametragePeriodeInscription parametragePeriodeIncription : parametragesPeriodesInscriptions) {
			if (isDateDansPeriodeInscription(date, parametragePeriodeIncription)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String> typesFormationsOuvertesEtudiant(Date date) {
		return typesFormationsOuvertes(date, getListeParametragePeriodeInscriptionEtudiant());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String> typesFormationsOuvertesGestionnaire(Date date) {
		return typesFormationsOuvertes(date, getListeParametragePeriodeInscriptionGestionnaire());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IParametragePeriodeInscription> getListeParametragePeriodeInscription(String codeProfil) {
		return periodeInscriptionRepository.getListeParametragePeriodeInscription(codeProfil);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionEtudiant() {
		return periodeInscriptionRepository.getListeParametragePeriodeInscriptionEtudiant();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionGestionnaire() {
		return periodeInscriptionRepository.getListeParametragePeriodeInscriptionGestionnaire();
	}

	// Private Methods
	private List<String> typesFormationsOuvertes(Date date, List<IParametragePeriodeInscription> parametragesPeriodesInscriptions) {
		List<String> typesFormationsOuvertes = new ArrayList<String>();
		for (IParametragePeriodeInscription parametragePeriodeInscription : parametragesPeriodesInscriptions) {
			if (isDateDansPeriodeInscription(date, parametragePeriodeInscription)) {
				typesFormationsOuvertes.add(parametragePeriodeInscription.typeFormation().code());
			}
		}
		return typesFormationsOuvertes;
	}

	private Boolean isDateDansPeriodeInscription(Date date, IParametragePeriodeInscription parametragePeriodeInscription) {
		GregorianCalendar calendar = new java.util.GregorianCalendar();
		calendar.setTime(parametragePeriodeInscription.dateFermeture());
		calendar.add(Calendar.DATE, 1);
		return date.after(parametragePeriodeInscription.dateOuverture()) && date.before(calendar.getTime());
	}
	
	public IParametragePeriodeInscription newParametragePeriodeInscription(String codeProfil) {
		return periodeInscriptionRepository.newParametragePeriodeInscription(codeProfil);
	}
}
