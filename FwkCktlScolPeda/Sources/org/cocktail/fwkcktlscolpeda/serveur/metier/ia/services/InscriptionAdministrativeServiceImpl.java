package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.common.service.AbstractServiceImpl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogique;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;

/**
 * 
 * @author jlafourc, atual
 *
 */
public class InscriptionAdministrativeServiceImpl extends AbstractServiceImpl implements InscriptionAdministrativeService {
	
    
	/**
	 * {@inheritDoc}
	 */
    public List<IScoInscription> getInscriptionAdministratives(IEtudiant etudiant, Integer annee) {
    	EOQualifier qualifier = 
    		EOInscription.TO_ETUDIANT.eq((EOEtudiant) etudiant)
    		.and(EOInscription.ANNEE.eq(annee));
    
    	ERXSortOrderings sortOrderings = EOInscription.TO_DIPLOME.dot(EODiplome.LIBELLE).ascs();

    	return new ArrayList<IScoInscription>(EOInscription.fetchSco_Inscriptions(editingContext(), qualifier, sortOrderings));
    }
    
    /**
		 * {@inheritDoc}
		 */
    public List<IScoInscription> getInscriptionAdministratives(IDiplome diplome, Integer annee) {
    	EOQualifier qualifier = 
    		EOInscription.TO_DIPLOME.eq((EODiplome) diplome)
    		.and(EOInscription.ANNEE.eq(annee));
    
    	ERXSortOrderings sortOrderings = EOInscription.TO_DIPLOME.dot(EODiplome.CODE).ascs();
    	
    	return new ArrayList<IScoInscription>(EOInscription.fetchSco_Inscriptions(editingContext(), qualifier, sortOrderings));
    }
    
    
    public void setEditingContext(EOEditingContext edc) {
    	this.editingContext = edc;
    	
    }
    
    /**
	 * {@inheritDoc}
	 */
    public List<IScoInscription> getInscriptionAdministrativesAvecIPValide(IEtudiant etudiant, Integer annee) {
    	EOQualifier qualifier = 
    		EOInscription.TO_ETUDIANT.eq((EOEtudiant) etudiant)
    		.and(EOInscription.ANNEE.eq(annee)).
    		and(EOInscription.TO_INSCRIPTIONS_PEDAGOGIQUES.dot(EOInscriptionPedagogique.TO_DEMANDE.dot(EODemande.TO_ETAPE).dot(EOEtape.CODE_ETAPE)).eq(EtapeInscriptionPedagogique.VALIDEE.getCodeEtape()));
    
    	ERXSortOrderings sortOrderings = EOInscription.TO_DIPLOME.dot(EODiplome.LIBELLE).ascs();

    	return new ArrayList<IScoInscription>(EOInscription.fetchSco_Inscriptions(editingContext(), qualifier, sortOrderings));
    }

    /**
     * {@inheritDoc}
     */
	public List<IScoInscription> getInscriptionAdministrative(ILien lienVersion, Integer annee, ILien lienParcoursDiplome, ILien lienAnnee, ILien lienParcoursAnnee, List<EtapeInscriptionPedagogique> etapeIps) {
    	List<String> codeEtapes = Lists.transform(etapeIps, new Function<EtapeInscriptionPedagogique, String>() {
			public String apply(EtapeInscriptionPedagogique etape) {
				return etape.getCodeEtape();
			}
		});
		
		ERXQualifierInSubquery ipAttenteConnexionSubQualifier = new ERXQualifierInSubquery(EOInscriptionPedagogique.TO_DEMANDE.isNull(), EOInscriptionPedagogique.ENTITY_NAME, "idInscription", "idInscription");
		
    	EOQualifier etatIpQualifier = EOInscriptionPedagogique.TO_DEMANDE.dot(EODemande.TO_ETAPE).dot(EOEtape.CODE_ETAPE).in(new NSArray<String>(codeEtapes));
		ERXQualifierInSubquery ipAutresSubQualifier = new ERXQualifierInSubquery(etatIpQualifier, EOInscriptionPedagogique.ENTITY_NAME, "idInscription", "idInscription");
    	
    	EOQualifier qualifier = 
    			EOInscription.TO_DIPLOME.dot(EODiplome.ID).eq(lienVersion.parentId())
					.and(ERXQ.or(ipAttenteConnexionSubQualifier, ipAutresSubQualifier));

    	if (annee != null) {
    		qualifier = ERXQ.and(qualifier, EOInscription.ANNEE.eq(annee));
    	}
    	if (lienParcoursDiplome != null) {
    		qualifier = ERXQ.and(qualifier, EOInscription.ID_PARCOURS_DIPLOME.eq(lienParcoursDiplome.childId()));
    	}
    	if (lienParcoursAnnee != null) {
    		qualifier = ERXQ.and(qualifier, EOInscription.ID_PARCOURS_ANNEE.eq(lienParcoursAnnee.childId()));
    	}
    	if (lienAnnee != null) {
    		qualifier = ERXQ.and(qualifier, EOInscription.NIVEAU.eq(lienAnnee.ordre()));
    	}
    	
		return new ArrayList<IScoInscription>(EOInscription.fetchSco_Inscriptions(editingContext, qualifier, null));
	}
}
