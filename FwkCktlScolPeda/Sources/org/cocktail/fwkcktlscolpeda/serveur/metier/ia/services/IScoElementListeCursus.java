package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;

import er.extensions.eof.ERXKey;

/**
 * 
 * @author isabelle
 *
 */
public class IScoElementListeCursus {

	public static final ERXKey<Integer> ANNEE_DEBUT = new ERXKey<Integer>("anneeDebut");
	public static final ERXKey<Integer> ANNEE_FIN = new ERXKey<Integer>("anneeFin");
	public static final ERXKey<String> DIPLOME_OBTENU = new ERXKey<String>("diplomeObtenu");
	public static final ERXKey<String> LIBELLE_FORMATION = new ERXKey<String>("libelleFormation");
	public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
	public static final ERXKey<EORne> RNE = new ERXKey<EORne>("rne");
	public static final ERXKey<EOGradeUniversitaire> GRADE_UNIVERSITAIRE = new ERXKey<EOGradeUniversitaire>("GradeUniversitaire");
	public static final ERXKey<EOTypeFormation> TYPE_FORMATION = new ERXKey<EOTypeFormation>("toTypeFormation");

	public static final String ANNEE_DEBUT_KEY = ANNEE_DEBUT.key();
	public static final String ANNEE_FIN_KEY = ANNEE_FIN.key();
	public static final String DIPLOME_OBTENU_KEY = DIPLOME_OBTENU.key();
	public static final String LIBELLE_FORMATION_KEY = LIBELLE_FORMATION.key();
	public static final String NIVEAU_KEY = NIVEAU.key();
	public static final String RNE_KEY = RNE.key();
	public static final String GRADE_UNIVERSITAIRE_KEY = GRADE_UNIVERSITAIRE.key();
	public static final String TYPE_FORMATION_KEY = TYPE_FORMATION.key();

	private Integer anneeDebut;  
	private Integer anneeFin;  
	private IRne rne;
	private ITypeFormation typeFormation;
	private IGradeUniversitaire gardeUniversitraire;
	private Integer niveau;
	private String libelleFormation;
	private String diplomeObtenu;
	private String typeInscription;
	private ICursus cursus;
	private boolean interruptionEtude;
	
	/**
	 * @return the anneeDebut
	 */
	public Integer getAnneeDebut() {
		return anneeDebut;
	}
	/**
	 * @param anneeDebut the anneeDebut to set
	 */
	public void setAnneeDebut(Integer anneeDebut) {
		this.anneeDebut = anneeDebut;
	}
	/**
	 * @return the anneeFin
	 */
	public Integer getAnneeFin() {
		return anneeFin;
	}
	/**
	 * @param anneeFin the anneeFin to set
	 */
	public void setAnneeFin(Integer anneeFin) {
		this.anneeFin = anneeFin;
	}
	/**
	 * @return the rne
	 */
	public IRne getRne() {
		return rne;
	}
	/**
	 * @param rne the rne to set
	 */
	public void setRne(IRne rne) {
		this.rne = rne;
	}
	/**
	 * @return the typeFormation
	 */
	public ITypeFormation getTypeFormation() {
		return typeFormation;
	}
	/**
	 * @param typeFormation the typeFormation to set
	 */
	public void setTypeFormation(ITypeFormation typeFormation) {
		this.typeFormation = typeFormation;
	}
	/**
	 * @return the gardeUniversitraire
	 */
	public IGradeUniversitaire getGardeUniversitraire() {
		return gardeUniversitraire;
	}
	/**
	 * @param gardeUniversitraire the gardeUniversitraire to set
	 */
	public void setGardeUniversitraire(IGradeUniversitaire gardeUniversitraire) {
		this.gardeUniversitraire = gardeUniversitraire;
	}
	/**
	 * @return the niveau
	 */
	public Integer getNiveau() {
		return niveau;
	}
	/**
	 * @param niveau the niveau to set
	 */
	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}
	/**
	 * @return the libelleFormation
	 */
	public String getLibelleFormation() {
		return libelleFormation;
	}
	/**
	 * @param libelleFormation the libelleFormation to set
	 */
	public void setLibelleFormation(String libelleFormation) {
		this.libelleFormation = libelleFormation;
	}
	/**
	 * @return the diplomeObtenu
	 */
	public String getDiplomeObtenu() {
		return diplomeObtenu;
	}
	/**
	 * @param diplomeObtenu the diplomeObtenu to set
	 */
	public void setDiplomeObtenu(String diplomeObtenu) {
		this.diplomeObtenu = diplomeObtenu;
	}
	/**
	 * @return the typeInscription
	 */
	public String getTypeInscription() {
		return typeInscription;
	}
	/**
	 * @param typeInscription the typeInscription to set
	 */
	public void setTypeInscription(String typeInscription) {
		this.typeInscription = typeInscription;
	}
	/**
	 * @return the cursus
	 */
	public ICursus getCursus() {
		return cursus;
	}
	/**
	 * @param cursus the cursus to set
	 */
	public void setCursus(ICursus cursus) {
		this.cursus = cursus;
	}
	/**
	 * @return the interruptionEtude
	 */
	public boolean isInterruptionEtude() {
		return interruptionEtude;
	}
	/**
	 * @param interruptionEtude the interruptionEtude to set
	 */
	public void setInterruptionEtude(boolean interruptionEtude) {
		this.interruptionEtude = interruptionEtude;
	}
}
