package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Fournit les inscriptions administratives
 */
public interface InscriptionAdministrativeService {

	/**
	 * @param etudiant etudiant pour lequel on cherche à récuperer la liste des inscriptions administratives
	 * @param annee annee sur laquelle porte la recherche
	 * @return liste des inscrption administratives pour un etudiant et une annee
	 */
	List<IScoInscription> getInscriptionAdministratives(IEtudiant etudiant, Integer annee);

	/**
	 * @param diplome : diplome 
	 * @param annee annee annee sur laquelle porte la recherche
	 * @return liste des inscrption administratives pour un diplome et une annee
	 */
	List<IScoInscription> getInscriptionAdministratives(IDiplome diplome, Integer annee);

	//TODO à virer!
	public void setEditingContext(EOEditingContext edc);
	
	 /**
	 * @param etudiant etudiant pour lequel on cherche à récuperer la liste des inscriptions administratives
	 * @param annee annee sur laquelle porte la recherche
	 * @return liste des inscriptions administratives, ayant l'inscription pédagogique validée 
	 * pour un etudiant et une annee
	 */
	List<IScoInscription> getInscriptionAdministrativesAvecIPValide(IEtudiant etudiant, Integer annee);
	
	/**
	 * @param lienVersion lienVersionDiplome
	 * @param annee annee de la recherche
	 * @param niveau le niveau sur lequel il est inscrit
	 * @param lienParcoursDiplome lien parcours diplome de l'IA
	 * @param lienParcoursAnnee lien parcours annee de l'IA
	 * @param etapeIps liste des étapes sur lesquelles peuvent se trouver les
	 * ips associées aux IA 
	 * @return la liste liste des inscriptions
	 */
	List<IScoInscription> getInscriptionAdministrative(ILien lienVersion, Integer annee, ILien lienParcoursDiplome, ILien lienAnnee, ILien lienParcoursAnnee, List<EtapeInscriptionPedagogique> etapeIps);

}