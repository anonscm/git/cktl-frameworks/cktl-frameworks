package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePeriodeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePeriodeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier._EOParametragePeriodeInscription;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Implementation de la source des parametrages de periodes d'inscription
 */
public class PeriodeInscriptionRepositoryEOF implements PeriodeInscriptionRepository {

    private EOEditingContext editingContext;
    
    /**
     * @param editingContext passage de l'editing context
     */
    @Inject
    public PeriodeInscriptionRepositoryEOF(EOEditingContext editingContext) {
    	this.editingContext = editingContext;
    }
    
    public EOEditingContext getEditingContext() {
	    return editingContext;
    }
    
    public void setEditingContext(EOEditingContext editingContext) {
	    this.editingContext = editingContext;
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<IParametragePeriodeInscription> getListeParametragePeriodeInscription(String codeProfil) {
        NSArray<EOParametragePeriodeInscription> parametrages =
            EOParametragePeriodeInscription.fetchSco_ParametragePeriodeInscriptions(
                editingContext, 
                EOParametragePeriodeInscription.PROFIL.eq(codeProfil), 
                EOParametragePeriodeInscription.TYPE_FORMATION.dot(EOTypeFormation.LIBELLE).asc().then(EOParametragePeriodeInscription.DATE_OUVERTURE.asc()));
        return new ArrayList<IParametragePeriodeInscription>(parametrages);
    }
    
	/**
	 * {@inheritDoc}
	 */
    public List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionEtudiant() {
        return getListeParametragePeriodeInscription(EOParametragePeriodeInscription.CODE_ETUDIANT);
    }

	/**
	 * {@inheritDoc}
	 */
	public List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionGestionnaire() {
        return getListeParametragePeriodeInscription(EOParametragePeriodeInscription.CODE_GESTIONNAIRE);
    }

	public IParametragePeriodeInscription newParametragePeriodeInscription(String codeProfil) {
		IParametragePeriodeInscription parametragePeriodeInscription = EOParametragePeriodeInscription.creerParametragePeriodeInscription(getEditingContext(), codeProfil);
		return parametragePeriodeInscription;
	}
}
