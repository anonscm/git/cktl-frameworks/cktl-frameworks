package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CktlFiltreUtils;

import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Classe permettant d'alimenter la liste des inscriptions
 * @author isabelle
 *
 */
public class IScoInscriptionControleur implements IInscriptionContoleur {
	
	private ERXDisplayGroup<IInscription> displayGroupInscription = null;
	private EOArrayDataSource dataSourceInscription = null;
	private EOEditingContext editingContext;
	private IEtudiantAnnee etudiantAnnee;

	public IScoInscriptionControleur(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee ) {
		setEditingContext(editingContext);
		setEtudiantAnnee(etudiantAnnee);
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @return the etudiantAnnee
	 */
	public IEtudiantAnnee getEtudiantAnnee() {
		return etudiantAnnee;
	}

	/**
	 * @param etudiantAnnee the etudiantAnnee to set
	 */
	public void setEtudiantAnnee(IEtudiantAnnee etudiantAnnee) {
		this.etudiantAnnee = etudiantAnnee;
	}

	public ERXDisplayGroup<IInscription> getDisplayGroupInscription() {
		
		displayGroupInscription = new ERXDisplayGroup<IInscription>();
		
		
		getDataSourceInscription().setArray(new NSArray(etudiantAnnee.toInscriptions()));
				
		displayGroupInscription.setDataSource(getDataSourceInscription());

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOInscription.D_CREATION_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
		displayGroupInscription.setSortOrderings(sortOrderings);

		displayGroupInscription.setSelectsFirstObjectAfterFetch(false);
		displayGroupInscription.fetch();
		return displayGroupInscription;
	}

	/**
	 * initialisation qu qualifier pour les filtres
	 */
	public EOQualifier getDisplayGroupInscriptionQualifier(String tokentypeformation, String tokenDiplome, String tokenGrade, String tokenNiveau, String tokenRedoublement, String tokenReorientation,
			String tokenPassageConditionnel, String tokenCycleAmenage, String tokenDate ) {
		CktlFiltreUtils filtreUtils = new CktlFiltreUtils();
		EOQualifier qualifier =
			    ERXQ.and( filtreUtils.getLikeQualifier(
			    		EOInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.LIBELLE_KEY).key(), tokentypeformation), filtreUtils
			        .getLikeQualifier(EOInscription.TO_DIPLOME.dot(EODiplome.LIBELLE_KEY).key(), tokenDiplome), filtreUtils.getLikeQualifier(
			        		EOInscription.TO_GRADE_UNIVERSITAIRE.dot(EOGradeUniversitaire.LIBELLE_KEY).key(), tokenGrade), filtreUtils.getLikeQualifier(
			        				EOInscription.NIVEAU_KEY, tokenNiveau), filtreUtils.getEqualsBooleanQualifier(EOInscription.REDOUBLEMENT_KEY, tokenRedoublement),
			        filtreUtils.getEqualsBooleanQualifier(EOInscription.REORIENTATION_KEY, tokenReorientation), filtreUtils.getEqualsBooleanQualifier(
			        		EOInscription.PASSAGE_CONDITIONNEL_KEY, tokenPassageConditionnel), filtreUtils.getEqualsBooleanQualifier(
			        				EOInscription.CYCLE_AMENAGE_KEY, tokenCycleAmenage), filtreUtils.getLikeQualifier(EOInscription.D_CREATION_KEY, tokenDate));

		return qualifier;
	}
	
	/**
	 * @return EOArrayDataSource : source des Commposants.
	 */
	public EOArrayDataSource getDataSourceInscription() {
		if (dataSourceInscription == null) {
			dataSourceInscription = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOInscription.class), getEditingContext());
		}
		return dataSourceInscription;
	}
}
