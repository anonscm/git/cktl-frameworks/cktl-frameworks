package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSysIneEtudiantOffset;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSysNumeroEtudiantOffset;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.jdbc.ERXSQLHelper;

public class NumeroIneEtudiantGenerateur extends generateurNumero{
	
	private static final String SEQUENCE_NAME = "SCO_SCOLARITE.SYS_INE_ETUDIANT_OFFSET_SEQ";

	
	/**
	 * @param edc : editing context
	 * @param anneeUniversitaire universitaire pour laquelle on veut un numero
	 */
	public NumeroIneEtudiantGenerateur(
	        EOEditingContext edc, 
	        Integer anneeUniversitaire) {
		setEditingContext(edc);
		setAnneeUniversitaire(anneeUniversitaire);
		setSequenceName(SEQUENCE_NAME);
	}

	/**
	 * constructeur par defaut
	 */
	public NumeroIneEtudiantGenerateur() {

	}


	/**
	 * {@inheritDoc}
	 */
	public Integer getNouveauNumero() {
			return  (getSequenceNextValue() - getOffsetValue());
		
	}


	

	Integer getOffsetValue() {
		EOSysIneEtudiantOffset numeroEtudiantOffset = (EOSysIneEtudiantOffset) getNumeroEtudiantOffset();
		Integer numero = numeroEtudiantOffset.offset();
		if (getPremierNumeroAnnee()) {
			numero = numero - 1;
		}
		return numero;
	}

	protected EOSysIneEtudiantOffset getNumeroEtudiantOffset() {
		EOSysIneEtudiantOffset numeroEtudiantOffset = EOSysIneEtudiantOffset.fetchSco_SysIneEtudiantOffset(getEditingContext(),
		    EOSysNumeroEtudiantOffset.ANNEE_UNIVERSITAIRE.eq(getAnneeUniversitaire()));
		
		setPremierNumeroAnnee(false);		
		if (numeroEtudiantOffset == null) {
			setPremierNumeroAnnee(true);
			numeroEtudiantOffset = EOSysIneEtudiantOffset.createSco_SysIneEtudiantOffset(getEditingContext(), getAnneeUniversitaire(), getSequenceNextValue() - 1);
		}
		
		return numeroEtudiantOffset;
	}


}
