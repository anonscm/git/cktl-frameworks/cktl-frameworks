package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePeriodeInscription;

/**
 * Service concernant les périodes d'inscription
 */
public interface PeriodeInscriptionService {

	/**
	 * @param date date testee
	 * @return vrai si au moins une periode d'inscription est ouverte pour l'étudiant à une date donnee
	 */
	Boolean isInscriptionOuverteEtudiant(Date date);

	/**
	 * @param date date testee
	 * @return vrai si au moins une periode d'inscription est ouverte pour le gestionnaire à une date donnee
	 */
	Boolean isInscriptionOuverteGestionnaire(Date date);

	/**
	 * @param date date testee
	 * @return liste des codes des types de formations encore ouvertes à une date donnée pour un etudiant
	 */
	List<String> typesFormationsOuvertesEtudiant(Date date);

	/**
	 * @param date date testee
	 * @return liste des codes des types de formations encore ouvertes à une date donnée pour un gestionnaire
	 */
	List<String> typesFormationsOuvertesGestionnaire(Date date);

	/**
	 * Liste des paramètres des périodes d'inscription pour le profil passé en paramètre.
	 * @param codeProfil un code profil
	 * @return liste des paramètres des périodes d'inscription pour les étudiants
	 */
	List<IParametragePeriodeInscription> getListeParametragePeriodeInscription(String codeProfil);
	
	/**
	 * Liste des paramètres des périodes d'inscription pour les étudiants.
	 * @return liste des paramètres des périodes d'inscription pour les étudiants
	 */
	List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionEtudiant();
	
	/**
	 * Liste des paramètres des périodes d'inscription pour les gestionnaires.
	 * @return liste des paramètres des périodes d'inscription pour les gestionnaires
	 */
	List<IParametragePeriodeInscription> getListeParametragePeriodeInscriptionGestionnaire();
	
	/**
	 * 
	 * Initialisation d'une nouvelle période d'inscription pour le profil passé en paramètre (gestionnaire ou étudiant)
	 * @param codeProfil un code profil
	 * @return nouvelle période d'inscription
	 */
	IParametragePeriodeInscription newParametragePeriodeInscription(String codeProfil);
}