package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSysNumeroEtudiantOffset;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.jdbc.ERXSQLHelper;
import er.extensions.localization.ERXLocalizer;

/**
 * génere un numéro d'étudiant XXYYYYY Formatage : Properties (sur 2 chiffres) : XX Les 5 chiffres suivants sont chrono à partir de 00001 (réinitialisation
 * chaque année)
 */
public class NumeroEtudiantGenerateurPrefixeSequence extends generateurNumero implements NumeroEtudiantGenerateur {
	private static final Integer SEQ_SIZE = 100000;
	private static final String SEQUENCE_NAME = "SCO_SCOLARITE.SYS_NUMERO_ETUDIANT_SEQ";
	
	/**
	 * @param edc : editing context
	 * @param anneeUniversitaire universitaire pour laquelle on veut un numero
	 */
	public NumeroEtudiantGenerateurPrefixeSequence(
	        EOEditingContext edc, 
	        Integer anneeUniversitaire) {
		setEditingContext(edc);
		setAnneeUniversitaire(anneeUniversitaire);
		setSequenceName(SEQUENCE_NAME);
		
	}

	/**
	 * constructeur par defaut
	 */
	public NumeroEtudiantGenerateurPrefixeSequence() {

	}

	/**
	 * {@inheritDoc}
	 * @throws Exception
	 */
	public Integer getNouveauNumero() throws Exception {
		try {
			return getPrefix() * SEQ_SIZE + (getSequenceNextValue() - getOffsetValue());
		} catch (Exception e) {
			System.err.println(e.getStackTrace());
			throw new Exception(ERXLocalizer.defaultLocalizer().localizedStringForKey("NumeroEtudiantGenerateurPrefixeSequence.erreurCreationNumero"));
		}

	}

	Integer getPrefix() {
		return FwkCktlScolPeda.getParamManager().getGenerateurNumeroEtudiantPrefix();
	}

	protected EOSysNumeroEtudiantOffset getNumeroEtudiantOffset() {
		EOSysNumeroEtudiantOffset numeroEtudiantOffset = EOSysNumeroEtudiantOffset.fetchSco_SysNumeroEtudiantOffset(getEditingContext(),
		    EOSysNumeroEtudiantOffset.ANNEE_UNIVERSITAIRE.eq(getAnneeUniversitaire()));
		
		setPremierNumeroAnnee(false);		
		if (numeroEtudiantOffset == null) {
			setPremierNumeroAnnee(true);
			numeroEtudiantOffset = EOSysNumeroEtudiantOffset.createSco_SysNumeroEtudiantOffset(getEditingContext(), getAnneeUniversitaire(), getSequenceNextValue() - 1);
		}
		
		return numeroEtudiantOffset;
	}

}
