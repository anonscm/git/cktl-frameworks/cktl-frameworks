package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSise;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISise;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeEchange;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeEtablissementSise;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.ConstantesEnqueteSise;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Classe de service permettant de faire l'export sise 
 * @author isabelle
 *
 */
public class ExportSiseServiceImpl implements ExportSiseService {

	private int anneeCursus = 0;
	private ICursus dernierCursus = null;
	private List<ICursus> listeCursus;
	private EOEditingContext editingContext;
	private IEtudiantAnnee etudiantAnnee;
	private IInfosEtudiant infosEtudiant;
	private IIndividu individuEtudiant;
	private IInscription inscriptionPrincipale;
	private IInscription inscriptionSecondaire;
	private Integer annee;
	private String codeEtablissement;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public void init(EOEditingContext editingContext,	IEtudiantAnnee etudiantAnnee, Integer annee, String codeEtablissement) {
		this.editingContext = editingContext;
		this.etudiantAnnee = etudiantAnnee;
		this.infosEtudiant = etudiantAnnee.toInfosEtudiant();
		this.individuEtudiant = infosEtudiant.toIndividuEtudiant();
		this.annee = annee;
		this.codeEtablissement = codeEtablissement;

		this.anneeCursus = 0;
		this.dernierCursus = null;
		this.listeCursus = (List<ICursus>) infosEtudiant.toCursus();
		if (listeCursus != null && listeCursus.size() > 0) {
			calculDerniercursus();
		}

		this.inscriptionPrincipale = etudiantAnnee.getInscriptionPrincipale();
		this.inscriptionSecondaire = etudiantAnnee.getInscriptionSecondaire();
	}

	/**
	 * {@inheritDoc}
	 */
	public ISise lancerImportTableSise() {
		ISise donneesSise =  doCreateDonneesSise();		
		creerDonneesSisePourEtudiantAnnee(donneesSise);
		return donneesSise;
	}

	private void setComplementsUniv(ISise donneesSise) {
		donneesSise.setTelens(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setDom1(getDom1());
		donneesSise.setDom2(getDom2());
		donneesSise.setDom3(getDom3());
		donneesSise.setDom4(getDom4());
		donneesSise.setDom5(getDom5());
		donneesSise.setParType(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setCge(ISise.ORACLE_EMPTY_STRING);
	}

	/**
	 * Calcul du dernier cursus de l'étudiant 
	 */
	private void calculDerniercursus() {
		for (ICursus cursus : listeCursus) {
			if (cursus.anneeFin() != null) {
				if (anneeCursus < cursus.anneeFin()) {
					if (!cursus.interruptionEtud()) {
						anneeCursus = cursus.anneeFin();
						dernierCursus = cursus;
					}
				}
			} else if (cursus.anneeDebut() != null) {
				int cursusAnneeDebutPlusUn = cursus.anneeDebut() + 1;
				if (anneeCursus < cursusAnneeDebutPlusUn) {
					if (!cursus.interruptionEtud()) {
						anneeCursus = cursusAnneeDebutPlusUn;
						dernierCursus = cursus;
					}
				}
			}
		}
	}

	ISise creerDonneesSisePourEtudiantAnnee(ISise donneesSise) {
		if (infosEtudiant.etudNumero() != null) {
			donneesSise.setNumins(infosEtudiant.etudNumero().toString());
		}
		donneesSise.setIdetu(infosEtudiant.codeIne());
		donneesSise.setCompos(codeEtablissement);
		setInscriptionsInfos(donneesSise);
		setDateNaissance(donneesSise);
		setCivilite(donneesSise);
		setBac(donneesSise);
		setInscription(donneesSise);
		setSituationEtudiant(donneesSise);
		setSituationParents(donneesSise);
		setComplementsUniv(donneesSise);
		return donneesSise;
	}

	private void setSituationParents(ISise donneesSise) {
		IAdresse adressePar = infosEtudiant.adresseParent();
		if (adressePar.toPays() != null) {
			donneesSise.setPaypar(adressePar.toPays().getCode());
		}
		donneesSise.setCpPar(getCodePostal(adressePar));
		
		IProfession profession1 = infosEtudiant.toProfession1();
		if (profession1 != null) {
			donneesSise.setPcspar(profession1.proCode());
		}

		IProfession profession2 = infosEtudiant.toProfession2();
		if (profession2 != null) {
			donneesSise.setPcspar2(profession2.proCode());
		}

		donneesSise.setActivi(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setLComRef(adressePar.ville());
		donneesSise.setComref(getCodeCommune(infosEtudiant.adresseParent()));
	}

	private void setSituationEtudiant(ISise donneesSise) {
		if (individuEtudiant.toPaysNationalite() != null) {
			donneesSise.setNation(individuEtudiant.toPaysNationalite().getCode());
		}
		IAdresse adresseEtud = infosEtudiant.adresseEtudiant();
		if (adresseEtud.toPays() != null) {
			donneesSise.setParipa(adresseEtud.toPays().getCode());
		}
		donneesSise.setCpEtu(getCodePostal(adresseEtud));
		donneesSise.setLcomEtu(adresseEtud.ville());
		donneesSise.setCometu(getCodeCommune(infosEtudiant.adresseEtudiant()));
	}

	private void setInscription(ISise donneesSise) {
		if (infosEtudiant.etudAnnee1inscUniv() != null) {
			donneesSise.setAninsc(infosEtudiant.etudAnnee1inscUniv().toString());
		}
		donneesSise.setSitupre(getSitupre());
		donneesSise.setConv(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setDipder(getDipder());
		donneesSise.setCurpar(getCurpar());
		donneesSise.setEchang(getEchang(donneesSise));
		donneesSise.setDiplom(getDiplom());
		donneesSise.setNiveau(getNiveau());
		donneesSise.setSpeciA(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setSpeciB(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setSpeciC(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setNumed(getNumeroEcoleDoctorale());
		if (infosEtudiant.etudReimmatriculation() != null) {
			donneesSise.setReimma(infosEtudiant.etudReimmatriculation());
		}
		if (infosEtudiant.etudAnnee1inscSup() != null) {
			donneesSise.setAnsup(infosEtudiant.etudAnnee1inscSup().toString());
		}
		if (infosEtudiant.etudAnnee1inscEtab() != null) {
			donneesSise.setAnetab(infosEtudiant.etudAnnee1inscEtab().toString());
		}
		donneesSise.setAmena(getCycleAmenage());
		donneesSise.setTyprepa(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setDispens(ISise.ORACLE_EMPTY_STRING);
		donneesSise.setFonctio(getFonction());
	}

	private void setBac(ISise donneesSise) {
		if (infosEtudiant.toBac() != null) {
			donneesSise.setBac(infosEtudiant.toBac().bacCodeSise());
		}
		if (infosEtudiant.etudAnbac() != null) {
			donneesSise.setAnbac(infosEtudiant.etudAnbac().toString());
		}
		if (infosEtudiant.toDepartementEtabBac() != null) {
			donneesSise.setDepbac(infosEtudiant.toDepartementEtabBac().cDepartement());
		}
		
		if (infosEtudiant.toRneEtabBac() != null) {
			if (infosEtudiant.toRneEtabBac().acadCode() != null) {
				String cAcademie = infosEtudiant.toRneEtabBac().acadCode();
				donneesSise.setAcabac(cAcademie.substring(1));
			} else if (infosEtudiant.toRneEtabBac().isUnEtablissementFrancaisALEtranger()) {
				donneesSise.setAcabac("00");
			}
		}
	}

	private void setCivilite(ISise donneesSise) {
		if (individuEtudiant.getCivilite() !=  null) {
			donneesSise.setSexe(individuEtudiant.getCivilite().getSexe());
		}
	}

	private void setDateNaissance(ISise donneesSise) {
		if (individuEtudiant.dNaissance() != null) {
			donneesSise.setJonais(new SimpleDateFormat("dd").format(individuEtudiant.dNaissance()));
			donneesSise.setMonais(new SimpleDateFormat("MM").format(individuEtudiant.dNaissance()));
			donneesSise.setAnnais(new SimpleDateFormat("yyyy").format(individuEtudiant.dNaissance()));
		}
	}


	void setInscriptionsInfos(ISise donneesSise) {
		if (inscriptionPrincipale != null) {
			donneesSise.setInspr("O");
			if (inscriptionPrincipale.toRegimeInscription() != null) {
				donneesSise.setRegime(inscriptionPrincipale.toRegimeInscription().code());
			}
		} else {
			donneesSise.setInspr("N");
			if (inscriptionSecondaire != null && inscriptionSecondaire.toRegimeInscription() != null) { 
				donneesSise.setRegime(inscriptionSecondaire.toRegimeInscription().code());
			}
		}
	}

	ISise doCreateDonneesSise() {
		ISise donneesSise = EOSise.createSco_Sise(editingContext);
		return donneesSise;
	}

	/**
	 * Calcul de la situation de l'année précédente
	 * @return situpre
	 */
	String getSitupre() {
		if (dernierCursus == null) {
			if (isSeulementBacFr(annee)) {
				return "A";
			}
			if (isSeulementBacEtr(annee)) {
				return "Q";
			}
			if (isSitupreNonScolarise(annee)) {
				return "T";
			}
		} else {
			if (isSitupreNonScolariseMaisDejaEnseignSup(annee)) {
				return "U";
			} 
			if (isSitupreCPGE(annee)) {
				return "D";
			}
			if (isSitupreIUT(annee)) {
				return "C";
			}
			if (isSitupreIngenieur(annee)) {
				return "E";
			}
			if (isSitupreEtablissementEtranger(annee)) {
				return "R";
			}
			if (isSitupreUniversite(annee)) {
				return "H";
			}
			if (isSitupreAutresEtablissementsSise(annee)) {
				return "K";
			}
		}

		return "S";
	}

	/**
	 * Calcul du type du dernier diplome obtenu
	 * @return
	 */
	String getDipder() {
		if (dernierCursus == null) {
			if (isSeulementBacFr()) {
				return "A";
			}
			if (isSeulementBacEtr()) {
				return "1";
			}
		} else if (dernierCursus.toTypeFormation() != null) {
			if (isDipderDUT()) {
				return "C";
			}
			if (isDipderCPGE()) {
				return "D";
			}
			if (isDipderIngenieur()) {
				return "E";
			}
			if (isDipderDAEU()) {
				return "F";
			}
			if (isDipderDEUG()) {
				return "I";
			}
			if (isDipderLicences()) {
				return "N";
			}
			if (isDipderMaitrise()) {
				return "Q";
			}
			if (isDipderMaster()) {
				return "U";
			}
			if (isDipderCertificat()) {
				return "Y";
			}
		}

		return ISise.ORACLE_EMPTY_STRING;
	}


	private String getCurpar() {
		IInscription inscription = inscriptionSecondaire;
		String curpar = ITypeEtablissementSise.CODE_SANS;
		if (inscription != null) {
			String tetabCode = null;
			if (inscription.toEtablissementCumulatif() != null && inscription.toEtablissementCumulatif().toRne() != null) {
				tetabCode = inscription.toEtablissementCumulatif().toRne().tetabCode();
			}
			if (tetabCode != null) {
				if (ConstantesEnqueteSise.getTypeEtabBTS().contains(tetabCode)) {
					curpar = ITypeEtablissementSise.CODE_BTS;
				}
				if (ConstantesEnqueteSise.getTypeEtabEcoleIngenieur().contains(tetabCode)) {
					curpar =  ITypeEtablissementSise.CODE_ECOLE_INGENIEUR;
				}
				if (ConstantesEnqueteSise.getTypeEtabEtablissementPrive().contains(tetabCode)) {
					curpar =  ITypeEtablissementSise.CODE_ETABLISSEMENT_PRIVE;
				}
				if (ConstantesEnqueteSise.getTypeEtabEtablissementEtranger().contains(tetabCode)) {
					if (inscription.toTypeEchange()!= null) {
						curpar =  ITypeEtablissementSise.CODE_ETABLISSEMENT_ETRANGER;
					} else {
						curpar =  ITypeEtablissementSise.CODE_SANS;
					}
				}
				if (ConstantesEnqueteSise.getTypeEtabEcoleNormale().contains(tetabCode)) {
					curpar =  ITypeEtablissementSise.CODE_ECOLE_NORMALE;
				}
				if (ConstantesEnqueteSise.getTypeEtabAutreEcole().contains(tetabCode)) {
					curpar =  ITypeEtablissementSise.CODE_AUTRES;
				}
				if (ConstantesEnqueteSise.getTypeEtabUniversite().contains(tetabCode)) {
					curpar =  ITypeEtablissementSise.CODE_UNIVERSITE;
				}

			}
		}

		return curpar;	

	}

	private String getCodePostal(IAdresse adresse) {
		if (adresse.codePostal() != null ) {
			return (adresse.codePostal());
		}
		return "99999";
	}

	String getEchang(ISise donneesSise) {
		IInscription inscription;
		if (!donneesSise.inspr().equals("N")) {
			inscription = inscriptionPrincipale;
		} else {
			inscription = inscriptionSecondaire;
		}

		if (inscription != null && inscription.toTypeEchange() == null) {
			return "0";
		}

		// pour les arrivées
		if (inscription != null && inscription.toTypeEchange() != null && 
				inscription.toTypeInscriptionFormation().codeInscription().equals(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL)) {
			if (isEchangeArriveeSocrateErasmus(inscription)) {
				return "1";
			}
			if (isEchangeArriveeTempus(inscription)) {
				return "2";
			}
			if (isEchangeArriveeSocrateComenius(inscription)) {
				return "3";
			}
			if (isEchangeArriveeLeonardoDaVinci(inscription)) {
				return "5";
			}
			if (isEchangeArriveePgmesFinances(inscription)) {
				return "6";
			}
			if (isEchangeArriveePgmesAccords(inscription)) {
				return "7";
			}
		}
		// pour les départs
		if (inscription != null && inscription.toTypeEchange() != null) { 
			if (isEchangeDepartSocrateErasmus(inscription)) {
				return "B";
			}
			if (isEchangeDepartTempus(inscription)) {
				return "C";
			}
			if (isEchangeDepartSocrateComenius(inscription)) {
				return "D";
			}
			if (isEchangeDepartLeonardoDaVinci(inscription)) {
				return "G";
			}
			if (isEchangeDepartPgmesFinances(inscription)) {
				return "H";
			}
			if (isEchangeDepartPgmesAccords(inscription)) {
				return "J";
			}
		}
		return ISise.ORACLE_EMPTY_STRING;
	}


	String getDiplom() {
		String infoDiplome = ISise.ORACLE_EMPTY_STRING;
		IInscription inscription = inscriptionPrincipale;
		if (inscription == null) {
			inscription = inscriptionSecondaire;
			if (inscription == null) {
				return infoDiplome;
			}
		}
		if (inscription.toDiplome() == null) {
			return infoDiplome;
		}

		infoDiplome = inscription.toDiplome().getSiseStr();
		return infoDiplome;
	}

	String getNiveau() {
		if (inscriptionPrincipale != null) {
			return inscriptionPrincipale.niveau().toString();
		} else if (inscriptionSecondaire != null) {
			return inscriptionSecondaire.niveau().toString();
		}
		return ISise.ORACLE_EMPTY_STRING;
	}

	private String getNumeroEcoleDoctorale() {
		String numeroEcoleDoctorale = ISise.ORACLE_EMPTY_STRING;

		IInscription inscription = inscriptionPrincipale;
		if (inscription == null) {
			inscription = inscriptionSecondaire;
		}

		if (inscription != null
				&& inscription.toDiplome() != null
				&& inscription.toDiplome().ecoleDoctorale() != null) {
			numeroEcoleDoctorale = StringUtils.rightPad(
					inscription.toDiplome().ecoleDoctorale().noEcoleDoctorale().toString(),
					numeroEcoleDoctorale.length());
		}

		return numeroEcoleDoctorale;
	}

	String getCodeCommune(IAdresse adresse) {
		if (adresse != null) {
			EOQualifier qualifierVille = ERXQ.or(EOCommune.LL_COM.eq(adresse.ville()), EOCommune.LC_COM.eq(adresse.ville())); 
			EOQualifier qualifier = ERXQ.and(qualifierVille, EOCommune.C_POSTAL.eq(adresse.codePostal()));
			EOCommune commune = EOCommune.fetchFirstByQualifier(editingContext, qualifier);
			if (commune != null) {
				return commune.cInsee();
			}
		}
		return ISise.ORACLE_EMPTY_STRING;
	}

	private String getCycleAmenage() {
		if (inscriptionPrincipale != null) {
			if (inscriptionPrincipale.cycleAmenage()) {
				return "1";
			}
		} else if (inscriptionSecondaire != null) {
			if (inscriptionSecondaire.cycleAmenage()) {
				return "1";
			}
		}
		return "0";		
	}

	private String getFonction() {
		if (etudiantAnnee.toSituationProfessionnelle() != null && (EOSituationProfessionnelle.CODE_SITPROF_FONCTIONNAIRE.equals(etudiantAnnee.toSituationProfessionnelle().codeSitProf()) 
				|| EOSituationProfessionnelle.CODE_SITPROF_MILITAIRE.equals(etudiantAnnee.toSituationProfessionnelle().codeSitProf()))) {
			return "K";
		} 
		return ISise.ORACLE_EMPTY_STRING;
	}

	private boolean isSeulementBacFr() {
		return (infosEtudiant.etudAnbac() != null)
				&& (infosEtudiant.toBac() != null)
				&& (infosEtudiant.toBac().bacType().equals(EOBac.BAC_TYPE_BAC) || infosEtudiant.toBac().bacType().equals(EOBac.BAC_TYPE_BAC_PROFESSIONEL));
	}

	private boolean isSeulementBacEtr() {
		return  (infosEtudiant.etudAnbac() != null)
				&& (infosEtudiant.toBac().bacCodeSise().equals(ConstantesEnqueteSise.getBacCodeSiseEnseignementSecondaireEtranger()));
	}

	private boolean isSeulementBacFr(int anneeEnCours) {
		return (infosEtudiant.etudAnbac() != null)
				&& (anneeEnCours == (infosEtudiant.etudAnbac()))
				&& (infosEtudiant.toBac() != null) && (!infosEtudiant.toBac().bacCodeSise().equals(ConstantesEnqueteSise.getBacCodeSiseEnseignementSecondaireEtranger()));
	}

	private boolean isSeulementBacEtr(int anneeEnCours) {
		return (infosEtudiant.etudAnbac() != null) && (anneeEnCours == (infosEtudiant.etudAnbac()))
				&&  (infosEtudiant.toBac() != null) && (infosEtudiant.toBac().bacCodeSise().equals(ConstantesEnqueteSise.getBacCodeSiseEnseignementSecondaireEtranger()));
	}

	private boolean isSitupreNonScolarise(int anneeEnCours) {
		return (infosEtudiant.etudAnbac() != null) && (anneeEnCours != (infosEtudiant.etudAnbac())) && (listeCursus == null || listeCursus.size() == 0);
	}

	private boolean isSitupreNonScolariseMaisDejaEnseignSup(int anneeEnCours) {
		return anneeEnCours != anneeCursus;
	}

	private boolean isSitupreCPGE(int anneeEnCours) {
		return anneeEnCours == anneeCursus && dernierCursus.toTypeFormation() != null && ConstantesEnqueteSise.getTypeFormationPourSitupreD().equals(dernierCursus.toTypeFormation().code());
	}

	private boolean isSitupreIUT(int anneeEnCours) {
		return anneeEnCours == anneeCursus && dernierCursus.toTypeFormation() != null && ConstantesEnqueteSise.getTypeFormationPourSitupreC().contains(dernierCursus.toTypeFormation().code());
	}

	private boolean isSitupreIngenieur(int anneeEnCours) {
		return anneeEnCours == anneeCursus && (dernierCursus.toTypeFormation() != null && (ConstantesEnqueteSise.getTypeFormationPourSitupreE().contains(dernierCursus.toTypeFormation().code())) 
				|| (dernierCursus.toCRne() != null && ConstantesEnqueteSise.getListeRneSitupreE().contains(dernierCursus.toCRne().cRne())));
	}

	private boolean isSitupreEtablissementEtranger(int anneeEnCours) {
		return anneeEnCours == anneeCursus && dernierCursus.toTypeFormation() != null && ConstantesEnqueteSise.getTypeFormationPourSitupreR().equals(dernierCursus.toTypeFormation().code());
	}

	private boolean isSitupreUniversite(int anneeEnCours) {
		return anneeEnCours == anneeCursus && (dernierCursus.toTypeFormation() != null && (ConstantesEnqueteSise.getTypeFormationPourSitupreH().contains(dernierCursus.toTypeFormation().code())) 
				|| (dernierCursus.toCRne() != null && ConstantesEnqueteSise.getListeRneSitupreH().contains(dernierCursus.toCRne().cRne())));
	}

	private boolean isSitupreAutresEtablissementsSise(int anneeEnCours) {
		return anneeEnCours == anneeCursus && (dernierCursus.toCRne() != null && ConstantesEnqueteSise.getListeRneSitupreK().contains(dernierCursus.toCRne().cRne()));
	}

	private Boolean isDipderDUT() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderDUT().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderCPGE() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderCPGE().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderDAEU() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderDAEU().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderDEUG() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderDEUG().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderLicences() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderLicences().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderMaitrise() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderMaitrise().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderMaster() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderMaster().contains(dernierCursus.toTypeFormation().code());
	}
	private Boolean isDipderIngenieur() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderIngenieur().contains(dernierCursus.toTypeFormation().code());
	}

	private Boolean isDipderCertificat() {
		return ConstantesEnqueteSise.getTypeFormationPourDipderCertificat().contains(dernierCursus.toTypeFormation().code());
	}

	private Boolean isEchangeArriveeSocrateErasmus(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.SOCRATES_ERASMUS_A);
	}

	private Boolean isEchangeDepartSocrateErasmus(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.SOCRATES_ERASMUS_D);
	}

	private Boolean isEchangeArriveeTempus(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.TEMPUS_A);
	}

	private Boolean isEchangeDepartTempus(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.TEMPUS_D);
	}

	private Boolean isEchangeArriveeSocrateComenius(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.SOCRATES_COMENIUS_A);
	}

	private Boolean isEchangeDepartSocrateComenius(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.SOCRATES_COMENIUS_D);
	}

	private Boolean isEchangeArriveeLeonardoDaVinci(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.LEONARDO_DA_VINCI_A);
	}

	private Boolean isEchangeDepartLeonardoDaVinci(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.LEONARDO_DA_VINCI_D);
	}

	private Boolean isEchangeArriveePgmesFinances(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.AUTRES_PGMES_FINANCES_UE_A);
	}

	private Boolean isEchangeDepartPgmesFinances(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.AUTRES_PGMES_FINANCES_UE_D);
	}

	private Boolean isEchangeArriveePgmesAccords(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_ARRIVEE) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.AUTRES_PGMES_ACCORDS_BI_A);
	}

	private Boolean isEchangeDepartPgmesAccords(IInscription inscription) {
		return inscription.toTypeEchange().sens().equals(ITypeEchange.TYPE_ECHANGE_DEPART) && inscription.toTypeEchange().codeEchange().equals(ITypeEchange.AUTRES_PGMES_ACCORDS_BI_D);
	}


	private String getDom1() {
		return ISise.ORACLE_EMPTY_STRING;
	}

	private String getDom2() {
		return ISise.ORACLE_EMPTY_STRING;
	}
	private String getDom3() {
		return ISise.ORACLE_EMPTY_STRING;
	}
	private String getDom4() {
		return ISise.ORACLE_EMPTY_STRING;
	}
	private String getDom5() {
		return ISise.ORACLE_EMPTY_STRING;
	}

	/**
	 * @return the anneeCursus
	 */
	public int getAnneeCursus() {
		return anneeCursus;
	}

	/**
	 * @param anneeCursus the anneeCursus to set
	 */
	public void setAnneeCursus(int anneeCursus) {
		this.anneeCursus = anneeCursus;
	}

	/**
	 * @return the dernierCursus
	 */
	public ICursus getDernierCursus() {
		return dernierCursus;
	}

	/**
	 * @param dernierCursus the dernierCursus to set
	 */
	public void setDernierCursus(ICursus dernierCursus) {
		this.dernierCursus = dernierCursus;
	}

	/**
	 * @return the listeCursus
	 */
	public List<ICursus> getListeCursus() {
		return listeCursus;
	}

	/**
	 * @param listeCursus the listeCursus to set
	 */
	public void setListeCursus(List<ICursus> listeCursus) {
		this.listeCursus = listeCursus;
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @return the etudiantAnnee
	 */
	public IEtudiantAnnee getEtudiantAnnee() {
		return etudiantAnnee;
	}

	/**
	 * @param etudiantAnnee the etudiantAnnee to set
	 */
	public void setEtudiantAnnee(IEtudiantAnnee etudiantAnnee) {
		this.etudiantAnnee = etudiantAnnee;
	}

	/**
	 * @return the annee
	 */
	public Integer getAnnee() {
		return annee;
	}

	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	/**
	 * @return the codeEtablissement
	 */
	public String getCodeEtablissement() {
		return codeEtablissement;
	}

	/**
	 * @param codeEtablissement the codeEtablissement to set
	 */
	public void setCodeEtablissement(String codeEtablissement) {
		this.codeEtablissement = codeEtablissement;
	}

}
