package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

/**
 * genere un numero d'étudiant 
 */
public interface NumeroEtudiantGenerateur {

	/**
	 * @return nouveau numéro d'étudiant
	 * @throws Exception 
	 */
	Integer getNouveauNumero() throws Exception;
}
