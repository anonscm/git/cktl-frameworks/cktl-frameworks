package org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * Interface permettant de recupérer la liste soitt des cursus soit des pré cursus avec les inscriptions
 * @author isabelle
 *
 */
public interface ICursusControleur {
	/**
	 * @return the displayGroupInscription
	 */
	public ERXDisplayGroup<IScoElementListeCursus> getDisplayGroupeElementsCursus();
	

	/**
	 * @param tokenAnneDebut le  filtre anneDebut
	 * @param tokenAnneeFin  le  filtre 
	 * @param tokenEtablissement  le  filtre 
	 * @param tokenTypeFormation  le  filtre 
	 * @param tokenGrade  le  filtre 
	 * @param tokenNiveau  le  filtre 
	 * @param tokenIntituleFormation  le  filtre 
	 * @param tokenDiplome  le  filtre 
	 * @param tokenTypeInsc  le  filtre 
	 * @return qualifier
	 */
	public EOQualifier getDisplayGroupElementsCursusQualifier(String tokenAnneDebut, String tokenAnneeFin, String tokenEtablissement, String tokenTypeFormation, String tokenGrade, String tokenNiveau,
			String tokenIntituleFormation, String tokenDiplome, String tokenTypeInsc);

}
