package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface IVersionDiplome extends IComposant {

	Integer annee();

	IDiplome getDiplome();

	void setAnnee(Integer anneeDepart);

	void setId(Integer i);

}
