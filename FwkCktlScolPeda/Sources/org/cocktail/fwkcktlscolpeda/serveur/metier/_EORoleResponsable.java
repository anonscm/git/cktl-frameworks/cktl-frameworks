// DO NOT EDIT.  Make changes to EORoleResponsable.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORoleResponsable extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_RoleResponsable";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EORoleResponsable.class);

  public EORoleResponsable localInstanceIn(EOEditingContext editingContext) {
    EORoleResponsable localInstance = (EORoleResponsable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EORoleResponsable.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EORoleResponsable.LOG.isDebugEnabled()) {
    	_EORoleResponsable.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EORoleResponsable.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EORoleResponsable.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EORoleResponsable.LOG.isDebugEnabled()) {
    	_EORoleResponsable.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EORoleResponsable.ID_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EORoleResponsable.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EORoleResponsable.LOG.isDebugEnabled()) {
    	_EORoleResponsable.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EORoleResponsable.LIBELLE_COURT_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(_EORoleResponsable.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (_EORoleResponsable.LOG.isDebugEnabled()) {
    	_EORoleResponsable.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, _EORoleResponsable.LIBELLE_LONG_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EORoleResponsable.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EORoleResponsable.LOG.isDebugEnabled()) {
    	_EORoleResponsable.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EORoleResponsable.READ_ONLY_KEY);
  }


  public static EORoleResponsable createSco_RoleResponsable(EOEditingContext editingContext, String code
, Integer id
, String libelleLong
, Integer readOnly
) {
    EORoleResponsable eo = (EORoleResponsable) EOUtilities.createAndInsertInstance(editingContext, _EORoleResponsable.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelleLong(libelleLong);
		eo.setReadOnly(readOnly);
    return eo;
  }

  public static ERXFetchSpecification<EORoleResponsable> fetchSpec() {
    return new ERXFetchSpecification<EORoleResponsable>(_EORoleResponsable.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORoleResponsable> fetchAllSco_RoleResponsables(EOEditingContext editingContext) {
    return _EORoleResponsable.fetchAllSco_RoleResponsables(editingContext, null);
  }

  public static NSArray<EORoleResponsable> fetchAllSco_RoleResponsables(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORoleResponsable.fetchSco_RoleResponsables(editingContext, null, sortOrderings);
  }

  public static NSArray<EORoleResponsable> fetchSco_RoleResponsables(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORoleResponsable> fetchSpec = new ERXFetchSpecification<EORoleResponsable>(_EORoleResponsable.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORoleResponsable> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORoleResponsable fetchSco_RoleResponsable(EOEditingContext editingContext, String keyName, Object value) {
    return _EORoleResponsable.fetchSco_RoleResponsable(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORoleResponsable fetchSco_RoleResponsable(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORoleResponsable> eoObjects = _EORoleResponsable.fetchSco_RoleResponsables(editingContext, qualifier, null);
    EORoleResponsable eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_RoleResponsable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORoleResponsable fetchRequiredSco_RoleResponsable(EOEditingContext editingContext, String keyName, Object value) {
    return _EORoleResponsable.fetchRequiredSco_RoleResponsable(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORoleResponsable fetchRequiredSco_RoleResponsable(EOEditingContext editingContext, EOQualifier qualifier) {
    EORoleResponsable eoObject = _EORoleResponsable.fetchSco_RoleResponsable(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_RoleResponsable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORoleResponsable localInstanceIn(EOEditingContext editingContext, EORoleResponsable eo) {
    EORoleResponsable localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
