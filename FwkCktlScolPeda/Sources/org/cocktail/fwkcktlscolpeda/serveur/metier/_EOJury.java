// DO NOT EDIT.  Make changes to EOJury.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOJury extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Jury";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<String> COMMENTAIRE_GENERAL = new ERXKey<String>("commentaireGeneral");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> EVOLUTIONS_ENVISAGEES = new ERXKey<String>("evolutionsEnvisagees");
  public static final ERXKey<Integer> ID_JURY = new ERXKey<Integer>("idJury");
  public static final ERXKey<String> MODALITES_RATTRAPAGE = new ERXKey<String>("modalitesRattrapage");
  public static final ERXKey<String> MODALITES_REDOUBLEMENT = new ERXKey<String>("modalitesRedoublement");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<java.math.BigDecimal> POINTS_JURY = new ERXKey<java.math.BigDecimal>("pointsJury");
  public static final ERXKey<String> POINTS_JURY_COMMENTAIRE = new ERXKey<String>("pointsJuryCommentaire");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> TO_JURY_CONVOCATIONS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation>("toJuryConvocations");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat> TO_JURY_ETAT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat>("toJuryEtat");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> TO_JURY_PERSONNESES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes>("toJuryPersonneses");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLienFormation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN_NIVEAU_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLienNiveauAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN_NIVEAU_SEMESTRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLienNiveauSemestre");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN_PARCOURS_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLienParcoursAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN_PARCOURS_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLienParcoursFormation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode> TO_SESSION_PERIODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode>("toSessionPeriode");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String COMMENTAIRE_GENERAL_KEY = COMMENTAIRE_GENERAL.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String EVOLUTIONS_ENVISAGEES_KEY = EVOLUTIONS_ENVISAGEES.key();
  public static final String ID_JURY_KEY = ID_JURY.key();
  public static final String MODALITES_RATTRAPAGE_KEY = MODALITES_RATTRAPAGE.key();
  public static final String MODALITES_REDOUBLEMENT_KEY = MODALITES_REDOUBLEMENT.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String POINTS_JURY_KEY = POINTS_JURY.key();
  public static final String POINTS_JURY_COMMENTAIRE_KEY = POINTS_JURY_COMMENTAIRE.key();
  // Relationships
  public static final String TO_JURY_CONVOCATIONS_KEY = TO_JURY_CONVOCATIONS.key();
  public static final String TO_JURY_ETAT_KEY = TO_JURY_ETAT.key();
  public static final String TO_JURY_PERSONNESES_KEY = TO_JURY_PERSONNESES.key();
  public static final String TO_LIEN_FORMATION_KEY = TO_LIEN_FORMATION.key();
  public static final String TO_LIEN_NIVEAU_ANNEE_KEY = TO_LIEN_NIVEAU_ANNEE.key();
  public static final String TO_LIEN_NIVEAU_SEMESTRE_KEY = TO_LIEN_NIVEAU_SEMESTRE.key();
  public static final String TO_LIEN_PARCOURS_ANNEE_KEY = TO_LIEN_PARCOURS_ANNEE.key();
  public static final String TO_LIEN_PARCOURS_FORMATION_KEY = TO_LIEN_PARCOURS_FORMATION.key();
  public static final String TO_SESSION_PERIODE_KEY = TO_SESSION_PERIODE.key();

  private static Logger LOG = Logger.getLogger(_EOJury.class);

  public EOJury localInstanceIn(EOEditingContext editingContext) {
    EOJury localInstance = (EOJury)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOJury.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.ANNEE_KEY);
  }

  public String commentaireGeneral() {
    return (String) storedValueForKey(_EOJury.COMMENTAIRE_GENERAL_KEY);
  }

  public void setCommentaireGeneral(String value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating commentaireGeneral from " + commentaireGeneral() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.COMMENTAIRE_GENERAL_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOJury.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOJury.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.D_MODIFICATION_KEY);
  }

  public String evolutionsEnvisagees() {
    return (String) storedValueForKey(_EOJury.EVOLUTIONS_ENVISAGEES_KEY);
  }

  public void setEvolutionsEnvisagees(String value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating evolutionsEnvisagees from " + evolutionsEnvisagees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.EVOLUTIONS_ENVISAGEES_KEY);
  }

  public Integer idJury() {
    return (Integer) storedValueForKey(_EOJury.ID_JURY_KEY);
  }

  public void setIdJury(Integer value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating idJury from " + idJury() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.ID_JURY_KEY);
  }

  public String modalitesRattrapage() {
    return (String) storedValueForKey(_EOJury.MODALITES_RATTRAPAGE_KEY);
  }

  public void setModalitesRattrapage(String value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating modalitesRattrapage from " + modalitesRattrapage() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.MODALITES_RATTRAPAGE_KEY);
  }

  public String modalitesRedoublement() {
    return (String) storedValueForKey(_EOJury.MODALITES_REDOUBLEMENT_KEY);
  }

  public void setModalitesRedoublement(String value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating modalitesRedoublement from " + modalitesRedoublement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.MODALITES_REDOUBLEMENT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOJury.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOJury.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.PERS_ID_MODIFICATION_KEY);
  }

  public java.math.BigDecimal pointsJury() {
    return (java.math.BigDecimal) storedValueForKey(_EOJury.POINTS_JURY_KEY);
  }

  public void setPointsJury(java.math.BigDecimal value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating pointsJury from " + pointsJury() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.POINTS_JURY_KEY);
  }

  public String pointsJuryCommentaire() {
    return (String) storedValueForKey(_EOJury.POINTS_JURY_COMMENTAIRE_KEY);
  }

  public void setPointsJuryCommentaire(String value) {
    if (_EOJury.LOG.isDebugEnabled()) {
    	_EOJury.LOG.debug( "updating pointsJuryCommentaire from " + pointsJuryCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJury.POINTS_JURY_COMMENTAIRE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat toJuryEtat() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat)storedValueForKey(_EOJury.TO_JURY_ETAT_KEY);
  }
  
  public void setToJuryEtat(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat value) {
    takeStoredValueForKey(value, _EOJury.TO_JURY_ETAT_KEY);
  }

  public void setToJuryEtatRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toJuryEtat from " + toJuryEtat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToJuryEtat(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat oldValue = toJuryEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_JURY_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_JURY_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOJury.TO_LIEN_FORMATION_KEY);
  }
  
  public void setToLienFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOJury.TO_LIEN_FORMATION_KEY);
  }

  public void setToLienFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toLienFormation from " + toLienFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLienFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_LIEN_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_LIEN_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienNiveauAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOJury.TO_LIEN_NIVEAU_ANNEE_KEY);
  }
  
  public void setToLienNiveauAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOJury.TO_LIEN_NIVEAU_ANNEE_KEY);
  }

  public void setToLienNiveauAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toLienNiveauAnnee from " + toLienNiveauAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienNiveauAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLienNiveauAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_LIEN_NIVEAU_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_LIEN_NIVEAU_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienNiveauSemestre() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOJury.TO_LIEN_NIVEAU_SEMESTRE_KEY);
  }
  
  public void setToLienNiveauSemestre(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOJury.TO_LIEN_NIVEAU_SEMESTRE_KEY);
  }

  public void setToLienNiveauSemestreRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toLienNiveauSemestre from " + toLienNiveauSemestre() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienNiveauSemestre(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLienNiveauSemestre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_LIEN_NIVEAU_SEMESTRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_LIEN_NIVEAU_SEMESTRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienParcoursAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOJury.TO_LIEN_PARCOURS_ANNEE_KEY);
  }
  
  public void setToLienParcoursAnnee(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOJury.TO_LIEN_PARCOURS_ANNEE_KEY);
  }

  public void setToLienParcoursAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toLienParcoursAnnee from " + toLienParcoursAnnee() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienParcoursAnnee(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLienParcoursAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_LIEN_PARCOURS_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_LIEN_PARCOURS_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienParcoursFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOJury.TO_LIEN_PARCOURS_FORMATION_KEY);
  }
  
  public void setToLienParcoursFormation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOJury.TO_LIEN_PARCOURS_FORMATION_KEY);
  }

  public void setToLienParcoursFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toLienParcoursFormation from " + toLienParcoursFormation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienParcoursFormation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLienParcoursFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_LIEN_PARCOURS_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_LIEN_PARCOURS_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode toSessionPeriode() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode)storedValueForKey(_EOJury.TO_SESSION_PERIODE_KEY);
  }
  
  public void setToSessionPeriode(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode value) {
    takeStoredValueForKey(value, _EOJury.TO_SESSION_PERIODE_KEY);
  }

  public void setToSessionPeriodeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode value) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("updating toSessionPeriode from " + toSessionPeriode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToSessionPeriode(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionPeriode oldValue = toSessionPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOJury.TO_SESSION_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOJury.TO_SESSION_PERIODE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> toJuryConvocations() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation>)storedValueForKey(_EOJury.TO_JURY_CONVOCATIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> toJuryConvocations(EOQualifier qualifier) {
    return toJuryConvocations(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> toJuryConvocations(EOQualifier qualifier, boolean fetch) {
    return toJuryConvocations(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> toJuryConvocations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation.TO_JURY_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation.fetchSco_JuryConvocations(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toJuryConvocations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToJuryConvocations(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation object) {
    includeObjectIntoPropertyWithKey(object, _EOJury.TO_JURY_CONVOCATIONS_KEY);
  }

  public void removeFromToJuryConvocations(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation object) {
    excludeObjectFromPropertyWithKey(object, _EOJury.TO_JURY_CONVOCATIONS_KEY);
  }

  public void addToToJuryConvocationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation object) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("adding " + object + " to toJuryConvocations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToJuryConvocations(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_CONVOCATIONS_KEY);
    }
  }

  public void removeFromToJuryConvocationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation object) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("removing " + object + " from toJuryConvocations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToJuryConvocations(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_CONVOCATIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation createToJuryConvocationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOJury.TO_JURY_CONVOCATIONS_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation) eo;
  }

  public void deleteToJuryConvocationsRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_CONVOCATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToJuryConvocationsRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryConvocation> objects = toJuryConvocations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToJuryConvocationsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> toJuryPersonneses() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes>)storedValueForKey(_EOJury.TO_JURY_PERSONNESES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> toJuryPersonneses(EOQualifier qualifier) {
    return toJuryPersonneses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> toJuryPersonneses(EOQualifier qualifier, boolean fetch) {
    return toJuryPersonneses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> toJuryPersonneses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes.TO_JURY_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes.fetchSco_JuryPersonneses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toJuryPersonneses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToJuryPersonneses(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes object) {
    includeObjectIntoPropertyWithKey(object, _EOJury.TO_JURY_PERSONNESES_KEY);
  }

  public void removeFromToJuryPersonneses(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes object) {
    excludeObjectFromPropertyWithKey(object, _EOJury.TO_JURY_PERSONNESES_KEY);
  }

  public void addToToJuryPersonnesesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes object) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("adding " + object + " to toJuryPersonneses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToJuryPersonneses(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_PERSONNESES_KEY);
    }
  }

  public void removeFromToJuryPersonnesesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes object) {
    if (_EOJury.LOG.isDebugEnabled()) {
      _EOJury.LOG.debug("removing " + object + " from toJuryPersonneses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToJuryPersonneses(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_PERSONNESES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes createToJuryPersonnesesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOJury.TO_JURY_PERSONNESES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes) eo;
  }

  public void deleteToJuryPersonnesesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOJury.TO_JURY_PERSONNESES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToJuryPersonnesesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryPersonnes> objects = toJuryPersonneses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToJuryPersonnesesRelationship(objects.nextElement());
    }
  }


  public static EOJury createSco_Jury(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, Integer idJury
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOJuryEtat toJuryEtat, org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLienFormation) {
    EOJury eo = (EOJury) EOUtilities.createAndInsertInstance(editingContext, _EOJury.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setIdJury(idJury);
		eo.setPersIdCreation(persIdCreation);
    eo.setToJuryEtatRelationship(toJuryEtat);
    eo.setToLienFormationRelationship(toLienFormation);
    return eo;
  }

  public static ERXFetchSpecification<EOJury> fetchSpec() {
    return new ERXFetchSpecification<EOJury>(_EOJury.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOJury> fetchAllSco_Juries(EOEditingContext editingContext) {
    return _EOJury.fetchAllSco_Juries(editingContext, null);
  }

  public static NSArray<EOJury> fetchAllSco_Juries(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJury.fetchSco_Juries(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJury> fetchSco_Juries(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOJury> fetchSpec = new ERXFetchSpecification<EOJury>(_EOJury.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJury> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOJury fetchSco_Jury(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJury.fetchSco_Jury(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJury fetchSco_Jury(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJury> eoObjects = _EOJury.fetchSco_Juries(editingContext, qualifier, null);
    EOJury eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Jury that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJury fetchRequiredSco_Jury(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJury.fetchRequiredSco_Jury(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJury fetchRequiredSco_Jury(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJury eoObject = _EOJury.fetchSco_Jury(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Jury that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJury localInstanceIn(EOEditingContext editingContext, EOJury eo) {
    EOJury localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
