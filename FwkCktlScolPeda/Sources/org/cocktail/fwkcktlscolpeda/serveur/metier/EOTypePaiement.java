package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOTypePaiement extends _EOTypePaiement implements ITypePaiement {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypePaiement.class);
	
	/** Le cache des types de paiement. */
    private static ERXEnterpriseObjectCache<EOTypePaiement> cacheTypePaiement;

	/**
     * @return le cache des {@link EOTypePaiement} par leur code
     */
    private static ERXEnterpriseObjectCache<EOTypePaiement> getTypePaiementCache() {
    	if (cacheTypePaiement == null) {
    		cacheTypePaiement = new ERXEnterpriseObjectCache<EOTypePaiement>(EOTypePaiement.class, CODE_KEY);
    	}
    	return cacheTypePaiement;
    }
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOTypePaiement} de type comptant ({@link EOTypePaiement#CODE_TYPE_COMPTANT}
	 */
	public static ITypePaiement typeComptant(EOEditingContext edc) {
		return getTypePaiementCache().objectForKey(edc, ITypePaiement.CODE_TYPE_COMPTANT);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOTypePaiement} de type différé ({@link EOTypePaiement#CODE_TYPE_DIFFERE}
	 */
	public static ITypePaiement typeDiffere(EOEditingContext edc) {
		return getTypePaiementCache().objectForKey(edc, ITypePaiement.CODE_TYPE_DIFFERE);
	}

	/**
	 * {@inheritDoc}
	 */
    public boolean isDiffere() {
        return ITypePaiement.CODE_TYPE_DIFFERE.equals(code());
    }

    /**
     * {@inheritDoc}
     */
    public boolean isComptant() {
        return ITypePaiement.CODE_TYPE_COMPTANT.equals(code());
    }
	
}
