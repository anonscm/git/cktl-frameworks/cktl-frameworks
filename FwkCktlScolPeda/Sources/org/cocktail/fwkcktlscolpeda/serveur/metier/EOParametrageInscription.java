package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

public class EOParametrageInscription extends _EOParametrageInscription {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParametrageInscription.class);
	
	/**
	 * Paramétrage par défaut
	 */
	public static final String INITIAL = "Initial";
	
}
