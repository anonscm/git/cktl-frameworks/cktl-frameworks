// DO NOT EDIT.  Make changes to EOLienComposer.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOLienComposer extends org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien {
  public static final String ENTITY_NAME = "Sco_LienComposer";

  // Attribute Keys
  public static final ERXKey<Integer> BASE_NOTE = new ERXKey<Integer>("baseNote");
  public static final ERXKey<Integer> CHILD_ID = new ERXKey<Integer>("childId");
  public static final ERXKey<Integer> CHOIX_ECTS = new ERXKey<Integer>("choixECTS");
  public static final ERXKey<Integer> CHOIX_MAX = new ERXKey<Integer>("choixMax");
  public static final ERXKey<Integer> CHOIX_MIN = new ERXKey<Integer>("choixMin");
  public static final ERXKey<java.math.BigDecimal> COEFFICIENT = new ERXKey<java.math.BigDecimal>("coefficient");
  public static final ERXKey<java.math.BigDecimal> DUREE_MAX = new ERXKey<java.math.BigDecimal>("dureeMax");
  public static final ERXKey<java.math.BigDecimal> DUREE_MIN = new ERXKey<java.math.BigDecimal>("dureeMin");
  public static final ERXKey<Integer> EPREUVE = new ERXKey<Integer>("epreuve");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Boolean> OBLIGATOIRE = new ERXKey<Boolean>("obligatoire");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Integer> PARENT_ID = new ERXKey<Integer>("parentId");
  public static final ERXKey<Integer> SANS_COMPOSANT = new ERXKey<Integer>("sansComposant");
  public static final ERXKey<Integer> SANS_NOTE = new ERXKey<Integer>("sansNote");
  public static final ERXKey<Integer> SEUIL = new ERXKey<Integer>("seuil");
  public static final ERXKey<Integer> TYPE_LIEN_ID = new ERXKey<Integer>("typeLienId");
  public static final ERXKey<Float> VALEUR_TEMPS_ETUDIANT = new ERXKey<Float>("valeurTempsEtudiant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE> _CHILD_AE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE>("_childAE");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> _CHILD_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("_childAP");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> _CHILD_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("_childDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> _CHILD_EC = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>("_childEC");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> _CHILD_PARCOURS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("_childParcours");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode> _CHILD_PERIODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode>("_childPeriode");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement> _CHILD_REGROUPEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement>("_childRegroupement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE> _CHILD_UE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE>("_childUE");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> _CHILD_VERSION_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("_childVersionDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE> _PARENT_AE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE>("_parentAE");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> _PARENT_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("_parentAP");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> _PARENT_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("_parentDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC> _PARENT_EC = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC>("_parentEC");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> _PARENT_PARCOURS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("_parentParcours");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode> _PARENT_PERIODE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode>("_parentPeriode");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement> _PARENT_REGROUPEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegroupement>("_parentRegroupement");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE> _PARENT_UE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE>("_parentUE");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome> _PARENT_VERSION_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome>("_parentVersionDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement> CHARGE_ENSEIGNEMENTS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement>("chargeEnseignements");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> CHILD = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("child");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable> CREDITABLES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCreditable>("creditables");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif> DESCRIPTIFS = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODescriptif>("descriptifs");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> PARENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("parent");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien> TYPE_LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien>("typeLien");

  // Attributes
  public static final String BASE_NOTE_KEY = BASE_NOTE.key();
  public static final String CHILD_ID_KEY = CHILD_ID.key();
  public static final String CHOIX_ECTS_KEY = CHOIX_ECTS.key();
  public static final String CHOIX_MAX_KEY = CHOIX_MAX.key();
  public static final String CHOIX_MIN_KEY = CHOIX_MIN.key();
  public static final String COEFFICIENT_KEY = COEFFICIENT.key();
  public static final String DUREE_MAX_KEY = DUREE_MAX.key();
  public static final String DUREE_MIN_KEY = DUREE_MIN.key();
  public static final String EPREUVE_KEY = EPREUVE.key();
  public static final String ID_KEY = ID.key();
  public static final String OBLIGATOIRE_KEY = OBLIGATOIRE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  public static final String PARENT_ID_KEY = PARENT_ID.key();
  public static final String SANS_COMPOSANT_KEY = SANS_COMPOSANT.key();
  public static final String SANS_NOTE_KEY = SANS_NOTE.key();
  public static final String SEUIL_KEY = SEUIL.key();
  public static final String TYPE_LIEN_ID_KEY = TYPE_LIEN_ID.key();
  public static final String VALEUR_TEMPS_ETUDIANT_KEY = VALEUR_TEMPS_ETUDIANT.key();
  // Relationships
  public static final String _CHILD_AE_KEY = _CHILD_AE.key();
  public static final String _CHILD_AP_KEY = _CHILD_AP.key();
  public static final String _CHILD_DIPLOME_KEY = _CHILD_DIPLOME.key();
  public static final String _CHILD_EC_KEY = _CHILD_EC.key();
  public static final String _CHILD_PARCOURS_KEY = _CHILD_PARCOURS.key();
  public static final String _CHILD_PERIODE_KEY = _CHILD_PERIODE.key();
  public static final String _CHILD_REGROUPEMENT_KEY = _CHILD_REGROUPEMENT.key();
  public static final String _CHILD_UE_KEY = _CHILD_UE.key();
  public static final String _CHILD_VERSION_DIPLOME_KEY = _CHILD_VERSION_DIPLOME.key();
  public static final String _PARENT_AE_KEY = _PARENT_AE.key();
  public static final String _PARENT_AP_KEY = _PARENT_AP.key();
  public static final String _PARENT_DIPLOME_KEY = _PARENT_DIPLOME.key();
  public static final String _PARENT_EC_KEY = _PARENT_EC.key();
  public static final String _PARENT_PARCOURS_KEY = _PARENT_PARCOURS.key();
  public static final String _PARENT_PERIODE_KEY = _PARENT_PERIODE.key();
  public static final String _PARENT_REGROUPEMENT_KEY = _PARENT_REGROUPEMENT.key();
  public static final String _PARENT_UE_KEY = _PARENT_UE.key();
  public static final String _PARENT_VERSION_DIPLOME_KEY = _PARENT_VERSION_DIPLOME.key();
  public static final String CHARGE_ENSEIGNEMENTS_KEY = CHARGE_ENSEIGNEMENTS.key();
  public static final String CHILD_KEY = CHILD.key();
  public static final String CREDITABLES_KEY = CREDITABLES.key();
  public static final String DESCRIPTIFS_KEY = DESCRIPTIFS.key();
  public static final String PARENT_KEY = PARENT.key();
  public static final String TYPE_LIEN_KEY = TYPE_LIEN.key();

  private static Logger LOG = Logger.getLogger(_EOLienComposer.class);

  public EOLienComposer localInstanceIn(EOEditingContext editingContext) {
    EOLienComposer localInstance = (EOLienComposer)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer baseNote() {
    return (Integer) storedValueForKey(_EOLienComposer.BASE_NOTE_KEY);
  }

  public void setBaseNote(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating baseNote from " + baseNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.BASE_NOTE_KEY);
  }

  public Integer choixECTS() {
    return (Integer) storedValueForKey(_EOLienComposer.CHOIX_ECTS_KEY);
  }

  public void setChoixECTS(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating choixECTS from " + choixECTS() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.CHOIX_ECTS_KEY);
  }

  public Integer choixMax() {
    return (Integer) storedValueForKey(_EOLienComposer.CHOIX_MAX_KEY);
  }

  public void setChoixMax(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating choixMax from " + choixMax() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.CHOIX_MAX_KEY);
  }

  public Integer choixMin() {
    return (Integer) storedValueForKey(_EOLienComposer.CHOIX_MIN_KEY);
  }

  public void setChoixMin(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating choixMin from " + choixMin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.CHOIX_MIN_KEY);
  }

  public java.math.BigDecimal coefficient() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienComposer.COEFFICIENT_KEY);
  }

  public void setCoefficient(java.math.BigDecimal value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating coefficient from " + coefficient() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.COEFFICIENT_KEY);
  }

  public java.math.BigDecimal dureeMax() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienComposer.DUREE_MAX_KEY);
  }

  public void setDureeMax(java.math.BigDecimal value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating dureeMax from " + dureeMax() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.DUREE_MAX_KEY);
  }

  public java.math.BigDecimal dureeMin() {
    return (java.math.BigDecimal) storedValueForKey(_EOLienComposer.DUREE_MIN_KEY);
  }

  public void setDureeMin(java.math.BigDecimal value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating dureeMin from " + dureeMin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.DUREE_MIN_KEY);
  }

  public Integer epreuve() {
    return (Integer) storedValueForKey(_EOLienComposer.EPREUVE_KEY);
  }

  public void setEpreuve(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating epreuve from " + epreuve() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.EPREUVE_KEY);
  }

  public Boolean obligatoire() {
    return (Boolean) storedValueForKey(_EOLienComposer.OBLIGATOIRE_KEY);
  }

  public void setObligatoire(Boolean value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating obligatoire from " + obligatoire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.OBLIGATOIRE_KEY);
  }

  public Integer sansComposant() {
    return (Integer) storedValueForKey(_EOLienComposer.SANS_COMPOSANT_KEY);
  }

  public void setSansComposant(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating sansComposant from " + sansComposant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.SANS_COMPOSANT_KEY);
  }

  public Integer sansNote() {
    return (Integer) storedValueForKey(_EOLienComposer.SANS_NOTE_KEY);
  }

  public void setSansNote(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating sansNote from " + sansNote() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.SANS_NOTE_KEY);
  }

  public Integer seuil() {
    return (Integer) storedValueForKey(_EOLienComposer.SEUIL_KEY);
  }

  public void setSeuil(Integer value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating seuil from " + seuil() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.SEUIL_KEY);
  }

  public Float valeurTempsEtudiant() {
    return (Float) storedValueForKey(_EOLienComposer.VALEUR_TEMPS_ETUDIANT_KEY);
  }

  public void setValeurTempsEtudiant(Float value) {
    if (_EOLienComposer.LOG.isDebugEnabled()) {
    	_EOLienComposer.LOG.debug( "updating valeurTempsEtudiant from " + valeurTempsEtudiant() + " to " + value);
    }
    takeStoredValueForKey(value, _EOLienComposer.VALEUR_TEMPS_ETUDIANT_KEY);
  }


  public static EOLienComposer createSco_LienComposer(EOEditingContext editingContext, Integer id
, Boolean obligatoire
) {
    EOLienComposer eo = (EOLienComposer) EOUtilities.createAndInsertInstance(editingContext, _EOLienComposer.ENTITY_NAME);    
		eo.setId(id);
		eo.setObligatoire(obligatoire);
    return eo;
  }

  public static ERXFetchSpecification<EOLienComposer> fetchSpecForSco_LienComposer() {
    return new ERXFetchSpecification<EOLienComposer>(_EOLienComposer.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOLienComposer> fetchAllSco_LienComposers(EOEditingContext editingContext) {
    return _EOLienComposer.fetchAllSco_LienComposers(editingContext, null);
  }

  public static NSArray<EOLienComposer> fetchAllSco_LienComposers(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLienComposer.fetchSco_LienComposers(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLienComposer> fetchSco_LienComposers(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOLienComposer> fetchSpec = new ERXFetchSpecification<EOLienComposer>(_EOLienComposer.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLienComposer> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOLienComposer fetchSco_LienComposer(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienComposer.fetchSco_LienComposer(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienComposer fetchSco_LienComposer(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLienComposer> eoObjects = _EOLienComposer.fetchSco_LienComposers(editingContext, qualifier, null);
    EOLienComposer eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_LienComposer that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienComposer fetchRequiredSco_LienComposer(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLienComposer.fetchRequiredSco_LienComposer(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLienComposer fetchRequiredSco_LienComposer(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLienComposer eoObject = _EOLienComposer.fetchSco_LienComposer(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_LienComposer that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLienComposer localInstanceIn(EOEditingContext editingContext, EOLienComposer eo) {
    EOLienComposer localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
