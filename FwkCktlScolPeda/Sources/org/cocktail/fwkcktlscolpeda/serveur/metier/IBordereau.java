package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import com.webobjects.foundation.NSTimestamp;

/**
 * Cette interface représente un bordereau.
 * 
 * @author Pascal MACOUIN
 */
public interface IBordereau {

	/**
	 * @return L'année universitaire pour laquelle est fait le paiement
	 */
	Integer anneeUniversitaire();

	/**
	 * Affecte l'année universitaire pour laquelle est fait le paiement.
	 * @param uneAnneeUniversitaire une annnée
	 */
	void setAnneeUniversitaire(Integer uneAnneeUniversitaire);

	/**
	 * @return La date d'envoi du bordereau à la compta (GFC) (correspond à la date à laquelle à été généré le bordereau coté compta)
	 */
	NSTimestamp dateEnvoiCompta();

	/**
	 * Affecte la date d'envoi du bordereau à la compta (GFC) (correspond à la date à laquelle à été généré le bordereau coté compta).
	 * @param uneDateEnvoiCompta une date
	 */
	void setDateEnvoiCompta(NSTimestamp uneDateEnvoiCompta);

	/**
	 * @return La date du paiement (correspond au critère de sélection des paiements lors de la génération du bordereau)
	 */
	NSTimestamp datePaiement();

	/**
	 * Affecte la date du paiement (correspond au critère de sélection des paiements lors de la génération du bordereau)
	 * @param uneDatePaiement une date de paiement
	 */
	void setDatePaiement(NSTimestamp uneDatePaiement);

	/**
	 * @return Le numéro du bordereau (clé permettant d'accéder au bordereau GFC)
	 */
	Integer numeroBordereau();

	/**
	 * Affecte le numéro du bordereau (clé permettant d'accéder au bordereau GFC)
	 * @param unNumeroBordereau un numéro de bordereau
	 */
	void setNumeroBordereau(Integer unNumeroBordereau);

	/**
	 * @return Le code de GESTION sur lequel est affecté ce bordereau (clé permettant d'accéder au bordereau GFC)
	 */
	String codeGestion();

	/**
	 * Affecte le code de GESTION sur lequel est affecté ce bordereau (clé permettant d'accéder au bordereau GFC).
	 * @param unCodeGestion un code gestion
	 */
	void setCodeGestion(String unCodeGestion);
	
	/**
	 * @return La liste des paiements de ce bordereau
	 */
	List<? extends IScoPaiement> toPaiements();

	/**
	 * @param unModePaiement un mode de paiement
	 * @return le total payé de tous les paiements ayant ce mode de paiement pour ce bordereau
	 */
	Number getTotalPaye(IModePaiement unModePaiement);
	
	/**
	 * @param unTypePaiement un type de paiement
	 * @return le total payé de tous les paiements ayant ce type de paiement pour ce bordereau
	 */
	Number getTotalPaye(ITypePaiement unTypePaiement);

	/**
	 * @return Le type de bordereau
	 */
	ITypeBordereau toTypeBordereau();
	
	/**
	 * @param typeBordereau Un type de bordereau
	 */
	void setToTypeBordereauRelationship(ITypeBordereau typeBordereau);

}