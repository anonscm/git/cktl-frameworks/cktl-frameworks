package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;

/**
 * Inscription administrative 
 */
public interface IScoInscription  extends IInscription {
	/**
	

	/**
	 * @return l'étudiant inscrit
	 */
	IEtudiant toEtudiant();
	
	/**

	/**
	 * @return la liste des liens parents
	 */
	List<? extends IInscriptionPedagogique> toInscriptionsPedagogiques();
	

	/**
	 * Lien vers l'étudiant
	 * @param Etudiant
	 */
	void  setToEtudiantRelationship(IEtudiant Etudiant);

	
}
