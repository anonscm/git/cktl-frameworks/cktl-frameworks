// DO NOT EDIT.  Make changes to EOFormule.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOFormule extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_Formule";

  // Attribute Keys
  public static final ERXKey<String> FORMULE = new ERXKey<String>("formule");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> MOYENNE_PONDEREE = new ERXKey<Integer>("moyennePonderee");
  public static final ERXKey<Integer> SESSION_COMPOSANT_ID = new ERXKey<Integer>("sessionComposantId");
  public static final ERXKey<Integer> TYPE_FORMULE_ID = new ERXKey<Integer>("typeFormuleId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant> SESSION_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant>("sessionComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule> TYPE_FORMULE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule>("typeFormule");

  // Attributes
  public static final String FORMULE_KEY = FORMULE.key();
  public static final String ID_KEY = ID.key();
  public static final String MOYENNE_PONDEREE_KEY = MOYENNE_PONDEREE.key();
  public static final String SESSION_COMPOSANT_ID_KEY = SESSION_COMPOSANT_ID.key();
  public static final String TYPE_FORMULE_ID_KEY = TYPE_FORMULE_ID.key();
  // Relationships
  public static final String SESSION_COMPOSANT_KEY = SESSION_COMPOSANT.key();
  public static final String TYPE_FORMULE_KEY = TYPE_FORMULE.key();

  private static Logger LOG = Logger.getLogger(_EOFormule.class);

  public EOFormule localInstanceIn(EOEditingContext editingContext) {
    EOFormule localInstance = (EOFormule)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String formule() {
    return (String) storedValueForKey(_EOFormule.FORMULE_KEY);
  }

  public void setFormule(String value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
    	_EOFormule.LOG.debug( "updating formule from " + formule() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFormule.FORMULE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOFormule.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
    	_EOFormule.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFormule.ID_KEY);
  }

  public Integer moyennePonderee() {
    return (Integer) storedValueForKey(_EOFormule.MOYENNE_PONDEREE_KEY);
  }

  public void setMoyennePonderee(Integer value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
    	_EOFormule.LOG.debug( "updating moyennePonderee from " + moyennePonderee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFormule.MOYENNE_PONDEREE_KEY);
  }

  public Integer sessionComposantId() {
    return (Integer) storedValueForKey(_EOFormule.SESSION_COMPOSANT_ID_KEY);
  }

  public void setSessionComposantId(Integer value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
    	_EOFormule.LOG.debug( "updating sessionComposantId from " + sessionComposantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFormule.SESSION_COMPOSANT_ID_KEY);
  }

  public Integer typeFormuleId() {
    return (Integer) storedValueForKey(_EOFormule.TYPE_FORMULE_ID_KEY);
  }

  public void setTypeFormuleId(Integer value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
    	_EOFormule.LOG.debug( "updating typeFormuleId from " + typeFormuleId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFormule.TYPE_FORMULE_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant sessionComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant)storedValueForKey(_EOFormule.SESSION_COMPOSANT_KEY);
  }
  
  public void setSessionComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant value) {
    takeStoredValueForKey(value, _EOFormule.SESSION_COMPOSANT_KEY);
  }

  public void setSessionComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
      _EOFormule.LOG.debug("updating sessionComposant from " + sessionComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setSessionComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant oldValue = sessionComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFormule.SESSION_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFormule.SESSION_COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule typeFormule() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule)storedValueForKey(_EOFormule.TYPE_FORMULE_KEY);
  }
  
  public void setTypeFormule(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule value) {
    takeStoredValueForKey(value, _EOFormule.TYPE_FORMULE_KEY);
  }

  public void setTypeFormuleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule value) {
    if (_EOFormule.LOG.isDebugEnabled()) {
      _EOFormule.LOG.debug("updating typeFormule from " + typeFormule() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeFormule(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule oldValue = typeFormule();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFormule.TYPE_FORMULE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFormule.TYPE_FORMULE_KEY);
    }
  }
  

  public static EOFormule createSco_Formule(EOEditingContext editingContext, Integer id
) {
    EOFormule eo = (EOFormule) EOUtilities.createAndInsertInstance(editingContext, _EOFormule.ENTITY_NAME);    
		eo.setId(id);
    return eo;
  }

  public static ERXFetchSpecification<EOFormule> fetchSpec() {
    return new ERXFetchSpecification<EOFormule>(_EOFormule.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOFormule> fetchAllSco_Formules(EOEditingContext editingContext) {
    return _EOFormule.fetchAllSco_Formules(editingContext, null);
  }

  public static NSArray<EOFormule> fetchAllSco_Formules(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFormule.fetchSco_Formules(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFormule> fetchSco_Formules(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOFormule> fetchSpec = new ERXFetchSpecification<EOFormule>(_EOFormule.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFormule> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOFormule fetchSco_Formule(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFormule.fetchSco_Formule(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFormule fetchSco_Formule(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFormule> eoObjects = _EOFormule.fetchSco_Formules(editingContext, qualifier, null);
    EOFormule eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_Formule that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFormule fetchRequiredSco_Formule(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFormule.fetchRequiredSco_Formule(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFormule fetchRequiredSco_Formule(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFormule eoObject = _EOFormule.fetchSco_Formule(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_Formule that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFormule localInstanceIn(EOEditingContext editingContext, EOFormule eo) {
    EOFormule localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
