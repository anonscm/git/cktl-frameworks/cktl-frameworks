// DO NOT EDIT.  Make changes to EOInscriptionPedagogiqueElement.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOInscriptionPedagogiqueElement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_InscriptionPedagogiqueElement";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> ID_LIEN = new ERXKey<Integer>("idLien");
  public static final ERXKey<String> ID_LIEN_CONTEXTE = new ERXKey<String>("idLienContexte");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> VALIDE = new ERXKey<String>("valide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique> TO_INSCRIPTION_PEDAGOGIQUE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique>("toInscriptionPedagogique");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien> TO_LIEN = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien>("toLien");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation> TO_LIEN_CONSULTATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation>("toLienConsultation");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> TO_NOTES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote>("toNotes");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda> TO_TYPE_INSCRIPTION_PEDA = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda>("toTypeInscriptionPeda");

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String ID_LIEN_KEY = ID_LIEN.key();
  public static final String ID_LIEN_CONTEXTE_KEY = ID_LIEN_CONTEXTE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String VALIDE_KEY = VALIDE.key();
  // Relationships
  public static final String TO_INSCRIPTION_PEDAGOGIQUE_KEY = TO_INSCRIPTION_PEDAGOGIQUE.key();
  public static final String TO_LIEN_KEY = TO_LIEN.key();
  public static final String TO_LIEN_CONSULTATION_KEY = TO_LIEN_CONSULTATION.key();
  public static final String TO_NOTES_KEY = TO_NOTES.key();
  public static final String TO_TYPE_INSCRIPTION_PEDA_KEY = TO_TYPE_INSCRIPTION_PEDA.key();

  private static Logger LOG = Logger.getLogger(_EOInscriptionPedagogiqueElement.class);

  public EOInscriptionPedagogiqueElement localInstanceIn(EOEditingContext editingContext) {
    EOInscriptionPedagogiqueElement localInstance = (EOInscriptionPedagogiqueElement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_EOInscriptionPedagogiqueElement.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EOInscriptionPedagogiqueElement.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.D_MODIFICATION_KEY);
  }

  public Integer idLien() {
    return (Integer) storedValueForKey(_EOInscriptionPedagogiqueElement.ID_LIEN_KEY);
  }

  public void setIdLien(Integer value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating idLien from " + idLien() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.ID_LIEN_KEY);
  }

  public String idLienContexte() {
    return (String) storedValueForKey(_EOInscriptionPedagogiqueElement.ID_LIEN_CONTEXTE_KEY);
  }

  public void setIdLienContexte(String value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating idLienContexte from " + idLienContexte() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.ID_LIEN_CONTEXTE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(_EOInscriptionPedagogiqueElement.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EOInscriptionPedagogiqueElement.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.PERS_ID_MODIFICATION_KEY);
  }

  public String valide() {
    return (String) storedValueForKey(_EOInscriptionPedagogiqueElement.VALIDE_KEY);
  }

  public void setValide(String value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
    	_EOInscriptionPedagogiqueElement.LOG.debug( "updating valide from " + valide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.VALIDE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique toInscriptionPedagogique() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique)storedValueForKey(_EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE_KEY);
  }
  
  public void setToInscriptionPedagogique(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE_KEY);
  }

  public void setToInscriptionPedagogiqueRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("updating toInscriptionPedagogique from " + toInscriptionPedagogique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToInscriptionPedagogique(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique oldValue = toInscriptionPedagogique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogiqueElement.TO_INSCRIPTION_PEDAGOGIQUE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien toLien() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien)storedValueForKey(_EOInscriptionPedagogiqueElement.TO_LIEN_KEY);
  }
  
  public void setToLien(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.TO_LIEN_KEY);
  }

  public void setToLienRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("updating toLien from " + toLien() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLien(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien oldValue = toLien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogiqueElement.TO_LIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogiqueElement.TO_LIEN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation toLienConsultation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation)storedValueForKey(_EOInscriptionPedagogiqueElement.TO_LIEN_CONSULTATION_KEY);
  }
  
  public void setToLienConsultation(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.TO_LIEN_CONSULTATION_KEY);
  }

  public void setToLienConsultationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("updating toLienConsultation from " + toLienConsultation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToLienConsultation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation oldValue = toLienConsultation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogiqueElement.TO_LIEN_CONSULTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogiqueElement.TO_LIEN_CONSULTATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda toTypeInscriptionPeda() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda)storedValueForKey(_EOInscriptionPedagogiqueElement.TO_TYPE_INSCRIPTION_PEDA_KEY);
  }
  
  public void setToTypeInscriptionPeda(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda value) {
    takeStoredValueForKey(value, _EOInscriptionPedagogiqueElement.TO_TYPE_INSCRIPTION_PEDA_KEY);
  }

  public void setToTypeInscriptionPedaRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda value) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("updating toTypeInscriptionPeda from " + toTypeInscriptionPeda() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeInscriptionPeda(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionPeda oldValue = toTypeInscriptionPeda();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOInscriptionPedagogiqueElement.TO_TYPE_INSCRIPTION_PEDA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOInscriptionPedagogiqueElement.TO_TYPE_INSCRIPTION_PEDA_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> toNotes() {
    return (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote>)storedValueForKey(_EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> toNotes(EOQualifier qualifier) {
    return toNotes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> toNotes(EOQualifier qualifier, boolean fetch) {
    return toNotes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> toNotes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote.TO_INSCRIPTION_PEDAGOGIQUE_ELEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlscolpeda.serveur.metier.EONote.fetchSco_Notes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toNotes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToNotes(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote object) {
    includeObjectIntoPropertyWithKey(object, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
  }

  public void removeFromToNotes(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote object) {
    excludeObjectFromPropertyWithKey(object, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
  }

  public void addToToNotesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote object) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("adding " + object + " to toNotes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToNotes(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
    }
  }

  public void removeFromToNotesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote object) {
    if (_EOInscriptionPedagogiqueElement.LOG.isDebugEnabled()) {
      _EOInscriptionPedagogiqueElement.LOG.debug("removing " + object + " from toNotes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToNotes(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
    }
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONote createToNotesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlscolpeda.serveur.metier.EONote.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONote) eo;
  }

  public void deleteToNotesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONote object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOInscriptionPedagogiqueElement.TO_NOTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToNotesRelationships() {
    Enumeration<org.cocktail.fwkcktlscolpeda.serveur.metier.EONote> objects = toNotes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToNotesRelationship(objects.nextElement());
    }
  }


  public static EOInscriptionPedagogiqueElement createSco_InscriptionPedagogiqueElement(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String idLienContexte
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique toInscriptionPedagogique, org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienConsultation toLienConsultation) {
    EOInscriptionPedagogiqueElement eo = (EOInscriptionPedagogiqueElement) EOUtilities.createAndInsertInstance(editingContext, _EOInscriptionPedagogiqueElement.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdLienContexte(idLienContexte);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToInscriptionPedagogiqueRelationship(toInscriptionPedagogique);
    eo.setToLienConsultationRelationship(toLienConsultation);
    return eo;
  }

  public static ERXFetchSpecification<EOInscriptionPedagogiqueElement> fetchSpec() {
    return new ERXFetchSpecification<EOInscriptionPedagogiqueElement>(_EOInscriptionPedagogiqueElement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOInscriptionPedagogiqueElement> fetchAllSco_InscriptionPedagogiqueElements(EOEditingContext editingContext) {
    return _EOInscriptionPedagogiqueElement.fetchAllSco_InscriptionPedagogiqueElements(editingContext, null);
  }

  public static NSArray<EOInscriptionPedagogiqueElement> fetchAllSco_InscriptionPedagogiqueElements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOInscriptionPedagogiqueElement> fetchSco_InscriptionPedagogiqueElements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOInscriptionPedagogiqueElement> fetchSpec = new ERXFetchSpecification<EOInscriptionPedagogiqueElement>(_EOInscriptionPedagogiqueElement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOInscriptionPedagogiqueElement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOInscriptionPedagogiqueElement fetchSco_InscriptionPedagogiqueElement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscriptionPedagogiqueElement fetchSco_InscriptionPedagogiqueElement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOInscriptionPedagogiqueElement> eoObjects = _EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElements(editingContext, qualifier, null);
    EOInscriptionPedagogiqueElement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_InscriptionPedagogiqueElement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscriptionPedagogiqueElement fetchRequiredSco_InscriptionPedagogiqueElement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOInscriptionPedagogiqueElement.fetchRequiredSco_InscriptionPedagogiqueElement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOInscriptionPedagogiqueElement fetchRequiredSco_InscriptionPedagogiqueElement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOInscriptionPedagogiqueElement eoObject = _EOInscriptionPedagogiqueElement.fetchSco_InscriptionPedagogiqueElement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_InscriptionPedagogiqueElement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOInscriptionPedagogiqueElement localInstanceIn(EOEditingContext editingContext, EOInscriptionPedagogiqueElement eo) {
    EOInscriptionPedagogiqueElement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
