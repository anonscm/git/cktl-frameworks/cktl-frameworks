package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * 
 * @author isabelle
 *
 */
public class EOJuryEtat extends _EOJuryEtat implements IJuryEtat {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOJuryEtat.class);
	
	   private static ERXEnterpriseObjectCache<EOJuryEtat> juryEtatCache;
	    
	    /**
	     * @return le cache des {@link EOJuryEtat} par leur type
	     */
	    public static ERXEnterpriseObjectCache<EOJuryEtat> getJuryEtatCache() {
	    	if (juryEtatCache == null) {
	    		juryEtatCache = new ERXEnterpriseObjectCache<EOJuryEtat>(EOJuryEtat.class, CODE_KEY);
	    	}
	    	return juryEtatCache;
	    }
	    
	    /**
	     * @param typeFormationCache le cache a setter
	     */
	    public static void setJuryEtatCache(ERXEnterpriseObjectCache<EOJuryEtat> juryEtatCache) {
	    	EOJuryEtat.juryEtatCache = juryEtatCache;
	    }
	    
		/**
		 * @param edc l'editingContext
		 * @return le {@link EOJuryEtat} "juryEnCours"
		 */
		public static EOJuryEtat juryEnCours(EOEditingContext edc) {
			return getJuryEtatCache().objectForKey(edc, JE_ENCOURS);
		}
	    
	    /**
		 * @param edc l'editingContext
		 * @return le {@link EOJuryEtat} "juryConvoque"
		 */
		public static EOJuryEtat juryConvoque(EOEditingContext edc) {
			return getJuryEtatCache().objectForKey(edc,JE_CONVOQUE);
		}
		
	    /**
		 * @param edc l'editingContext
		 * @return le {@link EOJuryEtat} "juryConvoque"
		 */
		public static EOJuryEtat juryClos(EOEditingContext edc) {
			return getJuryEtatCache().objectForKey(edc,JE_CLOS);
		}
}
