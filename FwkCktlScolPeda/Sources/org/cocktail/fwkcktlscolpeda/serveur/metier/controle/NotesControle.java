/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlscolpeda.serveur.metier.controle;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurFormuleHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurMoyenneAbstract;
import org.cocktail.fwkcktlscolpeda.serveur.metier.moyenne.CalculateurMoyennePonderee;


/**
 * Classe permettant le controle sur les notes 
 * @author isabelle
 *
 */
public class NotesControle {
	

	/**
	 * @param note note que l'on controle
	 * @return true si la note est inférieure ou égale à la base note
	 */
	public Boolean controleNoteBaseNote(INote note) {
		Boolean isOk = true;
		EOComposant composant = (EOComposant) note.toInscriptionPedagogiqueElement().toLien().child();
		if (ITypeComposant.TYPEAE_NOM.equals(composant.typeComposant().libelle())) {
			if (((EOAE) composant).baseNote() != null && note.noteInitiale() != null) {
				isOk = (new BigDecimal(((EOAE) composant).baseNote()).compareTo(note.noteInitiale()) >= 0);
			} else {
				isOk = (new BigDecimal(CalculateurFormuleHelper.BASE_NOTE_DEFAUT).compareTo(note.noteInitiale()) >= 0);
			}
		} else if (ITypeComposant.TYPEEC_NOM.equals(composant.typeComposant().libelle()) || ITypeComposant.TYPEUE_NOM.equals(composant.typeComposant().libelle())) {
			if (composant.creditable() != null && composant.creditable().baseNote() != null && note.noteInitiale() != null) {
				isOk = (new BigDecimal(composant.creditable().baseNote()).compareTo(note.noteInitiale()) >= 0);
			} else {
				isOk = (new BigDecimal(CalculateurFormuleHelper.BASE_NOTE_DEFAUT).compareTo(note.noteInitiale()) >= 0);
			}
		}
		return isOk;
	}
	
	/**
	 * @param note note que l'on controle
	 * @return true si la note est inférieure ou égale à la base note
	 */
	public Boolean controleNoteBaseNote(INote note, BigDecimal valeurSaisie) {
		Boolean isOk = true;
		EOComposant composant = (EOComposant) note.toInscriptionPedagogiqueElement().toLien().child();
		if (ITypeComposant.TYPEAE_NOM.equals(composant.typeComposant().libelle())) {
			if (((EOAE) composant).baseNote() != null && note.noteInitiale() != null) {
				isOk = (new BigDecimal(((EOAE) composant).baseNote()).compareTo(valeurSaisie) >= 0);
			} else {
				isOk = (new BigDecimal(CalculateurFormuleHelper.BASE_NOTE_DEFAUT).compareTo(valeurSaisie) >= 0);
			}
		} else if (ITypeComposant.TYPEEC_NOM.equals(composant.typeComposant().libelle()) || ITypeComposant.TYPEUE_NOM.equals(composant.typeComposant().libelle())) {
			if (composant.creditable() != null && composant.creditable().baseNote() != null && note.noteInitiale() != null) {
				isOk = (new BigDecimal(composant.creditable().baseNote()).compareTo(valeurSaisie) >= 0);
			} else {
				isOk = (new BigDecimal(CalculateurFormuleHelper.BASE_NOTE_DEFAUT).compareTo(valeurSaisie) >= 0);
			}
		}
		return isOk;
	}
	
}
