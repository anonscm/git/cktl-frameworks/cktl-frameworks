package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;


/**
 * {@inheritDoc}
 */
public class EOResponsableComposant extends _EOResponsableComposant implements IResponsableComposant {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOResponsableComposant.class);
	private EOIndividu individu = null;
	private EOStructure structure = null;

	/**
	 * @param editingContext contexte d'édition
	 * @param composant composant auquel on associe le reposable
	 * @param responsablePersId persId de l'individu ou de la composante qui est reponsable du composant
	 * @return un responsable composant
	 */
	public static EOResponsableComposant create(EOEditingContext editingContext, EOComposant composant, Integer responsablePersId) {
		EOResponsableComposant responsableComposant = (EOResponsableComposant) EOUtilities.createAndInsertInstance(editingContext,
		    EOResponsableComposant.ENTITY_NAME);
		responsableComposant.setComposantRelationship(composant);
		responsableComposant.setResponsablePersId(responsablePersId);
		return responsableComposant;
	}

	/**
	 * Suppression d'un responsable sur un composant et ses rôles.
	 * @throws Exception : renvoie une exception en cas d'anomalie
	 */
	public void supprimer() throws Exception {
		// Supprimer tous les roles
	    deleteAllRolesRelationships();
		
		// Supprimer la relation avec le composant
		removeObjectFromBothSidesOfRelationshipWithKey(composant(), _EOResponsableComposant.COMPOSANT_KEY);

		// Supprimer l'objet
		delete();
	}
	
	/**
	 * @param autreComposant le composant sur lequel on copie le responsable et ses roles
	 * @return le responsable sur le nouveau composant
	 */
	public IResponsableComposant copyToComposant(IComposant autreComposant) {
		EOResponsableComposant responsablecomposant = EOResponsableComposant.create(editingContext(), (EOComposant) autreComposant, this.responsablePersId());
		for (IResponsableComposantRole role : roles()) {
			responsablecomposant.addToRolesRelationship(role.copy(responsablecomposant));
		}
		return responsablecomposant;
	}

	/**
	 * {@inheritDoc}
	 */
	public IPersonne responsable() {
		if (individu != null || structure != null) {
			if (individu != null) {
				return individu;
			}
			return structure;
		}

		individu = chercheIndividu(responsablePersId());

		if (individu != null) {
			return individu;
		}

		structure = chercheStructure(responsablePersId());
		if (structure != null) {
			return structure;
		}

		return null;
	}

	public Integer composantId() {
		return composant().id();
	}
	
	private EOStructure chercheStructure(Integer responsablePersId) {
		return EOStructure.fetchByQualifier(editingContext(), EOStructure.PERS_ID.eq(responsablePersId));
	}

	private EOIndividu chercheIndividu(Integer responsablePersId) {
		return EOIndividu.fetchByQualifier(editingContext(), EOIndividu.PERS_ID.eq(responsablePersId));
	}

}
