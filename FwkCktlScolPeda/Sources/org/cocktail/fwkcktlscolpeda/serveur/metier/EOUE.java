package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.validation.ERXValidationFactory;

/**
 * Gestion des UE
 */
public class EOUE extends _EOUE implements IUE {

	private static final long serialVersionUID = 1L;

	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeUE(editingContext());
	}
	
	@Override
	public void initialise() {
		setSansComposant(0);
	}

	/**
	 * Verifie si un UE a des composants de type EC, UE ou regroupement
	 * @param edc : contexte d'edition.
	 * @return true si l'UE a des composants interdits, faux sinon
	 */
	public boolean hasComposantInterdit(EOEditingContext edc) {

		boolean hasComposantInterdit = false;

		NSArray<EOLien> liens = this.liensParents();
		for (EOLien lien : liens) {
			EOTypeComposant typeComposant = lien.child().typeComposant();
			if (typeComposant.equals(EOTypeComposant.typeEC(edc)) || typeComposant.equals(EOTypeComposant.typeUE(edc))
			        || typeComposant.equals(EOTypeComposant.typeRegroupement(edc))) {
				hasComposantInterdit = true;
			}
		}

		return hasComposantInterdit;
	}
		
	/**
	 * {@inheritDoc}
	 */
	public EOUE copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			//TODO : récupérer le persId par injection du user
			EOUE ue = EOComposantFactory.createComposant(EOUE.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(ue);
			return ue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
     * Valide choixMax
     * 
     * @param choixMax : le choix max
     * @return Object : le choix max
     * @throws ValidationException : exception
     */
	public Object validateChoixMax(Object choixMax) throws ValidationException {
    	if (choixMax == null && this.choixMin() != null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_UE.choixMax.NullAlorsQueChoixMinNonNull");
    	}
    	
    	if (choixMax != null && this.choixMin() == null) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_UE.choixMax.NonNullAlorsQueChoixMinNull");
    	}
    	
    	/*if (choixMax != null && this.choixMin() != null) {
    		if ((new BigDecimal(choixMax)).compareTo(new BigDecimal(this.choixMin())) < 0) {
    			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Regroupement.choixMax.InferieurAChoixMin");
    		}
    	}*/
		
		return choixMax;
    }

	/**
	 * @return IUE : le composant suivant associe au composant si il existe
	 */
	public IUE getComposantAvecOrigine() {
		if(super.getComposantSuivant() != null) {
			return ((IUE) (super.getComposantSuivant()));
		}
		
		return null;
	}
	
	/**
	 * @return IUE : le composant d'origine
	 */
	public IUE getComposantOrigine() {
		return (IUE) composantOrigine();
	}
	
	private void assignTo(EOUE ue) {
		super.assignTo(ue);
		ue.setChoixECTS(this.choixECTS());
		ue.setChoixMax(this.choixMax());
		ue.setChoixMin(this.choixMin());
		ue.setSansComposant(this.sansComposant());
		ue.setToCnuRelationship(this.toCnu());
		
	}
}
