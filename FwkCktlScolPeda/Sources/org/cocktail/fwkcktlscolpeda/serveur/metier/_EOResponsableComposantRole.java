// DO NOT EDIT.  Make changes to EOResponsableComposantRole.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOResponsableComposantRole extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ResponsableComposantRole";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant> RESPONSABLE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant>("responsableComposant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable> ROLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable>("role");

  // Attributes
  // Relationships
  public static final String RESPONSABLE_COMPOSANT_KEY = RESPONSABLE_COMPOSANT.key();
  public static final String ROLE_KEY = ROLE.key();

  private static Logger LOG = Logger.getLogger(_EOResponsableComposantRole.class);

  public EOResponsableComposantRole localInstanceIn(EOEditingContext editingContext) {
    EOResponsableComposantRole localInstance = (EOResponsableComposantRole)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant responsableComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant)storedValueForKey(_EOResponsableComposantRole.RESPONSABLE_COMPOSANT_KEY);
  }
  
  public void setResponsableComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant value) {
    takeStoredValueForKey(value, _EOResponsableComposantRole.RESPONSABLE_COMPOSANT_KEY);
  }

  public void setResponsableComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant value) {
    if (_EOResponsableComposantRole.LOG.isDebugEnabled()) {
      _EOResponsableComposantRole.LOG.debug("updating responsableComposant from " + responsableComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setResponsableComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOResponsableComposant oldValue = responsableComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOResponsableComposantRole.RESPONSABLE_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOResponsableComposantRole.RESPONSABLE_COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable role() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable)storedValueForKey(_EOResponsableComposantRole.ROLE_KEY);
  }
  
  public void setRole(org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable value) {
    takeStoredValueForKey(value, _EOResponsableComposantRole.ROLE_KEY);
  }

  public void setRoleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable value) {
    if (_EOResponsableComposantRole.LOG.isDebugEnabled()) {
      _EOResponsableComposantRole.LOG.debug("updating role from " + role() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setRole(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable oldValue = role();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOResponsableComposantRole.ROLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOResponsableComposantRole.ROLE_KEY);
    }
  }
  

  public static EOResponsableComposantRole createSco_ResponsableComposantRole(EOEditingContext editingContext, org.cocktail.fwkcktlscolpeda.serveur.metier.EORoleResponsable role) {
    EOResponsableComposantRole eo = (EOResponsableComposantRole) EOUtilities.createAndInsertInstance(editingContext, _EOResponsableComposantRole.ENTITY_NAME);    
    eo.setRoleRelationship(role);
    return eo;
  }

  public static ERXFetchSpecification<EOResponsableComposantRole> fetchSpec() {
    return new ERXFetchSpecification<EOResponsableComposantRole>(_EOResponsableComposantRole.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOResponsableComposantRole> fetchAllSco_ResponsableComposantRoles(EOEditingContext editingContext) {
    return _EOResponsableComposantRole.fetchAllSco_ResponsableComposantRoles(editingContext, null);
  }

  public static NSArray<EOResponsableComposantRole> fetchAllSco_ResponsableComposantRoles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOResponsableComposantRole.fetchSco_ResponsableComposantRoles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOResponsableComposantRole> fetchSco_ResponsableComposantRoles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOResponsableComposantRole> fetchSpec = new ERXFetchSpecification<EOResponsableComposantRole>(_EOResponsableComposantRole.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOResponsableComposantRole> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOResponsableComposantRole fetchSco_ResponsableComposantRole(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResponsableComposantRole.fetchSco_ResponsableComposantRole(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResponsableComposantRole fetchSco_ResponsableComposantRole(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOResponsableComposantRole> eoObjects = _EOResponsableComposantRole.fetchSco_ResponsableComposantRoles(editingContext, qualifier, null);
    EOResponsableComposantRole eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ResponsableComposantRole that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResponsableComposantRole fetchRequiredSco_ResponsableComposantRole(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResponsableComposantRole.fetchRequiredSco_ResponsableComposantRole(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResponsableComposantRole fetchRequiredSco_ResponsableComposantRole(EOEditingContext editingContext, EOQualifier qualifier) {
    EOResponsableComposantRole eoObject = _EOResponsableComposantRole.fetchSco_ResponsableComposantRole(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ResponsableComposantRole that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResponsableComposantRole localInstanceIn(EOEditingContext editingContext, EOResponsableComposantRole eo) {
    EOResponsableComposantRole localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
