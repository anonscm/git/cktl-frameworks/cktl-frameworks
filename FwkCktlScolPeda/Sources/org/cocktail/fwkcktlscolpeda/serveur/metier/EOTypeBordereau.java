package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

/**
 * Cette classe représente un type de bordereau.
 * 
 * @author Pascal MACOUIN
 */
public class EOTypeBordereau extends _EOTypeBordereau implements ITypeBordereau {
	private static final long serialVersionUID = -3616597534445140193L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeBordereau.class);
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isPaiement() {
		return ITypeBordereau.CODE_PAIEMENT.equals(code());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isRemboursement() {
		return ITypeBordereau.CODE_REMBOURSEMENT.equals(code());
	}
	
}
