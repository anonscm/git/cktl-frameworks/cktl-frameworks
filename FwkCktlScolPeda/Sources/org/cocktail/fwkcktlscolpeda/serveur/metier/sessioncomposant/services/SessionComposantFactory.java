package org.cocktail.fwkcktlscolpeda.serveur.metier.sessioncomposant.services;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionComposant;

/**
 * Cette interface représente la fabrique des {@link ISessionComposant}.
 * 
 * @author Pascal MACOUIN
 */
public interface SessionComposantFactory {
	/**
	 * Créer les sessions et les formules par défauts associées à ce composant.
	 * 
	 * @param unComposant Le composant associé aux sessions créés
	 * @return Les sessions créées pour ce composant
	 */
	List<ISessionComposant> creerSessionsComposant(IComposant unComposant);
}
