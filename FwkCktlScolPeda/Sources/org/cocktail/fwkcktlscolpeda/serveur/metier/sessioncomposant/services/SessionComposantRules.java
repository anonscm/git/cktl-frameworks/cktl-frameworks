package org.cocktail.fwkcktlscolpeda.serveur.metier.sessioncomposant.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;


/**
 *  Détermine les règles qui déterminent quelles formules sont associées à tel type de composant
 *  
 */
public class SessionComposantRules {
	
	private List<String> typesFormulesDiplome;
	private List<String> typesFormulesPeriode;
	private List<String> typesFormulesParcours;
	private List<String> typesFormulesRegroupement;
	private List<String> typesFormulesUE;
	private List<String> typesFormulesEC;

	/**
	 * @return les régles d'association des formules aux composants
	 */
	public Map<String, List<String>> getRules() {
		Map<String, List<String>> rules = new HashedMap();
		
		rules.put(ITypeComposant.TYPEVERSIONDIPLOME_NOM, getTypesFormulesVersionDiplome());
		rules.put(ITypeComposant.TYPEPERIODE_NOM, getTypesFormulesPeriode());
		rules.put(ITypeComposant.TYPEPARCOURS_NOM, getTypesFormulesParcours());
		rules.put(ITypeComposant.TYPEREGROUPEMENT_NOM, getTypesFormulesRegroupement());
		rules.put(ITypeComposant.TYPEUE_NOM, getTypesFormulesUE());
		rules.put(ITypeComposant.TYPEEC_NOM, getTypesFormulesEC());
		
		return rules;
	}
	
	private List<String> getTypesFormulesVersionDiplome() {
		if (typesFormulesDiplome == null) {
			typesFormulesDiplome = new ArrayList<String>();
			typesFormulesDiplome.add(ITypeFormule.TYPEMOYENNE_LBL);
			typesFormulesDiplome.add(ITypeFormule.TYPEADMISSION_LBL);
			typesFormulesDiplome.add(ITypeFormule.TYPECOMPENSATION_LBL);
		}
		
		return typesFormulesDiplome;
	}
	
	private List<String> getTypesFormulesPeriode() {
		if (typesFormulesPeriode == null) {
			typesFormulesPeriode = new ArrayList<String>();
			typesFormulesPeriode.add(ITypeFormule.TYPEMOYENNE_LBL);	
			typesFormulesPeriode.add(ITypeFormule.TYPEADMISSION_LBL);
			typesFormulesPeriode.add(ITypeFormule.TYPECOMPENSATION_LBL);
			typesFormulesPeriode.add(ITypeFormule.TYPEDETTES_LBL);
		}
		
		return typesFormulesPeriode;
	}
	
	private List<String> getTypesFormulesParcours() {
		if (typesFormulesParcours == null) {
			typesFormulesParcours = new ArrayList<String>();
			typesFormulesParcours.add(ITypeFormule.TYPEMOYENNE_LBL);	
		}
		
		return typesFormulesParcours;
	}
	
	private List<String> getTypesFormulesRegroupement() {
		if (typesFormulesRegroupement == null) {
			typesFormulesRegroupement = new ArrayList<String>();
			typesFormulesRegroupement.add(ITypeFormule.TYPEMOYENNE_LBL);	
		}
		
		return typesFormulesRegroupement;
	}
	
	private List<String> getTypesFormulesUE() {
		if (typesFormulesUE == null) {
			typesFormulesUE = new ArrayList<String>();
			typesFormulesUE.add(ITypeFormule.TYPEMOYENNE_LBL);	
			typesFormulesUE.add(ITypeFormule.TYPEADMISSION_LBL);
			typesFormulesUE.add(ITypeFormule.TYPECOMPENSATION_LBL);
		}
		
		return typesFormulesUE;
	}
	
	private List<String> getTypesFormulesEC() {
		if (typesFormulesEC == null) {
			typesFormulesEC = new ArrayList<String>();
			typesFormulesEC.add(ITypeFormule.TYPEMOYENNE_LBL);	
			typesFormulesEC.add(ITypeFormule.TYPEADMISSION_LBL);
			typesFormulesEC.add(ITypeFormule.TYPECOMPENSATION_LBL);
		}
		
		return typesFormulesEC;
	}
	
}
