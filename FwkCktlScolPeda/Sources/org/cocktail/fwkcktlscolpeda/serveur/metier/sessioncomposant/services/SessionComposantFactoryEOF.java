package org.cocktail.fwkcktlscolpeda.serveur.metier.sessioncomposant.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSessionComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IFormule;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISessionComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormule;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Cette classe représente la fabrique des {@link ISessionComposant}.
 * 
 * @author Pascal MACOUIN
 */
public class SessionComposantFactoryEOF implements SessionComposantFactory {

	public static final String SESSION = "Session";
	
	private EOEditingContext editingContext;
	
	private SessionComposantRules rules;
	

	/**
	 * Constructeur.
	 * @param editingContext Un editing context
	 */
	public SessionComposantFactoryEOF(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	private SessionComposantRules getSessionComposantRules() {
		if (rules == null) {
			rules = new SessionComposantRules();
		}
		
		return rules;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<ISessionComposant> creerSessionsComposant(IComposant unComposant) {
		ArrayList<ISessionComposant> listeSessions = new ArrayList<ISessionComposant>();
		
		listeSessions.add(creerSessionComposantIfNotExists(unComposant, 1, "Session 1"));
		listeSessions.add(creerSessionComposantIfNotExists(unComposant, 2, "Session 2"));
		if (unComposant instanceof IEC) {
			listeSessions.add(creerSessionComposantIfNotExists(unComposant, 3, "Session 3"));
		} else {
			listeSessions.add(creerSessionComposantIfNotExists(unComposant, 3, "Session Spéciale"));
		}
		
		return listeSessions;
	}
	
	/**
	 * Crée une session avec les formules associée (que la formule moyenne pour les EC).
	 * @param unComposant Un composant
	 * @param unLibelle Un libellé pour le session
	 * @return La session créée
	 */
	private ISessionComposant creerSessionComposantIfNotExists(IComposant unComposant, int unNumeroSession, String unLibelle) {
		ISessionComposant sessionComposant = sessionExistante(unComposant, unNumeroSession);
		if (sessionComposant == null) {
			sessionComposant = (ISessionComposant) EOUtilities.createAndInsertInstance(editingContext, EOSessionComposant.ENTITY_NAME);
			sessionComposant.setNumeroSession(unNumeroSession);
			sessionComposant.setLibelle(unLibelle);
			sessionComposant.setActive(0);
			sessionComposant.setComposantRelationship(unComposant);
		}
		ajouterFormulesIfNotExists(sessionComposant, unComposant.typeComposant());
		return sessionComposant;
	}

	private ISessionComposant sessionExistante(IComposant unComposant, int unNumeroSession) {
		ISessionComposant sessionComposant = null;
		List<? extends ISessionComposant> sessions = unComposant.sessions();
		for (ISessionComposant session : sessions) {
			if (unNumeroSession == session.numeroSession()) {
				sessionComposant = session;
			}
		}
		return sessionComposant;
	}
	
	/**
	 * Ajoute tous les types de formule pour la session.
	 * @param sessionComposant Une session
	 * @param typeComposant 
	 */
	private void ajouterFormulesIfNotExists(ISessionComposant sessionComposant, ITypeComposant typeComposant) {
		List<String> typeFormules =  getSessionComposantRules().getRules().get(typeComposant.nom());
		for (String nomTypeFormule : typeFormules) {
	    ITypeFormule typeFormule = EOTypeFormule.getFormuleDeType(editingContext, nomTypeFormule);
	    creerFormuleIfNotExists(sessionComposant, typeFormule);
    }
	}
	
	/**
	 * Crée un objet formule.
	 * @param typeFormule Le type de formule
	 * @return La formule créée
	 */
	private IFormule creerFormuleIfNotExists(ISessionComposant sessionComposant, ITypeFormule typeFormule) {
		IFormule formule = formuleExistante(sessionComposant, typeFormule);
		if (formule == null) {
			formule = (IFormule) EOUtilities.createAndInsertInstance(editingContext, EOFormule.ENTITY_NAME);
			formule.setTypeFormuleRelationship(typeFormule);
			formule.setSessionComposantRelationship(sessionComposant);
		}
		return formule;
	}

	private IFormule formuleExistante(ISessionComposant sessionComposant,
			ITypeFormule typeFormule) {
		IFormule formuleTrouvee = sessionComposant.getFormule(typeFormule.type());
		return formuleTrouvee;
	}
	
}
