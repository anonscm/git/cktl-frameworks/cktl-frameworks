package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape= JsonFormat.Shape.OBJECT)
public enum Absence {

	EX("EX", "Excusé"),
	NEX("NEX", "Non excusé");
	
	String code;
	String label;
	
	private Absence(String code, String label) {
		this.code = code;
		this.label = label;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
}
