package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

public class EOSise extends _EOSise implements ISise {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOSise.class);
	
	@Override
	public void awakeFromInsertion(EOEditingContext editingContext) {
		super.awakeFromInsertion(editingContext);
		setAcabac(ORACLE_EMPTY_STRING);
		setActivi(ORACLE_EMPTY_STRING);
		setAmena(ORACLE_EMPTY_STRING);
		setAnbac(ORACLE_EMPTY_STRING);
		setAnetab(ORACLE_EMPTY_STRING);
		setAninsc(ORACLE_EMPTY_STRING);
		setAnnais(ORACLE_EMPTY_STRING);
		setAnsup(ORACLE_EMPTY_STRING);
		setBac(ORACLE_EMPTY_STRING);
		setCge(ORACLE_EMPTY_STRING);
		setCometu(ORACLE_EMPTY_STRING);
		setCompos(ORACLE_EMPTY_STRING);
		setComref(ORACLE_EMPTY_STRING);
		setConv(ORACLE_EMPTY_STRING);
		setCpEtu(ORACLE_EMPTY_STRING);
		setCpPar(ORACLE_EMPTY_STRING);
		setCurpar(ORACLE_EMPTY_STRING);
		setDepbac(ORACLE_EMPTY_STRING);
		setDipder(ORACLE_EMPTY_STRING);
		setDiplom(ORACLE_EMPTY_STRING);
		setDispens(ORACLE_EMPTY_STRING);
		setDom1(ORACLE_EMPTY_STRING);
		setDom2(ORACLE_EMPTY_STRING);
		setDom3(ORACLE_EMPTY_STRING);
		setDom4(ORACLE_EMPTY_STRING);
		setDom5(ORACLE_EMPTY_STRING);
		setEchang(ORACLE_EMPTY_STRING);
		setFonctio(ORACLE_EMPTY_STRING);
		setIdetu(ORACLE_EMPTY_STRING);
		setInspr(ORACLE_EMPTY_STRING);
		setJonais(ORACLE_EMPTY_STRING);
		setLcomEtu(ORACLE_EMPTY_STRING);
		setLComRef(ORACLE_EMPTY_STRING);
		setMonais(ORACLE_EMPTY_STRING);
		setNation(ORACLE_EMPTY_STRING);
		setNiveau(ORACLE_EMPTY_STRING);
		setNumed(ORACLE_EMPTY_STRING);
		setNumins(ORACLE_EMPTY_STRING);
		setParipa(ORACLE_EMPTY_STRING);
		setParType(ORACLE_EMPTY_STRING);
		setPaypar(ORACLE_EMPTY_STRING);
		setPcspar(ORACLE_EMPTY_STRING);
		setPcspar2(ORACLE_EMPTY_STRING);
		setRegime(ORACLE_EMPTY_STRING);
		setReimma(ORACLE_EMPTY_STRING);
		setSexe(ORACLE_EMPTY_STRING);
		setSitupre(ORACLE_EMPTY_STRING);
		setSpeciA(ORACLE_EMPTY_STRING);
		setSpeciB(ORACLE_EMPTY_STRING);
		setSpeciC(ORACLE_EMPTY_STRING);
		setTelens(ORACLE_EMPTY_STRING);
		setTyprepa(ORACLE_EMPTY_STRING);
	}

}
