package org.cocktail.fwkcktlscolpeda.serveur.metier;


/**
 * Définition du contrat de suppression
 */
public interface ISupprimable {

	/**
	 * Fonction de suppression d'un élément
	 * @throws Exception : exception renvoyée
	 */
	void supprimer() throws Exception;
	
	/**
	 * Opération de suppression supplementaire effectuée avant la suppression effective
	 * @throws Exception : exception renvoyée
	 * @return Boolean : indique si le avant suppression s'est bien effectue ou non
	 */
	Boolean avantSuppression() throws Exception;
	
	
	/**
	 * Indique si un élément peut être supprimé
	 * @return : true si le composant peut être supprimé
	 */
	Boolean peutEtresupprime() throws Exception;
	
}
