package org.cocktail.fwkcktlscolpeda.serveur.metier;




/**
 * Décrit et gère un parcours
 */
public class EOParcours extends _EOParcours implements IParcours {
	private static final long serialVersionUID = 1L;
	
	@Override
	public EOTypeComposant getTypeComposant() {
		return EOTypeComposant.typeParcours(editingContext());
	}

	@Override
	public void initialise() {
		super.initialise();
		setParcoursType(false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public EOParcours copy(Integer anneeVersion, Integer numeroMajeur) {
		try {
			//TODO : récupérer le persId par injection du user
			EOParcours parcours = EOComposantFactory.createComposant(EOParcours.class, this.persIdCreation(), this.editingContext(), anneeVersion, numeroMajeur);
			assignTo(parcours);
			return parcours;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }

	/**
	 * @return IParcours : le composant suivant associe au composant si il existe
	 */
	public IParcours getComposantAvecOrigine() {
		if(super.getComposantSuivant() != null) {
			return ((IParcours) (super.getComposantSuivant()));
		}
		
		return null;
	}
	
	/**
	 * @return IParcours : le composant d'origine
	 */
	public IParcours getComposantOrigine() {
		return (IParcours) composantOrigine();
	}
	
	private void assignTo(EOParcours parcours) {
		super.assignTo(parcours);
				
	}

	// Code sise n'est plus obligatoire
	/**
	 * Quand c'est un parcours type, le code SISE est obligatoire.
	 * @param codeSise un code SISE
	 * @return le code SISE
	 */
//	public Object validateSise(String codeSise) {
//		if (Boolean.TRUE.equals(parcoursType()) && codeSise == null) {
//			throw ERXValidationFactory.defaultFactory().createCustomException(this, "Sco_Parcours.sise.NullPropertyException");
//		}
//		
//		return codeSise;
//	}
}
