package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

public class EOGdStrategie extends _EOGdStrategie {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOGdStrategie.class);
	private static ERXEnterpriseObjectCache<EOGdStrategie> gdStrategieCache;

	//Public fields
	public static final String STRATEGIE_ACCESFORMATION_CODE = "ACCES_FORMATION";
	public static final String STRATEGIE_ACCESELEMENTSFORMATION_CODE = "ACCES_ELEMENTS_FORMATION";
	public static final String STRATEGIE_ACCESELEMENT_CODE = "ACCES_ELEMENT";
	public static final String STRATEGIE_ACCESFORMATIONSCOMPOSANTE_CODE = "ACCES_FORMATIONS_COMPOSANTE";
	public static final String STRATEGIE_ACCESFORMATIONSCOMPOSANTEUSER_CODE = "ACCES_FORMATIONS_COMPOSANTE_USER";
	public static final String STRATEGIE_ACCESELEMENTSFORMATIONUSERRESP_CODE = "ACCES_ELEMENTS_FORMATION_USER_RESP";
	public static final String STRATEGIE_ACCESGRADE_CODE = "ACCES_GRADE";

	// Properties
    /**
     * @return le cache des {@link EOGdStrategie} par leur code
     */
    public static ERXEnterpriseObjectCache<EOGdStrategie> getGdStrategieCache() {
    	if (gdStrategieCache == null) {
    		gdStrategieCache = new ERXEnterpriseObjectCache<EOGdStrategie>(EOGdStrategie.class, CODE_KEY);
    	}
    	return gdStrategieCache;
    }
    
    /**
     * @param gdStrategieCache le cache a setter
     */
    public static void setGdStrategieCache(ERXEnterpriseObjectCache<EOGdStrategie> gdStrategieCache) {
    	EOGdStrategie.gdStrategieCache = gdStrategieCache;
    }
    
    /**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_FORMATION"
   	 */
   	public static EOGdStrategie strategieAccesFormation(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESFORMATION_CODE);
   	}
   	
   	/**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_ELEMENTS_FORMATION"
   	 */
   	public static EOGdStrategie strategieAccesElementsFormation(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESELEMENTSFORMATION_CODE);
   	}
    
    /**
	 * @param edc l'editingContext
	 * @return le {@link EOGdStrategie} "ACCES_ELEMENT"
	 */
	public static EOGdStrategie strategieAccesElement(EOEditingContext edc) {
		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESELEMENT_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGdStrategie} "ACCES_FORMATIONS_COMPOSANTE"
	 */
	public static EOGdStrategie strategieAccesFormationsComposante(EOEditingContext edc) {
		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESFORMATIONSCOMPOSANTE_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGdStrategie} "ACCES_FORMATIONS_COMPOSANTE_USER"
	 */
	public static EOGdStrategie strategieAccesFormationsComposanteUser(EOEditingContext edc) {
		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESFORMATIONSCOMPOSANTEUSER_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGdStrategie} "ACCES_ELEMENTS_FORMATION_USER_RESP"
	 */
	public static EOGdStrategie strategieAccesElementsFormationUserResponsable(EOEditingContext edc) {
		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESELEMENTSFORMATIONUSERRESP_CODE);
	}
	
	/**
	 * @param edc l'editingContext
	 * @return le {@link EOGdStrategie} "ACCES_GRADE"
	 */
	public static EOGdStrategie strategieAccesGrade(EOEditingContext edc) {
		return getGdStrategieCache().objectForKey(edc, STRATEGIE_ACCESGRADE_CODE);
	}
	
	@Override
	public String toString() {
		return this.libelle();
	}
	
}
