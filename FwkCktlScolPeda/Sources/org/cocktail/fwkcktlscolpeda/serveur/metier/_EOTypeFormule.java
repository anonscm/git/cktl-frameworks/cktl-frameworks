// DO NOT EDIT.  Make changes to EOTypeFormule.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeFormule extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_TypeFormule";

  // Attribute Keys
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> READ_ONLY = new ERXKey<Integer>("readOnly");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys

  // Attributes
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String READ_ONLY_KEY = READ_ONLY.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeFormule.class);

  public EOTypeFormule localInstanceIn(EOEditingContext editingContext) {
    EOTypeFormule localInstance = (EOTypeFormule)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeFormule.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeFormule.LOG.isDebugEnabled()) {
    	_EOTypeFormule.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormule.LIBELLE_KEY);
  }

  public Integer readOnly() {
    return (Integer) storedValueForKey(_EOTypeFormule.READ_ONLY_KEY);
  }

  public void setReadOnly(Integer value) {
    if (_EOTypeFormule.LOG.isDebugEnabled()) {
    	_EOTypeFormule.LOG.debug( "updating readOnly from " + readOnly() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormule.READ_ONLY_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOTypeFormule.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOTypeFormule.LOG.isDebugEnabled()) {
    	_EOTypeFormule.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFormule.TYPE_KEY);
  }


  public static EOTypeFormule createSco_TypeFormule(EOEditingContext editingContext, String libelle
, Integer readOnly
, String type
) {
    EOTypeFormule eo = (EOTypeFormule) EOUtilities.createAndInsertInstance(editingContext, _EOTypeFormule.ENTITY_NAME);    
		eo.setLibelle(libelle);
		eo.setReadOnly(readOnly);
		eo.setType(type);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeFormule> fetchSpec() {
    return new ERXFetchSpecification<EOTypeFormule>(_EOTypeFormule.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeFormule> fetchAllSco_TypeFormules(EOEditingContext editingContext) {
    return _EOTypeFormule.fetchAllSco_TypeFormules(editingContext, null);
  }

  public static NSArray<EOTypeFormule> fetchAllSco_TypeFormules(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeFormule.fetchSco_TypeFormules(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeFormule> fetchSco_TypeFormules(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeFormule> fetchSpec = new ERXFetchSpecification<EOTypeFormule>(_EOTypeFormule.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeFormule> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeFormule fetchSco_TypeFormule(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormule.fetchSco_TypeFormule(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormule fetchSco_TypeFormule(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeFormule> eoObjects = _EOTypeFormule.fetchSco_TypeFormules(editingContext, qualifier, null);
    EOTypeFormule eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_TypeFormule that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormule fetchRequiredSco_TypeFormule(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFormule.fetchRequiredSco_TypeFormule(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFormule fetchRequiredSco_TypeFormule(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeFormule eoObject = _EOTypeFormule.fetchSco_TypeFormule(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_TypeFormule that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFormule localInstanceIn(EOEditingContext editingContext, EOTypeFormule eo) {
    EOTypeFormule localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
