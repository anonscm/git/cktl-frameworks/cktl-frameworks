// DO NOT EDIT.  Make changes to EOGdPerimetreGirofle.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOGdPerimetreGirofle extends org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre {
  public static final String ENTITY_NAME = "Sco_GdPerimetreGirofle";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Integer> APP_ID = new ERXKey<Integer>("appId");
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> GRADE_UNIVERSITAIRE_ID = new ERXKey<Integer>("gradeUniversitaireId");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERIMETRE_PARENT_ID = new ERXKey<Integer>("perimetreParentId");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> RECURSIF = new ERXKey<Integer>("recursif");
  public static final ERXKey<Integer> STRATEGIE_ID = new ERXKey<Integer>("strategieId");
  public static final ERXKey<Integer> TYPE_COMPOSANT_ID = new ERXKey<Integer>("typeComposantId");
  public static final ERXKey<Integer> TYPE_DROIT_DONNEE_ID = new ERXKey<Integer>("typeDroitDonneeId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> APPLICATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("application");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> GD_PROFIL_PERIMETRES = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>("gdProfilPerimetres");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("gradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRE_PARENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetreParent");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRES_ENFANTS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetresEnfants");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> PROFILS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("profils");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie> STRATEGIE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie>("strategie");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structure");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant> TYPE_COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant>("typeComposant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee> TYPE_DROIT_DONNEE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee>("typeDroitDonnee");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String APP_ID_KEY = APP_ID.key();
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  public static final String C_STRUCTURE_KEY = C_STRUCTURE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String GRADE_UNIVERSITAIRE_ID_KEY = GRADE_UNIVERSITAIRE_ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERIMETRE_PARENT_ID_KEY = PERIMETRE_PARENT_ID.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String RECURSIF_KEY = RECURSIF.key();
  public static final String STRATEGIE_ID_KEY = STRATEGIE_ID.key();
  public static final String TYPE_COMPOSANT_ID_KEY = TYPE_COMPOSANT_ID.key();
  public static final String TYPE_DROIT_DONNEE_ID_KEY = TYPE_DROIT_DONNEE_ID.key();
  // Relationships
  public static final String APPLICATION_KEY = APPLICATION.key();
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String GD_PROFIL_PERIMETRES_KEY = GD_PROFIL_PERIMETRES.key();
  public static final String GRADE_UNIVERSITAIRE_KEY = GRADE_UNIVERSITAIRE.key();
  public static final String PERIMETRE_PARENT_KEY = PERIMETRE_PARENT.key();
  public static final String PERIMETRES_ENFANTS_KEY = PERIMETRES_ENFANTS.key();
  public static final String PROFILS_KEY = PROFILS.key();
  public static final String STRATEGIE_KEY = STRATEGIE.key();
  public static final String STRUCTURE_KEY = STRUCTURE.key();
  public static final String TYPE_COMPOSANT_KEY = TYPE_COMPOSANT.key();
  public static final String TYPE_DROIT_DONNEE_KEY = TYPE_DROIT_DONNEE.key();

  private static Logger LOG = Logger.getLogger(_EOGdPerimetreGirofle.class);

  public EOGdPerimetreGirofle localInstanceIn(EOEditingContext editingContext) {
    EOGdPerimetreGirofle localInstance = (EOGdPerimetreGirofle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.ANNEE_KEY);
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.COMPOSANT_ID_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(_EOGdPerimetreGirofle.C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.C_STRUCTURE_KEY);
  }

  public Integer gradeUniversitaireId() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_ID_KEY);
  }

  public void setGradeUniversitaireId(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating gradeUniversitaireId from " + gradeUniversitaireId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_ID_KEY);
  }

  public Integer recursif() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.RECURSIF_KEY);
  }

  public void setRecursif(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating recursif from " + recursif() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.RECURSIF_KEY);
  }

  public Integer strategieId() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.STRATEGIE_ID_KEY);
  }

  public void setStrategieId(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating strategieId from " + strategieId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.STRATEGIE_ID_KEY);
  }

  public Integer typeComposantId() {
    return (Integer) storedValueForKey(_EOGdPerimetreGirofle.TYPE_COMPOSANT_ID_KEY);
  }

  public void setTypeComposantId(Integer value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
    	_EOGdPerimetreGirofle.LOG.debug( "updating typeComposantId from " + typeComposantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.TYPE_COMPOSANT_ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EOGdPerimetreGirofle.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
      _EOGdPerimetreGirofle.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGdPerimetreGirofle.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGdPerimetreGirofle.COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire gradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(_EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_KEY);
  }
  
  public void setGradeUniversitaire(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_KEY);
  }

  public void setGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
      _EOGdPerimetreGirofle.LOG.debug("updating gradeUniversitaire from " + gradeUniversitaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setGradeUniversitaire(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = gradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGdPerimetreGirofle.GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie strategie() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie)storedValueForKey(_EOGdPerimetreGirofle.STRATEGIE_KEY);
  }
  
  public void setStrategie(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie value) {
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.STRATEGIE_KEY);
  }

  public void setStrategieRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
      _EOGdPerimetreGirofle.LOG.debug("updating strategie from " + strategie() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setStrategie(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGdStrategie oldValue = strategie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGdPerimetreGirofle.STRATEGIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGdPerimetreGirofle.STRATEGIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EOGdPerimetreGirofle.STRUCTURE_KEY);
  }
  
  public void setStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
      _EOGdPerimetreGirofle.LOG.debug("updating structure from " + structure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGdPerimetreGirofle.STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGdPerimetreGirofle.STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant typeComposant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant)storedValueForKey(_EOGdPerimetreGirofle.TYPE_COMPOSANT_KEY);
  }
  
  public void setTypeComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    takeStoredValueForKey(value, _EOGdPerimetreGirofle.TYPE_COMPOSANT_KEY);
  }

  public void setTypeComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant value) {
    if (_EOGdPerimetreGirofle.LOG.isDebugEnabled()) {
      _EOGdPerimetreGirofle.LOG.debug("updating typeComposant from " + typeComposant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant oldValue = typeComposant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOGdPerimetreGirofle.TYPE_COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOGdPerimetreGirofle.TYPE_COMPOSANT_KEY);
    }
  }
  

  public static EOGdPerimetreGirofle createSco_GdPerimetreGirofle(EOEditingContext editingContext, Integer appId
, NSTimestamp dateCreation
, String libelle
, Integer persIdCreation
) {
    EOGdPerimetreGirofle eo = (EOGdPerimetreGirofle) EOUtilities.createAndInsertInstance(editingContext, _EOGdPerimetreGirofle.ENTITY_NAME);    
		eo.setAppId(appId);
		eo.setDateCreation(dateCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOGdPerimetreGirofle> fetchSpecForSco_GdPerimetreGirofle() {
    return new ERXFetchSpecification<EOGdPerimetreGirofle>(_EOGdPerimetreGirofle.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOGdPerimetreGirofle> fetchAllSco_GdPerimetreGirofles(EOEditingContext editingContext) {
    return _EOGdPerimetreGirofle.fetchAllSco_GdPerimetreGirofles(editingContext, null);
  }

  public static NSArray<EOGdPerimetreGirofle> fetchAllSco_GdPerimetreGirofles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGdPerimetreGirofle.fetchSco_GdPerimetreGirofles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGdPerimetreGirofle> fetchSco_GdPerimetreGirofles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOGdPerimetreGirofle> fetchSpec = new ERXFetchSpecification<EOGdPerimetreGirofle>(_EOGdPerimetreGirofle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGdPerimetreGirofle> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOGdPerimetreGirofle fetchSco_GdPerimetreGirofle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGdPerimetreGirofle.fetchSco_GdPerimetreGirofle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGdPerimetreGirofle fetchSco_GdPerimetreGirofle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGdPerimetreGirofle> eoObjects = _EOGdPerimetreGirofle.fetchSco_GdPerimetreGirofles(editingContext, qualifier, null);
    EOGdPerimetreGirofle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_GdPerimetreGirofle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGdPerimetreGirofle fetchRequiredSco_GdPerimetreGirofle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGdPerimetreGirofle.fetchRequiredSco_GdPerimetreGirofle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGdPerimetreGirofle fetchRequiredSco_GdPerimetreGirofle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGdPerimetreGirofle eoObject = _EOGdPerimetreGirofle.fetchSco_GdPerimetreGirofle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_GdPerimetreGirofle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGdPerimetreGirofle localInstanceIn(EOEditingContext editingContext, EOGdPerimetreGirofle eo) {
    EOGdPerimetreGirofle localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
