package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * 
 * @author isabelle
 *
 */
public class EOExamen extends _EOExamen implements IExamen {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOExamen.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	public Long idExamenLong() {
		return idExamen() == null ? null : idExamen().longValue();
	}

	public Long idComposantAELong() {
		return super.idComposantAE() == null ? null : new Long(super.idComposantAE());
	}

	/**
	 * @param editingContext Un editing context
	 * @param id L'id de l'examen
	 * @return L'examen (ou <code>null</code> si non trouvé)
	 */
	public static IExamen fetchById(EOEditingContext editingContext, int id) {
		return EOExamen.fetchSco_Examen(editingContext, EOExamen.ID_EXAMEN.eq(id));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToExamenEtatRelationship(IExamenEtat value) {
		super.setToExamenEtatRelationship((EOExamenEtat) value);

	}

	/**
	 * {@inheritDoc}
	 */
	public void setToComposantAERelationship(IAE value) {
		super.setToComposantAERelationship((EOAE) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToSessionPeriodeRelationship(ISessionPeriode value) {
		super.setToSessionPeriodeRelationship((EOSessionPeriode) value);
	}
	
	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId utilisateur qui met à jour l'enregistrement
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModifcation(new NSTimestamp());
	}
	
	public void supprimer(EOEditingContext editingContext) throws Exception{
		for (EOExamenOrganisation examenOrganisation : toExamenOrganisations()) {
	    editingContext.deleteObject(examenOrganisation);
    }
		editingContext.deleteObject(this);
	}

}
