package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import er.extensions.eof.ERXKey;

/**
 * Lien de type composer
 */
public class EOLienComposer extends _EOLienComposer implements ISupprimable, ILienComposer {

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOLienComposer.class);

	// Attributes Keys
	public static final ERXKey<String> MUTUALISE = new ERXKey<String>("listeAutresParents");

	// Attributes
	public static final String MUTUALISE_KEY = MUTUALISE.key();

	/**
	 * Construit un {@link EOLienComposer} avec alimentation des valeurs par défaut.
	 */
	public EOLienComposer() {
		setObligatoire(false);
	}

	/**
	 * @return liste des parents du composant à l'exclusion du parent courant
	 */
	public String listeAutresParents() {
		return this.child().listeCodesAutresParents(this.parent());
	}

	@Override
	public Boolean avantSuppression() throws Exception {
		return true;
	}
	
	/**
	 * @param nouveauLien : nouveau lien auquel on assigne les valeur du lien courant
	 */
	public void assignTo(ILien nouveauLien) {
	    super.assignTo(nouveauLien);
	    ((EOLienComposer) nouveauLien).setBaseNote(this.baseNote());
	    ((EOLienComposer) nouveauLien).setChoixECTS(this.choixECTS());
	    ((EOLienComposer) nouveauLien).setChoixMax(this.choixMax());
	    ((EOLienComposer) nouveauLien).setChoixMin(this.choixMin());
	    ((EOLienComposer) nouveauLien).setCoefficient(this.coefficient());
	    ((EOLienComposer) nouveauLien).setDureeMax(this.dureeMax());
	    ((EOLienComposer) nouveauLien).setDureeMin(this.dureeMin());
	    ((EOLienComposer) nouveauLien).setEpreuve(this.epreuve());
	    ((EOLienComposer) nouveauLien).setObligatoire(this.obligatoire());
	    ((EOLienComposer) nouveauLien).setOrdre(this.ordre());
	    ((EOLienComposer) nouveauLien).setSansComposant(this.sansComposant());
	    ((EOLienComposer) nouveauLien).setSansNote(this.sansNote());
	    ((EOLienComposer) nouveauLien).setSeuil(this.seuil());
	    ((EOLienComposer) nouveauLien).setValeurTempsEtudiant(this.valeurTempsEtudiant());	    
	}

}
