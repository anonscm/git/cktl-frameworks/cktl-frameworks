// DO NOT EDIT.  Make changes to EOImplantationsComposant.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOImplantationsComposant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_ImplantationsGeoComposant";

  // Attribute Keys
  public static final ERXKey<Integer> COMPOSANT_ID = new ERXKey<Integer>("composantId");
  // Relationship Keys

  // Attributes
  public static final String COMPOSANT_ID_KEY = COMPOSANT_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOImplantationsComposant.class);

  public EOImplantationsComposant localInstanceIn(EOEditingContext editingContext) {
    EOImplantationsComposant localInstance = (EOImplantationsComposant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer composantId() {
    return (Integer) storedValueForKey(_EOImplantationsComposant.COMPOSANT_ID_KEY);
  }

  public void setComposantId(Integer value) {
    if (_EOImplantationsComposant.LOG.isDebugEnabled()) {
    	_EOImplantationsComposant.LOG.debug( "updating composantId from " + composantId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOImplantationsComposant.COMPOSANT_ID_KEY);
  }


  public static EOImplantationsComposant createSco_ImplantationsGeoComposant(EOEditingContext editingContext, Integer composantId
) {
    EOImplantationsComposant eo = (EOImplantationsComposant) EOUtilities.createAndInsertInstance(editingContext, _EOImplantationsComposant.ENTITY_NAME);    
		eo.setComposantId(composantId);
    return eo;
  }

  public static ERXFetchSpecification<EOImplantationsComposant> fetchSpec() {
    return new ERXFetchSpecification<EOImplantationsComposant>(_EOImplantationsComposant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOImplantationsComposant> fetchAllSco_ImplantationsGeoComposants(EOEditingContext editingContext) {
    return _EOImplantationsComposant.fetchAllSco_ImplantationsGeoComposants(editingContext, null);
  }

  public static NSArray<EOImplantationsComposant> fetchAllSco_ImplantationsGeoComposants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOImplantationsComposant.fetchSco_ImplantationsGeoComposants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOImplantationsComposant> fetchSco_ImplantationsGeoComposants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOImplantationsComposant> fetchSpec = new ERXFetchSpecification<EOImplantationsComposant>(_EOImplantationsComposant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOImplantationsComposant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOImplantationsComposant fetchSco_ImplantationsGeoComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImplantationsComposant.fetchSco_ImplantationsGeoComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImplantationsComposant fetchSco_ImplantationsGeoComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOImplantationsComposant> eoObjects = _EOImplantationsComposant.fetchSco_ImplantationsGeoComposants(editingContext, qualifier, null);
    EOImplantationsComposant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_ImplantationsGeoComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImplantationsComposant fetchRequiredSco_ImplantationsGeoComposant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImplantationsComposant.fetchRequiredSco_ImplantationsGeoComposant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImplantationsComposant fetchRequiredSco_ImplantationsGeoComposant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOImplantationsComposant eoObject = _EOImplantationsComposant.fetchSco_ImplantationsGeoComposant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_ImplantationsGeoComposant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImplantationsComposant localInstanceIn(EOEditingContext editingContext, EOImplantationsComposant eo) {
    EOImplantationsComposant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
