package org.cocktail.fwkcktlscolpeda.serveur.metier;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.collect.Ordering;
import com.webobjects.foundation.NSArray;

/**
 * {@inheritDoc}
 */
public class EOInscriptionPedagogiqueElement extends _EOInscriptionPedagogiqueElement implements IInscriptionPedagogiqueElement {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOInscriptionPedagogiqueElement.class);

	private static final NoteFinaleCalculator NoteFinaleCalculator = new NoteFinaleCalculator();
	
	/**
	 * {@inheritDoc}
	 */
	public void removeFromToNotesRelationship(INote note) {
		super.removeFromToNotesRelationship((EONote) note);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToLienRelationship(ILien value) {
		super.setToLienRelationship((EOLien) value);
	}
	
	@Override
	public void setToLienRelationship(EOLien value) {
		super.setToLienRelationship(value);
		if (value != null) {
			setIdLien(value.id());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToTypeInscriptionPedaRelationship(ITypeInscriptionPeda value) {
		super.setToTypeInscriptionPedaRelationship((EOTypeInscriptionPeda) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToInscriptionPedagogiqueRelationship(IInscriptionPedagogique value) {
		super.setToInscriptionPedagogiqueRelationship((EOInscriptionPedagogique) value);

	}

	/**
	 * {@inheritDoc}
	 */
	public INote noteFinale() {
		return NoteFinaleCalculator.noteFinale(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public INote noteResultatFinale() {
		return NoteFinaleCalculator.noteResultatFinale(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public INote noteSession(Integer numSession) {
		NSArray<EONote> listeNote = toNotes(EONote.NO_SESSION.eq(numSession));
		if (listeNote != null && listeNote.size() > 0) {
			return listeNote.get(0);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Boolean hasNotes() {
		return (toNotes() != null && !toNotes().isEmpty());
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer id() {
		String primaryKey = primaryKey();
		return primaryKey != null ? Integer.valueOf(primaryKey) : null;
	}
	
	static class NoteFinaleCalculator {
		
		private static final Ordering<INote> SessionOrdering = new Ordering<INote>() {
			@Override
			public int compare(INote n1, INote n2) {
				if (n1.noSession() != null && n2.noSession() != null) {
					return Ordering.natural().compare(n1.noSession(), n2.noSession());
				} else {
					return 0;
				}
			}
		};
		private static final Ordering<INote> TypeOrdering = new Ordering<INote>() {
			@Override
			public int compare(INote n1, INote n2) {
				ITypeNote typeNote1 = n1.toTypeNote();
				ITypeNote typeNote2 = n2.toTypeNote();
				if (typeNote1 != null && typeNote2 != null) {
					if (typeNote1.code().equals(ITypeNote.NOTE_SAISIE)) {
						return 1;
					} else if (typeNote2.code().equals(ITypeNote.NOTE_SAISIE)) {
						return -1;
					} else {
						return 0;
					}
				} else {
					return 0;
				}
			}
		};
		
		public INote noteFinale(IInscriptionPedagogiqueElement element) {
			List<INote> notes = new ArrayList<INote>();
			for (INote note : element.toNotes()) {
				if (note.noteInitiale() != null) {
					notes.add(note);
				}
			}
			INote max = derniereNote(notes);
			return max;
		}
		
		public INote noteResultatFinale(IInscriptionPedagogiqueElement element) {
			List<INote> notes = new ArrayList<INote>();
			for (INote note : element.toNotes()) {
				if (note.toNoteResultat() != null) {
					notes.add(note);
				}
			}
			INote max = derniereNote(notes);
			return max;
		}
		
		private INote derniereNote(List<? extends INote> notes) {
			INote max = null;
			if (!notes.isEmpty()) {
				max = SessionOrdering.compound(TypeOrdering).max(notes);
			}
			return max;
		}
	}
	
}
