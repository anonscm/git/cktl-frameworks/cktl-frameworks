package org.cocktail.fwkcktlscolpeda.serveur.metier;

public interface ITypeInscriptionFormation {

	String TYPE_INSCRIPTION_FORMATION_PRINCIPALE = "FO1";
	String TYPE_INSCRIPTION_FORMATION_CUMULATIF = "CUM1";
	String TYPE_INSCRIPTION_FORMATION_AUDITEUR_LIBRE = "AL";
	String TYPE_INSCRIPTION_FORMATION_COMPLEMENTAIRE = "FO2";
	String TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL = "ECH";
	String TYPE_INSCRIPTION_FORMATION_STAGIAIRE = "STAGIAIRE";
	String TYPE_INSCRIPTION_FORMATION_ANTICIPATION = "ANT";
	String TYPE_INSCRIPTION_FORMATION_DETTE = "DET";

	/**
	 * @return le  code du type d'inscription
	 */
	String codeInscription();
	
	/**
	 * @return le libellé du type d'inscription
	 */
	String libelle();
	
	/**
	 * 
	 * @return tur si la formation est de type complementaire
	 */
	Boolean isComplementaire();
	

}