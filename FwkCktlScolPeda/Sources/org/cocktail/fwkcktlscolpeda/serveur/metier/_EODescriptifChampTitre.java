// DO NOT EDIT.  Make changes to EODescriptifChampTitre.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODescriptifChampTitre extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_DescriptifChampTitre";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_KEY = ID.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String ORDRE_KEY = ORDRE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EODescriptifChampTitre.class);

  public EODescriptifChampTitre localInstanceIn(EOEditingContext editingContext) {
    EODescriptifChampTitre localInstance = (EODescriptifChampTitre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EODescriptifChampTitre.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EODescriptifChampTitre.LOG.isDebugEnabled()) {
    	_EODescriptifChampTitre.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampTitre.CODE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODescriptifChampTitre.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODescriptifChampTitre.LOG.isDebugEnabled()) {
    	_EODescriptifChampTitre.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampTitre.ID_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EODescriptifChampTitre.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EODescriptifChampTitre.LOG.isDebugEnabled()) {
    	_EODescriptifChampTitre.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampTitre.LIBELLE_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(_EODescriptifChampTitre.ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    if (_EODescriptifChampTitre.LOG.isDebugEnabled()) {
    	_EODescriptifChampTitre.LOG.debug( "updating ordre from " + ordre() + " to " + value);
    }
    takeStoredValueForKey(value, _EODescriptifChampTitre.ORDRE_KEY);
  }


  public static EODescriptifChampTitre createSco_DescriptifChampTitre(EOEditingContext editingContext, Integer id
, String libelle
) {
    EODescriptifChampTitre eo = (EODescriptifChampTitre) EOUtilities.createAndInsertInstance(editingContext, _EODescriptifChampTitre.ENTITY_NAME);    
		eo.setId(id);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EODescriptifChampTitre> fetchSpec() {
    return new ERXFetchSpecification<EODescriptifChampTitre>(_EODescriptifChampTitre.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODescriptifChampTitre> fetchAllSco_DescriptifChampTitres(EOEditingContext editingContext) {
    return _EODescriptifChampTitre.fetchAllSco_DescriptifChampTitres(editingContext, null);
  }

  public static NSArray<EODescriptifChampTitre> fetchAllSco_DescriptifChampTitres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODescriptifChampTitre.fetchSco_DescriptifChampTitres(editingContext, null, sortOrderings);
  }

  public static NSArray<EODescriptifChampTitre> fetchSco_DescriptifChampTitres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODescriptifChampTitre> fetchSpec = new ERXFetchSpecification<EODescriptifChampTitre>(_EODescriptifChampTitre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODescriptifChampTitre> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODescriptifChampTitre fetchSco_DescriptifChampTitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifChampTitre.fetchSco_DescriptifChampTitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifChampTitre fetchSco_DescriptifChampTitre(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODescriptifChampTitre> eoObjects = _EODescriptifChampTitre.fetchSco_DescriptifChampTitres(editingContext, qualifier, null);
    EODescriptifChampTitre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_DescriptifChampTitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifChampTitre fetchRequiredSco_DescriptifChampTitre(EOEditingContext editingContext, String keyName, Object value) {
    return _EODescriptifChampTitre.fetchRequiredSco_DescriptifChampTitre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODescriptifChampTitre fetchRequiredSco_DescriptifChampTitre(EOEditingContext editingContext, EOQualifier qualifier) {
    EODescriptifChampTitre eoObject = _EODescriptifChampTitre.fetchSco_DescriptifChampTitre(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_DescriptifChampTitre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODescriptifChampTitre localInstanceIn(EOEditingContext editingContext, EODescriptifChampTitre eo) {
    EODescriptifChampTitre localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
