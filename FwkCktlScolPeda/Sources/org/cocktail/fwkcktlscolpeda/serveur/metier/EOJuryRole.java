package org.cocktail.fwkcktlscolpeda.serveur.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * 
 * @author isabelle
 *
 */
public class EOJuryRole extends _EOJuryRole implements IJuryRole {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOJuryRole.class);
	
	   private static ERXEnterpriseObjectCache<EOJuryRole> juryRoleCache;
	    
	    /**
	     * @return le cache des {@link EOTypeFormation} par leur type
	     */
	    public static ERXEnterpriseObjectCache<EOJuryRole> getJuryRoleCache() {
	    	if (juryRoleCache == null) {
	    		juryRoleCache = new ERXEnterpriseObjectCache<EOJuryRole>(EOJuryRole.class, CODE_KEY);
	    	}
	    	return juryRoleCache;
	    }
	    
	    /**
	     * @param typeFormationCache le cache a setter
	     */
	    public static void setJuryRoleCache(ERXEnterpriseObjectCache<EOJuryRole> juryRoleCache) {
	    	EOJuryRole.juryRoleCache = juryRoleCache;
	    }
	    
		/**
		 * @param edc l'editingContext
		 * @return le {@link EOJuryRole} "Membre"
		 */
		public static EOJuryRole typeMembre(EOEditingContext edc) {
			return getJuryRoleCache().objectForKey(edc, JR_MEMBRE);
		}
	    
	    /**
		 * @param edc l'editingContext
		 * @return le {@link EOJuryRole} "Responsable"
		 */
		public static EOJuryRole typeResponsable(EOEditingContext edc) {
			return getJuryRoleCache().objectForKey(edc,JR_RESPONSABLE);
		}
		
	
}
