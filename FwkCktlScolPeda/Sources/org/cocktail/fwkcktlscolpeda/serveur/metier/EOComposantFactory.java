package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * classe utilitaire de création de composants, de descriptifs.
 */
public final class EOComposantFactory {

	// Constructor
	/**
	 * contructeur privée pour empécher l'instantation de la classe
	 */
	private EOComposantFactory() {
	}

	// Properties

	// Public Methods

	/**
	 * @param composantClass : classe pour laquelle on veut instancier un composant
	 * @param <T> : type du composant
	 * @param edc : contexte d'édition
	 * @param utilisateurPersId : identifiant de l'utilisateur qui effectue la creation
	 * @param anneeVersion infos de version du composant
	 * @param numeroVersionMajeur numero de version majeure
	 * @return composant de type T
	 * @throws InstantiationException : exception en cas d'instantiation incorrecte
	 * @throws IllegalAccessException : exception accès illegal
	 */
	public static <T extends EOComposant> T createComposant(Class<T> composantClass, Integer utilisateurPersId, EOEditingContext edc, Integer anneeVersion,
	    Integer numeroVersionMajeur) throws InstantiationException, IllegalAccessException {

		EOEntity entity = EOUtilities.entityForClass(edc, composantClass);
		@SuppressWarnings("unchecked")
		T newComposant = (T) EOUtilities.createAndInsertInstance(edc, entity.name());
		edc.insertObject(newComposant);
		newComposant.setTypeComposantRelationship(newComposant.getTypeComposant());
		if (newComposant.getTypeComposant() != null && !ITypeComposant.TYPEDIPLOME_NOM.equals(newComposant.getTypeComposant().nom())) {
			newComposant.setComposantInfoVersion(EOComposantInfoVersion.create(edc, anneeVersion, numeroVersionMajeur));
		}
		newComposant.initialise();
		newComposant.actualiseInfoEnregistrement(utilisateurPersId);
		return newComposant;
	}

	/**
	 * @param composant : composant pour lequel on ajoute un descripif
	 * @return descriptif associé au composant
	 */
	public static EODescriptif createDescriptif(EOComposant composant) {
		EODescriptif descriptif = (EODescriptif) EOUtilities.createAndInsertInstance(composant.editingContext(), EODescriptif.ENTITY_NAME);
		composant.editingContext().insertObject(descriptif);
		descriptif.setComposantRelationship(composant);
		descriptif.setComposantId(composant.id());
		descriptif.initialise();

		return descriptif;
	}

	/**
	 * @param ap : ap sur lequel on cree une charge d'enseignement
	 * @param typeChargeEnseignement : type de la charge d'enseignement
	 * @param utilisateurPersId : identifiant de l'utilisateur qui effectue la creation
	 * @return EOChargeEnseignement : une nouvelle charge d'enseignement sur l'AP
	 */
	public static EOChargeEnseignement createChargeEnseignementSurAP(EOAP ap, ITypeChargeEnseignement typeChargeEnseignement, Integer utilisateurPersId) {
		EOChargeEnseignement chargeEnseignement = (EOChargeEnseignement) EOUtilities.createAndInsertInstance(ap.editingContext(), EOChargeEnseignement.ENTITY_NAME);
		ap.editingContext().insertObject(chargeEnseignement);
		chargeEnseignement.setToTypeChargeEnseignementRelationship((EOTypeChargeEnseignement) typeChargeEnseignement);
		chargeEnseignement.setToAPRelationship(ap);
		chargeEnseignement.actualiserInfoEnregistrement(utilisateurPersId);

		return chargeEnseignement;
	}

	/**
	 * @param lien : lien sur lequel on cree une charge d'enseignement
	 * @param typeChargeEnseignement : type de la charge d'enseignement
	 * @param utilisateurPersId : identifiant de l'utilisateur qui effectue la creation
	 * @return EOChargeEnseignement : une nouvelle charge d'enseignement sur le lien
	 */
	public static EOChargeEnseignement createChargeEnseignementSurLien(EOLien lien, ITypeChargeEnseignement typeChargeEnseignement, Integer utilisateurPersId) {
		EOChargeEnseignement chargeEnseignement = (EOChargeEnseignement) EOUtilities.createAndInsertInstance(lien.editingContext(),
		    EOChargeEnseignement.ENTITY_NAME);
		lien.editingContext().insertObject(chargeEnseignement);
		chargeEnseignement.setToTypeChargeEnseignementRelationship((EOTypeChargeEnseignement) typeChargeEnseignement);
		chargeEnseignement.setToLienRelationship(lien);
		chargeEnseignement.actualiserInfoEnregistrement(utilisateurPersId);

		return chargeEnseignement;
	}

	/**
	 * @param parent : composant parent du lien
	 * @param enfant : composant enfant du lien
	 * @param typeLien : type de lien
	 * @return lien entre le parent et l'enfant du type typeLien
	 */
	public static EOLien createLien(IComposant parent, IComposant enfant, EOTypeLien typeLien) {
		EOComposant p = (EOComposant) parent;
		EOComposant e = (EOComposant) enfant;
		EOLien lien = createLien(EOLien.class, p, e, typeLien);
		return lien;
	}

	/**
	 * @param parent : composant parent du lien
	 * @param enfant : composant enfant du lien
	 * @return lien de type lienComposer
	 */
	public static EOLienComposer createLienComposer(IComposant parent, IComposant enfant) {
		EOComposant p = (EOComposant) parent;
		EOComposant e = (EOComposant) enfant;
		EOLienComposer lien = createLien(EOLienComposer.class, p, e, EOTypeLien.typeComposer(p.editingContext()));
		lien.setObligatoire(false);
		return lien;
	}

	/**
	 * @param diplome : diplome auquel on rattache le diplome intermedaire
	 * @return diplome intermediaire
	 */
	public static EODiplomeIntermediaire createDiplomeIntermediaire(EODiplome diplome) {
		EODiplomeIntermediaire diplomeIntermediaire = (EODiplomeIntermediaire) EOUtilities.createAndInsertInstance(diplome.editingContext(),
		    EODiplomeIntermediaire.ENTITY_NAME);
		diplome.editingContext().insertObject(diplomeIntermediaire);
		diplomeIntermediaire.setDiplomeRelationship(diplome);
		diplomeIntermediaire.setDiplomeId(diplome.id());
		return diplomeIntermediaire;
	}

	// Protected Methods
	// Private methods
	@SuppressWarnings("unchecked")
	private static <T extends EOLien> T createLien(Class<T> composantClass, EOComposant parent, EOComposant enfant, EOTypeLien typeLien) {
		if (!parent.lienPossible(enfant, typeLien)) {
			throw new IllegalArgumentException("Liaison invalide : Le type " + enfant.typeComposant() + " ne peut pas être un sous-composant du type "
			    + parent.typeComposant());
		} else {
			T nouveauLien;
			EOEntity entity = EOUtilities.entityForClass(parent.editingContext(), composantClass);
			nouveauLien = (T) EOUtilities.createAndInsertInstance(parent.editingContext(), entity.name());

			nouveauLien.setParentId(parent.id());
			nouveauLien.setParentRelationship(parent);

			nouveauLien.setChildId(enfant.id());
			nouveauLien.setChildRelationship(enfant);
			nouveauLien.setTypeLienRelationship(typeLien);
			nouveauLien.setOrdre(nouveauLien.getLastOrdre() + 1);

			return nouveauLien;
		}
	}

	/**
	 * @param bourseClass : classe pour laquelle on veut instancier une bourse
	 * @param <T> : type de la bourse
	 * @param edc : contexte d'édition
	 * @param utilisateurPersId : identifiant de l'utilisateur qui effectue la creation
	 * @return bourse
	 * @throws InstantiationException : exception en cas d'instantiation incorrecte
	 * @throws IllegalAccessException : exception accès illegal
	 */
	public static <T extends IBourses> T createBourse(Class<T> bourseClass, EOEditingContext edc, Integer utilisateurPersId) throws InstantiationException,
	    IllegalAccessException {
		EOEntity entity = EOUtilities.entityForClass(edc, bourseClass);
		@SuppressWarnings("unchecked")
		T newBourse = (T) EOUtilities.createAndInsertInstance(edc, entity.name());
		// edc.insertObject(newBourse);
		newBourse.setPersIdCreation(utilisateurPersId);
		newBourse.setPersIdModification(utilisateurPersId);
		newBourse.setIsCampusFrance(false);
		newBourse.setDCreation(new NSTimestamp());
		newBourse.setDMofication(new NSTimestamp());

		return newBourse;
	}

}
