package org.cocktail.fwkcktlscolpeda.serveur.metier;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * {@inheritDoc}
 */
public class EOSessionPeriode  extends _EOSessionPeriode  implements ISessionPeriode {
	private static final long serialVersionUID = -8234595292240594813L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOSessionPeriode.class);
	
	@Inject
	@Nullable
	private UserInfo userInfo;
	
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
        super.validateForInsert();
    }
	
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDateCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDateModification(new NSTimestamp());
	}
	
}
