// DO NOT EDIT.  Make changes to EOPeriodeRegimeInscription.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPeriodeRegimeInscription extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_PeriodeRegimeInscription";

  // Attribute Keys
  public static final ERXKey<Integer> PERIODE_ID = new ERXKey<Integer>("periodeId");
  public static final ERXKey<Integer> REGIME_INSCRIPTION_ID = new ERXKey<Integer>("regimeInscriptionId");
  // Relationship Keys

  // Attributes
  public static final String PERIODE_ID_KEY = PERIODE_ID.key();
  public static final String REGIME_INSCRIPTION_ID_KEY = REGIME_INSCRIPTION_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOPeriodeRegimeInscription.class);

  public EOPeriodeRegimeInscription localInstanceIn(EOEditingContext editingContext) {
    EOPeriodeRegimeInscription localInstance = (EOPeriodeRegimeInscription)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer periodeId() {
    return (Integer) storedValueForKey(_EOPeriodeRegimeInscription.PERIODE_ID_KEY);
  }

  public void setPeriodeId(Integer value) {
    if (_EOPeriodeRegimeInscription.LOG.isDebugEnabled()) {
    	_EOPeriodeRegimeInscription.LOG.debug( "updating periodeId from " + periodeId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriodeRegimeInscription.PERIODE_ID_KEY);
  }

  public Integer regimeInscriptionId() {
    return (Integer) storedValueForKey(_EOPeriodeRegimeInscription.REGIME_INSCRIPTION_ID_KEY);
  }

  public void setRegimeInscriptionId(Integer value) {
    if (_EOPeriodeRegimeInscription.LOG.isDebugEnabled()) {
    	_EOPeriodeRegimeInscription.LOG.debug( "updating regimeInscriptionId from " + regimeInscriptionId() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriodeRegimeInscription.REGIME_INSCRIPTION_ID_KEY);
  }


  public static EOPeriodeRegimeInscription createSco_PeriodeRegimeInscription(EOEditingContext editingContext, Integer periodeId
, Integer regimeInscriptionId
) {
    EOPeriodeRegimeInscription eo = (EOPeriodeRegimeInscription) EOUtilities.createAndInsertInstance(editingContext, _EOPeriodeRegimeInscription.ENTITY_NAME);    
		eo.setPeriodeId(periodeId);
		eo.setRegimeInscriptionId(regimeInscriptionId);
    return eo;
  }

  public static ERXFetchSpecification<EOPeriodeRegimeInscription> fetchSpec() {
    return new ERXFetchSpecification<EOPeriodeRegimeInscription>(_EOPeriodeRegimeInscription.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPeriodeRegimeInscription> fetchAllSco_PeriodeRegimeInscriptions(EOEditingContext editingContext) {
    return _EOPeriodeRegimeInscription.fetchAllSco_PeriodeRegimeInscriptions(editingContext, null);
  }

  public static NSArray<EOPeriodeRegimeInscription> fetchAllSco_PeriodeRegimeInscriptions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPeriodeRegimeInscription.fetchSco_PeriodeRegimeInscriptions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPeriodeRegimeInscription> fetchSco_PeriodeRegimeInscriptions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPeriodeRegimeInscription> fetchSpec = new ERXFetchSpecification<EOPeriodeRegimeInscription>(_EOPeriodeRegimeInscription.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPeriodeRegimeInscription> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPeriodeRegimeInscription fetchSco_PeriodeRegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodeRegimeInscription.fetchSco_PeriodeRegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodeRegimeInscription fetchSco_PeriodeRegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPeriodeRegimeInscription> eoObjects = _EOPeriodeRegimeInscription.fetchSco_PeriodeRegimeInscriptions(editingContext, qualifier, null);
    EOPeriodeRegimeInscription eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_PeriodeRegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodeRegimeInscription fetchRequiredSco_PeriodeRegimeInscription(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriodeRegimeInscription.fetchRequiredSco_PeriodeRegimeInscription(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriodeRegimeInscription fetchRequiredSco_PeriodeRegimeInscription(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPeriodeRegimeInscription eoObject = _EOPeriodeRegimeInscription.fetchSco_PeriodeRegimeInscription(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_PeriodeRegimeInscription that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriodeRegimeInscription localInstanceIn(EOEditingContext editingContext, EOPeriodeRegimeInscription eo) {
    EOPeriodeRegimeInscription localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
