// DO NOT EDIT.  Make changes to EOJuryRole.java instead.
package org.cocktail.fwkcktlscolpeda.serveur.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOJuryRole extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "Sco_JuryRole";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOJuryRole.class);

  public EOJuryRole localInstanceIn(EOEditingContext editingContext) {
    EOJuryRole localInstance = (EOJuryRole)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOJuryRole.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOJuryRole.LOG.isDebugEnabled()) {
    	_EOJuryRole.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryRole.CODE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOJuryRole.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOJuryRole.LOG.isDebugEnabled()) {
    	_EOJuryRole.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOJuryRole.LIBELLE_KEY);
  }


  public static EOJuryRole createSco_JuryRole(EOEditingContext editingContext, String code
, String libelle
) {
    EOJuryRole eo = (EOJuryRole) EOUtilities.createAndInsertInstance(editingContext, _EOJuryRole.ENTITY_NAME);    
		eo.setCode(code);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOJuryRole> fetchSpec() {
    return new ERXFetchSpecification<EOJuryRole>(_EOJuryRole.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOJuryRole> fetchAllSco_JuryRoles(EOEditingContext editingContext) {
    return _EOJuryRole.fetchAllSco_JuryRoles(editingContext, null);
  }

  public static NSArray<EOJuryRole> fetchAllSco_JuryRoles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOJuryRole.fetchSco_JuryRoles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOJuryRole> fetchSco_JuryRoles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOJuryRole> fetchSpec = new ERXFetchSpecification<EOJuryRole>(_EOJuryRole.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOJuryRole> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOJuryRole fetchSco_JuryRole(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryRole.fetchSco_JuryRole(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryRole fetchSco_JuryRole(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOJuryRole> eoObjects = _EOJuryRole.fetchSco_JuryRoles(editingContext, qualifier, null);
    EOJuryRole eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Sco_JuryRole that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryRole fetchRequiredSco_JuryRole(EOEditingContext editingContext, String keyName, Object value) {
    return _EOJuryRole.fetchRequiredSco_JuryRole(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOJuryRole fetchRequiredSco_JuryRole(EOEditingContext editingContext, EOQualifier qualifier) {
    EOJuryRole eoObject = _EOJuryRole.fetchSco_JuryRole(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Sco_JuryRole that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOJuryRole localInstanceIn(EOEditingContext editingContext, EOJuryRole eo) {
    EOJuryRole localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
