package org.cocktail.fwkcktlscolpeda.serveur;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlpersonne.common.metier.manager.CktlGrhumParamManager;

import er.extensions.eof.ERXEC;

/**
 * Gestion des parametres
 * @author juliencallewaert
 */
public class FwkCktlScolPedaParamManager extends CktlGrhumParamManager {

	public static final String FORMAT_ANNEE_EXERCICE_ANNEE_CIVILE = "org.cocktail.fwkcktlscolpeda.formatAnneeExercice_AnneeCivile";
	private static final String GENERATEUR_NUMERO_ETUDIANT_PREFIXE = "org.cocktail.coriandre.generateur.numeroEtudiantPrefixe";
	public static final String GRHUM_ANNEE_UNIVERSITAIRE = "GRHUM_ANNEE_UNIVERSITAIRE";
	public static final String GIROFLE_NB_MIN_ECTS_SEMESTRE = "org.cocktail.girofle.nbMinCreditECTSPourSemestre";
	public static final String CALCUL_MOYENNE_PONDEREE_MANQUE_NOTE = "org.cocktail.fwkcktlscolpeda.calcul.moyenne.ponderee.manque.note";

	/**
	 * Constructeur par défaut. Initialise le paramètre FORMAT_ANNEE_EXERCICE_ANNEE_CIVILE
	 */
	public FwkCktlScolPedaParamManager() {
		super(ERXEC.newEditingContext());
		addParam(FORMAT_ANNEE_EXERCICE_ANNEE_CIVILE,
		    "Indique si l'on doit afficher l'année d'exercice au format d'une année civile (ex: 2014 valeur O) ou universitaire (ex: 2014/2015  valeur N)", "N",
		    EOGrhumParametresType.codeActivation);
		addParam(GENERATEUR_NUMERO_ETUDIANT_PREFIXE, "Préfixe numérique du numéro d'étudiant généré", "34", EOGrhumParametresType.valeurNumerique);
		addParam(GIROFLE_NB_MIN_ECTS_SEMESTRE, "nombre mini de credit ECTS pour un semestre", "30", EOGrhumParametresType.valeurNumerique);
		addParam(CALCUL_MOYENNE_PONDEREE_MANQUE_NOTE, "Lance le calcul d'une moyenne pondérée même si une note est manquante (O(defaut)/N", "O", EOGrhumParametresType.codeActivation);
	}

	/**
	 * @return le préfixe pour le générateur de numéros d'étudiant
	 */
	public int getGenerateurNumeroEtudiantPrefix() {
		return getIntParam(GENERATEUR_NUMERO_ETUDIANT_PREFIXE);
	}

	public Integer getGirofleNbMinEctsSemestre() {
		return getIntParam(GIROFLE_NB_MIN_ECTS_SEMESTRE);
	}

	public Integer getGirofleNbMinEctsAnnee() {
		return getGirofleNbMinEctsSemestre() * 2;
	}

	/**
	 * @return <code>true</code> si l'année universitaire correspond à l'année civile
	 *         et donc être présentée et affichée comme une année civile (2014) et non universitaire (2014/2015)
	 */
	public boolean isFormatAnneeCivile() {
		return getBoolParam(FORMAT_ANNEE_EXERCICE_ANNEE_CIVILE);
	}
	
	public boolean getCalculMoyennePondereeSiManqueNote() {
		return getBoolParam(CALCUL_MOYENNE_PONDEREE_MANQUE_NOTE);
	}
}
