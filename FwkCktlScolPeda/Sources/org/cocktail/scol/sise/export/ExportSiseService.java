package org.cocktail.scol.sise.export;

import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ISise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Classe de service pour l'export SISE.
 * 
 * @author Pascal MACOUIN
 */
public class ExportSiseService {
	
	@Autowired
	ExportSiseRepository exportSiseRepository;
	
	/**
	 * Vide complètement la table des données SISE exportées précédemment.
	 * @return Le nombre de ligne supprimées
	 */
	@Transactional
	public long viderTableSise() {
		return exportSiseRepository.viderTableSise();
	}
	
	/**
	 * @param type le type d'établissement {@link ISise#ECOLE} ou {@link ISise#UNIV}
	 * @return les lignes préformatées de la table Sise
	 */
	public List<String> construireFichierSise(String type) {
		List<String> resultats = Collections.emptyList();
		if (ISise.ECOLE.equals(type)) {
			resultats = exportSiseRepository.construireFichierSiseEcole();
		} else if (ISise.UNIV.equals(type)) {
			resultats = exportSiseRepository.construireFichierSiseUniv();
		}
		return resultats;
	}
	
}
