package org.cocktail.scol.sise.export;

import java.util.Arrays;
import java.util.List;

import org.cocktail.scol.support.q.sco_scolarite.QSise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;

/**
 * 
 * @author isabelle
 *
 */
public class ExportSiseRepository {
	private QueryDslJdbcTemplate template;

	@Autowired
	public ExportSiseRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

	/**
	 * Vide complètement la table des données SISE {@link QSiseEcole}.
	 * @return le nombre d'enregistrement supprimés
	 */
	public long viderTableSise() {
		return template.delete(QSise.sise, new ExportSiseCallBackDeleteTout());
	}

	public List<String> construireFichierSiseEcole() {
		SQLQuery sqlQuery = template.newSqlQuery().from(QSise.sise);
		StringExpression concatenationColonnes = concatener(
				QSise.sise.numins,
				QSise.sise.idetu,
				QSise.sise.compos,
				QSise.sise.inspr,
				QSise.sise.regime,
				QSise.sise.annais,
				QSise.sise.sexe,
				QSise.sise.bac,
				QSise.sise.acabac,
				QSise.sise.anbac,
				QSise.sise.situpre,
				QSise.sise.dipder,
				QSise.sise.curpar,
				QSise.sise.nation,
				QSise.sise.paripa,
				QSise.sise.cpEtu,
				QSise.sise.paypar,
				QSise.sise.cpPar,
				QSise.sise.pcspar,
				QSise.sise.echang,
				QSise.sise.diplom,
				QSise.sise.niveau,
				QSise.sise.specia,
				QSise.sise.specib,
				QSise.sise.specic,
				QSise.sise.reimma,
				QSise.sise.ansup,
				QSise.sise.anetab,
				QSise.sise.lcometu,
				QSise.sise.lcomref,
				QSise.sise.amena,
				QSise.sise.typrepa,
				QSise.sise.pcspar2,
				QSise.sise.numed,
				QSise.sise.fonctio
				);
		List<String> resultats = template.query(sqlQuery, concatenationColonnes);
		return resultats;
	}

	private StringExpression concatener(StringPath...paths) {
		StringExpression concatenation = paths[0];
		for (StringPath path : Arrays.copyOfRange(paths, 1, paths.length)) {
			concatenation = concatenation.concat(path);
		}
		return concatenation;
	}

	public List<String> construireFichierSiseUniv() {
		SQLQuery sqlQuery = template.newSqlQuery().from(QSise.sise);
		StringExpression concatenationColonnes = concatener(
				QSise.sise.numins,
				QSise.sise.idetu,
				QSise.sise.compos,
				QSise.sise.inspr,
				QSise.sise.regime,
				QSise.sise.jonais,
				QSise.sise.monais,
				QSise.sise.annais,
				QSise.sise.sexe,
				QSise.sise.bac,
				QSise.sise.anbac,
				QSise.sise.aninsc,
				QSise.sise.situpre,
				QSise.sise.conv,
				QSise.sise.dipder,
				QSise.sise.curpar,
				QSise.sise.nation,
				QSise.sise.paripa,
				QSise.sise.cpEtu,
				QSise.sise.paypar,
				QSise.sise.cpPar,
				QSise.sise.pcspar,
				QSise.sise.activi,
				QSise.sise.echang,
				QSise.sise.diplom,
				QSise.sise.niveau,
				QSise.sise.specia,
				QSise.sise.specib,
				QSise.sise.specic,
				QSise.sise.numed,
				QSise.sise.reimma,
				QSise.sise.ansup,
				QSise.sise.anetab,
				QSise.sise.cometu,
				QSise.sise.comref,
				QSise.sise.lcometu,
				QSise.sise.lcomref,
				QSise.sise.depbac,
				QSise.sise.amena,
				QSise.sise.typrepa,
				QSise.sise.dispens,
				QSise.sise.pcspar2,
				QSise.sise.fonctio,
				QSise.sise.telens,
				QSise.sise.dom1,
				QSise.sise.dom2,
				QSise.sise.dom3,
				QSise.sise.dom4,
				QSise.sise.dom5,
				QSise.sise.parType,
				QSise.sise.cge
				);
		List<String> resultats = template.query(sqlQuery, concatenationColonnes);
		return resultats;
	}

}
