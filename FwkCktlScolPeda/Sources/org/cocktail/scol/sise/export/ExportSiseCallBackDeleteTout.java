package org.cocktail.scol.sise.export;

import org.springframework.data.jdbc.query.SqlDeleteCallback;

import com.mysema.query.sql.dml.SQLDeleteClause;

/**
 * Callback pour supprimer toutes les données de la table {@link QSiseEcole} ou {@link QSiseUniv}.
 * 
 * @author Pascal MACOUIN
 */
public class ExportSiseCallBackDeleteTout implements SqlDeleteCallback {
	public long doInSqlDeleteClause(SQLDeleteClause delete) {
		return delete.execute();
	}
}
