package org.cocktail.scol.support;


import javax.sql.DataSource;

import org.cocktail.scol.support.oracle.OracleQueryExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.SQLTemplates;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSDictionary;
import com.zaxxer.hikari.HikariDataSource;

/**
 * 
 */
@Configuration
@EnableTransactionManagement
public class PersistenceConfig {

	protected SQLTemplates oracleTmpl;

	public PersistenceConfig() {
		this.oracleTmpl = OracleTemplates.builder().printSchema().build();
	}
	
	@Bean
	public DataSource dataSource() {
		NSDictionary<String, Object> connectionDictionary = getConnectionDictionaryScol();
		String password = (String) connectionDictionary.get("password");
		String userName = (String) connectionDictionary.get("username");
		String url = (String) connectionDictionary.get("URL");
		
		HikariDataSource datasource = new HikariDataSource();
		datasource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		datasource.setJdbcUrl(url);
		datasource.setUsername(userName);
		datasource.setPassword(password);
		datasource.setAutoCommit(false);
		datasource.setMaximumPoolSize(5);
		datasource.setConnectionTestQuery("select 1 from dual");
		
		return datasource;
	}

	private NSDictionary<String, Object> getConnectionDictionaryScol() {
		NSDictionary<String, Object> connectionDictionary = EOModelGroup.defaultGroup().modelNamed("Sco_scolarite").connectionDictionary();
		return connectionDictionary;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
	
	@Bean
	public QueryDslJdbcTemplate queryDslJdbcTemplate(JdbcTemplate jdbcTemplate) {
		return new QueryDslJdbcTemplate(jdbcTemplate, oracleTmpl);
	}
	
	@Bean
	public OracleQueryExecutor oracleQueryExecutor(JdbcTemplate jdbcTemplate, QueryDslJdbcTemplate queryDslJdbcTemplate) {
		return new OracleQueryExecutor(queryDslJdbcTemplate, jdbcTemplate, this.oracleTmpl);
	}
	
}
