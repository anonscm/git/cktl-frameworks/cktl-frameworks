package org.cocktail.scol.support.oracle;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;
import org.springframework.data.jdbc.query.UncategorizedQueryException;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mysema.query.QueryException;
import com.mysema.query.sql.SQLTemplates;
import com.mysema.query.sql.oracle.OracleQuery;
import com.mysema.query.types.Expression;

/**
 *	Permet d'executer des OracleQuery dans 
 *	un QueryDslJdbcTemplate
 */
public class OracleQueryExecutor {

	private QueryDslJdbcTemplate queryDslJdbcTemplate;
	private JdbcTemplate jdbcTemplate;
	private SQLTemplates oracleTemplates;
	
	/**
	 * @param queryDslJdbcTemplate le template pour l'execution
	 * @param jdbcTemplate le template de base jdbc
	 */
	public OracleQueryExecutor(QueryDslJdbcTemplate queryDslJdbcTemplate, JdbcTemplate jdbcTemplate, SQLTemplates oracleTemplates) {
		this.queryDslJdbcTemplate = queryDslJdbcTemplate;
		this.jdbcTemplate = jdbcTemplate;
		this.oracleTemplates = oracleTemplates;
	}
	
	/**
	 * @param <T> le type générique d'élément recherché
	 * @param query la requete à executer
	 * @param expression tel le mapping
	 * @return la liste des resultats correspondants à la requete
	 */
	public <T> List<T> query(final OracleQuery query, final Expression<T> expression) {
		return queryDslJdbcTemplate.getJdbcOperations().execute(new ConnectionCallback<List<T>>() {
			public List<T> doInConnection(Connection con) throws SQLException, DataAccessException {
				OracleQuery liveQuery = query.clone(con);
				try {
					return liveQuery.list(expression);
				} catch (QueryException qe) {
					throw translateQueryException(qe, "SQLQuery", liveQuery.toString());
				}

			}
		});
	}
	
	/**
	 * @return une nouvelle instance de OracleQuery
	 */
	public OracleQuery newQuery() {
		return new OracleQuery(null, oracleTemplates);
	}
	
	/**
	 * Méthode utilitaire pour la gestion d'exceptions SQL
	 */
	private RuntimeException translateQueryException(QueryException qe, String task, String query) {
		Throwable t = qe.getCause();
		if (t instanceof SQLException) {
			return jdbcTemplate.getExceptionTranslator()
					.translate(task, query, (SQLException) t);
		}
		return new UncategorizedQueryException("Error in " + "SQLQuery", qe);
	}
	
}
