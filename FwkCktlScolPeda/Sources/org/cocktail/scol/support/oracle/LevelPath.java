package org.cocktail.scol.support.oracle;

import com.mysema.query.types.path.NumberPath;

/**
 *	Permet de configurer le level dans une requete hierarchique Oracle
 */
public final class LevelPath extends NumberPath<Long> {

	private static final long serialVersionUID = -3028621392369808119L;

	private static final String LEVEL = "LEVEL";
	
	public static final LevelPath PATH = new LevelPath(Long.class, LEVEL);

	private LevelPath(Class<? extends Long> type, String var) {
		super(type, var);
	}

}
