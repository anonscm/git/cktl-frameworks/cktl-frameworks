package org.cocktail.scol.support.oracle;

import com.google.common.collect.ImmutableList;
import com.mysema.query.types.Path;
import com.mysema.query.types.TemplateFactory;
import com.mysema.query.types.template.StringTemplate;

public class SysConnectByPathExpression extends StringTemplate {

	private static final long serialVersionUID = 1L;
	private static final String TEMPLATE = "SYS_CONNECT_BY_PATH({0},{1})";
	
	public SysConnectByPathExpression(Path<?> path, String separator) {
		super(TemplateFactory.DEFAULT.create(TEMPLATE), ImmutableList.of(path, StringTemplate.create("'" + separator + "'")));
	}

}
