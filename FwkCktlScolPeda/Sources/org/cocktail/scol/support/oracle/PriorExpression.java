package org.cocktail.scol.support.oracle;

import com.google.common.collect.ImmutableList;
import com.mysema.query.types.Path;
import com.mysema.query.types.TemplateFactory;
import com.mysema.query.types.template.NumberTemplate;

public class PriorExpression<T extends Number & Comparable<?>> extends NumberTemplate<T> {

	private static final long serialVersionUID = -5142177872770606887L;
	private static final String TEMPLATE  =  "PRIOR {0}";
	
	public PriorExpression(Class<T> clazz, Path<?> path) {
		super(clazz, TemplateFactory.DEFAULT.create(TEMPLATE), ImmutableList.of(path));
	}

}
