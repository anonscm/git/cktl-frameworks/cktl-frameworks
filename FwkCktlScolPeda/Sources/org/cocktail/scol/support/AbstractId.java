package org.cocktail.scol.support;

/**
 * Cette classe représente la clé primaire d'une table.
 * 
 * @author Pascal MACOUIN
 *
 * @param <T> Le type de la clé primaire
 */
public abstract class AbstractId<T> {
	private T id;
	
	protected AbstractId(T id) {
		this.setId(id);
	}
	
	private void setId(T id) {
		this.id = id;
	}
	
	/**
	 * @return La valeur de l'Id
	 */
	public T id() {
		return id;
	}
	
	public T getId() {
		return id;
	}

	@Override
	public String toString() {
		return id.toString();
	}
}
