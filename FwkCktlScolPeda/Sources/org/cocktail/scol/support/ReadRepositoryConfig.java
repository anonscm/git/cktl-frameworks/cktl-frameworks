package org.cocktail.scol.support;

import org.cocktail.scol.examen.ExamenEtudiantReadRepository;
import org.cocktail.scol.ia.InscriptionAdministrativeTermineeReadService;
import org.cocktail.scol.ip.InscriptionPedagogiqueReadRepository;
import org.cocktail.scol.jury.JuryReadService;
import org.cocktail.scol.maquette.AEReadRepository;
import org.cocktail.scol.maquette.ComposantReadRepository;
import org.cocktail.scol.maquette.ComposantSyllabusReadRepository;
import org.cocktail.scol.maquette.DiplomeReadService;
import org.cocktail.scol.maquette.LienConsultationReadRepository;
import org.cocktail.scol.maquette.PerimetreStrategieReadService;
import org.cocktail.scol.maquette.RegimeInscriptionReadRepository;
import org.cocktail.scol.sise.export.ExportSiseRepository;
import org.cocktail.scol.support.oracle.OracleQueryExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

@Configuration
public class ReadRepositoryConfig {

	@Bean
	public AEReadRepository aeReadRepository(QueryDslJdbcTemplate template) {
		return new AEReadRepository(template);
	}

	@Bean
	public DiplomeReadService diplomeReadService(QueryDslJdbcTemplate template) {
		return new DiplomeReadService(template);
	}

	@Bean
	public ComposantReadRepository composantReadService(QueryDslJdbcTemplate template) {
		return new ComposantReadRepository(template);
	}

	@Bean
	public ExamenEtudiantReadRepository examenEtudiantRepository(QueryDslJdbcTemplate template) {
		return new ExamenEtudiantReadRepository(template);
	}

	@Bean
	public ExportSiseRepository exportSiseRepository(QueryDslJdbcTemplate template) {
		return new ExportSiseRepository(template);
	}

	@Bean
	public InscriptionPedagogiqueReadRepository inscriptionPedagogiqueReadRepository(QueryDslJdbcTemplate template) {
		return new InscriptionPedagogiqueReadRepository(template);
	}

	@Bean
	public InscriptionAdministrativeTermineeReadService inscriptionAdministrativeTermineeReadService(QueryDslJdbcTemplate template) {
		return new InscriptionAdministrativeTermineeReadService(template);
	}

	@Bean
	public JuryReadService juryReadService(QueryDslJdbcTemplate template) {
		return new JuryReadService(template);
	}

	@Bean
	public LienConsultationReadRepository lienConsultationReadRepository(QueryDslJdbcTemplate template) {
		return new LienConsultationReadRepository(template);
	}

	@Bean
	public PerimetreStrategieReadService perimetreStrategieReadService(QueryDslJdbcTemplate template) {
		return new PerimetreStrategieReadService(template);
	}

	@Bean
	public ComposantSyllabusReadRepository composantSyllabusReadService(OracleQueryExecutor oracleQueryExecutor) {
		return new ComposantSyllabusReadRepository(oracleQueryExecutor);
	}

	@Bean
	public RegimeInscriptionReadRepository regimeInscriptionReadRepository(QueryDslJdbcTemplate template) {
		return new RegimeInscriptionReadRepository(template);
	}
}
