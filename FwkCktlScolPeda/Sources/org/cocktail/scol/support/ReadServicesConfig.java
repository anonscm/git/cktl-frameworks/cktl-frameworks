package org.cocktail.scol.support;

import org.cocktail.scol.examen.ExamenEtudiantReadService;
import org.cocktail.scol.sise.export.ExportSiseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReadServicesConfig {
	
	@Bean
	public ExamenEtudiantReadService examenEtudiantService() {
		return new ExamenEtudiantReadService();
	}
	
	@Bean
	public ExportSiseService exportSiseService() {
		return new ExportSiseService();
	}
}
