package org.cocktail.scol.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.mysema.query.Tuple;
import com.mysema.query.sql.RelationalPath;
import com.mysema.query.sql.SQLExpressions;
import com.mysema.query.types.expr.SimpleExpression;

public class QueryDslHelper {

	private static final String SUFFIX_SEQUENCE = "_SEQ";
	
	public static <T, K> Map<K, List<T>> flatten(List<Tuple> tuples, final SimpleExpression<K> identifiantExp, final SimpleExpression<T> exp) {
		return flatten(tuples, identifiantExp, new SimpleExpressionCollector(exp));
	}
	
	public static <T, K> Map<K, List<T>> flatten(List<Tuple> tuples, final SimpleExpression<K> identifiantExp, final Function<Tuple, T> collector) {
		Map<K, List<T>> index = new HashMap<K, List<T>>();
		for (Tuple tuple : tuples) {
			T valeurExp = collector.apply(tuple);
			K valeurId = tuple.get(identifiantExp);
			List<T> valeursExistantes = index.get(valeurId);
			if (valeursExistantes == null) {
				valeursExistantes = new ArrayList<T>();
				index.put(valeurId, valeursExistantes);
			}
			if (valeurExp != null) {
				valeursExistantes.add(valeurExp);
			}
		}
		return index;
	}
	
	public static class SimpleExpressionCollector<T> implements Function<Tuple, T> {

		private SimpleExpression<T> expression;
		
		public SimpleExpressionCollector(SimpleExpression<T> expression) {
			this.expression = expression;
		}
		
		public T apply(Tuple tuple) {
			return tuple.get(expression);
		}
		
	}
	
	/**
	 * @param table Une table (Q....)
	 * @return Une expression pour récupérer la valeur suivante d'une séquence
	 */
	public static SimpleExpression<Long> getNextValSequence(RelationalPath<?> table) {
		return SQLExpressions.nextval(table.getSchemaName() + "." + table.getTableName() + SUFFIX_SEQUENCE);
	}
}
