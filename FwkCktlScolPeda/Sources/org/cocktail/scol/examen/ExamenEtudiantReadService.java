package org.cocktail.scol.examen;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IExamenEtudiant;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Cette classe représente les services pour {@link IExamenEtudiant}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadService {
	@Autowired
	private ExamenEtudiantReadRepository examenEtudiantRepository;
	
	/**
	 * Enregistre la répartition des étudiants.
	 * @param listeEtudiant Une liste d'étudiants
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return <code>true</code> si l'appelant doit recharger les données
	 */
	@Transactional
	public boolean enregistrerListesExamenEtudiant(List<ExamenEtudiantRead> listeEtudiant, UserInfo userInfo) {
		boolean rechargementNecessaire = false;
		final List<ExamenEtudiantRead> listeDesInsert = new ArrayList<ExamenEtudiantRead>();
		final List<ExamenEtudiantRead> listeDesDelete = new ArrayList<ExamenEtudiantRead>();
		final List<ExamenEtudiantRead> listeDesUpdate = new ArrayList<ExamenEtudiantRead>();
		
		for (ExamenEtudiantRead etudiant : listeEtudiant) {
			if (etudiant.getExamenOrganisationIdOrigine() == null && etudiant.getExamenOrganisationId() != null) {
				listeDesInsert.add(etudiant);
			} else if (etudiant.getExamenOrganisationIdOrigine() != null && etudiant.getExamenOrganisationId() == null) {
				listeDesDelete.add(etudiant);
			} else if (etudiant.getExamenOrganisationIdOrigine() != null && etudiant.getExamenOrganisationId() != null
					&& !etudiant.getExamenOrganisationIdOrigine().equals(etudiant.getExamenOrganisationId())) {
				listeDesUpdate.add(etudiant);
			}
		}

		if (!listeDesInsert.isEmpty()) {
			examenEtudiantRepository.insererExamenEtudiant(listeDesInsert, userInfo);
			rechargementNecessaire = true;
		}
		
		if (!listeDesDelete.isEmpty()) {
			examenEtudiantRepository.supprimerExamenEtudiant(listeDesDelete);
		}
		
		if (!listeDesUpdate.isEmpty()) {
			examenEtudiantRepository.changerOrganisationExamenEtudiant(listeDesUpdate, userInfo);
		}
		
		return rechargementNecessaire;
	}
	
	/**
	 * Marquer tous les étudiants d'un examen comme convoqué.
	 * @param examenId Un id examen
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre d'étudiant marqué comme convoqué
	 */
	@Transactional
	public long marquerTousEtudiantsConvoque(QExamenId examenId, UserInfo userInfo) {
		return examenEtudiantRepository.marquerTousEtudiantsConvoque(examenId, userInfo);
	}

	/**
	 * Marquer tous les étudiants d'un examen comme non convoqué.
	 * @param examenId Un id examen
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre d'étudiant marqué comme non convoqué
	 */
	@Transactional
	public long marquerTousEtudiantsConvocationAnnulee(QExamenId examenId, UserInfo userInfo) {
		return examenEtudiantRepository.marquerTousEtudiantsConvocationAnnulee(examenId, userInfo);
	}
}
