package org.cocktail.scol.examen;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IExamenEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IExamenOrganisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogique;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.ref.support.q.grhum.QEtudiant;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.grhum.QRepartPersonneAdresse;
import org.cocktail.ref.support.q.workflow.QDemande;
import org.cocktail.ref.support.q.workflow.QEtape;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QEtudiantAnnee;
import org.cocktail.scol.support.q.sco_scolarite.QExamen;
import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.cocktail.scol.support.q.sco_scolarite.QExamenOrganisation;
import org.cocktail.scol.support.q.sco_scolarite.QInscription;
import org.cocktail.scol.support.q.sco_scolarite.QInscriptionPedaElt;
import org.cocktail.scol.support.q.sco_scolarite.QInscriptionPedagogique;
import org.cocktail.scol.support.q.sco_scolarite.QLien;
import org.cocktail.scol.support.q.sco_scolarite.QSessionPeriode;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.types.Expression;

/**
 * Cette classe représente l'implémentation QueryDsl de lecture/maj des {@link IExamenEtudiant}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadRepository {
	private QueryDslJdbcTemplate template;
	
	private QLien tLien = QLien.lien;
	private QInscriptionPedaElt tInscriptionPedaElement = QInscriptionPedaElt.inscriptionPedaElt;
	private QInscriptionPedagogique tInscriptionPedagogique = QInscriptionPedagogique.inscriptionPedagogique;
	private QInscription tInscription = QInscription.inscription;
	private QEtudiant tEtudiant = QEtudiant.etudiant;
	private QEtudiantAnnee tEtudiantAnnee = QEtudiantAnnee.etudiantAnnee;
	private QIndividuUlr tIndividuUlr = QIndividuUlr.individuUlr;
	private QRepartPersonneAdresse tRepartPersonneAdresse = QRepartPersonneAdresse.repartPersonneAdresse;
	private QComposant tComposant = QComposant.composant;
	private QExamen tExamen = QExamen.examen;
	private QExamenEtudiants tExamenEtudiants = QExamenEtudiants.examenEtudiants;
	private QExamenOrganisation tExamenOrganisation = QExamenOrganisation.examenOrganisation;
	private QSessionPeriode tSessionPeriode = QSessionPeriode.sessionPeriode;
	private QDemande tDemande = QDemande.demande;
	private QEtape tEtape = QEtape.etape;

	@Autowired
	public ExamenEtudiantReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

	/**
	 * Liste des étudiants possibles (ceux affectables) pour un examen.
	 * @param examenId Un id examen
	 * @return Un liste d'étudiant
	 */
	public List<ExamenEtudiantRead> getListeEtudiantPossibleExamen(QExamenId examenId) {
		SQLSubQuery examen = new SQLSubQuery()
			.from(tExamen)
			.where(tExamen.idExamen.eq(examenId.getId()));
		SQLSubQuery listeDesOrganisationsExamen = new SQLSubQuery()
			.from(tExamenOrganisation)
			.where(tExamenOrganisation.idExamen.eq(examenId.getId()));
		
		SQLQuery query = template.newSqlQuery()
				.distinct()
				.from(tInscriptionPedaElement)
				.innerJoin(tLien).on(tLien.idLien.eq(tInscriptionPedaElement.idLien))
				.innerJoin(tInscriptionPedagogique).on(tInscriptionPedagogique.idInscriptionPeda.eq(tInscriptionPedaElement.idInscriptionPeda))
				.innerJoin(tInscription).on(tInscription.idInscription.eq(tInscriptionPedagogique.idInscription))
				.innerJoin(tEtudiant).on(tEtudiant.etudNumero.eq(tInscription.idEtudiant))
				.innerJoin(tIndividuUlr).on(tIndividuUlr.noIndividu.eq(tEtudiant.noIndividu))
				.innerJoin(tComposant).on(tComposant.idComposant.eq(tInscription.idDiplome))
				.leftJoin(tExamenEtudiants).on(tExamenEtudiants.idEtudiantAnnee.eq(tInscription.idEtudiantAnnee)
						.and(tExamenEtudiants.idExamenOrganisation.in(listeDesOrganisationsExamen.list(tExamenOrganisation.idExamenOrganisation))))
				.innerJoin(tDemande).on(tDemande.idDemande.eq(tInscriptionPedagogique.idDemande))
				.innerJoin(tEtape).on(tEtape.idEtape.eq(tDemande.idEtape))
				.where(tLien.childId.eq(examen.unique(tExamen.idComposantAe))
						.and(tEtape.cEtape.eq(EtapeInscriptionPedagogique.VALIDEE.getCodeEtape())))
				.orderBy(tIndividuUlr.nomAffichage.asc(), tIndividuUlr.prenomAffichage.asc());
		
		Expression<?>[] paths = new Expression<?>[] {
				tInscription.idEtudiantAnnee,
				tEtudiant.etudCodeIne,
				tEtudiant.etudNumero,
				tIndividuUlr.nomAffichage,
				tIndividuUlr.prenomAffichage,
				tComposant.llComposant,
				tExamenEtudiants.idExamenEtudiants,
				tExamenEtudiants.idExamenOrganisation
		};
		
		ExamenEtudiantReadMapping examenEtudiantReadMapping = new ExamenEtudiantReadMapping(paths, tComposant, null);
		List<ExamenEtudiantRead> res = template.query(query, examenEtudiantReadMapping);
		
		return res;
	}
	
	/**
	 * Retourne la liste des étudiants d'un examen et joignable par mail (normalement tous les étudiants ont un mail).
	 * @param examenId Un id examen
	 * @param convoque <code>false</code> pour la liste des étudiants non convoqués, <code>true</code> pour la liste des étudiants convoqués 
	 * @return La liste des étudiants convoqués/non convoqués
	 */
	public List<ExamenEtudiantRead> getListeEtudiantJoignableParMail(QExamenId examenId, boolean convoque) {
		SQLQuery query = template.newSqlQuery()
				.from(tExamenEtudiants)
				.innerJoin(tExamenOrganisation).on(tExamenOrganisation.idExamenOrganisation.eq(tExamenEtudiants.idExamenOrganisation))
				.innerJoin(tExamen).on(tExamen.idExamen.eq(tExamenOrganisation.idExamen))
				.innerJoin(tComposant).on(tComposant.idComposant.eq(tExamen.idComposantAe))
				.innerJoin(tSessionPeriode).on(tSessionPeriode.idSessionPeriode.eq(tExamen.idSessionPeriode))
				.innerJoin(tEtudiantAnnee).on(tEtudiantAnnee.idEtudiantAnnee.eq(tExamenEtudiants.idEtudiantAnnee))
				.innerJoin(tEtudiant).on(tEtudiant.etudNumero.eq(tEtudiantAnnee.idEtudiant))
				.innerJoin(tIndividuUlr).on(tIndividuUlr.noIndividu.eq(tEtudiant.noIndividu))
				.innerJoin(tRepartPersonneAdresse).on(tRepartPersonneAdresse.persId.eq(tIndividuUlr.persId))
				.where(tExamen.idExamen.eq(examenId.getId())
						.and(tExamenEtudiants.convoque.eq(convoque ? 1 : 0))
						.and(tRepartPersonneAdresse.rpaValide.eq(EORepartPersonneAdresse.RPA_VALIDE_OUI))
						.and(tRepartPersonneAdresse.tadrCode.eq(EOTypeAdresse.TADR_CODE_ETUD)));
		
		Expression<?>[] paths = new Expression<?>[] {
				tExamenEtudiants.idExamenEtudiants,
				tIndividuUlr.nomAffichage,
				tIndividuUlr.prenomAffichage,
				tRepartPersonneAdresse.eMail,
				tComposant.llComposant,
				tSessionPeriode.libelle,
				tExamenOrganisation.dateExamen,
				tExamenOrganisation.salleExamen
		};
		
		ExamenEtudiantReadMapping examenEtudiantReadMapping = new ExamenEtudiantReadMapping(paths, null, tComposant);
		List<ExamenEtudiantRead> res = template.query(query, examenEtudiantReadMapping);
		
		return res;
	}
	
	/**
	 * Affecte les étudiants pour un {@link IExamenOrganisation} (insère un enregistrement en base).
	 * @param listeEtudiant Une liste d'étudiants à insérer dans une organisation
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre de ligne insérées
	 */
	public long insererExamenEtudiant(List<ExamenEtudiantRead> listeEtudiant, UserInfo userInfo) {
		Long persId = userInfo.persId().longValue();
		Timestamp now = new Timestamp(new Date().getTime());
		
		return template.insert(tExamenEtudiants, new ExamenEtudiantReadCallBackInsert(listeEtudiant, persId, now));
	}
	
	/**
	 * Supprime les étudiants pour un {@link IExamenOrganisation} (supprime un enregistrement en base).
	 * @param listeEtudiant Une liste d'étudiants à supprimer de l'organisation
	 * @return Le nombre de ligne supprimées
	 */
	public long supprimerExamenEtudiant(List<ExamenEtudiantRead> listeEtudiant) {
		return template.delete(tExamenEtudiants, new ExamenEtudiantReadCallBackDelete(listeEtudiant));
	}
	
	/**
	 * Change l'affectation des étudiants de {@link IExamenOrganisation} (update un enregistrement en base).
	 * @param listeEtudiant Une liste d'étudiants à changer d'organisation
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre de ligne mises à jour
	 */
	public long changerOrganisationExamenEtudiant(List<ExamenEtudiantRead> listeEtudiant, UserInfo userInfo) {
		Long persId = userInfo.persId().longValue();
		Timestamp now = new Timestamp(new Date().getTime());
		
		return template.update(tExamenEtudiants, new ExamenEtudiantReadCallBackUpdateOrganisation(listeEtudiant, persId, now));
	}
	
	/**
	 * Marquer tous les étudiants d'un examen comme convoqué.
	 * @param examenId Un id examen
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre d'étudiant marqué comme convoqué
	 */
	public long marquerTousEtudiantsConvoque(QExamenId examenId, UserInfo userInfo) {
		Long persId = userInfo.persId().longValue();
		Timestamp now = new Timestamp(new Date().getTime());
		
		return template.update(tExamenEtudiants, new ExamenEtudiantReadCallBackUpdateTousConvoque(examenId, true, persId, now));
	}

	/**
	 * Marquer tous les étudiants d'un examen comme non convoqué.
	 * @param examenId Un id examen
	 * @param userInfo L'utilisateur à l'origine de la mise à jour
	 * @return Le nombre d'étudiant marqué comme non convoqué
	 */
	public long marquerTousEtudiantsConvocationAnnulee(QExamenId examenId, UserInfo userInfo) {
		Long persId = userInfo.persId().longValue();
		Timestamp now = new Timestamp(new Date().getTime());
		
		return template.update(tExamenEtudiants, new ExamenEtudiantReadCallBackUpdateTousConvoque(examenId, false, persId, now));
	}
}
