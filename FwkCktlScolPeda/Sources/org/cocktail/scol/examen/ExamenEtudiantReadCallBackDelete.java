package org.cocktail.scol.examen;

import java.util.List;

import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.springframework.data.jdbc.query.SqlDeleteCallback;

import com.mysema.query.sql.dml.SQLDeleteClause;

/**
 * Callback pour supprimer par lot des données de la table {@link QExamenEtudiants}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadCallBackDelete implements SqlDeleteCallback {
	private QExamenEtudiants tExamenEtudiants = QExamenEtudiants.examenEtudiants;

	private List<ExamenEtudiantRead> listeDesDelete;

	public ExamenEtudiantReadCallBackDelete(List<ExamenEtudiantRead> listeDesDelete) {
		this.listeDesDelete = listeDesDelete;
	}
	
	public long doInSqlDeleteClause(SQLDeleteClause delete) {
		for (ExamenEtudiantRead etudiant : listeDesDelete) {
			delete
				.where(tExamenEtudiants.idExamenEtudiants.eq(etudiant.getExamenEtudiantId().getId()))
				.addBatch();
		}
		
		return delete.execute();
	}
}
