package org.cocktail.scol.examen;

import java.sql.Timestamp;

import org.cocktail.scol.support.q.sco_scolarite.QExamen;
import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.cocktail.scol.support.q.sco_scolarite.QExamenOrganisation;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenId;
import org.springframework.data.jdbc.query.SqlUpdateCallback;

import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.sql.dml.SQLUpdateClause;

/**
 * Callback pour mettre à jour par lot des changement d'organisation (de salle) d'étudiant dans la table {@link QExamenEtudiants}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadCallBackUpdateTousConvoque implements SqlUpdateCallback {
	private QExamen tExamen = QExamen.examen;
	private QExamenOrganisation tExamenOrganisation = QExamenOrganisation.examenOrganisation;
	private QExamenEtudiants tExamenEtudiants = QExamenEtudiants.examenEtudiants;
	
	private QExamenId examenId;
	private boolean convoque;
	private Long persId;
	private Timestamp now;
	
	public ExamenEtudiantReadCallBackUpdateTousConvoque(QExamenId examenId, boolean convoque, Long persId, Timestamp now) {
		this.examenId = examenId;
		this.convoque = convoque;
		this.persId = persId;
		this.now = now;
	}
	
	public long doInSqlUpdateClause(SQLUpdateClause update) {
		SQLSubQuery sqEtudiants = new SQLSubQuery()
			.from(tExamenEtudiants)
			.innerJoin(tExamenOrganisation).on(tExamenOrganisation.idExamenOrganisation.eq(tExamenEtudiants.idExamenOrganisation))
			.innerJoin(tExamen).on(tExamen.idExamen.eq(tExamenOrganisation.idExamen))
			.where(tExamen.idExamen.eq(examenId.getId()));
		
		update
			.set(tExamenEtudiants.convoque, convoque ? 1 : 0)
			.set(tExamenEtudiants.persIdModification, persId)
			.set(tExamenEtudiants.dModification, now)
			.where(tExamenEtudiants.idExamenEtudiants.in(sqEtudiants.list(tExamenEtudiants.idExamenEtudiants)));
		
		return update.execute();
	}
}
