package org.cocktail.scol.examen;

import org.cocktail.ref.support.q.grhum.QEtudiant;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.grhum.QRepartPersonneAdresse;
import org.cocktail.ref.support.q.grhum.id.QEtudiantId;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.cocktail.scol.support.q.sco_scolarite.QExamenOrganisation;
import org.cocktail.scol.support.q.sco_scolarite.QInscription;
import org.cocktail.scol.support.q.sco_scolarite.QSessionPeriode;
import org.cocktail.scol.support.q.sco_scolarite.id.QEtudiantAnneeId;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenEtudiantsId;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenOrganisationId;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

/**
 * Classe de mapping entre QueryDsl et le bean {@link ExamenEtudiantRead}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadMapping extends MappingProjection<ExamenEtudiantRead> {

	private static final long serialVersionUID = 1L;
	private QComposant tComposantDiplome;
	private QComposant tComposantAe;

	/**
	 * Contructeur.
	 * @param paths Un liste de colonne
	 * @param tComposantDiplome La table diplome
	 * @param tComposantAe La table AE
	 */
	public ExamenEtudiantReadMapping(Expression<?>[] paths, QComposant tComposantDiplome, QComposant tComposantAe) {
		super(ExamenEtudiantRead.class, paths);
		this.tComposantDiplome = tComposantDiplome;
		this.tComposantAe = tComposantAe;
	}

	@Override
	protected ExamenEtudiantRead map(Tuple row) {
		ExamenEtudiantRead examenEtudiant = new ExamenEtudiantRead();
		
		examenEtudiant.setExamenEtudiantId(QExamenEtudiantsId.newId(row.get(QExamenEtudiants.examenEtudiants.idExamenEtudiants)));
		examenEtudiant.setEtudAnneeId(QEtudiantAnneeId.newId(row.get(QInscription.inscription.idEtudiantAnnee)));
//		Long numeroEtudiant = row.get(QEtudiant.etudiant.etudNumero);
//		if (numeroEtudiant != null) {
//			examenEtudiant.setEtudNumero(numeroEtudiant.intValue());
//		}
		examenEtudiant.setEtudNumero(QEtudiantId.newId(row.get(QEtudiant.etudiant.etudNumero)));
		examenEtudiant.setEtudNumIne(row.get(QEtudiant.etudiant.etudCodeIne));
		examenEtudiant.setEtudNom(row.get(QIndividuUlr.individuUlr.nomAffichage));
		examenEtudiant.setEtudPrenom(row.get(QIndividuUlr.individuUlr.prenomAffichage));
		examenEtudiant.setEtudAdresseMail(row.get(QRepartPersonneAdresse.repartPersonneAdresse.eMail));
		if (tComposantDiplome != null) {
			examenEtudiant.setDiplomeLibelle(row.get(tComposantDiplome.llComposant));
		}
		if (tComposantAe != null) {
			examenEtudiant.setExamenLibelle(row.get(tComposantAe.llComposant));
		}
		examenEtudiant.setExamenSessionLibelle(row.get(QSessionPeriode.sessionPeriode.libelle));
		QExamenOrganisationId idExamenOrganisation = QExamenOrganisationId.newId(row.get(QExamenEtudiants.examenEtudiants.idExamenOrganisation));
		examenEtudiant.setExamenOrganisationIdOrigine(idExamenOrganisation);
		examenEtudiant.setExamenOrganisationId(idExamenOrganisation);
		examenEtudiant.setExamenOrganisationSalle(row.get(QExamenOrganisation.examenOrganisation.salleExamen));
		examenEtudiant.setExamenOrganisationDate(row.get(QExamenOrganisation.examenOrganisation.dateExamen));
		
		return examenEtudiant;
	}

}
