package org.cocktail.scol.examen;

import java.sql.Timestamp;
import java.util.List;

import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.springframework.data.jdbc.query.SqlUpdateCallback;

import com.mysema.query.sql.dml.SQLUpdateClause;

/**
 * Callback pour mettre à jour par lot des changement d'organisation (de salle) d'étudiant dans la table {@link QExamenEtudiants}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadCallBackUpdateOrganisation implements SqlUpdateCallback {
	private QExamenEtudiants tExamenEtudiants = QExamenEtudiants.examenEtudiants;
	
	private List<ExamenEtudiantRead> listeDesUpdate;
	private Long persId;
	private Timestamp now;
	
	public ExamenEtudiantReadCallBackUpdateOrganisation(List<ExamenEtudiantRead> listeDesUpdate, Long persId, Timestamp now) {
		this.listeDesUpdate = listeDesUpdate;
		this.persId = persId;
		this.now = now;
	}
	
	public long doInSqlUpdateClause(SQLUpdateClause update) {
		for (ExamenEtudiantRead etudiant : listeDesUpdate) {
			update
				.set(tExamenEtudiants.idExamenOrganisation, etudiant.getExamenOrganisationId().getId())
				.set(tExamenEtudiants.persIdModification, persId)
				.set(tExamenEtudiants.dModification, now)
				.where(tExamenEtudiants.idExamenEtudiants.eq(etudiant.getExamenEtudiantId().getId()))
				.addBatch();
		}
		
		return update.execute();
	}
}
