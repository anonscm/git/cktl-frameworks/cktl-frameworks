package org.cocktail.scol.examen;

import java.util.Date;

import org.cocktail.ref.support.q.grhum.id.QEtudiantId;
import org.cocktail.scol.support.q.sco_scolarite.id.QEtudiantAnneeId;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenEtudiantsId;
import org.cocktail.scol.support.q.sco_scolarite.id.QExamenOrganisationId;

public class ExamenEtudiantRead {
	public static final String ETUD_NUMERO = "etudNumeroString";
	public static final String ETUD_NUM_INE = "etudNumIne";
	public static final String ETUD_NOM = "etudNom";
	public static final String ETUD_PRENOM = "etudPrenom";
	public static final String DIPLOME_LIBELLE = "diplomeLibelle";

	private QExamenEtudiantsId examenEtudiantId;
	private QEtudiantAnneeId etudAnneeId;
	private QEtudiantId etudNumero;
	private String etudNumIne;
	private String etudNom;
	private String etudPrenom;
	private String etudAdresseMail;
	private String diplomeLibelle;
	private String examenLibelle;
	private String examenSessionLibelle;
	private QExamenOrganisationId examenOrganisationIdOrigine;
	private QExamenOrganisationId examenOrganisationId;
	private String examenOrganisationSalle;
	private Date examenOrganisationDate;
	
	public QExamenEtudiantsId getExamenEtudiantId() {
		return examenEtudiantId;
	}
	public void setExamenEtudiantId(QExamenEtudiantsId examenEtudiantId) {
		this.examenEtudiantId = examenEtudiantId;
	}
	public QEtudiantAnneeId getEtudAnneeId() {
		return etudAnneeId;
	}
	public void setEtudAnneeId(QEtudiantAnneeId etudAnneeId) {
		this.etudAnneeId = etudAnneeId;
	}
	public QEtudiantId getEtudNumero() {
		return etudNumero;
	}
	public void setEtudNumero(QEtudiantId etudNumero) {
		this.etudNumero = etudNumero;
	}
	public String getEtudNumeroString() {
		return etudNumero.getId().toString();
	}
	public String getEtudNumIne() {
		return etudNumIne;
	}
	public void setEtudNumIne(String etudNumIne) {
		this.etudNumIne = etudNumIne;
	}
	public String getEtudNom() {
		return etudNom;
	}
	public void setEtudNom(String etudNom) {
		this.etudNom = etudNom;
	}
	public String getEtudPrenom() {
		return etudPrenom;
	}
	public void setEtudPrenom(String etudPrenom) {
		this.etudPrenom = etudPrenom;
	}
	public String getEtudAdresseMail() {
		return etudAdresseMail;
	}
	public void setEtudAdresseMail(String etudAdresseMail) {
		this.etudAdresseMail = etudAdresseMail;
	}
	public String getDiplomeLibelle() {
		return diplomeLibelle;
	}
	public void setDiplomeLibelle(String diplomeLibelle) {
		this.diplomeLibelle = diplomeLibelle;
	}
	public String getExamenLibelle() {
		return examenLibelle;
	}
	public void setExamenLibelle(String examenLibelle) {
		this.examenLibelle = examenLibelle;
	}
	public String getExamenSessionLibelle() {
		return examenSessionLibelle;
	}
	public void setExamenSessionLibelle(String examenSessionLibelle) {
		this.examenSessionLibelle = examenSessionLibelle;
	}
	public QExamenOrganisationId getExamenOrganisationIdOrigine() {
		return examenOrganisationIdOrigine;
	}
	public void setExamenOrganisationIdOrigine(QExamenOrganisationId examenOrganisationIdOrigine) {
		this.examenOrganisationIdOrigine = examenOrganisationIdOrigine;
	}
	public QExamenOrganisationId getExamenOrganisationId() {
		return examenOrganisationId;
	}
	public void setExamenOrganisationId(QExamenOrganisationId examenOrganisationId) {
		this.examenOrganisationId = examenOrganisationId;
	}
	public String getExamenOrganisationSalle() {
		return examenOrganisationSalle;
	}
	public void setExamenOrganisationSalle(String examenOrganisationSalle) {
		this.examenOrganisationSalle = examenOrganisationSalle;
	}
	public Date getExamenOrganisationDate() {
		return examenOrganisationDate;
	}
	public void setExamenOrganisationDate(Date examenOrganisationDate) {
		this.examenOrganisationDate = examenOrganisationDate;
	}
}
