package org.cocktail.scol.examen;

import java.sql.Timestamp;
import java.util.List;

import org.cocktail.scol.support.QueryDslHelper;
import org.cocktail.scol.support.q.sco_scolarite.QExamenEtudiants;
import org.springframework.data.jdbc.query.SqlInsertCallback;

import com.mysema.query.sql.dml.SQLInsertClause;

/**
 * Callback pour insérer par lot des données dans la table {@link QExamenEtudiants}.
 * 
 * @author Pascal MACOUIN
 */
public class ExamenEtudiantReadCallBackInsert implements SqlInsertCallback {
	private QExamenEtudiants tExamenEtudiants = QExamenEtudiants.examenEtudiants;
	
	private List<ExamenEtudiantRead> listeDesInsert;
	private Long persId;
	private Timestamp now;

	public ExamenEtudiantReadCallBackInsert(List<ExamenEtudiantRead> listeDesInsert, Long persId, Timestamp now) {
		this.listeDesInsert = listeDesInsert;
		this.persId = persId;
		this.now = now;
	}

	public long doInSqlInsertClause(SQLInsertClause insert) {
		for (ExamenEtudiantRead etudiant : listeDesInsert) {
			insert
				.set(tExamenEtudiants.idExamenEtudiants, QueryDslHelper.getNextValSequence(tExamenEtudiants))
				.set(tExamenEtudiants.idExamenOrganisation, etudiant.getExamenOrganisationId().getId())
				.set(tExamenEtudiants.idEtudiantAnnee, etudiant.getEtudAnneeId().getId())
				.set(tExamenEtudiants.convoque, 0)
				.set(tExamenEtudiants.persIdCreation, persId)
				.set(tExamenEtudiants.persIdModification, persId)
				.set(tExamenEtudiants.dCreation, now)
				.set(tExamenEtudiants.dModification, now)
				.addBatch();
		}
		
		return insert.execute();
	}
}