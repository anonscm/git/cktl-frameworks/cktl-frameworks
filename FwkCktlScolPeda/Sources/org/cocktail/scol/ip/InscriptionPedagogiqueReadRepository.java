package org.cocktail.scol.ip;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogique;
import org.cocktail.ref.support.q.grhum.QEtudiant;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.workflow.QDemande;
import org.cocktail.ref.support.q.workflow.QEtape;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QGradeUniversitaire;
import org.cocktail.scol.support.q.sco_scolarite.QInscription;
import org.cocktail.scol.support.q.sco_scolarite.QInscriptionPedagogique;
import org.cocktail.scol.support.q.sco_scolarite.QLien;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeInscriptionFormation;
import org.cocktail.scol.support.q.sco_scolarite.QTypePeriode;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;

/**
 * Cette classe représente l'implémentation QueryDsl de lecture des {@link IInscriptionPedagogique}.
 * 
 * @author Pascal MACOUIN
 */
public class InscriptionPedagogiqueReadRepository {

	private QueryDslJdbcTemplate template;
	private static final QInscriptionPedagogique inscriptionPedagogique = QInscriptionPedagogique.inscriptionPedagogique;
	private static final QInscription inscription = QInscription.inscription;
	private static final QTypeInscriptionFormation typeInscriptionFormation = QTypeInscriptionFormation.typeInscriptionFormation;
	private static final QComposant composantDiplome = new QComposant("COMPOSANT_DIPLOME");
	private static final QGradeUniversitaire gradeUniversitaire = QGradeUniversitaire.gradeUniversitaire;
	private static final QComposant composantParcoursDiplome = new QComposant("COMPOSANT_PARCOURS_DIPLOME");
	private static final QComposant composantParcoursAnnee = new QComposant("COMPOSANT_PARCOURS_ANNEE");
	private static final QVLien vlien = QVLien.vLien;
	private static final QLien lien = QLien.lien;
	private static final QComposant composantEnfantLienContexte = new QComposant("COMPOSANT_LIEN_CONTEXTE");
	private static final QTypeComposant typeComposant = QTypeComposant.typeComposant;
	private static final QTypePeriode typePeriode = QTypePeriode.typePeriode;
	private static final QDemande demande = QDemande.demande;
	private static final QEtape etape = QEtape.etape;
	private static final QEtudiant tEtudiant = QEtudiant.etudiant;
	private static final QIndividuUlr tIndividu = QIndividuUlr.individuUlr;

	/**
	 * Constructeur.
	 * @param template Un template
	 */
	@Autowired
	public InscriptionPedagogiqueReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire.
	 * @param anneeExercice Une année universitaire
	 * @param principale pour tester si l'ip est principale ou pas
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice, boolean principale) {
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(critereAnneeInscription(anneeExercice).and(critereIpPrincipale(principale)));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire.
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice) {
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(critereAnneeInscription(anneeExercice));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques selon les criteres
	 * @param criteres le prédicat contenant les criteres
 	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesAvecDonneesEtudiant(Predicate criteres) {
		SQLQuery query = getQueryDeBase();
		List<Expression<?>> listePaths = getPathsDeBase();
		avecDonneesEtudiant(query, listePaths);
		
		// On ajoute les filtres du worlflow
		query.where(criteres);
		query.orderBy(getTriListeAvecDonneesEtudiant());
		
		return executerQuery(query, listePaths);

	}
	
	/**
	 * Rechercher les inscriptions pédagogiques selon les criteres
	 * @param criteres le prédicat contenant les criteres
 	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Predicate criteres) {
		SQLQuery query = getQueryDeBase();
		List<Expression<?>> listePaths = getPathsDeBase();
		
		// On ajoute les filtres du worlflow
		query.where(criteres);
		query.orderBy(getTriListeSansDonneesEtudiant());
		
		return executerQuery(query, listePaths);

	}
	
	/**
	 * Rechercher les inscriptions pédagogiques en attente pour une année universitaire.
	 * En attente => sans workflow ou pas sur l'étape IP_VALIDEE.
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesEnAttente(Integer anneeExercice) {
		Predicate criteres = critereAnneeInscription(anneeExercice).and(critereIpEnAttente());
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereAnneeInscription(Integer anneeExercice) {
		return inscription.annee.eq(anneeExercice);
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques en attente pour une année universitaire.
	 * En attente => sans workflow ou pas sur l'étape IP_VALIDEE.
	 * @param anneeExercice Une année universitaire
	 * @param principale pour tester si l'ip est principale ou pas
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesEnAttente(Integer anneeExercice, boolean principale) {
		Predicate criteres = 
			critereAnneeInscription(anneeExercice)
				.and(critereIpEnAttente())
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereIpPrincipale(boolean principale) {
		return inscriptionPedagogique.principale.eq(principale ? 1 : 0);
	}

	private BooleanExpression critereIpEnAttente() {
		return demande.idDemande.isNull()
			.or(etape.cEtape.eq(EtapeInscriptionPedagogique.VALID_ETUDIANT.getCodeEtape()));
	}

	/**
	 * Rechercher les inscriptions pédagogiques validées par les étudiants pour une année universitaire.
	 * Validées par les étudiants => Sur l'étape IP_VALIDEE.
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValideesParEtudiant(Integer anneeExercice) {
		Predicate criteres = critereAnneeInscription(anneeExercice).and(critereIpValideeParEtudiant());
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées par les étudiants pour une année universitaire.
	 * Validées par les étudiants => Sur l'étape IP_VALIDEE.
	 * @param anneeExercice Une année universitaire
	 * @param principale pour tester si l'ip est principale ou pas
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValideesParEtudiant(Integer anneeExercice, boolean principale) {
		Predicate criteres = 
			critereAnneeInscription(anneeExercice)
				.and(critereIpValideeParEtudiant())
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereIpValideeParEtudiant() {
		return etape.cEtape.eq(EtapeInscriptionPedagogique.VALID_GESTIONNAIRE.getCodeEtape());
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées pour une année universitaire.
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(Integer anneeExercice) {
		Predicate criteres = critereAnneeInscription(anneeExercice).and(critereIpValidee());
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées pour une année universitaire.
	 * @param anneeExercice Une année universitaire
 	 * @param principale pour tester si l'ip est principale ou pas
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(Integer anneeExercice, boolean principale) {
		Predicate criteres = critereAnneeInscription(anneeExercice).and(critereIpValidee()).and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereIpValidee() {
		return etape.cEtape.eq(EtapeInscriptionPedagogique.VALIDEE.getCodeEtape());
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour un étudiant et une année universitaire.
	 * @param etudiant Un étudiant
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(IEtudiant etudiant, Integer anneeExercice) {
		return getInscriptionsPedagogiques(critereAnneeInscription(anneeExercice).and(critereIpEtudiant(etudiant)));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour un étudiant et une année universitaire.
	 * @param etudiant Un étudiant
	 * @param anneeExercice Une année universitaire
 	 * @param principale pour tester si l'ip est principale ou pas
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(IEtudiant etudiant, Integer anneeExercice, boolean principale) {
		return getInscriptionsPedagogiques(critereAnneeInscription(anneeExercice).and(critereIpEtudiant(etudiant)).and(critereIpPrincipale(principale)));
	}

	private BooleanExpression critereIpEtudiant(IEtudiant etudiant) {
		return inscription.idEtudiant.eq(etudiant.id());
	}

	/**
	 * Rechercher les inscriptions pédagogiques validées pour un étudiant et une année universitaire.
	 * @param etudiant Un étudiant
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(IEtudiant etudiant, Integer anneeExercice) {
		return getInscriptionsPedagogiques(critereAnneeInscription(anneeExercice).and(critereIpEtudiant(etudiant)).and(critereIpValidee()));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées pour un étudiant et une année universitaire.
	 * @param etudiant Un étudiant
	 * @param anneeExercice Une année universitaire
 	 * @param principale pour tester si l'ip est principale ou pas 
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(IEtudiant etudiant, Integer anneeExercice, boolean principale) {
		BooleanExpression criteres = 
			critereAnneeInscription(anneeExercice)
				.and(critereIpEtudiant(etudiant))
				.and(critereIpValidee())
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiques(criteres);
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées pour un étudiant et une année universitaire.
	 * @param idInscription l'id de l'IA
	 * @param anneeExercice Une année universitaire
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(Long idInscription, Integer anneeExercice) {
		return getInscriptionsPedagogiques(critereAnneeInscription(anneeExercice).and(critereIdInscription(idInscription)).and(critereIpValidee()));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques validées pour un étudiant et une année universitaire.
	 * @param idInscription l'id de l'IA
	 * @param anneeExercice Une année universitaire
 	 * @param principale pour tester si l'ip est principale ou pas  
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiquesValidees(Long idInscription, Integer anneeExercice, boolean principale) {
		BooleanExpression criteres = 
			critereAnneeInscription(anneeExercice)
				.and(critereIdInscription(idInscription))
				.and(critereIpValidee())
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiques(criteres);
	}


	private BooleanExpression critereIdInscription(Long idInscription) {
		return inscription.idInscription.eq(idInscription);
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire et un diplôme.
	 * @param anneeExercice Une année universitaire
	 * @param diplome Un diplome
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice, IDiplome diplome) {
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(critereDiplomeInscription(diplome).and(critereAnneeInscription(anneeExercice)));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire et un diplôme.
	 * @param anneeExercice Une année universitaire
	 * @param diplome Un diplome
 	 * @param principale pour tester si l'ip est principale ou pas  
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice, IDiplome diplome, boolean principale) {
		BooleanExpression criteres = 
			critereDiplomeInscription(diplome)
				.and(critereAnneeInscription(anneeExercice))
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereDiplomeInscription(IDiplome diplome) {
		return inscription.idDiplome.eq(new Long(diplome.id()));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire et un diplôme.
	 * @param anneeExercice Une année universitaire
	 * @param parcoursDiplome Un parcours diplome
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice, IParcours parcoursDiplome) {
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(critereAnneeInscription(anneeExercice).and(critereInscriptionParcoursDiplome(parcoursDiplome)));
	}
	
	/**
	 * Rechercher les inscriptions pédagogiques pour une année universitaire et un diplôme.
	 * @param anneeExercice Une année universitaire
	 * @param parcoursDiplome Un parcours diplome
 	 * @param principale pour tester si l'ip est principale ou pas  
	 * @return La liste des inscriptions pédagogiques
	 */
	public List<InscriptionPedagogiqueRead> getInscriptionsPedagogiques(Integer anneeExercice, IParcours parcoursDiplome, boolean principale) {
		BooleanExpression criteres = 
			critereAnneeInscription(anneeExercice)
				.and(critereInscriptionParcoursDiplome(parcoursDiplome))
				.and(critereIpPrincipale(principale));
		return getInscriptionsPedagogiquesAvecDonneesEtudiant(criteres);
	}

	private BooleanExpression critereInscriptionParcoursDiplome(IParcours parcoursDiplome) {
		return inscription.idParcoursDiplome.eq(new Long(parcoursDiplome.id()));
	}
	
	private List<InscriptionPedagogiqueRead> executerQuery(SQLQuery query, List<Expression<?>> listePaths) {
		Expression<?>[] paths = listePaths.toArray(new Expression<?>[listePaths.size()]);
		InscriptionPedagogiqueReadMapping inscriptionPedagogiqueReadMapping = new InscriptionPedagogiqueReadMapping(composantDiplome, composantParcoursDiplome, composantParcoursAnnee, composantEnfantLienContexte, paths);

		List<InscriptionPedagogiqueRead> res = template.query(query, inscriptionPedagogiqueReadMapping);
		return res;
	}

	private SQLQuery getQueryDeBase() {
		SQLQuery query = template.newSqlQuery()
				.from(inscriptionPedagogique)
				.innerJoin(inscription).on(inscription.idInscription.eq(inscriptionPedagogique.idInscription))
				.innerJoin(composantDiplome).on(inscription.idDiplome.eq(composantDiplome.idComposant))
				.leftJoin(composantParcoursDiplome).on(inscription.idParcoursDiplome.eq(composantParcoursDiplome.idComposant))
				.leftJoin(composantParcoursAnnee).on(inscription.idParcoursAnnee.eq(composantParcoursAnnee.idComposant))
				.innerJoin(typeInscriptionFormation).on(inscription.idTypeInscriptionFormation.eq(typeInscriptionFormation.idTypeInscriptionFormation))
				.leftJoin(gradeUniversitaire).on(composantDiplome.dipGradeUniversitaireId.eq(gradeUniversitaire.idGradeUniversitaire))
				.leftJoin(vlien).on(inscriptionPedagogique.idLienContexte.eq(vlien.idLienContexte))
				.leftJoin(lien).on(vlien.idLien.eq(lien.idLien))
				.leftJoin(composantEnfantLienContexte).on(lien.childId.eq(composantEnfantLienContexte.idComposant))
				.leftJoin(typeComposant).on(composantEnfantLienContexte.typeComposantId.eq(typeComposant.idTypeComposant))
				.leftJoin(typePeriode).on(composantEnfantLienContexte.perIdType.eq(typePeriode.idTypePeriode))
				.leftJoin(demande).on(inscriptionPedagogique.idDemande.eq(demande.idDemande))
				.leftJoin(etape).on(demande.idEtape.eq(etape.idEtape))
				.distinct();
		return query;
	}
	
	private List<Expression<?>> getPathsDeBase() {
		List<Expression<?>> listePaths = new ArrayList<Expression<?>>();
		listePaths.add(inscriptionPedagogique.idInscriptionPeda);
		listePaths.add(inscription.niveau);
		listePaths.add(composantDiplome.code);
		listePaths.add(composantDiplome.llComposant);
		listePaths.add(gradeUniversitaire.libelle);
		listePaths.add(composantParcoursDiplome.llComposant);
		listePaths.add(composantParcoursAnnee.llComposant);
		listePaths.add(composantEnfantLienContexte.perOrdre);
		listePaths.add(composantEnfantLienContexte.llComposant);
		listePaths.add(typeComposant.nom);
		listePaths.add(typeComposant.libelle);
		listePaths.add(typePeriode.libelle);
		listePaths.add(typeInscriptionFormation.libelle);
		listePaths.add(etape.cEtape);
		listePaths.add(inscriptionPedagogique.principale);
		return listePaths;
	}

	private void avecDonneesEtudiant(SQLQuery query, List<Expression<?>> listePaths) {
		query.innerJoin(tEtudiant).on(inscription.idEtudiant.eq(tEtudiant.etudNumero));
		query.innerJoin(tIndividu).on(tEtudiant.noIndividu.eq(tIndividu.noIndividu));
		listePaths.add(tEtudiant.etudCodeIne);
		listePaths.add(tEtudiant.etudNumero);
		listePaths.add(tIndividu.nomAffichage);
		listePaths.add(tIndividu.prenomAffichage);
		listePaths.add(tIndividu.dNaissance);
	}
	
	private OrderSpecifier<?>[] getTriListeAvecDonneesEtudiant() {
		return new OrderSpecifier<?>[] {
				tIndividu.nomAffichage.asc(),
				tIndividu.prenomAffichage.asc(),
				tIndividu.dNaissance.asc(),
				composantDiplome.llComposant.asc(),
				typePeriode.libelle.asc(),
				composantEnfantLienContexte.perOrdre.asc()
		};
	}
	
	private OrderSpecifier<?>[] getTriListeSansDonneesEtudiant() {
		return new OrderSpecifier<?>[] {
				composantDiplome.llComposant.asc(),
				typePeriode.libelle.asc(),
				composantEnfantLienContexte.perOrdre.asc()
		};
	}
	
}
