package org.cocktail.scol.ip;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogiqueUtils;
import org.cocktail.ref.support.q.grhum.QEtudiant;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.workflow.QEtape;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QGradeUniversitaire;
import org.cocktail.scol.support.q.sco_scolarite.QInscription;
import org.cocktail.scol.support.q.sco_scolarite.QInscriptionPedagogique;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeInscriptionFormation;
import org.cocktail.scol.support.q.sco_scolarite.QTypePeriode;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

/**
 * Classe de mapping entre un tuple QueryDSL et le bean {@link InscriptionPedagogiqueRead}.
 * 
 * @author Pascal MACOUIN
 */
public class InscriptionPedagogiqueReadMapping extends MappingProjection<InscriptionPedagogiqueRead> {

	private QComposant composantDiplome;
	private QComposant composantParcoursDiplome;
	private QComposant composantParcoursAnnee;
	private QComposant composantEnfantLienContexte;
	
	public InscriptionPedagogiqueReadMapping(QComposant composantDiplome, QComposant composantParcoursDiplome, QComposant composantParcoursAnnee, QComposant composantEnfantLienContexte, Expression<?>[] args) {
		super(InscriptionPedagogiqueRead.class, args);
		this.composantDiplome = composantDiplome;
		this.composantParcoursDiplome = composantParcoursDiplome;
		this.composantParcoursAnnee = composantParcoursAnnee;
		this.composantEnfantLienContexte = composantEnfantLienContexte;
	}

	@Override
	protected InscriptionPedagogiqueRead map(Tuple row) {
		InscriptionPedagogiqueRead inscriptionPedagogiqueRead = new InscriptionPedagogiqueRead();

		inscriptionPedagogiqueRead.setInscriptionPedagogiqueId(row.get(QInscriptionPedagogique.inscriptionPedagogique.idInscriptionPeda));
		inscriptionPedagogiqueRead.setEtudiantCodeIne(row.get(QEtudiant.etudiant.etudCodeIne));
		inscriptionPedagogiqueRead.setEtudiantNumero(row.get(QEtudiant.etudiant.etudNumero));
		inscriptionPedagogiqueRead.setEtudiantNom(row.get(QIndividuUlr.individuUlr.nomAffichage));
		inscriptionPedagogiqueRead.setEtudiantPrenom(row.get(QIndividuUlr.individuUlr.prenomAffichage));
		inscriptionPedagogiqueRead.setEtudiantDateNaissance(row.get(QIndividuUlr.individuUlr.dNaissance));
		inscriptionPedagogiqueRead.setDiplomeCode(row.get(composantDiplome.code));
		inscriptionPedagogiqueRead.setDiplomeLibelle(row.get(composantDiplome.llComposant));
		inscriptionPedagogiqueRead.setParcoursDiplomeLibelle(row.get(composantParcoursDiplome.llComposant));
		inscriptionPedagogiqueRead.setGradeUniversitaireLibelle(row.get(QGradeUniversitaire.gradeUniversitaire.libelle));
		inscriptionPedagogiqueRead.setNiveau(row.get(QInscription.inscription.niveau));
		inscriptionPedagogiqueRead.setParcoursAnneeLibelle(row.get(composantParcoursAnnee.llComposant));
		
		if (ITypeComposant.TYPEPERIODE_NOM.equals(row.get(QTypeComposant.typeComposant.nom))) {
			inscriptionPedagogiqueRead.setPeriodeTypeLibelle(row.get(QTypePeriode.typePeriode.libelle));
			inscriptionPedagogiqueRead.setPeriodeNiveau(row.get(composantEnfantLienContexte.perOrdre));
			inscriptionPedagogiqueRead.setPeriodeLibelle(row.get(composantEnfantLienContexte.llComposant));
		}
		
		inscriptionPedagogiqueRead.setInscriptionTypeLibelle(row.get(QTypeInscriptionFormation.typeInscriptionFormation.libelle));
		String etapeCode = row.get(QEtape.etape.cEtape);
		inscriptionPedagogiqueRead.setEtapeCode(etapeCode);
		inscriptionPedagogiqueRead.setEtapeLibelleUtilisateur(EtapeInscriptionPedagogiqueUtils.getLibelleEtape(etapeCode));
		Integer principale = row.get(QInscriptionPedagogique.inscriptionPedagogique.principale);
		if (principale != null && principale.equals(1)) {
			inscriptionPedagogiqueRead.setPrincipale(true);
		} else {
			inscriptionPedagogiqueRead.setPrincipale(false);

		}
		
		return inscriptionPedagogiqueRead;
	}
}
