package org.cocktail.scol.ip;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Bean pour les listes "Inscriptions pédagogiques" de Paprika.
 * 
 * @author Pascal MACOUIN
 */
public class InscriptionPedagogiqueRead {
	public static final String INSCRIPTION_PEDAGOGIQUE_ID = "inscriptionPedagogiqueId";
	public static final String ETUDIANT_CODE_INE = "etudiantCodeIne";
	public static final String ETUDIANT_NUMERO = "etudiantNumero";
	public static final String ETUDIANT_NOM = "etudiantNom";
	public static final String ETUDIANT_PRENOM = "etudiantPrenom";
	public static final String ETUDIANT_DATE_NAISSANCE = "etudiantDateNaissance";
	public static final String DIPLOME_CODE = "diplomeCode";
	public static final String DIPLOME_LIBELLE = "diplomeLibelle";
	public static final String PARCOURS_DIPLOME_LIBELLE = "parcoursDiplomeLibelle";
	public static final String GRADE_UNIVERSITAIRE_LIBELLE = "gradeUniversitaireLibelle";
	public static final String NIVEAU = "niveau";
	public static final String PARCOURS_ANNEE_LIBELLE = "parcoursAnneeLibelle";
	public static final String PERIODE_TYPE_LIBELLE = "periodeTypeLibelle";
	public static final String PERIODE_NIVEAU = "periodeNiveau";
	public static final String PERIODE_LIBELLE = "periodeLibelle";
	public static final String INSCRIPTION_TYPE_LIBELLE = "inscriptionTypeLibelle";
	public static final String ETAPE_CODE = "etapeCode";
	public static final String ETAPE_LIBELLE_UTILISATEUR = "etapeLibelleUtilisateur";

	private Long inscriptionPedagogiqueId;
	private String etudiantCodeIne;
	private Long etudiantNumero;
	private String etudiantNom;
	private String etudiantPrenom;
	private Date etudiantDateNaissance;
	private String diplomeCode;
	private String diplomeLibelle;
	private String parcoursDiplomeLibelle;
	private String gradeUniversitaireLibelle;
	private Long niveau;
	private String parcoursAnneeLibelle;
	private String periodeTypeLibelle;
	private Long periodeNiveau;
	private String periodeLibelle;
	private String inscriptionTypeLibelle;
	private String etapeCode;
	private String etapeLibelleUtilisateur;
	private Boolean principale;
	
	public Long getInscriptionPedagogiqueId() {
		return inscriptionPedagogiqueId;
	}
	
	public void setInscriptionPedagogiqueId(Long inscriptionPedagogiqueId) {
		this.inscriptionPedagogiqueId = inscriptionPedagogiqueId;
	}
	
	public String getEtudiantCodeIne() {
		return etudiantCodeIne;
	}

	public void setEtudiantCodeIne(String etudiantCodeIne) {
		this.etudiantCodeIne = etudiantCodeIne;
	}

	public Long getEtudiantNumero() {
		return etudiantNumero;
	}

	public void setEtudiantNumero(Long etudiantNumero) {
		this.etudiantNumero = etudiantNumero;
	}

	public String getEtudiantNom() {
		return etudiantNom;
	}
	
	public void setEtudiantNom(String etudiantNom) {
		this.etudiantNom = etudiantNom;
	}
	
	public String getEtudiantPrenom() {
		return etudiantPrenom;
	}
	
	public void setEtudiantPrenom(String etudiantPrenom) {
		this.etudiantPrenom = etudiantPrenom;
	}
	
	public Date getEtudiantDateNaissance() {
		return this.etudiantDateNaissance;
	}
	
	public void setEtudiantDateNaissance(Date etudiantDateNaissance) {
		this.etudiantDateNaissance = etudiantDateNaissance;
	}
	
	public String getEtudiantDateNaissanceLibelle() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(getEtudiantDateNaissance());
	}

	public String getDiplomeCode() {
		return diplomeCode;
	}

	public void setDiplomeCode(String diplomeCode) {
		this.diplomeCode = diplomeCode;
	}

	public String getDiplomeLibelle() {
		return diplomeLibelle;
	}
	
	public void setDiplomeLibelle(String diplomeLibelle) {
		this.diplomeLibelle = diplomeLibelle;
	}
	
	public String getParcoursDiplomeLibelle() {
		return parcoursDiplomeLibelle;
	}
	
	public void setParcoursDiplomeLibelle(String parcoursDiplomeLibelle) {
		this.parcoursDiplomeLibelle = parcoursDiplomeLibelle;
	}
	
	public String getGradeUniversitaireLibelle() {
		return gradeUniversitaireLibelle;
	}
	
	public void setGradeUniversitaireLibelle(String gradeUniversitaireLibelle) {
		this.gradeUniversitaireLibelle = gradeUniversitaireLibelle;
	}
	
	public Long getNiveau() {
		return niveau;
	}
	
	public void setNiveau(Long niveau) {
		this.niveau = niveau;
	}
	
	public String getParcoursAnneeLibelle() {
		return parcoursAnneeLibelle;
	}
	
	public void setParcoursAnneeLibelle(String parcoursNiveauLibelle) {
		this.parcoursAnneeLibelle = parcoursNiveauLibelle;
	}
	
	public String getPeriodeTypeLibelle() {
		return periodeTypeLibelle;
	}
	
	public void setPeriodeTypeLibelle(String periodeTypeLibelle) {
		this.periodeTypeLibelle = periodeTypeLibelle;
	}
	
	public Long getPeriodeNiveau() {
		return periodeNiveau;
	}
	
	public void setPeriodeNiveau(Long periodeNiveau) {
		this.periodeNiveau = periodeNiveau;
	}
	
	public String getPeriodeLibelle() {
		return periodeLibelle;
	}
	
	public void setPeriodeLibelle(String periodeLibelle) {
		this.periodeLibelle = periodeLibelle;
	}
	
	public String getInscriptionTypeLibelle() {
		return inscriptionTypeLibelle;
	}
	
	public void setInscriptionTypeLibelle(String inscriptionTypeLibelle) {
		this.inscriptionTypeLibelle = inscriptionTypeLibelle;
	}
	
	public String getEtapeCode() {
		return etapeCode;
	}
	
	public void setEtapeCode(String etapeCode) {
		this.etapeCode = etapeCode;
	}

	public String getEtapeLibelleUtilisateur() {
		return etapeLibelleUtilisateur;
	}

	public void setEtapeLibelleUtilisateur(String etapeLibelleUtilisateur) {
		this.etapeLibelleUtilisateur = etapeLibelleUtilisateur;
	}
	
	public String libelleInscriptionPedagogique() {
		String libelle = this.diplomeCode + " - " + this.diplomeLibelle;
		if (periodeLibelle != null) {
			libelle = libelle + " - " + periodeLibelle;
		}
		return libelle;
	}

	public Boolean getPrincipale() {
		return principale;
	}

	public void setPrincipale(Boolean principale) {
		this.principale = principale;
	}


	
}
