package org.cocktail.scol.ia;

import java.util.Date;

public class InscriptionRead {

	private String libelleDiplome;
	private String codeTypeInscription;
	private Date dateCreation;
	
	public InscriptionRead(String libelleDiplome, String codeTypeInscription,
			Date dateCreation) {
		super();
		this.libelleDiplome = libelleDiplome;
		this.codeTypeInscription = codeTypeInscription;
		this.dateCreation = dateCreation;
	}

	public String getLibelleDiplome() {
		return libelleDiplome;
	}
	
	public String getCodeTypeInscription() {
		return codeTypeInscription;
	}
	
	public Date getDateCreation() {
		return dateCreation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codeTypeInscription == null) ? 0 : codeTypeInscription
						.hashCode());
		result = prime * result
				+ ((dateCreation == null) ? 0 : dateCreation.hashCode());
		result = prime * result
				+ ((libelleDiplome == null) ? 0 : libelleDiplome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InscriptionRead other = (InscriptionRead) obj;
		if (codeTypeInscription == null) {
			if (other.codeTypeInscription != null)
				return false;
		} else if (!codeTypeInscription.equals(other.codeTypeInscription))
			return false;
		if (dateCreation == null) {
			if (other.dateCreation != null)
				return false;
		} else if (!dateCreation.equals(other.dateCreation))
			return false;
		if (libelleDiplome == null) {
			if (other.libelleDiplome != null)
				return false;
		} else if (!libelleDiplome.equals(other.libelleDiplome))
			return false;
		return true;
	}
	
}
