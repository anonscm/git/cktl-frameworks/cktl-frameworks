package org.cocktail.scol.ia;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.workflow.EtapeInscriptionPedagogique;
import org.cocktail.ref.support.q.grhum.QEtudiant;
import org.cocktail.ref.support.q.grhum.QIndividuUlr;
import org.cocktail.ref.support.q.grhum.QSituationFamiliale;
import org.cocktail.ref.support.q.workflow.QDemande;
import org.cocktail.ref.support.q.workflow.QEtape;
import org.cocktail.scol.support.QueryDslHelper;
import org.cocktail.scol.support.q.sco_scolarite.QBourses;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QEtudiantAnnee;
import org.cocktail.scol.support.q.sco_scolarite.QInscription;
import org.cocktail.scol.support.q.sco_scolarite.QInscriptionPedagogique;
import org.cocktail.scol.support.q.sco_scolarite.QPaiement;
import org.cocktail.scol.support.q.sco_scolarite.QSituationProfessionnelle;
import org.cocktail.scol.support.q.sco_scolarite.QTypeInscriptionFormation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.SimpleExpression;
import com.mysema.query.types.path.NumberPath;

public class InscriptionAdministrativeTermineeReadService {

	private QueryDslJdbcTemplate template;
	private QEtudiantAnnee etudiantAnnee = QEtudiantAnnee.etudiantAnnee;
	private QEtudiant etudiant = QEtudiant.etudiant;
	private QIndividuUlr individu = QIndividuUlr.individuUlr;
	private QSituationFamiliale situationFamiliale = QSituationFamiliale.situationFamiliale;
	private QSituationProfessionnelle situationProfessionnelle = QSituationProfessionnelle.situationProfessionnelle;
	private QInscription inscription = QInscription.inscription;
	private QTypeInscriptionFormation typeInscription = QTypeInscriptionFormation.typeInscriptionFormation;
	private QComposant diplome = QComposant.composant;
	private QDemande demande = QDemande.demande;
	private QEtape etape = QEtape.etape;
	private QInscriptionPedagogique inscriptionPedagogique = QInscriptionPedagogique.inscriptionPedagogique;
	
	private QBourses bourses = QBourses.bourses;
	private QPaiement paiement = QPaiement.paiement;
	private NumberPath<Long> idEtudiantAnnee = etudiantAnnee.idEtudiantAnnee;
	
	private static final Ordering<InscriptionRead> orderByDateCreation = new Ordering<InscriptionRead>() {
		@Override
		public int compare(InscriptionRead left, InscriptionRead right) {
			return Ordering.natural().nullsLast().compare(left.getDateCreation(), right.getDateCreation());
		}
	};
	
	@Autowired
	public InscriptionAdministrativeTermineeReadService(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public List<InscriptionAdministrativeTermineeShortRead> getInscriptionsAdministrativesShortRead(Integer anneeExercice) {
		SQLQuery sql = template.newSqlQuery()
				.from(inscriptionPedagogique)
				.join(inscription).on(inscription.idInscription.eq(inscriptionPedagogique.idInscription))
				.join(etudiantAnnee).on(inscription.idEtudiantAnnee.eq(etudiantAnnee.idEtudiantAnnee))
				.join(etudiant).on(etudiantAnnee.idEtudiant.eq(etudiant.etudNumero))
				.join(individu).on(etudiant.noIndividu.eq(individu.noIndividu))
				.leftJoin(diplome).on(inscription.idDiplome.eq(diplome.idComposant))
				.leftJoin(typeInscription).on(inscription.idTypeInscriptionFormation.eq(typeInscription.idTypeInscriptionFormation))
				.leftJoin(demande).on(inscriptionPedagogique.idDemande.eq(demande.idDemande))
				.leftJoin(etape).on(demande.idEtape.eq(etape.idEtape))
				.where(etudiantAnnee.annee.eq(anneeExercice)
						.and(etape.cEtape.eq(EtapeInscriptionPedagogique.VALIDEE.getCodeEtape())))
				.distinct();
		ConstructorExpression<InscriptionAdministrativeTermineeShortRead> expression = 
				ConstructorExpression.create(InscriptionAdministrativeTermineeShortRead.class, 
						inscription.idInscription, 
						etudiant.etudCodeIne, 
						individu.nomAffichage, 
						individu.prenom,
						individu.dNaissance, 
						typeInscription.libelle,
						diplome.code,
						diplome.llComposant
						);
		return template.query(sql, expression);
	}
	
	public List<InscriptionAdministrativeTermineeRead> getInscriptionsAdministrativesRead(Integer anneeExercice) {
		SQLQuery sql = template.newSqlQuery()
				.from(etudiantAnnee)
				.join(etudiant).on(etudiantAnnee.idEtudiant.eq(etudiant.etudNumero))
				.join(individu).on(etudiant.noIndividu.eq(individu.noIndividu))
				.leftJoin(inscription).on(idEtudiantAnnee.eq(inscription.idEtudiantAnnee))
				.leftJoin(typeInscription).on(inscription.idTypeInscriptionFormation.eq(typeInscription.idTypeInscriptionFormation))
				.leftJoin(diplome).on(inscription.idDiplome.eq(diplome.idComposant))
				.leftJoin(situationFamiliale).on(etudiantAnnee.sitFamille.eq(situationFamiliale.cSituationFamille))
				.leftJoin(situationProfessionnelle).on(etudiantAnnee.idSituationProf.eq(situationProfessionnelle.idSituationProfessionnelle))
				.leftJoin(bourses).on(etudiant.etudNumero.eq(bourses.idEtudiant).and(bourses.annee.eq(etudiantAnnee.annee)))
				.leftJoin(paiement).on(idEtudiantAnnee.eq(paiement.idEtudiantAnnee).and(paiement.ordre.eq(Long.valueOf(1))))
				.where(etudiantAnnee.annee.eq(anneeExercice));
		
		QTuple selectExpr = new QTuple(
				etudiant.etudCodeIne, 
				etudiant.etudNumero, 
				individu.prenom, 
				individu.dNaissance, 
				individu.indNoInsee,
				individu.nomUsuel,
				individu.nomAffichage,
				situationFamiliale.lSituationFamille,
				situationProfessionnelle.libelle,
				diplome.llComposant,
				typeInscription.codeInscription,
				inscription.dCreation,
				idEtudiantAnnee,
				etudiantAnnee.statusBoursierCnous,
				etudiantAnnee.valide,
				bourses.idBourses,
				paiement.paiementValide
				);
		
		List<Tuple> tuples = template.query(sql, selectExpr);
		
		Map<Long, List<InscriptionRead>> inscriptionsCompByIdEtudiantAnnee = QueryDslHelper.flatten(tuples, idEtudiantAnnee, new Function<Tuple, InscriptionRead>() {

			public InscriptionRead apply(Tuple tuple) {
				String libelle = tuple.get(diplome.llComposant);
				String codeType = tuple.get(typeInscription.codeInscription);
				Date dateCreation = tuple.get(inscription.dCreation);
				return new InscriptionRead(libelle, codeType, dateCreation);
			}
			
		});
		Map<Long, List<Long>> boursesByIdEtudiantAnnee = QueryDslHelper.flatten(tuples, idEtudiantAnnee, bourses.idBourses);
		
		return tuplesToInscriptionsRead(tuples, inscriptionsCompByIdEtudiantAnnee, boursesByIdEtudiantAnnee);
	}

	private List<InscriptionAdministrativeTermineeRead> tuplesToInscriptionsRead(
			List<Tuple> tuples,
			Map<Long, List<InscriptionRead>> inscriptionsByIdEtudiantAnnee,
			Map<Long, List<Long>> boursesByIdEtudiantAnnee) {
		List<InscriptionAdministrativeTermineeRead> inscriptionsRead = new ArrayList<InscriptionAdministrativeTermineeRead>();
		HashSet<Long> idsTraites = new HashSet<Long>();
		for (Tuple tuple : tuples) {
			Long id = tuple.get(idEtudiantAnnee);
			if (!idsTraites.contains(id)) {
				InscriptionAdministrativeTermineeRead inscriptionRead = tupleToInscriptionRead(tuple);
				Set<InscriptionRead> inscriptions = Sets.newHashSet(inscriptionsByIdEtudiantAnnee.get(id));
				List<InscriptionRead> inscriptionsOrdonnees = orderByDateCreation.sortedCopy(inscriptions);
				inscriptionRead.setInscriptions(inscriptionsOrdonnees);
				List<Long> bourses = boursesByIdEtudiantAnnee.get(id);
				inscriptionRead.setBourses(bourses);
				inscriptionsRead.add(inscriptionRead);
				idsTraites.add(id);
			}
		}
		return inscriptionsRead;
	}

	private InscriptionAdministrativeTermineeRead tupleToInscriptionRead(Tuple tuple) {
		InscriptionAdministrativeTermineeRead ia = new InscriptionAdministrativeTermineeRead();
		ia.setId(tuple.get(etudiantAnnee.idEtudiantAnnee));
		ia.setCodeIne(tuple.get(etudiant.etudCodeIne));
		ia.setNoEtudiant(tuple.get(etudiant.etudNumero));
		ia.setNom(nom(tuple));
		ia.setPrenom(tuple.get(individu.prenom));
		ia.setDateNaissance(tuple.get(individu.dNaissance));
		ia.setCodeInsee(tuple.get(individu.indNoInsee));
		ia.setSituationFamiliale(tuple.get(situationFamiliale.lSituationFamille));
		ia.setSituationProfessionnelle(tuple.get(situationProfessionnelle.libelle));
		ia.setValide(valide(tuple, etudiantAnnee.valide));
		ia.setPaiementValide(valide(tuple, paiement.paiementValide));
		ia.setStatutBoursierCnous(tuple.get(etudiantAnnee.statusBoursierCnous));
		return ia;
	}

	private boolean valide(Tuple row, SimpleExpression<Integer> exp) {
		Integer intValide = row.get(exp);
		return intValide != null && intValide == 1 ? true : false;
	}


	private String nom(Tuple row) {
		String nomUsuel = row.get(individu.nomUsuel);
		String nomAffichage = row.get(individu.nomAffichage);
		return nomAffichage != null && !nomAffichage.isEmpty() ? nomAffichage : nomUsuel;
	}
	
}
