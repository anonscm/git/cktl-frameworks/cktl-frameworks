package org.cocktail.scol.ia;

import java.util.Date;

public class InscriptionAdministrativeTermineeShortRead {

	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";
	public static final String CODE_INE = "codeIne";
	public static final String TYPE_INSCRIPTION = "typeInscription";
	public static final String CODE_DIPLOME = "codeDiplome";
	public static final String LIBELLE_DIPLOME = "libelleDiplome";
	
	private Long id;
	private String codeIne;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String typeInscription;
	private String codeDiplome;
	private String libelleDiplome;
	
	public InscriptionAdministrativeTermineeShortRead(
			Long id, 
			String codeIne,
			String nom, 
			String prenom, 
			Date dateNaissance,
			String typeInscription, 
			String codeDiplome, 
			String libelleDiplome) {
		super();
		this.id = id;
		this.codeIne = codeIne;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.typeInscription = typeInscription;
		this.codeDiplome = codeDiplome;
		this.libelleDiplome = libelleDiplome;
	}
	
	public Long getId() {
		return id;
	}
	public String getCodeIne() {
		return codeIne;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public String getTypeInscription() {
		return typeInscription;
	}
	public String getCodeDiplome() {
		return codeDiplome;
	}
	public String getLibelleDiplome() {
		return libelleDiplome;
	}
	
}
