package org.cocktail.scol.ia;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionFormation;

public class InscriptionAdministrativeTermineeRead {

	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";
	public static final String CODE_INE = "codeIne";
	public static final String NO_ETUDIANT = "noEtudiant";
	public static final String CODE_INSEE = "codeInsee";
	public static final String SITUATION_FAMILIALE = "situationFamiliale";
	public static final String SITUATION_PROFESSIONNELLE = "situationProfessionnelle";
	public static final String BOURSIER = "boursier";
	public static final String PREMIERE_INSCRIPTION = "premiereInscription";
	public static final String DEUXIEME_INSCRIPTION = "deuxiemeInscription";
	public static final String TROISIEME_INSCRIPTION = "troisimeInscription";
	
	private Long id;
	private String codeIne;
	private Long noEtudiant;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String codeInsee;
	private String situationFamiliale;
	private String situationProfessionnelle;
	private List<InscriptionRead> inscriptions;
	private List<Long> bourses;
	private String statutBoursierCnous;
	private boolean valide;
	private boolean paiementValide;
	private String etat;
	
	public Long getId() {
		return id;
	}
	
	public InscriptionRead getPremiereInscription() {
		InscriptionRead inscriptionPrincipale = findInscription(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		InscriptionRead inscriptionEchangeInternational = findInscription(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL);
		return inscriptionPrincipale != null ? inscriptionPrincipale : inscriptionEchangeInternational;
	}
	
	private InscriptionRead findInscription(String codeTypeInscription) {
		for (InscriptionRead insc : inscriptions) {
			if (codeTypeInscription.equals(insc.getCodeTypeInscription())) {
				return insc;
			}
		}
		return null;
	}

	public InscriptionRead getDeuxiemeInscription() {
		InscriptionRead deuxiemeInscription = null;
		List<InscriptionRead> inscripitonsComplementaires = inscripitonsComplementaires();
		if (inscripitonsComplementaires.size() > 0) {
			deuxiemeInscription = inscripitonsComplementaires.get(0);
		}
		return deuxiemeInscription;
	}
	
	public InscriptionRead getTroisiemeInscription() {
		InscriptionRead troisiemeInscription = null;
		List<InscriptionRead> inscripitonsComplementaires = inscripitonsComplementaires();
		if (inscripitonsComplementaires.size() > 1) {
			troisiemeInscription = inscripitonsComplementaires.get(1);
		}
		return troisiemeInscription;
		}

	private List<InscriptionRead> inscripitonsComplementaires() {
		List<InscriptionRead> copyInscriptions = new ArrayList<InscriptionRead>();
		if (inscriptions != null) {
			copyInscriptions.addAll(inscriptions);
			InscriptionRead premiereInscription = getPremiereInscription();
			if (premiereInscription != null) {
				copyInscriptions.remove(premiereInscription);
			}
		}
		return copyInscriptions;
	}
	
	public boolean boursier() {
		return (isStatutBourseCNOUSEchelonValide() || (bourses != null && bourses.size() > 0));
	}
	
	private boolean isStatutBourseCNOUSEchelonValide() {
		if (statutBoursierCnous != null) {
			return StringUtils.isNumeric(statutBoursierCnous);
		}
		return false;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodeIne() {
		return codeIne;
	}
	public void setCodeIne(String codeIne) {
		this.codeIne = codeIne;
	}
	public Long getNoEtudiant() {
		return noEtudiant;
	}
	public void setNoEtudiant(Long noEtudiant) {
		this.noEtudiant = noEtudiant;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getCodeInsee() {
		return codeInsee;
	}
	public void setCodeInsee(String codeInsee) {
		this.codeInsee = codeInsee;
	}
	public String getSituationFamiliale() {
		return situationFamiliale;
	}
	public void setSituationFamiliale(String situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}
	public String getSituationProfessionnelle() {
		return situationProfessionnelle;
	}
	public void setSituationProfessionnelle(String situationProfession) {
		this.situationProfessionnelle = situationProfession;
	}
	public void setStatutBoursierCnous(String statutBoursierCnous) {
		this.statutBoursierCnous = statutBoursierCnous;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public boolean isValide() {
		return valide;
	}
	public void setValide(boolean valide) {
		this.valide = valide;
	}
	public boolean isPaiementValide() {
		return paiementValide;
	}
	public void setPaiementValide(boolean paiementValide) {
		this.paiementValide = paiementValide;
	}
	public void setInscriptions(
			List<InscriptionRead> inscriptions) {
		this.inscriptions = inscriptions;
	}
	public void setBourses(List<Long> bourses) {
		this.bourses = bourses;
	}
	
}
