package org.cocktail.scol.jury;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QJury;
import org.cocktail.scol.support.q.sco_scolarite.QJuryEtat;
import org.cocktail.scol.support.q.sco_scolarite.QLien;
import org.cocktail.scol.support.q.sco_scolarite.QSessionPeriode;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.QTuple;

public class JuryReadService {
	
	private QueryDslJdbcTemplate template;
	private QJury jury = QJury.jury;
	private QLien lienFormation = new QLien("l1");
	private QLien lienParcoursFormation = new QLien("l2");
	private QLien lienNiveauAnnee = new QLien("l3");
	private QLien lienParcoursAnnee = new QLien("l4");
	private QLien lienNiveauSemestre = new QLien("l5");
	private QComposant composantParentLienFormation = new QComposant("c1");
	private QComposant composantChildLienParcoursFormation = new QComposant("c2");
	private QComposant composantChildLienNiveauAnnee = new QComposant("c3");
	private QComposant composantChildLienParcoursAnnee = new QComposant("c4");
	private QComposant composantChildLienNiveauSemestre = new QComposant("c5");
	private QSessionPeriode sessionPeriode = QSessionPeriode.sessionPeriode;
	private QJuryEtat juryEtat = QJuryEtat.juryEtat;

	public JuryReadService(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public List<JuryRead> getJury(Integer anneeExercice, Integer idJury) {
		QTuple selectExpr = new QTuple(
				jury.idJury,
				composantParentLienFormation.llComposant,
				composantChildLienParcoursFormation.llComposant,
				composantChildLienNiveauAnnee.llComposant,
				composantChildLienParcoursAnnee.llComposant,
				composantChildLienNiveauSemestre.llComposant,
				sessionPeriode.libelle,
				sessionPeriode.dateOuverture,
				sessionPeriode.dateFermeture,
				juryEtat.code
				);
		
		SQLQuery sqlQuery = getQueryDeBase(anneeExercice);
		sqlQuery.where(jury.idJury.eq(Long.valueOf(idJury)));
		sqlQuery.orderBy(composantParentLienFormation.llComposant.asc());
		
		List<Tuple> tuples = template.query(sqlQuery, selectExpr);
		List<JuryRead> jurysRead = tuplesToJurysRead(tuples);

		return jurysRead;
		
	}
	
	public List<JuryRead> getJurys(Integer anneeExercice) {
		QTuple selectExpr = new QTuple(
				jury.idJury,
				composantParentLienFormation.llComposant,
				composantChildLienParcoursFormation.llComposant,
				composantChildLienNiveauAnnee.llComposant,
				composantChildLienParcoursAnnee.llComposant,
				composantChildLienNiveauSemestre.llComposant,
				sessionPeriode.libelle,
				sessionPeriode.dateOuverture,
				sessionPeriode.dateFermeture,
				juryEtat.code
				);
		
		SQLQuery sqlQuery = getQueryDeBase(anneeExercice);
		sqlQuery.orderBy(composantParentLienFormation.llComposant.asc());
		
		List<Tuple> tuples = template.query(sqlQuery, selectExpr);
		List<JuryRead> jurysRead = tuplesToJurysRead(tuples);

		return jurysRead;
		
	}
	
	private SQLQuery getQueryDeBase(Integer anneeExercice) {
		SQLQuery sqlQuery = template.newSqlQuery()
				.distinct()
				.from(jury)
				.innerJoin(lienFormation).on(jury.idLienFormation.eq(lienFormation.idLien))
				.leftJoin(lienParcoursFormation).on(jury.idLienParcoursFormation.eq(lienParcoursFormation.idLien))
				.leftJoin(lienNiveauAnnee).on(jury.idLienNiveauAnnee.eq(lienNiveauAnnee.idLien))
				.leftJoin(lienParcoursAnnee).on(jury.idLienParcoursAnnee.eq(lienParcoursAnnee.idLien))
				.leftJoin(lienNiveauSemestre).on(jury.idLienNiveauSemestre.eq(lienNiveauSemestre.idLien))
				.innerJoin(composantParentLienFormation).on(lienFormation.parentId.eq(composantParentLienFormation.idComposant))
				.leftJoin(composantChildLienParcoursFormation).on(lienParcoursFormation.childId.eq(composantChildLienParcoursFormation.idComposant))
				.leftJoin(composantChildLienNiveauAnnee).on(lienNiveauAnnee.childId.eq(composantChildLienNiveauAnnee.idComposant))
				.leftJoin(composantChildLienParcoursAnnee).on(lienParcoursAnnee.childId.eq(composantChildLienParcoursAnnee.idComposant))
				.leftJoin(composantChildLienNiveauSemestre).on(lienNiveauSemestre.childId.eq(composantChildLienNiveauSemestre.idComposant))
				.leftJoin(sessionPeriode).on(jury.idSessionPeriode.eq(sessionPeriode.idSessionPeriode))
				.innerJoin(juryEtat).on(jury.idJuryEtat.eq(juryEtat.idJuryEtat))
				.where(jury.annee.eq(anneeExercice));
		return sqlQuery;
	}
	
	private List<JuryRead> tuplesToJurysRead(List<Tuple> tuples) {
		List<JuryRead> jurysRead = new ArrayList<JuryRead>();
		for (Tuple tuple : tuples) {
			JuryRead juryRead = tupleToJuryRead(tuple);
			jurysRead.add(juryRead);
		}
		return jurysRead;
	}
	
	private JuryRead tupleToJuryRead(Tuple tuple) {
		JuryRead juryRead = new JuryRead(tuple.get(jury.idJury));
		juryRead.setLibelleFormation(tuple.get(composantParentLienFormation.llComposant));
		juryRead.setLibelleParcoursFormation(tuple.get(composantChildLienParcoursFormation.llComposant));
		juryRead.setLibelleNiveauAnnee(tuple.get(composantChildLienNiveauAnnee.llComposant));
		juryRead.setLibelleParcoursAnnee(tuple.get(composantChildLienParcoursAnnee.llComposant));
		juryRead.setLibelleNiveauSemestre(tuple.get(composantChildLienNiveauSemestre.llComposant));
		juryRead.setSession(tuple.get(sessionPeriode.libelle));
		juryRead.setSessionDateOuverture(tuple.get(sessionPeriode.dateOuverture));
		juryRead.setSessionDateFermeture(tuple.get(sessionPeriode.dateFermeture));
		juryRead.setCodeJuryEtat(tuple.get(juryEtat.code));
		return juryRead;
	}
	
}
