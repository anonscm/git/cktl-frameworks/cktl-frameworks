package org.cocktail.scol.jury;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IJuryEtat;

public class JuryRead {
	
	public static final String LIBELLE_FORMATION_KEY = "libelleFormation";
	public static final String LIBELLE_PARCOURS_FORMATION_KEY = "libelleParcoursFormation";
	public static final String LIBELLE_NIVEAU_ANNEE_KEY = "libelleNiveauAnnee";
	public static final String LIBELLE_PARCOURS_ANNEE_KEY = "libelleParcoursAnnee";
	public static final String LIBELLE_NIVEAU_SEMESTRE_KEY = "libelleNiveauSemestre";
	public static final String SESSION_KEY = "session";

	private Long id;
	private String libelleFormation;
	private String libelleParcoursFormation;
	private String libelleNiveauAnnee;
	private String libelleParcoursAnnee;
	private String libelleNiveauSemestre;
	private String session;
	private Date sessionDateOuverture;
	private Date sessionDateFermeture;
	private String codeJuryEtat;
	
	
	public JuryRead() {
		super();
	}
	
	public JuryRead(Long id) {
		super();
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getLibelleFormation() {
		return libelleFormation;
	}

	public void setLibelleFormation(String libelleFormation) {
		this.libelleFormation = libelleFormation;
	}

	public String getLibelleParcoursFormation() {
		return libelleParcoursFormation;
	}

	public void setLibelleParcoursFormation(String libelleParcoursFormation) {
		this.libelleParcoursFormation = libelleParcoursFormation;
	}

	public String getLibelleNiveauAnnee() {
		return libelleNiveauAnnee;
	}

	public void setLibelleNiveauAnnee(String libelleNiveauAnnee) {
		this.libelleNiveauAnnee = libelleNiveauAnnee;
	}

	public String getLibelleParcoursAnnee() {
		return libelleParcoursAnnee;
	}

	public void setLibelleParcoursAnnee(String libelleParcoursAnnee) {
		this.libelleParcoursAnnee = libelleParcoursAnnee;
	}

	public String getLibelleNiveauSemestre() {
		return libelleNiveauSemestre;
	}

	public void setLibelleNiveauSemestre(String libelleNiveauSemestre) {
		this.libelleNiveauSemestre = libelleNiveauSemestre;
	}
	
	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Date getSessionDateOuverture() {
		return sessionDateOuverture;
	}

	public void setSessionDateOuverture(Date sessionDateOuverture) {
		this.sessionDateOuverture = sessionDateOuverture;
	}

	public String getSessionDateOuvertureLibelle() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(getSessionDateOuverture());
	}
	
	public Date getSessionDateFermeture() {
		return sessionDateFermeture;
	}

	public void setSessionDateFermeture(Date sessionDateFermeture) {
		this.sessionDateFermeture = sessionDateFermeture;
	}
	
	public String getSessionDateFermetureLibelle() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(getSessionDateFermeture());
	}

	public String getCodeJuryEtat() {
		return codeJuryEtat;
	}

	public void setCodeJuryEtat(String codeJuryEtat) {
		this.codeJuryEtat = codeJuryEtat;
	}
	
	public Boolean isConvoque() {
		if (codeJuryEtat!=null) {
			return this.codeJuryEtat.equals(IJuryEtat.JE_CONVOQUE);
		}
		
		return null;
	}

}
