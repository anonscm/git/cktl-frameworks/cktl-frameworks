package org.cocktail.scol.maquette;

import java.util.List;

public class DiplomeAccrediteRead {

	public static final String CODE_KEY = "codeDiplome";
	public static final String STATUT_FORMATION_KEY = "statutFormation";
	public static final String TYPE_FORMATION_KEY = "typeFormation";
	public static final String SISE_KEY = "codeSise";
	public static final String SISE_ETAB_STR_KEY = "codeSiseEtab";
	public static final String LIBELLE_COURT_KEY = "libelleCourtDiplome";
	public static final String LIBELLE_KEY = "libelleLongDiplome";
	public static final String GRADE_UNIVERSITAIRE_KEY = "gradeUniversitaire";
	
	private Long id;
	private String typeFormation;
	private String codeSise;
	private String codeSiseEtab;
	private String codeDiplome;
	private String libelleCourtDiplome;
	private String libelleLongDiplome;
	private String gradeUniversitaire;
	private String statutFormation;
	private String habilitation;
	private List<String> structures;
	
	public DiplomeAccrediteRead(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return libelle du type de formation
	 */
	public String getTypeFormation() {
		return typeFormation;
	}

	public void setTypeFormation(String typeFormation) {
		this.typeFormation = typeFormation;
	}
	
	public String getCodeSise() {
		return codeSise;
	}

	public void setCodeSise(String codeSise) {
		this.codeSise = codeSise;
	}
	
	public String getCodeSiseEtab() {
		return codeSiseEtab;
	}

	public void setCodeSiseEtab(String codeSiseEtab) {
		this.codeSiseEtab = codeSiseEtab;
	}
	
	public String getCodeDiplome() {
		return codeDiplome;
	}

	public void setCodeDiplome(String codeDiplome) {
		this.codeDiplome = codeDiplome;
	}
	
	public String getLibelleCourtDiplome() {
		return libelleCourtDiplome;
	}

	public void setLibelleCourtDiplome(String libelleCourtDiplome) {
		this.libelleCourtDiplome = libelleCourtDiplome;
	}
	
	public String getLibelleLongDiplome() {
		return libelleLongDiplome;
	}

	public void setLibelleLongDiplome(String libelleLongDiplome) {
		this.libelleLongDiplome = libelleLongDiplome;
	}
	
	public String getGradeUniversitaire() {
		return gradeUniversitaire;
	}

	public void setGradeUniversitaire(String gradeUniversitaire) {
		this.gradeUniversitaire = gradeUniversitaire;
	}
	
	public String getStatutFormation() {
		return statutFormation;
	}

	public void setStatutFormation(String statutFormation) {
		this.statutFormation = statutFormation;
	}
	
	public String getHabilitation() {
		return habilitation;
	}

	public void setHabilitation(String habilitation) {
		this.habilitation = habilitation;
	}
	
	public List<String> getStructures() {
		return structures;
	}
	
	public void setStructures(List<String> structures) {
		this.structures = structures;
	}
	
}
