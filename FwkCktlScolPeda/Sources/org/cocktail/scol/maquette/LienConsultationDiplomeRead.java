package org.cocktail.scol.maquette;

/**
 * Représentation en lecture d'un lien de consultation dont le parent est un diplome
 */
public class LienConsultationDiplomeRead {
		private LienConsultationRead lienConsultationRead;
		private String libelleGradeUniversitaire;
		
		public LienConsultationRead getLienConsultationRead() {
	    return lienConsultationRead;
    }
		
		public void setLienConsultationRead(LienConsultationRead lienConsultationRead) {
	    this.lienConsultationRead = lienConsultationRead;
    }
		
		public String getLibelleGradeUniversitaire() {
	    return libelleGradeUniversitaire;
    }
		
		public void setLibelleGradeUniversitaire(String libelleGradeUniversitaire) {
	    this.libelleGradeUniversitaire = libelleGradeUniversitaire;
    }
}
