package org.cocktail.scol.maquette;

import java.util.List;

import org.cocktail.ref.support.q.grhum.QStructureUlr;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QStructuresComposant;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;


public class StructuresComposantReadService {
	
	private QueryDslJdbcTemplate template;
	private QStructureUlr structure = QStructureUlr.structureUlr;
	private QComposant composant = QComposant.composant;
	private QStructuresComposant structuresComposant = QStructuresComposant.structuresComposant;

	public StructuresComposantReadService(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public List<StructureRead> getStructures(Long composantId) {
		SQLQuery query = template.newSqlQuery()
				.distinct()
				.from(structure)
				.leftJoin(structuresComposant).on(structure.cStructure.eq(structuresComposant.cStructure))
				.where(structuresComposant.composantId.eq(composantId));
		
		ConstructorExpression<StructureRead> mapping = ConstructorExpression.create(StructureRead.class, 
				structure.cStructure,
				structure.llStructure);
		
		List<StructureRead> res = template.query(query, mapping);
		
		return res;
				
	}
	
}
