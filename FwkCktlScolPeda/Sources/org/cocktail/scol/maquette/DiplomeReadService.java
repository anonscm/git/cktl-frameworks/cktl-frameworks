package org.cocktail.scol.maquette;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.ref.support.q.grhum.QStructureUlr;
import org.cocktail.scol.support.QueryDslHelper;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QGradeUniversitaire;
import org.cocktail.scol.support.q.sco_scolarite.QHabilitation;
import org.cocktail.scol.support.q.sco_scolarite.QStatutFormation;
import org.cocktail.scol.support.q.sco_scolarite.QStructuresComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeFormation;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.QTuple;

public class DiplomeReadService {

	private QueryDslJdbcTemplate template;
	private QComposant composant = QComposant.composant;
	private QStatutFormation statutFormation = QStatutFormation.statutFormation;
	private QVLien vLien = QVLien.vLien;
	private QTypeFormation typeFormation = QTypeFormation.typeFormation;
	private QGradeUniversitaire gradeUniversitaire = QGradeUniversitaire.gradeUniversitaire;
	private QHabilitation habilitation = QHabilitation.habilitation;
	private QStructuresComposant structuresComposant = QStructuresComposant.structuresComposant;
	private QStructureUlr structureUlr = QStructureUlr.structureUlr;

	@Autowired
	public DiplomeReadService(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public List<DiplomeRead> getDiplomes(Long annee, boolean isEnModelisation) {
		SQLQuery query = template.newSqlQuery()
				.from(vLien)
				.innerJoin(composant).on(vLien.idDiplome.eq(composant.idComposant))
				.innerJoin(statutFormation).on(composant.dipStatutFormationId.eq(statutFormation.idStatutFormation))
				.where(vLien.vdipAnnee.eq(annee).and(predicatHabilitation(isEnModelisation)))
				.orderBy(composant.code.asc())
				.distinct();
		ConstructorExpression<DiplomeRead> mapping = ConstructorExpression.create(DiplomeRead.class, composant.idComposant, composant.code, composant.llComposant, statutFormation.code);
		List<DiplomeRead> res = template.query(query, mapping);
		return res;
	}
	
	public List<DiplomeAccrediteRead> getDiplomesAccredites(Long annee, Boolean isAccredite, Predicate predicatePerimetresStrategies) {
		SQLQuery query = template.newSqlQuery()
				.from(vLien)
				.innerJoin(composant).on(vLien.idDiplome.eq(composant.idComposant))
				.innerJoin(statutFormation).on(composant.dipStatutFormationId.eq(statutFormation.idStatutFormation))
				.leftJoin(gradeUniversitaire).on(composant.dipGradeUniversitaireId.eq(gradeUniversitaire.idGradeUniversitaire))
				.leftJoin(typeFormation).on(composant.dipTypeFormationId.eq(typeFormation.idTypeFormation))
				.leftJoin(habilitation).on(composant.dipHabilitationId.eq(habilitation.idHabilitation))
				.leftJoin(structuresComposant).on(composant.idComposant.eq(structuresComposant.composantId))
				.leftJoin(structureUlr).on(structuresComposant.cStructure.eq(structureUlr.cStructure))
				.where(vLien.vdipAnnee.eq(annee).and(predicatAccredite(isAccredite)).and(predicatePerimetresStrategies))
				.orderBy(composant.code.asc())
				.distinct();
		
		QTuple selectExpr = new QTuple(
				composant.idComposant, 
				typeFormation.libelle,
				composant.dipSiseMesr,
				composant.dipSiseEtablissement,
				composant.code,
				composant.lcComposant,
				composant.llComposant, 
				gradeUniversitaire.libelle,
				statutFormation.libelle,
				habilitation.numeroHabilitation,
				structureUlr.llStructure
				);
		
		List<Tuple> tuples = template.query(query, selectExpr);
		Map<Long, List<String>> structuresByIdComposant = QueryDslHelper.flatten(tuples, composant.idComposant, structureUlr.llStructure);
		List<DiplomeAccrediteRead> diplomes = tuplesToDiplomeAccrediteRead(tuples, structuresByIdComposant);
		return diplomes;
	}

	private List<DiplomeAccrediteRead> tuplesToDiplomeAccrediteRead(
			List<Tuple> tuples,
			Map<Long, List<String>> structuresByIdComposant) {
		List<DiplomeAccrediteRead> diplomesRead = new ArrayList<DiplomeAccrediteRead>();
		HashSet<Long> idsDiplomesTraites = new HashSet<Long>();
		for (Tuple tuple : tuples) {
			Long idComposant = tuple.get(composant.idComposant);
			if (!idsDiplomesTraites.contains(idComposant)) {
				DiplomeAccrediteRead diplomeRead = tupleToDiplomeAccrediteRead(tuple, idComposant);
				List<String> structures = structuresByIdComposant.get(idComposant);
				diplomeRead.setStructures(structures);
				diplomesRead.add(diplomeRead);
				idsDiplomesTraites.add(idComposant);
			}
		}
		return diplomesRead;
	}

	
	private DiplomeAccrediteRead tupleToDiplomeAccrediteRead(Tuple tuple,
			Long idComposant) {
		DiplomeAccrediteRead diplome = new DiplomeAccrediteRead(idComposant);
		diplome.setCodeDiplome(tuple.get(composant.code));
		diplome.setCodeSise(tuple.get(composant.dipSiseMesr));
		diplome.setCodeSiseEtab(tuple.get(composant.dipSiseEtablissement));
		diplome.setGradeUniversitaire(tuple.get(gradeUniversitaire.libelle));
		diplome.setHabilitation(tuple.get(habilitation.numeroHabilitation));
		diplome.setLibelleCourtDiplome(tuple.get(composant.lcComposant));
		diplome.setLibelleLongDiplome(tuple.get(composant.llComposant));
		diplome.setStatutFormation(tuple.get(statutFormation.libelle));
		diplome.setTypeFormation(tuple.get(typeFormation.libelle));
		return diplome;
	}

	private Predicate predicatAccredite(Boolean isAccredite) {
		Predicate predicat = null;
		
		if (isAccredite != null) {
			if (isAccredite) {
				predicat = statutFormation.code.in(EOStatutFormation.STATUT_EP_CODE, EOStatutFormation.STATUT_F_CODE).and(composant.dipHabilitationRequise.eq(1));
			} else {
				predicat = composant.dipHabilitationRequise.eq(0);
			}
		}
		
		return predicat;
	}

	private Predicate predicatHabilitation(Boolean isEnModelisation) {
		Predicate predicat;
		if (isEnModelisation) {
			predicat = composant.dipHabilitationRequise.eq(1);
		} else {
			predicat = statutFormation.code.in(EOStatutFormation.STATUT_EP_CODE, EOStatutFormation.STATUT_F_CODE)
					.or(composant.dipHabilitationRequise.eq(0));

		}
		return predicat;
	}	
	
}
