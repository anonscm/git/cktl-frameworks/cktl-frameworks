package org.cocktail.scol.maquette;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeDescriptifDestination;

import com.google.common.collect.Ordering;

public class ComposantSyllabusRead {

	private Long idLien;
	private Long idComposant;
	private Long ordreLien;
	private Long typeComposantId;
	private String libelle;
	private List<ComposantSyllabusRead> children;
	private Collection<Descriptif> descriptifs;
	private List<Long> selectedDescriptifs;
	private List<Descriptif> descriptifsPub;
	private List<Descriptif> descriptifsHab;

	
	private static final Ordering<Descriptif> orderByDateCreation = new Ordering<Descriptif>() {
		@Override
		public int compare(Descriptif left, Descriptif right) {
			return Ordering.natural().nullsLast().compare(left.getAnnee(), right.getAnnee());
		}
	};
	
	public ComposantSyllabusRead(Long idLien, Long idComposant, Long typeComposantId, String libelle, Long ordreLien) {
		this.idLien = idLien;
		this.idComposant = idComposant;
		this.libelle = libelle;
		this.ordreLien = ordreLien;
		this.typeComposantId = typeComposantId;
	}
	
	public Long getIdLien() {
		return idLien;
	}
	
	public Long getIdComposant() {
		return idComposant;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	public Long getTypeComposantId() {
		return typeComposantId;
	}
	
	public void setDescriptifs(Collection<Descriptif> descriptifs) {
		this.descriptifs = descriptifs;
	}
	
	public List<Descriptif> getDescriptifsHabilitation() {
		if (descriptifsHab == null) {
			descriptifsHab = getDescriptifsForType(ITypeDescriptifDestination.TYPEHABILITATION_LBL);
		}
		return descriptifsHab;
	}

	public List<Descriptif> getDescriptifsPublication() {
		if (descriptifsPub == null) {
			descriptifsPub = getDescriptifsForType(ITypeDescriptifDestination.TYPEPUBLICATION_LBL);
		}
		return descriptifsPub;
	}

	private List<Descriptif> getDescriptifsForType(String type) {
		List<Descriptif> descriptifsType = new ArrayList<ComposantSyllabusRead.Descriptif>();
		for (Descriptif desc : descriptifs) {
			if (type.equals(desc.getTypeDestination())) {
				descriptifsType.add(desc);
			}
		}
		return orderByDateCreation.sortedCopy(descriptifsType);
	}
	
	public List<ComposantSyllabusRead> getChildren() {
		return children;
	}
	
	public void setChildren(List<ComposantSyllabusRead> children) {
		this.children = children;
	}
	
	public Long getOrdreLien() {
		return ordreLien;
	}
	
	public void setSelectedDescriptifs(List<Long> selectedDescriptifs) {
		this.selectedDescriptifs = selectedDescriptifs;
	}

	public Long getSelectedDescriptifHabilitation() {
		return getSelectedDescriptif(getDescriptifsHabilitation());
	}

	public Long getSelectedDescriptifPublication() {
		return getSelectedDescriptif(getDescriptifsPublication());
	}
	
	private Long getSelectedDescriptif(List<Descriptif> descriptifs) {
		Long selected = null;
		for (Long descId : selectedDescriptifs) {
			for (Descriptif desc : descriptifs) {
				if (descId.equals(desc.getId())) {
					selected = descId;
					break;
				}
			}
		}
		return selected;
	}
	
	public static class Descriptif {
		
		private Long id;
		private String typeDestination;
		private String version;
		private Date annee;
		
		public Descriptif(Long id, String typeDestination, String version, Date annee) {
			this.id = id;
			this.typeDestination = typeDestination;
			this.version = version;
			this.annee = annee;
		}

		public Long getId() {
			return id;
		}

		public String getTypeDestination() {
			return typeDestination;
		}

		public String getVersion() {
			return version;
		}
		
		public Date getAnnee() {
			return annee;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((annee == null) ? 0 : annee.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime
					* result
					+ ((typeDestination == null) ? 0 : typeDestination
							.hashCode());
			result = prime * result
					+ ((version == null) ? 0 : version.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Descriptif other = (Descriptif) obj;
			if (annee == null) {
				if (other.annee != null)
					return false;
			} else if (!annee.equals(other.annee))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (typeDestination == null) {
				if (other.typeDestination != null)
					return false;
			} else if (!typeDestination.equals(other.typeDestination))
				return false;
			if (version == null) {
				if (other.version != null)
					return false;
			} else if (!version.equals(other.version))
				return false;
			return true;
		}
		
	}

	
}
