package org.cocktail.scol.maquette;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation; 
import org.cocktail.fwkcktlscolpeda.serveur.metier.IStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypePeriode;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QComposantInfoVersion;
import org.cocktail.scol.support.q.sco_scolarite.QGradeUniversitaire;
import org.cocktail.scol.support.q.sco_scolarite.QLien;
import org.cocktail.scol.support.q.sco_scolarite.QStatutFormation;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeFormation;
import org.cocktail.scol.support.q.sco_scolarite.QTypePeriode;
import org.cocktail.scol.support.q.sco_scolarite.QVDiplomeVersionMax;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;

/**
 * Cette classe représente l'implémentation QueryDsl de lecture des {@link ILienConsultation}.
 * 
 * @author Pascal MACOUIN
 */
public class LienConsultationReadRepository {

	private QueryDslJdbcTemplate template;

	private QComposant tComposant = QComposant.composant;
	private QComposant tComposantParent = new QComposant("COMPOSANT_PARENT");
	private QComposantInfoVersion tComposantInfoVersion = QComposantInfoVersion.composantInfoVersion;
	private QStatutFormation tStatutFormation = QStatutFormation.statutFormation;
	private QVLien vLien = QVLien.vLien;
	private QVDiplomeVersionMax vDiplomeVersionMax = QVDiplomeVersionMax.vDiplomeVersionMax;
	private QTypeComposant typeComposant = QTypeComposant.typeComposant;
	private QLien tlien = QLien.lien;
	private QTypePeriode typePeriode = QTypePeriode.typePeriode;
	private QTypeFormation typeFormation = QTypeFormation.typeFormation;
	private QGradeUniversitaire gradeUniversitaire = QGradeUniversitaire.gradeUniversitaire;

	@Autowired
	public LienConsultationReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

	/**
	 * Retourne tous les dernières versions des diplômes en production pour l'année.
	 * @param annee Une année
	 * @return La liste des diplômes
	 */
	public List<LienConsultationRead> getDiplomesVersionMaxEnProduction(long annee) {
		SQLQuery query = getQueryDiplomesVersionMaxEnProduction();
		query = query.where(clauseWhereDiplome(annee, null, null))
		    .orderBy(tComposantParent.code.asc());

		Expression<?>[] paths = new Expression<?>[] {
		    vLien.idLien,
		    vLien.idLienContexte,
		    vLien.parentId,
		    tComposantParent.code,
		    tComposantParent.llComposant,
		    tComposantParent.lcComposant,
		};

		LienConsultationReadMapping lienConsultationReadMapping = new LienConsultationReadMapping(paths, tComposantParent, null);
		List<LienConsultationRead> res = template.query(query, lienConsultationReadMapping);

		return res;
	}

	/**
	 * Retourne tous les dernières versions des diplômes en production pour l'année.
	 * @param annee Une année
	 * @param typeFormationsOuvertes liste des types de formations ouvertes, si null pas de conditions sur ce critere
	 * @param gradesInterdits liste des grades interdits, si null pas de conditions sur ce critere
	 * @return La liste des diplômes
	 */
	public List<LienConsultationDiplomeRead> getDiplomesVersionMaxEnProduction(long annee, List<String> typeFormationsOuvertes, List<String> gradesInterdits) {
		SQLQuery query = getQueryDiplomesVersionMaxEnProduction()
		    .leftJoin(typeFormation).on(tComposantParent.dipTypeFormationId.eq(typeFormation.idTypeFormation))
		    .leftJoin(gradeUniversitaire).on(tComposantParent.dipGradeUniversitaireId.eq(gradeUniversitaire.idGradeUniversitaire));

		query = query.where(clauseWhereDiplome(annee, typeFormationsOuvertes, gradesInterdits))
		    .orderBy(tComposantParent.code.asc());

		Expression<?>[] paths = new Expression<?>[] {
		    vLien.idLien,
		    vLien.idLienContexte,
		    vLien.parentId,
		    tComposantParent.code,
		    tComposantParent.llComposant,
		    tComposantParent.lcComposant,
		    gradeUniversitaire.libelle,
		    gradeUniversitaire.nbUnitesTemps
		};

		LienConsultationDiplomeReadMapping lienConsultationReadMapping = new LienConsultationDiplomeReadMapping(paths, tComposantParent, null);
		List<LienConsultationDiplomeRead> res = template.query(query, lienConsultationReadMapping);

		return res;

	}

	private SQLQuery getQueryDiplomesVersionMaxEnProduction() {
		return template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(tComposant).on(tComposant.idComposant.eq(vLien.childId))
		    .innerJoin(tComposantParent).on(tComposantParent.idComposant.eq(vLien.parentId))
		    .innerJoin(tStatutFormation).on(tStatutFormation.idStatutFormation.eq(tComposantParent.dipStatutFormationId))
		    .innerJoin(tComposantInfoVersion).on(tComposantInfoVersion.idInfoVersion.eq(tComposant.infoVersionId)
		        .and(tComposantInfoVersion.annee.eq(vLien.vdipAnnee)))
		    .innerJoin(vDiplomeVersionMax).on(vDiplomeVersionMax.idDiplome.eq(vLien.parentId)
		        .and(vDiplomeVersionMax.annee.eq(vLien.vdipAnnee))
		        .and(vDiplomeVersionMax.versionMajeure.isNull()
		            .or(vDiplomeVersionMax.versionMajeure.eq(tComposantInfoVersion.versionMajeure))));
	}

	private BooleanExpression clauseWhereDiplome(long annee, List<String> typeFormationsOuvertes, List<String> gradesInterdits) {
		BooleanExpression clauseWhere =
		    vLien.typeComposantNomParent.eq(ITypeComposant.TYPEDIPLOME_NOM)
		        .and(vLien.vdipAnnee.eq(annee))
		        .and(predicatEnProduction());
		if (typeFormationsOuvertes != null) {
			clauseWhere = clauseWhere.and(predicateOuvertAInscription(typeFormationsOuvertes));
		}
		if (gradesInterdits != null) {
			clauseWhere = clauseWhere.and(predicateGradesInterdits(gradesInterdits));
		}
		return clauseWhere;
	}

	/**
	 * Retourne la dernières version diplôme du diplôme pour l'année. <p>Peu importe que le diplôme soit en production ou non.
	 * @param idDiplome id du composant diplôme
	 * @param annee Une année
	 * @return La version diplôme maxi pour ce diplôme et cette année
	 */
	public LienConsultationRead getVersionDiplomeVersionMax(long idDiplome, long annee) {
		SQLQuery query = template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(tComposant).on(tComposant.idComposant.eq(vLien.childId))
		    .innerJoin(tComposantParent).on(tComposantParent.idComposant.eq(vLien.parentId))
		    .innerJoin(tComposantInfoVersion).on(tComposantInfoVersion.idInfoVersion.eq(tComposant.infoVersionId)
		        .and(tComposantInfoVersion.annee.eq(vLien.vdipAnnee)))
		    .innerJoin(vDiplomeVersionMax).on(vDiplomeVersionMax.idDiplome.eq(vLien.parentId)
		        .and(vDiplomeVersionMax.annee.eq(vLien.vdipAnnee))
		        .and(vDiplomeVersionMax.versionMajeure.isNull()
		            .or(vDiplomeVersionMax.versionMajeure.eq(tComposantInfoVersion.versionMajeure))))
		    .where(vLien.parentId.eq(idDiplome)
		        .and(vLien.vdipAnnee.eq(annee)));

		Expression<?>[] paths = new Expression<?>[] {
		    vLien.idLien,
		};

		LienConsultationReadMapping lienConsultationReadMapping = new LienConsultationReadMapping(paths, null, null);
		LienConsultationRead res = template.queryForObject(query, lienConsultationReadMapping);

		return res;
	}

	/**
	 * Diplôme en production.
	 * @return Prédicat d'un diplôme en production
	 */
	private Predicate predicatEnProduction() {
		return tStatutFormation.code.in(IStatutFormation.STATUT_EP_CODE, IStatutFormation.STATUT_F_CODE)
		    .or(tComposant.dipHabilitationRequise.eq(0));
	}

	private Predicate predicateOuvertAInscription(List<String> typeFormationsOuvertes) {
		return typeFormation.code.in(typeFormationsOuvertes);
	}

	private Predicate predicateGradesInterdits(List<String> gradesInterdits) {
		return gradeUniversitaire.typeGradeUniversitaire.notIn(gradesInterdits);
	}

	/**
	 * @param lienComposant le lien de la version de diplome
	 * @return les liens qui portent des parcours et qui sont directement sous le lien version de diplome
	 */
	public List<LienConsultationRead> getLiensParcoursSous(LienConsultationRead lienComposant) {
		SQLQuery query = template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(tlien).on(tlien.idLien.eq(vLien.idLien))
		    .innerJoin(tComposantParent).on(tlien.parentId.eq(tComposantParent.idComposant))
		    .innerJoin(tComposant).on(tlien.childId.eq(tComposant.idComposant))
		    .innerJoin(typeComposant).on(tComposant.typeComposantId.eq(typeComposant.idTypeComposant))
		    .where(vLien.idLienParent.eq(lienComposant.getIdLien())
		        .and(typeComposant.nom.eq(ITypeComposant.TYPEPARCOURS_NOM)));

		Expression<?>[] paths = new Expression<?>[] {
		    vLien.idLien,
		    vLien.idLienContexte,
		    tComposant.code,
		    tComposant.llComposant
		};

		LienConsultationReadMapping lienConsultationReadMapping = new LienConsultationReadMapping(paths, null, tComposant);
		List<LienConsultationRead> res = template.query(query, lienConsultationReadMapping);

		return res;
	}

	/**
	 * @param lienComposant lien consultation dont le child est le composant concerné
	 * @return les composants de type année qui sont directement sous le composant
	 */
	public List<LienConsultationRead> getLiensAnneeSous(LienConsultationRead lienComposant, List<Long> niveauxPossibles) {
		SQLQuery query = template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(tlien).on(tlien.idLien.eq(vLien.idLien))
		    .innerJoin(tComposantParent).on(tlien.parentId.eq(tComposantParent.idComposant))
		    .innerJoin(tComposant).on(tlien.childId.eq(tComposant.idComposant))
		    .innerJoin(typeComposant).on(tComposant.typeComposantId.eq(typeComposant.idTypeComposant))
		    .innerJoin(typePeriode).on(typePeriode.idTypePeriode.eq(tComposant.perIdType))
		    .where(clauseWhereAnnee(lienComposant, niveauxPossibles))
		    .orderBy(tComposant.perOrdre.asc());

		Expression<?>[] paths = new Expression<?>[] {
		    vLien.idLien,
		    vLien.idLienContexte,
		    vLien.childId,
		    tComposant.code,
		    tComposant.llComposant
		};

		LienConsultationReadMapping lienConsultationReadMapping = new LienConsultationReadMapping(paths, null, tComposant);
		List<LienConsultationRead> res = template.query(query, lienConsultationReadMapping);

		return res;
	}

	private BooleanExpression clauseWhereAnnee(LienConsultationRead lienComposant, List<Long> niveauxPossibles) {
		BooleanExpression clauseWhere =
		    vLien.idLienParent.eq(lienComposant.getIdLien())
		        .and(typeComposant.nom.eq(ITypeComposant.TYPEPERIODE_NOM))
		        .and(typePeriode.code.eq(ITypePeriode.CODE_ANNEE));
		if (niveauxPossibles != null) {
			clauseWhere = clauseWhere.and(tComposant.perOrdre.in(niveauxPossibles));
		}
		return clauseWhere;
	}
}
