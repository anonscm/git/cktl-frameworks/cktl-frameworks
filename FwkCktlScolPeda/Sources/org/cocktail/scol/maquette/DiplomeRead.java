package org.cocktail.scol.maquette;

public class DiplomeRead {

	private Long id;
	private String code;
	private String libelle;
	private String codeStatutFormation;
	
	public DiplomeRead(Long id, String code, String libelle, String codeStatutFormation) {
		super();
		this.id = id;
		this.code = code;
		this.libelle = libelle;
		this.codeStatutFormation = codeStatutFormation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiplomeRead other = (DiplomeRead) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	public String getCodeStatutFormation() {
		return codeStatutFormation;
	}
	
}
