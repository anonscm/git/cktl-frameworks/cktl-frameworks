package org.cocktail.scol.maquette;

/**
 *  Représentation en lecture d'un lien de consultation
 */
public class LienConsultationRead {
	private Long idLien;
	private String idLienContexte;
	private Long parentId;
	private Long childId;
	private String codeComposantParent;
	private String llComposantParent;
	private String llComposant;
	private String codeComposant;
	
	public Long getIdLien() {
		return idLien;
	}
	public void setIdLien(Long idLien) {
		this.idLien = idLien;
	}
	public String getIdLienContexte() {
		return idLienContexte;
	}
	public void setIdLienContexte(String idLienContexte) {
		this.idLienContexte = idLienContexte;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long childId) {
		this.parentId = childId;
	}
	public String getCodeComposantParent() {
		return codeComposantParent;
	}
	public void setCodeComposantParent(String codeComposantChild) {
		this.codeComposantParent = codeComposantChild;
	}
	public String getLlComposantParent() {
		return llComposantParent;
	}
	public void setLlComposantParent(String llComposantChild) {
		this.llComposantParent = llComposantChild;
	}
	
	public String getLlComposant() {
	  return llComposant;
  }
	public void setLlComposant(String llComposant) {
	  this.llComposant = llComposant;
  }
	public String getCodeComposant() {
	  return codeComposant;
  }
	public void setCodeComposant(String codeComposant) {
	  this.codeComposant = codeComposant;
  }
	
	public Long getChildId() {
	  return childId;
  }
	
	public void setChildId(Long childId) {
	  this.childId = childId;
  }
}
