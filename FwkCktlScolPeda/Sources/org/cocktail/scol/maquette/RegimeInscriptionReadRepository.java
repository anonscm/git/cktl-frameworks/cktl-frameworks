package org.cocktail.scol.maquette;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeInscription;
import org.cocktail.scol.support.ReadRepositoryConfig;
import org.cocktail.scol.support.q.sco_scolarite.QPeriodeRegimeInscription;
import org.cocktail.scol.support.q.sco_scolarite.QRegimeInscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Expression;

/**
 * Service de récupération en lecture des régimes d'inscription {@link IRegimeInscription}
 *
 */
public class RegimeInscriptionReadRepository {
	private QueryDslJdbcTemplate template;
	private QRegimeInscription regimeInscription = QRegimeInscription.regimeInscription;
	private QPeriodeRegimeInscription periodeRegimeInscription = QPeriodeRegimeInscription.periodeRegimeInscription;

	/**
	 * enregistrement du repository cf {@link ReadRepositoryConfig}
	 * @param template template qui sera utilisé dans les requetes
	 */
	@Autowired
	public RegimeInscriptionReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

	/**
	 * @return liste de tous les regimes d'inscription possibles
	 */
	public List<RegimeInscriptionRead> getRegimesInscriptions() {
		SQLQuery query = template.newSqlQuery().from(regimeInscription).orderBy(regimeInscription.code.asc());

		return applyQuery(query);
	}

	/**
	 * @param idPeriode id de la période pour laquelle on recherche un régime d'inscription
	 * @return liste de tous les regimes d'inscription pour une période donnée
	 */
	public List<RegimeInscriptionRead> getRegimesInscriptionsPeriode(Long idPeriode) {
		SQLQuery query = template.newSqlQuery()
		    .from(regimeInscription)
		    .innerJoin(periodeRegimeInscription).on(regimeInscription.idRegimeInscription.eq(periodeRegimeInscription.regimeInscriptionId))
		    .where(periodeRegimeInscription.periodeId.eq(idPeriode))
		    .orderBy(regimeInscription.code.asc());
		return applyQuery(query);
	}

	private List<RegimeInscriptionRead> applyQuery(SQLQuery query) {
		Expression<?>[] paths = new Expression<?>[] {
		    regimeInscription.idRegimeInscription,
		    regimeInscription.code,
		    regimeInscription.libelle
		};

		RegimeInscriptionMapping regimeInscriptionMapping = new RegimeInscriptionMapping(paths);
		List<RegimeInscriptionRead> res = template.query(query, regimeInscriptionMapping);

		return res;
	}
}
