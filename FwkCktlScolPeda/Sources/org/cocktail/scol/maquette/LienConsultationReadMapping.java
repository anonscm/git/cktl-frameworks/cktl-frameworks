package org.cocktail.scol.maquette;

import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

/**
 * Classe de mapping entre un tuple QueryDSL et le bean {@link LienConsultationRead}.
 * 
 * @author Pascal MACOUIN
 */
public class LienConsultationReadMapping  extends MappingProjection<LienConsultationRead> {

	private QComposant tComposantParent;
	private QComposant tComposant;

	/**
	 * @param paths les valeurs ramenées par la requetes
	 * @param tComposantParent valeur du composant parent 
	 * @param tComposant valeur du composant
	 */
	public LienConsultationReadMapping(Expression<?>[] paths, QComposant tComposantParent, QComposant tComposant) {
		super(LienConsultationRead.class, paths);
		this.tComposantParent = tComposantParent;
		this.tComposant = tComposant;
	}

	@Override
	protected LienConsultationRead map(Tuple row) {
		LienConsultationRead lienConsultationRead = new LienConsultationRead();
		
		lienConsultationRead.setIdLien(row.get(QVLien.vLien.idLien));
		lienConsultationRead.setIdLienContexte(row.get(QVLien.vLien.idLienContexte));
		lienConsultationRead.setParentId(row.get(QVLien.vLien.parentId));
		lienConsultationRead.setChildId(row.get(QVLien.vLien.childId));
		
		if (tComposantParent != null) {
			lienConsultationRead.setCodeComposantParent(row.get(tComposantParent.code));
			lienConsultationRead.setLlComposantParent(row.get(tComposantParent.llComposant));
		}
		
		if (tComposant != null) {
			lienConsultationRead.setCodeComposant(row.get(tComposant.code));
			lienConsultationRead.setLlComposant(row.get(tComposant.llComposant));
		}
		
		return lienConsultationRead;
	}

	
}
