package org.cocktail.scol.maquette;


public class StructureRead {
	
	private String cStructure;
	private String libelleLongStructure;
	
	public StructureRead() {
		super();
	}
	
	public StructureRead(String code) {
		super();
		this.cStructure = code;
	}
	
	public StructureRead(String cStructure, String libelleLongStructure) {
		super();
		this.cStructure = cStructure;
		this.libelleLongStructure = libelleLongStructure;
	}
	
	public String getcStructure() {
		return cStructure;
	}

	public String getLibelleLongStructure() {
		return libelleLongStructure;
	}

}
