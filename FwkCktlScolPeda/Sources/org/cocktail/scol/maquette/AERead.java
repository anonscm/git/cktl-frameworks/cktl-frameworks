package org.cocktail.scol.maquette;


/**
 * Mapping sur un AE en lecture avec éventuellement des infos relatives à l'état de l'examen
 */
public class AERead {
	private Long idComposant;
	private String codeTypeAE;
	private String libelleAE;
	private Long idExamen;
	private String etatExamenCode;
	private String etatExamen;
	private String libelleSessionExamen;

	public static final String CODE_TYPE_AE = "codeTypeAE";
	public static final String LIBELLE_AE = "libelleAE";
	public static final String ETAT_EXAMEN = "etatExamen";
	public static final String LIBELLE_SESSION_EXAMEN = "libelleSessionExamen";
	
	/**
	 * @param idcomposant id du composant
	 * @param codeTypeAE code du typeAE
	 * @param libelleAE libelle de l'AE
	 */
	public AERead(Long idcomposant, String codeTypeAE, String libelleAE) {
		super();
		this.idComposant = idcomposant;
		this.codeTypeAE = codeTypeAE;
		this.libelleAE = libelleAE;
	}
	
	/**
	 * @param idComposant id du composant
	 * @param codeTypeAE code du typeAE
	 * @param libelleAE libelle de l'AE
	 * @param etatExamen état de l'examen associé
	 */
	public AERead(Long idComposant, String codeTypeAE, String libelleAE,  String etatExamen) {
		this(idComposant, codeTypeAE, libelleAE);
		this.etatExamen = etatExamen;
	}
	
	/**
	 * @param idComposant id du composant
	 * @param codeTypeAE code du typeAE
	 * @param libelleAE libelle de l'AE
	 * @param idExamen id de l'examen associé
	 * @param etatExamen état de l'examen associé
	 */
	public AERead(Long idComposant, String codeTypeAE, String libelleAE, Long idExamen, String etatExamen) {
		this(idComposant, codeTypeAE, libelleAE);
		this.idExamen = idExamen;
		this.etatExamen = etatExamen;
	}
	
	/**
	 * @param idComposant id du lien
	 * @param codeTypeAE code du typeAE
	 * @param libelleAE libelle de l'AE
	 * @param idExamen id de l'examen associé
	 * @param etatExamenCode code de l'etat de l'examen
	 * @param etatExamen état de l'examen associé
	 * @param libelleSessionExamen libellé de la session associée à l'examen
	 */
	public AERead(Long idComposant, String codeTypeAE, String libelleAE, Long idExamen, String etatExamenCode, String etatExamen, String libelleSessionExamen) {
		this(idComposant, codeTypeAE, libelleAE);
		this.idExamen = idExamen;
		this.etatExamenCode = etatExamenCode;
		this.etatExamen = etatExamen;
		this.libelleSessionExamen = libelleSessionExamen;
	}
	
	public Long getIdComposant() {
		return idComposant;
	}

	public String getCodeTypeAE() {
		return codeTypeAE;
	}

	public String getLibelleAE() {
		return libelleAE;
	}
	
	public Long getIdExamen() {
		return idExamen;
	}
	
	public void setIdExamen(Long idExamen) {
		this.idExamen = idExamen;
	}
	
	public String getEtatExamenCode() {
		return etatExamenCode;
	}
	
	public void setEtatExamenCode(String etatExamenCode) {
		this.etatExamenCode = etatExamenCode;
	}
	
	public String getEtatExamen() {
		return etatExamen;
	}
	
	public void setEtatExamen(String etatExamen) {
		this.etatExamen = etatExamen;
	}

	public String getLibelleSessionExamen() {
		return libelleSessionExamen;
	}

	public void setLibelleSessionExamen(String libelleSessionExamen) {
		this.libelleSessionExamen = libelleSessionExamen;
	}
	
}
