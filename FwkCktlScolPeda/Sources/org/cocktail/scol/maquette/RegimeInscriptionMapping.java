package org.cocktail.scol.maquette;

import org.cocktail.scol.support.q.sco_scolarite.QRegimeInscription;
import org.cocktail.scol.support.q.sco_scolarite.id.QRegimeInscriptionId;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

/**
 * Classe de mapping entre un tuple QueryDSL et le bean {@link RegimeInscriptionRead}.
 */
public class RegimeInscriptionMapping extends MappingProjection<RegimeInscriptionRead> {
  private static final long serialVersionUID = 1L;

	/**
	 * @param paths  liste des colonnes passées au constructeur
	 */
	public RegimeInscriptionMapping(Expression<?>[] paths) {
	  super(RegimeInscriptionRead.class, paths);
  }

	@Override
  protected RegimeInscriptionRead map(Tuple row) {
	  RegimeInscriptionRead regimeInscriptionRead = new RegimeInscriptionRead();
	  regimeInscriptionRead.setId(QRegimeInscriptionId.newId(row.get(QRegimeInscription.regimeInscription.idRegimeInscription)));
	  regimeInscriptionRead.setCode(row.get(QRegimeInscription.regimeInscription.code));
	  regimeInscriptionRead.setLibelle(row.get(QRegimeInscription.regimeInscription.libelle));
	  return regimeInscriptionRead;
  }

}
