package org.cocktail.scol.maquette;

import org.cocktail.scol.support.q.sco_scolarite.id.QRegimeInscriptionId;

/**
 * Représentation en lecture d'un régime d'inscription
 *
 */
public class RegimeInscriptionRead {
	private String code;
	private String libelle;
	private QRegimeInscriptionId id;
	
	public String getCode() {
	  return code;
  }
	public void setCode(String code) {
	  this.code = code;
  }
	
	public String getLibelle() {
	  return libelle;
  }
	
	public void setLibelle(String libelle) {
	  this.libelle = libelle;
  }
	
	public QRegimeInscriptionId getId() {
	  return id;
  }
	
	public void setId(QRegimeInscriptionId id) {
	  this.id = id;
  }
}
