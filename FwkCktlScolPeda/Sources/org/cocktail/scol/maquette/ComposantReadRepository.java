package org.cocktail.scol.maquette;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.ref.support.q.grhum.QImplantationGeo;
import org.cocktail.ref.support.q.grhum.QStructureUlr;
import org.cocktail.scol.support.QueryDslHelper;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QComposantInfoVersion;
import org.cocktail.scol.support.q.sco_scolarite.QCreditable;
import org.cocktail.scol.support.q.sco_scolarite.QDiscipline;
import org.cocktail.scol.support.q.sco_scolarite.QDisciplinesComposant;
import org.cocktail.scol.support.q.sco_scolarite.QImplantationsComp;
import org.cocktail.scol.support.q.sco_scolarite.QStatutFormation;
import org.cocktail.scol.support.q.sco_scolarite.QStructuresComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QVDiplomeVersionMax;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.Tuple;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.NumberPath;
import com.webobjects.foundation.NSArray;

/**
 *  Service de récupération en lecture des composants
 */
public class ComposantReadRepository {
	
	private QueryDslJdbcTemplate template;
	private QComposant composant = QComposant.composant;
	private QComposant versionDiplome = new QComposant("versionDiplome"); 
	private QComposant diplome = new QComposant("diplome"); 
	private QStatutFormation statutFormation = QStatutFormation.statutFormation;
	private QComposantInfoVersion composantInfoVersion = QComposantInfoVersion.composantInfoVersion;
	private QVLien vLien = QVLien.vLien;
	private QTypeComposant typeComposant = QTypeComposant.typeComposant;
	private QVDiplomeVersionMax vDiplomeVersionMax = QVDiplomeVersionMax.vDiplomeVersionMax;
	private QCreditable creditable = QCreditable.creditable;
	private QDisciplinesComposant disciplinesComposant = QDisciplinesComposant.disciplinesComposant;
	private QDiscipline discipline = QDiscipline.discipline;
	private QStructuresComposant structuresComposant = QStructuresComposant.structuresComposant;
	private QStructureUlr structureUlr = QStructureUlr.structureUlr;
	private QImplantationGeo implantationGeo = QImplantationGeo.implantationGeo;
	private QImplantationsComp implantationsComp = QImplantationsComp.implantationsComp;
	private QComposant composantParent = new QComposant("composantParent");

	public ComposantReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}
	
	public List<ComposantRead> getComposantsMaquettage(Long annee, Predicate predicatePerimetresStrategies, ITypeComposant...typesComposant) {
		List<ComposantRead> composantsRead = getComposants(annee, false, predicatePerimetresStrategies, typesComposant);
		return composantsRead;
	}
	
	public List<ComposantRead> getComposantsModelisation(Long annee, Predicate predicatePerimetresStrategies, ITypeComposant...typesComposant) {
		List<ComposantRead> composantsRead = getComposants(annee, true, predicatePerimetresStrategies, typesComposant);
		return composantsRead;
	}
	
	private List<ComposantRead> getComposants(Long annee,
			boolean isEnModelisation,
			Predicate predicatePerimetresStrategies,
			ITypeComposant... typesComposant) {
		NumberPath<Long> idComposant = composant.idComposant;
		StringExpression libelleComposantParent = composantParent.code.concat(" - ").concat(composantParent.llComposant);
		QTuple selectExpr = new QTuple(
				idComposant,
				typeComposant.libelle,
				composant.code,
				composant.lcComposant,
				composant.llComposant,
				creditable.creditEcts.stringValue(),
				creditable.travailEtudiant,
				creditable.baseNote,
				composant.ecLibre,
				composant.parParcoursType,
				discipline.llDiscipline,
				libelleComposantParent,
				structureUlr.llStructure,
				implantationGeo.llImplantationGeo
				);
		
		SQLQuery sqlQuery = getComposantsQuery(annee, isEnModelisation, predicatePerimetresStrategies, typesComposant);
		List<Tuple> tuples = template.query(sqlQuery, selectExpr);
		
		Map<Long, List<String>> disciplinesByIdComposant = QueryDslHelper.flatten(tuples, idComposant, discipline.llDiscipline);
		Map<Long, List<String>> parentsByIdComposant = QueryDslHelper.flatten(tuples, idComposant, libelleComposantParent);
		Map<Long, List<String>> structuresByIdComposant = QueryDslHelper.flatten(tuples, idComposant, structureUlr.llStructure);
		Map<Long, List<String>> implantationsByIdComposant = QueryDslHelper.flatten(tuples, idComposant, implantationGeo.llImplantationGeo);
		
		List<ComposantRead> composantsRead = tuplesToComposantsRead(tuples, disciplinesByIdComposant, parentsByIdComposant, structuresByIdComposant, implantationsByIdComposant);
		return composantsRead;
	}
	
	private List<ComposantRead> tuplesToComposantsRead(
			List<Tuple> tuples,
			Map<Long, List<String>> disciplinesByTuple,
			Map<Long, List<String>> parentsByTuple,
			Map<Long, List<String>> structuresByTuple,
			Map<Long, List<String>> implantationsByTuple) {
		List<ComposantRead> composantsRead = new ArrayList<ComposantRead>();
		HashSet<Long> idsComposantsTraites = new HashSet<Long>();
		for (Tuple tuple : tuples) {
			Long idComposant = tuple.get(composant.idComposant);
			if (!idsComposantsTraites.contains(idComposant)) {
				ComposantRead composantRead = tupleToComposantRead(tuple,
						idComposant);
				List<String> disciplines = disciplinesByTuple.get(idComposant);
				composantRead.setDisciplines(disciplines);
				List<String> parents = parentsByTuple.get(idComposant);
				composantRead.setComposantsParent(parents);
				List<String> structures = structuresByTuple.get(idComposant);
				composantRead.setStructures(structures);
				List<String> implantations = implantationsByTuple.get(idComposant);
				composantRead.setImplantations(implantations);
				composantsRead.add(composantRead);
				idsComposantsTraites.add(idComposant);
			}
		}
		return composantsRead;
	}

	private ComposantRead tupleToComposantRead(Tuple tuple, Long idComposant) {
		ComposantRead composantRead = new ComposantRead(idComposant);
		composantRead.setTypeComposant(tuple.get(typeComposant.libelle));
		composantRead.setCode(tuple.get(composant.code));
		composantRead.setLibelleCourt(tuple.get(composant.lcComposant));
		composantRead.setLibelleLong(tuple.get(composant.llComposant));
		composantRead.setLibre(tuple.get(composant.ecLibre));
		composantRead.setCreditableCreditECTSNumber(tuple.get(creditable.creditEcts.stringValue()));
		composantRead.setCreditableTravailEtudiant(tuple.get(creditable.travailEtudiant));
		composantRead.setCreditableBaseNote(tuple.get(creditable.baseNote));
		composantRead.setParcoursType(tuple.get(composant.parParcoursType));
		return composantRead;
	}
	
	@SuppressWarnings("unchecked")
	private SQLQuery getComposantsQuery(Long annee, Boolean isEnModelisation, Predicate predicatePerimetresStrategies, ITypeComposant...typesComposant) {		
		NSArray<ITypeComposant> types = new NSArray<ITypeComposant>(typesComposant);
		NSArray<String> nomsTypes = (NSArray<String>) types.valueForKey(EOTypeComposant.NOM.key());

		QComposantInfoVersion infoVersionVersionDiplome = new QComposantInfoVersion("infoVersionVersionDiplome");

		SQLQuery query = template.newSqlQuery()
				.distinct()
				.from(composant)
				.innerJoin(typeComposant).on(composant.typeComposantId.eq(typeComposant.idTypeComposant))
				.leftJoin(composantInfoVersion).on(composant.infoVersionId.eq(composantInfoVersion.idInfoVersion))
				.leftJoin(vLien).on(composant.idComposant.eq(vLien.childId))
				.leftJoin(versionDiplome).on(vLien.idVersionDiplome.eq(versionDiplome.idComposant))
				.leftJoin(infoVersionVersionDiplome).on(versionDiplome.infoVersionId.eq(infoVersionVersionDiplome.idInfoVersion))
				.leftJoin(diplome).on(vLien.idDiplome.eq(diplome.idComposant))
				.leftJoin(vDiplomeVersionMax).on(vLien.idDiplome.eq(vDiplomeVersionMax.idDiplome).and(vDiplomeVersionMax.annee.eq(annee)))
				.leftJoin(statutFormation).on(diplome.dipStatutFormationId.eq(statutFormation.idStatutFormation))
				.leftJoin(creditable).on(composant.idComposant.eq(creditable.composantId))
				.leftJoin(disciplinesComposant).on(composant.idComposant.eq(disciplinesComposant.composantId))
				.leftJoin(discipline).on(disciplinesComposant.disciplineId.eq(discipline.idDiscipline))
				.leftJoin(structuresComposant).on(composant.idComposant.eq(structuresComposant.composantId))
				.leftJoin(structureUlr).on(structuresComposant.cStructure.eq(structureUlr.cStructure))
				.leftJoin(implantationsComp).on(composant.idComposant.eq(implantationsComp.composantId))
				.leftJoin(implantationGeo).on(implantationsComp.imgeoOrdre.eq(implantationGeo.imgeoOrdre))
				.leftJoin(composantParent).on(vLien.parentId.eq(composantParent.idComposant))
				.where(typeComposant.nom.in(nomsTypes)
					.and(composantInfoVersion.annee.eq(annee))
					.and(infoVersionVersionDiplome.idInfoVersion.isNull()
							.or(infoVersionVersionDiplome.annee.eq(annee)
									.and(infoVersionVersionDiplome.versionMajeure.isNull().or(infoVersionVersionDiplome.versionMajeure.eq(vDiplomeVersionMax.versionMajeure)))))
					.and(diplome.idComposant.isNull().or(predicatHabilitation(isEnModelisation)))
					.and(predicatePerimetresStrategies))
				.orderBy(composant.code.asc());

		return query;
	}
	
	private Predicate predicatHabilitation(Boolean isEnModelisation) {
		Predicate predicat;
		if (isEnModelisation) {
			predicat = diplome.dipHabilitationRequise.eq(1);
		} else {
			predicat = statutFormation.code.in(EOStatutFormation.STATUT_EP_CODE, EOStatutFormation.STATUT_F_CODE)
							.or(diplome.dipHabilitationRequise.eq(0));
		}
		return predicat;
	}
	
}
