package org.cocktail.scol.maquette;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


public class ComposantRead {
	
	public static final String TYPE_COMPOSANT_KEY = "typeComposant";
	public static final String CODE_KEY = "code";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_KEY = "libelleLong";
	public static final String CREDIT_ECTS_KEY = "creditableCreditECTS";
	public static final String CREDIT_ECTS_NUMBER_KEY = "creditableCreditECTSNumber";

	private Long id;
	private String typeComposant;
	private String code;
	private String libelleCourt;
	private String libelleLong;
	private String creditableCreditECTS;
	private BigDecimal creditableCreditECTSNumber;
	private BigDecimal creditableTravailEtudiant;
	private Long creditableBaseNote;
	private Long libre;
	private Long parcoursType;
	
	private List<String> disciplines;
	private List<String> composantsParent;
	private List<String> structures;
	private List<String> implantations;
	
	public ComposantRead() {
		super();
	}
	
	public ComposantRead(Long id) {
		super();
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTypeComposant() {
		return typeComposant;
	}
	
	public void setTypeComposant(String typeComposant) {
		this.typeComposant = typeComposant;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getLibelleCourt() {
		return libelleCourt;
	}

	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}
	
	public String getLibelleLong() {
		return libelleLong;
	}

	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}
	
	public String getCreditableCreditECTS() {
		return creditableCreditECTS;
	}
	
	public void setCreditableCreditECTS(String creditableCreditECTS) {
		this.creditableCreditECTS = creditableCreditECTS;
	}
	
	public BigDecimal getCreditableCreditECTSNumber() {
		return creditableCreditECTSNumber;
	}
	
	public void setCreditableCreditECTSNumber(String creditECTS) {
		if (creditECTS!=null) {
			BigDecimal creditECTSTemp = new BigDecimal(creditECTS.replace(",", "."));
			creditableCreditECTSNumber = creditECTSTemp.setScale(2, RoundingMode.CEILING);
		}
	}

	public BigDecimal getCreditableTravailEtudiant() {
		return creditableTravailEtudiant;
	}

	public void setCreditableTravailEtudiant(BigDecimal creditableTravailEtudiant) {
		this.creditableTravailEtudiant = creditableTravailEtudiant;
	}
	
	public Long getCreditableBaseNote() {
		return creditableBaseNote;
	}

	public void setCreditableBaseNote(Long creditableBaseNote) {
		this.creditableBaseNote = creditableBaseNote;
	}
	
	public Long getLibre() {
		return libre;
	}
	
	public void setLibre(Long libre) {
		this.libre = libre;
	}
	
	public Boolean isLibre() {
		return ((libre!=null) &&(libre == 1));
	}

	public Long getParcoursType() {
		return parcoursType;
	}

	public void setParcoursType(Long parcoursType) {
		this.parcoursType = parcoursType;
	}
	
	public Boolean isParcoursType() {
		return ((parcoursType!=null) &&(parcoursType == 1));
	}
	
	public boolean hasParents() {
		return composantsParent != null && !composantsParent.isEmpty() && !composantsParent.get(0).equals(" - ");
	}
	
	public List<String> getComposantsParent() {
		return composantsParent;
	}
	
	public void setComposantsParent(List<String> composantsParent) {
		this.composantsParent = composantsParent;
	}
	
	public List<String> getDisciplines() {
		return disciplines;
	}
	
	public void setDisciplines(List<String> disciplines) {
		this.disciplines = disciplines;
	}
	
	public List<String> getImplantations() {
		return implantations;
	}
	
	public void setImplantations(List<String> implantations) {
		this.implantations = implantations;
	}
	
	public List<String> getStructures() {
		return structures;
	}
	
	public void setStructures(List<String> structures) {
		this.structures = structures;
	}
	
}
