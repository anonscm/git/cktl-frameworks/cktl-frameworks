package org.cocktail.scol.maquette;

import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QGradeUniversitaire;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;

import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.MappingProjection;

/**
 * Classe de mapping entre un tuple QueryDSL et le bean {@link LienConsultationDiplomeRead}.
 */
public class LienConsultationDiplomeReadMapping extends MappingProjection<LienConsultationDiplomeRead> {
	private LienConsultationReadMapping lienConsultationReadMapping;

	/**
	 * @param paths les valeurs ramenées par la requetes
	 * @param tComposantParent valeur du composant parent
	 * @param tComposant valeur du composant
	 */
	public LienConsultationDiplomeReadMapping(Expression<?>[] paths, QComposant tComposantParent, QComposant tComposant) {
		super(LienConsultationDiplomeRead.class, paths);
		setLienConsultationReadMapping(new LienConsultationReadMapping(paths, tComposantParent, tComposant));
	}

	public LienConsultationReadMapping getLienConsultationReadMapping() {
		return lienConsultationReadMapping;
	}

	public void setLienConsultationReadMapping(LienConsultationReadMapping lienConsultationReadMapping) {
		this.lienConsultationReadMapping = lienConsultationReadMapping;
	}

	@Override
	protected LienConsultationDiplomeRead map(Tuple row) {
		LienConsultationDiplomeRead lienConsultationDiplomeRead = new LienConsultationDiplomeRead();
		lienConsultationDiplomeRead.setLienConsultationRead(getLienConsultationReadMapping().map(row));
		lienConsultationDiplomeRead.setLibelleGradeUniversitaire(row.get(QGradeUniversitaire.gradeUniversitaire.libelle));
		return lienConsultationDiplomeRead;
	}
}
