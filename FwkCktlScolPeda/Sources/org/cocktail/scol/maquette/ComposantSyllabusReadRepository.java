package org.cocktail.scol.maquette;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.scol.maquette.ComposantSyllabusRead.Descriptif;
import org.cocktail.scol.support.QueryDslHelper;
import org.cocktail.scol.support.oracle.OracleQueryExecutor;
import org.cocktail.scol.support.oracle.PriorExpression;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QDescriptif;
import org.cocktail.scol.support.q.sco_scolarite.QLien;
import org.cocktail.scol.support.q.sco_scolarite.QSelectionLienDescriptif;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QTypeDescriptifdestination;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.sql.oracle.OracleQuery;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.path.NumberPath;

/**
 * 
 * Service de récupération des informations sur le syllabus des composants.
 * 
 * @author Alexis Tual
 *
 */
public class ComposantSyllabusReadRepository {

	private QLien l = QLien.lien;
	private QComposant c = QComposant.composant;
	private QTypeComposant t = QTypeComposant.typeComposant;
	private QDescriptif desc = QDescriptif.descriptif;
	private PriorExpression<Long> priorLienParent = new PriorExpression<Long>(Long.class, l.idLien);
	private QTypeDescriptifdestination dest = QTypeDescriptifdestination.typeDescriptifdestination1;
	private QSelectionLienDescriptif selectDesc = QSelectionLienDescriptif.selectionLienDescriptif;
	
	private OracleQueryExecutor oracleQueryExecutor;
	
	private static final Ordering<ComposantSyllabusRead> orderByLienOrdre = new Ordering<ComposantSyllabusRead>() {
		@Override
		public int compare(ComposantSyllabusRead left, ComposantSyllabusRead right) {
			return Ordering.natural().nullsLast().compare(left.getOrdreLien(), right.getOrdreLien());
		}
	};
	
	@Autowired
	public ComposantSyllabusReadRepository(OracleQueryExecutor oracleQueryExecutor) {
		this.oracleQueryExecutor = oracleQueryExecutor;
	}
	
	/**
	 * @param idLienVersionDiplome l'id lien entre le diplome et la version de diplome
	 * @return le composant avec le descriptif du syllabus
	 */
	public ComposantSyllabusRead getDiplomeComposantSyllabusRead(Long idLienVersionDiplome) {
		List<Tuple> resultats = fetchComposantSyllabus(idLienVersionDiplome, true);
		ComposantSyllabusRead composant = populate(resultats);
		return composant;
	}

	/**
	 * @param idLien l'id lien dont on veut récupérer le syllabus du composant fils
	 * @return le composant avec le descriptif du syllabus
	 */
	public ComposantSyllabusRead getComposantSyllabusRead(Long idLien) {
		List<Tuple> resultats = fetchComposantSyllabus(idLien, false);
		ComposantSyllabusRead composant = populate(resultats);
		return composant;
	}
	
	private List<Tuple> fetchComposantSyllabus(Long idLien, boolean isDiplome) {
		NumberPath<Long> parentOrChild = isDiplome ? l.parentId : l.childId;
		OracleQuery query = oracleQueryExecutor.newQuery()
				.from(l)
				.join(c).on(parentOrChild.eq(c.idComposant))
				.leftJoin(desc).on(parentOrChild.eq(desc.composantId))
				.leftJoin(dest).on(desc.destinationId.eq(dest.idTypeDescriptifdestination))
				.leftJoin(selectDesc).on(l.idLien.eq(selectDesc.lienId))
			    .where(l.idLien.eq(idLien));
		QTuple select = new QTuple(l.idLien, c.idComposant, l.ordre, c.llComposant, c.typeComposantId, desc.idDescriptif, desc.versionDescriptif, dest.typeDescriptifdestination, selectDesc.descriptifId);
		List<Tuple> resultats = oracleQueryExecutor.query(query, select);
		return resultats;
	}

	private ComposantSyllabusRead populate(List<Tuple> resultats) {
		Map<Long, List<Descriptif>> descriptifsByIdLien = QueryDslHelper.flatten(resultats, l.idLien, descriptifsCollector());
		Map<Long, List<Long>> selectedDescriptifsByIdLien = QueryDslHelper.flatten(resultats, l.idLien, selectDesc.descriptifId);
		List<ComposantSyllabusRead> composants = tuplesToComposants(resultats, descriptifsByIdLien, selectedDescriptifsByIdLien, null);
		ComposantSyllabusRead composant = null;
		if (composants.size() == 1) {
			composant = composants.get(0);
			composant.setChildren(new ArrayList<ComposantSyllabusRead>());
		}
		return composant;
	}
	
	/**
	 * @param idLien le lien parent
	 * @return les lien fils avec composant fils, descriptifs syllabus
	 */
	public List<ComposantSyllabusRead> getComposantsSyllabusRead(Long idLien) {
		OracleQuery query = oracleQueryExecutor.newQuery()
				.from(l)
				.join(c).on(l.childId.eq(c.idComposant))
				.join(t).on(c.typeComposantId.eq(t.idTypeComposant))
				.leftJoin(desc).on(l.childId.eq(desc.composantId))
				.leftJoin(dest).on(desc.destinationId.eq(dest.idTypeDescriptifdestination))
				.leftJoin(selectDesc).on(l.idLien.eq(selectDesc.lienId))
					.connectByPrior(l.childId.eq(l.parentId))
					.startWith(l.idLien.eq(idLien))
			    .where(priorLienParent.isNotNull().and(t.nom.notIn(ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAP_NOM)));
		return fetchComposantsSyllabusRead(idLien, query);
	}
	
	private List<ComposantSyllabusRead> fetchComposantsSyllabusRead(
			Long idLien, OracleQuery query) {
		QTuple select = new QTuple(l.idLien,  c.idComposant, l.ordre, priorLienParent, c.llComposant, c.typeComposantId, desc.idDescriptif, desc.versionDescriptif, dest.typeDescriptifdestination, selectDesc.descriptifId);
		List<Tuple> resultats = oracleQueryExecutor.query(query, select);
		Map<Long, List<Descriptif>> descriptifsByIdLien = QueryDslHelper.flatten(resultats, l.idLien, descriptifsCollector());
		Map<Long, List<Long>> selectedDescriptifsByIdLien = QueryDslHelper.flatten(resultats, l.idLien, selectDesc.descriptifId);
		Multimap<Long, Tuple> tuplesByIdParent = Multimaps.index(resultats, byIdParent());
		return tuplesToComposants(idLien, resultats, descriptifsByIdLien, selectedDescriptifsByIdLien, tuplesByIdParent);
	}

	private Function<Tuple, Long> byIdParent() {
		return new Function<Tuple, Long>() {
			public Long apply(Tuple input) {
				 Long idLienParent = input.get(priorLienParent);
				 return idLienParent != null ? idLienParent : -1;
			}
		};
	}

	private Function<Tuple, Descriptif> descriptifsCollector() {
		return new Function<Tuple, ComposantSyllabusRead.Descriptif>() {
			
			public Descriptif apply(Tuple tuple) {
				return new Descriptif(tuple.get(desc.idDescriptif), tuple.get(dest.typeDescriptifdestination), tuple.get(desc.versionDescriptif), tuple.get(desc.annee));
			}
		};
	}

	private List<ComposantSyllabusRead> tuplesToComposants(
			Long idLienRacine, Collection<Tuple> resultats, 
			Map<Long, List<Descriptif>> descriptifsByIdLien, 
			Map<Long, List<Long>> selectedDescriptifsByIdLien, 
			Multimap<Long, Tuple> tuplesByIdParent) {
		Collection<Tuple> enfantsDirectRacine = tuplesByIdParent.get(idLienRacine);
		List<ComposantSyllabusRead> composants = tuplesToComposants(enfantsDirectRacine, descriptifsByIdLien, selectedDescriptifsByIdLien, tuplesByIdParent);
		return orderByLienOrdre.sortedCopy(composants);
	}

	private List<ComposantSyllabusRead> tuplesToComposants(
			Collection<Tuple> resultats,
			Map<Long, List<Descriptif>> descriptifsByIdLien,
			Map<Long, List<Long>> selectedDescriptifsByIdLien,
			Multimap<Long, Tuple> tuplesByIdParent) {
		List<ComposantSyllabusRead> composants = new ArrayList<ComposantSyllabusRead>();
		HashSet<Long> idsTraites = new HashSet<Long>();
		for (Tuple tuple : resultats) {
			Long id = tuple.get(l.idLien);
			if (!idsTraites.contains(id)) {
				ComposantSyllabusRead composantSyllabusRead = tupleToComposant(tuple);
				if (tuplesByIdParent != null) {
					Collection<Tuple> children = tuplesByIdParent.get(id);
					if (children != null && !children.isEmpty()) {
						List<ComposantSyllabusRead> childrenComposants = tuplesToComposants(children, descriptifsByIdLien, selectedDescriptifsByIdLien, tuplesByIdParent);
						composantSyllabusRead.setChildren(orderByLienOrdre.sortedCopy(childrenComposants));
					}
				}
				if (descriptifsByIdLien != null) {
					Set<Descriptif> descriptifs = Sets.newHashSet(descriptifsByIdLien.get(id));
					composantSyllabusRead.setDescriptifs(descriptifs);
				}
				if (selectedDescriptifsByIdLien != null) {
					List<Long> selectedDescriptifs = selectedDescriptifsByIdLien.get(id);
					composantSyllabusRead.setSelectedDescriptifs(selectedDescriptifs);
				}
				composants.add(composantSyllabusRead);
				idsTraites.add(id);
			}
		}
		return composants;
	}

	private ComposantSyllabusRead tupleToComposant(Tuple tuple) {
		Long idLien = tuple.get(l.idLien);
		Long idComposant = tuple.get(c.idComposant);
		String libelle = tuple.get(c.llComposant);
		Long ordreLien = tuple.get(l.ordre);
		Long typeComposantId = tuple.get(c.typeComposantId);
		ComposantSyllabusRead composantSyllabusRead = new ComposantSyllabusRead(idLien, idComposant, typeComposantId, libelle, ordreLien);
		return composantSyllabusRead;
	}
	
}
