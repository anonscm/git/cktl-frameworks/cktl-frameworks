package org.cocktail.scol.maquette;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.mysema.query.types.Predicate;

public class PerimetreStrategieReadService {

	private QueryDslJdbcTemplate template;
	private QComposant composant = QComposant.composant;
	
	
	public PerimetreStrategieReadService(QueryDslJdbcTemplate template) {
		this.template = template;
	}
			
	public Predicate getPredicates(List<ComposantRead> allComposants) {		
		Predicate predicat = null;

		if(allComposants.size()>0) {
			List<Long> composants = new ArrayList<Long>();
			
			for(ComposantRead composantRead : allComposants) {
				composants.add(composantRead.getId());
			}
			
			predicat = composant.idComposant.in(composants);
		}
		
		return predicat;
	}

}
