package org.cocktail.scol.maquette;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienConsultation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QComposant;
import org.cocktail.scol.support.q.sco_scolarite.QComposantInfoVersion;
import org.cocktail.scol.support.q.sco_scolarite.QExamen;
import org.cocktail.scol.support.q.sco_scolarite.QExamenEtat;
import org.cocktail.scol.support.q.sco_scolarite.QSessionPeriode;
import org.cocktail.scol.support.q.sco_scolarite.QStatutFormation;
import org.cocktail.scol.support.q.sco_scolarite.QTypeAe;
import org.cocktail.scol.support.q.sco_scolarite.QTypeComposant;
import org.cocktail.scol.support.q.sco_scolarite.QVDiplomeVersionMax;
import org.cocktail.scol.support.q.sco_scolarite.QVLien;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.query.QueryDslJdbcTemplate;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;

/**
 * Renvoie une liste de liens dont les enfants sont des AEs
 */
public class AEReadRepository {
	private QueryDslJdbcTemplate template;
	private QVLien vLien = QVLien.vLien;
	private QTypeAe typeAE = QTypeAe.typeAe;
	private QComposant composant = QComposant.composant;
	private QComposant versionDiplome = new QComposant("versionDiplome");
	private QComposant diplome = new QComposant("diplome");
	private QStatutFormation statutFormation = QStatutFormation.statutFormation;
	private QComposantInfoVersion composantInfoVersion = QComposantInfoVersion.composantInfoVersion;
	private QTypeComposant typeComposant = QTypeComposant.typeComposant;
	private QVDiplomeVersionMax vDiplomeVersionMax = QVDiplomeVersionMax.vDiplomeVersionMax;
	private QExamen examen = QExamen.examen;
	private QExamenEtat examenEtat = QExamenEtat.examenEtat;
	private QSessionPeriode sessionPeriode = QSessionPeriode.sessionPeriode;
	private QComposantInfoVersion infoVersionVersionDiplome = new QComposantInfoVersion("infoVersionVersionDiplome");

	@Autowired
	public AEReadRepository(QueryDslJdbcTemplate template) {
		this.template = template;
	}

	/**
	 * @param annee annee pour laquelle on cherche les liens
	 * @param isEnModelisation <code>true</code> si on recherche les composants en modelisation, <code>false</code> si on recherche les composants en maquettage
	 * @return liste des composants de type AE
	 */
	public List<AERead> getAEs(Long annee, boolean isEnModelisation) {
		SQLQuery query = template
		    .newSqlQuery()
		    .distinct()
		    .from(composant)
		    .innerJoin(typeComposant)
		    .on(composant.typeComposantId.eq(typeComposant.idTypeComposant))
		    .innerJoin(typeAE)
		    .on(composant.aeTypeId.eq(typeAE.idTypeAe))
		    .leftJoin(composantInfoVersion)
		    .on(composant.infoVersionId.eq(composantInfoVersion.idInfoVersion))
		    .leftJoin(vLien)
		    .on(composant.idComposant.eq(vLien.childId))
		    .leftJoin(versionDiplome)
		    .on(vLien.idVersionDiplome.eq(versionDiplome.idComposant))
		    .leftJoin(infoVersionVersionDiplome)
		    .on(versionDiplome.infoVersionId.eq(infoVersionVersionDiplome.idInfoVersion))
		    .leftJoin(diplome)
		    .on(vLien.idDiplome.eq(diplome.idComposant))
		    .leftJoin(vDiplomeVersionMax)
		    .on(vLien.idDiplome.eq(vDiplomeVersionMax.idDiplome).and(vDiplomeVersionMax.annee.eq(annee)))
		    .leftJoin(statutFormation)
		    .on(diplome.dipStatutFormationId.eq(statutFormation.idStatutFormation))
		    .where(
		        typeComposant.nom
		            .eq(ITypeComposant.TYPEAE_NOM)
		            .and(composantInfoVersion.annee.eq(annee))
		            .and(
		                infoVersionVersionDiplome.idInfoVersion.isNull()
		                    .or(infoVersionVersionDiplome.annee.eq(annee)
		                        .and(
		                            infoVersionVersionDiplome.versionMajeure.isNull().or(
		                                infoVersionVersionDiplome.versionMajeure.eq(vDiplomeVersionMax.versionMajeure)))))
		            .and(diplome.idComposant.isNull().or(predicatHabilitation(composant, statutFormation, isEnModelisation))));

		ConstructorExpression<AERead> mapping = ConstructorExpression.create(AERead.class, composant.idComposant, typeAE.code, composant.llComposant);
		List<AERead> res = template.query(query, mapping);
		return res;
	}

	/**
	 * @param idLienParent id du lien parent des AEs recherches
	 * @return liste des AEs qui sont sous l'arborescence du lien parent et qui sont soit à programmer soit en cours de programmation
	 */
	public List<AERead> getListeAEsAProgrammerOuEnCoursFromLienParent(Long idLienParent) {
		List<AERead> aeReads = getListeAEsFromLienParent(idLienParent);
		return getListeAEsAProgrammerOuEnCours(getListeAEIds(aeReads));
	}

	/**
	 * @param idComposant id du lien recherche
	 * @return liste contenant le composant si il est à programmer ou en cours de programmation
	 */
	public List<AERead> getListeAEfromAEIdAProgramme(Long idComposant) {
		List<Long> aeIds = Lists.newArrayList(idComposant);
		return getListeAEsAProgrammerOuEnCours(aeIds);
	}

	private List<AERead> getListeAEsAProgrammerOuEnCours(List<Long> aeReadIds) {

		SQLQuery query = template.newSqlQuery()
		    .from(composant)
		    .innerJoin(typeAE).on(composant.aeTypeId.eq(typeAE.idTypeAe))
		    .leftJoin(examen).on(composant.idComposant.eq(examen.idComposantAe))
		    .leftJoin(examenEtat).on(examen.idExamenEtat.eq(examenEtat.idExamenEtat))
		    .leftJoin(sessionPeriode).on(examen.idSessionPeriode.eq(sessionPeriode.idSessionPeriode))
		    .where(composant.idComposant.in(aeReadIds))
		    .distinct();

		ConstructorExpression<AERead> mapping = ConstructorExpression.create(AERead.class,
		    composant.idComposant, typeAE.code, composant.llComposant, examen.idExamen, examenEtat.code, examenEtat.libelle, sessionPeriode.libelle);
		List<AERead> res = template.query(query, mapping);
		return res;
	}

  

	private List<AERead> getListeAEsFromLienParent(Long idLienParent) {
		SQLQuery query = template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(composant).on(vLien.childId.eq(composant.idComposant))
		    .innerJoin(typeAE).on(composant.aeTypeId.eq(typeAE.idTypeAe))
		    .where(vLien.idLienContexte.contains(idLienParent.toString()))
		    .distinct();

		ConstructorExpression<AERead> mapping = ConstructorExpression.create(AERead.class,
		    vLien.childId, typeAE.code, composant.llComposant);
		List<AERead> res = template.query(query, mapping);
		return res;
	}

	private List<Long> getListeAEIds(List<AERead> aeReads) {
		return FluentIterable.from(aeReads).transform(new Function<AERead, Long>() {
			public Long apply(AERead aeRead) {
				return aeRead.getIdComposant();
			}
		}).toList();
	}

	/**
	 * @param idDiplome id du diplôme
	 * @param idLienParent id du lien parent des AEs recherches
	 * @return liste des AEs qui sont sous l'arborescence du lien parent et qui ont un examen organisé
	 */
	public List<AERead> getAEsAvecExamen(long idDiplome, Long idLienParent) {
		SQLQuery query = template.newSqlQuery()
		    .from(vLien)
		    .innerJoin(composant).on(vLien.childId.eq(composant.idComposant))
		    .innerJoin(typeAE).on(composant.aeTypeId.eq(typeAE.idTypeAe))
		    .innerJoin(examen).on(vLien.childId.eq(examen.idComposantAe))
		    .innerJoin(examenEtat).on(examen.idExamenEtat.eq(examenEtat.idExamenEtat))
		    .innerJoin(sessionPeriode).on(sessionPeriode.idSessionPeriode.eq(examen.idSessionPeriode))
		    .where(vLien.typeComposantNomChild.eq(ITypeComposant.TYPEAE_NOM)
		        .and(vLien.idDiplome.eq(idDiplome))
		        .and(vLien.idLienContexte.contains(ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR + idLienParent + ILienConsultation.ID_LIEN_CONTEXTE_SEPERATOR)))
		    .distinct();

		ConstructorExpression<AERead> mapping = ConstructorExpression.create(AERead.class,
		    vLien.childId, typeAE.code, composant.llComposant, examen.idExamen, examenEtat.code, examenEtat.libelle, sessionPeriode.libelle);
		List<AERead> res = template.query(query, mapping);
		return res;
	}

	/**
	 * @param idComposant id de l'AE recherche
	 * @return liste contenant le composant en question et qui ont un examen organisé
	 */
	public List<AERead> getAEsAvecExamenfromAEId(Long idComposant) {
		SQLQuery query = template.newSqlQuery()
		    .from(composant)
		    .innerJoin(typeAE).on(composant.aeTypeId.eq(typeAE.idTypeAe))
		    .innerJoin(examen).on(composant.idComposant.eq(examen.idComposantAe))
		    .innerJoin(examenEtat).on(examen.idExamenEtat.eq(examenEtat.idExamenEtat))
		    .innerJoin(sessionPeriode).on(sessionPeriode.idSessionPeriode.eq(examen.idSessionPeriode))
		    .where(composant.idComposant.eq(idComposant))
		    .distinct();

		ConstructorExpression<AERead> mapping = ConstructorExpression.create(AERead.class,
		    composant.idComposant, typeAE.code, composant.llComposant, examen.idExamen, examenEtat.code, examenEtat.libelle, sessionPeriode.libelle);
		List<AERead> res = template.query(query, mapping);
		return res;
	}

	private Predicate predicatHabilitation(QComposant diplome, QStatutFormation statutFormation, Boolean isEnModelisation) {
		Predicate predicat;
		if (isEnModelisation) {
			predicat = diplome.dipHabilitationRequise.eq(1);
		} else {
			predicat = statutFormation.code.in(EOStatutFormation.STATUT_EP_CODE, EOStatutFormation.STATUT_F_CODE)
			    .or(diplome.dipHabilitationRequise.eq(0));
		}
		return predicat;
	}
}
