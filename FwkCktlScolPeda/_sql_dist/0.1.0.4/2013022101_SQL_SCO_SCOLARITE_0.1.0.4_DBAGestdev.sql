-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 27/02/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/3
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.4
-- Date de publication : 27/02/2013
-- Auteur(s) : Association Cocktail
--
--
 
 -- activation d'affichage
set serveroutput on feedback on

-- CREATION DU USER
DROP USER SCO_SCOLARITE CASCADE;
CREATE USER SCO_SCOLARITE IDENTIFIED BY bdlmmdesco_scolarite
DEFAULT TABLESPACE DATA_CKTL_SCOL TEMPORARY TABLESPACE TEMP quota unlimited on DATA_CKTL_SCOL;
ALTER USER SCO_SCOLARITE quota unlimited on INDX_CKTL_SCOL;

GRANT CONNECT, RESOURCE TO SCO_SCOLARITE;
GRANT select, references on GRHUM.PERSONNE to SCO_SCOLARITE;
GRANT select, references on GRHUM.ETUDIANT to SCO_SCOLARITE;
GRANT select, references on GRHUM.LANGUE to SCO_SCOLARITE;
GRANT select, references on GRHUM.IMPLANTATION_GEO to SCO_SCOLARITE;
GRANT select, references on GRHUM.STRUCTURE_ULR to SCO_SCOLARITE;
GRANT select, references on GRHUM.DISCIPLINE to SCO_SCOLARITE;
