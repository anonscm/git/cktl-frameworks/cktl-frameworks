-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 12/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/11
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.6
-- Date de publication : 12/04/2013
-- Auteur(s) : Association Cocktail
--
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------ MISE A JOUR DES FOREIGN KEY  ------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLE LIEN_COMPOSER
-- Supprime la contrainte FK_LIEN_COMPOSER_LIENID si elle existe et la recrée
declare
  is_constraint_exists int;
begin   
  select count(*) into is_constraint_exists from all_constraints where owner = 'SCO_SCOLARITE' and constraint_name = 'FK_LIEN_COMPOSER_LIENID';
  if (is_constraint_exists = 1) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.LIEN_COMPOSER DROP CONSTRAINT FK_LIEN_COMPOSER_LIENID');
  end if;
end;
/
ALTER TABLE SCO_SCOLARITE.LIEN_COMPOSER ADD CONSTRAINT FK_LIEN_COMPOSER_LIENID FOREIGN KEY (ID_LIEN_COMPOSER) REFERENCES SCO_SCOLARITE.LIEN (ID_LIEN) DEFERRABLE INITIALLY DEFERRED;

-- Supprime la séquence LIEN_COMPOSER_SEQ
declare
  is_sequence_exists int;
begin   
  select count(*) into is_sequence_exists from all_sequences where sequence_owner = 'SCO_SCOLARITE' and sequence_name = 'LIEN_COMPOSER_SEQ';
  if (is_sequence_exists = 1) then
    EXECUTE IMMEDIATE('DROP SEQUENCE SCO_SCOLARITE.LIEN_COMPOSER_SEQ');
  end if;
end;
/

-- TABLE COMPOSANT
declare
  is_column_exists int;
begin   
  select count(*) into is_column_exists from all_tab_cols where owner = 'SCO_SCOLARITE' and column_name='PERS_ID_RESPONSABLE' and table_name='COMPOSANT';
  if (is_column_exists = 0) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.COMPOSANT ADD PERS_ID_RESPONSABLE NUMBER');
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.COMPOSANT ADD CONSTRAINT FK_COMPOSANT_PIDRESP FOREIGN KEY (PERS_ID_RESPONSABLE) REFERENCES GRHUM.PERSONNE (PERS_ID) DEFERRABLE INITIALLY DEFERRED');
    EXECUTE IMMEDIATE('COMMENT ON COLUMN SCO_SCOLARITE.COMPOSANT.PERS_ID_RESPONSABLE IS ''Pers_Id du responsable du composant''');    
  end if;
end;
/

-- TABLE AP
UPDATE SCO_SCOLARITE.AP SET VALEUR_HEURES = null;
ALTER TABLE SCO_SCOLARITE.AP MODIFY VALEUR_HEURES NUMBER(38,2);
UPDATE SCO_SCOLARITE.AP SET VALEUR_TEMPS_ETUDIANT = null;
ALTER TABLE SCO_SCOLARITE.AP MODIFY VALEUR_TEMPS_ETUDIANT NUMBER(38,2);

-- TABLE AE
UPDATE SCO_SCOLARITE.AE SET DUREE_MIN = null;
ALTER TABLE SCO_SCOLARITE.AE MODIFY DUREE_MIN NUMBER(38,2);
UPDATE SCO_SCOLARITE.AE SET DUREE_MAX = null;
ALTER TABLE SCO_SCOLARITE.AE MODIFY DUREE_MAX NUMBER(38,2);

-- TABLE LIEN_COMPOSER
UPDATE SCO_SCOLARITE.LIEN_COMPOSER SET DUREE_MIN = null;
ALTER TABLE SCO_SCOLARITE.LIEN_COMPOSER MODIFY DUREE_MIN NUMBER(38,2);
UPDATE SCO_SCOLARITE.LIEN_COMPOSER SET DUREE_MAX = null;
ALTER TABLE SCO_SCOLARITE.LIEN_COMPOSER MODIFY DUREE_MAX NUMBER(38,2);

-- TABLE CREDITABLE
UPDATE SCO_SCOLARITE.CREDITABLE SET TRAVAIL_ETUDIANT = null;
ALTER TABLE SCO_SCOLARITE.CREDITABLE MODIFY TRAVAIL_ETUDIANT NUMBER(38,2);

