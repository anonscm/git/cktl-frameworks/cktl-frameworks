--
-- Patch DML de SCO_IMPORT du 12/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 6/11
-- Type : DML
-- Schema : SCO_IMPORT
-- Numero de version : 0.1.0.6
-- Date de publication : 12/04/2013
-- Auteur(s) : Association Cocktail
--
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- TABLE DB_VERSION
INSERT INTO SCO_IMPORT.DB_VERSION VALUES(SCO_IMPORT.DB_VERSION_SEQ.NEXTVAL, '0.1.0.6', '12/04/2013', SYSDATE,'Version alpha du user SCO_IMPORT');

COMMIT;
