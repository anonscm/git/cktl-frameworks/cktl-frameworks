--
-- Patch DML de SCO_IMPORT du 26/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : SCO_IMPORT
-- Numero de version : 0.1.0.12
-- Date de publication : 26/09/2013
-- Auteur(s) : Chama LAATIK

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-------------------------------V20130926.110605__DML_Version_user.sql-------------------------------


--DB_VERSION 
INSERT INTO SCO_IMPORT.DB_VERSION (DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(SCO_IMPORT.DB_VERSION_SEQ.NEXTVAL,'0.1.0.12',to_date('26/09/2013','DD/MM/YYYY'),sysdate,'TODO');
COMMIT;