delete from SCO_SCOLARITE.charge_enseignement;
delete from SCO_SCOLARITE.creditable;
delete from SCO_SCOLARITE.diplome_intermediaire;
delete from SCO_SCOLARITE.disciplines_composant;
delete from SCO_SCOLARITE.gd_perimetre_girofle;
delete from SCO_SCOLARITE.implantations_comp;
delete from SCO_SCOLARITE.langues_composant;
delete from SCO_SCOLARITE.structures_composant;
delete from SCO_SCOLARITE.descriptif_champ_contenu;
delete from SCO_SCOLARITE.descriptif_liensutiles;
delete from SCO_SCOLARITE.descriptif;
delete from SCO_SCOLARITE.formule;
delete from SCO_SCOLARITE.session_composant;

delete from SCO_SCOLARITE.composant;
delete from SCO_SCOLARITE.lien;
delete from SCO_SCOLARITE.habilitation;

commit;