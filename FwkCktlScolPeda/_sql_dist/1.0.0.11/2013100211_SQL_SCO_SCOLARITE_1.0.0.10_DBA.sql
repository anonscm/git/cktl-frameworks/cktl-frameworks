-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 02/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/3
-- Type : DBA
-- Schema : SCO_SCOLARITE
-- Numero de version : 1.0.0.10
-- Date de publication : 02/10/2013
-- Auteur(s) : Association Cocktail
--
--
 
 -- activation d'affichage
set serveroutput on feedback on

GRANT SELECT, INSERT, UPDATE, DELETE, references on GRHUM.GD_PERIMETRE to SCO_SCOLARITE;


