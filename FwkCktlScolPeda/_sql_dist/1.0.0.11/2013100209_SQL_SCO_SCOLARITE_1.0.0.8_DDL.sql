-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 02/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : SCO_SCOLARITE
-- Numero de version : 1.0.0.8
-- Date de publication : 02/10/2013
-- Auteur(s) : Association Cocktail
--
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- TABLE TYPE_ECHANGE
declare
  is_constraint_exists int;
  is_table_exists int;
  is_seq_exists int;
begin
  --suppression des contraintes de clés étrangères de la table Inscription (recrées plus tard dans ce script)
  select count(*) into is_constraint_exists from all_constraints where owner = 'SCO_SCOLARITE' and constraint_name = 'FK_PREINSC_ECHANGE';
  if(is_constraint_exists = 1) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.INSCRIPTION DROP CONSTRAINT FK_PREINSC_ECHANGE');
  end if;
  
  select count(*) into is_constraint_exists from all_constraints where owner = 'SCO_SCOLARITE' and constraint_name = 'FK_PREINSC_TYPE';
  if(is_constraint_exists = 1) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.INSCRIPTION DROP CONSTRAINT FK_PREINSC_TYPE');
  end if;
  
   select count(*) into is_constraint_exists from all_constraints where owner = 'SCO_SCOLARITE' and constraint_name = 'FK_PREINSC_DIPLOME';
  if(is_constraint_exists = 1) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.INSCRIPTION DROP CONSTRAINT FK_PREINSC_DIPLOME');
  end if;
  
   select count(*) into is_constraint_exists from all_constraints where owner = 'SCO_SCOLARITE' and constraint_name = 'FK_PREINSC_GRADE';
  if(is_constraint_exists = 1) then
    EXECUTE IMMEDIATE('ALTER TABLE SCO_SCOLARITE.INSCRIPTION DROP CONSTRAINT FK_PREINSC_GRADE');
  end if;
  
  select count(*) into is_table_exists from all_tables where owner = 'SCO_SCOLARITE' and table_name = 'TYPE_ECHANGE';
  if (is_table_exists = 1) then
    EXECUTE IMMEDIATE('DROP TABLE SCO_SCOLARITE.TYPE_ECHANGE');
  end if;

  select count(*) into is_seq_exists from all_sequences where sequence_name= 'TYPE_ECHANGE_SEQ';
  if (is_seq_exists = 1) then
    execute immediate('DROP SEQUENCE SCO_SCOLARITE.TYPE_ECHANGE_SEQ');
  end if;
end;
/

CREATE TABLE SCO_SCOLARITE.TYPE_ECHANGE (
	ID_TYPE_ECHANGE NUMBER(38) NOT NULL,
	LIBELLE VARCHAR2(100) NOT NULL,
	SENS VARCHAR2(1) DEFAULT 'A' NOT NULL,
	D_CREATION DATE  NOT NULL, 
	D_MODIFICATION DATE, 
	PERS_ID_CREATION NUMBER  NOT NULL, 
	PERS_ID_MODIFICATION NUMBER,
    CONSTRAINT PK_TYPE_ECHANGE PRIMARY KEY (ID_TYPE_ECHANGE) USING INDEX TABLESPACE INDX_CKTL_SCOL
);
CREATE SEQUENCE SCO_SCOLARITE.TYPE_ECHANGE_SEQ START WITH 13;

COMMENT ON TABLE SCO_SCOLARITE.TYPE_ECHANGE IS 'Table de nomenclature contenant les différents types d''échange international';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.ID_TYPE_ECHANGE IS 'ID de la table ';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.LIBELLE IS 'libelle du type d''échange internationnal';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.SENS IS 'Sens de l''échange (A: Arrivée, D: Départ)';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.D_CREATION IS 'Date de creation de lenregistrement.';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.D_MODIFICATION IS 'Date de la derniere modification de lenregistrement.';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_ECHANGE.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

-- TABLE TYPE_INSCRIPTION_FORMATION
declare
  is_table_exists int;
  is_seq_exists int;
begin   
  
  select count(*) into is_table_exists from all_tables where owner = 'SCO_SCOLARITE' and table_name = 'TYPE_INSCRIPTION_FORMATION';
  if (is_table_exists = 1) then
    EXECUTE IMMEDIATE('DROP TABLE SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION');
  end if;

  select count(*) into is_seq_exists from all_sequences where sequence_name= 'TYPE_INSCRIPTION_FORMATION_SEQ';
  if (is_seq_exists = 1) then
    execute immediate('DROP SEQUENCE SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION_SEQ');
  end if;
end;
/

CREATE TABLE SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION (
	ID_TYPE_INSCRIPTION_FORMATION NUMBER(38) NOT NULL,
	CODE_INSCRIPTION VARCHAR2(50) NOT NULL,
	LIBELLE VARCHAR2(50) NOT NULL,
	D_CREATION DATE  NOT NULL, 
	D_MODIFICATION DATE, 
	PERS_ID_CREATION NUMBER  NOT NULL, 
	PERS_ID_MODIFICATION NUMBER,
    CONSTRAINT PK_TYPE_INSCRIPTION_FORMATION PRIMARY KEY (ID_TYPE_INSCRIPTION_FORMATION) USING INDEX TABLESPACE INDX_CKTL_SCOL
);
CREATE SEQUENCE SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION_SEQ START WITH 7;

COMMENT ON TABLE SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION IS 'Table de nomenclature contenant les différents types d''inscription à une formation';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.ID_TYPE_INSCRIPTION_FORMATION IS 'ID de la table ';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.CODE_INSCRIPTION IS 'code du type d''inscription ';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.LIBELLE IS 'libelle du type d''échange international';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.D_CREATION IS 'Date de creation de ''lenregistrement.';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.D_MODIFICATION IS 'Date de la derniere modification de l''enregistrement.';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

-- INSCRIPTION
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'REDOUBLEMENT' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (REDOUBLEMENT NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.REDOUBLEMENT IS ''Indique si la formation correspond à un redoublement''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'REORIENTATIONT' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (REORIENTATIONT NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.REORIENTATIONT IS ''Indique si la formation correspond à une ré-orientation''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'PASSAGE_CONDITIONNEL' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (PASSAGE_CONDITIONNEL NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.PASSAGE_CONDITIONNEL IS ''Indique si la formation correspond à un passage conditionnel''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'CYCLE_AMENAGE' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (CYCLE_AMENAGE NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.CYCLE_AMENAGE IS ''Indique si pour la formation le cyle est aménagé''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'ID_TYPE_INSCRIPTION_FORMATION' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (ID_TYPE_INSCRIPTION_FORMATION NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.ID_TYPE_INSCRIPTION_FORMATION IS ''type de inscription correspond à id de la table SCO_SCOLARITE_TYPE_INSCRIPTION_FORMATION''';
  end if;
end;
/


declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'ID_TYPE_ECHANGE' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (ID_TYPE_ECHANGE NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.ID_TYPE_ECHANGE IS ''type inscription correspond à id de la table SCO_SCOLARITE_TYPE_ECHANGE''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'GRADE' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (GRADE NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.GRADE IS ''grade de la formation suivie''';
  end if;
end;
/

declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'NIVEAU' and table_name = 'INSCRIPTION';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD (NIVEAU NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.INSCRIPTION.NIVEAU IS ''niveau de la formation suivie''';
  end if;
end;
/

	ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD CONSTRAINT FK_PREINSC_DIPLOME FOREIGN KEY (ID_DIPLOME) REFERENCES SCO_SCOLARITE.DIPLOME DEFERRABLE INITIALLY DEFERRED;
	ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD CONSTRAINT FK_PREINSC_TYPE FOREIGN KEY (ID_TYPE_INSCRIPTION_FORMATION) REFERENCES SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION DEFERRABLE INITIALLY DEFERRED;
	ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD CONSTRAINT FK_PREINSC_ECHANGE FOREIGN KEY (ID_TYPE_ECHANGE) REFERENCES SCO_SCOLARITE.TYPE_ECHANGE DEFERRABLE INITIALLY DEFERRED;
	ALTER TABLE SCO_SCOLARITE.INSCRIPTION ADD CONSTRAINT FK_PREINSC_GRADE FOREIGN KEY (GRADE) REFERENCES SCO_SCOLARITE.GRADE_UNIVERSITAIRE DEFERRABLE INITIALLY DEFERRED;


-- COMPOSANT
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'TAG_APPLICATION' and table_name = 'COMPOSANT';
  if (is_column_exists  = 0) then
     execute immediate 'ALTER TABLE SCO_SCOLARITE.COMPOSANT ADD (TAG_APPLICATION VARCHAR2(50))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.COMPOSANT.TAG_APPLICATION IS ''Nom de l''''application (autre que Girofle) qui gère cet élément (null pour Girofle)''';
  end if;
end;
/

-- TABLE LIEN_COMPOSER
UPDATE SCO_SCOLARITE.LIEN_COMPOSER SET COEFFICIENT = null;
ALTER TABLE SCO_SCOLARITE.LIEN_COMPOSER MODIFY COEFFICIENT NUMBER(38,5);

-- TABLE CREDITABLE
UPDATE SCO_SCOLARITE.CREDITABLE SET CREDIT_ECTS = null;
ALTER TABLE SCO_SCOLARITE.CREDITABLE MODIFY CREDIT_ECTS NUMBER(38,5);

