-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 02/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/3
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 1.0.0.4
-- Date de publication : 02/10/2013
-- Auteur(s) : Association Cocktail
--
--
 
 -- activation d'affichage
set serveroutput on feedback on

declare

  dbn varchar2(255);
  ibn varchar2(255);
  ord_data varchar2(1024);
  ord_indx varchar2(1024);
  is_tbs int;
        
   c int;

begin  

-- Test d'existence du User Sco_Scolarite
   select count(*) into c from dba_users where username = upper('SCO_SCOLARITE');
   if c <> 0 then
      execute immediate '
      DROP USER SCO_SCOLARITE CASCADE
      ';
   end if;   
  
  -- test d’existence du tablespace DATA
  select count(*) into is_tbs from dba_tablespaces where tablespace_name = 'DATA_CKTL_SCOL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into dbn from dba_data_files where upper(tablespace_name) = 'DATA_GRHUM';
    -- construction de l’ordre a executer
    ord_data := 'create tablespace DATA_CKTL_SCOL datafile '||chr(39)||dbn||'/data_cktl_scol.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace DATA : '||ord_data);
    -- creation du tablespace DATA
    execute immediate ord_data;
  end if;
  
  -- test d’existence du tablespace INDX
  select count(*) into is_tbs from dba_tablespaces where upper(tablespace_name) = 'INDX_CKTL_SCOL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into ibn from dba_data_files where upper(tablespace_name) = 'INDX_GRHUM';
    -- construction de l’ordre a executer
    ord_indx := 'create tablespace INDX_CKTL_SCOL datafile '||chr(39)||ibn||'/indx_cktl_scol.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace INDEXES : '||ord_indx);
    -- creation du tablespace INDX
    execute immediate ord_indx;
  end if;
end;
/

-- CREATION DU USER
CREATE USER SCO_SCOLARITE IDENTIFIED BY bdlmmdesco_scolarite
DEFAULT TABLESPACE DATA_CKTL_SCOL TEMPORARY TABLESPACE TEMP quota unlimited on DATA_CKTL_SCOL;
ALTER USER SCO_SCOLARITE quota unlimited on INDX_CKTL_SCOL;

GRANT CONNECT, RESOURCE TO SCO_SCOLARITE;
GRANT select, references on GRHUM.PERSONNE to SCO_SCOLARITE;
GRANT select, references on GRHUM.ETUDIANT to SCO_SCOLARITE;
GRANT select, references on GRHUM.LANGUE to SCO_SCOLARITE;
GRANT select, references on GRHUM.IMPLANTATION_GEO to SCO_SCOLARITE;
GRANT select, references on GRHUM.STRUCTURE_ULR to SCO_SCOLARITE;
GRANT select, references on GRHUM.DISCIPLINE to SCO_SCOLARITE;
