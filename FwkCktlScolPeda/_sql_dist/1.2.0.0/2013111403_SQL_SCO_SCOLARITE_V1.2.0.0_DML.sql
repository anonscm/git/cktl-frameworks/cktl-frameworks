--
-- Patch DML de SCO_SCOLARITE du 14/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 1.2.0.0
-- Date de publication : 14/11/2013
-- Auteur(s) : Julien Lafourcade

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------V20130828.144324__DML_donnee_TypeEtudiant_TypeInscriptionPeda.sql------------------
-- TABLE SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA
INSERT INTO SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA(ID_TYPE_INSCRIPTION_PEDA, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA_SEQ.nextval, 'EXEMPTE', 'Exempté (sans note)');
INSERT INTO SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA(ID_TYPE_INSCRIPTION_PEDA, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA_SEQ.nextval, 'DEJA_ACQUIS', 'Déjà acquis');

-- TABLE SCO_SCOLARITE.TYPE_ETUDIANT
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'CL', 'Etudiant classique');
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'FO', 'Etudiant en Formation Continue');
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'AL', 'Auditeur Libre');
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'RD', 'Redoublant');
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'CH', 'Changement d''orientation');
INSERT INTO SCO_SCOLARITE.TYPE_ETUDIANT(ID_TYPE_ETUDIANT, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_ETUDIANT_SEQ.nextval, 'EI', 'Echange Internationnal');

------------------------V20130911.102846__DML_ajout_donne_type_insc_peda.sql------------------------
-- TABLE SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA
INSERT INTO SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA(ID_TYPE_INSCRIPTION_PEDA, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_INSCRIPTION_PEDA_SEQ.nextval, 'STANDARD', 'Standard (avec note)');

--------------------------V20130911.110740__DML_ajout_donne_type_note.sql---------------------------
-- TABLE SCO_SCOLARITE.TYPE_NOTE
INSERT INTO SCO_SCOLARITE.TYPE_NOTE(ID_TYPE_NOTE, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_NOTE_SEQ.nextval, 'SAISIE', 'Saisie');
INSERT INTO SCO_SCOLARITE.TYPE_NOTE(ID_TYPE_NOTE, CODE, LIBELLE) VALUES ( SCO_SCOLARITE.TYPE_NOTE_SEQ.nextval, 'CALCULEE', 'Calculée');

---------------------------V20131105.155047__DML_Donnees_Type_Periode.sql---------------------------
INSERT INTO SCO_SCOLARITE.TYPE_PERIODE(ID_TYPE_PERIODE, CODE, LIBELLE) VALUES (SCO_SCOLARITE.TYPE_PERIODE_SEQ.nextval, 'ANNEE', 'Année');
INSERT INTO SCO_SCOLARITE.TYPE_PERIODE(ID_TYPE_PERIODE, CODE, LIBELLE) VALUES (SCO_SCOLARITE.TYPE_PERIODE_SEQ.nextval, 'SEMESTRE', 'Semestre');

---------------------------V20131105.162710__DML_MAJ_Donnees_Periode.sql----------------------------
update sco_scolarite.periode pp set ordre = (
  select substr(c.ll_composant, length(c.ll_composant) , 1) from sco_scolarite.periode p 
    inner join sco_scolarite.composant c on c.id_composant = p.id_periode
    where p.id_periode = pp.id_periode
    -- test d'un nombre
    and LENGTH(TRIM(TRANSLATE (substr(c.ll_composant, length(c.ll_composant) , 1), ' +-.0123456789',' '))) is null
    )
  ;

update sco_scolarite.periode pp set id_type_periode = (select id_type_periode from sco_scolarite.type_periode where code = 'ANNEE')
 where pp.id_periode in (  
  select id_periode from sco_scolarite.periode p 
    inner join sco_scolarite.composant c on c.id_composant = p.id_periode
        where c.ll_composant not like 'Semestre%'
  );
  
update sco_scolarite.periode pp set id_type_periode = (select id_type_periode from sco_scolarite.type_periode where code = 'SEMESTRE')
 where pp.id_periode in (  
  select id_periode from sco_scolarite.periode p 
    inner join sco_scolarite.composant c on c.id_composant = p.id_periode
        where c.ll_composant like 'Semestre%'
  );
    
-----------------------V20131113.092336__DML_Modification_type_formation.sql------------------------
DECLARE

apprentissage_id integer;
formation_continue_id integer;
next_val integer;

BEGIN 

-- recuperation du nextVal de SCO_SCOLARITE.TYPE_FORMATION_SEQ
select SCO_SCOLARITE.TYPE_FORMATION_SEQ.NEXTVAL into next_val from dual;

-- recuperation de l'identifiant de l'apprentissage
SELECT id_type_formation INTO apprentissage_id FROM SCO_SCOLARITE.TYPE_FORMATION WHERE code LIKE 'AP';

-- recuperation de l'identifiant de la formation continue
SELECT id_type_formation INTO formation_continue_id FROM SCO_SCOLARITE.TYPE_FORMATION WHERE code LIKE 'FC';

-- insertion du nouveau type de formation
INSERT INTO SCO_SCOLARITE.TYPE_FORMATION (id_type_formation, code, libelle, habilitable)
VALUES (next_val, 'FC', 'Formation Courte', 0);

-- mise à jour des id des types de formation dans la table diplome
UPDATE SCO_SCOLARITE.DIPLOME SET TYPE_FORMATION_ID = next_val
WHERE TYPE_FORMATION_ID = apprentissage_id or TYPE_FORMATION_ID = formation_continue_id;

-- suppression dans la table type_formation
DELETE FROM SCO_SCOLARITE.TYPE_FORMATION
WHERE ID_TYPE_FORMATION = apprentissage_id or ID_TYPE_FORMATION = formation_continue_id;

END;
/
--------------------------V20131113.165840__DML_Update_HeureEquivalent.sql--------------------------
UPDATE SCO_SCOLARITE.TYPE_AP SET HEURE_EQUIVALENT=1.5 WHERE CODE='CM';
UPDATE SCO_SCOLARITE.TYPE_AP SET HEURE_EQUIVALENT=0.75 WHERE CODE='TP';


--DB_VERSION 
INSERT INTO SCO_SCOLARITE.DB_VERSION(DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(SCO_SCOLARITE.DB_VERSION_SEQ.NEXTVAL,'1.2.0.0',to_date('14/11/2013','DD/MM/YYYY'),sysdate,'ajout des tables pour Paprika + évolution du type de formation + maj heure equivalent');
COMMIT;
