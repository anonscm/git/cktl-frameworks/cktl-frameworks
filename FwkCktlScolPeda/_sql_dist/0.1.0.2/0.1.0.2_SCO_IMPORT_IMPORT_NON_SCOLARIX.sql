﻿CREATE OR REPLACE PROCEDURE  SCO_IMPORT.IMPORT_NON_SCOLARIX IS

BEGIN

	-- Introduction
	DBMS_OUTPUT.PUT_LINE('Démarrage de l importation des données...');

	-- Vidage des tables d'import
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_AP');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_LIEN');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_EC');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_UE');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_PARCOURS');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_DIPLOME');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_DISCIPLINE');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_DOMAINE');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_GRADE');   
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_SPECIALISATION');   
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_SPECIALITE');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_VOCATION'); 
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_MODE_AP');
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_TYPE_AP');
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_TYPE_FORMATION');  
	EXECUTE IMMEDIATE('TRUNCATE TABLE SCO_IMPORT.IMPORT_TYPE_ORGANISATION'); 
	DBMS_OUTPUT.PUT_LINE('Tables de SCO_IMPORT vidées');
  	COMMIT;

  	-- Insertion des données dans IMPORT_DISCIPLINE
  	EXECUTE IMMEDIATE('CREATE TABLE SCO_IMPORT.TMP_IMPORT_DISCIPLINE
  	(ID             NUMBER(38,0),
   	 CODE           VARCHAR2(255),
   	 KEY            NUMBER(12,0),
   	 LIBELLE        VARCHAR2(255),
   	 LIBELLE_COURT  VARCHAR2(255), 
   	 VALIDE         NUMBER(1,0))
   	 ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_APOGEE
   	 ACCESS PARAMETERS (records delimited by newline skip 1
                      	    badfile W_FILE_APOGEE : ''IMPORT_DISCIPLINE.bad''
                      	    logfile W_FILE_APOGEE : ''IMPORT_DISCIPLINE.log''
                      	    fields terminated by '';''
                      	    optionally enclosed by ''"''
                      	    missing field VALUES are NULL)
   	 LOCATION (''IMPORT_DISCIPLINE.csv''))');
  	DBMS_OUTPUT.PUT_LINE('Tables des Disciplines insérées');

  	-- Insertion des données dans IMPORT_DOMAINE
  	EXECUTE IMMEDIATE('CREATE TABLE SCO_IMPORT.TMP_IMPORT_DOMAINE
  	(ID             NUMBER(38,0),
   	 CODE           VARCHAR2(255),
   	 LIBELLE        VARCHAR2(255),
   	 LIBELLE_COURT  VARCHAR2(255),
   	 TYPE           VARCHAR2(255), 
   	 VALIDE         NUMBER(1,0))
   	 ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_APOGEE
   	 ACCESS PARAMETERS (records delimited by newline skip 1
                      	    badfile W_FILE_APOGEE : ''IMPORT_DOMAINE.bad''
                      	    logfile W_FILE_APOGEE : ''IMPORT_DOMAINE.log''
                      	    fields terminated by '';''
                      	    optionally enclosed by ''"''
                      	    missing field VALUES are NULL)
   	 LOCATION (''IMPORT_DOMAINE.csv''))');
  	DBMS_OUTPUT.PUT_LINE('Tables des Domaines insérées');

  	-- Insertion des données dans IMPORT_DOMAINE
  	EXECUTE IMMEDIATE('CREATE TABLE SCO_IMPORT.TMP_IMPORT_GRADE
  	(ID            NUMBER(38,0),
   	 CODE          VARCHAR2(255),
   	 LIBELLE       VARCHAR2(255),
   	 LIBELLE_COURT VARCHAR2(255),
   	 TYPE          VARCHAR2(255),
   	 VALIDE        NUMBER(1,0))
   	 ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_APOGEE
   	 ACCESS PARAMETERS (records delimited by newline skip 1
                      	    badfile W_FILE_APOGEE : ''IMPORT_GRADE.bad''
                      	    logfile W_FILE_APOGEE : ''IMPORT_GRADE.log''
                      	    fields terminated by '';''
                      	    optionally enclosed by ''"''
                      	    missing field VALUES are NULL)
   	 LOCATION (''IMPORT_GRADE.csv''))');
  	DBMS_OUTPUT.PUT_LINE('Tables des Grades insérées');
  
END;