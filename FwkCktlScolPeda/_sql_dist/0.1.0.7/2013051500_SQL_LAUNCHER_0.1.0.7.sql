--
-- Lanceur de tous les scripts SQL 15/05/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 0
-- Type : Lanceur
-- Schema : SCO_SCOLARITE - SCO_IMPORT
-- Numero de version : 0.1.0.7
-- Date de publication : 15/05/2013
-- Auteur(s) : Association Cocktail
--
--


START 2013051503_SQL_SCO_SCOLARITE_0.1.0.7_DML.sql
START 2013051504_SQL_SCO_IMPORT_0.1.0.7_DBA.sql
START 2013051505_SQL_SCO_IMPORT_0.1.0.7_DDL.sql
START 2013051506_SQL_SCO_IMPORT_0.1.0.7_DML.sql
START 2013051507_SQL_SCO_IMPORT_IMPORT_SCOLARIX_0.1.0.7_PLSQL.sql
START 2013051508_SQL_SCO_IMPORT_IMPORT_GIROFLE_0.1.0.7_PLSQL.sql

EXIT
