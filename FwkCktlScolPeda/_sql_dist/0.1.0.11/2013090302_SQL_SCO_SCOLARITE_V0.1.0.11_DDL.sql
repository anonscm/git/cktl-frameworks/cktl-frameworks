--
-- Patch DDL de SCO_SCOLARITE du 03/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.11
-- Date de publication : 03/09/2013
-- Auteur(s) : ldrouot

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------V20130821.135338__DDL_habilitationPourVersionning.sql------------------------
-- ajout d'une colonne HABILITATION à la table TYPE_FORMATION
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'HABILITABLE' and table_name = 'TYPE_FORMATION';
  if (is_column_exists  = 0) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.TYPE_FORMATION ADD (HABILITABLE NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.TYPE_FORMATION.HABILITABLE IS ''Vrai si ce type de formation peut être habilitable''';
  end if;
end;
/


-- ajout d'une colonne HABILITATION_REQUISE à la table DIPLOME
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'HABILITATION_REQUISE' and table_name = 'DIPLOME';
  if (is_column_exists  = 0) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.DIPLOME ADD (HABILITATION_REQUISE NUMBER(1) DEFAULT 0)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.DIPLOME.HABILITATION_REQUISE IS ''Vrai si ce diplome doit être habilité''';
  end if;
end;
/


---------------------------V20130821.142118__DDL_AjoutNumHabilitation.sql---------------------------
--  renomme dans la table HABILTATION la colonne NIVEAU_FORMATION en NUMERO_HABILTATION
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'NIVEAU_FORMATION' and table_name = 'HABILITATION';
  if (is_column_exists  = 1) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.HABILITATION RENAME COLUMN NIVEAU_FORMATION TO NUMERO_HABILITATION';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.HABILITATION.NUMERO_HABILITATION IS ''numéro d''''habilitation''';
  end if;
end;
/

--------------------------V20130903.145656__DDL_ajoutComposantOrigine.sql---------------------------
-- ajout d'une colonne COMPOSANT_ORIGINE_ID à la table COMPOSANT
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'COMPOSANT_ORIGINE_ID' and table_name = 'COMPOSANT';
  if (is_column_exists  = 0) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.COMPOSANT ADD (COMPOSANT_ORIGINE_ID NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.COMPOSANT.COMPOSANT_ORIGINE_ID IS ''Identifiant du composant d''''origine lorsque ce dernier a été dupliqué''';
  end if;
end;
/



