--
-- Lanceur de tous les scripts SQL 10/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 0
-- Type : Lanceur
-- Schema : SCO_SCOLARITE - SCO_IMPORT
-- Numero de version : 0.1.0.9
-- Date de publication : 10/06/2013
-- Auteur(s) : Association Cocktail
--
--
START 2013061004_SQL_SCO_IMPORT_0.1.0.9_DBA.sql
START 2013061005_SQL_SCO_IMPORT_0.1.0.9_DDL.sql
START 2013061006_SQL_SCO_IMPORT_0.1.0.9_DML.sql

START 2013061007_SQL_SCO_IMPORT_IMPORT_SCOLARIX_0.1.0.9_PLSQL.sql
START 2013061008_SQL_SCO_IMPORT_IMPORT_GIROFLE_0.1.0.9_PLSQL.sql

EXIT
