--
-- Lanceur de tous les scripts SQL 16/07/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 0
-- Type : Lanceur
-- Schema : SCO_SCOLARITE - SCO_IMPORT
-- Numero de version : 0.1.0.10
-- Date de publication : 16/07/2013
-- Auteur(s) : Association Cocktail
--
--

START 2013071601_SQL_SCO_SCOLARITE_0.1.0.10_DBA.sql
START 2013071602_SQL_SCO_SCOLARITE_0.1.0.10_DDL.sql
START 2013071603_SQL_SCO_SCOLARITE_0.1.0.10_DML.sql

EXIT
