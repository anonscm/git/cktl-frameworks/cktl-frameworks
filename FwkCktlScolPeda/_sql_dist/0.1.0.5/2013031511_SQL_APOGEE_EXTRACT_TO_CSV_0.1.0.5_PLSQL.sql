-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 15/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 10/11
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.5
-- Date de publication : 15/03/2013
-- Auteur(s) : Association Cocktail
--
--
create or replace 
procedure SCO_IMPORT.EXTRACT_TO_CSV(
SQLQUERY    IN VARCHAR2,
P_DIR       IN VARCHAR2,
P_FILENAME  IN VARCHAR2
) IS

  L_OUTPUT  UTL_FILE.FILE_TYPE;
  curid     NUMBER; 
  desctab   DBMS_SQL.DESC_TAB; 
  colcnt    NUMBER; 
  namevar   VARCHAR2(4000); 
  
BEGIN

  curid := DBMS_SQL.OPEN_CURSOR;
  L_OUTPUT := UTL_FILE.FOPEN (P_DIR, P_FILENAME, 'w');
  DBMS_SQL.PARSE(curid, SQLQUERY, dbms_sql.NATIVE);
  DBMS_SQL.DESCRIBE_COLUMNS(curid, colcnt, desctab);
  
  FOR i IN 1 .. colcnt LOOP
    DBMS_SQL.DEFINE_COLUMN(curid, i, namevar,4000);
  END LOOP;
  
  IF DBMS_SQL.execute(curid) = 0 THEN
    FOR i IN 1 .. colcnt LOOP
      IF (i>1) then
        UTL_FILE.PUT (L_OUTPUT, ';');
      end IF;
      UTL_FILE.PUT (L_OUTPUT, desctab(i).col_name);
      
    END LOOP;
    UTL_FILE.NEW_LINE (L_OUTPUT);
    
    WHILE DBMS_SQL.FETCH_ROWS(curid) > 0 LOOP
      FOR i IN 1 .. colcnt LOOP
        IF (i>1) then
          UTL_FILE.PUT (L_OUTPUT, ';');
        end IF;
        DBMS_SQL.COLUMN_VALUE(curid, i, namevar);
        
        IF (namevar IS NOT NULL) 
        then
          UTL_FILE.PUT (L_OUTPUT, '"' || namevar || '"');
        end IF;
      END LOOP;
      UTL_FILE.NEW_LINE (L_OUTPUT);
    END LOOP;
  END IF;
  
  UTL_FILE.FCLOSE (L_OUTPUT);

END;
/
