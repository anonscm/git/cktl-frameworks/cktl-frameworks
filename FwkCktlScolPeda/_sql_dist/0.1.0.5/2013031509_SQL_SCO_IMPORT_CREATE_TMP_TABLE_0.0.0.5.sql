-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 15/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 8/11
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.5
-- Date de publication : 15/03/2013
-- Auteur(s) : Association Cocktail
--
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

--SET SERVEROUTPUT ON
 
 declare 
 nbr_lgs int;
 
 begin

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_DISCIPLINE';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_DISCIPLINE');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_DISCIPLINE supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_DOMAINE';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_DOMAINE');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_DOMAINE supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_GRADE';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_GRADE');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_GRADE supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_SPECIALISATION';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_SPECIALISATION');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_SPECIALISATION supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_SPECIALITE';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_SPECIALITE');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_SPECIALITE supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_VOCATION';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_VOCATION');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_VOCATION supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_MODE_AP';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_MODE_AP');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_MODE_AP supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_TYPE_AP';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_TYPE_AP');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_TYPE_AP supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_AP';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_AP');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_AP supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_EC';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_EC');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_EC supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_UE';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_UE');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_UE supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_LIEN';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_LIEN');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_LIEN supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_PARCOURS';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_PARCOURS');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_PARCOURS supprimée.');
end if;

select count(*) into nbr_lgs from all_tables where owner = 'SCO_IMPORT' and table_name = 'TMP_IMPORT_DIPLOME';
if (nbr_lgs = 1) then
  EXECUTE IMMEDIATE('DROP TABLE SCO_IMPORT.TMP_IMPORT_DIPLOME');
  DBMS_OUTPUT.PUT_LINE('Table TMP_IMPORT_DIPLOME supprimée.');
end if;

end;
/

 -- Insertion des données dans IMPORT_DISCIPLINE
CREATE TABLE SCO_IMPORT.TMP_IMPORT_DISCIPLINE
(ID_DISCIPLINE  NUMBER(38,0),
  CODE   VARCHAR2(255),
  KEY NUMBER(12,0),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255), 
  VALIDE VARCHAR2(1))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_DISCIPLINE.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_DISCIPLINE.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_DISCIPLINE.csv'));

-- Insertion des données dans IMPORT_DOMAINE
CREATE TABLE SCO_IMPORT.TMP_IMPORT_DOMAINE
(ID_DOMAINE NUMBER(38,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  TYPE   VARCHAR2(255), 
  VALIDE NUMBER(1,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_DOMAINE.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_DOMAINE.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_DOMAINE.csv'));

-- Insertion des données dans IMPORT_GRADE
CREATE TABLE SCO_IMPORT.TMP_IMPORT_GRADE
(ID_GRADE  NUMBER(38,0),
  CODE  VARCHAR2(255),
  LIBELLE   VARCHAR2(255),
  LIBELLE_COURT VARCHAR2(255),
  TYPE  VARCHAR2(255),
  VALIDE NUMBER(1,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_GRADE.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_GRADE.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_GRADE.csv'));

-- Insertion des données dans IMPORT_SPECIALISATION
CREATE TABLE SCO_IMPORT.TMP_IMPORT_SPECIALISATION
(ID_SPECIALISATION  NUMBER(38,0),
  KEY VARCHAR2(255),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_SPECIALISATION.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_SPECIALISATION.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_SPECIALISATION.csv'));

-- Insertion des données dans IMPORT_SPECIALITE
CREATE TABLE SCO_IMPORT.TMP_IMPORT_SPECIALITE
(ID_SPECIALITE  NUMBER(38,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  VALIDE NUMBER(1,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_SPECIALITE.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_SPECIALITE.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_SPECIALITE.csv'));

-- Insertion des données dans IMPORT_VOCATION
CREATE TABLE SCO_IMPORT.TMP_IMPORT_VOCATION
(ID_VOCATION NUMBER(38,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  VALIDE NUMBER(1,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_VOCATION.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_VOCATION.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_VOCATION.csv'));

-- Insertion des données dans IMPORT_MODE_AP
CREATE TABLE SCO_IMPORT.TMP_IMPORT_MODE_AP
(ID_MODE_AP NUMBER(38,0),
  CODE   VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  LIBELLE VARCHAR2(255))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_MODE_AP.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_MODE_AP.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_MODE_AP.csv'));

-- Insertion des données dans IMPORT_TYPE_AP
CREATE TABLE SCO_IMPORT.TMP_IMPORT_TYPE_AP
(ID_TYPE_AP NUMBER(38,0),
  CODE   VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  LIBELLE VARCHAR2(255))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_TYPE_AP.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_TYPE_AP.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_TYPE_AP.csv'));

-- Insertion des données dans IMPORT_AP
CREATE TABLE SCO_IMPORT.TMP_IMPORT_AP
(ID_AP  NUMBER(38,0),
  VERSION_DIPLOME_ID NUMBER(12,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  TYPE_AP_ID NUMBER(38,0),
  MODE_AP_ID NUMBER(38,0),
  VALEUR_HEURES  NUMBER(12,0),
  NB_GROUPES NUMBER(12,0),
  SEUIL  NUMBER(12,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_AP.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_AP.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_AP.csv'));
 
-- Insertion des données dans IMPORT_EC
CREATE TABLE SCO_IMPORT.TMP_IMPORT_EC
(ID_EC  NUMBER(38,0),
  DISCIPLINE_ID  NUMBER(12,0),
  VERSION_DIPLOME_ID NUMBER(12,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  CREDIT_ECTS NUMBER(12,0),
  TRAVAIL_ETUDIANT   NUMBER(12,0),
  BASE_NOTE  NUMBER(12,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_EC.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_EC.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_EC.csv'));

-- Insertion des données dans IMPORT_LIEN
CREATE TABLE SCO_IMPORT.TMP_IMPORT_LIEN
(ID_LIEN NUMBER(38,0),
  EC_ID  NUMBER(38,0),
  AP_ID  NUMBER(38,0),
  SEMESTRE   NUMBER(12,0),
  MAJEUR VARCHAR2(1))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_LIEN.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_LIEN.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_LIEN.csv'));

-- Insertion des données dans IMPORT_UE
CREATE TABLE SCO_IMPORT.TMP_IMPORT_UE
(ID_UE  NUMBER(38,0),
  DOMAINE_ID NUMBER(12,0),
  VERSION_DIPLOME_ID NUMBER(12,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  CREDIT_ECTS NUMBER(12,0),
  TRAVAIL_ETUDIANT   NUMBER(12,0),
  BASE_NOTE  NUMBER(12,0))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_UE.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_UE.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_UE.csv'));

-- Insertion des données dans IMPORT_PARCOURS
CREATE TABLE SCO_IMPORT.TMP_IMPORT_PARCOURS
(ID_PARCOURS NUMBER(38,0),
  SPECIALISATION_ID  NUMBER(12,0),
  VOCATION_ID NUMBER(12,0),
  VERSION_DIPLOME_ID NUMBER(12,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_PARCOURS.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_PARCOURS.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_PARCOURS.csv'));

-- Insertion des données dans IMPORT_DIPLOME
CREATE TABLE SCO_IMPORT.TMP_IMPORT_DIPLOME
(ID_DIPLOME NUMBER(38,0),
  GRADE_UNIVERSITAIRE_ID NUMBER(12,0),
  DOMAINE_ID NUMBER(12,0),
  VOCATION_ID NUMBER(12,0),
  STATUT_FORMATION_ID NUMBER(12,0),
  TYPE_ORGANISATION_ID   NUMBER(12,0),
  CODE   VARCHAR2(255),
  LIBELLE VARCHAR2(255),
  LIBELLE_COURT  VARCHAR2(255),
  SISE_MESR  VARCHAR2(255),
  MENTION VARCHAR2(255),
  SPECIALITE VARCHAR2(255),
  ANNEE_DEPART   NUMBER(12,0),
  ANNEE_ARRIVEE  NUMBER(12,0),
  MODELE VARCHAR2(255))
  ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY W_FILE_NON_SCOLARIX
  ACCESS PARAMETERS (records delimited by newline skip 1
  badfile W_FILE_NON_SCOLARIX : 'IMPORT_DIPLOME.bad'
  logfile W_FILE_NON_SCOLARIX : 'IMPORT_DIPLOME.log'
  fields terminated by ';'
  optionally enclosed by '"'
  missing field VALUES are NULL)
  LOCATION ('IMPORT_DIPLOME.csv'));

