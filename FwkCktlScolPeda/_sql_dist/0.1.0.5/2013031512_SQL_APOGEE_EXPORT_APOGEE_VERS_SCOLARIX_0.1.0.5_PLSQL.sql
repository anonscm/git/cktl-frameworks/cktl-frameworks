-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_SCOLARITE du 15/03/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 11/11
-- Type : DML
-- Schema : SCO_SCOLARITE
-- Numero de version : 0.1.0.5
-- Date de publication : 15/03/2013
-- Auteur(s) : Association Cocktail
--
--
create or replace
PROCEDURE SCO_IMPORT.EXPORT_APOGEE_VERS_SCOLARIX IS

sqlquery VARCHAR2(32767);
csv_name VARCHAR2(255);
dir_file VARCHAR2(255) DEFAULT 'W_FILE_NON_SCOLARIX';
apo_sch  VARCHAR2(255) DEFAULT 'SCO_IMPORT';
apo_usr  VARCHAR2(255) DEFAULT apo_sch || '.';
apo_suffix VARCHAR(255) DEFAULT '';
BEGIN

  apo_usr := '';
  apo_suffix :='@apoprod';
  
	-- Extraction des données pour IMPORT_DISCIPLINE
  sqlquery := 'SELECT cod_dsi, lib_dsi, lic_dsi, tem_en_sve_dsi FROM ' || apo_usr || 'discipline_sis' || apo_suffix || ' WHERE tem_en_sve_dsi = ''O''';
  csv_name := 'IMPORT_DISCIPLINE.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
  DBMS_OUTPUT.PUT_LINE('Extraction des données de Disciplines : OK');

  -- Extraction des données pour IMPORT_DOMAINE
  sqlquery := 'SELECT cod_dfd, lib_dfd, '''', '''', tem_en_sve_dfd FROM ' || apo_usr || 'domaine_formation_dip' || apo_suffix ;
  csv_name := 'IMPORT_DOMAINE.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Domaines : OK');

	-- Extraction des données pour IMPORT_GRADE
  sqlquery := 'SELECT cod_cur, lib_cur, tem_en_sve_cur FROM ' || apo_usr || 'cursus_lmd' || apo_suffix || '  WHERE tem_en_sve_cur = ''O''';
  csv_name := 'IMPORT_GRADE.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Grades : OK');

  -- Extraction des données pour IMPORT_SPECIALISATION
  --sqlquery := '';
  --csv_name := 'IMPORT_SPECIALISATION.csv';
  --sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	--DBMS_OUTPUT.PUT_LINE('Extraction des données de Spécialisations : OK');

	-- Extraction des données pour IMPORT_SPECIALITE
  sqlquery := 'SELECT cod_spe, lib_spe, lic_spe, tem_en_sve_spe FROM ' || apo_usr || 'specialite' || apo_suffix ;
  csv_name := 'IMPORT_SPECIALITE.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Spécialités : OK');

	-- Extraction des données pour IMPORT_VOCATION
  sqlquery := 'SELECT cod_fiv, lib_fiv, tem_en_sve_fiv FROM ' || apo_usr || 'finalite_vdi' || apo_suffix || '  WHERE tem_en_sve_fiv = ''O''';
  csv_name := 'IMPORT_VOCATION.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Vocations : OK');

  -- Extraction des données pour IMPORT_MODE_AP
  --sqlquery := '';
  --csv_name := 'IMPORT_MODE_AP.csv';
  --sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	--DBMS_OUTPUT.PUT_LINE('Extraction des données de Modes d''AP : OK');

  -- Extraction des données pour IMPORT_TYPE_AP
  --sqlquery := '';
  --csv_name := 'IMPORT_TYPE_AP.csv';
  --sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	--DBMS_OUTPUT.PUT_LINE('Extraction des données de Types d''AP : OK');

	-- Extraction des données pour IMPORT_AP
  sqlquery :=             'SELECT '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.cod_nel, '''', I_CHAR.nbr_heu_cm_elp, I_CHAR.nrm_max_cm_elp, I_CHAR.nbr_grp_cm_th_cal ';
  sqlquery := sqlquery || 'FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || ' I_CHAR ';
  sqlquery := sqlquery || 'WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''CM'' AND I_CHAR.cod_anu = ''2012''';
  sqlquery := sqlquery || 'UNION ';
  sqlquery := sqlquery || 'SELECT '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.cod_nel, '''', I_CHAR.nbr_heu_td_elp, I_CHAR.nrm_max_td_elp, I_CHAR.nbr_grp_td_th_cal ';
  sqlquery := sqlquery || 'FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || '  I_CHAR ';
  sqlquery := sqlquery || 'WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''TD'' AND I_CHAR.cod_anu = ''2012''';
  csv_name := 'IMPORT_AP.csv';
  sqlquery := sqlquery || 'UNION ';
  sqlquery := sqlquery || 'SELECT '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.cod_nel, '''', I_CHAR.nbr_heu_tp_elp, I_CHAR.nrm_max_tp_elp, I_CHAR.nbr_grp_tp_th_cal ';
  sqlquery := sqlquery || 'FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || ' I_CHAR ';
  sqlquery := sqlquery || 'WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''TP'' AND I_CHAR.cod_anu = ''2012''';
  csv_name := 'IMPORT_AP.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données d''AP : OK');

	-- Extraction des données pour IMPORT_EC
  sqlquery := 'SELECT '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.lic_elp, I_COMP.nbr_crd_elp, '''', I_COMP.bar_sai_elp FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || '  I_CHAR WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''EC'' AND I_CHAR.cod_anu = ''2012''';
  csv_name := 'IMPORT_EC.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données d''EC : OK');

	-- Extraction des données pour IMPORT_REPARITION_AP
  --sqlquery := '';
  --csv_name := 'IMPORT_REPARTITION_AP.csv';
  --sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	--DBMS_OUTPUT.PUT_LINE('Extraction des données de Répartitions d''AP : OK');

	-- Extraction des données pour IMPORT_UE
  sqlquery := 'SELECT '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.lic_elp, I_COMP.nbr_crd_elp, '''', I_COMP.bar_sai_elp FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || ' I_CHAR WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''UE'' AND I_CHAR.cod_anu = ''2012''';
  csv_name := 'IMPORT_UE.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données d''UE : OK');

	-- Extraction des données pour IMPORT_PARCOURS
  sqlquery := 'SELECT '''', '''', I_CHAR.cod_anu, I_COMP.cod_elp, I_COMP.lib_elp, I_COMP.lic_elp FROM ' || apo_usr || 'element_pedagogi' || apo_suffix || ' I_COMP, ' || apo_usr || 'elp_charge_ens' || apo_suffix || ' I_CHAR WHERE I_CHAR.cod_elp = I_COMP.cod_elp AND I_COMP.cod_nel = ''PAR'' AND I_CHAR.cod_anu = ''2012''';
  csv_name := 'IMPORT_PARCOURS.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Pacours : OK');

	-- Extraction des données pour IMPORT_DIPLOME
  sqlquery :=             'SELECT I_DIPL.cod_dip, I_DIPL.lib_dip, I_DIPL.lic_dip, I_VERS.cod_sis_vdi, I_VERS.cod_cur, I_VERS.cod_fiv, I_DIPL.cod_ndi, I_NATU.lib_ndi, I_NATU.lic_ndi, ''H'', ''S'', I_MENT.lib_mev, I_VERS.cod_svd, ''1'', I_VERS.dur_ann_vdi ';
  sqlquery := sqlquery || 'FROM ' || apo_usr || 'diplome' || apo_suffix || ' I_DIPL, ' || apo_usr || 'version_diplome' || apo_suffix || ' I_VERS, ' || apo_usr || 'nature_diplome' || apo_suffix || ' I_NATU, ' || apo_usr || 'mention_vdi' || apo_suffix || ' I_MENT ';
  sqlquery := sqlquery || 'WHERE I_VERS.cod_dip = I_DIPL.cod_dip AND I_NATU.cod_ndi = I_DIPL.cod_ndi AND I_MENT.cod_mev = I_VERS.cod_mev AND I_VERS.cod_car_vdi is NULL AND I_VERS.daa_fin_val_vdi > ''2012''';
  csv_name := 'IMPORT_DIPLOME.csv';
  sco_import.extract_to_csv(sqlquery, dir_file, csv_name);
	DBMS_OUTPUT.PUT_LINE('Extraction des données de Diplomes : OK');

END;
/
