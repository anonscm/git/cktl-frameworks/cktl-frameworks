--
-- Patch DDL de SCO_SCOLARITE du 12/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : SCO_SCOLARITE
-- Numero de version : 1.0.0.14
-- Date de publication : 12/11/2013
-- Auteur(s) : ldrouot

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-----------------------------V20131105.152815__DDL_ajout_colone_cnu.sql-----------------------------
-- ajout d'une colonne CNU_ID à la table COMPOSANT
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'NO_CNU' and table_name = 'SCO_SCOLARITE.COMPOSANT';
  if (is_column_exists  = 0) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.COMPOSANT ADD (NO_CNU NUMBER(38))';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.COMPOSANT.NO_CNU IS ''code CNU correspondant de la table GRHUM.CNU ''';
  end if;
end;
/

-----------------------V20131106.143805__DDL_Sco_scolarite_ajout_de_HETD.sql------------------------
-- ajout d'une colonne HEURE_EQUIVALENT à la table TYPE_AP
declare
  is_column_exists int;
begin 
  Select count(*) into is_column_exists from all_tab_cols where column_name = 'HEURE_EQUIVALENT' and table_name = 'SCO_SCOLARITE.TYPE_AP';
  if (is_column_exists  = 0) then
    execute immediate 'ALTER TABLE SCO_SCOLARITE.TYPE_AP ADD (HEURE_EQUIVALENT NUMBER(38,3) DEFAULT 1)';
    execute immediate 'COMMENT ON COLUMN SCO_SCOLARITE.TYPE_AP.HEURE_EQUIVALENT IS ''Equivalent à une heure de référence pour le type AP''';
  end if;
end;
/



