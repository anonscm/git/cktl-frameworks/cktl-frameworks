--
-- Patch DML de SCO_IMPORT du 24/10/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DML
-- Schema : SCO_IMPORT
-- Numero de version : 0.1.0.13
-- Date de publication : 24/10/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;




--DB_VERSION 
INSERT INTO SCO_IMPORT.DB_VERSION (DBV_ID,DBV_LIBELLE,DBV_DATE,DBV_INSTALL,DBV_COMMENT) VALUES(SCO_IMPORT.DB_VERSION_SEQ.NEXTVAL,'0.1.0.13',to_date('24/10/2013','DD/MM/YYYY'),sysdate,'Version Bêta 13');
COMMIT;