--
-- Lanceur de tous les scripts SQL 21/05/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 0
-- Type : Lanceur
-- Schema : SCO_SCOLARITE - SCO_IMPORT
-- Numero de version : 0.1.0.8
-- Date de publication : 21/05/2013
-- Auteur(s) : Association Cocktail
--
--

START 2013052101_SQL_SCO_SCOLARITE_0.1.0.8_DBA.sql
START 2013052102_SQL_SCO_SCOLARITE_0.1.0.8_DDL.sql
START 2013052103_SQL_SCO_SCOLARITE_0.1.0.8_DML.sql
START 2013052107_SQL_SCO_IMPORT_IMPORT_SCOLARIX_0.1.0.8_PLSQL.sql
START 2013052108_SQL_SCO_IMPORT_IMPORT_GIROFLE_0.1.0.8_PLSQL.sql

EXIT
