/* Outils.java created by cpinsard on Thu 04-Sep-2003 */

/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.common;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.webobjects.foundation.NSTimestamp;

public class Outils {
/** Formatage d'un numero insee */
	public static String formatteInsee(String numero) {
		String chaine = "";
		
		for (int i=0;i<numero.length();i++)
		{
			chaine = chaine + numero.charAt(i);
			if ( (i==0) || (i == 2) || (i == 4) || (i == 6) | (i==9))
				chaine = chaine + " ";
		}

		return chaine;
	}
	// Utilitaires de temps
	// ANNEE CIVILE
	public static int anneeCivile() {
	    return new GregorianCalendar().get(Calendar.YEAR);
	}
	public static NSTimestamp debutAnneeCivile() {
		String chaineDate = "01/01/" + anneeCivile();
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}
	public static NSTimestamp finAnneeCivile() {
		String chaineDate = "31/12/" + anneeCivile();
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}
	// ANNEE UNIVERSITAIRE - 2000-2001 ==> 2000
	public static int anneeUniversitaire() {
		GregorianCalendar today = new GregorianCalendar();
		int annee = today.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) >= 8)	// les mois commencent à zéro
			return annee;
		else
			return annee - 1;
	}

	// DEBUT ANNEE UNIVERSITAIRE
	public static NSTimestamp debutAnneeUniversitaire() {
		String chaineDate = "01/09/" + anneeUniversitaire();
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}

	// FIN ANNEE UNIVERSITAIRE
	public static NSTimestamp finAnneeUniversitaire() {
		String chaineDate = "31/08/" + (anneeUniversitaire() + 1);
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}
	public static NSTimestamp debutAnneeUniversitairePourDate(NSTimestamp dateJour) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(dateJour);
		int annee = calendar.get(Calendar.YEAR);
		int jour = calendar.get(Calendar.DAY_OF_MONTH);
		int mois = calendar.get(Calendar.MONTH);
		if (mois > 8 || (mois == 8 && jour > 1)) {	// les mois commencent à zéro
			annee = annee + 1;
		}
		String chaineDate = "01/09/" + annee;
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}
	
	// FIN ANNEE UNIVERSITAIRE
	public static NSTimestamp finAnneeUniversitairePourDate(NSTimestamp dateJour) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(dateJour);
		int annee = calendar.get(Calendar.YEAR);
		if (calendar.get(Calendar.MONTH) >= 8) {	// les mois commencent à zéro
			annee = annee + 1;
		}
		String chaineDate = "31/08/" + annee;
		return DateCtrl.stringToDate(chaineDate,"%d/%m/%Y");
	}
	
}
