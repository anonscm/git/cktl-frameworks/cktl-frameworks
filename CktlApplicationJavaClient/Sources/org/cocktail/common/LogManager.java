/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.common;

import java.io.PrintStream;

import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimestamp;

public class LogManager {
	private static StringBuffer log;	// pour stocker les logs
	private static String modeConsole, nomFichier;
	private static String serverPathOutputForWO = null;
	
	/** d&eacute;finit le WOOutputPath (&agrave; utiliser uniquement du c&ocirc;t&eacute; serveur */
	public static void setServerPathOutputForWO(String pathOutput) {
		serverPathOutputForWO = pathOutput;
	}
	/** retourne true si on est en mode Debug */
	public static boolean estModeDebug() {
		return NSLog.allowedDebugLevel() == NSLog.DebugLevelInformational;
	}
	/** Imprime un message d'information dans le fichier de log */ 
	public static void logInformation(String unMessage) {
		log(unMessage);
	}
	/** Imprime un message de debug d&eacute;taill&eacute; dans le fichier de log */
	public static void logDetail(String unMessage) {
		log(unMessage,NSLog.DebugLevelInformational);
	}
	/** Imprime un message d'erreur dans le fichier log contenant les erreurs */
	public static void logErreur(String unMessage) {
		NSLog.debug.appendln(unMessage);
		log.append("\n" + unMessage);
	}
	/** Imprime un message de log suite &agrave; une erreur */
	public static void logException(Throwable e) {
		if (NSLog.debug.isEnabled()) {
			String exception = NSLog.throwableAsString(e);
			NSLog.debug.appendln(exception);
			log.append("\n" + exception);
		} else {
			e.printStackTrace();
		}
	}
	/** pr&eacute;pare le mode de logging et de debug 
	 * @param nomFichierLog nom du fichier de log
	 * @param modeLogging ("O" = mode console, "N" = mode fichier)
	 * @param modeDebug ("O" = mode debug, "N" = mode information)
	 * */
	public static void preparerLog(String nomFichierLog,String modeLogging,String modeDebug) {
		NSLog.debug.setIsVerbose(false);
		// pour pouvoir resetter les logs
		modeConsole = modeLogging;
		nomFichier = nomFichierLog;
		if (modeLogging != null && modeLogging.equals("N")) {
			if (serverPathOutputForWO == null) {
				// Redéfinir les print streams
				String date = DateCtrl.dateToStringSansDelim(new NSTimestamp());
				String path = (String)System.getProperty("user.dir");
				int position = path.lastIndexOf("/");
				if (position >= 0)
					path = path.substring(0,position);
				String filePath = path + "/" + nomFichierLog + date + ".txt";
				PrintStream aStream = NSLog.printStreamForPath(filePath);
				if (aStream != null) {
					((NSLog.PrintStreamLogger)NSLog.out).setPrintStream(aStream);
					((NSLog.PrintStreamLogger)NSLog.debug).setPrintStream(aStream);
				}
			}
		}
		log = new StringBuffer();
		changerModeDebug(modeDebug);
	}
	/** Change le mode de debut
	 *  @param modeDebug ("O" = mode debug, "N" = mode information)
	 * */
	public static void changerModeDebug(String modeDebug) {
		if (modeDebug != null && modeDebug.equals("O")) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelInformational);
			//NSLog.debug.setAllowedDebugLevel(NSLog.DebugLevelInformational);	// pour avoir l'affichage jbdc dans la console
		} else {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelCritical);
			//NSLog.debug.setAllowedDebugLevel(NSLog.DebugLevelCritical);
		}
	}
	/** reset le log */
	public static void reset() {
		if (estModeDebug()) {
			preparerLog(nomFichier,modeConsole,"O");
		} else {
			preparerLog(nomFichier,modeConsole,"N");
		}
	}
	/** retourne le texte du log */
	public static String texteLog() {
		return log.toString();
	}
	private static void log(String unMessage,int niveau) {
		 //if (NSLog.debug.allowedDebugLevel() == niveau) {
		if (NSLog.allowedDebugLevel() == niveau) {
			 NSLog.debug.appendln(unMessage);
			 log.append("\n" + unMessage);
		}
	}
	private static void log(String unMessage) {
		NSLog.out.appendln(unMessage);
		log.append("\n" + unMessage);
	}
}
