//
//  SuperFinder
//  ObjetsUniversite
//
//  Created by Christine Buttin on Fri Mar 11 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
// contient toutes les méthodes de recherche sur des EOGenericRecord ou des méthodes générales
//
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.common.modele;
 


import org.cocktail.common.DateCtrl;
import org.cocktail.common.utilities.EOFUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/** Utilitaires pour EOF. Les m&eacute;thodes de recherche de cette classe sont maintenues pour des raisons de compatibilit&eacute; avec
 * Mangue
 * @author christine<BR>
 *
 */
public class SuperFinder {
	/** retourne le generic record pour la globalID pass&eacute;e en param&egrave;tre dans l'editing context */
	public static EOGenericRecord objetForGlobalIDDansEditingContext(EOGlobalID globalID,EOEditingContext editingContext) {
		return (EOGenericRecord)editingContext.faultForGlobalID(globalID,editingContext);
	}
	// UtilitairesGraphiques de recherche
	/** Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @param forceRefresh true si forcer le refresh des objets
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon,boolean forceRefresh) {
		return EOFUtilities.rechercherAvecQualifier(editingContext, nomEntite, qualifier, sansDoublon,forceRefresh);
	}
	/** Recherche toutes les entit&eacute;s d&eacute;finies par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon) {
		return EOFUtilities.rechercherAvecQualifier(editingContext,nomEntite,qualifier,sansDoublon,false);
	}
	/** Retourne le contenu complet d'une table sans qualification des entit&eacute;s
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntite(EOEditingContext editingContext,String nomEntite) {
		return EOFUtilities.rechercherAvecQualifier(editingContext,nomEntite,null,true);
	}
	/** Retourne le contenu complet d'une table sans qualification des entit&eacute;s et avec un refresh
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntiteAvecRefresh(EOEditingContext editingContext,String nomEntite) {
		return EOFUtilities.rechercherAvecQualifier(editingContext,nomEntite,null,true,true);
	}
	/** Recherche toutes les entit&eacute;s dont un champ a une certaine valeur
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext,String nomEntite,String attribut,String valeur) {
		return EOFUtilities.rechercherAvecAttribut(editingContext, nomEntite, attribut, valeur);
	}
	/** Recherche toutes les entit&eacute;s dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String attribut,Object valeur) {
		 return EOFUtilities.rechercherAvecAttributEtValeurEgale(editingContext, nomEntite, attribut, valeur);
	}
	/** Recherche l'entit&eacute; dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objet trouv&eacute;
	 */
	public static EOGenericRecord rechercherObjetAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String attribut,String valeur) {
		return EOFUtilities.rechercherObjetAvecAttributEtValeurEgale(editingContext, nomEntite, attribut, valeur);
	}

	/** Evalue la cl&eacute; primaire pour une entit&eacute; en cherchant dans la table de s&eacute;quence pour une base Oracle
	 * ou en la calculant pour les autres bases
	 * @param editingContext
	 * @param nomEntite
	 * @param nomClePrimaire
	 * @param nomEntiteSequenceOracle non null si Oracle g&egrave;re la s&eacute;quence sinon on consid&egrave;re qu'il s'agit d'une autre base
	 * @param estUnLong true si la cl&eacute; doit &ecirc;tre retourn&eacute;e comme un Long
	 * @return valeur de la cl&eacute; primaire
	 */
	public static Number clePrimairePour(EOEditingContext editingContext,String nomEntite,String nomClePrimaire,String nomEntiteSequenceOracle,boolean estUnLong) {
		if (nomEntiteSequenceOracle != null) {
				return numeroSequenceOracle(editingContext,nomEntiteSequenceOracle,estUnLong);
		} else {
			return numeroSequenceAutreBase(editingContext,nomEntite,nomClePrimaire,estUnLong);
		}
	}
	/** Recherche le num&eacute;ro de s&eacute;quence dans une table de s&eacute;quence Oracle (doit comporter un attribut nextval)
	 * @param editingContext
	 * @param nomEntiteSequenceOracle
	 * @param estUnLong true si la cl&eacute; doit &ecirc;tre retourn&eacute;e comme un Long
	 * @return valeur trouv&eacute;e ou null
	 */
	public static Number numeroSequenceOracle(EOEditingContext editingContext,String nomEntiteSequenceOracle,boolean estUnLong) {
		if (nomEntiteSequenceOracle == null || nomEntiteSequenceOracle.equals("")) {
			return null;
		}
		EOFetchSpecification  myFetch = new EOFetchSpecification(nomEntiteSequenceOracle,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			return (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Calcule la cl&eacute; primaire pour une base quelconque
	 * @param editingContext
	 * @param nomEntite
	 * @param nomClePrimaire (comme d&eacute;finie dans le mod&egrave;le)
	 * @param estUnLong true si la cl&eacute; doit &ecirc;tre retourn&eacute;e comme un Long
	 * @return valeur trouv&eacute;e ou null
	 */
	public static Number numeroSequenceAutreBase(EOEditingContext editingContext,String nomEntite,String nomClePrimaire,boolean estUnLong) {
		NSArray mySort = new NSArray(new EOSortOrdering(nomClePrimaire,EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,null,mySort);
		fs.setFetchLimit(1);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		if (estUnLong) {
			long noInd = 0;
			try {
				noInd = ((Number)((EOGenericRecord)results.objectAtIndex(0)).valueForKey(nomClePrimaire)).longValue();
			} catch (Exception e) {}
			return new Long(noInd + 1);
		} else {
			int noInd = 0;
			try {
				noInd = ((Number)((EOGenericRecord)results.objectAtIndex(0)).valueForKey(nomClePrimaire)).intValue();
			} catch (Exception e) {}
			return new Integer(noInd + 1);
		}
	}
	/** retourne une date format&eacute;e sous forme de String : le format est jj/mm/aaaa
	 * @param record	record pour lequel on doit formatter une date
	 * @param cle cl&eacute; du champ date
	 * @return date format&eacute;e
	 */
	public static String dateFormatee(EOGenericRecord record,String cle) {
		if (record.valueForKey(cle) == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(record.valueForKey(cle));
		}
	}
	
	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
	 * sera automatiquement compl&eacute;t&eacute;e
	 * @param record record pour lequel on doit fournir une date (NSTimestamp)
	 * @param cle cl&eacute; du champ date
	 * @param uneDate valeur de la date (String)
	 */
	public static void setDateFormatee(EOGenericRecord record,String cle,String uneDate) {
		if (uneDate == null) {
			record.takeValueForKey(null,cle);
		}
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
	        record.takeValueForKey(null,cle);
	    } else {
	    		record.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
	    } 
	}
	/** retourne une date format&eacute;e sous forme de String : le format est jj/mm/aaaa
	 * @param object	objet pour lequel on doit formatter une date (doit impl&eacute;menter le key-value coding)
	 * @param cle cl&eacute; du champ date
	 * @return date format&eacute;e
	 */
	public static String dateFormatee(NSKeyValueCoding object,String cle) {
		if (object.valueForKey(cle) == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(object.valueForKey(cle));
		}
	}
	
	/** transforme une date fournie sous forme de string en une NSTimestamp. Si la date n'est pas compl&egrave;te (i.e jj/mm/aaaa), elle
	 * sera automatiquement compl&eacute;t&eacute;e
	 * @param object objet pour lequel on doit fournir une date (NSTimestamp)  (doit impl&eacute;menter le key-value coding)
	 * @param cle cl&eacute; du champ date
	 * @param uneDate valeur de la date (String)
	 */
	public static void setDateFormatee(NSKeyValueCoding object,String cle,String uneDate) {
		if (uneDate == null) {
			object.takeValueForKey(null,cle);
		}
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
			object.takeValueForKey(null,cle);
	    } else {
	    	object.takeValueForKey(DateCtrl.stringToDate(myDate),cle);
	    } 
	}
	/** Retourne le qualifier pour d&eacute;terminer les objets valides pour une p&eacute;riode
	 * @param champDateDebut	nom du champ de date d&eacute;but sur lequel construire le qualifier
	 * @param debutPeriode	date de d&eacute;but de p&eacute;riode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de p&eacute;riode
	 * @return qualifier construit ou null si date d&eacute;but est nulle
	 */
	public static EOQualifier qualifierPourPeriode(String champDateDebut,NSTimestamp debutPeriode,String champDateFin,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			return null;
		}
		String stringQualifier = "";
		NSMutableArray args = new NSMutableArray(debutPeriode);
		// champDateFin = nil or champDateFin >= debutPeriode
		stringQualifier = "(" + champDateFin + " = nil OR " + champDateFin + " >= %@)";
		if (finPeriode != null) {
			args.addObject(finPeriode);
			// champDateDebut <= finPeriode
			stringQualifier = stringQualifier + " AND (" + champDateDebut + " <= %@)";
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
	}
	/** Construit un OR qualifier pour l'attribut pass&eacute; en param&egrave;tre &grave; partir des valeurs fournies
	 * @param attribut attribut sur lequel construire le qualifier
	 * @param valeurs	liste des valeurs recherch&eacute;es
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static EOQualifier construireORQualifier(String attribut,NSArray valeurs) {
		return construireQualifier(attribut,valeurs,false);
	}
	/** Construit un AND qualifier pour l'attribut pass&eacute; en param&egrave;tre &grave; partir des valeurs fournies
	 * @param attribut attribut sur lequel construire le qualifier
	 * @param valeurs	liste des valeurs recherch&eacute;es
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static EOQualifier construireAndQualifier(String attribut,NSArray valeurs) {
		return construireQualifier(attribut,valeurs,true);
	}
	/** Supprime le dernier op&eacute;rateur du string qualifier
	 * @param stringQualifier string qualifier pour lequel on veut supprimer l'op&eacute;rator
	 * @param operateurASupprimer	op&eacute;rateur &agrave; supprimer :
	 * @return qualifier construit ou null si liste des valeurs vides
	 */
	public static String nettoyerQualifier(String stringQualifier,String operateurASupprimer) {
		int lastIndex = stringQualifier.lastIndexOf(operateurASupprimer);
		if (lastIndex > 0) {
			stringQualifier = stringQualifier.substring(0,lastIndex);
		}
		return stringQualifier;
	}
	// méthodes privées
	private static EOQualifier construireQualifier(String attribut,NSArray valeurs,boolean estAndQualifier) {
		if (valeurs == null || valeurs.count() == 0) {
			return null;
		}
		NSMutableArray qualifiers = new NSMutableArray();
		java.util.Enumeration e = valeurs.objectEnumerator();
		while (e.hasMoreElements()) {
			Object valeur = (Object)e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(attribut + " = %@", new NSArray(valeur)));

		}
		if (estAndQualifier) {
			return new EOAndQualifier(qualifiers);
		} else {
			return new EOOrQualifier(qualifiers);
		}
	}
}
