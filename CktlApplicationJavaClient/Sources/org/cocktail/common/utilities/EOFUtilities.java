//
//  SuperFinder
//  ObjetsUniversite
//
//  Created by Christine Buttin on Fri Mar 11 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
// contient toutes les méthodes de recherche sur des EOGenericRecord ou des méthodes générales
//
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.common.utilities;
 


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author christine<BR>
 *
 */
public class EOFUtilities {
	/** Returns the EO generic record of the provided globalID in the editing context   */
	public static EOGenericRecord objectWithGlobalIDInEditingContext(EOGlobalID globalID,EOEditingContext editingContext) {
		return (EOGenericRecord)editingContext.faultForGlobalID(globalID,editingContext);
	}
	/** Retourne le generic record pour la globalID pass&eacute;e en param&egrave;tre dans l'editing context */
	public static EOGenericRecord objetForGlobalIDDansEditingContext(EOGlobalID globalID,EOEditingContext editingContext) {
		return objectWithGlobalIDInEditingContext(globalID,editingContext);
	}
	/** Fetch the objects with the provided qualifier
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @param qualifier
	 * @param usesDistinct true to discard identical record
	 * @param forceRefresh true if existing objects are refreshed with fetch values
	 * @return an array of EOGenericRecord
	 */
	public static NSArray fetchWithQualifier(EOEditingContext editingContext,String entityName,EOQualifier qualifier,boolean usesDistinct,boolean forceRefresh) {
		EOFetchSpecification fs = new EOFetchSpecification(entityName,qualifier,null);
		fs.setUsesDistinct(usesDistinct);
		fs.setRefreshesRefetchedObjects(forceRefresh);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Fetch the objects with the provided qualifier
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @param qualifier
	 * @param usesDistinct true to discard identical record
	 * @return an array of EOGenericRecord
	 * */
	public static NSArray fetchWithQualifier(EOEditingContext editingContext,String entityName,EOQualifier qualifier,boolean usesDistinct) {
		return fetchWithQualifier(editingContext,entityName,qualifier,usesDistinct,false);
	}
	/** Fetch all the objects
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @return an array of EOGenericRecord
	 */
	public static NSArray fetchEntity(EOEditingContext editingContext,String entityName) {
		return fetchWithQualifier(editingContext,entityName,null,true);
	}
	/** Fetch all the objects with a refresh of existing objects
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @return an array of EOGenericRecord	 */
	public static NSArray fetchEntityWithRefresh(EOEditingContext editingContext,String entityName) {
		return fetchWithQualifier(editingContext,entityName,null,true,true);
	}
	/** Fetch all the objects whose attribute has the same (caseInsensitiveLike) value
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @param attribute	attribute to be used in the fetch
	 * @param value value of the attribute
	 * @return an array of EOGenericRecord	 */
	public static NSArray fetchWithAttribute(EOEditingContext editingContext,String entityName,String attribute,String value) {
		NSArray values = new NSArray(value);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(attribute + " caseInsensitiveLike %@",values);
		return fetchWithQualifier(editingContext,entityName,qualifier,true);
	}
	/** Fetch all the objects whose attribute has the same (=) value
	 * @param editingContext
	 * @param entityName entity to fetch
	 * @param attribute	attribute to be used in the fetch
	 * @param value value of the attribute
	 * @return objets trouv&eacute;s
	 */
	public static NSArray fetchWithAttributeAndSameValue(EOEditingContext editingContext,String entityName,String attribute,String value) {
		NSArray values = new NSArray(value);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(attribute + " = %@",values);
		return fetchWithQualifier(editingContext,entityName,qualifier,true);
	}
	/** Returns the first fetch object whose attribute has the same (=) value
	 * @param editingContext
	 * @param entityName  fetched entity
	 * @param attribute	  attribute to look for
	 * @param value	  value of the attribute
	 * @return an EOGenericRecord or null if nothing is fetch
	 */
	public static EOGenericRecord fetchObjectWithAttributeAndValue(EOEditingContext editingContext,String entityName,String attribute,String value) {
		NSArray resultats = fetchWithAttributeAndSameValue(editingContext,entityName,attribute,value);
		try {
			return (EOGenericRecord)resultats.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Fetch tous les objets de l'entit&eacute; d&eacute;finis par le qualifier fourni en param&egrave;tre
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @param forceRefresh true si forcer le refresh des objets
	 * @return un tableau de EOGenericRecord
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon,boolean forceRefresh) {
		return fetchWithQualifier(editingContext,nomEntite,qualifier,sansDoublon,forceRefresh);
	}
	/** Recherche toutes les objets associ&eacute;s &agrave; l'entit&eacute; respectant le qualifier
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param qualifier
	 * @param sansDoublon true si supprimer doublons
	 * @return un tableau de EOGenericRecord
	 */
	public static NSArray rechercherAvecQualifier(EOEditingContext editingContext,String nomEntite,EOQualifier qualifier,boolean sansDoublon) {
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,sansDoublon,false);
	}
	/** Recherche toutes les objets associ&eacute;s &agrave; l'entit&eacute;
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return un tableau de EOGenericRecord
	 */
	public static NSArray rechercherEntite(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true);
	}
	/** Recherche toutes les objets associ&eacute;s &agrave; l'entit&eacute; et avec un refresh
	 * @param editingContext
	 * @param nomEntite &agrave; rechercher
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherEntiteAvecRefresh(EOEditingContext editingContext,String nomEntite) {
		return rechercherAvecQualifier(editingContext,nomEntite,null,true,true);
	}
	/** Recherche toutes les objets dont un champ a une certaine valeur (caseInsensitiveLike)
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut	attribut sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttribut(EOEditingContext editingContext,String nomEntite,String attribut,String valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(attribut + " caseInsensitiveLike %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	/** Recherche toutes les objets dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut	champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objets trouv&eacute;s
	 */
	public static NSArray rechercherAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String attribut,Object valeur) {
		NSArray values = new NSArray(valeur);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(attribut + " = %@",values);
		return rechercherAvecQualifier(editingContext,nomEntite,qualifier,true);
	}
	/** Recherche le premier objet trouv&eacute; dont un champ a une certaine valeur en faisant une &eacute;galit&eacute;
	 * @param editingContext
	 * @param nomEntite 	&agrave; rechercher
	 * @param attribut		champ sur lequel effectuer la recherche
	 * @param valeur		valeur du champ &agrave; recherch&eacute;
	 * @return objet trouv&eacute; ou null
	 */
	public static EOGenericRecord rechercherObjetAvecAttributEtValeurEgale(EOEditingContext editingContext,String nomEntite,String attribut,String valeur) {
		NSArray resultats = rechercherAvecAttributEtValeurEgale(editingContext,nomEntite,attribut,valeur);
		try {
			return (EOGenericRecord)resultats.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
