
//ModelePage.java
//Mangue

//Created by Christine Buttin on Tue May 06 2003.
//Copyright (c) 2003 __MyCompanyName__. All rights reserved.

/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Constructor;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eodistribution.client.EODistributionChannel;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
/** Classe abstraite d&eacute;finissant le mod&egrave;le d'une page de Mangue
 * @author christine<BR>
 *
 * D&eacute;crit le mod&egrave;le d'une page comportant une liste de s&eacute;lection et des boutons pour cr&eacute;er, modifier ou supprimer
 * des donn&eacute;es. Les modifications de donn&eacute;es sont faites dans une autre page<BR>
 * Pour utiliser cette classe :
 *<UL>cr&eacute;er une sous-classe java pour le contr&ocirc;leur</UL>
 *<UL>dupliquer le fichier nib ModelePage et lui donner le m&ecirc;me nom que la classe java du contr&ocirc;leur</UL>
 *<UL>dans le fichier nib :
 *	<UL>ajouter un display group depuis le mod&egrave;le et connecter l'outlet du contr&ocirc;leur au display group</UL>
 *	<UL>sp&eacute;cifier si il y a un fetch on load</UL>
 *	<UL>connecter l'outlet delegate du display group au file's owner</UL>
 *	<UL>cr&eacute;er l'interface de la fen&ecirc;tre</UL></UL>
 *	Pour faire des initialisations avant l'affichage de la fen&ecirc;tre, surcharger la m&eacute;thode preparerFenetre<BR>
 *	Lors de l'utilisation d'une sous-classe dans un dialogue, invoquer directement la m&eacute;thode preparerFenetre apr&egrave;s
 *  le chargement de l'archive<BR>
 * Pour faire des op&eacute;rations avant fermeture de la fen&ecirc;tre, surcharger la m&eacute;thode terminer<BR>
 *	Lors de l'utilisation d'une sous-classe dans un dialogue, invoquer directement la m&eacute;thode terminer avant de fermer la fen&ecirc;tre<BR>
 * Utilise des notifications pour communiquer avec les pages de modification<BR>
 */
// 27/09/2010 - l'appel à connectionWasEstablished est fait uniquement lors du chargement d'interface Netbeans
// 17/01/2011 - correction pour éviter les fetchs en mode fenêtre dans connectionWasEstablished
public abstract class ModelePage extends EOInterfaceController {
	/** Notification envoy&eacute;e signalant que le contr&ocirc;leur n'est plus &grave; suivre */
	public static String ARRET_CONTROLEUR = "ArreterControleur";
	/** Outlet sur la vue des boutons de modification */
	public EOView vueBoutonsModification;
	/** Outlet pour la table contenant les objets du display group */
	public EOTable listeAffichage;
	private boolean estEditable;
	private boolean invisibleApresValidation;
	private boolean modificationEnCours;
	private boolean gereDoubleClic;
	private String nomClasseCreation;
	private String titreFenetreCreation;
	private boolean fermetureExterieure;
	private boolean archiveLoaded;
	private boolean selectionApresFetch;

	/** Notification envoy&eacute;e pour le verrouillage des &eacutes;crans en cas de modification */
	public static String LOCKER_ECRAN = "NotifBlocageEcrans";
	/** Notification envoy&eacute;e pour le d&eacute;verrouillage des &eacutes;crans apr&egrave;s validation */
	public static String DELOCKER_ECRAN = "NotifDeblocageEcrans";
	/** Notification envoy&eacute;e pour synchronisation des donn&eacute;es suite &agrave; la fermeture d'un dialogue de cr&eacute;ation/modificatio */
	public static String SYNCHRONISER = "Synchronisation";
	/** Notification envoy&eacute;e pour raffraichissement des associations apr&egrave;s modification par d'autres interfaces */
	public static String RAFFRAICHIR = "RaffraichirAssociations";
	public ModelePage() {
		super();
		fermetureExterieure = false;
		archiveLoaded = false;
		selectionApresFetch = false;
	}
	public ModelePage(EOEditingContext substitutionEditingContext) {
		super(substitutionEditingContext);
		fermetureExterieure = false;
		archiveLoaded = false;
		selectionApresFetch = false;
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void prepareComponent() {
		// 12/11/2010 - archiveLoaded est setté en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à establishConnection
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.prepareComponent();
			}
		}
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void establishConnection() {
		// 12/11/2010 - archiveLoaded est setté en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à prepareComponent
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
				setConnected(true);
				connectionWasEstablished();
			} catch (Exception exc) {
				//exc.printStackTrace();
				// Il s'agit d'une interface qui est dans le même package
				super.establishConnection();
			}
			archiveLoaded = true;
		}
	}
	/** initialise la page
	 * @param estEditable true si on peut modifier les donn&eacute;es dans la table du display group
	 * @param invisibleApresValidation true si on doit masquer la fen&ecirc;tre apr&egrave;s validation ou annulation
	 * @param nomClasseCreation nom de la classe du contr&ocirc;leur pour cr&eacute;er/modifier les donn&eacute;es
	 * @param titreFenetreCreation titre de la fen&ecirc;tre de cr&eacute;ation/modification des donn&eacute;es
	 * @param gereDoubleClic true si gestion par d&eacute;faut du double-clic
	 */
	public void initialiser(boolean estEditable,boolean invisibleApresValidation,String nomClasseCreation,String titreFenetreCreation,boolean gereDoubleClic) {
		this.estEditable = estEditable;
		this.invisibleApresValidation = invisibleApresValidation;   
		this.nomClasseCreation = nomClasseCreation;
		this.titreFenetreCreation = titreFenetreCreation;
		this.gereDoubleClic = gereDoubleClic;
	}
	/** Retourne true si le contr&ocirc;leur est affich&eacute; en permanence. 
	 * Retourne toujours false, &grave; surcharger si n&eacute;cessaire */
	public boolean estAffichagePermanent() {
		return false;
	}
	public void connectionWasEstablished() {
		LogManager.logDetail(this.getClass().getName() + " - connectionWasEstablished");
		setFetchesOnConnect(false);	 // 17/01/2011 - pour éviter un fetch lors de l'affichage en mode fenêtre
		setFetchesOnConnectEnabled(false);  // 17/01/2011 - pour éviter un fetch lors de l'affichage en mode fenêtre
		super.connectionWasEstablished();
		activer();
	}
	public void connectionWasBroken() {
		LogManager.logDetail(this.getClass().getName() + " - connectionWasBroken");
		super.connectionWasBroken();
		if (!fermetureExterieure) {
			terminer();
			if (supercontroller() != null && supercontroller() instanceof EOFrameController) 	{ // Il s'agit d'un composant racine
				String name = ((EOFrameController)supercontroller()).label();
				NSNotificationCenter.defaultCenter().postNotification(ARRET_CONTROLEUR,name);
			}
		}
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}

	/** Pour d&eacute;caler la fen&ecirc;tre &agrave; la position fournie */
	public void decalerFenetre(int X,int Y) {
		if (supercontroller() != null) {
			((EOFrameController)supercontroller()).window().setLocation(X,Y);
		}
	}
	public void activer() {
		preparerFenetre();
	}
	public void afficherFenetre() {
		if (supercontroller() != null && supercontroller() instanceof EOFrameController) {
			((EOFrameController)supercontroller()).activateWindow();
		}
	}
	public void arreter() { 
		LogManager.logDetail(this.getClass().getName() + " - arreter");
		fermetureExterieure = true;
		terminer();
		NSNotificationCenter.defaultCenter().removeObserver(this);

		if (supercontroller() != null && supercontroller() instanceof EOFrameController) {
			((EOFrameController)supercontroller()).closeWindow();
		}	
	}
	// accesseurs
	/** retourne true si une modification est en cours */
	public boolean modificationEnCours() {
		return modificationEnCours;
	}
	public void setModificationEnCours(boolean aBool) {
		modificationEnCours = aBool;
		// pour interdire/autoriser la fermeture de la fenêtre
		lockerFenetre(aBool);
	}
	/** retourne true si on souhaite que le premier objet soit s&eacute;lectionn&eacute; apr&egrave;s un fetch */
	public boolean selectionApresFetch() {
		return selectionApresFetch;
	}
	/** true si on souhaite que le premier objet soit s&eacute;lectionn&eacute; apr&egrave;s un fetch */
	public void setSelectionApresFetch(boolean selectionApresFetch) {
		this.selectionApresFetch = selectionApresFetch;
	}
	/** surcharge du parent */
	public boolean saveIfUserConfirms(String operationTitle,String message) {
		if (isEdited()) {
			boolean result =  EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous abandonner les modifications ?", "Oui","Non");
			if (result) {
				// abandon des modifications
				annuler();
			}
			return result;
		} else {
			return true;
		}
	}
	// délégation du display group   
	public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
		if (group == displayGroup()) {
			return modificationEnCours == false;
		} else {
			return true;
		}
	}
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
		LogManager.logDetail(getClass().getName() + " : displayGroupDidChangeSelection");
	} 

	// actions
	/** Action pour ajouter : ouvre un dialogue de modification */
	public void ajouter() {
		afficherPageModification(true);
	}
	/** Action pour modifier : ouvre un dialogue de modification */
	public void modifier() {
		afficherPageModification(false);
	}
	/** Action pour supprimer. La suppression sera enregistr&eacute;e dans la base */
	public void supprimer() {
		LogManager.logDetail(getClass().getName() + " : suppression");
		if (EODialogs.runConfirmOperationDialog("Alerte",messageConfirmationDestruction(),"Oui","Non")) {
			if (traitementsPourSuppression()) {	
				if (save()) {
					LogManager.logDetail(getClass().getName() + " : enregistrement");
					traitementsApresSuppression();
				}
			} else {
				LogManager.logDetail(getClass().getName() + " : revert");
				revertChanges(false);
				traitementsApresRevert();
			}
		}
	}
	/** Action pour valider. Enregistre les donn&eacute;es dans la base et ferme la fen&ecirc;tre si param&eacute;tr&eacute; &agrave;
	 * l'initialisation */
	public void valider() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (traitementsAvantValidation()) {
			LogManager.logDetail(getClass().getName() + " : Validation donnees");
			if (invisibleApresValidation) {
				saveAndMakeInvisible();
			} else if (save()) {
				modificationEnCours = false;	// 22/09/09 - déplacer pour permettre aux sous-classes de changer le cours du traitement
				traitementsApresValidation();
			}
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	/** Action pour annuler les modifications. Effectue un revert et ferme la fen&ecirc;tre si param&eacute;tr&eacute; &agrave;
	 * l'initialisation */
	public void annuler() {
		if (invisibleApresValidation) {
			revertAndMakeInvisible();
		} else {
			// la fenêtre restant affichée, on se remet dans l'état initial sans demander son avis à l'utilisateur
			revertChanges(false);
			traitementsApresRevert();
		}
		LogManager.logDetail(getClass().getName() + " :annulation");
	}
	/** Action pour imprimer : doit &ecirc;tre surcharg&eacute;e lorsque l'impression est possible dans la page. */
	public void imprimer() {
	}
	// Notification reçue pour synchronisation des données
	/** Me&eacute;thode d&eacute;clench&eacute;e suite &agrave; une notification de raffraichissement des associations */
	public void raffraichir(NSNotification aNotif) {
		LogManager.logDetail(getClass().getName() + " : raffraichissement");
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	/** Me&eacute;thode d&eacute;clench&eacute;e suite &agrave; une notification de resynchronisation des donn&eacute;es */
	public void synchroniser(NSNotification aNotif) {
		if (displayGroup() == null) {
			if (controllerDisplayGroup() != null) {
				controllerDisplayGroup().redisplay();
			}
			return;
		}
		LogManager.logDetail(getClass().getName() + " : synchronisation des donnees");
		// Ne gère la synchronisation que pour les objets de la même classe que le display group
		try {
			String nomClasse = (String)aNotif.object();
			LogManager.logDetail(getClass().getName() + " : synchronisation des donnees - classe " + nomClasse);
			String nomClasseDisplayGroup = "";
			if (displayGroup().displayedObjects().count() > 0) {
				nomClasseDisplayGroup = displayGroup().displayedObjects().objectAtIndex(0).getClass().getName();
			} else {
				// créer un objet bidon pour déterminer sa classe
				nomClasseDisplayGroup = displayGroup().dataSource().classDescriptionForObjects().createInstanceWithEditingContext(null,null).getClass().getName();
			}
			LogManager.logDetail(getClass().getName() + " :nom classe DG " + nomClasseDisplayGroup);
			// Si la synchronisation concerne un objet de la même classe que le display group, on gère la synchronisation
			if (nomClasse.equals(nomClasseDisplayGroup)) {
				initialiserDisplayGroup();
				modificationEnCours = false;
			}
			// forcer le réaffichage des boutons
			if (controllerDisplayGroup() != null) {
				controllerDisplayGroup().redisplay();
			}
		} catch (Exception e) {
			LogManager.logErreur("La notification de synchronisation attend un objet de type String");
			LogManager.logException(e);
		}
	}
	// méthodes du contrôleur display group
	/** M&eacute;thode du controller DG pour autoriser la cr&eacute;ation d'objet. On peut saisir si il n'y a pas de modification
	 * en cours et que les conditions sur la page sont OK */
	public boolean modeSaisiePossible() {
		return !modificationEnCours() && conditionSurPageOK();
	}
	/** M&eacute;thode du controller display group pour autoriser les boutons de modification et suppression. On peut modifier/supprimer
	 * si le display group a un objet s&eacute;lectionn&eacute; et que la saisie est possible */
	public boolean boutonModificationAutorise() {
		return displayGroup() != null && displayGroup().selectedObject() != null && modeSaisiePossible();
	}
	/** M&eacute;thode du controller display group pour autoriser les boutons d'affichage de pages secondaires. On peut afficher une
	 * fonctionnalit&eacute; secondaire si le display group a un objet s&eacute;lectionn&eacute; et qu'on n'est pas en mode
	 * modification */
	public boolean boutonAffichageSecondaireAutorise() {
		return displayGroup() != null && displayGroup().selectedObject() != null && !modificationEnCours();
	}
	/** M&eacute;thode du controller display group pour autoriser le bouton de validation. On peut valider si il y a eu
	 * des modifications */
	public boolean peutValider() {
		return isEdited();
	}
	// méthode de délégation du distribution channel
	/** M&eacute;thode de d&eacute;l&eacute;gation du gestionnaire d'erreurs */
	public Throwable distributionChannelShouldThrowServerException(EODistributionChannel channel,
			Throwable clientExceptionForServerException,
			String originalServerExceptionClassName,
			String originalServerExceptionMessage) {
		// raffraîchir les données à partir des données du serveur
		LogManager.logErreur("distributionChannelShouldThrowServerException classe erreur : " + clientExceptionForServerException.getClass());
		String debString = "Mangue -";
		if (originalServerExceptionMessage != null && originalServerExceptionMessage.startsWith(debString)) {
			EODialogs.runInformationDialog("",originalServerExceptionMessage.substring(debString.indexOf("-")+1));
		} else {
			LogManager.logException(clientExceptionForServerException);
			LogManager.logErreur("nom classe exception " + originalServerExceptionClassName);
			EODialogs.runInformationDialog("","Une erreur est survenue.\n" + originalServerExceptionMessage + "\n" + "Erreur : " + originalServerExceptionClassName);
		}
		if (clientExceptionForServerException.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException") == false) {
			editingContext().revert();
			EOApplication.sharedApplication().refreshData();
		}
		return null;
	}

	// méthodes protégées
	/** Traitements &agrave; ex&eacute;cuter avant la destruction d'un objet.<BR>
	 * Return true si la suppression s'est bien pass&eacute;e */
	protected abstract boolean traitementsPourSuppression();
	/** M&eacute;thode &agrave; surcharger pour faire des traitements apr&egrave;s une suppression de donn&eacute;es dans la base.<BR>
	 * Dans la m&eacute;thode surcharg&eacute;e il est imp&eacute;ratif d'appeler super */
	protected void traitementsApresSuppression() {
		LogManager.logDetail(getClass().getName() + " : traitementsApresSuppression");
		if (displayGroup() != null) {
			displayGroup().setSelectedObject(null);
		}
	}
	/** Retourne le message de confirmation aupr&egrave;s de l'utilisateur avant d'effectuer une suppression  */
	protected abstract String messageConfirmationDestruction();
	/** Mm&eacute;thode du document controller appel&eacute;e en cas de probl&egrave;me pendant le save */
	protected void saveFailed(Exception exception,boolean showErrorDialog,String saveOperationTitle) {
		if (exception.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
			LogManager.logInformation(this.getClass().getName() + " - erreur lors du validateForSave " + exception.getMessage());
			afficherExceptionValidation(exception.getMessage());
		} else {
			LogManager.logErreur("Erreur lors de l'enregistrement des donnees");
			LogManager.logException(exception);
			EODialogs.runErrorDialog("Erreur","Erreur lors de l'enregistrement des donnees !\nMESSAGE : " + exception.getMessage());
			revertChanges(false);
			traitementsApresRevert();
		}
	}
	/** Pr&eacute;pare la liste d'affichage et les boutons et fetch le display group.<BR>
	 * M&eacute;thode &agrave; surcharger pour faire des initialisations une fois l'archive charg&eacute;e<BR>
	 * les sous-classes doivent invoquer la m&eacute;thode de la super classe : l'appel &agrave; super dans la sous-classe
	 * est &agrave; faire &agrave; la fin de la m&eacute;thode si on ne veut pas que le display group soit charg&eacute; 
	 * avec les objets fetch&eacute;s */
	protected void preparerFenetre() {
		LogManager.logDetail("Preparation fenetre : " + getClass().getName());
		if (listeAffichage != null) {
			GraphicUtilities.changerTaillePolice(listeAffichage,11);
			listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			if (!estEditable) {
				GraphicUtilities.rendreNonEditable(listeAffichage);
				if (gereDoubleClic && conditionSurPageOK()) {	// pas d'action possible du double-clic si l'utilisateur n'a pas les droits
					listeAffichage.table().addMouseListener(new DoubleClickListener());
				}
			}
		}
		if (!conditionSurPageOK()) {
			if (vueBoutonsModification != null) {
				vueBoutonsModification.setVisible(false);
			}
		} else {
			// se préparer pour recevoir les notifications de synchronisation si la création/modification est permise
			if (vueBoutonsModification != null) {
				NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniser", new Class[] {NSNotification.class}),
						SYNCHRONISER,null);
			}
		}
		if (displayGroup() != null) {
			displayGroup().setSelectsFirstObjectAfterFetch(selectionApresFetch);
			// pour établir des critères de tri ou d'affichage
			parametrerDisplayGroup(); 
			initialiserDisplayGroup();
		}


		// s'installer pour recevoir les notifications du distribution context
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
		store.distributionChannel().setDelegate(this);

		modificationEnCours = false;
	}
	/** m&eacute;thode &agrave; invoqu&eacute;e pour initialiser le display group */
	protected void initialiserDisplayGroup() {
		if (conditionsOKPourFetch()) {
			LogManager.logDetail(getClass().getName() + " :initialisation du display group");

			modifierDisplayGroupAvecObjets(fetcherObjets());
		}	
	}     
	/** permet d'interdire/activer la fermeture de la fen&ecirc;tre en supprimant/ajoutant le window listener */
	protected void lockerFenetre(boolean lock) {
		if (supercontroller() != null && supercontroller() instanceof EOFrameController) {
			EOFrameController controller = (EOFrameController)supercontroller();
			if (lock) {
				WindowListener[] listeners = controller.window().getWindowListeners();
				for (int i = 0; i < listeners.length;i++) {
					if (listeners[i] == controller) {
						controller.window().removeWindowListener((WindowListener)listeners[i]);
					}
				}
				LogManager.logDetail("Lock fenetre : " + getClass().getName());
			} else {
				controller.window().addWindowListener(controller);
				LogManager.logDetail("Unlock fenetre : " + getClass().getName());
			}
		}
	}
	protected void modifierDisplayGroupAvecObjets(NSArray objets) {
		if (displayGroup() != null) {
			if (selectionApresFetch() == false) {
				displayGroup().setSelectedObject(null);	// en premier pour que cela déclenche d'abord toutes les notifications ad-hoc
			}
			displayGroup().setObjectArray(objets);
			displayGroup().updateDisplayedObjects();
		}
	}       
	/** m&eacute;thode &agrave; surcharger pour retourner les objets affich&eacute;s par le display group */
	protected abstract NSArray fetcherObjets();
	/** m&eacute;thode &agrave; surcharger pour param&eacute;trer le display group (tri ou filtre) :
	 * Attention en cas de filtre de ne pas mettre des crit&egrave;res qui rendraient les objets invisibles apr&egrave;s cr&eacute;ation.
	 * Utiliser plut&ocirc;t dans les filtres de comparaisons sur la diff&eacute;rence que sur l'&eacute;gali&eacute; */
	protected abstract void parametrerDisplayGroup();
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		return true;
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions sont OK pour ex&eacute;cuter le fetch dans le display group principal */
	protected abstract boolean conditionsOKPourFetch();
	/** m&eacute;thode &agrave; surcharger pour faire des traitements avant l'enregistrement des donn&eacute;es dans la base
	 * retourne true si on peut enregistrer */
	protected abstract boolean traitementsAvantValidation();
	/** m&eacute;thode &agrave; surcharger pour faire des traitements apr&egrave;s l'enregistrement des donn&eacute;es dans la base */
	protected abstract void traitementsApresValidation();
	/** m&eacute;thode &agrave; surcharger pour faire des traitements apr&egrave;s un revert des donn&eacute;es dans la base
	 *  Dans la m&eacute;thode surcharg&eacute;e il est imp&eacute;ratif d'appeler super */
	protected void traitementsApresRevert() {
		LogManager.logDetail(getClass().getName() + " : traitementsApresRevert");
		if (displayGroup() != null) {
			displayGroup().updateDisplayedObjects();
		}
		modificationEnCours = false;
	}
	/** affiche un message suite &agrave; une exception de validation */
	protected abstract void afficherExceptionValidation(String message);
	/** m&eacute;thode &agrave; surcharger pour terminer avant fermeture de la fen&egrave;tre */
	protected abstract void terminer();
	//  méthodes privées
	private void afficherPageModification(boolean estCreation) {
		if (nomClasseCreation != null) {
			try {
				Class classe = Class.forName(nomClasseCreation);
				ModelePageModification controleur;
				// instancier le controleur
				if (estCreation) {
					controleur = (ModelePageModification)classe.newInstance();
					LogManager.logDetail("Creation d'un objet pour la classe " + nomClasseCreation);
				} else {
					Constructor constructor = classe.getConstructor(new Class[] {EOGlobalID.class});
					LogManager.logDetail("Modification pour la classe " + nomClasseCreation + " de l'objet " + displayGroup().selectedObject());
					EOGlobalID objetCourant = editingContext().globalIDForObject((EOGenericRecord)displayGroup().selectedObject());
					controleur = (ModelePageModification)constructor.newInstance(new Object[] {objetCourant});
				}
				modificationEnCours = true;
				// forcer le réaffichage des boutons
				controllerDisplayGroup().redisplay();
				// afficher l'interface
				EOFrameController.runControllerInNewFrame(controleur,titreFenetreCreation); 
			} catch (Exception e) {
				LogManager.logException(e);
			}			
		}
	}

	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				if (!modificationEnCours) {
					modifier();
				}
			}
		}
	}
}
