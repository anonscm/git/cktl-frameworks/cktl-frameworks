/*
 * Created on 19 juil. 2005
 *
 * Modélise toutes les pages qui gèrent des données de l'individu
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

/**
 * @author christine<BR>
 *
 * Mod&eacute;lise toutes les pages qui g&egrave;rent des donn&eacute;es de l'individu
 */
public abstract class ModelePageAvecIndividu extends ModelePageComplete implements InterfacePageAvecIndividu {
    private Number individuCourantID;

    public ModelePageAvecIndividu() {
		super();
}
    public ModelePageAvecIndividu(EOEditingContext edc) {
		super(edc);
}
    public void initialiser(Number individuID,boolean estEditable,boolean gereDoubleClic) {
        super.initialiser(estEditable,gereDoubleClic);
        this.individuCourantID = individuID;
    }
    public void initialiser(Number individuID) {
    	initialiser(individuID,false,true);
    }
    // Notifications
    /** Suite &agrave; une notification que l'individu courant a chang&eacute;. L'objet de la notification
     * contient le noIndividu. Refetch le display group */
    public void employeHasChanged(NSNotification aNotif) {
		if (aNotif.object() == null) {
			LogManager.logDetail("Pas d'individu selectionne pour la page " + getClass().getName());
			nettoyer();
		} else { 
			individuCourantID = (Number)aNotif.object();
    		if (individuCourantID != null) {
    			changerIndividu();
    			displayGroup().setObjectArray(fetcherObjets());	// A REVOIR
    				LogManager.logDetail(identiteIndividu() + " : selectionne pour la page " + getClass().getName());
    			} else {
    				LogManager.logDetail(" Individu inconnu selectionne pour la page " + getClass().getName());
    				displayGroup().setObjectArray(null);
    			}
    			displayGroup().setSelectionIndexes(new NSArray(new Integer(0)));
    			updaterDisplayGroups();
    		}
    }
    /** Suite &agrave; une notification pour signaler qu'il n'y a plus d'individu s&eacute;lectionn&eacute;. Refetch le display group */
   public void nettoyer(NSNotification aNotif) {
    	LogManager.logDetail("Nettoyer pour la page " + getClass().getName());
    	nettoyer();
    }
    // méthodes du controller DG
   /** Return true si la saisie est possible et que l'individu courant n'est pas nul */
    public boolean modeSaisiePossible() {
    		if (super.modeSaisiePossible()) {
    			return individuCourantID() != null;
    		} else {
    			return false;
    		}
    }
    // méthodes protégées
    /** On peut fetcher si l'individu courant existe */
    protected boolean conditionsOKPourFetch() {
    	return individuCourantID() != null;
    }
   /** Affiche un message suite &agrave; une exception de validation */
   protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR","MANGUE -" + message);
		controllerDisplayGroup().redisplay();
	}
    /** Retourne le noIndividu de l'individu courant */
    protected Number individuCourantID() {
    	return individuCourantID;
    }
    protected void setIndividuCourantID(Number individuID) {
    	individuCourantID = individuID;
    }
    /** Met &agrave; nul l'individu courant et les objets du display group */
    protected void nettoyer() {
    	individuCourantID = null;
    	changerIndividu();
		displayGroup().setSelectedObject(null);
		displayGroup().setObjectArray(null);
		updaterDisplayGroups();
	}
    // Méthodes abstraites
    /** Pour modifier l'individu courant */
    protected abstract void changerIndividu();
    /** Retourne l'identit&eacute; de l'individu */
    protected abstract String identiteIndividu();
}
