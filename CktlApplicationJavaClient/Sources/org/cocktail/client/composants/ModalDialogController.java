/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import java.awt.event.WindowListener;
import java.lang.reflect.Method;

import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EOComponentController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/**  g&egrave;re un composant dans un dialogue modal<BR>
 * @author christine
 *
 */
public class ModalDialogController extends EOModalDialogController {
	private EOComponentController controleurPrincipal;

	public ModalDialogController(EOComponentController controleur,String titreFenetre) {
		super();
		controleurPrincipal = controleur;
		addSubcontroller(controleur);
		component().setSize(controleur.component().getSize());
		setWindowPosition(EOComponentController.Center);
		window().setName(titreFenetre);
	}
	public void activateWindow() {
		if (controleurPrincipal instanceof ModelePageComplete == false) {
			// il s'agit d'une page qui est déjà en mode modification ou dans laquelle la saisie est contrainte
			lockerFenetre();
		} else {
			// pour les pages complètes on ne lock la fenêtre qu'en mode modification
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockFenetre", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockFenetre", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
		}
		super.activateWindow();
	}
	public boolean closeWindow() {
		NSNotificationCenter.defaultCenter().removeObserver(this);
		try {
			Method methode = controleurPrincipal.getClass().getMethod("modificationEnCours");
			// invoquer la méthode pour vérifier si une modification est en cours
			Boolean result = (Boolean)methode.invoke(controleurPrincipal);
			if (result.booleanValue()) {
				try {
					// vérifier si le controleur principal est une sous-classe de EODocumentController
					methode = controleurPrincipal.getClass().getMethod("isEdited");
					// invoquer la méthode pour vérifier si une modification est en cours
					result = (Boolean)methode.invoke(controleurPrincipal);
				} catch (Exception e) {}
				if (result.booleanValue()) {
					// modification en cours
					if (EODialogs.runConfirmOperationDialog("Attention","Une modification est en cours, voulez-vous vraiment fermer la fenêtre ","Oui","Non")) {
						// vérifier si il existe une méthode d'annulation et l'activer
						methode = controleurPrincipal.getClass().getMethod("annuler");
						methode.invoke(controleurPrincipal);
						return super.closeWindow();
					}
				} else {
					super.closeWindow();
				}
			} else {
				super.closeWindow();
			}
		} catch (NoSuchMethodException e) {
			// le controleur ne supporte pas cette méthode, on ferme directement la fenêtre
			return super.closeWindow();
		} catch (Exception e) {
			LogManager.logException(e);
			return super.closeWindow();
		}
		return false;
	}
	// Notifications
	/** interdit la fermeture de la fen&ecir;tre en cliquant sur la case de fermeture */
	public void lockFenetre(NSNotification aNotif) {
		if (aNotif.object() == controleurPrincipal) {
			lockerFenetre();
		}
	}
	/** permet la fermeture de la fen&ecir;tre en cliquant sur la case de fermeture */
	public void unlockFenetre(NSNotification aNotif) {
		if (aNotif.object() == controleurPrincipal) {
			window().addWindowListener(this);
		}
	}
	/** permet d'interdire la fermeture de la fen&ecirc;tre  en cliquant dans la case de fermeture */
	protected void lockerFenetre() {
		WindowListener[] listeners = window().getWindowListeners();
		for (int i = 0; i < listeners.length;i++) {
			if (listeners[i] == this) {
				window().removeWindowListener((WindowListener)listeners[i]);
			}
		} 
		window().repaint();
	} 
	public void finishModal() {
		super.finishModal();
	}
}
