/*
 * Created on 19 mai 2005
 *
 * Décrit le modèle de base d'une page qui permet la création ou la modification de données. Communique avec une page d'affichage
 * en envoyant une notification pour la synchronisation du display group
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import java.awt.Cursor;

import org.cocktail.common.LogManager;
import org.cocktail.common.modele.SuperFinder;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;

/**
 * @author christine<BR>
 *
 * Mod&egrave;le de base d'une page qui permet la cr&eacute;ation ou la modification de donn&eacute;es. On est en modification &agrave; l'affichage
 * du composant<BR>
 * Pour utiliser cette classe :
 *<UL>cr&eacute;er une sous-classe java pour le contr&ocirc;leur</UL>
 *<UL>dupliquer le fichier nib ModelePageModification et lui donner le m&ecirc;me nom que la classe java du contr&ocirc;leur</UL>
 *<UL>dans le fichier nib :
 *	<UL>remplacer le display group par le display group r&eacute;el</UL>
 *	<UL>d&eacute;sactiver le fetch on load</UL>
 *	<UL>cr&eacute;er l'interface de la fen&ecirc;tre</UL></UL>
 *	Pour faire des initialisations avant l'affichage de la fen&ecirc;tre, surcharger la m&eacute;thode preparerFenetre<BR>
 *	Lors de l'utilisation d'une sous-classe dans un dialogue, invoquer directement la m&eacute;thode preparerFenetre apr&egrave;s
 *  le chargement de l'archive<BR>
 *  Pour faire des op&eacute;rations avant fermeture de la fen&ecirc;tre, surcharger la m&eacute;thode terminer<BR>
 *	Lors de l'utilisation d'une sous-classe dans un dialogue, invoquer directement la m&eacute;thode terminer avant de fermer la fen&ecirc;tre<BR>
 */
public abstract class ModelePageModification extends EOInterfaceController {
	private EOGlobalID objetAModifierID;
	private boolean surveillerModification;	// pour éviter que le document soit marqué comme modifié quand il n'y a pas eu encore de modification
	private boolean archiveLoaded;
	
	public ModelePageModification() {
		super();
		objetAModifierID = null;
	}
	public ModelePageModification(EOEditingContext substitutionEditingContext) {
		super(substitutionEditingContext);
		objetAModifierID = null;
	}
	
	/** constructeur
	 * @param objetAModifier global ID de l'objet pour lequel on veut effectuer des modifications ou null si cr&eacute;ation
	 */
	public ModelePageModification(EOGlobalID objetAModifier) {
		super();
		this.objetAModifierID = objetAModifier;
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void prepareComponent() {
		// On vérifie isConnected car cette méthode est appelée après establishConnection dans le cas où on charge
		// une interface via EOFrameController.runControllerInNewFrame
		if (archiveLoaded == false) {
			try {
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.prepareComponent();
			}
			archiveLoaded = true;
		}
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void establishConnection() {
		if (archiveLoaded == false) {
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
				setConnected(true);
				connectionWasEstablished();
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.establishConnection();
			}
			archiveLoaded = true;
		}
	}
	public void connectionWasEstablished() {
		preparerFenetre();
	}
	public void connectionWasBroken() {
		terminer();
	}
	/** M&eacute;thode &agrave; invoquer lors de l'affichage du composant via un ModalDialogController */
	public void activer() {
		preparerFenetre();
	}
	public void setEdited(boolean flag) {
		if (surveillerModification) {
			super.setEdited(flag);
			controllerDisplayGroup().redisplay();
		} else {
			surveillerModification = true;	// dorénavant prendre en compte toutes les modifications
		}
	}
	public boolean saveIfUserConfirms(String operationTitle,String message) {
		if (isEdited()) {
			boolean result =  EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous abandonner les modifications en cours ?", "Oui","Non");
			if (result) {
				// abandon des modifications
				annuler();
			}
			return result;
		} else {
			return true;
		}
	}

	// méthodes du controller DG
	/** M&eacute;thode du controller display group : retourne true si il y a eu des modifications */
	public boolean peutValider() {
		return isEdited();
	}
	// actions 
	/** Action pour annuler : revert et ferme la fen&circ;tre. Poste une notification pour delocker les fen&circ;tres */
	public void annuler() {
		LogManager.logDetail(this.getClass().getName() + " - annuler");
		revertAndMakeInvisible();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,null);
	}
	/** Action pour valider : enregistre les modifications et ferme la fen&circ;tre. Poste une notification pour delocker les fen&circ;tres */
	public void valider() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (traitementsAvantValidation()) {
			if (saveAndMakeInvisible()) {
				LogManager.logDetail(this.getClass().getName() + " - valider");
				envoyerNotifSynchronisation();
				NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,null);
			}
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	/** Action pour valider : enregistre les modifications sans fermer la fen&circ;tre. Poste une notification pour synchroniser les autres fen&circ;tres */
	public boolean validerSansFermerFenetre() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		boolean validationOK = traitementsAvantValidation();
		if (validationOK) {
			validationOK = save();
			if (validationOK) {
				LogManager.logDetail(this.getClass().getName() + " - valider");
				envoyerNotifSynchronisation();
			}
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		return validationOK;
	}
	// méthodes protégées
	/** traitements &agrave; ex&eacute;cuter suite &agrave; la cr&eacute;tion d'un objet */
	protected abstract void traitementsPourCreation();
	/** m&eacute;thode &agrave; surcharger pour faire des traitements avant l'enregistrement des donn&eacute;es dans la base
	 * retourne true si on peut enregistrer */
	protected abstract boolean traitementsAvantValidation();
	/** Meacute;thode &agrave; surcharger pour faire des initialisations une fois l'archive charg&eacute;e en invoquant
	 * un appel &agrave; la m&eacute;thode de la super classe. Si pas d'objet &agrave; modifier, cr&eacute;e un nouvel
	 * objet et invoque traitementsPourCreation sinon initialise le display group avec cet objet et le s&eacute;lectionne.<BR>
	 * L'appel &agrave; super doit &ecirc;tre fait apr&egrave;s si on ne veut pas que le display group soit initialis&eacute; */
	protected void preparerFenetre() {
		LogManager.logDetail(this.getClass().getName() + " - preparerFenetre");
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		if (objetAModifierID == null) {
			// création d'un objet dans le display group
			insertObject();
			traitementsPourCreation();
			surveillerModification = true;
		} else {
			EOGenericRecord objetAModifier = (EOGenericRecord)SuperFinder.objetForGlobalIDDansEditingContext(objetAModifierID,editingContext());
			displayGroup().setObjectArray(new NSArray(objetAModifier));
			displayGroup().setSelectedObject(objetAModifier);
			surveillerModification = false;
		}
	}
	protected void updaterDisplayGroups() {
		displayGroup().redisplay();
		controllerDisplayGroup().redisplay();
	}
	/** M&eacute;thode du document controller appel&eacute;e en cas de probl&egrave;me pendant le save */
	protected void saveFailed(Exception exception,boolean showErrorDialog,String saveOperationTitle) {
		if (exception.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
			LogManager.logInformation(this.getClass().getName() + " - erreur lors du validateForSave " + exception.getMessage());
			EODialogs.runErrorDialog("Erreur",exception.getMessage());
		} else {
			LogManager.logException(exception);
			EODialogs.runErrorDialog("Erreur","Erreur lors de l'enregistrement des donnees !\nMESSAGE : " + exception.getMessage());
			revertChanges(false);
		}
	}

	/** M&eacute;thode &agrave; surcharger pour terminer avant fermeture de la fen&egrave;tre en invoquant
	 * un appel &agrave; la m&eacute;thode de la super classe */
	protected void terminer() {
		// supprimer l'écoute des notifications
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	/** Ajoute une relation &grave; un objet source
	 * @param source source de la relation	
	 * @param destin globalID de l'objet destinataire de la relation, peut &ecirc;tre nul
	 * @param nomRelation cl&eacute; utilis&eacute;e pour d&eacute;finir cette relation
	 */
	protected boolean ajouterRelation(EOGenericRecord source, Object destin,String nomRelation) {
		if (destin != null) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)destin,editingContext());
			if (source != null && recordDestin != null) {
				LogManager.logDetail(this.getClass().getName() + " - ajouterRelation : " + nomRelation + ", objet : " + recordDestin);
				source.addObjectToBothSidesOfRelationshipWithKey(recordDestin,nomRelation);
				displayGroup().updateDisplayedObjects();
				return true;
			}
		}
		return false;
	}
	// méthodes privées
	private void envoyerNotifSynchronisation() {
		String classeObjet = displayGroup().allObjects().objectAtIndex(0).getClass().getName();
		// on pase en paramètre la classe de l'objet modifié pour qu'on puisse vérifier lors de la réception de la notification
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.SYNCHRONISER,classeObjet);
	}
}



