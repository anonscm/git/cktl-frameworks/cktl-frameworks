/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import org.cocktail.client.components.DialogueWithDisplayGroup;

import com.webobjects.foundation.NSNotificationCenter;

/** Mod&eacute;lise une fen&ecirc;tre d'interface secondaire qui ne sera pas affich&eacute;e dans une vue &grave; onglets */
public abstract class FenetreSecondaire extends DialogueWithDisplayGroup {
	/** Notification envoy&eacute;e signalant que le contr&ocirc;leur n'est plus &grave; suivre : l'objet de la notificatin
	 * contient le nom de la fen&ecirc;tre */
	public final static String NOTIFICATION_ARRET_CONTROLEUR = "NotificationArreterControleur";
	private boolean fermetureExterieure;
	private String titreFenetre;
	
	/** constructeur pour cr&eacute; une fen&circ;tre secondaire
	 * @param titreFenetre titre de la fen&circ;tre
	 * @param isModal true si la fen&ecirc;tre est modale */
	public FenetreSecondaire(String titreFenetre,boolean isModal) {
		super(null,null,null,titreFenetre,false);
		this.titreFenetre = titreFenetre;
		fermetureExterieure = false;
	}
	/** constructeur pour cr&eacute; une fen&circ;tre secondaire non modale
	 * @param titreFenetre titre de la fen&circ;tre */
	public FenetreSecondaire(String titreFenetre) {
		this(titreFenetre,false);
	}
	public void connectionWasBroken() {
		if (!fermetureExterieure) {
			NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_ARRET_CONTROLEUR,titreFenetre);
		}
	}

	public void arreter() {
		/*if (supercontroller() instanceof EOFrameController) {
			((EOFrameController)supercontroller()).closeWindow();
			fermetureExterieure = true;
		} else if (supercontroller() instanceof EODialogController) {
			((EODialogController)supercontroller()).closeWindow();
			fermetureExterieure = true;
		} */
		super.terminate();
		fermetureExterieure = true;
	}
	// Méthodes protégées
	/** pas de s&eacute;lection d'objet dans une fen&circ;tre secondaire */
	protected Object selectedObject() {
		return null;
	}
}
