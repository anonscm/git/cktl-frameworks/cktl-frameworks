/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import javax.swing.JComboBox;

import org.cocktail.common.DateCtrl;
import org.cocktail.common.LogManager;
import org.cocktail.common.Outils;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOArchiveController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/** G&egrave;re la saisie de dates avec une date d&eacute;but, une date fin et un popup pour d&eacute;finir les p&eacute;riodes. Communique avec
 * les composants qui l'incorporent par une notification
 * @author christine
 *
 */
public class GestionPeriode extends EOArchiveController {
	/** Outlet */
	public JComboBox popupPeriode;
	private String debutPeriode,finPeriode;
	private boolean actionEnCours,envoyerNotification,modificationPossible, demarrageEnCours;
	private int selectionInitiale;	// sélection initiale du popup
	 /** p&eacute;riode d'affichage des cong&eacute;s */
	 public static int TOUTE_PERIODE = 0,PERIODE_SAISIE = 1,ANNEE_CIVILE = 2,ANNEE_UNIVERSITAIRE = 3;
    /** Notification envoy&eacute;e pour changer la p&eacute;riode. Envoie un dictionnaire contenant les cl&eacute;s "dateDebut" et/ou "dateFin",
     * valeurs NSTimestamp */
    public static String NOTIFICATION_PERIODE_HAS_CHANGED = "NotificationChangerPeriode";
    /** constructeur
     * @param dateDebut date d&eacute;but de la p&eacute;riode
     * @param dateFin  date fin de la p&eacute;riode
     * @param s&eacute;lection initiale dans le popup
     * @param modificationPossible	true si les dates peuvent &ecirc;tre modifi&eacute;es
     */
	public GestionPeriode(String dateDebut,String dateFin,int selectionInitiale,boolean modificationPossible) {
		super();
		demarrageEnCours = true;
		debutPeriode = dateDebut;
		finPeriode = dateFin;
		this.selectionInitiale = selectionInitiale;
		this.modificationPossible = modificationPossible;
	}
    /** constructeur
     * @param dateDebut date d&eacute;but de la p&eacute;riode
     * @param dateFin  date fin de la p&eacute;riode
     * @param s&eacute;lection initiale dans le popup
     */
	public GestionPeriode(String dateDebut,String dateFin,int selectionInitiale) {
		this(dateDebut, dateFin, selectionInitiale, true);
	}
	/** m&eacute;thode d'initialisation */
	public void init() {
		EOArchive.loadArchiveNamed("GestionPeriode",this,"org.cocktail.client.composants.interfaces",null);
		demarrageEnCours = false;
		envoyerNotification = true;
		popupPeriode.setSelectedIndex(selectionInitiale);
		popupPeriode.setEnabled(modificationPossible);
	}
	// Accesseurs
	public boolean modificationPossible() {
		return modificationPossible;
	}
	public void setModificationPossible(boolean aBool) {
		this.modificationPossible = aBool;
		popupPeriode.setEnabled(aBool);
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	public String debutPeriode() {
		return debutPeriode;
	}
	public void setDebutPeriode(String debut) { 
			if (debut == null) {
			this.debutPeriode = null;
		} else {
			this.debutPeriode = DateCtrl.dateCompletion(debut);
		}
		if (!actionEnCours) {		// l'utilisateur n'a pas cliqué sur le popup mais a saisi une date fin
			if (finPeriode != null && debutPeriode != null && DateCtrl.isBefore(DateCtrl.stringToDate(finPeriode),DateCtrl.stringToDate(debutPeriode))) {
				EODialogs.runErrorDialog("Erreur","La date de fin ne peut être antérieure à la date de début");
				debutPeriode = null;
			}
			envoyerNotification = true;
			popupPeriode.setSelectedIndex(PERIODE_SAISIE);
		}
	}
	public String finPeriode() {
		return finPeriode;
	}
	public void setFinPeriode(String fin) {
		if (fin == null) {
			this.finPeriode = null;
		} else {
			this.finPeriode = DateCtrl.dateCompletion(fin);
		}
		if (!actionEnCours) {		// l'utilisateur n'a pas cliqué sur le popup mais a saisi une date fin
			if (finPeriode != null && debutPeriode != null && DateCtrl.isBefore(DateCtrl.stringToDate(finPeriode),DateCtrl.stringToDate(debutPeriode))) {
				EODialogs.runErrorDialog("Erreur","La date de fin ne peut être antérieure à la date de début");
				finPeriode = null;
			}
			envoyerNotification = true;
			popupPeriode.setSelectedIndex(PERIODE_SAISIE);
		}
	}
	public NSTimestamp dateDebut() {
		if (debutPeriode() != null) {
			return DateCtrl.stringToDate(debutPeriode());
		} else {
			return null;
		}
	}
	public NSTimestamp dateFin() {
		if (finPeriode() != null) {
			return DateCtrl.stringToDate(finPeriode());
		} else {
			return null;
		}
	}
	/** Action pour mettre la p&eacute;riode sur l'ann&eacute;e civile */
	public void choisirAnneeCivile() {
		popupPeriode.setSelectedIndex(ANNEE_CIVILE);
	}
	/** Action pour mettre la p&eacute;riode sur l'ann&eacute;e universitaire */
	public void choisirAnneeUniversitaire() {
		popupPeriode.setSelectedIndex(ANNEE_UNIVERSITAIRE);
	}
	/** Pour changer les dates de la p&eacute;riode */
	public void changerDates(String dateDebut,String dateFin) {
		envoyerNotification = false;
		debutPeriode = dateDebut;
		finPeriode = dateFin;
		if (debutPeriode == null && finPeriode == null) {
			popupPeriode.setSelectedIndex(TOUTE_PERIODE);
		} else {
			popupPeriode.setSelectedIndex(PERIODE_SAISIE);
		}
	}
	// Actions
	/** Action du popup de p&eacute;riode */
	public void datesHasChanged() {
		actionEnCours = true;
		if (popupPeriode == null || demarrageEnCours) {		// au démarrage  
			actionEnCours = false;
			return;
		}
		int index = popupPeriode.getSelectedIndex();

		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y");
		if (index == TOUTE_PERIODE) {
			setDebutPeriode(null);
			setFinPeriode(null);
		} else if (index == ANNEE_CIVILE) {
			setDebutPeriode(formatter.format(Outils.debutAnneeCivile()));
			setFinPeriode(formatter.format(Outils.finAnneeCivile()));
		} else if (index == ANNEE_UNIVERSITAIRE) {
			setDebutPeriode(formatter.format(Outils.debutAnneeUniversitaire()));
			setFinPeriode(formatter.format(Outils.finAnneeUniversitaire()));
		}

		controllerDisplayGroup().redisplay();
		if (envoyerNotification) {
			posterNotificationChangement();
		}
		actionEnCours = false;
	}
	// méthodes privées
	private void posterNotificationChangement() {
		NSMutableDictionary dict = new NSMutableDictionary();
		if (debutPeriode != null) {
			dict.setObjectForKey(dateDebut(),"dateDebut");
		}
		if (finPeriode != null) {
			dict.setObjectForKey(dateFin(),"dateFin");
		}
		LogManager.logDetail("GestionPeriode - changementDates " + dict);
		NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_PERIODE_HAS_CHANGED,dict,null);
	}
}
