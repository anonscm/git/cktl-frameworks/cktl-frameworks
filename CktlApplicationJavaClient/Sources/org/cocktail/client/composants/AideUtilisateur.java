/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import java.awt.Cursor;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;

import org.cocktail.client.impression.UtilitairesImpression;
import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;

/** Affiche une aide utilisateur avec une zone contenant du texte (html) provenant d'un composant wo et la possibili&eacute; 
 * d'afficher un pdf complet
 **/
public class AideUtilisateur extends FenetreSecondaire {
	public JScrollPane scrollPane;
	private String nomPageAide,documentationComplete;
	private EOEditingContext editingContext;
	/** Constructeur
	 * 
	 * @param nomPageAide nom du fichier d'aide
	 * @param documentationComplete nom du fichier pdf contenant la documentation compl&egrave;te (sans l'extension .pdf). Peut &ecir;tre nul
	 */
	public AideUtilisateur(String nomPageAide,String documentationComplete) {
		super("Aide Utilisateur");
		this.nomPageAide = nomPageAide;
		this.documentationComplete = documentationComplete;
		this.editingContext = new EOEditingContext();
		EOArchive.loadArchiveNamed("AideUtilisateur", this, "org.cocktail.client.composants.interfaces", this.disposableRegistry());
	}
	// Actions
	/** Action : pour afficher l'aide compl&egrave;te */
	public void afficherAideComplete() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		LogManager.logDetail("Impression aide complete");
		try {
			UtilitairesImpression.afficherPdfAvecMethode(editingContext,"clientSideRequestDocumentAide", new Class[] {String.class}, new Object[] {documentationComplete},documentationComplete);
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	// Méthodes du controller DG
	public boolean peutAfficherAide() {
		if (documentationComplete == null || documentationComplete.length() == 0) {
			return false;
		} else {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			Boolean result = (Boolean)store.invokeStatelessRemoteMethodWithKeyPath("session", "clientSideRequestExisteDocumentAide", new Class[] {String.class}, new Object[] {nomPageAide});
			return result.booleanValue();
		}
	}
	// Autres
	protected void prepareInterface() {
		String texteAide = htmlPageAide();
		if (texteAide != null && texteAide.length() > 0) {
			JEditorPane editorPane = new JEditorPane();
			editorPane.setEditable(false);
			editorPane.setSize(scrollPane.getViewport().getViewSize());
	        editorPane.setEditorKit(new HTMLEditorKit());
	        editorPane.setText(texteAide);
			scrollPane.getViewport().setView(editorPane);
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		} else {
			EODialogs.runErrorDialog("Erreur", "Page d'aide " + nomPageAide + " inconnue !!");
		}
	}
	private String htmlPageAide() {
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
		return (String)store.invokeStatelessRemoteMethodWithKeyPath("session", "clientSideRequestHtmlPourPageAide", new Class[] {String.class}, new Object[] {nomPageAide});

	}
}
