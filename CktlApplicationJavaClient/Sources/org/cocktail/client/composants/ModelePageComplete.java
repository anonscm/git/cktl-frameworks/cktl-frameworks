/*
 * Created on 13 juin 2005
 *
 * Classe abstraite qui modélise une page comp&egrave;te comportant un display group affiché dans une table
 * et dans laquelle les modifications se font dans la page
 * i.e on a deux jeux de bouton : créer/modifier/supprimer et valider/annuler. 
 * La page reste affich&eacute;e apr&egrave; validation
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.composants;

import javax.swing.JFrame;

import org.cocktail.client.InterfaceApplication;
import org.cocktail.common.LogManager;
import org.cocktail.common.modele.SuperFinder;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOWindowObserver;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;


/** Classe abstraite qui modelise une page comportant un display group affiche dans une table
 *
 * Classe abstraite qui modelise une page comportant un display group affiche dans une table<BR>
 * Les modifications se font directement dans la page<BR>
 * i.e on a deux jeux de bouton : creer/modifier/supprimer et valider/annuler. Une partie des comportements de la superclasse sont
 * surcharges.<BR>
 * Gere toutes les activations/desactivations de bouton et les actions associees<BR>
 * Implemente l'interface de gestion de la completion automatique de champs date<BR>
 */
public abstract class ModelePageComplete extends ModelePage {  
	/** Outlet sur la vue des boutons de validation */
	public EOView vueBoutonsValidation;
	private boolean	modeCreation;
	private boolean estLocke;
	private boolean afficherDialogue;	// pour gérer les notifications lors de l'activation de la fenêtre alors qu'elle est en modification

    public ModelePageComplete() {
		super();
    }
	public ModelePageComplete(EOEditingContext edc) {
		super(edc);
	}
	
	/** initialise la page
	 * @param estEditable true si on peut modifier les donnees dans la table du display group
	 * @param gereDoubleClic gestion par defaut du double-clic ?
	 */
	public void initialiser(boolean estEditable,boolean gereDoubleClic) {
		super.initialiser(estEditable,false,null,null,gereDoubleClic);
	}
	// accesseurs
	/** retourne true si on est en mode creation */
	public boolean modeCreation() {
		return modeCreation;
	}
	public void setModeCreation(boolean aBool) {
		modeCreation = aBool;
	}
	public boolean estLocke() {
		return estLocke;
	}
	/** Surcharge du parent pour notifier en mode fenetre que la fenetre active change */
	public void setModificationEnCours(boolean aBool) {
		// pour gérer les notifications lors de l'activation de la fenêtre alors qu'elle est en modification
		super.setModificationEnCours(aBool);
		if (((InterfaceApplication)EOApplication.sharedApplication()).modeFenetre() || 
				supercontroller() instanceof EOFrameController) {
			if (aBool) {
				afficherDialogue = true;
				NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("activeWindowDidChange", new Class[] {NSNotification.class}), EOWindowObserver.ActiveWindowChangedNotification, null);
			} else {
				NSNotificationCenter.defaultCenter().removeObserver(this,EOWindowObserver.ActiveWindowChangedNotification,null);
			}
		}
	}
	// actions
	/** Action pour ajouter : invoque traitementsPourCreation pour faire les traitements specifiques */
	public void ajouter() {
		LogManager.logDetail(getClass().getName() + " : ajout d'un objet de type : " + entityName());
		modeCreation = true;
		insertObject();
		traitementsPourCreation();
		// changer cet état après l'insert pour que la méthode de délégation shouldChangeSelection fonctionne
		setModificationEnCours(true);
		// afficher les boutons de validation
		afficherBoutons(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		updaterDisplayGroups();
	}
	/** Action pour modifier */
	public void modifier() {
		if (displayGroup() != null) {
			LogManager.logDetail(getClass().getName() + " : modification de l'objet : " + displayGroup().selectedObject());
		}
		modeCreation = false;
		setModificationEnCours(true);
		// afficher les boutons de validation
		afficherBoutons(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		raffraichirAssociations();	// dans le cas du double-clic, les associations ne sont pas raffraîchies
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	/** Action du bouton annuler */
	public void annuler() {
		LogManager.logDetail(getClass().getName() + " Annulation");
		editingContext().revert();
		setEdited(false);
		traitementsApresRevert();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
	}

	// Notifications
	//	Notification reçue lorsque l'utilisateur active une fenêtre ou un dialogue (envoyée par le WindowObserver)
	public void activeWindowDidChange(NSNotification notification) {
		// attention, le window observer envoie des floppées de notifications qu'on ne peut pas arrêter et pas nécessairement
		// dans l'ordre espéré. Il faut donc ajouter un booléen pour savoir si le dialogue a été affiché car son affichage génère lui-même
		// des notifications
		EOWindowObserver windowObserver = (EOWindowObserver)notification.object();
		if (windowObserver.activeWindow() != null && windowObserver.activeWindow() instanceof JFrame) {
			if (windowObserver.controllerForLatestDeactivatedWindow() == supercontroller() && windowObserver.controllerForActiveWindow() != supercontroller()) {
				if (afficherDialogue) {
					if (EODialogs.runConfirmOperationDialog("Attention","Modification de données en cours. Voulez-vous vraiment changer de fenêtre ?","Oui","Non")) {
						afficherDialogue = false;
					} else {
						((JFrame)((EOFrameController)supercontroller()).window()).toFront();
						afficherDialogue = false;
					}
				} else {
					afficherDialogue = true;
				}
			}
		}
	}
	// LOCK ECRAN
	public void lockSaisie(NSNotification aNotif) {
		if (aNotif.object() != this) {
			LogManager.logDetail("Lock fenetre : " + getClass().getName());
			estLocke = true;
			lockerFenetre(true);
			updaterDisplayGroups();
		}
	} 
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		if (aNotif.object() != this) {
			LogManager.logDetail("UnLock fenetre : " + getClass().getName());
			estLocke = false;
			lockerFenetre(false);
			updaterDisplayGroups();
		}
	} 
	// méthodes du Controleur DG
	/** M&eacute;thode du controller DG pour autoriser la creation d'objet. On peut saisir si il n'y a pas de modification
	 * en cours, que les conditions sur la page sont OK et que la page n'est pas lock&eacute;e */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && !estLocke;
	}
	/** M&eacute;thode du controller display group pour autoriser les boutons de modification et suppression. On peut modifier/supprimer
	 * si le display group a un objet selectionne, que la saisie est possible et que la page n'est pas lockee */
	public boolean boutonModificationAutorise() {
		return super.boutonModificationAutorise() && !estLocke;
	}
	/** M&eacute;thode du controller display group pour autoriser les boutons d'affichage de pages secondaires. On peut afficher une
	 * fonctionnalite secondaire si le display group a un objet selectionne, qu'on n'est pas en mode
	 * modification et que la page n'est pas lockee */
	public boolean boutonAffichageSecondaireAutorise() {
		return super.boutonAffichageSecondaireAutorise() && !estLocke;
	}
	// méthodes protégées
	/** Prepare les notifications puis invoque la methode du parent.<BR>
	 * Methode a surcharger pour faire des initialisations une fois l'archive chargee<BR>
	 * les sous-classes doivent invoquer la methode de la super classe : l'appel &agrave; super dans la sous-classe
	 * est a faire a la fin de la methode si on ne veut pas que le display group soit charge */
	protected void preparerFenetre() {
		loadNotifications();
		super.preparerFenetre();	
		remettreEtatSelection();
	}
	/** Pr&eacute;pare les notifications de lock/unlock de la page. A surcharger si d'autres notifications sont g&eacute;r&eacute;es */
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
	}
	/** Traitements &agrave; ex&eacute;cuter suite &agrave; la cr&eacute;tion d'un objet */
	protected abstract void traitementsPourCreation();
	/** M&eacute;thode &agrave; surcharger pour faire des traitements apres l'enregistrement des donnees dans la base. 
	 * Dans la m&eacute;thode surcharg&eacute;e il est imperatif d'appeler super */
	protected void traitementsApresValidation() {
		LogManager.logDetail(getClass().getName() + " : traitementsApresValidation");
		remettreEtatSelection();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
	}
	/** Methode a surcharger pour faire des traitements apres un revert des donnees dans la base
	 * Dans la methode surchargee il est imperatif d'appeler super */
	protected  void traitementsApresRevert() {
		super.traitementsApresRevert();
		remettreEtatSelection();
	}
	/** M&eacute;thode &agrave; surcharger pour faire des traitements apres une suppression de donn&eacute;es dans la base
	 * Dans la m&eacute;thode surcharg&eacute;e il est imp&eacute;ratif d'appeler super */
	protected void traitementsApresSuppression() {
		super.traitementsApresSuppression();
		updaterDisplayGroups();
	}
	/** Remet en &eacute;tat la s&eacute;lection */
	protected void remettreEtatSelection() {
		afficherBoutons(false);
		setModificationEnCours(false);
		modeCreation = false;
		parametrerDisplayGroup();
		updaterDisplayGroups();
	}
	/** Raffraichissement des display groups */
	protected void updaterDisplayGroups() {
		if (displayGroup() != null) {
			displayGroup().updateDisplayedObjects();
		}
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	/** Ajoute une relation &grave; un objet source
	 * @param source source de la relation	
	 * @param destin globalID de l'objet destinataire de la relation, peut &ecirc;tre nul
	 * @param nomRelation cl&eacute; utilis&eacute;e pour d&eacute;finir cette relation
	 */
	protected boolean ajouterRelation(EOGenericRecord source, Object destin,String nomRelation) {
		if (!estLocke()) {	// on ne traite pas les notifications qui ne nous concernent pas
			if (destin != null) {
				EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)destin,editingContext());
				if (source != null && recordDestin != null) {
					source.addObjectToBothSidesOfRelationshipWithKey(recordDestin,nomRelation);
					LogManager.logDetail(getClass().getName() + " : Ajout de l'objet " + recordDestin + " a la relation " + nomRelation);
					updaterDisplayGroups();
					return true;
				}
			}
		}
		return false;
	}
	/** Affiche les boutons de validation ou les boutons de modification selon la valeur du param&egrave;tre */
	protected void afficherBoutons(boolean estValidation) {
		if (vueBoutonsValidation != null) {
			vueBoutonsValidation.setVisible(estValidation);
		}
		if (vueBoutonsModification != null) {
			vueBoutonsModification.setVisible(!estValidation);
		}
	}
	/** Raffraichit les associations */
	protected void raffraichirAssociations() {
		if (controllerDisplayGroup() != null) {
			java.util.Enumeration e = controllerDisplayGroup().observingAssociations().objectEnumerator();
			while (e.hasMoreElements()) {
				EOAssociation association = (EOAssociation)e.nextElement();
				association.subjectChanged();
			}
		}
	}

}
