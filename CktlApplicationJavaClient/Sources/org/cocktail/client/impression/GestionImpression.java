/*
 * Created on 22 mai 2006
 *
 * Gère l'impression des données
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.impression;

import javax.swing.JProgressBar;

import org.cocktail.common.Constantes;
import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOTextField;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;

/** G&egrave;re l'impression diff&eacute;r&eacute;e de donn&eacute;es. Affiche un dialogue avec un message et un thermom&egrave;tre<BR>
 * Travaille dans l'editing context qu'on lui fournit<BR>
 * Poste une notification en fin d'impression pour signaler si impression OK ou non (voir les constantes ci-dessous)<BR>
 * @author christine
 *
 */
public class GestionImpression extends ThreadInterfaceController {
	public JProgressBar progressBar;
	public EOTextField champMessage;
	private EOEditingContext editingContext;
	private String nomFichierImpression;
	private int typeImpression;
	
	/** message de notification en fin d'impression */
	public static String FIN_IMPRESSION = "ImpressionTerminee";
	/** message envoy&eacute;e par la notification de fin d'impression si impression OK */
	public static String OK = "OK";
	/** message envoy&eacute;e par la notification de fin d'impression si impression &eacute;chou&eacute;e */
	public static String ECHEC = "Echec";
	
	public GestionImpression(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	/** Lance l'impression comme une t&acirc;che asynchrone
	 * @param nom de la m&eacute;thode d'impression &agrave; d&eacute;clencher
	 * @param classe des param&egrave;tres
	 * @param valeur des param&egrave;tres
	 * @param message &agrave; afficher &agrave; l'utilisateur
	 * @param typeImpression type du fichier g&eacute;n&eacute;r&eacute; (Constantes.IMPRESSION_PDF, Constantes.IMPRESSION_EXCEL */
	public void lancerImpression(String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierImpression,String message,int typeImpression) {
		progressBar.setIndeterminate(true);
		// s'assurer qu'on travaille dans le même editing context que l'appelant
		setEditingContext(editingContext);
		this.nomFichierImpression = nomFichierImpression;
		this.typeImpression = typeImpression;
		champMessage.setText(message);
		try {
			if (typeImpression == Constantes.IMPRESSION_PDF) {
				UtilitairesImpression.preparerPdfAvecMethode(editingContext,nomMethode,classeParametres,parametres);
			} else if (typeImpression == Constantes.IMPRESSION_EXCEL) {
				UtilitairesImpression.preparerFichierExcelAvecMethode(editingContext,nomMethode,classeParametres,parametres);
			}
		} catch (Exception e) {
			LogManager.logException(e);
			terminerAvecErreur("Problème lors du lancement de l'impression");
		}
	}
	/** m&eacute;thode vide pour emp&circ;cher le comportement par d&eacute;faut */
	public void setEdited(boolean aBool) {
		// on ne fait rien pour empêcher de marquer la fenêtre comme éditée
		super.setEdited(false);
	}
	public void recupererMessage(NSNotification aNotif) {
		String message = message(aNotif);
		if (message != null) {
			int i = message.lastIndexOf("\n");
			if (i >= 0) {
				message = message.substring(i + 1);
			} 
			champMessage.setText(message);
		}
	}
	/** M&eacute;thode de notification pour r&eacute;cup&eacute;rer le r&eacute;sultat */
	public void recupererResultat(NSNotification aNotif) {
		NSDictionary dict = (NSDictionary)objetResultatAction(aNotif);
		if (dict == null) {
			terminerAvecErreur("Erreur pendant l'impression");
		} else {
			NSData data = (NSData)dict.objectForKey("data");
			if (data == null) {
				terminerAvecErreur((String)dict.objectForKey("message"));
			} else {
				if (typeImpression == Constantes.IMPRESSION_PDF) {
					UtilitairesImpression.afficherPdf(data,nomFichierImpression);
					champMessage.setText("Afichage du pdf");
				} else if (typeImpression == Constantes.IMPRESSION_EXCEL) {
					UtilitairesImpression.afficherFichierExport(data,nomFichierImpression);
					champMessage.setText("Afichage du fichier Excel");
				}
				terminer();
				NSNotificationCenter.defaultCenter().postNotification(FIN_IMPRESSION,OK);
			}	
		}
	}

	/** M&eacute;thode de notification pour r&eacute;cup&eacute;rer une erreur */
	public void recupererErreur(NSNotification aNotif) {
		terminerAvecErreur("Erreur lors de la préparation du fichier PDF.\n" + aNotif.object());
	}
	public void terminerTraitement(NSNotification aNotif) {
	//	terminer();

	}
	// péthodes protégées
	protected void terminer() {
		((EODialogController)supercontroller()).closeWindow();
		super.terminer();
	}
	// méthodes privées
	private void terminerAvecErreur(String message) {
		EODialogs.runErrorDialog("Erreur",message);
		NSNotificationCenter.defaultCenter().postNotification(FIN_IMPRESSION,ECHEC);
		terminer();
	}
	

}
