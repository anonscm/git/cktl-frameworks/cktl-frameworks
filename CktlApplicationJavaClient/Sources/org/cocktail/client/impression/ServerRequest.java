//
//  ServerRequest.java
//  TestThread
//
//  Created by Christine Buttin on Mon Jun 28 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.impression;

import java.util.TimerTask;

import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;

public class ServerRequest extends TimerTask {
	private EODistributedObjectStore store;
	private EOEditingContext editingContext;
	
	
	public ServerRequest(EODistributedObjectStore aStore,EOEditingContext ec) {
		super();
		store = aStore;
		editingContext = ec;
	}
	
	public void run() {
		try {
			String message = (String)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestMessage", null,null,false);
			if (message != null) {
				if (message.equals("termine")) {
					NSDictionary resultatAction = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestResultatAction", null,null,false);
					// vérifier si une exception c'est produite
					message = extraireInformationDiagnostic(resultatAction,"exception");
					if (message != null) {
						NSNotificationCenter.defaultCenter().postNotification("erreurArrivee",message);
						cancel();
					} else {
						// poster le résultat obtenu via une notification si il existe un résultat
						if (resultatAction.objectForKey("resultat") != null) {
								NSNotificationCenter.defaultCenter().postNotification("resultatObtenu",resultatAction.objectForKey("resultat"));
						}
						cancel();
					}
					NSNotificationCenter.defaultCenter().postNotification("threadTermine",null);
				} else {
					NSNotificationCenter.defaultCenter().postNotification("messageInformation",message);
				}
			}
		} catch (Exception e) {
			LogManager.logException(e);
		}
	}
	
	// méthodes privées
	private String extraireInformationDiagnostic(NSDictionary dictionnaire,String cle) {
		NSDictionary diagnostic = (NSDictionary)dictionnaire.objectForKey("diagnostic");
		if (diagnostic != null) {
			return (String)diagnostic.objectForKey(cle);
		} else {
			return null;
		}
	}
}
