//
//  ThreadInterfaceController.java
//  TestThread
//
//  Created by Christine Buttin on Tue Jun 29 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
// Contient les méthodes pour la préparation d'un traitement asynchrone côté client. Peut être sous-classé. Lors de l'instaciation, d'autres
// objets peuvent s'enregistrer pour recevoir les notifications
// Utilise les timers Java

/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.impression;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class ThreadInterfaceController extends EOInterfaceController {
	private java.util.Timer timer;
	public ThreadInterfaceController() {
        super();
    }
	
	public void preparerPourTraitementAsynchrone() {
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
		// s'inscrire pour recevoir les notifications
		enregistrerPourNotifications(this);	
		// déclencher un timer pour schéduler une task
		timer = new java.util.Timer();
		ServerRequest serverTask = new ServerRequest(store,editingContext());
		timer.schedule(serverTask,0,1);
	}
	/** enregistrer un objet pour qu'il reçoive les notifications */
	public void enregistrerPourNotifications(Object demandeur) {
		NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererResultat",new Class [] {NSNotification.class}),"resultatObtenu",null);
		NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererMessage",new Class [] {NSNotification.class}),"messageInformation",null);
		NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("terminerTraitement",new Class [] {NSNotification.class}),"threadTermine",null);
		NSNotificationCenter.defaultCenter().addObserver(demandeur,new NSSelector("recupererErreur",new Class [] {NSNotification.class}),"erreurArrivee",null);
	}   
	/** supprimer la r&eacute;ception des notifications */
	public void supprimerPourNotifications(Object demandeur) {
		NSNotificationCenter.defaultCenter().removeObserver(demandeur,"resultatObtenu",null);
		NSNotificationCenter.defaultCenter().removeObserver(demandeur,"messageInformation",null);
		NSNotificationCenter.defaultCenter().removeObserver(demandeur,"threadTermine",null);
		NSNotificationCenter.defaultCenter().removeObserver(demandeur,"erreurArrivee",null);
	}		
	/** retourne la classe du r&eacute;sultat de l'action
	*/
	public String classeResultatAction(NSNotification aNotif) {
		NSDictionary resultat = (NSDictionary)aNotif.object();
		if (resultat != null) {
			return (String)resultat.objectForKey("classe");
		} else {
			return null;
		}
	}
	/** retourne le du r&eacute;sultat de l'action
	*/
	public Object objetResultatAction(NSNotification aNotif) {
		NSDictionary resultat = (NSDictionary)aNotif.object();
		if (resultat != null) {
			return resultat.objectForKey("valeur");
		} else {
			return null;
		}
	}
	/** retourne un message envoy&eacute; par le serveur
	*/
	public String message(NSNotification aNotif) {
		return (String)aNotif.object();
	}
	
	/** ces méthodes doivent être définies par la sous-classe ou les classes qui s'enregistrent pour recevoir les notifications */
	public abstract void recupererMessage(NSNotification aNotif);
	public abstract void recupererResultat(NSNotification aNotif);
	public abstract void recupererErreur(NSNotification aNotif);
	public abstract void terminerTraitement(NSNotification aNotif);
	
	// méthodes protégéées
	/** terminer le traitement */
	protected void terminer() {
		supprimerPourNotifications(this);
		timer.cancel();
	}
	
}