/*
 * Created on 11 mai 2006
 *
 * Contient des utilitaires pour l'impression des données
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.impression;

import java.awt.event.WindowListener;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.swing.JDialog;

import org.cocktail.client.InterfaceApplicationAvecImpression;
import org.cocktail.common.Constantes;
import org.cocktail.common.LogManager;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;

/**
 * @author christine
 *
 * Contient des utilitaires pour l'impression des donn&eacute;es c&ocirc;t&eacute; client
 */
public class UtilitairesImpression {
	public static final String MAC_OS_X_OPEN = "open ";
	// Chaines correspondant a System.getProperties().getProperty(os.name)
	public static final String MAC_OS_X_OS_NAME = "Mac OS X";
	//public static final String WINDOWS2000_OS_NAME = "Windows 2000";
	// Chaines correspondant aux commandes permettant de lancer une commande sur les diffÈrents systemes
	public static final String MAC_OS_X_EXEC = "open ";
	public static final String WINDOWS_EXEC = "launch.bat ";
	private static int numImpression = 1;
	
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur avec retour imm&eacute;diat (utilisation
	 * de threads) en affichant un dialogue d'impression. La m&eacute;thode invoqu&eacute;e sur le serveur ne doit rien retourner. <BR>
	 * Lance une exception si le d&eacute;clenchement de l'impression s'est mal pass&eacute;
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * @param typeImpression (fichier pdf ou fichier Excel)
	 * 
	 */
	public static void imprimerAvecDialogue(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf,String titreFenetre,int typeImpression) throws Exception {
		LogManager.logDetail("imprimerAvecDialogue : " + nomMethode);
		GestionImpression controleur = new GestionImpression(editingContext);
		EODialogController.runControllerInNewDialog(controleur,titreFenetre);
		controleur.preparerPourTraitementAsynchrone();
		((JDialog)((EODialogController)controleur.supercontroller()).window()).setModal(true);	// pour le cas où d'autres dialogues masqueraient la fenêtre
		WindowListener[] listeners = ((EODialogController)controleur.supercontroller()).window().getWindowListeners();
		for (int i = 0; i < listeners.length;i++) {
			((EODialogController)controleur.supercontroller()).window().removeWindowListener((WindowListener)listeners[i]);
		}
		controleur.lancerImpression(nomMethode,classeParametres,parametres,nomFichierPdf,"Préparation...",typeImpression);
	}
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur avec retour imm&eacute;diat (utilisation
	 * de threads) en affichant un dialogue d'impression. La m&eacute;thode invoqu&eacute;e sur le serveur ne doit rien retourner. <BR>
	 * Lance une exception si le d&eacute;clenchement de l'impression s'est mal pass&eacute;.
	 * G&eacute;n&egrave;re un fichier pdf
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * 
	 */
	public static void imprimerAvecDialogue(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf,String titreFenetre) throws Exception {
		imprimerAvecDialogue(editingContext, nomMethode, classeParametres, parametres, nomFichierPdf, titreFenetre,Constantes.IMPRESSION_PDF);
	}
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur avec retour imm&eacute;diat (utilisation
	 * de threads). La m&eacute;thode invoqu@&eacute;e sur le serveur ne doit rien retourner. Cette m&eacute;thode est &agrave; utiliser lorsqu'on
	 * ne veut pas passer par le dialogue d'impression<BR>
	 * Lance une exception si le d&eacute;cmenchement de l'impression s'est mal pass&eacute;
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * 
	 */
	public static void preparerPdfAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres) throws Exception {
		LogManager.logDetail("preparerPdfAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			Boolean result = (Boolean)store.invokeRemoteMethodWithKeyPath(editingContext,"session",nomMethode,classeParametres,parametres,true);
			if (result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement de l'impression");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur avec retour imm&eacute;diat (utilisation
	 * de threads). La m&eacute;thode invoqu@&eacute;e sur le serveur ne doit rien retourner. Cette m&eacute;thode est &agrave; utiliser lorsqu'on
	 * ne veut pas passer par le dialogue d'impression<BR>
	 * Lance une exception si le d&eacute;cmenchement de l'impression s'est mal pass&eacute;
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * 
	 */
	public static void preparerFichierExcelAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres) throws Exception {
		LogManager.logDetail("preparerFichierExcelAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			Boolean result = (Boolean)store.invokeRemoteMethodWithKeyPath(editingContext,"session",nomMethode,classeParametres,parametres,true);
			if (result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement de l'impression");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur. N'utilise pas de threads. La m&eacute;thode
	 * invoqu@&eacute;e sur le serveur doit retourner un dictionnaire contenant les data (cl&eacute; : data, valeur : NSData)
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * @param nomFichierPdf nom du fichier Pdf g&eacute;n&eacute;r&eacute; sur le poste client (sans extension .pdf)
	 */
	public static void afficherPdfAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf) throws Exception {
		LogManager.logDetail("afficherPdfAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			NSDictionary dictionary = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext,"session",nomMethode,classeParametres,parametres,true);
			NSData datas = (NSData)dictionary.objectForKey("data");
			if (datas == null) {
				throw new Exception("Erreur lors de la génération du pdf de " + nomFichierPdf);
			} else {
				afficherPdf(datas,nomFichierPdf);
			}
		} catch (Exception e) {
			LogManager.logErreur("afficherPdfAvecMethode - Erreur lors de la génération du pdf de " + nomFichierPdf);
			throw e;
		}
	}
	/** D&eacute;clenche la m&eacute;thode d'impression pass&eacute; en param&egrave;tre sur le serveur. N'utilise pas de threads. La m&eacute;thode
	 * invoqu@&eacute;e sur le serveur doit retourner un dictionnaire contenant les data (cl&eacute; : data, valeur : NSData)
	 * @param editingContext
	 * @param nomMethode nom de la m&eacute;thode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des param&egrave;tres
	 * @param objets	tableau contenant les param&egrave;tres la m&eacute;thode d'impression
	 * @param nomFichierExcel nom du fichier Excel g&eacute;n&eacute;r&eacute; sur le poste client (sans extension .pdf)
	 */
	public static void afficherFichierExcelAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierExcel) throws Exception {
		LogManager.logDetail("afficherPdfAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			NSDictionary dictionary = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext,"session",nomMethode,classeParametres,parametres,true);
			NSData datas = (NSData)dictionary.objectForKey("data");
			if (datas == null) {
				throw new Exception("Erreur lors de la génération du fichier Excel de " + nomFichierExcel);
			} else {
				afficherFichierExport(datas,nomFichierExcel);
			}
		} catch (Exception e) {
			LogManager.logErreur("afficherFichierExcelAvecMethode - Erreur lors de la génération du pdf de " + nomFichierExcel);
			throw e;
		}
	}
	/** affiche des datas qui contiennent du pdf
	 * @param datas
	 * @param fileName nom du fichier Pdf sans extension
	 */
	public static void afficherPdf(NSData datas, String fileName)	{
		afficherFichier(datas,null,fileName,"pdf");
	}
	/** affiche des datas qui contiennent des donn&eacute;es pour Excel
	 * @param datas
	 * @param fileName nom du fichier excel sans extension
	 */
	public static void afficherFichierExport(NSData datas, String fileName)	{
		afficherFichier(datas,null,fileName,"xls");
	}
	/** affiche des donn&eacute;es &agrave; exporter au format tab-tab-return
	 * @param texte texte &agrave; exporter
	 * @param dir directory export
	 * @param fileName nom du fichier sans extension
	 */
	public static void afficherFichierExport(String texte, String dir,String fileName)	{
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,dir,fileName,"xls");
	}
	/** affiche des donn&eacute;es &agrave; exporter au format tab-tab-return
	 * @param texte texte &agrave; exporter
	 * @param fileName nom du fichier sans extension
	 */
	public static void afficherFichierExport(String texte, String fileName)	{
		afficherFichierExport(texte,null,fileName);
	}
	// méthodes privées
	private static void afficherFichier(NSData datas, String dir,String fileName,String extension) {
		String cheminDir = ((InterfaceApplicationAvecImpression)EOApplication.sharedApplication()).directoryImpression();
		if (dir != null) {
			cheminDir = dir;
		}
		byte b[] = datas.bytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(b);
		// Remplacer tous les / par des - si il y en a dans le nom de fichier
		fileName = fileName.replaceAll("/", "-");
		fileName = fileName + "_" + numImpression++;
		String filePath = cheminDir + File.separator + fileName + "." + extension;
		if (new File(cheminDir).exists() == false) {
			filePath =  ((InterfaceApplicationAvecImpression)EOApplication.sharedApplication()).directoryImpressionParDefaut() + fileName + "." + extension;
		}
		try {
			StreamCtrl.saveContentToFile (stream, filePath);
			openFile(filePath);
		} catch (Exception e) {e.printStackTrace();}
	}
	private static void openFile(String filePath)	{
		File	aFile = new File(filePath);
		if (System.getProperty("os.name").equals(MAC_OS_X_OS_NAME)) {
			try { 
				Runtime.getRuntime().exec(MAC_OS_X_OPEN+aFile);
			}
			catch(Exception exception) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());		
				LogManager.logException(exception);
			}
		} else	{
			try {
				Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
        runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""} ); 
			}
			catch(Exception exception) {
				LogManager.logErreur("Impossible d'ouvrir le fichier pdf");
				LogManager.logException(exception);
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());
				LogManager.logException(exception);
			}	
		}
	}
}
