/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Cursor;

import org.cocktail.common.utilities.EOFUtilities;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**	Same class as DialogueDoubleSansFetch<BR>
 * Display a list with two columns (code, title) and a text field to look for objects. Objects are not displayed directly, they are fetched
 * in the database when the user clicks on the "Search" button. Objects must implement the RecordWithTitleAndCode interface
 * @author christine
 *
 */
public class DoubleDialogNoFetch extends SimpleDialogNoFetch {
	private String codeToSearch;
	private String codeAttributeToSearch;
	
	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param attributeToSearch attribute of the code on which the search is done
	 * @param codeAttributeToSearch attribute on which the search is done
	 * @param dialogTitle title of the dialog
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 * @param displayDistinctValues true if the displayed values are unique
	 * @param canSearchAll true if the search text can be null;
	 */
	public DoubleDialogNoFetch(String entityName, String validationNotification,String attributeToSearch,String codeAttributeToSearch,String dialogTitle,boolean multipleSelectionAllowed,boolean isModal,boolean displayDistinctValues,boolean isBasicCharacter,boolean canSearchAll) {
		// le champ de recherche est obligatoire
		super(entityName,validationNotification,attributeToSearch,dialogTitle,multipleSelectionAllowed,isModal,displayDistinctValues,isBasicCharacter,canSearchAll);
		this.codeAttributeToSearch = codeAttributeToSearch;
	}
	public void init() {
		EOArchive.loadArchiveNamed("DoubleDialogNoFetch",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	// accessors
	/** Return the code to search for */
	public String codeToSearch() {
		return codeToSearch;
	}
	/** Sets the code to search for */
	public void setCodeToSearch(String aText) {
		this.codeToSearch = aText;
	}
	// actions
	/** Search for objects */
	public void search() {
		// 29/04/2010 - ajouter pour le cas où l'utilisateur valide en tapant un retour chariot
		if (!canSearch()) {
			return;
		}		if (codeToSearch() == null) {
			super.search();
		} else {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			NSMutableArray args = new NSMutableArray("*" + codeToSearch() + "*");
			String stringQualifier = codeAttributeToSearch() + " caseinsensitivelike %@";
			if (textToSearch() != null) {
				args.addObject("*" + textToSearch() + "*");
				stringQualifier = stringQualifier + " AND " + textToSearch() +  " caseinsensitivelike %@";
			}
			EOQualifier qualifierForRecherche = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			if (qualifier() != null) {	// qualifier setter chez le parent
				NSMutableArray qualifs = new NSMutableArray(qualifier());
				qualifs.addObject(qualifierForRecherche);
				qualifierForRecherche = new EOAndQualifier(qualifs);
			}
			NSArray results = EOFUtilities.rechercherAvecQualifier(editingContext(),entityName(),qualifierForRecherche,false);
			displayGroup().setObjectArray(results);
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	// Méthodes du controller DG
	public boolean canSearch() {
		return codeToSearch() != null || super.canSearch();
	}
	// Méthodes protégées
	protected String codeAttributeToSearch() {
		return codeAttributeToSearch;
	}
}
