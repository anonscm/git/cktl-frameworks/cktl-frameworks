/*
 * Created on 24 juin 2005
 *
 * Classe de base pour des archives supportant des relations master-detail
 */
/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.EOFUtilities;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author christine<BR>
 *
 *  Abstract class for archive controllers managing master-detail relationships
 * The main display group is the master display group
 * The detail display group contains destination objects
 */
public abstract class ArchiveWithMasterDetail extends ArchiveWithDisplayGroup {
	public EODisplayGroup detailDisplayGroup;
	private String masterDetailRelationship;

	 /**	Constructor
	 * @param entityName Master Entity
	 * @param masterDetailRelationship detail relationship
	 */
	public ArchiveWithMasterDetail(String entityName,String masterDetailRelationship) {
        super(entityName);
        this.masterDetailRelationship = masterDetailRelationship;
    
    }
	/** initialization method : loads the interface file
	 * Objects are not fetch */
	public void init() {
		super.init();
		detailDisplayGroup().setSelectsFirstObjectAfterFetch(false);
	}
	// accesseurs
	/** Returns the detail display group */
	public EODisplayGroup detailDisplayGroup() {
		return detailDisplayGroup;
	}
	/** Prepares the main display group with the master object and prepares the detail display group
	 * @param masterGlobalID global ID of the master object */
	public void prepareDisplayGroupsWithObject(EOGlobalID masterGlobalID) {
		EOGenericRecord masterObject = null;
		if (masterGlobalID != null) {
			masterObject = EOFUtilities.objetForGlobalIDDansEditingContext(masterGlobalID,editingContext());
			if (masterObject != null) {
				displayGroup().setObjectArray(new NSArray(masterObject));
				detailDisplayGroup().setObjectArray((NSArray)masterObject.valueForKey(masterDetailRelationship));
			}  else {
				displayGroup().setObjectArray(null);
				detailDisplayGroup().setObjectArray(null);
			}
		} else {
			displayGroup().setObjectArray(null);
			detailDisplayGroup().setObjectArray(null);
		}
		displayGroup().setSelectedObject(masterObject);
		updateDisplayGroups();
	}
	/** Retourne le display group de detail */
	public EODisplayGroup displayGroupDetail() {
		return detailDisplayGroup();
	}
	/** Pr&eacute;pare le display group principal avec l'objet maitre et pr&eacute;pare le display group de d&eacute;tail
	 * @param masterGlobalID global ID of the master object */
	public void setObjetMaitre(EOGlobalID masterGlobalID) {
		prepareDisplayGroupsWithObject(masterGlobalID);
	}
	// méthodes protégées
	protected String masterDetailRelationship() {
		return masterDetailRelationship;
	}
	public EOGenericRecord masterObject() {
		return (EOGenericRecord)displayGroup().selectedObject();
	}
	protected void updateDisplayGroups() {
		super.updaterDisplayGroups();
		detailDisplayGroup().updateDisplayedObjects();
	}
	protected void updaterDisplayGroups() {
		updateDisplayGroups();
	}
	protected String nomRelation() {
		return masterDetailRelationship;
	}
	public EOGenericRecord objetMaitre() {
		return (EOGenericRecord)displayGroup().selectedObject();
	}

}
