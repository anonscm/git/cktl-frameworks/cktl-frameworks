/*
 * Created on 14 avr. 2006
 *
 * Affiche un dialogue avec un message affiché dans un champ texte
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import javax.swing.JDialog;

import org.cocktail.component.COLabel;
import org.cocktail.component.COTextArea;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eointerface.swing.EOTextArea;

/** Classe identique &agrave; PaletteWithMessage
 * Affiche une palette avec un titre et un message. Le fichier d'interface de la sous-classe peut fournir
 * des outlets sur le champ label et la text area<BR>
 * @author christine
 *
 */
public abstract class PaletteAvecMessage extends EODialogController {
	/** outlet : label du message affich&eacute; agrave; l'utilisateur  */
	public COLabel labelField;
	/** outlet : zone d'affichage du texte  */
	public COTextArea textArea;
	private String texte,texteLabel;
	private int X,Y;
	
	/** Constructeur
	 * @param X	position horizontale du dialogue
	 * @param Y	position verticale du dialogue
	 * @param texteLabel texte affich&eacute; dans le label
	 * @param texte texte affich&eacute; dans la texArea;
	 */
	public PaletteAvecMessage(int X,int Y,String texteLabel,String texte) {
		this.texte = texte;
		this.texteLabel = texteLabel;
		this.X = X;
		this.Y = Y;
		init();
	}
	public void connectionWasEstablished() {
		window().setLocation(X,Y);
		((JDialog)window()).setJMenuBar(null);
		((JDialog)window()).setUndecorated(true);
	}
	
	/** Pour afficher le dialogue */
	public void afficherFenetre() {
		if (textArea != null && texte != null) {
			textArea.setText(texte);
		}
		if (labelField != null && texteLabel != null) {
			labelField.setText(texteLabel);
		}
		activateWindow();
		}
	/** Pour fermer le dialogue */
	public void fermerFenetre() {
		closeWindow();
	}

	/** permet d'afficher ou de masquer la fen&ecirc;tre selon son &eacute;tat courant */
	public void changerEtat() {
		window().setVisible(!window().isVisible());
	}
	// Accessors
	/** Returns the label component */
	public COLabel labelField() {
		return labelField;
	}
	/** Set the label component */
	public void setLabelField(COLabel labelField) {
		this.labelField = labelField;
	}
	/** Returns the text area */
	public EOTextArea textArea() {
		return textArea;
	}
	/** Set the text area component */
	public void setTextArea(COTextArea textArea) {
		this.textArea = textArea;
	}
	/** Returns the text displayed in the text area */
	public String text() {
		return texte;
	}
	/** Set the text displayed in the text area */
	public void setText(String text) {
		this.texte = text;
		if (textArea != null) {
			textArea.setText(text);
		}
	}
	/** Returns the text of the label */
	public String labelText() {
		return texteLabel;
	}
	/** Set the text of the label */
	public void setLabelText(String aText) {
		this.texteLabel = aText;
		if (labelField != null) {
			labelField.setText(aText);
		}
	}
	// Méthodes protégées
	/** Cette m&eacute;thode doit charger l'archive<BR>
	 * EOArchive.loadArchiveNamed(...) */
	protected abstract void init();

}
