/*
 * Created on 13 mai 2005
 *
 * Affiche un dialogue de sélection et permet la création de nouveaux objets
 * les objets métiers qui sont affichés doivent répondre aux méthodes libelle() et setLibelle()
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSMutableArray;

/** Cette classe est l'&eacute;quivalent de SelectionWithCreation.<BR>
 * @author christine<BR>

 * Affiche un dialogue de s&eacute;lection et permet la cr&eacute;ation de nouveaux objets.
 * Les objets m&eacute;tiers qui sont affich&eacute;s doivent impl&eacute;menter l'interface RecordAvecLibelle
 */
public abstract class SelectionAvecCreation extends DialogueSimple {
	private String texteSaisi;

	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param estModal true si c'est un dialogue modal
	 * @param supprimerDoublons true si la liste affiche des valeurs uniques
	 */
	public SelectionAvecCreation(String nomEntite,String notificationValidation,String titreDialogue,boolean estModal,boolean supprimerDoublons) {
		// on met à true aChampRecherche pour laisser l'espace de saisie visible dans la vue
		super(nomEntite,notificationValidation,titreDialogue,false,false,estModal,supprimerDoublons);
	}
	/** Constructeur sans suppression des doublons
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param estModal true si c'est un dialogue modal
	 */
	public SelectionAvecCreation(String nomEntite,String notificationValidation,String titreDialogue,boolean estModal) {
		// on met à true aChampRecherche pour laisser l'espace de saisie visible dans la vue
		super(nomEntite,notificationValidation,titreDialogue,false,false,estModal,true);
	}
	public void init() {
		EOArchive.loadArchiveNamed("SelectionAvecCreation",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	// Acesseurs
	/** Retourne le texte saisi */
	public String texteSaisi() {
		return texteSaisi;
	}
	/** Modifie le texte saisi */
	public void setTexteSaisi(String texteSaisi) {
		this.texteSaisi = texteSaisi;
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.canValidate() || (texteSaisi() != null && texteSaisi().length() > 0);
	}
	// Méthodes protégées
	/** Si a un objet a &eacute;t&eacute; cr&eacute;&eacute;, l'ajoute au display group et &agrave; l'editing context, 
	 * enregistre les modifications et retourne son globalID sinon retourne le globalID de l'objet s&eacute;lectionn&eacute; */
	protected Object selectedObject() {
		if (texteSaisi() != null && texteSaisi().length() > 0) {
			EOClassDescription descriptionClass = EOClassDescription.classDescriptionForEntityName(entityName());
			EOGenericRecord record = (EOGenericRecord)descriptionClass.createInstanceWithEditingContext(editingContext(),null);
			if (record instanceof RecordAvecLibelle) {
				record.takeValueForKey(texteSaisi(),"libelle");
				calculerClePrimaire(record);
				editingContext().insertObject(record);
				editingContext().saveChanges();
				NSMutableArray objects = new NSMutableArray(displayGroup().allObjects());
				objects.addObject(record);
				displayGroup().setObjectArray(objects);
				displayGroup().selectObject(record);
				displayGroup.updateDisplayedObjects();
			}
		}
		return super.selectedObject();
	}
	/** Calcule la cl&eacute;primaire du record : ne rien faire si cl&eacute; g&eacute;r&eacute;&eacute; automatiquement par EOF */
	protected abstract void calculerClePrimaire(EOGenericRecord record);

}
