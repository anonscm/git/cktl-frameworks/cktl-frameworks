/* SelectionSimpleController
 * Dialogue de sélection avec deux boutons valider et annuler
 * Gère la sélection multiple et le double-clic  */

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.EOFUtilities;
import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/** Cette classe est l'&eacute;quivalent de SimpleDialog.<BR>
 * Affiche une table contenant des objets m&eacute;tier : l'utilisateur peut choisir un objet ou plus.
 * Affiche deux boutons pour annuler ou valider la s&eacute;lection. L'utilisateur peut s&eacute;lectionner et valider par un double-clic sur la liste.<BR>
 * Police par d&eacute;faut : Helvetica plain 11
 * Les objets doivent impl&eacute;menter l'interface RecordAvecLibelle */
public class DialogueSimple extends AbstractSimpleDialog {

	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param peutRechercher true si l'utilisateur peut effectuer des recherches
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 * @param supprimerDoublons true si la liste affiche des valeurs uniques
	 */
	public DialogueSimple(String nomEntite,String notificationValidation,String titreDialogue,boolean peutRechercher,boolean supporteSelectionMultiple,boolean estModal,boolean supprimerDoublons) {
		super(nomEntite,notificationValidation,titreDialogue,peutRechercher,supporteSelectionMultiple,estModal,supprimerDoublons);
		if (supporteSelectionMultiple) {
			setMessageForMultipleSelection("Vous pouvez sélectionner plusieurs éléments");
		}
	}
	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param peutRechercher true si l'utilisateur peut effectuer des recherches
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 */
	public DialogueSimple(String nomEntite,String notificationValidation,String titreDialogue,boolean peutRechercher,boolean supporteSelectionMultiple,boolean estModal) {
		this(nomEntite,notificationValidation,titreDialogue,peutRechercher,supporteSelectionMultiple,estModal,false);
	}
	/** Charge l'interface */
	public void init() {
		EOArchive.loadArchiveNamed("DialogueSimple",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	/** modifie le libell&eacute; de la premi&egrave;re colonne */
	public void changerLibellePremiereColonne(String libelle) {
		changeHeaderFirstColumn(libelle);
	}
	/** Retourne true si l'utilisateur a s&eacute;lectionn&eacute; un &eacute;l&eacute;ment de la liste */
	public boolean peutValider() {
		return super.canValidate();
	}
	// Méthodes protégées
	/** Trie les objets de la table */
	protected void sortTable() {
		if (displayGroup.sortOrderings() == null) {
		displayGroup.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("libelle",EOSortOrdering.CompareAscending)));
		}
	}
	protected NSArray fetchObjects() {
		NSArray results;
		if (qualifier() == null) {
			results = EOFUtilities.rechercherEntite(editingContext(),entityName());
		} else {
			results = EOFUtilities.rechercherAvecQualifier(editingContext(),entityName(),qualifier(),displayDistinctValues());
		}
		if (!displayDistinctValues()) {
			return results;
		}
		// supprimer les doublons de libellé
		java.util.Enumeration e = results.objectEnumerator();
		NSMutableArray newResults = new NSMutableArray();
		NSMutableArray alreadySeenObjects = new NSMutableArray();
		while (e.hasMoreElements()) {
			Object object = e.nextElement();
			if (object instanceof RecordAvecLibelle) {
				String title = ((RecordAvecLibelle)object).libelle();
				if (title != null) {
					if (alreadySeenObjects.containsObject(title) == false) {
						newResults.addObject(object);
						alreadySeenObjects.addObject(title);
					}
				}
			}
		}
		return newResults;
	}
	protected EOQualifier qualifierForSearching(String texteAChercher) {
		if (texteAChercher == null || texteAChercher.length() == 0) {
			return null;
		} else {
			return EOQualifier.qualifierWithQualifierFormat("libelle caseinsensitivelike %@", new NSArray("*" + texteAChercher + "*"));
		}
	}
	protected void changerLibelleDeColonne(String libelle,int colonne) {
		displayTable().getColumnModel().getColumn(colonne).setHeaderValue(libelle);
	}
}
