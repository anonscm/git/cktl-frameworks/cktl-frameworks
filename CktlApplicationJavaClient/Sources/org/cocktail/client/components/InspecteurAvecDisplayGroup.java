/*
 * Created on 12 avr. 2006
 *
 * Inspecteur avec display group et editing context<BR>
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import javax.swing.JDialog;

import org.cocktail.client.components.utilities.GraphicUtilities;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Classe identique &agrave InspectorWithDisplayGroup<BR>
 * Inspecteur avec display group et editing context<BR>
 * Utilise une notification pour communiquer l'objet sur lequel on travaille
 * @author christine
 *
 * 
 */
public abstract class InspecteurAvecDisplayGroup extends EODialogController {
	public EODisplayGroup displayGroup;
	public EOTable listeAffichage;	// pour afficher les données du display group
	public EOEditingContext editingContext;
	private EOGenericRecord objetCourant;
	private String nomNotificationSynchronisation;

	/** Constructeur
	 * @param editingContext editing de travail
	 * @param nomNotificationSynchronisation	nom de la notification envoy&eacute;e pour synchroniser les donn&eacutees; (peut &ecirc;tre nulle)
	 */
	public InspecteurAvecDisplayGroup(EOEditingContext editingContext,String nomNotificationSynchronisation) {
		this.editingContext = editingContext;
		this.nomNotificationSynchronisation = nomNotificationSynchronisation;
	}
	/** Constructeur
	 * @param editingContext editing de travail
	 */
	public InspecteurAvecDisplayGroup(EOEditingContext editingContext) {
		this(editingContext,null);
	}
	public void connectionWasEstablished() {
		if (nomNotificationSynchronisation != null) {
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getObjetCourant", new Class[] {NSNotification.class}),nomNotificationSynchronisation,null);
		}
	}
	public void connectionWasBroken() {
		if (nomNotificationSynchronisation != null) {
			NSNotificationCenter.defaultCenter().removeObserver(this);
		}
	} 
	public void afficher() {
		if (listeAffichage != null) {
			GraphicUtilities.rendreNonEditable(listeAffichage);
		}
		((JDialog)window()).setModal(false);
		setWindowResizable(window(),false);
		activateWindow();
		updaterDisplayGroup();
	}
	public void terminer() {
		closeWindow();
	}
	/** Modifie l'objet courant. L'objet de la notification comporte l'objet courant */
	public void getObjetCourant(NSNotification aNotif) {
		objetCourant = (EOGenericRecord)aNotif.object();
		updaterDisplayGroup();
	}
	/** &agrave; d&eacute;finir dans la sous-classe, en particulier pour le chargement de l'archive */
	public abstract void init();

	// méthodes protégées
	protected EOGenericRecord objetCourant() {
		return objetCourant;
	}
	protected abstract NSArray fetcherObjets();

	// méthodes privées
	private void updaterDisplayGroup() {
		if (displayGroup != null) {
			displayGroup.setObjectArray(fetcherObjets());
			displayGroup.updateDisplayedObjects();
		}
	}
}
