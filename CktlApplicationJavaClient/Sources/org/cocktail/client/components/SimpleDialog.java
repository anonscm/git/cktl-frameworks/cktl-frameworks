/* SelectionSimpleController
 * Dialogue de sélection avec deux boutons valider et annuler
 * Gère la sélection multiple et le double-clic  */

/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.EOFUtilities;
import org.cocktail.common.utilities.RecordWithTitle;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/** Display a table containing enterprise objects: the user can select one or more objects.
 * Display  two buttons to cancel or validate the selection. The user can select and validate by double-clicking on the list<BR>
 * The default font Helvetica plain 11
 * Objects must implement the RecordWithTitle interface */
public class SimpleDialog extends AbstractSimpleDialog {

	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if there is a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 * @param displayDistinctValues true if the displayed values are unique
	 */
	public SimpleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal,boolean displayDistinctValues) {
		super(entityName,validationNotification,dialogTitle,canSearch,multipleSelectionAllowed,isModal,displayDistinctValues);
		if (multipleSelectionAllowed) {
			setMessageForMultipleSelection("You can select several items");
		}
	}
	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if thereis a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 */
	public SimpleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal) {
		this(entityName,validationNotification,dialogTitle,canSearch,multipleSelectionAllowed,isModal,false);
	}
	/** Loads the interface file */
	public void init() {
		EOArchive.loadArchiveNamed("SimpleDialog",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	/** Sort the objects of the table */
	public void sortTable() {
		if (displayGroup() != null) {
			displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("title",EOSortOrdering.CompareAscending)));
		}
	}
	// Méthodes protégées
	protected NSArray fetchObjects() {
		NSArray results;
		if (qualifier() == null) {
			results = EOFUtilities.rechercherEntite(editingContext(),entityName());
		} else {
			results = EOFUtilities.rechercherAvecQualifier(editingContext(),entityName(),qualifier(),true);
		}
		if (!displayDistinctValues() || results.count() == 0) {
			return results;
		}
		return supprimerDoublons(results);
	}
    protected NSArray supprimerDoublons(NSArray anArray) {
    	// supprimer les doublons de libellé
		java.util.Enumeration e = anArray.objectEnumerator();
		NSMutableArray newResults = new NSMutableArray();
		NSMutableArray alreadySeenObjects = new NSMutableArray();
		while (e.hasMoreElements()) {
			Object object = e.nextElement();
			String title = null;
			if (object instanceof RecordWithTitle) {
				title = ((RecordWithTitle)object).title();
				if (alreadySeenObjects.containsObject(title) == false) {
					newResults.addObject(object);
					alreadySeenObjects.addObject(title);
				}
			}
		}
		return new NSArray(newResults);
    }
	protected EOQualifier qualifierForSearching(String textToSearch) {
		if (textToSearch == null || textToSearch.length() == 0) {
			return null;
		} else {
			return EOQualifier.qualifierWithQualifierFormat("title caseinsensitivelike %@", new NSArray("*" + textToSearch + "*"));
		}
	}
}
