/*
 * Created on 19 mai 2005
 *
 * Méthodes utilitaires pour gérer les matrices`
 * Ne comporte que des méthodes de classe
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components.utilities;

import javax.swing.AbstractButton;

import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.foundation.NSArray;

/**
 * @author christine<BR>
 * Static methods to manage radio button matrix<BR>
 * M&eacute;thodes utilitaires pour g&eacute;rer les matrices de radio bouton
 */
public class MatrixUtilities {
	/** To disable a matrix */
	public static void disable(EOMatrix matrix) {
		matrix.setEnabled(false);
		disableButtons(matrix);
	}
	/** Pour d&eacute;sactiver une matrice */
	public static void desactiver(EOMatrix matrice) {
		disable(matrice);
	}
	/** To disable the components of a matrix */
	public static void disableButtons(EOMatrix matrix) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			((AbstractButton)matrix.getComponent(i)).setEnabled(false);
		}
	}
	/** Pour d&eacute;sactiver les composants d'une matrice */
	public static void desactiverBoutons(EOMatrix matrice) {
		disableButtons(matrice);
	}
	/** To enable the components of a matrix */
	public static void enableButtons(EOMatrix matrix) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			((AbstractButton)matrix.getComponent(i)).setEnabled(true);
		}
	}
	/** To enable/disable the button at index */
	public static void enableButton(EOMatrix matrix,int index,boolean state) {
		((AbstractButton)matrix.getComponent(index)).setEnabled(state);
	}
	/** Pour activer les composants d'une matrice */
	public static void activerBoutons(EOMatrix matrice) {
		enableButtons(matrice);
	}
	/** Pour autorisr/interdire le bouton &grave; l'index fourni */
	public static void autoriserBouton(EOMatrix matrice,int index,boolean etat) {
		enableButton(matrice,index,etat);
	}
	/** To unselect the components of a matrix */
	public static void unselectButtons(EOMatrix matrix) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			((AbstractButton)matrix.getComponent(i)).setSelected(false);
		}
		matrix.repaint();
	}
	/** to select the component at provided index */
	public static void selectButton(EOMatrix matrix,int index) {	
		boolean changeState = false;	// Pour gérer le cas où le bouton n'est pas autorisé mais on veut qu'il reflète un état
		if (matrix.getComponent(index).isEnabled() == false) {
			changeState = true;
			matrix.getComponent(index).setEnabled(true);
		}
		((AbstractButton)matrix.getComponent(index)).doClick();
		if (changeState) {
			matrix.getComponent(index).setEnabled(false);
		}
	}
	/** Pour d&eacute;s&eacute;lectionner les composants d'une matrice */
	public static void deselectionnerBoutons(EOMatrix matrice) {
		unselectButtons(matrice);
	}
	/** Pour s&eacute;lectionner le composants &grave; l'index fourni */
	public static void selectionnerBouton(EOMatrix matrice,int index) {	
		selectButton(matrice, index);
	}
	/** Return the position of the selected button or -1 */
	public static int selectedIndex(EOMatrix matrix) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			if (((AbstractButton)matrix.getComponent(i)).isSelected()) {
				return i;
			}
		}
		return -1;
	}
	/** Return the selected button or null */
	public static AbstractButton selectedButton(EOMatrix matrix) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			if (((AbstractButton)matrix.getComponent(i)).isSelected()) {
				return ((AbstractButton)matrix.getComponent(i));
			}
		}
		return  null;
	}
	/** Return the text of the selected button or null */
	public static String selectedText(EOMatrix matrix) {
		AbstractButton bouton = boutonSelectionne(matrix);
		if (bouton != null) {
			return bouton.getText();
		} else {
			return  null;
		}
	}
	/** Retourne la position du bouton s&eacute;lectionn&eacute; ou -1 */
	public static int colonneSelectionnee(EOMatrix matrice) {
		return selectedIndex(matrice);
	}
	/** Retourne le bouton s&eacute;lectionn&eacute; ou null */
	public static AbstractButton boutonSelectionne(EOMatrix matrice) {
		return selectedButton(matrice);
	}
	/** retourne le texte du bouton s&eacute;lectionn&eacute; ou null */
	public static String labelSelectionne(EOMatrix matrice) {
		return selectedText(matrice);
	}
	/** Modify the selection of the buttons
	 * @param matrix
	 * @param enabledArray array containing the selected state of the buttons (Boolean). Must be the same length as the number of button */
	public static void changeSelection(EOMatrix matrix,NSArray enabledArray) {
		for (int i= 0;i < matrix.getComponents().length;i++) {
			boolean value = ((Boolean)enabledArray.objectAtIndex(i)).booleanValue();
			((AbstractButton)matrix.getComponent(i)).setSelected(value);
		}
	}
	/** Mdifie la s&eacute;lection des boutons 
	 * @param matrice
	 * @param tableauEtats tableau des &acute;tats des boutons(Boolean). Il doit y avoir autant d'&eacute;tats que de boutons */
	public static void changerSelection(EOMatrix matrice,NSArray tableauEtats) {
		for (int i= 0;i < matrice.getComponents().length;i++) {
			boolean value = ((Boolean)tableauEtats.objectAtIndex(i)).booleanValue();
			((AbstractButton)matrice.getComponent(i)).setSelected(value);
		}
	}

}
