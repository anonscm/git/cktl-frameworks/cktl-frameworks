/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components.utilities;

import org.cocktail.client.components.AbstractSimpleDialog;
import org.cocktail.client.components.DialogueDouble;
import org.cocktail.client.components.DialogueDoubleSansFetch;
import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.DialogueSimpleAvecStrings;
import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** M&ecirc;me classe que DialogUtilities<BR>
 * M&eacute;thods statiques pour afficher des dialogues de s&eacute;lection */
public class UtilitairesDialogue {
	/** Affichage d'un dialogue modal de type DialogueSimple ou DialogueDouble
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite	nom entit&eacute; &agrave; afficher
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param aChampRecherche true si champ de recherche
	 * @param estSelectionMultiple true si s&eacute;lection multiple
	 * @param qualifier	qualifier de la liste d'objets &agrave; afficher, peut &ecirc;tre null
	 * @param dialogueSimple true si dialogue simple
	 * @param sansDoublonLibelle true si il ne doit pas y avoir de doublons dans les libell&eacute;s
	 */
	public static void afficherDialogue(Object caller,String nomEntite,String nomMethodeNotification,boolean aChampRecherche,boolean estSelectionMultiple,EOQualifier qualifier,boolean dialogueSimple,boolean sansDoublonLibelle) {
		LogManager.logDetail("Affichage du dialogue de selection pour : " + nomEntite);
		if (qualifier != null) {
			LogManager.logDetail("Qualifier de restriction : " + qualifier);
		}
		String nomNotification = caller.getClass().getName() + "_Selection" + nomEntite;
		String titreFenetre = "Sélection " + nomEntite;
		AbstractSimpleDialog controleur = null;
		if (dialogueSimple) {
			controleur = new DialogueSimple(nomEntite,nomNotification,titreFenetre,aChampRecherche,estSelectionMultiple,true,sansDoublonLibelle);
			controleur.init();
		} else {
			DialogueDouble controleurDouble = new DialogueDouble(nomEntite,nomNotification,titreFenetre,aChampRecherche,estSelectionMultiple,true,sansDoublonLibelle);
			controleurDouble.init();
			controleur = (AbstractSimpleDialog)controleurDouble;
		}
		if (qualifier != null) {
			controleur.setQualifier(qualifier);
		}
		// enregistre l'appelant pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(nomMethodeNotification,new Class[] {NSNotification.class}),nomNotification,null);
		controleur.displayDialog();
	}
	/** Affichage d'un dialogue modal de type DialogueSimple ou DialogueDouble sans s&eacute;lection multiple, ni suppression des doublons
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite	nom entit&eacute; &agrave; afficher
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param aChampRecherche true si champ de recherche
	 * @param qualifier	qualifier de la liste d'objets &agrave; afficher, peut &ecirc;tre null
	 * @param dialogueSimple true si dialogue simple
	 */
	public static void afficherDialogue(Object caller,String nomEntite,String nomMethodeNotification,boolean aChampRecherche,EOQualifier qualifier,boolean dialogueSimple) {
		afficherDialogue(caller, nomEntite, nomMethodeNotification, aChampRecherche,false, qualifier, dialogueSimple,false);
	}
	/** Affichage d'un dialogue modal de type DialogueSimple ou DialogueDouble sans champ de recherche, sans s&eacute;lection multiple, ni suppression des doublons
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite	nom entit&eacute; &agrave; afficher
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param dialogueSimple true si dialogue simple
	 */
	public static void afficherDialogue(Object caller,String nomEntite,String nomMethodeNotification,boolean dialogueSimple) {
		afficherDialogue(caller, nomEntite, nomMethodeNotification, false,false, null, dialogueSimple,false);
	}
	/** Affichage d'un dialogue modal de type DialogueSimple sans champ de recherche, sans s&eacute;lection multiple, ni suppression des doublons
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite	nom entit&eacute; &agrave; afficher
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 */
	public static void afficherDialogue(Object caller,String nomEntite,String nomMethodeNotification) {
		afficherDialogue(caller, nomEntite, nomMethodeNotification, false,false, null, true,false);
	}
	/** Affichage d'un dialogue modal de type DialogueSimpleSimpleSansFetch ou DialogueDoubleSansFetch : les fetchs ne sont faits que sur
	 * demande de l'utilisateur
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite nom Entit&eacute;e affich&eacute;e
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param attributCodePourRecherche attribut du code sur lequel la recherche est faite (peut &ecirc;tre nul si on ne fait des recherches que sur les libell&eacute;s)
	 * @param attributLibellePourRecherche attribut du libell&eacute; sur lequel la recherche est faite
	 * @param estSelectionMultiple true si on permet une s&eacute;lection multiple
	 * @param qualifier qualifier suppl&eacute;mentaire &agrave; prendre en compte
	 * @param peutToutRechercher true si la saisie d'un libell&eacute;n'est pas obligatoire;
	 * @param sansCaractereAccentue true si la recherche est faite sans caract&egrave;re accentu&eacite;
	 * @param sansDoublonLibelle doit-on supprimer les doublons dans les libell&eacute;s
	 */
	public static void afficherDialogueSansFetch(Object caller,String nomEntite,String nomMethodeNotification,String attributCodePourRecherche, String attributLibellePourRecherche,boolean estSelectionMultiple,EOQualifier qualifier,boolean peutToutRechercher,boolean sansCaractereAccentue, boolean sansDoublonLibelle) {
		NSLog.out.appendln("Affichage du dialogue de selection pour : " + nomEntite);
		if (qualifier != null) {
			NSLog.out.appendln("Qualifier de restriction : " + qualifier);
		}
		String nomNotification = nomNotificationPourCallerEtEntite(caller,nomEntite);
		String titreFenetre = "Selection " + nomEntite;
		DialogueSimpleSansFetch controleur = null;
		if (attributCodePourRecherche == null) {
			controleur = new DialogueSimpleSansFetch(nomEntite,nomNotification,attributLibellePourRecherche,titreFenetre,estSelectionMultiple,true,sansDoublonLibelle,sansCaractereAccentue,peutToutRechercher);
			controleur.init();
		} else {
			DialogueDoubleSansFetch controleurDouble = new DialogueDoubleSansFetch(nomEntite,nomNotification,attributCodePourRecherche, attributLibellePourRecherche,titreFenetre,estSelectionMultiple,true,sansDoublonLibelle,sansCaractereAccentue,peutToutRechercher);
			controleurDouble.init();
			controleur = (DialogueSimpleSansFetch)controleurDouble;
		}
	if (qualifier != null) {
		controleur.setQualifier(qualifier);
	}
	// enregistrer l'appelant pour recevoir les notifications
	NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(nomMethodeNotification,new Class[] {NSNotification.class}),nomNotification,null);
	controleur.displayDialog();
	}
	/** Affichage d'un dialogue modal de type DialogueSimpleSimpleSansFetch : les fetchs ne sont faits que sur
	 * demande de l'utilisateur 
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite nom Entit&eacute;e affich&eacute;e
	 * @param attributLibellePourRecherche attribut du libell&eacute; sur lequel la recherche est faite
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param qualifier qualifier suppl&eacute;mentaire &agrave; prendre en compte
	 * @param peutToutRechercher true si la saisie d'un libell&eacute;n'est pas obligatoire;
	 * @param sansCaractereAccentue true si la recherche est faite sans caract&egrave;re accentu&eacite;
	 * @param sansDoublonLibelle doit-on supprimer les doublons dans les libell&eacute;s
	 * */
	public static void afficherDialogueSansFetch(Object caller,String nomEntite,String nomMethodeNotification,String attributLibellePourRecherche,boolean estSelectionMultiple,EOQualifier qualifier,boolean peutToutRechercher,boolean sansCaractereAccentue, boolean sansDoublonLibelle) {
		afficherDialogueSansFetch(caller, nomEntite, nomMethodeNotification,null, attributLibellePourRecherche, estSelectionMultiple,qualifier,peutToutRechercher,sansCaractereAccentue,sansDoublonLibelle);
	}
	/** Affichage d'un dialogue modal affichant une liste de strings
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la string ou le tableau de strings s&eacute;lectionn&acute;
	 * @param titreDialogue titre du dialogue
	 * @param estSelectionMultiple true si on permet une s&eacute;lection multiple
	 * @param strings liste de strings &agrave; afficher
	 * */	
	public static void afficherDialogueAvecStrings(Object caller,String nomMethodeNotification,String titreDialogue,boolean estSelectionMultiple,NSArray strings) {
		DialogueSimpleAvecStrings controleur = new DialogueSimpleAvecStrings(nomMethodeNotification,titreDialogue,estSelectionMultiple,true,strings);
		controleur.init();
		String nomNotification = caller.getClass().getName() + "_SelectionStrings";
		// enregistrer l'appelant pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(nomMethodeNotification,new Class[] {NSNotification.class}),nomNotification,null);
		controleur.displayDialog();	
	}
	/** Affichage d'un dialogue modal affichant une liste de strings sans s&eacute;lection multiple
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la string ou le tableau de strings s&eacute;lectionn&acute;
	 * @param titreDialogue titre du dialogue
	 * @param strings liste de strings &agrave; afficher
	 * */	
	public static void afficherDialogueAvecStrings(Object caller,String nomMethodeNotification,String titreDialogue,NSArray strings) {
		afficherDialogueAvecStrings(caller, nomMethodeNotification, titreDialogue, false, strings);
	}
	/** Retourne le nom de la notification envoy&eacute;e pour fournir l'objet s&eacute;lectionn&eacute;
	 * 
	 * @param caller objet appelant cette m&eacute;thode
	 * @param nomEntite entit&eacute; affich&eacute;e dans le dialogue
	 * @return name of the notification
	 */
	public static String nomNotificationPourCallerEtEntite(Object caller,String nomEntite) {
		return 	caller.getClass().getName() + "_Selection" + nomEntite;
	}
}
