/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components.utilities;

import org.cocktail.client.components.AbstractSimpleDialog;
import org.cocktail.client.components.DialogueDouble;
import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.DialogueSimpleAvecStrings;
import org.cocktail.client.components.DoubleDialogNoFetch;
import org.cocktail.client.components.SimpleDialogNoFetch;
import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Same class as UtilitairesDialogue<BR>
 * Static methods to display selection dialogs<BR>Static methods to display selection dialogs<BR>
 */
public class DialogUtilities {
	/** Display a modal dialog of type SimpleDialog or DoubleDialog
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param canSearch true if there is a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param qualifier qualifier to restrict the displayed objects, can be null
	 * @param isSimpleDialog true if it's a simple dialog (one column in the table)
	 * @param displayDistinctValues true if the displayed values are unique
	 **/
	public static void displayDialog(Object caller,String entityName,String validationNotificationMethod,boolean canSearch,boolean multipleSelectionAllowed,EOQualifier qualifier,boolean isSimpleDialog,boolean displayDistinctValues) {
		LogManager.logDetail("Display selection dialog for entity: " + entityName);
		if (qualifier != null) {
			LogManager.logDetail("Restriction Qualifier: " + qualifier);
		}
		String nomNotification = caller.getClass().getName() + "_Selection" + entityName;
		String titreFenetre = "Selection " + entityName;
		AbstractSimpleDialog controleur = null;
		if (isSimpleDialog) {
			controleur = new DialogueSimple(entityName,nomNotification,titreFenetre,canSearch,multipleSelectionAllowed,true,displayDistinctValues);
			controleur.init();
		} else {
			DialogueDouble controleurDouble = new DialogueDouble(entityName,nomNotification,titreFenetre,canSearch,multipleSelectionAllowed,true,displayDistinctValues);
			controleurDouble.init();
			controleur = (AbstractSimpleDialog)controleurDouble;
		}
		if (qualifier != null) {
			controleur.setQualifier(qualifier);
		}
		// enregistre l'appelant pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(validationNotificationMethod,new Class[] {NSNotification.class}),nomNotification,null);
		controleur.displayDialog();
	}
	/** Display a modal dialog of type SimpleDialog or DoubleDialog with all the objects qualided by the qualifier
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param canSearch true if there is a text field for searching objects
	 * @param qualifier qualifier to restrict the displayed objects, can be null
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isSimpleDialog true if it's a simple dialog (one column in the table)
	 **/
	public static void displayDialog(Object caller,String entityName,String validationNotificationMethod,boolean canSearch,EOQualifier qualifier,boolean isSimpleDialog) {
		displayDialog(caller, entityName, validationNotificationMethod, canSearch,false, qualifier, isSimpleDialog,false);
	}
	/** Display a modal dialog of type SimpleDialog or DoubleDialog with all the objects  with single selection and noSearch
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param isSimpleDialog true if it's a simple dialog (one column in the table)
	 **/
	public static void displayDialog(Object caller,String entityName,String validationNotificationMethod,boolean isSimpleDialog) {
		displayDialog(caller, entityName, validationNotificationMethod, false,false, null, isSimpleDialog,false);
	}
	/** Display a modal dialog of type SimpleDialog with all the objects  with single selection and noSearch
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 **/
	public static void displayDialog(Object caller,String entityName,String validationNotificationMethod) {
		displayDialog(caller, entityName, validationNotificationMethod, false,false, null, true,false);
	}
	/** Display a modal dialog of type SimpleDialogNoFetch or DoubleDialogNoFetch : fetchs are made when the user clicks
	 * the "Search" button
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param codeAttributeToSearch attribute corresponding to the first column on which to search records (can be null if only titles are searched)
	 * @param attributeToSearch attribute corresponding to the second column on which to search records
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param qualifier qualifier to restrict the displayed objects, can be null
	 * @param canSearchAll true if the search text can be null;
	 * @param isBasicCharacter true if the search is made with basic characters (no accentuated characters)
	 * @param displayDistinctValues true if the displayed values are unique
	 **/
	
	public static void displayDialogNoFetch(Object caller,String entityName,String validationNotificationMethod,String codeAttributeToSearch, String attributeToSearch,boolean multipleSelectionAllowed,EOQualifier qualifier,boolean canSearchAll, boolean isBasicCharacter,boolean displayDistinctValues) {
		NSLog.out.appendln("Display selection dialog for entity: " + entityName);
		if (qualifier != null) {
			NSLog.out.appendln("Restriction Qualifier: " + qualifier);
		}
		String notificationName = caller.getClass().getName() + "_Selection" + entityName;
		String dialogTitle = "Selection " + entityName;
		SimpleDialogNoFetch controller = null;
		if (codeAttributeToSearch == null) {
				controller = new SimpleDialogNoFetch(entityName,attributeToSearch,notificationName,dialogTitle,multipleSelectionAllowed,true,displayDistinctValues,isBasicCharacter,canSearchAll);
				controller.init();
			} else {
				DoubleDialogNoFetch doubleController = new DoubleDialogNoFetch(entityName,attributeToSearch,codeAttributeToSearch, notificationName,dialogTitle,multipleSelectionAllowed,true,displayDistinctValues,isBasicCharacter,canSearchAll);
				doubleController.init();
				controller = (SimpleDialogNoFetch)doubleController;
			}
			//}
		if (qualifier != null) {
			controller.setQualifier(qualifier);
		}
		// enregistrer l'appelant pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(validationNotificationMethod,new Class[] {NSNotification.class}),notificationName,null);
		controller.displayDialog();
	}
	/** Display SimpleDialogNoFetch modal dialog with any records qualified by the qualifier:
	 * fetchs are made when the user clicks the "Search" button
	 * @param caller object calling this method
	 * @param entityName entity to be displayed
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param codeAttributeToSearch attribute corresponding to the first column on which to search records (can be null if only titles are searched)
	 * @param attributeToSearch attribute corresponding to the second column on which to search records
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param qualifier qualifier to restrict the displayed objects, can be null
	 * @param canSearchAll true if the search text can be null;
	 * @param isBasicCharacter true if the search is made with basic characters (no accentuated characters)
	 * @param displayDistinctValues true if the displayed values are unique
	 **/
	public static void displayDialogNoFetch(Object caller,String entityName,String validationNotificationMethod,String attributeToSearch,boolean multipleSelectionAllowed,EOQualifier qualifier,boolean canSearchAll, boolean isBasicCharacter,boolean displayDistinctValues) {
		displayDialogNoFetch(caller, entityName, validationNotificationMethod,null, attributeToSearch, multipleSelectionAllowed,qualifier,canSearchAll,isBasicCharacter,displayDistinctValues);
	}
	
	/** Modal dialog displaying a list of Strings
	 * @param caller object calling this method
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param strings liste of strings to display
	 * */	
	public static void displayDialogWithStrings(Object caller,String validationNotificationMethod,String dialogTitle,boolean multipleSelectionAllowed,NSArray strings) {
		DialogueSimpleAvecStrings controller = new DialogueSimpleAvecStrings(validationNotificationMethod,dialogTitle,multipleSelectionAllowed,true,strings);
		controller.init();
		String notificationName = caller.getClass().getName() + "_SelectionStrings";
		// enregistrer l'appelant pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(caller,new NSSelector(validationNotificationMethod,new Class[] {NSNotification.class}),notificationName,null);
		controller.displayDialog();	
	}
	/** Modal dialog displaying a list of Strings in single selection mode
	 * @param caller object calling this method
	 * @param validationNotificationMethod name of the notification method to be invoked in the caller when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param strings liste of strings to display
	 * */		
	public static void displayDialogWithStrings(Object caller,String validationNotificationMethod,String dialogTitle,NSArray strings) {
		displayDialogWithStrings(caller, validationNotificationMethod, dialogTitle, strings);
	}
	/** Returns the name of the notification used to provide the selected object
	 * 
	 * @param entityName entity to be selected
	 * @param aClass class of the controller displaying the dialog
	 * @return name of the notification
	 */
	public static String notificationNameForEntityAndClass(String entityName, Class aClass) {
		return 	aClass.getName() + "_Selection" + entityName;
	}
}
