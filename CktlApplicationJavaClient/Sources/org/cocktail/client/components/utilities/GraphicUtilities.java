
//UtilitairesGraphiques.java
//PapayeAdmin

//Created by Christine Buttin on Tue Jun 10 2003.
//Copyright (c) 2003 __MyCompanyName__. All rights reserved.

/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components.utilities;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Enumeration;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;

import org.cocktail.component.COTextField;

import com.webobjects.eoapplication.EOUserInterfaceParameters;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.eointerface.swing.EOViewLayout;
import com.webobjects.eointerface.swing._EODefaultBorder;
import com.webobjects.foundation.NSArray;

// 02/02/2011 - ajout d'une méthode pour centrer les vues
public class GraphicUtilities {
	public final static Color COL_FOND_ACTIF = Color.WHITE;
	public final static Color COL_TEXTE_ACTIF = Color.BLACK;
	public final static Color COL_FOND_INACTIF = new Color((float)0.8,(float)0.8,(float)0.8,(float)1.0);
	//  public final static Color COL_FOND_INACTIF = Color.LIGHT_GRAY;
	public final static Color COL_TEXTE_INACTIF = Color.BLACK;

	/** Pr&eacute;pare les boutons d'une vue en leur affectant comme image si elle existe, le nom du bouton
	 * Le tableau pass&eacute; en param&egrave;tre contient les composants de la vue
	 * Change aussi les fonts des composants
	 * @param components
	 */
	public static void preparerInterface(NSArray components) {
		Enumeration e = components.objectEnumerator();
		while (e.hasMoreElements()) {
			Component component = (Component)e.nextElement();
			preparerFont(component);
			if (component.getClass().getName().equals("javax.swing.JButton")) {
				//System.out.println(((JButton)component).getText());
				preparerBouton((JButton)component,((JButton)component).getText(),true);
			} else if (component instanceof EOView) {
				NSArray componentsVue = new NSArray(((EOView)component).getComponents());
				preparerInterface(componentsVue);
			} else if (component instanceof JTabbedPane) {
				JTabbedPane pane = (JTabbedPane)component;
				for (int i = 0; i < pane.getTabCount();i++) {
					EOView sousPane = (EOView)pane.getComponentAt(i);
					NSArray componentsVue = new NSArray(((EOView)sousPane).getComponents());
					preparerInterface(componentsVue);
				}
			} 
		}
	}
	public static void preparerBouton(JButton bouton,String nomImage,boolean enabled) {
		if (bouton != null) {   // dans certains cas, tous les boutons de la page n'existent pas
			if (nomImage != null) {
				Icon icone = EOUserInterfaceParameters.localizedIcon(nomImage);
				if (icone != null) {
					bouton.setBorderPainted(false);
					bouton.setIcon(icone);
					bouton.setText("");
					bouton.setName(nomImage);
				} 
			}
			bouton.setEnabled(enabled);	
		}
	}
	public static void preparerBouton(JButton bouton,String nomImage) {
		preparerBouton(bouton,nomImage,true);
	}
	/** remplir un pop-up avec un tableau de valeurs */
	public static void remplirPopup(JComboBox popup,Object[] anArray) {
		popup.removeAllItems();
		for (int i = 0; i < anArray.length;i++) {
			popup.addItem(anArray[i]);
		}
	}
	/** To make a table not editable */
	public static void makeTableReadOnly(EOTable aTable) {
		JTable actualTable = aTable.table();
		java.util.Enumeration e = actualTable.getColumnModel().getColumns() ;
		while (e.hasMoreElements()) {
			((TableColumn)e.nextElement()).setCellEditor(null);
		}
	}
	public static void changeTableFont(EOTable aTable,Font font) {
		JTable actualTable = aTable.table();
		actualTable.setFont(font);
	}
	/** Change the font of a text field */
	public static void changeFont(COTextField field,Font newFont) {
		if (field != null) {
			field.setFont(newFont);
		}
	}
	/** Rendre une table non &eacute;ditable */
	public static void rendreNonEditable(EOTable aTable) {
		makeTableReadOnly(aTable);
	}
	public static void changerTaillePolice(EOTable aTable,int taille) {
		JTable actualTable = aTable.table();
		Font police = actualTable.getFont();
		Font nouvellePolice = new Font(police.getName(),police.getStyle(),taille);
		actualTable.setFont(nouvellePolice);
	}
	public static void changerHauteurLigne(EOTable aTable,int hauteur) {
		JTable actualTable = aTable.table();
		actualTable.setRowHeight(hauteur);
	}
	public static void swaperView(JComponent container,JComponent nouvelleVue) {
		if (nouvelleVue != null) {
			preparerInterface(new NSArray(nouvelleVue.getComponents()));
		}
		container.setVisible(false);
		container.removeAll();
		container.setLayout(new BorderLayout());
		if (nouvelleVue != null) {
			container.add(nouvelleVue,BorderLayout.CENTER);
		}
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewSansPreparation(JComponent container,JComponent nouvelleVue) {
	
		container.setVisible(false);
		container.removeAll();
		container.setLayout(new BorderLayout());
		if (nouvelleVue != null) {
			container.add(nouvelleVue,BorderLayout.CENTER);
		}
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewAvecVueLayout(JComponent container,JComponent nouvelleVue) {
		container.setVisible(false);
		container.removeAll();
		container.setLayout(new EOViewLayout());
		if (nouvelleVue != null) {
			container.add(nouvelleVue, new Integer(EOViewLayout.HeightSizable));
		}
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewEtCentrer(JComponent container,JComponent nouvelleVue) {
		container.setVisible(false);
		container.removeAll();
		int positionGauche = (container.getWidth() - nouvelleVue.getWidth()) / 2;
		if (positionGauche < 0) {
			positionGauche = 0;
		}
		int positionEnHaut = (container.getHeight() - nouvelleVue.getHeight()) / 2;
		if (positionEnHaut < 0) {
			positionEnHaut = 0;
		}
		nouvelleVue.setLocation(positionGauche,positionEnHaut);
		if (nouvelleVue != null) {
			container.add(nouvelleVue);
		}
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewEtCentrer(JComponent container,JComponent nouvelleVue, int largeurContainer,int hauteurContainer) {
		container.setVisible(false);
		container.removeAll();
		int positionGauche = (largeurContainer - nouvelleVue.getWidth()) / 2;
		if (positionGauche < 0) {
			positionGauche = 0;
		}
		int positionEnHaut = (hauteurContainer - nouvelleVue.getHeight()) / 2;
		if (positionEnHaut < 0) {
			positionEnHaut = 0;
		}
		nouvelleVue.setLocation(positionGauche,positionEnHaut);
		if (nouvelleVue != null) {
			container.add(nouvelleVue);
		}
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewEtCentrerDansEOView(JComponent container, JComponent nouvelleVue) {
		// 02/02/2011 - Pour que les composants soient centrés, on calcule leur position de centrage et on les insère dans une EOView
		EOView nouveauPanel = new EOView();
		container.setVisible(false);
		container.removeAll();
		int positionGauche = (container.getWidth() - nouvelleVue.getWidth()) / 2;
		if (positionGauche < 0) {
			positionGauche = 0;
		}
		int positionEnHaut = (container.getHeight() - nouvelleVue.getHeight()) / 2;
		if (positionEnHaut < 0) {
			positionEnHaut = 0;
		}
		nouveauPanel.setLocation(positionGauche, positionEnHaut);
		nouveauPanel.setSize(nouvelleVue.getSize());
		container.add(nouveauPanel);
		nouveauPanel.add(nouvelleVue);
		container.setVisible(true);
		container.validate();
	}
	public static void swaperViewEtCentrerDansEOView(JComponent container, JComponent nouvelleVue,int largeurContainer,int hauteurContainer) {
		// 02/02/2011 - Pour que les composants soient centrés, on calcule leur position de centrage et on les insère dans une EOView
		EOView nouveauPanel = new EOView();
		container.setVisible(false);
		container.removeAll();
		int positionGauche = (largeurContainer - nouvelleVue.getWidth()) / 2;
		if (positionGauche < 0) {
			positionGauche = 0;
		}
		int positionEnHaut = (hauteurContainer - nouvelleVue.getHeight()) / 2;
		if (positionEnHaut < 0) {
			positionEnHaut = 0;
		}
		nouveauPanel.setLocation(positionGauche, positionEnHaut);
		nouveauPanel.setSize(nouvelleVue.getSize());
		container.add(nouveauPanel);
		nouveauPanel.add(nouvelleVue);
		container.setVisible(true);
		container.validate();
	}
	private static void preparerFont(Component component) {
		Font font = component.getFont();
		if (component instanceof JTable == false) {
			if (font.getName().equals("Serif") || font.getName().equals("SansSerif")) {
				component.setFont(new Font("Helvetica",font.getStyle(),font.getSize()));
			}
		}
		if (component instanceof EOView) {	// pour les boîtes avec titre		
			Border border = ((EOView)component).getBorder();
			if (border != null && border instanceof _EODefaultBorder) {
				font = ((_EODefaultBorder)border).font();
				if (font != null && (font.getName().equals("Serif") || font.getName().equals("SansSerif"))) {
					((_EODefaultBorder)border).setFont(new Font("Helvetica",font.getStyle(),font.getSize()));
				}
			}
		}
	}
}
