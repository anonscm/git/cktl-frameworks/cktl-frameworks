/*
 * Created on 13 mai 2005
 *
 * Affiche un dialogue de sélection et permet la création de nouveaux objets
 * les objets métiers qui sont affichés doivent répondre aux méthodes libelle() et setLibelle()
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.RecordWithTitle;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSMutableArray;

/** Same class as SelectionWithCreation
 * @author christine<BR>
 * Display a selection dialog where the user can create new enterprise objects. The enterprise objects displayed in the dialog must 
 * implement the RecordWithTitle interface<BR>
 */
public abstract class SelectionWithCreation extends SimpleDialog {
	private String inputText;
	
	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param isModal true if it's a modal dialog
	 * @param displayDistinctValues true if the displayed values are unique
	 */
	public SelectionWithCreation(String entityName,String validationNotification,String dialogTitle,boolean isModal,boolean displayDistinctValues) {
		// on met à true aChampRecherche pour laisser l'espace de saisie visible dans la vue
		super(entityName,validationNotification,dialogTitle,false,false,isModal,displayDistinctValues);
	}
	/** Constructor. Display all the values
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param isModal true if it's a modal dialog
	 */
	public SelectionWithCreation(String entityName,String validationNotification,String dialogTitle,boolean isModal) {
		// on met à true aChampRecherche pour laisser l'espace de saisie visible dans la vue
		this(entityName,validationNotification,dialogTitle,isModal,false);
	}
	public void init() {
		EOArchive.loadArchiveNamed("SelectionWithCreation",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	 }
	// Accesseurs
	/** Return the text the user input */
	public String inputText() {
		return inputText;
	}
	/** Modifies the text the user input */
	public void setInputText(String aStr) {
		inputText = aStr;
	}
	// Méthodes protégées
	/** If an object has been created, insert it in the display group and the editing context, save the changes and return its globalID
	 * else return the globalID of the selectedObject
	 */
	protected Object selectedObject() {
		if (inputText() != null && inputText().length() > 0) {
			EOClassDescription descriptionClass = EOClassDescription.classDescriptionForEntityName(entityName());
			EOGenericRecord record = (EOGenericRecord)descriptionClass.createInstanceWithEditingContext(editingContext(),null);
			if (record instanceof RecordWithTitle) {
				record.takeValueForKey(inputText(),"title");
			} 
			evaluatePrimaryKey(record);
			editingContext().insertObject(record);
			editingContext().saveChanges();
			NSMutableArray objects = new NSMutableArray(displayGroup().allObjects());
			objects.addObject(record);
			displayGroup().setObjectArray(objects);
			displayGroup().selectObject(record);
			displayGroup.updateDisplayedObjects();
        }
		return super.selectedObject();
	}
	/** Evaluate the record primary key. Do nothing if the primary key is managed automatically by EOF */
	protected abstract void evaluatePrimaryKey(EOGenericRecord record);
}
