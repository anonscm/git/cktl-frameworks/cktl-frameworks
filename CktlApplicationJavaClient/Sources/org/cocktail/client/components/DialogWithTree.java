/*
 * Created on 11 avr. 2006
 *
 * Choix de données dans une vue arborescente
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;

import org.cocktail.component.utilities.CRITreeView;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Display a modal dialog with a tree view.Abstract methods are defined to attach data to the nodes of the tree.
 *  Default font of the label and the tree is Helvetica 11<BR>
 *  Affiche un dialogue modal avec choix de donn&eacute;es dans une vue arborescente.
 *  Les m&eacute;thodes abstraites permettent de d&eacute;finir les donn&eacute;es attach&eacute;es &agrave; cet arbre.
 * 	La police du label et de l'arbre est Helvetica 11<BR>
 * @author christine
 *
 */
//24/09/2010 - modification pour qu'en cas de fermeture de la fenêtre sans validation, il n'y ait pas d'objet sélectionné (DT 2839)
public abstract class DialogWithTree extends EOModalDialogController {
	/** Outlet */
	public CRITreeView treeView;
	private Font treeFont;
	public EODisplayGroup controllerDisplayGroup;
	public EODisplayGroup displayGroup;
	public EOEditingContext editingContext;
	private EOGenericRecord selectedObject;
	private Dimension treeViewSize;	// 24/09/2010 - le tree view change de taille (DT 2839)

	/** 
	 * Constructor
	 */
	public DialogWithTree() {
		super();
		loadArchive();
		init();
		treeViewSize = treeView.getSize();
	}
	public void connectionWasEstablished() {
		setWindowResizable(window(),false);
		((JDialog)window()).setModal(true);
		((JDialog)window()).setTitle(dialogTitle());
		if (treeView != null && treeFont != null) {
			treeView.setFont(treeFont);
		}
		loadNotifications();
	}
	public void connectionWasBroken() {
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}	
	/** Select an object providing the way to load data<BR>
	 * @param fetchOnLoad true if data is fetch when the dialog is displayed
	 * @param manageNodes true if enterprise objects implement the DynamicTreeInterface interface
	 * An Exception is thrown when the tree cannot be initialized
	 */
	public EOGenericRecord selectObject(EOEditingContext editingContext,boolean fetchOnLoad,boolean manageNodes) throws Exception {
		treeView.setSize(treeViewSize);
		if (manageNodes && fetchOnLoad) {
			// Un arbre qui gère ses propres fils est nécessairement dynamique
			fetchOnLoad = false;
		}
		selectedObject = null;
		initPourSelection();
		treeView.initialize(editingContext,entityName(),true,fetchOnLoad,manageNodes);
		activateWindow();	
		
		return selectedObject;
	}
	/** Select an object with fetching data on load.<BR>
	 * An Exception is thrown when the tree cannot be initialized */
	public EOGenericRecord selectObject(EOEditingContext editingContext) throws Exception {
		return selectObject(editingContext,true,false);
	}
	/** S&eacute;lection d'un objet en pr&eacute;cisant le mode de chargement des donn&eacute;es
	 * @param toutCharger true si les donn&eacute;es sont charg&eacute;es &agrave; l'affichage du dialogue
	 * @param gereFils true si les objets m&eacute;iers impl&eacute;mentent l'interface InterfaceArbreAvecFils
	 * Lance une exception si l'arbre ne peut pas &ecirc;tre initialis&eacute;
	 */
	public EOGenericRecord selectionnerObjet(EOEditingContext editingContext,boolean toutCharger,boolean gereFils) throws Exception {
		if (gereFils && toutCharger) {
			// Un arbre qui gère ses propres fils est nécessairement dynamique
			toutCharger = false;
		}
		selectedObject = null;
		initPourSelection();
		treeView.initialiser(editingContext,entityName(),true,toutCharger,gereFils);
		activateWindow();	
		
		return selectedObject;
	}
	/** S&eacute;lection d'un objet en chargeant toutes les donnn&eacute;es
	 * Lance une exception si l'arbre ne peut pas &ecirc;tre initialis&eacute; */
	public EOGenericRecord selectionnerObjet(EOEditingContext editingContext) throws Exception {
		return selectionnerObjet(editingContext,true,false);
	}
	// Notifications
	/** Notification sent by a simple click on the tree view : a node has been selected */
	public void notificationSimpleClic(NSNotification aNotif){
		if (aNotif.object() == treeView) {
			NSDictionary result = (NSDictionary)aNotif.userInfo();
			selectedObject = (EOGenericRecord)result.objectForKey("selectedRecord");
			controllerDisplayGroup.redisplay();
		}
	}
	/** Notification sent by a double click on the tree view : a node has been selected and validated as selection */
	public void notificationDoubleClic(NSNotification aNotif) {
		if (aNotif.object() == treeView) {
			NSDictionary result = aNotif.userInfo();
			selectedObject = (EOGenericRecord)result.objectForKey("selectedRecord");
			if (canSelectObject(selectedObject)) {
				deactivateWindow();
			}
		}
	}

	// Actions
	public void validate() {
		closeWindow();
	}
	public void cancel() {
		selectedObject = null;
		closeWindow();	
	}
	// Méthodes du controller DG
	/** Return true if an object of the tree is selected.<BR>
	 * Retourne true si un objet de l'arbre est s&eacute;lectionn&eacute;
	 */
	public boolean canValidate() {
		return selectedObject != null;
	}
	// Accessors
	public CRITreeView treeView() {
		return treeView;
	}
	public void setTreeView(CRITreeView treeView) {
		this.treeView = treeView;
	}
	public Font treeFont() {
		return treeFont;
	}
	public void setTreeFont(Font treeFont) {
		this.treeFont = treeFont;
	}
	public EODisplayGroup controllerDisplayGroup() {
		return controllerDisplayGroup;
	}
	public void setControllerDisplayGroup(EODisplayGroup controllerDisplayGroup) {
		this.controllerDisplayGroup = controllerDisplayGroup;
	}
	public EODisplayGroup displayGroup() {
		return displayGroup;
	}
	public void setDisplayGroup(EODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}
	public EOEditingContext getEditingContext() {
		return editingContext;
	}
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	// méthodes protégées
	protected void loadArchive() {
		EOArchive.loadArchiveNamed("DialogWithTree", this,"org.cocktail.client.components.interfaces", this.disposableRegistry());
	}
	/** title of the dialog */
	protected abstract String dialogTitle();
	/** Title of the tree */
	protected abstract String treeTitle();
	/** Displayed entity */
	protected abstract String entityName();
	/** Qualifier for column */
	protected abstract EOQualifier qualifierForColumn();
	/** Qualifier to restrict the displayed nodes */
	protected abstract EOQualifier restrictionQualifier();
	/** Relationship to the parent */
	protected abstract String parentRelationship();
	/** Attribute to display */
	protected abstract String attributeForDisplay();
	/** Returns true if the user can select the object by double-clicking */
	protected abstract boolean canSelectObject(EOGenericRecord objet);
	protected void updateTreeView() {
		treeView.setRestrictionQualifier(restrictionQualifier());
		treeView.update((javax.swing.tree.TreePath)null);
	}
	// méthodes privées
	private void init() {
		// Prepare the controller display group
		if (controllerDisplayGroup != null) {
			controllerDisplayGroup.setObjectArray(new NSArray(this));
			controllerDisplayGroup.setSelectedObject(this);
		}
		treeView.setDynamic(false);
		treeView.setTitle(treeTitle());
		treeView.setParentRelationship(parentRelationship());
		treeView.setFieldForDisplay(attributeForDisplay());
	}
	private void initPourSelection() {
		treeView.setQualifierForFirstColumn(qualifierForColumn());
		treeView.setRestrictionQualifier(restrictionQualifier());
	}
	private void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationSimpleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_CLICK,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationDoubleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_DOUBLE_CLICK,null);		// Chargement de l'organigramme
	}
	/** 
	 * To listen for the double-click 
	 */
	public class ClickTbvListener extends MouseAdapter { 
		public void mouseClicked(MouseEvent e) { 
			if (e.getClickCount() == 2) 
				validate();
		} 
	}

}
