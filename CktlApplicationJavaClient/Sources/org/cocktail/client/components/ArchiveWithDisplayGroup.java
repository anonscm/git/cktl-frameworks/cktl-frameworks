/*
 * Created on 20 juin 2005
 *
 * Classe de base pour des archives supportant des display group
 */
/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOArchiveController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/**
 * @author christine<BR>
 *
 * Abstract class for archive controllers using a display group and an editing context
 */
public abstract class ArchiveWithDisplayGroup extends EOArchiveController {
	public EOEditingContext editingContext;
	public EODisplayGroup displayGroup;
	private String archiveName;
	private String packageName;

	/** Constructor
	 * @param editingContext editing context used to fetch objects
	 * @param archiveName name of the interface file
	 * @param packageName name of the package that contains the interface file
	 */
	public ArchiveWithDisplayGroup(EOEditingContext editingContext,String archiveName,String packageName) {
		super();
		this.archiveName = archiveName;
		this.packageName = packageName;
		this.editingContext = editingContext;
	}
	/** Constructor
	 * 
	 * @param archiveName name of the interface file
	 * @param packageName name of the package that contains the interface file
	 */
	public ArchiveWithDisplayGroup(String archiveName,String packageName) {
		this(null,archiveName,packageName);
	}
	/** Constructor
	 * @param editingContext editing context used to fetch objects
	 * @param archiveName name of the interface file
	 */
	public ArchiveWithDisplayGroup(EOEditingContext editingContext,String archiveName) {
		this(archiveName);
		this.editingContext = editingContext;
	}
	/** Constructor
	 * 
	 * @param archiveName name of the interface file
	 */
	public ArchiveWithDisplayGroup(String archiveName) {
		this(archiveName,null);
	}

	// accessors
	/** Returns the display group */
	public EODisplayGroup displayGroup() {
		return displayGroup;
	}
	/** To set up the display group */
	public void setDisplayGroup(EODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}
	/** Returns the editing context */
	public EOEditingContext editingContext() {
		return editingContext;
	}
	/** initialization method : loads the interface file
	 * Objects are not fetch */
	public void init() {
		// pour gérer le cas où l'archive doit être initialisée avec un editing context spécifique
		// le fait de charger l'archive change l'editing context
		EOEditingContext editingContextParent = editingContext;
		EOArchive.loadArchiveNamed(archiveName,this,packageName,this.disposableRegistry());
		if (editingContextParent != null) {
			editingContext = editingContextParent;
		}
		if (displayGroup() != null) {
			displayGroup().setSelectsFirstObjectAfterFetch(false);
		}
	}
	/** to be called when closing the interface : do nothing */
	public void terminate() {
	}
	/** &agrave; appeler &agrave; la fermeture de l'interface (ne fait rien) */
	public void terminer() {
		terminate();
	}
	// méthodes du controller DG
	/** Returns true if data can be modified (override, this method always returns false) */
	public boolean canModify() {
		return false;
	}
	/** Retourne true si les modifications sont autoris&eacute;es (retourne toujours false, m&eacute;thode &agrave; surcharger */
	public boolean modeSaisiePossible() {
		return canModify();
	}
  	// méthodes protégées
	/** update the display group and the controller display group */
	protected void updateDisplayGroups() {
		if (displayGroup() != null) {
			displayGroup().updateDisplayedObjects();
		}
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	/** Display a selection dialog with a display table containing one or two columns. Objects displayed in the dialog must implement
	 * the RecordWithValue or RecordWithValueAndCode interface. The user selection is returned through a notification
	 * @param entityName		entity whose objects must be displayed
	 * @param notificationMethod name of the method which should be invoked to get back the globalID of the selected object
	 * @param canSearch true if the fields for searching objects are displayed
	 * @param qualifier	qualifier to fetch objects
	 * @param simpleDialogue true if the table displays only one column
	 */
	protected void displayDialog(String entityName,String notificationMethod,boolean canSearch,EOQualifier qualifier,boolean simpleDialogue) {
		String notificationName = "Selection" + entityName;
		String dialogTitle = "Selection " + entityName;
		SimpleDialog controller = null;
		if (simpleDialogue) {
			controller = new SimpleDialog(entityName,notificationName,dialogTitle,canSearch,false,true);
			controller.init();
		} else {
			DoubleDialog controleurDouble = new DoubleDialog(entityName,notificationName,dialogTitle,canSearch,false,true);
			controleurDouble.init();
			controller = (SimpleDialog)controleurDouble;
		}
		if (qualifier != null) {
			controller.setQualifier(qualifier);
		}
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector(notificationMethod,new Class[] {NSNotification.class}),notificationName,null);
		controller.displayDialog();
	}
	/** update le display group et le controller display group */
	protected void updaterDisplayGroups() {
		updateDisplayGroups();
	}
	/** Affichage d'un dialogue de type SelectionSimple ou SelectionDouble
	 * @param nomEntite	nom entit&eacute; &agrave; afficher
	 * @param nomMethodeNotification nom de la m&eacute;thode de notification invoqu&eacute;e pour retourner la globalID de la valeur s&eacute;lectionn&acute;e
	 * @param aChampRecherche true si champ de recherche
	 * @param qualifier	qualifier de la liste d'objets &agrave; afficher
	 * @param dialogueSimple true si dialogue simple
	 */
	protected void afficherDialogue(String nomEntite,String nomMethodeNotification,boolean aChampRecherche,EOQualifier qualifier,boolean dialogueSimple) {
		String nomNotification = "Selection" + nomEntite;
		String titreFenetre = "Selection " + nomEntite;
		SimpleDialog controleur = null;
		if (dialogueSimple) {
			controleur = new SimpleDialog(nomEntite,nomNotification,titreFenetre,aChampRecherche,false,true);
			controleur.init();
		} else {
			DoubleDialog controleurDouble = new DoubleDialog(nomEntite,nomNotification,titreFenetre,aChampRecherche,false,true);
			controleurDouble.init();
			controleur = (SimpleDialog)controleurDouble;
		}
		if (qualifier != null) {
			controleur.setQualifier(qualifier);
		}
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector(nomMethodeNotification,new Class[] {NSNotification.class}),nomNotification,null);
		controleur.displayDialog();
	}
}
