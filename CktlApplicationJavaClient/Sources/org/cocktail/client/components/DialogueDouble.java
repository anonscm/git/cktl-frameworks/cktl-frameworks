/*
 * Created on 17 mai 2005
 *
 * Permet la sélection de valeurs avec deux champs affichés
 * Les valeurs sélectionnées sont retournées sous la forme d'une notification
 */
/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Cette classe est l'&eacute;quivalent de DoubleDialog.<BR>
 * @author christine<BR>
 * Affiche une table avec deux colonnes contenant des objets m&eacute;tiers :l'utilisateur peut choisir un objet ou plus.
 * Affiche deux boutons pour annuler ou valider la s&eacute;lection. L'utilisateur peut s&eacute;lectionner et valider par un double-clic sur la liste.<BR>
 * Police par d&eacute;faut : Helvetica plain 11
 * Les objets doivent impl&eacute;menter l'interface RecordAvecLibelle */
public class DialogueDouble extends DialogueSimple {
	private String codePourRecherche;

	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param peutRechercher true si l'utilisateur peut effectuer des recherches
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 * @param supprimerDoublons true si la liste affiche des valeurs uniques
	 */
	public DialogueDouble(String nomEntite,String notificationValidation,String titreDialogue,boolean peutRechercher,boolean supporteSelectionMultiple,boolean estModal,boolean supprimerDoublons) {
		super(nomEntite,notificationValidation,titreDialogue,peutRechercher,supporteSelectionMultiple,estModal,supprimerDoublons);

	}
	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param peutRechercher true si l'utilisateur peut effectuer des recherches
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 */
	public DialogueDouble(String nomEntite,String notificationValidation,String titreDialogue,boolean peutRechercher,boolean supporteSelectionMultiple,boolean estModal) {
		super(nomEntite,notificationValidation,titreDialogue,peutRechercher,supporteSelectionMultiple,estModal);

	}
	public void init() {
		EOArchive.loadArchiveNamed("DialogueDouble",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	/** Trie la table sur les codes */
	public void prepareInterface() {
		super.prepareInterface();
		trierObjetsSurCode();
	}
	/** Change the title of the second column */
	public void changeTitleSecondColumn(String libelle) {
		changeColumnHeader(libelle,1);
	}
	/** modifie le ibell&eacute; de la deuxi&egrave;me colonne */
	public void changerLibelleDeuxiemeColonne(String libelle) {
		changeColumnHeader(libelle,1);
	}
	/** Accesseurs */
	/** Retourne le code sur lequel on fait la recherche */
	public String codePourRecherche() {
		return codePourRecherche;
	}
	/** Modifie le code sur lequel on fait la recherche */
	public void setCodePourRecherche(String aStr) {
		this.codePourRecherche = aStr;
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = qualifierForSearching(textToSearch());
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		if (codePourRecherche != null && codePourRecherche.length() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code caseinsensitivelike %@", new NSArray("*" + codePourRecherche + "*")));
		}
		qualifier = new EOAndQualifier(qualifiers);
		displayGroup().setQualifier(qualifier);
		updateDisplayGroups();
	}
	/** trie les objets sur le code */
	public void trierObjetsSurCode() {
		displayGroup.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("code",EOSortOrdering.CompareAscending)));
	}
	/** Supprime du display group les objets qui ont le m&circ;me code */
	public void supprimerDoublonsDuDisplayGroup() {
		displayGroup.setObjectArray(supprimerDoublons(displayGroup.allObjects()));
	}
	// Méthodes protégées
	/** Supprime les objets avec le m&ecirc;me code */
	protected NSArray supprimerDoublons(NSArray anArray) {
		NSMutableArray liste = new NSMutableArray();
		NSMutableArray resultat = new NSMutableArray();
		String	chaine = "";
		for (int i = 0;i < anArray.count();i++) {
			chaine =(String)((EOGenericRecord)anArray.objectAtIndex(i)).valueForKey("code");
			if (!liste.containsObject(chaine)) {
				liste.addObject(chaine);
				resultat.addObject(anArray.objectAtIndex(i));
			}
		}	
		return new NSArray(resultat);
	}
}
