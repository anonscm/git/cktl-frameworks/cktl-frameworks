/*
 * Created on 17 mai 2005
 *
 * Permet la sélection de valeurs avec deux champs affichés
 * Les valeurs sélectionnées sont retournées sous la forme d'une notification
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** This class is the equivalent of DialogueDouble
 * @author christine<BR>
 * Display a table with two columns containing enterprise objects: the user can select one or more objects.
 * Display  two buttons to cancel or validate the selection. The user can select and validate by double-clicking on the list<BR>
 * The default font Helvetica plain 11
 * Objects must implement the RecordWithTitleAndCode interface or the RecordAvecLibelleEtCode interface */
public class DoubleDialog extends SimpleDialog {
	private String codeToSearch;

	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if thereis a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 * @param displayDistinctValues true if the displayed values are unique
	 */
	public DoubleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal,boolean displayDistinctValues) {
		super(entityName,validationNotification,dialogTitle,canSearch,multipleSelectionAllowed,isModal,displayDistinctValues);

	}
	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if thereis a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 */
	public DoubleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal) {
		super(entityName,validationNotification,dialogTitle,canSearch,multipleSelectionAllowed,isModal);

	}
	public void init() {
		EOArchive.loadArchiveNamed("DoubleDialog",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	/** Sort the table on the code columns */
	public void prepareInterface() {
		super.prepareInterface();
		sortObjectsOnCode();
	}
	/** Change the title of the second column */
	public void changeTitleSecondColumn(String libelle) {
		changeColumnHeader(libelle,1);
	}
	/** Sort objects on the code */
	public void sortObjectsOnCode() {
		displayGroup.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("code",EOSortOrdering.CompareAscending)));
	}
	// Accessors
	/** Return the code used to search for records */
	public String codeToSearch() {
		return codeToSearch;
	}
	/** Modifies the code used to search for records */
	public void setCodeToSearch(String aStr) {
		this.codeToSearch = aStr;
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = qualifierForSearching(textToSearch());
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		if (codeToSearch != null && codeToSearch.length() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code caseinsensitivelike %@", new NSArray("*" + codeToSearch + "*")));
		}
		qualifier = new EOAndQualifier(qualifiers);
		displayGroup().setQualifier(qualifier);
		updateDisplayGroups();
	}
	/** Remove objects with the same code from the display group */
	public void removeFromDisplayGroupIdenticalObjects() {
		displayGroup.setObjectArray(removeIdenticalObjects(displayGroup.allObjects()));
	}
	// Méthodes protégées
	/** Remove objects with the same code and returns the resulting objects */
	protected NSArray removeIdenticalObjects(NSArray anArray) {
		NSMutableArray liste = new NSMutableArray();
		NSMutableArray resultat = new NSMutableArray();
		String	chaine = "";
		for (int i = 0;i < anArray.count();i++) {
			chaine =(String)((EOGenericRecord)anArray.objectAtIndex(i)).valueForKey("code");
			if (!liste.containsObject(chaine)) {
				liste.addObject(chaine);
				resultat.addObject(anArray.objectAtIndex(i));
			}
		}	
		return resultat;
	}
}
