/*
 * Created on 12 avr. 2006
 *
 * Inspecteur avec display group et editing context<BR>
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import javax.swing.JDialog;

import org.cocktail.client.components.utilities.GraphicUtilities;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Same class as InspectorWithDisplayGroup<BR>
 * Inspector with a display group and editing context<BR>
 * Uses a notification to change the displayed object
 * @author christine
 * 
 */
public abstract class InspectorWithDisplayGroup extends EODialogController {
	public EODisplayGroup displayGroup;
	/** Outlet */
	public EOTable displayList;	// pour afficher les données du display group
	public EOEditingContext editingContext;
	private EOGenericRecord currentDisplayedObject;
	private String notificationForSynchronisationName;

	/** Constructor
	 * @param editingContext
	 * @param notificationForSynchronisationName	name of the notification to be sent to the inspector to refresh the data
	 */
	public InspectorWithDisplayGroup(EOEditingContext editingContext,String notificationForSynchronisationName) {
		this.editingContext = editingContext;
		this.notificationForSynchronisationName = notificationForSynchronisationName;
	}
	/** Constructor
	 * @param editingContext
	 */
	public InspectorWithDisplayGroup(EOEditingContext editingContext) {
		this(editingContext,null);
	}
	public void connectionWasEstablished() {
		if (notificationForSynchronisationName != null) {
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getCurrentObject", new Class[] {NSNotification.class}),notificationForSynchronisationName,null);
		}
	}
	public void connectionWasBroken() {
		if (notificationForSynchronisationName != null) {
			NSNotificationCenter.defaultCenter().removeObserver(this);
		}
	} 
	public void display() {
		if (displayList != null) {
			GraphicUtilities.rendreNonEditable(displayList);
		}
		((JDialog)window()).setModal(false);
		setWindowResizable(window(),false);
		activateWindow();
		updaterDisplayGroup();
	}
	public void terminate() {
		closeWindow();
	}
	// Notifications
	/** Changes the currently displayed object. The notification object contains the object to be displayed */
	public void getCurrentObject(NSNotification aNotif) {
		currentDisplayedObject = (EOGenericRecord)aNotif.object();
		updaterDisplayGroup();
	}
	/** Subclass must implement it, must load the archive */
	public abstract void init();
	// méthodes protégées
	/** Return the object which is currently displayed */
	protected EOGenericRecord currentDisplayedObject() {
		return currentDisplayedObject;
	}
	/** Returns the result of the fetch */
	protected abstract NSArray fetchObjects();

	// méthodes privées
	private void updaterDisplayGroup() {
		if (displayGroup != null) {
			displayGroup.setObjectArray(fetchObjects());
			displayGroup.updateDisplayedObjects();
		}
	}
}
