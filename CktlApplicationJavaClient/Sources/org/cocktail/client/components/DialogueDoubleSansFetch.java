/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Cursor;

import org.cocktail.common.utilities.EOFUtilities;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**	M&ecirc;me classe que DoubleDialogNoFetch<BR>
 * Affiche une liste d'objets comportant deux colonnes (code, libelle) avec un champ de recherche. Les donn&eacute;es ne sont pas affich&eacute;es directement
 * mais fetch&eacute;es dans la base lorsque l'utilisateur clique sur le bouton "Recherche". Les objets doivent impl&eacute;menter
 * l'interface RecordAvecLibelleEtCode.<BR>
 * @author christine
 *
 */
public class DialogueDoubleSansFetch extends DialogueSimpleSansFetch {
	private String codePourRecherche;
	private String codeAttributPourRecherche;
	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param validationNotification notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param codeAttributPourRecherche code attribut sur lequel est effectu&eacute; la recherche (1ere colonne)
	 * @param attributPourRecherche attribut sur lequel est effectu&eacute; la recherche
	 * @param titreDialogue titre du dialogue
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModale true si c'est un dialogue modal
	 * @param supprimerDoublons true si la liste affiche des valeurs uniques
	 * @param sansCaractereAccentue true si la recherche est faite sans caract&egrave;re accentu&eacite;
	 * @param peutToutRechercher true si la saisie d'un libell&eacute;n'est pas obligatoire;
	 */
	public DialogueDoubleSansFetch(String nomEntite,String validationNotification, String codeAttributPourRecherche,String attributPourRecherche,String titreDialogue,boolean supporteSelectionMultiple,boolean estModale,boolean supprimerDoublons,boolean sansCaractereAccentue, boolean peutToutRechercher) {
		// le champ de recherche est obligatoire
		super(nomEntite,validationNotification,attributPourRecherche,titreDialogue,supporteSelectionMultiple,estModale,supprimerDoublons,sansCaractereAccentue,peutToutRechercher);
		this.codeAttributPourRecherche = codeAttributPourRecherche;
	}
	public void init() {
		EOArchive.loadArchiveNamed("DialogueDoubleSansFetch",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	// accesseurs
	/** Retourne le texte du champ de recherche sur le code */
	public String codePourRecherche() {
		return codePourRecherche;
	}
	/** Modifie le texte du champ de recherche sur le code */
	public void setCodePourRecherche(String aText) {
		this.codePourRecherche = aText;
	}
	// Méthode du controller DG
	public boolean peutRechercher() {
		return codePourRecherche() != null || super.peutRechercher();
	}
	// actions
	/** Recherche les objets selon les crit&egrave;res saisis */
	public void rechercher() {
		// 29/04/2010 - ajouter pour le cas où l'utilisateur valide en tapant un retour chariot
		if (!peutRechercher()) {
			return;
		}
		if (codePourRecherche() == null) {
			super.rechercher();
		} else {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			NSMutableArray args = new NSMutableArray("*" + codePourRecherche() + "*");
			String stringQualifier = codeAttributPourRecherche + " caseinsensitivelike %@";
			if (textToSearch() != null) {
				args.addObject("*" + textToSearch() + "*");
				stringQualifier = stringQualifier + " AND " + attributPourRecherche() +  " caseinsensitivelike %@";
			}
			EOQualifier qualifierForRecherche = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			if (qualifier() != null) {	// qualifier setter chez le parent
				NSMutableArray qualifs = new NSMutableArray(qualifier());
				qualifs.addObject(qualifierForRecherche);
				qualifierForRecherche = new EOAndQualifier(qualifs);
			}
			NSArray results = EOFUtilities.rechercherAvecQualifier(editingContext(),entityName(),qualifierForRecherche,false);
			displayGroup().setObjectArray(results);
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	// Méthodes protégées
	protected String codeAttributPourRecherche() {
		return codeAttributPourRecherche;
	}
}
