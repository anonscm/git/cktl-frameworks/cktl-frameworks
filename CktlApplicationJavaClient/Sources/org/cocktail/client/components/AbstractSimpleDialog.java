/* SelectionSimpleController
 * Dialogue de sélection avec deux boutons valider et annuler
 * Gère la sélection multiple et le double-clic  */

/*
 * Copyright Consortium Cocktail
 *
 * 
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.component.COLabel;
import org.cocktail.component.COView;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/** Display a table containing enterprise objects: the user can select one or more objects.
 * Display  two buttons to cancel or validate the selection. The user can select and validate by double-clicking on the list<BR>
 * The default font Helvetica plain 11
 */
public abstract class AbstractSimpleDialog extends DialogueWithDisplayGroup {
	/** Outlet : table to display the objects of the display group */
	public EOTable displayTable;
	/** Outlet : view that contains the search fields */
	public COView searchView;
	/** Outlet : label that contains the multiple selection message */
	public COLabel cOLabel1;
	private boolean canSearch;
	private boolean multipleSelectionAllowed;
	private boolean displayDistinctValues;
	private String messageForMultipleSelection;
	private String textToSearch;
	/** Notification sent when the selection is cancelled */
	public static String CANCEL_NOTIFICATION = "SimpleDialogCancelSelection";

	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if thereis a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 * @param displayDistinctValues true if the displayed values are unique
	 */
	public AbstractSimpleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal,boolean displayDistinctValues) {
		super(entityName,validationNotification,CANCEL_NOTIFICATION,dialogTitle,isModal);
		this.canSearch = canSearch;
		this.multipleSelectionAllowed = multipleSelectionAllowed;
		this.displayDistinctValues = displayDistinctValues;
	}
	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param canSearch true if thereis a text field for searching objects
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param isModal true if it's a modal dialog
	 */
	public AbstractSimpleDialog(String entityName,String validationNotification,String dialogTitle,boolean canSearch,boolean multipleSelectionAllowed,boolean isModal) {
		this(entityName,validationNotification,dialogTitle,canSearch,multipleSelectionAllowed,isModal,false);
	}
	/** Loads the interface file */
	public abstract void init();
	public void connectionWasEstablished() {
		super.connectionWasEstablished();

		displayGroup().setObjectArray(fetchObjects());
		if (displayGroup().displayedObjects().count() > 0) {
			displayGroup().setSelectedObject(displayGroup.displayedObjects().objectAtIndex(0));
		}
	}
	public void prepareInterface() {
		// pour masquer le champ de recherche
		if (searchView != null)  {
			searchView.setVisible(canSearch);
		}
		prepareSelection();
		GraphicUtilities.makeTableReadOnly(displayTable);
		displayTable.table().addMouseListener(new DoubleClickListener());
		sortTable();
	}
	/** modify the header of the first column */
	public void changeHeaderFirstColumn(String header) {
		changeColumnHeader(header,0);
	}
	// Methodes du controller DG
	/** Return true if an object is selected in the list */
	public boolean canValidate() {
		return displayGroup().selectedObject() != null;
	}
	// Accessors
	public String textToSearch() {
		return textToSearch;
	}
	public void setTextToSearch(String aStr) {
		this.textToSearch = aStr;
		displayGroup().setQualifier(qualifierForSearching(aStr));
		updateDisplayGroups();
	}
	/** Returns the message to display for multiple selection */
	public String messageForMultipleSelection() {
		if (isMultipleSelectionAllowed()) {
			return messageForMultipleSelection;
		} else {
			return "";
		}
	}
	/** Set the message to display for multiple selection */
	public void setMessageForMultipleSelection(String aString) {
		messageForMultipleSelection = aString;
	}

	// Méthodes protégées
	protected JTable displayTable() {
		return displayTable.table();
	}
	/** Returns true if distinct values must be displayed in the table */
	protected boolean displayDistinctValues() {
		return displayDistinctValues;
	}
	/** Returns true if multiple selection is allowed */
	protected boolean isMultipleSelectionAllowed() {
		return multipleSelectionAllowed;
	}
	/** Returns an array of global ids if multiple selection is allowed or a global id if single selection allowed */
	protected Object selectedObject() {
		if (multipleSelectionAllowed) {
			NSMutableArray globalIDs = new NSMutableArray();
			java.util.Enumeration e = displayGroup.selectedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				globalIDs.addObject(editingContext().globalIDForObject((EOEnterpriseObject)e.nextElement()));
			}
			return new NSArray(globalIDs);
		} else {
			return editingContext().globalIDForObject((EOGenericRecord)displayGroup().selectedObject());
		} 
	}
	/** Sort objects in the table */
	protected abstract void sortTable();
	/** Returns the qualifier to search for text */
	protected abstract EOQualifier qualifierForSearching(String textToSearch);
	/** Return the array of fetch objects */
	protected abstract NSArray fetchObjects();
	protected void changeColumnHeader(String libelle,int colonne) {
		displayTable().getColumnModel().getColumn(colonne).setHeaderValue(libelle);
	}

	// méthodes privées
	private void prepareSelection() {
		int typeSel;
		if (multipleSelectionAllowed) {
			typeSel = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;

		} else {
			typeSel = ListSelectionModel.SINGLE_SELECTION;
		}
		displayTable.table().getSelectionModel().setSelectionMode(typeSel);
	}


	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		private boolean validationOn;	// Pour éviter les rafales de double-clic en cas de traitement trop long dans la notification envoyée
		public void mouseClicked(MouseEvent e) {
			if(e.getClickCount() == 2 && !validationOn) {
				validationOn = true;
				validate();
				validationOn  = false;
			}
		}
	}



}
