/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
/**
 * @author christine<BR>
 *
 * Abstract class for a modal dialog with an editing context, a display group and a controller display group<BR>

 */
public abstract class ModalDialogWithDisplayGroup extends EOModalDialogController {
	private EODisplayGroup displayGroup;
	private EOEditingContext editingContext;
	private EODisplayGroup controllerDisplayGroup;	// 19/01/2011 - to be used with Netbeans

	/** 
	 * Constructor
	 */
	public ModalDialogWithDisplayGroup() {
		super();
		changeDisplayGroup(new EODisplayGroup());
	}
	/** to imitate EOEntity controller */
	public EODisplayGroup displayGroup() {
		return displayGroup;
	}
	public void setDisplayGroup(EODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}
	/** to imitate EOEntity controller */
	public EOEditingContext editingContext() {
		return editingContext;
	}	
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	/** to imitate EOEntity controller */
	public EODisplayGroup controllerDisplayGroup() {
		return controllerDisplayGroup;
	}
	public void setControllerDisplayGroup(EODisplayGroup controllerDisplayGroup) {
		if (controllerDisplayGroup != this.controllerDisplayGroup) {
			System.out.println("changing DF0");
			changeDisplayGroup(controllerDisplayGroup);
		}
	}
	// Private methods
	private void changeDisplayGroup(EODisplayGroup aGroup) {
		controllerDisplayGroup = aGroup;
		controllerDisplayGroup.setObjectArray(new NSArray(this));
		controllerDisplayGroup.selectObject(this);

	}
	
}
