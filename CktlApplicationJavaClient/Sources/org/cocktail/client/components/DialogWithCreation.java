/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import com.webobjects.eoapplication.EOArchive;

/**  Manage a dialog where user can input text. Send a notification when the user validates<BR>
 * The default font Helvetica plain 11<BR>
 * Affiche un dialogue de saisie d'information avec un message affich&eacute;.Envoie une notification quand l'utilisateur valide
 * @author christine
 *
 */
public class DialogWithCreation extends DialogueWithDisplayGroup {
	/** Notification sent when the user validates<BR>
	 * Notification envoy&eacue;e lorsque l'utilisateur valide la saisie
	 */
	public final static String VALIDATION_NOTIFICATION = "NotificationValidateCreation";
	/** Notification sent when the user cancels<BR>
	 * Notification envoy&eacue;e lorsque l'utilisateur annule la saisie
	 */
	public final static String CANCEL_NOTIFICATION_ = "NotificationCancelCreation";
	private String text;
	private String message;
	
	/** Constructor
	 * @param dialogTitle title of the dialog
	 * @param message message displayed to the user to indicate the nature of the input
	 * @param isModal true if the dialog is a modal dialog
	 */
	public DialogWithCreation(String dialogTitle,String message,boolean isModal) {
		super(null,VALIDATION_NOTIFICATION,CANCEL_NOTIFICATION_,dialogTitle,isModal);
		this.message = message;
	}
	/** Constructor for a modal dialog
	 * @param dialogTitle title of the dialog
	 * @param message message displayed to the user to indicate the nature of the input
	 */
	public DialogWithCreation(String dialogTitle,String message) {
		this(dialogTitle,message,true);
	}
	/** Loads the interface file */
	public void init() {
		EOArchive.loadArchiveNamed("DialogWithCreation",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	// Accessors
	/** Returns the user input<BR>
	 * Retourne le texte saisi par l'utilisateur */
	public String text() {
		return text;
	}
	/** Set the user input.<BR>
	 * Modifie le texte saisi par l'utilisateur */
	public void setText(String aText) {
		this.text = aText;
	}
	/** Returns the message displayed to the user.<BR>
	 * Retourne le message affich&eacute; &agrave; l'utilisateur */
	public String message() {
		return message;
	}
	/** Set the message displayed to the user.<BR>
	 * Modifie le message affich&eacute; &agrave; l'utilisateur */
	public void setMessage(String aMessage) {
		this.message = aMessage;
	}

	// Méthodes du controller DG
	/** The user can validate when some text is input.<BR>
	 * L'utilisateur peut valider quand du texte est saisi */
	public boolean canValidate() {
		return text != null && text.length() > 0;
	}
	// Méthodes protégées
	protected void prepareInterface() {
	}
	/** Returns the user input (String). <BR>
	 * Retourne le texte saisi par l'utilisateur (String) */
	protected Object selectedObject() {
		return text();
	}
	
}
