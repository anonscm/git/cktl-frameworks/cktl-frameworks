/*
 * Created on 14 mars 2006
 *
 * Gère une liste de sélection dont les éléments sont des simples strings
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.RecordWithTitle;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/** Same class as DialogueSimpleAvecStrings<BR>
 * Displays a dialog with a list of strings for selection.<BR>
 * @author christine
 *
 */
public class SimpleDialogWithStrings extends SimpleDialog {
	private NSMutableArray stringsToDisplay;
	/** Constructor
	 * @param validationNotification notification sent when the user validates the selection
	 * @param dialogTitle title of the dialog
	 * @param multipleSelectionAllowed true if the user can select multiple records
	 * @param strings array of strings to display
	 */
	public SimpleDialogWithStrings(String validationNotification,String dialogTitle,boolean multipleSelectionAllowed,boolean isModal,NSArray strings) {
		super(null,validationNotification,dialogTitle,false,multipleSelectionAllowed,isModal,false);
		prepareStrings(strings);
	}
	// méthodes protégées
	protected NSArray fetchObjects() {
		return stringsToDisplay;
	}
	/** Returns an array of strings if multiple selection is allowed or a unique string if simple selection allowed */
	protected Object selectedObject() {
		if (isMultipleSelectionAllowed()) {
			NSMutableArray strings = new NSMutableArray();
			java.util.Enumeration e = displayGroup.selectedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				strings.addObject(((StringWithTitle)e.nextElement()).title());
			}
			return strings; 
		} else {
			return ((StringWithTitle)displayGroup().selectedObject()).title();	
		} 
	}
	// méthodes privées
	private void prepareStrings(NSArray strings) {
		if (strings != null) {
			stringsToDisplay = new NSMutableArray();
			java.util.Enumeration e = strings.objectEnumerator();
			while (e.hasMoreElements()) {
				Object string = e.nextElement();
				stringsToDisplay.addObject(new StringWithTitle((String)string));
			}
		}
	}
	/** Maps a string with an object implementing the RecordWithTitle and NSKeyValueCoding Interface */
	public class StringWithTitle implements RecordWithTitle, NSKeyValueCoding {
		private String title;

		public StringWithTitle(String text) {
			title = text;
		}
		public String title() {
			return title;
		}
		public String toString() {
			return title;
		}
		// interface keyValueCoding
		public Object valueForKey(String cle) {
			if (cle.equals("title")) {
				return title;
			} else {
				return null;
			}
		}
		public void takeValueForKey(Object valeur,String cle) {
		}

	}
}
