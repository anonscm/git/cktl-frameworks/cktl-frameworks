/*
 * Created on 31 mai 2005
 *
 * Dialogue qui gère un editing context et un display group
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Cursor;

import javax.swing.JDialog;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

/**
 * @author christine<BR>
 *
 * Abstract class for a dialog with an editing context, a display group and a controller display group<BR>
 * Manage two actions : cancel to cancel the dialog and validate to validate the selection <BR>
 * A notification is sent when the user selects values or when he cancels the dialog
 */
public abstract class DialogueWithDisplayGroup extends EODialogController {
	/** Outlets */
	public EODisplayGroup displayGroup;
	public EODisplayGroup controllerDisplayGroup;
	public EOEditingContext editingContext;
	private String entityName;
	private String validationNotification, cancelNotification;
	private String dialogTitle;
	private boolean isModal;
	private EOQualifier qualifier;

	/** Constructor
	 * @param entityName entity to be displayed
	 * @param validationNotification notification sent when the user validates the selection. The selected object will be sent. Pass null when you want to manage the notification
	 * @param dialogTitle title of the dialog
	 * @param isModal true if it's a modal dialog
	 */
	public DialogueWithDisplayGroup(String entityName,String validationNotification,String cancelNotification,String dialogTitle,boolean isModal) {
		this.entityName = entityName;
		this.validationNotification = validationNotification;
		this.cancelNotification = cancelNotification;
		this.isModal = isModal;
		this.dialogTitle = dialogTitle;
		editingContext = new EOEditingContext();
	}
	/** Overrides the parent */
	public void connectionWasEstablished() {
		prepareInterface();
		if (controllerDisplayGroup != null) {
			controllerDisplayGroup.setObjectArray(new NSArray(this));
			controllerDisplayGroup.setSelectedObject(this);
		}
		if (dialogTitle != null) {
			((JDialog)window()).setTitle(dialogTitle);
		}
	}
	// accesseurs
	/** Returns the controller display group : it's a display group which contains one object (the dialog controller), this object is
	 * selected in the display group */
	public EODisplayGroup controllerDisplayGroup() {
		return controllerDisplayGroup;
	}
	/**Returns the qualifier used to fetch objects  */
	public EOQualifier qualifier() {
		return qualifier;
	}
	/** Set the qualifier to fetch objects
	 * @param qualifier to limit the displayed objects
	 */
	public void setQualifier(EOQualifier qualifier) {
		this.qualifier = qualifier;
	}
	public void setDialogTitle(String aString) {
		dialogTitle = aString;
	}
	public void displayDialog() {
		setWindowResizable(window(),false);
		((JDialog)window()).setModal(isModal);		
		activateWindow();
	}
	/** Affiche le dialogue. M&ecirc;me m&eacute;thode que displayDialog(). Mmaintenue pour des raisons de compatibilit&eacute; */
	public void afficherFenetre() {
		displayDialog();
//		((JDialog)window()).setLocation(10000, 10000);
	}
	// actions
	/** Cancel button action : post a notification to notify the selection has been cancelled and close the dialog */
	public void cancel() {
		if (cancelNotification() != null) {
			NSNotificationCenter.defaultCenter().postNotification(cancelNotification(), null);
		}
		terminate();
	}
	/** Validate button action : post a notification with an object and close the dialog */
	public void validate() {
		window().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (selectedObject() != null && validationNotification() != null) {
			NSNotificationCenter.defaultCenter().postNotification(validationNotification(), selectedObject());
		}
		window().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		terminate();
	}
	public EODisplayGroup displayGroup() {
		return displayGroup;
	}
	// méthodes protégées
	/** Prepare the interface, once the archive has been loaded */
	protected abstract void prepareInterface();
	/** Return the selected object to be sent with the validation notification */
	protected abstract Object selectedObject();
	protected String entityName() {
		return entityName;
	}
	protected String validationNotification() {
		return validationNotification;
	}
	protected String cancelNotification() {
		return cancelNotification;
	}
	protected EOEditingContext editingContext() {
		return editingContext;
	}
	protected void setEditingContext(EOEditingContext unEditingContext) {
		editingContext = unEditingContext;
	}
	protected void setDisplayGroup(EODisplayGroup aGroup) {
		displayGroup = aGroup;
	}
	protected void terminate() {
		qualifier = null;
		closeWindow();
	}
	
	/** update the display group and the controller display group */
	protected void updateDisplayGroups() {
		if (displayGroup() != null) {
			displayGroup().updateDisplayedObjects();
		}
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}

}
