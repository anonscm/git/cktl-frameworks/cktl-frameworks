/*
 * Created on 31 mars 2006
 *
 * Affiche un dialogue de sélection avec un champ de recherche. Les données ne sont pas afficher directement mais fetcher dans la base
 * après recherche
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Cursor;

import org.cocktail.common.utilities.EOFUtilities;
import org.cocktail.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**	M&ecirc;me classe que SimpleDialogNoFetch<BR>
 * Affiche une liste d'objets avec un champ de recherche. Les donn&eacute;es ne sont pas affich&eacute;es directement
 * mais fetch&eacute;es dans la base lorsque l'utilisateur clique sur le bouton "Recherche". Les objets doivent impl&eacute;menter
 * l'interface RecordAvecLibelle.<BR>
 * @author christine
 *
 */
public class DialogueSimpleSansFetch extends DialogueSimple {
	private String attributPourRecherche;
	private boolean sansCaractereAccentue;
	private boolean peutToutRechercher;

	/** Constructeur
	 * @param nomEntite entit&eacute; &agrave; afficher
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param attributPourRecherche attribut sur lequel est effectu&eacute; la recherche
	 * @param titreDialogue titre du dialogue
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 * @param supprimerDoublons true si la liste affiche des valeurs uniques
	 * @param sansCaractereAccentue true si la recherche est faite sans caract&egrave;re accentu&eacite;
	 * @param peutToutRechercher true si la saisie d'un libell&eacute;n'est pas obligatoire;
	 */
	public DialogueSimpleSansFetch(String nomEntite,String notificationValidation,String attributPourRecherche,String titreDialogue,boolean supporteSelectionMultiple,boolean estModal,boolean supprimerDoublons,boolean sansCaractereAccentue,boolean peutToutRechercher) {
		// le champ de recherche est obligatoire
		super(nomEntite,notificationValidation,titreDialogue,true,supporteSelectionMultiple,estModal,supprimerDoublons);
		this.attributPourRecherche = attributPourRecherche;
		this.sansCaractereAccentue = sansCaractereAccentue;
		this.peutToutRechercher = peutToutRechercher;
	}
	public void init() {
		EOArchive.loadArchiveNamed("DialogueSimpleSansFetch",this,"org.cocktail.client.components.interfaces",this.disposableRegistry());
	}
	
	/** M&eacute;thode de d&eacute;l&eacute;gation du display group  */
	public void displayGroupDidChangeSelection(EODisplayGroup eod)	{
		if (eod == displayGroup() && controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	// Actions
	/** Recherche les objets selon le crit&egrave;re saisi */
	public void rechercher() {
		// 29/04/2010 - ajouter pour le cas où l'utilisateur valide en tapant un retour chariot
		if (!peutRechercher()) {
			return;
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		String text = "*";
		if (textToSearch() != null) {
			if (sansCaractereAccentue) {
				text = "*" + StringCtrl.chaineClaire(textToSearch(), true) + text + "*"; 
			} else {
				text = "*" + textToSearch() + text + "*";
			}
		}
		NSArray args = new NSArray(text);
		String stringQualifier = attributPourRecherche() + " caseinsensitivelike %@";
		EOQualifier qualifierForSearching = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		if (qualifier() != null) {	// qualifier setter chez le parent
			NSMutableArray qualifs = new NSMutableArray(qualifier());
			qualifs.addObject(qualifierForSearching);
			qualifierForSearching = new EOAndQualifier(qualifs);
		}
		NSArray results = EOFUtilities.fetchWithQualifier(editingContext(),entityName(),qualifierForSearching,displayDistinctValues());
		displayGroup().setObjectArray(results);
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	// Méthodes du controller DG
	public boolean peutRechercher() {
		return peutToutRechercher || textToSearch() != null;
	}
	// méthodes protégées
	protected NSArray fetchObjects() {
		// le fetch sera fait plus tard
		return new NSArray();
	}
	protected String attributPourRecherche() {
		return attributPourRecherche;
	}
}
