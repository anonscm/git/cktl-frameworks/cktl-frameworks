/*
 * Created on 11 avr. 2006
 *
 * Choix de données dans une vue arborescente
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COTreeView;
import org.cocktail.component.utilities.CRITreeView;

import sun.reflect.Reflection;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Choix de donn&eacute;es dans une vue arborescente<BR>
 * Les m&eacute;thodes abstraites permettent de d&eacute;finir les donn&eacute;es attach&eacute;es &agrave; cet arbre<BR>
 * @author christine
 *

 */
// 24/09/2010 - modification pour qu'en cas de fermeture de la fenêtre sans validation, il n'y ait pas d'objet sélectionné (DT 2839)
// 01/12/2010 - ajout de méthodes pour gérer les cas des arbres sans relations explicites
public abstract class DialogueModalAvecArbre extends EOModalDialogController {
	// Objets Graphiques
	public COTreeView treeView;
	public JButton boutonValider;
	public EODisplayGroup displayGroup;
	public EODisplayGroup controllerDisplayGroup;
	public EOEditingContext editingContext;
	private EOGenericRecord objetSelectionne;
	private String nomClasseAppelant;
	private boolean estLocke;
	private Dimension treeViewSize;	// 24/09/2010 - le tree view change de taille (DT 2839)
	/** 
	 * Constructeur
	 */
	public DialogueModalAvecArbre() {
		super();
		loadArchive();
		init();
		treeViewSize = treeView.getSize();

	}
	public void connectionWasEstablished() {
		if (controllerDisplayGroup != null) {
			controllerDisplayGroup.setObjectArray(new NSArray(this));
			controllerDisplayGroup.setSelectedObject(this);
		}
		setWindowResizable(window(),false);
		((JDialog)window()).setModal(true);
		((JDialog)window()).setTitle(titreFenetre());
		boutonValider.setEnabled(false);

		loadNotifications();
	}
	public void connectionWasBroken() {
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}	
	/** Retourne le controller display group : c'est un display group qui contient un seul objet (ChoixAvecArbre) qui est s&eacute;lectionn&eacute;
	 * dans le display group */
	public EODisplayGroup controllerDisplayGroup() {
		return controllerDisplayGroup;
	}
	/**
	 * Selection d'un objet en pr&eacute;cisant le mode de chargement des donn&eacute;es
	 * @param toutCharger si on charge toutes les donn&eacute;es au d&eacute;marrage
	 * @param gereFils si les objets m&eacute;tier impl&eacute;mentent l'interface InterfaceArbreAvecFils
	 *
	 */
	public EOGenericRecord selectionnerObjet(EOEditingContext editingContext,boolean toutCharger,boolean gereFils) {
		treeView.setSize(treeViewSize);
		nomClasseAppelant = Reflection.getCallerClass(3).getName();	// Pour gérer les notifications de lock, getCallerClass(3) vu dans la classe System
		LogManager.logDetail("Selection dans un arbre avec construction dynamique " + toutCharger);
		if (gereFils && toutCharger) {
			// Un arbre qui gère ses propres fils est nécessairement dynamique
			toutCharger = false;
		}
		initPourSelection();
		objetSelectionne = null;
		try {
			treeView.initialize(editingContext,nomEntiteAffichee(),true,toutCharger,gereFils);
			LogManager.logDetail(this.getClass().getName() + " : selection d'un objet");
			activateWindow();	
			nomClasseAppelant = null;
			return objetSelectionne;
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logErreur(this.getClass().getName()+".initializeTreeView() - Erreur : l'initialisation du treeView a echoue");
			return null;
		}
	}
	/**
	 * Selection d'un objet
	 *
	 */
	public EOGenericRecord selectionnerObjet(EOEditingContext editingContext) {
		return selectionnerObjet(editingContext,true,false);
	}
	// Notifications
	// LOCK ECRAN
	public void lockSaisie(NSNotification aNotif) {
		if (aNotif.object() == null || (nomClasseAppelant != null && nomClasseAppelant.equals(aNotif.object()) == false)) {
			estLocke = true;
			if (treeView != null) {
				treeView.setEnabled(!estLocke);
			}
		}
	} 
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		if (aNotif.object() == null || (nomClasseAppelant != null && nomClasseAppelant.equals(aNotif.object()) == false)) {
			estLocke = false;
			if (treeView != null) {
				treeView.setEnabled(!estLocke);
			}
		}
	} 
	/** Notification revue lors d'un simple click sur le tree view : une structure a ete selectionnee */
	public void notificationSimpleClic(NSNotification aNotif){
		// 22/01/09 pour le problème de cast dans la gestion des rôles
		if (aNotif.object() == treeView) {
			NSDictionary result = (NSDictionary)aNotif.userInfo();
			objetSelectionne = (EOGenericRecord)result.objectForKey("selectedRecord");
			// Si l'utilisateur peut choisir cet objet, alors on permet la validation
			boutonValider.setEnabled(peutSelectionnerObjet(objetSelectionne));
		}
	}
	/** Notification revue lors d'un double click sur le tree view : une structure a ete selectionnee */
	public void notificationDoubleClic(NSNotification aNotif) {
		// 22/01/09 pour le problème de cast dans la gestion des rôles
		if (aNotif.object() == treeView) {
			NSDictionary result = aNotif.userInfo();
			objetSelectionne = (EOGenericRecord)result.objectForKey("selectedRecord");
			if (peutSelectionnerObjet(objetSelectionne)) {
				LogManager.logDetail(this.getClass().getName() + " - objet selectionne : " + objetSelectionne);
				deactivateWindow();
			}
		}
	}
	// actions
	public void valider() {
		LogManager.logDetail(this.getClass().getName() + " - objet selectionne : " + objetSelectionne);
		closeWindow();
	}
	public void annuler() {
		LogManager.logDetail(this.getClass().getName() + " - Annulation");
		objetSelectionne = null;
		closeWindow();	
	}
	// méthodes protégées
	protected void loadArchive() {
		EOArchive.loadArchiveNamed("DialogueModalAvecArbre", this,"org.cocktail.client.components.interfaces", this.disposableRegistry());
	}
	protected abstract String titreFenetre();
	protected abstract String titreArbre();
	protected abstract String nomEntiteAffichee();
	protected abstract EOQualifier qualifierForColumn();
	protected abstract EOQualifier restrictionQualifier();
	protected abstract String parentRelationship();
	protected abstract String attributeOfParent(); // 01/12/2010
	protected abstract String parentAttributeOfChild();	// 01/12/2010
	protected abstract String attributeForDisplay();
	/** Cette m&eacute;thode return true si l'utilisateur peut choisir cet objet suite &agrave; un double-clic */
	protected abstract boolean peutSelectionnerObjet(EOGenericRecord objet);

	protected void updateTreeView() {
		treeView.setRestrictionQualifier(restrictionQualifier());
		treeView.update((javax.swing.tree.TreePath)null);
	}
	// méthodes privées
	private void init() {
		treeView.setDynamic(false);
		treeView.setTitle(titreArbre());
		// 01/12/2010 - on donne priorité à la relation
		if (parentRelationship() != null) {
			treeView.setParentRelationship(parentRelationship());
		} else {
			// Si ces valeurs sont nulles, le treeView affichera une erreur plus tard
			treeView.setAttributeOfParent(attributeOfParent());
			treeView.setParentAttributeOfChild(parentAttributeOfChild());
		}
		treeView.setFieldForDisplay(attributeForDisplay());
	}
	private void initPourSelection() {
		treeView.setQualifierForFirstColumn(qualifierForColumn());
		treeView.setRestrictionQualifier(restrictionQualifier());
	}
	private void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationSimpleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_CLICK,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationDoubleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_DOUBLE_CLICK,null);		// Chargement de l'organigramme
	}

	/** 
	 * Classe d'ecoute pour les doubleclics dans la table view 
	 */
	public class ClickTbvListener extends MouseAdapter { 
		public void mouseClicked(MouseEvent e) { 
			if (e.getClickCount() == 2) 
				valider();
		} 
	}
}
