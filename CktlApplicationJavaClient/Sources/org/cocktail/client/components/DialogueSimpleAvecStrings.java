/*
 * Created on 14 mars 2006
 *
 * Gère une liste de sélection dont les éléments sont des simples strings
 */
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client.components;

import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/** Cette classe est l'&eacute;quivalent de SimpleDialogWithString.<BR>
 * Affiche un dialogue avec une liste de strings
 * @author christine
 *
 */
public class DialogueSimpleAvecStrings extends DialogueSimple {
	private NSMutableArray stringsToDisplay;
	/** Constructeur
	 * @param notificationValidation notification envoy&eacute;e lorsque l'utilisateur valide la s&eacute;lection
	 * @param titreDialogue titre du dialogue
	 * @param supporteSelectionMultiple true si l'utilisateur peut s&eacute;lectionner plusieurs records
	 * @param estModal true si c'est un dialogue modal
	 */
	public DialogueSimpleAvecStrings(String notificationValidation,String titreDialogue,boolean supporteSelectionMultiple,boolean estModal,NSArray strings) {
		super(null,notificationValidation,titreDialogue,false,supporteSelectionMultiple,estModal,false);
		preparerStrings(strings);
	}
	// méthodes protégées
	protected NSArray fetchObjects() {
		return stringsToDisplay;
	}
	/** Retourne un tableau de strings si la s&eacute;lection multiple est autoris&eacute;e, une String sinon */
	protected Object selectedObject() {
		if (isMultipleSelectionAllowed()) {
			NSMutableArray strings = new NSMutableArray();
			java.util.Enumeration e = displayGroup.selectedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				strings.addObject(((StringAvecLibelle)e.nextElement()).libelle());
			}
			return strings; 
		} else {
			return ((StringAvecLibelle)displayGroup().selectedObject()).libelle();	
		} 
	}
	// méthodes privées
	private void preparerStrings(NSArray strings) {
		if (strings != null) {
			stringsToDisplay = new NSMutableArray();
			java.util.Enumeration e = strings.objectEnumerator();
			while (e.hasMoreElements()) {
				Object string = e.nextElement();
				stringsToDisplay.addObject(new StringAvecLibelle((String)string));
			}
		}
	}
	/** Transforme une string en un objet impl&eacute;mentant les interfaces RecordAvecLibelle et NSKeyValueCoding  */
	public class StringAvecLibelle implements RecordAvecLibelle, NSKeyValueCoding {
		private String libelle;

		public StringAvecLibelle(String text) {
			libelle = text;
		}
		public String libelle() {
			return libelle;
		}
		public String toString() {
			return libelle;
		}
		// interface keyValueCoding
		public Object valueForKey(String cle) {
			if (cle.equals("libelle")) {
				return libelle;
			} else {
				return null;
			}
		}
		public void takeValueForKey(Object valeur,String cle) {
		}

	}
}
