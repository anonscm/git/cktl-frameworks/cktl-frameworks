/*
 * Created on 20 mai 2005
 *
 * Classe Application client
 */
/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.WindowListener;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.FenetreSecondaire;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.VueAvecOnglets;
import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOComponentController;
import com.webobjects.eoapplication.EOController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.EOUserInterfaceParameters;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EOTextAssociation;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimeZone;
/**
 * classe Application client<BR>
 * @author christine
 */
/* Actions : les actions sont toutes construites sur le même schéma. Elles invoquent une méthode d&eacute;finie dans la 
 * nomenclature et implémentée dans le superviseur. 
 * Cette méthode appelle une autre méthode (2 types selon qu'il s'agit d'une page de gestion d'un individu ou non) 
 * qui passe en paramètre son nom et la classe du contrleur à instancier.
 * Exemple : afficherEtatCivil action qui invoque afficherInfosPourIndividu("afficherEtatCivil","org.cocktail.mangue.client.individu.GestionEtatCivil");
 * Les noms de méthode correspondant à des actions doivent être définies dans la nomenclature si elles requièrent une
 * gestion spécifique liée à l'affichage en mode fenêtre ou onglets.
 * Dans le cas de la gestion des préférences, il n'y a pas de gestion spécifique => pas de définition dans la nomenclature
 * 
 * En ce qui concerne la gestion des correspondances entre items de menus et onglets, lire la documentation de la nomenclature.
 * Selon les droits de l'agent, les items de menu sont autoris&eacute;s ou non. La nomenclature contient les attributs &agrave; v&eacute;rifier pour ces droits.
 * 
 * Gestion du menu de fenêtres et de l'activation des contrôleurs :
 * On utilise un dictionnaire qui contient comme clé le titre associé à l'action du menu "Fenêtres" et comme valeur le contrôleur:
 * 	- dans le cas d'un affichage en mode "Fenetre", le contrôleur correspond au contrôleur du type d'infos géré
 * 	- dans le cas d'un affichage en mode "Onglet", c'est une sous-classe de VueAvecOnglets.
 * 	- les actions générales du menu "Fenêtres" ont aussi une entrée dans le dictionnaire, l'objet est le Superviseur
 * Les actions générales ("Tout Ramener au Premier Plan", "Tout fermer") sont définies dans le Superviseur, les actions affichant les fenêtres de contrôleur
 * d'interface sont définies dans ModelePage
 * Toutes les actions du menu "Fenêtres" sont stockées dans les additionalActions() de EOApplication afin que le menu soit affiché.
 * Le dictionnaire est utilisé pour savoir si un contrôleur est déjà affiché suite à une autre commande de menu ou si il faut l'afficher directement
 * Lorsqu'on affiche les vues avec des onglets : on détermine l'onglet dans lequel on affiche le composant par le chemin d'accès complet de l'item
 * de menu. Exemple : Employé/Infos/Etat Civil => on veut afficher dans la fenêtre Employé, l'onglet Etat Civil de la vue à onglets Infos
 * 
 * Gêre les notifications de sélection des onglets (dans le cas d'un affichage en onglets) : la notification contient le titre de l'onglet.
 * Il est utilisé pour déterminer l'action des defaultActions() à déclencher. 
 */
public abstract class ApplicationCocktail extends EOApplication implements InterfaceApplication,InterfaceApplicationAvecImpression, InterfaceApplicationAvecPreferences {
	private EOEditingContext editingContext;
	private boolean estLocke;
	private boolean modeFenetre;
	private boolean actionEnCours;
	private String derniereAction;
	private ListeControleurs listeControleurs;	// gère la liste des contrôleurs secondaires
	private static JTextComponent lastActivedTextField;
	private static String TOUT_AFFICHER = "Tout Ramener au Premier Plan";
	/** Notification &agrave; envoyer pour demander le d&eacute;clenchement d'une action */
	public static String ACTIVER_ACTION = "ActiverAction";

	public ApplicationCocktail() {
		super();
	} 
	/** Retourne le nom de l'application lue dans JavaClient.wo */
	public String nomApplication() {
		java.util.Enumeration e = arguments().keyEnumerator();
		while (e.hasMoreElements()) {
			String cle = (String)e.nextElement();
			if (cle.equals("applicationName")) {
				return (String)arguments().objectForKey(cle);
			}
		}
		return "ApplicationCocktail";
	}
	public boolean modeFenetre() {
		return modeFenetre;
	}
	public boolean canQuit() {
		if (hasEditedDocuments()) {
			return false;
		} else {
			return super.canQuit();
		}
	}
	/** Retourne l'editing context de l'application */
	public EOEditingContext editingContext() {
		return editingContext;
	}
	// Gestion des préférences
	/** Retourne les pr&eacute;f&eacute;rences par d&eacute;faut (d&eacute;finies dans JavaClient.wo) */
	public NSDictionary preferencesParDefaut() {
		NSMutableDictionary valeursParDefaut = new NSMutableDictionary();
		java.util.Enumeration e = arguments().keyEnumerator();
		while (e.hasMoreElements()) {
			String cle = (String)e.nextElement();
			if (cle.startsWith("prefs")) {
				valeursParDefaut.setObjectForKey(arguments().objectForKey(cle),cle);
			}
		}
		return valeursParDefaut;
	}
	// Autres méthodes
	public void finishInitialization() {
		super.finishInitialization();
		TimeZone.setDefault(TimeZone.getTimeZone("GMT") );
		NSTimeZone.setDefaultTimeZone(NSTimeZone.getGMT());
		preparerLog();
		LogManager.logInformation("Default Time Zone " + TimeZone.getDefault().toString());
		LogManager.logInformation("Default NSTime Zone " + NSTimeZone.defaultTimeZone().toString());
		// Indiquer la couleur de fond quand le champ est actif et inactif
		EOTextAssociation.setDefaultBackgroundColors(GraphicUtilities.COL_FOND_ACTIF,GraphicUtilities.COL_FOND_INACTIF);
		setQuitsOnLastWindowClose(false);
		loadNotifications();
		editingContext = new EOEditingContext();
		listeControleurs = new ListeControleurs();
		afficherDialogueConnexion();
	}
	// méthode appelée en cas de time-out de session
	public void quitWithMessage(String title,String message) {
		super.quitWithMessage("Erreur","La session avec le serveur est interrompue. L'application va quitter automatiquement");
	}
	/** Ajouter la gestion du menu Edition (copier/coller/tout s&eacute;lectionner) et quitter aux actions par d&eacute;faut */
	public NSArray defaultActions() {
		NSMutableArray actions = new NSMutableArray();
		actions.addObject(EOAction.actionForObject("quitter","App/Quitter","Quitter l'application",null, null, null, 100,1600, this));
		actions.addObject(EOAction.actionForObject("copier","Edit/Copier","Copier",null,null,KeyStroke.getKeyStroke('C',acceleratorForOS()),EOAction.EditCategoryPriority,EOAction.PasteboardCopyActionPriority,this));
		actions.addObject(EOAction.actionForObject("toutSelectionner","Edit/Tout Sélectionner","Tout Sélectionner",null,null,KeyStroke.getKeyStroke('A',acceleratorForOS()),EOAction.EditCategoryPriority,EOAction.PasteboardSelectAllActionPriority,this));
		NSArray menusActions = MenuActionCocktail.menuActionsCocktail();
		if (menusActions != null) {
			actions.addObjectsFromArray(MenuActionCocktail.actions());
		}
		// On ne retourne plus les default actions pour ne pas avoir le menu cut/copy/paste
		//return EOAction.mergedActions(actions, super.defaultActions());
		return actions;
	}
	public boolean canPerformActionNamed(String unNom) {
		if (windowObserver() != null && windowObserver().activeWindow() != null) {
			if (unNom.equals("couper") || unNom.equals("copier") || unNom.equals("coller") || unNom.equals("toutSelectionner")) {
				Component component = windowObserver().activeWindow().getFocusOwner();
				// Lors de l'activation du menu le focusOwner change pour le root pane, on prend donc le dernier champ texte activé
				if (component instanceof JRootPane) {
					if (lastActivedTextField != null) {
						boolean actionOK = lastActivedTextField.isEditable() && lastActivedTextField.isEnabled();
						if (unNom.equals("couper") || unNom.equals("copier")) {
							actionOK = actionOK &&  lastActivedTextField.getSelectedText() != null;
						} else if (unNom.equals("coller")) {
							Clipboard clip = lastActivedTextField.getToolkit().getSystemClipboard();
							Transferable content = clip.getContents(null);
							actionOK = actionOK && content.isDataFlavorSupported(DataFlavor.stringFlavor);
						} else if (unNom.equals("toutSelectionner")) {
							actionOK = actionOK && lastActivedTextField.getText().length() > 0;
						}
						return actionOK;
					}
					return true;
				} else if (component instanceof JTextComponent) {
					lastActivedTextField = (JTextComponent)component;
					return false;
				} else {
					lastActivedTextField = null;
					return false;
				}
			}
		}
		if (estLocke) {
			if (unNom.equals("toutAfficher")) {
				return true;
			} else {
				return false;
			}
		} else {
			if (unNom.equals("modeNormal")) {
				return LogManager.estModeDebug();
			}
			if (unNom.equals("modeDebug")) {
				return !LogManager.estModeDebug();
			}
		}
		return peutActiverAction(unNom);
	}
	// actions
	/** Action du menu Edition pour couper du texte dans un champ texte */
	public void couper() {
		if (lastActivedTextField != null) {
			lastActivedTextField.cut();
		}
	}
	/** Action du menu Edition pour copier du texte dans un champ texte */
	public void copier() {
		if (lastActivedTextField != null) {
			lastActivedTextField.copy();
		}
	}
	/** Action du menu Edition pour coller du texte dans un champ texte */
	public void coller() {
		if (lastActivedTextField != null) {
			lastActivedTextField.paste();
		}
	}
	/** Action du menu Edition pour s&eacute;lectionner tout le texte d'un champ texte */
	public void toutSelectionner() {
		if (lastActivedTextField != null) {
			lastActivedTextField.selectAll();
		}
	}
	/** Action du menu Fen&circ;tre pour afficher toutes les fen&circ;tres */
	public void toutAfficher() {
		EOFrameController controleur = null;
		// vérifier si il y a une fenêtre en cours d'édition
		if (editedDocuments().count() > 0) {
			controleur = (EOFrameController)((EOInterfaceController)editedDocuments().objectAtIndex(0)).supercontroller();
		}
		// ne pas utiliser la méthode du WindowObserver.bringAllToFront car elle casse les menus
		java.util.Enumeration e = documents().objectEnumerator();
		while (e.hasMoreElements()) {
			((EOFrameController)((EOInterfaceController)e.nextElement()).supercontroller()).activate();
		}
		if (controleur != null) {
			((EOFrameController)controleur).activateWindow();
		}
	}
	/** Action du menu Fen&circ;tre pour toutFermer toutes les fen&circ;tres */
	public void toutFermer() {
		java.util.Enumeration e = listeControleurs.controleurs().objectEnumerator();
		while (e.hasMoreElements()) {
			EOController controleur = (EOController)e.nextElement();
			if (controleur instanceof ModelePage) {
				((ModelePage)controleur).arreter();
				supprimerControleurPourAction(controleur);
			} else if (controleur instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleur).arreter();
				supprimerControleurPourAction(controleur);
			} else if (controleur instanceof FenetreSecondaire) {
				((FenetreSecondaire)controleur).arreter();
				supprimerControleurPourAction(controleur);
			}
		}
	}
	/** Action pour afficher les pr&eacute;f&eacute;rences */
	public void afficherPreferences() {
		try {
			if (nomClasseControleurPreferences() != null) {
				EOComponentController preferences = (EOComponentController)Class.forName(nomClasseControleurPreferences()).newInstance();
				EOFrameController.runControllerInNewFrame(preferences,"Préférences Utilisateur");
				((EOFrameController)preferences.supercontroller()).window().setLocationRelativeTo(null);		// centrage sur l'écran
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Modifie le mode de l'application en mode normal */
	public void modeNormal() {
		preparerLogPourDebug("N");
	}
	/** Modifie le mode de l'application en mode debug */
	public void modeDebug() {
		preparerLogPourDebug("O");
	}
	/** Affiche la fen&circ;tre de logs */
	/** Surcharge de la m&acute;thode de EOApplication pour gérer le Ctrl-Q
	 * On ne permet de quitter que si on peut utiliser le menu "quitter"
	 */	
	public void quit() {
		if (!actionEnCours && canPerformActionNamed("quitter")) {
			// on utilise actionEnCours car sinon l'affichage du dialogue dans quitter() envoie des évènements
			// qui créent une boucle infinie sur quit()
			actionEnCours = true;
			quitter();
		}
	}
	/** Action pour quitter */
	public void quitter() {
		if (EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment quitter l'application ?","Oui","Non")) {
			super.quit();
		} else {
			actionEnCours = false;
		}
	}
	// NOTIFICATIONS
	/** Suite &agrave; la notification de verrouillage des fen&circ;tres : ModelePage.LOCKER_ECRAN */
	public void lockSaisie(NSNotification aNotif) {
		estLocke = true;
		changerVerrou(!estLocke);
	} 
	/** Suite &agrave; la notification de verrouillage des fen&circ;tres : ModelePage.DELOCKER_ECRAN */
	public void unlockSaisie(NSNotification aNotif) {
		estLocke = false;
		changerVerrou(!estLocke);
	} 
	/** Suite &agrave; la notification pour signaler qu'un contr&ocirc;leur de fen&ecirc;tre est devenu inactif : InterfaceAvecFenetre.ARRET_CONTROLEUR 
	 * L'objet de la notification contient la classe du contr&ocirc;leur
	 **/
	public void arreterControleur(NSNotification aNotif) {
		EOController controleur = listeControleurs.controleurAvecNom((String)aNotif.object());
		supprimerControleurPourAction(controleur);	
	}
	/** Suite &agrave; la notification pour signaler qu'une action de menu est activ&eacute;e : ApplicationCocktail.ACTIVER_ACTION<BR>
	 * L'objet de la notification est le nom de la m&eacute;thode &agrave; activer. Recherche dans les defaults actions 
	 * l'action correspondant au titre passé en paramètre et déclenche l'action
	 */
	public void declencherAction(NSNotification aNotif) {
		if (!actionEnCours) 	{	// pour gérer le problême des notifications suite à la sélection d'un onglet résultant d'une action utilisateur sur un menu
			String nomMethode = null;
			if (aNotif.object() != null) {
				nomMethode = methodePourNomAction((String)aNotif.object());
			} else {
				// Déclenchée par les préférences lors du changement mode fenêtre/modeOnglet
				nomMethode = derniereAction;
			}
			if (nomMethode != null && canPerformActionNamed(nomMethode)) {
				try {
					java.lang.reflect.Method methode = this.getClass().getMethod(nomMethode);	
					methode.invoke(this);
				} catch (Exception e) {
					LogManager.logException(e);
				}
			}
		}
	}
	/** Notification reçue lorsque l'utilisateur active une fenêtre ou un dialogue (envoyée par le WindowObserver)<BR>
	 * Met &grave; jour le menu de fen&ecirc;tres */
	public void activeWindowDidChange(NSNotification notification) {
		// le menu Fenêtre n'étant pas automatiquement mis à jour par WO quand on ouvre une nouvelle fenêtre
		// on le fait manuellement en java
		if (windowObserver().activeWindow() != null && windowObserver().activeWindow() instanceof JFrame) {
			JMenuBar menubar = menuBar();
			JMenu menu = menubar.getMenu(menubar.getMenuCount() - 1);
			menu.removeAll();

			java.util.Enumeration e = EOAction.sortedActions(additionalActions()).objectEnumerator();
			while (e.hasMoreElements()) {
				EOAction action = (EOAction)e.nextElement();
				JMenuItem menuItem = new JMenuItem(action);
				menuItem.setText(action.actionTitle());
				menuItem.setEnabled(action.actionTitle().equals(TOUT_AFFICHER) || !estLocke);
				menu.add(menuItem);
			}
		}
	}
	// Autres méthodes
	/** Supprime un contr&ocirc;leur de la liste des contr&eocirc;leurs en cours */
	public void supprimerControleurPourAction(EOController controleur) {
		java.util.Enumeration e = listeControleurs.nomControleurs().objectEnumerator();
		while (e.hasMoreElements()) {
			String titre = (String)e.nextElement();
			EOController controller = (EOController)listeControleurs.controleurAvecNom(titre);
			if (controller == controleur) {
				EOAction action = actionPourTitre(titre);
				NSMutableArray actions = new NSMutableArray(additionalActions());
				actions.removeObject(action);
				setAdditionalActions(actions);
				listeControleurs.supprimerControleur(titre);
				break;
			}
		}
	}
	/** retourne le contr&ocirc;leur de document associ&eacute; &agrave; la fen&ecirc;tre dont le nom est pass&eacute; en param&egrave;tre */
	public EOController controleurDocumentPourFenetre(String nomFenetre) {
		return (EOController)listeControleurs.controleurAvecNom(nomFenetre);
	}
	/** retourne le nom de la m&eacute;thode associ&eacute;e &agrave; une action en recherchant dans les defaults actions */
	public String methodePourNomAction(String nomAction) {
		java.util.Enumeration e = defaultActions().objectEnumerator();
		while (e.hasMoreElements()) {
			EOAction action = (EOAction)e.nextElement();
			if (action.actionTitle().equals(nomAction)) {
				return action.actionName();
			}
		}
		return null;
	}
	/** Retourne le mail du destinataire des mails en cas d'erreur */
	public abstract String destinaireMailErreur();
	// Méthodes protégées
	protected abstract void afficherDialogueConnexion();
	/** A surcharger. Retourner la classe compl&egrave;te du contr&ocirc;leur de pr&eacute;f&eacute;rences */
	protected abstract String nomClasseControleurPreferences();
	/** Retourne true pour autoriser l'item de menu li&eacute; &grave; l'action */
	protected abstract boolean peutActiverAction(String nomAction);
	/** Retourne le nom du package qui contient les vues &agrave; onglet */
	protected abstract String nomPackagePourVuesOnglets();
	/** A surcharger pour ajouter d'autres notifications */
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("declencherAction", new Class[] { NSNotification.class }), ACTIVER_ACTION, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("arreterControleur", new Class[] { NSNotification.class }), FenetreSecondaire.NOTIFICATION_ARRET_CONTROLEUR, null);
	}
	/** Affiche le contr&ocirc;leur li&eacute; &agrave; l'action
	 * @param Action action Cocktail
	 * @param nomMethode
	 * @param nomClasse
	 */
	protected void afficherControleurPourAction(String nomAction) {
		LogManager.logDetail("action : " + nomAction);
		MenuActionCocktail action = MenuActionCocktail.actionAvecNom(nomAction);
		if (nomAction == null) {
			return;
		}
		derniereAction = action.nomMethode();
		String nomClasse = action.classeControleur();
		preparerPourAction(true);
		boolean composantAffiche = true;
		String titre = nomAction; // retourne le titre de l'onglet ou la fenêtre à afficher pour cette méthode
		if (titre == null) {		// cas d'une méthode non définie dans la nomenclatures
			return;
		}
		if (afficherFenetre(titre) == false || (!modeFenetre && !(composantAffiche = estAfficheComposant(titre,nomClasse))))  {
			try {
				ModelePage controleur = (ModelePage)Class.forName(nomClasse).newInstance();
				if (composantAffiche) {
					afficherFenetre(controleur,titre);
				} else {
					afficherComposant(controleur,titre);
				}
			} catch (Exception e) {
				LogManager.logErreur("nomClasse inconnu " + nomClasse);
				LogManager.logException(e);
			}
		}
		preparerPourAction(false);
	}
	/** Retourne la barre de menu */
	protected JMenuBar menuBar() {
		return ((JFrame)windowObserver().activeWindow()).getJMenuBar();
	}
	/** Modifie la police de tous les menus de la barre de menu */
	protected void modifierPoliceMenuBar(Font font) {
		JMenuBar menubar = menuBar();
		for (int i = 0; i < menubar.getMenuCount();i++) {
			JMenu menu = menubar.getMenu(i);
			modifierPolice(menu);
		}
		menubar.setFont(font);
	}
	/** A surcharger. Retourne le nom de l'image &agrave; utiliser pour iconiser une fen&ecirc;tre de l'application */
	protected abstract String nomImagePourIconisation();
	/** A surcharger. Retourne le nom du fichier de log client */
	protected abstract 	String nomFichierLogClient();
	/**  pour modifier la position d'une fen&ecir;tre qui ne va pas au coin gauche sup&eacute;rieur de l'&eacute;cran */
	protected abstract void changerPositionFenetre(EOInterfaceController controleurFenetre);

	// méthodes privées

	// affiche la fenêtre dont le titre est passé en paramètre
	private boolean afficherFenetre(String titre) {
		String titreFenetre;
		String path = null;
		if (!modeFenetre) {
			path = pathPourTitre(titre);
			if (path != null) {
				titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
			} else {
				titreFenetre = null;
			}
		} else {
			titreFenetre = titre;
		}
		EOController controleur = null;
		if (titreFenetre != null) {
			controleur = (EOController)listeControleurs.controleurAvecNom(titreFenetre);
			if (controleur != null && controleur instanceof EOInterfaceController) {
				((EOFrameController)controleur.supercontroller()).activate();
			}
		}
		if (controleur != null && !modeFenetre) {
			if (path != null && controleur instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleur).activerOngletPourPath(path,"/");
			}
		}
		return controleur != null;
	}
	// affiche le composant dont le titre est passé en paramètre
	//	dans le cas d'une fenêtre, affiche la fenêtre, dans le cas d'une vue à Onglets, instancie le contrôleur de fenêtre 
	//	et affiche la vue choisie en se basant sur le path de la commande de menu
	private void afficherFenetre(ModelePage controleur,String titre) {
		String titreFenetre;
		String path = null;
		EOInterfaceController controleurPourFenetre;
		if (!modeFenetre) {
			path = pathPourTitre(titre);
			if (path != null) {
				titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
				controleurPourFenetre = instancierControleurAvecNom(titreFenetre);
			} else {
				titreFenetre = null;
				controleurPourFenetre = null;
			}
		} else {
			titreFenetre = titre;
			controleurPourFenetre = controleur;
		}
		if (controleurPourFenetre != null) {
			ajouterAction(controleurPourFenetre,titreFenetre);
			EOFrameController.runControllerInNewFrame(controleurPourFenetre,titreFenetre);
			if (controleurPourFenetre != null && controleurPourFenetre.supercontroller() instanceof EOFrameController) {
				((EOFrameController)controleurPourFenetre.supercontroller()).window().show();
				JFrame frame = (JFrame)((EOFrameController)controleurPourFenetre.supercontroller()).window();
				ImageIcon icon = (ImageIcon)EOUserInterfaceParameters.localizedIcon(nomImagePourIconisation());
				icon.setDescription(titreFenetre);
				frame.setIconImage(icon.getImage());
			}

		}
		changerPositionFenetre(controleurPourFenetre);
		if (controleurPourFenetre instanceof VueAvecOnglets) {
			((VueAvecOnglets)controleurPourFenetre).ajouterControleurPourOngletAvecPath(controleur,path,"/");
		}

	}
	// Affiche un composant dans un onglet
	private void afficherComposant(ModelePage controleur,String titre) {
		if (modeFenetre) {
			return;
		}
		String path = pathPourTitre(titre);
		if (path != null) {
			String titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
			EOController controleurPourFenetre = (EOController)listeControleurs.controleurAvecNom(titreFenetre);
			if (controleur != null && controleurPourFenetre instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleurPourFenetre).ajouterControleurPourOngletAvecPath(controleur,path,"/");
			}
		}
	}
	private EOInterfaceController instancierControleurAvecNom(String nomFenetre) {
		String nom = StringCtrl.chaineSansAccents(nomFenetre);
		try {
			String nomPackage = nomPackagePourVuesOnglets();
			if (nomPackage.endsWith(".") == false) {
				nomPackage += ".";
			}
			VueAvecOnglets controleur =  (VueAvecOnglets)Class.forName(nomPackage).newInstance();	// on attend un constructeur du type new GestionEmploye() 
			controleur.initialiser(nomFenetre,0,0);
			
			return controleur;
		} catch (Exception e) {
			LogManager.logErreur("nomClasse inconnu : " + nom);
			LogManager.logException(e);
			return null;
		}
	}
	//	retourne true si le composant de cette classe est affiché
	private boolean estAfficheComposant(String titre,String nomClasse) {
		String titreFenetre;
		String path = pathPourTitre(titre);
		if (path == null) {
			return false;
		}
		titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);

		EOController controleur = (EOController)listeControleurs.controleurAvecNom(titreFenetre);
		if (controleur != null && controleur instanceof VueAvecOnglets && ((VueAvecOnglets)controleur).estActifControleurPourClasse(nomClasse)) {
			return true;
		} else {
			return false;
		}
	}
	private void ajouterActionMenuFenetre(EOController controleur,String titre,String nomAction,int priorite) {
		EOAction action = EOAction.actionForObject(nomAction,"Fen/" + titre, titre, null, null,  null, 2000,priorite, controleur);
		listeControleurs.ajouterControleur(controleur,titre);
		setAdditionalActions(EOAction.mergedActions(additionalActions(),new NSArray(action)));	// pour que le menu "Fenêtres"  apparaisse
	}
	private void ajouterAction(EOController controleur,String titre) {
		ajouterActionMenuFenetre(controleur,titre,"afficherFenetre",100);
	}
	private EOAction actionPourTitre(String titre) {
		java.util.Enumeration e = additionalActions().objectEnumerator();
		while (e.hasMoreElements()) {
			EOAction action = (EOAction)e.nextElement();
			if (action.actionTitle().equals(titre)) {
				return action;
			}
		}
		return null;
	}
	private String pathPourTitre(String titre) {
		java.util.Enumeration e = defaultActions().objectEnumerator();
		while (e.hasMoreElements()) {
			EOAction action = (EOAction)e.nextElement();
			if (action.actionTitle().equals(titre)) {
				return action.descriptionPath();
			}
		}
		return null;
	}
	private void changerVerrou(boolean enabled) {
		if (windowObserver().activeWindow() != null) {
			if (windowObserver().activeWindow() instanceof JFrame) {
				JMenuBar menubar = ((JFrame)windowObserver().activeWindow()).getJMenuBar();
				JMenu menu = menubar.getMenu(menubar.getMenuCount() - 1);
				for (int i = 0; i < menu.getItemCount(); i ++) {
					JMenuItem menuItem = menu.getItem(i);
					if (menuItem.getText().equals(((JFrame)windowObserver().activeWindow()).getTitle()) == false) {
						// les articles de menu et les fenêtres sont identiques
						menuItem.setEnabled(menuItem.getText().equals(TOUT_AFFICHER) || enabled);
					}
				}
				enableMenuEdit(enabled);
			}
		}
		// locker les fenêtres en cas de mode onglet
		if (!modeFenetre) {
			java.util.Enumeration e = listeControleurs.controleurs().objectEnumerator();
			while (e.hasMoreElements()) {
				EOController controleur = (EOController)e.nextElement();
				// 
				if (controleur instanceof ModelePage  && ((ModelePage)controleur).estAffichagePermanent() && controleur.supercontroller() != null && controleur.supercontroller() instanceof EOFrameController) {
					EOFrameController controller = (EOFrameController)controleur.supercontroller();
					if (!enabled) {
						WindowListener[] listeners = controller.window().getWindowListeners();
						for (int i = 0; i < listeners.length;i++) {
							if (listeners[i] == controller) {
								controller.window().removeWindowListener((WindowListener)listeners[i]);
							}
						}
					} else {
						controller.window().addWindowListener(controller);
					}
				}
			}
		}
	}
	private void preparerPourAction(boolean estDemarrage) {
		Window activeWindow = windowObserver().activeWindow();
		if (activeWindow != null) {
			Cursor cursor;
			if (estDemarrage) {
				cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
			} else {
				cursor = Cursor.getDefaultCursor();
			}
			activeWindow.setCursor(cursor);
		}
		actionEnCours = estDemarrage;		// pour gérer le problême des notifications suite à la sélection d'un onglet résultant d'une action utilisateur sur un menu
	}

	private void modifierPolice(JMenu menu) {
		for (int i = 0; i < menu.getMenuComponentCount();i++) {
			JMenuItem menuItem = menu.getItem(i);
			if (menuItem != null) {	// a l'initialisation
				if (menuItem instanceof JMenu) {
					modifierPolice((JMenu)menuItem);
				} else {
					Font font = menuItem.getFont();
					menuItem.setFont(new Font(font.getName(),font.getStyle(),10));
				}

			}
			Font font = menu.getFont();
			menu.setFont(new Font(font.getName(),font.getStyle(),10));
		}
	}
	//	Gestion des logs
	private void preparerLog() {
		LogManager.preparerLog(nomFichierLogClient(), (String)arguments().valueForKey("modeConsole"), (String)arguments().valueForKey("modeDebug"));
	}
	private void preparerLogPourDebug(String modeDebug) {
		LogManager.changerModeDebug(modeDebug);
	}
	private void enableMenuEdit(boolean aBool) {
		if (supercontroller() instanceof EOFrameController) {
			JMenuBar menuBar = ((JFrame)((EOFrameController)supercontroller()).window()).getJMenuBar();
			JMenu menu = menuBar.getMenu(1);
			menu.setEnabled(true);
			for (int i= 0;i < menu.getItemCount();i++) {
				JMenuItem item = menu.getItem(i);
				item.setEnabled(aBool);
			}
		}
	}
	private int acceleratorForOS() {
		String operationSystem = System.getProperty("os.name");
		if (operationSystem != null) {
			operationSystem = operationSystem.toLowerCase();
			if (operationSystem.startsWith("mac")) {
				return java.awt.event.InputEvent.META_MASK;
			}
		}
		return java.awt.event.InputEvent.CTRL_MASK;
	}
	//	Classe privée pour gérer les contrôleurs 
	private class ListeControleurs {
		private NSMutableDictionary dictionnaireControleurs;

		public ListeControleurs() {
			dictionnaireControleurs = new NSMutableDictionary();
		}
		// nom associé au contrôleur
		public void ajouterControleur(EOController controleur,String nom) {
			dictionnaireControleurs.setObjectForKey(controleur,nom);
		}
		public void supprimerControleur(String nom) {
			dictionnaireControleurs.removeObjectForKey(nom);
		}
		public EOController controleurAvecNom(String nom) {
			return (EOController)dictionnaireControleurs.objectForKey(nom);
		}
		public NSArray controleurs() {
			return dictionnaireControleurs.allValues();
		}
		public NSArray nomControleurs() {
			return dictionnaireControleurs.allKeys();
		}
	}
	
}