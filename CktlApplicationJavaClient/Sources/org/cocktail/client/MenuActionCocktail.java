/*
 * Copyright Consortium Cocktail
 *
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.client;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** G&grave;re les actions de menu */
public class MenuActionCocktail {
	private static NSMutableArray menuActionsCocktail;
	private EOAction action;
	private String classeControleur;
	public MenuActionCocktail(EOAction action,String classeControleur) {
		this.classeControleur = classeControleur;
		this.action = action;
	}
	/** Retourne le nom de l'action */
	public String nomAction() {
		return action.actionTitle();
	}
	/** Retourne le nom de la m&eacute;thode */
	public String nomMethode() {
		return action.actionName();
	}
	/** Retourne la classe compl&grave;te du controleur */
	public String classeControleur() {
		return classeControleur;
	}
	/** Retourne la liste des actions Cocktail
	 * 
	 * @return NSArray tableaux d'actions
	 */
	public static NSArray menuActionsCocktail() {
		return menuActionsCocktail;
	}
	/** Ajoute une action &grave; la liste des actions Cocktail */
	public static void ajouterMenuAction(EOAction action,String classeControleur) {
		if (menuActionsCocktail() == null) {
			menuActionsCocktail = new NSMutableArray();
		}
		menuActionsCocktail.addObject(new MenuActionCocktail(action,classeControleur));
	}
	/** Retourne un tableau de EOActions ou null si pas d'action */
	public static NSArray actions() {
		if (menuActionsCocktail() == null) {
			return null;
		} else {
			return (NSArray)menuActionsCocktail().valueForKey("action");
		}
	}
	/** Retourne l'action avec le nom pass&eacute; en param&egrave;tre ou null si non trouv&eacute;e */
	public static MenuActionCocktail actionAvecNom(String unNom) {
		if (menuActionsCocktail() == null || unNom == null) {
			return null;
		}
		java.util.Enumeration e = menuActionsCocktail().objectEnumerator();
		while (e.hasMoreElements()) {
			MenuActionCocktail menuAction = (MenuActionCocktail)e.nextElement();
			if (menuAction.equals(unNom)) {
				return menuAction;
			}
		}
		return null;
	}
}
