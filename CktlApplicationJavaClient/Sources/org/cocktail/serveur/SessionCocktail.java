/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.serveur;


//Session.java

//Created by christine on Wed Apr 13 2005


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.cocktail.common.LogManager;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

import org.cocktail.fwkcktlwebapp.common.metier.EOCompte;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;

public class SessionCocktail extends CktlWebSession {
	private ServerThreadManager threader;
	

	public SessionCocktail() {
		super();
		defaultEditingContext().setDelegate(this);
	}
	/*	public NSArray editingContextShouldFetchObjects(EOEditingContext context,EOFetchSpecification fetchSpec)  {
		if (fetchSpec.entityName().equals("Carriere") || fetchSpec.entityName().equals("ElementCarriere")) {
			System.out.println("editingContextShouldFetchObjects " + fetchSpec);}
		System.out.println(context.insertedObjects());
		return null;
	}	
	 */	
	/*	public NSData distributionContextWillSendData(EODistributionContext distributionContext,
												  NSData data) {
		System.out.println("distributionContextWillSendData");
		return data;
	}*/
	public boolean editingContextShouldPresentException(EOEditingContext context,Throwable exception) {
		LogManager.logException(exception);
		return true;
	}
	//public boolean distributionContextShouldSave(EODistributionContext distributionContext,EOEditingContext editingContext) {
//	System.out.println("distributionContextShouldSave " + editingContext);	
//	System.out.println(">>> inserted " + editingContext.insertedObjects());	
//	System.out.println(">>> updated " + editingContext.updatedObjects());
//	return true;
//	}
	/** Autorise l'utilisation de m&eacute;thode distante */
	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return (path.startsWith("session"));
	}
	/**
	 * Verification du login/mot de passe avec SAUT
	 */
	public Boolean clientSideRequestCheckPassword(String login, String passwd) {
		Boolean retour = Boolean.FALSE;
		CktlConfig cfg = ((ApplicationCocktailServeur)WOApplication.application()).config();
		if(cfg == null) {
			return retour;
		}
		NSLog.out.appendln("sautUrl="+cfg.stringForKey("SAUT_URL"));
		SAUTClient sautClient = new SAUTClient(cfg.stringForKey("SAUT_URL"));

		String alias = cfg.stringForKey("APP_ALIAS");
		NSLog.out.appendln("alias="+alias);
		java.util.Properties infos = SAUTClient.toProperties(sautClient.requestDecryptedUserInfo(login,passwd,alias));
		if(infos.getProperty("errno").equals("0"))
			retour = Boolean.TRUE;
		else
			retour = Boolean.FALSE;
		return retour;
	}
	/**
	 * Verification du login/mot de passe sans SAUT		
	 */
	public Boolean clientSideRequestCheckCryptedPassword(String passwd, String cryptedPasswd) {
			if (!passwd.equals(cryptedPasswd)) {
				return new Boolean(CktlUserInfoDB.equalsToPass(passwd, cryptedPasswd, null, false, false));
			}
		return Boolean.TRUE;
	}

	/** retourne un param&egrave;tre de la config de l'application (nomApplication.config)
	 * @param paramKey
	 * @return valeur du param&egrave;tre
	 */
	public String clientSideRequestGetParam(String paramKey) {
		return ((ApplicationCocktailServeur)WOApplication.application()).getParam(paramKey);
	}
	/** Retourne le nom de la base utilis&eacute;e li&eacute;e au mod&egrave;le fourni */
	public String clientSideRequestRecupererNomBase(String nomModele) {
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed(nomModele);
		NSDictionary vDico = vModel.connectionDictionary();
		NSArray url = NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@");
		if (url.count() == 0)
			return " Connexion ??";
		if (url.count() == 1)
			return (String)url.objectAtIndex(0);
		return (String)(NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@")).objectAtIndex(1); 
	}
	/** Retourne le nom de la table d'une entit&eacute; du mod&egrave;le
	 * @param nomModele	nom du mod&egrave;le
	 * @param nomEntite	nom de l'entit&eacute;
	 * @return nom de la table
	 */
	public String clientSideRequestRecupererNomTablePourModeleEtEntite(String nomModele,String nomEntite) {
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed(nomModele);
		if (vModel != null) {
			return vModel.entityNamed(nomEntite).externalName();
		} else {
			return null;
		}
	}
	// Aide Utilisateur
	/** Retourne le contenu de l'aide utilisateur lue dans un wo component (partie HTML)
		@param pageAide nom du composant d'aide
	*/
	public String clientSideRequestHtmlPourPageAide(String pageAide) {
		WOComponent page = WOApplication.application().pageWithName(pageAide,context());
		WOResponse response = page.generateResponse();
		NSData data = response.content();
		return new String(data.bytes(0, data.length()));

	}
	/** Retourne true si il existe un document pdf avec comme nom documentAide (dans les ressources de l'application)
	@param documentAide nom du document d'aide
	 */
	public Boolean clientSideRequestExisteDocumentAide(String documentAide) {
		InputStream stream = null;
		try {
			stream = WOApplication.application().resourceManager().inputStreamForResourceNamed(documentAide + ".pdf",null,null);
			return new Boolean(true);
		}
		catch (Exception e) {
			// on est en version 5.2.1
			String path = WOApplication.application().path() + "/Contents/Resources/" + documentAide + ".pdf";
			File pdfFile = new File(path.toString());
			try {
				stream = new FileInputStream(pdfFile);
				return new Boolean(true);
			}
			catch (Exception e1) {
				return new Boolean(false);
			}
		}
	}
	/** Evaluation du login, password, email, domaine,uid d'un individu en utilisant des proc&eacute;dures
	 * stock&eacute;es de Grhum */
	public NSDictionary clientSideRequestEvaluerInformationsCompte(String nom,String prenom) {
		EOEditingContext editingContext = new EOEditingContext();
		NSMutableDictionary dict = new NSMutableDictionary(), dictInfo = new NSMutableDictionary();
		dict.setObjectForKey(nom, "nom");
		dict.setObjectForKey(prenom, "prenom");
		NSDictionary result = EOUtilities.executeStoredProcedureNamed(editingContext,"consLogin",dict);
		String login = (String)result.objectForKey("login");
		if (login != null) {
			dictInfo.setObjectForKey(login, "login");
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"consPasswd",new NSDictionary());
		String password = (String)result.objectForKey("password");
		if (password != null) {
			dictInfo.setObjectForKey(password, "password");
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"consEmailLong",dict);
		String emailLong = (String)result.objectForKey("emailLong");
		if (emailLong != null) {
			int index = emailLong.indexOf("@");
			if (index > 0) {
				dictInfo.setObjectForKey(emailLong.substring(0,index),"email");
				dictInfo.setObjectForKey(emailLong.substring(index + 1),"domaine");
			}
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"uidLibre",new NSDictionary());
		Number uid = (Number)result.objectForKey("uid");
		if (uid != null) {
			dictInfo.setObjectForKey(uid,"uid");
		}
		return dictInfo;
	}
	//	méthodes utilisées par l'impression pour gérer les threads
	/** Lorsque l'action est terminée, retourne un dictionnaire qui contient :
	<UL>- le résultat de l'action si celle-ci retourne une valeur (clé : resultat). 
	Ce dernier est lui-même un dictionnaire qui contient la classe du résultat (clé : classe) et la valeur du résultat (clé  valeur)</UL>
	<UL>- le diagnostic de l'action (clé : diagnostic)
	Ce dernier est lui-même un dictionnaire qui contient l'exception si elle s'est produite (clé : exception) et le message (clé: message)
	 */
	public NSDictionary clientSideRequestResultatAction() {
		NSMutableDictionary resultatAction = new NSMutableDictionary();
		if (threader != null && threader.status() != null) {
			if (threader.status().equals(ServerThreadManager.TERMINE)) {
				if (threader.resultat() != null) {
					resultatAction.setObjectForKey(threader.resultat(),"resultat");
				}
				if (threader.diagnostic() != null) {
					resultatAction.setObjectForKey(threader.diagnostic(),"diagnostic");
				}
				threader = null;
			} 
		}
		return resultatAction;
	}

	public String clientSideRequestMessage() {
		try {
			if (threader != null && threader.status() != null) {
				if (threader.status().equals(ServerThreadManager.EN_COURS)) {
					return threader.message();
				} else if (threader.status().equals(ServerThreadManager.TERMINE)) {
					return "termine";
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			LogManager.logException(e);
		}
		return null;
	}
}