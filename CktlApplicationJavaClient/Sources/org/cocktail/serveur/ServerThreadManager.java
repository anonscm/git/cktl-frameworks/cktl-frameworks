//
//  ServerThreadManager.java
//  TestThread
//
//  Created by Christine Buttin on Mon Jun 28 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.serveur;
import org.cocktail.common.LogManager;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/** @author christine
 * G&egrave;re le lancement de threads sur le serveur d'applications :
 * La m&eacute;thode invoqu&eacute; doit admettre comme dernier param&eegrave;tre une r&eacute;f&eacu;terence sur le thread manager
 * pour pouvoir communiquer avec celui-ci
 * ex: public void imprimer(Object parametre1,Object parametre2,org.cocktail.mangue.serveur.ServerThreadManager thread)
 */

public class ServerThreadManager extends Thread {
	private NSMutableDictionary resultat;
	private NSMutableDictionary diagnostic;
	private String nomMethodeAInvoquer;
	private Class[] classeParametres;
	private Object[] parametres;
	private Object target;
	private Class classeTarget;

	/** Statuts du serveur */
	public static final String EN_COURS = "En cours";
	public static final String TERMINE = "Termine";
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la m&eacute;thode
	 * @param classeCible classe de cet objet
	 * @param nomMethode m&eacute;thode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la m&eacute;thode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		target = objetCible;
		classeTarget = classeCible;
		nomMethodeAInvoquer = nomMethode;
		preparerParametres(classes,valeurs);	// pour ajouter la référence sur le thread manager
		resultat = new NSMutableDictionary(2);
		diagnostic = new NSMutableDictionary(3);
		
	}
	
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la m&eacute;thode
	 * @param nomMethode m&eacute;thode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la m&eacute;thode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Object objetCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(objetCible,objetCible.getClass(),nomMethode,classes,valeurs);
	}
	/** constructeur pour invocation d'une m&eacute;thode de classe
	 * @param classeCible classe
	 * @param nomMethode m&eacute;thode &agrave invoquer
	 * @param classes classes des param&egraves;tres de la m&eacute;thode
	 * @param valeurs valeurs des param&egraves;tres 
	 */
	public ServerThreadManager(Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(null,classeCible,nomMethode,classes,valeurs);
	}
	
	/** retourne le r&eacute;sultat de la m&eacute;thode invoqu&eacute;e sous la forme d'un dictionnaire 
	 * dont les cl&eacute;s sont la classe du r&eacute;sultat et le r&eacute;sultat lui-m&ecirc;me **/
	public NSDictionary resultat() {
		return resultat;
	}
	/** retourne le diagnostic de la m&eacute;thode invoqu&eacute;e sous la forme d'un dictionnaire **/
	public NSDictionary diagnostic() {
		return diagnostic;
	}
	
	/** retourne la classe du r&eacute;sultat */
	public String classeResultat() {
		return (String)resultat.objectForKey("classe");
	}
	
	/** retourne la valeur du r&eacute;sultat */
	public Object valeurResultat() {
		return resultat.objectForKey("valeur");
	}
	
	/** modifie la classe du r&eacute;sultat */
	public void setClasseResultat(String nomClasse) {
		resultat.setObjectForKey(nomClasse,"classe");
	}
	/** modifie la valeur du r&eacute;sultat */
	public void setValeurResultat(Object valeur) {
		resultat.setObjectForKey(valeur,"valeur");
	}
	/** retourne le status de l'op&eacute;ration en cours */
	public String status() {
		return (String)diagnostic.objectForKey("status");
	}
	/** retourne le message d'information de l'op&eacute;ration en cours. Peut contenir plusieurs messages s&eacute;par&eacute;s par
	 * "\n" si l'op&eacute;ration a &eacute;mis plusieurs messages qui n'ont pas &eacute;t&eacute; transmis */
	public String message() {
		String texte = (String)diagnostic.objectForKey("message");
		diagnostic.removeObjectForKey("message");
		return texte;
	}
	/** retourne le message d'exception d&eacute;clench&eacute; pendant l'op&eacute;ration */
	public String exception() {
		return (String)diagnostic.objectForKey("exception");
	}
	/** modifie le status de l'op&eacute;ration en cours */
	public void setStatus(String unTexte) {
		diagnostic.setObjectForKey(unTexte,"status");
	}
	/** modifie le message d'information de l'op&eacute;ration en cours */
	public void setMessage(String unTexte) {
		setMessage(unTexte,true);
	}
	/** modifie le message d'information de l'op&eacute;ration en cours. Si append = true ajoute le texte
	 * au message courant */
	public void setMessage(String unTexte,boolean append) {
		// concaténer les messages si ils n'ont pas encore été lus
		if (append && diagnostic.objectForKey("message") != null) {
			diagnostic.setObjectForKey(diagnostic.objectForKey("message") + "\n" + unTexte,"message");
		} else {
			diagnostic.setObjectForKey(unTexte,"message");
		}
	}
	/** modifie le message d'exception de l'op&eacute;ration en cours */
	public void setException(String unTexte) {
		if (unTexte != null) {		
			diagnostic.setObjectForKey(unTexte,"exception");
		}
	}
	/** lance l'ex&eacute;cution de la m&eacute;thode : &agrave; utiliser pour une application Java Client */
	public void run() {
		lancerOperation();
	}
	/** lance l'ex&eacute;cution de la m&eacute;thode : &agrave; utiliser pour une application WO HTML */
	public void lancerOperation() {
		try {
			setStatus(EN_COURS);
			java.lang.reflect.Method methode = classeTarget.getMethod(nomMethodeAInvoquer,classeParametres);
			Object object = methode.invoke(target,parametres);
			if (object != null) {
				setClasseResultat(object.getClass().getName());
				setValeurResultat(object);
			}
			setStatus(TERMINE);

		} catch(Exception e) {
			LogManager.logException(e);
			setException(e.getMessage());
			setStatus(TERMINE);

		}
	}
	private void preparerParametres(Class[] classes,Object[] valeurs) {
		classeParametres = new Class[classes.length + 1];
		for (int i = 0; i < classes.length; i++) {
			classeParametres[i] = classes[i];
		}
		try {
			classeParametres[classes.length] = this.getClass();
		} catch (Exception e) {}
		parametres = new Object[valeurs.length + 1];
		for (int i = 0; i < valeurs.length; i++) {
			parametres[i] = valeurs[i];
		}
		parametres[valeurs.length] = this;
	}		
}
