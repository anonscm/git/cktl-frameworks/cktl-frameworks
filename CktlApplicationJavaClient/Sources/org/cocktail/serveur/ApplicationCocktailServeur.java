/*
 * Copyright Consortium Cocktail
 *
 * Framework applicatif pour des applications WebObjects Java Client.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.serveur;
import java.util.TimeZone;

import org.cocktail.common.LogManager;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

public abstract class ApplicationCocktailServeur extends CktlWebApplication {
    private NSMutableDictionary appParametres;
	private RemoteLogManager logManager;
	
    public static void main(String argv[]) {
        WOApplication.main(argv, ApplicationCocktailServeur.class);
    }
	public ApplicationCocktailServeur() {
        super();
				
		TimeZone.setDefault( TimeZone.getTimeZone("GMT"));
		NSTimeZone.setDefaultTimeZone(NSTimeZone.getGMT());
		System.setProperty("java.awt.headless","true");
		// installer le receveur des invocations de mÈthodes distantes
		logManager = new RemoteLogManager();
		CktlConfig cfg = ((CktlWebApplication)WOApplication.application()).config();
		if (cfg != null) {
			String mode = cfg.stringForKey("MODE_CONSOLE");
			//mode = "N";
			String modeDebug = cfg.stringForKey("MODE_DEBUG");
			if (outputPath() != null) {
				LogManager.setServerPathOutputForWO(outputPath());
			}
			LogManager.preparerLog("LogMangueServeur",mode,modeDebug);
			LogManager.logInformation("Bienvenue dans " + this.name() + "!");
			LogManager.logInformation("OS : " + System.getProperty("os.name"));
			LogManager.logInformation("Directory Impression : " + System.getProperty("DIRECTORY_IMPRESSION"));
			LogManager.logInformation("Mode Console " + mode + ", Mode Debug " + modeDebug);
			LogManager.logDetail("Default Time Zone " + TimeZone.getDefault());
			LogManager.logDetail("Default NSTime Zone " + NSTimeZone.defaultTimeZone());
		}
	}
	public String getParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}
	// méthodes privées
	private String parametresTableName() {
		return null;
	}
	/**
		*
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			java.util.Enumeration enumerator = vParam.objectEnumerator();
			while (enumerator.hasMoreElements()) {
				vTmpRec = (EOGenericRecord) enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
					|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					if (a != null) {
						a.addObject((String) vTmpRec.valueForKey("paramValue"));
					}
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}

}	