--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  RECHERCHE, GRHUM
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.1.0
-- Date de publication :  01/04/2012
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- 
----------------------------------------------

grant insert on CKTL_GED.TYPE_DOCUMENT to recherche;
grant select on CKTL_GED.TYPE_DOCUMENT_SEQ to recherche;
grant select on GRHUM.personnel_ulr to recherche;
grant select on mangue.individu_diplomes to recherche;
GRANT SELECT ON GRHUM.GD_APPLICATION TO RECHERCHE;
GRANT SELECT ON GRHUM.GD_APPLICATION_SEQ TO RECHERCHE;
GRANT SELECT ON GRHUM.GD_DOMAINE_seq TO RECHERCHE;
GRANT Select ON GRHUM.GD_DOMAINE TO RECHERCHE;
GRANT SELECT ON GRHUM.GD_FONCTION_seq TO RECHERCHE;
GRANT INSERT ON GRHUM.GD_DOMAINE TO RECHERCHE;
GRANT INSERT ON GRHUM.GD_APPLICATION TO RECHERCHE;
GRANT INSERT ON GRHUM.GD_FONCTION TO RECHERCHE;
GRANT SELECT ON GRHUM.COMPTE TO RECHERCHE;
GRANT SELECT ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;
GRANT INSERT ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;
GRANT SELECT ON GRHUM.GRHUM_PARAMETRES_SEQ TO RECHERCHE;
GRANT SELECT ON GRHUM.GRHUM_PARAMETRES_TYPE TO RECHERCHE;


create or replace procedure RECHERCHE.INIT_1_1_0_PHYSALIS is
ass_id_doc integer;
tas_id_pere integer;
nb integer;
le_dom_id integer;
nb_ass_doct integer;
nb_assres_doct integer;
pers_id_createur integer;
type_id_parametre_texte integer;
type_id_parametre_numerique integer;
type_id_parametre_c_structure integer;

begin

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM GRHUM.COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');


-- TABLE DOCTORANT_STATUT 
Insert into RECHERCHE.DOCTORANT_STATUT (ID_STATUT,LIB_STATUT,CODE_STATUT) values (1,'Contractuel service public Etat Français','CONTRACT');
Insert into RECHERCHE.DOCTORANT_STATUT (ID_STATUT,LIB_STATUT,CODE_STATUT) values (2,'Fonctionnaire Etat Français','FONC');
Insert into RECHERCHE.DOCTORANT_STATUT (ID_STATUT,LIB_STATUT,CODE_STATUT) values (3,'Salarié privé','SALARIE');
Insert into RECHERCHE.DOCTORANT_STATUT (ID_STATUT,LIB_STATUT,CODE_STATUT) values (4,'Autre','AUTRE');

-- TABLE DOCTORANT_ORIGINE
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (1,'Equivalence bac+5','A');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (2,'Diplôme d''ingénieur (universitaire ou non)','E');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (3,'Autre diplôme universitaire de 2ème cycle (hors diplôme d''ingénieur universitaire)','R');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (4,'Diplôme de 3ème cycle : DEA, DESS, MASTER LMD, docteur en médecine, (hors Diplôme d''ingénieur)','U');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (5,'Diplôme de fin de 2ème cycle des études médicales et pharmaceutiques','V');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (6,'Diplôme visé école de management','W');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (7,'Diplôme d''établissement étranger supérieur','X');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (8,'Autre Diplôme supérieur','Y');
Insert into RECHERCHE.DOCTORANT_ORIGINE (ID_ORIGINE,LIB_ORIGINE,CODE_ORIGINE) values (9,'Aucun Diplôme supérieur','Z');

-- TABLE THESE_DOMAINE
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (36,'Sciences juridiques',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (37,'Sciences politiques',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (61,'Pluri droit - Sciences politiques',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (38,'Sciences économiques',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (39,'Sciences de gestion',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (62,'Pluri sciences économiques et gestion',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (40,'Administration économique et sociale (AES)',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (17,'Sciences du langage - Linguistique',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (18,'Langues et littératures anciennes',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (19,'Langues et littératures françaises',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (20,'Littérature générale et comparée',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (21,'Arts',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (64,'Pluri lettres - Sciences du langage - Arts',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (22,'Français, langue étrangère',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (23,'Langues Littératures Civilisations étrangères',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (24,'Langues étrangères appliquées',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (25,'Cultures et langues régionales',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (65,'Pluri langues',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (26,'Philosophie - épistemologie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (27,'Histoire',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (28,'Géographie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (29,'Aménagement',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (30,'Archéologie - Ethnologie - Préhistoire',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (31,'Sciences religieuses',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (32,'Psychologie - Sciences cognitives',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (33,'Sociologie - Démographie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (34,'Sciences de l''éducation',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (35,'Science de l''information et la communication',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (66,'Pluri sciences Humaines et sociales',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (67,'Pluri lettres - langues - SC humaines',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (1,'Mathématiques',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (2,'Physique',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (3,'Chimie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (43,'Physique Chimie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (42,'Mathématique et Informatique',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (4,'Mathématiques appliquées aux sciences sociales',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (11,'Mécanique, Génie mécanique, Ingénérie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (12,'Génie civil - Aménagement',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (13,'Génie des procédés - Matériaux',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (14,'Informatique',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (15,'Electronique - Génie électrique - EEA',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (16,'Sciences et technologies industrielles',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (41,'Formation générale aux métiers de l''ingénieur',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (68,'Pluri sciences fondamentales et applications',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (5,'Sciences de l''univers, de la terre, de l''espace',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (6,'Sciences de la vie, biologie, santé',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (69,'Pluri sciences de la vie, de la santé, de la terre',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (70,'Pluri sciences',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (10,'STAPS',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (7,'Médecine',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (8,'Odontologie',null,'O');
Insert into RECHERCHE.THESE_DOMAINE (ID_THESE_DOMAINE,LIBELLE_DOMAINE,OPTION_DOMAINE,TEM_VALIDE) values (9,'Pharmacie',null,'O');


-- Les mentions de réussite à la soutenance

Insert into RECHERCHE.THESE_MENTION (ID_MENTION,LIB_MENTION,TEM_SUCCES) values (RECHERCHE.THESE_MENTION_SEQ.NEXTVAL,'Trés Honorable avec les félicitations du jury','O');
Insert into RECHERCHE.THESE_MENTION (ID_MENTION,LIB_MENTION,TEM_SUCCES) values (RECHERCHE.THESE_MENTION_SEQ.NEXTVAL,'Trés Honorable','O');
Insert into RECHERCHE.THESE_MENTION (ID_MENTION,LIB_MENTION,TEM_SUCCES) values (RECHERCHE.THESE_MENTION_SEQ.NEXTVAL,'Honorable','O');
Insert into RECHERCHE.THESE_MENTION (ID_MENTION,LIB_MENTION,TEM_SUCCES) values (RECHERCHE.THESE_MENTION_SEQ.NEXTVAL,'Sans','O');

-- les types de contrats pour le financement
Insert into RECHERCHE.THESE_TYPE_CONTRAT (ID_TTC,LIB_TYPE_CONTRAT,TEM_VALIDE) values (RECHERCHE.THESE_TYPE_CONTRAT_SEQ.NEXTVAL,'Contrat doctoral','O');
Insert into RECHERCHE.THESE_TYPE_CONTRAT (ID_TTC,LIB_TYPE_CONTRAT,TEM_VALIDE) values (RECHERCHE.THESE_TYPE_CONTRAT_SEQ.NEXTVAL,'Cifre','O');
Insert into RECHERCHE.THESE_TYPE_CONTRAT (ID_TTC,LIB_TYPE_CONTRAT,TEM_VALIDE) values (RECHERCHE.THESE_TYPE_CONTRAT_SEQ.NEXTVAL,'CDD','O');
Insert into RECHERCHE.THESE_TYPE_CONTRAT (ID_TTC,LIB_TYPE_CONTRAT,TEM_VALIDE) values (RECHERCHE.THESE_TYPE_CONTRAT_SEQ.NEXTVAL,'CDI','O');
Insert into RECHERCHE.THESE_TYPE_CONTRAT (ID_TTC,LIB_TYPE_CONTRAT,TEM_VALIDE) values (RECHERCHE.THESE_TYPE_CONTRAT_SEQ.NEXTVAL,'Bourse','O');

-- Les types de financement des theses (siredo)
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Non Renseigné','-');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Salarié','N');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Personnel','N');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Contrat doctoral MESR','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Convention CIFRE','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Organisme de recherche','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Ecole','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Autre ministère','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Collectivité locale ou territoriale','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Associations ou fondations','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Entreprise (hors CIFRE) ','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Crédits ANR','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Contrat de recherche','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Pour étrangers (bourses du gouvernement français ou étranger) ','O');
Insert into RECHERCHE.TYPE_FINANCEMENT (ID_TYPE_FINANCEMENT,LIBELLE_TYPE_FINANCEMENT,TYPE_FINANCEMENT) values (RECHERCHE.TYPE_FINANCEMENT_SEQ.NEXTVAL,'Autres financement','O');

-- données pour la GED , différents type de documents utilisés par physalis
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES (CKTL_GED.TYPE_DOCUMENT_SEQ.NEXTVAL, 'org.cocktail.ged.typedocument.recherche.doctorant.piece', 'Piece administrative');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES (CKTL_GED.TYPE_DOCUMENT_SEQ.NEXTVAL, 'org.cocktail.ged.typedocument.recherche.doctorant.lettre', 'Lettre');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES (CKTL_GED.TYPE_DOCUMENT_SEQ.NEXTVAL, 'org.cocktail.ged.typedocument.recherche.doctorant.diplome', 'Diplome');
INSERT INTO "CKTL_GED"."TYPE_DOCUMENT" (TD_ID, TD_STR_ID, TD_LIBELLE) VALUES (CKTL_GED.TYPE_DOCUMENT_SEQ.NEXTVAL, 'org.cocktail.ged.typedocument.recherche.doctorant.cv', 'CV');

-- recuperation de l'identifiant d'un parametre de type texte
SELECT type_id INTO type_id_parametre_texte FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'FREE_TEXT';

-- recuperation de l'identifiant d'un parametre de type numerique
-- SELECT type_id INTO type_id_parametre_numerique FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'VALEUR_NUMERIQUE';

-- recuperation de l'identifiant d'un parametre de type c_structure
SELECT type_id INTO type_id_parametre_c_structure FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'C_STRUCTURE';

-- donnees table GRHUM.GRHUM_PARAMETRES à configurer
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.ETABLISSEMENT','Association Cocktail','Nom de l''établissement tel qu''il apparaitra dans les édtions', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.INTITULE_MINISTERE','MINISTERE DE L''ENSEIGNEMENT SUPERIEUR ET DE LA RECHERCHE','intitulé exact du ministère : apparaitera sur le diplome de doctorat', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.RECTORAT','Bordeaux','le centre administratif du rectorat de la region', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.QUALITE_CHEF_ETABLISSEMENT','Le Grand Manitou','qualité du chef de l''établissement qui change souvent', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.NOM_CHEF_ETABLISSEMENT','O. KENOBI','nom du chef de l''établissement', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.ETAB_DIPLOME_DEFAUT','0640251A','Code SISE (c_etab) de l''établissement qui délivre le diplome par défaut. valeur non obligatoire au fonctionnement', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.FGRA_CODE_DOCTORAT','D','a récuperer de scolarite.scol_formation_grade, permet de limiter la recherche des etudiants a ceux inscrits dans ce grade...', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_c_structure);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.VILLE_ETABLISSEMENT','Cardesse','Commune de l''établissement : utile pour certaines éditions', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.NOM_COMPLET_CHEF_ETABLISSEMENT','Obiwan KENOBI','Nom complet qui apparait sur les conventions aux membres du jury', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_texte);
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE', '33079', 'c_structure de l''établissement gestionnaire des theses', pers_id_createur, pers_id_createur,
sysdate, sysdate, type_id_parametre_c_structure);

	
-- Initialisation du nombre de theses autorisées par enseignant par ED 
insert into grhum.grhum_parametres 
	select grhum.grhum_parametres_seq.NEXTVAL, 'org.cocktail.recherche.physalis.nbThesesMaximum.' || s.c_structure, 4 , 'nombre maximum de these encadrées par un directeur de l''ED : '|| s.lc_structure, 
	pers_id_createur, pers_id_createur, sysdate, sysdate, type_id_parametre_texte
	from grhum.structure_ulr s, GRHUM.repart_type_groupe rtg
	where s.c_structure = rtg.c_structure
	and rtg.tgrp_code = 'ED';
	
END;

/

create or replace function mangue.get_c_corps_agent(noindividu INTEGER)
  return VARCHAR2
/*
  Fonction qui renvoie le code corps (contractuel ou titulaire) d'un agent ou null si inexistant
  Auteur : CRI UPPA
  creation : 26/04/2012
*/
IS
  cGrade        GRHUM.GRADE.C_GRADE%TYPE;
  cCorps        GRHUM.CORPS.C_CORPS%TYPE;
BEGIN
  select mangue.get_c_grade_agent(noindividu) into cGrade from dual;
  if cGrade is not null
  then
    select c_corps into cCorps from grhum.grade where c_grade = cGrade;
  end if;
  RETURN cCorps;
END;

/

grant execute on mangue.get_c_corps_agent to recherche;


create or replace
procedure RECHERCHE.INIT_1_1_0_DTH as

  cursor lesDths is 
    select distinct i.no_individu, i.pers_id, i.nom_usuel || ' ' || i.prenom
    from grhum.individu_ulr i, mangue.individu_diplomes idip, GRHUM.personnel_ulr p
    where i.no_individu = idip.no_individu
    and i.no_individu = p.no_dossier_pers
    and idip.tem_valide = 'O'
    and i.tem_valide = 'O'
    and idip.c_diplome in ('0731000','1000000')
    and mangue.get_c_corps_agent(i.no_individu) not in ('300','H04','DIRC')
    union all
    select distinct i.no_individu, i.pers_id,i.nom_usuel || ' ' || i.prenom
    from grhum.individu_ulr i, GRHUM.personnel_ulr p
    where i.no_individu = p.no_dossier_pers
    and i.tem_valide = 'O'
    and mangue.get_c_corps_agent(i.no_individu) in ('300','H04','DIRC');
  
  noIndividuDth integer;
  persIdDth integer;
  dateDiplome date;
  laboDth varchar2(10);
  edDth varchar2(10);
  nomPrenom varchar2(200);
  nb integer;
  noIndividuCreateur integer;
  etablissement varchar2(10);

begin
    -- recuperation no_individu d'un grhum_createur
    select i.no_individu into noIndividuCreateur from (select param_value from (
    select * from grhum.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
    where rownum=1) x, grhum.compte c, grhum.individu_ulr i where c.pers_id  = i.pers_id and x.param_value=c.cpt_login;
    
    select c_rne into etablissement
    from grhum.structure_ulr
    where tem_valide = 'O'
    and c_type_structure = 'E';
    

  open lesDths;
  loop
    fetch lesDths into noIndividuDth,persIdDth,nomPrenom;
    exit when lesDths%NOTFOUND;
    
    select count(*) into nb
    from grhum.repart_association ra, grhum.association a,grhum.structure_ulr s, grhum.repart_type_groupe rtg
    where ra.ass_id = a.ass_id
    and ra.c_structure = s.c_structure
    and s.c_structure = rtg.c_structure
    and s.tem_valide = 'O'
    and rtg.tgrp_code = 'LA' --in ('LA','SR','ED')
    and ra.ras_d_ouverture <= sysdate and (ra.ras_d_fermeture >= sysdate or ra.ras_d_fermeture is null)
    and a.ass_id in (325,326)
    and ra.pers_id = persIdDth;
    
    if (nb = 0) 
    then
      dbms_output.put_line ('Problème pour ' || nomPrenom || ' aucune labo trouvé (no_individu : '|| noIndividuDth || ', pers_id : ' || persIdDth);
    else
      select c_structure into laboDth 
      from (
      select s.c_structure
      from grhum.repart_association ra, grhum.association a,grhum.structure_ulr s, grhum.repart_type_groupe rtg
      where ra.ass_id = a.ass_id
      and ra.c_structure = s.c_structure
      and s.c_structure = rtg.c_structure
      and s.tem_valide = 'O'
      and rtg.tgrp_code = 'LA'--in ('LA','SR','ED')
      and ra.ras_d_ouverture <= sysdate and (ra.ras_d_fermeture >= sysdate or ra.ras_d_fermeture is null)
      and a.ass_id in (325,326)
      and ra.pers_id = persIdDth)
      where rownum = 1;
      
      select count(*) into nb
      from grhum.repart_structure rs, grhum.structure_ulr ed, grhum.structure_ulr labo, grhum.repart_type_groupe rtg
      where rs.pers_id = labo.pers_id 
      and rs.c_structure = ed.c_structure
      and ed.c_structure = rtg.c_structure
      and rtg.tgrp_code = 'ED'
      and labo.c_structure = laboDth;

      if (nb = 0)
      then 
        dbms_output.put_line ('Problème pour ' || nomPrenom || ' aucune école doctorale trouvée (no_individu : '|| noIndividuDth || ', pers_id : ' || persIdDth || ', Labo : ' ||laboDth);
      else 
        select ed.c_structure into edDth
        from grhum.repart_structure rs, grhum.structure_ulr ed, grhum.structure_ulr labo, grhum.repart_type_groupe rtg
        where rs.pers_id = labo.pers_id 
        and rs.c_structure = ed.c_structure
        and ed.c_structure = rtg.c_structure
        and rtg.tgrp_code = 'ED'
        and labo.c_structure = laboDth;
        
        select count(*) into nb
        from (
              select *
              from mangue.individu_diplomes
              where tem_valide = 'O'
              and c_diplome in ('0731000','1000000')
              and no_individu = noIndividuDth
              order by c_diplome
              )
        where rownum = 1;

        if (nb = 1) 
        then 
              select d_diplome into dateDiplome
              from (
                  select *
                from mangue.individu_diplomes
                where tem_valide = 'O'
                and c_diplome in ('0731000','1000000')
                and no_individu = noIndividuDth
                    order by c_diplome
             )
          where rownum = 1;
        else dateDiplome := null;
        end if;
		
        --dbms_output.put_line('insert into recherche.directeur_these_habilite(ID_DTH,NO_INDIVIDU,NO_INDIVIDU_UTILISATEUR,DATE_HDR,TEM_VALIDE,c_structure_ed,C_STRUCTURE_LABO,LABO_EXTERNE_DTH,C_RNE,C_STRUCTURE_ETAB)
        --values (RECHERCHE.DIRECTEUR_THESE_HABILITE_SEQ.NEXTVAL,'||noIndividuDth||','||noIndividuCreateur||',to_date('''||to_char(dateDiplome,'dd/mm/yyyy')||''',''dd/mm/yyyy''),''O'','||edDth||','||laboDth||',null,'||etablissement||',null);');
        insert into recherche.directeur_these_habilite(ID_DTH,NO_INDIVIDU,NO_INDIVIDU_UTILISATEUR,DATE_HDR,TEM_VALIDE,c_structure_ed,C_STRUCTURE_LABO,LABO_EXTERNE_DTH,C_RNE,C_STRUCTURE_ETAB)
        values (RECHERCHE.DIRECTEUR_THESE_HABILITE_SEQ.NEXTVAL,noIndividuDth,noIndividuCreateur,dateDiplome,'O',edDth,laboDth,null,etablissement,null);

      end if;
    end if;
    

  end loop;
  close lesDths;  

end;
/

CREATE OR REPLACE PROCEDURE RECHERCHE.INIT_1_1_0_DROITS IS

domaine_id integer;
physalis_app_id integer;

BEGIN

-- recuperation du dom_id du domaine RECHERCHE
SELECT dom_id into domaine_id FROM grhum.GD_DOMAINE WHERE DOM_LC LIKE 'RECHERCHE';

-- insertion de l'application Physalis
INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) 
VALUES (GRHUM.GD_APPLICATION_seq.nextval, domaine_id, 'PHYSALIS', 'PHYSALIS');

-- recuperation de l'identifiant de l'application Physalis
SELECT app_id INTO physalis_app_id FROM GRHUM.GD_APPLICATION WHERE app_str_id LIKE 'PHYSALIS';

-- insertion des differents droits
INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','PHYSALIS','Acces a Physalis','Acces a Physalis');

INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','AJOUT_ETUDIANT','Acces a l''ajout d''un etudiant','Acces a l''ajout d''un etudiant');

INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','RECHERCHER_DOCTORANT','Acces a la recherche de doctorant','Acces a la recherche de doctorant');

INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','DIR_THESE_HABILITES','Acces aux directeurs de these habilites','Acces aux directeurs de these habilites');

INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','GESTION_PARAMETRES','Acces a la gestion des parametres','Acces a la gestion des parametres');

INSERT INTO grhum.GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (grhum.GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','GESTION_PARAM_EC', 'Acces a la gestion des parametres des ecoles doct', 'Acces a la gestion des parametres des ecoles doct');


END;
/



create or replace
procedure RECHERCHE.INIT_1_1_0 as
BEGIN
 	RECHERCHE.INIT_1_1_0_PHYSALIS();
 	RECHERCHE.INIT_1_1_0_DTH();
	RECHERCHE.INIT_1_1_0_DROITS();
  INSERT INTO RECHERCHE.DB_VERSION
  (
    DBV_ID,
    DBV_LIBELLE,
    DBV_DATE,
    DBV_INSTALL,
    DBV_COMMENT
  )
  VALUES
  (
    RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
    '1.1.0',
    to_date('2012/09/14', 'yyyy/mm/dd'),
    sysdate,
    'Patch pour Physalis'
  );
END;
/

execute RECHERCHE.INIT_1_1_0();
/


COMMIT;


DROP PROCEDURE RECHERCHE.INIT_1_1_0_DROITS;
drop procedure RECHERCHE.INIT_1_1_0_PHYSALIS;
drop procedure RECHERCHE.INIT_1_1_0_DTH;
drop procedure RECHERCHE.INIT_1_1_0;


revoke insert on CKTL_GED.TYPE_DOCUMENT from recherche;
revoke select on CKTL_GED.TYPE_DOCUMENT_SEQ from recherche;
revoke execute on mangue.get_c_corps_agent from recherche;
revoke select on GRHUM.personnel_ulr from recherche;
revoke select on mangue.individu_diplomes from recherche;
revoke SELECT ON GRHUM.GD_APPLICATION from RECHERCHE;
revoke SELECT ON GRHUM.GD_APPLICATION_SEQ from RECHERCHE;
revoke SELECT ON GRHUM.GD_DOMAINE_seq from RECHERCHE;
revoke Select ON GRHUM.GD_DOMAINE from RECHERCHE;
revoke SELECT ON GRHUM.GD_FONCTION_seq from RECHERCHE;
revoke INSERT ON GRHUM.GD_DOMAINE from RECHERCHE;
revoke INSERT ON GRHUM.GD_APPLICATION from RECHERCHE;
revoke INSERT ON GRHUM.GD_FONCTION from RECHERCHE;
