SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/2
-- Type: DDL
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.2
-- Date de publication :  15/05/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

ALTER TABLE RECHERCHE.DIRECTEUR_THESE ADD (DATE_AUTORISATION DATE );
COMMENT ON COLUMN RECHERCHE.DIRECTEUR_THESE.DATE_AUTORISATION IS 'Date d''autorisation du conseil scientifique pour diriger une these';
