
BEGIN


-- SUIVI ADMINISTRATIF DES CONTRATS

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat',
  LIBELLE => 'Suivi administratif des contrats de recherche',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'nego', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Négociation', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'standby', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Stand-by', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'cont_ab', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Contrat abandonné', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'cont_val', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Contrat validé', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'signat', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Signature', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat', 
  CODE => 'archiv', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Archivé', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


-- TYPES REDACTION CONTRATS

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactioncontrat',
  LIBELLE => 'Type de redaction des contrats',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactioncontrat', 
  CODE => 'daj', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'DAJ', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactioncontrat', 
  CODE => 'drv', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'DRV', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactioncontrat', 
  CODE => 'partenaire', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Partenaire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- TYPES REDACTION CLAUSES PI

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactionclausespi',
  LIBELLE => 'Type de redaction des des clauses de PI',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactionclausespi', 
  CODE => 'daj', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'DAJ', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactionclausespi', 
  CODE => 'drv', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'DRV', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeredactionclausespi', 
  CODE => 'partenaire', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Partenaire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


-- TYPES APPORTS DRV SUR CONTRAT

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat',
  LIBELLE => 'Type des apports DRV sur contrat',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'prot_sf', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Protection du savoir-faire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'cl_exp_re', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Clause d''exploitation ré-équilibrée', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'augm_mf', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Augmentation du montant financier', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'cl_pub_av', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Clauses publications plus avantageuses', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'pi_reeq', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'PI ré-équilibrée', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'mep_parlt', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Mise en place d''un partenariat à long terme (AC, GIS, Réseau…)', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat', 
  CODE => 'mep_cad_cont', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Mise en place d''un cadre contractuel', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


-- TYPES APPORTS DAJ SUR CONTRAT
RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat',
  LIBELLE => 'Type des apports DAJ sur contrat',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat', 
  CODE => 'conv_ame', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Convention améliorée/rééquilibrée', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat', 
  CODE => 'limit_et', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Limitation de l''étendue de la responsabilité de l''établissement', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat', 
  CODE => 'conv_ad_a', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Convention adressée pour avis', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat', 
  CODE => 'conv_ad_ar', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Convention adressée pour avis et rédaction', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat', 
  CODE => 'conv_ad_arn', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Convention adressée pour avis rédaction et négociation', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- TYPES PROPRIETE DES RESULTATS
RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats',
  LIBELLE => 'Type de propriété des résultats',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'par_dom', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'PI par domaine', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'pi_et', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'PI Etablissements', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'pi_part', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'PI Partenaire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'coprop', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Copropriété', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'pi_par_tr', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'PI par type de résultats', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats', 
  CODE => 'sans_res', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Sans résultat', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- TYPES EXPLOITATION DES RESULTATS

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats',
  LIBELLE => 'Types exploitation des résultats',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats', 
  CODE => 'exp_et', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Exploitation Etablissements', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats', 
  CODE => 'exp_part', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Exploitation au partenaire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats', 
  CODE => 'par_dom', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Par domaine', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats', 
  CODE => 'ss_exp', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Sans exploitation', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


-- TYPES GESTION DES RESULTATS
RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats',
  LIBELLE => 'Types de gestion des résultats',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'don_num', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Données numériques', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'logiciel', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Logiciel', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'rapport', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Rapport', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'sav_fai', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Savoir-Faire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'proto', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Prototype', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typegestionresultats', 
  CODE => 'proc', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Procédé/Méthode', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- TYPES SUIVI VALO

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typesuivivalorisation',
  LIBELLE => 'Types de suivi valorisation',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typesuivivalorisation', 
  CODE => 'a_suivre', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Valorisation à suivre', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typesuivivalorisation', 
  CODE => 'aucun_enj', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Aucun enjeu Valo', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.typesuivivalorisation', 
  CODE => 'prio_elev', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Priorité élevée sur la valorisation', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements',
  LIBELLE => 'Types de financement des AAP',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'alloc_these', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Allocation de thèse', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'alloc_pd', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Allocation post-doc', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'inv', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Investissement', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'fonc', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Fonctionnement', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'colloque', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Colloque', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typefinancements', 
  CODE => 'autre', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Autre', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- MOYENS DE DEPOT DU DOSSIER

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyendepotdossier',
  LIBELLE => 'Moyens de depot de dossier aap',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyendepotdossier', 
  CODE => 'envoi_mail', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Envoi par mail', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyendepotdossier', 
  CODE => 'visite_chercheur', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Visite du chercheur', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;
RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyendepotdossier', 
  CODE => 'autre', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Autre', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;



RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus',
  LIBELLE => 'Etat de refus/acceptation',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus', 
  CODE => 'pa', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Accepté', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus', 
  CODE => 'pr', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Refusé', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus', 
  CODE => 'enc', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'En cours', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus', 
  CODE => 'ab', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Abandon', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.etatacceptationrefus', 
  CODE => 'lc', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Liste complémentaire', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- TYPES SUIVI VALORISATION

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.typesuivivalorisation',
  LIBELLE => 'Types de suivi valorisation',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);


RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typesuivivalorisation', 
  CODE => 'a_suivre', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Valorisation à suivre', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typesuivivalorisation', 
  CODE => 'aucun_enj', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Aucun enjeu Valo', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typesuivivalorisation', 
  CODE => 'prio_elev', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Priorité élevée sur la valorisation', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


-- MOYENS ENVOI DOSSIERS

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyenenvoidossiers',
  LIBELLE => 'Moyens d''envoi des dossiers',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyenenvoidossiers', 
  CODE => 'de', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Dossier électronique', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyenenvoidossiers', 
  CODE => 'dp', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Dossier papier', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.moyenenvoidossiers', 
  CODE => 'ds', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Saisie site ouebe', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

--TYPES ETATS MONTAGE

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.typeetatmontage',
  LIBELLE => 'Types d''états de montage',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typeetatmontage', 
  CODE => 'encours', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'En cours', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typeetatmontage', 
  CODE => 'abandon', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Abandon', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typeetatmontage', 
  CODE => 'label', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Labellisé', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.typeetatmontage', 
  CODE => 'non_labels', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Non labellisé', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- LISTE DES AVIS AAP

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.aap.listeavis',
  LIBELLE => 'Liste des avis aap',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'DICO', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 1, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.listeavis', 
  CODE => 'f', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Favorable', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.listeavis', 
  CODE => 'd', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'Défavorable', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.aap.listeavis', 
  CODE => 'r', 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'A reformuler', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

-- LANGUES DE CONTRATS

RECHERCHE.INS_NOMENCLATURE(
  STR_ID => 'org.cocktail.fwkcktlsangria.contrats.langues',
  LIBELLE => 'Sélection de langues pour les contrats',
  DESCRIPTION => null, 
  TYPE_NOMENCLATURE => 'LISTE', 
  TYPE_VALEURS => 'String', 
  VALID => 1, 
  MODIFIABLE => 0, 
  COMPLETABLE => 1
);

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.langues', 
  CODE => null, 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'fr', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;

RECHERCHE.INS_NOMENCLATURE_OBJET(
  NOMENCLATURE_STR_ID => 'org.cocktail.fwkcktlsangria.contrats.langues', 
  CODE => null, 
  LIBELLE_COURT => null, 
  LIBELLE_LONG => 'en', 
  VALEUR => null, 
  DATE_DEBUT_VALIDITE => TO_DATE('01/01/2000', 'DD/MM/YYYY'), 
  DATE_FIN_VALIDITE => null, 
  VISIBLE => 1, 
  PERSONNALISEE => 0, 
  ORIGINE => 'R'
) ;


END;
/

commit;