SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°2/2
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.3
-- Date de publication :  24/06/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--

declare

pers_id_createur integer;
type_id_parametre_texte integer;
type_id_parametre_url integer;
type_id_parametre_date integer;
tas_id_pere integer;

begin

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM GRHUM.COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');

-- recuperation de l'identifiant d'un parametre de type texte
SELECT type_id INTO type_id_parametre_texte FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'FREE_TEXT';

-- recuperation de l'identifiant d'un parametre de type Date
SELECT type_id INTO type_id_parametre_date FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'DATE';

-- recuperation de l'identifiant d'un parametre de type URL
SELECT type_id INTO type_id_parametre_url FROM Grhum.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'URL';

-- donnees table GRHUM.GRHUM_PARAMETRES à configurer
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.COORDONNEES_ETABLISSEMENT','ASSOCIATION COCKTAIL
37, rue GUIBAL
13003 - MARSEILLE ','Nom et adresse de l''établissement tel qu''il apparaitra dans les éditions', pers_id_createur, pers_id_createur, sysdate, sysdate, type_id_parametre_texte);

INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.DATE_LIMITE_INSCRIPTION_DOCTORAT','31/10/2013',
'Date limite d''inscription du doctorant qui est utilisee dans les éditions', pers_id_createur, pers_id_createur, sysdate, sysdate, type_id_parametre_date);

INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.LOGO_ETABLISSEMENT','http://www.asso-cocktail.fr/sites/all/themes/Classique/images/header-object.png',
'Logo de l''etablissement tel qu''il apparaitra dans les éditions', pers_id_createur, pers_id_createur, sysdate, sysdate, type_id_parametre_url);

-- retours à la lignes prévus pour laisser de la place pour une signature
INSERT INTO GRHUM.GRHUM_PARAMETRES(PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_CREATION, D_MODIFICATION, PARAM_TYPE_ID)
VALUES (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.physalis.SIGNATURE','Le Président et,
Par délégation, le Vice-Président du conseil scientifique







M. Vice-President', 'Signature des éditions de document', pers_id_createur, pers_id_createur, sysdate, sysdate, type_id_parametre_texte);

-- recuperation du type d'association "role"
select tas_id into tas_id_pere from grhum.type_association where tas_code = 'ROLE';

-- Pour la cotutelle, une association pour la présence d'un doctorant dans chaque etablissement
INSERT INTO GRHUM.ASSOCIATION (ASS_ID,ASS_LIBELLE,D_CREATION,D_MODIFICATION,TAS_ID,ASS_RACINE,ASS_CODE) 
VALUES (ASSOCIATION_SEQ.NEXTVAL,'ETABLISSEMENT D''ACCUEIL',SYSDATE,SYSDATE,tas_id_pere,NULL,'D_ETAB_ACC');

INSERT INTO RECHERCHE.DB_VERSION
(
  DBV_ID,
  DBV_LIBELLE,
  DBV_DATE,
  DBV_INSTALL,
  DBV_COMMENT
)
VALUES
(
  RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
  '1.3.3',
  to_date('2013/06/24', 'yyyy/mm/dd'),
  sysdate,
  'Ajout de parametres pour Physalis et d''un role pour les etablissements d''accueil'
);

END;
/

commit;
