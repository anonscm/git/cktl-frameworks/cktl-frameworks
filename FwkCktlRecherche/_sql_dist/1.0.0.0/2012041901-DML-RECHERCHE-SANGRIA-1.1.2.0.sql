create or replace procedure RECHERCHE.CHARGER_PROGRAMMES 
as
	pers_id_anr integer;
	pers_id_ue integer;
	c_struct_anr integer;
	c_struct_ue integer;
begin

	pers_id_anr := 161569;
	pers_id_ue := 161570;
	select c_structure into c_struct_anr from GRHUM.structure_ulr where pers_id = pers_id_anr;
	select c_structure into c_struct_ue from GRHUM.structure_ulr where pers_id = pers_id_ue;

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001', 'Biologie - santé', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.001', 'Appel à Projets Transnational dans le cadre de l''ERA-NET EMIDA', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.002', 'Appel à Projets Transnational dans le cadre de l''ERA-Net NEURON ""Maladies Mentales""', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.003', 'Appel à Projets Transnational dans le cadre de l''ERA-NET PRIOMEDCHILD', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.004', 'Maladie d''Alzheimer et Maladies Apparentées (MALZ)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.005', 'Mécanismes intégrés de l''inflammation MI2', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.006', 'Recherche finalisée sur les cellules souches (RFCS)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.007', 'Recherche partenariale en biotechnologies pour la santé (BiotecS)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.008', 'Recherche partenariale en technologies pour la santé et l''autonomie (TecSan)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.001.009', 'Troisième Appel à Projets Transnational dans le cadre de l''ERA-NET PathoGenoMics', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.002', 'Ecosystèmes et Développement Durable', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.002.001', 'Programme de recherche Alimentation et Industries Alimentaires (ALIA)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.002.002', 'Programme Ecosystèmes, territoires, ressources vivantes et agricultures - SYSTERRA', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.002.003', 'Programme Génomique, Biotechnologies végétales', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003', 'Energie durable et environnement', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.001', 'BIOENERGIES 2010', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.002', 'Efficacité énergétique et réduction des émissions de CO2 dans les systèmes industriels', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.003', 'Habitat intelligent et solaire photovoltaïque', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.004', 'Programme Hydrogène et Piles à Combustible (H-PAC)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.005', 'Programme Production Durable et Technologies de l''Environnement (ECOTECH)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.006', 'Programme Stockage Innovant de l''Energie (Stock-E)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.007', 'Villes durables', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.003.008', 'Programme de Recherche et d''Innovation dans les Transports terrestres (PREDIT)', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.004', 'Ingénierie, Procédés et Sécurité', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.004.001', 'Chimie Durable ¿ Industries ¿ Innovation', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.004.002', 'Concepts Systèmes et Outils pour la Sécurité Globale', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.004.003', 'Programme Matériaux Fonctionnels et Procédés Innovants', c_struct_anr, 0, null);




RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.005', 'Programmes non-thématiques', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.005.001', 'Programme ""Chaires d''excellence""', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.005.002', 'Programme ""Retour Post-Doctorants"" édition 2010', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.005.003', 'Programme Blanc International Edition 2010', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.005.004', 'Jeune chercheurs / chercheuses', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.006', 'Programmes transversaux', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.006.001', 'Contaminants, Ecosystèmes, Santé (CES)', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007', 'Sciences et Technologies de l''information', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.001', 'Programme Conception et Simulation', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.002', 'Programme CONTENUS ET INTERACTIONS', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.003', 'Programme NANOTECHNOLOGIES ET NANOSYSTEMES (P2N)', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.004', 'Réseaux du Futur et Services VERSO', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.005', 'Systèmes Embarqués et Grandes Infrastructures', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.007.006', 'Programme CONTENUS ET INTERACTIONS Défi Multimédia (REPERE)', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008', 'Sciences Humaines et Sociales', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008.001', 'Appel à projets franco-allemand en sciences humaines et sociales', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008.002', 'Appel à projets ORA ""Open Research Area in Europe""', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008.003', 'ESPACE ET TERRITOIRE : Les énigmes spatiales de la vie en société', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008.004', 'La création : processus, acteurs, objets, contextes - Édition 2010', c_struct_anr, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('ANR.008.005', 'Les Suds, aujourd''hui II', c_struct_anr, 0, null);



RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001', 'COOPERATION', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.001', 'santé', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.002', 'alimentation, agriculture et pêche et biotechnologie', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.003', 'technologies de l''information et de la communication', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.004', 'nanosciences, nanotechnologies, matériaux et nouvelles technologies de production', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.005', 'énergie', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.006', 'environnement (changement climatique inclus)', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.007', 'transports (aéronautique comprise)', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.008', 'sciences socio-économiques et humaines', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.009', 'espace', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.001.010', 'sécurité', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.002', 'IDEES (ERC)', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.002.001', 'Bourse jeune chercheur', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.002.002', 'Bourse chercheur confirmé', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003', 'PERSONNES (Marie Curie)', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.001', 'ITN Initial Training Network', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.002', 'IEF Intra-European Fellowships', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.003', 'ERG European Reintegration Grants', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.004', 'IAAP Industry-Academia partnerships ', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.005', 'IRSES International Research Staff Exchange Scheme', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.006', 'IOF International Outgoing Fellowships', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.007', 'IIF International Incoming Fellowships', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.008', 'IRG International Reintegration Grants', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.003.009', 'NIGHT Researchers Night', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004', 'CAPACITES', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.001', 'Infrastructures de recherche', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.002', 'Recherche au profit des PME', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.003', 'Régions de la connaissance', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.004', 'Potentiel de recherche', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.005', 'La science dans la société', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.004.006', 'Activités spécifiques de coopération internationale', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.005', 'AUTRES Programmes', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.005.001', 'COST', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.005.002', 'EUREKA...', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.006', 'FEDER ', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.006.001', 'Recherche', c_struct_ue, 0, null);

RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.007', 'INTERREG', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.007.001', 'France ESpagne Andorre', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.007.002', 'SUDOE', c_struct_ue, 0, null);
RECHERCHE.INSERT_OR_UPDATE_PROGRAMME('UNE.007.003', 'Espace Atlantique', c_struct_ue, 0, null);



end;
/


execute RECHERCHE.CHARGER_PROGRAMMES;

