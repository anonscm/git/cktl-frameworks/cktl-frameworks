SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1
-- Type: DBA
-- Schéma:  RECHERCHE
-- Numéro de version :  1.0.0
-- Date de publication :  12/09/2012
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


/*
 Création du schéma RECHERCHE dédié la gestion de la Recherche 
*/


whenever sqlerror exit sql.sqlcode ;



CREATE USER "RECHERCHE" IDENTIFIED BY ctblPitch
DEFAULT TABLESPACE DATA_GRHUM
TEMPORARY TABLESPACE TEMP
QUOTA UNLIMITED ON DATA_GRHUM
QUOTA UNLIMITED ON INDX_GRHUM
;

GRANT CREATE SESSION TO RECHERCHE;


COMMIT;


PROMPT Le mot de passe du user est ctblPitch
PROMPT il est vivement recommandé de le changer
PROMPT Alter user RECHERCHE identified by "unvraimotdepasse";
