--
-- Patch DDL à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 1.1.1
-- Date de publication : 13/11/2012
-- Auteur(s) : Julien LAFOURCADE, Jean-Marc PUSSACQ
-- Licence : CeCILL version 2
--
--

ALTER TABLE ACCORDS.TRANCHE ADD (
	REPORT_N_MOINS_1 NUMBER(20,2) DEFAULT 0 NOT NULL,
	REPORT_N_PLUS_1 NUMBER(20,2) DEFAULT 0 NOT NULL
);