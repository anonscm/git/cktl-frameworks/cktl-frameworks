SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/2
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.1
-- Date de publication :  19/04/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


whenever sqlerror exit sql.sqlcode ;

DECLARE
	DS_COUNT NUMBER;
BEGIN
  SELECT MAX(DS_ORDRE) + 1 INTO DS_COUNT FROM GRHUM.DOMAINE_SCIENTIFIQUE;
  EXECUTE IMMEDIATE 'CREATE SEQUENCE GRHUM.DOMAINE_SCIENTIFIQUE_SEQ INCREMENT BY 1 START WITH ' || DS_COUNT || ' NOCACHE  NOORDER  NOCYCLE';
END;
/

UPDATE GRHUM.DOMAINE_SCIENTIFIQUE SET DS_VERSION = 1, DS_NOUVEAU_CODE = DS_CODE;


-- ST 

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_NOUVEAU_CODE, DS_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, 'ST', GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL, 'Sciences et technologies', 2, NULL, NULL);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_NOUVEAU_CODE, DS_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL,'ST1', GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL, 'Mathématiques', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 1);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'ST2', 'Physique', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 2);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'ST3', 'Sciences de la terre et de l''univers', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 3);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'ST4', 'Chimie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 4);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'ST5', 'Sciences pour l''ingénieur', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 8);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'ST6', 'Sciences et technologies de l''information et de la communication', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'ST'), 9);


-- SVE 

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE', 'Sciences du vivant et environnement', 2, NULL, NULL);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1', 'Biologie, santé', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS1', 'Biologie moléculaire et structurale, biochimie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS2', 'Génétique, génomique, bioinformatique', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS3', 'Biologie cellulaire, biologie du développement animal', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS4', 'Physiologie, physiopathologie, biologie systémique médicale', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS5', 'Neurobiologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS6', 'Immunologie, microbiologie, virologie, parasitologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE1_LS7', 'Epidémiologie, santé publique, recherche clinique, technologies biomédicales', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE1'), 5);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE2', 'Agronomie, écologie, environnement', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE'), 10);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE2_LS3', 'Biologie cellulaire et biologie du développement végétal', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE2'), 10);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE2_LS8', 'Evolution, écologie, biologie des populations', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE2'), 10);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SVE2_LS9', 'Biotechnologies, sciences environnementales, biologie synthétique, agronomie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SVE2'), 10);

-- SHS

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS', 'Sciences humaines et sociales', 2, NULL, NULL);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS1', 'Marchés et organisations', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS1_1', 'Economie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS1'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS1_2', 'Finance, management', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS1'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2', 'Normes, institutions et comportements sociaux', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), NULL);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2_1', 'Droit', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS2'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2_2', 'Science politique', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS2'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2_3', 'Anthropologie et ethnologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS2'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2_4', 'Sociologie, Démographie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS2'), 7);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS2_5', 'Sciences de l''information et de la communication', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS2'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS3', 'Espace, environnement et sociétés', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS3_1', 'Géographie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS3'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS3_2', 'Aménagement et urbanisme', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS3'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS3_3', 'Architecture', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS3'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS4', 'Esprit humain, langage, éducation', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS4_1', 'Linguistique', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS4'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS4_2', 'Psychologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS4'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS4_3', 'Sciences de l''éducation', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS4'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS4_4', 'Sciences et techniques des activités physiques et sportives', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS4'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS5', 'Langues, textes, arts et cultures', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS5_1', 'Langues / littératures anciennes françaises, littérature comparée', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS5'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS5_2', 'Littératures et langues étrangères, Civilisations, Cultures et langues régionales', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS5'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS5_3', 'Arts', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS5'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS5_4', 'Philosophie, sciences des religions, théologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS5'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS6', 'Mondes anciens et contemporains', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS6_1', 'Histoire', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS6'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS6_2', 'Histoire de l''art', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS6'), 6);

INSERT INTO GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE, DS_CODE, DS_NOUVEAU_CODE, DS_LIBELLE, DS_VERSION, DS_PARENT_ORDRE, DS_ANCIEN_ORDRE) 
  VALUES (GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.NEXTVAL, GRHUM.DOMAINE_SCIENTIFIQUE_SEQ.CURRVAL,'SHS6_3', 'Archéologie', 2, (SELECT DS_ORDRE FROM GRHUM.DOMAINE_SCIENTIFIQUE WHERE DS_NOUVEAU_CODE = 'SHS6'), 6);



INSERT INTO RECHERCHE.DB_VERSION
(
  DBV_ID,
  DBV_LIBELLE,
  DBV_DATE,
  DBV_INSTALL,
  DBV_COMMENT
)
VALUES
(
  RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
  '1.3.1',
  to_date('2013/04/19', 'yyyy/mm/dd'),
  sysdate,
  'Ajout de la nouvelle nomenclature des domaines scientifiques'
);