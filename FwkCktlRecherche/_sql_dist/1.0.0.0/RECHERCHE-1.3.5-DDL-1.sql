-- Ajout d'un type de contrat doctoral
CREATE TABLE RECHERCHE.TYPE_CONTRAT_DOCTORAL 
(
  ID_TYPE_CONTRAT_DOCTORAL NUMBER NOT NULL 
, LIBELLE_TYPE_CONTRAT_DOCTORAL VARCHAR2(100) 
, CONSTRAINT TYPE_CONTRAT_DOCTORAL_PK PRIMARY KEY 
	(ID_TYPE_CONTRAT_DOCTORAL) ENABLE 
);

COMMENT ON COLUMN RECHERCHE.TYPE_CONTRAT_DOCTORAL.ID_TYPE_CONTRAT_DOCTORAL IS 'Clé primaire de la table TYPE_CONTRAT_DOCTORAL';
COMMENT ON COLUMN RECHERCHE.TYPE_CONTRAT_DOCTORAL.LIBELLE_TYPE_CONTRAT_DOCTORAL IS 'Libellé des types de contrats doctoraux';

-- Ajout de la clé étrangere sur la table Doctorant financement
ALTER TABLE RECHERCHE.DOCTORANT_FINANCEMENT 
ADD (ID_TYPE_CONTRAT_DOCTORAL NUMBER );

ALTER TABLE RECHERCHE.DOCTORANT_FINANCEMENT ADD CONSTRAINT FK_DOCTFIN_TCD 
FOREIGN KEY (ID_TYPE_CONTRAT_DOCTORAL) REFERENCES RECHERCHE.TYPE_CONTRAT_DOCTORAL (ID_TYPE_CONTRAT_DOCTORAL) ENABLE;
COMMENT ON COLUMN RECHERCHE.DOCTORANT_FINANCEMENT.ID_TYPE_CONTRAT_DOCTORAL IS 'Clé étrangère vers la table RECHERCHE.TYPE_CONTRAT_DOCTORAL';

-- Ajout d'un type de contrat doctoral
CREATE TABLE RECHERCHE.SITUATION_DOCTEUR 
(
  ID_SITUATION_DOCTEUR NUMBER NOT NULL 
, LIBELLE_SITUATION_DOCTEUR VARCHAR2(50) 
, TYPE_CONTRAT_POST_DOCTORAL VARCHAR2(1)
, CONSTRAINT SITUATION_DOCTEUR_PK PRIMARY KEY 
	(ID_SITUATION_DOCTEUR) ENABLE 
);

COMMENT ON COLUMN RECHERCHE.SITUATION_DOCTEUR.ID_SITUATION_DOCTEUR IS 'Clé primaire de la table SITUATION_DOCTEUR';
COMMENT ON COLUMN RECHERCHE.SITUATION_DOCTEUR.LIBELLE_SITUATION_DOCTEUR IS 'Libellé de la situation du docteur';
COMMENT ON COLUMN RECHERCHE.SITUATION_DOCTEUR.TYPE_CONTRAT_POST_DOCTORAL IS '''O'' si le contrat est de type post-doctoral (ATER exclus) ''N'' sinon';

ALTER TABLE RECHERCHE.DOCTORANT_THESE ADD (ID_SITUATION_DOCTEUR NUMBER);
ALTER TABLE RECHERCHE.DOCTORANT_THESE ADD CONSTRAINT FK_SITUATION_DOCTEUR 
FOREIGN KEY (ID_SITUATION_DOCTEUR) REFERENCES RECHERCHE.SITUATION_DOCTEUR (ID_SITUATION_DOCTEUR) ENABLE;
COMMENT ON COLUMN RECHERCHE.DOCTORANT_THESE.ID_SITUATION_DOCTEUR IS 'Clé étrangère vers la table RECHERCHE.SITUATION_DOCTEUR indiquant la situation du docteur apres la thèse';