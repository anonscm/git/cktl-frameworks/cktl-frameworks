CREATE OR REPLACE PROCEDURE GRHUM.INIT_RECHERCHE_1_1_2_1 IS

c_struct_groupe_rech integer;

c_struct_tutelles integer;
pers_id_tutelles integer;

c_struct_coordinateurs integer;
pers_id_coordinateurs integer;

sangria_app_id integer;

pers_id_createur integer;


BEGIN

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');


select c_structure_pere into c_struct_groupe_rech from structure_ulr s 
  inner join grhum.grhum_parametres gp on s.c_structure = gp.param_value and gp.param_key like 'GROUPE_EQUIPES_RECHERCHE';

Insert into GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'org.cocktail.sangria.groupes.recherche', c_struct_groupe_rech,'C_structure du groupe recherche', PERS_ID_CREATEUR, PERS_ID_CREATEUR, sysdate, sysdate,'15');


-- gestion des droits sur la page d'accueil de sangria


select app_id into sangria_app_id from GRHUM.GD_APPLICATION where app_str_id like 'SANGRIA';

Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','UNITE_RECH','Acces aux unites de recherche','Acces aux unites de recherche');
Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','FEDERATIONS_RECH','Acces aux federations de recheche','Acces aux federations de recheche');
Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','ECOLES_DOCTORALES','Acces aux ecoles doctorales','Acces aux ecoles doctorales');


Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','PROJETS_SCIENTIF','Acces aux projets scientifiques','Acces aux projets scientifiques');
Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','DEMANDE_SUBVENTION','Acces aux demandes de subventions','Acces aux demandes de subventions');
Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','CONTRAT_RECHERCHE','Acces aux contrats de recherche','Acces aux contrats de recherche');


Insert into GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
values (GD_FONCTION_SEQ.NEXTVAL,sangria_app_id,'Recherche','ADMINISTRATION','Acces administration','Acces administration');






END;

EXECUTE GRHUM.INIT_RECHERCHE_1_1_2_1();
/

COMMIT;

DROP PROCEDURE GRHUM.INIT_RECHERCHE_1_1_2_1;
/

