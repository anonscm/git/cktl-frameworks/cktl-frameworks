SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°2/3
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.2.0
-- Date de publication :  26/11/2012
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


whenever sqlerror exit sql.sqlcode ;

GRANT EXECUTE ON  GRHUM.INS_STRUCTURE_ULR TO RECHERCHE;
GRANT EXECUTE ON  GRHUM.MAJ_REPART_TYPE_GROUPE TO RECHERCHE;


GRANT SELECT ON  GRHUM.GRHUM_PARAMETRES_SEQ TO RECHERCHE;
GRANT SELECT ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;
GRANT INSERT ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;



create or replace procedure RECHERCHE.INIT_1_2_0
as
c_struc_rech integer;
c_struct_poles integer;
pers_id_poles integer;


pers_id_createur integer;

begin

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM grhum.COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');


-- recupération du c_strcuture de la racine de la recherche
select param_value into c_struc_rech from grhum.grhum_parametres where param_key = 'org.cocktail.sangria.groupes.recherche';

GRHUM.Ins_Structure_Ulr ( c_struct_poles, pers_id_poles, 'POLES DE COMPETITIVITE', 'POLES DE COMPETITIVITE', 'A', c_struc_rech, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);
GRHUM.Maj_Repart_Type_Groupe(c_struct_poles, 'G','O'); 

Insert into GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) 
values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL, 'org.cocktail.sangria.groupes.polecompetitivite', c_struct_poles,'C_structure du groupe des pole de compet', PERS_ID_CREATEUR, PERS_ID_CREATEUR, sysdate, sysdate,'15');



  INSERT INTO RECHERCHE.DB_VERSION
  (
    DBV_ID,
    DBV_LIBELLE,
    DBV_DATE,
    DBV_INSTALL,
    DBV_COMMENT
  )
  VALUES
  (
    RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
    '1.2.0',
    to_date('2012/11/27', 'yyyy/mm/dd'),
    sysdate,
    'Ajout de la repartition statuts / contrats de travail'
  );

END;
/



execute RECHERCHE.INIT_1_2_0();
/

drop procedure RECHERCHE.INIT_1_2_0;
/ 

REVOKE EXECUTE ON  GRHUM.INS_STRUCTURE_ULR FROM RECHERCHE;
REVOKE EXECUTE ON  GRHUM.MAJ_REPART_TYPE_GROUPE FROM RECHERCHE;


REVOKE SELECT ON  GRHUM.GRHUM_PARAMETRES_SEQ FROM RECHERCHE;

REVOKE INSERT ON GRHUM.GRHUM_PARAMETRES FROM RECHERCHE;


