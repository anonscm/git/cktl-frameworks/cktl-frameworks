SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°5
-- Type: DDL
-- Schéma:  RECHERCHE
-- Numéro de version :  1.0.0.0
-- Date de publication :  13/12/2010
-- Auteurs : Julien Lafourcade, Bruno Garnier
-- Licence : CeCILL version 2
--
--


/*
 Création des tables RECHERCHE dédié la gestion de la Recherche 
 l'application Sangria l'utilisera
*/


whenever sqlerror exit sql.sqlcode ;


-- NOMENCLATURE ERC

CREATE SEQUENCE RECHERCHE.ERC_SEQ  INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCACHE ;


CREATE TABLE RECHERCHE.ERC (
  ERC_ID NUMBER NOT NULL,
  ERC_ORDRE VARCHAR2(10),
  ERC_LIBELLE VARCHAR2(255) NOT NULL,
  ERC_TYPE VARCHAR2(10) NOT NULL,
  ERC_PARENT NUMBER
);

ALTER TABLE RECHERCHE.ERC ADD (PRIMARY KEY (ERC_ID) USING INDEX TABLESPACE INDX_GRHUM);

create or replace procedure ghrum.init_erc 
as
erc_id_racine integer;
erc_id_pe integer;
erc_id_sh integer;
erc_id_ls integer;
begin

select recherche.erc_seq.nextval into erc_id_racine from dual;
select recherche.erc_seq.nextval into erc_id_pe from dual;
select recherche.erc_seq.nextval into erc_id_sh from dual;
select recherche.erc_seq.nextval into erc_id_ls from dual;

INSERT INTO RECHERCHE.ERC(ERC_ID, ERC_ORDRE, ERC_LIBELLE, ERC_TYPE, ERC_PARENT) VALUES(erc_id_racine, 'ERC', 'Nomenclature ERC (European Research Council)', 'CAT', NULL);
INSERT INTO RECHERCHE.ERC(ERC_ID, ERC_ORDRE, ERC_LIBELLE, ERC_TYPE, ERC_PARENT) VALUES(erc_id_pe, 'PE', 'Mathematics, physical sciences, information and communication, engineering, universe and earth sciences', 'CAT', erc_id_racine);
INSERT INTO RECHERCHE.ERC(ERC_ID, ERC_ORDRE, ERC_LIBELLE, ERC_TYPE, ERC_PARENT) VALUES(erc_id_sh, 'ERC', 'Mathematics, physical sciences, information and communication, engineering, universe and earth sciences', 'CAT', erc_id_racine);

INSERT INTO RECHERCHE.ERC(ERC_ID, ERC_ORDRE, ERC_LIBELLE, ERC_TYPE, ERC_PARENT) VALUES(RECHERCHE.ERC_SEQ.NEXTVAL, Nomenclature ERC (European Research Council)

end;
/



