--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°3/3
-- Type : DML
-- Schéma modifié :  RECHERCHE, GRHUM
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.2.0
-- Date de publication :  01/04/2012
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- 
----------------------------------------------



INSERT INTO GRHUM.ASSOCIATION (ASS_ID,ASS_LIBELLE,D_CREATION,D_MODIFICATION,TAS_ID,ASS_RACINE,ASS_CODE,ASS_ALIAS,ASS_LOCALE,D_OUVERTURE,D_FERMETURE) 
VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL,'POLE DE COMPETITIVITE',sysdate,sysdate,'1',null,'POLECOMPET',null,'N',null,null);

INSERT INTO GRHUM.ASSOCIATION_RESEAU
  (
    ASR_ID,
    ASS_ID_PERE,
    ASS_ID_FILS,
    ASR_RANG,
    ASR_COMMENTAIRE,
    D_CREATION,
    D_MODIFICATION
  )
  VALUES
  (
    GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL,
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'RECHERCHE'),
    (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'POLECOMPET'),
    null,
    'Role de pole de compet',
    sysdate,
    sysdate
  );