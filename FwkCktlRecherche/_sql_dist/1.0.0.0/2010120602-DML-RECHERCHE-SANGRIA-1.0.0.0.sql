create or replace
procedure       init_recherche2 
as
pers_id_anr integer;
pers_id_ue integer;
c_struct_anr integer;
c_struct_ue integer;

c_struc_rech integer;
pers_id_rech integer;
c_struct_etab integer;
c_struc_rech_1 integer;
pers_id_rech_1 integer;
c_struc_rech_2 integer;
pers_id_rech_2 integer;
c_struc_rech_3 integer;
pers_id_rech_3 integer;

pers_id_createur integer;

c_struct_parent integer;
begin


select param_value into c_struct_parent from grhum.grhum_parametres where param_key = 'ANNUAIRE_FOU_VALIDE_MORALE';


-- recupération du c_strcuture de la racine de l'etablissement
select c_structure into c_struct_etab from structure_ulr where c_structure_pere = c_structure;

-- creation d'un groupe recherche qui servira a mettre tous les groupes utilisés par Sangria
GRHUM.Ins_Structure_Ulr ( c_struc_rech, pers_id_rech, 'Recherche', 'RECH', 'A', c_struct_etab, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);
GRHUM.Maj_Repart_Type_Groupe(c_struc_rech, 'G','O'); 

-- creation des 2 groupes necessaires a sangria sous le groupe recherche
GRHUM.Ins_Structure_Ulr ( c_struc_rech_1, pers_id_rech_1, 'FINANCEURS DE PROJETS', 'FINANCEURS_AAP', 'A', c_struc_rech, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);
GRHUM.Maj_Repart_Type_Groupe(c_struc_rech_1, 'G','O'); 
GRHUM.Ins_Structure_Ulr ( c_struc_rech_2, pers_id_rech_2, 'ETABLISSEMENTS GESTIONNAIRES FINANCIERS AAP', 'ETAB_GEST_FIN_AAP', 'A', c_struc_rech, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);
GRHUM.Maj_Repart_Type_Groupe(c_struc_rech_2, 'G','O'); 

-- creation d'un groupe contenant les equipes de recherche
GRHUM.Ins_Structure_Ulr ( c_struc_rech_3, pers_id_rech_3, 'Equipes de recherche', 'EQUIPES RECHERCHE', 'A', c_struc_rech, NULL, NULL,
                                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                               NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);
GRHUM.Maj_Repart_Type_Groupe(c_struc_rech_3, 'G','O'); 

-- Recup du pers_id de GRHUM_CREATEUR
SELECT MIN(PERS_ID) INTO PERS_ID_CREATEUR FROM COMPTE WHERE CPT_LOGIN IN 
  (SELECT PARAM_VALUE FROM GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY='GRHUM_CREATEUR');

-- creation des parametres de GRHUM_PARAMETRES utilisées par Sangria

INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'GROUPE_FINANCEURS_AAP',c_struc_rech_1,'C_structure du groupe des financeurs pour les appels a projet',pers_id_createur,pers_id_createur,sysdate, sysdate, 15);
INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'GROUPE_ETAB_GEST_FIN_AAP',c_struc_rech_2,'C_structure du groupe des etablissements gestionnaires financiers pour les appels a projet',pers_id_createur,pers_id_createur,sysdate, sysdate, 15);
INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'GROUPE_EQUIPES_RECHERCHE',c_struc_rech_3,'C_structure du groupe des equipes de recherche',pers_id_createur,pers_id_createur,sysdate, sysdate, 15);
INSERT INTO GRHUM.GRHUM_PARAMETRES (PARAM_ORDRE,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION,PARAM_TYPE_ID) values (GRHUM.GRHUM_PARAMETRES_SEQ.NEXTVAL,'GROUPE_PARTENAIRES_RECHERCHE',NULL,'C_structure du groupe des partenaire de la recherche de recherche',pers_id_createur,pers_id_createur,sysdate, sysdate, 15);

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
-- DEBUT DE LA PARTIE SUR LES FINANCEURS. 
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

-- 


-- CHOIX N°1
-- LES STRUCTURES AGENCE NATIONALE DE LA RECHERCHE et UNION EUROPENNE EXISTENT
-- IL FAUT RECUPERER LEURS PERS_ID ET LES INSCRIRE CI_DESSOUS (DECOMMENTER LES 4 LIGNES)

-- pers_id_anr := 124079;
-- pers_id_ue := 126050;
-- select c_structure into c_struct_anr from GRHUM.structure_ulr where pers_id = pers_id_anr;
-- select c_structure into c_struct_ue from GRHUM.structure_ulr where pers_id = pers_id_ue;



-- CHOIX N°2 
-- LES STRUCTURES N'EXISTENT PAS ET VOUS DECOMMENTEZ LES 2 INSTRUCTIONS DE CREATION CI-DESSOUS

-- GRHUM.Ins_Structure_Ulr ( c_struct_anr, pers_id_anr, 'Agence nationale de la recherche', 'ANR', 'A', c_struct_parent, NULL, NULL,
-- NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
-- NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
-- NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);

-- GRHUM.Ins_Structure_Ulr ( c_struct_ue, pers_id_ue, 'UNION EUROPEENNE', 'UE', 'A', c_struct_parent, NULL, NULL,
-- NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
-- NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--NULL, NULL, '+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL);


-- rattachement des 2 structures ANR et UE au groupe des financeurs AAP

grhum.INS_REPART_STRUCTURE(pers_id_anr,c_struc_rech_1);
grhum.INS_REPART_STRUCTURE(pers_id_ue,c_struc_rech_1);

Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (4,null,c_struct_anr,'Biologie - santé',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (5,null,c_struct_anr,'Ecosystèmes et Développement Durable',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (6,null,c_struct_anr,'Energie durable et environnement',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (7,null,c_struct_anr,'Ingénierie, Procédés et Sécurité',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (8,null,c_struct_anr,'Programmes non-thématiques',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (9,null,c_struct_anr,'Programmes transversaux',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (10,null,c_struct_anr,'Sciences et Technologies de l''information',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (11,null,c_struct_anr,'Sciences Humaines et Sociales',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (12,4,c_struct_anr,' Appel à Projets Transnational dans le cadre de l''ERA-NET EMIDA',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (13,4,c_struct_anr,' Appel à Projets Transnational dans le cadre de l''ERA-Net NEURON ""Maladies Mentales""',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (14,4,c_struct_anr,' Appel à Projets Transnational dans le cadre de l''ERA-NET PRIOMEDCHILD',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (15,4,c_struct_anr,' Maladie d''Alzheimer et Maladies Apparentées (MALZ)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (16,4,c_struct_anr,' Mécanismes intégrés de l''inflammation MI2',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (17,4,c_struct_anr,' Recherche finalisée sur les cellules souches (RFCS)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (18,4,c_struct_anr,' Recherche partenariale en biotechnologies pour la santé (BiotecS)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (19,4,c_struct_anr,' Recherche partenariale en technologies pour la santé et l''autonomie (TecSan)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (20,4,c_struct_anr,' Troisième Appel à Projets Transnational dans le cadre de l''ERA-NET PathoGenoMics',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (21,5,c_struct_anr,' Programme de recherche Alimentation et Industries Alimentaires (ALIA)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (22,5,c_struct_anr,' Programme Ecosystèmes, territoires, ressources vivantes et agricultures - SYSTERRA',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (23,5,c_struct_anr,' Programme Génomique, Biotechnologies végétales',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (24,6,c_struct_anr,' BIOENERGIES 2010',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (25,6,c_struct_anr,' Efficacité énergétique et réduction des émissions de CO2 dans les systèmes industriels',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (26,6,c_struct_anr,' Habitat intelligent et solaire photovoltaïque',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (27,6,c_struct_anr,' Programme Hydrogène et Piles à Combustible (H-PAC)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (28,6,c_struct_anr,' Programme Production Durable et Technologies de l''Environnement (ECOTECH)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (29,6,c_struct_anr,' Programme Stockage Innovant de l''Energie (Stock-E)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (30,6,c_struct_anr,' Villes durables',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (32,7,c_struct_anr,' Chimie Durable ¿ Industries ¿ Innovation',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (33,7,c_struct_anr,' Concepts Systèmes et Outils pour la Sécurité Globale',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (34,7,c_struct_anr,' Programme Matériaux Fonctionnels et Procédés Innovants',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (35,8,c_struct_anr,' Programme ""Chaires d''excellence""',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (36,8,c_struct_anr,' Programme ""Retour Post-Doctorants"" édition 2010',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (37,8,c_struct_anr,' Programme Blanc International Edition 2010',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (3,8,c_struct_anr,'Jeune chercheurs / chercheuses',pers_id_createur,pers_id_createur,to_timestamp('28/01/10','DD/MM/RR HH24:MI:SSXFF'),to_timestamp('28/01/10','DD/MM/RR HH24:MI:SSXFF'));
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (38,9,c_struct_anr,' Contaminants, Ecosystèmes, Santé (CES)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (39,10,c_struct_anr,' Programme Conception et Simulation',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (40,10,c_struct_anr,' Programme CONTENUS ET INTERACTIONS',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (42,10,c_struct_anr,' Programme NANOTECHNOLOGIES ET NANOSYSTEMES (P2N)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (43,10,c_struct_anr,' Réseaux du Futur et Services VERSO',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (44,10,c_struct_anr,' Systèmes Embarqués et Grandes Infrastructures',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (45,11,c_struct_anr,' Appel à projets franco-allemand en sciences humaines et sociales',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (46,11,c_struct_anr,' Appel à projets ORA ""Open Research Area in Europe""',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (47,11,c_struct_anr,' ESPACE ET TERRITOIRE : Les énigmes spatiales de la vie en société',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (48,11,c_struct_anr,' La création : processus, acteurs, objets, contextes - Édition 2010',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (49,11,c_struct_anr,' Les Suds, aujourd''hui II',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (50,null,c_struct_ue,'COOPERATION',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (51,null,c_struct_ue,'IDEES (ERC)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (52,null,c_struct_ue,'PERSONNES (Marie Curie)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (53,null,c_struct_ue,'CAPACITES',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (54,null,c_struct_ue,'AUTRES Programmes',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (55,null,c_struct_ue,'FEDER ',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (56,null,c_struct_ue,'INTERREG',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (57,50,c_struct_ue,'santé',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (58,50,c_struct_ue,'alimentation, agriculture et pêche et biotechnologie',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (59,50,c_struct_ue,'technologies de l¿information et de la communication',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (60,50,c_struct_ue,'nanosciences, nanotechnologies, matériaux et nouvelles technologies de production',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (61,50,c_struct_ue,'énergie',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (62,50,c_struct_ue,'environnement (changement climatique inclus)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (63,50,c_struct_ue,'transports (aéronautique comprise)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (64,50,c_struct_ue,'sciences socio-économiques et humaines',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (65,50,c_struct_ue,'espace',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (66,50,c_struct_ue,'sécurité',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (67,51,c_struct_ue,'Bourse jeune chercheur',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (68,51,c_struct_ue,'Bourse chercheur confirmé',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (69,52,c_struct_ue,'ITN Initial Training Network',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (70,52,c_struct_ue,'IEF Intra-European Fellowships',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (71,52,c_struct_ue,'ERG European Reintegration Grants',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (72,52,c_struct_ue,'IAAP Industry-Academia partnerships ',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (73,52,c_struct_ue,'IRSES International Research Staff Exchange Scheme',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (74,52,c_struct_ue,'IOF International Outgoing Fellowships',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (75,52,c_struct_ue,'IIF International Incoming Fellowships',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (76,52,c_struct_ue,'IRG International Reintegration Grants',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (77,52,c_struct_ue,'NIGHT Researchers Night',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (78,53,c_struct_ue,'Infrastructures de recherche',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (79,53,c_struct_ue,'Recherche au profit des PME',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (80,53,c_struct_ue,'Régions de la connaissance',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (81,53,c_struct_ue,'Potentiel de recherche',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (82,53,c_struct_ue,'La science dans la société',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (83,53,c_struct_ue,'Activités spécifiques de coopération internationale',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (84,54,c_struct_ue,'COST',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (85,54,c_struct_ue,'EUREKA...',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (86,55,c_struct_ue,'Recherche',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (87,56,c_struct_ue,'France ESpagne Andorre',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (88,56,c_struct_ue,'SUDOE',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (89,56,c_struct_ue,'Espace Atlantique',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (31,6,c_struct_anr,' Programme de Recherche et d''Innovation dans les Transports terrestres (PREDIT)',pers_id_createur,pers_id_createur,sysdate,sysdate);
Insert into RECHERCHE.PROGRAMMES (PROG_ORDRE,PROG_PERE,C_STRUCTURE,LL_PROGRAMME,PERS_ID_CREATION,PERS_ID_MODIFICATION,D_CREATION,D_MODIFICATION) values (41,10,c_struct_anr,' Programme CONTENUS ET INTERACTIONS Défi Multimédia (REPERE)',pers_id_createur,pers_id_createur,sysdate,sysdate);


--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
-- FIN DE LA PARTIE SUR LES FINANCEURS. 
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------


-- association direction
	
INSERT INTO "GRHUM"."ASSOCIATION" (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL,'DOCTORANT',SYSDATE,SYSDATE,tas_id_doc,NULL,'DOCT','N');
INSERT INTO "GRHUM"."ASSOCIATION" (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'DIRECTION', TO_DATE('27/04/11', 'DD/MM/RR'), TO_DATE('27/04/11', 'DD/MM/RR'), '1', 'DIRECTION', 'N');
INSERT INTO "GRHUM"."ASSOCIATION" (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'MEMBRE PERMANENT', sysdate, sysdate, '1', 'MEMBRE P', 'N');
INSERT INTO "GRHUM"."ASSOCIATION" (ASS_ID, ASS_LIBELLE, D_CREATION, D_MODIFICATION, TAS_ID, ASS_CODE, ASS_LOCALE) VALUES (GRHUM.ASSOCIATION_SEQ.NEXTVAL, 'MEMBRE NON PERMANENT', sysdate, sysdate, '1', 'MEMBRE NP', 'N');


  -- nouveau domaine
  insert into jefy_admin.domaine values (11,'RECHERCHE');

  -- nouvelle fonction et type_application RECHERCHE
  JEFY_ADMIN.API_APPLICATION.creerTypeApplication(120,11,'RECHERCHE','SANGRIA');
jefy_admin.api_application.creerFonction(2100,'RECH','Recherche','Acces à l''application de gestion de la recherche','Acces Recherche','N','N',120);


END;

/

execute grhum.init_recherche2();

/ 

drop procedure grhum.init_recherche2;

/

commit;
