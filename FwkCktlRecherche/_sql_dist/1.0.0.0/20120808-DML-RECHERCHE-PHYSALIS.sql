CREATE OR REPLACE PROCEDURE GRHUM.INIT_PHYSALIS_1_0_0_1 IS

domaine_id integer;
physalis_app_id integer;

BEGIN

-- recuperation du dom_id du domaine RECHERCHE
SELECT dom_id into domaine_id FROM GD_DOMAINE WHERE DOM_LC LIKE 'RECHERCHE';

-- insertion de l'application Physalis
INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) 
VALUES (GRHUM.GD_APPLICATION_seq.nextval, domaine_id, 'PHYSALIS', 'PHYSALIS');

-- recuperation de l'identifiant de l'application Physalis
SELECT app_id INTO physalis_app_id FROM GRHUM.GD_APPLICATION WHERE app_str_id LIKE 'PHYSALIS';

-- insertion des differents droits
INSERT INTO GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','PHYSALIS','Acces a Physalis','Acces a Physalis');

INSERT INTO GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','AJOUT_ETUDIANT','Acces a l''ajout d''un etudiant','Acces a l''ajout d''un etudiant');

INSERT INTO GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','RECHERCHER_DOCTORANT','Acces a la recherche de doctorant','Acces a la recherche de doctorant');

INSERT INTO GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','DIR_THESE_HABILITES','Acces aux directeurs de these habilites','Acces aux directeurs de these habilites');

INSERT INTO GD_FONCTION (FON_ID,APP_ID,FON_CATEGORIE,FON_ID_INTERNE,FON_LC,FON_DESCRIPTION) 
VALUES (GD_FONCTION_SEQ.NEXTVAL,physalis_app_id,'Physalis','GESTION_PARAMETRES','Acces a la gestion des parametres','Acces a la gestion des parametres');

END;
/

EXECUTE GRHUM.INIT_PHYSALIS_1_0_0_1();
/

COMMIT;

DROP PROCEDURE GRHUM.INIT_PHYSALIS_1_0_0_1;
/