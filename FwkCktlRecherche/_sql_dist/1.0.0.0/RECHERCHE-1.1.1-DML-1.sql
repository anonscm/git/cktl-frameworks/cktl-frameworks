--
-- Patch DDL à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.1.1
-- Date de publication : 13/11/2012
-- Auteur(s) : Julien LAFOURCADE, Jean-Marc PUSSACQ
-- Licence : CeCILL version 2
--
--

GRANT SELECT ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;
GRANT UPDATE ON GRHUM.GRHUM_PARAMETRES TO RECHERCHE;
GRANT SELECT ON GRHUM.GRHUM_PARAMETRES_TYPE TO RECHERCHE;

create or replace procedure RECHERCHE.INIT_1_1_1_PHYSALIS is
type_id_parametre_texte integer;

begin

-- recuperation de l'identifiant d'un parametre de type texte
SELECT type_id INTO type_id_parametre_texte FROM GRHUM.GRHUM_PARAMETRES_TYPE WHERE type_id_interne like 'FREE_TEXT';

-- donnees table GRHUM.GRHUM_PARAMETRES à mettre a jour
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.ETABLISSEMENT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.INTITULE_MINISTERE';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.RECTORAT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.QUALITE_CHEF_ETABLISSEMENT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.NOM_CHEF_ETABLISSEMENT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.ETAB_DIPLOME_DEFAUT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.FGRA_CODE_DOCTORAT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.VILLE_ETABLISSEMENT';
UPDATE GRHUM.GRHUM_PARAMETRES SET param_type_id = type_id_parametre_texte WHERE PARAM_KEY = 'org.cocktail.physalis.NOM_COMPLET_CHEF_ETABLISSEMENT';

END;

/


create or replace
procedure RECHERCHE.INIT_1_1_1 as
BEGIN
	RECHERCHE.INIT_1_1_1_PHYSALIS();
	INSERT INTO RECHERCHE.DB_VERSION(DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) 
		VALUES (RECHERCHE.DB_VERSION_SEQ.NEXTVAL, '1.1.1', to_date('2012/11/13', 'yyyy/mm/dd'), sysdate, 'Patch pour Physalis');
END;
/

execute RECHERCHE.INIT_1_1_1();
/

COMMIT;

drop procedure RECHERCHE.INIT_1_1_1_PHYSALIS;
drop procedure RECHERCHE.INIT_1_1_1;

REVOKE SELECT ON GRHUM.GRHUM_PARAMETRES FROM RECHERCHE;
REVOKE UPDATE ON GRHUM.GRHUM_PARAMETRES FROM RECHERCHE;
REVOKE SELECT ON GRHUM.GRHUM_PARAMETRES_TYPE FROM RECHERCHE;
