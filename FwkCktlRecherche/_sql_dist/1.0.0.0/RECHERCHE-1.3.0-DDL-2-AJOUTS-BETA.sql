SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/1
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.0
-- Date de publication :  15/04/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


whenever sqlerror exit sql.sqlcode ;


ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_PARENT_ORDRE NUMBER);
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_ANCIEN_ORDRE NUMBER);
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_NOUVEAU_CODE VARCHAR2(20));
ALTER TABLE GRHUM.DOMAINE_SCIENTIFIQUE ADD (DS_VERSION NUMBER);

COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_PARENT_ORDRE IS 'Id du domaine parent';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_ANCIEN_ORDRE IS 'Id du domaine ancienne version';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_NOUVEAU_CODE IS 'Le code en version VARCHAR';
COMMENT ON COLUMN GRHUM.DOMAINE_SCIENTIFIQUE.DS_VERSION IS 'Version de la nomenclature du domaine';
