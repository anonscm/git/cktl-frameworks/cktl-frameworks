SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/2
-- Type: DDL
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.0
-- Date de publication :  17/04/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


whenever sqlerror exit sql.sqlcode ;


ALTER TABLE RECHERCHE.DOCTORANT_FINANCEMENT ADD (DATE_DEBUT_FINANCEMENT DATE );
ALTER TABLE RECHERCHE.DOCTORANT_FINANCEMENT ADD (DATE_FIN_FINANCEMENT DATE );
COMMENT ON COLUMN RECHERCHE.DOCTORANT_FINANCEMENT.DATE_DEBUT_FINANCEMENT IS 'Date de début de financement si le doctorant n''a pas d''employeur (bourse par exemple)';
COMMENT ON COLUMN RECHERCHE.DOCTORANT_FINANCEMENT.DATE_FIN_FINANCEMENT IS 'Date de fin de financement si le doctorant n''a pas d''employeur (bourse par exemple)';
