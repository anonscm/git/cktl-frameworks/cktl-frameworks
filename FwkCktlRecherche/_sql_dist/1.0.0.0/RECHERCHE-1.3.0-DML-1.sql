SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/2
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.0
-- Date de publication :  17/04/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--


whenever sqlerror exit sql.sqlcode ;

INSERT INTO RECHERCHE.DB_VERSION
(
  DBV_ID,
  DBV_LIBELLE,
  DBV_DATE,
  DBV_INSTALL,
  DBV_COMMENT
)
VALUES
(
  RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
  '1.3.0',
  to_date('2013/04/17', 'yyyy/mm/dd'),
  sysdate,
  'Ajout des dates de financement dans le cas d une these sans employeur'
);
