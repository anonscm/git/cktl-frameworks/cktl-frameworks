SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°1/2
-- Type: DDL
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.3
-- Date de publication :  24/06/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

COMMENT ON COLUMN RECHERCHE.DIRECTEUR_THESE.DATE_AUTORISATION IS 'Date d''autorisation du conseil scientifique pour diriger une these';

ALTER TABLE RECHERCHE.DOCTORANT_THESE ADD (DATE_FIN_ANTICIPEE_THESE DATE );
ALTER TABLE RECHERCHE.DOCTORANT_THESE ADD (MOTIF_FIN_ANTICIPEE_THESE VARCHAR2(500));

COMMENT ON COLUMN RECHERCHE.DOCTORANT_THESE.DATE_FIN_ANTICIPEE_THESE IS 'Date de fin anticipée de la thèse';
COMMENT ON COLUMN RECHERCHE.DOCTORANT_THESE.MOTIF_FIN_ANTICIPEE_THESE IS 'Motif en cas de fin anticipée de la thèse';

