SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°3/3
-- Type: DML
-- Schéma:  RECHERCHE
-- Numéro de version :  1.3.3
-- Date de publication :  27/06/2013
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--
--

INSERT INTO GRHUM.ASSOCIATION_RESEAU
(
  ASR_ID,
  ASS_ID_PERE,
  ASS_ID_FILS,
  ASR_RANG,
  ASR_COMMENTAIRE,
  D_CREATION,
  D_MODIFICATION
)
VALUES
(
  GRHUM.ASSOCIATION_RESEAU_SEQ.NEXTVAL,
  (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'DOCT'),
  (SELECT ASS_ID FROM GRHUM.ASSOCIATION WHERE ASS_CODE LIKE 'D_ETAB_ACC'),
  null,
  'Role de d''etablissement d''accueil pour une these',
  sysdate,
  sysdate
);
  
INSERT INTO RECHERCHE.DB_VERSION
(
  DBV_ID,
  DBV_LIBELLE,
  DBV_DATE,
  DBV_INSTALL,
  DBV_COMMENT
)
VALUES
(
  RECHERCHE.DB_VERSION_SEQ.NEXTVAL,
  '1.3.3',
  to_date('2013/06/24', 'yyyy/mm/dd'),
  sysdate,
  'Ajout d''un role pour les etablissements d''accueil'
);

END;
/

commit;
  