SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--

--
-- 
-- Fichier: n°2 à ne passer que pour une utilisation en BETA
-- Type: DDL
-- Schéma:  RECHERCHE
-- Numéro de version :  1.0.0
-- Date de publication :  07/12/2010
-- Auteurs : Julien Lafourcade, Jean-Marc Pussacq, Bruno Garnier
-- Licence : CeCILL version 2
--



whenever sqlerror exit sql.sqlcode ;



CREATE OR REPLACE PROCEDURE GRHUM.INIT_RECH_1_0_0_DDL_AJOUT_BETA
AS
	TC_COUNT NUMBER;
BEGIN
  SELECT MAX(TYCON_ID) + 1 INTO TC_COUNT FROM ACCORDS.TYPE_CONTRAT;
  EXECUTE IMMEDIATE 'CREATE SEQUENCE ACCORDS.TYPE_CONTRAT_SEQ INCREMENT BY 1 START WITH ' || TC_COUNT || ' NOCACHE  NOORDER  NOCYCLE';



END;
/

EXECUTE GRHUM.INIT_RECH_1_0_0_DDL_AJOUT_BETA();
/

DROP PROCEDURE GRHUM.INIT_RECH_1_0_0_DDL_AJOUT_BETA;
/
