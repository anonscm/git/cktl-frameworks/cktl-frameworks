package org.cocktail.fwkcktlrecherche.server.metier.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.wounit.rules.MockEditingContext;

/**
 * Classe de tests sur les directeurs de physalis
 */
public class DoctorantTheseServiceTest {
	
	@Rule
	public MockEditingContext editingContext = new MockEditingContext("Physalis");
	
	@Before
	public void setUp() {
		
	}
	
	@Test
	public void suppressionFinanceur() {

		EODoctorantThese these = mock(EODoctorantThese.class);
		EODoctorantFinanceur financeur = mock(EODoctorantFinanceur.class);
		ContratPartenaire contratPartenaire = mock(ContratPartenaire.class);
		IPersonne partenaire = mock(IPersonne.class);
		Contrat contrat = mock(Contrat.class);
		NSArray<EOAssociation> associations = new NSMutableArray<EOAssociation>();
		associations.add(mock(EOAssociation.class));
		
		when(financeur.toContratPartenaire()).thenReturn(contratPartenaire);
		when(contratPartenaire.partenaire()).thenReturn(partenaire);
		when(these.toContrat()).thenReturn(contrat);
		when(financeur.toRepartAssociation()).thenReturn(mock(EORepartAssociation.class));
		
		when(contratPartenaire.rolesPartenaireDansContrat(partenaire, contrat)).thenReturn(associations);
		
		DoctorantTheseService service = new DoctorantTheseService(editingContext, 1) {
			
			@Override
			protected FactoryContratPartenaire createFactoryContratPartenaire() {
				FactoryContratPartenaire factoryContratPartenaire = mock(FactoryContratPartenaire.class);
				return factoryContratPartenaire;
			}
			
		};
		assertTrue(service.supprimerFinanceur(these, financeur));
		
	}
	
}
