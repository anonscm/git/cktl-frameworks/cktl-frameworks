package org.cocktail.fwkcktlrecherche.server.metier.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

/**
 * Classe de tests sur les directeurs de physalis
 */
public class DirecteurTheseServiceTest {
	
	private Calendar dateDebutThese;
	private Calendar dateFinThese;

	private static final int ANNEE_DEBUT_THESE = 2012;
	private static final int ANNEE_FIN_THESE = 2015; 
	private static final int ANNEE_FIN_ENCADREMENT = 2014; 
	private static final int MOIS_DEBUT_THESE = Calendar.SEPTEMBER; 
	private static final int MOIS_FIN_THESE = Calendar.AUGUST;
	private static final int JOUR_DEBUT_THESE = 1; 
	private static final int JOUR_FIN_THESE = 31; 

	private static final int QUOTITE_MAX = 100;
	private static final int QUOTITE_INTER = 50;
	
	@Rule
	public MockEditingContext editingContext = new MockEditingContext("Physalis");
	
	@Before
	public void setUp() {
		dateDebutThese = Calendar.getInstance();
		dateDebutThese.set(ANNEE_DEBUT_THESE, MOIS_DEBUT_THESE, JOUR_DEBUT_THESE);

		dateFinThese = Calendar.getInstance();
		dateFinThese.set(ANNEE_FIN_THESE, MOIS_FIN_THESE, JOUR_FIN_THESE);
	}
	
	@Test
	public void laQuotiteDoitEtreInferieureALaQuotiteMaxQuandLeDirecteurNEncadrePasTouteLaThese() {
		
		Calendar dateDebutEncadrement = dateDebutThese;

		Calendar dateFinEncadrement = Calendar.getInstance();
		dateFinEncadrement.set(ANNEE_FIN_ENCADREMENT, MOIS_FIN_THESE, JOUR_FIN_THESE);
		
		DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(editingContext);
		BigDecimal quotite = dts.calculQuotite(new NSTimestamp(dateDebutThese.getTime()), new NSTimestamp(dateFinThese.getTime()), new BigDecimal(QUOTITE_MAX), 
				new NSTimestamp(dateDebutEncadrement.getTime()), new NSTimestamp(dateFinEncadrement.getTime()));
		
		assertTrue(quotite.compareTo(new BigDecimal(QUOTITE_MAX)) == -1);
	}
	
	@Test
	public void laQuotiteDoitEtreInferieureALaQuotiteMaxAvecUnDirecteurPasA100pour100() {
		
		Calendar dateDebutEncadrement = dateDebutThese;

		Calendar dateFinEncadrement = Calendar.getInstance();
		dateFinEncadrement.set(ANNEE_FIN_ENCADREMENT, MOIS_FIN_THESE, JOUR_FIN_THESE);
		
		BigDecimal quotite = new BigDecimal(60);
		
		DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(editingContext);
		quotite = quotite.add(dts.calculQuotite(new NSTimestamp(dateDebutThese.getTime()), new NSTimestamp(dateFinThese.getTime()), new BigDecimal(QUOTITE_INTER), 
				new NSTimestamp(dateDebutEncadrement.getTime()), new NSTimestamp(dateFinEncadrement.getTime())));
		
		assertTrue(quotite.compareTo(new BigDecimal(QUOTITE_MAX)) == -1);
	}
	
	@Test
	public void laQuotiteDoitEtreSupALaQuotiteMax() {
		
		Calendar dateDebutEncadrement = dateDebutThese;
		Calendar dateFinEncadrement = dateFinThese;
		
		BigDecimal quotite = new BigDecimal(60);
		
		DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(editingContext);
		quotite = quotite.add(dts.calculQuotite(new NSTimestamp(dateDebutThese.getTime()), new NSTimestamp(dateFinThese.getTime()), new BigDecimal(QUOTITE_INTER), 
				new NSTimestamp(dateDebutEncadrement.getTime()), new NSTimestamp(dateFinEncadrement.getTime())));
		
		assertTrue(quotite.compareTo(new BigDecimal(QUOTITE_MAX)) == 1);
	}
	
	@Test
	public void laQuotiteDoitEtreEgaleA100() {
		
		Calendar dateDebutEncadrement = dateDebutThese;

		Calendar dateFinEncadrement = Calendar.getInstance();
		dateFinEncadrement.set(ANNEE_FIN_THESE, MOIS_FIN_THESE, JOUR_FIN_THESE);
		
		BigDecimal quotite = new BigDecimal(QUOTITE_INTER);
		
		DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(editingContext);
		quotite = quotite.add(dts.calculQuotite(new NSTimestamp(dateDebutThese.getTime()), new NSTimestamp(dateFinThese.getTime()), new BigDecimal(QUOTITE_INTER), 
				new NSTimestamp(dateDebutEncadrement.getTime()), new NSTimestamp(dateFinEncadrement.getTime())));
		
		assertTrue(quotite.compareTo(new BigDecimal(QUOTITE_MAX)) == 0);
	}
	
}
