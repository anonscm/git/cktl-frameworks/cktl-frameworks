package org.cocktail.fwkcktlrecherche.server.droits;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;


public abstract class PerimetreStrategieRecherche {
	
	private NSArray<EOStructure> listeLaboratoires;
	
	/**
	 * @param edc editingContext
	 * @return la liste des laboratoires dans le référentiel
	 */
	public NSArray<EOStructure> listeLaboratoires(EOEditingContext edc) {
		if (listeLaboratoires == null) {
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc);
			listeLaboratoires = dts.listeLaboratoires();
		}
		return listeLaboratoires;
	}
	
	protected NSArray<EOStructure> listeLaboratoiresDontIndividuEstDirecteurOuSecretaire(EOEditingContext edc, EOIndividu individu) {
		
		if (listeLaboratoires(edc) == null) {
			return new NSArray<EOStructure>();
		}
		
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.PERS_ID.eq(individu.persId()),
				ERXQ.or(
						EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().directionAssociation(edc)),
						EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().directeurAdjointAssociation(edc)),
						EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().secretraireAdministrativeAssociation(edc))
						),
				EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(DateCtrl.getDateJour()),
				ERXQ.or(
						EORepartAssociation.RAS_D_FERMETURE.isNull(), 
						EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(DateCtrl.getDateJour())
						)
				);
		
		NSArray<EORepartAssociation> repartAssociations = EORepartAssociation.fetchAll(edc, qualifier);
		NSArray<EOStructure> laboratoiresDontIndividuEstDirecteurOuSecretaire = new NSMutableArray<EOStructure>();
		
		for (EORepartAssociation ra : repartAssociations) {
			if (listeLaboratoires(edc).contains(ra.toStructure())) {
				laboratoiresDontIndividuEstDirecteurOuSecretaire.add(ra.toStructure());
			}
		}
		
		return laboratoiresDontIndividuEstDirecteurOuSecretaire;
	}
	
}
