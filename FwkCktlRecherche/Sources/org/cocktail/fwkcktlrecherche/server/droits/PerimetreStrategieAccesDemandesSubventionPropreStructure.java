package org.cocktail.fwkcktlrecherche.server.droits;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;

/**
 * Cette strategie definit un perimetre sur la composante de l'utilisateur (structure).
 */
public class PerimetreStrategieAccesDemandesSubventionPropreStructure extends PerimetreStrategieRecherche implements IPerimetreStrategie {

	private EOEditingContext edc;
	private EOGdPerimetreSangria perimetre;

	/**
	 * @return l'editingContext
	 */
	public EOEditingContext edc() {
		return edc;
	}
	
	// Constructor
	/**
	 * Constructeur auquel on passe le perimetre de la strategie.
	 * @param perimetre perimetre associe pour la stratégie, utilisé pour récupérer le editing context
	 * @param edc editingContext
	 */
	public PerimetreStrategieAccesDemandesSubventionPropreStructure(EOGdPerimetreSangria perimetre, EOEditingContext edc) {
		this.perimetre = perimetre;
		this.edc = edc;
	}

	// Properties

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		EOIndividu individu = EOIndividu.fetchFirstByQualifier(edc(), EOIndividu.NO_INDIVIDU.eq(Integer.parseInt(perimetre.getUserInfo().noIndividu().toString())));
		
		NSArray<EOStructure> listeLaboratoiresDontIndividuEstDirecteur = listeLaboratoiresDontIndividuEstDirecteurOuSecretaire(edc, individu);
		NSArray<EOQualifier> sousQualifiers = new NSMutableArray<EOQualifier>();
		
		for (EOStructure laboratoire : listeLaboratoiresDontIndividuEstDirecteur) {
			sousQualifiers.add(Contrat.CONTRAT_PARTENAIRES.dot(ContratPartenaire.PERS_ID).eq(laboratoire.persId())); //TODO:
		}
		
		EOQualifier sousQualifier = ERXQ.or(sousQualifiers);
		return new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, EOAap.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
	}

}
