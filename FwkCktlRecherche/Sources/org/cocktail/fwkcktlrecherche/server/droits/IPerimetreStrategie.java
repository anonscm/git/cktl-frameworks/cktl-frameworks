package org.cocktail.fwkcktlrecherche.server.droits;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Cette interface definit une strategie de construction des perimetres.
 */
public interface IPerimetreStrategie {

	/**
	 * Qualifier correspondant à la strategie.
	 * @return un qualifier construit avec la strategie courante
	 */
	EOQualifier getQualifier();
}
