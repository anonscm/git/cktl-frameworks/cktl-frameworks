package org.cocktail.fwkcktlrecherche.server.droits;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Cette classe fournit des perimetres de donnees pour l'application sangria.
 */
public class PerimetreSangriaProvider implements IPerimetreProvider<EOGdPerimetre> {
	private List<EOGdPerimetreSangria> perimetres;

	// Private fields

	// Properties
	private IPerimetreStrategie getPerimetreStrategie(EOGdPerimetreSangria perimetre) {
		return PerimetreStrategieFactory.createStrategy(perimetre);
	}

	private List<EOGdPerimetreSangria> getPerimetres() {
		return perimetres;
	}

	/**
	 * Renseigne la liste des perimetres fournis au PerimetreSangriaProvider
	 * @param perimetres perimetres fournis au PerimetreSangriaProvider
	 */
	public void setPerimetres(List<EOGdPerimetreSangria> perimetres) {
		this.perimetres = perimetres;
	}

	// Public Method

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getAllQualifiers() {
		return getAllQualifiers(null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getAllQualifiers(NSArray<EOGdStrategieSangria> strategies, UserInfo userInfo) {
		EOQualifier allQualifiers = null;

		for (EOGdPerimetreSangria perimetre : getPerimetres()) {
			if ((strategies == null) || (strategies.contains(perimetre.strategie()))) {
				perimetre.setUserInfo(userInfo);
				IPerimetreStrategie perimetreStrategie = getPerimetreStrategie(perimetre);
				if (perimetreStrategie.getQualifier() != null) {
					allQualifiers = ERXQ.or(allQualifiers, perimetreStrategie.getQualifier());
				}
			}
		}
		return allQualifiers;
	}

	// Protected Methods
	// Private methods

}
