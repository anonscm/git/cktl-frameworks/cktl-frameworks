package org.cocktail.fwkcktlrecherche.server.droits;

import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;

import er.extensions.eof.ERXEC;


/**
 * Cette classe permet de construire une strategie de construction de perimetre pour un perimetre de donnees.
 */
public final class PerimetreStrategieFactory {

	private PerimetreStrategieFactory() {
	}

	/**
	 * Creation d'une strategie à partir d'un perimetre de donnees.
	 * @param perimetre perimetre de donnees pour lequel est definie la strategie.
	 * @return IPerimetreStrategie strategie
	 */
	public static IPerimetreStrategie createStrategy(EOGdPerimetreSangria perimetre) {
		
		if (EOGdStrategieSangria.STRATEGIE_CONSULTATION_TOUTES_DEMANDES_SUBVENTION_STRUCTURE_CODE.equals(perimetre.strategie().code())) {
			return new PerimetreStrategieAccesToutesDemandesSubvention(perimetre, ERXEC.newEditingContext());
		}
		
		if (EOGdStrategieSangria.STRATEGIE_CONSULTATION_DEMANDES_SUBVENTION_STRUCTURE_CODE.equals(perimetre.strategie().code())) {
			return new PerimetreStrategieAccesDemandesSubventionPropreStructure(perimetre, ERXEC.newEditingContext());
		}
		
		if (EOGdStrategieSangria.STRATEGIE_CONSULTATION_TOUS_CONTRATS_STRUCTURE_CODE.equals(perimetre.strategie().code())) {
			return new PerimetreStrategieAccesTousContrats(perimetre, ERXEC.newEditingContext());
		}
		
		if (EOGdStrategieSangria.STRATEGIE_CONSULTATION_CONTRATS_STRUCTURE_CODE.equals(perimetre.strategie().code())) {
			return new PerimetreStrategieAccesContratsPropreStructure(perimetre, ERXEC.newEditingContext());
		}

		return new PerimetreStrategieDefault();
	}
}
