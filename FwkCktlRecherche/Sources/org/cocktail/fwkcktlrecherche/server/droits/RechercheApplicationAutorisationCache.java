package org.cocktail.fwkcktlrecherche.server.droits;

import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;


public class RechercheApplicationAutorisationCache extends
		AutorisationsCache {

	public static final String APP_STRING_ID_SANGRIA = "SANGRIA";
	public static final String APP_STRING_ID_PHYSALIS = "PHYSALIS";

	public RechercheApplicationAutorisationCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}
	
	/* Debut droits pour Sangria */
	
	public Boolean hasDroitAccesSangria() {
		return hasDroitUtilisationOnFonction("SANGRIA");
	}
	
	public Boolean hasDroitConnaissanceSangria() {
		return hasDroitConnaissanceOnFonction("SANGRIA");
	}
	
	public Boolean hasDroitUtilisationContrats() {
		return hasDroitUtilisationOnFonction("ENTRE_DANS_CONT_RECH");
	}
	
	/* Droits */
	
	public Boolean hasDroits() {
		return (showStructure() || showProductionScientifique() || showAdministration());
	}
	
	/* Structures */
	
	public Boolean showStructure() {
		return (hasDroitUtilisationUniteRecherche() || hasDroitConnaissanceUniteRecherche() || 
				hasDroitUtilisationFederationsRecherche() || hasDroitConnaissanceFederationsRecherche() || 
				hasDroitUtilisationEcolesDoctorales() || hasDroitConnaissanceEcolesDoctorales() );
	}
	
	public Boolean hasDroitUtilisationUniteRecherche() {
		return hasDroitUtilisationOnFonction("UNITE_RECH");
	}
	
	public Boolean hasDroitConnaissanceUniteRecherche() {
		return hasDroitConnaissanceOnFonction("UNITE_RECH");
	}
	
	public Boolean hasDroitUtilisationFederationsRecherche() {
		return hasDroitUtilisationOnFonction("FEDERATIONS_RECH");
	}
	
	public Boolean hasDroitConnaissanceFederationsRecherche() {
		return hasDroitConnaissanceOnFonction("FEDERATIONS_RECH");
	}
	
	public Boolean hasDroitUtilisationEcolesDoctorales() {
		return hasDroitUtilisationOnFonction("ECOLES_DOCTORALES");
	}
	
	public Boolean hasDroitConnaissanceEcolesDoctorales() {
		return hasDroitConnaissanceOnFonction("ECOLES_DOCTORALES");
	}
	
	/* Production scientifique */
	
	public Boolean showProductionScientifique() {
		return (hasDroitUtilisationProjetsScientifiques() || hasDroitConnaissanceProjetsScientifiques() || 
				hasDroitUtilisationDemandeSubvention() || hasDroitConnaissanceDemandeSubvention() || 
				hasDroitUtilisationContratRecherche() || hasDroitConnaissanceContratRecherche() );
	}
	
	public Boolean hasDroitUtilisationProjetsScientifiques() {
		return hasDroitUtilisationOnFonction("PROJETS_SCIENTIF");
	}
	
	public Boolean hasDroitConnaissanceProjetsScientifiques() {
		return hasDroitConnaissanceOnFonction("PROJETS_SCIENTIF");
	}
	
	public Boolean hasDroitUtilisationDemandeSubvention() {
		return hasDroitUtilisationOnFonction("DEMANDE_SUBVENTION");
	}
	
	public Boolean hasDroitConnaissanceDemandeSubvention() {
		return hasDroitConnaissanceOnFonction("DEMANDE_SUBVENTION");
	}
	
	public Boolean hasDroitUtilisationContratRecherche() {
		return hasDroitUtilisationOnFonction("CONTRAT_RECHERCHE");
	}
	
	public Boolean hasDroitConnaissanceContratRecherche() {
		return hasDroitConnaissanceOnFonction("CONTRAT_RECHERCHE");
	}
	
	/* Administration */
	
	public Boolean showAdministration() {
		return (hasDroitUtilisationAllAdministration() || hasDroitConnaissanceAllAdministration());
	}
	
	public Boolean hasDroitUtilisationAllAdministration() {
		return hasDroitUtilisationOnFonction("ADMINISTRATION");
	}
	
	public Boolean hasDroitConnaissanceAllAdministration() {
		return hasDroitConnaissanceOnFonction("ADMINISTRATION");
	}
	
	/* Fin droits pour Sangria */
	
	/* Debut droits pour Physalis */
	
	public boolean hasDroitAccesPhysalis() {
		return hasDroitUtilisationOnFonction("PHYSALIS");
	}
	
	public boolean hasDroitConnaissancePhysalis() {
		return hasDroitConnaissanceOnFonction("PHYSALIS");
	}
	
	public Boolean hasDroitsPhysalis() {
		return (showGestionDoctorants() || showParametres() || showEditions());
	}
	
	public Boolean showGestionDoctorants() {
		return (hasDroitUtilisationAjouterEtudiant() || hasDroitConnaissanceAjouterEtudiant()
				|| hasDroitUtilisationRechercherDoctorant() || hasDroitConnaissanceRechercherDoctorant());
	}
	
	public Boolean hasDroitUtilisationAjouterEtudiant() {
		return hasDroitUtilisationOnFonction("AJOUT_ETUDIANT");
	}
	
	public Boolean hasDroitConnaissanceAjouterEtudiant() {
		return hasDroitConnaissanceOnFonction("AJOUT_ETUDIANT");
	}
	
	public Boolean hasDroitUtilisationRechercherDoctorant() {
		return hasDroitUtilisationOnFonction("RECHERCHER_DOCTORANT");
	}
	
	public Boolean hasDroitConnaissanceRechercherDoctorant() {
		return hasDroitConnaissanceOnFonction("RECHERCHER_DOCTORANT");
	}
	
	public Boolean showGestionListeTheses() {
		return (hasDroitUtilisationListeTheses() || hasDroitConnaissanceListeTheses());
	}
	
	public Boolean hasDroitUtilisationListeTheses() {
		return hasDroitUtilisationOnFonction("LISTE_THESES") || hasDroitUtilisationOnFonction("LISTE_THESES_MAX");
	}
	
	public Boolean hasDroitConnaissanceListeTheses() {
		return hasDroitConnaissanceOnFonction("LISTE_THESES");
	}
	
	public Boolean showGestionDirecteursThese() {
		return (hasDroitUtilisationDirecteursThese() || hasDroitConnaissanceDirecteursThese());
	}
	
	public Boolean hasDroitUtilisationDirecteursThese() {
		return hasDroitUtilisationOnFonction("GESTION_DIR_THESE");
	}
	
	public Boolean hasDroitConnaissanceDirecteursThese() {
		return hasDroitConnaissanceOnFonction("GESTION_DIR_THESE");
	}
	
	public Boolean showParametres() {
		return (hasDroitUtilisationGestionParametres() || hasDroitConnaissanceGestionParametres()
				|| hasDroitUtilisationGestionParametresEC() || hasDroitConnaissanceGestionParametresEC());
	}
	
	public Boolean hasDroitUtilisationGestionParametres() {
		return hasDroitUtilisationOnFonction("GESTION_PARAMETRES");
	}
	
	public Boolean hasDroitConnaissanceGestionParametres() {
		return hasDroitConnaissanceOnFonction("GESTION_PARAMETRES");
	}
	
	public Boolean hasDroitUtilisationGestionParametresEC() {
		return hasDroitUtilisationOnFonction("GESTION_PARAM_EC");
	}
	
	public Boolean hasDroitConnaissanceGestionParametresEC() {
		return hasDroitConnaissanceOnFonction("GESTION_PARAM_EC");
	}
	
	public Boolean hasDroitUtilisationGestionDonnees() {
		return true; //TODO: ajouter droit
		// return hasDroitUtilisationOnFonction("GESTION_DONNEES");
	}
	
	public Boolean hasDroitConnaissanceGestionDonnees() {
		return true;
		// return hasDroitConnaissanceOnFonction("GESTION_DONNEES");
	}
	
	public Boolean showEditions() {
		return (hasDroitUtilisationEnquetesSiredo() || hasDroitConnaissanceEnquetesSiredo()
				|| hasDroitUtilisationExportDonnees() || hasDroitConnaissanceExportDonnees());
	}
	
	public Boolean hasDroitUtilisationEnquetesSiredo() {
		return hasDroitUtilisationOnFonction("ENQUETES_SIREDO") || hasDroitUtilisationOnFonction("ENQUETES_SIREDO_MAX");
	}
	
	public Boolean hasDroitConnaissanceEnquetesSiredo() {
		return hasDroitConnaissanceOnFonction("ENQUETES_SIREDO");
	}
	
	public Boolean hasDroitVoirToutesEnquetesSiredo() {
		return hasDroitUtilisationOnFonction("ENQUETES_SIREDO_MAX");
	}
	
	public Boolean hasDroitUtilisationExportDonnees() {
		return hasDroitUtilisationOnFonction("EXPORT_EXCEL");
	}
	
	public Boolean hasDroitConnaissanceExportDonnees() {
		return hasDroitConnaissanceOnFonction("EXPORT_EXCEL");
	}
	
	public Boolean hasDroitVoirToutesTheses() {
		return hasDroitUtilisationOnFonction("LISTE_THESES_MAX");
	}
	
	public Boolean hasDroitEditerTheses() {
		return hasDroitUtilisationOnFonction("EDITER_THESE");
	}

	public Boolean hasDroitSupprimerThesesEtDoctorants() {
		return hasDroitUtilisationOnFonction("SUPPR_THESES_DOCT");
	}
	
	/* Fin droits pour Physalis */
}
