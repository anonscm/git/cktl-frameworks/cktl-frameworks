package org.cocktail.fwkcktlrecherche.server.droits;

import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Cette interface definit le service que doit apporter un fournisseur de perimetres pour un perimetre de donnees.
 * @param <T> perimetre de données (selon les applications concernées)
 */
public interface IPerimetreProvider<T extends EOGdPerimetre> {

	/**
	 * Construit un qualifier pour l'ensemble des strategies que connait le IPerimetreProvider
	 * @return l'ensemble des qualifiers pour toutes les strategies 
	 */
	EOQualifier getAllQualifiers();

	/**
	 * Construit un qualifier pour un sous-ensemble de strategies.
	 * @param strategies tableau contenant les strategies pour lesquelles on souhaite construire un qualifier
	 * @param userInfo utilisateur pour lequel on recherche les strategies
	 * @return EOQualifier qualifier construit à partir de la liste des stratégies
	 */
	 EOQualifier getAllQualifiers(NSArray<EOGdStrategieSangria> strategies, UserInfo userInfo);
}
