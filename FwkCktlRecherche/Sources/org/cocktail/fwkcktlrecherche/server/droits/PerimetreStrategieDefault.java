package org.cocktail.fwkcktlrecherche.server.droits;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Strategie par défaut.
 */
public class PerimetreStrategieDefault implements IPerimetreStrategie {

	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getQualifier() {
		return null;
	}

}
