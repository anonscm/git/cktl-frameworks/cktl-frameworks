package org.cocktail.fwkcktlrecherche.server;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlpersonne.common.metier.manager.CktlGrhumParamManager;
import org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

public class ParametresRecherche extends CktlGrhumParamManager {


	public static final String PARAM_PREFIXE_ECOLE_DOCTORALE = "org.cocktail.recherche.physalis.";
	public static final String PARAM_ECOLE_DOCTORALE_NB_THESE = "org.cocktail.recherche.physalis.nbThesesMaximum.";
	public static final String PARAM_ECOLE_DOCTORALE_QUOTITE = "org.cocktail.recherche.physalis.quotiteMinimale.";
	public static final String PARAM_ECOLE_DOCTORALE_TEST = "org.cocktail.recherche.physalis.test.";
	
	public static final String PARAM_C_STRUCTURE_ETAB_GESTIONNAIRE_THESE = "org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE";
	public static final String PARAM_FGRA_CODE_DOCTORAT = "org.cocktail.physalis.FGRA_CODE_DOCTORAT";
	public static final String PARAM_ETAB_DIPLOME_DEFAUT = "org.cocktail.physalis.ETAB_DIPLOME_DEFAUT";
	public static final String PARAM_INTITULE_MINISTERE = "org.cocktail.physalis.INTITULE_MINISTERE";
	public static final String PARAM_QUALITE_CHEF_ETABLISSEMENT = "org.cocktail.physalis.QUALITE_CHEF_ETABLISSEMENT";
	public static final String PARAM_VILLE_EDITION_DIPLOME = "org.cocktail.physalis.VILLE_EDITION_DIPLOME";
	
	public static final String ALLER_CHERCHER_DANS_PARTENAIRES_RECHERCHE = "org.cocktail.fwkcktlsangriaguiajax.contratsrecherche.AllerChercherDansPartenairesRecherche";
	public static final String PARAM_GROUPE_PARTENAIRES_RECHERCHE = "GROUPE_PARTENAIRES_RECHERCHE";
    public static final String GROUPE_ETABLISSEMENTS_GESTIONNAIRE_FINANCIER_KEY = "GROUPE_ETAB_GEST_FIN_AAP";
	public static final String POLE_COMPETITIVITE_KEY = "org.cocktail.sangria.groupes.polecompetitivite";

	public static final String GROUPE_VALIDATEURS_FINANCIERS_KEY = "org.cocktail.sangria.groupes.validateursfinanciers";
	public static final String GROUPE_VALIDATEURS_JURIDIQUES_KEY = "org.cocktail.sangria.groupes.validateursjuridiques";
	public static final String GROUPE_VALIDATEURS_VALO_KEY = "org.cocktail.sangria.groupes.validateursvalo";
	public static final String GROUPE_GESTIONNAIRES_CONTRATS_KEY = "org.cocktail.sangria.groupes.gestionnairescontrats";
	
	public static final String PARAM_PHYSALIS_UTILISATION = "org.cocktail.recherche.physalis.utilisation";

	public static final Map<String, String> groupesParTypeValidations;
	
	static {
		groupesParTypeValidations = new HashMap<String, String>();
		groupesParTypeValidations.put(EOValidationContrat.T_VALIDATION_FINANCIERE, GROUPE_VALIDATEURS_FINANCIERS_KEY);
		groupesParTypeValidations.put(EOValidationContrat.T_VALIDATION_JURIDIQUE, GROUPE_VALIDATEURS_JURIDIQUES_KEY);
		groupesParTypeValidations.put(EOValidationContrat.T_VALIDATION_VALO, GROUPE_VALIDATEURS_VALO_KEY);
	}
	
	public ParametresRecherche() {
		super(ERXEC.newEditingContext());
		addParam(PARAM_VILLE_EDITION_DIPLOME, "Ville d'édition du diplôme", "Ville", EOGrhumParametresType.texteLibre);
	}
	
	
	/**
	 * Methode retournant le libelle affiche pour un parametre donne
	 * @param paramKey le parametre dont on souhaite le libelle
	 * @return
	 */
	public static String getLibelleForParamKey(String paramKey) {
		
		if (paramKey.contains(ParametresRecherche.PARAM_ECOLE_DOCTORALE_NB_THESE)) {
			return "Nombre maximal de thèses par directeur";
		}
		if (paramKey.contains(ParametresRecherche.PARAM_ECOLE_DOCTORALE_QUOTITE)) {
			return "Quotité minimale pour un directeur de thèse";
		}
		if (paramKey.contains(ParametresRecherche.PARAM_ECOLE_DOCTORALE_TEST)) {
			return "Test";
		}
		return null;
	}
	
}
