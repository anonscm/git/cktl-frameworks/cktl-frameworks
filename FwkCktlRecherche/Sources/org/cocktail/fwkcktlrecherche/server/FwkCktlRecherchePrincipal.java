package org.cocktail.fwkcktlrecherche.server;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.eof.ERXEC;

/*
 * Pour utiliser une classe Principal il faut rajouter les proprietes suivantes dans le fichier build.properties
 * principalClass = com.mycoolframework.ExampleFrameworkPrincipal
 * project.principal.class = com.mycoolframework.ExampleFrameworkPrincipal
 * project.type = framework
 */

public class FwkCktlRecherchePrincipal extends ERXFrameworkPrincipal{

	public static final Logger LOG = Logger.getLogger(FwkCktlRecherchePrincipal.class);
	
	public EOEditingContext edc;
    
	// Registers the class as the framework principal
    static {
        setUpFrameworkPrincipalClass(FwkCktlRecherchePrincipal.class);
    }
	
	protected static FwkCktlRecherchePrincipal sharedInstance;
	
	public static FwkCktlRecherchePrincipal sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = (FwkCktlRecherchePrincipal)sharedInstance(FwkCktlRecherchePrincipal.class);
		}
		return sharedInstance;
	}
	
	public EOEditingContext edc() {
		if (edc == null) {
			edc = ERXEC.newEditingContext();
		}
		return edc;
	}

	@Override
    public void finishInitialization() {
		ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
		moduleRegister.addModule(new FwkCktlRechercheModule());
    }

}