package org.cocktail.fwkcktlrecherche.server.tasks;

import java.util.List;
import java.util.TimerTask;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

/**
 * 
 * @author jlafourc
 *
 */
public class MettreAJourDirecteursTheseHabilitesTask extends TimerTask {

	private static final String CODE_DIPLOME_HDR = "0731000";
	private static final String CODE_DIMPLOME_DOCTORAT_ETAT = "1000000";
	
	private static final String CODE_PROFESSEUR = "300";
	private static final String CODE_DIRECTEUR_RECHERCHE_CNRS = "223";
	private static final String CODE_DIRECTEUR_RECHERCHE = "H04";
	
	private NSArray<String> codesDiplomes = new NSArray<String>(CODE_DIPLOME_HDR, CODE_DIMPLOME_DOCTORAT_ETAT);
	private NSArray<String> codesCorps = new NSArray<String>(CODE_PROFESSEUR, CODE_DIRECTEUR_RECHERCHE_CNRS, CODE_DIRECTEUR_RECHERCHE);
	
	private NSArray<EODiplome> diplomes;
	
	private EOEditingContext edc = ERXEC.newEditingContext();
	private NSArray<EOAssociation> roleMembres;
	
	private EOIndividu individuGrhumCreateur;
	
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		init();
		if (GRHUtilities.isModuleGRHInUse(edc)) {
			
			EOQualifier qualifierDirecteurValide = EODirecteurTheseHabilite.TEM_VALIDE.eq(EODirecteurTheseHabilite.TEM_VALIDE_OUI);
			EOQualifier qualifierDirecteurInvalide = EODirecteurTheseHabilite.TEM_VALIDE.eq(EODirecteurTheseHabilite.TEM_VALIDE_NON);
			NSArray<EOIndividu> individusDirecteurTheseToutesUnitesAujourdhui = new NSMutableArray<EOIndividu>();
			
			for (EOStructure unite : getUnitesDeRecherche()) {
				
				NSArray<EOIndividu> membresUnite = getMembresUnite(unite);
				NSArray<EOIndividuDiplome> individuDiplomesOk = getIndividusAvecDiplomeOk(membresUnite);
				NSArray<EOIndividu> individusAvecDiplomeOk = (NSArray<EOIndividu>) individuDiplomesOk.valueForKey(EOIndividuDiplome.TO_INDIVIDU_KEY);
				
				NSArray<EOIndividu> individusAvecCorpsOk = getIndividusAvecCorpsOk(membresUnite);
				
				NSArray<EOIndividu> individusDirecteurTheseAujourdhui = new NSMutableArray<EOIndividu>();
				individusDirecteurTheseAujourdhui.addAll(individusAvecDiplomeOk);
				individusDirecteurTheseAujourdhui.addAll(individusAvecCorpsOk);
				individusDirecteurTheseAujourdhui = ERXArrayUtilities.arrayWithoutDuplicates(individusDirecteurTheseAujourdhui);
				individusDirecteurTheseToutesUnitesAujourdhui.addAll(individusDirecteurTheseAujourdhui);
				
				NSArray<EOIndividu> individusDejaDirecteurs = (NSArray<EOIndividu>) EODirecteurTheseHabilite.fetchAll(edc, qualifierDirecteurValide).valueForKey(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY);
				NSArray<EOIndividu> individusAnciensDirecteurs = (NSArray<EOIndividu>) EODirecteurTheseHabilite.fetchAll(edc, qualifierDirecteurInvalide).valueForKey(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY);
				NSArray<EOIndividu> individusDevenantDirecteurs = ERXArrayUtilities.arrayMinusArray(individusDirecteurTheseAujourdhui, individusDejaDirecteurs);
				
				// Ajout des nouveaux directeurs
				for (EOIndividu individu : individusDevenantDirecteurs) {
					if (individusAnciensDirecteurs.count() > 0 && individusAnciensDirecteurs.contains(individu)) {
						EOQualifier qualifier = EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS.dot(EOIndividu.NO_INDIVIDU).eq(individu.noIndividu());
						EODirecteurTheseHabilite directeurTheseHabilite = EODirecteurTheseHabilite.fetchFirstByQualifier(edc, qualifier);
						directeurTheseHabilite.setTemValide(EODirecteurTheseHabilite.TEM_VALIDE_OUI);
					} else {
						EOStructure ecoleDoctorale = getEdPourUnite(unite);
						if (ecoleDoctorale != null) {
							EODirecteurTheseHabilite directeurTheseHabilite = EODirecteurTheseHabilite.creerInstance(edc);
							directeurTheseHabilite.setToIndividuFwkpersRelationship(individu);
							directeurTheseHabilite.setToIndividuUtilisateurFwkpersRelationship(individuGrhumCreateur);
							directeurTheseHabilite.setToStructureLaboFwkpersRelationship(unite);
							if (unite.etablissement() != null && unite.etablissement().cRne() != null) {
								EORne rneEtablissement = EORne.fetchFirstByQualifier(edc, EORne.C_RNE.eq(unite.etablissement().cRne()));
								directeurTheseHabilite.setToRneFwkPersRelationship(rneEtablissement);
							} else {
								directeurTheseHabilite.setToStructureEtabFwkPersRelationship(unite.etablissement()); 
							}
							directeurTheseHabilite.setToStructureEdFwkpersRelationship(getEdPourUnite(unite));
							EOIndividuDiplome individuDiplome = ERXArrayUtilities.firstObject(ERXQ.filtered(individuDiplomesOk, EOIndividuDiplome.TO_INDIVIDU.eq(individu)));
							if (individuDiplome != null) {
								directeurTheseHabilite.setDateHdr(individuDiplome.dDiplome());
							}
							directeurTheseHabilite.setTemValide(EODirecteurTheseHabilite.TEM_VALIDE_OUI);
						} else {
							NSLog.out.appendln("Le directeur de thèse " + individu.getNomAndPrenom() + " n'a pas été ajouté car aucune école doctorale n'a été trouvée pour " + unite.llStructure());
						}
					}
					try {
						edc.saveChanges();
					} catch (Exception e) {
						edc.revert();
						NSLog.err.appendln("Erreur lors de l'ajout du directeur de thèse " + individu.getNomAndPrenom());
						e.printStackTrace();
					}
				}
				
			}
			
			//Dévalidation des anciens directeurs
			individusDirecteurTheseToutesUnitesAujourdhui = ERXArrayUtilities.arrayWithoutDuplicates(individusDirecteurTheseToutesUnitesAujourdhui);
			NSArray<EOIndividu> individusDirecteurs = (NSArray<EOIndividu>) EODirecteurTheseHabilite.fetchAll(edc, qualifierDirecteurValide).valueForKey(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY);
			NSArray<EOIndividu> individusDirecteursASupprimer = ERXArrayUtilities.arrayMinusArray(individusDirecteurs, individusDirecteurTheseToutesUnitesAujourdhui);
			for (EOIndividu individu : individusDirecteursASupprimer) {
				EOQualifier qualifier = EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS.dot(EOIndividu.NO_INDIVIDU).eq(individu.noIndividu());
				EODirecteurTheseHabilite directeurTheseHabilite = EODirecteurTheseHabilite.fetchFirstByQualifier(edc, qualifier);
				directeurTheseHabilite.setTemValide(EODirecteurTheseHabilite.TEM_VALIDE_NON);
				edc.saveChanges();
			}
		}
	}

	private NSArray<EOIndividu> getIndividusAvecCorpsOk(NSArray<EOIndividu> individus) {
		NSDictionary<EOIndividu, EOGrade> gradesPourIndividus = IndividusGradesService.creerNouvelleInstance(edc).gradesPourIndividus(individus, ERXTimestampUtilities.today());
		
		NSArray<EOIndividu> individusAvecCorpsOk = new NSMutableArray<EOIndividu>();
		
		for (EOIndividu individu : gradesPourIndividus.allKeys()) {
			EOGrade grade =  gradesPourIndividus.objectForKey(individu);
			if (codesCorps.contains(grade.toCorps().cCorps())) {
				individusAvecCorpsOk.add(individu);
			}
		}
		return individusAvecCorpsOk;
	}

	@SuppressWarnings("unchecked")
	private NSArray<EOIndividuDiplome> getIndividusAvecDiplomeOk(NSArray<EOIndividu> individus) {
		EOQualifier qualifierIndividuDiplome = 
			EOIndividuDiplome.TO_INDIVIDU.in(individus)
				.and(EOIndividuDiplome.TO_DIPLOME.in(diplomes));
		
		return EOIndividuDiplome.fetchAll(edc, qualifierIndividuDiplome, null);
	}
	
	private void init() {
		roleMembres = FactoryAssociation.shared().typesMembresUniteRechercheAssociation(edc).getFils(edc);
		diplomes = EODiplome.fetchAll(edc, ERXQ.in(EODiplome.C_DIPLOME_KEY, codesDiplomes));
		
		EOCompte compteGrhumCreateur = EOCompte.compteForLogin(edc, EOGrhumParametres.getFirstGrhumCreateur(edc).paramValue());
		individuGrhumCreateur = 
			EOIndividu.fetchFirstByQualifier(edc, EOIndividu.PERS_ID.eq(compteGrhumCreateur.persId()));
	}
	
	private List<EOStructure> getUnitesDeRecherche() {
		return StructuresRechercheHelper.unitesDeRecherche(edc);
	}

	@SuppressWarnings("unchecked")
	private NSArray<EOIndividu> getMembresUnite(EOStructure unite) {
		EOQualifier qualifier = 
			EORepartAssociation.TO_STRUCTURE.eq(unite)
			.and(EORepartAssociation.RAS_D_OUVERTURE.lte(ERXTimestampUtilities.today()))
			.and(
				EORepartAssociation.RAS_D_FERMETURE.gte(ERXTimestampUtilities.today())
				.or(EORepartAssociation.RAS_D_FERMETURE.isNull())
			)
			.and(EORepartAssociation.TO_ASSOCIATION.in(roleMembres));
		NSArray<EORepartAssociation> repartAssociations = EORepartAssociation.fetchAll(edc, qualifier);
		return ERXArrayUtilities.arrayBySelectingInstancesOfClass(
				(NSArray<IPersonne>) repartAssociations.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY),
				EOIndividu.class
			);
	}
	
	private EOStructure getEdPourUnite(EOStructure unite) {
		EOQualifier qualifier = 
				EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).eq(EOTypeGroupe.TGRP_CODE_ED)
				.and(EOStructure.TO_REPART_STRUCTURES_ELTS.dot(EORepartStructure.PERS_ID).eq(unite.persId()));
		return ERXArrayUtilities.firstObject(EOStructure.fetchAll(edc, qualifier));
	}

	
}
