package org.cocktail.fwkcktlrecherche.server;

import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;

import com.webobjects.eocontrol.EOEditingContext;

public class RechercheApplicationUser extends ApplicationUser {

	public RechercheApplicationUser(EOEditingContext ec,
			EOUtilisateur utilisateur) {
		super(ec, utilisateur);
	}

	public RechercheApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	public RechercheApplicationUser(EOEditingContext ec, String tyapStrId,
			EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
	}

	public RechercheApplicationUser(EOEditingContext ec, String tyapStrId,
			Integer persId) {
		super(ec, tyapStrId, persId);
	}



}
