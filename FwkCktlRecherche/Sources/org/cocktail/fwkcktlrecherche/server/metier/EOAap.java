/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.collections.functors.InvokerTransformer;
import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlnomenclatures.server.metier.factory.BaseFactoryNomenclature;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.controles.aap.ControleAap;
import org.cocktail.fwkcktlrecherche.server.metier.controles.aap.ControleEtablissementGestionnaireFinancier;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * 
 */
public class EOAap extends _EOAap {

	private static final long serialVersionUID = 8216453487035180941L;

	private static final String ACCEPTE = "pa";
	
	/**
	 * 
	 */
	public EOAap() {
		super();
	}


	private ContratUtilities contratUtilities = null;

	public static final String NUMERO_KEY = "numero";

	/**
	 * 
	 * 
	 * @return
	 */
	private ContratUtilities contratUtilities() {
		if (contratUtilities == null) {
			contratUtilities = ContratUtilities.creerNouvelleInstance(editingContext());
		}
		return contratUtilities;
	}

	/**
	 * 
	 * 
	 * @return le numero avec l'index si il est présent
	 */
	public String numero() {
		if (contrat().conIndex() == null) {
			return null;
		}
		return contrat().exerciceEtIndex();
	}

	public static final String OBJET_KEY = "objet";

	/**
	 * 
	 * 
	 * @return l'object du contrat lié
	 */
	public String objet() {
		return contrat().conObjet();
	}

	/**
	 * 
	 * 
	 * @param objet
	 *            l'objet
	 */
	public void setObjet(String objet) {
		contrat().setConObjet(objet);
	}

	/**
	 * 
	 * 
	 * @return l'objet court du contrat lié
	 */
	public String objetCourt() {
		return contrat().conObjetCourt();
	}

	/**
	 * 
	 * 
	 * @param objetCourt
	 *            l'objetCourt
	 */
	public void setObjetCourt(String objetCourt) {
		contrat().setConObjetCourt(objetCourt);
	}

	/**
	 * 
	 * 
	 * @return la date de création du contrat
	 */
	public NSTimestamp getDateDebut() {
		return contrat().conDateCreation();
	}

	/**
	 * 
	 * 
	 * @param dateDebut
	 *            la date de début
	 */
	public void setDateDebut(NSTimestamp dateDebut) {
		contrat().setConDateCreation(dateDebut);
	}

	/**
	 * 
	 * 
	 * @return la date de fin
	 */
	public NSTimestamp getDateFin() {
		return contrat().conDateCloture();
	}

	/**
	 * 
	 * 
	 * @param dateFin
	 *            la date de fin
	 */
	public void setDateFin(NSTimestamp dateFin) {
		contrat().setConDateCloture(dateFin);
	}

	/**
	 * 
	 * 
	 * @param structureRecherche
	 *            la structure de recherche a ajouter
	 * @throws Exception
	 *             dans le cas ou ca se passe mal
	 */
	public void ajouterStructureRechercheConcernee(EOStructure structureRecherche) throws Exception {
		contratUtilities().ajouterStructureRechercheConcerneePourContrat(structureRecherche, contrat());
	}

	/**
	 * 
	 * 
	 * @param structureRecherche
	 * @throws Exception
	 */
	public void supprimerStructureRechercheConcernee(EOStructure structureRecherche, Integer persId) throws Exception {
		contratUtilities().supprimerStructureRechercheConcerneePourContrat(structureRecherche, contrat(), persId);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getStructuresRecherchesConcernees() {
		return contratUtilities().getStructuresRecherchesConcerneesPourContrat(contrat());
	}

	// RESPONSABLES SCIENTIFIQUES

	/**
	 * 
	 * 
	 * @param responsable
	 * @param structureRecherche
	 * @throws Exception
	 */
	public void ajouterResponsableScientifiquePourStructureRecherche(EOIndividu responsable, EOStructure structureRecherche) throws Exception {
		contratUtilities().ajouterResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contrat());
	}

	/**
	 * 
	 * 
	 * @param responsable
	 * @param structureRecherche
	 * @throws Exception
	 */
	public void supprimerResponsableScientifiquePourStructureRecherche(EOIndividu responsable, EOStructure structureRecherche, Integer persId) throws Exception {
		contratUtilities().supprimerResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contrat(), persId);
	}

	/**
	 * 
	 * 
	 * @param structureRecherche
	 * @return
	 */
	public NSArray<EOIndividu> responsablesScientifiquesPourStructureRecherche(EOStructure structureRecherche) {
		return contratUtilities().responsablesScientifiquesPourStructureRechercheEtContrat(structureRecherche, contrat());
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public NSArray<EOIndividu> getResponsablesScientifiques() {
		return contratUtilities().getResponsablesScientifiquePourContrat(contrat());
	}

	// GESTIONNAIRE ADMINISTRATIF ET FINANCIER DE LA CONVENTION

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getGestionnaireAdminEtFinConvention() {
		EOAssociation role = FactoryAssociation.shared().gestionnaireAdminEtFinConventionAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());

	}

	/**
	 * 
	 * 
	 * @param nouveauGestionnaireAdminEtFinConvention
	 * @param utilisateurPersId
	 */
	public void setGestionnaireAdminEtFinConvention(EOIndividu nouveauGestionnaireAdminEtFinConvention, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().gestionnaireAdminEtFinConventionAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauGestionnaireAdminEtFinConvention, role, contrat(), utilisateurPersId);
	}

	// CHARGE DU DOSSIER

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getChargeDuDossier() {
		EOAssociation role = FactoryAssociation.shared().chargeDuDossierAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());

	}

	/**
	 * 
	 * 
	 * @param nouveauChargeDuDossier
	 * @param utilisateurPersId
	 */
	public void setChargeDuDossier(EOIndividu nouveauChargeDuDossier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().chargeDuDossierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauChargeDuDossier, role, contrat(), utilisateurPersId);
	}

	// PARTENAIRES DE LA CONVENTION

	/**
	 * 
	 * 
	 * @param partenaire
	 * @throws Exception
	 */
	public void ajouterPartenaire(IPersonne partenaire) throws Exception {
		contratUtilities().ajouterPartenaireDansContrat(partenaire, contrat());
	}

	/**
	 * 
	 * 
	 * @param partenaire
	 * @throws Exception
	 */
	public void supprimerPartenaire(IPersonne partenaire, Integer persIdUtilisateur) throws Exception {
		contratUtilities().supprimerPartenaireDansContrat(partenaire, contrat(), persIdUtilisateur);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public NSArray<IPersonne> getPartenaires() {
		return contratUtilities().getPartenairesDansContrat(contrat());
	}

	// COORDINATION INTERNE

	/**
	 * 
	 * 
	 * @return
	 */
	public EOStructure getCoordinationInterne() {
		EOAssociation role = FactoryAssociation.shared().coordinationInterneAssociation(editingContext());
		return (EOStructure) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouvelleCoordinationInterne
	 * @param utilisateurPersId
	 */
	public void setCoordinationInterne(EOStructure nouvelleCoordinationInterne, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().coordinationInterneAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouvelleCoordinationInterne, role, contrat(), utilisateurPersId);
	}

	// COORDINATION EXTERNE

	/**
	 * 
	 * 
	 * @return
	 */
	public EOStructure getCoordinationExterne() {
		EOAssociation role = FactoryAssociation.shared().coordinationExterneAssociation(editingContext());
		return (EOStructure) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouvelleCoordinationExterne
	 * @param utilisateurPersId
	 */
	public void setCoordinationExterne(EOStructure nouvelleCoordinationExterne, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().coordinationExterneAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouvelleCoordinationExterne, role, contrat(), utilisateurPersId);
	}

	// COORDINATEUR INTERNE

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getCoordinateurInterne() {
		EOAssociation role = FactoryAssociation.shared().coordinateurInterneAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouveauCoordinateurInterne
	 * @param utilisateurPersId
	 */
	public void setCoordinateurInterne(EOIndividu nouveauCoordinateurInterne, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().coordinateurInterneAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauCoordinateurInterne, role, contrat(), utilisateurPersId);
	}

	// COORDINATEUR EXTERNE

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getCoordinateurExterne() {
		EOAssociation role = FactoryAssociation.shared().coordinateurExterneAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouveauCoordinateurExterne
	 * @param utilisateurPersId
	 */
	public void setCoordinateurExterne(EOIndividu nouveauCoordinateurExterne, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().coordinateurExterneAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauCoordinateurExterne, role, contrat(), utilisateurPersId);

	}

	// FINANCEUR

	/**
	 * 
	 * 
	 * @return
	 */
	public EOStructure getFinanceur() {
		EOAssociation role = FactoryAssociation.shared().financeurAssociation(editingContext());
		return (EOStructure) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouveauFinanceur
	 * @param utilisateurPersId
	 */
	public void setFinanceur(EOStructure nouveauFinanceur, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().financeurAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauFinanceur, role, contrat(), utilisateurPersId);
	}

	// ETABLISSEMENT GESTIONNAIRE FINANCIER

	/**
	 * 
	 * 
	 * @return
	 */
	public EOStructure getEtablisssementGestionnaireFinancier() {
		EOAssociation role = FactoryAssociation.shared().etablissementGestionnaireFinancierAssociation(editingContext());
		return (EOStructure) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouvelEtablisssementGestionnaireFinancier
	 * @param utilisateurPersId
	 */
	public void setEtablisssementGestionnaireFinancier(EOStructure nouvelEtablisssementGestionnaireFinancier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().etablissementGestionnaireFinancierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouvelEtablisssementGestionnaireFinancier, role, contrat(), utilisateurPersId);
	}

	// DESTINATAIRE ALERTE SUIVI

	/**
	 * 
	 * 
	 * @param nouveauDestinataire
	 * @param utilisateurPersId
	 */
	public void ajouterDestinataireAlerteSuivi(EOIndividu nouveauDestinataire, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviAssociation(editingContext());
		contratUtilities().ajouterPartenaireAvecRoleAuContrat(nouveauDestinataire, role, contrat(), utilisateurPersId);
	}

	/**
	 * 
	 * 
	 * @param destinataire
	 * @param utilisateurPersId
	 */
	public void supprimerDestinataireAlerteSuivi(EOIndividu destinataire, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviAssociation(editingContext());
		contratUtilities().supprimerRolePourPartenaireDansContrat(role, destinataire, contrat(), utilisateurPersId);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public NSArray<EOIndividu> getDestinatairesAlerteSuivi() {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviAssociation(editingContext());
		return ERXArrayUtilities.arrayBySelectingInstancesOfClass(contratUtilities().getPersonnesAvecRoleDansContrat(role, contrat()), EOIndividu.class);
	}

	// DESTINATAIRE ALERTE SUIVI VALO

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getDestinataireAlerteSuiviValo() {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviValoAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouveauDestinataire
	 * @param utilisateurPersId
	 */
	public void setDestinataireAlerteSuiviValo(EOIndividu nouveauDestinataire, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviValoAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauDestinataire, role, contrat(), utilisateurPersId);

	}

	// DESTINATAIRE ALERTE SUIVI FINANCIER

	/**
	 * 
	 * 
	 * @return
	 */
	public EOIndividu getDestinataireAlerteSuiviFinancier() {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviFinancierAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * 
	 * 
	 * @param nouveauDestinataire
	 * @param utilisateurPersId
	 */
	public void setDestinataireAlerteSuiviFinancier(EOIndividu nouveauDestinataire, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().destinataireAlerteSuiviFinancierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauDestinataire, role, contrat(), utilisateurPersId);

	}

	// LABORATOIRE PRINCIPAL

	/**
	 * 
	 * 
	 * @return
	 */
	public EOStructure getLaboratoirePrincipal() {
		return ERXArrayUtilities.firstObject(getStructuresRecherchesConcernees());
	}

	/**
	 * 
	 * 
	 * @param nouveauLabo
	 * @param utilisateurPersId
	 */
	public void setLaboratoirePrincipal(EOStructure nouveauLabo, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().laboratoirePrincipalAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauLabo, role, contrat(), utilisateurPersId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlrecherche.server.metier._EOAap#setProgrammeRelationship
	 * (org.cocktail.fwkcktlrecherche.server.metier.EOProgramme)
	 */
	@Override
	public void setProgrammeRelationship(EOProgramme value) {
		super.setProgrammeRelationship(value);
		if (value == null)
			setSousProgrammeRelationship(null);
	}

	// POLES DE COMPETITIVITE

	/**
	 * 
	 * 
	 * @param nouveauPole
	 * @param utilisateurPersId
	 */
	public void ajouterPoleDeCompetitivite(EOStructure nouveauPole, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().poleDeCompetitivite(editingContext());
		contratUtilities().ajouterPartenaireAvecRoleAuContrat(nouveauPole, role, contrat(), utilisateurPersId);
	}

	/**
	 * 
	 * 
	 * @param pole
	 * @param utilisateurPersId
	 */
	public void supprimerPoleDeCompetitivite(EOStructure pole, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().poleDeCompetitivite(editingContext());
		contratUtilities().supprimerRolePourPartenaireDansContrat(role, pole, contrat(), utilisateurPersId);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public NSArray<EOStructure> getPolesDeCompetitivite() {
		EOAssociation role = FactoryAssociation.shared().poleDeCompetitivite(editingContext());
		return ERXArrayUtilities.arrayBySelectingInstancesOfClass(
				contratUtilities().getPersonnesAvecRoleDansContrat(role, contrat()),
				EOStructure.class
			);
	}

	/**
	 * 
	 */
	public void validateForSave() throws ValidationException {

		super.validateForSave();

		if (!aapSupprime()) {
			if (contrat().exerciceCocktail() == null) {
				throw new ValidationException("L'année doit être renseignée");
			}
			if (StringUtils.isBlank(objet())) {
				throw new ValidationException("L'objet ne doit pas être vide");
			}
			if (getStructuresRecherchesConcernees().isEmpty()) {
				throw new ValidationException("Il doit y avoir au moins un laboratoire concerné");
			}
			if (getResponsablesScientifiques().isEmpty()) { 
				throw new ValidationException("Il doit y avoir au moins un responsable scientifique");
			}
		}

	}
	
	/**
	 * @return true si les champs d'acception sont remplis qu'on est 
	 * pas en cours de modifs
	 */
	public Boolean isAccepte() {
		Boolean isAccepte = StringUtils.equals(etatAcceptation(), ACCEPTE);
		Boolean isDateSaisie = dateRefusAcceptation() != null;
		Boolean enCoursDeModif = editingContext().hasChanges();
		
		return isAcceptable() && isAccepte && isDateSaisie && !enCoursDeModif;
	}
	
	public Boolean isAcceptable() {
		return checkIsAcceptable().isEmpty();
	}
	
	/**
	 * @return le resultat des controles sur l'aap
	 */
	@SuppressWarnings("unchecked")
	public List<ResultatControle> checkIsAcceptable() {
		List<Class<? extends ControleAap>> classesDeControles = new ArrayList<Class<? extends ControleAap>>();
		classesDeControles.add(ControleEtablissementGestionnaireFinancier.class);
		Transformer invokerTransformer = TransformerUtils.invokerTransformer("check", new Class[]{EOAap.class}, new Object[]{this});
		Transformer transformer = TransformerUtils.chainedTransformer(TransformerUtils.instantiateTransformer(), invokerTransformer);
		List<ResultatControle> resultats = (List<ResultatControle>) CollectionUtils.collect(classesDeControles,  transformer);
		CollectionUtils.filter(
			resultats, 
			PredicateUtils.notPredicate(
				PredicateUtils.equalPredicate(ResultatControle.RESULTAT_OK)
			)
		);
		return resultats;
	}
	
	
	/**
	 * @return true si l'objet est sur le point d'être enregistré
	 * et que les données sur l'acceptation sont saisie 
	 */
	public Boolean isAcceptationEnCours() {
		Boolean isAccepte = StringUtils.equals(etatAcceptation(), ACCEPTE);
		Boolean isDateSaisie = dateRefusAcceptation() != null;
		Boolean enCoursDeModif =  editingContext().hasChanges()	;
		
		return isAccepte && isDateSaisie && enCoursDeModif;
	}
	
	public Boolean isContratLieCree() {
		return contratLie() != null;
	}
	
	public Double getTotalDemande() {
		NSArray<EORepartTypeFinancementAap> repartTypeFinancements = typesFinancementAap();
		NSArray<Float> montantDemandes = (NSArray<Float>) repartTypeFinancements.valueForKey(EORepartTypeFinancementAap.MONTANT_DEMANDE.key());
		montantDemandes = ERXArrayUtilities.removeNullValues(montantDemandes);
		BigDecimal totalMontants = (BigDecimal) montantDemandes.valueForKey(ERXKey.sum().key());
		return totalMontants.doubleValue();
	}
	
	public Double getTotalObtenu() {
		NSArray<EORepartTypeFinancementAap> repartTypeFinancements = typesFinancementAap();
		NSArray<Float> montantObtenus = (NSArray<Float>) repartTypeFinancements.valueForKey(EORepartTypeFinancementAap.MONTANT_OBTENU.key());
		montantObtenus = ERXArrayUtilities.removeNullValues(montantObtenus);
		BigDecimal totalMontants = (BigDecimal) montantObtenus.valueForKey(ERXKey.sum().key());
		return totalMontants.doubleValue();
	}

	/**
	 * LES NOMENCLATURES.
	 */

	public static class Nomenclatures extends BaseFactoryNomenclature {

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getListeTypesFinancement() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.typefinancements").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getTypeSuiviValo() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.typesuivivalorisation").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getMoyensDeDepot() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.moyendepotdossier").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getTypesEtatsAcceptation() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.etatacceptationrefus").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getTypeEtatMontage() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.typeetatmontage").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getTypesDenvoisAuFinanceur() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.moyenenvoidossiers").asMapOfStrings();
		}

		/**
		 * 
		 * 
		 * @return
		 */
		public static NSDictionary<String, String> getListeDesAvis() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.aap.listeavis").asMapOfStrings();
		}

	}

}
