/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEvaluationDocument.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOEvaluationDocument extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "EvaluationDocument";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();

	// Attributs non visibles
	public static final String DOCUMENT_ID_KEY = "documentId";
	public static final String EVALUATION_ID_KEY = "evaluationId";
	public static final String ID_KEY = "id";

	//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";

	public static final String DOCUMENT_ID_COLKEY = "DOCUMENT_ID";
	public static final String EVALUATION_ID_COLKEY = "EVALUATION_ID";
	public static final String ID_COLKEY = "id";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> DOCUMENT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("document");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr> EVALUATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr>("evaluation");
	
	public static final String DOCUMENT_KEY = DOCUMENT.key();
	public static final String EVALUATION_KEY = EVALUATION.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public org.cocktail.fwkcktlged.serveur.metier.EODocument document() {
	    return (org.cocktail.fwkcktlged.serveur.metier.EODocument) storedValueForKey(DOCUMENT_KEY);
	}

	public void setDocumentRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = document();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOCUMENT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, DOCUMENT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr evaluation() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr) storedValueForKey(EVALUATION_KEY);
	}

	public void setEvaluationRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr oldValue = evaluation();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EVALUATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, EVALUATION_KEY);
	    }
	}
	
	public static EOEvaluationDocument createEvaluationDocument(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, java.lang.Integer persIdCreation, org.cocktail.fwkcktlged.serveur.metier.EODocument document, org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr evaluation) {
		EOEvaluationDocument eo = (EOEvaluationDocument) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluationDocument.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
		eo.setDocumentRelationship(document);
		eo.setEvaluationRelationship(evaluation);
		return eo;
	}

	public EOEvaluationDocument localInstanceIn(EOEditingContext editingContext) {
		return (EOEvaluationDocument) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOEvaluationDocument creerInstance(EOEditingContext editingContext) {
		return (EOEvaluationDocument) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOEvaluationDocument.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOEvaluationDocument> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOEvaluationDocument>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOEvaluationDocument fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEvaluationDocument fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOEvaluationDocument> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEvaluationDocument eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOEvaluationDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOEvaluationDocument fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOEvaluationDocument> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEvaluationDocument eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOEvaluationDocument fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOEvaluationDocument eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOEvaluationDocument ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOEvaluationDocument fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAllEvaluationDocuments(EOEditingContext editingContext) {
		return _EOEvaluationDocument.fetchAllEvaluationDocuments(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchAllEvaluationDocuments(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOEvaluationDocument.fetchEvaluationDocuments(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationDocument> fetchEvaluationDocuments(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOEvaluationDocument> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOEvaluationDocument>(_EOEvaluationDocument.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOEvaluationDocument> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOEvaluationDocument fetchEvaluationDocument(EOEditingContext editingContext, String keyName, Object value) {
		return _EOEvaluationDocument.fetchEvaluationDocument(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOEvaluationDocument fetchEvaluationDocument(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOEvaluationDocument> eoObjects = _EOEvaluationDocument.fetchEvaluationDocuments(editingContext, qualifier, null);
	    EOEvaluationDocument eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one EvaluationDocument that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
