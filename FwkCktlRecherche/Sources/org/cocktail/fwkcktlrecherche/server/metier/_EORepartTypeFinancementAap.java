/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartTypeFinancementAap.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartTypeFinancementAap extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartTypeFinancementAap";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "rtfcOrdre";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_DEMANDE = new er.extensions.eof.ERXKey<java.lang.Float>("montantDemande");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_OBTENU = new er.extensions.eof.ERXKey<java.lang.Float>("montantObtenu");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_FINANCEMENT = new er.extensions.eof.ERXKey<java.lang.String>("typeFinancement");
	
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String MONTANT_DEMANDE_KEY = MONTANT_DEMANDE.key();
	public static final String MONTANT_OBTENU_KEY = MONTANT_OBTENU.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String TYPE_FINANCEMENT_KEY = TYPE_FINANCEMENT.key();

	// Attributs non visibles
	public static final String AAP_ORDRE_KEY = "aapOrdre";
	public static final String RTFC_ORDRE_KEY = "rtfcOrdre";

	//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String MONTANT_DEMANDE_COLKEY = "MONTANT_DEMANDE";
	public static final String MONTANT_OBTENU_COLKEY = "MONTANT_OBTENU";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIF";
	public static final String TYPE_FINANCEMENT_COLKEY = "T_FINANCEMENT";

	public static final String AAP_ORDRE_COLKEY = "AAP_ORDRE";
	public static final String RTFC_ORDRE_COLKEY = "RTFC_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOAap> AAP = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOAap>("aap");
	
	public static final String AAP_KEY = AAP.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Float montantDemande() {
		return (java.lang.Float) storedValueForKey(MONTANT_DEMANDE_KEY);
	}

	public void setMontantDemande(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_DEMANDE_KEY);
	}
	
	public java.lang.Float montantObtenu() {
		return (java.lang.Float) storedValueForKey(MONTANT_OBTENU_KEY);
	}

	public void setMontantObtenu(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_OBTENU_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.String typeFinancement() {
		return (java.lang.String) storedValueForKey(TYPE_FINANCEMENT_KEY);
	}

	public void setTypeFinancement(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_FINANCEMENT_KEY);
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOAap aap() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOAap) storedValueForKey(AAP_KEY);
	}

	public void setAapRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOAap value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOAap oldValue = aap();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AAP_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, AAP_KEY);
	    }
	}
	
	public static EORepartTypeFinancementAap createRepartTypeFinancementAap(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, java.lang.Integer persIdCreation, java.lang.String typeFinancement, org.cocktail.fwkcktlrecherche.server.metier.EOAap aap) {
		EORepartTypeFinancementAap eo = (EORepartTypeFinancementAap) EOUtilities.createAndInsertInstance(editingContext, _EORepartTypeFinancementAap.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
		eo.setTypeFinancement(typeFinancement);
		eo.setAapRelationship(aap);
		return eo;
	}

	public EORepartTypeFinancementAap localInstanceIn(EOEditingContext editingContext) {
		return (EORepartTypeFinancementAap) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartTypeFinancementAap creerInstance(EOEditingContext editingContext) {
		return (EORepartTypeFinancementAap) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartTypeFinancementAap.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartTypeFinancementAap> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartTypeFinancementAap>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartTypeFinancementAap fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartTypeFinancementAap fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartTypeFinancementAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartTypeFinancementAap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartTypeFinancementAap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartTypeFinancementAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartTypeFinancementAap fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartTypeFinancementAap eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartTypeFinancementAap ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartTypeFinancementAap fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAllRepartTypeFinancementAaps(EOEditingContext editingContext) {
		return _EORepartTypeFinancementAap.fetchAllRepartTypeFinancementAaps(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchAllRepartTypeFinancementAaps(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartTypeFinancementAap.fetchRepartTypeFinancementAaps(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> fetchRepartTypeFinancementAaps(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartTypeFinancementAap> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartTypeFinancementAap>(_EORepartTypeFinancementAap.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartTypeFinancementAap fetchRepartTypeFinancementAap(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartTypeFinancementAap.fetchRepartTypeFinancementAap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartTypeFinancementAap fetchRepartTypeFinancementAap(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartTypeFinancementAap> eoObjects = _EORepartTypeFinancementAap.fetchRepartTypeFinancementAaps(editingContext, qualifier, null);
	    EORepartTypeFinancementAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartTypeFinancementAap that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
