/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODoctorantStatut.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODoctorantStatut extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DoctorantStatut";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idStatut";

	public static final er.extensions.eof.ERXKey<java.lang.String> CODE_STATUT = new er.extensions.eof.ERXKey<java.lang.String>("codeStatut");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIB_STATUT = new er.extensions.eof.ERXKey<java.lang.String>("libStatut");
	
	public static final String CODE_STATUT_KEY = CODE_STATUT.key();
	public static final String LIB_STATUT_KEY = LIB_STATUT.key();

	// Attributs non visibles
	public static final String ID_STATUT_KEY = "idStatut";

	//Colonnes dans la base de donnees
	public static final String CODE_STATUT_COLKEY = "CODE_STATUT";
	public static final String LIB_STATUT_COLKEY = "LIB_STATUT";

	public static final String ID_STATUT_COLKEY = "ID_STATUT";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> TO_DOCTORANTS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant>("toDoctorants");
	
	public static final String TO_DOCTORANTS_KEY = TO_DOCTORANTS.key();

	// Accessors methods
	public java.lang.String codeStatut() {
		return (java.lang.String) storedValueForKey(CODE_STATUT_KEY);
	}

	public void setCodeStatut(java.lang.String value) {
	    takeStoredValueForKey(value, CODE_STATUT_KEY);
	}
	
	public java.lang.String libStatut() {
		return (java.lang.String) storedValueForKey(LIB_STATUT_KEY);
	}

	public void setLibStatut(java.lang.String value) {
	    takeStoredValueForKey(value, LIB_STATUT_KEY);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> toDoctorants() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_DOCTORANTS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> toDoctorants(EOQualifier qualifier) {
		return toDoctorants(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> toDoctorants(EOQualifier qualifier, boolean fetch) {
	    return toDoctorants(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> toDoctorants(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EODoctorant.TO_DOCTORANT_STATUT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EODoctorant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toDoctorants();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToDoctorantsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorant object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_DOCTORANTS_KEY);
	}

	public void removeFromToDoctorantsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorant object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DOCTORANTS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorant createToDoctorantsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Doctorant");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DOCTORANTS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorant) eo;
	}

	public void deleteToDoctorantsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorant object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DOCTORANTS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToDoctorantsRelationships() {
		java.util.Enumeration objects = toDoctorants().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToDoctorantsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EODoctorant) objects.nextElement());
	    }
	}

	public static EODoctorantStatut createDoctorantStatut(EOEditingContext editingContext, java.lang.String codeStatut, java.lang.String libStatut) {
		EODoctorantStatut eo = (EODoctorantStatut) EOUtilities.createAndInsertInstance(editingContext, _EODoctorantStatut.ENTITY_NAME);    
		eo.setCodeStatut(codeStatut);
		eo.setLibStatut(libStatut);
		return eo;
	}

	public EODoctorantStatut localInstanceIn(EOEditingContext editingContext) {
		return (EODoctorantStatut) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODoctorantStatut creerInstance(EOEditingContext editingContext) {
		return (EODoctorantStatut) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODoctorantStatut.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantStatut> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantStatut>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODoctorantStatut fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODoctorantStatut fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODoctorantStatut> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODoctorantStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODoctorantStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODoctorantStatut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODoctorantStatut> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODoctorantStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODoctorantStatut fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODoctorantStatut eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODoctorantStatut ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODoctorantStatut fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAllDoctorantStatuts(EOEditingContext editingContext) {
		return _EODoctorantStatut.fetchAllDoctorantStatuts(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchAllDoctorantStatuts(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODoctorantStatut.fetchDoctorantStatuts(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantStatut> fetchDoctorantStatuts(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantStatut> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantStatut>(_EODoctorantStatut.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODoctorantStatut> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODoctorantStatut fetchDoctorantStatut(EOEditingContext editingContext, String keyName, Object value) {
		return _EODoctorantStatut.fetchDoctorantStatut(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODoctorantStatut fetchDoctorantStatut(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODoctorantStatut> eoObjects = _EODoctorantStatut.fetchDoctorantStatuts(editingContext, qualifier, null);
	    EODoctorantStatut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DoctorantStatut that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
