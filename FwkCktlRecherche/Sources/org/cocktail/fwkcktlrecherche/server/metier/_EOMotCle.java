/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMotCle.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOMotCle extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "MotCle";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("libelle");
	
	public static final String LIBELLE_KEY = LIBELLE.key();

	// Attributs non visibles
	public static final String ID_KEY = "id";

	//Colonnes dans la base de donnees
	public static final String LIBELLE_COLKEY = "MC_LIBELLE";

	public static final String ID_COLKEY = "MC_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINES_SCIENTIFIQUES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domainesScientifiques");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique>("repartMotCleDomaineScientifiques");
	
	public static final String DOMAINES_SCIENTIFIQUES_KEY = DOMAINES_SCIENTIFIQUES.key();
	public static final String REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY = REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES.key();

	// Accessors methods
	public java.lang.String libelle() {
		return (java.lang.String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_KEY);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> domainesScientifiques() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(DOMAINES_SCIENTIFIQUES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> domainesScientifiques(EOQualifier qualifier) {
		return domainesScientifiques(qualifier, null);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> domainesScientifiques(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> results;
			results = domainesScientifiques();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
		return results;
	}
	  
	public void addToDomainesScientifiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique object) {
		addObjectToBothSidesOfRelationshipWithKey(object, DOMAINES_SCIENTIFIQUES_KEY);
	}

	public void removeFromDomainesScientifiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, DOMAINES_SCIENTIFIQUES_KEY);
	}

	public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique createDomainesScientifiquesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fwkpers_DomaineScientifique");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, DOMAINES_SCIENTIFIQUES_KEY);
	    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) eo;
	}

	public void deleteDomainesScientifiquesRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, DOMAINES_SCIENTIFIQUES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllDomainesScientifiquesRelationships() {
		java.util.Enumeration objects = domainesScientifiques().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteDomainesScientifiquesRelationship((org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> repartMotCleDomaineScientifiques() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> repartMotCleDomaineScientifiques(EOQualifier qualifier) {
		return repartMotCleDomaineScientifiques(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> repartMotCleDomaineScientifiques(EOQualifier qualifier, boolean fetch) {
	    return repartMotCleDomaineScientifiques(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> repartMotCleDomaineScientifiques(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique.MOT_CLE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = repartMotCleDomaineScientifiques();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToRepartMotCleDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique object) {
		addObjectToBothSidesOfRelationshipWithKey(object, REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public void removeFromRepartMotCleDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique createRepartMotCleDomaineScientifiquesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartMotCleDomaineScientifique");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique) eo;
	}

	public void deleteRepartMotCleDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_MOT_CLE_DOMAINE_SCIENTIFIQUES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllRepartMotCleDomaineScientifiquesRelationships() {
		java.util.Enumeration objects = repartMotCleDomaineScientifiques().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteRepartMotCleDomaineScientifiquesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique) objects.nextElement());
	    }
	}

	public static EOMotCle createMotCle(EOEditingContext editingContext, java.lang.String libelle) {
		EOMotCle eo = (EOMotCle) EOUtilities.createAndInsertInstance(editingContext, _EOMotCle.ENTITY_NAME);    
		eo.setLibelle(libelle);
		return eo;
	}

	public EOMotCle localInstanceIn(EOEditingContext editingContext) {
		return (EOMotCle) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOMotCle creerInstance(EOEditingContext editingContext) {
		return (EOMotCle) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOMotCle.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOMotCle> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOMotCle>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOMotCle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMotCle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOMotCle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMotCle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOMotCle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOMotCle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOMotCle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMotCle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOMotCle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOMotCle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOMotCle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOMotCle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAllMotCles(EOEditingContext editingContext) {
		return _EOMotCle.fetchAllMotCles(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOMotCle> fetchAllMotCles(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOMotCle.fetchMotCles(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOMotCle> fetchMotCles(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOMotCle> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOMotCle>(_EOMotCle.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOMotCle> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOMotCle fetchMotCle(EOEditingContext editingContext, String keyName, Object value) {
		return _EOMotCle.fetchMotCle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOMotCle fetchMotCle(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOMotCle> eoObjects = _EOMotCle.fetchMotCles(editingContext, qualifier, null);
	    EOMotCle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one MotCle that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
