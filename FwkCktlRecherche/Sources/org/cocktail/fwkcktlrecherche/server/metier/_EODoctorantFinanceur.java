/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODoctorantFinanceur.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODoctorantFinanceur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DoctorantFinanceur";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idFinanceur";

	

	// Attributs non visibles
	public static final String CP_ORDRE_KEY = "cpOrdre";
	public static final String ID_FINANCEMENT_KEY = "idFinancement";
	public static final String ID_FINANCEUR_KEY = "idFinanceur";
	public static final String RAS_ID_KEY = "rasId";

	//Colonnes dans la base de donnees

	public static final String CP_ORDRE_COLKEY = "CP_ORDRE";
	public static final String ID_FINANCEMENT_COLKEY = "ID_FINANCEMENT";
	public static final String ID_FINANCEUR_COLKEY = "ID_FINANCEUR";
	public static final String RAS_ID_COLKEY = "RAS_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> TO_CONTRAT_PARTENAIRE = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("toContratPartenaire");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement> TO_DOCTORANT_FINANCEMENT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement>("toDoctorantFinancement");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> TO_REPART_ASSOCIATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>("toRepartAssociation");
	
	public static final String TO_CONTRAT_PARTENAIRE_KEY = TO_CONTRAT_PARTENAIRE.key();
	public static final String TO_DOCTORANT_FINANCEMENT_KEY = TO_DOCTORANT_FINANCEMENT.key();
	public static final String TO_REPART_ASSOCIATION_KEY = TO_REPART_ASSOCIATION.key();

	// Accessors methods
	public org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaire() {
	    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire) storedValueForKey(TO_CONTRAT_PARTENAIRE_KEY);
	}

	public void setToContratPartenaireRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.ContratPartenaire oldValue = toContratPartenaire();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_PARTENAIRE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_PARTENAIRE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement toDoctorantFinancement() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement) storedValueForKey(TO_DOCTORANT_FINANCEMENT_KEY);
	}

	public void setToDoctorantFinancementRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement oldValue = toDoctorantFinancement();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_FINANCEMENT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_FINANCEMENT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation toRepartAssociation() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation) storedValueForKey(TO_REPART_ASSOCIATION_KEY);
	}

	public void setToRepartAssociationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation oldValue = toRepartAssociation();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REPART_ASSOCIATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REPART_ASSOCIATION_KEY);
	    }
	}
	
	public static EODoctorantFinanceur createDoctorantFinanceur(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaire, org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement toDoctorantFinancement, org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation toRepartAssociation) {
		EODoctorantFinanceur eo = (EODoctorantFinanceur) EOUtilities.createAndInsertInstance(editingContext, _EODoctorantFinanceur.ENTITY_NAME);    
		eo.setToContratPartenaireRelationship(toContratPartenaire);
		eo.setToDoctorantFinancementRelationship(toDoctorantFinancement);
		eo.setToRepartAssociationRelationship(toRepartAssociation);
		return eo;
	}

	public EODoctorantFinanceur localInstanceIn(EOEditingContext editingContext) {
		return (EODoctorantFinanceur) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODoctorantFinanceur creerInstance(EOEditingContext editingContext) {
		return (EODoctorantFinanceur) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODoctorantFinanceur.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantFinanceur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantFinanceur>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODoctorantFinanceur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODoctorantFinanceur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODoctorantFinanceur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODoctorantFinanceur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODoctorantFinanceur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODoctorantFinanceur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODoctorantFinanceur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODoctorantFinanceur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODoctorantFinanceur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODoctorantFinanceur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODoctorantFinanceur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODoctorantFinanceur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAllDoctorantFinanceurs(EOEditingContext editingContext) {
		return _EODoctorantFinanceur.fetchAllDoctorantFinanceurs(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchAllDoctorantFinanceurs(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODoctorantFinanceur.fetchDoctorantFinanceurs(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinanceur> fetchDoctorantFinanceurs(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantFinanceur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantFinanceur>(_EODoctorantFinanceur.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODoctorantFinanceur> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODoctorantFinanceur fetchDoctorantFinanceur(EOEditingContext editingContext, String keyName, Object value) {
		return _EODoctorantFinanceur.fetchDoctorantFinanceur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODoctorantFinanceur fetchDoctorantFinanceur(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODoctorantFinanceur> eoObjects = _EODoctorantFinanceur.fetchDoctorantFinanceurs(editingContext, qualifier, null);
	    EODoctorantFinanceur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DoctorantFinanceur that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
