package org.cocktail.fwkcktlrecherche.server.metier.factory;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

public class FactoryAssociation {

	// LIBELLES DES ASSOCIATIONS
	public final static String CHARGE_DU_DOSSIER_LIBELLE = "CHARGE DU DOSSIER";
	public final static String RESPONSABLE_SCIENTIFIQUE_LIBELLE = "RESPONSABLE SCIENTIFIQUE";
	public final static String LABORATOIRE_CONCERNE_LIBELLE = "LABORATOIRE ASSOCIÉ";
	public final static String GESTIONNAIRE_ADMIN_ET_FINANCIER_CONVENTION_LIBELLE = "GESTIONNAIRE ADMIN. ET FIN. CONVENTION";
	public final static String PARTENAIRE_LIBELLE = "PARTENAIRE";
	public final static String RAPPORTEUR_DESIGNE_LIBELLE = "RAPPORTEUR DESIGNE";
	public final static String COORDINATION_INTERNE_LIBELLE = "COORDINATION INTERNE";
	public final static String COORDINATION_EXTERNE_LIBELLE = "COORDINATION EXTERNE";
	public final static String COORDINATEUR_INTERNE_LIBELLE = "COORDINATEUR INTERNE";
	public final static String COORDINATEUR_EXTERNE_LIBELLE = "COORDINATEUR EXTERNE";
	public final static String FINANCEUR_LIBELLE = "FINANCEUR";
	public final static String ETABLISSEMENT_GESTIONNAIRE_FINANCIER_LIBELLE = "ETABLISSEMENT GESTIONNAIRE FINANCIER";
	public final static String DESTINATAIRE_ALERTE_SUIVI_LIBELLE = "DESTINATAIRE ALERTE SUIVI";
	public final static String DESTINATAIRE_ALERTE_SUIVI_VALO_LIBELLE = "DESTINATAIRE ALERTE SUIVI VALO";
	public final static String DESTINATAIRE_ALERTE_SUIVI_FINANCE_LIBELLE = "DESTINATAIRE ALERTE FINANCE";
	public final static String CREATEUR_DU_DOSSIER_LIBELLE = "CREATEUR DU DOSSIER";
	public final static String VALIDATEUR_JURIDIQUE_LIBELLE = "VALIDATEUR JURIDIQUE";
	public final static String VALIDATEUR_FINANCIER_LIBELLE = "VALIDATEUR FINANCIER";
	public final static String VALIDATEUR_VALO_LIBELLE = "VALIDATEUR VALO";
	public final static String VALIDATEUR_SCIENTIFIQUE_LIBELLE = "VALIDATEUR SCIENTIFIQUE";
	public final static String LABORATOIRE_PRINCIPAL_LIBELLE = "LABORATOIRE PRINCIPAL";
	public final static String COORDINATEUR_DU_PROJET_LIBELLE = "COORDINATEUR DU PROJET";
	public final static String PORTEUR_DU_PROJET_LIBELLE = "PORTEUR DU PROJET";
	public final static String PORTEUR_DU_PROJET_ADJOINT_LIBELLE = "PORTEUR DU PROJET ADJOINT";
	public final static String TUTELLE_LIBELLE = "TUTELLE DE";
	public final static String TUTELLE_PRINCIPALE_LIBELLE = "TUTELLE PRINCIPALE";
	public final static String ETABLISSEMENT_RATTACHEMENT_PRINCIPAL_LIBELLE = "ETABLISSEMENT RATTACHEMENT PRINC.";
	public final static String ETABLISSEMENT_RATTACHEMENT_LIBELLE = "ETABLISSEMENT RATTACHEMENT";
	public final static String EQUIPE_DE_RECHERCHE_LIBELLE = "EQUIPE DE RECHERCHE";
	public final static String RESPONSABLE_ADMINISTRATIF = "RESPONSABLE ADMINISTRATIF";
	public final static String GESTIONNAIRE_ADMIN_ET_FIN = "GESTIONNAIRE ADMIN. ET FIN.";
	public final static String MEMBRE_LIBELLE = "MEMBRE";
	public final static String MEMBRE_PERMANENT_LIBELLE = "MEMBRE PERMANENT";
	public final static String MEMBRE_NON_PERMANENT_LIBELLE = "MEMBRE NON PERMANENT";
	public final static String DIRECTION_LIBELLE = "DIRECTION";
	public final static String DIRECTEUR_ADJOINT_LIBELLE = "DIRECTEUR-ADJOINT";
	public final static String TYPES_MEMBRES_UNITE_LIBELLE = "TYPES MEMBRES UNITE";
	public final static String TYPES_MEMBRES_STRUCTURE_RECHERCHE_LIBELLE = "TYPES MEMBRES STRUCTURE RECHERCHE";

	// ASS_CODE des associations
	public final static String CHARGE_DU_DOSSIER_CODE = "CHARGDOSSIER";
	public final static String RESPONSABLE_SCIENTIFIQUE_CODE = "RESPSCIENTIF";
	public final static String LABORATOIRE_CONCERNE_CODE = "LABORATOIRE ";
	public final static String GESTIONNAIRE_ADMIN_ET_FINANCIER_CONVENTION_CODE = "GESTADMFICON";
	public final static String PARTENAIRE_CODE = "PARTENAIRE";
	public final static String RAPPORTEUR_DESIGNE_CODE = "RAPPODESIGNE";
	public final static String COORDINATION_INTERNE_CODE = "COORDINATIOI";
	public final static String COORDINATION_EXTERNE_CODE = "COORDINATIOE";
	public final static String COORDINATEUR_INTERNE_CODE = "COORDINTERNE";
	public final static String COORDINATEUR_EXTERNE_CODE = "COORDEXTERNE";
	public final static String FINANCEUR_CODE = "FINANCEUR";
	public final static String ETABLISSEMENT_GESTIONNAIRE_FINANCIER_CODE = "ETABGESTFINA";
	public final static String DESTINATAIRE_ALERTE_SUIVI_CODE = "DESTALRTSUIV";
	public final static String DESTINATAIRE_ALERTE_SUIVI_VALO_CODE = "DESTALRTSUVA";
	public final static String DESTINATAIRE_ALERTE_SUIVI_FINANCE_CODE = "DESTALRTFINA";
	public final static String CREATEUR_DU_DOSSIER_CODE = "CREATDOSSIER";
	public final static String VALIDATEUR_JURIDIQUE_CODE = "VALIDJURIDIQ";
	public final static String VALIDATEUR_FINANCIER_CODE = "VALIDFINANCI";
	public final static String VALIDATEUR_VALO_CODE = "VALIDVALO";
	public final static String VALIDATEUR_SCIENTIFIQUE_CODE = "VALIDSCIENTI";
	// public final static String LABORATOIRE_PRINCIPAL_CODE =
	// "LABORATOIRE PRINCIPAL";
	public final static String COORDINATEUR_DU_PROJET_CODE = "COORDINTPROJ";
	public final static String PORTEUR_DU_PROJET_CODE = "PORTEURPROJ";
	public final static String PORTEUR_DU_PROJET_ADJOINT_CODE = "PORTEURPROJA";
	public final static String TUTELLE_CODE = "TUTELLEDE";
	public final static String TUTELLE_PRINCIPALE_CODE = "TUTELLEPRINC";
	public final static String ETABLISSEMENT_RATTACHEMENT_PRINCIPAL_CODE = "ETABRATTPRIN";
	public final static String ETABLISSEMENT_RATTACHEMENT_CODE = "ETABRATTACHE";
	public final static String EQUIPE_DE_RECHERCHE_CODE = "EQUIPERECHER";
	public final static String RESPONSABLE_ADMINISTRATIF_CODE = "RESPADMIN";
	public final static String RESPONSABLE_CODE = "RESPONSABLE";
	public final static String GESTIONNAIRE_ADMIN_ET_FIN_CODE = "GESTADMINFIN";
	public final static String MEMBRE_CODE = "MEMBRE";
	public final static String MEMBRE_PERMANENT_CODE = "MEMBRE P";
	public final static String MEMBRE_NON_PERMANENT_CODE = "MEMBRE NP";
	public final static String DIRECTION_CODE = "DIRECTION";
	public final static String DIRECTEUR_ADJOINT_CODE = "DIRECTEUR-AD";
	public final static String TYPES_MEMBRES_UNITE_CODE = "TYP_MEM_UNI";
	public final static String TYPES_MEMBRES_STRUCTURE_RECHERCHE_CODE = "TYPE_MEM_SR";
	public static final String ECOLE_DOCTORALE_RATTACHEMENT_CODE = "D_ED_R";
	public static final String LABORATOIRE_THESE_CODE = "D_LAB_THESE";
	public static final String DOCTORANT_CODE = "DOCT";
	public static final String ETABLISSEMENT_ACCUEIL_CODE = "D_ETAB_ACC";
	public static final String ETABLISSEMENT_COTUTELLE_CODE = "D_COT_ETAB";
	public static final String ENCADRANT_COTUTELLE_CODE = "D_COT_ENCA";
	public static final String DIRECTION_THESE_CODE = "D_DIR";
	public static final String ROLES_JURY_CODE = "D_TYPE_JURY";
	public static final String RAPPORTEUR_THESE_CODE = "D_JURY_RAP";
	public static final String MEMBRE_JURY_CODE = "D_JR_MEM";
	public static final String PRESIDENT_JURY_CODE = "D_JR_PRES";
	public static final String INVITE_JURY_CODE = "D_JR_INV";
	public static final String DIRECTEUR_THESE_CODE = "D_DIR_THESE";
	public static final String CO_ENCADRANT_THESE_CODE = "D_DIR_COENC";
	public static final String EMPLOYEUR_THESE = "D_EMPLOYEUR";
	public static final String FINANCEUR_THESE = "D_FINANCEUR";
	public static final String AFFECTATION = "AFFECT";
	public static final String POLECOMPET = "POLECOMPET";
	public static final String SECRETAIRE_ADMINISTRATIVE_CODE = "SECRÉTAIRE A";

	private EOEditingContext ec = null;
	private NSMutableDictionary<String, EOAssociation> associations = null;

	private static FactoryAssociation sharedFactoryAssociation = null;

	public FactoryAssociation(EOEditingContext ec) {
		this.ec = ec;
		associations = new NSMutableDictionary<String, EOAssociation>();
	}

	public static FactoryAssociation shared() {
		if (sharedFactoryAssociation == null) {
			sharedFactoryAssociation = new FactoryAssociation(ERXEC.newEditingContext());
		}
		return sharedFactoryAssociation;
	}

	public EOQualifier qualifierByLibelle(String libelle) {
		return ERXQ.equals(EOAssociation.ASS_LIBELLE_KEY, libelle);
	}

	public EOQualifier qualifierByCode(String assCode) {
		return ERXQ.like(EOAssociation.ASS_CODE_KEY, assCode);
	}

	public EOAssociation associationWithLibelle(String libelle) {
		EOAssociation association = (EOAssociation) associations.valueForKey(libelle);

		if (association == null) {
			association = EOAssociation.fetchByQualifier(ec, qualifierByLibelle(libelle));
			associations.takeValueForKey(association, libelle);
		}

		return association;
	}

	public EOAssociation associationWithCode(String assCode) {
		EOAssociation association = (EOAssociation) associations.valueForKey(assCode);

		if (association == null) {
			association = EOAssociation.fetchByQualifier(ec, qualifierByCode(assCode));
			associations.takeValueForKey(association, assCode);
		}

		return association;
	}

	public NSArray<EOAssociation> associationsFilleDe(EOAssociation associationPere, EOEditingContext editingContext) {
		NSArray<EOAssociation> roles = null;

		if (associationPere != null) {
			roles = associationPere.getFils(editingContext);
		}
		return roles;

	}

	public EOAssociation chargeDuDossierAssociation(EOEditingContext editingContext) {
		return associationWithCode(CHARGE_DU_DOSSIER_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation responsableScientifiqueAssociation(EOEditingContext editingContext) {
		return associationWithCode(RESPONSABLE_SCIENTIFIQUE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation laboratoireConcerneAssociation(EOEditingContext editingContext) {
		return associationWithCode(LABORATOIRE_CONCERNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation gestionnaireAdminEtFinConventionAssociation(EOEditingContext editingContext) {
		return associationWithCode(GESTIONNAIRE_ADMIN_ET_FINANCIER_CONVENTION_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation partenaireAssociation(EOEditingContext editingContext) {
		return associationWithCode(PARTENAIRE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation rapporteurDesigneAssociation(EOEditingContext editingContext) {
		return associationWithCode(RAPPORTEUR_DESIGNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coordinationInterneAssociation(EOEditingContext editingContext) {
		return associationWithCode(COORDINATION_INTERNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coordinationExterneAssociation(EOEditingContext editingContext) {
		return associationWithCode(COORDINATION_EXTERNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coordinateurInterneAssociation(EOEditingContext editingContext) {
		return associationWithCode(COORDINATEUR_INTERNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coordinateurExterneAssociation(EOEditingContext editingContext) {
		return associationWithCode(COORDINATEUR_EXTERNE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation financeurAssociation(EOEditingContext editingContext) {
		return associationWithCode(FINANCEUR_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation etablissementGestionnaireFinancierAssociation(EOEditingContext editingContext) {
		return associationWithCode(ETABLISSEMENT_GESTIONNAIRE_FINANCIER_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation destinataireAlerteSuiviAssociation(EOEditingContext editingContext) {
		return associationWithCode(DESTINATAIRE_ALERTE_SUIVI_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation destinataireAlerteSuiviValoAssociation(EOEditingContext editingContext) {
		return associationWithCode(DESTINATAIRE_ALERTE_SUIVI_VALO_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation destinataireAlerteSuiviFinancierAssociation(EOEditingContext editingContext) {
		return associationWithCode(DESTINATAIRE_ALERTE_SUIVI_FINANCE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation createurDuDossierAssociation(EOEditingContext editingContext) {
		return associationWithCode(CREATEUR_DU_DOSSIER_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation validateurJuridiqueAssociation(EOEditingContext editingContext) {
		return associationWithCode(VALIDATEUR_JURIDIQUE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation validateurFinancierAssociation(EOEditingContext editingContext) {
		return associationWithCode(VALIDATEUR_FINANCIER_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation validateurValoAssociation(EOEditingContext editingContext) {
		return associationWithCode(VALIDATEUR_VALO_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation validateurScientifiqueAssociation(EOEditingContext editingContext) {
		return associationWithCode(VALIDATEUR_SCIENTIFIQUE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation laboratoirePrincipalAssociation(EOEditingContext editingContext) {
		return associationWithLibelle(LABORATOIRE_PRINCIPAL_LIBELLE).localInstanceIn(editingContext);
	}

	public EOAssociation porteurAssociation(EOEditingContext editingContext) {
		return associationWithCode(PORTEUR_DU_PROJET_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coordinateurProjetAssociation(EOEditingContext editingContext) {
		return associationWithCode(COORDINATEUR_DU_PROJET_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation porteurProjetAdjointAssociation(EOEditingContext editingContext) {
		return associationWithCode(PORTEUR_DU_PROJET_ADJOINT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation tutelleAssociation(EOEditingContext editingContext) {
		return associationWithCode(TUTELLE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation etablissementRattachementPrincipalAssociation(EOEditingContext editingContext) {
		return associationWithCode(ETABLISSEMENT_RATTACHEMENT_PRINCIPAL_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation etablissementRattachementAssociation(EOEditingContext editingContext) {
		return associationWithCode(ETABLISSEMENT_RATTACHEMENT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation tutellePrincipaleAssociation(EOEditingContext editingContext) {
		return associationWithCode(TUTELLE_PRINCIPALE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation equipeDeRechercheAssociation(EOEditingContext editingContext) {
		return associationWithCode(EQUIPE_DE_RECHERCHE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation responsableAdministratifAssociation(EOEditingContext editingContext) {
		return associationWithCode(RESPONSABLE_ADMINISTRATIF_CODE).localInstanceIn(editingContext);
	}
	
	public EOAssociation secretraireAdministrativeAssociation(EOEditingContext editingContext) {
		return associationWithCode(SECRETAIRE_ADMINISTRATIVE_CODE).localInstanceIn(editingContext);
	}

	 public EOAssociation responsableAssociation(EOEditingContext editingContext) {
	    return associationWithCode(RESPONSABLE_CODE).localInstanceIn(editingContext);
	  }
	
	public EOAssociation gestionnaireAdminEtFinAssociation(EOEditingContext editingContext) {
		return associationWithCode(GESTIONNAIRE_ADMIN_ET_FIN_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation membreAssociation(EOEditingContext editingContext) {
		// return
		// associationWithCode(MEMBRE_CODE).localInstanceIn(editingContext);
		return associationWithCode(MEMBRE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation directionAssociation(EOEditingContext editingContext) {
		return associationWithCode(DIRECTION_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation membrePermanentAssociation(EOEditingContext editingContext) {
		return associationWithCode(MEMBRE_PERMANENT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation membreNonPermanentAssociation(EOEditingContext editingContext) {
		return associationWithCode(MEMBRE_NON_PERMANENT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation directeurAdjointAssociation(EOEditingContext editingContext) {
		return associationWithCode(DIRECTEUR_ADJOINT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation typesMembresUniteRechercheAssociation(EOEditingContext editingContext) {
		return associationWithCode(TYPES_MEMBRES_UNITE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation typesMembresStructureRechercheAssociation(EOEditingContext editingContext) {
		return associationWithCode(TYPES_MEMBRES_STRUCTURE_RECHERCHE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation ecoleDoctoraleAssociation(EOEditingContext editingContext) {
		return associationWithCode(ECOLE_DOCTORALE_RATTACHEMENT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation laboratoireTheseAssociation(EOEditingContext editingContext) {
		return associationWithCode(LABORATOIRE_THESE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation doctorantAssociation(EOEditingContext editingContext) {
		return associationWithCode(DOCTORANT_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation etablissementCotutelleAssociation(EOEditingContext editingContext) {
		return associationWithCode(ETABLISSEMENT_COTUTELLE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation encadrantCotutelleAssociation(EOEditingContext editingContext) {
		return associationWithCode(ENCADRANT_COTUTELLE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation directionTheseAssociation(EOEditingContext editingContext) {
		return associationWithCode(DIRECTION_THESE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation rolesJuryAssociation(EOEditingContext editingContext) {
		return associationWithCode(ROLES_JURY_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation rapporteurTheseAssociation(EOEditingContext editingContext) {
		return associationWithCode(RAPPORTEUR_THESE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation membreJuryAssociation(EOEditingContext editingContext) {
		return associationWithCode(MEMBRE_JURY_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation presidentJuryAssociation(EOEditingContext editingContext) {
		return associationWithCode(PRESIDENT_JURY_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation inviteJuryAssociation(EOEditingContext editingContext) {
		return associationWithCode(INVITE_JURY_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation directeurTheseAssociation(EOEditingContext editingContext) {
		return associationWithCode(DIRECTEUR_THESE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation coEncadrantTheseAssociation(EOEditingContext editingContext) {
		return associationWithCode(CO_ENCADRANT_THESE_CODE).localInstanceIn(editingContext);
	}

	public EOAssociation employeurThese(EOEditingContext editingContext) {
		return associationWithCode(EMPLOYEUR_THESE).localInstanceIn(editingContext);
	}

	public EOAssociation financeurThese(EOEditingContext editingContext) {
		return associationWithCode(FINANCEUR_THESE).localInstanceIn(editingContext);
	}
	
	public EOAssociation affectation(EOEditingContext editingContext) {
		return associationWithCode(AFFECTATION).localInstanceIn(editingContext);
	}
	
	public EOAssociation poleDeCompetitivite(EOEditingContext editingContext) {
		return associationWithCode(POLECOMPET).localInstanceIn(editingContext);
	}
	
	public EOAssociation etablissementAccueil(EOEditingContext editingContext) {
		return associationWithCode(ETABLISSEMENT_ACCUEIL_CODE).localInstanceIn(editingContext);
	}

}
