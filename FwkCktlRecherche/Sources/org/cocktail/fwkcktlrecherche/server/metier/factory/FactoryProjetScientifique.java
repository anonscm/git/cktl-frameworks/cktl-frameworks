package org.cocktail.fwkcktlrecherche.server.metier.factory;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.Projet;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.TypeProjet;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryProjet;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXTimestampUtilities;

public class FactoryProjetScientifique extends Factory {

	private static FactoryProjet factoryProjet;
	
	public FactoryProjetScientifique(EOEditingContext ec) {
		super(ec, false);
	}
	
	private static FactoryProjet factoryProjet() {
		if(factoryProjet == null) {
			factoryProjet = new FactoryProjet(null, false);
		}
		return factoryProjet;
	}
	
	public static EOProjetScientifique creerProjetScientifiqueVide(EOEditingContext ec, String intituleDuProjet, Integer persId) throws ExceptionUtilisateur, Exception {
		factoryProjet().setEditingContext(ec);
		
		EOProjetScientifique projetScientifique = EOProjetScientifique.creerInstance(ec);		
				
		// Recup et affectation type du projet
		EOQualifier qualifierForTypeProjet =  TypeProjet.TPJT_CODE.eq("RECHERCHE");
		TypeProjet typeProjet = TypeProjet.fetch(ec, qualifierForTypeProjet);
		
		// Creation du projet
		Projet projet = factoryProjet().creerProjet(null, typeProjet, "");
		projetScientifique.setProjetRelationship(projet);
				
		// Creation de la convention pour le projet
		EOIndividu utlIndividu =  EOIndividu.fetchFirstByQualifier(ec, EOIndividu.PERS_ID.eq(persId));
		EOUtilisateur utilisateur = EOUtilisateur.fetchByQualifier(ec, ERXQ.equals(EOUtilisateur.PERS_ID_KEY, persId));
		
		FactoryConvention factoryConvention = new FactoryConvention(ec, false);

		Contrat contrat = factoryConvention.creerConventionVierge(utilisateur, (EOStructure) utlIndividu.getEtablissementsAffectation(null).lastObject());
		ec.insertObject(contrat);

		contrat.setTypeClassificationContratRelationship(TypeClassificationContrat.typeClassificationContratForCode(ec, "PROJ_SCIEN"));
		contrat.setExerciceCocktailRelationship(EOExerciceCocktail.fetchFirstByQualifier(ec, ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, EOExercice.getExerciceOuvert(ec).exeExercice())));
		
		TypeContrat typeContrat = TypeContrat.fetch(ec, TypeContrat.TYCON_ID_INTERNE.eq("CONT_PROJ"));
		contrat.setTypeContratRelationship(typeContrat);
		
		NSArray<EOStructure> services = utlIndividu.getServices();
		
		if(services.isEmpty()) {
			contrat.setCentreResponsabiliteRelationship(null, persId);
		}
		else {
			contrat.setCentreResponsabiliteRelationship(services.lastObject(), persId);
		}
		
		// Mise en relation du contrat et du projet
		factoryProjet().ajouterContrat(projet, contrat);
		
		// Attributs du projet scientifique

		projetScientifique.setIntitule(intituleDuProjet);
		
		projetScientifique.setProjetSupprime(false);
		
		projetScientifique.setDCreation(ERXTimestampUtilities.today());
		projetScientifique.setDModification(ERXTimestampUtilities.today());
		
		contrat.avenantZero().setAvtDateDeb(null);
		contrat.avenantZero().setAvtDateFin(null);
		
		projetScientifique.setPersIdCreation(utlIndividu.persId());
		projetScientifique.setPersIdModification(utlIndividu.persId());
		
		return projetScientifique;
		
		
		
	}

}
