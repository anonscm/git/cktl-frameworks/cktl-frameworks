package org.cocktail.fwkcktlrecherche.server.metier.factory;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class FactoryAap extends Factory {

	public FactoryConvention factoryConvention;
	
	public FactoryAap(EOEditingContext ec, boolean withlog) {
		super(ec, withlog);
		factoryConvention = new FactoryConvention(ec, false);
	}

	@SuppressWarnings("unchecked")
	public EOAap creerAapVide(String objet, Integer persId) throws Exception {
		

		EOAap aap = EOAap.creerInstance(ec);
		
		// Récupération du centre de responsabilité
		// Juste pour la création de la convention, on ne  s'en sert pas après
		EOQualifier qualifierPourIndividu = ERXQ.equals(EOIndividu.PERS_ID_KEY, persId);
		EOIndividu utlIndividu =  (EOIndividu) EOIndividu.fetchFirstByQualifier(ec, qualifierPourIndividu);
		
		EOQualifier qualifierPourUtilisateur = ERXQ.equals(EOUtilisateur.PERS_ID_KEY, persId);
		EOUtilisateur createur = EOUtilisateur.fetchFirstByQualifier(ec, qualifierPourUtilisateur);
		
		
		aap.setAapSupprime(false);
	
	
		
		
		Contrat contrat = factoryConvention.creerConventionVierge(createur, (EOStructure) utlIndividu.getEtablissementsAffectation(null).lastObject());
		
		contrat.setTypeClassificationContratRelationship(
		    TypeClassificationContrat.fetch(ec, TypeClassificationContrat.TCC_CODE.eq(TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_AAP))
		);
		contrat.setTypeContratRelationship(
		    TypeContrat.fetch(ec, TypeContrat.TYCON_ID_INTERNE.eq("CONT_COLLAB_RECH"))
		);
		
		contrat.setConObjet(objet);
		contrat.setExerciceCocktailRelationship(EOExerciceCocktail.fetchFirstByQualifier(ec, ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, EOExercice.getExerciceOuvert(ec).exeExercice())));

		contrat.setCentreResponsabiliteRelationship((EOStructure) ERXArrayUtilities.firstObject(utlIndividu.getServices()), persId);
		
		
		aap.setContratRelationship(contrat);

    	aap.setTtc(false);
		
		aap.setEtatAcceptation("enc");
				
		aap.setPersIdCreation(persId);
		aap.setPersIdModification(persId);
		
		
		
		aap.setDCreation(ERXTimestampUtilities.today());
		aap.setDModification(ERXTimestampUtilities.today());
		
		return aap;
		
	}
	
	public void supprimerAap(EOAap aap) throws Exception {
		
	  Contrat aapContrat =  aap.contrat();
		
		try {
			factoryConvention.supprimerConvention(aapContrat);
		} catch (ExceptionUtilisateur e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		aap.setAapSupprime(true);
		
	}
}
