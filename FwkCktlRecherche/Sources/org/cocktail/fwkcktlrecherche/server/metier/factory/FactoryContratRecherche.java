package org.cocktail.fwkcktlrecherche.server.metier.factory;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXTimestampUtilities;

public class FactoryContratRecherche extends Factory {

	public FactoryConvention factoryConvention;
	
	public FactoryContratRecherche(EOEditingContext ec, boolean withlog) {
		super(ec, withlog);
		factoryConvention = new FactoryConvention(ec, false);
	}

	public EOContratRecherche creerContratVide(String objet, Integer persId) throws ExceptionUtilisateur, Exception {
		
		EOQualifier qualifierForIndividu = ERXQ.equals(EOIndividu.PERS_ID_KEY, persId);
		EOIndividu inviduUtilisateur = EOIndividu.fetchFirstByQualifier(ec, qualifierForIndividu);
		
		EOQualifier qualifierForUtilisateur = ERXQ.equals(EOUtilisateur.PERS_ID_KEY, persId);
		EOUtilisateur utilisateur = EOUtilisateur.fetchFirstByQualifier(ec, qualifierForUtilisateur);
		
		// Creation 
		EOContratRecherche contratRecherche = EOContratRecherche.creerInstance(ec);
		
		// Creation de la convention pour le CSC
		Contrat contrat = factoryConvention.creerConventionVierge(utilisateur, null);
		ec.insertObject(contrat);
		
		// Mise en relation du CSC et de la convention
		contratRecherche.setContratRelationship(contrat);
		contrat.setTypeClassificationContratRelationship(TypeClassificationContrat.typeClassificationContratForCode(ec, "CONV"));
		contrat.setConObjet(objet);
		contrat.setExerciceCocktailRelationship(EOExerciceCocktail.fetchFirstByQualifier(ec, ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, EOExercice.getExerciceOuvert(ec).exeExercice())));
	
		creerValidationFinancierePourContrat(ec, contratRecherche, persId);
		creerValidationJuridiquePourContrat(ec, contratRecherche, persId);
		creerValidationValorisationPourContrat(ec, contratRecherche, persId);
		
		contratRecherche.setContratSupprime(false);
		
		contratRecherche.setDOuvertureDossier(ERXTimestampUtilities.today());
		contratRecherche.setCreateurDuDossier(inviduUtilisateur, persId);
		contratRecherche.setSensMontantFinancier(EOContratRecherche.CODE_SENS_MONTANT_FINANCIER_ENTRANT);
		//contratRecherche.setChargeDuDossier(inviduUtilisateur, persId);
		
		// Mise à jour des meta-données sur la manipulation des EO
		
		contratRecherche.setPersIdCreation(persId);
		contratRecherche.setPersIdModification(persId);
		
		contratRecherche.setDCreation(getDateJour());
		contratRecherche.setDModification(getDateJour());
		
		contrat.avenantZero().setAvtDateDeb(null);
		contrat.avenantZero().setAvtDateFin(null);
		
		return contratRecherche;
	}
	
	public void supprimerContratRecherche(EOContratRecherche contratRecherche) throws Exception {
		Contrat contrat =  contratRecherche.contrat();
		
		try {
			factoryConvention.supprimerConvention(contrat);
		} catch (ExceptionUtilisateur e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		contratRecherche.setContratSupprime(true);
		
	}
	
	public void creerContratRechercheAvecAap(EOAap aap, Integer persId) throws ExceptionUtilisateur, Exception {
		
		// Création du contrat
		EOContratRecherche contratRecherche = creerContratVide(aap.objet(), persId);
		
		contratRecherche.contrat().setExerciceCocktailRelationship(aap.contrat().exerciceCocktail());
		
		contratRecherche.setObjetCourt(aap.objetCourt());
		
		contratRecherche.setDomaineScientifiqueRelationship(aap.domaineScientifique());
		 
		for (EOStructure labo : aap.getStructuresRecherchesConcernees()) {
			contratRecherche.ajouterStructureConcernee(labo, persId);
			for (EOIndividu responsable : aap.responsablesScientifiquesPourStructureRecherche(labo)) {
				contratRecherche.ajouterResponsableScientifiquePourStructureRecherche(responsable, labo); 
			}
		}
		
		for (IPersonne partenaire : aap.getPartenaires()) {
			contratRecherche.ajouterPartenaire(partenaire);
		}
		
		if (!contratRecherche.partenaires().contains(aap.getFinanceur())) {
			contratRecherche.ajouterPartenaire(aap.getFinanceur());
		}
		
	}
	
	// METHODES POUR LA CREATION DES DIFFERENTES VALIDATIONS D'UN CONTRAT
	
	public static EOValidationContrat creerValidationJuridiquePourContrat(EOEditingContext editingContext, EOContratRecherche contratRecherche, Integer utilisateurPersId) {
		return creerValidationPourContrat(editingContext, contratRecherche, EOValidationContrat.T_VALIDATION_JURIDIQUE, utilisateurPersId);
	}
	
	public static EOValidationContrat creerValidationFinancierePourContrat(EOEditingContext editingContext, EOContratRecherche contratRecherche, Integer utilisateurPersId) {
		return creerValidationPourContrat(editingContext, contratRecherche, EOValidationContrat.T_VALIDATION_FINANCIERE, utilisateurPersId);
	}

	public static EOValidationContrat creerValidationValorisationPourContrat(EOEditingContext editingContext, EOContratRecherche contratRecherche, Integer utilisateurPersId) {
		return creerValidationPourContrat(editingContext, contratRecherche, EOValidationContrat.T_VALIDATION_VALO, utilisateurPersId);
	}
	
	public static EOValidationContrat creerValidationPourContrat(EOEditingContext editingContext, EOContratRecherche contratRecherche, String typeValidation, Integer utilisateurPersId) {
		EOValidationContrat validationContrat = EOValidationContrat.creerInstance(editingContext);
		validationContrat.setContratRechercheRelationship(contratRecherche);
		validationContrat.setValide(false);
		validationContrat.setTypeValidation(typeValidation);
		validationContrat.setPersIdCreation(utilisateurPersId);
		validationContrat.setPersIdModification(utilisateurPersId);
		validationContrat.setDCreation(MyDateCtrl.getDateJour());
		validationContrat.setDModification(MyDateCtrl.getDateJour());
		return validationContrat;
	}
	
}
