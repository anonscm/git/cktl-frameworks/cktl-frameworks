/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.foundation.ERXTimestampUtilities;
import er.extensions.validation.ERXValidationFactory;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class EOProgramme extends _EOProgramme {

	private static final long serialVersionUID = 3340204365656311793L;

	private static final char CHAR_SEPARATEUR_CODE = '.';
	private static final String STRING_SEPARATEUR_CODE = String.valueOf(CHAR_SEPARATEUR_CODE);

	// Messages de validation
	public static final String LE_CODE_DU_PROGRAMME_DOIT_ETRE_SEPARE_PAR_SEPARATEUR = "Le code du programme doit avoir au moins 2 parties séparées par \"" + STRING_SEPARATEUR_CODE + "\"";
	public static final String LES_PARTIES_AUTRES_QUE_LA_PREMIERE_PARTIE_DU_CODE_DOIVENT_ETRE_COMPOSEES_EXACTEMENT_DE_3_CHIFFRES = "Les parties autres que la première partie du code doivent être composées exactement de 3 chiffres";
	public static final String LA_PREMIERE_PARTIE_DU_CODE_DOIT_ETRE_IDENTIQUE_AU_CELLE_DU_CODE_DES_AUTRES_PROGRAMMES_DU_FINANCEUR = "La première partie du code doit être identique au celle du code des autres programmes du financeur";
	public static final String LA_PREMIERE_PARTIE_DU_CODE_DOIT_ETRE_COMPOSEE_UNIQUEMENT_DE_CARACTERES_ALPHNUMERIQUES = "La première partie du code doit être composée uniquement de caractères alphnumériques";
	public static final String LE_CODE_DU_PROGRAMME_NE_DOIT_PAS_ETRE_VIDE = "Le code du programme ne doit pas être vide";

	public static final Integer NIVEAU_PROGRAMME = 1;
	public static final Integer NIVEAU_SOUS_PROGRAMME = 2;

	public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");

	public Integer niveau() {
		return ERXStringUtilities.numberOfOccurrencesOfCharInString(CHAR_SEPARATEUR_CODE, code());
	}

	@Override
	public String toString() {
		return code() + " - " + libelle();
	}

	public static String premierePartieDuCode(String code) {
		return code.split("\\" + STRING_SEPARATEUR_CODE)[0];
	}

	public void checkCode() throws ValidationException {

		String code = code();

		if (ERXStringUtilities.stringIsNullOrEmpty(code)) {
			throw new ValidationException(EOProgramme.LE_CODE_DU_PROGRAMME_NE_DOIT_PAS_ETRE_VIDE);
		}

		NSArray<String> parties = new NSArray<String>(code.split("\\" + STRING_SEPARATEUR_CODE));

		Pattern premierePartiePattern = Pattern.compile("[\\p{Alnum}]{3,}");
		Pattern autrePartiePattern = Pattern.compile("[\\p{Digit}]{3}");

		if (parties.count() <= 1) {
			throw new ValidationException(EOProgramme.LE_CODE_DU_PROGRAMME_DOIT_ETRE_SEPARE_PAR_SEPARATEUR);
		}

		NSArray<EOProgramme> programmesDuFinanceur = programmesDuFinanceur(editingContext(), financeur(), null);
		programmesDuFinanceur.remove(this);
		if (programmesDuFinanceur.isEmpty()) {
			Matcher premierePartieMatcher = premierePartiePattern.matcher(parties.get(0));
			if (!premierePartieMatcher.matches()) {
				throw new ValidationException(EOProgramme.LA_PREMIERE_PARTIE_DU_CODE_DOIT_ETRE_COMPOSEE_UNIQUEMENT_DE_CARACTERES_ALPHNUMERIQUES);
			}
		} else {
			String premierePartieDesProgrammes = premierePartieDuCode(ERXArrayUtilities.firstObject(programmesDuFinanceur).code());
			if (!ERXStringUtilities.stringEqualsString(parties.get(0), premierePartieDesProgrammes)) {
				throw new ValidationException(EOProgramme.LA_PREMIERE_PARTIE_DU_CODE_DOIT_ETRE_IDENTIQUE_AU_CELLE_DU_CODE_DES_AUTRES_PROGRAMMES_DU_FINANCEUR);
			}
		}

		NSArray<String> autresParties = ERXArrayUtilities.arrayByRemovingFirstObject(parties);
		for (String partie : autresParties) {
			Matcher autrePartieMatcher = autrePartiePattern.matcher(partie);
			if (!autrePartieMatcher.matches()) {
				throw new ValidationException(EOProgramme.LES_PARTIES_AUTRES_QUE_LA_PREMIERE_PARTIE_DU_CODE_DOIVENT_ETRE_COMPOSEES_EXACTEMENT_DE_3_CHIFFRES);
			}
		}

	}

	public NSArray<EOProgramme> programmesFils(Integer annee) {
		EOQualifier qualifier = EOProgramme.CODE.startsWith(code()).and(EOProgramme.CODE.ne(code()));
		if (annee != null) {
			qualifier = ERXQ.and(qualifier, EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).eq(annee));
		}
		return EOProgramme.fetchAll(editingContext(), qualifier, EOProgramme.CODE.ascs());
	}

	public static NSArray<EOProgramme> programmesDuFinanceur(EOEditingContext editingContext, EOStructure financeur, Integer annee) {
		EOQualifier qualifier = EOProgramme.FINANCEUR.eq(financeur);
		if (annee != null) {
			qualifier = ERXQ.and(qualifier, EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).eq(annee));
		}
		NSArray<EOProgramme> tousLesProgrammes = EOProgramme.fetchAll(editingContext, qualifier);
		NSArray<EOProgramme> programmesPremierNiveau = ERXQ.filtered(tousLesProgrammes, EOProgramme.NIVEAU.eq(1));
		return ERXS.sorted(programmesPremierNiveau, EOProgramme.CODE.ascs());
	}

	public void affecterUneAnnee(EOEditingContext editingContext, Integer annee, Integer utilisateurPersId) {
		EOProgrammeAnnee programmeAnnee = createAnneesRelationship();
		programmeAnnee.setAnnee(annee);
		programmeAnnee.setPersIdCreation(utilisateurPersId);
		programmeAnnee.setPersIdModification(utilisateurPersId);
		programmeAnnee.setDCreation(ERXTimestampUtilities.today());
		programmeAnnee.setDModification(ERXTimestampUtilities.today());
	}

	public static String propositionDeCodePourProgramme(EOEditingContext editingContext, EOStructure financeur) {
		if (financeur == null) {
			return null;
		}
		NSArray<EOProgramme> programmesExistants = programmesDuFinanceur(editingContext, financeur, null);
		if (programmesExistants.isEmpty()) {
			return null;
		} else {
			NSArray<String> codes = EOProgramme.CODE.arrayValueInObject(programmesExistants);
			String premierePartie = premierePartieDuCode(ERXArrayUtilities.firstObject(codes));
			NSArray<Integer> indexes = new NSMutableArray<Integer>();

			for (String code : codes) {
				String indexDuProgramme = code.split("\\" + STRING_SEPARATEUR_CODE)[1];
				indexes.add(Integer.parseInt(indexDuProgramme));
			}

			Integer max = (Integer) indexes.valueForKey("@" + NSArray.MaximumOperatorName) + 1;
			DecimalFormat df = new DecimalFormat("000");
			return premierePartie + STRING_SEPARATEUR_CODE + df.format(max);
		}

	}

	public static String propositionDeCodePourProgramme(EOEditingContext editingContext, EOProgramme programmePere) {

		if (programmePere == null) {
			return null;
		}

		NSArray<EOProgramme> programmesExistants = programmePere.programmesFils(null);

		Integer max;

		if (programmesExistants.isEmpty()) {
			max = 1;
		} else {
			NSArray<String> codes = EOProgramme.CODE.arrayValueInObject(programmesExistants);
			NSArray<Integer> indexes = new NSMutableArray<Integer>();

			for (String code : codes) {
				String indexDuProgramme = code.split("\\" + STRING_SEPARATEUR_CODE)[2];
				indexes.add(Integer.parseInt(indexDuProgramme));
			}

			max = (Integer) indexes.valueForKey("@" + NSArray.MaximumOperatorName) + 1;
		}

		String premierePartie = programmePere.code();

		DecimalFormat df = new DecimalFormat("000");
		return premierePartie + STRING_SEPARATEUR_CODE + df.format(max);

	}

	public static void reporterSelectionDeProgrammesSurAnnees(EOEditingContext editingContext, NSArray<EOProgramme> selection, Integer annee, Integer utilisateurPersId) {
		for (EOProgramme programme : selection) {
			EOQualifier qualifier = EOProgrammeAnnee.ANNEE.eq(annee).and(EOProgrammeAnnee.PROGRAMME.eq(programme));
			EOProgrammeAnnee programmeRecherche = EOProgrammeAnnee.fetchFirstByQualifier(editingContext, qualifier);
			if (programmeRecherche == null) {
				EOProgrammeAnnee nouveauProgrammeAnnee = EOProgrammeAnnee.creerInstance(editingContext);
				nouveauProgrammeAnnee.setAnnee(annee);
				nouveauProgrammeAnnee.setProgrammeRelationship(programme);
				nouveauProgrammeAnnee.setDCreation(ERXTimestampUtilities.today());
				nouveauProgrammeAnnee.setDModification(ERXTimestampUtilities.today());
				nouveauProgrammeAnnee.setPersIdCreation(utilisateurPersId);
				nouveauProgrammeAnnee.setPersIdModification(utilisateurPersId);
			}
		}
	}

	@Override
	public void validateForSave() throws ValidationException {
		super.validateForSave();
		checkCode();
	}

	@Override
	public void delete() {
		deleteAllAnneesRelationships();
		for (EOProgramme programmeFils : programmesFils(null)) {
			programmeFils.delete();
		}
		super.delete();
	}

	@Override
	public void validateForDelete() throws ValidationException {
		super.validateForDelete();
		NSArray<EOAap> aapsLiesAuProgramme = EOAap.fetchAll(editingContext(), EOAap.PROGRAMME.eq(this));
		if (!aapsLiesAuProgramme.isEmpty()) {
			throw ERXValidationFactory.defaultFactory().createCustomException(this, "LiaisonAapRestante");
		}
	}

}
