package org.cocktail.fwkcktlrecherche.server.metier.finder;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class FinderValidationContrat extends Finder {

	private EOContratRecherche contratRecherche;
	private String typeValidation;
	
	private EOQualifier contratRechercheQualifier;
	private EOQualifier typeValidationQualifier;
	
	
	public FinderValidationContrat(EOEditingContext ec) {
		super(ec, EOValidationContrat.ENTITY_NAME);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void clearAllCriteria() {
		setContratRecherche(null);
		setTypeValidation(null);

	}

	@Override
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setContratRecherche(EOContratRecherche contratRecherche) {
		this.contratRecherche = contratRecherche;
		if(contratRecherche != null)
			contratRechercheQualifier = ERXQ.equals(EOValidationContrat.CONTRAT_RECHERCHE_KEY, contratRecherche);
	}

	public EOContratRecherche getContratRecherche() {
		return contratRecherche;
	}

	public void setTypeValidation(String typeValidation) {
		this.typeValidation = typeValidation;
		if(typeValidation != null)
			typeValidationQualifier = ERXQ.equals(EOValidationContrat.TYPE_VALIDATION_KEY, typeValidation);
	}

	public String getTypeValidation() {
		return typeValidation;
	}
	
	public EOValidationContrat findValidationJuridiqueForContrat() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		setTypeValidation(EOValidationContrat.T_VALIDATION_JURIDIQUE);
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.contratRechercheQualifier);
		this.addOptionalQualifier(this.typeValidationQualifier);

		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un appel à projet.");
		
		// fetch
		NSArray<EOValidationContrat> validations = find();
		if(validations == null || validations.isEmpty())
			return null;
		else
				return validations.get(0);
	}

	public EOValidationContrat findValidationFinanciereForContrat() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		setTypeValidation(EOValidationContrat.T_VALIDATION_FINANCIERE);
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.contratRechercheQualifier);
		this.addOptionalQualifier(this.typeValidationQualifier);

		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un appel à projet.");
		
		// fetch
		NSArray<EOValidationContrat> validations = find();
		if(validations == null || validations.isEmpty())
			return null;
		else
				return validations.get(0);
	}

	
	public EOValidationContrat findValidationValoForContrat() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		setTypeValidation(EOValidationContrat.T_VALIDATION_VALO);
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.contratRechercheQualifier);
		this.addOptionalQualifier(this.typeValidationQualifier);

		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un appel à projet.");
		
		// fetch
		NSArray<EOValidationContrat> validations = find();
		if(validations == null || validations.isEmpty())
			return null;
		else
				return validations.get(0);
	}
	
	public EOValidationContrat findValidationScientifiqueForContrat() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		setTypeValidation(EOValidationContrat.T_VALIDATION_SCIENTIFIQUE);
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.contratRechercheQualifier);
		this.addOptionalQualifier(this.typeValidationQualifier);

		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un appel à projet.");
		
		// fetch
		NSArray<EOValidationContrat> validations = find();
		if(validations == null || validations.isEmpty())
			return null;
		else
				return validations.get(0);
	}
	
	
	@Override
	public NSArray find() throws ExceptionFinder {
		EOQualifier qualifier = this.getGlobalQualifier();
		ERXFetchSpecification<EOValidationContrat> fetchSpecification = new ERXFetchSpecification<EOValidationContrat>(EOValidationContrat.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		return fetchSpecification.fetchObjects(ec); 
	}
	
}
