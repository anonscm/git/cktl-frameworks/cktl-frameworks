package org.cocktail.fwkcktlrecherche.server.metier.finder;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;


public class FinderContratRecherche {

	private static String EXERCICE_KEY = "EXERCICE";
	private static String ORDRE_KEY = "ORDRE";
	private static String STRUCTURE_RECHERCHE_KEY = "STRUCTURE_RECHERCHE";
	private static String FINANCEUR_KEY = "FINANCEUR";
	private static String RESPONSABLE_SCIENTIFIQUE_KEY = "RESPONSABLE_SCIENTIFIQUE";
	private static String PARTENAIRE_KEY = "PARTENAIRE";
	private static String SUPPRIME_KEY = "SUPPRIME";
	private static String TYPE_CLASSIFICATION_KEY = "TYPE_CLASSIFICATION";

	private EOEditingContext editingContext;
	

	
	private NSMutableDictionary<String, EOQualifier> contratQualifiers = null;
	
	private Integer exercice;
	private Integer ordre;
	private EOStructure structureRecherche;
	private EOIndividu responsableScientifique;
	private IPersonne partenaire;
	private String acronymeToken;
	
	
	private FactoryAssociation factoryAssociation = null;
	
	public FinderContratRecherche(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public NSMutableDictionary<String, EOQualifier> contratQualifiers() {
		if(contratQualifiers == null) {
			contratQualifiers = new NSMutableDictionary<String, EOQualifier>();
		}
		return contratQualifiers;
	}


	private FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}
	
	public void clear() {
		contratQualifiers().clear();
	}

	public void setExercice(Integer exercice) {
		if(exercice != null) {
			EOQualifier exerciceQualifier = ERXQ.equals(Contrat.EXERCICE_COCKTAIL_KEY + "." + EOExerciceCocktail.EXE_ORDRE_KEY, exercice);
			contratQualifiers().takeValueForKey(exerciceQualifier, EXERCICE_KEY);
		}
		this.exercice = exercice;
	}

	public void setOrdre(Integer ordre) {
		if(ordre != null) {
			EOQualifier ordreQualifier = ERXQ.equals(Contrat.CON_INDEX_KEY, ordre);
			contratQualifiers().takeValueForKey(ordreQualifier, ORDRE_KEY);
		}
		this.ordre = ordre;
	}

	public void setFinanceur(EOStructure financeur) {
		if(financeur != null) {
			EOAssociation financeurAssociation = factoryAssociation().financeurAssociation(editingContext);
			NSArray<EORepartAssociation> repartAssociations = EORepartAssociation.fetchAll(editingContext, 
																							ERXQ.and(
																									ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, financeurAssociation),
																									ERXQ.containsObject(EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY, financeur)
																							)
																						);
			if(!repartAssociations.isEmpty()) {
				EOQualifier financeurQualifier = ERXQ.in(EOContratRecherche.CONTRAT_KEY + "." + Contrat.GROUPE_PARTENAIRE_KEY + "." + EOStructure.TO_REPART_STRUCTURES_ELTS_KEY + "." +  EORepartStructure.TO_REPART_ASSOCIATIONS_KEY, repartAssociations);
				contratQualifiers().takeValueForKey(financeurQualifier, FINANCEUR_KEY);
			}
		}
	}

	public void setResponsableScientifique(EOIndividu responsableScientifique) {
		if(responsableScientifique != null) {
			EOAssociation respoScientAssociation = factoryAssociation().responsableScientifiqueAssociation(editingContext);
			
			EOStructure.fetchFirstByQualifier(editingContext, EOStructure.TO_RNE.dot(EORne.C_RNE).eq("CODE"));
			
			NSArray<EORepartAssociation> repartAssociations = EORepartAssociation.fetchAll(editingContext, 
																							ERXQ.and(
																									ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, respoScientAssociation),
																									ERXQ.containsObject(EORepartAssociation.TO_INDIVIDUS_ASSOCIES_KEY, responsableScientifique)
																							)
																						);
			if(!repartAssociations.isEmpty()) {
				EOQualifier responsableScientifiqueQualifier = ERXQ.in(Contrat.GROUPE_PARTENAIRE_KEY + "." + EOStructure.TO_REPART_STRUCTURES_ELTS_KEY + "." +  EORepartStructure.TO_REPART_ASSOCIATIONS_KEY  , repartAssociations);
				contratQualifiers().takeValueForKey(responsableScientifiqueQualifier, RESPONSABLE_SCIENTIFIQUE_KEY);
			}								
		}
		this.responsableScientifique = responsableScientifique;
	}

	public void setStructureRecherche(EOStructure structureRecherche) {
		if(structureRecherche != null) {
			EOQualifier structureRechercheQualifier = ERXQ.equals(Contrat.CONTRAT_PARTENAIRES_KEY + "." + EOContratPartenaire.PERS_ID_KEY, structureRecherche.persId());
			contratQualifiers().takeValueForKey(structureRechercheQualifier, STRUCTURE_RECHERCHE_KEY);
		}
		this.structureRecherche = structureRecherche;
	}

	public void setSupprime(Boolean supprime) {
		if(supprime != null) {
			EOQualifier supprimeQualifier;
			if(supprime == false) {
				supprimeQualifier = ERXQ.or( 
										ERXQ.equals(Contrat.CON_SUPPR_KEY, "N"),
										ERXQ.isNull(Contrat.CON_SUPPR_KEY)
										);
			}
			else {
				supprimeQualifier = ERXQ.equals(Contrat.CON_SUPPR_KEY, 'O');
			}
			contratQualifiers().takeValueForKey(supprimeQualifier, SUPPRIME_KEY);
		}
	}
	

	public void setTypeClassification(String typeClassification) {
		if(typeClassification != null) {
			EOQualifier typeClassificationQualifier = ERXQ.equals(Contrat.TYPE_CLASSIFICATION_CONTRAT_KEY + "." + TypeClassificationContrat.TCC_CODE_KEY, typeClassification);
			contratQualifiers().takeValueForKey(typeClassificationQualifier, TYPE_CLASSIFICATION_KEY);
		}
	}
	
	
	public void setPartenaire(EOStructure partenaire) {
		if(partenaire != null) {
			EOQualifier partenaireQualifier = ERXQ.equals(Contrat.CONTRAT_PARTENAIRES_KEY + "." + EOContratPartenaire.PERS_ID_KEY, partenaire.persId());
			contratQualifiers().takeValueForKey(partenaireQualifier, PARTENAIRE_KEY);
		}
		this.partenaire = partenaire;
	}
	
	public EOQualifier qualifier(Boolean contratRecherche) {
		EOQualifier qualifier = null;
		for(EOQualifier _qualifier : contratQualifiers().values()) {
			if(contratRecherche) {
				_qualifier = EOContratRecherche.CONTRAT.prefix(_qualifier);
			}
			if(qualifier == null) {
				qualifier = _qualifier;
			}
			else {
				qualifier = ERXQ.and(qualifier, _qualifier);
			}
		}
		return qualifier;
	}

	public NSArray<EOContratRecherche> findContratRecherches() throws Exception {
		
		setTypeClassification("CONV");
		setSupprime(false);
		
		if (!this.check())
			throw new Exception(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un contrat de recherche.");
			
		ERXSortOrderings sortOrderings = ERXS.ascs(EOContratRecherche.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_ORDRE_KEY).toString()).toString(),EOContratRecherche.CONTRAT.dot(Contrat.CON_INDEX_KEY).toString());
		return EOContratRecherche.fetchAll(editingContext, qualifier(true), sortOrderings);
		
	}
	
	public NSArray<Contrat> findContrats() throws Exception {
		
		setTypeClassification("CONV");
		setSupprime(false);
		
		if (!this.check())
			throw new Exception(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un contrat de recherche.");
			
		ERXSortOrderings sortOrderings = ERXS.ascs(Contrat.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_ORDRE_KEY).toString(),Contrat.CON_INDEX_KEY);
		return Contrat.fetchAll(editingContext, qualifier(false), sortOrderings);
		
	}
	
	private Boolean check() {
		return !contratQualifiers().isEmpty();
	}
	
	
	public NSArray<NSDictionary<String, Object>> findContratsAsRowRaws() {
		return null;
	}
	
	
	private String requetePourContrats() {
		
		String selectSousRequeteNomenclatureSuivi = " select nob.libelle_long";
		String fromSousRequeteNomenclatureSuivi = " from RECHERCHE.nomenclature_nomenclatures nn";
		String innerJoinSousRequeteNomenclatureSuivi = " inner join RECHERCHE.nomenclature_objets nob on nn.id = nob.nomenclature_id and str_id = 'org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat'";
		String whereSousRequeteNomenclatureSuivi = " where code =  cr.suivi_admin";
		String sousRequeteNomenclatureSuivi = selectSousRequeteNomenclatureSuivi + fromSousRequeteNomenclatureSuivi 
				+ innerJoinSousRequeteNomenclatureSuivi
				+ whereSousRequeteNomenclatureSuivi;
		
		String selectRequeteContrats = " select c.con_ordre, c.exe_ordre, c.con_index, c.con_objet, cr.conr_ordre as contrat_recherche_id, tc.tycon_libelle as type_contrat , (" + sousRequeteNomenclatureSuivi + ") as suivi";
		String fromRequeteContrats = " from accords.contrat c";

		String requeteContrats = selectRequeteContrats + fromRequeteContrats;
		
		if(structureRecherche != null) {
			String innerJoinCp = " inner join ACCORDS.contrat_partenaire cp_labo on c.con_ordre = cp_labo.con_ordre and cp_labo.pers_id = " + structureRecherche.persId();
			String innerJoinStr = " inner join structure_ulr s_labo on cp_labo.pers_id = s_labo.pers_id";
			String innerJoinRtg = " inner join repart_type_groupe rtg_labo on s_labo.c_structure = rtg_labo.c_structure and rtg_labo.tgrp_code = 'SR'";
			requeteContrats += innerJoinCp + innerJoinStr + innerJoinRtg;
		}
		
		if(partenaire != null) {
			String innerJoinCp = " inner join ACCORDS.contrat_partenaire cp_part on c.con_ordre = cp_part.con_ordre and cp_part.pers_id = " + partenaire.persId();
			String innerJoinStr = " inner join structure_ulr s_part on cp_part.pers_id = s_part.pers_id";
			String innerJoinRtg = " inner join repart_type_groupe rtg_part on s_part.c_structure = rtg_part.c_structure and rtg_part.tgrp_code != 'SR'";
			requeteContrats += innerJoinCp + innerJoinStr + innerJoinRtg;
		}
		if(responsableScientifique != null) {
			String innerJoinCp = " inner join ACCORDS.contrat_partenaire cp_resp_sc on c.con_ordre = cp_resp_sc.con_ordre";
			String innerJoinCpc = " inner join ACCORDS.contrat_part_contact cpc_resp_sc on cp_resp_sc.cp_ordre = cpc_resp_sc.cp_ordre and cpc_resp_sc.pers_id_contact = " + responsableScientifique.persId();
			requeteContrats += innerJoinCp + innerJoinCpc;
		}
		

		  
		    
		String innerJoinCr = " left outer join RECHERCHE.contrat_recherche cr on cr.con_ordre = c.con_ordre";
		String innerJoinTc = " inner join ACCORDS.type_contrat tc on c.con_nature  = tc.tycon_id";
		String innerJoinTcc = " inner join ACCORDS.type_classification_contrat tcc on tcc.tcc_id = c.tcc_id and  tcc.tcc_code = 'CONV'";
		
		String conSuppr = " c.con_suppr = 'N'";
		
		String where = "WHERE " + conSuppr;
		
		
		
    if(getAcronymeToken() != null) {
      where += " AND upper(c.con_objet_court) like upper('%"+getAcronymeToken()+"%')";
    }
    
     
		
		requeteContrats += innerJoinCr + innerJoinTc + innerJoinTcc + where;
		
		if(exercice != null) {
			String whereExercice  = " and c.exe_ordre = " + exercice;
			requeteContrats += whereExercice;
		}
		if(ordre != null) {
		    String whereOrdre  = " and c.con_index = " + ordre; 
		    requeteContrats += whereOrdre;
		}
		String orderBy  = " order by c.exe_ordre ASC, c.con_index ASC";
		requeteContrats += orderBy;
		
		return requeteContrats;
		
	}
	

	public NSArray<NSDictionary<String, Object>> findContratsRows() { 
		
		return EOUtilities.rawRowsForSQL(editingContext, "FwkCktlPersonne", requetePourContrats(), null);
	}

  public String getAcronymeToken() {
    return acronymeToken;
  }

  public void setAcronymeToken(String acronymeToken) {
    this.acronymeToken = acronymeToken;
  }

	
}
