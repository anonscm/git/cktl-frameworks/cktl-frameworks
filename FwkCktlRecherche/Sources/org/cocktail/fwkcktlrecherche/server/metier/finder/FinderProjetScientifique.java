package org.cocktail.fwkcktlrecherche.server.metier.finder;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.EOProjet;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class FinderProjetScientifique extends Finder {

	public FinderProjetScientifique(EOEditingContext ec) {
		super(ec, EOProjetScientifique.ENTITY_NAME);
		// TODO Auto-generated constructor stub
	}

	private Integer exercice;
	private Integer ordre;
	
	private EOQualifier exerciceQualifier = null;
	private EOQualifier ordreQualifier = null;
	
	
	@Override
	public boolean canFind() {
		return true;
	}

	@Override
	public void clearAllCriteria() {
		setExercice(null);
		setOrdre(null);
	}

	@Override
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setExercice(Integer exercice) {
		this.exercice = exercice;
		if(exercice != null) {
			
			exerciceQualifier = ERXQ.equals(EOProjetScientifique.PROJET.dot(EOProjet.EXERCICE_COCKTAIL_KEY).dot(EOExercice.EXE_ORDRE_KEY).toString(), this.exercice);
		}
		else {
			exerciceQualifier = null;
		}
	}

	public Integer getExercice() {
		return exercice;
	}

	public void setOrdre(Integer ordre) {
		this.ordre = ordre;
		if(ordre != null) {
			
			EOQualifier qualifierContrats = ERXQ.and(
						ERXQ.equals(Contrat.CON_INDEX_KEY, this.ordre),
						ERXQ.equals("typeClassificationContrat" + "." + TypeClassificationContrat.TCC_CODE_KEY, "PROJ_SCIEN")
				);
			ERXFetchSpecification<Contrat> fetchSpec = new ERXFetchSpecification<Contrat>(Contrat.ENTITY_NAME,qualifierContrats, null);
			NSArray<Contrat> resContrats = ec.objectsWithFetchSpecification(fetchSpec);
			ordreQualifier = ERXQ.hasValues(EOProjetScientifique.PROJET_KEY + "." + "projetContrats" + "." + "contrat", resContrats);
		}
		else {
			ordreQualifier = null;
		}
	}

	public Integer getOrdre() {
		return ordre;
	}
	
	public NSArray find() throws ExceptionFinder {
			
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
				
		// nouveaux criteres 
		this.addOptionalQualifier(this.exerciceQualifier);
		this.addOptionalQualifier(this.ordreQualifier);
		
		// au moins un critere necessaire
		if (!this.checkCriteria())
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher un projet scientifique.");
		
		if(this.getGlobalQualifier() == null)
			return new NSArray();
				
		
		// fetch
		// fetch
		return ERXQ.filtered((NSMutableArray<?>) super.find().mutableClone(), getSupprimeQualifier());		
	
	}

	private Boolean checkCriteria() {
		return (
			(exercice != null)
			||
			(ordre != null)
		);
	}
	
	private EOQualifier getSupprimeQualifier() {
		return ERXQ.isFalse(EOProjetScientifique.PROJET_SUPPRIME_KEY);
	}
}