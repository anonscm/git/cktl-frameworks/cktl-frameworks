package org.cocktail.fwkcktlrecherche.server.metier.finder;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.qualifiers.ERXFalseQualifier;

/**
 * 
 * @author Julien Lafourcade
 * 
 */
public class FinderAap {

	private Integer exercice;
	private Integer ordre;
	private EOStructure laboratoire;
	private EOStructure financeur;
	private EOIndividu responsableScientifique;
	private EOStructure partenaire;
	private String acronymeToken;

	private EOEditingContext edc = null;
	private EOQualifier perimetresQualifier;

	/**
	 * 
	 * @param ec
	 *            Pour faire la recherche
	 */
	public FinderAap(EOEditingContext ec) {
		edc = ec;
	}

	public FactoryAssociation getFactoryAssociation() {
		return FactoryAssociation.shared();
	}

	/**
	 * Réinitialisation du finder
	 */
	public void clear() {
		setExercice(null);
		setOrdre(null);
		setLaboratoire(null);
		setResponsableScientifique(null);
		setFinanceur(null);

	}

	public void setExercice(Integer exercice) {
		this.exercice = exercice;
	}

	public Integer getExercice() {
		return exercice;
	}

	/**
	 * 
	 * @return le qualifier sur l'exercie s'il est renseigné
	 */
	public EOQualifier exerciceQualifier() {
		if (getExercice() != null) {
			return EOAap.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL).dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(getExercice());
		} else {
			return null;
		}
	}

	public void setOrdre(Integer ordre) {
		this.ordre = ordre;
	}

	public Integer getOrdre() {
		return ordre;
	}

	/**
	 * 
	 * @return le qualifier sur l'ordre s'il est renseigné
	 */
	public EOQualifier ordreQualifier() {

		EOQualifier qualifier = null;

		if (getOrdre() != null) {
			qualifier = EOAap.CONTRAT.dot(Contrat.CON_INDEX).eq(getOrdre());
		}

		return qualifier;

	}

	public void setFinanceur(EOStructure financeur) {
		this.financeur = financeur;
	}

	public EOStructure getFinanceur() {
		return financeur;
	}

	/**
	 * 
	 * @return le qualifier sur le financeur s'il est renseigné
	 */
	public EOQualifier financeurQualifier() {

		EOQualifier qualifier = null;

		if (getFinanceur() != null) {
			EOQualifier sousQualifier = Contrat.GROUPE_PARTENAIRE.dot(EOStructure.TO_REPART_STRUCTURES_ELTS).dot(EORepartStructure.TO_REPART_ASSOCIATIONS).dot(EORepartAssociation.TO_ASSOCIATION).eq(getFactoryAssociation().financeurAssociation(edc))
					.and(Contrat.GROUPE_PARTENAIRE.dot(EOStructure.TO_REPART_STRUCTURES_ELTS).dot(EORepartStructure.TO_REPART_ASSOCIATIONS).dot(EORepartAssociation.PERS_ID).eq(getFinanceur().persId()));
			qualifier = new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, EOAap.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
		}

		return qualifier;

	}

	public void setResponsableScientifique(EOIndividu responsableScientifique) {
		this.responsableScientifique = responsableScientifique;
	}

	public EOIndividu getResponsableScientifique() {
		return responsableScientifique;
	}

	/**
	 * 
	 * @return le qualifier sur le responsable scientifique s'il est renseigné
	 */
	public EOQualifier responsableScientifiqueQualifier() {

		EOQualifier qualifier = null;

		if (getResponsableScientifique() != null) {
			EOQualifier sousQualifier = Contrat.GROUPE_PARTENAIRE.dot(EOStructure.TO_REPART_STRUCTURES_ELTS).dot(EORepartStructure.TO_REPART_ASSOCIATIONS).dot(EORepartAssociation.TO_ASSOCIATION)
					.eq(getFactoryAssociation().responsableScientifiqueAssociation(edc))
					.and(Contrat.GROUPE_PARTENAIRE.dot(EOStructure.TO_REPART_STRUCTURES_ELTS).dot(EORepartStructure.TO_REPART_ASSOCIATIONS).dot(EORepartAssociation.PERS_ID).eq(getResponsableScientifique().persId()));
			qualifier = new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, EOAap.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
		}

		return qualifier;
	}

	public void setLaboratoire(EOStructure laboratoire) {
		this.laboratoire = laboratoire;
	}

	public EOStructure getLaboratoire() {
		return laboratoire;
	}

	/**
	 * 
	 * @return le qualifier sur le laboratoire s'il est renseigné
	 */
	public EOQualifier laboratoireQualifier() {

		EOQualifier qualifier = null;

		if (getLaboratoire() != null) {
			EOQualifier sousQualifier = Contrat.CONTRAT_PARTENAIRES.dot(ContratPartenaire.PERS_ID).eq(getLaboratoire().persId());
			qualifier = new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, EOAap.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
		}

		return qualifier;
	}

	public void setPartenaire(EOStructure partenaire) {
		this.partenaire = partenaire;
	}

	public EOStructure getPartenaire() {
		return partenaire;
	}

	/**
	 * 
	 * @return la qualifier sur le partenaire s'il est renseigné
	 */
	public EOQualifier partenaireQualifier() {

		EOQualifier qualifier = null;

		if (getPartenaire() != null) {
			EOQualifier sousQualifier = Contrat.CONTRAT_PARTENAIRES.dot(ContratPartenaire.PERS_ID).eq(getPartenaire().persId());
			qualifier = new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, EOAap.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
		}

		return qualifier;
	}
	
	public String getAcronymeToken() {
		return acronymeToken;
	}

	public void setAcronymeToken(String acronymeToken) {
		this.acronymeToken = acronymeToken;
	}

	/**
	 * 
	 * @return la qualifier sur l'acronyme s'il est renseigné
	 */
	public EOQualifier acronymeQualifier() {
		if (getAcronymeToken() == null) {
			return null;
		} else {
			return EOAap.CONTRAT.dot(Contrat.CON_OBJET_COURT).contains(getAcronymeToken());
		}
	}

	private EOQualifier supprimeQualifier() {
		return EOAap.AAP_SUPPRIME.isFalse();
	}

	private EOQualifier globalQualifier() {
		NSArray<EOQualifier> qualifiers = 
				new NSArray<EOQualifier>(
						exerciceQualifier(), ordreQualifier(), 
						acronymeQualifier(), laboratoireQualifier(), 
						responsableScientifiqueQualifier(), financeurQualifier(), 
						partenaireQualifier(), supprimeQualifier(), getPerimetresQualifier()
					);

		qualifiers = ERXArrayUtilities.removeNullValues(qualifiers);

		return ERXQ.and(qualifiers);
	}
	
	public EOQualifier getPerimetresQualifier() {
		if (perimetresQualifier == null) {
			return new ERXFalseQualifier();
		}
		return perimetresQualifier;
	}

	public void setPerimetresQualifier(EOQualifier perimetresQualifier) {
		this.perimetresQualifier = perimetresQualifier;
	}
	
	/**
	 * Recherche des AAP.
	 * 
	 * @return Liste des AAP trouvees.
	 * @throws AucunCritereFourniException Si aucun cretère n'est fourni
	 */
	public NSArray<EOAap> find() throws AucunCritereFourniException {

		if (!this.checkCriteria()) {
			throw new AucunCritereFourniException();
		}

		ERXSortOrderings order = 
				EOAap.CONTRAT
					.dot(Contrat.EXERCICE_COCKTAIL)
					.dot(EOExerciceCocktail.EXE_ORDRE_KEY)
						.asc()
				.then(
					EOAap.CONTRAT
						.dot(Contrat.CON_INDEX)
							.asc()
					);
		
		return EOAap.fetchAll(edc, globalQualifier(), order);
	}

	private Boolean checkCriteria() {
		return ((exercice != null) || (ordre != null) || (laboratoire != null) || (responsableScientifique != null) || (financeur != null) || (partenaire != null) || (acronymeToken != null));
	}



}
