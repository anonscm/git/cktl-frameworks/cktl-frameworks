package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlnomenclatures.server.metier.factory.BaseFactoryNomenclature;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.ContratRechercheMailService;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;

import com.google.inject.Inject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

/**
 * 
 * @author Julien Lafourcade
 * 
 */
public class EOContratRecherche extends _EOContratRecherche implements IContratRecherche {

	private static final long serialVersionUID = -5084253948257750454L;

	public static final String NUMERO_KEY = "numero";
	public static final String OBJET_KEY = "objet";
	public static final String OBJET_COURT_KEY = "objetCourt";

	public static final String CODE_SENS_MONTANT_FINANCIER_ENTRANT = "E";
	public static final String CODE_SENS_MONTANT_FINANCIER_SORTANT = "S";

	private ContratUtilities contratUtilities = null;

	@Inject
	private ContratRechercheMailService mailService;
	
	
	private ContratUtilities contratUtilities() {
		if (contratUtilities == null) {
			contratUtilities = ContratUtilities.creerNouvelleInstance(editingContext());
		}
		return contratUtilities;
	}
	
	public void setMailService(ContratRechercheMailService mailService) {
		this.mailService = mailService;
	}

	/**
	 * @return Concatenation de l'annee de l'exercice avec l'index du contrat (donne une valeur unique)
	 */
	public String numero() {
		if (contrat().conIndex() == null) {
			return null;
		}
		return contrat().exerciceEtIndex();
	}

	public String getObjet() {
		return contrat().conObjet();
	}

	/**
	 * @param objet L'objet
	 */
	public void setObjet(String objet) {
		contrat().setConObjet(objet);
	}

	public String getObjetCourt() {
		return contrat().conObjetCourt();
	}

	/**
	 * @param objetCourt L'objet court
	 */
	public void setObjetCourt(String objetCourt) {
		contrat().setConObjetCourt(objetCourt);
	}

	// CHARGE DU DOSSIER

	/**
	 * @return renvoie le chargé du dossier
	 */
	public EOIndividu chargeDuDossier() {
		EOAssociation role = FactoryAssociation.shared().chargeDuDossierAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	/**
	 * @param nouveauChargeDuDossier la nouvelle valeur
	 * @param utilisateurPersId persId de l'utilisateur qui la modif
	 */
	public void setChargeDuDossier(EOIndividu nouveauChargeDuDossier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().chargeDuDossierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauChargeDuDossier, role, contrat(), utilisateurPersId);
	}

	// STRUCTURES CONCERNEES

	public void ajouterStructureConcernee(EOStructure structureRecherche, Integer persIdUtilisateur) throws Exception {
		contratUtilities().ajouterStructureRechercheConcerneePourContrat(structureRecherche, contrat());
		if (structureRecherchePrincipale() == null) {
			setStructureRecherchePrincipale(structureRecherche, persIdUtilisateur);
		}
	}

	public void supprimerStructureRechercheConcernee(EOStructure structureRecherche, Integer persId) throws Exception {
		contratUtilities().supprimerStructureRechercheConcerneePourContrat(structureRecherche, contrat(), persId);
	}

	public NSArray<EOStructure> structureRechercheConcernees() {
		return contratUtilities().getStructuresRecherchesConcerneesPourContrat(contrat());
	}

	public EOStructure structureRecherchePrincipale() {
		if (StructuresRechercheHelper.isStructureRecherche(contrat().centreResponsabilite())) {
			return contrat().centreResponsabilite();
		} else {
			return null;
		}
	}

	public void setStructureRecherchePrincipale(EOStructure structure, Integer persIdUtilisateur) {
		contrat().setCentreResponsabiliteRelationship(structure, persIdUtilisateur);
	}

	// RESPONSABLES SCIENTIFIQUES

	public void ajouterResponsableScientifiquePourStructureRecherche(EOIndividu responsable, EOStructure structureRecherche) throws Exception {
		contratUtilities().ajouterResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contrat());
	}

	public void supprimerResponsableScientifiquePourStructureRecherche(EOIndividu responsable, EOStructure structureRecherche, Integer persId) throws Exception {
		contratUtilities().supprimerResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contrat(), persId);
	}

	public NSArray<EOIndividu> responsablesScientifiquesPourLaboratoire(EOStructure structureRecherche) {
		return contratUtilities().responsablesScientifiquesPourStructureRechercheEtContrat(structureRecherche, contrat());
	}

	public NSArray<EOIndividu> responsablesScientifiques() {
		return contratUtilities().getResponsablesScientifiquePourContrat(contrat());
	}

	// GESTIONNAIRE ADMINISTRATIF ET FINANCIER DE LA CONVENTION

	public EOIndividu gestionnaireAdminEtFinConvention() {
		EOAssociation role = FactoryAssociation.shared().gestionnaireAdminEtFinAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setGestionnaireAdminEtFinConvention(EOIndividu nouveauChargeDuDossier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().gestionnaireAdminEtFinAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauChargeDuDossier, role, contrat(), utilisateurPersId);
	}

	// PARTENAIRES DE LA CONVENTION

	public void ajouterPartenaire(IPersonne partenaire) throws Exception {
		contratUtilities().ajouterPartenaireDansContrat(partenaire, contrat());
	}

	public void supprimerPartenaire(IPersonne partenaire, Integer persIdUtilisateur) throws Exception {
		contratUtilities().supprimerPartenaireDansContrat(partenaire, contrat(), persIdUtilisateur);
	}

	public NSArray<IPersonne> partenaires() {
		return contratUtilities().getPartenairesDansContrat(contrat());
	}

	// ETABLISSEMENT GESTIONNAIRE FINANCIER

	public EOStructure etablisssementGestionnaireFinancier() {
		return contrat().etablissement();
	}

	public void setEtablisssementGestionnaireFinancier(EOStructure nouvelEtablissementGestionnaire, Integer utilisateurPersId) {
		contrat().setEtablissementRelationship(nouvelEtablissementGestionnaire, utilisateurPersId);
	}

	public Boolean hasLaboratoiresConernes() {
		return structureRechercheConcernees().count() > 0;
	}

	@Override
	public void setProgrammeRelationship(EOProgramme value) {
		super.setProgrammeRelationship(value);
		if (value == null) {
			setSousProgrammeRelationship(null);
		}
	}

	// FINANCEUR

	public EOStructure getFinanceur() {
		EOAssociation role = FactoryAssociation.shared().financeurAssociation(editingContext());
		return (EOStructure) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setFinanceur(EOStructure nouveauFinanceur, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().financeurAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauFinanceur, role, contrat(), utilisateurPersId);
	}

	// CREATEUR DU DOSSIER

	public EOIndividu getCreateurDuDossier() {
		EOAssociation role = FactoryAssociation.shared().createurDuDossierAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setCreateurDuDossier(EOIndividu nouveauCreateurDuDossier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().createurDuDossierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauCreateurDuDossier, role, contrat(), utilisateurPersId);
	}

	// VALIDATEUR JURIDIQUE

	public EOIndividu getValidateurJuridique() {
		EOAssociation role = FactoryAssociation.shared().validateurJuridiqueAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setValidateurJuridique(EOIndividu nouveauValidateurJuridique, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().validateurJuridiqueAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauValidateurJuridique, role, contrat(), utilisateurPersId);
	}

	// VALIDATION JURIDIQUE

	public EOValidationContrat validationJuridique() {
		NSArray<EOValidationContrat> validations = toValidationContrats(EOValidationContrat.TYPE_VALIDATION.eq(EOValidationContrat.T_VALIDATION_JURIDIQUE));
		return ERXArrayUtilities.firstObject(validations);
	}

	// VALIDATEUR FINANCIER

	public EOIndividu validateurFinancier() {
		EOAssociation role = FactoryAssociation.shared().validateurFinancierAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setValidateurFinancier(EOIndividu nouveauValidateurFinancier, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().validateurFinancierAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauValidateurFinancier, role, contrat(), utilisateurPersId);
	}

	// VALIDATION FINANCIERE

	public EOValidationContrat validationFinanciere() {
		NSArray<EOValidationContrat> validations = toValidationContrats(EOValidationContrat.TYPE_VALIDATION.eq(EOValidationContrat.T_VALIDATION_FINANCIERE));
		return ERXArrayUtilities.firstObject(validations);
	}

	// VALIDATEUR VALO

	public EOIndividu validateurValo() {
		EOAssociation role = FactoryAssociation.shared().validateurValoAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contrat());
	}

	public void setValidateurValo(EOIndividu nouveauValidateurValo, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().validateurValoAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauValidateurValo, role, contrat(), utilisateurPersId);
	}

	// VALIDATION VALO

	public EOValidationContrat validationValorisation() {
		NSArray<EOValidationContrat> validations = toValidationContrats(EOValidationContrat.TYPE_VALIDATION.eq(EOValidationContrat.T_VALIDATION_VALO));
		return ERXArrayUtilities.firstObject(validations);
	}

	public boolean isMontantFinancierEntrant() {
		return CODE_SENS_MONTANT_FINANCIER_ENTRANT.equals(sensMontantFinancier());
	}

	public boolean isMontantFinancierSortant() {
		return CODE_SENS_MONTANT_FINANCIER_SORTANT.equals(sensMontantFinancier());
	}

	public String getLibelleSensMontantFinancier() {
		String libelle;

		if (isContratSansIncidenceFinanciere()) {
			libelle = "";
		} else if (isMontantFinancierEntrant()) {
			libelle = "Entrant (recette)";
		} else {
			libelle = "Sortant (dépense)";
		}

		return libelle;
	}

	public void validateForSave() throws ValidationException {

		if (!contratSupprime()) {
			checkObjet();
			checkExercice();
			checkNbStructuresConcernees();
			checkDateOuverture();
			checkCreateurDossier();
			checkEtablissement();
			checkDatesDebutEtFin();
			checkModeGestion();
			checkTypeContrat();
			checkCentre();
			checkPartenairePrincipal();
			checkNbPartenaires();
			checkNbAvenants();
		}

		super.validateForSave();
	}

	private void checkNbAvenants() {
		if (contrat().avenantsDontInitialNonSupprimes().count() < 1) {
			throw new ValidationException("L'acte de partenariat doit posséder au moins un avenant.");
		}
	}

	private void checkNbPartenaires() {
		if (contrat().contratPartenaires().count() <= 1) {
			throw new ValidationException("Vous devez indiquer au moins 2 partenaires.");
		}
	}

	private void checkPartenairePrincipal() {
		if (contrat().partenairePrincipal() == null) {
			throw new ValidationException("Vous devez indiquer le partenaire principal.");
		}
	}

	private void checkCentre() {
		if (contrat().centreResponsabilite() == null) {
			throw new ValidationException("Vous devez choisir un service gestionnaire.");
		}
	}

	private void checkTypeContrat() {
		if (contrat().typeContrat() == null) {
			throw new ValidationException("Vous devez choisir un type de contrat.");
		}
	}

	private void checkModeGestion() {
		if (contrat().modeDeGestion() == null) {
			throw new ValidationException("Vous devez choisir un mode de gestion.");
		}
	}

	private void checkDatesDebutEtFin() {
		if (contrat().avenantZero().avtDateSignature() != null) {
			if (contrat().avenantZero().avtDateDeb() == null) {
				throw new ValidationException("La date de début est obligatoire si la date de signature est renseignée");
			}
			if (contrat().avenantZero().avtDateFin() == null) {
				throw new ValidationException("La date de fin est obligatoire si la date de signature est renseignée");
			}

		}
		
		if (contrat().avenantZero().avtDateDeb() != null 
				&& contrat().avenantZero().avtDateFin() != null 
				&& contrat().avenantZero().avtDateDeb().compareTo(contrat().avenantZero().avtDateFin()) > 0) {
			throw new ValidationException("La date de début ne peut pas être supérieure à la date de fin");
		}
	}

	private void checkEtablissement() {
		if (etablisssementGestionnaireFinancier() == null) {
			throw new ValidationException("Aucun établissement gestionnaire financier renseigné pour le contrat.");
		}
	}

	private void checkCreateurDossier() {
		if (getCreateurDuDossier() == null) {
			throw new ValidationException("Le créateur du dossier doit être renseigné");
		}
	}

	private void checkDateOuverture() {
		if (dOuvertureDossier() == null) {
			throw new ValidationException("La date d'ouverture du dossier doit être renseignée");
		}
	}

	private void checkNbStructuresConcernees() {
		if (structureRechercheConcernees().count() == 0) {
			throw new ValidationException("Il doit y avoir au moins un laboratoire concerné");
		}
	}

	private void checkObjet() {
		if (MyStringCtrl.isEmpty(contrat().conObjet())) {
			throw new ValidationException("L'objet ne doit pas être vide");
		}
	}

	private void checkExercice() {
		if (contrat().exerciceCocktail().exeExercice() == null) {
			throw new ValidationException("L'année doit être renseignée");
		}
	}

	public Boolean hasTypeDeResultats() {
		return toRepartTypeResultatsContrats().isEmpty();
	}

	public Boolean hasApportsDrv() {
		return toRepartApportDrvContrats().isEmpty();
	}

	public Boolean hasApportsDaj() {
		return toRepartApportDajContrats().isEmpty();
	}

	public String getSuiviAdministratifLibelle() {
		return (String) Nomenclatures.suivisAdministratifs().valueForKey(suiviAdministratif());
	}

	public Boolean getPeutDemanderValidation(Integer persId) {
		return getIsInGroupe(persId, ParametresRecherche.GROUPE_GESTIONNAIRES_CONTRATS_KEY);
	}

	public Boolean getPeutEffectuerValidationFinanciere(Integer persId) {
		return getIsInGroupe(persId, ParametresRecherche.GROUPE_VALIDATEURS_FINANCIERS_KEY);
	}

	public Boolean getPeutEffectuerValidationValo(Integer persId) {
		return getIsInGroupe(persId, ParametresRecherche.GROUPE_VALIDATEURS_VALO_KEY);
	}

	public Boolean getPeutEffectuerValidationJuridique(Integer persId) {
		return getIsInGroupe(persId, ParametresRecherche.GROUPE_VALIDATEURS_JURIDIQUES_KEY);
	}

	protected Boolean getIsInGroupe(Integer persId, String groupeParamKey) {
		EOStructure groupe;
		try {
			groupe = EOStructureForGroupeSpec.getGroupeForParamKey(editingContext(), groupeParamKey);
		} catch (Exception e) {
			return false;
		}

		NSArray<EORepartStructure> repartStructures = groupe.toRepartStructuresElts(EORepartStructure.PERS_ID.eq(persId));

		return !repartStructures.isEmpty();
	}

	
	/**
	 * Envoi une demande de validation juridique du contrat
	 * @param persId le persId de l'utilisateur qui envoie la demande
	 */
	public void demanderLaValidationJuridique(Integer persId) {
		demanderLaValidation(persId, EOValidationContrat.T_VALIDATION_JURIDIQUE);
	}
	
	
	/**
	 * Envoi une demande de validation financiere du contrat
	 * @param persId le persId de l'utilisateur qui envoie la demande
	 */
	public void demanderLaValidationFinanciere(Integer persId) {
		demanderLaValidation(persId, EOValidationContrat.T_VALIDATION_FINANCIERE);
	}
	
	
	/**
	 * Envoi une demande de validation valo du contrat
	 * @param persId le persId de l'utilisateur qui envoie la demande
	 */
	public void demanderLaValidationValo(Integer persId) {
		demanderLaValidation(persId, EOValidationContrat.T_VALIDATION_VALO);
	}
	
	
	/**
	 * Envoie une demande de validation du contrat
	 * @param persId le persId de l'utilisateur qui envoie la demande
	 * @param typeValidationContrat le type de validation
	 */
	public void demanderLaValidation(Integer persId, String typeValidationContrat) {
		mailService.envoyerMailDemandeDeValidation(this, typeValidationContrat, persId);
	}
	
	/**
	 * Envoie le mail de validation du contrat
	 * @param persId le persId de l'utilisateur qui envoie la demande
	 * @param typeValidationContrat le type de validation
	 */
	public void envoyerMailValidation(Integer persId, String typeValidationContrat) {
		mailService.envoyerMailValidation(this, typeValidationContrat, persId);
	}
	
	/**
	 * Renseigne des données concernant la validation financiere
	 * @param persId le persId de l'utilisateur qui valide
	 */
	public void effectuerValidationFinanciere(Integer persId) {
		EOIndividu validateur = EOIndividu.fetchFirstByQualifier(editingContext(), EOIndividu.PERS_ID.eq(persId));
		setValidateurFinancier(validateur, persId);
		validationFinanciere().passerValide(persId);
	}
	
	/**
	 * Renseigne des données concernant la validation juridique
	 * @param persId le persId de l'utilisateur qui valide
	 */
	public void effectuerValidationJuridique(Integer persId) {
		EOIndividu validateur = EOIndividu.fetchFirstByQualifier(editingContext(), EOIndividu.PERS_ID.eq(persId));
		setValidateurJuridique(validateur, persId);
		validationJuridique().passerValide(persId);
	}
	
	/**
	 * Renseigne des données concernant la validation valo
	 * @param persId le persId de l'utilisateur qui valide
	 */
	public void effectuerValidationValo(Integer persId) {
		EOIndividu validateur = EOIndividu.fetchFirstByQualifier(editingContext(), EOIndividu.PERS_ID.eq(persId));
		setValidateurValo(validateur, persId);
		validationValorisation().passerValide(persId);
	}
	
	
	/*
	 * 
	 * LES NOMENCLATURES
	 */

	public static class Nomenclatures extends BaseFactoryNomenclature {

		public static NSDictionary<String, String> suivisAdministratifs() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.suiviadministratifcontrat").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesRedactionContrat() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeredactioncontrat").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesRedactionClausesPi() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeredactionclausespi").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesApportDrvContrat() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeapportsdrvcontrat").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesApportDajContrat() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeapportsdajcontrat").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesProprieteResultats() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeproprieteresultats").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesExploitationResultats() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typeexploitationresultats").asMapOfStrings();
		}

		public static NSDictionary<String, String> typesDeResultats() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typegestionresultats").asMapOfStrings();
		}

		public static NSDictionary<String, String> typeSuiviValo() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.typesuivivalorisation").asMapOfStrings();
		}

		public static NSArray<String> selectionLangueContrats() {
			return shared().nomenclatureWithStringId("org.cocktail.fwkcktlsangria.contrats.langues").asArrayOfStrings();
		}

	}


	public Boolean isContratSansIncidenceFinanciere() {
		return ERXStringUtilities.stringEqualsString(contrat().modeDeGestion().mgLibelleCourt(), ModeGestion.MODE_GESTION_SIF);
	}

	public String getTypeContrat() {
		return contrat().typeContrat().tyconLibelle();
	}

}
