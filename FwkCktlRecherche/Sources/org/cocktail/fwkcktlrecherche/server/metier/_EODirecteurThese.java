/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODirecteurThese.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODirecteurThese extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DirecteurThese";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idDirecteur";

	public static final er.extensions.eof.ERXKey<java.lang.String> COMMENTAIRE = new er.extensions.eof.ERXKey<java.lang.String>("commentaire");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_AUTORISATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateAutorisation");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE_LABO = new er.extensions.eof.ERXKey<java.lang.String>("libelleLabo");
	
	public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
	public static final String DATE_AUTORISATION_KEY = DATE_AUTORISATION.key();
	public static final String LIBELLE_LABO_KEY = LIBELLE_LABO.key();

	// Attributs non visibles
	public static final String CP_ORDRE_KEY = "cpOrdre";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STRUCTURE_ETAB_KEY = "cStructureEtab";
	public static final String C_STRUCTURE_LABO_KEY = "cStructureLabo";
	public static final String ID_DIRECTEUR_KEY = "idDirecteur";
	public static final String ID_THESE_KEY = "idThese";

	//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_AUTORISATION_COLKEY = "DATE_AUTORISATION";
	public static final String LIBELLE_LABO_COLKEY = "LIBELLE_LABO";

	public static final String CP_ORDRE_COLKEY = "CP_ORDRE";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_STRUCTURE_ETAB_COLKEY = "C_STRUCTURE_ETAB";
	public static final String C_STRUCTURE_LABO_COLKEY = "C_STRUCTURE_LABO";
	public static final String ID_DIRECTEUR_COLKEY = "ID_DIRECTEUR";
	public static final String ID_THESE_COLKEY = "ID_THESE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> TO_CONTRAT_PARTENAIRE_CW = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("toContratPartenaireCw");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> TO_DOCTORANT_THESE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese>("toDoctorantThese");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_ETAB_FWK_PERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureEtabFwkPers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_LABO_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureLaboFwkpers");
	
	public static final String TO_CONTRAT_PARTENAIRE_CW_KEY = TO_CONTRAT_PARTENAIRE_CW.key();
	public static final String TO_DOCTORANT_THESE_KEY = TO_DOCTORANT_THESE.key();
	public static final String TO_RNE_FWKPERS_KEY = TO_RNE_FWKPERS.key();
	public static final String TO_STRUCTURE_ETAB_FWK_PERS_KEY = TO_STRUCTURE_ETAB_FWK_PERS.key();
	public static final String TO_STRUCTURE_LABO_FWKPERS_KEY = TO_STRUCTURE_LABO_FWKPERS.key();

	// Accessors methods
	public java.lang.String commentaire() {
		return (java.lang.String) storedValueForKey(COMMENTAIRE_KEY);
	}

	public void setCommentaire(java.lang.String value) {
	    takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateAutorisation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_AUTORISATION_KEY);
	}

	public void setDateAutorisation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_AUTORISATION_KEY);
	}
	
	public java.lang.String libelleLabo() {
		return (java.lang.String) storedValueForKey(LIBELLE_LABO_KEY);
	}

	public void setLibelleLabo(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_LABO_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaireCw() {
	    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire) storedValueForKey(TO_CONTRAT_PARTENAIRE_CW_KEY);
	}

	public void setToContratPartenaireCwRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.ContratPartenaire oldValue = toContratPartenaireCw();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_PARTENAIRE_CW_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_PARTENAIRE_CW_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese) storedValueForKey(TO_DOCTORANT_THESE_KEY);
	}

	public void setToDoctorantTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese oldValue = toDoctorantThese();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_THESE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_THESE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORne) storedValueForKey(TO_RNE_FWKPERS_KEY);
	}

	public void setToRneFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEtabFwkPers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	}

	public void setToStructureEtabFwkPersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureEtabFwkPers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureLaboFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_STRUCTURE_LABO_FWKPERS_KEY);
	}

	public void setToStructureLaboFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureLaboFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_LABO_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_LABO_FWKPERS_KEY);
	    }
	}
	
	public static EODirecteurThese createDirecteurThese(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaireCw, org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese) {
		EODirecteurThese eo = (EODirecteurThese) EOUtilities.createAndInsertInstance(editingContext, _EODirecteurThese.ENTITY_NAME);    
		eo.setToContratPartenaireCwRelationship(toContratPartenaireCw);
		eo.setToDoctorantTheseRelationship(toDoctorantThese);
		return eo;
	}

	public EODirecteurThese localInstanceIn(EOEditingContext editingContext) {
		return (EODirecteurThese) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODirecteurThese creerInstance(EOEditingContext editingContext) {
		return (EODirecteurThese) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODirecteurThese.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODirecteurThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODirecteurThese>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODirecteurThese fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODirecteurThese fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODirecteurThese> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODirecteurThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODirecteurThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODirecteurThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODirecteurThese> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODirecteurThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODirecteurThese fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODirecteurThese eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODirecteurThese ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODirecteurThese fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAllDirecteurTheses(EOEditingContext editingContext) {
		return _EODirecteurThese.fetchAllDirecteurTheses(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchAllDirecteurTheses(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODirecteurThese.fetchDirecteurTheses(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurThese> fetchDirecteurTheses(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODirecteurThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODirecteurThese>(_EODirecteurThese.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODirecteurThese> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODirecteurThese fetchDirecteurThese(EOEditingContext editingContext, String keyName, Object value) {
		return _EODirecteurThese.fetchDirecteurThese(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODirecteurThese fetchDirecteurThese(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODirecteurThese> eoObjects = _EODirecteurThese.fetchDirecteurTheses(editingContext, qualifier, null);
	    EODirecteurThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DirecteurThese that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
