/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrandSecteurDisciplinaire.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOGrandSecteurDisciplinaire extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "GrandSecteurDisciplinaire";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "gsdId";

	public static final er.extensions.eof.ERXKey<java.lang.String> GSD_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("gsdLibelle");
	
	public static final String GSD_LIBELLE_KEY = GSD_LIBELLE.key();

	// Attributs non visibles
	public static final String GSD_ID_KEY = "gsdId";

	//Colonnes dans la base de donnees
	public static final String GSD_LIBELLE_COLKEY = "GSD_LIBELLE";

	public static final String GSD_ID_COLKEY = "GSD_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique>("repartGrandSecteurDisciplinaireDomaineScientifiques");
	
	public static final String REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY = REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES.key();

	// Accessors methods
	public java.lang.String gsdLibelle() {
		return (java.lang.String) storedValueForKey(GSD_LIBELLE_KEY);
	}

	public void setGsdLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, GSD_LIBELLE_KEY);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> repartGrandSecteurDisciplinaireDomaineScientifiques() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> repartGrandSecteurDisciplinaireDomaineScientifiques(EOQualifier qualifier) {
		return repartGrandSecteurDisciplinaireDomaineScientifiques(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> repartGrandSecteurDisciplinaireDomaineScientifiques(EOQualifier qualifier, boolean fetch) {
	    return repartGrandSecteurDisciplinaireDomaineScientifiques(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> repartGrandSecteurDisciplinaireDomaineScientifiques(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique.GRAND_SECTEUR_DISCIPLINAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = repartGrandSecteurDisciplinaireDomaineScientifiques();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique object) {
		addObjectToBothSidesOfRelationshipWithKey(object, REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public void removeFromRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique createRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartGrandSecteurDisciplinaireDomaineScientifique");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique) eo;
	}

	public void deleteRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_GRAND_SECTEUR_DISCIPLINAIRE_DOMAINE_SCIENTIFIQUES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationships() {
		java.util.Enumeration objects = repartGrandSecteurDisciplinaireDomaineScientifiques().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteRepartGrandSecteurDisciplinaireDomaineScientifiquesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique) objects.nextElement());
	    }
	}

	public static EOGrandSecteurDisciplinaire createGrandSecteurDisciplinaire(EOEditingContext editingContext, java.lang.String gsdLibelle) {
		EOGrandSecteurDisciplinaire eo = (EOGrandSecteurDisciplinaire) EOUtilities.createAndInsertInstance(editingContext, _EOGrandSecteurDisciplinaire.ENTITY_NAME);    
		eo.setGsdLibelle(gsdLibelle);
		return eo;
	}

	public EOGrandSecteurDisciplinaire localInstanceIn(EOEditingContext editingContext) {
		return (EOGrandSecteurDisciplinaire) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOGrandSecteurDisciplinaire creerInstance(EOEditingContext editingContext) {
		return (EOGrandSecteurDisciplinaire) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOGrandSecteurDisciplinaire.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOGrandSecteurDisciplinaire> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOGrandSecteurDisciplinaire>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOGrandSecteurDisciplinaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOGrandSecteurDisciplinaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrandSecteurDisciplinaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOGrandSecteurDisciplinaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOGrandSecteurDisciplinaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrandSecteurDisciplinaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOGrandSecteurDisciplinaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOGrandSecteurDisciplinaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOGrandSecteurDisciplinaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOGrandSecteurDisciplinaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAllGrandSecteurDisciplinaires(EOEditingContext editingContext) {
		return _EOGrandSecteurDisciplinaire.fetchAllGrandSecteurDisciplinaires(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchAllGrandSecteurDisciplinaires(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOGrandSecteurDisciplinaire.fetchGrandSecteurDisciplinaires(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> fetchGrandSecteurDisciplinaires(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOGrandSecteurDisciplinaire> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOGrandSecteurDisciplinaire>(_EOGrandSecteurDisciplinaire.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOGrandSecteurDisciplinaire fetchGrandSecteurDisciplinaire(EOEditingContext editingContext, String keyName, Object value) {
		return _EOGrandSecteurDisciplinaire.fetchGrandSecteurDisciplinaire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOGrandSecteurDisciplinaire fetchGrandSecteurDisciplinaire(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOGrandSecteurDisciplinaire> eoObjects = _EOGrandSecteurDisciplinaire.fetchGrandSecteurDisciplinaires(editingContext, qualifier, null);
	    EOGrandSecteurDisciplinaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one GrandSecteurDisciplinaire that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
