/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOProgramme.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOProgramme extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Programme";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final er.extensions.eof.ERXKey<java.lang.String> CODE = new er.extensions.eof.ERXKey<java.lang.String>("code");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("libelle");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> MODIFIABLE = new er.extensions.eof.ERXKey<java.lang.Boolean>("modifiable");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	
	public static final String CODE_KEY = CODE.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String LIBELLE_KEY = LIBELLE.key();
	public static final String MODIFIABLE_KEY = MODIFIABLE.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();

	// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String ID_KEY = "id";

	//Colonnes dans la base de donnees
	public static final String CODE_COLKEY = "CODE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIBELLE_COLKEY = "LL_PROGRAMME";
	public static final String MODIFIABLE_COLKEY = "MODIFIABLE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ID_COLKEY = "ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> ANNEES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee>("annees");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> FINANCEUR = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("financeur");
	
	public static final String ANNEES_KEY = ANNEES.key();
	public static final String FINANCEUR_KEY = FINANCEUR.key();

	// Accessors methods
	public java.lang.String code() {
		return (java.lang.String) storedValueForKey(CODE_KEY);
	}

	public void setCode(java.lang.String value) {
	    takeStoredValueForKey(value, CODE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.String libelle() {
		return (java.lang.String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_KEY);
	}
	
	public java.lang.Boolean modifiable() {
		return (java.lang.Boolean) storedValueForKey(MODIFIABLE_KEY);
	}

	public void setModifiable(java.lang.Boolean value) {
	    takeStoredValueForKey(value, MODIFIABLE_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure financeur() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(FINANCEUR_KEY);
	}

	public void setFinanceurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = financeur();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FINANCEUR_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, FINANCEUR_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> annees() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(ANNEES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> annees(EOQualifier qualifier) {
		return annees(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> annees(EOQualifier qualifier, boolean fetch) {
	    return annees(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> annees(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee.PROGRAMME_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = annees();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToAnneesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee object) {
		addObjectToBothSidesOfRelationshipWithKey(object, ANNEES_KEY);
	}

	public void removeFromAnneesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, ANNEES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee createAnneesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ProgrammeAnnee");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, ANNEES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee) eo;
	}

	public void deleteAnneesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, ANNEES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllAnneesRelationships() {
		java.util.Enumeration objects = annees().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteAnneesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee) objects.nextElement());
	    }
	}

	public static EOProgramme createProgramme(EOEditingContext editingContext, java.lang.String code, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.String libelle, java.lang.Boolean modifiable, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, org.cocktail.fwkcktlpersonne.common.metier.EOStructure financeur) {
		EOProgramme eo = (EOProgramme) EOUtilities.createAndInsertInstance(editingContext, _EOProgramme.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setModifiable(modifiable);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setFinanceurRelationship(financeur);
		return eo;
	}

	public EOProgramme localInstanceIn(EOEditingContext editingContext) {
		return (EOProgramme) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOProgramme creerInstance(EOEditingContext editingContext) {
		return (EOProgramme) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOProgramme.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOProgramme> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOProgramme>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOProgramme fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOProgramme fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOProgramme> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOProgramme eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOProgramme fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOProgramme fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOProgramme> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOProgramme eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOProgramme fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOProgramme eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOProgramme ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOProgramme fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAllProgrammes(EOEditingContext editingContext) {
		return _EOProgramme.fetchAllProgrammes(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOProgramme> fetchAllProgrammes(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOProgramme.fetchProgrammes(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOProgramme> fetchProgrammes(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOProgramme> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOProgramme>(_EOProgramme.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOProgramme> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOProgramme fetchProgramme(EOEditingContext editingContext, String keyName, Object value) {
		return _EOProgramme.fetchProgramme(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOProgramme fetchProgramme(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOProgramme> eoObjects = _EOProgramme.fetchProgrammes(editingContext, qualifier, null);
	    EOProgramme eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one Programme that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
