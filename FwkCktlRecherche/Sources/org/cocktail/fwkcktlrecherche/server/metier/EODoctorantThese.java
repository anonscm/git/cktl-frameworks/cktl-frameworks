/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;


public class EODoctorantThese extends _EODoctorantThese {

    private static final long serialVersionUID = -7191088412012931502L;

	public static final String SC_AVENANT_ZERO = "avenantZero";
	public static final String SC_DATE_DEB = "dateDeb";
	public static final String SC_DATE_FIN = "dateFin";
	public static final String SC_DATE_FIN_ANTICIPEE = "dateFinAnticipee";

    public static final ERXKey<EOStructure> ECOLE_DOCTORALE = new ERXKey<EOStructure>("ecoleDoctorale");
    public static final ERXKey<EOStructure> LABORATOIRES = new ERXKey<EOStructure>("laboratoires");
    
    public static final EOSortOrdering SORT_NOM_DOCTORANT = EOSortOrdering.sortOrderingWithKey(TO_DOCTORANT_KEY + "." + EODoctorant.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.NOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
	
	
	public Avenant avenantZero() {
		return toContrat().avenantZero();
	}

	public NSTimestamp dateDeb() {
		return toContrat().dateDebut();
	}

	public NSTimestamp dateFin() {
		return toContrat().dateFin();
	}

	public boolean isTemporary() {
		return editingContext().globalIDForObject(this).isTemporary();
	}

	public EOStructure ecoleDoctorale() {

		EOAssociation roleEcoleDoctorale = FactoryAssociation.shared().ecoleDoctoraleAssociation(editingContext());
		if (roleEcoleDoctorale == null) {
			return null;
		}
		NSArray<ContratPartenaire> partenairesEcole = toContrat().partenairesForAssociation(roleEcoleDoctorale);
		if (partenairesEcole.isEmpty()) {
			return null;
		}
		return (EOStructure) partenairesEcole.lastObject().valueForKey(ContratPartenaire.PARTENAIRE_KEY);
	}
	
	public NSArray<EOStructure> getLaboratoires() {
		return toContrat().structuresPartenairesForAssociation(
				FactoryAssociation.shared().laboratoireTheseAssociation(editingContext()));
	}

	public Integer nbDirecteurs() {
		return nbDirecteurs(null, null);
	}

	public Integer nbDirecteurs(NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSArray<EORepartAssociation> lesRepartAssocDirecteur = null;
		lesRepartAssocDirecteur = toContrat().repartAssociationPartenairesForAssociationAuxDates(FactoryAssociation.shared().directeurTheseAssociation(this.editingContext()),
				dateDebut, dateFin);
		if (lesRepartAssocDirecteur != null) {
			return lesRepartAssocDirecteur.count();
		} else {
			return 0;
		}
	}

	public Boolean isDateCoherente(NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (dateDebut.before(toContrat().dateDebut()) || dateFin.after(toContrat().dateFin())) {
			return false;
		}
		return true;

	}
	
	public Boolean isCotutelle() {
		return cotutelle().equals("O");
	}
	
	public Boolean isTabCotutelleDisabled() {
		return !isCotutelle();
	}
	
	public NSArray<EOMembreJuryThese> jures() {
		EOQualifier qualifier = 
					EOMembreJuryThese.TO_CONTRAT_PARTENAIRE.in(
							toContrat().partenairesForAssociations(
									FactoryAssociation.shared().rolesJuryAssociation(editingContext()).getFils(editingContext()
											)
										)
									);
		return toMembreJuryTheses(qualifier);

	}
	
	public NSArray<EOMembreJuryThese> rapporteurs() {
		EOQualifier qualifier = 
					EOMembreJuryThese.TO_CONTRAT_PARTENAIRE.in(
							toContrat().partenairesForAssociation(
									FactoryAssociation.shared().rapporteurTheseAssociation(editingContext()))
									);
		return toMembreJuryTheses(qualifier);

	}
	
}
