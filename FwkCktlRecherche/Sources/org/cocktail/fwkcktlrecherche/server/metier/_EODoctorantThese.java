/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODoctorantThese.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODoctorantThese extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DoctorantThese";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idThese";

	public static final er.extensions.eof.ERXKey<java.lang.Integer> ANNEE_REF_OBTENTION = new er.extensions.eof.ERXKey<java.lang.Integer>("anneeRefObtention");
	public static final er.extensions.eof.ERXKey<java.lang.String> CODE_SISE_DIPLOME = new er.extensions.eof.ERXKey<java.lang.String>("codeSiseDiplome");
	public static final er.extensions.eof.ERXKey<java.lang.String> CONTRACTUEL = new er.extensions.eof.ERXKey<java.lang.String>("contractuel");
	public static final er.extensions.eof.ERXKey<java.lang.String> COTUTELLE = new er.extensions.eof.ERXKey<java.lang.String>("cotutelle");
	public static final er.extensions.eof.ERXKey<java.lang.String> C_RNE_DELIVRE_DIPLOME = new er.extensions.eof.ERXKey<java.lang.String>("cRneDelivreDiplome");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_FIN_ANTICIPEE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateFinAnticipee");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PREV_SOUTENANCE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePrevSoutenance");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_SOUTENANCE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateSoutenance");
	public static final er.extensions.eof.ERXKey<java.lang.String> FDIP_CODE = new er.extensions.eof.ERXKey<java.lang.String>("fdipCode");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> ID_MENTION = new er.extensions.eof.ERXKey<java.lang.Integer>("idMention");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIEU_SOUTENANCE = new er.extensions.eof.ERXKey<java.lang.String>("lieuSoutenance");
	public static final er.extensions.eof.ERXKey<java.lang.String> MOTIF_FIN_ANTICIPEE = new er.extensions.eof.ERXKey<java.lang.String>("motifFinAnticipee");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> NUMERO_DIPLOME = new er.extensions.eof.ERXKey<java.lang.Integer>("numeroDiplome");
	public static final er.extensions.eof.ERXKey<java.lang.String> OBSERVATIONS_SOUTENANCE = new er.extensions.eof.ERXKey<java.lang.String>("observationsSoutenance");
	public static final er.extensions.eof.ERXKey<java.lang.String> RESUME_THESE = new er.extensions.eof.ERXKey<java.lang.String>("resumeThese");
	
	public static final String ANNEE_REF_OBTENTION_KEY = ANNEE_REF_OBTENTION.key();
	public static final String CODE_SISE_DIPLOME_KEY = CODE_SISE_DIPLOME.key();
	public static final String CONTRACTUEL_KEY = CONTRACTUEL.key();
	public static final String COTUTELLE_KEY = COTUTELLE.key();
	public static final String C_RNE_DELIVRE_DIPLOME_KEY = C_RNE_DELIVRE_DIPLOME.key();
	public static final String DATE_FIN_ANTICIPEE_KEY = DATE_FIN_ANTICIPEE.key();
	public static final String DATE_PREV_SOUTENANCE_KEY = DATE_PREV_SOUTENANCE.key();
	public static final String DATE_SOUTENANCE_KEY = DATE_SOUTENANCE.key();
	public static final String FDIP_CODE_KEY = FDIP_CODE.key();
	public static final String ID_MENTION_KEY = ID_MENTION.key();
	public static final String LIEU_SOUTENANCE_KEY = LIEU_SOUTENANCE.key();
	public static final String MOTIF_FIN_ANTICIPEE_KEY = MOTIF_FIN_ANTICIPEE.key();
	public static final String NUMERO_DIPLOME_KEY = NUMERO_DIPLOME.key();
	public static final String OBSERVATIONS_SOUTENANCE_KEY = OBSERVATIONS_SOUTENANCE.key();
	public static final String RESUME_THESE_KEY = RESUME_THESE.key();

	// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ID_DOCTORANT_KEY = "idDoctorant";
	public static final String ID_THESE_KEY = "idThese";
	public static final String ID_THESE_DOMAINE_KEY = "idTheseDomaine";
	public static final String PRSC_ORDRE_KEY = "prscOrdre";

	//Colonnes dans la base de donnees
	public static final String ANNEE_REF_OBTENTION_COLKEY = "ANNEE_REF_OBTENTION";
	public static final String CODE_SISE_DIPLOME_COLKEY = "CODE_SISE_DIPLOME";
	public static final String CONTRACTUEL_COLKEY = "CONTRACTUEL";
	public static final String COTUTELLE_COLKEY = "COTUTELLE";
	public static final String C_RNE_DELIVRE_DIPLOME_COLKEY = "C_RNE_DELIVRE_DIPLOME";
	public static final String DATE_FIN_ANTICIPEE_COLKEY = "DATE_FIN_ANTICIPEE";
	public static final String DATE_PREV_SOUTENANCE_COLKEY = "DATE_PREV_SOUTENANCE";
	public static final String DATE_SOUTENANCE_COLKEY = "DATE_SOUTENANCE";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String ID_MENTION_COLKEY = "ID_MENTION";
	public static final String LIEU_SOUTENANCE_COLKEY = "LIEU_SOUTENANCE";
	public static final String MOTIF_FIN_ANTICIPEE_COLKEY = "MOTIF_FIN_ANTICIPEE";
	public static final String NUMERO_DIPLOME_COLKEY = "NUMERO_DIPLOME";
	public static final String OBSERVATIONS_SOUTENANCE_COLKEY = "OBSERVATIONS_SOUTENANCE";
	public static final String RESUME_THESE_COLKEY = "RESUME_THESE";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ID_DOCTORANT_COLKEY = "ID_DOCTORANT";
	public static final String ID_THESE_COLKEY = "ID_THESE";
	public static final String ID_THESE_DOMAINE_COLKEY = "ID_THESE_DOMAINE";
	public static final String PRSC_ORDRE_COLKEY = "PRSC_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> TO_CONTRAT = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("toContrat");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> TO_DIRECTEURS_THESE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese>("toDirecteursThese");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant> TO_DOCTORANT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorant>("toDoctorant");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> TO_MEMBRE_JURY_THESES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese>("toMembreJuryTheses");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique> TO_PROJET_SCIENTIFIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique>("toProjetScientifique");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine> TO_THESE_DOMAINE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine>("toTheseDomaine");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention> TO_THESE_MENTION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention>("toTheseMention");
	
	public static final String TO_CONTRAT_KEY = TO_CONTRAT.key();
	public static final String TO_DIRECTEURS_THESE_KEY = TO_DIRECTEURS_THESE.key();
	public static final String TO_DOCTORANT_KEY = TO_DOCTORANT.key();
	public static final String TO_MEMBRE_JURY_THESES_KEY = TO_MEMBRE_JURY_THESES.key();
	public static final String TO_PROJET_SCIENTIFIQUE_KEY = TO_PROJET_SCIENTIFIQUE.key();
	public static final String TO_THESE_DOMAINE_KEY = TO_THESE_DOMAINE.key();
	public static final String TO_THESE_MENTION_KEY = TO_THESE_MENTION.key();

	// Accessors methods
	public java.lang.Integer anneeRefObtention() {
		return (java.lang.Integer) storedValueForKey(ANNEE_REF_OBTENTION_KEY);
	}

	public void setAnneeRefObtention(java.lang.Integer value) {
	    takeStoredValueForKey(value, ANNEE_REF_OBTENTION_KEY);
	}
	
	public java.lang.String codeSiseDiplome() {
		return (java.lang.String) storedValueForKey(CODE_SISE_DIPLOME_KEY);
	}

	public void setCodeSiseDiplome(java.lang.String value) {
	    takeStoredValueForKey(value, CODE_SISE_DIPLOME_KEY);
	}
	
	public java.lang.String contractuel() {
		return (java.lang.String) storedValueForKey(CONTRACTUEL_KEY);
	}

	public void setContractuel(java.lang.String value) {
	    takeStoredValueForKey(value, CONTRACTUEL_KEY);
	}
	
	public java.lang.String cotutelle() {
		return (java.lang.String) storedValueForKey(COTUTELLE_KEY);
	}

	public void setCotutelle(java.lang.String value) {
	    takeStoredValueForKey(value, COTUTELLE_KEY);
	}
	
	public java.lang.String cRneDelivreDiplome() {
		return (java.lang.String) storedValueForKey(C_RNE_DELIVRE_DIPLOME_KEY);
	}

	public void setCRneDelivreDiplome(java.lang.String value) {
	    takeStoredValueForKey(value, C_RNE_DELIVRE_DIPLOME_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateFinAnticipee() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_FIN_ANTICIPEE_KEY);
	}

	public void setDateFinAnticipee(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_FIN_ANTICIPEE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePrevSoutenance() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PREV_SOUTENANCE_KEY);
	}

	public void setDatePrevSoutenance(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PREV_SOUTENANCE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateSoutenance() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_SOUTENANCE_KEY);
	}

	public void setDateSoutenance(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_SOUTENANCE_KEY);
	}
	
	public java.lang.String fdipCode() {
		return (java.lang.String) storedValueForKey(FDIP_CODE_KEY);
	}

	public void setFdipCode(java.lang.String value) {
	    takeStoredValueForKey(value, FDIP_CODE_KEY);
	}
	
	public java.lang.Integer idMention() {
		return (java.lang.Integer) storedValueForKey(ID_MENTION_KEY);
	}

	public void setIdMention(java.lang.Integer value) {
	    takeStoredValueForKey(value, ID_MENTION_KEY);
	}
	
	public java.lang.String lieuSoutenance() {
		return (java.lang.String) storedValueForKey(LIEU_SOUTENANCE_KEY);
	}

	public void setLieuSoutenance(java.lang.String value) {
	    takeStoredValueForKey(value, LIEU_SOUTENANCE_KEY);
	}
	
	public java.lang.String motifFinAnticipee() {
		return (java.lang.String) storedValueForKey(MOTIF_FIN_ANTICIPEE_KEY);
	}

	public void setMotifFinAnticipee(java.lang.String value) {
	    takeStoredValueForKey(value, MOTIF_FIN_ANTICIPEE_KEY);
	}
	
	public java.lang.Integer numeroDiplome() {
		return (java.lang.Integer) storedValueForKey(NUMERO_DIPLOME_KEY);
	}

	public void setNumeroDiplome(java.lang.Integer value) {
	    takeStoredValueForKey(value, NUMERO_DIPLOME_KEY);
	}
	
	public java.lang.String observationsSoutenance() {
		return (java.lang.String) storedValueForKey(OBSERVATIONS_SOUTENANCE_KEY);
	}

	public void setObservationsSoutenance(java.lang.String value) {
	    takeStoredValueForKey(value, OBSERVATIONS_SOUTENANCE_KEY);
	}
	
	public java.lang.String resumeThese() {
		return (java.lang.String) storedValueForKey(RESUME_THESE_KEY);
	}

	public void setResumeThese(java.lang.String value) {
	    takeStoredValueForKey(value, RESUME_THESE_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.Contrat toContrat() {
	    return (org.cocktail.cocowork.server.metier.convention.Contrat) storedValueForKey(TO_CONTRAT_KEY);
	}

	public void setToContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.Contrat oldValue = toContrat();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorant toDoctorant() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorant) storedValueForKey(TO_DOCTORANT_KEY);
	}

	public void setToDoctorantRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorant value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorant oldValue = toDoctorant();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique toProjetScientifique() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique) storedValueForKey(TO_PROJET_SCIENTIFIQUE_KEY);
	}

	public void setToProjetScientifiqueRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique oldValue = toProjetScientifique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROJET_SCIENTIFIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROJET_SCIENTIFIQUE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine toTheseDomaine() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine) storedValueForKey(TO_THESE_DOMAINE_KEY);
	}

	public void setToTheseDomaineRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine oldValue = toTheseDomaine();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_THESE_DOMAINE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_THESE_DOMAINE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention toTheseMention() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention) storedValueForKey(TO_THESE_MENTION_KEY);
	}

	public void setToTheseMentionRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention oldValue = toTheseMention();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_THESE_MENTION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_THESE_MENTION_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> toDirecteursThese() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_DIRECTEURS_THESE_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> toDirecteursThese(EOQualifier qualifier) {
		return toDirecteursThese(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> toDirecteursThese(EOQualifier qualifier, boolean fetch) {
	    return toDirecteursThese(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> toDirecteursThese(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese.TO_DOCTORANT_THESE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toDirecteursThese();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToDirecteursTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_DIRECTEURS_THESE_KEY);
	}

	public void removeFromToDirecteursTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DIRECTEURS_THESE_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese createToDirecteursTheseRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DirecteurThese");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DIRECTEURS_THESE_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese) eo;
	}

	public void deleteToDirecteursTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DIRECTEURS_THESE_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToDirecteursTheseRelationships() {
		java.util.Enumeration objects = toDirecteursThese().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToDirecteursTheseRelationship((org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> toMembreJuryTheses() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_MEMBRE_JURY_THESES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> toMembreJuryTheses(EOQualifier qualifier) {
		return toMembreJuryTheses(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> toMembreJuryTheses(EOQualifier qualifier, boolean fetch) {
	    return toMembreJuryTheses(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> toMembreJuryTheses(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese.TO_DOCTORANT_THESE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toMembreJuryTheses();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToMembreJuryThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_MEMBRE_JURY_THESES_KEY);
	}

	public void removeFromToMembreJuryThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MEMBRE_JURY_THESES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese createToMembreJuryThesesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MembreJuryThese");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_MEMBRE_JURY_THESES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese) eo;
	}

	public void deleteToMembreJuryThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MEMBRE_JURY_THESES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToMembreJuryThesesRelationships() {
		java.util.Enumeration objects = toMembreJuryTheses().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToMembreJuryThesesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese) objects.nextElement());
	    }
	}

	public static EODoctorantThese createDoctorantThese(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.Contrat toContrat, org.cocktail.fwkcktlrecherche.server.metier.EODoctorant toDoctorant, org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine toTheseDomaine) {
		EODoctorantThese eo = (EODoctorantThese) EOUtilities.createAndInsertInstance(editingContext, _EODoctorantThese.ENTITY_NAME);    
		eo.setToContratRelationship(toContrat);
		eo.setToDoctorantRelationship(toDoctorant);
		eo.setToTheseDomaineRelationship(toTheseDomaine);
		return eo;
	}

	public EODoctorantThese localInstanceIn(EOEditingContext editingContext) {
		return (EODoctorantThese) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODoctorantThese creerInstance(EOEditingContext editingContext) {
		return (EODoctorantThese) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODoctorantThese.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantThese>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODoctorantThese fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODoctorantThese fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODoctorantThese> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODoctorantThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODoctorantThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODoctorantThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODoctorantThese> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODoctorantThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODoctorantThese fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODoctorantThese eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODoctorantThese ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODoctorantThese fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAllDoctorantTheses(EOEditingContext editingContext) {
		return _EODoctorantThese.fetchAllDoctorantTheses(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchAllDoctorantTheses(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODoctorantThese.fetchDoctorantTheses(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantThese> fetchDoctorantTheses(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantThese>(_EODoctorantThese.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODoctorantThese> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODoctorantThese fetchDoctorantThese(EOEditingContext editingContext, String keyName, Object value) {
		return _EODoctorantThese.fetchDoctorantThese(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODoctorantThese fetchDoctorantThese(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODoctorantThese> eoObjects = _EODoctorantThese.fetchDoctorantTheses(editingContext, qualifier, null);
	    EODoctorantThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DoctorantThese that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
