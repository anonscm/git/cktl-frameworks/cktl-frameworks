/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAap.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOAap extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Aap";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "aapOrdre";

	public static final er.extensions.eof.ERXKey<java.lang.Boolean> AAP_SUPPRIME = new er.extensions.eof.ERXKey<java.lang.Boolean>("aapSupprime");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_PASSAGE_COM_MIXTE = new er.extensions.eof.ERXKey<java.lang.String>("avisPassageComMixte");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_PASSAGE_CONSEIL_RECHERCHE = new er.extensions.eof.ERXKey<java.lang.String>("avisPassageConseilRecherche");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_PASSAGE_CS = new er.extensions.eof.ERXKey<java.lang.String>("avisPassageCs");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_RAPPORTEUR_DESIGNE = new er.extensions.eof.ERXKey<java.lang.String>("avisRapporteurDesigne");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> CLASSEMENT_PASSAGE_CS = new er.extensions.eof.ERXKey<java.lang.Integer>("classementPassageCs");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> CON_ORDRE_LIE = new er.extensions.eof.ERXKey<java.lang.Integer>("conOrdreLie");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ALERTE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateAlerte");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ALERTE_SUIVI = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateAlerteSuivi");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ARCHIVAGE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateArchivage");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_COM_PERMANENT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateComPermanent");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_DEPOT_DOSSIER = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateDepotDossier");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ENV_FIN_DOSSIER = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateEnvFinDossier");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PASSAGE_COM_MIXTE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePassageComMixte");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PASSAGE_CONSEIL_RECHERCHE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePassageConseilRecherche");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PASSAGE_CS = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePassageCs");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_REFUS_ACCEPTATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateRefusAcceptation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_TRANSFERT_FINANCIER = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateTransfertFinancier");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_TRANSFERT_VALO = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateTransfertValo");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> DOSSIER_COMPLET = new er.extensions.eof.ERXKey<java.lang.Boolean>("dossierComplet");
	public static final er.extensions.eof.ERXKey<java.lang.String> ETAT_ACCEPTATION = new er.extensions.eof.ERXKey<java.lang.String>("etatAcceptation");
	public static final er.extensions.eof.ERXKey<java.lang.String> ETAT_MONTAGE = new er.extensions.eof.ERXKey<java.lang.String>("etatMontage");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_DEMANDE = new er.extensions.eof.ERXKey<java.lang.Float>("montantDemande");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_OBTENU = new er.extensions.eof.ERXKey<java.lang.Float>("montantObtenu");
	public static final er.extensions.eof.ERXKey<java.lang.String> MOYEN_DEPOT_DOSSIER = new er.extensions.eof.ERXKey<java.lang.String>("moyenDepotDossier");
	public static final er.extensions.eof.ERXKey<java.lang.String> MOYEN_NOTIFICATION_REFUS_ACCEPTATION = new er.extensions.eof.ERXKey<java.lang.String>("moyenNotificationRefusAcceptation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> SUBVENTION_OBTENUE = new er.extensions.eof.ERXKey<java.lang.Boolean>("subventionObtenue");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUIVI_VALO = new er.extensions.eof.ERXKey<java.lang.String>("suiviValo");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUJET_ALERTE = new er.extensions.eof.ERXKey<java.lang.String>("sujetAlerte");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUJET_ALERTE_SUIVI = new er.extensions.eof.ERXKey<java.lang.String>("sujetAlerteSuivi");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUPPORT_ENV_FINANCEUR = new er.extensions.eof.ERXKey<java.lang.String>("supportEnvFinanceur");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> TTC = new er.extensions.eof.ERXKey<java.lang.Boolean>("ttc");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_FINANCEMENT_DEMANDE = new er.extensions.eof.ERXKey<java.lang.String>("typeFinancementDemande");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_SUBVENTION_OBTENUE = new er.extensions.eof.ERXKey<java.lang.String>("typeSubventionObtenue");
	
	public static final String AAP_SUPPRIME_KEY = AAP_SUPPRIME.key();
	public static final String AVIS_PASSAGE_COM_MIXTE_KEY = AVIS_PASSAGE_COM_MIXTE.key();
	public static final String AVIS_PASSAGE_CONSEIL_RECHERCHE_KEY = AVIS_PASSAGE_CONSEIL_RECHERCHE.key();
	public static final String AVIS_PASSAGE_CS_KEY = AVIS_PASSAGE_CS.key();
	public static final String AVIS_RAPPORTEUR_DESIGNE_KEY = AVIS_RAPPORTEUR_DESIGNE.key();
	public static final String CLASSEMENT_PASSAGE_CS_KEY = CLASSEMENT_PASSAGE_CS.key();
	public static final String CON_ORDRE_LIE_KEY = CON_ORDRE_LIE.key();
	public static final String DATE_ALERTE_KEY = DATE_ALERTE.key();
	public static final String DATE_ALERTE_SUIVI_KEY = DATE_ALERTE_SUIVI.key();
	public static final String DATE_ARCHIVAGE_KEY = DATE_ARCHIVAGE.key();
	public static final String DATE_COM_PERMANENT_KEY = DATE_COM_PERMANENT.key();
	public static final String DATE_DEPOT_DOSSIER_KEY = DATE_DEPOT_DOSSIER.key();
	public static final String DATE_ENV_FIN_DOSSIER_KEY = DATE_ENV_FIN_DOSSIER.key();
	public static final String DATE_PASSAGE_COM_MIXTE_KEY = DATE_PASSAGE_COM_MIXTE.key();
	public static final String DATE_PASSAGE_CONSEIL_RECHERCHE_KEY = DATE_PASSAGE_CONSEIL_RECHERCHE.key();
	public static final String DATE_PASSAGE_CS_KEY = DATE_PASSAGE_CS.key();
	public static final String DATE_REFUS_ACCEPTATION_KEY = DATE_REFUS_ACCEPTATION.key();
	public static final String DATE_TRANSFERT_FINANCIER_KEY = DATE_TRANSFERT_FINANCIER.key();
	public static final String DATE_TRANSFERT_VALO_KEY = DATE_TRANSFERT_VALO.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String DOSSIER_COMPLET_KEY = DOSSIER_COMPLET.key();
	public static final String ETAT_ACCEPTATION_KEY = ETAT_ACCEPTATION.key();
	public static final String ETAT_MONTAGE_KEY = ETAT_MONTAGE.key();
	public static final String MONTANT_DEMANDE_KEY = MONTANT_DEMANDE.key();
	public static final String MONTANT_OBTENU_KEY = MONTANT_OBTENU.key();
	public static final String MOYEN_DEPOT_DOSSIER_KEY = MOYEN_DEPOT_DOSSIER.key();
	public static final String MOYEN_NOTIFICATION_REFUS_ACCEPTATION_KEY = MOYEN_NOTIFICATION_REFUS_ACCEPTATION.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String SUBVENTION_OBTENUE_KEY = SUBVENTION_OBTENUE.key();
	public static final String SUIVI_VALO_KEY = SUIVI_VALO.key();
	public static final String SUJET_ALERTE_KEY = SUJET_ALERTE.key();
	public static final String SUJET_ALERTE_SUIVI_KEY = SUJET_ALERTE_SUIVI.key();
	public static final String SUPPORT_ENV_FINANCEUR_KEY = SUPPORT_ENV_FINANCEUR.key();
	public static final String TTC_KEY = TTC.key();
	public static final String TYPE_FINANCEMENT_DEMANDE_KEY = TYPE_FINANCEMENT_DEMANDE.key();
	public static final String TYPE_SUBVENTION_OBTENUE_KEY = TYPE_SUBVENTION_OBTENUE.key();

	// Attributs non visibles
	public static final String AAP_ORDRE_KEY = "aapOrdre";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_KEY = "domaineScientifiqueOrdre";
	public static final String PROGRAMME_ID_KEY = "programmeId";
	public static final String SOUS_PROGRAMME_ID_KEY = "sousProgrammeId";

	//Colonnes dans la base de donnees
	public static final String AAP_SUPPRIME_COLKEY = "AAP_SUPPR";
	public static final String AVIS_PASSAGE_COM_MIXTE_COLKEY = "AVIS_PASSAGE_COM_MIXTE";
	public static final String AVIS_PASSAGE_CONSEIL_RECHERCHE_COLKEY = "AVIS_PASSAGE_CONSEIL_RECHERCHE";
	public static final String AVIS_PASSAGE_CS_COLKEY = "AVIS_PASSAGE_CS";
	public static final String AVIS_RAPPORTEUR_DESIGNE_COLKEY = "AVIS_RAPPORTEUR_DESIGNE";
	public static final String CLASSEMENT_PASSAGE_CS_COLKEY = "CLASSEMENT_PASSAGE_CS";
	public static final String CON_ORDRE_LIE_COLKEY = "CON_ORDRE_LIE";
	public static final String DATE_ALERTE_COLKEY = "D_ALERTE";
	public static final String DATE_ALERTE_SUIVI_COLKEY = "D_ALERTE_SUIVI";
	public static final String DATE_ARCHIVAGE_COLKEY = "D_ARCHIVAGE";
	public static final String DATE_COM_PERMANENT_COLKEY = "DATE_COM_PERMANANT";
	public static final String DATE_DEPOT_DOSSIER_COLKEY = "DATE_DEPOT_DOSSIER";
	public static final String DATE_ENV_FIN_DOSSIER_COLKEY = "DATE_ENV_DOSSIER_FIN";
	public static final String DATE_PASSAGE_COM_MIXTE_COLKEY = "DATE_PASSAGE_COM_MIXTE";
	public static final String DATE_PASSAGE_CONSEIL_RECHERCHE_COLKEY = "DATE_PASSAGE_CONSEIL_RECHERCHE";
	public static final String DATE_PASSAGE_CS_COLKEY = "DATE_PASSAGE_CS";
	public static final String DATE_REFUS_ACCEPTATION_COLKEY = "DATE_REFUS_ACCEPT";
	public static final String DATE_TRANSFERT_FINANCIER_COLKEY = "D_TRANSFERT_FINANCIER";
	public static final String DATE_TRANSFERT_VALO_COLKEY = "D_TRANSFERT_VALO";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DOSSIER_COMPLET_COLKEY = "DOSSIER_COMPLET";
	public static final String ETAT_ACCEPTATION_COLKEY = "ETAT_ACCEPT";
	public static final String ETAT_MONTAGE_COLKEY = "ETAT_MONTAGE";
	public static final String MONTANT_DEMANDE_COLKEY = "MONTANT_DEMANDE";
	public static final String MONTANT_OBTENU_COLKEY = "MONTANT_OBT";
	public static final String MOYEN_DEPOT_DOSSIER_COLKEY = "MOYEN_DEPOT_DOSSIER";
	public static final String MOYEN_NOTIFICATION_REFUS_ACCEPTATION_COLKEY = "MOY_NOTIF_REF_ACC";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String SUBVENTION_OBTENUE_COLKEY = "SUBVENTION_OBT";
	public static final String SUIVI_VALO_COLKEY = "SUIVI_VALO";
	public static final String SUJET_ALERTE_COLKEY = "SUJET_ALERTE";
	public static final String SUJET_ALERTE_SUIVI_COLKEY = "SUJET_ALERTE_SUIVI";
	public static final String SUPPORT_ENV_FINANCEUR_COLKEY = "SUPPORT_ENV_FINANCEUR";
	public static final String TTC_COLKEY = "TTC";
	public static final String TYPE_FINANCEMENT_DEMANDE_COLKEY = "TYPE_FINANCEMENT_DEMANDE";
	public static final String TYPE_SUBVENTION_OBTENUE_COLKEY = "TYPE_SUBV_OBT";

	public static final String AAP_ORDRE_COLKEY = "AAP_ORDRE";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_COLKEY = "APP_DOMAINE_SCIENT_ORDRE";
	public static final String PROGRAMME_ID_COLKEY = "APP_PROG_ID";
	public static final String SOUS_PROGRAMME_ID_COLKEY = "APP_SS_PROG_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT_LIE = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contratLie");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme> PROGRAMME = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme>("programme");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme> SOUS_PROGRAMME = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme>("sousProgramme");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> TO_REPART_AAP_AVIS_RAPPORTEURS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur>("toRepartAapAvisRapporteurs");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> TYPES_FINANCEMENT_AAP = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap>("typesFinancementAap");
	
	public static final String CONTRAT_KEY = CONTRAT.key();
	public static final String CONTRAT_LIE_KEY = CONTRAT_LIE.key();
	public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();
	public static final String PROGRAMME_KEY = PROGRAMME.key();
	public static final String SOUS_PROGRAMME_KEY = SOUS_PROGRAMME.key();
	public static final String TO_REPART_AAP_AVIS_RAPPORTEURS_KEY = TO_REPART_AAP_AVIS_RAPPORTEURS.key();
	public static final String TYPES_FINANCEMENT_AAP_KEY = TYPES_FINANCEMENT_AAP.key();

	// Accessors methods
	public java.lang.Boolean aapSupprime() {
		return (java.lang.Boolean) storedValueForKey(AAP_SUPPRIME_KEY);
	}

	public void setAapSupprime(java.lang.Boolean value) {
	    takeStoredValueForKey(value, AAP_SUPPRIME_KEY);
	}
	
	public java.lang.String avisPassageComMixte() {
		return (java.lang.String) storedValueForKey(AVIS_PASSAGE_COM_MIXTE_KEY);
	}

	public void setAvisPassageComMixte(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_PASSAGE_COM_MIXTE_KEY);
	}
	
	public java.lang.String avisPassageConseilRecherche() {
		return (java.lang.String) storedValueForKey(AVIS_PASSAGE_CONSEIL_RECHERCHE_KEY);
	}

	public void setAvisPassageConseilRecherche(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_PASSAGE_CONSEIL_RECHERCHE_KEY);
	}
	
	public java.lang.String avisPassageCs() {
		return (java.lang.String) storedValueForKey(AVIS_PASSAGE_CS_KEY);
	}

	public void setAvisPassageCs(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_PASSAGE_CS_KEY);
	}
	
	public java.lang.String avisRapporteurDesigne() {
		return (java.lang.String) storedValueForKey(AVIS_RAPPORTEUR_DESIGNE_KEY);
	}

	public void setAvisRapporteurDesigne(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_RAPPORTEUR_DESIGNE_KEY);
	}
	
	public java.lang.Integer classementPassageCs() {
		return (java.lang.Integer) storedValueForKey(CLASSEMENT_PASSAGE_CS_KEY);
	}

	public void setClassementPassageCs(java.lang.Integer value) {
	    takeStoredValueForKey(value, CLASSEMENT_PASSAGE_CS_KEY);
	}
	
	public java.lang.Integer conOrdreLie() {
		return (java.lang.Integer) storedValueForKey(CON_ORDRE_LIE_KEY);
	}

	public void setConOrdreLie(java.lang.Integer value) {
	    takeStoredValueForKey(value, CON_ORDRE_LIE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateAlerte() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ALERTE_KEY);
	}

	public void setDateAlerte(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ALERTE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateAlerteSuivi() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ALERTE_SUIVI_KEY);
	}

	public void setDateAlerteSuivi(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ALERTE_SUIVI_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateArchivage() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ARCHIVAGE_KEY);
	}

	public void setDateArchivage(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ARCHIVAGE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateComPermanent() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_COM_PERMANENT_KEY);
	}

	public void setDateComPermanent(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_COM_PERMANENT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateDepotDossier() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_DEPOT_DOSSIER_KEY);
	}

	public void setDateDepotDossier(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_DEPOT_DOSSIER_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateEnvFinDossier() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ENV_FIN_DOSSIER_KEY);
	}

	public void setDateEnvFinDossier(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ENV_FIN_DOSSIER_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePassageComMixte() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PASSAGE_COM_MIXTE_KEY);
	}

	public void setDatePassageComMixte(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PASSAGE_COM_MIXTE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePassageConseilRecherche() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PASSAGE_CONSEIL_RECHERCHE_KEY);
	}

	public void setDatePassageConseilRecherche(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PASSAGE_CONSEIL_RECHERCHE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePassageCs() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PASSAGE_CS_KEY);
	}

	public void setDatePassageCs(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PASSAGE_CS_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateRefusAcceptation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_REFUS_ACCEPTATION_KEY);
	}

	public void setDateRefusAcceptation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_REFUS_ACCEPTATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateTransfertFinancier() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_TRANSFERT_FINANCIER_KEY);
	}

	public void setDateTransfertFinancier(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_TRANSFERT_FINANCIER_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateTransfertValo() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_TRANSFERT_VALO_KEY);
	}

	public void setDateTransfertValo(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_TRANSFERT_VALO_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Boolean dossierComplet() {
		return (java.lang.Boolean) storedValueForKey(DOSSIER_COMPLET_KEY);
	}

	public void setDossierComplet(java.lang.Boolean value) {
	    takeStoredValueForKey(value, DOSSIER_COMPLET_KEY);
	}
	
	public java.lang.String etatAcceptation() {
		return (java.lang.String) storedValueForKey(ETAT_ACCEPTATION_KEY);
	}

	public void setEtatAcceptation(java.lang.String value) {
	    takeStoredValueForKey(value, ETAT_ACCEPTATION_KEY);
	}
	
	public java.lang.String etatMontage() {
		return (java.lang.String) storedValueForKey(ETAT_MONTAGE_KEY);
	}

	public void setEtatMontage(java.lang.String value) {
	    takeStoredValueForKey(value, ETAT_MONTAGE_KEY);
	}
	
	public java.lang.Float montantDemande() {
		return (java.lang.Float) storedValueForKey(MONTANT_DEMANDE_KEY);
	}

	public void setMontantDemande(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_DEMANDE_KEY);
	}
	
	public java.lang.Float montantObtenu() {
		return (java.lang.Float) storedValueForKey(MONTANT_OBTENU_KEY);
	}

	public void setMontantObtenu(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_OBTENU_KEY);
	}
	
	public java.lang.String moyenDepotDossier() {
		return (java.lang.String) storedValueForKey(MOYEN_DEPOT_DOSSIER_KEY);
	}

	public void setMoyenDepotDossier(java.lang.String value) {
	    takeStoredValueForKey(value, MOYEN_DEPOT_DOSSIER_KEY);
	}
	
	public java.lang.String moyenNotificationRefusAcceptation() {
		return (java.lang.String) storedValueForKey(MOYEN_NOTIFICATION_REFUS_ACCEPTATION_KEY);
	}

	public void setMoyenNotificationRefusAcceptation(java.lang.String value) {
	    takeStoredValueForKey(value, MOYEN_NOTIFICATION_REFUS_ACCEPTATION_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.Boolean subventionObtenue() {
		return (java.lang.Boolean) storedValueForKey(SUBVENTION_OBTENUE_KEY);
	}

	public void setSubventionObtenue(java.lang.Boolean value) {
	    takeStoredValueForKey(value, SUBVENTION_OBTENUE_KEY);
	}
	
	public java.lang.String suiviValo() {
		return (java.lang.String) storedValueForKey(SUIVI_VALO_KEY);
	}

	public void setSuiviValo(java.lang.String value) {
	    takeStoredValueForKey(value, SUIVI_VALO_KEY);
	}
	
	public java.lang.String sujetAlerte() {
		return (java.lang.String) storedValueForKey(SUJET_ALERTE_KEY);
	}

	public void setSujetAlerte(java.lang.String value) {
	    takeStoredValueForKey(value, SUJET_ALERTE_KEY);
	}
	
	public java.lang.String sujetAlerteSuivi() {
		return (java.lang.String) storedValueForKey(SUJET_ALERTE_SUIVI_KEY);
	}

	public void setSujetAlerteSuivi(java.lang.String value) {
	    takeStoredValueForKey(value, SUJET_ALERTE_SUIVI_KEY);
	}
	
	public java.lang.String supportEnvFinanceur() {
		return (java.lang.String) storedValueForKey(SUPPORT_ENV_FINANCEUR_KEY);
	}

	public void setSupportEnvFinanceur(java.lang.String value) {
	    takeStoredValueForKey(value, SUPPORT_ENV_FINANCEUR_KEY);
	}
	
	public java.lang.Boolean ttc() {
		return (java.lang.Boolean) storedValueForKey(TTC_KEY);
	}

	public void setTtc(java.lang.Boolean value) {
	    takeStoredValueForKey(value, TTC_KEY);
	}
	
	public java.lang.String typeFinancementDemande() {
		return (java.lang.String) storedValueForKey(TYPE_FINANCEMENT_DEMANDE_KEY);
	}

	public void setTypeFinancementDemande(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_FINANCEMENT_DEMANDE_KEY);
	}
	
	public java.lang.String typeSubventionObtenue() {
		return (java.lang.String) storedValueForKey(TYPE_SUBVENTION_OBTENUE_KEY);
	}

	public void setTypeSubventionObtenue(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_SUBVENTION_OBTENUE_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
	    return (org.cocktail.cocowork.server.metier.convention.Contrat) storedValueForKey(CONTRAT_KEY);
	}

	public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
	    }
	}
	
	public org.cocktail.cocowork.server.metier.convention.Contrat contratLie() {
	    return (org.cocktail.cocowork.server.metier.convention.Contrat) storedValueForKey(CONTRAT_LIE_KEY);
	}

	public void setContratLieRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contratLie();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_LIE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_LIE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) storedValueForKey(DOMAINE_SCIENTIFIQUE_KEY);
	}

	public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOMAINE_SCIENTIFIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_SCIENTIFIQUE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOProgramme programme() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProgramme) storedValueForKey(PROGRAMME_KEY);
	}

	public void setProgrammeRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgramme value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOProgramme oldValue = programme();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROGRAMME_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, PROGRAMME_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOProgramme sousProgramme() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProgramme) storedValueForKey(SOUS_PROGRAMME_KEY);
	}

	public void setSousProgrammeRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgramme value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOProgramme oldValue = sousProgramme();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SOUS_PROGRAMME_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, SOUS_PROGRAMME_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> toRepartAapAvisRapporteurs() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_REPART_AAP_AVIS_RAPPORTEURS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> toRepartAapAvisRapporteurs(EOQualifier qualifier) {
		return toRepartAapAvisRapporteurs(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> toRepartAapAvisRapporteurs(EOQualifier qualifier, boolean fetch) {
	    return toRepartAapAvisRapporteurs(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> toRepartAapAvisRapporteurs(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur.AAP_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toRepartAapAvisRapporteurs();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToRepartAapAvisRapporteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_AAP_AVIS_RAPPORTEURS_KEY);
	}

	public void removeFromToRepartAapAvisRapporteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_AAP_AVIS_RAPPORTEURS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur createToRepartAapAvisRapporteursRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartAapAvisRapporteur");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_AAP_AVIS_RAPPORTEURS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur) eo;
	}

	public void deleteToRepartAapAvisRapporteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_AAP_AVIS_RAPPORTEURS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToRepartAapAvisRapporteursRelationships() {
		java.util.Enumeration objects = toRepartAapAvisRapporteurs().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToRepartAapAvisRapporteursRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartAapAvisRapporteur) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> typesFinancementAap() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TYPES_FINANCEMENT_AAP_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> typesFinancementAap(EOQualifier qualifier) {
		return typesFinancementAap(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> typesFinancementAap(EOQualifier qualifier, boolean fetch) {
	    return typesFinancementAap(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> typesFinancementAap(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap.AAP_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = typesFinancementAap();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToTypesFinancementAapRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TYPES_FINANCEMENT_AAP_KEY);
	}

	public void removeFromTypesFinancementAapRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TYPES_FINANCEMENT_AAP_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap createTypesFinancementAapRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeFinancementAap");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TYPES_FINANCEMENT_AAP_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap) eo;
	}

	public void deleteTypesFinancementAapRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TYPES_FINANCEMENT_AAP_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllTypesFinancementAapRelationships() {
		java.util.Enumeration objects = typesFinancementAap().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteTypesFinancementAapRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap) objects.nextElement());
	    }
	}

	public static EOAap createAap(EOEditingContext editingContext, java.lang.Boolean aapSupprime, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, java.lang.Boolean ttc) {
		EOAap eo = (EOAap) EOUtilities.createAndInsertInstance(editingContext, _EOAap.ENTITY_NAME);    
		eo.setAapSupprime(aapSupprime);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setTtc(ttc);
		return eo;
	}

	public EOAap localInstanceIn(EOEditingContext editingContext) {
		return (EOAap) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOAap creerInstance(EOEditingContext editingContext) {
		return (EOAap) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOAap.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOAap> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOAap> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOAap>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOAap fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOAap fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOAap> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOAap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOAap fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOAap> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOAap fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOAap eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOAap ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOAap fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOAap> fetchAllAaps(EOEditingContext editingContext) {
		return _EOAap.fetchAllAaps(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOAap> fetchAllAaps(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOAap.fetchAaps(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOAap> fetchAaps(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOAap> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOAap>(_EOAap.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOAap> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOAap fetchAap(EOEditingContext editingContext, String keyName, Object value) {
		return _EOAap.fetchAap(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOAap fetchAap(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOAap> eoObjects = _EOAap.fetchAaps(editingContext, qualifier, null);
	    EOAap eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one Aap that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
