/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODoctorant.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODoctorant extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Doctorant";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idDoctorant";

	public static final er.extensions.eof.ERXKey<java.lang.Integer> ANNEE_INSC_THESE = new er.extensions.eof.ERXKey<java.lang.Integer>("anneeInscThese");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> ANNEE_M2 = new er.extensions.eof.ERXKey<java.lang.Integer>("anneeM2");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_COMMISSION = new er.extensions.eof.ERXKey<java.lang.String>("avisCommission");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ANNULATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateAnnulation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_COMMISSION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateCommission");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> ETUD_NUMERO = new er.extensions.eof.ERXKey<java.lang.Integer>("etudNumero");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> HIST_NUMERO = new er.extensions.eof.ERXKey<java.lang.Integer>("histNumero");
	
	public static final String ANNEE_INSC_THESE_KEY = ANNEE_INSC_THESE.key();
	public static final String ANNEE_M2_KEY = ANNEE_M2.key();
	public static final String AVIS_COMMISSION_KEY = AVIS_COMMISSION.key();
	public static final String DATE_ANNULATION_KEY = DATE_ANNULATION.key();
	public static final String DATE_COMMISSION_KEY = DATE_COMMISSION.key();
	public static final String ETUD_NUMERO_KEY = ETUD_NUMERO.key();
	public static final String HIST_NUMERO_KEY = HIST_NUMERO.key();

	// Attributs non visibles
	public static final String C_PAYS_DIPLOME_ORIGINE_KEY = "cPaysDiplomeOrigine";
	public static final String C_RNE_DIPLOME_ORIGINE_KEY = "cRneDiplomeOrigine";
	public static final String ID_DOCTORANT_KEY = "idDoctorant";
	public static final String ID_ORIGINE_KEY = "idOrigine";
	public static final String ID_STATUT_KEY = "idStatut";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	//Colonnes dans la base de donnees
	public static final String ANNEE_INSC_THESE_COLKEY = "ANNEE_INSC_THESE";
	public static final String ANNEE_M2_COLKEY = "ANNEE_M2";
	public static final String AVIS_COMMISSION_COLKEY = "AVIS_COMMISSION";
	public static final String DATE_ANNULATION_COLKEY = "DATE_ANNULATION";
	public static final String DATE_COMMISSION_COLKEY = "DATE_COMMISSION";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";

	public static final String C_PAYS_DIPLOME_ORIGINE_COLKEY = "C_PAYS_DIPLOME_ORIGINE";
	public static final String C_RNE_DIPLOME_ORIGINE_COLKEY = "C_RNE_DIPLOME_ORIGINE";
	public static final String ID_DOCTORANT_COLKEY = "ID_DOCTORANT";
	public static final String ID_ORIGINE_COLKEY = "ID_ORIGINE";
	public static final String ID_STATUT_COLKEY = "ID_STATUT";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine> TO_DOCTORANT_ORIGINE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine>("toDoctorantOrigine");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut> TO_DOCTORANT_STATUT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut>("toDoctorantStatut");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> TO_DOCTORANT_THESES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese>("toDoctorantTheses");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_ETABLISSEMENT_DIPLOME_ORIGINE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toEtablissementDiplomeOrigine");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiantFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_DIPLOME = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysDiplome");
	
	public static final String TO_DOCTORANT_ORIGINE_KEY = TO_DOCTORANT_ORIGINE.key();
	public static final String TO_DOCTORANT_STATUT_KEY = TO_DOCTORANT_STATUT.key();
	public static final String TO_DOCTORANT_THESES_KEY = TO_DOCTORANT_THESES.key();
	public static final String TO_ETABLISSEMENT_DIPLOME_ORIGINE_KEY = TO_ETABLISSEMENT_DIPLOME_ORIGINE.key();
	public static final String TO_ETUDIANT_FWKPERS_KEY = TO_ETUDIANT_FWKPERS.key();
	public static final String TO_INDIVIDU_FWKPERS_KEY = TO_INDIVIDU_FWKPERS.key();
	public static final String TO_PAYS_DIPLOME_KEY = TO_PAYS_DIPLOME.key();

	// Accessors methods
	public java.lang.Integer anneeInscThese() {
		return (java.lang.Integer) storedValueForKey(ANNEE_INSC_THESE_KEY);
	}

	public void setAnneeInscThese(java.lang.Integer value) {
	    takeStoredValueForKey(value, ANNEE_INSC_THESE_KEY);
	}
	
	public java.lang.Integer anneeM2() {
		return (java.lang.Integer) storedValueForKey(ANNEE_M2_KEY);
	}

	public void setAnneeM2(java.lang.Integer value) {
	    takeStoredValueForKey(value, ANNEE_M2_KEY);
	}
	
	public java.lang.String avisCommission() {
		return (java.lang.String) storedValueForKey(AVIS_COMMISSION_KEY);
	}

	public void setAvisCommission(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_COMMISSION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateAnnulation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ANNULATION_KEY);
	}

	public void setDateAnnulation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ANNULATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateCommission() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_COMMISSION_KEY);
	}

	public void setDateCommission(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_COMMISSION_KEY);
	}
	
	public java.lang.Integer etudNumero() {
		return (java.lang.Integer) storedValueForKey(ETUD_NUMERO_KEY);
	}

	public void setEtudNumero(java.lang.Integer value) {
	    takeStoredValueForKey(value, ETUD_NUMERO_KEY);
	}
	
	public java.lang.Integer histNumero() {
		return (java.lang.Integer) storedValueForKey(HIST_NUMERO_KEY);
	}

	public void setHistNumero(java.lang.Integer value) {
	    takeStoredValueForKey(value, HIST_NUMERO_KEY);
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine toDoctorantOrigine() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine) storedValueForKey(TO_DOCTORANT_ORIGINE_KEY);
	}

	public void setToDoctorantOrigineRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine oldValue = toDoctorantOrigine();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_ORIGINE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_ORIGINE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut toDoctorantStatut() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut) storedValueForKey(TO_DOCTORANT_STATUT_KEY);
	}

	public void setToDoctorantStatutRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut oldValue = toDoctorantStatut();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_STATUT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_STATUT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORne toEtablissementDiplomeOrigine() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORne) storedValueForKey(TO_ETABLISSEMENT_DIPLOME_ORIGINE_KEY);
	}

	public void setToEtablissementDiplomeOrigineRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toEtablissementDiplomeOrigine();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETABLISSEMENT_DIPLOME_ORIGINE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETABLISSEMENT_DIPLOME_ORIGINE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiantFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant) storedValueForKey(TO_ETUDIANT_FWKPERS_KEY);
	}

	public void setToEtudiantFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiantFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETUDIANT_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETUDIANT_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(TO_INDIVIDU_FWKPERS_KEY);
	}

	public void setToIndividuFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysDiplome() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays) storedValueForKey(TO_PAYS_DIPLOME_KEY);
	}

	public void setToPaysDiplomeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysDiplome();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_DIPLOME_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_DIPLOME_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> toDoctorantTheses() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_DOCTORANT_THESES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> toDoctorantTheses(EOQualifier qualifier) {
		return toDoctorantTheses(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> toDoctorantTheses(EOQualifier qualifier, boolean fetch) {
	    return toDoctorantTheses(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> toDoctorantTheses(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese.TO_DOCTORANT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toDoctorantTheses();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToDoctorantThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_DOCTORANT_THESES_KEY);
	}

	public void removeFromToDoctorantThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DOCTORANT_THESES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese createToDoctorantThesesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DoctorantThese");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DOCTORANT_THESES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese) eo;
	}

	public void deleteToDoctorantThesesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DOCTORANT_THESES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToDoctorantThesesRelationships() {
		java.util.Enumeration objects = toDoctorantTheses().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToDoctorantThesesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese) objects.nextElement());
	    }
	}

	public static EODoctorant createDoctorant(EOEditingContext editingContext, java.lang.Integer anneeInscThese, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuFwkpers) {
		EODoctorant eo = (EODoctorant) EOUtilities.createAndInsertInstance(editingContext, _EODoctorant.ENTITY_NAME);    
		eo.setAnneeInscThese(anneeInscThese);
		eo.setToIndividuFwkpersRelationship(toIndividuFwkpers);
		return eo;
	}

	public EODoctorant localInstanceIn(EOEditingContext editingContext) {
		return (EODoctorant) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODoctorant creerInstance(EOEditingContext editingContext) {
		return (EODoctorant) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODoctorant.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODoctorant> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorant>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODoctorant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODoctorant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODoctorant> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODoctorant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODoctorant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODoctorant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODoctorant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODoctorant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODoctorant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODoctorant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODoctorant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODoctorant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAllDoctorants(EOEditingContext editingContext) {
		return _EODoctorant.fetchAllDoctorants(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorant> fetchAllDoctorants(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODoctorant.fetchDoctorants(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODoctorant> fetchDoctorants(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODoctorant> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorant>(_EODoctorant.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODoctorant> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODoctorant fetchDoctorant(EOEditingContext editingContext, String keyName, Object value) {
		return _EODoctorant.fetchDoctorant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODoctorant fetchDoctorant(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODoctorant> eoObjects = _EODoctorant.fetchDoctorants(editingContext, qualifier, null);
	    EODoctorant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one Doctorant that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
