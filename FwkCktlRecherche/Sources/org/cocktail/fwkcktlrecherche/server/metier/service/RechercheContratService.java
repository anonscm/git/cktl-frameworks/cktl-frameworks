package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.qualifiers.ERXFalseQualifier;

/**
 * Classe de service permettant de recherche des
 * contrats qu'ils soient de recherche ou pas
 */
public class RechercheContratService {
	
	private EOEditingContext edc;
	
	/**
	 * @param edc l'editingContext utlisé pour la recherche
	 */
	public RechercheContratService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * 
	 * @param criteres de recherche
	 * @return les projets correspodants aux critères
	 */
	public List<RechercheContratResultat> rechercher(RechercheContratCriteres criteres) {
		
		List<Contrat> contrats = getContratsForQualifier(getQualifierForCriteres(criteres));
		List<EOContratRecherche> contratsRecherche = getContratsRechercheForContrats(contrats);
		
		List<Contrat> contratsNonRecherche;
		
		if (criteres.getAutresQueRecherche()) {
			contratsNonRecherche = getContratsNonRecherche(contrats, contratsRecherche);
		} else {
			contratsNonRecherche = new ArrayList<Contrat>();
		}
		
		return getResultats(contratsNonRecherche, contratsRecherche);
		
	}
	
	/**
	 * @param resultat le résultat à transformer
	 * @return le contrat de recherche correspondant
	 */
	public EOContratRecherche getSelectedContratRecherchePourResultat(RechercheContratResultat resultat) {

		if (resultat.isTypeRecherche()) {
			return 
				EOContratRecherche.fetchContratRecherche(
					edc, 
					EOContratRecherche.CONTRAT
						.dot(Contrat.CON_ORDRE)
							.eq(resultat.getId())
				);
		}
		return null;
	}
	
	/**
	 * @param resultat le résultat à transformer
	 * @return le contrat correspondant
	 */
	public Contrat getSelectedContratPourResultat(RechercheContratResultat resultat) {

		if (!resultat.isTypeRecherche()) {
			return 
				Contrat.fetch(
					edc, 
					Contrat.CON_ORDRE
						.eq(resultat.getId())
				);
		}
		return null;
	}
	
	/**
	 * @param resultats à traiter
	 * @return la liste des contrats
	 */
	public List<EOContratRecherche> getContratsPourResultats(List<RechercheContratResultat> resultats) {
		List<Integer> ids = new ArrayList<Integer>();
		for (RechercheContratResultat resultat : resultats) {
			ids.add(resultat.getId());
		}
		
		EOQualifier qualifier = EOContratRecherche.CONTRAT.dot(Contrat.CON_ORDRE).in(new NSArray<Integer>(ids));
		
		ERXSortOrderings sortOrderings = 
			EOContratRecherche.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL)
				.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).desc()
				.then(EOContratRecherche.CONTRAT.dot(Contrat.CON_INDEX).desc()
			);
		
		return EOContratRecherche.fetchAll(edc, qualifier, sortOrderings);
	}
	
	
	protected List<RechercheContratResultat> getResultats(List<Contrat> contrats, List<EOContratRecherche> contratsRecherche) {
		List<RechercheContratResultat> resultats = new ArrayList<RechercheContratService.RechercheContratResultat>();
		
		for (EOContratRecherche c : contratsRecherche) {
			resultats.add(getResultatsPourContrat(c));
		}
		
		for (Contrat c : contrats) {
			resultats.add(getResultatsPourContrat(c));
		}

		return resultats;
	}
	
	protected RechercheContratResultat getResultatsPourContrat(Contrat c) {
		RechercheContratResultat r = new RechercheContratResultat();
		r.setId(c.conOrdre());
		r.setAnnee(c.exerciceCocktail().exeExercice());
		r.setNumero(c.conIndex());
		r.setObjet(c.conObjet());
		r.setTypeRecherche(false);
		r.setTypeContrat(c.typeContrat().tyconLibelle());
		r.setSuiviAdministratif(null);
		
		return r;
	}
	
	protected RechercheContratResultat getResultatsPourContrat(EOContratRecherche c) {
		RechercheContratResultat r = getResultatsPourContrat(c.contrat());
		r.setTypeRecherche(true);
		r.setSuiviAdministratif(c.getSuiviAdministratifLibelle());
		
		return r;
	}
	
	protected List<Contrat> getContratsForQualifier(EOQualifier qualifier) {
		return ERXArrayUtilities.arrayWithoutDuplicates(
				Contrat.fetchAll(edc, qualifier, null)
			   );
	}
	
	protected List<EOContratRecherche> getContratsRechercheForContrats(List<Contrat> contrats) {
		EOQualifier qualifier = EOContratRecherche.CONTRAT.in(new NSArray<Contrat>(contrats));
		return ERXArrayUtilities.arrayWithoutDuplicates(
				EOContratRecherche.fetchAll(edc, qualifier, null)
			   );		
	}
	
	@SuppressWarnings("unchecked")
	protected List<Contrat> getContratsNonRecherche(List<Contrat> contrats, List<EOContratRecherche> contratsRecherche) {
		
		List<Contrat> _contratsRecherche = new ArrayList<Contrat>();
		
		for (EOContratRecherche contratRecherche : contratsRecherche) {
			_contratsRecherche.add(contratRecherche.contrat());
		}
		
		return ListUtils.removeAll(contrats, _contratsRecherche);
	}
	
	protected EOQualifier getQualifierForCriteres(RechercheContratCriteres criteres) {
		return 
			ERXQ.and(
				getQualifierForAcronyme(criteres.getToken()),
				getQualifierForAnnee(criteres.getAnnee()),
				getQualifierForNumero(criteres.getNumero()),
				getQualifierForStructurePorteuse(criteres.getStructurePorteuse()),
				getQualifierForResponsableScientifique(criteres.getResponsableScientifique()),
				getQualifierForPartenaire(criteres.getPartenaire()),
				getQualifierForSupprime(),
				getQualifierForTypeConvention(),
				getQualifierForPerimetres(criteres.getPerimetresQualifier())
			);
	}
	
	/**
	 * @param annee l'annee du contrat
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForAnnee(Integer annee) {
		
		if (annee == null) { return null; }
		
		return Contrat.EXERCICE_COCKTAIL
			.dot(EOExerciceCocktail.EXE_EXERCICE_KEY)
				.eq(annee);
	
	}
	
	/**
	 * @param numero le numero du contrat
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForNumero(Integer numero) {
		
		if (numero == null) { return null; }
		
		return Contrat.CON_INDEX
				.eq(numero);

	}
	
	/**
	 * @param structure la structurePorteuse du contrat
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForStructurePorteuse(EOStructure structure) {
		
		if (structure == null) { return null; }
		
		
		EOQualifier sousQualifier = 
				Contrat.CONTRAT_PARTENAIRES
					.dot(ContratPartenaire.PERS_ID)
						.eq(structure.persId());
		
		return new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, Contrat.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
	
	}
	/**
	 * @param responsable le responsable scientifique du contrat
	 * @return le qualifier correspondant aux contrats ayant ce responsable scientifique
	 */
	protected EOQualifier getQualifierForResponsableScientifique(EOIndividu responsable) {
		
		if (responsable == null) { return null; }
		
		EORelationship groupeRelationship = 
				ERXEOAccessUtilities
					.entityNamed(null, Contrat.ENTITY_NAME)
						.relationshipNamed(Contrat.GROUPE_PARTENAIRE_KEY);
		
		String sourceAttributeName = groupeRelationship.sourceAttributes().lastObject().name();
		String destinationAttributeName = groupeRelationship.destinationAttributes().lastObject().name();
		
		
		FactoryAssociation factoryAssociation = FactoryAssociation.shared();
		EOAssociation responsableScientifiqueAssociation = factoryAssociation.responsableScientifiqueAssociation(edc);
		
		EOQualifier sousQualifier =
				EOStructure.TO_REPART_STRUCTURES_ELTS
					.dot(EORepartStructure.TO_REPART_ASSOCIATIONS)
					.dot(EORepartAssociation.TO_ASSOCIATION)
						.eq(responsableScientifiqueAssociation)
				.and(
					EOStructure.TO_REPART_STRUCTURES_ELTS
						.dot(EORepartStructure.TO_REPART_ASSOCIATIONS)
						.dot(EORepartAssociation.PERS_ID)
							.eq(responsable.persId()));
		
		return new ERXQualifierInSubquery(sousQualifier, EOStructure.ENTITY_NAME, sourceAttributeName, destinationAttributeName);

	}
	
	/**
	 * @param structure le partenaire du contrat
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForPartenaire(IPersonne partenaire) {
		
		if (partenaire == null) { return null; }
		
		
		EOQualifier sousQualifier = 
				Contrat.CONTRAT_PARTENAIRES
					.dot(ContratPartenaire.PERS_ID)
						.eq(partenaire.persId());
		
		return new ERXQualifierInSubquery(sousQualifier, Contrat.ENTITY_NAME, Contrat.CON_ORDRE_KEY, Contrat.CON_ORDRE_KEY);
	
	}
	
	/**
	 * @param token le token
	 * @return le qualifier correspodant aux projet qui contiennent le token dans leur acronyme
	 */
	protected EOQualifier getQualifierForAcronyme(String token) {
		
		if (token == null) { return null; }
		
		return Contrat.CON_OBJET_COURT
					.contains(token);
		
	}
	
	protected EOQualifier getQualifierForSupprime() {
		return Contrat.QUALIFIER_NON_SUPPR;
	}
	
	protected EOQualifier getQualifierForTypeConvention() {
		return Contrat.TYPE_CLASSIFICATION_CONTRAT.dot(TypeClassificationContrat.TCC_CODE).eq(TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_CONV);
	}
	
	protected EOQualifier getQualifierForPerimetres(EOQualifier perimetresQualifier) {
		if (perimetresQualifier == null) {
			return new ERXFalseQualifier();
		}
		return perimetresQualifier;
	}
	
	/**
	 * Bean permettant de stocker les résultats de la recherche 
	 */
	public static class RechercheContratResultat {
		
		private Integer id;
		private Integer annee;
		private Integer numero;
		private String objet;
		private Boolean typeRecherche;
		private String typeContrat;
		private String suiviAdministratif;
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getAnnee() {
			return annee;
		}
		public void setAnnee(Integer annee) {
			this.annee = annee;
		}
		public Integer getNumero() {
			return numero;
		}
		public void setNumero(Integer numero) {
			this.numero = numero;
		}
		public String getObjet() {
			return objet;
		}
		public void setObjet(String objet) {
			this.objet = objet;
		}
		/**
		 * @return "Oui" ou "Non"
		 */
		public String isTypeRechercheDisplayString() {
			if (isTypeRecherche()) {
				return "Oui";
			} else {
				return "Non";
			}
		}
		public Boolean isTypeRecherche() {
			return typeRecherche;
		}
		public void setTypeRecherche(Boolean typeRecherche) {
			this.typeRecherche = typeRecherche;
		}
		public String getTypeContrat() {
			return typeContrat;
		}
		public void setTypeContrat(String typeContrat) {
			this.typeContrat = typeContrat;
		}
		public String getSuiviAdministratif() {
			return suiviAdministratif;
		}
		public void setSuiviAdministratif(String suiviAdministratif) {
			this.suiviAdministratif = suiviAdministratif;
		}
		
	}
	
	
	/**
	 * 
	 * @author Julien Lafoucade
	 * 
	 * Bean utilitaire pour les critères de recherche de contrat
	 *
	 */
	public static class RechercheContratCriteres {
		
		private String token;
		private Integer annee;
		private Integer numero;
		private EOStructure structurePorteuse;
		private EOIndividu responsableScientifique;
		private EOStructure partenaire;
		private Boolean autresQueRecherche = false;
		private EOQualifier perimetresQualifier;
		
		public String getToken() {
			return token;
		}
		
		public void setToken(String token) {
			this.token = token;
		}
		
		public Integer getAnnee() {
			return annee;
		}
		
		public void setAnnee(Integer annee) {
			this.annee = annee;
		}
		
		public Integer getNumero() {
			return numero;
		}
		
		public void setNumero(Integer numero) {
			this.numero = numero;
		}
		
		public EOStructure getStructurePorteuse() {
			return structurePorteuse;
		}
		
		public void setStructurePorteuse(EOStructure structurePorteuse) {
			this.structurePorteuse = structurePorteuse;
		}
		
		public EOIndividu getResponsableScientifique() {
			return responsableScientifique;
		}
		
		public void setResponsableScientifique(EOIndividu responsableScientifique) {
			this.responsableScientifique = responsableScientifique;
		}

		public EOStructure getPartenaire() {
			return partenaire;
		}

		public void setPartenaire(EOStructure partenaire) {
			this.partenaire = partenaire;
		}

		public Boolean getAutresQueRecherche() {
			return autresQueRecherche;
		}

		public void setAutresQueRecherche(Boolean autresQueRecherche) {
			this.autresQueRecherche = autresQueRecherche;
		}

		public EOQualifier getPerimetresQualifier() {
			return perimetresQualifier;
		}

		public void setPerimetresQualifier(EOQualifier perimetresQualifier) {
			this.perimetresQualifier = perimetresQualifier;
		}
		
	}
}
