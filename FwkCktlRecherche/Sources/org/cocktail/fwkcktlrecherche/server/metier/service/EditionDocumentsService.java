package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.util.Calendar;
import java.util.Date;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de services sur les éditions de documents de Physalis
 */
public class EditionDocumentsService {
	
	public static final Integer NOMBRE_ANNEES_THESE = 3;
	private static final Integer MOIS_FIN_ANNEE_UNIVERSITAIRE = Calendar.AUGUST;
	private static final Integer MOIS_DEBUT_ANNEE_UNIVERSITAIRE = Calendar.SEPTEMBER;

	/**
	 * @param these la these
	 * @return l'année d'inscription en these
	 */
	public static Integer anneeDInscriptionThese(EODoctorantThese these) {
		NSTimestamp dateDebutThese = these.toContrat().dateDebut();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(debutAnneeUniversitaire(dateDebutThese));
		Integer annneeDebutThese = calendar.get(Calendar.YEAR);
		
		calendar.setTime(debutAnneeUniversitaire(new Date()));
		Integer annneeDebutAnneeUnivCourante = calendar.get(Calendar.YEAR);

		return annneeDebutAnneeUnivCourante - annneeDebutThese + 1;
	}
	
	/**
	 * Retourne la date de rentrée universitaire d'une date donnée
	 * 
	 * @param date date
	 * @return date de rentree universitaire
	 */
	public static Date debutAnneeUniversitaire(Date date) {

		final Integer un = 1;

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);

		Integer mois = calendar.get(Calendar.MONTH);
		Integer annee = calendar.get(Calendar.YEAR);

		if (mois < MOIS_DEBUT_ANNEE_UNIVERSITAIRE) {
			annee = annee - 1;
		}

		calendar.set(annee, MOIS_DEBUT_ANNEE_UNIVERSITAIRE, un, 0, 0, 0);
		return calendar.getTime();
	}

	/**
	 * Retourne la date de fin d'année universitaire d'une date donnée
	 * 
	 * @param date date
	 * @return date de fin d'année universitaire
	 */
	public static Date finAnneeUniversitaire(Date date) {

		final Integer trenteEtUn = 31;

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);

		Integer mois = calendar.get(Calendar.MONTH);
		Integer annee = calendar.get(Calendar.YEAR);

		if (mois >= MOIS_FIN_ANNEE_UNIVERSITAIRE) {
			annee = annee + 1;
		}

		calendar.set(annee, MOIS_FIN_ANNEE_UNIVERSITAIRE, trenteEtUn, 0, 0, 0);
		return calendar.getTime();

	}
	
	/**
	 * @param these these
	 * @return retourne true si l'année d'inscription en cours est inférieure ou égale au nombre d'années max en these
	 */
	public static Boolean anneeInscriptionOk(EODoctorantThese these) {
		return anneeDInscriptionThese(these) <= NOMBRE_ANNEES_THESE;
	}
	
	/**
	 * @param these these
	 * @return retourne true si l'année d'inscription en cours est supérieure au nombre d'années max en these
	 */
	public static Boolean anneeInscriptionKo(EODoctorantThese these) {
		return anneeDInscriptionThese(these) > NOMBRE_ANNEES_THESE;
	}
	
}
