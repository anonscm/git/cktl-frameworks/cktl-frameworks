package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantDocument;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOAvenant;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;

import com.ibm.icu.util.Calendar;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe de services sur les theses
 */
public class DoctorantTheseService {

	private EOEditingContext edc;
	private Integer persId;
	private CotutelleService cotutelleService;
	
	/**
	 * Crée une nouvelle instance de DirecteurTheseService
	 * @param edc editingContext
	 * @param persId persId de l'utilisateur
	 * @return DirecteurTheseService
	 */
	public static DoctorantTheseService creerNouvelleInstance(EOEditingContext edc, Integer persId) {
		return new DoctorantTheseService(edc, persId);
	}
	
	/**
	 * Contructeur
	 * @param edc editingContext
	 * @param persId persId de l'utilisateur
	 */
	public DoctorantTheseService(EOEditingContext edc, Integer persId) {
		this.edc = edc;
		this.persId = persId;
	}
	
	private EOEditingContext edc() {
		return edc;
	}
	
	private Integer getPersId() {
		return persId;
	}
	
	private CotutelleService getCotutelleService() {
		if (cotutelleService == null) {
			cotutelleService = CotutelleService.creerNouvelleInstance(edc(), getPersId());
		}
		return cotutelleService;
	}
	
	/**
	 * Supprime une these et tout ce qu'elle contient
	 * @param these la these à supprimer
	 * @return true si succes false sinon
	 */
	public boolean supprimerTouteLaThese(EODoctorantThese these) {
		if (these == null) {
			return false;
		}
		
		return supprimerEncadrants(these)  && supprimerDevenirDuDocteur(these) && supprimerDocuments(these) && supprimerMembresJury(these, true) 
				&& supprimerMembresJury(these, false) && supprimerFinanceursEtFinancement(these) && supprimerCotutelle(these) 
				&& supprimerLaboratoires(these) && /*supprimerEcoleDoctorale(these) && /*supprimerAvenants(these) &&*/ supprimerThese(these);
	}
	
	public boolean supprimerThese(EODoctorantThese these) {
		if (these == null) {
			return false;
		}
		
		try {
//			suppressionContratsPartenaire(these);
//			suppressionStructures(these);
			
			//suppression tranches
//			NSArray<Tranche> tranches = these.toContrat().tranches();
//			int nbTranches = tranches.size();
//			
//			for (int i = 0; i < nbTranches; i++) {
//				edc().deleteObject(tranches.get(i));
//			}
			
			//suppression avenants
//			these.toContrat().avenantZero().setDomaineScientifiqueRelationship(null);
//			AvenantDomaineScientifique ads = AvenantDomaineScientifique.fetch(edc(), AvenantDomaineScientifique.AVENANT.dot(Avenant.CONTRAT).eq(these.toContrat()));
//			edc().deleteObject(ads);
			
//			NSArray<Avenant> avenants = these.toContrat().avenants();
//			for (Avenant avenant : avenants) {
//				FactoryAvenant fa = new FactoryAvenant(edc(), false);
//				fa.supprimerAvenant(avenant);
////				edc().deleteObject(avenants.get(i)); // dans le cas ou on supprime totalement
//			}

			//suppression projet contrat
//			ProjetContrat pc = ProjetContrat.fetch(edc(), ProjetContrat.CONTRAT.eq(these.toContrat()));
//			edc().deleteObject(pc);
			
			//suppresion contrat
//			edc().deleteObject(these.toContrat()); // dans le cas ou on supprime totalement
//			these.toContrat().setConSuppr(Contrat.CON_SUPPR_OUI);
			
			FactoryContrat fc = new FactoryContrat(edc(), false);
			fc.supprimerAvenantsAdmin(these.toContrat(), false);
			fc.supprimerContratAdmin(these.toContrat());
			
			these.setToContratRelationship(null);
			these.setToTheseDomaineRelationship(null);
			these.setToTheseMentionRelationship(null);
			these.setToDoctorantRelationship(null);
			these.setToProjetScientifiqueRelationship(null);			
			edc().deleteObject(these);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean suppressionStructures(EODoctorantThese these) {
		try {
			EOQualifier qualifierRepartTypeGroup = EORepartTypeGroupe.C_STRUCTURE.eq(these.toContrat().groupePartenaire().cStructure());
			NSArray<EORepartTypeGroupe> tgrps = EORepartTypeGroupe.fetchAll(edc(), qualifierRepartTypeGroup);
			int nb = tgrps.size();
			for (int i = 0; i < nb; i++) {
				edc().deleteObject(tgrps.get(i));
			}
			
			EOQualifier qualifierRepartAsso = EORepartAssociation.TO_STRUCTURE.dot(EOStructure.C_STRUCTURE).eq(these.toContrat().groupePartenaire().cStructure());
			NSArray<EORepartAssociation> rass = EORepartAssociation.fetchAll(edc(), qualifierRepartAsso);
			nb = rass.size();
			for (int i = 0; i < nb; i++) {
				edc().deleteObject(rass.get(i));
			}
			
			EOQualifier qualifierRepartStruct1 = EORepartStructure.PERS_ID.eq(these.toContrat().groupePartenaire().persId());
			NSArray<EORepartStructure> rstruct1 = EORepartStructure.fetchAll(edc(), qualifierRepartStruct1);
			nb = rstruct1.size();
			for (int i = 0; i < nb; i++) {
				edc().deleteObject(rstruct1.get(i));
			}
			
			EOQualifier qualifierRepartStruct2 = EORepartStructure.C_STRUCTURE.eq(these.toContrat().groupePartenaire().cStructure());
			NSArray<EORepartStructure> rstruct2 = EORepartStructure.fetchAll(edc(), qualifierRepartStruct2);
			nb = rstruct2.size();
			for (int i = 0; i < nb; i++) {
				edc().deleteObject(rstruct2.get(i));
			}
			
			//archive la structure partenaire de l'acte
			//these.toContrat().groupePartenaire().setTemValide(EOStructure.TEM_VALIDE_NON);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean suppressionContratsPartenaire(EODoctorantThese these) {
		
		FactoryContratPartenaire fcp = createFactoryContratPartenaire();
		
		NSArray<ContratPartenaire> contratsPartenaire = these.toContrat().contratPartenaires();
		int nb = contratsPartenaire.size();
		
		for (int i = 0; i < nb; i++) {
			try {
	            fcp.supprimerContratPartenaire(contratsPartenaire.get(i), getPersId());
            } catch (Exception e) {
            	e.printStackTrace();
	            return false;
            }
		}

		return true;
	}
	
	public boolean supprimerEcoleDoctorale(EODoctorantThese these) {
		NSArray<EORepartAssociation> raEcolesDoctorales = these.toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().ecoleDoctoraleAssociation(edc()));
		
		for (EORepartAssociation raEcoleDoctorale : raEcolesDoctorales) {
			EOStructure ecoleDoctorale = (EOStructure) raEcoleDoctorale.toPersonneElt();
			
			if (raEcoleDoctorale != null) {
				FactoryContratPartenaire fcp = createFactoryContratPartenaire();
				ContratPartenaire cpToDelete = these.toContrat().partenaireForPersId(raEcoleDoctorale.persId());
				try {
					fcp.supprimerLeRole(cpToDelete, factoryAssociation().ecoleDoctoraleAssociation(edc()));
					fcp.supprimerContratPartenaire(cpToDelete, getPersId());
					these.toDoctorant().toIndividuFwkpers().supprimerUnRole(edc(), factoryAssociation().doctorantAssociation(edc()), ecoleDoctorale, getPersId(), raEcoleDoctorale.rasDOuverture());
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Supprime l'affectation d'une thèse à un laboratoire ainsi que le repart structure correspondant
	 * @param these la these
	 * @param raLaboratoire le repart association du laboratoire
	 * @param isModeDebug boolean indiquant si l'on affiche les logs du mode debug
	 * @return true si succes, false sinon
	 * @throws Exception exception
	 */
	public boolean supprimerLaboratoire(EODoctorantThese these, EORepartAssociation raLaboratoire, boolean isModeDebug) throws Exception {
		
		if (these == null || raLaboratoire == null) {
			return false;
		}
		
		EOStructure laboratoire = (EOStructure) raLaboratoire.toStructure();
		
		FactoryContratPartenaire fcp = new FactoryContratPartenaire(edc(), isModeDebug);
		ContratPartenaire cpToDelete = these.toContrat().partenaireForPersId(laboratoire.persId());
		
		fcp.supprimerLeRole(cpToDelete, factoryAssociation().laboratoireTheseAssociation(edc()));
		fcp.supprimerContratPartenaire(cpToDelete, getPersId());
		these.toDoctorant().toIndividuFwkpers()
				.supprimerUnRole(edc(), factoryAssociation().doctorantAssociation(edc()), laboratoire, getPersId(), raLaboratoire.rasDOuverture());
		
		NSArray<EORepartStructure> repartStructuresLaboratoireDoctorant = laboratoire.toRepartStructuresElts(
				ERXQ.equals(EORepartStructure.TO_PERSONNE_ELT_KEY, these.toDoctorant().toIndividuFwkpers())
			);
		EORepartStructure repartStructureLaboratoireDoctorant = ERXArrayUtilities.firstObject(repartStructuresLaboratoireDoctorant);
		if (repartStructureLaboratoireDoctorant != null) {
			laboratoire.deleteToRepartStructuresEltsRelationship(repartStructureLaboratoireDoctorant);
		}
		
		NSArray<EORepartStructure> repartStructuresContratLaboratoire = these.toContrat().groupePartenaire().toRepartStructuresElts(
				ERXQ.equals(EORepartStructure.TO_PERSONNE_ELT_KEY, laboratoire)
			);
		EORepartStructure repartStructureContratLaboratoire = ERXArrayUtilities.firstObject(repartStructuresContratLaboratoire);
		if (repartStructureContratLaboratoire != null) {
			these.toContrat().groupePartenaire().deleteToRepartStructuresEltsRelationship(repartStructureContratLaboratoire);
		}
		
		return true;
	}
	
	private boolean supprimerLaboratoires(EODoctorantThese these) {
		NSArray<EORepartAssociation> laboratoires = repartAssociationLaboratoires(these);
		for (EORepartAssociation raLaboratoire : laboratoires) {
			try {
				supprimerLaboratoire(these, raLaboratoire, false);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public boolean supprimerEncadrants(EODoctorantThese these) {
		NSArray<EODirecteurThese> encadrants = these.toDirecteursThese(null, true);
		boolean result = true;
		
		for (EODirecteurThese encadrant : encadrants) {
			if (!supprimerEncadrant(these, encadrant, false)) {
				result = false;
			}
		}
		
		return result;
	}
	
	public boolean supprimerEncadrant(EODoctorantThese these, EODirecteurThese encadrant, boolean isModeDebug) {
		ContratPartenaire ancienneCotutelle = null;
		FactoryContratPartenaire fcp = new FactoryContratPartenaire(edc(), isModeDebug);
		ContratPartenaire cpToDelete = encadrant.toContratPartenaireCw();
		
		try {
			if (encadrant.isDirecteurCotutelle() && verificationEtablissementDirecteurAvecCeluiDeCotutelle(these, encadrant)) {
				
				/*Suppression des présences dans l'établissement*/
				int nbPresenceEtablissement = 0;
				for (EORepartAssociation repartAssociationPresenceEtablissement : getCotutelleService().listePresenceEtablissementCotutelle(these)) {
					nbPresenceEtablissement++;
					ContratUtilities cu = ContratUtilities.creerNouvelleInstance(edc());
					cu.supprimerRolePourPartenaireDansContrat(factoryAssociation().etablissementAccueil(edc()), repartAssociationPresenceEtablissement.toPersonne(), 
							these.toContrat(), getPersId(), repartAssociationPresenceEtablissement.rasDOuverture());
				}
				
	//			if (nbPresenceEtablissement > 1) {
	//				session.addSimpleInfoMessage("Remarque", "Les présences dans les établissements de cotutelle ont été automatiquement supprimées dans l'onglet cotutelle");
	//			} else if (nbPresenceEtablissement == 1) {
	//				session.addSimpleInfoMessage("Remarque", "La présence dans l'établissement de cotutelle a été automatiquement supprimée dans l'onglet cotutelle");
	//			}
				
				/*Suppression de l'établissement de cotutelle*/
				FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
				cp_exist.setContrat(these.toContrat());
				cp_exist.setPartenaire(encadrant.toStructureEtabFwkPers());
				ancienneCotutelle = (ContratPartenaire) cp_exist.find().lastObject();
				// un partenaire peut avoir plusieur roles
				NSArray<EOAssociation> tousLesRolesCotutelle = ancienneCotutelle.rolesPartenaireDansContrat(ancienneCotutelle.partenaire(), these.toContrat());
				// suppression du role etablissement Cotutelle
				fcp.supprimerLeRole(ancienneCotutelle, factoryAssociation().etablissementCotutelleAssociation(edc()));
	//			session().addSimpleInfoMessage("Remarque", "L'établissement de cotutelle a été automatiquement supprimé dans l'onglet cotutelle.");
				// si pas d'autres roles que etablissement cotutelle alors
				// on supprime le partenauire
				if (tousLesRolesCotutelle.count() == 1) {
					fcp.supprimerContratPartenaire(ancienneCotutelle, getPersId());
				}
			}
			
			// un partenaire peut avoir plusieur roles
			NSArray<EOAssociation> tousLesRoles = cpToDelete.rolesPartenaireDansContrat(cpToDelete.partenaire(), these.toContrat());
			// on supprime le role de directeur ou encadrant
			fcp.supprimerLeRole(cpToDelete, encadrant.roleDirecteur());
			// si pas d'autres roles que directeur ou encadrant alors on
			// supprime le partenauire
			if (tousLesRoles.count() == 1) {
				fcp.supprimerContratPartenaire(cpToDelete, getPersId());
			}
//			encadrant.setToContratPartenaireCwRelationship(null); //TODO: supprimer
//			encadrant.setToDoctorantTheseRelationship(null);
//			encadrant.setToStructureLaboFwkpersRelationship(null);
//			encadrant.setToRneFwkpersRelationship(null);
			these.removeFromToDirecteursTheseRelationship(encadrant);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean verificationEtablissementDirecteurAvecCeluiDeCotutelle(EODoctorantThese these, EODirecteurThese encadrant) {
		NSArray<EORepartAssociation> repartAssociationCotutelle = getCotutelleService().listeEtablissementsCotutelle(these);
			
		if (encadrant == null 
				|| encadrant.toStructureEtabFwkPers() == null 
				|| encadrant.toStructureEtabFwkPers().getAdressePrincipale() == null 
				|| encadrant.toStructureEtabFwkPers().getAdressePrincipale().toPays() == null 
				|| repartAssociationCotutelle.size() == 0) {
			return false;
		}
		
		for (EORepartAssociation raEtablissement : repartAssociationCotutelle) {
			if (encadrant.toStructureEtabFwkPers().equals(raEtablissement.toPersonne())) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean supprimerCotutelle(EODoctorantThese these) {
		
		EOQualifier qualifierPresenceEtablissementCotutelle = ERXQ.equals(EOAssociation.ASS_CODE_KEY, factoryAssociation().etablissementAccueil(edc()).assCode());
		NSArray<EOAssociation> lesAssociationsPresence = EOAssociation.fetchAll(edc(), qualifierPresenceEtablissementCotutelle);
		for (EORepartAssociation association :  these.toContrat().repartAssociationPartenairesForAssociations(lesAssociationsPresence)) {
			ContratUtilities cu = ContratUtilities.creerNouvelleInstance(edc());
			try {
				cu.supprimerRolePourPartenaireDansContrat(factoryAssociation().etablissementAccueil(edc()), association.toPersonne(),
						these.toContrat(), getPersId(), association.rasDOuverture());
	        } catch (Exception e) {
	        	e.printStackTrace();
	        	return false;
	        }
		}
		
		EOQualifier qualEtabCotutelle = ERXQ.equals(EOAssociation.ASS_CODE_KEY, factoryAssociation().etablissementCotutelleAssociation(edc()).assCode());
		NSArray<EOAssociation> lesAssociationsCotuelle = EOAssociation.fetchAll(edc(), qualEtabCotutelle);
		for (EORepartAssociation association : these.toContrat().repartAssociationPartenairesForAssociations(lesAssociationsCotuelle)) {
			
			EOQualifier qualCP = ERXQ.and(ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, association.persId()),
					ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, these.toContrat()));
			ContratPartenaire cp = ContratPartenaire.fetch(edc(), qualCP);
			
			FactoryContratPartenaire fcp = createFactoryContratPartenaire();
			try {
				fcp.supprimerLeRole(cp, factoryAssociation().etablissementCotutelleAssociation(edc()));
				fcp.supprimerContratPartenaire(cp, getPersId());
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	public boolean supprimerFinanceur(EODoctorantThese these, EODoctorantFinanceur financeur) {
		// il existe forcement le role de financeur si + d'un role on ne supprime pas le partenaire
		try {
			supprimerLesRepartAssociationDuFinanceur(financeur);
			supprimerLesContratsPartenaireDuFinanceur(these, financeur);
//			financeur.setToContratPartenaireRelationship(null); // TODO: A tester 18/11/2014
			supprimerLeFinancementDuDoctorant(financeur);
			suppressionObjetFinanceur(financeur);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	protected void suppressionObjetFinanceur(EODoctorantFinanceur financeur) {
		edc().deleteObject(financeur);
	}

	protected void supprimerLeFinancementDuDoctorant(EODoctorantFinanceur financeur) {
		financeur.setToDoctorantFinancementRelationship(null);
	}

	protected void supprimerLesRepartAssociationDuFinanceur(EODoctorantFinanceur financeur) {
		// on supprime le repart_association lié au financeur
		edc().deleteObject(financeur.toRepartAssociation());
		financeur.setToRepartAssociationRelationship(null); // TODO: A tester 18/11/2014
	}

	protected void supprimerLesContratsPartenaireDuFinanceur(EODoctorantThese these, EODoctorantFinanceur financeur) throws Exception {
		ContratPartenaire cp = financeur.toContratPartenaire();
		FactoryContratPartenaire fcp = createFactoryContratPartenaire();
		NSArray<EOAssociation> tousLesRoles = cp.rolesPartenaireDansContrat(cp.partenaire(), these.toContrat());
		if (tousLesRoles == null || tousLesRoles.count() == 1) {
			fcp.supprimerContratPartenaire(cp, getPersId());
		}
	}

	protected FactoryContratPartenaire createFactoryContratPartenaire() {
		return new FactoryContratPartenaire(edc(), false);
	}
	
	public boolean supprimerFinancement(EODoctorantThese these, EODoctorantFinancement financement) {
		FactoryContratPartenaire fc = createFactoryContratPartenaire();
		ContratPartenaire ancienCp = financement.toContratPartenaire();
		try {
			if (ancienCp != null) {
				NSArray<EOAssociation> tousLesRoles = ancienCp.rolesPartenaireDansContrat(ancienCp.partenaire(), these.toContrat());
				// on supprime le repart_association lié au financement
				edc().deleteObject(financement.toRepartAssociation());
				// il existe forcement le role d'employeur si + d'un role on
				// ne supprimer pas le partenaire
				if (tousLesRoles == null || tousLesRoles.count() == 1) {
					fc.supprimerContratPartenaire(ancienCp, getPersId());
				}
			}
			
			financement.setToAvenantRelationship(null);
			financement.setToTypeFinancementRelationship(null);
			edc().deleteObject(financement);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean supprimerFinanceursEtFinancement(EODoctorantThese these) {
		boolean result = true;
		for (EOAvenant avenant : these.toContrat().avenants()) {
			EOQualifier qual = ERXQ.is(EODoctorantFinancement.TO_AVENANT_KEY, avenant);
			NSArray<EODoctorantFinancement> financements = EODoctorantFinancement.fetchDoctorantFinancements(edc(), qual, null);
			
			if (!financements.isEmpty()) {
				EOQualifier qualFinanceurs = ERXQ.equals(EODoctorantFinanceur.TO_DOCTORANT_FINANCEMENT_KEY, financements.get(0));
				NSArray<EODoctorantFinanceur> financeurs = EODoctorantFinanceur.fetchDoctorantFinanceurs(edc(), qualFinanceurs, null);
				
				for (EODoctorantFinanceur financeur : financeurs) {
					if (!supprimerFinanceur(these, financeur)) {
						result = false;
					}
				}

				for (EODoctorantFinancement financement : financements) {
					if (!supprimerFinancement(these, financement)) {
						result = false;
					}
				}
			} 
		}
		
		return result;
	}
	
	public boolean supprimerMembreJury(EODoctorantThese these, EOMembreJuryThese membreJury, boolean isTypeRapporteur) {
		
		ContratPartenaire cp = null;
		try {
			FactoryContratPartenaire fcp = createFactoryContratPartenaire();
			FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
			cp_exist.setContrat(these.toContrat());
			if (isTypeRapporteur) {
				cp_exist.setPartenaire(membreJury.repartAssociationRapporteur().toPersonneElt());
			} else {
				cp_exist.setPartenaire(membreJury.repartAssociationJury().toPersonneElt());
			}
			
			cp = (ContratPartenaire) cp_exist.find().lastObject();

			NSArray<EOAssociation> tousLesRoles = cp.rolesPartenaireDansContrat(cp.partenaire(), these.toContrat());
			// il existe forcement un des roles de membre donc si + d'un role on
			// ne supprimer pas le partenaire
			if (isTypeRapporteur) {
				fcp.supprimerLeRole(cp, membreJury.repartAssociationRapporteur().toAssociation());
			} else {
				fcp.supprimerLeRole(cp, membreJury.repartAssociationJury().toAssociation());
			}
			
			if (tousLesRoles.count() == 1) {
				fcp.supprimerContratPartenaire(cp, getPersId());
				membreJury.setToDoctorantTheseRelationship(null);
				membreJury.setToCorpsRelationship(null);
				membreJury.setToContratPartenaireRelationship(null);
				edc().deleteObject(membreJury);
			} else {
				// si il a plus d'un role, on cherche si il a un role de
				// rapporteur ou un des roles de membres de jury
				// si c'est le cas, on ne supprime pas le membre (un rapporteur
				// peut être membre du jury)
				if ((!isTypeRapporteur && !tousLesRoles.contains(factoryAssociation().rapporteurTheseAssociation(edc())))
						|| isTypeRapporteur && EOQualifier.filteredArrayWithQualifier(
								tousLesRoles,
								ERXQ.in(EOAssociation.ASS_CODE_KEY,
										(NSArray) factoryAssociation().rolesJuryAssociation(edc()).getFils(edc()).valueForKey(EOAssociation.ASS_CODE_KEY))).isEmpty()) {
					membreJury.setToDoctorantTheseRelationship(null);
					membreJury.setToCorpsRelationship(null);
					membreJury.setToContratPartenaireRelationship(null);
					edc().deleteObject(membreJury);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean supprimerMembresJury(EODoctorantThese these, boolean isTypeRapporteur) { 		
		NSArray<EOAssociation> associations = null;
		if (isTypeRapporteur) {
			associations = new NSArray<EOAssociation>(factoryAssociation().rapporteurTheseAssociation(edc()));
		} else {
			associations = factoryAssociation().associationsFilleDe(factoryAssociation().rolesJuryAssociation(edc()), edc());
		}
		
		EOQualifier qualMjt = ERXQ.and(ERXQ.equals(EOMembreJuryThese.TO_DOCTORANT_THESE_KEY, these),
					ERXQ.in(EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, these.toContrat().partenairesForAssociations(associations)));
		NSArray<EOMembreJuryThese> membresJury = EOMembreJuryThese.fetchMembreJuryTheses(edc(), qualMjt, null);
		
		for (EOMembreJuryThese membreJury : membresJury) {
			supprimerMembreJury(these, membreJury, isTypeRapporteur);
			
		}
		
		return true;
	}
	
	/**
	 * Supprime les documents enregistrés dans la GED
	 * @param these la these a supprimer
	 * @return true si les documents ont été supprimés
	 */
	public boolean supprimerDocuments(EODoctorantThese these) {
		NSArray<AvenantDocument> documents = these.toContrat().avenantZero().avenantDocuments();
		int nbDoc = documents.size();
		PersonneApplicationUser persAppUser = new PersonneApplicationUser(edc(), getPersId());
		
		try {
			if (documents != null) {
				FactoryAvenant fa = new FactoryAvenant(edc(), false);
				for (int i = 0; i < nbDoc; i++) {
					fa.supprimerDocument(edc(), documents.get(i), persAppUser, null);
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Supprime le devenir du docteur
	 * @param these la these a supprimer
	 * @return true si les devenirs ont été supprimés
	 */
	public boolean supprimerDevenirDuDocteur(EODoctorantThese these) {
		try {
			EOQualifier qualifier = EORepartSituationDocteur.TO_DOCTORANT_THESE.eq(these);
			NSArray<EORepartSituationDocteur> situations = EORepartSituationDocteur.fetchAll(edc(), qualifier);
			int nbSituations = situations.size();
			
			for (int i = 0; i < nbSituations; i++) {
				edc().deleteObject(situations.get(i));
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/* Modification date lors d'un abandon */
	
	public NSArray<EORepartAssociation> repartAssociationLaboratoires(EODoctorantThese these) {
		
		NSArray<EORepartAssociation> repartAssociationsLabosContrat = 
				these.toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().laboratoireTheseAssociation(edc())); // repart asso de la these avec role labo 
		NSArray<IPersonne> personnes = new NSMutableArray<IPersonne>(); // structure labo
		for (EORepartAssociation repartAssociation : repartAssociationsLabosContrat) {
			personnes.add(repartAssociation.toPersonne());
		}
		
		EOQualifier qualifier = 
				EORepartAssociation.TO_STRUCTURE.in(ERXArrayUtilities.arrayBySelectingInstancesOfClass(personnes, EOStructure.class))
				.and(EORepartAssociation.TO_ASSOCIATION.eq(factoryAssociation().doctorantAssociation(edc())))
				.and(EORepartAssociation.TO_INDIVIDUS_ASSOCIES.containsObject(these.toDoctorant().toIndividuFwkpers())
				.and(EORepartAssociation.RAS_D_OUVERTURE.between(these.dateDeb(), these.dateFin(), true))
				.and(EORepartAssociation.RAS_D_FERMETURE.between(these.dateDeb(), these.dateFin(), true)));
		
		return EORepartAssociation.fetchAll(edc(), qualifier);
	}
	
	/**
	 * Modifie les dates de fin lors d'un abandon de these
	 * @param these la these abandonnée
	 * @param dateFin la date d'abandon
	 * @return true si succes, false sinon
	 * @throws Exception 
	 */
	public boolean modifierDatesLorsDUnAbandon(EODoctorantThese these, NSTimestamp dateFin) throws Exception {
		
		if (these == null || dateFin == null) {
			return false;
		}
		
		these.setDateSoutenance(null);
		
		return modifierDateFinAffectationLaboratoires(these, dateFin) && modifierDateFinAffectationEncadrants(these, dateFin) 
				&& modifierDateFinAffectationCotutelle(these, dateFin) && modifierDateFinFinancementEtFinanceurs(these, dateFin)
				&& modifierDateFinAvenants(these, dateFin) && supprimerMembresJury(these, true) && supprimerMembresJury(these, false)
				&& supprimerDevenirDuDocteur(these);
	}
	
	/**
	 * Modifie la date de fin des appartenances aux laboratoires
	 * @param these la these
	 * @param dateFin la nouvelle date de fin
	 * @return true si succes, false sinon
	 * @throws Exception 
	 */
	private boolean modifierDateFinAffectationLaboratoires(EODoctorantThese these, NSTimestamp dateFin) throws Exception {
		NSArray<EORepartAssociation> laboratoires = repartAssociationLaboratoires(these);
		for (EORepartAssociation raLaboratoire : laboratoires) {
		
			//modification du repart association entre le doctorant et le laboratoire
			if (rasDFermetureApres(raLaboratoire, dateFin)) {
				raLaboratoire.setRasDFermeture(dateFin);
			} 
			
			//modification du repart association entre le laboratoire et le contrat
			NSArray<EORepartAssociation> repartAssociationsLabosContrat = these.toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().laboratoireTheseAssociation(edc()));
			for (EORepartAssociation raLaboContrat : repartAssociationsLabosContrat) {
				if (raLaboratoire.toStructure().equals(raLaboContrat.toPersonne())) {
					if (rasDFermetureApres(raLaboContrat, dateFin)) {
						raLaboContrat.setRasDFermeture(dateFin);
					} 
				}
			}
			
			// suppression de l'affectation si le debut de l'affectation est apres la nouvelle date de fin de these
			if (rasDOuvertureApres(raLaboratoire, dateFin)) {
				supprimerLaboratoire(these, raLaboratoire, false);
			}
		}
		
		return true;
	}
	
	private boolean modifierDateFinAffectationEncadrants(EODoctorantThese these, NSTimestamp dateFin) {
		NSArray<EODirecteurThese> encadrants = these.toDirecteursThese(null, true);
		
		for (EODirecteurThese encadrant : encadrants) {
			/*if (rasDOuvertureApres(encadrant.repartAssociationDirecteur(), dateFin)) {
				supprimerEncadrant(these, encadrant, false);
			} else*/ if (rasDFermetureApres(encadrant.repartAssociationDirecteur(), dateFin)) {
				encadrant.repartAssociationDirecteur().setRasDFermeture(dateFin);
			}
		}
		
		return true;
	}
	
	private boolean modifierDateFinAffectationCotutelle(EODoctorantThese these, NSTimestamp dateFin) {
		NSArray<EORepartAssociation> listeEtablissementsCotutelle = listeEtablissementsCotutelle(these);
		
		for (EORepartAssociation etablissementCotutelle : listeEtablissementsCotutelle) {
			if (rasDFermetureApres(etablissementCotutelle, dateFin)) {
				etablissementCotutelle.setRasDFermeture(dateFin);
			}
			
			if (rasDOuvertureApres(etablissementCotutelle, dateFin)) {
				getCotutelleService().supprimerCotutelle(these, etablissementCotutelle, false);
			}
		}
		
		NSArray<EORepartAssociation> listePresenceEtablissementsCotutelle = listePresenceEtablissementCotutelle(these);
		
		for (EORepartAssociation presenceEtablissementCotutelle : listePresenceEtablissementsCotutelle) {
			if (rasDFermetureApres(presenceEtablissementCotutelle, dateFin)) {
				presenceEtablissementCotutelle.setRasDFermeture(dateFin);
			}
			
			if (rasDOuvertureApres(presenceEtablissementCotutelle, dateFin)) {
				getCotutelleService().supprimerPresenceEtablissement(these, presenceEtablissementCotutelle);
			}
		}
		
		return true;
	}
	
	private boolean modifierDateFinFinancementEtFinanceurs(EODoctorantThese these, NSTimestamp dateFin) {
		
		for (EOAvenant avenant : avenantsNonSupprimes(these)) {
			EOQualifier qual = ERXQ.is(EODoctorantFinancement.TO_AVENANT_KEY, avenant);
			NSArray<EODoctorantFinancement> financements = EODoctorantFinancement.fetchDoctorantFinancements(edc(), qual, null);
			
			for (EODoctorantFinancement financement : financements) {
				
				EOQualifier qualFinanceurs = ERXQ.equals(EODoctorantFinanceur.TO_DOCTORANT_FINANCEMENT_KEY, financement);
				NSArray<EODoctorantFinanceur> financeurs = EODoctorantFinanceur.fetchDoctorantFinanceurs(edc(), qualFinanceurs, null);
				
				for (EODoctorantFinanceur financeur : financeurs) {
					if (rasDFermetureApres(financeur.toRepartAssociation(), dateFin)) {
						financeur.toRepartAssociation().setRasDFermeture(dateFin);
					}
					
					if (rasDOuvertureApres(financeur.toRepartAssociation(), dateFin)) {
						supprimerFinanceur(these, financeur);
					}
				}
			
				if (rasDFermetureApres(financement.toRepartAssociation(), dateFin)) {
					financement.toRepartAssociation().setRasDFermeture(dateFin);
				}
				
				if (financement.dateFinFinancement() != null && financement.dateFinFinancement().after(dateFin)) {
					financement.setDateFinFinancement(dateFin);
				}
				
				//un seul financement donc pas d'ouverture apres la nouvelle date de fin
			}
		}
		
		return true;
	}
	
	public boolean supprimerAvenant(Avenant avenant, boolean isModeDebug) {
		FactoryAvenant fa = new FactoryAvenant(edc(), isModeDebug);
		fa.supprimerAvenant(avenant);
		return true;
	}
	
	public boolean modifierDateFinAvenants(EODoctorantThese these, NSTimestamp dateFin) {
		for (Avenant avenant : avenantsNonSupprimes(these)) {
			if (avenant.avtDateFin() != null && avenant.avtDateFin().after(dateFin)) {
				avenant.setAvtDateFin(dateFin);
			}
			
			if (avenant.avtDateDeb() != null && avenant.avtDateDeb().after(dateFin)) {
				supprimerAvenant(avenant, false);
			}
		}
		
		return true;
	}
	
	/**
	 * Modifie les dates de fin lors de la soutenance réelle de these
	 * @param these la these
	 * @param dateFin la date de soutenance
	 * @return true si succes, false sinon
	 * @throws Exception 
	 */
	public boolean modifierDatesLorsDUneSoutenance(EODoctorantThese these, NSTimestamp dateFin) throws Exception {
		
		if (these == null || dateFin == null) {
			return false;
		}
		
		these.setDateFinAnticipee(null);
		these.setMotifFinAnticipee(null);
		
		dateFin = mettreAMinuit(dateFin);
		
		return modifierDateFinAffectationLaboratoiresLorsDUneSoutenance(these, dateFin) && modifierDateFinAffectationEncadrantsLorsDUneSoutenance(these, dateFin) 
				&& modifierDateFinAffectationCotutelleLorsDUneSoutenance(these, dateFin) && modifierDateFinAvenantsLorsDUneSoutenance(these, dateFin);
	}

	protected NSTimestamp mettreAMinuit(NSTimestamp date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		Integer jour = calendar.get(Calendar.DAY_OF_MONTH);
		Integer mois = calendar.get(Calendar.MONTH);
		Integer annee = calendar.get(Calendar.YEAR);
		
		calendar.set(annee, mois, jour, 0, 0, 0);
		return new NSTimestamp(calendar.getTime());
	}
	
	/**
	 * Modifie la date de fin des appartenances aux laboratoires lors de la mise à jours de la date de soutenance
	 * @param these la these
	 * @param dateFin la nouvelle date de fin
	 * @return true si succes, false sinon
	 * @throws Exception 
	 */
	private boolean modifierDateFinAffectationLaboratoiresLorsDUneSoutenance(EODoctorantThese these, NSTimestamp dateFin) throws Exception { // TODO:
		NSArray<EORepartAssociation> laboratoires = repartAssociationLaboratoires(these);
		for (EORepartAssociation raLaboratoire : laboratoires) {
			
			//modification du repart association entre le doctorant et le laboratoire
			if (these.dateFin().equals(raLaboratoire.rasDFermeture())) {
				raLaboratoire.setRasDFermeture(dateFin);
			} 
			
			//modification du repart association entre le laboratoire et le contrat
			NSArray<EORepartAssociation> repartAssociationsLabosContrat = these.toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().laboratoireTheseAssociation(edc()));
			for (EORepartAssociation raLaboContrat : repartAssociationsLabosContrat) {
				if (raLaboratoire.toStructure().equals(raLaboContrat.toPersonne())) {
					if (these.dateFin().equals(raLaboContrat.rasDFermeture())) {
						raLaboContrat.setRasDFermeture(dateFin);
					} 
				}
			}
			
			// suppression de l'affectation si le debut de l'affectation est apres la nouvelle date de fin de these
			if (rasDOuvertureApres(raLaboratoire, dateFin)) {
				supprimerLaboratoire(these, raLaboratoire, false);
			}
		}
		
		return true;
	}
	
	private boolean modifierDateFinAffectationEncadrantsLorsDUneSoutenance(EODoctorantThese these, NSTimestamp dateFin) {
		NSArray<EODirecteurThese> encadrants = these.toDirecteursThese(null, true);
		
		for (EODirecteurThese encadrant : encadrants) {
			/*if (rasDOuvertureApres(encadrant.repartAssociationDirecteur(), dateFin)) {
				supprimerEncadrant(these, encadrant, false);
			} else*/ 
			
			if (these.dateFin().equals(encadrant.repartAssociationDirecteur().rasDFermeture())) {
				encadrant.repartAssociationDirecteur().setRasDFermeture(dateFin);
			}
		}
		
		return true;
	}
	
	private boolean modifierDateFinAffectationCotutelleLorsDUneSoutenance(EODoctorantThese these, NSTimestamp dateFin) {
		NSArray<EORepartAssociation> listeEtablissementsCotutelle = listeEtablissementsCotutelle(these);
		
		for (EORepartAssociation etablissementCotutelle : listeEtablissementsCotutelle) {
			if (these.dateFin().equals(etablissementCotutelle.rasDFermeture())) {
				etablissementCotutelle.setRasDFermeture(dateFin);
			}
			
			if (rasDOuvertureApres(etablissementCotutelle, dateFin)) {
				getCotutelleService().supprimerCotutelle(these, etablissementCotutelle, false);
			}
		}
		
		return true;
	}
	
	public boolean modifierDateFinAvenantsLorsDUneSoutenance(EODoctorantThese these, NSTimestamp dateFin) {
		for (Avenant avenant : avenantsNonSupprimes(these)) {
			if (these.dateFin().equals(avenant.avtDateFin())) {
				avenant.setAvtDateFin(dateFin);
			}
			
			if (avenant.avtDateDeb() != null && avenant.avtDateDeb().after(dateFin)) {
				supprimerAvenant(avenant, false);
			}
		}
		
		return true;
	}
	
	private NSArray<Avenant> avenantsNonSupprimes(EODoctorantThese these) {
		EOQualifier qualifier = Avenant.AVT_SUPPR.eq(Avenant.AVT_SUPPR_NON);
		return these.toContrat().avenants(qualifier);
	}

	private boolean rasDFermetureApres(EORepartAssociation repartAssociation, NSTimestamp dateFin) {
		return repartAssociation != null 
				&& repartAssociation.rasDFermeture() != null 
				&& repartAssociation.rasDFermeture().after(dateFin);
	}

	private boolean rasDOuvertureApres(EORepartAssociation repartAssociation, NSTimestamp dateFin) {
		return repartAssociation != null 
				&& repartAssociation.rasDOuverture() != null 
				&& repartAssociation.rasDOuverture().after(dateFin);
	}

	private NSArray<EORepartAssociation> listePresenceEtablissementCotutelle(EODoctorantThese these) {
		return getCotutelleService().listePresenceEtablissementCotutelle(these);
	}
	
	private NSArray<EORepartAssociation> listeEtablissementsCotutelle(EODoctorantThese these) {
		return getCotutelleService().listeEtablissementsCotutelle(these);
	}
	
	private FactoryAssociation factoryAssociation() {
	    return FactoryAssociation.shared();
    }
}
