package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class LaboratoireService {
	
	public void ajouterLaboratoire(EODoctorantThese these, EOStructure laboratoire, NSTimestamp dateDebut, NSTimestamp dateFin, String commentaire, Integer userPersId, boolean isModeDebug) {
		EOEditingContext edc = these.editingContext();
		creerPartenaireLabo(edc, these, laboratoire, dateDebut, dateFin, commentaire, isModeDebug);
		NSArray<EORepartAssociation> ra_exist = repartAssociationsIndividuRoleDoctorantLaboratoire(these.toDoctorant().toIndividuFwkpers(), laboratoire);
		
		if (ra_exist != null) {
			if (ra_exist.isEmpty()) {
				these.toDoctorant().toIndividuFwkpers().definitUnRole(edc, FactoryAssociation.shared().doctorantAssociation(edc), 
						laboratoire, userPersId, dateDebut, dateFin, commentaire, null, null);
			} else if (ra_exist.count() == 1) {
				ra_exist.get(0).setRasDOuverture(dateDebut);
				ra_exist.get(0).setRasDFermeture(dateFin);
				ra_exist.get(0).setRasCommentaire(commentaire);
			}
		}
	}
	
	private ContratPartenaire creerPartenaireLabo(EOEditingContext edc, EODoctorantThese these, EOStructure laboratoire, NSTimestamp dateDebut, NSTimestamp dateFin, String commentaire, boolean isModeDebug) {
		ContratPartenaire cp = null;
		try {
			FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc);
			cp_exist.setContrat(these.toContrat());
			cp_exist.setPartenaire(laboratoire);
			cp = (ContratPartenaire) cp_exist.find().lastObject();

			// si le contrat partenaire n'existe pas, on le crée
			// sinon on ajoute le role au partenaire
			FactoryContratPartenaire fc = new FactoryContratPartenaire(edc, isModeDebug);
			if (cp == null) {
				cp = fc.creerContratPartenaire(these.toContrat(), laboratoire, new NSArray<EOAssociation>(FactoryAssociation.shared().laboratoireTheseAssociation(edc)),
						dateDebut, dateFin, Boolean.FALSE, null, commentaire);
			} else {
				//TODO: ajouter commentaire (voire dates)
				fc.ajouterLeRole(cp, FactoryAssociation.shared().laboratoireTheseAssociation(edc));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cp;
	}
	
	protected NSArray<EORepartAssociation> repartAssociationsIndividuRoleDoctorantLaboratoire(EOIndividu individu, EOStructure laboratoire) {
		
		if (individu == null || laboratoire == null) {
			return null;
		}
		
		EOEditingContext edc = laboratoire.editingContext();
		
		EOQualifier qual = ERXQ.and(
				EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().doctorantAssociation(edc)),
				EORepartAssociation.TO_STRUCTURE.eq(laboratoire),
				EORepartAssociation.TO_INDIVIDUS_ASSOCIES.eq(individu)
				);
				
		return EORepartAssociation.fetchAll(edc, qual);
	}
	
}
