package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe permettant d'utiliser des methodes sur les droits
 */
public final class DroitsService {

	private static final String TYPE_GROUPE_LABORATOIRE = "LA";
	private static final String TYPE_GROUPE_ECOLE_DOCTORALE = "ED";
	
	private EOEditingContext edc;
	private FactoryAssociation factoryAssociation;
	private Integer persId;
	
	/**
	 * Crée une nouvelle instance de DirecteurTheseService
	 * @param edc editingContext
	 * @param factoryAssociation FactoryAssociation
	 * @param persId persId de la personne connectée
	 * @return DirecteurTheseService
	 */
	public static DroitsService creerNouvelleInstance(EOEditingContext edc, FactoryAssociation factoryAssociation, Integer persId) {
		return new DroitsService(edc, factoryAssociation, persId);
	}
	
	private DroitsService(EOEditingContext edc, FactoryAssociation factoryAssociation, Integer persId) {
		this.edc = edc;
		this.factoryAssociation = factoryAssociation;
		this.persId = persId;
	}
	
	private EOEditingContext edc() {
		return edc;
	}
	
	private FactoryAssociation factoryAssociation() {
		return factoryAssociation;
	}
	
	private Integer persId() {
		return persId;
	}
	
	/**
	 * @return les ecoles doctorales dont fait partie la personne connectée 
	 */
	public NSArray<EOStructure> listeEcolesDoctoralesResponsableOuSecretaire() {
		return listeStructureResponsableOuSecretaire(factoryAssociation().ecoleDoctoraleAssociation(edc()));
	}
	
	/**
	 * @return les laboratoires dont fait partie la personne connectée 
	 */
	public NSArray<EOStructure> listeLaboratoiresResponsableOuSecretaire() {
		return listeStructureResponsableOuSecretaire(factoryAssociation().laboratoireTheseAssociation(edc()));
	}
	
	/**
	 * @param association type d'association
	 * @return les structures dont la personne connectée est directeur ou secretaire
	 */
	private NSArray<EOStructure> listeStructureResponsableOuSecretaire(EOAssociation association) {
		
		NSArray<EORepartAssociation> repartAssociation = 
				EORepartAssociation.fetchAll(edc(), EORepartAssociation.TO_ASSOCIATION.eq(association));
		
		NSMutableArray<EOStructure> structures = new NSMutableArray<EOStructure>();
		
		for (EORepartAssociation ra : repartAssociation) {
			EOIndividu responsable = ra.toStructuresAssociees().get(0).toResponsable();
			NSArray<EOSecretariat> secretaires = ra.toStructuresAssociees().get(0).toSecretariats();
			
			if (responsable != null && responsable.persId().equals(persId())) { 
				if (!structures.contains((EOStructure) ra.toPersonne())) {
					structures.add((EOStructure) ra.toPersonne());
				}
			}
			
			for (EOSecretariat secretaire : secretaires) {
				if (secretaire.toIndividu().persId().equals(persId())) {
					if (!structures.contains((EOStructure) ra.toPersonne())) {
						structures.add((EOStructure) ra.toPersonne());
					}
				}
			}
		}
		
		return structures;	
	}
	
	/**
	 * @return les ecoles doctorales dont la personne connectée est membre
	 */
	public NSArray<EOStructure> listeEcolesDoctoralesMembre() {
		return listeStructuresMembre(TYPE_GROUPE_ECOLE_DOCTORALE);
	}
	
	/**
	 * @return les laboratoires dont la personne connectée est membre
	 */
	public NSArray<EOStructure> listeLaboratoiresMembre() {
		return listeStructuresMembre(TYPE_GROUPE_LABORATOIRE);
	}

	/**
	 * @param typeGroupe le type de structure dont la personne est membre
	 * @return la liste des structures dont la personne est membre
	 */
	private NSArray<EOStructure> listeStructuresMembre(String typeGroupe) {
	    NSArray<EOStructure> structuresMembre = EOStructure.fetchAll(edc(), EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE_KEY).eq(typeGroupe));
		NSMutableArray<EOStructure> structures = new NSMutableArray<EOStructure>();
		for (EOStructure structure : structuresMembre) {
			NSArray<EORepartStructure> rs = structure.toRepartStructuresElts();
			for (EORepartStructure unRs : rs) {
				if (persId().equals(unRs.toPersonneElt().persId())) {
					if (!structures.contains(structure)) {
						structures.add(structure);
					}
				}
			}
		}
		return structures;
    }
	
	/**
	 * @return liste des ecoles doctorales visibles sans droit
	 */
	public NSArray<EOStructure> listeEcolesDoctoralesVisiblesSansDroit() {
		NSMutableArray<EOStructure> ecolesDoctorales = new NSMutableArray<EOStructure>();
		if (listeEcolesDoctoralesResponsableOuSecretaire() != null) {
			ecolesDoctorales.addAll(listeEcolesDoctoralesResponsableOuSecretaire());
		}
		if (listeEcolesDoctoralesMembre() != null) {
			ecolesDoctorales.addAll(listeEcolesDoctoralesMembre());
		}
		ecolesDoctorales = (NSMutableArray<EOStructure>) ERXArrayUtilities.arrayWithoutDuplicates(ecolesDoctorales);
		return ecolesDoctorales.immutableClone();
	}
	
	/**
	 * @return liste des laboratoires visibles sans droit
	 */
	public NSArray<EOStructure> listeLaboratoiresVisiblesSansDroit() {
		NSMutableArray<EOStructure> laboratoires = new NSMutableArray<EOStructure>();
		if (listeLaboratoiresResponsableOuSecretaire() != null) {
			laboratoires.addAll(listeLaboratoiresResponsableOuSecretaire());
		}
		if (listeLaboratoiresMembre() != null) {
			laboratoires.addAll(listeLaboratoiresMembre());
		}
		laboratoires = (NSMutableArray<EOStructure>) ERXArrayUtilities.arrayWithoutDuplicates(laboratoires);
		return laboratoires.immutableClone();
	}
	
}
