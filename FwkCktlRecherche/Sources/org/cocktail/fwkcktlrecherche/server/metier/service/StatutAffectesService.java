package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.finder.CarriereFinder;
import org.cocktail.fwkcktlgrh.common.metier.services.ContratsService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlrecherche.server.metier.EOStatutTypeContrat;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class StatutAffectesService {

	private CarriereFinder carriereFinder;
	private EOEditingContext edc;
	
	private StatutAffectesService(EOEditingContext edc) {
		this.edc = edc;
		this.carriereFinder = new CarriereFinder();
		this.carriereFinder.setEdc(edc);
	}
	
	public EOEditingContext edc() {
		return edc;
	}

	public static StatutAffectesService creerNouvelleInstance(EOEditingContext edc) {
		return new StatutAffectesService(edc);
	}
	
	public NSDictionary<EOIndividu, EOAssociation> statutsPourIndividusAffectesEtDate(NSArray<EOIndividu> individus, NSTimestamp date) {

		NSMutableDictionary<EOIndividu, EOAssociation> statutsPourIndividus = new NSMutableDictionary<EOIndividu, EOAssociation>();
		
		NSArray<EOElements> elementsDeCarriere = carriereFinder.elementsDeCarrierePourIndividusEtDate(individus, date);
		NSArray<EOContrat> contrats = ContratsService.creerNouvelleInstance(edc()).contratsPourIndividusEtDate(individus, date);
		
		NSArray<EOStatutTypeContrat> statutTypeContrats = EOStatutTypeContrat.fetchAll(edc());
		
		for (EOIndividu individu : individus) {
			
			EOAssociation statut = null;
			
			NSArray<EOContrat> contratsIndividu = 
					ERXQ.filtered(
							contrats, 
							ERXQ.equals(ERXQ.keyPath(EOContrat.TO_PERSONNEL_KEY, EOPersonnel.TO_INDIVIDU_KEY), individu)
						);	
			
			NSArray<EOElements> elementsIndividu = 
					ERXQ.filtered(
							elementsDeCarriere, 
							ERXQ.equals(EOElements.TO_INDIVIDU_KEY, individu)
						);
			
			if (contratsIndividu.isEmpty() == false) {
				EOContrat contrat = ERXArrayUtilities.firstObject(contratsIndividu);
				EOTypeContratTravail type = contrat.toTypeContratTravail();
				NSArray<EOStatutTypeContrat> statutTypeContratsFiltres = ERXQ.filtered(statutTypeContrats, EOStatutTypeContrat.TYPE_CONTRAT_TRAVAIL.eq(type));
				if (statutTypeContratsFiltres.isEmpty() == false) {
					statut = ERXArrayUtilities.firstObject(statutTypeContratsFiltres).statut();
				}
			} else if (elementsIndividu.isEmpty() == false) {
				statut = FactoryAssociation.shared().membrePermanentAssociation(edc());
			}
			
			if (statut != null) {
				statutsPourIndividus.put(individu, statut);
			}
			
		}
		return statutsPourIndividus.immutableClone();		
		
	}
	
}
