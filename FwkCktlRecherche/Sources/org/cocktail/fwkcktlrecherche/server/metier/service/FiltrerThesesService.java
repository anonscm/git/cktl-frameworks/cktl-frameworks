package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.util.Arrays;
import java.util.List;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe de service pour filtrer les theses
 */
public final class FiltrerThesesService {

	private EOEditingContext edc;
	public static final String CODE_ANNEE_C = "C";
	public static final String CODE_ANNEE_U = "U";
	public static final List<String> CODES_ANNEES = Arrays.asList(CODE_ANNEE_C, CODE_ANNEE_U);

	
	/**
	 * Crée une nouvelle instance de FiltrerThesesService
	 * @param edc editingContext
	 * @return FiltrerThesesService
	 */
	public static FiltrerThesesService creerNouvelleInstance(EOEditingContext edc) {
		return new FiltrerThesesService(edc);
	}
	
	private FiltrerThesesService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	private EOEditingContext edc() {
		return edc;
	}
	
	/**
	 * @param toutesLesTheses la liste des theses a filtrer
	 * @param titreThese le titre de la these à fitrer
	 * @param nomDoctorant le nom du doctorant à filtrer
	 * @param prenomDoctorant le prenom du doctorant à filtrer
	 * @param ecoleDoctorale l'ecole doctorale à filtrer
	 * @param laboratoire le laboratoire à filtrer
	 * @param laboratoires une liste de laboratoires à filtrer
	 * @param etatThese l'etat de la these à filtrer
	 * @param anneeCivileSoutenance annee civile de la soutenance à filtrer
	 * @param directeur le directeur à filtrer
	 * @param encadrant l'encadrant à filtrer
	 * @param anneeDebutAnneeUniversitaire l'année de début de l'année universitaire à filtrer
	 * @param civileOuUniversitaire si anneeDebutAnneeUniversitaire est civile ou universitaire
	 * @param isSeulementPremiereAnneeInscription filtre seulement les doctorants en premiere année
	 * @param domaineScientifique le domaine scientifique à filtrer
	 * @param sexe sexe de la personne
	 * @return les theses repondant aux criteres
	 */
	public NSArray<EODoctorantThese> filtrerListeTheses(NSArray<EODoctorantThese> toutesLesTheses, String titreThese, String nomDoctorant, String prenomDoctorant, 
			EOStructure ecoleDoctorale, EOStructure laboratoire, NSArray<EOStructure> laboratoires, EtatsThese etatThese, Integer anneeCivileSoutenance, 
			EOIndividu directeur, EOIndividu encadrant, Integer anneeDebutAnneeUniversitaire, String civileOuUniversitaire, Boolean isSeulementPremiereAnneeInscription, 
			EODomaineScientifique domaineScientifique, Sexe sexe) {
		if (toutesLesTheses == null) {
			return null;
		}
		
		EOQualifier qualifier = ERXQ.and(
				getContainsQualifier(EODoctorantThese.TO_CONTRAT.dot(Contrat.CON_OBJET_KEY).key(), titreThese),
				getContainsQualifier(EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_INDIVIDU_FWKPERS.dot(EOIndividu.NOM_AFFICHAGE_KEY)).key(), nomDoctorant),
				getContainsQualifier(EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_INDIVIDU_FWKPERS.dot(EOIndividu.PRENOM_AFFICHAGE_KEY)).key(), prenomDoctorant)
						);
		if (ecoleDoctorale != null) {
			qualifier = ERXQ.and(qualifier, EODoctorantThese.ECOLE_DOCTORALE.eq(ecoleDoctorale));
		}
		if (laboratoire != null) {
			qualifier = ERXQ.and(qualifier, EODoctorantThese.LABORATOIRES.containsObject(laboratoire));
		}
		if (laboratoires != null) {
			EOQualifier qualifierLabos = null;
			for (EOStructure _laboratoire : laboratoires) {
				qualifierLabos = ERXQ.or(qualifierLabos, EODoctorantThese.LABORATOIRES.containsObject(_laboratoire));
			}
			qualifier = ERXQ.and(qualifier, qualifierLabos);
		}
		if (etatThese != null) {
			
			EOQualifier qualifierEtatThese = null;
		
			if (etatThese.equals(EtatsThese.EN_COURS)) {
				qualifierEtatThese = ERXQ.and(
					ERXQ.or(EODoctorantThese.DATE_SOUTENANCE.isNull(), EODoctorantThese.DATE_SOUTENANCE.after(new NSTimestamp())),
					EODoctorantThese.DATE_FIN_ANTICIPEE.isNull()
					);
			} else if (etatThese.equals(EtatsThese.SOUTENUE)) {
				qualifierEtatThese = ERXQ.and(EODoctorantThese.DATE_SOUTENANCE.isNotNull(), EODoctorantThese.DATE_SOUTENANCE.before(new NSTimestamp()));
			} else if (etatThese.equals(EtatsThese.ABANDONNEE)) {
				qualifierEtatThese = EODoctorantThese.DATE_FIN_ANTICIPEE.isNotNull(); 
			}
			
			qualifier = ERXQ.and(qualifier, qualifierEtatThese);
		}
		if (sexe != null) {
			EOQualifier qualifierSexe = null;
			
			if (Sexe.FEMME.equals(sexe)) {
				qualifierSexe = EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_INDIVIDU_FWKPERS.dot(EOIndividu.TO_CIVILITE.dot(EOCivilite.SEXE_ONP))).eq(EOCivilite.SEXE_FEMININ);
			} else if (Sexe.HOMME.equals(sexe)) {
				qualifierSexe = EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_INDIVIDU_FWKPERS.dot(EOIndividu.TO_CIVILITE.dot(EOCivilite.SEXE_ONP))).eq(EOCivilite.SEXE_MASCULIN);
			}
			
			qualifier = ERXQ.and(qualifier, qualifierSexe);
		}
		
		NSMutableArray<EODoctorantThese> doctorantTheses = toutesLesTheses.mutableClone();
		doctorantTheses = EOQualifier.filteredArrayWithQualifier(doctorantTheses, qualifier).mutableClone();
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = thesesASupprimer(doctorantTheses, directeur, encadrant, anneeDebutAnneeUniversitaire, 
				civileOuUniversitaire, isSeulementPremiereAnneeInscription, domaineScientifique, anneeCivileSoutenance);
		
		doctorantTheses.removeAll(doctorantThesesASupprimer);
		
		return doctorantTheses;
	}
	
	private NSMutableArray<EODoctorantThese> thesesASupprimer(NSMutableArray<EODoctorantThese> doctorantTheses, IIndividu directeur, IIndividu encadrant, 
			Integer anneeDebutAnneeUniversitaire, String civileOuUniversitaire, Boolean isSeulementPremiereAnneeInscription, 
			EODomaineScientifique domaineScientifique, Integer anneeCivileSoutenance) {
		   
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
		
		if (directeur != null && encadrant != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonEncadrement(doctorantTheses, directeur, encadrant));
		}

		if (anneeDebutAnneeUniversitaire != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonAnnee(doctorantTheses, anneeDebutAnneeUniversitaire, civileOuUniversitaire, isSeulementPremiereAnneeInscription));
		}
		
		if (anneeCivileSoutenance != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonAnneeSoutenance(doctorantTheses, anneeCivileSoutenance));
		}
		
		if (domaineScientifique != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonDomaineScientifique(doctorantTheses, domaineScientifique));
		}
		
		return (NSMutableArray<EODoctorantThese>) ERXArrayUtilities.arrayWithoutDuplicates(doctorantThesesASupprimer);
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonEncadrement(NSMutableArray<EODoctorantThese> doctorantTheses, IIndividu directeur, IIndividu encadrant) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
		
		if (directeur != null && encadrant != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonDirecteurEtEncadrant(doctorantTheses, directeur, encadrant));
		} else if (directeur != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonDirecteur(doctorantTheses, directeur));
		} else if (encadrant != null) {
			doctorantThesesASupprimer.addAll(supprimerSelonEncadrant(doctorantTheses, encadrant));
		}
		
		return doctorantThesesASupprimer;
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonEncadrant(NSMutableArray<EODoctorantThese> doctorantTheses, IIndividu encadrant) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
	    boolean encadrantTrouve;
	    
	    for (EODoctorantThese these : doctorantTheses) {
	    	encadrantTrouve = false;
	    	int nbDir = these.toDirecteursThese().count();
	    	if (nbDir == 0) {
	    		doctorantThesesASupprimer.add(these);
	    	} else {
	    		for (int i = 0; i < nbDir; i++) {
	    			EODirecteurThese dir = these.toDirecteursThese().get(i);
	    			if (encadrant.equals(dir.toIndividuDirecteur()) && factoryAssociation().coEncadrantTheseAssociation(edc()).equals(dir.roleDirecteur())) {
	    				encadrantTrouve = true;
	    			}
	    			
	    			if (i == (nbDir - 1) && !encadrantTrouve) {
	    				doctorantThesesASupprimer.add(these);
	    			}
	    		}
	    	}
	    }
	    
	    return doctorantThesesASupprimer;
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonDirecteur(NSMutableArray<EODoctorantThese> doctorantTheses, IIndividu directeur) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
	    boolean directeurTrouve;
	    
	    for (EODoctorantThese these : doctorantTheses) {
	    	directeurTrouve = false;
	    	int nbDir = these.toDirecteursThese().count();
	    	if (nbDir == 0) {
	    		doctorantThesesASupprimer.add(these);
	    	} else {
	    		for (int i = 0; i < nbDir; i++) {
	    			EODirecteurThese dir = these.toDirecteursThese().get(i);
	    			if (directeur.equals(dir.toIndividuDirecteur()) && factoryAssociation().directeurTheseAssociation(edc()).equals(dir.roleDirecteur())) {
	    				directeurTrouve = true;
	    			}
	    			
	    			if (i == (nbDir - 1) && !directeurTrouve) {
	    				doctorantThesesASupprimer.add(these);
	    			}
	    		}
	    	}
	    }
	    
	    return doctorantThesesASupprimer;
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonDirecteurEtEncadrant(NSMutableArray<EODoctorantThese> doctorantTheses, IIndividu directeur, IIndividu encadrant) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
	    boolean directeurTrouve;
	    boolean encadrantTrouve;
	    
	    for (EODoctorantThese these : doctorantTheses) {
	    	directeurTrouve = false;
	    	encadrantTrouve = false;

	    	int nbDir = these.toDirecteursThese().count();
	    	
	    	if (nbDir == 0) {
	    		doctorantThesesASupprimer.add(these);
	    	} else {
	    		for (int i = 0; i < nbDir; i++) {
	    			EODirecteurThese dir = these.toDirecteursThese().get(i);
	    			if (directeur.equals(dir.toIndividuDirecteur()) && factoryAssociation().directeurTheseAssociation(edc()).equals(dir.roleDirecteur())) {
	    				directeurTrouve = true;
	    			}
	    			
	    			if (encadrant.equals(dir.toIndividuDirecteur()) && factoryAssociation().coEncadrantTheseAssociation(edc()).equals(dir.roleDirecteur())) {
	    				encadrantTrouve = true;
	    			}
	    			
	    			if (i == (nbDir - 1) && (!directeurTrouve || !encadrantTrouve)) {
	    				doctorantThesesASupprimer.add(these);
	    			}
	    		}
	    	}
	    }
	    
	    return doctorantThesesASupprimer;
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonDomaineScientifique(NSMutableArray<EODoctorantThese> doctorantTheses, EODomaineScientifique domaineScientifique) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
	    //suppression selon le domaine scientifique
		if (domaineScientifique != null) {
			for (EODoctorantThese these : doctorantTheses) {
				boolean aSupprimer = true;
				for (Avenant avenant : these.toContrat().avenants()) {
					if (domaineScientifique.equals(avenant.domaineScientifique())) {
						aSupprimer = false;
					}
				}
				
				if (aSupprimer) {
					doctorantThesesASupprimer.add(these);
				}
			}
		}
		
		return doctorantThesesASupprimer;
    }

	private NSMutableArray<EODoctorantThese> supprimerSelonAnnee(NSMutableArray<EODoctorantThese> doctorantTheses, Integer anneeDebutAnneeUniversitaire, 
			String civileOuUniversitaire, Boolean isSeulementPremiereAnneeInscription) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
		
	    //suppression selon l'année universitaire
		if (anneeDebutAnneeUniversitaire != null) {
			NSTimestamp debutAnneeUniv = null;
			NSTimestamp finAnneeUniv = null;
			if (CODE_ANNEE_U.equals(civileOuUniversitaire)) {
				debutAnneeUniv = new NSTimestamp(EnquetesSiredoService.debutAnneeUniversitaire(anneeDebutAnneeUniversitaire));
				finAnneeUniv = new NSTimestamp(EnquetesSiredoService.finAnneeUniversitaire(anneeDebutAnneeUniversitaire));
			} else if (CODE_ANNEE_C.equals(civileOuUniversitaire)) {
				debutAnneeUniv = new NSTimestamp(EnquetesSiredoService.debutAnneeCivile(anneeDebutAnneeUniversitaire));
				finAnneeUniv = new NSTimestamp(EnquetesSiredoService.finAnneeCivile(anneeDebutAnneeUniversitaire));
			}
			
			// on verifie selon l'avenant zero
			if (CODE_ANNEE_U.equals(civileOuUniversitaire) && isSeulementPremiereAnneeInscription) {
				for (EODoctorantThese these : doctorantTheses) {
					if (!DateCtrl.isBetweenEq(these.toContrat().avenantZero().avtDateDeb(), debutAnneeUniv, finAnneeUniv)) {
						doctorantThesesASupprimer.add(these);
					}
				}
			} else { // on verifie pour tous les avenants
				for (EODoctorantThese these : doctorantTheses) {
					boolean aSupprimer = true;
					for (Avenant avenant : these.toContrat().avenants()) {
						if (isAvenantEntreDates(debutAnneeUniv, finAnneeUniv, avenant)) {
							aSupprimer = false;
						}
					}
					
					if (aSupprimer) {
						doctorantThesesASupprimer.add(these);
					}
				}
			}
		}
		
		return doctorantThesesASupprimer;
    }
	
	private NSMutableArray<EODoctorantThese> supprimerSelonAnneeSoutenance(NSMutableArray<EODoctorantThese> doctorantTheses, Integer anneeCivileSoutenance) {
		
		NSMutableArray<EODoctorantThese> doctorantThesesASupprimer = new NSMutableArray<EODoctorantThese>();
		
		if (anneeCivileSoutenance != null) {
			NSTimestamp debutAnneeUniv = new NSTimestamp(EnquetesSiredoService.debutAnneeCivile(anneeCivileSoutenance));
			NSTimestamp finAnneeUniv = new NSTimestamp(EnquetesSiredoService.finAnneeCivile(anneeCivileSoutenance));
			for (EODoctorantThese these : doctorantTheses) {
				if (!DateCtrl.isBetweenEq(these.dateSoutenance(), debutAnneeUniv, finAnneeUniv)) {
					doctorantThesesASupprimer.add(these);
				}
			}
		}
		
		return doctorantThesesASupprimer;
    }
	
	private boolean isAvenantEntreDates(NSTimestamp debutAnneeUniv, NSTimestamp finAnneeUniv, Avenant avenant) {
	    return (avenant.avtDateDeb().before(debutAnneeUniv) && avenant.avtDateFin().after(finAnneeUniv))
	    || (avenant.avtDateDeb().before(debutAnneeUniv) && avenant.avtDateFin().after(debutAnneeUniv))
	    || (avenant.avtDateDeb().before(finAnneeUniv) && avenant.avtDateFin().after(finAnneeUniv))
	    || (avenant.avtDateDeb().after(debutAnneeUniv) && avenant.avtDateFin().before(finAnneeUniv));
    }
	
	private FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}
	
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	private EOQualifier getContainsQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.contains(nomColonne, filtre);
		}
		return qualifier;
	}
	
	/* Nomenclature */
	
	/**
	 * Enum des etats possible d'une these
	 */
	public enum EtatsThese {
		  EN_COURS ("En cours"),
		  SOUTENUE ("Soutenue"),
		  ABANDONNEE ("Abandonnée");
		   
		  private String name = "";
		   
		  EtatsThese(String name) {
		    this.name = name;
		  }
		  
		  /**
		   * @return libelle de l'etat
		   */
		  public String toString() {
		    return name;
		  }
	}
	
	/**
	 * Enum des etats possible d'une these
	 */
	public enum Sexe {
		  HOMME ("Homme"),
		  FEMME ("Femme");
		   
		  private String sexe = "";
		   
		  Sexe(String sexe) {
		    this.sexe = sexe;
		  }
		  
		  /**
		   * @return libelle de l'etat
		   */
		  public String toString() {
		    return sexe;
		  }
	}
	
}
