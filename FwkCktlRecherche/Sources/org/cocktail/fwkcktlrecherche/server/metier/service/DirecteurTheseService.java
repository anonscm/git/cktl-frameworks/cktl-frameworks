package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe permettant d'utiliser des methodes sur les directeurs de these
 *
 */
public final class DirecteurTheseService {
	
	private static final String LIBELLE_DIPLOME_HDR = "HABIL. DIR. RECH";
	private static final String LIBELLE_DIPLOME_DOCTORAT_ETAT = "DOCTORAT D'ETAT";
	private static final Integer SCALE = 10;
	private EOEditingContext edc;
	
	/**
	 * Crée une nouvelle instance de DirecteurTheseService
	 * @param edc editingContext
	 * @return DirecteurTheseService
	 */
	public static DirecteurTheseService creerNouvelleInstance(EOEditingContext edc) {
		return new DirecteurTheseService(edc);
	}
	
	private DirecteurTheseService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	private EOEditingContext edc() {
		return edc;
	}
	
	/**
	 * Retourne le directeur de these habilité d'une these
	 * @param directeurThese le directeur de these
	 * @param doctorantThese la these
	 * @return le directeur de these
	 */
	public EODirecteurTheseHabilite getDirecteurTheseHabiliteFromIndividu(EOIndividu directeurThese, EODoctorantThese doctorantThese) {
		EOQualifier qualDirTheseHabilite = ERXQ.and(ERXQ.equals(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY, directeurThese),
				ERXQ.equals(EODirecteurTheseHabilite.TO_STRUCTURE_ED_FWKPERS_KEY, doctorantThese.ecoleDoctorale()));
		return EODirecteurTheseHabilite.fetchDirecteurTheseHabilite(edc(), qualDirTheseHabilite);
	}
	
	/**
	 * Retourne le premier laboratoire dans lequel l'individu est présent
	 * @param individu individu
	 * @return la structure laboratoire
	 */
	public EOStructure getLaboratoireIndividu(EOIndividu individu) {
	    NSArray<EOStructure> services = individu.getServices();
	    for (EOStructure str : services) {
	    	for (EORepartTypeGroupe rtg : str.toRepartTypeGroupes()) {
	    		if (rtg.tgrpCode().equals(EOTypeGroupe.TGRP_CODE_LA)) {
	    			return str;
	    		}
	    	}
	    }
	    return null;
    }
	
	/**
	 * Retourne l'ecole doctorale dans lequel l'individu est présent
	 * @param individu individu
	 * @param membre le type d'association membre
	 * @return l'ecole doctorale de l'individu
	 */
	public EOStructure getEcoleDoctoraleIndividu(EOIndividu individu, EOAssociation membre) {
		EOStructure laboratoire = getLaboratoireIndividu(individu);
		if (laboratoire != null) {
			NSArray<EOStructure> ecolesDoctorales = listeEcolesDoctorales();
			for (EOStructure ecoleDoctorale : ecolesDoctorales) {
				NSArray<EOStructure> labos = getLaboratoiresEcoleDoctorale(ecoleDoctorale, membre);
				for (EOStructure labo : labos) {
					if (laboratoire.cStructure().equals(labo.cStructure())) {
						 return ecoleDoctorale;
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Retourne les laboratoires d'une ecole doctorale passee en parametre
	 * @param ecoleDoctorale l'ecole doctorale
	 * @param membre le type d'association membre
	 * @return les laboratoires
	 */
	public NSArray<EOStructure> getLaboratoiresEcoleDoctorale(EOStructure ecoleDoctorale, EOAssociation membre) {
	    EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, ecoleDoctorale.cStructure()), ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY,
	    		membre), ERXQ.equals(
	    		EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, 
	    				EOTypeGroupe.TGRP_CODE_LA));
	    NSArray<EORepartAssociation> lesRepartAssoLabo = ERXArrayUtilities.arrayWithoutDuplicates(EORepartAssociation.fetchAll(edc(), qualLabos));
	    NSArray<EOStructure> labos = (NSArray<EOStructure>) lesRepartAssoLabo.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	    return labos;
    }

	/**
	 * Retourne toutes les ecoles doctorales
	 * @return toutes les ecoles doctorales
	 */
	public NSArray<EOStructure> listeEcolesDoctorales() {
		EOQualifier qualDoct = ERXQ.and(ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED"),
				ERXQ.equals(EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
		NSArray<EOStructure> lesEds = EOStructure.fetchAll(edc(), qualDoct, null);
		if (lesEds.count() == 0) {
			return null;
		} else {
			return lesEds;
		}
	}
	
	/**
	 * @return la liste des laboratoires
	 */
	public NSArray<EOStructure> listeLaboratoires() {
		EOQualifier qualLabo = ERXQ.and(ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "LA"),
				ERXQ.equals(EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
		NSArray<EOStructure> lesLabos = EOStructure.fetchAll(edc(), qualLabo, null);
		if (lesLabos.count() == 0) {
			return null;
		} else {
			return lesLabos;
		}
	}
	
	/**
	 * Retourne l'établissement dans lequel l'individu est présent
	 * @param individu individu
	 * @return l'etablissement
	 */
	public EORne getEtablissementIndividu(EOIndividu individu) {
		NSArray<EOStructure> etablissements = individu.getEtablissementsAffectation(null);
		if (etablissements.size() > 0) {
			 return etablissements.get(0).toRne();
		}
		return null;
	}
	
	/**
	 * Retourne la date de HDR de l'individu
	 * @param individu individu
	 * @return la date de HDR
	 */
	public NSTimestamp getDateHDRIndividu(EOIndividu individu) {
		NSArray<EOIndividuDiplome> diplomes = EOIndividuDiplome.fetchAllForIndividu(edc(), individu);
		for (EOIndividuDiplome diplome : diplomes) {
			if (diplome.toDiplome().lcDiplome().equals(LIBELLE_DIPLOME_HDR) || diplome.toDiplome().lcDiplome().equals(LIBELLE_DIPLOME_DOCTORAT_ETAT)) {
				 return diplome.dDiplome();
			}
		}
		return null;
	}
	
	public BigDecimal calculSommeQuotiteThese(EODoctorantThese these, EODirecteurThese editingDirecteurThese, NSTimestamp dateDebutEncadrement, NSTimestamp dateFinEncadrement) {
		BigDecimal totalQuotite = new BigDecimal(0);
		NSTimestamp debutThese = these.dateDeb();
		NSTimestamp finThese = these.dateFin();
		
		BigDecimal nbJoursThese = new BigDecimal(DateCtrl.nbJoursEntreDates(debutThese, finThese));
		
	    NSArray<EODirecteurThese> listeDirecteurs = these.toDirecteursThese(null, true);
	    for (EODirecteurThese currentDirecteur : listeDirecteurs) {
	    	if (currentDirecteur != editingDirecteurThese && currentDirecteur.repartAssociationDirecteur() != null && currentDirecteur.repartAssociationDirecteur().rasQuotite() != null) {
	    		BigDecimal nbJoursEncadrementCurrentDirecteur = new BigDecimal(DateCtrl.nbJoursEntreDates(currentDirecteur.repartAssociationDirecteur().rasDOuverture(), currentDirecteur.repartAssociationDirecteur().rasDFermeture()));
	    		BigDecimal ratio = nbJoursEncadrementCurrentDirecteur.divide(nbJoursThese, SCALE, BigDecimal.ROUND_CEILING);
	    		BigDecimal currentQuotite = ratio.multiply(currentDirecteur.repartAssociationDirecteur().rasQuotite());
	    		totalQuotite = totalQuotite.add(currentQuotite);
	    	}
	    }
	    return totalQuotite;
	}
	
	public BigDecimal calculQuotite(EODoctorantThese these, BigDecimal quotite, NSTimestamp dateDebutEncadrement, NSTimestamp dateFinEncadrement) {
		return calculQuotite(these.dateDeb(), these.dateFin(), quotite, dateDebutEncadrement, dateFinEncadrement);
	}
	
	public BigDecimal calculQuotite(NSTimestamp dateDebutThese, NSTimestamp dateFinThese, BigDecimal quotite, NSTimestamp dateDebutEncadrement, NSTimestamp dateFinEncadrement) {
		BigDecimal nbJoursThese = new BigDecimal(DateCtrl.nbJoursEntreDates(dateDebutThese, dateFinThese));
		BigDecimal nbJoursEncadrement = new BigDecimal(DateCtrl.nbJoursEntreDates(dateDebutEncadrement, dateFinEncadrement));
		
		BigDecimal ratio = nbJoursEncadrement.divide(nbJoursThese, SCALE, BigDecimal.ROUND_CEILING);
		return ratio.multiply(quotite);
	}
	
}
