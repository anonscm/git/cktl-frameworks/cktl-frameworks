package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.foundation.ERXProperties;

/**
 * Permet de récupérer certain codes de groupes applicatifs 
 * recherche et encore des qualifier associés 
 */
public class GroupeRechercheService {

	private EOEditingContext edc;
	
	/**
	 * @param edc utlisé pour aller chercher les valeurs des paramètres
	 */
	public GroupeRechercheService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * @return le groupe des partenaires de la recherche
	 */
	public EOQualifier qualifierForGroupePartenairesRecherche() {
		if (ERXProperties.booleanForKey(ParametresRecherche.ALLER_CHERCHER_DANS_PARTENAIRES_RECHERCHE)) {
			String codeStructureGroupePartenairesRecherche = EOGrhumParametres.parametrePourCle(edc, ParametresRecherche.PARAM_GROUPE_PARTENAIRES_RECHERCHE);
			return EOStructure.C_STRUCTURE.eq(codeStructureGroupePartenairesRecherche);
		} else {
			return null;
		}

	}

	/**
	 * @return le groupe des poles de compétitivité
	 */
	public EOQualifier qualifierForGroupePolesDeCompetitivitesRecherche() {
		String codeStructureGroupePoles = EOGrhumParametres.parametrePourCle(edc, ParametresRecherche.POLE_COMPETITIVITE_KEY);
		return EOStructure.C_STRUCTURE.eq(codeStructureGroupePoles);

	}
	
}
