package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.util.Calendar;
import java.util.Date;

/**
 * Classe de service pour les editions d'editions siredo
 */
public class EnquetesSiredoService {

	private static final Integer MOIS_DEBUT_ANNEE_CIVILE = Calendar.JANUARY;
	private static final Integer MOIS_FIN_ANNEE_UNIVERSITAIRE = Calendar.AUGUST;
	private static final Integer MOIS_DEBUT_ANNEE_UNIVERSITAIRE = Calendar.SEPTEMBER;
	private static final Integer MOIS_FIN_ANNEE_CIVILE = Calendar.DECEMBER;

	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de rentrée universitaire d'une année civile donnée
	 */
	public static Date debutAnneeUniversitaire(Integer anneeCivile) {
		final Integer un = 1;
		Calendar calendar = Calendar.getInstance();
		calendar.set(anneeCivile, MOIS_DEBUT_ANNEE_UNIVERSITAIRE, un, 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de fin de l'année universitaire d'une année civile donnée
	 */
	public static Date finAnneeUniversitaire(Integer anneeCivile) {
		final Integer trenteEtUn = 31;
		Calendar calendar = Calendar.getInstance();
		calendar.set(anneeCivile + 1, MOIS_FIN_ANNEE_UNIVERSITAIRE, trenteEtUn, 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de rentrée universitaire precedente d'une année civile donnée
	 */
	public static Date debutAnneeUniversitairePrecedente(Integer anneeCivile) {
		return debutAnneeUniversitaire(anneeCivile - 1);
	}
	
	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de fin de l'année universitaire precedente d'une année civile donnée
	 */
	public static Date finAnneeUniversitairePrecedente(Integer anneeCivile) {
		return finAnneeUniversitaire(anneeCivile - 1);
	}

	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de debut d'année civile d'une année civile donnée
	 */
	public static Date debutAnneeCivile(Integer anneeCivile) {
		final Integer un = 1;
		Calendar calendar = Calendar.getInstance();
		calendar.set(anneeCivile, MOIS_DEBUT_ANNEE_CIVILE, un, 0, 0, 0);
		return calendar.getTime();
	}

	/**
	 * @param anneeCivile année civile
	 * @return Retourne la date de fin de l'année civile d'une année civile donnée
	 */
	public static Date finAnneeCivile(Integer anneeCivile) {
		final Integer trenteEtUn = 31;
		Calendar calendar = Calendar.getInstance();
		calendar.set(anneeCivile, MOIS_FIN_ANNEE_CIVILE, trenteEtUn, 0, 0, 0);
		return calendar.getTime();
	}
	
}
