package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.math.BigDecimal;

import org.apache.commons.lang.time.DateUtils;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * 
 * @author jlafourc
 *
 */
public class CreationContratService {

	private EOEditingContext edc;
	
	/**
	 * @param edc L'editingContext de travail
	 */
	public CreationContratService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}
	
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * Créé un contrat simple (pas un contrat
	 * de recherche à partir des informations 
	 * contenues dans l'appel à projet
	 * @param aap qui contient les informations
	 * @param persId l'id de l'utilisateur
	 * @throws Exception 
	 * @throws  
	 */
	public void creerContratPourAap(EOAap aap, Integer persId) throws Exception {
		
		FactoryConvention factoryContrat = new FactoryConvention(getEdc(), false);
		
		EOQualifier qualifierForUtilisateur = ERXQ.equals(EOUtilisateur.PERS_ID_KEY, persId);
		EOUtilisateur utilisateur = EOUtilisateur.fetchFirstByQualifier(getEdc(), qualifierForUtilisateur);
		
		Contrat contrat = factoryContrat.creerConventionVierge(utilisateur, aap.getEtablisssementGestionnaireFinancier());
		
		contrat.setConObjet(aap.objet());
		contrat.setConObjetCourt(aap.objetCourt());
		
		contrat.avenantZero().setAvtMontantHt(new BigDecimal(aap.getTotalObtenu()));
		
		contrat.avenantZero().setDomaineScientifiqueRelationship(aap.domaineScientifique());
		
		contrat.setDateDebut(aap.dateRefusAcceptation());
		NSTimestamp dateFin = 
			new NSTimestamp(DateUtils.addYears(aap.dateRefusAcceptation(), 1));
		contrat.setDateFin(dateFin);
		contrat.setExerciceCocktailRelationship(
			EOExerciceCocktail.fetchFirstByQualifier(
				getEdc(), 
				ERXQ.equals(
					EOExerciceCocktail.EXE_EXERCICE_KEY, 
					EOExercice.getExerciceOuvert(getEdc()).exeExercice()
				)
			)
		);
		
		TypeContrat typeSubventions = TypeContrat.fetch(getEdc(), TypeContrat.TYCON_ID_INTERNE.eq(TypeContrat.TYPE_SUBVENTIONS)); 
		contrat.setTypeContratRelationship(typeSubventions);
		
		FactoryContratPartenaire factoryContratPartenaire = new FactoryContratPartenaire(getEdc(), false);
		
		for (EOStructure s : aap.getStructuresRecherchesConcernees()) {
			Boolean principal = false;
			if (ERXEOControlUtilities.eoEquals(s, aap.getLaboratoirePrincipal())) {
				principal = true;
			}
			ContratPartenaire cp = factoryContratPartenaire.creerContratPartenaire(contrat, s, null, principal, null);
			for (EOIndividu i : aap.responsablesScientifiquesPourStructureRecherche(s)) {
				factoryContratPartenaire.ajouterContact(
					cp, 
					i, 
					new NSArray<EOAssociation>(FactoryAssociation.shared().responsableScientifiqueAssociation(getEdc()))
				);
			}
		}
		
		for (IPersonne p : aap.getPartenaires()) {
			factoryContratPartenaire.creerContratPartenaire(contrat, p, null, false, null);
		}
		
		contrat.setCentreResponsabiliteRelationship(aap.getLaboratoirePrincipal(), persId);
		
		aap.setContratLieRelationship(contrat);
		
	}
	
	
}
