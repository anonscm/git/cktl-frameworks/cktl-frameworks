package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class JuryService {

	private EOEditingContext edc;

	public JuryService(EOEditingContext edc) {
		super();
		this.setEdc(edc);
	}

	public EOEditingContext edc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public NSArray<EOMembreJuryThese> listeMembresJury(EODoctorantThese these) {
		return listeMembres(these, associationsMembresJury());
	}
	
	public NSArray<EOMembreJuryThese> listeMembresJuryAvecRoleMembre(EODoctorantThese these) {
		return listeMembres(these, associationsMembresJuryAvecRoleMembre());
	}
	
	public NSArray<EOMembreJuryThese> listeRapporteurs(EODoctorantThese these) {
		return listeMembres(these, associationsRaporteurs());
	}
	
	public NSArray<EOMembreJuryThese> listePresidents(EODoctorantThese these) {
		return listeMembres(these, associationsPresident());
	}
	
	public NSArray<EOMembreJuryThese> listeMembres(EODoctorantThese these, NSArray<EOAssociation> associationsMembres) {
		EOQualifier qualMjt = ERXQ.and(ERXQ.equals(EOMembreJuryThese.TO_DOCTORANT_THESE_KEY, these),
				ERXQ.in(EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, these.toContrat().partenairesForAssociations(associationsMembres)));
		return EOMembreJuryThese.fetchMembreJuryTheses(edc(), qualMjt, null);
	}
	
	public NSArray<EOAssociation> associationsRaporteurs() {
		return new NSArray<EOAssociation>(factoryAssociation().rapporteurTheseAssociation(edc()));
	}
	
	public NSArray<EOAssociation> associationsMembresJury() {
		return factoryAssociation().associationsFilleDe(factoryAssociation().rolesJuryAssociation(edc()), edc());
	}
	
	public NSArray<EOAssociation> associationsMembresJuryAvecRoleMembre() {
		return new NSArray<EOAssociation>(factoryAssociation().membreJuryAssociation(edc()));
	}
	
	public NSArray<EOAssociation> associationsPresident() {
		return new NSArray<EOAssociation>(factoryAssociation().presidentJuryAssociation(edc()));
	}
	
	public FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}
	
}
