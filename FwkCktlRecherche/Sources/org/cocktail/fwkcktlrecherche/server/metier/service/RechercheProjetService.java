package org.cocktail.fwkcktlrecherche.server.metier.service;

import java.util.List;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.Projet;
import org.cocktail.cocowork.server.metier.convention.ProjetContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class RechercheProjetService {

	private EOEditingContext edc;

	/**
	 * Constructeur avec l'edc
	 * @param edc l'editingContext de travail
	 */
	public RechercheProjetService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * 
	 * @param criteres de recherche
	 * @return les projets correspodants aux critères
	 */
	public List<EOProjetScientifique> rechercher(RechercheProjetCriteres criteres) {
		
		return ERXArrayUtilities.arrayWithoutDuplicates(EOProjetScientifique.fetchAll(edc, getQualifierForCriteres(criteres)));
		
	}
	
	
	protected EOQualifier getQualifierForCriteres(RechercheProjetCriteres criteres) {
		return 
			ERXQ.and(
				getQualifierForAcronyme(criteres.getToken()),
				getQualifierForAnnee(criteres.getAnnee()),
				getQualifierForNumero(criteres.getNumero()),
				getQualifierForStructurePorteuse(criteres.getStructurePorteuse()),
				getQualifierForResponsableScientifique(criteres.getResponsableScientifique()),
				getQualifierForSupprime()
			);
	}
	
	
	/**
	 * @param annee l'annee du projet
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForAnnee(Integer annee) {
		
		if (annee == null) { return null; }
		
		return EOProjetScientifique.PROJET
			.dot(Projet.EXERCICE_COCKTAIL)
			.dot(EOExerciceCocktail.EXE_EXERCICE_KEY)
				.eq(annee);
	
	}
	
	/**
	 * @param numero le numero du projet
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForNumero(Integer numero) {
		
		if (numero == null) { return null; }
		
		return EOProjetScientifique.PROJET
			.dot(Projet.PROJET_CONTRATS)
			.dot(ProjetContrat.CONTRAT)
			.dot(Contrat.CON_INDEX)
				.eq(numero);

	}
	
	/**
	 * @param structure la structurePorteuse du projet
	 * @return le qualifier correspondant
	 */
	protected EOQualifier getQualifierForStructurePorteuse(EOStructure structure) {
		
		if (structure == null) { return null; }
		
		EORelationship projetRelationship = 
				ERXEOAccessUtilities
					.entityNamed(null, EOProjetScientifique.ENTITY_NAME)
						.relationshipNamed(EOProjetScientifique.PROJET_KEY);
		
		String sourceAttributeName = projetRelationship.sourceAttributes().lastObject().name();
		String destinationAttributeName = projetRelationship.destinationAttributes().lastObject().name();
		
		EOQualifier sousQualifier = 
				Projet.PROJET_CONTRATS
					.dot(ProjetContrat.CONTRAT)
					.dot(Contrat.CONTRAT_PARTENAIRES)
					.dot(ContratPartenaire.PERS_ID)
						.eq(structure.persId());
		
		return new ERXQualifierInSubquery(sousQualifier, Projet.ENTITY_NAME, sourceAttributeName, destinationAttributeName);
	
	}
	/**
	 * @param responsable le responsable scientifique du projet
	 * @return le qualifier correspondant aux contrats ayant ce responsable scientifique
	 */
	protected EOQualifier getQualifierForResponsableScientifique(EOIndividu responsable) {
		
		if (responsable == null) { return null; }
		
		EORelationship projetRelationship = 
				ERXEOAccessUtilities
					.entityNamed(null, EOProjetScientifique.ENTITY_NAME)
						.relationshipNamed(EOProjetScientifique.PROJET_KEY);
		
		String sourceAttributeName = projetRelationship.sourceAttributes().lastObject().name();
		String destinationAttributeName = projetRelationship.destinationAttributes().lastObject().name();
		
		
		FactoryAssociation factoryAssociation = FactoryAssociation.shared();
		EOAssociation responsableScientifiqueAssociation = factoryAssociation.responsableScientifiqueAssociation(edc);
		
		EOQualifier sousQualifier =
				Projet.PROJET_CONTRATS
					.dot(ProjetContrat.CONTRAT)
					.dot(Contrat.GROUPE_PARTENAIRE)
					.dot(EOStructure.TO_REPART_STRUCTURES_ELTS)
					.dot(EORepartStructure.TO_REPART_ASSOCIATIONS)
					.dot(EORepartAssociation.TO_ASSOCIATION)
						.eq(responsableScientifiqueAssociation)
				.and(
					Projet.PROJET_CONTRATS
						.dot(ProjetContrat.CONTRAT)
						.dot(Contrat.GROUPE_PARTENAIRE)
						.dot(EOStructure.TO_REPART_STRUCTURES_ELTS)
						.dot(EORepartStructure.TO_REPART_ASSOCIATIONS)
						.dot(EORepartAssociation.PERS_ID)
							.eq(responsable.persId()));
		
		return new ERXQualifierInSubquery(sousQualifier, Projet.ENTITY_NAME, sourceAttributeName, destinationAttributeName);

	}
	
	
	/**
	 * @param token le token
	 * @return le qualifier correspodant aux projet qui contiennent le token dans leur acronyme
	 */
	protected EOQualifier getQualifierForAcronyme(String token) {
		
		if (token == null) { return null; }
		
		return EOProjetScientifique.PROJET
				.dot(Projet.PROJET_CONTRATS_KEY)
				.dot(ProjetContrat.CONTRAT)
				.dot(Contrat.CON_OBJET_COURT)
					.contains(token);
		
	}
	
	protected EOQualifier getQualifierForSupprime() {
		return EOProjetScientifique.PROJET_SUPPRIME.isFalse();
	}
	
	/**
	 * 
	 * @author Julien Lafoucade
	 * 
	 * Bean utilitaire pour les critères de recherche de projet
	 *
	 */
	public static class RechercheProjetCriteres {
		
		private String token;
		private Integer annee;
		private Integer numero;
		private EOStructure structurePorteuse;
		private EOIndividu responsableScientifique;
		
		public String getToken() {
			return token;
		}
		
		public void setToken(String token) {
			this.token = token;
		}
		
		public Integer getAnnee() {
			return annee;
		}
		
		public void setAnnee(Integer annee) {
			this.annee = annee;
		}
		
		public Integer getNumero() {
			return numero;
		}
		
		public void setNumero(Integer numero) {
			this.numero = numero;
		}
		
		public EOStructure getStructurePorteuse() {
			return structurePorteuse;
		}
		
		public void setStructurePorteuse(EOStructure structurePorteuse) {
			this.structurePorteuse = structurePorteuse;
		}
		
		public EOIndividu getResponsableScientifique() {
			return responsableScientifique;
		}
		
		public void setResponsableScientifique(EOIndividu responsableScientifique) {
			this.responsableScientifique = responsableScientifique;
		}
		
	}
	
	
}
