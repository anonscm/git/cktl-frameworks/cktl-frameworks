package org.cocktail.fwkcktlrecherche.server.metier.service;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Classe de service sur les cotutelles
 */
public final class CotutelleService {

	private EOEditingContext edc;
	private Integer persId;
	private NSArray<EORepartAssociation> repartAssociationCotutelle;
	private NSArray<EORepartAssociation> repartAssociationPresenceEtablissement;

	/**
	 * Crée une nouvelle instance de CotutelleService
	 * @param edc editing context
	 * @param persId persId de la personne connectée
	 * @return CotutelleService
	 */
	public static CotutelleService creerNouvelleInstance(EOEditingContext edc, Integer persId) {
		return new CotutelleService(edc, persId);
	}
	
	private CotutelleService(EOEditingContext edc, Integer persId) {
		this.edc = edc;
		this.persId = persId;
	}
	
	private EOEditingContext edc() {
		return edc;
	}
	
	private Integer getPersId() {
		return persId;
	}
	
	/**
	 * @param these la these
	 * @return la liste des etablissements de cotutelle
	 */
	public NSArray<EORepartAssociation> listeEtablissementsCotutelle(EODoctorantThese these) {
		if (repartAssociationCotutelle == null) {
			EOQualifier qual = ERXQ.equals(EOAssociation.ASS_CODE_KEY, factoryAssociation().etablissementCotutelleAssociation(edc()).assCode());
			NSArray<EOAssociation> lesAssociations = EOAssociation.fetchAll(edc(), qual);
			repartAssociationCotutelle = these.toContrat().repartAssociationPartenairesForAssociations(lesAssociations);
		}
		return repartAssociationCotutelle;
	}
	
	/**
	 * @param these la these
	 * @return la liste des présences du doctorant dans les établissements
	 */
	public NSArray<EORepartAssociation> listePresenceEtablissementCotutelle(EODoctorantThese these) {
		if (repartAssociationPresenceEtablissement == null) {
			EOQualifier qual = ERXQ.equals(EOAssociation.ASS_CODE_KEY, factoryAssociation().etablissementAccueil(edc()).assCode());
			NSArray<EOAssociation> lesAssociations = EOAssociation.fetchAll(edc(), qual);
			repartAssociationPresenceEtablissement = these.toContrat().repartAssociationPartenairesForAssociations(lesAssociations);
		}
		return repartAssociationPresenceEtablissement;
	}
	
	public boolean supprimerPresenceEtablissement(EODoctorantThese these, EORepartAssociation repartAssociationPresenceEtablissement) {
		ContratUtilities cu = ContratUtilities.creerNouvelleInstance(edc());
			cu.supprimerRolePourPartenaireDansContrat(factoryAssociation().etablissementAccueil(edc()), repartAssociationPresenceEtablissement.toPersonne(),
					these.toContrat(), getPersId(), repartAssociationPresenceEtablissement.rasDOuverture());
		return true;
	}
	
	public boolean supprimerCotutelle(EODoctorantThese these, EORepartAssociation repartAssociationCotutelle, boolean isModeDebug) {
		EOQualifier qualCP = ERXQ.and(ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, repartAssociationCotutelle.persId()),
				ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, these.toContrat()));
		ContratPartenaire selectedContratPartenaireCotutelle = ContratPartenaire.fetch(edc(), qualCP);
		
		FactoryContratPartenaire fcp = new FactoryContratPartenaire(edc(), isModeDebug);
		fcp.supprimerLeRole(selectedContratPartenaireCotutelle, factoryAssociation().etablissementCotutelleAssociation(edc()));
		try {
			fcp.supprimerContratPartenaire(selectedContratPartenaireCotutelle, getPersId());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}
	
}
