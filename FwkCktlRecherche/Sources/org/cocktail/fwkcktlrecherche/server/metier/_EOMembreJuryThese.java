/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMembreJuryThese.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOMembreJuryThese extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "MembreJuryThese";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "mjtOrdre";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE_STRUCT_EXTERNE = new er.extensions.eof.ERXKey<java.lang.String>("libelleStructExterne");
	public static final er.extensions.eof.ERXKey<java.lang.String> RAPPORTEUR_SEANCE = new er.extensions.eof.ERXKey<java.lang.String>("rapporteurSeance");
	public static final er.extensions.eof.ERXKey<java.lang.String> TEM_ENSEIGNANT = new er.extensions.eof.ERXKey<java.lang.String>("temEnseignant");
	public static final er.extensions.eof.ERXKey<java.lang.String> TITRE_SPECIAL = new er.extensions.eof.ERXKey<java.lang.String>("titreSpecial");
	
	public static final String LIBELLE_STRUCT_EXTERNE_KEY = LIBELLE_STRUCT_EXTERNE.key();
	public static final String RAPPORTEUR_SEANCE_KEY = RAPPORTEUR_SEANCE.key();
	public static final String TEM_ENSEIGNANT_KEY = TEM_ENSEIGNANT.key();
	public static final String TITRE_SPECIAL_KEY = TITRE_SPECIAL.key();

	// Attributs non visibles
	public static final String C_CORPS_KEY = "cCorps";
	public static final String CP_ORDRE_KEY = "cpOrdre";
	public static final String C_RNE_KEY = "cRne";
	public static final String ID_THESE_KEY = "idThese";
	public static final String MJT_ORDRE_KEY = "mjtOrdre";

	//Colonnes dans la base de donnees
	public static final String LIBELLE_STRUCT_EXTERNE_COLKEY = "LIBELLE_STRUCT_EXTERNE";
	public static final String RAPPORTEUR_SEANCE_COLKEY = "RAPPORTEUR_SEANCE";
	public static final String TEM_ENSEIGNANT_COLKEY = "TEM_ENSEIGNANT";
	public static final String TITRE_SPECIAL_COLKEY = "TITRE_SPECIAL";

	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String CP_ORDRE_COLKEY = "CP_ORDRE";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String ID_THESE_COLKEY = "ID_THESE";
	public static final String MJT_ORDRE_COLKEY = "MJT_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> TO_CONTRAT_PARTENAIRE = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("toContratPartenaire");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> TO_CORPS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("toCorps");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> TO_DOCTORANT_THESE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese>("toDoctorantThese");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
	
	public static final String TO_CONTRAT_PARTENAIRE_KEY = TO_CONTRAT_PARTENAIRE.key();
	public static final String TO_CORPS_KEY = TO_CORPS.key();
	public static final String TO_DOCTORANT_THESE_KEY = TO_DOCTORANT_THESE.key();
	public static final String TO_RNE_KEY = TO_RNE.key();

	// Accessors methods
	public java.lang.String libelleStructExterne() {
		return (java.lang.String) storedValueForKey(LIBELLE_STRUCT_EXTERNE_KEY);
	}

	public void setLibelleStructExterne(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_STRUCT_EXTERNE_KEY);
	}
	
	public java.lang.String rapporteurSeance() {
		return (java.lang.String) storedValueForKey(RAPPORTEUR_SEANCE_KEY);
	}

	public void setRapporteurSeance(java.lang.String value) {
	    takeStoredValueForKey(value, RAPPORTEUR_SEANCE_KEY);
	}
	
	public java.lang.String temEnseignant() {
		return (java.lang.String) storedValueForKey(TEM_ENSEIGNANT_KEY);
	}

	public void setTemEnseignant(java.lang.String value) {
	    takeStoredValueForKey(value, TEM_ENSEIGNANT_KEY);
	}
	
	public java.lang.String titreSpecial() {
		return (java.lang.String) storedValueForKey(TITRE_SPECIAL_KEY);
	}

	public void setTitreSpecial(java.lang.String value) {
	    takeStoredValueForKey(value, TITRE_SPECIAL_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaire() {
	    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire) storedValueForKey(TO_CONTRAT_PARTENAIRE_KEY);
	}

	public void setToContratPartenaireRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.ContratPartenaire oldValue = toContratPartenaire();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_PARTENAIRE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_PARTENAIRE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOCorps toCorps() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps) storedValueForKey(TO_CORPS_KEY);
	}

	public void setToCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = toCorps();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese) storedValueForKey(TO_DOCTORANT_THESE_KEY);
	}

	public void setToDoctorantTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese oldValue = toDoctorantThese();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_THESE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_THESE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORne) storedValueForKey(TO_RNE_KEY);
	}

	public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
	    }
	}
	
	public static EOMembreJuryThese createMembreJuryThese(EOEditingContext editingContext, java.lang.String rapporteurSeance, java.lang.String temEnseignant, org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaire, org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese) {
		EOMembreJuryThese eo = (EOMembreJuryThese) EOUtilities.createAndInsertInstance(editingContext, _EOMembreJuryThese.ENTITY_NAME);    
		eo.setRapporteurSeance(rapporteurSeance);
		eo.setTemEnseignant(temEnseignant);
		eo.setToContratPartenaireRelationship(toContratPartenaire);
		eo.setToDoctorantTheseRelationship(toDoctorantThese);
		return eo;
	}

	public EOMembreJuryThese localInstanceIn(EOEditingContext editingContext) {
		return (EOMembreJuryThese) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOMembreJuryThese creerInstance(EOEditingContext editingContext) {
		return (EOMembreJuryThese) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOMembreJuryThese.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOMembreJuryThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOMembreJuryThese>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOMembreJuryThese fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOMembreJuryThese fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOMembreJuryThese> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMembreJuryThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOMembreJuryThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOMembreJuryThese fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOMembreJuryThese> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMembreJuryThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOMembreJuryThese fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOMembreJuryThese eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOMembreJuryThese ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOMembreJuryThese fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAllMembreJuryTheses(EOEditingContext editingContext) {
		return _EOMembreJuryThese.fetchAllMembreJuryTheses(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchAllMembreJuryTheses(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOMembreJuryThese.fetchMembreJuryTheses(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOMembreJuryThese> fetchMembreJuryTheses(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOMembreJuryThese> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOMembreJuryThese>(_EOMembreJuryThese.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOMembreJuryThese> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOMembreJuryThese fetchMembreJuryThese(EOEditingContext editingContext, String keyName, Object value) {
		return _EOMembreJuryThese.fetchMembreJuryThese(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOMembreJuryThese fetchMembreJuryThese(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOMembreJuryThese> eoObjects = _EOMembreJuryThese.fetchMembreJuryTheses(editingContext, qualifier, null);
	    EOMembreJuryThese eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one MembreJuryThese that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
