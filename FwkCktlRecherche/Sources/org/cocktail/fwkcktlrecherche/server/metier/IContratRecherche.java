package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

/**
 * Interface métier du contrat de recheche
 */
public interface IContratRecherche {
	
	/**
	 * @return Concatenation de l'annee de l'exercice avec l'index du contrat (donne une valeur unique)
	 */
	String numero();
	
	/**
	 * @return l'intitulé long du contrat
	 */
	String getObjet();
	
	/**
	 * @return le créateur du dossier
	 */
	IIndividu getCreateurDuDossier();
	
	/**
	 * @return la liste des structures concernées
	 */
	List<? extends IStructure> structureRechercheConcernees();
	
	/**
	 * @return la liste des responsables scientifiques
	 */
	List<? extends IIndividu> responsablesScientifiques();
	
	/**
	 * @return le type de contrat
	 */
	String getTypeContrat();
}
