/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EODirecteurTheseHabilite extends _EODirecteurTheseHabilite {
	
    private static final long serialVersionUID = 8998488857382272008L;
	public static final EOSortOrdering SORT_NOM = EOSortOrdering.sortOrderingWithKey(TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static NSArray<EOSortOrdering> ARRAY_SORT_NOM = new NSArray<EOSortOrdering>(SORT_NOM);
	public static final String TEM_VALIDE_OUI = "O";
	public static final String TEM_VALIDE_NON = "N";

	public String libelleLabo() {
		if (isLaboratoireLocal()) {
			return (String) valueForKeyPath(TO_STRUCTURE_LABO_FWKPERS_KEY + "." + EOStructure.LL_STRUCTURE_KEY);
		} else {
			return (String) storedValueForKey(LABO_EXTERNE_DTH_KEY);
		}
	}

	public String libelleEtab() {
		if (isEtablissementFrancais()) {
			return (String) valueForKeyPath(TO_RNE_FWK_PERS_KEY + "." + EORne.LL_RNE_KEY);
		} else {
			return (String) valueForKeyPath(TO_STRUCTURE_ETAB_FWK_PERS_KEY + "." + EOStructure.LL_STRUCTURE_KEY);
		}
	}

	public Boolean isEtablissementFrancais() {
		EORne leRne = (EORne) storedValueForKey(TO_RNE_FWK_PERS_KEY);
		if (leRne == null)
			return false;
		else
			return true;
	}

	public Boolean isLaboratoireLocal() {
		EOStructure leLabo = (EOStructure) storedValueForKey(TO_STRUCTURE_LABO_FWKPERS_KEY);
		if (leLabo == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void validateForSave() throws ValidationException {

		if ((isEtablissementFrancais() && toRneFwkPers() == null) || (!isEtablissementFrancais() && toStructureEtabFwkPers() == null)) {
			throw new ValidationException("Vous devez sélectionner un établissement");
		}
		if ((isLaboratoireLocal() && toStructureLaboFwkpers() == null) || (!isLaboratoireLocal() && laboExterneDth() == null)) {
			throw new ValidationException("Vous devez sélectionner un laboratoire");
		}
		if (toStructureEdFwkpers() == null) {
			throw new ValidationException("Vous devez sélectionner une école doctorale");
		}
		// TODO Auto-generated method stub
		super.validateForSave();
	}

}
