/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOZoneGeographique.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOZoneGeographique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "ZoneGeographique";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "zgOrdre";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> ZG_CODE = new er.extensions.eof.ERXKey<java.lang.String>("zgCode");
	public static final er.extensions.eof.ERXKey<java.lang.String> ZG_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("zgLibelle");
	
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String ZG_CODE_KEY = ZG_CODE.key();
	public static final String ZG_LIBELLE_KEY = ZG_LIBELLE.key();

	// Attributs non visibles
	public static final String UTL_ORDRE_CREATION_KEY = "utlOrdreCreation";
	public static final String UTL_ORDRE_MODIFICATION_KEY = "utlOrdreModification";
	public static final String ZG_ORDRE_KEY = "zgOrdre";

	//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ZG_CODE_COLKEY = "ZG_CODE";
	public static final String ZG_LIBELLE_COLKEY = "ZG_LL";

	public static final String UTL_ORDRE_CREATION_COLKEY = "UTL_ORDRE_CREATION";
	public static final String UTL_ORDRE_MODIFICATION_COLKEY = "UTL_ORDRE_MODIFICATION";
	public static final String ZG_ORDRE_COLKEY = "ZG_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_CREATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurCreation");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_MODIFICATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurModification");
	
	public static final String UTILISATEUR_CREATION_KEY = UTILISATEUR_CREATION.key();
	public static final String UTILISATEUR_MODIFICATION_KEY = UTILISATEUR_MODIFICATION.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.String zgCode() {
		return (java.lang.String) storedValueForKey(ZG_CODE_KEY);
	}

	public void setZgCode(java.lang.String value) {
	    takeStoredValueForKey(value, ZG_CODE_KEY);
	}
	
	public java.lang.String zgLibelle() {
		return (java.lang.String) storedValueForKey(ZG_LIBELLE_KEY);
	}

	public void setZgLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, ZG_LIBELLE_KEY);
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation() {
	    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur) storedValueForKey(UTILISATEUR_CREATION_KEY);
	}

	public void setUtilisateurCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	    if (value == null) {
	    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurCreation();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_CREATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_CREATION_KEY);
	    }
	}
	
	public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurModification() {
	    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur) storedValueForKey(UTILISATEUR_MODIFICATION_KEY);
	}

	public void setUtilisateurModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
	    if (value == null) {
	    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurModification();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_MODIFICATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_MODIFICATION_KEY);
	    }
	}
	
	public static EOZoneGeographique createZoneGeographique(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.String zgCode, java.lang.String zgLibelle, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurModification) {
		EOZoneGeographique eo = (EOZoneGeographique) EOUtilities.createAndInsertInstance(editingContext, _EOZoneGeographique.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setZgCode(zgCode);
		eo.setZgLibelle(zgLibelle);
		eo.setUtilisateurCreationRelationship(utilisateurCreation);
		eo.setUtilisateurModificationRelationship(utilisateurModification);
		return eo;
	}

	public EOZoneGeographique localInstanceIn(EOEditingContext editingContext) {
		return (EOZoneGeographique) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOZoneGeographique creerInstance(EOEditingContext editingContext) {
		return (EOZoneGeographique) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOZoneGeographique.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOZoneGeographique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOZoneGeographique>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOZoneGeographique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOZoneGeographique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOZoneGeographique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOZoneGeographique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOZoneGeographique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOZoneGeographique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOZoneGeographique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOZoneGeographique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOZoneGeographique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOZoneGeographique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOZoneGeographique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOZoneGeographique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAllZoneGeographiques(EOEditingContext editingContext) {
		return _EOZoneGeographique.fetchAllZoneGeographiques(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchAllZoneGeographiques(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOZoneGeographique.fetchZoneGeographiques(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOZoneGeographique> fetchZoneGeographiques(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOZoneGeographique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOZoneGeographique>(_EOZoneGeographique.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOZoneGeographique> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOZoneGeographique fetchZoneGeographique(EOEditingContext editingContext, String keyName, Object value) {
		return _EOZoneGeographique.fetchZoneGeographique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOZoneGeographique fetchZoneGeographique(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOZoneGeographique> eoObjects = _EOZoneGeographique.fetchZoneGeographiques(editingContext, qualifier, null);
	    EOZoneGeographique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one ZoneGeographique that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
