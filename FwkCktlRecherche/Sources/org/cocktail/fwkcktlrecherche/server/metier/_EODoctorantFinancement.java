/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODoctorantFinancement.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODoctorantFinancement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DoctorantFinancement";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idFinancement";

	public static final er.extensions.eof.ERXKey<java.lang.String> COMMENTAIRE_SANS_FINANCEMENT = new er.extensions.eof.ERXKey<java.lang.String>("commentaireSansFinancement");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_DEBUT_FINANCEMENT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateDebutFinancement");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_FIN_FINANCEMENT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateFinFinancement");
	
	public static final String COMMENTAIRE_SANS_FINANCEMENT_KEY = COMMENTAIRE_SANS_FINANCEMENT.key();
	public static final String DATE_DEBUT_FINANCEMENT_KEY = DATE_DEBUT_FINANCEMENT.key();
	public static final String DATE_FIN_FINANCEMENT_KEY = DATE_FIN_FINANCEMENT.key();

	// Attributs non visibles
	public static final String AVT_ORDRE_KEY = "avtOrdre";
	public static final String CP_ORDRE_KEY = "cpOrdre";
	public static final String ID_FINANCEMENT_KEY = "idFinancement";
	public static final String ID_TTC_KEY = "idTtc";
	public static final String ID_TYPE_CONTRAT_DOCTORAL_KEY = "idTypeContratDoctoral";
	public static final String ID_TYPE_FINANCEMENT_KEY = "idTypeFinancement";
	public static final String RAS_ID_KEY = "rasId";

	//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_SANS_FINANCEMENT_COLKEY = "COMMENTAIRE_SANS_FINANCEMENT";
	public static final String DATE_DEBUT_FINANCEMENT_COLKEY = "DATE_DEBUT_FINANCEMENT";
	public static final String DATE_FIN_FINANCEMENT_COLKEY = "DATE_FIN_FINANCEMENT";

	public static final String AVT_ORDRE_COLKEY = "AVT_ORDRE";
	public static final String CP_ORDRE_COLKEY = "CP_ORDRE";
	public static final String ID_FINANCEMENT_COLKEY = "ID_FINANCEMENT";
	public static final String ID_TTC_COLKEY = "ID_TTC";
	public static final String ID_TYPE_CONTRAT_DOCTORAL_COLKEY = "ID_TYPE_CONTRAT_DOCTORAL";
	public static final String ID_TYPE_FINANCEMENT_COLKEY = "ID_TYPE_FINANCEMENT";
	public static final String RAS_ID_COLKEY = "RAS_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant> TO_AVENANT = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant>("toAvenant");
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> TO_CONTRAT_PARTENAIRE = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("toContratPartenaire");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> TO_FINANCEUR = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur>("toFinanceur");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation> TO_REPART_ASSOCIATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation>("toRepartAssociation");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat> TO_THESE_TYPE_CONTRAT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat>("toTheseTypeContrat");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral> TO_TYPE_CONTRAT_DOCTORAL = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral>("toTypeContratDoctoral");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement> TO_TYPE_FINANCEMENT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement>("toTypeFinancement");
	
	public static final String TO_AVENANT_KEY = TO_AVENANT.key();
	public static final String TO_CONTRAT_PARTENAIRE_KEY = TO_CONTRAT_PARTENAIRE.key();
	public static final String TO_FINANCEUR_KEY = TO_FINANCEUR.key();
	public static final String TO_REPART_ASSOCIATION_KEY = TO_REPART_ASSOCIATION.key();
	public static final String TO_THESE_TYPE_CONTRAT_KEY = TO_THESE_TYPE_CONTRAT.key();
	public static final String TO_TYPE_CONTRAT_DOCTORAL_KEY = TO_TYPE_CONTRAT_DOCTORAL.key();
	public static final String TO_TYPE_FINANCEMENT_KEY = TO_TYPE_FINANCEMENT.key();

	// Accessors methods
	public java.lang.String commentaireSansFinancement() {
		return (java.lang.String) storedValueForKey(COMMENTAIRE_SANS_FINANCEMENT_KEY);
	}

	public void setCommentaireSansFinancement(java.lang.String value) {
	    takeStoredValueForKey(value, COMMENTAIRE_SANS_FINANCEMENT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateDebutFinancement() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_DEBUT_FINANCEMENT_KEY);
	}

	public void setDateDebutFinancement(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_DEBUT_FINANCEMENT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateFinFinancement() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_FIN_FINANCEMENT_KEY);
	}

	public void setDateFinFinancement(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_FIN_FINANCEMENT_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.Avenant toAvenant() {
	    return (org.cocktail.cocowork.server.metier.convention.Avenant) storedValueForKey(TO_AVENANT_KEY);
	}

	public void setToAvenantRelationship(org.cocktail.cocowork.server.metier.convention.Avenant value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.Avenant oldValue = toAvenant();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_AVENANT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_AVENANT_KEY);
	    }
	}
	
	public org.cocktail.cocowork.server.metier.convention.ContratPartenaire toContratPartenaire() {
	    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire) storedValueForKey(TO_CONTRAT_PARTENAIRE_KEY);
	}

	public void setToContratPartenaireRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.ContratPartenaire oldValue = toContratPartenaire();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_PARTENAIRE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_PARTENAIRE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation toRepartAssociation() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation) storedValueForKey(TO_REPART_ASSOCIATION_KEY);
	}

	public void setToRepartAssociationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation oldValue = toRepartAssociation();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REPART_ASSOCIATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REPART_ASSOCIATION_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat toTheseTypeContrat() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat) storedValueForKey(TO_THESE_TYPE_CONTRAT_KEY);
	}

	public void setToTheseTypeContratRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat oldValue = toTheseTypeContrat();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_THESE_TYPE_CONTRAT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_THESE_TYPE_CONTRAT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral toTypeContratDoctoral() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral) storedValueForKey(TO_TYPE_CONTRAT_DOCTORAL_KEY);
	}

	public void setToTypeContratDoctoralRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral oldValue = toTypeContratDoctoral();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CONTRAT_DOCTORAL_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CONTRAT_DOCTORAL_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement toTypeFinancement() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement) storedValueForKey(TO_TYPE_FINANCEMENT_KEY);
	}

	public void setToTypeFinancementRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement oldValue = toTypeFinancement();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FINANCEMENT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FINANCEMENT_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> toFinanceur() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_FINANCEUR_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> toFinanceur(EOQualifier qualifier) {
		return toFinanceur(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> toFinanceur(EOQualifier qualifier, boolean fetch) {
	    return toFinanceur(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> toFinanceur(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur.TO_DOCTORANT_FINANCEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toFinanceur();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToFinanceurRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_FINANCEUR_KEY);
	}

	public void removeFromToFinanceurRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FINANCEUR_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur createToFinanceurRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DoctorantFinanceur");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_FINANCEUR_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur) eo;
	}

	public void deleteToFinanceurRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FINANCEUR_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToFinanceurRelationships() {
		java.util.Enumeration objects = toFinanceur().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToFinanceurRelationship((org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur) objects.nextElement());
	    }
	}

	public static EODoctorantFinancement createDoctorantFinancement(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.Avenant toAvenant, org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement toTypeFinancement) {
		EODoctorantFinancement eo = (EODoctorantFinancement) EOUtilities.createAndInsertInstance(editingContext, _EODoctorantFinancement.ENTITY_NAME);    
		eo.setToAvenantRelationship(toAvenant);
		eo.setToTypeFinancementRelationship(toTypeFinancement);
		return eo;
	}

	public EODoctorantFinancement localInstanceIn(EOEditingContext editingContext) {
		return (EODoctorantFinancement) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODoctorantFinancement creerInstance(EOEditingContext editingContext) {
		return (EODoctorantFinancement) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODoctorantFinancement.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantFinancement> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantFinancement>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODoctorantFinancement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODoctorantFinancement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODoctorantFinancement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODoctorantFinancement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODoctorantFinancement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODoctorantFinancement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODoctorantFinancement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODoctorantFinancement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODoctorantFinancement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODoctorantFinancement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODoctorantFinancement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODoctorantFinancement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAllDoctorantFinancements(EOEditingContext editingContext) {
		return _EODoctorantFinancement.fetchAllDoctorantFinancements(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchAllDoctorantFinancements(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODoctorantFinancement.fetchDoctorantFinancements(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODoctorantFinancement> fetchDoctorantFinancements(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODoctorantFinancement> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODoctorantFinancement>(_EODoctorantFinancement.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODoctorantFinancement> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODoctorantFinancement fetchDoctorantFinancement(EOEditingContext editingContext, String keyName, Object value) {
		return _EODoctorantFinancement.fetchDoctorantFinancement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODoctorantFinancement fetchDoctorantFinancement(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODoctorantFinancement> eoObjects = _EODoctorantFinancement.fetchDoctorantFinancements(editingContext, qualifier, null);
	    EODoctorantFinancement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DoctorantFinancement that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
