/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEnterpriseObjectCache;



public class EOGdStrategieSangria extends _EOGdStrategieSangria {

	private static final long serialVersionUID = 1L;
	private static ERXEnterpriseObjectCache<EOGdStrategieSangria> gdStrategieCache;
	public static final String STRATEGIE_CONSULTATION_DEMANDES_SUBVENTION_STRUCTURE_CODE = "ACCES_CONSULTATION_DEMANDES_SUBVENTION_STRUCTURE";
	public static final String STRATEGIE_CONSULTATION_TOUTES_DEMANDES_SUBVENTION_STRUCTURE_CODE = "ACCES_CONSULTATION_TOUTES_DEMANDES_SUBVENTION_STRUCTURE";
	public static final String STRATEGIE_CONSULTATION_CONTRATS_STRUCTURE_CODE = "ACCES_CONSULTATION_CONTRATS_STRUCTURE";
	public static final String STRATEGIE_CONSULTATION_TOUS_CONTRATS_STRUCTURE_CODE = "ACCES_CONSULTATION_TOUS_CONTRATS_STRUCTURE";
	
	/**
     * @return le cache des {@link EOGdStrategie} par leur code
     */
    public static ERXEnterpriseObjectCache<EOGdStrategieSangria> getGdStrategieCache() {
    	if (gdStrategieCache == null) {
    		gdStrategieCache = new ERXEnterpriseObjectCache<EOGdStrategieSangria>(EOGdStrategieSangria.class, CODE_KEY);
    	}
    	return gdStrategieCache;
    }
    
    /**
     * @param gdStrategieCache le cache a setter
     */
    public static void setGdStrategieCache(ERXEnterpriseObjectCache<EOGdStrategieSangria> gdStrategieCache) {
    	EOGdStrategieSangria.gdStrategieCache = gdStrategieCache;
    }
    
    /**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_CONSULTATION_DEMANDES_SUBVENTION_STRUCTURE"
   	 */
   	public static EOGdStrategieSangria strategieAccesConsultationDemandesSubventionStructure(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_CONSULTATION_DEMANDES_SUBVENTION_STRUCTURE_CODE);
   	}
    
    /**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_CONSULTATION_TOUTES_DEMANDES_SUBVENTION_STRUCTURE"
   	 */
   	public static EOGdStrategieSangria strategieAccesConsultationToutesDemandesSubventionStructure(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_CONSULTATION_TOUTES_DEMANDES_SUBVENTION_STRUCTURE_CODE);
   	}
    
    /**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_CONSULTATION_CONTRATS_STRUCTURE"
   	 */
   	public static EOGdStrategieSangria strategieAccesConsultationContratsStructure(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_CONSULTATION_CONTRATS_STRUCTURE_CODE);
   	}
    
    /**
   	 * @param edc l'editingContext
   	 * @return le {@link EOGdStrategie} "ACCES_CONSULTATION_TOUS_CONTRATS_STRUCTURE"
   	 */
   	public static EOGdStrategieSangria strategieAccesConsultationTousContratsStructure(EOEditingContext edc) {
   		return getGdStrategieCache().objectForKey(edc, STRATEGIE_CONSULTATION_TOUS_CONTRATS_STRUCTURE_CODE);
   	}
	
	@Override
	public String toString() {
		return this.libelle();
	}

}
