/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartMotCleUniteRecherche.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartMotCleUniteRecherche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartMotCleUniteRecherche";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	

	// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String ID_KEY = "id";
	public static final String MOT_CLE_ID_KEY = "motCleId";

	//Colonnes dans la base de donnees

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ID_COLKEY = "MCUR_ID";
	public static final String MOT_CLE_ID_COLKEY = "MC_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOMotCle> MOT_CLE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOMotCle>("motCle");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> UNITE_RECHERCHE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("uniteRecherche");
	
	public static final String MOT_CLE_KEY = MOT_CLE.key();
	public static final String UNITE_RECHERCHE_KEY = UNITE_RECHERCHE.key();

	// Accessors methods
	public org.cocktail.fwkcktlrecherche.server.metier.EOMotCle motCle() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOMotCle) storedValueForKey(MOT_CLE_KEY);
	}

	public void setMotCleRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOMotCle value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOMotCle oldValue = motCle();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOT_CLE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, MOT_CLE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure uniteRecherche() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(UNITE_RECHERCHE_KEY);
	}

	public void setUniteRechercheRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = uniteRecherche();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UNITE_RECHERCHE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, UNITE_RECHERCHE_KEY);
	    }
	}
	
	public static EORepartMotCleUniteRecherche createRepartMotCleUniteRecherche(EOEditingContext editingContext, org.cocktail.fwkcktlrecherche.server.metier.EOMotCle motCle, org.cocktail.fwkcktlpersonne.common.metier.EOStructure uniteRecherche) {
		EORepartMotCleUniteRecherche eo = (EORepartMotCleUniteRecherche) EOUtilities.createAndInsertInstance(editingContext, _EORepartMotCleUniteRecherche.ENTITY_NAME);    
		eo.setMotCleRelationship(motCle);
		eo.setUniteRechercheRelationship(uniteRecherche);
		return eo;
	}

	public EORepartMotCleUniteRecherche localInstanceIn(EOEditingContext editingContext) {
		return (EORepartMotCleUniteRecherche) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartMotCleUniteRecherche creerInstance(EOEditingContext editingContext) {
		return (EORepartMotCleUniteRecherche) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartMotCleUniteRecherche.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartMotCleUniteRecherche> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartMotCleUniteRecherche>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartMotCleUniteRecherche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartMotCleUniteRecherche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartMotCleUniteRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartMotCleUniteRecherche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartMotCleUniteRecherche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartMotCleUniteRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartMotCleUniteRecherche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartMotCleUniteRecherche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartMotCleUniteRecherche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartMotCleUniteRecherche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAllRepartMotCleUniteRecherches(EOEditingContext editingContext) {
		return _EORepartMotCleUniteRecherche.fetchAllRepartMotCleUniteRecherches(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchAllRepartMotCleUniteRecherches(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartMotCleUniteRecherche.fetchRepartMotCleUniteRecherches(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> fetchRepartMotCleUniteRecherches(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartMotCleUniteRecherche> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartMotCleUniteRecherche>(_EORepartMotCleUniteRecherche.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartMotCleUniteRecherche fetchRepartMotCleUniteRecherche(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartMotCleUniteRecherche.fetchRepartMotCleUniteRecherche(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartMotCleUniteRecherche fetchRepartMotCleUniteRecherche(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartMotCleUniteRecherche> eoObjects = _EORepartMotCleUniteRecherche.fetchRepartMotCleUniteRecherches(editingContext, qualifier, null);
	    EORepartMotCleUniteRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartMotCleUniteRecherche that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
