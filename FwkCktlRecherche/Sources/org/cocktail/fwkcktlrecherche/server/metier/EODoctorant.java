/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;


public class EODoctorant extends _EODoctorant {
	private static Logger log = Logger.getLogger(EODoctorant.class);

	public String civiliteNomPrenom() {
		EOIndividu ind = toIndividuFwkpers();
		return ind.toCivilite().cCivilite() + " " + ind.nomUsuel() + " " + ind.prenom();
	}

	/*
	 * si le doctorant n'est pas etudiant, on vérifie qu'il n'a pas été inscrit
	 * depuis la derniere consultation de son dossier. Si c'est le cas, on crée
	 * le lien avec l'étudiant et on retourne TRUE
	 */
	public Boolean lienDoctorantEtudiant() {
		if (toEtudiantFwkpers() == null) {
			EOQualifier qualEtud = ERXQ.equals(EOEtudiant.TO_INDIVIDU_KEY, toIndividuFwkpers());
			EOEtudiant unEtudiant = EOEtudiant.fetchByKeyValue(editingContext(), EOEtudiant.TO_INDIVIDU_KEY, toIndividuFwkpers());
			if (unEtudiant != null) {
				setToEtudiantFwkpersRelationship(unEtudiant);
				return true;
			}
		}
		return false;
	}

}
