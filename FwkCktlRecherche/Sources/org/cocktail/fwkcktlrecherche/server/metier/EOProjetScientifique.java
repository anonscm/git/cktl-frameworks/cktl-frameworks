/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;


public class EOProjetScientifique extends _EOProjetScientifique {

	private static Logger log = Logger.getLogger(EOProjetScientifique.class);
	  
	//LES FACTORIES
	private FactoryContratPartenaire factoryContratPartenaire;
	
	private FactoryContratPartenaire getFactoryContratPartenaire() {
		if(factoryContratPartenaire == null) {
			factoryContratPartenaire = new FactoryContratPartenaire(this.editingContext(), false);
		}
		return factoryContratPartenaire;
	}
	
    private ContratUtilities contratUtilities = null;
    
    private ContratUtilities contratUtilities() {
    	if(contratUtilities == null) {
    		contratUtilities = ContratUtilities.creerNouvelleInstance(editingContext());
    	}
    	return contratUtilities;
    }
	

	private FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}

  
  public Contrat contratProjet() {
	  NSArray<Contrat> contrats = (NSArray<Contrat>) projet().projetContrats().valueForKey("contrat");
	  return ERXQ.filtered(contrats, ERXQ.equals("typeClassificationContrat", TypeClassificationContrat.typeClassificationContratForCode(editingContext(), "PROJ_SCIEN"))).get(0);
	}
	

	// INDEX DU PROJET 
  public static final String INDEX_KEY = "index";

	public Number index() {
		return contratProjet().conIndex();
	}
	
	// EXERCICE 
	
	public void setExercice(EOExerciceCocktail value) {
		projet().setExerciceCocktailRelationship(value);
		contratProjet().setExerciceCocktailRelationship(value);
	}
	
	// Intitulé du projet
	public static final String INTITULE_KEY = "intitule";

	public String intitule() {
		return contratProjet().conObjet();
	}
	
	public void setIntitule(String intitule) {
		projet().setPjtLibelle(intitule);
		contratProjet().setConObjet(intitule);
	}
	
	// OBJET COURT 
	
	public String libelleCourt() {
		return contratProjet().conObjetCourt();
	}
	
	public String libelle() {
		return contratProjet().conObjet();
	}
	
	public void setLibelleCourt(String libelleCourt) {
		contratProjet().setConObjetCourt(libelleCourt);
	}
	
	// date de debut
	
	public NSTimestamp dateDebut() {
		return contratProjet().avenantZero().avtDateDeb();
	}
	
	public void setDateDebut(NSTimestamp dateDebut) {
		try {
			contratProjet().avenantZero().setAvtDateDeb(dateDebut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// date du fin
	
	public NSTimestamp dateFin() {
		return contratProjet().avenantZero().avtDateFin();
	}

	public void setDateFin(NSTimestamp dateFin) {
		try {
			contratProjet().avenantZero().setAvtDateFin(dateFin);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public NSArray<EOAap> aapsDuProjet() {
		NSArray<Contrat> contratsAap = 
				ERXQ.filtered((NSArray<Contrat>) projet().projetContrats().valueForKey("contrat"),
				Contrat.TYPE_CLASSIFICATION_CONTRAT.dot(TypeClassificationContrat.TCC_CODE).eq(TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_AAP)
			);
		if(contratsAap.isEmpty()) {
			return new NSArray<EOAap>();
		} 
		else {
			EOQualifier qualifier = EOContratRecherche.CONTRAT.in(contratsAap);
			ERXSortOrderings sortOrderings = EOAap.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL).dot(EOExerciceCocktail.EXE_ORDRE_KEY).asc().then(EOAap.CONTRAT.dot(Contrat.CON_INDEX).asc());
			return EOAap.fetchAll(editingContext(), qualifier, sortOrderings);
		}

	}
	
	public NSArray<EOContratRecherche> contratsRecherche() {
		NSArray<Contrat> contratsContratsRecherche = 
				ERXQ.filtered((NSArray<Contrat>) projet().projetContrats().valueForKey("contrat"),
				Contrat.TYPE_CLASSIFICATION_CONTRAT.dot(TypeClassificationContrat.TCC_CODE).eq(TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_CONV)
			);
		if(contratsContratsRecherche.isEmpty()) {
			return new NSArray<EOContratRecherche>();
		} 
		else {
			EOQualifier qualifier = EOContratRecherche.CONTRAT.in(contratsContratsRecherche);
			ERXSortOrderings sortOrderings = EOContratRecherche.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL).dot(EOExerciceCocktail.EXE_ORDRE_KEY).asc().then(EOContratRecherche.CONTRAT.dot(Contrat.CON_INDEX).asc());
			return EOContratRecherche.fetchAll(editingContext(), qualifier, sortOrderings);
		}
	}
	
	// LABORATOIRES CONCERNES
	
	public void ajouterStructureRechercheConcernee(EOStructure structureRecherche) throws Exception {
	      contratUtilities().ajouterStructureRechercheConcerneePourContrat(structureRecherche, contratProjet());		
	}
	
	public void supprimerStructureRechercheConcernee(EOStructure structureRecherche, Integer persId) throws Exception {
	      contratUtilities().supprimerStructureRechercheConcerneePourContrat(structureRecherche, contratProjet(), persId);
	}
	
	public NSArray<EOStructure> getStructuresRechercheConcernees() {
    	return contratUtilities().getStructuresRecherchesConcerneesPourContrat(contratProjet());
	}

	
	// RESPONSABLES SCIENTIFIQUES
	
	public void ajouterResponsableScientifiquePourStructureRecherche(EOIndividu responsable,  EOStructure structureRecherche) throws Exception {
    	contratUtilities().ajouterResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contratProjet());
	}

	
	public void supprimerResponsableScientifiquePourStructureRecherche(EOIndividu responsable, EOStructure structureRecherche, Integer persId) throws Exception {
    	contratUtilities().supprimerResponsableScientifiquePourStructureRechercheEtContrat(responsable, structureRecherche, contratProjet(), persId);
	}
		
	public NSArray<EOIndividu> getResponsablesScientifiques() {	
		return contratUtilities().getResponsablesScientifiquePourContrat(contratProjet());
	}
	
	public NSArray<EOIndividu> getResponsablesScientifiquesForStructureRecherche(EOStructure structureRecherche) {
    	return contratUtilities().responsablesScientifiquesPourStructureRechercheEtContrat(structureRecherche, contratProjet());
	}
	
	public EOIndividu getPorteurProjet() {
		EOAssociation role = FactoryAssociation.shared().porteurAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contratProjet());
	}

	public void setPorteurProjet(EOIndividu nouveauPorteur, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().porteurAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauPorteur, role, contratProjet(), utilisateurPersId);
	}
	
	public EOIndividu getPorteurProjetAdjoint() {
		EOAssociation role = FactoryAssociation.shared().porteurProjetAdjointAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contratProjet());
	}

	public void setPorteurProjetAdjoint(EOIndividu nouveauPorteur, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().porteurProjetAdjointAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauPorteur, role, contratProjet(), utilisateurPersId);	
	}
	
	public EOIndividu getCoordinateurProjet() {
		EOAssociation role = FactoryAssociation.shared().coordinateurProjetAssociation(editingContext());
		return (EOIndividu) contratUtilities().personnePourRoleDansGroupePartenaireDeContrat(role, contratProjet());
	}

	public void setCoordinateurProjet(EOIndividu nouveauCoordinateur, Integer utilisateurPersId) {
		EOAssociation role = FactoryAssociation.shared().coordinateurProjetAssociation(editingContext());
		contratUtilities().setPersonneAvecRoleDansGroupePartenaireDeContrat(nouveauCoordinateur, role, contratProjet(), utilisateurPersId);	
	}
	

}
