/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;


public class EODirecteurThese extends _EODirecteurThese {
	
    private static final long serialVersionUID = -6919176668438530265L;

	public static final String M_TO_INDIVIDU_DIRECTEUR = "toIndividuDirecteur";
	public static final String TO_REPART_ASSOCIATION = "repartAssociationDirecteur";
	public static final String M_LIBELLE_LABORATOIRE = "libelleLaboratoire";
	public static final String ROLE = "libelleRoleDirecteur";
	public static final String ASSOCIATION = "roleDirecteur";
	public static final String IS_COTUTELLE = "isDirecteurCotutelle";
	public static final String LIBELLE_ETABLISSEMENT = "libelleEtablissement";

	public final static EOSortOrdering SORT_NOM_DIRECTEUR_ASC = EOSortOrdering.sortOrderingWithKey(EODirecteurThese.M_TO_INDIVIDU_DIRECTEUR + ".getNomAndPrenom",
			EOSortOrdering.CompareAscending);


	public EOIndividu toIndividuDirecteur() {
		EOIndividu ind = null;
		try {
			ind = (EOIndividu) toContratPartenaireCw().partenaire();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ind;
	}

	public String libelleLaboratoire() {
		if (libelleLabo() != null) {
			return libelleLabo();
		} else {
			EOStructure lab = toStructureLaboFwkpers();
			if (lab != null) {
				return lab.llStructure();
			} else {
				return null;
			}
		}
	}

	public String libelleEtablissement() {
		if (toStructureEtabFwkPers() != null) {
			return toStructureEtabFwkPers().llStructure();
		} else {
			EORne etab = toRneFwkpers();
			if (etab != null) {
				return etab.llRne();
			} else {
				return null;
			}
		}
	}

	public NSArray<EOAssociation> rolesTypeDirecteur() {
		NSArray<EOAssociation> roles = null;
		EOAssociation associationContact = FactoryAssociation.shared().directionTheseAssociation(editingContext());
		if (associationContact != null) {
			roles = associationContact.getFils(editingContext());
		}
		return roles;
	}

	public EOAssociation roleDirecteur() {

		// on recupere le libelle du role de la personne
		EOQualifier qualAssociation = ERXQ.and(
				ERXQ.equals(EORepartAssociation.PERS_ID_KEY, toContratPartenaireCw().valueForKey(EOContratPartenaire.PERS_ID_KEY)),
				ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, toContratPartenaireCw().contrat().groupePartenaire().valueForKey(EORepartAssociation.C_STRUCTURE_KEY)),
				ERXQ.in(EORepartAssociation.TO_ASSOCIATION_KEY,
						FactoryAssociation.shared().associationsFilleDe(FactoryAssociation.shared().directionTheseAssociation(editingContext()), editingContext())));
		NSArray<EORepartAssociation> lesRepartAssoc = EORepartAssociation.fetchAll(editingContext(), qualAssociation);

		return lesRepartAssoc.isEmpty() ? null : lesRepartAssoc.lastObject().toAssociation();
	}

	public String libelleRoleDirecteur() {

		EOAssociation leRole = roleDirecteur();
		if (leRole != null)
			return leRole.assLibelle();
		else
			return null;
	}

	public EORepartStructure repartStructureDirecteur() {

		EOQualifier qualStructure = ERXQ.and(ERXQ.equals(EORepartStructure.PERS_ID_KEY, toContratPartenaireCw().valueForKey(EOContratPartenaire.PERS_ID_KEY)),
				ERXQ.equals(EORepartStructure.C_STRUCTURE_KEY, toContratPartenaireCw().contrat().groupePartenaire().valueForKey(EORepartAssociation.C_STRUCTURE_KEY)));
		NSArray<EORepartStructure> lesRepartStructures = EORepartStructure.fetchAll(editingContext(), qualStructure);
		return lesRepartStructures.lastObject();
	}

	public EORepartAssociation repartAssociationDirecteur() {

		if (toContratPartenaireCw() == null) {
			return null;
		}
		
		EOQualifier qualAssociation = ERXQ.and(
				ERXQ.equals(EORepartAssociation.PERS_ID_KEY, toContratPartenaireCw().valueForKey(EOContratPartenaire.PERS_ID_KEY)),
				ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, toContratPartenaireCw().contrat().groupePartenaire().valueForKey(EORepartAssociation.C_STRUCTURE_KEY)),
				ERXQ.in(EORepartAssociation.TO_ASSOCIATION_KEY,
						FactoryAssociation.shared().associationsFilleDe(FactoryAssociation.shared().directionTheseAssociation(editingContext()), editingContext())));
		NSArray<EORepartAssociation> lesRepartAssoc = EORepartAssociation.fetchAll(editingContext(), qualAssociation);

		return lesRepartAssoc.lastObject();
	}

	// si l'etablissement du directeur est un partenaire avec le role
	// d'etablissement de cotutelle alors il y a cotutelle
	public Boolean isDirecteurCotutelle() {
		if (toStructureEtabFwkPers() != null) {
			EOQualifier qualAssociation = ERXQ.and(ERXQ.equals(EORepartAssociation.PERS_ID_KEY, toStructureEtabFwkPers().valueForKey(EOStructure.PERS_ID_KEY)),
					ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, toContratPartenaireCw().contrat().groupePartenaire().valueForKey(EORepartAssociation.C_STRUCTURE_KEY)),
					ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, FactoryAssociation.shared().etablissementCotutelleAssociation(editingContext())));
			NSArray<EORepartAssociation> lesRepartAssoc = EORepartAssociation.fetchAll(editingContext(), qualAssociation);
			if (lesRepartAssoc.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public void validateForInsert() throws ValidationException {

		super.validateForInsert();
	}

	public void validateForSave() throws ValidationException {

		super.validateForSave();
	}
}
