package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;

/**
 * Vérifie qu'il y au moins un rapporteur de thèse
 * @author jlafourc
 *
 */
public class ControleRapporteursThese implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		if (doctorantThese.rapporteurs().isEmpty()) {
			return new ResultatControle(false, "Il n'y a aucun rapporteur sur la these");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}

}
