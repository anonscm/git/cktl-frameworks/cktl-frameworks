package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;


/**
 * Controle du lieu de soutenance
 */
public class ControleLieuDeSoutenance implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		if (doctorantThese.lieuSoutenance() == null) {
			return new ResultatControle(false, "Le lieu de la soutenance n'est pas renseigné");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}

}
