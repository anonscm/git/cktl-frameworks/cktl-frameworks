package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Controle si on un adresse correcte pour le doctorant
 * @author jlafourc
 */
public class ControleAdresseDoctorant implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		NSArray<EORepartPersonneAdresse> adresses = doctorantThese.toDoctorant().toIndividuFwkpers().toRepartPersonneAdresses();
		
		EOQualifier qualifierTypeAdresse = EOGrhumParametres.PARAM_KEY.eq("org.cocktail.physalis.TYPE_ADRESSE_DOCTORANT");
		
		EOGrhumParametres typeAdresse = EOGrhumParametres.fetchFirstByQualifier(doctorantThese.editingContext(), qualifierTypeAdresse);
		
		for (EORepartPersonneAdresse adresse : adresses) {
			if (adresse.estAdressePrincipale() || adresse.tadrCode().equals(typeAdresse.paramValue())) {
				return ResultatControle.RESULTAT_OK;
			}
		}
		
		return new ResultatControle(false, "Le doctorant n'a pas d'adresse principale ou d'adresse du type qui est renseigné dans les paramètres de l'application.");
	}

}
