package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

/**
 * Controle si le doctorant est en cotutelle
 */
public class ControleCotutelle implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		if (doctorantThese.isCotutelle()) {
			return ResultatControle.RESULTAT_OK;
		}
		
		return new ResultatControle(false, "Le doctorant n'a pas de cotutelle.");
	}

}
