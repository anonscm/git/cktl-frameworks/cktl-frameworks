package org.cocktail.fwkcktlrecherche.server.metier.controles.aap;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

/**
 * 
 * Interface à implémenter pour un controle sur un AAP
 * 
 */
public interface ControleAap {

	/**
	 * Effectue le controle
	 * 
	 * @param aap l'aap à controler
	 * @return Renvoie le résultat du controle
	 */
	ResultatControle check(EOAap aap);

}
