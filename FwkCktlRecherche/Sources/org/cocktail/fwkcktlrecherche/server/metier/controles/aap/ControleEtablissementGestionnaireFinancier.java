package org.cocktail.fwkcktlrecherche.server.metier.controles.aap;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

/**
 * 
 * Controle que l'établissment gestionnaire est bien renseigné
 * 
 * @author jlafourc
 *
 */
public class ControleEtablissementGestionnaireFinancier implements ControleAap {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EOAap aap) {
		if (aap.getEtablisssementGestionnaireFinancier() != null) {
			return ResultatControle.RESULTAT_OK;
		} else {
			return new ResultatControle(false, "L'établissement gestionnaire n'est pas renseigné");
		}
	}

}
