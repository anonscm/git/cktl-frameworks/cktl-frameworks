package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

/**
 * Controle si le diplome a un code sise
 */
public class ControleSise implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		if (doctorantThese.codeSiseDiplome() != null) {
			return ResultatControle.RESULTAT_OK;
		}
		
		return new ResultatControle(false, "Le diplôme n'a pas de code sise.");
	}

}
