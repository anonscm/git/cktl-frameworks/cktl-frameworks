package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;


/**
 * 
 * Interface à implémenter pour un controle avant 
 * l'impression d'un Document
 *
 */
public interface ControleDoctorantThese {
	
	/**
	 * Effectue le controle
	 * @param doctorantThese la these à controler
	 * @return Renvoie le résultat du controle
	 */
	ResultatControle check(EODoctorantThese doctorantThese);

}
