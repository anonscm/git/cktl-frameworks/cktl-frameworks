package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Controle que la these à un directeur
 * @author jlafourc
 *
 */
public class ControleDirecteurThese implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		EOQualifier qualifier = EODirecteurThese.TO_CONTRAT_PARTENAIRE_CW.in(
				doctorantThese.toContrat().partenairesForAssociation(
					FactoryAssociation.shared().directeurTheseAssociation(doctorantThese.editingContext()))
				);
		if (doctorantThese.toDirecteursThese(qualifier).isEmpty()) {
			return new ResultatControle(false, "La thèse n'a pas de directeur");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}

}
