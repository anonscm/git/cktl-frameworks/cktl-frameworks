package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;

/**
 * Controle de la date de soutenance de la thèse
 */
public class ControleDateDeSoutenance implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		if (doctorantThese.dateSoutenance() == null) {
			return new ResultatControle(false, "La date de soutenance n'est pas renseignée");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}

}
