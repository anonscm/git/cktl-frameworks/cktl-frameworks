package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

/**
 * Controle si le doctorant est en cotutelle
 */
public class ControleMention implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		if (doctorantThese.toTheseMention() != null) {
			return ResultatControle.RESULTAT_OK;
		}
		
		return new ResultatControle(false, "La thèse n'a pas de mention.");
	}

}
