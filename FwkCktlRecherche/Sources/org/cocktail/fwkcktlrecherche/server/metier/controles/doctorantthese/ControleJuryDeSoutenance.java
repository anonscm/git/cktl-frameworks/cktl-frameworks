package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;


/**
 * Controle si le jury de soutance n'est pas vide
 */
public class ControleJuryDeSoutenance implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		if (doctorantThese.jures().isEmpty()) {
			return new ResultatControle(false, "Le jury de soutenance ne contient aucun juré");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}

}
