package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.EditionDocumentsService;


/**
 * Controle si l'annee d'inscription est bonne pour une inscription sans dérogation
 */
public class ControleAnneeInscription implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		if (EditionDocumentsService.anneeInscriptionKo(doctorantThese)) {
			return new ResultatControle(false, "La thèse dépasse " + EditionDocumentsService.NOMBRE_ANNEES_THESE + " ans.");
		} else {
			return ResultatControle.RESULTAT_OK;
		}
	}
	
}
