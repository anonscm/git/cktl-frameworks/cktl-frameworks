package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Controle si le doctorant a les infos necessaires sur la cotutelle
 */
public class ControleInformationsCotutelle implements ControleDoctorantThese {

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		EOQualifier qual = ERXQ.equals(EOAssociation.ASS_CODE_KEY,  FactoryAssociation.shared().etablissementCotutelleAssociation(doctorantThese.editingContext()).assCode());
		NSArray<EOAssociation> lesAssociations = EOAssociation.fetchAll(doctorantThese.editingContext(), qual);
		NSArray<EORepartAssociation> lesRa = doctorantThese.toContrat().repartAssociationPartenairesForAssociations(lesAssociations);
		
		if (lesRa.size() == 0) {
			return new ResultatControle(false, "Le doctorant n'a pas d'établissement de cotutelle.");
		}
		
		EOQualifier qualCP = ERXQ.and(
				ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, lesRa.get(0).persId()),
				ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, doctorantThese.toContrat())
			);
		ContratPartenaire cp = ContratPartenaire.fetch(doctorantThese.editingContext(), qualCP);
		
		if (cp == null || cp.cpDateSignature() == null) {
			return new ResultatControle(false, "La cotutelle n'a pas de signature.");
		}
		
		return ResultatControle.RESULTAT_OK;
		
	}

}
