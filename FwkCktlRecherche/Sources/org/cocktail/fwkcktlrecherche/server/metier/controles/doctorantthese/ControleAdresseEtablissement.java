package org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Controle si l'on a une adresse pour l'etablissement
 */
public class ControleAdresseEtablissement implements ControleDoctorantThese {

	private static final String TYPE_ADRESSE_ETABLISSEMENT = "PRO";
	private static final String MESSAGE_ERREUR_ADRESSE = "L'établissement n'a pas d'adresse professionelle dans le référentiel.";

	/**
	 * {@inheritDoc}
	 */
	public ResultatControle check(EODoctorantThese doctorantThese) {
		
		
		EOQualifier qualifierCStructureEtablissement = EOGrhumParametres.PARAM_KEY.eq("org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE");
		EOGrhumParametres cStructureEtablissement = EOGrhumParametres.fetchFirstByQualifier(doctorantThese.editingContext(), qualifierCStructureEtablissement);
		
		if (cStructureEtablissement.paramValue() == null) {
			return new ResultatControle(false, "Le paramètre 'org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE' n'est pas renseigné.");
		}
		
		EOStructure structureEtablissement = EOStructure.fetchByQualifier(doctorantThese.editingContext(), EOStructure.C_STRUCTURE.eq(cStructureEtablissement.paramValue()));
		
		NSArray<EORepartPersonneAdresse> adresses = structureEtablissement.toRepartPersonneAdresses();
		
		for (EORepartPersonneAdresse adresse : adresses) {
			if (TYPE_ADRESSE_ETABLISSEMENT.equals(adresse.tadrCode())) {
				return ResultatControle.RESULTAT_OK;
			}
		}
		
		return new ResultatControle(false, MESSAGE_ERREUR_ADRESSE);
	}

}
