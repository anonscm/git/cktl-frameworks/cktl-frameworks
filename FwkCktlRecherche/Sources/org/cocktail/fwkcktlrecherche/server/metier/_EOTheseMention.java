/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTheseMention.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOTheseMention extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TheseMention";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idMention";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIB_MENTION = new er.extensions.eof.ERXKey<java.lang.String>("libMention");
	public static final er.extensions.eof.ERXKey<java.lang.String> TEM_SUCCES = new er.extensions.eof.ERXKey<java.lang.String>("temSucces");
	
	public static final String LIB_MENTION_KEY = LIB_MENTION.key();
	public static final String TEM_SUCCES_KEY = TEM_SUCCES.key();

	// Attributs non visibles
	public static final String ID_MENTION_KEY = "idMention";

	//Colonnes dans la base de donnees
	public static final String LIB_MENTION_COLKEY = "LIB_MENTION";
	public static final String TEM_SUCCES_COLKEY = "TEM_SUCCES";

	public static final String ID_MENTION_COLKEY = "ID_MENTION";

	// Relationships
	

	// Accessors methods
	public java.lang.String libMention() {
		return (java.lang.String) storedValueForKey(LIB_MENTION_KEY);
	}

	public void setLibMention(java.lang.String value) {
	    takeStoredValueForKey(value, LIB_MENTION_KEY);
	}
	
	public java.lang.String temSucces() {
		return (java.lang.String) storedValueForKey(TEM_SUCCES_KEY);
	}

	public void setTemSucces(java.lang.String value) {
	    takeStoredValueForKey(value, TEM_SUCCES_KEY);
	}
	
	public static EOTheseMention createTheseMention(EOEditingContext editingContext, java.lang.String libMention, java.lang.String temSucces) {
		EOTheseMention eo = (EOTheseMention) EOUtilities.createAndInsertInstance(editingContext, _EOTheseMention.ENTITY_NAME);    
		eo.setLibMention(libMention);
		eo.setTemSucces(temSucces);
		return eo;
	}

	public EOTheseMention localInstanceIn(EOEditingContext editingContext) {
		return (EOTheseMention) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOTheseMention creerInstance(EOEditingContext editingContext) {
		return (EOTheseMention) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOTheseMention.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOTheseMention> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseMention>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOTheseMention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTheseMention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOTheseMention> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTheseMention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOTheseMention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOTheseMention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOTheseMention> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTheseMention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOTheseMention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOTheseMention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOTheseMention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOTheseMention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAllTheseMentions(EOEditingContext editingContext) {
		return _EOTheseMention.fetchAllTheseMentions(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchAllTheseMentions(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOTheseMention.fetchTheseMentions(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOTheseMention> fetchTheseMentions(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOTheseMention> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseMention>(_EOTheseMention.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOTheseMention> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOTheseMention fetchTheseMention(EOEditingContext editingContext, String keyName, Object value) {
		return _EOTheseMention.fetchTheseMention(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOTheseMention fetchTheseMention(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOTheseMention> eoObjects = _EOTheseMention.fetchTheseMentions(editingContext, qualifier, null);
	    EOTheseMention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one TheseMention that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
