/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTheseTypeContrat.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOTheseTypeContrat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TheseTypeContrat";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idTtc";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIB_TYPE_CONTRAT = new er.extensions.eof.ERXKey<java.lang.String>("libTypeContrat");
	public static final er.extensions.eof.ERXKey<java.lang.String> TEM_VALIDE = new er.extensions.eof.ERXKey<java.lang.String>("temValide");
	
	public static final String LIB_TYPE_CONTRAT_KEY = LIB_TYPE_CONTRAT.key();
	public static final String TEM_VALIDE_KEY = TEM_VALIDE.key();

	// Attributs non visibles
	public static final String ID_TTC_KEY = "idTtc";

	//Colonnes dans la base de donnees
	public static final String LIB_TYPE_CONTRAT_COLKEY = "LIB_TYPE_CONTRAT";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String ID_TTC_COLKEY = "ID_TTC";

	// Relationships
	

	// Accessors methods
	public java.lang.String libTypeContrat() {
		return (java.lang.String) storedValueForKey(LIB_TYPE_CONTRAT_KEY);
	}

	public void setLibTypeContrat(java.lang.String value) {
	    takeStoredValueForKey(value, LIB_TYPE_CONTRAT_KEY);
	}
	
	public java.lang.String temValide() {
		return (java.lang.String) storedValueForKey(TEM_VALIDE_KEY);
	}

	public void setTemValide(java.lang.String value) {
	    takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}
	
	public static EOTheseTypeContrat createTheseTypeContrat(EOEditingContext editingContext, java.lang.String libTypeContrat, java.lang.String temValide) {
		EOTheseTypeContrat eo = (EOTheseTypeContrat) EOUtilities.createAndInsertInstance(editingContext, _EOTheseTypeContrat.ENTITY_NAME);    
		eo.setLibTypeContrat(libTypeContrat);
		eo.setTemValide(temValide);
		return eo;
	}

	public EOTheseTypeContrat localInstanceIn(EOEditingContext editingContext) {
		return (EOTheseTypeContrat) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOTheseTypeContrat creerInstance(EOEditingContext editingContext) {
		return (EOTheseTypeContrat) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOTheseTypeContrat.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOTheseTypeContrat> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseTypeContrat>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOTheseTypeContrat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTheseTypeContrat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOTheseTypeContrat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTheseTypeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOTheseTypeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOTheseTypeContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOTheseTypeContrat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTheseTypeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOTheseTypeContrat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOTheseTypeContrat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOTheseTypeContrat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOTheseTypeContrat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAllTheseTypeContrats(EOEditingContext editingContext) {
		return _EOTheseTypeContrat.fetchAllTheseTypeContrats(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchAllTheseTypeContrats(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOTheseTypeContrat.fetchTheseTypeContrats(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOTheseTypeContrat> fetchTheseTypeContrats(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOTheseTypeContrat> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseTypeContrat>(_EOTheseTypeContrat.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOTheseTypeContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOTheseTypeContrat fetchTheseTypeContrat(EOEditingContext editingContext, String keyName, Object value) {
		return _EOTheseTypeContrat.fetchTheseTypeContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOTheseTypeContrat fetchTheseTypeContrat(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOTheseTypeContrat> eoObjects = _EOTheseTypeContrat.fetchTheseTypeContrats(editingContext, qualifier, null);
	    EOTheseTypeContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one TheseTypeContrat that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
