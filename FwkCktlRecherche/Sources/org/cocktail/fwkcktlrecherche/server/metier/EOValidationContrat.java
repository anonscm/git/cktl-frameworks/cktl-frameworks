package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.foundation.NSTimestamp;


/**
 * Validation des contrats de recherche
 */
public class EOValidationContrat extends _EOValidationContrat {

	private static final long serialVersionUID = -7058042857053348832L;

	public static final String T_VALIDATION_JURIDIQUE = "JURIDIQUE";
	public static final String T_VALIDATION_FINANCIERE = "FINANCE";
	public static final String T_VALIDATION_VALO = "VALO";
	public static final String T_VALIDATION_SCIENTIFIQUE = "SCIENT";

	/**
	 * @param persId l'utilisateur qui la passe à valide
	 */
	public void passerValide(Integer persId) {
		setValide(true);
		setDateValidation(new NSTimestamp());
		setPersIdModification(persId);
		setDModification(new NSTimestamp());
	}
	
	
}
