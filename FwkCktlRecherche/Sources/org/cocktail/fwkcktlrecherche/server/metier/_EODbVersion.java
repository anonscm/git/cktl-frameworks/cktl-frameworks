/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODbVersion.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODbVersion extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DbVersion";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "dbvId";

	public static final er.extensions.eof.ERXKey<java.lang.String> DBV_COMMENT = new er.extensions.eof.ERXKey<java.lang.String>("dbvComment");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DBV_DATE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dbvDate");
	public static final er.extensions.eof.ERXKey<java.lang.String> DBV_INSTALL = new er.extensions.eof.ERXKey<java.lang.String>("dbvInstall");
	public static final er.extensions.eof.ERXKey<java.lang.String> DBV_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("dbvLibelle");
	
	public static final String DBV_COMMENT_KEY = DBV_COMMENT.key();
	public static final String DBV_DATE_KEY = DBV_DATE.key();
	public static final String DBV_INSTALL_KEY = DBV_INSTALL.key();
	public static final String DBV_LIBELLE_KEY = DBV_LIBELLE.key();

	// Attributs non visibles
	public static final String DBV_ID_KEY = "dbvId";

	//Colonnes dans la base de donnees
	public static final String DBV_COMMENT_COLKEY = "DBV_COMMENT";
	public static final String DBV_DATE_COLKEY = "DBV_DATE";
	public static final String DBV_INSTALL_COLKEY = "DBV_INSTALL";
	public static final String DBV_LIBELLE_COLKEY = "DBV_LIBELLE";

	public static final String DBV_ID_COLKEY = "DBV_ID";

	// Relationships
	

	// Accessors methods
	public java.lang.String dbvComment() {
		return (java.lang.String) storedValueForKey(DBV_COMMENT_KEY);
	}

	public void setDbvComment(java.lang.String value) {
	    takeStoredValueForKey(value, DBV_COMMENT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dbvDate() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DBV_DATE_KEY);
	}

	public void setDbvDate(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DBV_DATE_KEY);
	}
	
	public java.lang.String dbvInstall() {
		return (java.lang.String) storedValueForKey(DBV_INSTALL_KEY);
	}

	public void setDbvInstall(java.lang.String value) {
	    takeStoredValueForKey(value, DBV_INSTALL_KEY);
	}
	
	public java.lang.String dbvLibelle() {
		return (java.lang.String) storedValueForKey(DBV_LIBELLE_KEY);
	}

	public void setDbvLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, DBV_LIBELLE_KEY);
	}
	
	public static EODbVersion createDbVersion(EOEditingContext editingContext, java.lang.String dbvInstall, java.lang.String dbvLibelle) {
		EODbVersion eo = (EODbVersion) EOUtilities.createAndInsertInstance(editingContext, _EODbVersion.ENTITY_NAME);    
		eo.setDbvInstall(dbvInstall);
		eo.setDbvLibelle(dbvLibelle);
		return eo;
	}

	public EODbVersion localInstanceIn(EOEditingContext editingContext) {
		return (EODbVersion) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODbVersion creerInstance(EOEditingContext editingContext) {
		return (EODbVersion) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODbVersion.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODbVersion> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODbVersion>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODbVersion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODbVersion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODbVersion> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODbVersion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODbVersion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODbVersion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODbVersion> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODbVersion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODbVersion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODbVersion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODbVersion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODbVersion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAllDbVersions(EOEditingContext editingContext) {
		return _EODbVersion.fetchAllDbVersions(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODbVersion> fetchAllDbVersions(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODbVersion.fetchDbVersions(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODbVersion> fetchDbVersions(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODbVersion> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODbVersion>(_EODbVersion.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODbVersion> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODbVersion fetchDbVersion(EOEditingContext editingContext, String keyName, Object value) {
		return _EODbVersion.fetchDbVersion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODbVersion fetchDbVersion(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODbVersion> eoObjects = _EODbVersion.fetchDbVersions(editingContext, qualifier, null);
	    EODbVersion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DbVersion that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
