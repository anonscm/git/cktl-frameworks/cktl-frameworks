/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartStructureRechercheDomaineScientifique.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartStructureRechercheDomaineScientifique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartStructureRechercheDomaineScientifique";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "rurdsId";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> PRINCIPAL = new er.extensions.eof.ERXKey<java.lang.Boolean>("principal");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> RURDS_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("rurdsId");
	
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String PRINCIPAL_KEY = PRINCIPAL.key();
	public static final String RURDS_ID_KEY = RURDS_ID.key();

	// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_KEY = "domaineScientifiqueOrdre";

	//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PRINCIPAL_COLKEY = "PRINCIPAL";
	public static final String RURDS_ID_COLKEY = "RURDS_ID";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_COLKEY = "DOMAINE_SCIENT_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> UNITE_RECHERCHE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("uniteRecherche");
	
	public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();
	public static final String UNITE_RECHERCHE_KEY = UNITE_RECHERCHE.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.Boolean principal() {
		return (java.lang.Boolean) storedValueForKey(PRINCIPAL_KEY);
	}

	public void setPrincipal(java.lang.Boolean value) {
	    takeStoredValueForKey(value, PRINCIPAL_KEY);
	}
	
	public java.lang.Integer rurdsId() {
		return (java.lang.Integer) storedValueForKey(RURDS_ID_KEY);
	}

	public void setRurdsId(java.lang.Integer value) {
	    takeStoredValueForKey(value, RURDS_ID_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) storedValueForKey(DOMAINE_SCIENTIFIQUE_KEY);
	}

	public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOMAINE_SCIENTIFIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_SCIENTIFIQUE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure uniteRecherche() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(UNITE_RECHERCHE_KEY);
	}

	public void setUniteRechercheRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = uniteRecherche();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UNITE_RECHERCHE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, UNITE_RECHERCHE_KEY);
	    }
	}
	
	public static EORepartStructureRechercheDomaineScientifique createRepartStructureRechercheDomaineScientifique(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, java.lang.Integer rurdsId, org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique, org.cocktail.fwkcktlpersonne.common.metier.EOStructure uniteRecherche) {
		EORepartStructureRechercheDomaineScientifique eo = (EORepartStructureRechercheDomaineScientifique) EOUtilities.createAndInsertInstance(editingContext, _EORepartStructureRechercheDomaineScientifique.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setRurdsId(rurdsId);
		eo.setDomaineScientifiqueRelationship(domaineScientifique);
		eo.setUniteRechercheRelationship(uniteRecherche);
		return eo;
	}

	public EORepartStructureRechercheDomaineScientifique localInstanceIn(EOEditingContext editingContext) {
		return (EORepartStructureRechercheDomaineScientifique) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartStructureRechercheDomaineScientifique creerInstance(EOEditingContext editingContext) {
		return (EORepartStructureRechercheDomaineScientifique) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartStructureRechercheDomaineScientifique.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartStructureRechercheDomaineScientifique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartStructureRechercheDomaineScientifique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartStructureRechercheDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartStructureRechercheDomaineScientifique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartStructureRechercheDomaineScientifique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartStructureRechercheDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartStructureRechercheDomaineScientifique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartStructureRechercheDomaineScientifique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartStructureRechercheDomaineScientifique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartStructureRechercheDomaineScientifique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAllRepartStructureRechercheDomaineScientifiques(EOEditingContext editingContext) {
		return _EORepartStructureRechercheDomaineScientifique.fetchAllRepartStructureRechercheDomaineScientifiques(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchAllRepartStructureRechercheDomaineScientifiques(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartStructureRechercheDomaineScientifique.fetchRepartStructureRechercheDomaineScientifiques(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> fetchRepartStructureRechercheDomaineScientifiques(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(_EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartStructureRechercheDomaineScientifique fetchRepartStructureRechercheDomaineScientifique(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartStructureRechercheDomaineScientifique.fetchRepartStructureRechercheDomaineScientifique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartStructureRechercheDomaineScientifique fetchRepartStructureRechercheDomaineScientifique(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartStructureRechercheDomaineScientifique> eoObjects = _EORepartStructureRechercheDomaineScientifique.fetchRepartStructureRechercheDomaineScientifiques(editingContext, qualifier, null);
	    EORepartStructureRechercheDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartStructureRechercheDomaineScientifique that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
