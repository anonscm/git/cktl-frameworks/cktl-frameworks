/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdPerimetreSangria.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOGdPerimetreSangria extends org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre {
	public static final String ENTITY_NAME = "GdPerimetreSangria";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final er.extensions.eof.ERXKey<java.lang.Integer> APP_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("appId");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("libelle");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERIMETRE_PARENT_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("perimetreParentId");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> STRATEGIE_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("strategieId");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> TYPE_DROIT_DONNEE_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("typeDroitDonneeId");
	
	public static final String APP_ID_KEY = APP_ID.key();
	public static final String DATE_CREATION_KEY = DATE_CREATION.key();
	public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
	public static final String LIBELLE_KEY = LIBELLE.key();
	public static final String PERIMETRE_PARENT_ID_KEY = PERIMETRE_PARENT_ID.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String STRATEGIE_ID_KEY = STRATEGIE_ID.key();
	public static final String TYPE_DROIT_DONNEE_ID_KEY = TYPE_DROIT_DONNEE_ID.key();

	// Attributs non visibles
	public static final String ID_KEY = "id";

	//Colonnes dans la base de donnees
	public static final String APP_ID_COLKEY = "$attribute.columnName";
	public static final String DATE_CREATION_COLKEY = "$attribute.columnName";
	public static final String DATE_MODIFICATION_COLKEY = "$attribute.columnName";
	public static final String LIBELLE_COLKEY = "$attribute.columnName";
	public static final String PERIMETRE_PARENT_ID_COLKEY = "$attribute.columnName";
	public static final String PERS_ID_CREATION_COLKEY = "$attribute.columnName";
	public static final String PERS_ID_MODIFICATION_COLKEY = "$attribute.columnName";
	public static final String STRATEGIE_ID_COLKEY = "STRATEGIE_ID";
	public static final String TYPE_DROIT_DONNEE_ID_COLKEY = "$attribute.columnName";

	public static final String ID_COLKEY = "ID_GD_PERIMETRE_SANGRIA";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication> APPLICATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication>("application");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre> GD_PROFIL_PERIMETRES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilPerimetre>("gdProfilPerimetres");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRE_PARENT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetreParent");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre> PERIMETRES_ENFANTS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdPerimetre>("perimetresEnfants");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> PROFILS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("profils");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria> STRATEGIE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria>("strategie");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee> TYPE_DROIT_DONNEE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitDonnee>("typeDroitDonnee");
	
	public static final String APPLICATION_KEY = APPLICATION.key();
	public static final String GD_PROFIL_PERIMETRES_KEY = GD_PROFIL_PERIMETRES.key();
	public static final String PERIMETRE_PARENT_KEY = PERIMETRE_PARENT.key();
	public static final String PERIMETRES_ENFANTS_KEY = PERIMETRES_ENFANTS.key();
	public static final String PROFILS_KEY = PROFILS.key();
	public static final String STRATEGIE_KEY = STRATEGIE.key();
	public static final String TYPE_DROIT_DONNEE_KEY = TYPE_DROIT_DONNEE.key();

	// Accessors methods
	public java.lang.Integer appId() {
		return (java.lang.Integer) storedValueForKey(APP_ID_KEY);
	}

	public void setAppId(java.lang.Integer value) {
	    takeStoredValueForKey(value, APP_ID_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
	}

	public void setDateCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
	}

	public void setDateModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
	}
	
	public java.lang.String libelle() {
		return (java.lang.String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_KEY);
	}
	
	public java.lang.Integer perimetreParentId() {
		return (java.lang.Integer) storedValueForKey(PERIMETRE_PARENT_ID_KEY);
	}

	public void setPerimetreParentId(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERIMETRE_PARENT_ID_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.Integer strategieId() {
		return (java.lang.Integer) storedValueForKey(STRATEGIE_ID_KEY);
	}

	public void setStrategieId(java.lang.Integer value) {
	    takeStoredValueForKey(value, STRATEGIE_ID_KEY);
	}
	
	public java.lang.Integer typeDroitDonneeId() {
		return (java.lang.Integer) storedValueForKey(TYPE_DROIT_DONNEE_ID_KEY);
	}

	public void setTypeDroitDonneeId(java.lang.Integer value) {
	    takeStoredValueForKey(value, TYPE_DROIT_DONNEE_ID_KEY);
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria strategie() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria) storedValueForKey(STRATEGIE_KEY);
	}

	public void setStrategieRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria oldValue = strategie();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRATEGIE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, STRATEGIE_KEY);
	    }
	}
	
	public static EOGdPerimetreSangria createGdPerimetreSangria(EOEditingContext editingContext, java.lang.Integer appId, com.webobjects.foundation.NSTimestamp dateCreation, java.lang.String libelle, java.lang.Integer persIdCreation) {
		EOGdPerimetreSangria eo = (EOGdPerimetreSangria) EOUtilities.createAndInsertInstance(editingContext, _EOGdPerimetreSangria.ENTITY_NAME);    
		eo.setAppId(appId);
		eo.setDateCreation(dateCreation);
		eo.setLibelle(libelle);
		eo.setPersIdCreation(persIdCreation);
		return eo;
	}

	public EOGdPerimetreSangria localInstanceIn(EOEditingContext editingContext) {
		return (EOGdPerimetreSangria) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOGdPerimetreSangria creerInstance(EOEditingContext editingContext) {
		return (EOGdPerimetreSangria) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOGdPerimetreSangria.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOGdPerimetreSangria> fetchAllGdPerimetreSangrias(EOEditingContext editingContext) {
		return _EOGdPerimetreSangria.fetchAllGdPerimetreSangrias(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOGdPerimetreSangria> fetchAllGdPerimetreSangrias(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOGdPerimetreSangria.fetchGdPerimetreSangrias(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOGdPerimetreSangria> fetchGdPerimetreSangrias(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOGdPerimetreSangria> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOGdPerimetreSangria>(_EOGdPerimetreSangria.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOGdPerimetreSangria> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOGdPerimetreSangria fetchGdPerimetreSangria(EOEditingContext editingContext, String keyName, Object value) {
		return _EOGdPerimetreSangria.fetchGdPerimetreSangria(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOGdPerimetreSangria fetchGdPerimetreSangria(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOGdPerimetreSangria> eoObjects = _EOGdPerimetreSangria.fetchGdPerimetreSangrias(editingContext, qualifier, null);
	    EOGdPerimetreSangria eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one GdPerimetreSangria that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
