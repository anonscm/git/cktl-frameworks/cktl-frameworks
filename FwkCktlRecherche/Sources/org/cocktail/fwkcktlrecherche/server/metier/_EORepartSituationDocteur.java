/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartSituationDocteur.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartSituationDocteur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartSituationDocteur";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idRsd";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_DEBUT_CONTRAT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateDebutContrat");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_FIN_CONTRAT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateFinContrat");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	
	public static final String DATE_DEBUT_CONTRAT_KEY = DATE_DEBUT_CONTRAT.key();
	public static final String DATE_FIN_CONTRAT_KEY = DATE_FIN_CONTRAT.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();

	// Attributs non visibles
	public static final String ID_RSD_KEY = "idRsd";
	public static final String ID_SITUATION_DOCTEUR_KEY = "idSituationDocteur";
	public static final String ID_THESE_KEY = "idThese";

	//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_CONTRAT_COLKEY = "DATE_DEBUT_CONTRAT";
	public static final String DATE_FIN_CONTRAT_COLKEY = "DATE_FIN_CONTRAT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_RSD_COLKEY = "ID_RSD";
	public static final String ID_SITUATION_DOCTEUR_COLKEY = "ID_SITUATION_DOCTEUR";
	public static final String ID_THESE_COLKEY = "ID_THESE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese> TO_DOCTORANT_THESE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese>("toDoctorantThese");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur> TO_SITUATION_DOCTEUR = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur>("toSituationDocteur");
	
	public static final String TO_DOCTORANT_THESE_KEY = TO_DOCTORANT_THESE.key();
	public static final String TO_SITUATION_DOCTEUR_KEY = TO_SITUATION_DOCTEUR.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dateDebutContrat() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_DEBUT_CONTRAT_KEY);
	}

	public void setDateDebutContrat(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_DEBUT_CONTRAT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateFinContrat() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_FIN_CONTRAT_KEY);
	}

	public void setDateFinContrat(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_FIN_CONTRAT_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese) storedValueForKey(TO_DOCTORANT_THESE_KEY);
	}

	public void setToDoctorantTheseRelationship(org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese oldValue = toDoctorantThese();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DOCTORANT_THESE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DOCTORANT_THESE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur toSituationDocteur() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur) storedValueForKey(TO_SITUATION_DOCTEUR_KEY);
	}

	public void setToSituationDocteurRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur oldValue = toSituationDocteur();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_DOCTEUR_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_DOCTEUR_KEY);
	    }
	}
	
	public static EORepartSituationDocteur createRepartSituationDocteur(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dateDebutContrat, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese toDoctorantThese, org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur toSituationDocteur) {
		EORepartSituationDocteur eo = (EORepartSituationDocteur) EOUtilities.createAndInsertInstance(editingContext, _EORepartSituationDocteur.ENTITY_NAME);    
		eo.setDateDebutContrat(dateDebutContrat);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setToDoctorantTheseRelationship(toDoctorantThese);
		eo.setToSituationDocteurRelationship(toSituationDocteur);
		return eo;
	}

	public EORepartSituationDocteur localInstanceIn(EOEditingContext editingContext) {
		return (EORepartSituationDocteur) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartSituationDocteur creerInstance(EOEditingContext editingContext) {
		return (EORepartSituationDocteur) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartSituationDocteur.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartSituationDocteur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartSituationDocteur>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartSituationDocteur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartSituationDocteur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartSituationDocteur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartSituationDocteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartSituationDocteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartSituationDocteur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartSituationDocteur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartSituationDocteur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartSituationDocteur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartSituationDocteur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAllRepartSituationDocteurs(EOEditingContext editingContext) {
		return _EORepartSituationDocteur.fetchAllRepartSituationDocteurs(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchAllRepartSituationDocteurs(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartSituationDocteur.fetchRepartSituationDocteurs(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartSituationDocteur> fetchRepartSituationDocteurs(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartSituationDocteur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartSituationDocteur>(_EORepartSituationDocteur.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartSituationDocteur> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartSituationDocteur fetchRepartSituationDocteur(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartSituationDocteur.fetchRepartSituationDocteur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartSituationDocteur fetchRepartSituationDocteur(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartSituationDocteur> eoObjects = _EORepartSituationDocteur.fetchRepartSituationDocteurs(editingContext, qualifier, null);
	    EORepartSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartSituationDocteur that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
