/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODotationAnnuelle.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODotationAnnuelle extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DotationAnnuelle";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "ordre";

	public static final er.extensions.eof.ERXKey<java.lang.Integer> ANNEE = new er.extensions.eof.ERXKey<java.lang.Integer>("annee");
	public static final er.extensions.eof.ERXKey<java.lang.String> COMMENTAIRE = new er.extensions.eof.ERXKey<java.lang.String>("commentaire");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT = new er.extensions.eof.ERXKey<java.lang.Float>("montant");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	
	public static final String ANNEE_KEY = ANNEE.key();
	public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String MONTANT_KEY = MONTANT.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();

	// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PROVENANCE_KEY = "cStructureProvenance";
	public static final String ORDRE_KEY = "ordre";
	public static final String TYPE_DOTATION_ID_KEY = "typeDotationId";

	//Colonnes dans la base de donnees
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String C_STRUCTURE_COLKEY = "C_STR";
	public static final String C_STRUCTURE_PROVENANCE_COLKEY = "C_STR_PROVENANCE";
	public static final String ORDRE_COLKEY = "ORDRE";
	public static final String TYPE_DOTATION_ID_COLKEY = "TYPE_DOTATION_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structure");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> STRUCTURE_PROVENANCE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("structureProvenance");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation> TYPE_DOTATION = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation>("typeDotation");
	
	public static final String STRUCTURE_KEY = STRUCTURE.key();
	public static final String STRUCTURE_PROVENANCE_KEY = STRUCTURE_PROVENANCE.key();
	public static final String TYPE_DOTATION_KEY = TYPE_DOTATION.key();

	// Accessors methods
	public java.lang.Integer annee() {
		return (java.lang.Integer) storedValueForKey(ANNEE_KEY);
	}

	public void setAnnee(java.lang.Integer value) {
	    takeStoredValueForKey(value, ANNEE_KEY);
	}
	
	public java.lang.String commentaire() {
		return (java.lang.String) storedValueForKey(COMMENTAIRE_KEY);
	}

	public void setCommentaire(java.lang.String value) {
	    takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Float montant() {
		return (java.lang.Float) storedValueForKey(MONTANT_KEY);
	}

	public void setMontant(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structure() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(STRUCTURE_KEY);
	}

	public void setStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structure();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure structureProvenance() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(STRUCTURE_PROVENANCE_KEY);
	}

	public void setStructureProvenanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = structureProvenance();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_PROVENANCE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_PROVENANCE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation typeDotation() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation) storedValueForKey(TYPE_DOTATION_KEY);
	}

	public void setTypeDotationRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation oldValue = typeDotation();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_DOTATION_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_DOTATION_KEY);
	    }
	}
	
	public static EODotationAnnuelle createDotationAnnuelle(EOEditingContext editingContext, java.lang.Integer annee, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Float montant, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, org.cocktail.fwkcktlpersonne.common.metier.EOStructure structure, org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation typeDotation) {
		EODotationAnnuelle eo = (EODotationAnnuelle) EOUtilities.createAndInsertInstance(editingContext, _EODotationAnnuelle.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setStructureRelationship(structure);
		eo.setTypeDotationRelationship(typeDotation);
		return eo;
	}

	public EODotationAnnuelle localInstanceIn(EOEditingContext editingContext) {
		return (EODotationAnnuelle) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODotationAnnuelle creerInstance(EOEditingContext editingContext) {
		return (EODotationAnnuelle) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODotationAnnuelle.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODotationAnnuelle> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODotationAnnuelle>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODotationAnnuelle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODotationAnnuelle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODotationAnnuelle> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODotationAnnuelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODotationAnnuelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODotationAnnuelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODotationAnnuelle> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODotationAnnuelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODotationAnnuelle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODotationAnnuelle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODotationAnnuelle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODotationAnnuelle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAllDotationAnnuelles(EOEditingContext editingContext) {
		return _EODotationAnnuelle.fetchAllDotationAnnuelles(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchAllDotationAnnuelles(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODotationAnnuelle.fetchDotationAnnuelles(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODotationAnnuelle> fetchDotationAnnuelles(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODotationAnnuelle> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODotationAnnuelle>(_EODotationAnnuelle.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODotationAnnuelle> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODotationAnnuelle fetchDotationAnnuelle(EOEditingContext editingContext, String keyName, Object value) {
		return _EODotationAnnuelle.fetchDotationAnnuelle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODotationAnnuelle fetchDotationAnnuelle(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODotationAnnuelle> eoObjects = _EODotationAnnuelle.fetchDotationAnnuelles(editingContext, qualifier, null);
	    EODotationAnnuelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DotationAnnuelle that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
