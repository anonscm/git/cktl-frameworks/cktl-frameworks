/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODirecteurTheseHabilite.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EODirecteurTheseHabilite extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DirecteurTheseHabilite";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idDth";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_HDR = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateHdr");
	public static final er.extensions.eof.ERXKey<java.lang.String> LABO_EXTERNE_DTH = new er.extensions.eof.ERXKey<java.lang.String>("laboExterneDth");
	public static final er.extensions.eof.ERXKey<java.lang.String> TEM_VALIDE = new er.extensions.eof.ERXKey<java.lang.String>("temValide");
	
	public static final String DATE_HDR_KEY = DATE_HDR.key();
	public static final String LABO_EXTERNE_DTH_KEY = LABO_EXTERNE_DTH.key();
	public static final String TEM_VALIDE_KEY = TEM_VALIDE.key();

	// Attributs non visibles
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STRUCTURE_ED_KEY = "cStructureEd";
	public static final String C_STRUCTURE_ETAB_KEY = "cStructureEtab";
	public static final String C_STRUCTURE_LABO_KEY = "cStructureLabo";
	public static final String ID_DTH_KEY = "idDth";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NO_INDIVIDU_UTILISATEUR_KEY = "noIndividuUtilisateur";

	//Colonnes dans la base de donnees
	public static final String DATE_HDR_COLKEY = "DATE_HDR";
	public static final String LABO_EXTERNE_DTH_COLKEY = "LABO_EXTERNE_DTH";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_STRUCTURE_ED_COLKEY = "C_STRUCTURE_ED";
	public static final String C_STRUCTURE_ETAB_COLKEY = "C_STRUCTURE_ETAB";
	public static final String C_STRUCTURE_LABO_COLKEY = "C_STRUCTURE_LABO";
	public static final String ID_DTH_COLKEY = "ID_DTH";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NO_INDIVIDU_UTILISATEUR_COLKEY = "NO_INDIVIDU_UTILISATEUR";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_UTILISATEUR_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuUtilisateurFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_FWK_PERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneFwkPers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_ED_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureEdFwkpers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_ETAB_FWK_PERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureEtabFwkPers");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_LABO_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureLaboFwkpers");
	
	public static final String TO_INDIVIDU_FWKPERS_KEY = TO_INDIVIDU_FWKPERS.key();
	public static final String TO_INDIVIDU_UTILISATEUR_FWKPERS_KEY = TO_INDIVIDU_UTILISATEUR_FWKPERS.key();
	public static final String TO_RNE_FWK_PERS_KEY = TO_RNE_FWK_PERS.key();
	public static final String TO_STRUCTURE_ED_FWKPERS_KEY = TO_STRUCTURE_ED_FWKPERS.key();
	public static final String TO_STRUCTURE_ETAB_FWK_PERS_KEY = TO_STRUCTURE_ETAB_FWK_PERS.key();
	public static final String TO_STRUCTURE_LABO_FWKPERS_KEY = TO_STRUCTURE_LABO_FWKPERS.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dateHdr() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_HDR_KEY);
	}

	public void setDateHdr(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_HDR_KEY);
	}
	
	public java.lang.String laboExterneDth() {
		return (java.lang.String) storedValueForKey(LABO_EXTERNE_DTH_KEY);
	}

	public void setLaboExterneDth(java.lang.String value) {
	    takeStoredValueForKey(value, LABO_EXTERNE_DTH_KEY);
	}
	
	public java.lang.String temValide() {
		return (java.lang.String) storedValueForKey(TEM_VALIDE_KEY);
	}

	public void setTemValide(java.lang.String value) {
	    takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(TO_INDIVIDU_FWKPERS_KEY);
	}

	public void setToIndividuFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuUtilisateurFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(TO_INDIVIDU_UTILISATEUR_FWKPERS_KEY);
	}

	public void setToIndividuUtilisateurFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuUtilisateurFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_UTILISATEUR_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_UTILISATEUR_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneFwkPers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EORne) storedValueForKey(TO_RNE_FWK_PERS_KEY);
	}

	public void setToRneFwkPersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneFwkPers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_FWK_PERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_FWK_PERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEdFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_STRUCTURE_ED_FWKPERS_KEY);
	}

	public void setToStructureEdFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureEdFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_ED_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_ED_FWKPERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEtabFwkPers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	}

	public void setToStructureEtabFwkPersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureEtabFwkPers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_ETAB_FWK_PERS_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureLaboFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(TO_STRUCTURE_LABO_FWKPERS_KEY);
	}

	public void setToStructureLaboFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureLaboFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_LABO_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_LABO_FWKPERS_KEY);
	    }
	}
	
	public static EODirecteurTheseHabilite createDirecteurTheseHabilite(EOEditingContext editingContext, java.lang.String temValide, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuFwkpers, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuUtilisateurFwkpers, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEdFwkpers) {
		EODirecteurTheseHabilite eo = (EODirecteurTheseHabilite) EOUtilities.createAndInsertInstance(editingContext, _EODirecteurTheseHabilite.ENTITY_NAME);    
		eo.setTemValide(temValide);
		eo.setToIndividuFwkpersRelationship(toIndividuFwkpers);
		eo.setToIndividuUtilisateurFwkpersRelationship(toIndividuUtilisateurFwkpers);
		eo.setToStructureEdFwkpersRelationship(toStructureEdFwkpers);
		return eo;
	}

	public EODirecteurTheseHabilite localInstanceIn(EOEditingContext editingContext) {
		return (EODirecteurTheseHabilite) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EODirecteurTheseHabilite creerInstance(EOEditingContext editingContext) {
		return (EODirecteurTheseHabilite) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EODirecteurTheseHabilite.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EODirecteurTheseHabilite> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODirecteurTheseHabilite>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EODirecteurTheseHabilite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODirecteurTheseHabilite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> eoObjects = fetchAll(editingContext, qualifier, null);
	    EODirecteurTheseHabilite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EODirecteurTheseHabilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EODirecteurTheseHabilite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODirecteurTheseHabilite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EODirecteurTheseHabilite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EODirecteurTheseHabilite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EODirecteurTheseHabilite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EODirecteurTheseHabilite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAllDirecteurTheseHabilites(EOEditingContext editingContext) {
		return _EODirecteurTheseHabilite.fetchAllDirecteurTheseHabilites(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchAllDirecteurTheseHabilites(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EODirecteurTheseHabilite.fetchDirecteurTheseHabilites(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> fetchDirecteurTheseHabilites(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EODirecteurTheseHabilite> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EODirecteurTheseHabilite>(_EODirecteurTheseHabilite.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EODirecteurTheseHabilite fetchDirecteurTheseHabilite(EOEditingContext editingContext, String keyName, Object value) {
		return _EODirecteurTheseHabilite.fetchDirecteurTheseHabilite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EODirecteurTheseHabilite fetchDirecteurTheseHabilite(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EODirecteurTheseHabilite> eoObjects = _EODirecteurTheseHabilite.fetchDirecteurTheseHabilites(editingContext, qualifier, null);
	    EODirecteurTheseHabilite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one DirecteurTheseHabilite that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
