/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.logging.ERXLogger;
import er.extensions.validation.ERXValidationFactory;


public class EOProgrammeAnnee extends _EOProgrammeAnnee {

    public EOProgrammeAnnee() {
        super();
    }

    public static NSArray<Integer> anneesPourStructure(EOEditingContext editingContext, EOStructure structure) {
        NSArray<EOProgramme> programmes = EOProgramme.fetchAll(editingContext, EOProgramme.FINANCEUR.eq(structure));
        
        if(programmes.isEmpty()) {
          return new NSArray<Integer>();
        }
        
        NSArray<Integer> annees = ERXArrayUtilities.flatten((NSArray<Integer>) programmes.valueForKeyPath(EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).key()));
        
        try {
          annees = ERXArrayUtilities.arrayWithoutDuplicates(annees).sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
        }
        catch (ComparisonException e) {
          ERXLogger.log.error("Erreur lors de la récupération des années", e);
          return new NSArray<Integer>();
        }
        
        return annees;
        
      }
      
      /**
       * Valide que l'annee peut être suppprimée
       * On teste si le programme est null dans cas, ca veut dire qu'il a été supprimé avant
       * donc on supprime l'année dans tous les cas
       */
      @Override
      public void validateForDelete() throws ValidationException {
        super.validateForDelete();
        
        if(programme() != null) {
	        NSArray<EOAap> aapsLiesAuProgramme = EOAap.fetchAll(editingContext(), EOAap.PROGRAMME.eq(programme())
	            .and(EOAap.CONTRAT.dot(Contrat.EXERCICE_COCKTAIL).dot(EOExerciceCocktail.EXE_ORDRE_KEY).eq(annee())));
	        if(aapsLiesAuProgramme.isEmpty() == false) {
	          throw ERXValidationFactory.defaultFactory().createCustomException(this, "LiaisonAapRestante");
	        }
        }
      }
      
      @Override
      public void delete() {
        if(programme().niveau() == EOProgramme.NIVEAU_PROGRAMME) {
          NSArray<EOProgrammeAnnee> programmeAnnees = EOProgrammeAnnee.fetchAll(
              editingContext(), EOProgrammeAnnee.PROGRAMME.dot(EOProgramme.CODE).startsWith(programme().code()).and(ANNEE.eq(annee())));
          NSArray<EOProgrammeAnnee> programmeAnneeFils = ERXQ.filtered(programmeAnnees, EOProgrammeAnnee.PROGRAMME.dot(EOProgramme.NIVEAU).eq(EOProgramme.NIVEAU_SOUS_PROGRAMME));
          for(EOProgrammeAnnee programmeAnnee : programmeAnneeFils) {
            programmeAnnee.delete();
          }
        }
        super.delete();
      }
}
