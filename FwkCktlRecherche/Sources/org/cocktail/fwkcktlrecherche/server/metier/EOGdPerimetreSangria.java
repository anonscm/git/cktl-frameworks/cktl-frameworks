/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import javax.annotation.Nullable;


public class EOGdPerimetreSangria extends _EOGdPerimetreSangria {

	@Inject
	@Nullable
	private UserInfo userInfo;

	private static final long serialVersionUID = 1L;

	// Constructor
	// Properties

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	// Public Methods
	/**
	 * @param editingContext : contexte d'édition. Necessaire pour reveiller le mecanisme d'injection du user
	 * @return Un EOGdPerimetreSangria cree et insere dans le contexte d'edition
	 */
	public static EOGdPerimetreSangria create(EOEditingContext editingContext) {
		EOGdPerimetreSangria eo = (EOGdPerimetreSangria) createAndInsertInstance(editingContext, EOGdPerimetreSangria.ENTITY_NAME);
		return eo;
	}
	
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		actualiseInfoEnregistrement();
	}

	@Override
	public void validateBeforeTransactionSave() throws ValidationException {
		super.validateBeforeTransactionSave();
		actualiseInfoEnregistrement();
	}

	// Protected Methods
	// Private methods	
	private void actualiseInfoEnregistrement() {
		if ((userInfo != null) && (userInfo.persId() != null)) {
			if (persIdCreation() == null) {
				setPersIdCreation(userInfo.persId().intValue());
				setDateCreation(new NSTimestamp());
			}
			setPersIdModification(userInfo.persId().intValue());
			setDateModification(new NSTimestamp());
		}
	}
}
