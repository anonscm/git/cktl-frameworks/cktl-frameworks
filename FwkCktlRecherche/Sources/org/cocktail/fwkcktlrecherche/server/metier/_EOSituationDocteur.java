/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSituationDocteur.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOSituationDocteur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "SituationDocteur";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idSituationDocteur";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE_SITUATION_DOCTEUR = new er.extensions.eof.ERXKey<java.lang.String>("libelleSituationDocteur");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_CONTRAT_POST_DOCTORAL = new er.extensions.eof.ERXKey<java.lang.String>("typeContratPostDoctoral");
	
	public static final String LIBELLE_SITUATION_DOCTEUR_KEY = LIBELLE_SITUATION_DOCTEUR.key();
	public static final String TYPE_CONTRAT_POST_DOCTORAL_KEY = TYPE_CONTRAT_POST_DOCTORAL.key();

	// Attributs non visibles
	public static final String ID_SITUATION_DOCTEUR_KEY = "idSituationDocteur";

	//Colonnes dans la base de donnees
	public static final String LIBELLE_SITUATION_DOCTEUR_COLKEY = "LIBELLE_SITUATION_DOCTEUR";
	public static final String TYPE_CONTRAT_POST_DOCTORAL_COLKEY = "TYPE_CONTRAT_POST_DOCTORAL";

	public static final String ID_SITUATION_DOCTEUR_COLKEY = "ID_SITUATION_DOCTEUR";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> REPART_SITUATION_DOCTEURS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur>("repartSituationDocteurs");
	
	public static final String REPART_SITUATION_DOCTEURS_KEY = REPART_SITUATION_DOCTEURS.key();

	// Accessors methods
	public java.lang.String libelleSituationDocteur() {
		return (java.lang.String) storedValueForKey(LIBELLE_SITUATION_DOCTEUR_KEY);
	}

	public void setLibelleSituationDocteur(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_SITUATION_DOCTEUR_KEY);
	}
	
	public java.lang.String typeContratPostDoctoral() {
		return (java.lang.String) storedValueForKey(TYPE_CONTRAT_POST_DOCTORAL_KEY);
	}

	public void setTypeContratPostDoctoral(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_CONTRAT_POST_DOCTORAL_KEY);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> repartSituationDocteurs() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(REPART_SITUATION_DOCTEURS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> repartSituationDocteurs(EOQualifier qualifier) {
		return repartSituationDocteurs(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> repartSituationDocteurs(EOQualifier qualifier, boolean fetch) {
	    return repartSituationDocteurs(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> repartSituationDocteurs(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur.TO_SITUATION_DOCTEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = repartSituationDocteurs();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToRepartSituationDocteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur object) {
		addObjectToBothSidesOfRelationshipWithKey(object, REPART_SITUATION_DOCTEURS_KEY);
	}

	public void removeFromRepartSituationDocteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_SITUATION_DOCTEURS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur createRepartSituationDocteursRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartSituationDocteur");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_SITUATION_DOCTEURS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur) eo;
	}

	public void deleteRepartSituationDocteursRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_SITUATION_DOCTEURS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllRepartSituationDocteursRelationships() {
		java.util.Enumeration objects = repartSituationDocteurs().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteRepartSituationDocteursRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur) objects.nextElement());
	    }
	}

	public static EOSituationDocteur createSituationDocteur(EOEditingContext editingContext, java.lang.String libelleSituationDocteur, java.lang.String typeContratPostDoctoral) {
		EOSituationDocteur eo = (EOSituationDocteur) EOUtilities.createAndInsertInstance(editingContext, _EOSituationDocteur.ENTITY_NAME);    
		eo.setLibelleSituationDocteur(libelleSituationDocteur);
		eo.setTypeContratPostDoctoral(typeContratPostDoctoral);
		return eo;
	}

	public EOSituationDocteur localInstanceIn(EOEditingContext editingContext) {
		return (EOSituationDocteur) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOSituationDocteur creerInstance(EOEditingContext editingContext) {
		return (EOSituationDocteur) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOSituationDocteur.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOSituationDocteur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOSituationDocteur>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOSituationDocteur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOSituationDocteur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOSituationDocteur> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOSituationDocteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOSituationDocteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOSituationDocteur> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOSituationDocteur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOSituationDocteur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOSituationDocteur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOSituationDocteur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAllSituationDocteurs(EOEditingContext editingContext) {
		return _EOSituationDocteur.fetchAllSituationDocteurs(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchAllSituationDocteurs(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOSituationDocteur.fetchSituationDocteurs(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOSituationDocteur> fetchSituationDocteurs(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOSituationDocteur> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOSituationDocteur>(_EOSituationDocteur.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOSituationDocteur> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOSituationDocteur fetchSituationDocteur(EOEditingContext editingContext, String keyName, Object value) {
		return _EOSituationDocteur.fetchSituationDocteur(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOSituationDocteur fetchSituationDocteur(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOSituationDocteur> eoObjects = _EOSituationDocteur.fetchSituationDocteurs(editingContext, qualifier, null);
	    EOSituationDocteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one SituationDocteur that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
