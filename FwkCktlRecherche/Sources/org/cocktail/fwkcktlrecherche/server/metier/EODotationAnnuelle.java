/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class EODotationAnnuelle extends _EODotationAnnuelle {

	private static final long serialVersionUID = -2353721063928623312L;

	public static final String DOTATION_SCIENTIFIQUE_CODE = "DOTATION_SCIENT";
	public static final String DOTATION_ETABLISSEMENT_RATTACHEMENT_CODE = "DOTATION_ETAB_RAT";
	public static final String CREDITS_INFRASTRUCTURE_CODE = "CREDITS_INFRA";

	/**
	 * 
	 * @param editingContext Dans lequel il faut mettre l'instance créée
	 * @param persId Pour les metadonnées
	 * @return une nouvelle instance
	 */
	public static EODotationAnnuelle creerInstance(EOEditingContext editingContext, Integer persId) {
		EODotationAnnuelle dotationAnnuelle = creerInstance(editingContext);
		dotationAnnuelle.setDCreation(MyDateCtrl.getDateJour());
		dotationAnnuelle.setDModification(MyDateCtrl.getDateJour());
		dotationAnnuelle.setPersIdCreation(persId);
		dotationAnnuelle.setPersIdModification(persId);
		return dotationAnnuelle;
	}


	/**
	 * 
	 *
	 * @throws ValidationException 
	 */
	public void validateObjectMetier() throws ValidationException {
		// VERIFICATION DE LA STRUCTURE DE PROVENANCE
		if (!StringUtils.equals(typeDotation().code(), CREDITS_INFRASTRUCTURE_CODE) && structureProvenance() == null) {
			throw new ValidationException("Vous devez sélectionner la provenance de la dotation.");
		}
	}

	@Override
	public void validateForSave() throws ValidationException {
		super.validateForSave();
		validateObjectMetier();
	}
}
