/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEvaluationMesr.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOEvaluationMesr extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "EvaluationMesr";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "evalMesrId";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> ALRM_MI_PARCOURS_DATE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("alrmMiParcoursDate");
	public static final er.extensions.eof.ERXKey<java.lang.String> ALRM_MI_PARCOURS_TEXTE = new er.extensions.eof.ERXKey<java.lang.String>("alrmMiParcoursTexte");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> ANNEE = new er.extensions.eof.ERXKey<java.lang.Integer>("annee");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_CA = new er.extensions.eof.ERXKey<java.lang.String>("avisCa");
	public static final er.extensions.eof.ERXKey<java.lang.String> AVIS_CS = new er.extensions.eof.ERXKey<java.lang.String>("avisCs");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ENVOI_AERES = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateEnvoiAeres");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ENVOI_MESR = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateEnvoiMesr");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PASSAGE_CA = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePassageCa");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_PASSAGE_CS = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("datePassageCs");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_RECEPTION_DOSSIER_DRV = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateReceptionDossierDrv");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateReceptionDossierNiveauIntermediaire");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateReceptionDossierNiveauSuperieur");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DEBUT_VISITE_AERES = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("debutVisiteAeres");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> DOSSIER_COMPLET = new er.extensions.eof.ERXKey<java.lang.Integer>("dossierComplet");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> FIN_VISITE_AERES = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("finVisiteAeres");
	public static final er.extensions.eof.ERXKey<java.lang.String> NOTE_AERES = new er.extensions.eof.ERXKey<java.lang.String>("noteAeres");
	public static final er.extensions.eof.ERXKey<java.lang.String> NOTE_APPRECIATION_DU_SUJET = new er.extensions.eof.ERXKey<java.lang.String>("noteAppreciationDuSujet");
	public static final er.extensions.eof.ERXKey<java.lang.String> NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION = new er.extensions.eof.ERXKey<java.lang.String>("noteQualiteScientifiqueProduction");
	public static final er.extensions.eof.ERXKey<java.lang.String> NOTE_RAYONNEMENT_ATTRACTIVITE = new er.extensions.eof.ERXKey<java.lang.String>("noteRayonnementAttractivite");
	public static final er.extensions.eof.ERXKey<java.lang.String> NOTE_STRATEGIE_GOUVERNANCE = new er.extensions.eof.ERXKey<java.lang.String>("noteStrategieGouvernance");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> TITRE = new er.extensions.eof.ERXKey<java.lang.String>("titre");
	
	public static final String ALRM_MI_PARCOURS_DATE_KEY = ALRM_MI_PARCOURS_DATE.key();
	public static final String ALRM_MI_PARCOURS_TEXTE_KEY = ALRM_MI_PARCOURS_TEXTE.key();
	public static final String ANNEE_KEY = ANNEE.key();
	public static final String AVIS_CA_KEY = AVIS_CA.key();
	public static final String AVIS_CS_KEY = AVIS_CS.key();
	public static final String DATE_ENVOI_AERES_KEY = DATE_ENVOI_AERES.key();
	public static final String DATE_ENVOI_MESR_KEY = DATE_ENVOI_MESR.key();
	public static final String DATE_PASSAGE_CA_KEY = DATE_PASSAGE_CA.key();
	public static final String DATE_PASSAGE_CS_KEY = DATE_PASSAGE_CS.key();
	public static final String DATE_RECEPTION_DOSSIER_DRV_KEY = DATE_RECEPTION_DOSSIER_DRV.key();
	public static final String DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE_KEY = DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE.key();
	public static final String DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR_KEY = DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String DEBUT_VISITE_AERES_KEY = DEBUT_VISITE_AERES.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String DOSSIER_COMPLET_KEY = DOSSIER_COMPLET.key();
	public static final String FIN_VISITE_AERES_KEY = FIN_VISITE_AERES.key();
	public static final String NOTE_AERES_KEY = NOTE_AERES.key();
	public static final String NOTE_APPRECIATION_DU_SUJET_KEY = NOTE_APPRECIATION_DU_SUJET.key();
	public static final String NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION_KEY = NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION.key();
	public static final String NOTE_RAYONNEMENT_ATTRACTIVITE_KEY = NOTE_RAYONNEMENT_ATTRACTIVITE.key();
	public static final String NOTE_STRATEGIE_GOUVERNANCE_KEY = NOTE_STRATEGIE_GOUVERNANCE.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String TITRE_KEY = TITRE.key();

	// Attributs non visibles
	public static final String ALRM_MI_PARCOURS_DEST_NO_IND_KEY = "alrmMiParcoursDestNoInd";
	public static final String COU_NUM_AVIS_DEF_MSTP_KEY = "couNumAvisDefMstp";
	public static final String COU_NUM_RAPPORT_AERES_KEY = "couNumRapportAeres";
	public static final String COU_NUM_REPONSE_AERES_KEY = "couNumReponseAeres";
	public static final String C_STRUCTURE_UNITE_KEY = "cStructureUnite";
	public static final String EVAL_MESR_ID_KEY = "evalMesrId";
	public static final String LABEL_DEMANDE_ID_KEY = "labelDemandeId";
	public static final String LABEL_OBTENU_ID_KEY = "labelObtenuId";

	//Colonnes dans la base de donnees
	public static final String ALRM_MI_PARCOURS_DATE_COLKEY = "ALRM_MI_PARCOURS_DATE";
	public static final String ALRM_MI_PARCOURS_TEXTE_COLKEY = "ALRM_MI_PARCOURS_TXT";
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String AVIS_CA_COLKEY = "AVIS_CA";
	public static final String AVIS_CS_COLKEY = "AVIS_CS";
	public static final String DATE_ENVOI_AERES_COLKEY = "DATE_ENVOI_AERES";
	public static final String DATE_ENVOI_MESR_COLKEY = "DATE_ENVOI_MESR";
	public static final String DATE_PASSAGE_CA_COLKEY = "DATE_PASSAGE_CA";
	public static final String DATE_PASSAGE_CS_COLKEY = "DATE_PASSAGE_CS";
	public static final String DATE_RECEPTION_DOSSIER_DRV_COLKEY = "DATE_RECEPTION_DOSSIER_DRV";
	public static final String DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE_COLKEY = "DATE_RECEPTION_NIV_INTER";
	public static final String DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR_COLKEY = "DATE_RECEPTION_NIV_SUP";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEBUT_VISITE_AERES_COLKEY = "DEBUT_VISITE_AERES";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DOSSIER_COMPLET_COLKEY = "DOSSIER_COMPLET";
	public static final String FIN_VISITE_AERES_COLKEY = "FIN_VISITE_AERES";
	public static final String NOTE_AERES_COLKEY = "NOTE_AERES";
	public static final String NOTE_APPRECIATION_DU_SUJET_COLKEY = "NOTE_APPREC_SUJ";
	public static final String NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION_COLKEY = "NOTE_CAL_SCIENT_PROD";
	public static final String NOTE_RAYONNEMENT_ATTRACTIVITE_COLKEY = "NOTE_RAY_ATTR";
	public static final String NOTE_STRATEGIE_GOUVERNANCE_COLKEY = "NOTE_START_GOUV";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TITRE_COLKEY = "TITRE";

	public static final String ALRM_MI_PARCOURS_DEST_NO_IND_COLKEY = "ALRM_MI_PARCOURS_DEST";
	public static final String COU_NUM_AVIS_DEF_MSTP_COLKEY = "COU_AVIS_DEF_MSTP";
	public static final String COU_NUM_RAPPORT_AERES_COLKEY = "COU_RAPPORT_AERES";
	public static final String COU_NUM_REPONSE_AERES_COLKEY = "COU_REPONSE_AERES";
	public static final String C_STRUCTURE_UNITE_COLKEY = "C_STRUCT_UNITE";
	public static final String EVAL_MESR_ID_COLKEY = "EVAL_MESR_ID";
	public static final String LABEL_DEMANDE_ID_COLKEY = "LBL_DEMANDE";
	public static final String LABEL_OBTENU_ID_COLKEY = "LBL_OBTENU";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> ALRM_MI_PARCOURS_DESTINATAIRE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("alrmMiParcoursDestinataire");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> AVIS_DEFINITIF_MSTP = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("avisDefinitifMstp");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> EVALUATION_DOCUMENTS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument>("evaluationDocuments");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> EVALUATION_NOTE_EQUIPES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe>("evaluationNoteEquipes");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOLabels> LABEL_DEMANDE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOLabels>("labelDemande");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOLabels> LABEL_OBTENU = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOLabels>("labelObtenu");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> RAPPORT_AERES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("rapportAeres");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> REPONSE_AERES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("reponseAeres");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> UNITE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("unite");
	
	public static final String ALRM_MI_PARCOURS_DESTINATAIRE_KEY = ALRM_MI_PARCOURS_DESTINATAIRE.key();
	public static final String AVIS_DEFINITIF_MSTP_KEY = AVIS_DEFINITIF_MSTP.key();
	public static final String EVALUATION_DOCUMENTS_KEY = EVALUATION_DOCUMENTS.key();
	public static final String EVALUATION_NOTE_EQUIPES_KEY = EVALUATION_NOTE_EQUIPES.key();
	public static final String LABEL_DEMANDE_KEY = LABEL_DEMANDE.key();
	public static final String LABEL_OBTENU_KEY = LABEL_OBTENU.key();
	public static final String RAPPORT_AERES_KEY = RAPPORT_AERES.key();
	public static final String REPONSE_AERES_KEY = REPONSE_AERES.key();
	public static final String UNITE_KEY = UNITE.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp alrmMiParcoursDate() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(ALRM_MI_PARCOURS_DATE_KEY);
	}

	public void setAlrmMiParcoursDate(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, ALRM_MI_PARCOURS_DATE_KEY);
	}
	
	public java.lang.String alrmMiParcoursTexte() {
		return (java.lang.String) storedValueForKey(ALRM_MI_PARCOURS_TEXTE_KEY);
	}

	public void setAlrmMiParcoursTexte(java.lang.String value) {
	    takeStoredValueForKey(value, ALRM_MI_PARCOURS_TEXTE_KEY);
	}
	
	public java.lang.Integer annee() {
		return (java.lang.Integer) storedValueForKey(ANNEE_KEY);
	}

	public void setAnnee(java.lang.Integer value) {
	    takeStoredValueForKey(value, ANNEE_KEY);
	}
	
	public java.lang.String avisCa() {
		return (java.lang.String) storedValueForKey(AVIS_CA_KEY);
	}

	public void setAvisCa(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_CA_KEY);
	}
	
	public java.lang.String avisCs() {
		return (java.lang.String) storedValueForKey(AVIS_CS_KEY);
	}

	public void setAvisCs(java.lang.String value) {
	    takeStoredValueForKey(value, AVIS_CS_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateEnvoiAeres() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ENVOI_AERES_KEY);
	}

	public void setDateEnvoiAeres(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ENVOI_AERES_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateEnvoiMesr() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ENVOI_MESR_KEY);
	}

	public void setDateEnvoiMesr(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ENVOI_MESR_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePassageCa() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PASSAGE_CA_KEY);
	}

	public void setDatePassageCa(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PASSAGE_CA_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp datePassageCs() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_PASSAGE_CS_KEY);
	}

	public void setDatePassageCs(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_PASSAGE_CS_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateReceptionDossierDrv() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_RECEPTION_DOSSIER_DRV_KEY);
	}

	public void setDateReceptionDossierDrv(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_RECEPTION_DOSSIER_DRV_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateReceptionDossierNiveauIntermediaire() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE_KEY);
	}

	public void setDateReceptionDossierNiveauIntermediaire(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_RECEPTION_DOSSIER_NIVEAU_INTERMEDIAIRE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateReceptionDossierNiveauSuperieur() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR_KEY);
	}

	public void setDateReceptionDossierNiveauSuperieur(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_RECEPTION_DOSSIER_NIVEAU_SUPERIEUR_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp debutVisiteAeres() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DEBUT_VISITE_AERES_KEY);
	}

	public void setDebutVisiteAeres(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DEBUT_VISITE_AERES_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Integer dossierComplet() {
		return (java.lang.Integer) storedValueForKey(DOSSIER_COMPLET_KEY);
	}

	public void setDossierComplet(java.lang.Integer value) {
	    takeStoredValueForKey(value, DOSSIER_COMPLET_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp finVisiteAeres() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(FIN_VISITE_AERES_KEY);
	}

	public void setFinVisiteAeres(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, FIN_VISITE_AERES_KEY);
	}
	
	public java.lang.String noteAeres() {
		return (java.lang.String) storedValueForKey(NOTE_AERES_KEY);
	}

	public void setNoteAeres(java.lang.String value) {
	    takeStoredValueForKey(value, NOTE_AERES_KEY);
	}
	
	public java.lang.String noteAppreciationDuSujet() {
		return (java.lang.String) storedValueForKey(NOTE_APPRECIATION_DU_SUJET_KEY);
	}

	public void setNoteAppreciationDuSujet(java.lang.String value) {
	    takeStoredValueForKey(value, NOTE_APPRECIATION_DU_SUJET_KEY);
	}
	
	public java.lang.String noteQualiteScientifiqueProduction() {
		return (java.lang.String) storedValueForKey(NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION_KEY);
	}

	public void setNoteQualiteScientifiqueProduction(java.lang.String value) {
	    takeStoredValueForKey(value, NOTE_QUALITE_SCIENTIFIQUE_PRODUCTION_KEY);
	}
	
	public java.lang.String noteRayonnementAttractivite() {
		return (java.lang.String) storedValueForKey(NOTE_RAYONNEMENT_ATTRACTIVITE_KEY);
	}

	public void setNoteRayonnementAttractivite(java.lang.String value) {
	    takeStoredValueForKey(value, NOTE_RAYONNEMENT_ATTRACTIVITE_KEY);
	}
	
	public java.lang.String noteStrategieGouvernance() {
		return (java.lang.String) storedValueForKey(NOTE_STRATEGIE_GOUVERNANCE_KEY);
	}

	public void setNoteStrategieGouvernance(java.lang.String value) {
	    takeStoredValueForKey(value, NOTE_STRATEGIE_GOUVERNANCE_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.String titre() {
		return (java.lang.String) storedValueForKey(TITRE_KEY);
	}

	public void setTitre(java.lang.String value) {
	    takeStoredValueForKey(value, TITRE_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu alrmMiParcoursDestinataire() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu) storedValueForKey(ALRM_MI_PARCOURS_DESTINATAIRE_KEY);
	}

	public void setAlrmMiParcoursDestinataireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = alrmMiParcoursDestinataire();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ALRM_MI_PARCOURS_DESTINATAIRE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, ALRM_MI_PARCOURS_DESTINATAIRE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlged.serveur.metier.EODocument avisDefinitifMstp() {
	    return (org.cocktail.fwkcktlged.serveur.metier.EODocument) storedValueForKey(AVIS_DEFINITIF_MSTP_KEY);
	}

	public void setAvisDefinitifMstpRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = avisDefinitifMstp();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AVIS_DEFINITIF_MSTP_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, AVIS_DEFINITIF_MSTP_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOLabels labelDemande() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOLabels) storedValueForKey(LABEL_DEMANDE_KEY);
	}

	public void setLabelDemandeRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOLabels value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOLabels oldValue = labelDemande();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LABEL_DEMANDE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, LABEL_DEMANDE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOLabels labelObtenu() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOLabels) storedValueForKey(LABEL_OBTENU_KEY);
	}

	public void setLabelObtenuRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOLabels value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOLabels oldValue = labelObtenu();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LABEL_OBTENU_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, LABEL_OBTENU_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlged.serveur.metier.EODocument rapportAeres() {
	    return (org.cocktail.fwkcktlged.serveur.metier.EODocument) storedValueForKey(RAPPORT_AERES_KEY);
	}

	public void setRapportAeresRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = rapportAeres();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RAPPORT_AERES_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, RAPPORT_AERES_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlged.serveur.metier.EODocument reponseAeres() {
	    return (org.cocktail.fwkcktlged.serveur.metier.EODocument) storedValueForKey(REPONSE_AERES_KEY);
	}

	public void setReponseAeresRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = reponseAeres();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REPONSE_AERES_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, REPONSE_AERES_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOStructure unite() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure) storedValueForKey(UNITE_KEY);
	}

	public void setUniteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = unite();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UNITE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, UNITE_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> evaluationDocuments() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(EVALUATION_DOCUMENTS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> evaluationDocuments(EOQualifier qualifier) {
		return evaluationDocuments(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> evaluationDocuments(EOQualifier qualifier, boolean fetch) {
	    return evaluationDocuments(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> evaluationDocuments(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument.EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = evaluationDocuments();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToEvaluationDocumentsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument object) {
		addObjectToBothSidesOfRelationshipWithKey(object, EVALUATION_DOCUMENTS_KEY);
	}

	public void removeFromEvaluationDocumentsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, EVALUATION_DOCUMENTS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument createEvaluationDocumentsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EvaluationDocument");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, EVALUATION_DOCUMENTS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument) eo;
	}

	public void deleteEvaluationDocumentsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, EVALUATION_DOCUMENTS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllEvaluationDocumentsRelationships() {
		java.util.Enumeration objects = evaluationDocuments().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteEvaluationDocumentsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationDocument) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> evaluationNoteEquipes() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(EVALUATION_NOTE_EQUIPES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> evaluationNoteEquipes(EOQualifier qualifier) {
		return evaluationNoteEquipes(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> evaluationNoteEquipes(EOQualifier qualifier, boolean fetch) {
	    return evaluationNoteEquipes(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> evaluationNoteEquipes(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe.EVALUATION_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = evaluationNoteEquipes();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToEvaluationNoteEquipesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe object) {
		addObjectToBothSidesOfRelationshipWithKey(object, EVALUATION_NOTE_EQUIPES_KEY);
	}

	public void removeFromEvaluationNoteEquipesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, EVALUATION_NOTE_EQUIPES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe createEvaluationNoteEquipesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EvaluationNoteEquipe");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, EVALUATION_NOTE_EQUIPES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe) eo;
	}

	public void deleteEvaluationNoteEquipesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, EVALUATION_NOTE_EQUIPES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllEvaluationNoteEquipesRelationships() {
		java.util.Enumeration objects = evaluationNoteEquipes().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteEvaluationNoteEquipesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationNoteEquipe) objects.nextElement());
	    }
	}

	public static EOEvaluationMesr createEvaluationMesr(EOEditingContext editingContext, java.lang.Integer annee, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, java.lang.String titre, org.cocktail.fwkcktlpersonne.common.metier.EOStructure unite) {
		EOEvaluationMesr eo = (EOEvaluationMesr) EOUtilities.createAndInsertInstance(editingContext, _EOEvaluationMesr.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setTitre(titre);
		eo.setUniteRelationship(unite);
		return eo;
	}

	public EOEvaluationMesr localInstanceIn(EOEditingContext editingContext) {
		return (EOEvaluationMesr) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOEvaluationMesr creerInstance(EOEditingContext editingContext) {
		return (EOEvaluationMesr) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOEvaluationMesr.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOEvaluationMesr> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOEvaluationMesr>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOEvaluationMesr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEvaluationMesr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOEvaluationMesr> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEvaluationMesr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOEvaluationMesr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOEvaluationMesr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOEvaluationMesr> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEvaluationMesr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOEvaluationMesr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOEvaluationMesr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOEvaluationMesr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOEvaluationMesr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAllEvaluationMesrs(EOEditingContext editingContext) {
		return _EOEvaluationMesr.fetchAllEvaluationMesrs(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchAllEvaluationMesrs(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOEvaluationMesr.fetchEvaluationMesrs(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOEvaluationMesr> fetchEvaluationMesrs(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOEvaluationMesr> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOEvaluationMesr>(_EOEvaluationMesr.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOEvaluationMesr> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOEvaluationMesr fetchEvaluationMesr(EOEditingContext editingContext, String keyName, Object value) {
		return _EOEvaluationMesr.fetchEvaluationMesr(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOEvaluationMesr fetchEvaluationMesr(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOEvaluationMesr> eoObjects = _EOEvaluationMesr.fetchEvaluationMesrs(editingContext, qualifier, null);
	    EOEvaluationMesr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one EvaluationMesr that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
