/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartTypeResultatsContrat.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartTypeResultatsContrat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartTypeResultatsContrat";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "rtrcOrdre";

	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_RESULTATS_CONTRAT = new er.extensions.eof.ERXKey<java.lang.String>("typeResultatsContrat");
	
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String TYPE_RESULTATS_CONTRAT_KEY = TYPE_RESULTATS_CONTRAT.key();

	// Attributs non visibles
	public static final String CONR_ORDRE_KEY = "conrOrdre";
	public static final String RTRC_ORDRE_KEY = "rtrcOrdre";

	//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TYPE_RESULTATS_CONTRAT_COLKEY = "TYPE_RES";

	public static final String CONR_ORDRE_COLKEY = "CONR_ORDRE";
	public static final String RTRC_ORDRE_COLKEY = "RTRC_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche> CONTRAT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche>("contrat");
	
	public static final String CONTRAT_KEY = CONTRAT.key();

	// Accessors methods
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.String typeResultatsContrat() {
		return (java.lang.String) storedValueForKey(TYPE_RESULTATS_CONTRAT_KEY);
	}

	public void setTypeResultatsContrat(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_RESULTATS_CONTRAT_KEY);
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche contrat() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche) storedValueForKey(CONTRAT_KEY);
	}

	public void setContratRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche oldValue = contrat();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
	    }
	}
	
	public static EORepartTypeResultatsContrat createRepartTypeResultatsContrat(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, java.lang.String typeResultatsContrat, org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche contrat) {
		EORepartTypeResultatsContrat eo = (EORepartTypeResultatsContrat) EOUtilities.createAndInsertInstance(editingContext, _EORepartTypeResultatsContrat.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setTypeResultatsContrat(typeResultatsContrat);
		eo.setContratRelationship(contrat);
		return eo;
	}

	public EORepartTypeResultatsContrat localInstanceIn(EOEditingContext editingContext) {
		return (EORepartTypeResultatsContrat) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartTypeResultatsContrat creerInstance(EOEditingContext editingContext) {
		return (EORepartTypeResultatsContrat) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartTypeResultatsContrat.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartTypeResultatsContrat> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartTypeResultatsContrat>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartTypeResultatsContrat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartTypeResultatsContrat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartTypeResultatsContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartTypeResultatsContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartTypeResultatsContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartTypeResultatsContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartTypeResultatsContrat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartTypeResultatsContrat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartTypeResultatsContrat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartTypeResultatsContrat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAllRepartTypeResultatsContrats(EOEditingContext editingContext) {
		return _EORepartTypeResultatsContrat.fetchAllRepartTypeResultatsContrats(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchAllRepartTypeResultatsContrats(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartTypeResultatsContrat.fetchRepartTypeResultatsContrats(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> fetchRepartTypeResultatsContrats(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartTypeResultatsContrat> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartTypeResultatsContrat>(_EORepartTypeResultatsContrat.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartTypeResultatsContrat fetchRepartTypeResultatsContrat(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartTypeResultatsContrat.fetchRepartTypeResultatsContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartTypeResultatsContrat fetchRepartTypeResultatsContrat(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartTypeResultatsContrat> eoObjects = _EORepartTypeResultatsContrat.fetchRepartTypeResultatsContrats(editingContext, qualifier, null);
	    EORepartTypeResultatsContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartTypeResultatsContrat that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
