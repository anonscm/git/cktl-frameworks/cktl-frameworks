/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVScolInscriptionEtudiant.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOVScolInscriptionEtudiant extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "VScolInscriptionEtudiant";

	// Attributes

	public static final er.extensions.eof.ERXKey<java.lang.String> ABREVIATION = new er.extensions.eof.ERXKey<java.lang.String>("abreviation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> FANN_KEY = new er.extensions.eof.ERXKey<java.lang.Integer>("fannKey");
	public static final er.extensions.eof.ERXKey<java.lang.String> FDIP_CODE = new er.extensions.eof.ERXKey<java.lang.String>("fdipCode");
	public static final er.extensions.eof.ERXKey<java.lang.String> FDOM_CODE = new er.extensions.eof.ERXKey<java.lang.String>("fdomCode");
	public static final er.extensions.eof.ERXKey<java.lang.String> FGRA_CODE = new er.extensions.eof.ERXKey<java.lang.String>("fgraCode");
	public static final er.extensions.eof.ERXKey<java.lang.String> FORMATION = new er.extensions.eof.ERXKey<java.lang.String>("formation");
	public static final er.extensions.eof.ERXKey<java.lang.String> GRADE = new er.extensions.eof.ERXKey<java.lang.String>("grade");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> HIST_NUMERO = new er.extensions.eof.ERXKey<java.lang.Integer>("histNumero");
	public static final er.extensions.eof.ERXKey<java.lang.String> MENTION = new er.extensions.eof.ERXKey<java.lang.String>("mention");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> NIVEAU = new er.extensions.eof.ERXKey<java.lang.Integer>("niveau");
	public static final er.extensions.eof.ERXKey<java.lang.String> SALARIE = new er.extensions.eof.ERXKey<java.lang.String>("salarie");
	public static final er.extensions.eof.ERXKey<java.lang.String> SPECIALISATION = new er.extensions.eof.ERXKey<java.lang.String>("specialisation");
	public static final er.extensions.eof.ERXKey<java.lang.String> SPECIALITE = new er.extensions.eof.ERXKey<java.lang.String>("specialite");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_INSCRIPTION = new er.extensions.eof.ERXKey<java.lang.String>("typeInscription");
	
	public static final String ABREVIATION_KEY = ABREVIATION.key();
	public static final String FANN_KEY_KEY = FANN_KEY.key();
	public static final String FDIP_CODE_KEY = FDIP_CODE.key();
	public static final String FDOM_CODE_KEY = FDOM_CODE.key();
	public static final String FGRA_CODE_KEY = FGRA_CODE.key();
	public static final String FORMATION_KEY = FORMATION.key();
	public static final String GRADE_KEY = GRADE.key();
	public static final String HIST_NUMERO_KEY = HIST_NUMERO.key();
	public static final String MENTION_KEY = MENTION.key();
	public static final String NIVEAU_KEY = NIVEAU.key();
	public static final String SALARIE_KEY = SALARIE.key();
	public static final String SPECIALISATION_KEY = SPECIALISATION.key();
	public static final String SPECIALITE_KEY = SPECIALITE.key();
	public static final String TYPE_INSCRIPTION_KEY = TYPE_INSCRIPTION.key();

	// Attributs non visibles
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String FSPN_KEY_KEY = "fspnKey";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	//Colonnes dans la base de donnees
	public static final String ABREVIATION_COLKEY = "ABREVIATION";
	public static final String FANN_KEY_COLKEY = "FANN_KEY";
	public static final String FDIP_CODE_COLKEY = "FDIP_CODE";
	public static final String FDOM_CODE_COLKEY = "FDOM_CODE";
	public static final String FGRA_CODE_COLKEY = "FGRA_CODE";
	public static final String FORMATION_COLKEY = "FORMATION";
	public static final String GRADE_COLKEY = "GRADE";
	public static final String HIST_NUMERO_COLKEY = "HIST_NUMERO";
	public static final String MENTION_COLKEY = "MENTION";
	public static final String NIVEAU_COLKEY = "NIVEAU";
	public static final String SALARIE_COLKEY = "SALARIE";
	public static final String SPECIALISATION_COLKEY = "SPECIALISATION";
	public static final String SPECIALITE_COLKEY = "SPECIALITE";
	public static final String TYPE_INSCRIPTION_COLKEY = "TYPE_INSCRIPTION";

	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String FSPN_KEY_COLKEY = "FSPN_KEY";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT_FWKPERS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiantFwkpers");
	
	public static final String TO_ETUDIANT_FWKPERS_KEY = TO_ETUDIANT_FWKPERS.key();

	// Accessors methods
	public java.lang.String abreviation() {
		return (java.lang.String) storedValueForKey(ABREVIATION_KEY);
	}

	public void setAbreviation(java.lang.String value) {
	    takeStoredValueForKey(value, ABREVIATION_KEY);
	}
	
	public java.lang.Integer fannKey() {
		return (java.lang.Integer) storedValueForKey(FANN_KEY_KEY);
	}

	public void setFannKey(java.lang.Integer value) {
	    takeStoredValueForKey(value, FANN_KEY_KEY);
	}
	
	public java.lang.String fdipCode() {
		return (java.lang.String) storedValueForKey(FDIP_CODE_KEY);
	}

	public void setFdipCode(java.lang.String value) {
	    takeStoredValueForKey(value, FDIP_CODE_KEY);
	}
	
	public java.lang.String fdomCode() {
		return (java.lang.String) storedValueForKey(FDOM_CODE_KEY);
	}

	public void setFdomCode(java.lang.String value) {
	    takeStoredValueForKey(value, FDOM_CODE_KEY);
	}
	
	public java.lang.String fgraCode() {
		return (java.lang.String) storedValueForKey(FGRA_CODE_KEY);
	}

	public void setFgraCode(java.lang.String value) {
	    takeStoredValueForKey(value, FGRA_CODE_KEY);
	}
	
	public java.lang.String formation() {
		return (java.lang.String) storedValueForKey(FORMATION_KEY);
	}

	public void setFormation(java.lang.String value) {
	    takeStoredValueForKey(value, FORMATION_KEY);
	}
	
	public java.lang.String grade() {
		return (java.lang.String) storedValueForKey(GRADE_KEY);
	}

	public void setGrade(java.lang.String value) {
	    takeStoredValueForKey(value, GRADE_KEY);
	}
	
	public java.lang.Integer histNumero() {
		return (java.lang.Integer) storedValueForKey(HIST_NUMERO_KEY);
	}

	public void setHistNumero(java.lang.Integer value) {
	    takeStoredValueForKey(value, HIST_NUMERO_KEY);
	}
	
	public java.lang.String mention() {
		return (java.lang.String) storedValueForKey(MENTION_KEY);
	}

	public void setMention(java.lang.String value) {
	    takeStoredValueForKey(value, MENTION_KEY);
	}
	
	public java.lang.Integer niveau() {
		return (java.lang.Integer) storedValueForKey(NIVEAU_KEY);
	}

	public void setNiveau(java.lang.Integer value) {
	    takeStoredValueForKey(value, NIVEAU_KEY);
	}
	
	public java.lang.String salarie() {
		return (java.lang.String) storedValueForKey(SALARIE_KEY);
	}

	public void setSalarie(java.lang.String value) {
	    takeStoredValueForKey(value, SALARIE_KEY);
	}
	
	public java.lang.String specialisation() {
		return (java.lang.String) storedValueForKey(SPECIALISATION_KEY);
	}

	public void setSpecialisation(java.lang.String value) {
	    takeStoredValueForKey(value, SPECIALISATION_KEY);
	}
	
	public java.lang.String specialite() {
		return (java.lang.String) storedValueForKey(SPECIALITE_KEY);
	}

	public void setSpecialite(java.lang.String value) {
	    takeStoredValueForKey(value, SPECIALITE_KEY);
	}
	
	public java.lang.String typeInscription() {
		return (java.lang.String) storedValueForKey(TYPE_INSCRIPTION_KEY);
	}

	public void setTypeInscription(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_INSCRIPTION_KEY);
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiantFwkpers() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant) storedValueForKey(TO_ETUDIANT_FWKPERS_KEY);
	}

	public void setToEtudiantFwkpersRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiantFwkpers();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETUDIANT_FWKPERS_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETUDIANT_FWKPERS_KEY);
	    }
	}
	
	public static EOVScolInscriptionEtudiant createVScolInscriptionEtudiant(EOEditingContext editingContext, java.lang.Integer fannKey, java.lang.String fdipCode, java.lang.String fdomCode, java.lang.String fgraCode, java.lang.String formation, java.lang.Integer histNumero, java.lang.String mention, java.lang.Integer niveau, java.lang.String salarie, org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiantFwkpers) {
		EOVScolInscriptionEtudiant eo = (EOVScolInscriptionEtudiant) EOUtilities.createAndInsertInstance(editingContext, _EOVScolInscriptionEtudiant.ENTITY_NAME);    
		eo.setFannKey(fannKey);
		eo.setFdipCode(fdipCode);
		eo.setFdomCode(fdomCode);
		eo.setFgraCode(fgraCode);
		eo.setFormation(formation);
		eo.setHistNumero(histNumero);
		eo.setMention(mention);
		eo.setNiveau(niveau);
		eo.setSalarie(salarie);
		eo.setToEtudiantFwkpersRelationship(toEtudiantFwkpers);
		return eo;
	}

	public EOVScolInscriptionEtudiant localInstanceIn(EOEditingContext editingContext) {
		return (EOVScolInscriptionEtudiant) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOVScolInscriptionEtudiant creerInstance(EOEditingContext editingContext) {
		return (EOVScolInscriptionEtudiant) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOVScolInscriptionEtudiant.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOVScolInscriptionEtudiant> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOVScolInscriptionEtudiant>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOVScolInscriptionEtudiant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVScolInscriptionEtudiant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVScolInscriptionEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOVScolInscriptionEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOVScolInscriptionEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVScolInscriptionEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOVScolInscriptionEtudiant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOVScolInscriptionEtudiant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOVScolInscriptionEtudiant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOVScolInscriptionEtudiant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAllVScolInscriptionEtudiants(EOEditingContext editingContext) {
		return _EOVScolInscriptionEtudiant.fetchAllVScolInscriptionEtudiants(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchAllVScolInscriptionEtudiants(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOVScolInscriptionEtudiant.fetchVScolInscriptionEtudiants(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> fetchVScolInscriptionEtudiants(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOVScolInscriptionEtudiant> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOVScolInscriptionEtudiant>(_EOVScolInscriptionEtudiant.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOVScolInscriptionEtudiant fetchVScolInscriptionEtudiant(EOEditingContext editingContext, String keyName, Object value) {
		return _EOVScolInscriptionEtudiant.fetchVScolInscriptionEtudiant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOVScolInscriptionEtudiant fetchVScolInscriptionEtudiant(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOVScolInscriptionEtudiant> eoObjects = _EOVScolInscriptionEtudiant.fetchVScolInscriptionEtudiants(editingContext, qualifier, null);
	    EOVScolInscriptionEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one VScolInscriptionEtudiant that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
