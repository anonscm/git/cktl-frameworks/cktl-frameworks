/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartGrandSecteurDisciplinaireDomaineScientifique.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EORepartGrandSecteurDisciplinaireDomaineScientifique extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RepartGrandSecteurDisciplinaireDomaineScientifique";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "gsdsId";

	

	// Attributs non visibles
	public static final String DS_ID_KEY = "dsId";
	public static final String GSD_ID_KEY = "gsdId";
	public static final String GSDS_ID_KEY = "gsdsId";

	//Colonnes dans la base de donnees

	public static final String DS_ID_COLKEY = "DS_ID";
	public static final String GSD_ID_COLKEY = "GSD_ID";
	public static final String GSDS_ID_COLKEY = "GSDS_ID";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire> GRAND_SECTEUR_DISCIPLINAIRE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire>("grandSecteurDisciplinaire");
	
	public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();
	public static final String GRAND_SECTEUR_DISCIPLINAIRE_KEY = GRAND_SECTEUR_DISCIPLINAIRE.key();

	// Accessors methods
	public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) storedValueForKey(DOMAINE_SCIENTIFIQUE_KEY);
	}

	public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOMAINE_SCIENTIFIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_SCIENTIFIQUE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire grandSecteurDisciplinaire() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire) storedValueForKey(GRAND_SECTEUR_DISCIPLINAIRE_KEY);
	}

	public void setGrandSecteurDisciplinaireRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire oldValue = grandSecteurDisciplinaire();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRAND_SECTEUR_DISCIPLINAIRE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, GRAND_SECTEUR_DISCIPLINAIRE_KEY);
	    }
	}
	
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique createRepartGrandSecteurDisciplinaireDomaineScientifique(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique, org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire grandSecteurDisciplinaire) {
		EORepartGrandSecteurDisciplinaireDomaineScientifique eo = (EORepartGrandSecteurDisciplinaireDomaineScientifique) EOUtilities.createAndInsertInstance(editingContext, _EORepartGrandSecteurDisciplinaireDomaineScientifique.ENTITY_NAME);    
		eo.setDomaineScientifiqueRelationship(domaineScientifique);
		eo.setGrandSecteurDisciplinaireRelationship(grandSecteurDisciplinaire);
		return eo;
	}

	public EORepartGrandSecteurDisciplinaireDomaineScientifique localInstanceIn(EOEditingContext editingContext) {
		return (EORepartGrandSecteurDisciplinaireDomaineScientifique) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique creerInstance(EOEditingContext editingContext) {
		return (EORepartGrandSecteurDisciplinaireDomaineScientifique) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EORepartGrandSecteurDisciplinaireDomaineScientifique.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartGrandSecteurDisciplinaireDomaineScientifique>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartGrandSecteurDisciplinaireDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartGrandSecteurDisciplinaireDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EORepartGrandSecteurDisciplinaireDomaineScientifique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EORepartGrandSecteurDisciplinaireDomaineScientifique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAllRepartGrandSecteurDisciplinaireDomaineScientifiques(EOEditingContext editingContext) {
		return _EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchAllRepartGrandSecteurDisciplinaireDomaineScientifiques(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchAllRepartGrandSecteurDisciplinaireDomaineScientifiques(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchRepartGrandSecteurDisciplinaireDomaineScientifiques(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchRepartGrandSecteurDisciplinaireDomaineScientifiques(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EORepartGrandSecteurDisciplinaireDomaineScientifique> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EORepartGrandSecteurDisciplinaireDomaineScientifique>(_EORepartGrandSecteurDisciplinaireDomaineScientifique.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchRepartGrandSecteurDisciplinaireDomaineScientifique(EOEditingContext editingContext, String keyName, Object value) {
		return _EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchRepartGrandSecteurDisciplinaireDomaineScientifique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EORepartGrandSecteurDisciplinaireDomaineScientifique fetchRepartGrandSecteurDisciplinaireDomaineScientifique(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> eoObjects = _EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchRepartGrandSecteurDisciplinaireDomaineScientifiques(editingContext, qualifier, null);
	    EORepartGrandSecteurDisciplinaireDomaineScientifique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one RepartGrandSecteurDisciplinaireDomaineScientifique that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
