/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOContratRecherche.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOContratRecherche extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "ContratRecherche";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "conrOrdre";

	public static final er.extensions.eof.ERXKey<java.lang.Boolean> CONTRAT_SUPPRIME = new er.extensions.eof.ERXKey<java.lang.Boolean>("contratSupprime");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_ARCHIVAGE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateArchivage");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_NOTIFIATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateNotifiation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dCreation");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dModification");
	public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> D_OUVERTURE_DOSSIER = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dOuvertureDossier");
	public static final er.extensions.eof.ERXKey<java.lang.String> EXPLOITATION_DES_RESULTATS = new er.extensions.eof.ERXKey<java.lang.String>("exploitationDesResultats");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> MONTANT_AUGMENTATION = new er.extensions.eof.ERXKey<java.lang.Integer>("montantAugmentation");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_DEMANDE = new er.extensions.eof.ERXKey<java.lang.Float>("montantDemande");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_OBTENU = new er.extensions.eof.ERXKey<java.lang.Float>("montantObtenu");
	public static final er.extensions.eof.ERXKey<java.lang.Float> MONTANT_PRELEVEMENT_EFFECTUE = new er.extensions.eof.ERXKey<java.lang.Float>("montantPrelevementEffectue");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> PARTICIPATION_DAJ_NEGOCIATIONS = new er.extensions.eof.ERXKey<java.lang.Boolean>("participationDajNegociations");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_CREATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdCreation");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> PERS_ID_MODIFICATION = new er.extensions.eof.ERXKey<java.lang.Integer>("persIdModification");
	public static final er.extensions.eof.ERXKey<java.lang.String> PROPRIETE_DES_RESULTATS = new er.extensions.eof.ERXKey<java.lang.String>("proprieteDesResultats");
	public static final er.extensions.eof.ERXKey<java.lang.String> REDACTION_CLAUSES_PI = new er.extensions.eof.ERXKey<java.lang.String>("redactionClausesPi");
	public static final er.extensions.eof.ERXKey<java.lang.String> REDACTION_CONTRAT = new er.extensions.eof.ERXKey<java.lang.String>("redactionContrat");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> REDEVANCES = new er.extensions.eof.ERXKey<java.lang.Boolean>("redevances");
	public static final er.extensions.eof.ERXKey<java.lang.String> SENS_MONTANT_FINANCIER = new er.extensions.eof.ERXKey<java.lang.String>("sensMontantFinancier");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> SOUMIS_TVA = new er.extensions.eof.ERXKey<java.lang.Boolean>("soumisTva");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> SUBVENTION_OBTENUE = new er.extensions.eof.ERXKey<java.lang.Boolean>("subventionObtenue");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUIVI_ADMINISTRATIF = new er.extensions.eof.ERXKey<java.lang.String>("suiviAdministratif");
	public static final er.extensions.eof.ERXKey<java.lang.String> SUIVI_VALO = new er.extensions.eof.ERXKey<java.lang.String>("suiviValo");
	public static final er.extensions.eof.ERXKey<java.lang.Float> TAUX_PRELEVEMENT_EFFECTUE = new er.extensions.eof.ERXKey<java.lang.Float>("tauxPrelevementEffectue");
	public static final er.extensions.eof.ERXKey<java.lang.Float> TAUX_TVA = new er.extensions.eof.ERXKey<java.lang.Float>("tauxTva");
	public static final er.extensions.eof.ERXKey<java.lang.String> TYPE_INTERVENTION_SUR_CONTRAT = new er.extensions.eof.ERXKey<java.lang.String>("typeInterventionSurContrat");
	public static final er.extensions.eof.ERXKey<java.lang.Boolean> UTILISATION_CONTRAT_TYPE_DAJ = new er.extensions.eof.ERXKey<java.lang.Boolean>("utilisationContratTypeDaj");
	
	public static final String CONTRAT_SUPPRIME_KEY = CONTRAT_SUPPRIME.key();
	public static final String DATE_ARCHIVAGE_KEY = DATE_ARCHIVAGE.key();
	public static final String DATE_NOTIFIATION_KEY = DATE_NOTIFIATION.key();
	public static final String D_CREATION_KEY = D_CREATION.key();
	public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
	public static final String D_OUVERTURE_DOSSIER_KEY = D_OUVERTURE_DOSSIER.key();
	public static final String EXPLOITATION_DES_RESULTATS_KEY = EXPLOITATION_DES_RESULTATS.key();
	public static final String MONTANT_AUGMENTATION_KEY = MONTANT_AUGMENTATION.key();
	public static final String MONTANT_DEMANDE_KEY = MONTANT_DEMANDE.key();
	public static final String MONTANT_OBTENU_KEY = MONTANT_OBTENU.key();
	public static final String MONTANT_PRELEVEMENT_EFFECTUE_KEY = MONTANT_PRELEVEMENT_EFFECTUE.key();
	public static final String PARTICIPATION_DAJ_NEGOCIATIONS_KEY = PARTICIPATION_DAJ_NEGOCIATIONS.key();
	public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
	public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
	public static final String PROPRIETE_DES_RESULTATS_KEY = PROPRIETE_DES_RESULTATS.key();
	public static final String REDACTION_CLAUSES_PI_KEY = REDACTION_CLAUSES_PI.key();
	public static final String REDACTION_CONTRAT_KEY = REDACTION_CONTRAT.key();
	public static final String REDEVANCES_KEY = REDEVANCES.key();
	public static final String SENS_MONTANT_FINANCIER_KEY = SENS_MONTANT_FINANCIER.key();
	public static final String SOUMIS_TVA_KEY = SOUMIS_TVA.key();
	public static final String SUBVENTION_OBTENUE_KEY = SUBVENTION_OBTENUE.key();
	public static final String SUIVI_ADMINISTRATIF_KEY = SUIVI_ADMINISTRATIF.key();
	public static final String SUIVI_VALO_KEY = SUIVI_VALO.key();
	public static final String TAUX_PRELEVEMENT_EFFECTUE_KEY = TAUX_PRELEVEMENT_EFFECTUE.key();
	public static final String TAUX_TVA_KEY = TAUX_TVA.key();
	public static final String TYPE_INTERVENTION_SUR_CONTRAT_KEY = TYPE_INTERVENTION_SUR_CONTRAT.key();
	public static final String UTILISATION_CONTRAT_TYPE_DAJ_KEY = UTILISATION_CONTRAT_TYPE_DAJ.key();

	// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String CONR_ORDRE_KEY = "conrOrdre";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_KEY = "domaineScientifiqueOrdre";
	public static final String PROGRAMME_ID_KEY = "programmeId";
	public static final String SOUS_PROGRAMME_ID_KEY = "sousProgrammeId";
	public static final String SUIVI_VALO_EVT_ID_KEY = "suiviValoEvtId";
	public static final String ZONE_GEOGRAPHIQUE_ORDRE_KEY = "zoneGeographiqueOrdre";

	//Colonnes dans la base de donnees
	public static final String CONTRAT_SUPPRIME_COLKEY = "CR_SUPPR";
	public static final String DATE_ARCHIVAGE_COLKEY = "DATE_ARCHIVAGE";
	public static final String DATE_NOTIFIATION_COLKEY = "DATE_NOTIF";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_DOSSIER_COLKEY = "DATE_OUVERT_DOSSIER";
	public static final String EXPLOITATION_DES_RESULTATS_COLKEY = "EXPLOITATION_RESULTATS";
	public static final String MONTANT_AUGMENTATION_COLKEY = "MONTANT_AUGMENTATION";
	public static final String MONTANT_DEMANDE_COLKEY = "MONTANT_DEMANDE";
	public static final String MONTANT_OBTENU_COLKEY = "MONTANT_OBT";
	public static final String MONTANT_PRELEVEMENT_EFFECTUE_COLKEY = "MONTANT_PRELEV_REFFECT";
	public static final String PARTICIPATION_DAJ_NEGOCIATIONS_COLKEY = "PARTICIAPATION_DAJ_NEGO";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PROPRIETE_DES_RESULTATS_COLKEY = "PROPRIETE_RESULTATS";
	public static final String REDACTION_CLAUSES_PI_COLKEY = "REDACTION_CLAUSES_PI";
	public static final String REDACTION_CONTRAT_COLKEY = "REDACTION_CONTRAT";
	public static final String REDEVANCES_COLKEY = "REDEVANCES";
	public static final String SENS_MONTANT_FINANCIER_COLKEY = "SENS_MONTANT_FINANCIER";
	public static final String SOUMIS_TVA_COLKEY = "SOUMIS_TVA";
	public static final String SUBVENTION_OBTENUE_COLKEY = "SUBVENTION_OBT";
	public static final String SUIVI_ADMINISTRATIF_COLKEY = "SUIVI_ADMIN";
	public static final String SUIVI_VALO_COLKEY = "SUIVI_VALO";
	public static final String TAUX_PRELEVEMENT_EFFECTUE_COLKEY = "TAUX_PRELEV_EFFECT";
	public static final String TAUX_TVA_COLKEY = "TAUX_TVA";
	public static final String TYPE_INTERVENTION_SUR_CONTRAT_COLKEY = "TYPE_INTERVENTION";
	public static final String UTILISATION_CONTRAT_TYPE_DAJ_COLKEY = "UTIL_CONTRAT_TYPE_DAJ";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String CONR_ORDRE_COLKEY = "CONR_ORDRE";
	public static final String DOMAINE_SCIENTIFIQUE_ORDRE_COLKEY = "DOMAINE_SCIENT_ORDRE";
	public static final String PROGRAMME_ID_COLKEY = "PROGRAMME_ID";
	public static final String SOUS_PROGRAMME_ID_COLKEY = "SS_PROGRAMME_ID";
	public static final String SUIVI_VALO_EVT_ID_COLKEY = "SUIVI_VALO_EVT_ID";
	public static final String ZONE_GEOGRAPHIQUE_ORDRE_COLKEY = "ZG_ORDRE";

	// Relationships
    public static final er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new er.extensions.eof.ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme> PROGRAMME = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme>("programme");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme> SOUS_PROGRAMME = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOProgramme>("sousProgramme");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement> SUIVI_VALO_EVENEMENT = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement>("suiviValoEvenement");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> TO_REPART_APPORT_DAJ_CONTRATS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont>("toRepartApportDajContrats");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> TO_REPART_APPORT_DRV_CONTRATS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont>("toRepartApportDrvContrats");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> TO_REPART_CONTRAT_LANGUES = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue>("toRepartContratLangues");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> TO_REPART_TYPE_RESULTATS_CONTRATS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat>("toRepartTypeResultatsContrats");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> TO_VALIDATION_CONTRATS = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat>("toValidationContrats");
    public static final er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique> ZONE_GEOGRAPHIQUE = new er.extensions.eof.ERXKey<org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique>("zoneGeographique");
	
	public static final String CONTRAT_KEY = CONTRAT.key();
	public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();
	public static final String PROGRAMME_KEY = PROGRAMME.key();
	public static final String SOUS_PROGRAMME_KEY = SOUS_PROGRAMME.key();
	public static final String SUIVI_VALO_EVENEMENT_KEY = SUIVI_VALO_EVENEMENT.key();
	public static final String TO_REPART_APPORT_DAJ_CONTRATS_KEY = TO_REPART_APPORT_DAJ_CONTRATS.key();
	public static final String TO_REPART_APPORT_DRV_CONTRATS_KEY = TO_REPART_APPORT_DRV_CONTRATS.key();
	public static final String TO_REPART_CONTRAT_LANGUES_KEY = TO_REPART_CONTRAT_LANGUES.key();
	public static final String TO_REPART_TYPE_RESULTATS_CONTRATS_KEY = TO_REPART_TYPE_RESULTATS_CONTRATS.key();
	public static final String TO_VALIDATION_CONTRATS_KEY = TO_VALIDATION_CONTRATS.key();
	public static final String ZONE_GEOGRAPHIQUE_KEY = ZONE_GEOGRAPHIQUE.key();

	// Accessors methods
	public java.lang.Boolean contratSupprime() {
		return (java.lang.Boolean) storedValueForKey(CONTRAT_SUPPRIME_KEY);
	}

	public void setContratSupprime(java.lang.Boolean value) {
	    takeStoredValueForKey(value, CONTRAT_SUPPRIME_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateArchivage() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_ARCHIVAGE_KEY);
	}

	public void setDateArchivage(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_ARCHIVAGE_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dateNotifiation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(DATE_NOTIFIATION_KEY);
	}

	public void setDateNotifiation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, DATE_NOTIFIATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dCreation() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_CREATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dModification() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public com.webobjects.foundation.NSTimestamp dOuvertureDossier() {
		return (com.webobjects.foundation.NSTimestamp) storedValueForKey(D_OUVERTURE_DOSSIER_KEY);
	}

	public void setDOuvertureDossier(com.webobjects.foundation.NSTimestamp value) {
	    takeStoredValueForKey(value, D_OUVERTURE_DOSSIER_KEY);
	}
	
	public java.lang.String exploitationDesResultats() {
		return (java.lang.String) storedValueForKey(EXPLOITATION_DES_RESULTATS_KEY);
	}

	public void setExploitationDesResultats(java.lang.String value) {
	    takeStoredValueForKey(value, EXPLOITATION_DES_RESULTATS_KEY);
	}
	
	public java.lang.Integer montantAugmentation() {
		return (java.lang.Integer) storedValueForKey(MONTANT_AUGMENTATION_KEY);
	}

	public void setMontantAugmentation(java.lang.Integer value) {
	    takeStoredValueForKey(value, MONTANT_AUGMENTATION_KEY);
	}
	
	public java.lang.Float montantDemande() {
		return (java.lang.Float) storedValueForKey(MONTANT_DEMANDE_KEY);
	}

	public void setMontantDemande(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_DEMANDE_KEY);
	}
	
	public java.lang.Float montantObtenu() {
		return (java.lang.Float) storedValueForKey(MONTANT_OBTENU_KEY);
	}

	public void setMontantObtenu(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_OBTENU_KEY);
	}
	
	public java.lang.Float montantPrelevementEffectue() {
		return (java.lang.Float) storedValueForKey(MONTANT_PRELEVEMENT_EFFECTUE_KEY);
	}

	public void setMontantPrelevementEffectue(java.lang.Float value) {
	    takeStoredValueForKey(value, MONTANT_PRELEVEMENT_EFFECTUE_KEY);
	}
	
	public java.lang.Boolean participationDajNegociations() {
		return (java.lang.Boolean) storedValueForKey(PARTICIPATION_DAJ_NEGOCIATIONS_KEY);
	}

	public void setParticipationDajNegociations(java.lang.Boolean value) {
	    takeStoredValueForKey(value, PARTICIPATION_DAJ_NEGOCIATIONS_KEY);
	}
	
	public java.lang.Integer persIdCreation() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}
	
	public java.lang.Integer persIdModification() {
		return (java.lang.Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(java.lang.Integer value) {
	    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}
	
	public java.lang.String proprieteDesResultats() {
		return (java.lang.String) storedValueForKey(PROPRIETE_DES_RESULTATS_KEY);
	}

	public void setProprieteDesResultats(java.lang.String value) {
	    takeStoredValueForKey(value, PROPRIETE_DES_RESULTATS_KEY);
	}
	
	public java.lang.String redactionClausesPi() {
		return (java.lang.String) storedValueForKey(REDACTION_CLAUSES_PI_KEY);
	}

	public void setRedactionClausesPi(java.lang.String value) {
	    takeStoredValueForKey(value, REDACTION_CLAUSES_PI_KEY);
	}
	
	public java.lang.String redactionContrat() {
		return (java.lang.String) storedValueForKey(REDACTION_CONTRAT_KEY);
	}

	public void setRedactionContrat(java.lang.String value) {
	    takeStoredValueForKey(value, REDACTION_CONTRAT_KEY);
	}
	
	public java.lang.Boolean redevances() {
		return (java.lang.Boolean) storedValueForKey(REDEVANCES_KEY);
	}

	public void setRedevances(java.lang.Boolean value) {
	    takeStoredValueForKey(value, REDEVANCES_KEY);
	}
	
	public java.lang.String sensMontantFinancier() {
		return (java.lang.String) storedValueForKey(SENS_MONTANT_FINANCIER_KEY);
	}

	public void setSensMontantFinancier(java.lang.String value) {
	    takeStoredValueForKey(value, SENS_MONTANT_FINANCIER_KEY);
	}
	
	public java.lang.Boolean soumisTva() {
		return (java.lang.Boolean) storedValueForKey(SOUMIS_TVA_KEY);
	}

	public void setSoumisTva(java.lang.Boolean value) {
	    takeStoredValueForKey(value, SOUMIS_TVA_KEY);
	}
	
	public java.lang.Boolean subventionObtenue() {
		return (java.lang.Boolean) storedValueForKey(SUBVENTION_OBTENUE_KEY);
	}

	public void setSubventionObtenue(java.lang.Boolean value) {
	    takeStoredValueForKey(value, SUBVENTION_OBTENUE_KEY);
	}
	
	public java.lang.String suiviAdministratif() {
		return (java.lang.String) storedValueForKey(SUIVI_ADMINISTRATIF_KEY);
	}

	public void setSuiviAdministratif(java.lang.String value) {
	    takeStoredValueForKey(value, SUIVI_ADMINISTRATIF_KEY);
	}
	
	public java.lang.String suiviValo() {
		return (java.lang.String) storedValueForKey(SUIVI_VALO_KEY);
	}

	public void setSuiviValo(java.lang.String value) {
	    takeStoredValueForKey(value, SUIVI_VALO_KEY);
	}
	
	public java.lang.Float tauxPrelevementEffectue() {
		return (java.lang.Float) storedValueForKey(TAUX_PRELEVEMENT_EFFECTUE_KEY);
	}

	public void setTauxPrelevementEffectue(java.lang.Float value) {
	    takeStoredValueForKey(value, TAUX_PRELEVEMENT_EFFECTUE_KEY);
	}
	
	public java.lang.Float tauxTva() {
		return (java.lang.Float) storedValueForKey(TAUX_TVA_KEY);
	}

	public void setTauxTva(java.lang.Float value) {
	    takeStoredValueForKey(value, TAUX_TVA_KEY);
	}
	
	public java.lang.String typeInterventionSurContrat() {
		return (java.lang.String) storedValueForKey(TYPE_INTERVENTION_SUR_CONTRAT_KEY);
	}

	public void setTypeInterventionSurContrat(java.lang.String value) {
	    takeStoredValueForKey(value, TYPE_INTERVENTION_SUR_CONTRAT_KEY);
	}
	
	public java.lang.Boolean utilisationContratTypeDaj() {
		return (java.lang.Boolean) storedValueForKey(UTILISATION_CONTRAT_TYPE_DAJ_KEY);
	}

	public void setUtilisationContratTypeDaj(java.lang.Boolean value) {
	    takeStoredValueForKey(value, UTILISATION_CONTRAT_TYPE_DAJ_KEY);
	}
	
	public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
	    return (org.cocktail.cocowork.server.metier.convention.Contrat) storedValueForKey(CONTRAT_KEY);
	}

	public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
	    if (value == null) {
	    	org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
	    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique) storedValueForKey(DOMAINE_SCIENTIFIQUE_KEY);
	}

	public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DOMAINE_SCIENTIFIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, DOMAINE_SCIENTIFIQUE_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOProgramme programme() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProgramme) storedValueForKey(PROGRAMME_KEY);
	}

	public void setProgrammeRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgramme value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOProgramme oldValue = programme();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROGRAMME_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, PROGRAMME_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOProgramme sousProgramme() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOProgramme) storedValueForKey(SOUS_PROGRAMME_KEY);
	}

	public void setSousProgrammeRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOProgramme value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOProgramme oldValue = sousProgramme();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SOUS_PROGRAMME_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, SOUS_PROGRAMME_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement suiviValoEvenement() {
	    return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement) storedValueForKey(SUIVI_VALO_EVENEMENT_KEY);
	}

	public void setSuiviValoEvenementRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement oldValue = suiviValoEvenement();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SUIVI_VALO_EVENEMENT_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, SUIVI_VALO_EVENEMENT_KEY);
	    }
	}
	
	public org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique zoneGeographique() {
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique) storedValueForKey(ZONE_GEOGRAPHIQUE_KEY);
	}

	public void setZoneGeographiqueRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique value) {
	    if (value == null) {
	    	org.cocktail.fwkcktlrecherche.server.metier.EOZoneGeographique oldValue = zoneGeographique();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ZONE_GEOGRAPHIQUE_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, ZONE_GEOGRAPHIQUE_KEY);
	    }
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> toRepartApportDajContrats() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_REPART_APPORT_DAJ_CONTRATS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> toRepartApportDajContrats(EOQualifier qualifier) {
		return toRepartApportDajContrats(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> toRepartApportDajContrats(EOQualifier qualifier, boolean fetch) {
	    return toRepartApportDajContrats(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> toRepartApportDajContrats(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toRepartApportDajContrats();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToRepartApportDajContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DAJ_CONTRATS_KEY);
	}

	public void removeFromToRepartApportDajContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DAJ_CONTRATS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont createToRepartApportDajContratsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartApportDajCont");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_APPORT_DAJ_CONTRATS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont) eo;
	}

	public void deleteToRepartApportDajContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DAJ_CONTRATS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToRepartApportDajContratsRelationships() {
		java.util.Enumeration objects = toRepartApportDajContrats().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToRepartApportDajContratsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> toRepartApportDrvContrats() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_REPART_APPORT_DRV_CONTRATS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> toRepartApportDrvContrats(EOQualifier qualifier) {
		return toRepartApportDrvContrats(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> toRepartApportDrvContrats(EOQualifier qualifier, boolean fetch) {
	    return toRepartApportDrvContrats(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> toRepartApportDrvContrats(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toRepartApportDrvContrats();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToRepartApportDrvContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DRV_CONTRATS_KEY);
	}

	public void removeFromToRepartApportDrvContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DRV_CONTRATS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont createToRepartApportDrvContratsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartApportDrvCont");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_APPORT_DRV_CONTRATS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont) eo;
	}

	public void deleteToRepartApportDrvContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_APPORT_DRV_CONTRATS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToRepartApportDrvContratsRelationships() {
		java.util.Enumeration objects = toRepartApportDrvContrats().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToRepartApportDrvContratsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> toRepartContratLangues() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_REPART_CONTRAT_LANGUES_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> toRepartContratLangues(EOQualifier qualifier) {
		return toRepartContratLangues(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> toRepartContratLangues(EOQualifier qualifier, boolean fetch) {
	    return toRepartContratLangues(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> toRepartContratLangues(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue.CONTRAT_RECHERCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toRepartContratLangues();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToRepartContratLanguesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_CONTRAT_LANGUES_KEY);
	}

	public void removeFromToRepartContratLanguesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_CONTRAT_LANGUES_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue createToRepartContratLanguesRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartContratLangue");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_CONTRAT_LANGUES_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue) eo;
	}

	public void deleteToRepartContratLanguesRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_CONTRAT_LANGUES_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToRepartContratLanguesRelationships() {
		java.util.Enumeration objects = toRepartContratLangues().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToRepartContratLanguesRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartContratLangue) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> toRepartTypeResultatsContrats() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_REPART_TYPE_RESULTATS_CONTRATS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> toRepartTypeResultatsContrats(EOQualifier qualifier) {
		return toRepartTypeResultatsContrats(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> toRepartTypeResultatsContrats(EOQualifier qualifier, boolean fetch) {
	    return toRepartTypeResultatsContrats(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> toRepartTypeResultatsContrats(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toRepartTypeResultatsContrats();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToRepartTypeResultatsContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_RESULTATS_CONTRATS_KEY);
	}

	public void removeFromToRepartTypeResultatsContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_RESULTATS_CONTRATS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat createToRepartTypeResultatsContratsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeResultatsContrat");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_TYPE_RESULTATS_CONTRATS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat) eo;
	}

	public void deleteToRepartTypeResultatsContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_RESULTATS_CONTRATS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToRepartTypeResultatsContratsRelationships() {
		java.util.Enumeration objects = toRepartTypeResultatsContrats().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToRepartTypeResultatsContratsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat) objects.nextElement());
	    }
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> toValidationContrats() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(TO_VALIDATION_CONTRATS_KEY);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> toValidationContrats(EOQualifier qualifier) {
		return toValidationContrats(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> toValidationContrats(EOQualifier qualifier, boolean fetch) {
	    return toValidationContrats(qualifier, null, fetch);
	}
	
	public com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> toValidationContrats(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings, boolean fetch) {
		com.webobjects.foundation.NSArray<org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat> results;
    	if (fetch) {
    		EOQualifier fullQualifier;
			EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat.CONTRAT_RECHERCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
			results = org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    	} else {
			results = toValidationContrats();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
    	}
		return results;
	}
	  
	public void addToToValidationContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_VALIDATION_CONTRATS_KEY);
	}

	public void removeFromToValidationContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VALIDATION_CONTRATS_KEY);
	}

	public org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat createToValidationContratsRelationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ValidationContrat");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, TO_VALIDATION_CONTRATS_KEY);
	    return (org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat) eo;
	}

	public void deleteToValidationContratsRelationship(org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VALIDATION_CONTRATS_KEY);
	    editingContext().deleteObject(object);
	}

	public void deleteAllToValidationContratsRelationships() {
		java.util.Enumeration objects = toValidationContrats().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	deleteToValidationContratsRelationship((org.cocktail.fwkcktlrecherche.server.metier.EOValidationContrat) objects.nextElement());
	    }
	}

	public static EOContratRecherche createContratRecherche(EOEditingContext editingContext, com.webobjects.foundation.NSTimestamp dCreation, com.webobjects.foundation.NSTimestamp dModification, java.lang.Integer persIdCreation, java.lang.Integer persIdModification, java.lang.String sensMontantFinancier, org.cocktail.cocowork.server.metier.convention.Contrat contrat) {
		EOContratRecherche eo = (EOContratRecherche) EOUtilities.createAndInsertInstance(editingContext, _EOContratRecherche.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
		eo.setSensMontantFinancier(sensMontantFinancier);
		eo.setContratRelationship(contrat);
		return eo;
	}

	public EOContratRecherche localInstanceIn(EOEditingContext editingContext) {
		return (EOContratRecherche) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOContratRecherche creerInstance(EOEditingContext editingContext) {
		return (EOContratRecherche) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOContratRecherche.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOContratRecherche> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOContratRecherche>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOContratRecherche fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOContratRecherche fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOContratRecherche> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOContratRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOContratRecherche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOContratRecherche fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOContratRecherche> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContratRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOContratRecherche fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOContratRecherche eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOContratRecherche ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOContratRecherche fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAllContratRecherches(EOEditingContext editingContext) {
		return _EOContratRecherche.fetchAllContratRecherches(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchAllContratRecherches(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOContratRecherche.fetchContratRecherches(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOContratRecherche> fetchContratRecherches(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOContratRecherche> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOContratRecherche>(_EOContratRecherche.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOContratRecherche> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOContratRecherche fetchContratRecherche(EOEditingContext editingContext, String keyName, Object value) {
		return _EOContratRecherche.fetchContratRecherche(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOContratRecherche fetchContratRecherche(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOContratRecherche> eoObjects = _EOContratRecherche.fetchContratRecherches(editingContext, qualifier, null);
	    EOContratRecherche eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one ContratRecherche that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
