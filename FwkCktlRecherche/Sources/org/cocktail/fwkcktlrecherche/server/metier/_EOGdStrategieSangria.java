/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGdStrategieSangria.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOGdStrategieSangria extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "GdStrategieSangria";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final er.extensions.eof.ERXKey<java.lang.String> CODE = new er.extensions.eof.ERXKey<java.lang.String>("code");
	public static final er.extensions.eof.ERXKey<java.lang.Integer> ID = new er.extensions.eof.ERXKey<java.lang.Integer>("id");
	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("libelle");
	
	public static final String CODE_KEY = CODE.key();
	public static final String ID_KEY = ID.key();
	public static final String LIBELLE_KEY = LIBELLE.key();

	// Attributs non visibles

	//Colonnes dans la base de donnees
	public static final String CODE_COLKEY = "CODE";
	public static final String ID_COLKEY = "ID_GD_STRATEGIE";
	public static final String LIBELLE_COLKEY = "LIBELLE";


	// Relationships
	

	// Accessors methods
	public java.lang.String code() {
		return (java.lang.String) storedValueForKey(CODE_KEY);
	}

	public void setCode(java.lang.String value) {
	    takeStoredValueForKey(value, CODE_KEY);
	}
	
	public java.lang.Integer id() {
		return (java.lang.Integer) storedValueForKey(ID_KEY);
	}

	public void setId(java.lang.Integer value) {
	    takeStoredValueForKey(value, ID_KEY);
	}
	
	public java.lang.String libelle() {
		return (java.lang.String) storedValueForKey(LIBELLE_KEY);
	}

	public void setLibelle(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_KEY);
	}
	
	public static EOGdStrategieSangria createGdStrategieSangria(EOEditingContext editingContext, java.lang.String code, java.lang.Integer id, java.lang.String libelle) {
		EOGdStrategieSangria eo = (EOGdStrategieSangria) EOUtilities.createAndInsertInstance(editingContext, _EOGdStrategieSangria.ENTITY_NAME);    
		eo.setCode(code);
		eo.setId(id);
		eo.setLibelle(libelle);
		return eo;
	}

	public EOGdStrategieSangria localInstanceIn(EOEditingContext editingContext) {
		return (EOGdStrategieSangria) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOGdStrategieSangria creerInstance(EOEditingContext editingContext) {
		return (EOGdStrategieSangria) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOGdStrategieSangria.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOGdStrategieSangria> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOGdStrategieSangria>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOGdStrategieSangria fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOGdStrategieSangria fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOGdStrategieSangria> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGdStrategieSangria eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOGdStrategieSangria fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOGdStrategieSangria fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOGdStrategieSangria> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGdStrategieSangria eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOGdStrategieSangria fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOGdStrategieSangria eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOGdStrategieSangria ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOGdStrategieSangria fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAllGdStrategieSangrias(EOEditingContext editingContext) {
		return _EOGdStrategieSangria.fetchAllGdStrategieSangrias(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchAllGdStrategieSangrias(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOGdStrategieSangria.fetchGdStrategieSangrias(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOGdStrategieSangria> fetchGdStrategieSangrias(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOGdStrategieSangria> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOGdStrategieSangria>(_EOGdStrategieSangria.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOGdStrategieSangria> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOGdStrategieSangria fetchGdStrategieSangria(EOEditingContext editingContext, String keyName, Object value) {
		return _EOGdStrategieSangria.fetchGdStrategieSangria(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOGdStrategieSangria fetchGdStrategieSangria(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOGdStrategieSangria> eoObjects = _EOGdStrategieSangria.fetchGdStrategieSangrias(editingContext, qualifier, null);
	    EOGdStrategieSangria eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one GdStrategieSangria that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
