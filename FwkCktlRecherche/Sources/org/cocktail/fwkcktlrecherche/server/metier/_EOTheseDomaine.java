/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTheseDomaine.java instead.
package org.cocktail.fwkcktlrecherche.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class _EOTheseDomaine extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TheseDomaine";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "idTheseDomaine";

	public static final er.extensions.eof.ERXKey<java.lang.String> LIBELLE_DOMAINE = new er.extensions.eof.ERXKey<java.lang.String>("libelleDomaine");
	public static final er.extensions.eof.ERXKey<java.lang.String> OPTION_DOMAINE = new er.extensions.eof.ERXKey<java.lang.String>("optionDomaine");
	public static final er.extensions.eof.ERXKey<java.lang.String> TEM_VALIDE = new er.extensions.eof.ERXKey<java.lang.String>("temValide");
	
	public static final String LIBELLE_DOMAINE_KEY = LIBELLE_DOMAINE.key();
	public static final String OPTION_DOMAINE_KEY = OPTION_DOMAINE.key();
	public static final String TEM_VALIDE_KEY = TEM_VALIDE.key();

	// Attributs non visibles
	public static final String ID_THESE_DOMAINE_KEY = "idTheseDomaine";

	//Colonnes dans la base de donnees
	public static final String LIBELLE_DOMAINE_COLKEY = "LIBELLE_DOMAINE";
	public static final String OPTION_DOMAINE_COLKEY = "OPTION_DOMAINE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String ID_THESE_DOMAINE_COLKEY = "ID_THESE_DOMAINE";

	// Relationships
	

	// Accessors methods
	public java.lang.String libelleDomaine() {
		return (java.lang.String) storedValueForKey(LIBELLE_DOMAINE_KEY);
	}

	public void setLibelleDomaine(java.lang.String value) {
	    takeStoredValueForKey(value, LIBELLE_DOMAINE_KEY);
	}
	
	public java.lang.String optionDomaine() {
		return (java.lang.String) storedValueForKey(OPTION_DOMAINE_KEY);
	}

	public void setOptionDomaine(java.lang.String value) {
	    takeStoredValueForKey(value, OPTION_DOMAINE_KEY);
	}
	
	public java.lang.String temValide() {
		return (java.lang.String) storedValueForKey(TEM_VALIDE_KEY);
	}

	public void setTemValide(java.lang.String value) {
	    takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}
	
	public static EOTheseDomaine createTheseDomaine(EOEditingContext editingContext, java.lang.String libelleDomaine, java.lang.String temValide) {
		EOTheseDomaine eo = (EOTheseDomaine) EOUtilities.createAndInsertInstance(editingContext, _EOTheseDomaine.ENTITY_NAME);    
		eo.setLibelleDomaine(libelleDomaine);
		eo.setTemValide(temValide);
		return eo;
	}

	public EOTheseDomaine localInstanceIn(EOEditingContext editingContext) {
		return (EOTheseDomaine) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOTheseDomaine creerInstance(EOEditingContext editingContext) {
		return (EOTheseDomaine) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, _EOTheseDomaine.ENTITY_NAME);
	}

	/* Finders */
	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<EOTheseDomaine> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseDomaine>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static EOTheseDomaine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTheseDomaine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<EOTheseDomaine> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTheseDomaine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static EOTheseDomaine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static EOTheseDomaine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<EOTheseDomaine> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTheseDomaine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static EOTheseDomaine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOTheseDomaine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet EOTheseDomaine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static EOTheseDomaine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAllTheseDomaines(EOEditingContext editingContext) {
		return _EOTheseDomaine.fetchAllTheseDomaines(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchAllTheseDomaines(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return _EOTheseDomaine.fetchTheseDomaines(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<EOTheseDomaine> fetchTheseDomaines(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<EOTheseDomaine> fetchSpec = new er.extensions.eof.ERXFetchSpecification<EOTheseDomaine>(_EOTheseDomaine.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<EOTheseDomaine> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static EOTheseDomaine fetchTheseDomaine(EOEditingContext editingContext, String keyName, Object value) {
		return _EOTheseDomaine.fetchTheseDomaine(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static EOTheseDomaine fetchTheseDomaine(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<EOTheseDomaine> eoObjects = _EOTheseDomaine.fetchTheseDomaines(editingContext, qualifier, null);
	    EOTheseDomaine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one TheseDomaine that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
