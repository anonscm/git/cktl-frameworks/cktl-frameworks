/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlrecherche.server.metier;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;


public class EOMembreJuryThese extends _EOMembreJuryThese {
	private static Logger log = Logger.getLogger(EOMembreJuryThese.class);
	public static final String LIBELLE_ROLE_MEMBRE = "libelleRoleJury";
	public static final String LIBELLE_ETABLISSEMENT = "libelleEtablissement";
	public static final String IS_RAPPORTEUR_SEANCE = "isRapporteurSeance";
	public final static EOSortOrdering SORT_NOM_MEMBRE_ASC = EOSortOrdering.sortOrderingWithKey(EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY + "."
			+ ContratPartenaire.PARTENAIRE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);

	public String libelleRoleJury() {
		return repartAssociationJury().toAssociation().assLibelle();
	}
	
	public String libelleEtablissement() {
		if (toRne() != null) {
			return toRne().llRne();
		} else {
			return libelleStructExterne();
		}
	}
	
	public String libelleCorpsEtTitre() {
		String libelle = "";
		if (toCorps() != null) {
			libelle += toCorps().llCorps();
		}
		if (titreSpecial() != null) {
			if (!libelle.equals("")) {
				libelle += " ";
			}
			libelle += titreSpecial();
		}
		return libelle;
	}

	public EORepartAssociation repartAssociationJury() {
		NSArray<EOAssociation> lesRolesJury = FactoryAssociation.shared().associationsFilleDe(FactoryAssociation.shared().rolesJuryAssociation(editingContext()), editingContext());
		return repartAssociationMembre(lesRolesJury);
	}

	public EORepartAssociation repartAssociationRapporteur() {
		NSArray<EOAssociation> leRoleRapporteur = new NSArray<EOAssociation>(FactoryAssociation.shared().rapporteurTheseAssociation(editingContext()));
		return repartAssociationMembre(leRoleRapporteur);
	}

	public EORepartAssociation repartAssociationMembre(NSArray<EOAssociation> associations) {
		EOQualifier qual = ERXQ.and(ERXQ.equals(EORepartAssociation.PERS_ID_KEY, toContratPartenaire().partenaire().persId()),
				ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, toContratPartenaire().contrat().groupePartenaire().cStructure()),
				ERXQ.in(EORepartAssociation.TO_ASSOCIATION_KEY, associations));
		NSArray<EORepartAssociation> lesRepartAssoc = EORepartAssociation.fetchAll(editingContext(), qual);
		return lesRepartAssoc.isEmpty() ? null : lesRepartAssoc.lastObject();

	}

	public Boolean isRapporteurSeance() {
		if (rapporteurSeance().equals("O")) {
			return true;
		}
		return false;
	}
}
