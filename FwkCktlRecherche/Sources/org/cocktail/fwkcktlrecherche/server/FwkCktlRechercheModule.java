package org.cocktail.fwkcktlrecherche.server;

import com.google.inject.AbstractModule;

/**
 * Module de configuration de l'injection dans le fwk recherche
 */
public class FwkCktlRechercheModule extends AbstractModule {

	@Override
	protected void configure() {
	}

}
