package org.cocktail.fwkcktlrecherche.server.util;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;


public class ContratUtilities {
	
    private FactoryContratPartenaire factoryContratPartenaire;
    private EOEditingContext editingContext;
    
	public static ContratUtilities creerNouvelleInstance(EOEditingContext editingContext) {
		return new ContratUtilities(editingContext);
	}
	
	private ContratUtilities(EOEditingContext editingContext) {
		this.editingContext = editingContext;
		factoryContratPartenaire = new FactoryContratPartenaire(editingContext, false);
	}
	
    public void ajouterPartenaireAvecRoleAuContrat(IPersonne partenaire, EOAssociation role, Contrat contrat, Integer utilisateurPersId) {
    	if (partenaire != null) {
        	EOStructureForGroupeSpec.ajouterUnRole(editingContext, partenaire, role, contrat.groupePartenaire(), utilisateurPersId, null, null, null, null, null, true);
    	}
    }

    public void ajouterPartenaireAvecRoleEtDatesAuContrat(IPersonne partenaire, EOAssociation role, Contrat contrat, NSTimestamp debut, NSTimestamp fin, Integer utilisateurPersId) {
    	if (partenaire != null) {
        	EOStructureForGroupeSpec.ajouterUnRole(editingContext, partenaire, role, contrat.groupePartenaire(), utilisateurPersId, debut, fin, null, null, null, true);
    	}
    }

    public void supprimerRolePourPartenaireDansContrat(EOAssociation role, IPersonne partenaire, Contrat contrat, Integer utilisateurPersId) {
    	EOStructureForGroupeSpec.sharedInstance().supprimerUnRole(editingContext, partenaire, role, contrat.groupePartenaire(), utilisateurPersId, null);
    }

    public void supprimerRolePourPartenaireDansContrat(EOAssociation role, IPersonne partenaire, Contrat contrat, Integer utilisateurPersId, NSTimestamp rasDOuverture) {
    	EOStructureForGroupeSpec.sharedInstance().supprimerUnRole(editingContext, partenaire, role, contrat.groupePartenaire(), utilisateurPersId, rasDOuverture);
    }
    
    public IPersonne personnePourRoleDansGroupePartenaireDeContrat(EOAssociation role, Contrat contrat) {
    	IPersonne personne  = null;
    	NSArray<EORepartAssociation> resultats = contrat.groupePartenaire().toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(role));
    	if (!resultats.isEmpty()) {
        	personne = ERXArrayUtilities.firstObject(resultats).toPersonneElt(); 
    	}
    	return personne;
    }

    public void setPersonneAvecRoleDansGroupePartenaireDeContrat(IPersonne nouvellePersonne, EOAssociation role, Contrat contrat, Integer utilisateurPersId) {

    	IPersonne personne = personnePourRoleDansGroupePartenaireDeContrat(role, contrat);

    	if (personne != null) {
    		if (personne.equals(nouvellePersonne)) {
    			return;
    		}
    		supprimerRolePourPartenaireDansContrat(role, personne, contrat, utilisateurPersId);
    	}

    	if (nouvellePersonne != null) {
    		try {
    			ajouterPartenaireAvecRoleAuContrat(nouvellePersonne, role, contrat, utilisateurPersId);
    		} catch (Exception e) {
    			throw NSForwardException._runtimeExceptionForThrowable(e);
    		}
    	}
      

    }
    
    public NSArray<EOStructure> getStructuresRecherchesConcerneesPourContrat(Contrat contrat) {
    	NSArray<ContratPartenaire> contratPartenaires = contrat.contratPartenaires(ContratPartenaire.PARTENAIRE.dot(IPersonne.IS_STRUCTURE_KEY).isTrue());
    	NSArray<EOStructure> partenaires = (NSArray<EOStructure>) contratPartenaires.valueForKey(ContratPartenaire.PARTENAIRE.key());
    	NSArray<EOStructure> structuresRecherche = ERXQ.filtered(partenaires, EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).contains(EOTypeGroupe.TGRP_CODE_SR));
    	return structuresRecherche;
    }
    
    public void ajouterStructureRechercheConcerneePourContrat(EOStructure structureRecherche, Contrat contrat) throws Exception {
        if (structureRecherche != null) {
          if (contrat.partenaireForPersId(structureRecherche.persId()) == null) {
              factoryContratPartenaire.creerContratPartenaire(contrat, structureRecherche, new NSArray<EOAssociation>(FactoryAssociation.shared().laboratoireConcerneAssociation(editingContext)), false, null);
          }
        }
      }

    public void supprimerStructureRechercheConcerneePourContrat(EOStructure structureRecherche, Contrat contrat, Integer persId) throws Exception {
    	if (structureRecherche != null) {
    		factoryContratPartenaire.supprimerContratPartenaire(contrat.partenaireForPersonne(structureRecherche), persId);
    		EOStructureForGroupeSpec.sharedInstance().supprimerLesRoles(editingContext, structureRecherche, FactoryAssociation.shared().laboratoireConcerneAssociation(editingContext), contrat.groupePartenaire(), persId);
        }
	}
    

    public void ajouterResponsableScientifiquePourStructureRechercheEtContrat(EOIndividu responsable, EOStructure structureRecherche, Contrat contrat) throws Exception {
      if (responsable != null && structureRecherche != null) {
          factoryContratPartenaire.ajouterContact(contrat.partenaireForPersonne(structureRecherche), responsable, new NSArray<EOAssociation>(FactoryAssociation.shared().responsableScientifiqueAssociation(editingContext)));
      }
    }

    public void supprimerResponsableScientifiquePourStructureRechercheEtContrat(EOIndividu responsable, EOStructure structureRecherche, Contrat contrat, Integer persId) throws Exception {
      if (responsable != null && structureRecherche != null) {
        ContratPartenaire contratPartenaire = contrat.partenaireForPersonne(structureRecherche);
        // Obligation de cloner sinon ca poutre pendant l'iteration
        NSArray<ContratPartContact> contacts = contratPartenaire.contratPartContacts().mutableClone();
        for (ContratPartContact contact : contacts) {
          if (ERXEOControlUtilities.eoEquals(contact.personne(), responsable)) {
              factoryContratPartenaire.supprimerContact(contact, contratPartenaire, persId);
              EOStructureForGroupeSpec.sharedInstance().supprimerLesRoles(editingContext, responsable, FactoryAssociation.shared().responsableScientifiqueAssociation(editingContext), contrat.groupePartenaire(), persId);
          }
        }
      }
    }

    public NSArray<EOIndividu> responsablesScientifiquesPourStructureRechercheEtContrat(EOStructure structureRecherche, Contrat contrat) {
      NSArray<EOIndividu> responsablesScientifiques = new NSMutableArray<EOIndividu>();
      if (structureRecherche == null) {
        return responsablesScientifiques;
      }
      ContratPartenaire contratPartenaire = contrat.partenaireForPersonne(structureRecherche);
      NSArray<ContratPartContact> contacts = contratPartenaire.contratPartContacts(ContratPartContact.PERSONNE.dot(IPersonne.IS_INDIVIDU_KEY).isTrue());
      responsablesScientifiques = (NSArray<EOIndividu>) contacts.valueForKey(ContratPartContact.PERSONNE.key());
      return responsablesScientifiques;
    }
    
	public NSArray<EOIndividu> getResponsablesScientifiquePourContrat(Contrat contrat) {
//		NSArray<EOIndividu> personnes = EOStructureForGroupeSpec.getPersonnesInGroupe(contrat.groupePartenaire(), FactoryAssociation.shared().responsableScientifiqueAssociation(editingContext));
//		return ERXQ.filtered(personnes, ERXQ.isTrue(IPersonne.IS_INDIVIDU_KEY));
		NSArray<EOIndividu> responsablesScientifiquesDuContrat = new NSMutableArray<EOIndividu>();
		NSArray<EOStructure> labos = getStructuresRecherchesConcerneesPourContrat(contrat);
		for (EOStructure labo : labos) {
			NSArray<EOIndividu> responsablesScientifiquesDuLaboratoire = responsablesScientifiquesPourStructureRechercheEtContrat(labo, contrat);
			for (EOIndividu responsableDuLaboratoire : responsablesScientifiquesDuLaboratoire) {
				responsablesScientifiquesDuContrat.add(responsableDuLaboratoire);
			}
		} 
		return responsablesScientifiquesDuContrat;
	}

    public void ajouterPartenaireDansContrat(IPersonne partenaire, Contrat contrat) throws Exception {
      if (partenaire != null) {
	      if (contrat.partenaireForPersonne(partenaire) == null) {
	    	Boolean principal = false;
	    	if (contrat.contratPartenaires(ContratPartenaire.QUALIFIER_PARTENAIRE_PRINCIPAL).isEmpty()) {
	    		principal = true;
	    	}
	        factoryContratPartenaire.creerContratPartenaire(contrat, partenaire, null, principal, null);
	      }
      }

    }

    public void supprimerPartenaireDansContrat(IPersonne partenaire, Contrat contrat, Integer persIdUtilisateur) throws Exception {
      if (partenaire != null) {
          factoryContratPartenaire.supprimerContratPartenaire(contrat.partenaireForPersonne(partenaire), persIdUtilisateur);
      }
    }

    public NSArray<IPersonne> getPartenairesDansContrat(Contrat contrat) {
      NSArray<IPersonne> partenaires = (NSArray<IPersonne>) contrat.contratPartenaires().valueForKey("partenaire");
      NSArray<IPersonne> partenairesPourContratRecherche = new NSMutableArray<IPersonne>();
      for (IPersonne partenaire : partenaires) {
        if (partenaire.isStructure()) {
          NSArray<EOTypeGroupe> typesGroupes = EOStructureForGroupeSpec.getTypesGroupe((EOStructure) partenaire);
          NSArray<String> typesGroupesCodes = (NSArray<String>) typesGroupes.valueForKey(EOTypeGroupe.TGRP_CODE_KEY);
          if (!typesGroupesCodes.contains("SR")) {
            partenairesPourContratRecherche.add(partenaire);
          }
        } else {
          partenairesPourContratRecherche.add(partenaire);
        }
      }
      return partenairesPourContratRecherche;
    }
    
    
    @SuppressWarnings("unchecked")
	public NSArray getPersonnesAvecRoleDansContrat(EOAssociation role, Contrat contrat) {
    	return EOStructureForGroupeSpec.getPersonnesInGroupe(contrat.groupePartenaire(), role);
    }
	
}
