package org.cocktail.fwkcktlrecherche.server.util;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

public class RechercheEOUtils {

	
	/** Recherche le num&eacute;ro de s&eacute;quence dans une table de s&eacute;quence Oracle (doit comporter un attribut nextval)
	 * @param editingContext
	 * @param nomEntiteSequenceOracle
	 * @return valeur trouv&eacute;e ou null
	 */
	public static Integer numeroSequenceOracle(EOEditingContext editingContext,String nomEntiteSequenceOracle) {
		if (nomEntiteSequenceOracle == null || nomEntiteSequenceOracle.equals("")) {
			return null;
		}
		EOFetchSpecification  myFetch = new EOFetchSpecification(nomEntiteSequenceOracle,null,null);
		NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
		try {
			Number numero = (Number)((NSArray) result.valueForKey("nextval")).objectAtIndex(0);
			return new Integer(numero.intValue());
		} catch (Exception e) {
			return null;
		}
	}
	

}
