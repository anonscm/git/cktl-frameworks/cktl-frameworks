package org.cocktail.fwkcktlrecherche.server.util;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODotationAnnuelle;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeDotation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;

public class DotationsAnnuellesHelper {

	public static NSArray<EODotationAnnuelle> dotationsPourStructureRechercheEtProvenanceAvecTypeDotation(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure structureProvenance, String typeDotation, Boolean includeEditingContextChanges) {
		
		EOQualifier qualifier = EODotationAnnuelle.TYPE_DOTATION.dot(EOTypeDotation.CODE).eq(typeDotation)
								.and(EODotationAnnuelle.STRUCTURE_PROVENANCE.eq(structureProvenance))
								.and(EODotationAnnuelle.STRUCTURE.eq(structureRecherche));
		
		ERXFetchSpecification<EODotationAnnuelle> fetchSpecification = new ERXFetchSpecification<EODotationAnnuelle>(EODotationAnnuelle.ENTITY_NAME, qualifier, EODotationAnnuelle.ANNEE.descs());
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		
		return fetchSpecification.fetchObjects(editingContext);
		
	}
	
	public static NSArray<EODotationAnnuelle> dotationsScientifiquesPourStructureRechercheEtTutelle(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure tutelle, Boolean includeEditingContextChanges) {
		return dotationsPourStructureRechercheEtProvenanceAvecTypeDotation(editingContext, structureRecherche, tutelle, EODotationAnnuelle.DOTATION_SCIENTIFIQUE_CODE, includeEditingContextChanges);
	}
	
	public static NSArray<EODotationAnnuelle> creditsInfrastructuresPourStructureRecherche(EOEditingContext editingContext, EOStructure structureRecherche, Boolean includeEditingContextChanges) {
		return dotationsPourStructureRechercheEtProvenanceAvecTypeDotation(editingContext, structureRecherche, null, EODotationAnnuelle.CREDITS_INFRASTRUCTURE_CODE, includeEditingContextChanges);
	}
	
	public static NSArray<EODotationAnnuelle> dotationsPourStructureRechercheEtEtablissementRattachement(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure etablissementRattachement, Boolean includeEditingContextChanges) {
		return dotationsPourStructureRechercheEtProvenanceAvecTypeDotation(editingContext, structureRecherche, etablissementRattachement, EODotationAnnuelle.DOTATION_ETABLISSEMENT_RATTACHEMENT_CODE, includeEditingContextChanges);
	}
	
	public static EODotationAnnuelle creerDotationPourStructureRecherche(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure structureProvenance, Integer annee, Float montant, String typeDotation, Integer persId) {
		EODotationAnnuelle dotationAnnuelle = EODotationAnnuelle.creerInstance(editingContext);
		dotationAnnuelle.setAnnee(annee);
		dotationAnnuelle.setMontant(montant);
		dotationAnnuelle.setStructureRelationship(structureRecherche);
		dotationAnnuelle.setStructureProvenanceRelationship(structureProvenance);
		dotationAnnuelle.setTypeDotationRelationship(
				EOTypeDotation.fetchFirstByQualifier(editingContext, 
						EOTypeDotation.CODE.eq(typeDotation)
					)
				);
		dotationAnnuelle.setPersIdCreation(persId);
		dotationAnnuelle.setPersIdModification(persId);
		dotationAnnuelle.setDCreation(MyDateCtrl.getDateJour());
		dotationAnnuelle.setDModification(MyDateCtrl.getDateJour());
		return dotationAnnuelle;
	}
	
	public static EODotationAnnuelle creerDotationScientifiquePourStructureRecherche(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure tutelle, Integer annee, Float montant, Integer persId) {
		return creerDotationPourStructureRecherche(editingContext, structureRecherche, tutelle, annee, montant, EODotationAnnuelle.DOTATION_SCIENTIFIQUE_CODE, persId);
	}
	
	public static EODotationAnnuelle creerDotationEtablissementDeRattachementPourStructureRecherche(EOEditingContext editingContext, EOStructure structureRecherche, EOStructure etablissementDeRattachement, Integer annee, Float montant, Integer persId) {
		return creerDotationPourStructureRecherche(editingContext, structureRecherche, etablissementDeRattachement, annee, montant, EODotationAnnuelle.DOTATION_ETABLISSEMENT_RATTACHEMENT_CODE, persId);
	}
	
	public static EODotationAnnuelle creerCreditsInfrastructuresPourStructureRecherche(EOEditingContext editingContext, EOStructure structureRecherche, Integer annee, Float montant, Integer persId) {
		return creerDotationPourStructureRecherche(editingContext, structureRecherche, null, annee, montant, EODotationAnnuelle.CREDITS_INFRASTRUCTURE_CODE, persId);
	}
	
}
