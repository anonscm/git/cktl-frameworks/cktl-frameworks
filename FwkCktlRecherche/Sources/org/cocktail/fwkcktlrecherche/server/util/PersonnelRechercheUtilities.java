package org.cocktail.fwkcktlrecherche.server.util;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlgrh.common.metier.EOContratHeberge;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class PersonnelRechercheUtilities {

	public static EOCarriere carrierePourPersonnel(EOEditingContext editingContext, EOIndividu personnel) {
		EOQualifier qualifierCarriere =
			ERXQ.and(
					ERXQ.equals(EOCarriere.TO_PERSONNEL_KEY + "." + EOPersonnel.TO_INDIVIDU_KEY, personnel),
					ERXQ.equals(EOCarriere.TEM_VALIDE_KEY, "O"),
					ERXQ.lessThanOrEqualTo(EOCarriere.D_DEB_CARRIERE_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOCarriere.D_FIN_CARRIERE_KEY),
							ERXQ.greaterThanOrEqualTo(EOCarriere.D_FIN_CARRIERE_KEY, MyDateCtrl.getDateJour())
							)					
					);
		NSArray<EOCarriere> carrieres = EOCarriere.fetch(editingContext, qualifierCarriere, null);
		if(!carrieres.isEmpty()) {
			return carrieres.get(0);
		}
		return null;
	}
	
	public static EOContrat contratPourPersonnel(EOEditingContext editingContext, EOIndividu personnel) {
		EOQualifier qualifierContrat =
			ERXQ.and(
					ERXQ.equals(EOContrat.TO_PERSONNEL_KEY + "." + EOPersonnel.TO_INDIVIDU_KEY, personnel),
					ERXQ.equals(EOContrat.TEM_ANNULATION_KEY, "N"),
					ERXQ.lessThanOrEqualTo(EOContrat.D_DEB_CONTRAT_TRAV_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOContrat.D_FIN_CONTRAT_TRAV_KEY),
							ERXQ.greaterThanOrEqualTo(EOContrat.D_FIN_CONTRAT_TRAV_KEY, MyDateCtrl.getDateJour())
							)
					);		
		
		NSArray<EOContrat> contrats = EOContrat.fetch(editingContext, qualifierContrat, null);
		if(!contrats.isEmpty()) {
			return contrats.get(0);
		}
		return null;				
	}
	
	public static EOContratHeberge contratHebergePourPersonnel(EOEditingContext editingContext, EOIndividu personnel) {
		EOQualifier qualifierHeberge = 
			ERXQ.and(
					ERXQ.equals(EOContratHeberge.TO_PERSONNEL_KEY + "." + EOPersonnel.TO_INDIVIDU_KEY, personnel),
					ERXQ.equals(EOContratHeberge.TEM_VALIDE_KEY, "O"),
					ERXQ.lessThanOrEqualTo(EOContratHeberge.DATE_DEBUT_KEY, MyDateCtrl.getDateJour()),
					ERXQ.or(
							ERXQ.isNull(EOContratHeberge.DATE_FIN_KEY),
							ERXQ.greaterThanOrEqualTo(EOContratHeberge.DATE_FIN_KEY, MyDateCtrl.getDateJour())
							)
					);		
			
		NSArray<EOContratHeberge> contratsHeberges = EOContratHeberge.fetch(editingContext, qualifierHeberge, null);
		if(!contratsHeberges.isEmpty()) {
			return contratsHeberges.get(0);
		}
		return null;				

	}
	
	
	
	public static String employeurPourPersonnel(EOEditingContext editingContext, EOIndividu personnel) {
		
		// ON ESSAIE DE RECHERCHER LA CARRIERE
		if(carrierePourPersonnel(editingContext, personnel) != null) {
			NSArray<EOStructure> etablissements = personnel.getEtablissementsAffectation(null);
			if(!etablissements.isEmpty()) {
				return etablissements.get(0).llStructure();
			}
		}
		
		// ON ESSAIE REPERER DES CONTRATS DANS L'ETABLISSEMENT		
		if(contratPourPersonnel(editingContext, personnel) != null) {
			NSArray<EOStructure> etablissements = personnel.getEtablissementsAffectation(null);
			if(!etablissements.isEmpty()) {
				return etablissements.get(0).llStructure();
			}
		}		
		
		// ON ESSAIE ENFIN DE VOIR SI C'EST UN HEBERGE	
		EOContratHeberge contratHeberge = contratHebergePourPersonnel(editingContext, personnel);
		if(contratHeberge != null) {
			return contratHeberge.toRne().llRne();
		}	
		return null;
		
	}
	
	public static EOGrade gradePourPersonnel(EOEditingContext editingContext, EOIndividu personnel) {
		
		// ON ESSAIE DE RECHERCHER LA CARRIERE
		EOCarriere carriere = carrierePourPersonnel(editingContext, personnel);
		if(carriere != null) {
			EOElements element = carriere.elementActuel();
			if(element != null) {
				return element.toGrade();
			}
		}
		
		// ON ESSAIE REPERER DES CONTRATS DANS L'ETABLISSEMENT	
		EOContrat contrat = contratPourPersonnel(editingContext, personnel);
		if(contrat != null) {
			EOContratAvenant avenant = contrat.avenantActuel();
			if(avenant != null) {
				return avenant.toGrade();
			}
		}		
		
		// ON ESSAIE ENFIN DE VOIR SI C'EST UN HEBERGE	
		EOContratHeberge contratHeberge = contratHebergePourPersonnel(editingContext, personnel);
		if(contratHeberge != null) {
			return contratHeberge.toGrade();
		}	
		
		return null;
		
	}
	
	
	public static void affecterPersonneDansEquipe(EOEditingContext editingContext, EOIndividu personne, EOStructure equipe, NSTimestamp debut, NSTimestamp fin, Integer utilisateurPersId) {
		EOAssociation association = FactoryAssociation.shared().membreAssociation(editingContext);
		personne.definitUnRole(editingContext, association, equipe, utilisateurPersId, debut, fin, null, null, null);
	}
	
	public static void affecterPersonneDansStructureRecherche(EOEditingContext editingContext, EOIndividu personne, EOStructure unite, NSTimestamp debut, NSTimestamp fin, EOAssociation typeMembre, Integer utilisateurPersId) {
	  
	  // On fait les verifications sur les EC au cas ou
	  personne = ERXEOControlUtilities.localInstanceOfObject(editingContext, personne);
	  unite = ERXEOControlUtilities.localInstanceOfObject(editingContext, unite);
	  typeMembre = ERXEOControlUtilities.localInstanceOfObject(editingContext, typeMembre);
	  
		if(typeMembre != null) {
			EOStructureForGroupeSpec.definitUnRole(editingContext, personne, typeMembre, unite, utilisateurPersId, debut, fin, null, null, null, false);
		} else {
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext, personne, unite, utilisateurPersId);
		}
    	
	}
}
