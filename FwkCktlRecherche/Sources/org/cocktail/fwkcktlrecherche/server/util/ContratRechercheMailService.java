package org.cocktail.fwkcktlrecherche.server.util;

import org.cocktail.fwkcktlrecherche.server.metier.IContratRecherche;

/**
 * Service d'envoie d'email relatifs aux contrats de recherche
 */
public interface ContratRechercheMailService {

	/**
	 * Envoi d'un email pour la demande de validation d'un contrat de recherhe
	 * @param contrat le contrat à valider
	 * @param typeValidation le type de validation à effectuer
	 * @param persId l'id de l'utilisateur qui envoie le mail
	 */
	void envoyerMailDemandeDeValidation(IContratRecherche contrat, String typeValidation, Integer persId);
	
	/**
	 * Envoi d'un email pour la validation d'un contrat de recherhe
	 * @param contrat le contrat validé
	 * @param typeValidation le type de validation effectué
	 * @param persId l'id de l'utilisateur qui envoie le mail
	 */	
	void envoyerMailValidation(IContratRecherche contrat, String typeValidation, Integer persId);
	
}
