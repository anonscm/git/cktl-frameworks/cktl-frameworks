package org.cocktail.fwkcktlrecherche.server.util;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EODotationAnnuelle;
import org.cocktail.fwkcktlrecherche.server.metier.EOEvaluationMesr;
import org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire;
import org.cocktail.fwkcktlrecherche.server.metier.EOMotCle;
import org.cocktail.fwkcktlrecherche.server.metier.EOReconnaissanceUnite;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleDomaineScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartMotCleUniteRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartStructureRechercheDomaineScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class StructuresRechercheHelper {
	//private static Logger log = Logger.getLogger(EOUniteRecherche.class);



	public static EOGrandSecteurDisciplinaire grandSecteurDisciplinaireUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EODomaineScientifique domaineScientifique = domaineScientifiquePrincipalUniteRecherche(editingContext, uniteRecherche, includeEditingContextChanges);
		if (domaineScientifique == null) {
			return null;
		}
		EOQualifier qual = ERXQ.equals(EORepartGrandSecteurDisciplinaireDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY, domaineScientifique);
		EORepartGrandSecteurDisciplinaireDomaineScientifique repartGrandSecteurDisciplinaireDomaineScientifique = EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchFirstByQualifier(editingContext, qual);
		return repartGrandSecteurDisciplinaireDomaineScientifique.grandSecteurDisciplinaire();
	}

	public static EODomaineScientifique domaineScientifiquePrincipalUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EOQualifier qualifier =  ERXQ.and(
					ERXQ.equals(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY, uniteRecherche),
					ERXQ.isTrue(EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY)
					);
		ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		NSArray<EORepartStructureRechercheDomaineScientifique> repartUniteRechercheDomaineScientifiques = fetchSpecification.fetchObjects(editingContext);
		if (repartUniteRechercheDomaineScientifiques == null || repartUniteRechercheDomaineScientifiques.isEmpty()) {
			return null;
		} else {
			return repartUniteRechercheDomaineScientifiques.get(0).domaineScientifique();
		}
	}
	
	
	public static void setDomaineScientifiquePrincipalUniteRecherche(EOEditingContext editingContext, EODomaineScientifique domaineScientifique, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EOQualifier qualifier =  ERXQ.and(
					ERXQ.equals(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY, uniteRecherche),
					ERXQ.isTrue(EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY)
					);
		ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		NSArray<EORepartStructureRechercheDomaineScientifique> repartUniteRechercheDomaineScientifiques = fetchSpecification.fetchObjects(editingContext);
		if (repartUniteRechercheDomaineScientifiques != null && !repartUniteRechercheDomaineScientifiques.isEmpty()) {
			repartUniteRechercheDomaineScientifiques.get(0).setDomaineScientifiqueRelationship(domaineScientifique);
		} else {
			EORepartStructureRechercheDomaineScientifique repartUniteRechercheDomaineScientifique = new EORepartStructureRechercheDomaineScientifique();
			editingContext.insertObject(repartUniteRechercheDomaineScientifique);
			repartUniteRechercheDomaineScientifique.setDCreation(DateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setDModification(DateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setPersIdCreation(uniteRecherche.persIdCreation());
			repartUniteRechercheDomaineScientifique.setPersIdModification(uniteRecherche.persIdModification());
			repartUniteRechercheDomaineScientifique.setDomaineScientifiqueRelationship(domaineScientifique);
			repartUniteRechercheDomaineScientifique.setUniteRechercheRelationship(uniteRecherche);
			repartUniteRechercheDomaineScientifique.setPrincipal(true);
		}		
	}

	public static void ajouterDomaineScientifiqueSecondaireUniteRecherche(EOEditingContext editingContext, EODomaineScientifique domaineScientifique, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		if (!domainesScientifiquesSecondaireUniteRecherches(editingContext, uniteRecherche, includeEditingContextChanges).contains(domaineScientifique)) {
			EORepartStructureRechercheDomaineScientifique repartUniteRechercheDomaineScientifique = new EORepartStructureRechercheDomaineScientifique();
			editingContext.insertObject(repartUniteRechercheDomaineScientifique);
			repartUniteRechercheDomaineScientifique.setDCreation(DateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setDModification(DateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setPersIdCreation(uniteRecherche.persIdCreation());
			repartUniteRechercheDomaineScientifique.setPersIdModification(uniteRecherche.persIdModification());
			repartUniteRechercheDomaineScientifique.setDomaineScientifiqueRelationship(domaineScientifique);
			repartUniteRechercheDomaineScientifique.setUniteRechercheRelationship(uniteRecherche);
			repartUniteRechercheDomaineScientifique.setPrincipal(false);
		}
	}
	
	public static void supprimerDomaineScientifiqueSecondaireUniteRecherche(EOEditingContext editingContext, EODomaineScientifique domaineScientifique, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EOQualifier qualifier =  ERXQ.and(
					ERXQ.equals(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY, uniteRecherche),
					ERXQ.isFalse(EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY),
					ERXQ.equals(EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY, domaineScientifique)
					);
		ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		NSArray<EORepartStructureRechercheDomaineScientifique> repartUniteRechercheDomaineScientifiques = fetchSpecification.fetchObjects(editingContext);
		if (repartUniteRechercheDomaineScientifiques != null && !repartUniteRechercheDomaineScientifiques.isEmpty()) {
			editingContext.deleteObject(repartUniteRechercheDomaineScientifiques.get(0));
		}
	}
	
	public static NSArray<EODomaineScientifique> domainesScientifiquesSecondaireUniteRecherches(EOEditingContext editingContext, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EOQualifier qualifier =  ERXQ.and(
					ERXQ.equals(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY, uniteRecherche),
					ERXQ.isFalse(EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY)
					);
		ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		NSArray<EORepartStructureRechercheDomaineScientifique> repartUniteRechercheDomaineScientifiques = fetchSpecification.fetchObjects(editingContext);
		if (repartUniteRechercheDomaineScientifiques == null || repartUniteRechercheDomaineScientifiques.isEmpty()) {
			return new NSArray<EODomaineScientifique>();
		} else {
			return (NSArray<EODomaineScientifique>) repartUniteRechercheDomaineScientifiques.valueForKey(EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
		}
	}
	
	public static NSArray<EOStructure> unitesRecherchePourGrandSecteurDisciplinaire(EOEditingContext editingContext, EOGrandSecteurDisciplinaire grandSecteurDisciplinaire, Boolean includeEditingContextChanges) {
		NSArray<EODomaineScientifique> domaineScientifiques = grandSecteurDisciplinaire.domainesScientifiques();
		EOQualifier qual = ERXQ.and(
			ERXQ.in(EORepartStructureRechercheDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY, domaineScientifiques),
			ERXQ.isTrue(EORepartStructureRechercheDomaineScientifique.PRINCIPAL_KEY),
			new ERXQualifierInSubquery(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).eq(EOTypeGroupe.TGRP_CODE_LA), 
					EOStructure.ENTITY_NAME, EORepartStructureRechercheDomaineScientifique.C_STRUCTURE_KEY, EOStructure.C_STRUCTURE_KEY),
			new ERXQualifierInSubquery(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).eq(EOTypeGroupe.TGRP_CODE_SR), 
					EOStructure.ENTITY_NAME, EORepartStructureRechercheDomaineScientifique.C_STRUCTURE_KEY, EOStructure.C_STRUCTURE_KEY),
			ERXQ.equals(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY + "." + EOStructure.TEM_VALIDE_KEY, "O")
		);
		ERXSortOrderings sorts = EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE.dot(EOStructure.LL_STRUCTURE).ascInsensitives();
		ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartStructureRechercheDomaineScientifique>(EORepartStructureRechercheDomaineScientifique.ENTITY_NAME, qual, sorts);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);
		NSArray<EORepartStructureRechercheDomaineScientifique> repartUniteRechercheDomaineScientifiques =  fetchSpecification.fetchObjects(editingContext);
		return ERXArrayUtilities.arrayWithoutDuplicates(
				(NSArray<EOStructure>) repartUniteRechercheDomaineScientifiques.valueForKey(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY)
			);
	}
	
	/*
	private static FactoryAssociation factoryAssociation() {
		if(factoryAssociation == null)
			factoryAssociation = new FactoryAssociation(ERXEC.newEditingContext(), false);
		return factoryAssociation;
	}
	 */

	public static EOStructure tutellePrincipalePourStructureRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOAssociation association = FactoryAssociation.shared().tutellePrincipaleAssociation(editingContext);

		NSArray<EOStructure> tutellesPrincipales = (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);

		return ERXArrayUtilities.firstObject(tutellesPrincipales);
	}

	public static void definirTutellePrincipalePourStructureRecherche(EOEditingContext editingContext, EOStructure nouvelleTutelle, EOStructure uniteRecherche, Integer persIdUtilisateur) {

		if (nouvelleTutelle == null) {
			return;
		}
		EOStructure tutelle = tutellePrincipalePourStructureRecherche(editingContext, uniteRecherche);

		if (tutelle != null) {
			if (tutelle.equals(nouvelleTutelle)) {
				return;
			}
			tutelle.supprimerLesRoles(editingContext, FactoryAssociation.shared().tutellePrincipaleAssociation(editingContext), uniteRecherche, persIdUtilisateur);
		}

		EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext, nouvelleTutelle, uniteRecherche, persIdUtilisateur, FactoryAssociation.shared().tutellePrincipaleAssociation(editingContext));


	}

	public static NSArray<EOStructure> tutellesStructureRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOAssociation association = FactoryAssociation.shared().tutelleAssociation(editingContext);
		return (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	}

	public static void ajouterTutellePourStructureRecherche(EOEditingContext editingContext, EOStructure nouvelleTutelle, EOStructure uniteRecherche, Integer persIdUtilisateur) {
		if (nouvelleTutelle != null) {
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext, nouvelleTutelle, uniteRecherche, persIdUtilisateur, FactoryAssociation.shared().tutelleAssociation(editingContext));
		}
	}


	public static void supprimerTutellePourStructureRecherche(EOEditingContext editingContext, EOStructure tutelle, EOStructure uniteRecherche, Integer persIdUtilisateur) throws Exception {
		if (tutelle != null) {
			EOQualifier qualifier = EODotationAnnuelle.STRUCTURE_PROVENANCE.eq(tutelle).and(EODotationAnnuelle.STRUCTURE.eq(uniteRecherche));
			if (EODotationAnnuelle.fetchAll(editingContext, qualifier).count() > 0) {
				throw new Exception("Il existe des dotations rattachées à cette tutelle");
			}
			if (ERXEOControlUtilities.eoEquals(tutelle, tutellePrincipalePourStructureRecherche(editingContext, uniteRecherche))) {
				throw new Exception("Vous ne pouvez pas supprimer la tutelle principale");
			}
			tutelle.supprimerLesRoles(editingContext, FactoryAssociation.shared().tutelleAssociation(editingContext), uniteRecherche, persIdUtilisateur);
			tutelle.supprimerLesRoles(editingContext, FactoryAssociation.shared().tutellePrincipaleAssociation(editingContext), uniteRecherche, persIdUtilisateur);
		}
	}
	
	
	public static NSArray<EOStructure> listeDesTutelles(EOEditingContext editingContext) {
		
		NSArray<EOStructure> tutelles = new NSArray<EOStructure>();
		String codeStructureGroupeTutelles = EOGrhumParametres.parametrePourCle(editingContext, "org.cocktail.sangria.groupes.tutelles");
		if (codeStructureGroupeTutelles != null) {
			EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.C_STRUCTURE).eq(codeStructureGroupeTutelles);
			return EOStructure.fetchAll(editingContext, qualifier);
		} 
		return tutelles;
		
	}

	public static EOStructure etablissementRattachementPrincipalPourStructureRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOAssociation association = FactoryAssociation.shared().etablissementRattachementPrincipalAssociation(editingContext);
		NSArray<EOStructure> etablissementsRattachementPrincipal =  (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);

		if (etablissementsRattachementPrincipal.isEmpty()) {
			return null;
		} else {
			return etablissementsRattachementPrincipal.get(0);
		}
	}

	public static void definirEtablissementRattachementPrincipalPourUniteRecherche(EOEditingContext editingContext, EOStructure nouvelEtablissement, EOStructure uniteRecherche, Integer persIdUtilisateur) {

		if (nouvelEtablissement == null) {
			return;
		}
		EOStructure etablissement = etablissementRattachementPrincipalPourStructureRecherche(editingContext, uniteRecherche);
 
		if (etablissement != null) {
			if (etablissement.equals(nouvelEtablissement)) {
				return;
			}
			etablissement.supprimerLesRoles(editingContext, FactoryAssociation.shared().etablissementRattachementPrincipalAssociation(editingContext), uniteRecherche, persIdUtilisateur);
		}

		EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext, nouvelEtablissement, uniteRecherche, persIdUtilisateur, FactoryAssociation.shared().etablissementRattachementPrincipalAssociation(editingContext));


	} 

	public static NSArray<EOStructure> etablissementsRattachementPourStructureRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOAssociation association = FactoryAssociation.shared().etablissementRattachementAssociation(editingContext);
		return (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	}

	public static void ajouterEtablissementRattachementPourUniteRecherche(EOEditingContext editingContext, EOStructure nouvelEtablissement, EOStructure uniteRecherche, Integer persIdUtilisateur) {
		if (nouvelEtablissement != null) {
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext, nouvelEtablissement, uniteRecherche, persIdUtilisateur, FactoryAssociation.shared().etablissementRattachementAssociation(editingContext));
		}
	}

	public static void supprimerEtablissementRattachementPourStructureRecherche(EOEditingContext editingContext, EOStructure etablissement, EOStructure uniteRecherche, Integer persIdUtilisateur) throws Exception {
		if (etablissement != null) {
			EOQualifier qualifier = EODotationAnnuelle.STRUCTURE_PROVENANCE.eq(etablissement).and(EODotationAnnuelle.STRUCTURE.eq(uniteRecherche));
			if (EODotationAnnuelle.fetchAll(editingContext, qualifier).count() > 0) {
				throw new Exception("Il existe des dotations rattachées à cet établissement");
			}
			if (ERXEOControlUtilities.eoEquals(etablissement, etablissementRattachementPrincipalPourStructureRecherche(editingContext, uniteRecherche))) {
				throw new Exception("Vous ne pouvez pas supprimer l'établissement princpal");
			}
			etablissement.supprimerLesRoles(editingContext, FactoryAssociation.shared().etablissementRattachementAssociation(editingContext), uniteRecherche, persIdUtilisateur);
		}
	}

	public static NSArray<EOStructure> equipesDeRecherchePourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		return (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(
					EORepartAssociation.TO_ASSOCIATION.eq(
							FactoryAssociation.shared().equipeDeRechercheAssociation(editingContext)
						)
				).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
		
	}

	public static NSArray<EOStructure> equipesDeRechercheActuellesPourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().equipeDeRechercheAssociation(editingContext)),
				ERXQ.or(
						EORepartAssociation.RAS_D_OUVERTURE.isNull(),
						EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(DateCtrl.getDateJour())
						),
				ERXQ.or(
						EORepartAssociation.RAS_D_FERMETURE.isNull(), 
						EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(DateCtrl.getDateJour())
						)
				);
		return (NSArray<EOStructure>) uniteRecherche.toRepartAssociationsElts(qualifier).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
		
	}

	public static NSArray<EORepartAssociation> repartAssociationsEquipesDeRecherchePourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_STRUCTURE.eq(uniteRecherche),
				EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().equipeDeRechercheAssociation(editingContext))
				);
		return EORepartAssociation.fetchAll(editingContext, qualifier);
	}

	public static NSArray<EORepartAssociation> repartAssociationsEquipesDeRechercheActuellesPourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_STRUCTURE.eq(uniteRecherche),
				EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().equipeDeRechercheAssociation(editingContext)),
				ERXQ.or(
						EORepartAssociation.RAS_D_OUVERTURE.isNull(),
						EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(DateCtrl.getDateJour())
						),
				ERXQ.or(
						EORepartAssociation.RAS_D_FERMETURE.isNull(), 
						EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(DateCtrl.getDateJour())
						)
				);
		
		return EORepartAssociation.fetchAll(editingContext, qualifier);
	}
	
	public static NSArray<EOReconnaissanceUnite> reconnaissancesPourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche,  Boolean includeEditingContextChanges) {
		EOQualifier qualifier = ERXQ.equals(EOReconnaissanceUnite.UNITE_KEY, uniteRecherche);
		ERXFetchSpecification<EOReconnaissanceUnite> fetchSpecification = new ERXFetchSpecification<EOReconnaissanceUnite>(EOReconnaissanceUnite.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);		
		return fetchSpecification.fetchObjects(editingContext);
	}

	public static NSArray<EOEvaluationMesr> evaluationsPourUniteRecherche(EOEditingContext editingContext, EOStructure uniteRecherche, Boolean includeEditingContextChanges) {
		EOQualifier qualifier = ERXQ.equals(EOEvaluationMesr.UNITE_KEY, uniteRecherche);
		ERXFetchSpecification<EOEvaluationMesr> fetchSpecification = new ERXFetchSpecification<EOEvaluationMesr>(EOEvaluationMesr.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(includeEditingContextChanges);		
		return fetchSpecification.fetchObjects(editingContext);		
	}
	
	public static NSArray<EOMotCle> motsClesPossiblesPourUniteRecherche(EOEditingContext edc, EOStructure uniteRecherche, Boolean checkDomainesScientifiques) {
		EODomaineScientifique domaineScientifiquePrincipal = domaineScientifiquePrincipalUniteRecherche(edc, uniteRecherche, true);
		NSArray<EODomaineScientifique> domainesScientifiquesSecondaires = domainesScientifiquesSecondaireUniteRecherches(edc, uniteRecherche, true);
		if (domaineScientifiquePrincipal == null && domainesScientifiquesSecondaires.isEmpty()) {
			return new NSArray<EOMotCle>();
		}
		
		EOQualifier qualifier = null;
		
		if (checkDomainesScientifiques) {
  		NSArray<EODomaineScientifique> domaineScientifiques = new NSMutableArray<EODomaineScientifique>();
  		if (domaineScientifiquePrincipal != null) {
  			domaineScientifiques.add(domaineScientifiquePrincipal);
  		}
  		if (!domainesScientifiquesSecondaires.isEmpty()) {
  			domaineScientifiques.addAll(domainesScientifiquesSecondaires);
  		}
  		qualifier = ERXQ.in(EORepartMotCleDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY, domaineScientifiques);
		}
		ERXFetchSpecification<EORepartMotCleDomaineScientifique> fetchSpecification = new ERXFetchSpecification<EORepartMotCleDomaineScientifique>(EORepartMotCleDomaineScientifique.ENTITY_NAME, qualifier, EORepartMotCleDomaineScientifique.MOT_CLE.dot(EOMotCle.LIBELLE).ascInsensitives());
		NSArray<EOMotCle> motsCles = new NSMutableArray<EOMotCle>();
		NSArray<EOMotCle> tousLesMotsCles = (NSArray<EOMotCle>) fetchSpecification.fetchObjects(edc).valueForKey(EORepartMotCleDomaineScientifique.MOT_CLE_KEY);
		for (EOMotCle motCle : tousLesMotsCles) {
			if (!motsCles.contains(motCle)) {
				motsCles.add(motCle);
			}
		}
		return motsCles;
	}
	
	public static NSArray<EOMotCle> motsClesDefinitsPourUniteRecherche(EOEditingContext edc, EOStructure uniteRecherche) {
		EOQualifier qualifier = ERXQ.equals(EORepartMotCleUniteRecherche.UNITE_RECHERCHE_KEY, uniteRecherche);
		NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EORepartMotCleUniteRecherche.MOT_CLE_KEY + "." + EOMotCle.LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		ERXFetchSpecification<EORepartMotCleUniteRecherche> fetchSpecification = new ERXFetchSpecification<EORepartMotCleUniteRecherche>(EORepartMotCleUniteRecherche.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpecification.setIncludeEditingContextChanges(true);
		return (NSArray<EOMotCle>) fetchSpecification.fetchObjects(edc).valueForKey(EORepartMotCleUniteRecherche.MOT_CLE_KEY);
	}
	
	public static NSArray<EOStructure> federationsRecherche(EOEditingContext edc) {
		EOQualifier qualifier = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "FR");
		ERXFetchSpecification<EOStructure> fetchSpecification = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifier, null);
		return edc.objectsWithFetchSpecification(fetchSpecification);
	}

	
	public static NSArray<EOStructure> ecolesDoctorale(EOEditingContext edc) {
		EOQualifier qualifier = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED");
		ERXFetchSpecification<EOStructure> fetchSpecification = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifier, null);
		return edc.objectsWithFetchSpecification(fetchSpecification);
	}

	public static NSArray<EOStructure> unitesPourFederation(EOEditingContext edc, EOStructure federation, Boolean includeEditingContextChanges) {
		
		EOAssociation association = FactoryAssociation.shared().membreAssociation(edc);
		return (NSArray<EOStructure>) federation.toRepartAssociationsElts(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	}
	
	public static void ajouterUniteAFederation(EOEditingContext edc, EOStructure uniteRecherche, EOStructure federationRecherche, Integer utilisateurPersId) {
		if (!unitesPourFederation(edc, federationRecherche, true).contains(uniteRecherche)) {
			uniteRecherche.definitUnRole(edc, FactoryAssociation.shared().membreAssociation(edc), federationRecherche, utilisateurPersId, null, null, null, null, 1);
			//EOStructureForGroupeSpec.affecterPersonneDansGroupe(edc, uniteRecherche, federationRecherche, utilisateurPersId, factoryAssociation.membreAssociation());
		}
	}
	
	public static void supprimerUniteAFederation(EOEditingContext edc, EOStructure uniteRecherche, EOStructure federationRecherche) {
		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, federationRecherche),
				ERXQ.containsObject(EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY, uniteRecherche),
				ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, FactoryAssociation.shared().membreAssociation(edc))
		);
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<EORepartAssociation> repartAssociations = fetchSpecification.fetchObjects(edc);
		if (!repartAssociations.isEmpty()) {
			edc.deleteObject(repartAssociations.get(0));
		}
	}
	
	public static EOStructure unitePourEquipe(EOEditingContext editingContext, EOStructure equipe) {
		EOAssociation association = FactoryAssociation.shared().equipeDeRechercheAssociation(editingContext);
		NSArray<EOStructure> unites =  (NSArray<EOStructure>) equipe.toRepartAssociations(EORepartAssociation.TO_ASSOCIATION.eq(association)).valueForKey(EORepartAssociation.TO_STRUCTURE_KEY);
		if (unites.isEmpty()) {
			return null;
		} else {
			return unites.get(0);
		}
	}
	
	public static NSArray<EOStructure> unitesDeRecherche(EOEditingContext editingContext) {

    	EOQualifier qualifier = 		
    		ERXQ.and(
    			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_LA), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
    			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_SR), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
    			EOStructure.QUAL_STRUCTURES_VALIDE
    		);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	return EOStructure.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EOStructure> federationsDeRecherche(EOEditingContext editingContext) {
    	EOQualifier qualifier = 		
        		ERXQ.and(
        			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_FR), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
        			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_SR), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
        			EOStructure.QUAL_STRUCTURES_VALIDE
        		);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	return EOStructure.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EOStructure> cellulesDeValorisation(EOEditingContext editingContext) {
    	EOQualifier qualifier = 		
        		ERXQ.and(
        			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq("CV"), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
        			new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_SR), EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
        			EOStructure.QUAL_STRUCTURES_VALIDE
        		);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	return EOStructure.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EOStructure> autresStructuresDeRecherche(EOEditingContext editingContext) {
	  /*
	  EOQualifier subQualifier = EORepartTypeGroupe.TGRP_CODE.ne("LA").and(EORepartTypeGroupe.TGRP_CODE.ne("ED"))
	      .and(EORepartTypeGroupe.TGRP_CODE.ne("FR")).and(EORepartTypeGroupe.TGRP_CODE.ne("CV"));
	  EOQualifier qualifier = EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE).eq("SR").and(
	      new ERXQualifierInSubquery(subQualifier, EOStructure.ENTITY_NAME, EOStructure.TO_REPART_TYPE_GROUPES_KEY));
	  */
	  
		EOQualifier qualifier = ERXQ.and(
									ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "SR"),
									EOStructure.QUAL_STRUCTURES_VALIDE
								);
    	EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
    	
    	NSArray<EOStructure> resultats = EOStructure.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order)).mutableClone();
    	
    	resultats.removeAll(unitesDeRecherche(editingContext));
    	resultats.removeAll(ecolesDoctorale(editingContext));
    	resultats.removeAll(federationsDeRecherche(editingContext));
    	resultats.removeAll(cellulesDeValorisation(editingContext));
    	
    	return resultats;
    	
	}
	
	public static Boolean isStructureRecherche(EOStructure structure) {
	  if (structure == null) {
	    return false;
	  }
	  return !structure.toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_SR)).isEmpty();
	}
	
	public static Boolean isUniteDeRecherche(EOStructure structure) {
		if (structure == null) {
			return false;
		}
		if (!isStructureRecherche(structure)) {
			return false;
		}
		return !structure.toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_LA)).isEmpty();
	}

	public static Boolean isFederationDeRecherche(EOStructure structure) {
		if (structure == null) {
			return false;
		}
		if (!isStructureRecherche(structure)) {
			return false;
		}
		return !structure.toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_FR)).isEmpty();
	}	
	
	public static Boolean isEcoleDoctorale(EOStructure structure) {
		if (structure == null) {
			return false;
		}
		if (!isStructureRecherche(structure)) {
			return false;
		}
		return !structure.toRepartTypeGroupes(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_ED)).isEmpty();
	}	
	
	private static EOQualifier qualifierForStructureRecherchePourIndividu(EOIndividu individu) {
		return ERXQ.and(
					ERXQ.or(
							EOStructure.TO_REPART_ASSOCIATIONS_ELTS.dot(EORepartAssociation.TO_INDIVIDUS_ASSOCIES).containsObject(individu),
							EOStructure.TO_REPART_STRUCTURES_ELTS.dot(EORepartStructure.TO_INDIVIDU_ELTS).containsObject(individu)
					),
					new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_LA).or(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_ED)).or(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_FR)),
							EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY),
					new ERXQualifierInSubquery(EORepartTypeGroupe.TGRP_CODE.eq(EOTypeGroupe.TGRP_CODE_SR), 
							EORepartTypeGroupe.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, EORepartTypeGroupe.C_STRUCTURE_KEY)
				);
	}
	
	public static NSArray<EOStructure> structuresRecherchePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		return ERXArrayUtilities.arrayWithoutDuplicates(EOStructure.fetchAll(editingContext, qualifierForStructureRecherchePourIndividu(individu)));
	}
	
	/**
	 * 
	 * @param editingContext editing context
	 * @param individu individu pour lequel on recherche la structure recherche
	 * @return la structure recherche la plus recente
	 */
	public static EOStructure structureRechercheActuellePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.TO_REPART_ASSOCIATIONS_ELTS.dot(EORepartAssociation.RAS_D_OUVERTURE).key(), EOSortOrdering.CompareDescending);
		NSArray<EOStructure> str = ERXArrayUtilities.arrayWithoutDuplicates(EOStructure.fetchAll(
				editingContext, qualifierForStructureRecherchePourIndividu(individu), new NSArray<EOSortOrdering>(order)));
		
		if (str != null) {
			return str.get(0);
		}
		
		return null;
	}
	
	public static EORepartAssociation repartAssociationStructureRechercheActuellePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_INDIVIDUS_ASSOCIES.containsObject(individu),
				EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_LA)
					.or(EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_ED))
					.or(EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_FR)),
				EORepartAssociation.RAS_D_OUVERTURE.isNotNull(),
				ERXQ.or(
						EORepartAssociation.RAS_D_FERMETURE.isNull(), 
						EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(DateCtrl.getDateJour())
						)
				);
		
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EORepartAssociation.RAS_D_OUVERTURE.key(), EOSortOrdering.CompareDescending);
		
		NSArray<EORepartAssociation> ass = EORepartAssociation.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
		
		return EORepartAssociation.fetchFirstByQualifier(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EORepartAssociation> repartsAssociationsStructureRechercheActuellePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_INDIVIDUS_ASSOCIES.containsObject(individu),
				EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_LA)
					.or(EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_ED)),
				EORepartAssociation.RAS_D_OUVERTURE.isNotNull(),
				ERXQ.or(
						EORepartAssociation.RAS_D_FERMETURE.isNull(), 
						EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(DateCtrl.getDateJour())
						)
				);
		
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EORepartAssociation.RAS_D_OUVERTURE.key(), EOSortOrdering.CompareDescending);
		
		return EORepartAssociation.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EORepartAssociation> repartsAssociationsStructureRecherchePasseesPourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qualifier = ERXQ.and(
				EORepartAssociation.TO_INDIVIDUS_ASSOCIES.containsObject(individu),
				EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_LA)
					.or(EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(EOTypeGroupe.TGRP_CODE_ED)),
				EORepartAssociation.RAS_D_OUVERTURE.isNotNull(),
				EORepartAssociation.RAS_D_FERMETURE.lessThanOrEqualTo(DateCtrl.getDateJour())
				);
		
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EORepartAssociation.RAS_D_OUVERTURE.key(), EOSortOrdering.CompareDescending);
		
		return EORepartAssociation.fetchAll(editingContext, qualifier, new NSArray<EOSortOrdering>(order));
	}
	
	public static NSArray<EOStructure> repartsStructuresRecherchePourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		return ERXArrayUtilities.arrayWithoutDuplicates(EOStructure.fetchAll(editingContext, qualifierForStructureRecherchePourIndividu(individu)));
	}
	
	/**
	 * @param editingContext editing context
	 * @param typeGroupe EOTypeGroupe le type du groupe
	 * @param structure structure dont on veut connaitre l'appartenance à une structure d'un groupe donné
	 * @return la liste des structures d'un groupe donné dont la structure passée en paramètre est membre
	 */
	public static NSArray<EORepartAssociation> repartsAssociationsTypeGroupeDUneStructure(EOEditingContext editingContext, String typeGroupe, EOStructure structure) {
		EOQualifier qual = ERXQ.and(
				 EORepartAssociation.TO_STRUCTURES_ASSOCIEES.eq(structure),
				 EORepartAssociation.TO_ASSOCIATION.eq(FactoryAssociation.shared().membreAssociation(editingContext)),
				 EORepartAssociation.TO_STRUCTURE.dot(EOStructure.TO_REPART_TYPE_GROUPES.dot(EORepartTypeGroupe.TGRP_CODE)).eq(typeGroupe)
				 );
		
		EOQualifier dateQualifier = ERXQ.and(
					EORepartAssociation.RAS_D_OUVERTURE.lessThanOrEqualTo(ERXTimestampUtilities.today()),
					ERXQ.or(
							EORepartAssociation.RAS_D_FERMETURE.greaterThanOrEqualTo(ERXTimestampUtilities.today()),
							EORepartAssociation.RAS_D_FERMETURE.isNull()
					)
				);
		qual = ERXQ.and(qual, dateQualifier);
		 
		return EORepartAssociation.fetchAll(editingContext, qual);
	}
	
	/**
	 * @param editingContext editing context
	 * @param structure structure dont on veut connaitre la (ou les) école(s) doctorale(s)
	 * @return la liste des ecoles doctorales dont la structure est membre
	 */
	public static NSArray<EORepartAssociation> repartsAssociationsEcolesDoctoralesDUneStructure(EOEditingContext editingContext, EOStructure structure) {
		return repartsAssociationsTypeGroupeDUneStructure(editingContext, EOTypeGroupe.TGRP_CODE_ED, structure);
	}
	
	/**
	 * @param editingContext editing context
	 * @param structure structure dont on veut connaitre la (ou les) école(s) doctorale(s)
	 * @return la liste des fédérations de recherche dont la structure est membre
	 */
	public static NSArray<EORepartAssociation> repartsAssociationsFederationsRechercheDUneStructure(EOEditingContext editingContext, EOStructure structure) {
		return repartsAssociationsTypeGroupeDUneStructure(editingContext, EOTypeGroupe.TGRP_CODE_FR, structure);
	}
	
}
