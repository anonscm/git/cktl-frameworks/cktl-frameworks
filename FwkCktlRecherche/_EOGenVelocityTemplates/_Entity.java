/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to ${entity.classNameWithOptionalPackage}.java instead.
#if ($entity.superclassPackageName)
package $entity.superclassPackageName;
#end

import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXGenericRecord;

public abstract class ${entity.prefixClassNameWithoutPackage} extends #if ($entity.parentClassNameSet)${entity.parentClassName}#elseif ($entity.partialEntitySet)er.extensions.partials.ERXPartial<${entity.partialEntity.className}>#elseif ($entity.parentSet)${entity.parent.classNameWithDefault}#elseif ($EOGenericRecord)${EOGenericRecord}#else ERXGenericRecord#end {
	public static final String ENTITY_NAME = "$entity.name";

	// Attributes
#set( $pkCount = $entity.primaryKeyAttributes.size() )  
#if ($pkCount == 1)
	public static final String ENTITY_PRIMARY_KEY = "$entity.singlePrimaryKeyAttribute.name";
#end	

#foreach ($attribute in $entity.sortedClassAttributes)
	public static final er.extensions.eof.ERXKey<$attribute.getJavaClassName(false)> ${attribute.uppercaseUnderscoreName} = new er.extensions.eof.ERXKey<$attribute.getJavaClassName(false)>("$attribute.name");
#end
	
#foreach ($attribute in $entity.sortedClassAttributes)
	public static final String ${attribute.uppercaseUnderscoreName}_KEY = ${attribute.uppercaseUnderscoreName}.key();
#end

	// Attributs non visibles
#foreach ($attribute in $entity.nonClassAttributes)
	public static final String ${attribute.uppercaseUnderscoreName}_KEY = "$attribute.name";
#end

	//Colonnes dans la base de donnees
#foreach ($attribute in $entity.sortedClassAttributes)
	public static final String ${attribute.uppercaseUnderscoreName}_COLKEY = "$attribute.columnName";
#end

#foreach ($attribute in $entity.nonClassAttributes)
	public static final String ${attribute.uppercaseUnderscoreName}_COLKEY = "$attribute.columnName";
#end

	// Relationships
#foreach ($relationship in $entity.sortedClassRelationships)
    public static final er.extensions.eof.ERXKey<$relationship.actualDestination.classNameWithDefault> ${relationship.uppercaseUnderscoreName} = new er.extensions.eof.ERXKey<$relationship.actualDestination.classNameWithDefault>("$relationship.name");
#end
	
#foreach ($relationship in $entity.sortedClassRelationships)
	public static final String ${relationship.uppercaseUnderscoreName}_KEY = ${relationship.uppercaseUnderscoreName}.key();
#end

	// Accessors methods
#foreach ($attribute in $entity.sortedClassAttributes)
#if ($attribute.userInfo.ERXConstantClassName)
	public $attribute.userInfo.ERXConstantClassName ${attribute.name}() {
		Number value = (Number)storedValueForKey(${attribute.uppercaseUnderscoreName}_KEY);
	    return ($attribute.userInfo.ERXConstantClassName)value;
	}

	public void set${attribute.capitalizedName}($attribute.userInfo.ERXConstantClassName value) {
		takeStoredValueForKey(value, ${attribute.uppercaseUnderscoreName}_KEY);
	}
	
#else
	public $attribute.getJavaClassName(false) ${attribute.name}() {
		return ($attribute.getJavaClassName(false)) storedValueForKey(${attribute.uppercaseUnderscoreName}_KEY);
	}

	public void set${attribute.capitalizedName}($attribute.getJavaClassName(false) value) {
	    takeStoredValueForKey(value, ${attribute.uppercaseUnderscoreName}_KEY);
	}
	
#end
#end
#foreach ($relationship in $entity.sortedClassToOneRelationships)
#if (!$relationship.inherited) 
	public $relationship.actualDestination.classNameWithDefault ${relationship.name}() {
	    return ($relationship.actualDestination.classNameWithDefault) storedValueForKey(${relationship.uppercaseUnderscoreName}_KEY);
	}

	public void set${relationship.capitalizedName}Relationship($relationship.actualDestination.classNameWithDefault value) {
	    if (value == null) {
	    	$relationship.actualDestination.classNameWithDefault oldValue = ${relationship.name}();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ${relationship.uppercaseUnderscoreName}_KEY);
	    	}
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, ${relationship.uppercaseUnderscoreName}_KEY);
	    }
	}
	
#end
#end
#foreach ($relationship in $entity.sortedClassToManyRelationships)
#if (!$relationship.inherited) 
	public com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> ${relationship.name}() {
	    return (com.webobjects.foundation.NSArray)storedValueForKey(${relationship.uppercaseUnderscoreName}_KEY);
	}

#if (!$relationship.inverseRelationship || $relationship.flattened || !$relationship.inverseRelationship.classProperty)
	public com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> ${relationship.name}(EOQualifier qualifier) {
		return ${relationship.name}(qualifier, null);
	}
	
#else
	public com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> ${relationship.name}(EOQualifier qualifier) {
		return ${relationship.name}(qualifier, null, false);
	}

	public com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> ${relationship.name}(EOQualifier qualifier, boolean fetch) {
	    return ${relationship.name}(qualifier, null, fetch);
	}
	
#end
	public com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> ${relationship.name}(EOQualifier qualifier, com.webobjects.foundation.NSArray sortOrderings#if ($relationship.inverseRelationship && !$relationship.flattened && $relationship.inverseRelationship.classProperty), boolean fetch#end) {
		com.webobjects.foundation.NSArray<$relationship.actualDestination.classNameWithDefault> results;
#if ($relationship.inverseRelationship && !$relationship.flattened && $relationship.inverseRelationship.classProperty)
    	if (fetch) {
    		EOQualifier fullQualifier;
#if (${relationship.actualDestination.genericRecord})
			EOQualifier inverseQualifier = new EOKeyValueQualifier("${relationship.inverseRelationship.name}", EOQualifier.QualifierOperatorEqual, this);
#else
			EOQualifier inverseQualifier = new EOKeyValueQualifier(${relationship.actualDestination.classNameWithDefault}.${relationship.inverseRelationship.uppercaseUnderscoreName}_KEY, EOQualifier.QualifierOperatorEqual, this);
#end
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			} else {
				com.webobjects.foundation.NSMutableArray<EOQualifier> qualifiers = new com.webobjects.foundation.NSMutableArray<EOQualifier>();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}
#if (${relationship.actualDestination.genericRecord})
			EOFetchSpecification fetchSpec = new EOFetchSpecification("${relationship.actualDestination.name}", qualifier, sortOrderings);
			fetchSpec.setIsDeep(true);
			results = (com.webobjects.foundation.NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
#else
			results = ${relationship.actualDestination.classNameWithDefault}.fetchAll(editingContext(), fullQualifier, sortOrderings);
#end
    	} else {
#end
			results = ${relationship.name}();
			if (qualifier != null) {
				results = (com.webobjects.foundation.NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
			}
			if (sortOrderings != null) {
				results = (com.webobjects.foundation.NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
			}
#if ($relationship.inverseRelationship && !$relationship.flattened && $relationship.inverseRelationship.classProperty)
    	}
#end
		return results;
	}
	  
	public void addTo${relationship.capitalizedName}Relationship($relationship.actualDestination.classNameWithDefault object) {
		addObjectToBothSidesOfRelationshipWithKey(object, ${relationship.uppercaseUnderscoreName}_KEY);
	}

	public void removeFrom${relationship.capitalizedName}Relationship($relationship.actualDestination.classNameWithDefault object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, ${relationship.uppercaseUnderscoreName}_KEY);
	}

	public $relationship.actualDestination.classNameWithDefault create${relationship.capitalizedName}Relationship() {
	    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("${relationship.actualDestination.name}");
	    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
	    editingContext().insertObject(eo);
	    addObjectToBothSidesOfRelationshipWithKey(eo, ${relationship.uppercaseUnderscoreName}_KEY);
	    return ($relationship.actualDestination.classNameWithDefault) eo;
	}

	public void delete${relationship.capitalizedName}Relationship($relationship.actualDestination.classNameWithDefault object) {
	    removeObjectFromBothSidesOfRelationshipWithKey(object, ${relationship.uppercaseUnderscoreName}_KEY);
#if (!$relationship.ownsDestination)
	    editingContext().deleteObject(object);
#end
	}

	public void deleteAll${relationship.capitalizedName}Relationships() {
		java.util.Enumeration objects = ${relationship.name}().immutableClone().objectEnumerator();
	    while (objects.hasMoreElements()) {
	    	delete${relationship.capitalizedName}Relationship(($relationship.actualDestination.classNameWithDefault) objects.nextElement());
	    }
	}

#end
#end
	public #if (!$entity.partialEntitySet)static #end${entity.classNameWithOptionalPackage}#if (!$entity.partialEntitySet) create#else init#end${entity.name}(EOEditingContext editingContext#foreach ($attribute in $entity.sortedClassAttributes)
#if (!$attribute.allowsNull)
#set ($restrictingQualifierKey = 'false')
#foreach ($qualifierKey in $entity.restrictingQualifierKeys)#if ($attribute.name == $qualifierKey)#set ($restrictingQualifierKey = 'true')#end#end
#if ($restrictingQualifierKey == 'false')
#if ($attribute.userInfo.ERXConstantClassName), ${attribute.userInfo.ERXConstantClassName}#else, ${attribute.getJavaClassName(false)}#end ${attribute.name}#end
#end
#end
#foreach ($relationship in $entity.sortedClassToOneRelationships)
#if ($relationship.mandatory && !($relationship.ownsDestination && $relationship.propagatesPrimaryKey)), ${relationship.actualDestination.classNameWithDefault} ${relationship.name}#end
#end
) {
		${entity.classNameWithOptionalPackage} eo = (${entity.classNameWithOptionalPackage})#if ($entity.partialEntitySet)this;#else EOUtilities.createAndInsertInstance(editingContext, ${entity.prefixClassNameWithoutPackage}.ENTITY_NAME);#end
    
#foreach ($attribute in $entity.sortedClassAttributes)
#if (!$attribute.allowsNull)
#set ($restrictingQualifierKey = 'false')
#foreach ($qualifierKey in $entity.restrictingQualifierKeys) 
#if ($attribute.name == $qualifierKey)
#set ($restrictingQualifierKey = 'true')
#end
#end
#if ($restrictingQualifierKey == 'false')
		eo.set${attribute.capitalizedName}(${attribute.name});
#end
#end
#end
#foreach ($relationship in $entity.sortedClassToOneRelationships)
#if ($relationship.mandatory && !($relationship.ownsDestination && $relationship.propagatesPrimaryKey))
		eo.set${relationship.capitalizedName}Relationship(${relationship.name});
#end
#end
		return eo;
	}

	public $entity.classNameWithOptionalPackage localInstanceIn(EOEditingContext editingContext) {
		return (${entity.classNameWithOptionalPackage}) er.extensions.eof.ERXEOControlUtilities.localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param eoeditingcontext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static $entity.classNameWithOptionalPackage creerInstance(EOEditingContext editingContext) {
		return ($entity.classNameWithOptionalPackage) er.extensions.eof.ERXEOControlUtilities.createAndInsertObject(editingContext, ${entity.prefixClassNameWithoutPackage}.ENTITY_NAME);
	}

	/* Finders */
#if ($entity.parentSet)
#else
	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, null, null);
	}

	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	}
	  
	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
	    return fetchAll(editingContext, qualifier, null);
	}

	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}
	  
	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		er.extensions.eof.ERXFetchSpecification<${entity.classNameWithOptionalPackage}> fetchSpec = new er.extensions.eof.ERXFetchSpecification<${entity.classNameWithOptionalPackage}>(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    return fetchSpec.fetchObjects(editingContext);
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, com.webobjects.foundation.NSArray sortOrderings).
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throw IllegalStateException Si plusieurs objets sont retrournes 
	 */
	public static ${entity.classNameWithOptionalPackage} fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static ${entity.classNameWithOptionalPackage} fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> eoObjects = fetchAll(editingContext, qualifier, null);
	    ${entity.classNameWithOptionalPackage} eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}

	public static ${entity.classNameWithOptionalPackage} fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}
	  
	public static ${entity.classNameWithOptionalPackage} fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    ${entity.classNameWithOptionalPackage} eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else {
	    	eoObject = eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	}  
	  
	/**
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException Si aucun objet est trouvé.
	 */
	public static ${entity.classNameWithOptionalPackage} fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		${entity.classNameWithOptionalPackage} eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	    	throw new NoSuchElementException("Aucun objet ${entity.classNameWithOptionalPackage} ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	
	public static ${entity.classNameWithOptionalPackage} fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	
#end
	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll${entity.pluralName}(EOEditingContext editingContext) {
		return ${entity.prefixClassNameWithoutPackage}.fetchAll${entity.pluralName}(editingContext, null);
	}

	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetchAll${entity.pluralName}(EOEditingContext editingContext, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		return ${entity.prefixClassNameWithoutPackage}.fetch${entity.pluralName}(editingContext, null, sortOrderings);
	}

	public static com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> fetch${entity.pluralName}(EOEditingContext editingContext, EOQualifier qualifier, com.webobjects.foundation.NSArray<EOSortOrdering> sortOrderings) {
		er.extensions.eof.ERXFetchSpecification<${entity.classNameWithOptionalPackage}> fetchSpec = new er.extensions.eof.ERXFetchSpecification<${entity.classNameWithOptionalPackage}>(${entity.prefixClassNameWithoutPackage}.ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> eoObjects = fetchSpec.fetchObjects(editingContext);
		return eoObjects;
	}

	public static ${entity.classNameWithOptionalPackage} fetch${entity.name}(EOEditingContext editingContext, String keyName, Object value) {
		return ${entity.prefixClassNameWithoutPackage}.fetch${entity.name}(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	public static ${entity.classNameWithOptionalPackage} fetch${entity.name}(EOEditingContext editingContext, EOQualifier qualifier) {
	    com.webobjects.foundation.NSArray<${entity.classNameWithOptionalPackage}> eoObjects = ${entity.prefixClassNameWithoutPackage}.fetch${entity.pluralName}(editingContext, qualifier, null);
	    ${entity.classNameWithOptionalPackage} eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	    	eoObject = null;
	    } else if (count == 1) {
	    	eoObject = eoObjects.objectAtIndex(0);
	    } else {
	    	throw new IllegalStateException("There was more than one ${entity.name} that matched the qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	}
	
}
