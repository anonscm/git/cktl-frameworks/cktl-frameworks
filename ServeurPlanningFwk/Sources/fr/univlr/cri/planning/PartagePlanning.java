package fr.univlr.cri.planning;

import java.util.Enumeration;
import java.util.Properties;

import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.cri.planning._imports.CktlLog;
import fr.univlr.cri.planning._imports.DateCtrl;

/**
 * <body> Cette classe contient les methodes nécessaires à une DirectAction 
 * pour un partage d'information avec le Serveur de Planning.<br>
 * <br>
 * <strong>On y trouve :</strong><br>
 * <br> _ 1 methode de recupération des parametres envoyés par le serveur;<br> _
 * 3 methodes de formatage de la reponse avant renvoit au serveur;<br>
 * <br>
 * 
 * <strong>Avant d'utiliser ces methodes, certaines données doivent etre
 * renseignées dans la base de données :</strong><br>
 * <br> _ table sp_methode : 1 ligne pour la DirectAction qui enverra des
 * données au serveur (avec attribut met_bool_serveur à 1);<br> _ table
 * sp_met_serveur : 1 ligne indiquant l'URI ("/wa/maMethode?"), l'id_Appli (voir
 * Saut.Application), le type de passage des parametres (1: url, 2: content) et
 * le type de données retourné;<br> _ table sp_param : indiquer les parametres
 * a fournir avec attribut Place à 1 pour parametre consernant l'identifiant, 2
 * pour date debut, 3 pour date fin et eventuellement 4 pour le type
 * identifiant.<br>
 * <br>
 * 
 * <strong>Exemple de code :</strong><br>
 * <br> 1/ Recupération des parametres reçus
 * <p class="example">
 * WORequest requete = context().request(); <br>
 * NSDictionary dicP = PartagePlanning.dicoParams(requete); <br> 
 * </p>
 * <br> 2/ retourner les resultats au Serveur de Planning
 *  <p class="example">
 * // exemple d'un resultat de type planning <br>
 * NSMutableArray plannings = new NSMutableArray();<br>
 * SPOccupation occ = new SPOccupation();<br>
 * // ... insérer les infos dans occ<br>
 * // ... créer autant de SPOccupation que nécessaire<br>
 * plannings.addObject(occ);<br>
 * // reponsePlanning insère les infos dans un WOResponse à retourner<br>
 * WOResponse resp = PartagePlanning.reponsePlanning(plannings, 1, null);<br>
 * return resp; <br>
 * </p>
 * 
 * @author Celine MENETREY <celine.menetrey at univ-lr.fr> <body/>
 */

public class PartagePlanning {

	// ********************************************************
	// Methodes pour Appli Serveur du Serveur de Planning
	// ********************************************************

	// methode de traitement de la requête (dans l'url)
	// renvoit dico des parametres transmis (sans modif)
	/**
	 * Methode de traitement de la requête, place les parametres dans un
	 * dictionaire.
	 * 
	 * @param requete
	 *          WORequest
	 * @return le NSDictionary des parametres.
	 */
	public static NSDictionary dicoParams(WORequest requete) {
		NSMutableDictionary dic = new NSMutableDictionary();

		String para = requete.contentString();
		if (para != null && para.length() != 0) {
			// ------------ param dans Content ------------

			Properties prop = SPMethodes.stringToProperties(para);
			Enumeration en = prop.keys();

			while (en.hasMoreElements()) {

				String nomParam = en.nextElement().toString();

				String param = prop.getProperty(nomParam).toString();
				try {
					Integer in = new Integer(param);
					dic.takeValueForKey((Number) in, nomParam);
				} catch (NumberFormatException n) {
					NSTimestamp in = DateCtrl.stringToDate(param,
							SPConstantes.DATE_FORMAT);
					if (in != null)
						dic.takeValueForKey(in, nomParam);
					else
						dic.takeValueForKey(param, nomParam);
				}
			}
		} else // ------------- param dans url ------------------
		{
			NSArray arr = requete.formValueKeys(); // tablo des clés

			for (int i = 0; i < arr.count(); i++) {
				if (arr.objectAtIndex(i) != null) // pour valeurs renseignées
				{
					String nomParam = arr.objectAtIndex(i).toString();

					// essaie de caster en Number puis NSTimestamp, si pb laisse String
					String param = requete.formValueForKey(nomParam).toString();
					try {
						Integer in = new Integer(param);
						dic.takeValueForKey((Number) in, nomParam);
					} catch (NumberFormatException n) {
						NSTimestamp in = DateCtrl.stringToDate(param,
								SPConstantes.DATE_FORMAT);
						if (in != null)
							dic.takeValueForKey(in, nomParam);
						else
							dic.takeValueForKey(param, nomParam);
					}
				}
			}
		}

//		dic.takeValueForKey(requete.headerForKey(SPConstantes.PAR1_METHCLIENT),
//				SPConstantes.PAR1_METHCLIENT);

		return new NSDictionary(dic);
	}

	// *********** methodes de traitement de la reponse ************

	// ----------- reponse Occupations --------------
	/**
	 * Methode de formatage des resultats Planning pour renvoit au serveur.
	 * 
	 * @param trans
	 *          NSArray d'SPOccupation; <br>
	 * @param statut
	 *          int à 1 si aucune erreur, à 0 si probleme rencontré;<br>
	 * @param erreur
	 *          String définissant l'erreur survenue ou null.
	 * @return le WOResponse à retourner.
	 * @see SPOccupation
	 */
	public static WOResponse reponsePlanning(
			NSArray trans, int statut, String erreur) {
		StringBuffer buffer = new StringBuffer();

		String n = new Integer(0).toString();
		int nbErr = 0;
		for (int i = 0; i < trans.count(); i++) {
			// recupérer les Occupations
			Object test = trans.objectAtIndex(i);
			SPOccupation req = new SPOccupation();
			if (test instanceof SPOccupation) {
				req = (SPOccupation) test;

				String rep = SPConstantes.PROP_OC_DEB	+ n	+ "="	+ DateCtrl.dateToString(req.getDateDebut(),	SPConstantes.DATE_FORMAT);
				buffer.append(rep + "\n");
				String rep2 = SPConstantes.PROP_OC_FIN + n + "=" + DateCtrl.dateToString(req.getDateFin(), 	SPConstantes.DATE_FORMAT);
				buffer.append(rep2 + "\n");
				String rep3 = SPConstantes.PROP_OC_TYPE + n + "=" + req.getTypeTemps();
				buffer.append(rep3 + "\n");

				if (req.getAffichage().equals(SPConstantes.AFF_PRIVEE)) {
					String rep4 = SPConstantes.PROP_OC_AFF + n + "=" + req.getAffichage();
					buffer.append(rep4 + "\n");
				}
				
				if (req.getDetailsTemps() != null) {
					String rep4 = SPConstantes.PROP_OC_DETAIL + n + "=" + req.getDetailsTemps();
					buffer.append(rep4 + "\n");
				}
			} else {
				 // probleme de type de variable
				if (statut == 1) {
					statut = 0;
					erreur = "Un objet du NSArray reponse fourni par DirectAction n'est pas une SPOccupation.";
				}
				nbErr++;
			}

			n = new Integer(i + 1 - nbErr).toString();
		} // fin for

		buffer.append(SPConstantes.PROP_OC_NB + "=" + n + "\n");

		buffer.append(SPConstantes.PROP_STATUT + "=" + String.valueOf(statut) + "\n");

		if (statut != 1 && erreur != null) {
			buffer.append(SPConstantes.PROP_ERREUR + "=" + erreur + "\n");
		}

		WOResponse resultat = new WOResponse();
		String charset = "utf-8";
		if (WOMessage.defaultEncoding().equals("ISO8859_1")) {
			charset = "iso-8859-1";
		}
		resultat.setHeader("text/html; charset=" + charset, "content-type");
		resultat.setContent(buffer.toString());
		
		return resultat;

	}

	// ------------- reponse Numeric ---------
	/**
	 * Methode de formatage de resultat numérique pour renvoit au serveur.
	 * 
	 * @param num
	 *          Number resultat; <br>
	 * @param statut
	 *          int à 1 si aucune erreur, à 0 si probleme rencontré;<br>
	 * @param erreur
	 *          String définissant l'erreur survenue ou null.
	 * @return le WOResponse à retourner.
	 */
	public static WOResponse reponseNumeric(Number num, int statut, String erreur) {
		StringBuffer buffer = new StringBuffer();

		String rep = SPConstantes.PROP_NUMB + "=" + String.valueOf(num.intValue());
		buffer.append(rep + "\n");

		buffer
				.append(SPConstantes.PROP_STATUT + "=" + String.valueOf(statut) + "\n");

		if (statut != 1 && erreur != null)
			buffer.append(SPConstantes.PROP_ERREUR + "=" + erreur + "\n");

		WOResponse resultat = new WOResponse();
		resultat.setContent(buffer.toString());
		return resultat;
	}

	/**
	 * Methode de formatage de resultat booleen pour renvoit au serveur.
	 * 
	 * @param bool
	 *          Boolean resultat; <br>
	 * @param statut
	 *          int à 1 si aucune erreur, à 0 si probleme rencontre;<br>
	 * @param erreur
	 *          String définissant l'erreur survenue ou null.
	 * @return le WOResponse à retourner.
	 */
	public static WOResponse reponseBooleen(Boolean bool, int statut,
			String erreur) {
		StringBuffer buffer = new StringBuffer();

		String rep = SPConstantes.PROP_BOOL + "=" + bool.toString(); // VERIFIER
		// AFFICHAGE
		// bool
		buffer.append(rep + "\n");

		buffer
				.append(SPConstantes.PROP_STATUT + "=" + String.valueOf(statut) + "\n");

		if (statut != 1 && erreur != null)
			buffer.append(SPConstantes.PROP_ERREUR + "=" + erreur + "\n");

		WOResponse resultat = new WOResponse();
		resultat.setContent(buffer.toString());
		return resultat;
	}

}
