package fr.univlr.cri.planning;

import java.io.Serializable;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.cri.planning._imports.DateCtrl;

/**
 * Cette classe est une structure créée pour stocker des informations de
 * Planning. Héritant de Object, cette classe gère 6 attributs accessible via
 * des methodes get et set : <br>
 * _ idKey : Number de valeur 1:INDIVIDU, 2:OBJET ou 3:SALLE; <br>
 * _ idVal : Number identifiant (ex : noIndividu); <br>
 * _ dateDebut : NSTimestamp de la date de debut de periode, format "dd/mm/aaaa
 * hh:mm"; <br>
 * _ dateFin : NSTimestamp de la date de fin de periode, format
 * "dd/mm/aaaa hh:mm"; <br>
 * _ typeTemps : String du motif d'occupation du temps; On peut trouver les
 * différents typeTemps dans la table SP_TYPE_TEMPS. <br>
 * _ detailsTemps : String de details; <br>
 * _ affichage : String de valeur SPConstantes.AFF_PUBLIC pour un affichage
 * public sinon SPConstantes.AFF_PRIVEE.<br>
 * <br>
 * 
 * 
 * @author Celine MENETREY <celine.menetrey at univ-lr.fr>
 */

public class SPOccupation
		extends Object
		implements Serializable, NSKeyValueCoding {

	private Number idKey; // 1 : INDIVIDU, 2 : OBJET, 3 : SALLE
	private Number idVal;
	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private String typeTemps; // OCCUPE (reunion...), TRAVAIL (horaire, H_Sup),

	// CONGES (FER_ANN...)
	private String detailsTemps;

	private String affichage = SPConstantes.AFF_PUBLIC;

	private String uid;

	/**
	 * constructeur par defaut, attributs à null.
	 * 
	 */
	public SPOccupation() {
		setDateDebut(null);
		dateFin = null;
		idKey = null;
		idVal = null;
		typeTemps = null;
		detailsTemps = null;
	}

	// autres constructeurs

	/**
	 * constructeur complet.
	 */

	public SPOccupation(
			NSTimestamp dateD, NSTimestamp dateF, Number idk, Number idv, String libTypeT, String detailsT, String affich) {
		setDateDebut(dateD);
		dateFin = dateF;
		idKey = idk;
		idVal = idv;
		typeTemps = libTypeT;
		detailsTemps = detailsT;
		if (affich.equals(SPConstantes.AFF_PRIVEE))
			affichage = affich;
	}

	// pour envoit methodes serveur -> serveur : ne renvoit pas idk et idv
	/**
	 * constructeur utile pour les envoies d'une DirectAction vers le Serveur de
	 * Planning. idKey et idval a null.
	 */
	public SPOccupation(
			NSTimestamp dateD, NSTimestamp dateF, String libTypeT, String detailsT) {
		setDateDebut(dateD);
		setDateFin(dateF);
		idKey = null;
		idVal = null;
		typeTemps = libTypeT;
		detailsTemps = detailsT;
	}

	/**
	 * Constructeur assurant la gestion des repetition via les ICS. On stocke
	 * l'UID des évenement pour effectuer des soustractions via les RECURRENCE
	 */
	public SPOccupation(
			NSTimestamp dateD, NSTimestamp dateF, String libTypeT, String detailsT, String anUid) {
		setDateDebut(dateD);
		setDateFin(dateF);
		idKey = null;
		idVal = null;
		typeTemps = libTypeT;
		detailsTemps = detailsT;
		uid = anUid;
	}

	/**
	 * constructeur utile pour les envoies d'une DirectAction vers le Serveur de
	 * Planning. idKey et idval a null.
	 */

	public SPOccupation(
			String dateD, String dateF, String libTypeT, String detailsT) {
		setDateDebut(DateCtrl.stringToDate(dateD, SPConstantes.DATE_FORMAT));
		setDateFin(DateCtrl.stringToDate(dateF, SPConstantes.DATE_FORMAT));
		idKey = null;
		idVal = null;
		typeTemps = libTypeT;
		detailsTemps = detailsT;
	}

	// pour envoit methode Cliente -> serveur : ne connait pas typeT et detailsT
	/**
	 * constructeur utile pour les envoies d'une Methode Cliente vers le Serveur
	 * de Planning. typetemps et detailsTemps a null.
	 */
	public SPOccupation(
			String dateD, String dateF, Number idk, Number idv) {
		setDateDebut(DateCtrl.stringToDate(dateD, SPConstantes.DATE_FORMAT));
		setDateFin(DateCtrl.stringToDate(dateF, SPConstantes.DATE_FORMAT));
		idKey = idk;
		idVal = idv;
		typeTemps = null;
		detailsTemps = null;
	}

	/**
	 * constructeur utile pour les envoies d'une Methode Cliente vers le Serveur
	 * de Planning. typetemps et detailsTemps a null.
	 */

	public SPOccupation(
			NSTimestamp dateD, NSTimestamp dateF, Number idk, Number idv) {
		setDateDebut(dateD);
		setDateFin(dateF);
		idKey = idk;
		idVal = idv;
		typeTemps = null;
		detailsTemps = null;
	}

	// autre

	/*
	 * public SPOccupation(NSDictionary rec, NSDictionary trad) { // le dico 'rec'
	 * est une structure dont les noms des champs sont // donnés par 'trad' //
	 * recup valeur String debut = rec.valueForKey(dicoInverse(trad,
	 * SPConstantes.OC_DEB)) .toString(); String fin =
	 * rec.valueForKey(dicoInverse(trad, SPConstantes.OC_FIN)) .toString(); String
	 * motif = rec.valueForKey(dicoInverse(trad, SPConstantes.OC_TYPE))
	 * .toString(); String detail = rec.valueForKey(dicoInverse(trad,
	 * SPConstantes.OC_DETAIL)) .toString();
	 * 
	 * if (debut.startsWith("(")) // j'enlève les (" ... ") debut =
	 * debut.substring(2, debut.length() - 2); if (fin.startsWith("(")) fin =
	 * fin.substring(2, fin.length() - 2); if (motif.startsWith("(")) motif =
	 * motif.substring(2, motif.length() - 2); if (detail.startsWith("(")) detail
	 * = detail.substring(2, detail.length() - 2);
	 * 
	 * setDateDebutFromString(debut); setDateFinFromString(fin);
	 * setDetailsTemps(detail); idVal = null; idKey = null; // val et key
	 * renseignées dans ServeurPlanning // a mettre en cache pour pas recréer a
	 * chaque fois ? NSDictionary dic = new SPDataBase(new EOEditingContext())
	 * .fetchDicoTypeTemps();
	 * 
	 * String type = dicoInverse(dic, motif); if (type != null)
	 * setTypeTemps(dicoInverse(dic, motif)); else { setTypeTemps(motif); // si
	 * trouve pas ??? laisse libelle ou code par defo //System.out.println("new
	 * type temps = " + getTypeTemps()); LRLog.trace("new type temps = " +
	 * getTypeTemps()); } }
	 */

	// ------------ METHODE DE TRADUCTION TYPE ---------------
	/*
	 * public String libelleTypeTemps(NSDictionary dico, String typeTemps) {
	 * 
	 * return dico.objectForKey(typeTemps).toString(); }
	 */

	// ------------ METHODE D'AFFICHAGE ------------------------
	/**
	 * Methode retournant un String représentant l' SPOccupation.
	 * 
	 * @return un String
	 */
	public String toString() {

		String dateD = DateCtrl.dateToString(getDateDebut(), SPConstantes.DATE_FORMAT);
		String dateF = DateCtrl.dateToString(getDateFin(), SPConstantes.DATE_FORMAT);

		String res = "\n idVal=" + idVal + " idKey=" + idKey + " dateDebut="
				+ dateD + " dateFin=" + dateF + " typeTemps=" + typeTemps;
		if (affichage.equals(SPConstantes.AFF_PRIVEE))
			res += " affichage=" + affichage;
		if (detailsTemps != null && !detailsTemps.equals(""))
			res += " detailsTemps=" + detailsTemps;

		return res;
	}

	// Methode Privée pour lecture dico sens Object -> key

	/*
	 * private String dicoInverse(NSDictionary dico, String obj) { String rep =
	 * null; NSArray arr = dico.allKeysForObject(obj); if (arr.count() != 0) rep =
	 * arr.lastObject().toString(); return rep; }
	 */

	// -------------- traitement
	// pour trouver premiere occ d'un tableau d'occ
	// n'appartient pas à une instance precise

	/**
	 * Methode retournant la SPOccupation, dont la date de debut est la plus
	 * petite.
	 * 
	 * @param occups
	 *          NSArray de SPOccupations
	 * @return une SPOccupation
	 */
	public static SPOccupation findFirstObject(NSArray occups) {
		SPOccupation occ1 = (SPOccupation) occups.objectAtIndex(0);
		NSTimestamp debut1 = occ1.getDateDebut();
		SPOccupation first = occ1;

		for (int i = 1; i < occups.count(); i++) {
			SPOccupation occ2 = (SPOccupation) occups.objectAtIndex(i);
			if (debut1.after(occ2.getDateDebut()))
				first = occ2;
		}
		return first;
	}

	// pour trouver derniere occ d'un tableau d'occ
	/**
	 * Methode retournant la SPOccupation, dont la date de fin est la plus grande.
	 * 
	 * @param occups
	 *          NSArray de SPOccupations
	 * @return une SPOccupation
	 */
	public static SPOccupation findLastObject(NSArray occups) {
		SPOccupation occ1 = (SPOccupation) occups.objectAtIndex(0);
		NSTimestamp fin1 = occ1.getDateFin();
		SPOccupation last = occ1;
		for (int i = 1; i < occups.count(); i++) {
			SPOccupation occ2 = (SPOccupation) occups.objectAtIndex(i);
			if (fin1.before(occ2.getDateFin()))
				last = occ2;
		}
		return last;
	}

	// pour trouver parmis les occ d'un tableau ceux interférant avec un
	// intervalle
	// -> plus d'une minute en commun
	/**
	 * Methode retournant les SPOccupations qui interférent avec un intervalle de
	 * temps.
	 * 
	 * @param occups
	 *          NSArray de SPOccupations
	 * @param debut
	 *          NSTimestamp
	 * @param fin
	 *          NSTimestamp
	 * @return NSArray d'SPOccupations
	 */
	public static NSArray findObjectCutingZone(NSArray occups, NSTimestamp debut, NSTimestamp fin) {
		NSMutableArray rep = new NSMutableArray();
		// pour eviter qu'1 occ soit selectionnée si 1 seule minute en commun :
		// ex : occ fini a 15h00 et debut = 15h00
		debut = new NSTimestamp(debut.getTime() + 1000 * 60); // ajout d'une minute
		fin = new NSTimestamp(fin.getTime() - 1000 * 60); // suppression d'une
																											// minute
		for (int i = 0; i < occups.count(); i++) {
			SPOccupation occ = (SPOccupation) occups.objectAtIndex(i);
			if (!occ.getDateFin().before(debut) && !occ.getDateDebut().after(fin))
				// 2 cas faux : finOcc avant debut ou debutOcc apres fin
				// donc cas vrai : Non finOcc avant debut ET Non debutOcc apres fin.
				rep.addObject(occ);
		}
		return new NSArray(rep);
	}

	// --------------------- Getters et Setters ------------------

	/**
	 * Methode retournant la date de debut.
	 * 
	 * @return dateDebut : un NSTimestamp
	 */
	public NSTimestamp getDateDebut() {
		return dateDebut;
	}

	/**
	 * Methode de mise à jour de la date de debut.
	 * 
	 * @param dateDebut
	 *          : un NSTimestamp
	 */
	public void setDateDebut(NSTimestamp value) {
		dateDebut = value;
	}

	/**
	 * Methode de mise à jour de la date de debut.
	 * 
	 * @param debut
	 *          : un String
	 */
	public void setDateDebutFromString(String debut) {
		setDateDebut(DateCtrl.stringToDate(debut, SPConstantes.DATE_FORMAT));
	}

	/**
	 * Methode retournant la date de fin.
	 * 
	 * @return dateFin : un NSTimestamp
	 */
	public NSTimestamp getDateFin() {
		return dateFin;
	}

	/**
	 * Methode de mise à jour de la date de fin.
	 * 
	 * @param dateFin
	 *          : un NSTimestamp
	 */
	public void setDateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}

	/**
	 * Methode de mise à jour de la date de fin.
	 * 
	 * @param fin
	 *          : un String
	 */
	public void setDateFinFromString(String fin) {
		dateFin = DateCtrl.stringToDate(fin, SPConstantes.DATE_FORMAT);
	}

	/**
	 * Methode retournant l'identifiant.
	 * 
	 * @return idval : un Number
	 */
	public Number getIdVal() {
		return idVal;
	}

	/**
	 * Methode de mise à jour de l'identifiant.
	 * 
	 * @param idVal
	 *          : un Number
	 */
	public void setIdVal(Number idVal) {
		this.idVal = idVal;
	}

	/**
	 * Methode retournant le type d'identifiant.
	 * 
	 * @return idKey : un Number,à 1:Individu, 2:Objet ou 3:Salle.
	 */
	public Number getIdKey() {
		return idKey;
	}

	/**
	 * Methode de mise à jour du type d'identifiant.
	 * 
	 * @param idKey
	 *          : un Number
	 */
	public void setIdKey(Number idKey) {
		this.idKey = idKey;
	}

	/**
	 * Methode retournant le type d'occupation du temps.
	 * 
	 * @return typeTemps : un String
	 */
	public String getTypeTemps() {
		return typeTemps;
	}

	/**
	 * Methode de mise à jour du type d'occupation du temps.
	 * 
	 * @param typeTemps
	 *          : un String
	 */
	public void setTypeTemps(String typeTemps) {
		this.typeTemps = typeTemps;
	}

	/**
	 * Methode retournant les details sur la periode consernée.
	 * 
	 * @return detailsTemps : un String
	 */
	public String getDetailsTemps() {
		return detailsTemps;
	}

	/**
	 * Methode de mise à jour de details sur la periode consernée.
	 * 
	 * @param detailsTemps
	 *          : un String
	 */
	public void setDetailsTemps(String detailsTemps) {
		this.detailsTemps = detailsTemps;
	}

	/**
	 * Methode retournant le type d'affichage de cette occupation.
	 * 
	 * @return affichage String "PRIVATE" ou "PUBLIC".
	 */
	public String getAffichage() {
		return affichage;
	}

	/**
	 * Methode de mise à jour du type d'affichage de cette occupation.
	 * 
	 * @param aff
	 *          String de valeur SPConstantes.AFF_PRIVEE ou AFF_PUBLIC.
	 */
	public void setAffichage(String aff) {
		if (aff.equals(SPConstantes.AFF_PRIVEE) || aff.equals(SPConstantes.AFF_PUBLIC))
			this.affichage = aff;
	}

	public final static String UID_KEY = "uid";
	public final static String DATE_DEBUT_KEY = "dateDebut";

	/**
	 * Utilisé pour effectuer des filtres sur des {@link NSArray} de
	 * {@link SPOccupation} A compléter selon les besoins
	 */
	public Object valueForKey(String key) {
		Object object = null;

		if (key.equals(UID_KEY)) {
			object = uid;
		} else if (key.equals(DATE_DEBUT_KEY)) {
			object = getDateDebut();
		}

		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.webobjects.foundation.NSKeyValueCoding#takeValueForKey(java.lang.Object
	 * , java.lang.String)
	 */
	public void takeValueForKey(Object arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public final String getUid() {
		return uid;
	}

	public final void setUid(String uid) {
		this.uid = uid;
	}
}