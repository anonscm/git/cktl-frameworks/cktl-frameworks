package fr.univlr.cri.planning._imports;


import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.webobjects.foundation.NSMutableDictionary;

/**
 * Propose les methodes de manipulation des chaines des caracteres. Elle
 * n'est constituee que des methodes statiques.</p> 
 */
public class StringCtrl {
  /**
   * Une chaine vide afin d'eviter la multiplication des objets-chaines. Ne pas
   * mettre en <em>static</em> et <em>final</em> a la fois, car sinon
   * l'effet de "economie" sera perdu.
   */
  private static String emptyString = "";

  /**
   * Les caracteres indiquant la fin d'une sequence.
   */
  public static final String EndLineChars = " \t\n\r[](){}\"'<>";
  
  /**
   * Les mot qui ne sont pas mis en majusqule lors de formatage des
   * sequences de mots.
   * 
   * @see #capitalizeWords(String)
   */
  public static final String[] IgnoreWordsForCapitalize =
    {"au","aux","ce","ces","de","des","du","en","et","la","le",
     "les","ne","nos","on","or","ou","où","par","pas","pour",
     "puis","qq.","qqch.","qqn","que","qui","quoi","sa","sauf",
     "se","ses","si","sur","te","tu","un","une","vs","ça","çà"};

  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>StringCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public StringCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }
  
  /**
   * Retourne une chaine de caracteres vide. Cette valeur est utilisee
   * afin d'eviter la multiplication des objets <em>String</em> vides.
   */
  public static String emptyString() {
    return emptyString;
  }
  
  /**
   * Retourne une chaine de caracteres contenant le nom et le prenom
   * de la personne. Le format de la chaine retournee correspond a "P.Nom".
   */
  public static String formatName(String prenom, String nom) {
    int n;
    StringBuffer sb = new StringBuffer();
    // Prenom dans le format P.
    n = prenom.indexOf("-");
    sb.append(prenom.substring(0, 1).toUpperCase()).append(".");
    // Si le prenom est de type Prenom-Prenom
    if (n > 0) {
      sb.append("-");
      sb.append(prenom.substring(n+1).trim().substring(0,1).toUpperCase());
      sb.append(".");
    }
    sb.append(" ");
    // Le nom
    sb.append(nom.substring(0,1).toUpperCase()).append(nom.substring(1).toLowerCase());
    return sb.toString();
  }

  /**
   * Retourne le numerot de telephone dans le format "xx.xx.xx.xx.xx".
   */
  public static String formatPhoneNumber(String phone) {
    int digCount = 0;
    StringBuffer sb = new StringBuffer();

    phone = normalize(phone);
    for(int i=0; i<phone.length(); i++) {
      if ((digCount == 2) && (sb.length() < 14)) {
        sb.append(".");
        digCount = 0;
      }
      if (isBasicDigit(phone.charAt(i))) {
        sb.append(phone.charAt(i));
        digCount++;
      }
    }
    return sb.toString();
  }

  /**
   * Reduit la taille d'une chaine de caracteres <i>s</i> en elliminant
   * les lignes vides, espaces, tabulations. La longeur de la chaine
   * resultant ne depasse pas <i>maxLen</i>. La chaine <i>suffix</i> est
   * ajoutee a la fin de la chaine resultant si sa longeur depasse
   * <i>maxLen</i>.
   */
  public static String compactString(String s, int maxLen, String suffix) {
    if (s == null) {
      return emptyString;
    } else {
      StringBuffer rez = new StringBuffer();
      boolean inIgnore = false;
      int i;

      if (maxLen < 0)
        maxLen = s.length();
      for (i = 0; i < s.length(); i++) {
        if (Character.isWhitespace(s.charAt(i))) {
          inIgnore = true;
        } else {
          if (inIgnore) {
            if (i > maxLen)
              break;
            rez.append(" ");
          }
          rez.append(s.charAt(i));
          inIgnore = false;
        }
      }
      if ((i < s.length()) && (suffix != null))
        rez.append(suffix);
      return rez.toString();
    }
  }
  
  /**
   * Teste si la chaine de caracteres <i>s</i> est vide. La chaine
   * est consideree vide, si elle a correspond a une des valeurs <i>null</i>,
   * "" ou "*nil*". La derniere valeur peut etre generee par certaines methodes
   * de WebObjects.
   */
  public static boolean isEmpty(String s) {
    if (s == null) return true;
    s = s.trim();
    return ((s.length() == 0) || s.equals("*nil*"));
  }
  

  /**
   * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
   * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
   * S'il le faut, les caracteres sont ellimines a la fin de la chaine.
   * 
   * @see #cut(String, int, boolean)
   * @see #extendWithChars(String, String, int, boolean)
   */
  public static String cut(String s, int maxLen) {
    return cut(s, maxLen, false);
  }

  /**
   * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
   * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
   * La valeur inFront indique si les caracteres doivent etre ellimines
   * au debut (<i>true</i>) ou a la fin de la chaine (<i>false</i>) ou au de.
   * 
   * @see #cut(String, int)
   * @see #extendWithChars(String, String, int, boolean)
   */
  public static String cut(String s, int maxLen, boolean inFront) {
    if ((s == null) || (s.length() <= maxLen)) return s;
    if (inFront) return s.substring(s.length()-maxLen);
    else return s.substring(0, maxLen);
  }
  
  /**
   * Retourne la chaine en son format "normalize". Les espaces au debut et a
   * la fin de la chaine sont elliminees. Si la chaine est <i>null</i>, elle
   * est transformee en une chaine vide "".
   * 
   * @see #normalize(String, String)
   */
  public static String normalize(String s) {
    return normalize(s, null);
  }

  /**
   * Retourne la chaine en son format "normalize". Les espaces au debut et a
   * la fin de la chaine sont elliminees. Si la chaine est <i>null</i>, elle
   * est transformee en chaine vide "".
   * 
   * <p>Si la chaine resultant est vide, et la valeur <i>valueIfEmpty</i> n'est
   * pas <i>null</i>, alors <i>valueIfEmpty</i> est retournee.
   * 
   * @see #normalize(String)
   */
  public static String normalize(String s, String valueIfEmpty) {
    if (isEmpty(s)) {
      return (valueIfEmpty != null)?valueIfEmpty:emptyString;
    }
    return s.trim();
  }
  
  /**
   * Convertie la valeur numerique representee dans une chaine de caracteres
   * <i>num</i> en un entier. Si la chaine <i>num</i> ne peux pas etre convertie
   * corectement, la valeur <i>defaultValue</i> est retournee.
   * 
   * @see #toInteger(String, int)
   */
  public static int toInt(String num, int defaultValue) {
    if (num == null) return defaultValue;
    try {
      num = num.trim();
      return Integer.valueOf(num).intValue();
    } catch(NumberFormatException ex) {
      return defaultValue;
    }
  }

  /**
   * Convertie la valeur numerique representee dans une chaine de caracteres
   * <i>num</i> en un objet de la classe <code>Integer</code>. Si la chaine
   * <i>num</i> ne peux pas etre convertie corectement, l'objet
   * <code>Integer</code> avec la valeur <i>defaultValue</i> est retourne.
   * 
   * @see #toInt(String, int)
   */
  public static Integer toInteger(String num, int defaultValue) {
    return new Integer(toInt(num, defaultValue));
  }

  /**
   * Convertie une valeur representee dans une chaine de caracteres
   * <i>value</i> en une valeur booleenne. Les valeurs suivantes sont
   * consideres comme valuers de <i>vrai</i> :
   * <center>"OUI", "YES", "O", "Y", "VRAI", "TRUE", "1".</center>
   * 
   * <p>La case de caracteres est ingnoree. Toutes les autres valeurs sont
   * considerees comme <i>faux</i> y compris la valeur <i>null</i>.</p>
   */
  public static boolean toBool(String value) {
    if (value != null) {
      value = value.toUpperCase();
      if (value.equals("O")) return true; // Lettre "o"
      if (value.equals("1")) return true;
      if (value.equals("OUI")) return true;
      if (value.equals("VRAI")) return true;
      if (value.equals("YES")) return true;
      if (value.equals("TRUE")) return true;
      if (value.equals("Y")) return true;
      if (value.equals("OK")) return true;
    }
    return false;
  }

  /**
   * Teste si la chaine <i>substring</i> est contenue dans la chaine <i>s</i>.
   * La case des caracteres est ingnoree.
   * 
   * @see #startsWithIgnoreCase(String, String) 
   */
  public static boolean containsIgnoreCase(String s, String substring) {
    s = s.toUpperCase();
    substring = substring.toUpperCase();
    return (s.indexOf(substring) >= 0);
  }

  /**
   * Teste si la chaine <i>s</i> commence par la chaine <i>substring</i>.
   * La case des caracteres est ingnoree.
   * 
   * @see #containsIgnoreCase(String, String)
   */
  public static boolean startsWithIgnoreCase(String s, String substring) {
    s = s.toUpperCase();
    substring = substring.toUpperCase();
    return s.startsWith(substring);
  }

  /**
   * Retourne le reste de la chaine <i>s</i> venant apres le <i>prefix</i>.
   * Retounr une chaine vide "", si la chaine <i>s</i> ne commence pas par
   * <i>prefix</i>. La case des caracteres est respectee.
   * 
   * @see #getSuffix(String, String)  
   */
  public static String getSuffix(String s, String prefix) {
    // String s2 = s.toUpperCase();
    // prefix = prefix.toUpperCase();
    if (s.startsWith(prefix))
      return s.substring(prefix.length());
    else
      return emptyString;
  }

  /**
   * Retourne le debut de la chaine <i>s</i> venant avant le <i>suffix</i>.
   * Retounr une chaine vide "", si la chaine <i>s</i> ne termine pas par
   * <i>suffix</i>. La case des caracteres est respectee.
   * 
   * @see #getPrefix(String, String)
   */
  public static String getPrefix(String s, String suffix) {
    // String s2 = s.toUpperCase();
    // suffix = suffix.toUpperCase();
    if (s.endsWith(suffix))
      return s.substring(0, s.length() - suffix.length());
    else
      return emptyString;
  }

  /**
   * Convertie le numero number en une chaine de caracteres. S'il le faut,
   * les "0" sont ajoutes au debut de la chaine pour qu'elle ait la longeur
   * <i>digits</i>.
   * 
   * @see #extendWithChars(String, String, int, boolean)
   */
  public static String get0Int(int number, int digits) {
    String s = String.valueOf(number);
    return extendWithChars(s, "0", digits, true);
  }

  /**
   * Reformat une URL HTTP en remplacant les caracteres speciaux
   * par leurs codes correspondants. Cette implementation remplace les
   * espace " " par "%20".
   */
  public static String toHttp(String url) {
    return replace(url, " ", "%20");
  }

  /**
   * Remplace dans la chaine de caracteres <i>s</i> toutes les occurences
   * de la chaine <i>what</i> par la chaine <i>byWhat</i>.
   */
  public static String replace(String s, String what, String byWhat) {
    StringBuffer sb;
    int i;
    
//    if ((s == null) || (what == null)) return s;
    
		if ((s == null)) {
			return s;
		}
    
    sb = new StringBuffer();
    if (byWhat == null) byWhat = emptyString;
    do {
      i = s.indexOf(what);
      if (i >= 0) {
        sb.append(s.substring(0, i));
        sb.append(byWhat);
        s = s.substring(i+what.length());
      }
    } while(i != -1);
    sb.append(s);
    return sb.toString();
  }
  
  /**
	 * Remplace dans la chaine de caracteres <i>s</i> toutes les occurences 
	 * dont le code unicode est <i>what</i> par la chaine <i>byWhat</i>.
	 */
	public static String replace(String s, int what, String byWhat) {
		StringBuffer sb;
		int i;

		if ((s == null)) {
			return s;
		}

		sb = new StringBuffer();
		if (byWhat == null)
			byWhat = "";
		do {
			i = s.indexOf(what);
			if (i >= 0) {
				sb.append(s.substring(0, i));
				sb.append(byWhat);
				s = s.substring(i + 1);
			}
		} while (i != -1);
		sb.append(s);
		return sb.toString();
	}

  /**
   * Remplace dans la chaine <i>str</i> les identifiants encadres par des
   * caracteres "%" par les valeurs correspondantes du dictionnaire
   * <code>dico</code>.
   * 
   * <p>Exemple :<br>
   * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
   * Hashtable table = new Hashtable();<br>
   * table.put("nom", "XIV");<br>
   * table.put("prenom", "Louis");<br>
   * String res = replaceWithDico("Bonjour %nom% %prenom%!", table);</code></pre></td></tr></table>
   * </p>
   */
  public static String replaceWithDico(String str, Dictionary dico) {
    String res = str;
    String key;
    for (Enumeration enumeration = dico.keys(); enumeration.hasMoreElements();) {
      key = (String)enumeration.nextElement();
      res = replace(res, "%"+key+"%", dico.get(key).toString());
    }
    return res;
  }  

  /**
   * Modifie la chaine de caracteres <i>text</i> en ajoutant la chaine
   * <i>quoteSymbols</i> au debut de chaque ligne de text.
   * 
   * <p>Par exemple :
   * <center><code>StringCtrl.quoteText(unMessageMail, "> ");</code></center>
   * <p>
   */
  public static String quoteText(String text, String quoteSymbols) {
    if ((text == null) || (quoteSymbols == null)) return text;
    BufferedReader r = new BufferedReader(new StringReader(text));
    StringBuffer sb = new StringBuffer(emptyString);
    String line;
    try {
      while((line = r.readLine()) != null)
        sb.append(quoteSymbols).append(line).append("\n");
      return sb.toString();
    } catch(Exception ex) {
      ex.printStackTrace();
      return text;
    }
  }

  /**
  * Decompose la chaine de caracteres <i>strings</i> en un vecteur
  * des chaines. La chaine est decomposee suivant le separateur <i>separator</i>.
  * Ignore toutes les chaines vides.
  * 
  * @see #toVector(String, String, boolean)
  */
  public static Vector toVector(String strings, String separator) {
    return toVector(strings, separator, true);
  }

  /**
  * Decompose la chaine de caracteres <i>strings</i> en un vecteur
  * des chaines. La chaine est decomposee suivant le separateur <i>separator</i>.
  * Les chaines vides sont ignorees si <i>ignoreEmptyString</i> est <i>true</i>.
  * 
  * @see #toVector(String, String)
  */
  public static Vector toVector(String strings, String separator,
                                boolean ignoreEmptyString)
  {
    Vector v = new Vector();
    boolean omitString;

    if (strings != null) {
      int idx;
      String s;
      while ((idx = strings.indexOf(separator)) >=0) {
        s = strings.substring(0, idx);
        strings = strings.substring(idx+separator.length());
        omitString = (ignoreEmptyString && ((s == null) || (s.trim().length() == 0)));
        if (!omitString) v.addElement(s);
      }
      omitString = (ignoreEmptyString && ((strings == null) || (strings.trim().length() == 0)));
      // if ((!omitString) && (!v.contains(strings))) v.addElement(strings);
      if (!omitString) v.addElement(strings);
    }
    return v;
  }

  /**
   * Test si le caractere <i>c</i> est une lettre "de base" (a-z, A-Z).
   * Il ne doit pas etre une lettre accentue, une chiffre ou un autre caractere
   * special. 
   */
  public static boolean isBasicLetter(char c) {
    int numVal = Character.getNumericValue(c);
    return (((Character.getNumericValue('a') <= numVal) &&
             (numVal <= Character.getNumericValue('z'))) ||
            ((Character.getNumericValue('A') <= numVal) &&
             (numVal <= Character.getNumericValue('Z'))));
  }

  /**
   * Teste si le caractere <i>c</i> est un chiffre entre 0 et 9.
   */
  public static boolean isBasicDigit(char c) {
    int numVal = Character.getNumericValue(c);
    return ((Character.getNumericValue('0') <= numVal) &&
            (numVal <= Character.getNumericValue('9')));
  }

  /**
   * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
   * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
   * (<i>isBasicDigit</i>) ou un des caracteres <i>acceptChars</i>.
   * 
   * @see #isBasicLetter(char)
   * @see #isBasicDigit(char)
   * @see #isAcceptChar(char) 
   */
  public static boolean isAcceptChar(char c, String acceptChars) {
    boolean rep = isBasicLetter(c);
    if (!rep) rep = isBasicDigit(c);
    if ((!rep) && (acceptChars != null)) {
      for(int i=0; i<acceptChars.length(); i++)
        if (c == acceptChars.charAt(i)) return true;
    }
    return rep;
  }

  /**
   * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
   * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
   * (<i>isBasicDigit</i>) ou un des caracteres supplementaires acceptes par
   * defaut (<i>defaultAcceptChars</i>).
   * 
   * @see #isBasicLetter(char)
   * @see #isBasicDigit(char)
   * @see #isAcceptChar(char, String)
   * @see #defaultAcceptChars() 
   */
  public static boolean isAcceptChar(char c) {
    return isAcceptChar(c, defaultAcceptChars());
  }

  /**
   * Retourne la liste des caracteres acceptes par defaut comme caracteres
   * legales.
   * 
   * <p>Cette implementation renvoie la chaine "._-".
   * 
   * @see #isAcceptChar(char)
   */
  public static String defaultAcceptChars() {
    return "._-";
  }

  /**
   * Test si la chaine de caracteres est "acceptable". Elle l'est si tous
   * les caracteres de la chaine sont acceptables (<i>isAcceptChar</i>) :
   * les lettres "de base", les chiffres et les caracteres acceptes par defaut.
   * 
   * @see #isAcceptChar(char)
   */
  public static boolean isAcceptBasicString(String aString) {
    for(int i=0; i<aString.length(); i++) {
      if (!isAcceptChar(aString.charAt(i))) return false;
    }
    return true;
  }

  /**
   * Convertie la chaine de caracteres <i>s</i> en sa representation "legale".
   * Tous les caracteres de la chaine doivent etre une lettre latine, un
   * chiffre ou un des caracteres <i>acceptChars</i>. Chaque caractere
   * "illegal" est remplace par <i>charToReplace</i>.
   * 
   * @see #isAcceptChar(char, String)
   * @see #toBasicString(String)
   */
  public static String toBasicString(String s, String acceptChars, char charToReplace) {
    StringBuffer newStr;

    if ((s == null) || (s.length() == 0)) return s;
    newStr = new StringBuffer();
    for(int i=0; i<s.length(); i++) {
      if (isAcceptChar(s.charAt(i), acceptChars))
        newStr.append(s.charAt(i));
      else
        newStr.append(charToReplace);
    }
    return newStr.toString();
  }

  /**
   * Convertie la chaine de caracteres <i>s</i> en sa representation "legale".
   * Tous les caracteres de la chaine doivent etre une lettre latine, un
   * chiffre ou un des caracteres acceptes par defaut. Chaque caractere
   * "illegal" est remplace par <i>charToReplace</i>.
   * 
   * <p>Cette methode peut etre utilisee pour changer le nom des fichiers en
   * leur representation "portable" (pas d'espaces, pas des lettres accentuees,
   * etc.)</p>
   * 
   * @see #isAcceptChar(char, String)
   * @see #defaultAcceptChars()
   * @see #toBasicString(String, String, char)
   */
  public static String toBasicString(String aString) {
    return toBasicString(aString, defaultAcceptChars(), '_');
  }

  /**
   * Verifie si le caractere <i>ch</i> est un des caracteres indiquant
   * la fin d'une suite des caracteres. Attention, ceci n'est pas forcement
   * le caractere de la fin d'une ligne de text.
   * 
   * <p>L'implementation actuelle reconnais les caracteres suivants comme
   * la fin d'une suite : " \t\n\r\"[](){}"<p>
   */
  private static boolean isEndLineChar(char ch) {
    for(int i=0; i<EndLineChars.length(); i++) {
      if (ch == EndLineChars.charAt(i)) return true;
    }
    return false;
  }
  
  /**
   * Active toutes les URL dans le <i>text</i> en les transformant en
   * balises HTML <code>&lt;A&gt;</code> correspondantes. Uniquement les
   * balises correspondant au protocole <i>protocolRef</i> sont transformees.
   * 
   * @param text Le text dans lequel les liens sont actives.
   * @param protocolRef Le protocol dont les liens sont actives. Ceci doit est
   *   une expression de type "http://", "ftp://", etc...
   * @param cssClass Indique le nom de la classe CSS qui decrit le lien HTTM
   *   genere. Cette valeur peut etre <i>null</i> (aucune classe a utiliser).
   * @param linkTarget Indique le nom de target (le nom d'une fenetre ou d'une
   *   frame de navigateur Web) ou le contenue de lien sera affiche. Cette
   *   valeur peut etre <i>null</i> aucun target.
   * 
   * @see #activateURL(String, String, String)
   */
  public static String activateURL(String text,
                                   String protocolRef,
                                   String cssClass,
                                   String linkTarget)
  {
    String lowerString = text.toLowerCase();
    String part;
    StringBuffer result = new StringBuffer();
    int i=0;
    int aIdx, protIdx;
    int endIdx;

    if (text == null) return text;
    if (cssClass == null) cssClass = emptyString;
      else  cssClass = " class=\""+cssClass+"\"";
    if (linkTarget == null) linkTarget = emptyString;
      else linkTarget = " target=\""+linkTarget+"\"";
    i = 0;
    while(true) {
      aIdx = lowerString.indexOf("<a ", i);
      protIdx = lowerString.indexOf(protocolRef, i);
      // System.out.println("Index : "+i+", aIdx : "+aIdx+", protIdx : "+protIdx+", length : "+aString.length());
      if (protIdx == -1) {
        result.append(text.substring(i));
        break;
      }
      if ((aIdx >= 0) && (aIdx < protIdx)) {
        endIdx = lowerString.indexOf("</a>", i);
        result.append(text.substring(i, endIdx+4));
        i = endIdx + 4;
        continue;
      }
      result.append(text.substring(i, protIdx));
      for(endIdx=protIdx; endIdx<text.length(); endIdx++) {
        if (isEndLineChar(text.charAt(endIdx))) break;
      }
      part = text.substring(protIdx, endIdx);
      result.append("<a").append(cssClass).append(linkTarget);
      result.append(" href=\"").append(part).append("\">");
      result.append(part).append("</a>");
      i = endIdx;
    }
    return result.toString();
  }

  /**
   * Active toutes les URL dans le <i>text</i> en les transformant en
   * balises HTML <code>&lt;A&gt;</code> correspondantes. Cette methode
   * transforme les balises correspondant aux protocoles "http://" et "ftp://".
   * 
   * @param aString Le text dans lequel les liens sont actives.
   * @param cssClass Indique le nom de la classe CSS qui decrit le lien HTTM
   *   genere. Cette valeur peut etre <i>null</i> (aucune classe a utiliser).
   * @param linkTarget Indique le nom de target (le nom d'une fenetre ou d'une
   *   frame de navigateur Web) ou le contenue de lien sera affiche. Cette
   *   valeur peut etre <i>null</i> aucun target.
   * 
   * @see #activateURL(String, String, String, String)
   */
  public static String activateURL(String aString, String cssClass, String linkTarget) {
    aString = activateURL(aString, "http://", cssClass, linkTarget);
    return activateURL(aString, "ftp://", cssClass, linkTarget);
  }
  
  /**
   * Effectue un simple encodage d'un text HTML <i>htmlChain</i>. Ce codage
   * remplace chaque caractere du text par une expression "&#xxxx;", où
   * <i>xxxx</i> et le code de caractere. Ce codage rend le text illisible
   * mais permet de l'afficher dans un navigateur Web.
   */
  public static String codeHTML(String htmlChain) {
    StringBuffer chain = new StringBuffer();
    for(int i=0; i<htmlChain.length(); i++) {
      chain.append("&#").append((int)htmlChain.charAt(i)).append(";");
    }
    return chain.toString();
  }
  
  /**
   * Extrait une partie du text <i>htmlText</i> (typiquement, en format HTML)
   * qui se trouve entre les balises &lt;tag&gt; et &lt;/tag&gt;. La case
   * des balises est ignoree. 
   * 
   * <p>Par exemple, pour recuperer le text entre les balises &lt;body&gt;
   * d'un document html:
   * <center><code>StringCtrl.textBetweenTag(monHtml, "body", false)</code></center>
   * </p>
   * 
   * @param htmlText Le text en format HTML.
   * @param tag Le nom d'une balise. Le text sera cherche entre la balise
   *   ouvrante &lt;tag&gt; et fermante &lt;tag&gt;.
   * @param sameIfNotFount Si cette valeur est <i>false</i>, alors si la balise
   *   n'existe pas ou aucun text n'a ete trouve, la resultant sera <i>null</i>.
   *   Si la valeur est <i>true</i>, alors la methode va renvoyer le même text
   *   <i>htmlText</i>.
   */
  public static String textBetweenTag(String htmlText,
                                      String tag,
                                      boolean sameIfNotFount)
  {
    int beginIdx, endIdx, idx;
    boolean beginFound, endFound;
    String foundTag;
    String toFind1, toFind2;
    
    toFind1 = tag.toUpperCase()+">";
    toFind2 = tag.toUpperCase()+" ";
    endFound = false;
    endIdx = 0;
    beginFound = false;
    beginIdx = 0;
    while ((!beginFound) && (beginIdx != -1)) {
      beginIdx = htmlText.indexOf("<", beginIdx);
      if (beginIdx != -1) {
        beginIdx++;
        idx = htmlText.indexOf(">", beginIdx);
        if (idx == -1)
          foundTag = htmlText.substring(beginIdx).toUpperCase();
        else
          foundTag = htmlText.substring(beginIdx, idx+1).toUpperCase();
        if (foundTag.startsWith(toFind1) || foundTag.startsWith(toFind2)) {
          if (foundTag.endsWith("/>")) return emptyString;
          beginFound = true;
          beginIdx = idx+1;
        }
      }
    }
    if (beginFound) {
      endIdx = beginIdx;
      while ((!endFound) && (endIdx != -1)) {
        endIdx = htmlText.indexOf("</", endIdx);
        if (endIdx != -1) {
          endIdx += 2;
          idx = htmlText.indexOf(">", endIdx);
          if (idx == -1)
            foundTag = htmlText.substring(endIdx).toUpperCase();
          else
            foundTag = htmlText.substring(endIdx, idx+1).toUpperCase();
          if (foundTag.equals(toFind1)) {
            endFound = true;
            endIdx -= 2;
          }
        }
      }
    }
    if (beginFound) {
      if (endFound) return htmlText.substring(beginIdx, endIdx);
      else return htmlText.substring(beginIdx);
    }
    if (sameIfNotFount) return htmlText;
    else return null; 
  }

  /**
   * Elimine les caracteres de type "espace" du debut et de la fin du
   * text passe en parametre. Les caracteres elimines sont espace, tabulation,
   * passage a la ligne, etc... 
   */
  public static String trimText(String aText) {
    int i;
    for(i=0; i < aText.length(); i++)
      if (!Character.isWhitespace(aText.charAt(i))) break;
    if (i > 0) aText = aText.substring(i);
    for(i=aText.length()-1; i>=0; i--)
      if (!Character.isWhitespace(aText.charAt(i))) break;
    if (i==0)
      aText = emptyString;
    else if (i < (aText.length()-1))
      aText = aText.substring(0, i+1);
    return aText;
  }
  
  /**
   * Formate une chaine de caracteres sur la longueur spécifiee, en ajoutant
   * au besoin des caracteres au debut ou a la fin de la chaine.
   * Si la chaine original est trop longue, elle est coupee.
   *
   *
   * @param s La chaine originale
   * @param addChars Le caractere a ajouter au debut ou a la fin de la chaine
   *   originale.
   * @param length Longueur de la chaine a renvoyer.
   * @param inFront Indiquer <i>true</i> si on veut ajouter les charcteres
   *   au debut, <i>false</i> si c'est a la fin de la chaine.
   * @return La chaine modifiée
   */
  
  public static String extendWithChars(String s, String addChars, int length,
                                       boolean inFront)
  {
    //Ajouter les caractéres
    for(;s.length() < length; s = ((inFront)?(addChars+s):(s+addChars)));
    return s;
  }
  
  /**
   * Renvoie une chaine de caractères avec la première lettre en majuscule et
   * les autres en minuscules.
   * 
   * @see #capitalizeWords(String)
   */
  public static String capitalize(String s) {
      if (isEmpty(s)) return emptyString;
      String debut = (s.substring(0,1)).toUpperCase();
      if (s.length() == 1) return debut;
      String fin = (s.substring(1,s.length())).toLowerCase();
      return debut.concat(fin);
  }

  /**
   * Renvoie une chaine avec tous les mots ayant leur premiere lettre en
   * majuscule. Par exemple, la chaine :
   * <center><coe>ACHATS D'ETUDES ET PRESTATIONS DE SERVICES</code></center>
   * est transformée en :
   * <center><code>Achats d'Etudes et Prestations de Services</code></center>
   * 
   * Les mots sont récupérés en se basant sur les séparateurs de la chaine
   * <code>EndLineChars</code>. Les mots d'un caractère sont mis en minuscule.
   * Les mots qui se trouvent dans le tableau
   * <code>IgnoreWordsForCapitalize</code> sont également mis en minuscules.
   * 
   * @see #capitalize(String)
   */
  public static String capitalizeWords(String s) {
    if (isEmpty(s)) return emptyString;
    boolean shouldIgnoreWord;
    StringTokenizer stok = new StringTokenizer(s, EndLineChars, true);
    String words[] = new String[stok.countTokens()];
    //recuperer les mots
    for (int i = 0; i < words.length; i++) {
      words[i] = stok.nextToken();
    }
    //On met une majuscule en debut de chaque mot
    for (int i = 0; i < words.length; i++) {
      //on ignore les caracteres de délimitation
      if (EndLineChars.indexOf(words[i]) < 0) {
        shouldIgnoreWord = false;
        //On passe le mot en minuscule
        words[i] = words[i].toLowerCase();
        //si le mot est dans la liste des mots a ignorer, ben on l'ignore -;)
        //Pareil si on a une seule lettre
        if (words[i].length() == 1) continue;
        for (int j = 0; j < IgnoreWordsForCapitalize.length; j++) {
          if (IgnoreWordsForCapitalize[j].equals(words[i])) {
            shouldIgnoreWord = true;
            break;
          }
        }
        if (!shouldIgnoreWord)
          words[i] = words[i].substring(0, 1).toUpperCase()
                      + words[i].substring(1, words[i].length());
      }
    }
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < words.length; buf.append(words[i++]));
    return buf.toString();
  }
  
  /**
   * Teste si la chaine de caracteres <code>chain</code> ressemble au
   * schema <code>pattern</code>. Le schema peut comporter deux caracteres
   * speciaux "*" et "?". Le "*" represente 0 ou plusieurs carcteres dans
   * la chaine, et "?" - un seule caractere.
   * 
   * <p>Exemples&nbsp;:</p>
   * <div class="example"><code>StringCtrl.like("*tmp.??a");</code></div>
   */
  public static boolean like(String chain, String pattern) {
    boolean compareExact = true;
    StringTokenizer st;
    String part;
    
    if ((chain == null) || (pattern == null)) return false;
    st = new StringTokenizer(pattern, "*?", true);
    while (st.hasMoreTokens()) {
      part = st.nextToken();
      if (part.equals("*")) {
        compareExact = false;
      } else if (part.equals("?")) {
        compareExact = true;
        if (chain.length() == 0) return false;
        chain = chain.substring(1); 
      } else {
        if (compareExact) { // Exact place
          if (!chain.startsWith(part)) return false;
          chain = chain.substring(part.length());
        } else { // Anny place
          if (chain.indexOf(part) == -1) return false;
          chain = chain.substring(chain.indexOf(part)+part.length());
        }
        compareExact = true;
      }
    }
    return ((chain.length() == 0) || (!compareExact));
  }
  
  /**
   * Trie la liste des chaines de caracteres <code>strings</code>.
   * Le parametre <code>ascending</code> indique l'ordre de tri : croissant
   * (<i>true</i>) ou decroissant (<i>false</i>). Le parametre
   * <code>ignoreCase</code> indique si la case de symboles doit etre
   * respectee ou ignoree.
   * 
   * <p>Attention, cette methode modifie la liste originale
   * <code>strings</code>.</p>
   * 
   * @see #sortedStrings(Vector, boolean, boolean)
   */
  public static void sortStrings(Vector strings,
                                 boolean ascending, boolean ignoreCase)
  {
    int sign = (ascending)?1:-1;
  	if (strings == null || strings.size() == 0) return;
    String value;
  	for (int i = 1; i < strings.size(); i++) {
  	  for (int j = i; j > 0; j--) {
  	    if ((compare((String)strings.elementAt(j),
                     (String)strings.elementAt(j-1),
                     ignoreCase) * sign) < 0)
        {
            value = (String)strings.elementAt(j);
            strings.setElementAt(strings.elementAt(j-1), j);
            strings.setElementAt(value, j-1);
  	    } else
  	      break;
  	  }
  	}
  }
  
  /**
   * Trie la liste des chaines de caracteres <code>strings</code> et
   * retourne la liste trie. Le parametre <code>ascending</code> indique
   * l'ordre de tri : croissant (<i>true</i>) ou decroissant (<i>false</i>).
   * Le parametre <code>ignoreCase</code> indique si la case de symboles doit
   * etre respectee ou ignoree.
   * 
   * <p>Cette methode ne modifie pas la liste originale et retourne une
   * copie triee.</p>
   * 
   * @see #sortStrings(Vector, boolean, boolean)
   */
  public static Vector sortedStrings(Vector strings,
                                     boolean ascending, boolean ignoreCase)
  {
    if (strings == null) return null;
    Vector sorted = new Vector(strings.size());
    for(int i=0; i<strings.size(); i++)
      sorted.addElement(strings.elementAt(i));
    sortStrings(sorted, ascending, ignoreCase);
    return sorted;
  }
  
  /**
   * Compare deux chaines de caracteres. Retourne -1 si la chaine
   * <code>val1</code> est inferieure a <code>val2</code>, 1 dans le cas
   * contraire et 0 si les chaines sont egales. Le parametre
   * <code>ignoreCase</code> indique si la case des symboles doit etre ignoree.
   */
  private static int compare(String val1, String val2, boolean ignoreCase) {
  	if (ignoreCase) {
  		val1 = val1.toUpperCase();
      val2 = val2.toUpperCase();
  	}
    return val1.compareTo(val2);
  }

	// Le dictionnaire de correspondance accents -> sans accents
 private static final Hashtable myDicoAccents = (new NSMutableDictionary(new String[] {
			"A", "A", "A", "A", "A", "A", "C", "E", "E", "E", "E", "I", "I", "I", "I", "N", "O", "O", "O", "O", "O", "U", "U", "U", "U", "Y", "a", "a", "a", "a", "a", "a", "c", "e", "e", "e", "e", "i", "i", "i", "i", "n", "o",
			"o", "o", "o", "o", "u", "u", "u", "u", "y"
	}, new Integer[] {
  		new Integer(0xC0), new Integer(0xC1), new Integer(0xC2), new Integer(0xC3), new Integer(0xC4), new Integer(0xC5), new Integer(0xC7), new Integer(0xC8), new Integer(0xC9), new Integer(0xCA), new Integer(0xCB), new Integer(0xCC), new Integer(0xCD), new Integer(0xCE), new Integer(0xCF), new Integer(0xD1), new Integer(0xD2), new Integer(0xD3), new Integer(0xD4), new Integer(0xD5), new Integer(0xD6), new Integer(0xD9), new Integer(0xDA), new Integer(0xDB), new Integer(0xDC), new Integer(0xDD),
  		new Integer(0xE0), new Integer(0xE1), new Integer(0xE2), new Integer(0xE3), new Integer(0xE4), new Integer(0xE5), new Integer(0xE7), new Integer(0xE8), new Integer(0xE9), new Integer(0xEA), new Integer(0xEB), new Integer(0xEC), new Integer(0xED), new Integer(0xEE), new Integer(0xEF), new Integer(0xF1), new Integer(0xF2), new Integer(0xF3), new Integer(0xF4), new Integer(0xF5), new Integer(0xF6), new Integer(0xF9), new Integer(0xFA), new Integer(0xFB), new Integer(0xFC), new Integer(0xFD)
	})).hashtable();

  
  /**
   * Transformer une chaine accentuee en chaine sans accents.
   */
  public static String chaineSansAccents(String chaine) {
  	String res = chaine;
  	Enumeration enumAccents = myDicoAccents.keys();
  	while (enumAccents.hasMoreElements()) {
  	    /*String key = (String) enumAccents.nextElement();
  	    res = StringCtrl.replace(res, key, myDicoAccents.get(key).toString());*/
			Integer key = (Integer) enumAccents.nextElement();
			res = replace(res, key.intValue(), myDicoAccents.get(key).toString());
  	}
  	return res;
  }
  
  /**
	 * Supprime les caracteres accentues d'une chaine en les remplacant par la chaine specifiee en parametre.
	 * 
	 * @param chaine
	 * @param remplacement
	 * @return la chaine sans accent
	 */
	public static String chaineSansAccents(String chaine, String remplacement) {
		String res = chaine;
		for (Enumeration enumAccents = myDicoAccents.keys(); enumAccents.hasMoreElements();) {
			//String key = (String) enumAccents.nextElement();
			//res = replace(res, key, remplacement);
			Integer key = (Integer) enumAccents.nextElement();
			res = replace(res, key.intValue(), remplacement);
		}
		return res;
	}
  

  /**
   * Retourne une chaine de caractere sans les caracteres non numeriques, ou une chaine vide si s=null.
   * @param s
   * @return la chaine avec seulement les chiffres
   */
  public static String keepDigits(String s) {
  	StringBuffer sb = new StringBuffer();
  	s = StringCtrl.normalize(s);
  	for (int i = 0; i < s.length(); i++) {
  		if (StringCtrl.isBasicDigit(s.charAt(i))) {
  			sb.append(s.charAt(i));
  		}
  	}
  	return sb.toString();
  } 
  
  /**
   * @param s
   * @return true si s est une adresse email construite correctement.
   */
  public static boolean isEmailValid(String s) {
  	java.util.regex.Pattern p = java.util.regex.Pattern.compile(".+@.+\\.[a-z]+");
  	java.util.regex.Matcher m = p.matcher(s);
  	return (m.matches());
  }

  /**
   * @param s
   * @return La chaine s avec sa premiere en majuscule. Les autres lettres ne
   *         sont pas modifiees contrairement a la methode
   *         {@link StringCtrl#capitalize(String)}.
   */
  public static String initCap(String s) {
  	if (isEmpty(s)) {
  		return emptyString();
  	}
  	String debut = (s.substring(0, 1)).toUpperCase();
  	if (s.length() == 1)
  		return debut;
  	String fin = (s.substring(1, s.length()));
  	return debut.concat(fin);
  }

}
