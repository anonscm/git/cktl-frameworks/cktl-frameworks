package fr.univlr.cri.planning._imports;


import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Classe de gestion des type de données java pur <code>GregorianCalendar</code>
 */
public class JavaDateCtrl {

  /**
   * @return Un <code>GregorianCalendar</code> initialise a la date du jour (sans les heures/minutes/etc).
   * Utilisez getToday().getTime() pour recuperer la date du jour nettoyee des secondes sous forme de Date.
   */
  public static GregorianCalendar nowDay() {
  	final GregorianCalendar gc = new GregorianCalendar();
  	gc.set(Calendar.HOUR_OF_DAY, 0);
  	gc.set(Calendar.MINUTE, 0);
  	gc.set(Calendar.SECOND, 0);
  	gc.set(Calendar.MILLISECOND, 0);
  	return gc;
  }
	
}
