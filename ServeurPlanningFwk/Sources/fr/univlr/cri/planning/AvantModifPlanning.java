package fr.univlr.cri.planning;

import java.util.Properties;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.cri.planning._imports.CktlLog;
import fr.univlr.cri.planning._imports.DateCtrl;
import fr.univlr.cri.planning._imports.StringCtrl;

/**
 * Cette classe contient les methodes nécessaires pour rechercher des conflits
 * ou connaitre la disponibilité d'une entité gràce au Serveur de Planning .<br>
 * <br>
 * Pour connaitre la disponibilité, la recherche de conflit est utilisée. <br>
 * Les conflits sont recherchés dans toutes les DirectActions ( BD
 * Repartition.MET_KEY_SERV ) qui retourne des informations planning et sont
 * utilisées pour le type d'identifiant choisi (1:Individu, 2:Objet, 3:Salle).
 * Pour un individu, les horaires de travail ne peuvent pas former un conflit. <br>
 * <br>
 * <strong>On y trouve :</strong><br>
 * <br>
 * _ 3 methodes principales au choix : <br>
 * . _ 2 methodes de recherche des conflits par le serveur (différence de
 * parametres); <br>
 * . _ 1 methode vérifiant la disponibilité d'un id sur une periode; <br>
 * _ 2 methodes de recupération de la reponse; <br>
 * _ 2 methodes pour la gestion des erreurs/exceptions; <br>
 * <br>
 * 
 * <strong>Exemple de code :</strong><br>
 * <br>
 * 1/ Cas de vérification de disponibilité : <br>
 * <p class="example">
 * // creation parametres <br>
 * NSTimestamp dateD = DateCtrl.stringToDate("19/05/2006 14:00",
 * SPConstantes.DATE_FORMAT); <br>
 * NSTimestamp dateF = DateCtrl.stringToDate("19/05/2006 17:00",
 * SPConstantes.DATE_FORMAT); <br>
 * // appel au serveur de planning <br>
 * Properties prop =
 * AvantModifPlanning.isDisponible(SPConstantes.IDKEY_INDIVIDU, new
 * Integer(4371), dateD, dateF); <br>
 * // recuperation des resultats <br>
 * boolean isDispo = AvantModifPlanning.getBoolean(prop); // true si disponible
 * <br>
 * if (! isDispo) <br>
 * . String message = AvantModifPlanning.getMessage(prop); // détails des
 * conflits et/ou hors horaires
 * </p>
 * <br>
 * 2/ Cas de recherche de conflits (avant modification d'un planning) : <br>
 * <p class="example">
 * // creation parametres <br>
 * NSMutableArray args = new NSMutableArray(); <br>
 * NSTimestamp dateD = DateCtrl.stringToDate("18/05/2006 00:00",
 * SPConstantes.DATE_FORMAT); <br>
 * NSTimestamp dateF = DateCtrl.stringToDate("18/05/2006 23:59",
 * SPConstantes.DATE_FORMAT); <br>
 * String motif = "C_ANN"; // ex : congé annuel <br>
 * SPOccupation occ = new SPOccupation(dateD, dateF, motif, null); <br>
 * args.addObject(occ); // on peut insérer plusieurs SPOccupations <br>
 * // appel au serveur de planning <br>
 * Properties prop =
 * AvantModifPlanning.isInConflict(SPConstantes.IDKEY_INDIVIDU, new
 * Integer(4371), new NSArray(args)); <br>
 * // recuperation des resultats <br>
 * boolean enConflit = AvantModifPlanning.getBoolean(prop); // true si conflit
 * trouvé <br>
 * if (enConflit) <br>
 * . String message = AvantModifPlanning.getMessage(prop); // details des
 * conflits
 * </p>
 * 
 * 
 */

public class AvantModifPlanning {

	/**
	 * Methode demandant au Serveur de Planning de chercher si de nouvelles
	 * occupations entrent en conflit avec celles déjà présentes. A utiliser avant
	 * d'ajouter des occupations à un planning.
	 * 
	 * @param urlCaller
	 *          L'url de l'application appelante (utiliser
	 *          criApp.getApplicationInstanceURL(context()))
	 * @param idkey
	 *          Number d'un type identifiant (1:Individu, 2:objet, 3:salle);<br>
	 * @param idval
	 *          Number d'un identifiant (numéro d'Individu, d'objet ou de salle);<br>
	 * @param spOccups
	 *          NSArray de SPOccupations contenant les renseignements de
	 *          dateDebut, dateFin et type.
	 * 
	 * @return un Properties renvoyé par le serveur de planning.
	 */

	public static Properties isInConflict(String urlCaller, Number idkey, Number idval, NSArray spOccups) {

		// param
		StringBuffer buffer = new StringBuffer();

		// ajout CT : l'url de l'appli appelante
		buffer.append(SPConstantes.PROP_CALLER + " = " + urlCaller + "\n");

		buffer.append(SPConstantes.PROP_OC_IDKEY + " = " + idkey.toString() + "\n");
		buffer.append(SPConstantes.PROP_OC_IDVAL + " = " + idval.toString() + "\n");

		int nb = -1; // numero des parametres enregistrés
		for (int i = 0; i < spOccups.count(); i++) {
			nb++;
			if (spOccups.objectAtIndex(i) instanceof SPOccupation) {
				SPOccupation occ = (SPOccupation) spOccups.objectAtIndex(i);

				String motif = occ.getTypeTemps();
				String debut = DateCtrl.dateToString(occ.getDateDebut(), SPConstantes.DATE_FORMAT);
				String fin = DateCtrl.dateToString(occ.getDateFin(), SPConstantes.DATE_FORMAT);

				buffer.append(SPConstantes.PROP_OC_DEB + nb + " = " + debut + "\n");
				buffer.append(SPConstantes.PROP_OC_FIN + nb + " = " + fin + "\n");
				buffer.append(SPConstantes.PROP_OC_TYPE + nb + " = " + motif + "\n");
			} else {
				CktlLog.trace("Le NSArray en parametre doit etre conposé de SPOccupations.");
				nb--;
			}
		}

		// connection au serveurPlanning
		Properties resp = new Properties();
		if (DemandePlanning.serveurPlanningAccessible())
			resp = DemandePlanning.connection(SPConstantes.METHOD_NAME_CLIENT_MODIF, buffer, SPConstantes.DIRECT_ACTION_CHANGE_PLANNING);
		// 1 booleen (true si conflit) + 1 string (mess alert
		// court) + statut + erreur

		// sinon retourne Properties vide.

		return resp;
	}

	// -----------------------
	// idem avec param diff
	/**
	 * Methode demandant au Serveur de Planning de chercher si de nouvelles
	 * occupations entrent en conflit avec celles déjà présentes. A utiliser avant
	 * d'ajouter des occupations à un planning.
	 * 
	 * @param urlCaller
	 *          L'url de l'application appelante (utiliser
	 *          criApp.getApplicationInstanceURL(context()))
	 * @param idkey
	 *          Number d'un type identifiant (1:Individu, 2:objet, 3:salle);<br>
	 * @param idval
	 *          Number d'un identifiant (numéro d'Individu, d'objet ou de salle);<br>
	 * @param debuts
	 *          NSArray contenant les dateDebut en NSTimestamp ou String.
	 * @param fins
	 *          NSArray contenant les dateFin en NSTimestamp ou String.
	 * @param motifs
	 *          NSArray contenant les types d'occupation du temps en String.
	 * 
	 * @return un Properties renvoyé par le serveur de planning.
	 */
	public static Properties isInConflict(String urlCaller, Number idkey, Number idval,
			NSArray debuts, NSArray fins, NSArray motifs) {

		// param
		StringBuffer buffer = new StringBuffer();

		buffer.append(SPConstantes.PROP_OC_IDKEY + " = " + idkey.toString() + "\n");
		buffer.append(SPConstantes.PROP_OC_IDVAL + " = " + idval.toString() + "\n");

		String debut = null;
		String fin = null;
		String motif = null;

		int nb = -1; // numero des parametres enregistrés
		int tabInf = debuts.count();
		if ((fins.count() < tabInf) && (fins.count() < motifs.count())) {
			tabInf = fins.count();
		}
		if ((motifs.count() < tabInf) && (motifs.count() < fins.count())) {
			tabInf = motifs.count();
		}

		// on ignore les groups lorsque l'un des tableaux n'a plus de valeurs
		for (int i = 0; i < tabInf; i++) {
			if (fins.objectAtIndex(i) != null || motifs.objectAtIndex(i) != null) {
				nb++;
				motif = motifs.objectAtIndex(i).toString();
				if (debuts.objectAtIndex(i) instanceof NSTimestamp)
					debut = DateCtrl.dateToString((NSTimestamp) debuts.objectAtIndex(i), SPConstantes.DATE_FORMAT);
				else
					debut = debuts.objectAtIndex(i).toString();
				if (fins.objectAtIndex(i) instanceof NSTimestamp)
					fin = DateCtrl.dateToString((NSTimestamp) fins.objectAtIndex(i), SPConstantes.DATE_FORMAT);
				else
					fin = fins.objectAtIndex(i).toString();

				buffer.append(SPConstantes.PROP_OC_DEB + nb + " = " + debut + "\n");
				buffer.append(SPConstantes.PROP_OC_FIN + nb + " = " + fin + "\n");
				buffer.append(SPConstantes.PROP_OC_TYPE + nb + " = " + motif + "\n");

			} else {
				CktlLog.trace("Les 3 NSArray en parametre devraient contenir le meme nombre d'elements.");
				nb--;
			}
		}

		// connection au serveurPlanning
		Properties resp = new Properties();
		if (DemandePlanning.serveurPlanningAccessible())
			resp = DemandePlanning.connection(SPConstantes.METHOD_NAME_CLIENT_MODIF, buffer, SPConstantes.DIRECT_ACTION_CHANGE_PLANNING);
		// 1 booleen (true si conflit) + 1 string (mess alert
		// court) + statut + erreur

		// sinon retourne Properties vide.

		return resp;
	}

	/**
	 * Methode vérifiant la disponibilité d'une entité (personne, objet ou salle).
	 * Elle utilise la methode isInConflict qui appelle le serveur de planning.
	 * Une personne est considérée disponible sur une periode si elle n'a
	 * enregistré ni congés, ni occupations (type Mission, DT, Reunion...) et
	 * qu'elle est sur son temps de travail. Un objet ou une salle sont considérés
	 * disponbible si ils ne sont pas Reservés.
	 * 
	 * @param urlCaller
	 *          L'URL de l'application appelante (utiliser
	 *          criApp.getApplicationInstanceURL(context()))
	 * @param idkey
	 *          Number d'un type identifiant (1:Individu, 2:objet, 3:salle);<br>
	 * @param idval
	 *          Number d'un identifiant (numéro d'Individu, d'objet ou de salle);<br>
	 * @param debut
	 *          NSTimestamp date de deput periode.
	 * @param fin
	 *          NSTimestamp date de fin periode.
	 * @return un Properties renvoyé par le serveur de planning.
	 */

	public static Properties isDisponible(String urlCaller, Number idkey, Number idval,
			NSTimestamp debut, NSTimestamp fin) {
		Properties repDispo = new Properties();
		boolean dispo = false;

		if (debut != null && fin != null) {
			SPOccupation occ = new SPOccupation(debut, fin, SPConstantes.SPOCC_TYPE_MISSION, null);
			NSMutableArray arr = new NSMutableArray();
			arr.addObject(occ);
			Properties prop = isInConflict(urlCaller, idkey, idval, arr);

			boolean conflit = getBoolean(prop);
			String conflitsMess = getMessage(prop);

			// SI il s'agit d'un individu,
			// verif qu'il est dans son temps de travail
			if (idkey.toString().equals(SPConstantes.IDKEY_INDIVIDU.toString())) {
				// on cherche horaires sur toutes la journée
				String dDay = DateCtrl.dateToString(debut, SPConstantes.DATE_FORMAT);
				dDay = dDay.substring(0, 11) + "00:00";
				String fDay = dDay.substring(0, 11) + "23:59"; // dDay : pour faire sur
																												// une seule journée
				NSTimestamp debutDay = DateCtrl.stringToDate(dDay, SPConstantes.DATE_FORMAT);
				NSTimestamp finDay = DateCtrl.stringToDate(fDay, SPConstantes.DATE_FORMAT);

				StringBuffer buffer = new StringBuffer();
				buffer.append("PERIODE HORS HORAIRES de travail; ");
				Properties propHoraires = DemandePlanning.connectionAuServeur(
						SPConstantes.DIRECT_ACTION_IS_DISPONIBLE, idkey, idval, debutDay, finDay);
				// si isDisponible n'est pas reliee a des methode serveur, alors
				// on considere que c'est toujours OK pour les horaires
				String strIsDisponibleConnected = (String) propHoraires.getProperty(SPConstantes.PROP_IS_DISPONIBLE_CONNECTED);
				if (!StringCtrl.isEmpty(strIsDisponibleConnected) && strIsDisponibleConnected.equals("false")) {
					dispo = true;
				} else {
					// methode isDisponible connectee, on fait donc l'analyse
					NSArray occHoraires = DemandePlanning.getPlanning(propHoraires);
					for (int i = 0; i < occHoraires.count(); i++) {
						NSArray occs = (NSArray) occHoraires.objectAtIndex(i);
						for (int j = 0; j < occs.count(); j++) {
							SPOccupation occh = (SPOccupation) occs.objectAtIndex(j);
							if (DateCtrl.isBeforeEq(occh.getDateDebut(), debut) && DateCtrl.isAfterEq(occh.getDateFin(), fin)) {
								dispo = true;
							} else {
								String d = DateCtrl.dateToString(occh.getDateDebut(), SPConstantes.DATE_FORMAT);
								String f = DateCtrl.dateToString(occh.getDateFin(), SPConstantes.DATE_FORMAT);
								d = d.substring(11);
								f = f.substring(11);
								buffer.append("Horaire defini de " + d + " a " + f + " ; ");
							}
						}
					}
				}

				// resultat booleen et message
				if (conflit)
					repDispo.setProperty(SPConstantes.PROP_BOOL, "false");
				else if (!conflit && dispo)
					// boolean true si disponible ( horaire et non conflit)
					repDispo.setProperty(SPConstantes.PROP_BOOL, "true"); // key, value

				// string message : conflit + hors horaire
				if (conflitsMess != null && !conflitsMess.startsWith("null")) {
					if (dispo)
						repDispo.setProperty(SPConstantes.PROP_MESSAGE, conflitsMess);
					else
						repDispo.setProperty(SPConstantes.PROP_MESSAGE, conflitsMess + buffer.toString());
				} else if (!dispo)
					repDispo.setProperty(SPConstantes.PROP_MESSAGE, buffer.toString());

				// Statut et erreur des properties :prop, horaires
				if (getStatut(prop) && getStatut(propHoraires))
					repDispo.setProperty(SPConstantes.PROP_STATUT, "1");
				else {
					repDispo.setProperty(SPConstantes.PROP_STATUT, "0");
					if (getStatut(prop) && getError(propHoraires) != null && !getError(propHoraires).startsWith("null"))
						repDispo.setProperty(SPConstantes.PROP_ERREUR, getError(propHoraires));
					else if (getStatut(propHoraires) && getError(prop) != null && !getError(prop).startsWith("null"))
						repDispo.setProperty(SPConstantes.PROP_ERREUR, getError(prop));
					else if (getError(propHoraires) != null && getError(prop) != null && getError(prop).equals(getError(propHoraires))) // null
																																																															// !!
						repDispo.setProperty(SPConstantes.PROP_ERREUR, getError(prop));
					else if (getError(propHoraires) != null && getError(prop) != null)
						repDispo.setProperty(SPConstantes.PROP_ERREUR, getError(prop) + " + " + getError(propHoraires));
					else
						repDispo.setProperty(SPConstantes.PROP_ERREUR, "Erreur inconnue.");
				}
			} else // il s'agit d'un OBJET ou d'une SALLE
			{
				// boolean true si disponible (non conflit)
				if (conflit)
					repDispo.setProperty(SPConstantes.PROP_BOOL, "false");
				else
					repDispo.setProperty(SPConstantes.PROP_BOOL, "true"); // key, value
				// string message : conflit
				if (conflitsMess != null && !conflitsMess.equals("null"))
					repDispo.setProperty(SPConstantes.PROP_MESSAGE, conflitsMess);
				// Statut et erreur des properties :prop, horaires
				if (getStatut(prop))
					repDispo.setProperty(SPConstantes.PROP_STATUT, "1");
				else {
					repDispo.setProperty(SPConstantes.PROP_STATUT, "0");
					repDispo.setProperty(SPConstantes.PROP_ERREUR, getError(prop));
				}
			}
		} else // probleme de Dates
		{
			repDispo.setProperty(SPConstantes.PROP_STATUT, "0");
			repDispo.setProperty(SPConstantes.PROP_ERREUR, "Dates invalides.");
		}

		// retourne properties contenant un booleen a true si disponible (en horaire
		// + non conflit)
		// + string message donnant le messAlertConflits + mess si hors horaire
		// donnant info horaire de la journée.
		// + statut + erreur
		return repDispo;
	}

	// -----------------------
	/**
	 * Methode retournant un booléen renvoyé par le serveur. Apres
	 * rechercheConflits : ll indique true si au moins un conflit a été trouvé.
	 * Apres isDisponible : il indique true si pas de conflit et en periode
	 * d'horaire de travail.
	 * 
	 * @param prop
	 *          Properties reçu du serveur;
	 * @return un boolean.
	 */

	public static boolean getBoolean(Properties prop) {
		boolean ok = false;
		if (prop.getProperty(SPConstantes.PROP_BOOL) != null)
			if (prop.getProperty(SPConstantes.PROP_BOOL).equals("true"))
				ok = true;
		return ok;
	}

	// ---------------------------
	/**
	 * Methode retournant un message détaillant les conflits trouvés et
	 * eventuellement (pour isDisponible) les horaires d'une personne sur 1
	 * journée.
	 * 
	 * @param prop
	 *          Properties reçu du serveur;
	 * @return un String ou null.
	 */

	public static String getMessage(Properties prop) {
		String message = null;
		if (prop.getProperty(SPConstantes.PROP_MESSAGE) != null)
			message = prop.getProperty(SPConstantes.PROP_MESSAGE);

		return message;
	}

	// -------------------------------
	/**
	 * Methode testant si l'appel au serveur s'est bien passé. "true" est renvoyée
	 * si une application est inacessible, pour ne pas gêner la récupération des
	 * autres résultats.
	 * 
	 * @param prop
	 *          Properties reçu du serveur;
	 * @return un boolean à true si aucun probleme.
	 */

	public static boolean getStatut(Properties prop) {
		return DemandePlanning.getStatut(prop);
	}

	// --------------------------------
	/**
	 * Methode renvoyant l'erreur survenue ou null si pas d'erreur.
	 * 
	 * @param prop
	 *          Properties reçu du serveur;
	 * @return un String de l'erreur renvoyé par le serveur ou null.
	 */

	public static String getError(Properties prop) {
		return DemandePlanning.getError(prop);
	}
}
