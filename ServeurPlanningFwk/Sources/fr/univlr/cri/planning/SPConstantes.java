package fr.univlr.cri.planning;


/**
 * Cette classe définie les constantes utiles aux methodes interagissant avec le Serveur de Planning.
 * 
 * Les constrantes <code>protected</code> ne sont utilisee que par le framework.
 */
public abstract class SPConstantes {
	
	
	/**Cette constante donne le format de date utilisé par le Serveur de Planning. 
	 * Elle peut etre utilisée lors d'un partage d'informations avec le Serveur de planning,
	 * en utilisant la struture SPOccupation.
	 */
	public final static String DATE_FORMAT = "%d/%m/%Y %H:%M";
	
	/* Cette constante donne la clé pour l'url du Serveur de Planning. 
	 * L'Url est cherchée dans fichier .config ou table 'grhum_parametres'
	 */
	protected final static String SP_KEY_URL = "GRHUM_PLANNING_URL"; 
	
	// info DemandePlanning + rechercheConflits
	
	/**Cette constante indique que la requete concerne un Individu. 
	 * Elle est utilisée lors d'une demande d'informations au Serveur de planning :
	 * horaires, congés, réunions...
	 */
	public final static Integer IDKEY_INDIVIDU = new Integer(1); 
	public final static String LABEL_INDIVIDU = "individu";
	
	/**Cette constante indique que la requete concerne un Objet. 
	 * Elle est utilisée lors d'une demande d'informations au Serveur de planning :
	 * réservation de matériels...
	 */
	public final static Integer IDKEY_OBJET = new Integer(2);
	public final static String LABEL_OBJET = "objet";
	
	/**Cette constante indique que la requete concerne une Salle. 
	 * Elle est utilisée lors d'une demande d'informations au Serveur de planning :
	 * réservation de locaux...
	 */
	public final static Integer IDKEY_SALLE = new Integer(3);
	public final static String LABEL_SALLE = "salle";
	
	/**
	 * Cette constante indique que la requete concerne une specialisation d'un diplome.
	 * C'est la description d'un diplome detaillé (dans la base, MRSEM_KEY)
	 */
	public final static Integer IDKEY_DIPLOME = new Integer(4);
	public final static String LABEL_DIPLOME = "diplome";
	
	/**
	 * Cette constante indique que la requete concerne une groupe etudiant.
	 * C'est la description d'un groupe (dans la base, GGRP_KEY)
	 */
	public final static Integer IDKEY_GROUPE_ETUDIANT = new Integer(5);
	public final static String LABEL_GROUPE_ETUDIANT = "groupeEtudiant";
	
	/**
	 * Cette constante indique que la requete concerne une groupe.
	 * C'est la description d'un groupe (dans la base, C_STRUCTURE)
	 */
	public final static Integer IDKEY_GROUPE = new Integer(6);
	public final static String LABEL_GROUPE = "groupe";
	
	 
	/**Cette constante indique que l'affichage de cette occupation est publique. 
	 */
	public final static String AFF_PUBLIC = "PUBLIC";
	
	/**Cette constante indique que l'affichage de cette occupation est privée. 
	 */
	public final static String AFF_PRIVEE = "PRIVATE";
	
	/**Cette constante indique que l'affichage de cette occupation est confidentielle. 
	 */
	public final static String AFF_CONFIDENTIEL = "CONFIDENTIAL";
	
	// info DemandePlanning -> ServeurPlanning
	
	/**Clé du parametre 1 envoyé au Serveur de planning.*/
	public final static String PAR1_METHCLIENT = "nomclient";  
	
//	/**Clé du parametre 2 entre DemandePlanning et le Serveur de planning.*/
//	public static String PAR2_IDKEYS = "idkeys";
//	/**Clé du parametre 3 entre DemandePlanning et le Serveur de planning.*/
//	public static String PAR3_IDVALS = "idvals";
//	/**Clé du parametre 4 entre DemandePlanning et le Serveur de planning.*/
//	public static String PAR4_DATESDEB = "debuts";
//	/**Clé du parametre 5 entre DemandePlanning et le Serveur de planning.*/
//	public static String PAR5_DATESFIN = "fins";
	

	// info ServeurPlanning -> Framework
	
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_STATUT = "statut"; 
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_ERREUR = "erreur";
	
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_BOOL = "boolean";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_NUMB = "number";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_NB = "nbOcc";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_DETAIL = "detail";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_AFF = "affichage";
	
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning ou
	 * clé WORequest entre RechercheConflits et le Serveur de Planning.*/
	public final static String PROP_OC_DEB = "debut";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_FIN = "fin";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_IDVAL = "idval";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_IDKEY = "idkey";
	/**Clé WOReponse Content entre le Serveur de planning et DemandePlanning.*/
	public final static String PROP_OC_TYPE = "type";

	/**Clé WOReponse Content entre le Serveur de planning et RecherchePlanning.*/
	public final static String PROP_MESSAGE = "message";
	/**Clé WOReponse contenant l'url de l'application qui fait l'appel.*/
	public final static String PROP_CALLER = "urlCaller";
	/**Clé WOReponse Content entre le Serveur de planning et AvantModifPlanning.*/
	public final static String PROP_IS_DISPONIBLE_CONNECTED = "isDisponibleConnected";
    
	/** SPOccupation.typeTemps pour les occupations typees missions */
	public final static String SPOCC_TYPE_MISSION 	= "MIS";
	/** SPOccupation.typeTemps pour les occupations typees traitement de demande de travaux */
	public final static String SPOCC_TYPE_DT 				= "DT";
	/** SPOccupation.typeTemps pour les occupations issues de conges (absences) */
	public final static String SPOCC_TYPE_ABSENCE		= "ABS";
	/** SPOccupation.typeTemps pour les occupations issues de conges (horaires et heures sup) */
	public final static String SPOCC_TYPE_PRESENCE	= "PRE";
	/** SPOccupation.typeTemps pour les occupations provenant d'une source iCalendar */
	public final static String SPOCC_TYPE_ICS 			= "ICS";
	
	// noms de DirectAction vers le serveur de planning
	
	/** verifier qu'une application repond */
	public final static String DIRECT_ACTION_WOALIVE = "WOAliveCheck";
	/** le point d'entree pour savoir la disponibilite horaire et occupation */
	public final static String DIRECT_ACTION_IS_DISPONIBLE = "isDisponible";
	/** le point d'entree pour interroger un planning */
	protected final static String DIRECT_ACTION_READ_PLANNING = "servPlan";
	/** en vue d'une modification de planning (a coupler a isDisponible) */
	protected final static String DIRECT_ACTION_CHANGE_PLANNING = "modifPlan";
	
	// noms de methodes vers le serveur de planning
	
	/** */
	protected final static String METHOD_NAME_CLIENT_MODIF = "clientModif";
	
	// les types de variables
	public final static int TYPE_BOOLEAN 				= 3;
	public final static int TYPE_NUMBER 				= 10;
	public final static int TYPE_INTEGER 				= 11;
	public final static int TYPE_BIG_DECIMAL 		= 13;
	public final static int TYPE_BIG_INTEGER 		= 14;
	public final static int TYPE_FLOAT 					= 15;
	public final static int TYPE_REAL 					= 16;
	public final static int TYPE_SHORT 					= 17;
	public final static int TYPE_LONG 					= 18;
	public final static int TYPE_SP_OCCUPATION 	= 31;
}
