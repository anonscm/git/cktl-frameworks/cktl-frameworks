package fr.univlr.cri.planning;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOHTTPConnection;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import fr.univlr.cri.planning._imports.CktlLog;
import fr.univlr.cri.planning._imports.DateCtrl;
import fr.univlr.cri.planning._imports.StringCtrl;

/**
 * <body> Cette classe contient les methodes nécessaires pour une demande
 * d'information au Serveur de Planning (pour methode Cliente).<br>
 * <br>
 * <strong>On y trouve :</strong><br>
 * <br> _ 1 methode de vérification de l'accessibilité du serveur de Planning;<br> _
 * 3 methodes de connection, choisir 1 selon le format ou nombre de parametres;<br> _
 * 2 methodes pour la gestion des erreurs/exceptions ( getStatut et
 * getError );<br> _ 5 methodes de récupération des résultats retournés
 * par le Serveur de Planning, choisir 1 selon le type d'information voulue.<br>
 * <br>
 * 
 * <strong>Avant d'utiliser ces methodes, certaines données doivent etre
 * renseignées dans la base de données :</strong><br>
 * <br> _ table sp_methode : 1 ligne pour la methode cliente qui accédera au
 * serveur (avec attribut met_bool_serveur à 0), vérifier que les DirectActions
 * à utiliser sont inscrites;<br> _ table sp_met_client : 1 ligne indiquant le
 * type de données attendu en retour et, si SPOccupation, le niveau de
 * traitement des données (0:aucun, 1:horaire+occupation prior, 2:occupation
 * prior);<br> _ table sp_repartition : indiquer les numéros des DirectActions
 * à utiliser et le type d'identifiant conserné(1:individu, 2:objet, 3:salle).<br>
 * <br>
 * 
 * <strong>Exemple de code :</strong><br>
 * <br> 1/ Cas d'une requete simple :
 * <br>
 * <p class="example">
 * if (DemandePlanning.serveurPlanningAccessible())<br> {<br>
 *  // creation des parametres <br>
 * NSTimestamp debut = DateCtrl.stringToDate("16/04/2006 12:00",
 * SPConstantes.OC_DATE_FORMAT);<br>
 * NSTimestamp fin = DateCtrl.stringToDate("23/04/2006 12:00",
 * SPConstantes.OC_DATE_FORMAT);<br>
 * Integer noIndividu = new Integer("4371");<br>
 * // appel au serveur de planning
 * <br>Properties response = DemandePlanning.connectionAuServeur(
 * "maMethode", SPConstantes.IDKEY_INDIVIDU, noIndividu, debut, fin );
 * <br>// recuperation des resultats
 * </p>
 * <br> 2/ Cas d'une requete multiple :
 * <br>
 * <p class="example">
 * if (DemandePlanning.serveurPlanningAccessible())<br> {<br>
 *  // creation des parametres
 * <br> SPOccupation o1 = new SPOccupation(deb1, fin1, SPConstantes.IDKEY_INDIVIDU, Integer.valueOf(val1));
 * <br> SPOccupation o2 = new SPOccupation(deb2, fin2, SPConstantes.IDKEY_INDIVIDU, Integer.valueOf(val2)); 
 * <br> NSMutableArray param = new NSMutableArray();
 * <br> param.addObject(o1); param.addObject(o2);
 * <br> // appel au serveur de planning
 * <br> Properties response = DemandePlanning.connectionAuServeur("maMethode", param);
 * <br> //recuperation des resultats
 * </p>
 * <br> 3/ Récupération des résultats :
 * <br>
 * <p class="example">
 * if (DemandePlanning.getStatut(response)) <br> {<br>
 *  // exemple pour un resultat de type Planning : <br>
 * NSArray occups = DemandePlanning.getPlanning(response);<br>
 * // occups est un NSArray d'NSArrays de SPOccupations<br>
 * for (int i = 0; i < occups.count(); i++) <br> {<br>
 * .  NSArray occs = (NSArray) occups.objectAtIndex(i);<br>
 * .  for (int k = 0; k < occs.count(); k++)  <br>.  {<br>
 * . .   SPOccupation oc = (SPOccupation) occs.objectAtIndex(k);
 * <br>. .   // traitement<br> .  }
 * <br> }<br>
 * else<br>
 * .  String erreur = DemandePlanning.getError(response); <br> }<br>
 * </p>
 * 
 * @author Celine MENETREY <celine.menetrey at univ-lr.fr> <body/>
 */

public class DemandePlanning {

	private static String _SPUrl = "";
	
	static {
		_SPUrl = (String) ((Hashtable) WOApplication.application().valueForKeyPath("config")).get(SPConstantes.SP_KEY_URL);
	}
	
	//http://CRISTA05:4855/cgi-bin/WebObjects/ServeurPlanning.woa/wa/
	 //http://www.univ-lr.fr/cgi-bin/WebObjects/ServeurPlanning.woa/wa/
	// ********************************************************
	// Methodes pour Client du Serveur de Planning
	// ********************************************************

	// peut pas tester avec client local, faut une vraie appli
	// ajout attribut _SPUrl

	/**
	 * Methode testant si le serveur de Planning est accessible.
	 * 
	 * @return un boolean à true si Serveur de Planning accessible.
	 */

	public static boolean serveurPlanningAccessible() {
		boolean ok = false;

			URL spUrl;
			try {
				spUrl = new URL(_SPUrl);

				String adress = _SPUrl + SPConstantes.DIRECT_ACTION_WOALIVE;

				WOHTTPConnection htt = new WOHTTPConnection(spUrl.getHost(), 
						spUrl.getPort());
				htt.setKeepAliveEnabled(false);

				WORequest req = new WORequest("GET", adress, "HTTP/1.1", null, null,
						null);
				boolean bool = htt.sendRequest(req);
				if (bool) // requete bien envoyée
				{
					String res = htt.readResponse().contentString();
					if (res.equals("OK"))
						ok = true;
				}
			} catch (MalformedURLException e) {
			}
			catch (Exception exc){
			}
		return ok;
	}

	// -----------------------------------------------
	// Methodes Principales pour connection au serveur
	// -----------------------------------------------

	/**
	 * Methode de connection au serveur de Planning prennant 1 String et 4
	 * Objects, 1 pour chaque paramètre. Cette methode est utilisée pour envoyer
	 * une requete simple.
	 * 
	 * @param nomMeth
	 *          String du nom de la methode appelant le serveur (celui indiqué
	 *          dans BD); <br>
	 * @param idkey
	 *          Object (Integer) d'un type identifiant (1:Individu, 2:objet,
	 *          3:salle);<br>
	 * @param idval
	 *          Object (Integer ou String) d'un identifiant (numéro d'Individu,
	 *          d'objet ou de salle);<br>
	 * @param debut
	 *          Object (NSTimestamp ou String) d'une date de debut format
	 *          "jj/mm/aaaa hh:mm";<br>
	 * @param fin
	 *          Object (NSTimestamp ou String) d'une date de fin format
	 *          "jj/mm/aaaa hh:mm";<br>
	 * @return un Properties contenant les resultats renvoyés par le
	 *         serveur de planning.
	 * @see SPConstantes
	 */

	// pour un seul groupe de parametres
	public static Properties connectionAuServeur(String nomMeth, Object idkey,
			Object idval, Object debut, Object fin) {

		Properties response = connectionAuServeur(nomMeth, new NSArray(
				(Object) idkey), new NSArray(idval), new NSArray(debut), new NSArray(
				fin));

		return response;
	}

	/**
	 * Methode de connection au serveur de Planning prennant 1 String et 1
	 * NSArrays de SPOccupations. Cette methode est utilisée pour envoyer une
	 * requete multiple, 1SPOccupation correspondant à une requete simple.
	 * 
	 * @param nomMeth
	 *          String du nom de la methode appelant le serveur (celui indiqué
	 *          dans BD); <br>
	 * @param occups
	 *          NSArray contenant des SPOccupations;<br>
	 * @return un Properties contenant les resultats renvoyés par le
	 *         serveur de planning.
	 * @see SPOccupation
	 */

	// avec params dans 1 tableau de SPOccupations
	public static Properties connectionAuServeur(String nomMeth, NSArray occups) {
		StringBuffer buffer = paramsToBuffer(occups);

		Properties response = connection(nomMeth, buffer, SPConstantes.DIRECT_ACTION_READ_PLANNING);

		return response;
	}

	/**
	 * Methode de connection au serveur de Planning prennant 1 String et 4
	 * NSArrays, 1 pour chaque paramètre. Cette methode est utilisée pour envoyer
	 * une requete multiple : les premieres valeurs de chaque NSArray forme la
	 * requete 1, les secondes valeurs la requete 2, etc.
	 * 
	 * @param nomMeth
	 *          String du nom de la methode appelant le serveur (celui indiqué
	 *          dans BD); <br>
	 * @param idkeys
	 *          NSArray contenant des Integers, types identifiant (1:Individu,
	 *          2:objet, 3:salle);<br>
	 * @param idvals
	 *          NSArray contenant les identifiants (Integer ou String : numéro
	 *          d'Individu, d'objet ou de salle);<br>
	 * @param debuts
	 *          NSArray contenant les dates de debut de periode format "jj/mm/aaaa
	 *          hh:mm" (NSTimestamp ou String);<br>
	 * @param finis
	 *          NSArray contenant les dates de fin de periode format "jj/mm/aaaa
	 *          hh:mm" (NSTimestamp ou String);<br>
	 * @return un Properties contenant les resultats renvoyés par le
	 *         serveur de planning.
	 * @see SPConstantes
	 */

	// avec params dans 4 tableaux
	public static Properties connectionAuServeur(String nomMeth, NSArray idkeys,
			NSArray idvals, NSArray debuts, NSArray finis) {

		StringBuffer buffer = paramsToBuffer(idkeys, idvals, debuts, finis);

		Properties response = connection(nomMeth, buffer, SPConstantes.DIRECT_ACTION_READ_PLANNING);

		return response;
	}

	// -------------------------------------------------
	// Methodes de recuperation des Resultats
	// -------------------------------------------------

	// ------------------------------------
	// Pour tableau d'Occupation en NSArray

	/**
	 * Methode de recupération de resultats Planning. Un resultat Planning
	 * contient des données : debut, fin, motif, detail + identifiant et type
	 * identifiant. La methode retourne 1 NSArray contenant des NSArrays de
	 * SPOccupations : 1 seul NSArray si requete simple, plusieurs si requete
	 * multiple.
	 * 
	 * @param prop
	 *          Properties retournées par le serveur de planning.
	 * @return un NSArray contenant des NSArray de SPOccupations.
	 * @see SPOccupation
	 */

	public static NSArray getPlanning(Properties prop) {
		NSMutableArray tabOcc = new NSMutableArray();

		// changement passage param par Content plutot Header

		if (getStatut(prop) && (prop.getProperty(SPConstantes.PROP_OC_NB) != null)) {
			String nb = prop.getProperty(SPConstantes.PROP_OC_NB);
			NSMutableArray tab = new NSMutableArray();

			for (int i = 0; i < Integer.parseInt(nb); i++) {
				SPOccupation occ = new SPOccupation(); // dans for sinon pb

				occ.setDateDebutFromString(
						prop.getProperty(SPConstantes.PROP_OC_DEB + i));
				occ.setDateFinFromString(
						prop.getProperty(SPConstantes.PROP_OC_FIN + i));
				occ.setIdVal(
						new Integer(prop.getProperty(SPConstantes.PROP_OC_IDVAL + i)));
				occ.setIdKey(
						new Integer(
								prop.getProperty(SPConstantes.PROP_OC_IDKEY + i)));
				occ.setTypeTemps(
						prop.getProperty(SPConstantes.PROP_OC_TYPE + i));
				
				if (prop.getProperty(SPConstantes.PROP_OC_AFF + i) != null) 
					occ.setAffichage(prop.getProperty(SPConstantes.PROP_OC_AFF + i));
				// fait parti de quel group
				// truc : si plusieurs group, insérer singe ":-)"
				if (prop.getProperty(SPConstantes.PROP_OC_DETAIL + i) != null) {
					String det = prop.getProperty(SPConstantes.PROP_OC_DETAIL + i);
					if (det.endsWith(":-)")) // fin du group
					{
						if (!det.equals(":-)")) {
							det = det.substring(0, det.length() - 3);
							occ.setDetailsTemps(det);
						}
						tab.addObject(occ);
						tabOcc.addObject(tab);
						tab = new NSMutableArray();
					} else {
						occ.setDetailsTemps(det);
						tab.addObject(occ);
					}
				} else
					tab.addObject(occ);
			}
		}// si nbOcc null, tableau renvoyé vide,
		// sinon renvoit des NSArray d'SPOccupation
		return new NSArray(tabOcc);
	}

	/*
	 * public static SPOccupation resultatUneOccupation(WOResponse resp) {
	 * SPOccupation occ = new SPOccupation(); if (getOK(resp) &&
	 * (resp.headerForKey(SPConstantes.KEY_OC_NB) != null)) {
	 * occ.setDateDebutFromString(resp.headerForKey(SPConstantes.KEY_OC_DEB +
	 * "0")); occ .setDateFinFromString(resp .headerForKey(SPConstantes.KEY_OC_FIN +
	 * "0")); occ.setIdVal(new Integer(resp.headerForKey(SPConstantes.KEY_OC_IDVAL +
	 * "0"))); occ.setIdKey(new
	 * Integer(resp.headerForKey(SPConstantes.KEY_OC_IDKEY + "0")));
	 * occ.setTypeTemps(resp.headerForKey(SPConstantes.KEY_OC_TYPE + "0")); if
	 * (resp.headerForKey(SPConstantes.KEY_OC_DETAIL + "0") != null) { String
	 * detail = resp.headerForKey(SPConstantes.KEY_OC_DETAIL + "0"); if (!
	 * detail.equals(":-)")) occ.setDetailsTemps(
	 * resp.headerForKey(SPConstantes.KEY_OC_DETAIL + "0")); } } return occ; // si
	 * pb, renvoit un SPOccupation avec attributs null }
	 */

	// ------------------------------------
	// Pour Nombre de Demi-Journées
	/**
	 * Methode de recupération des resultats numériques. Pour une requete simple,
	 * un NSArray d'un seul nombre est retourné. Pour une requete multiple, un
	 * NSArray de plusieurs nombres est retourné.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un NSArray de nombres.
	 */

	public static NSArray getNumeric(Properties prop) {

		NSMutableArray arr = new NSMutableArray();
		if (getStatut(prop)) {
			int i = 1;
			while (prop.getProperty(SPConstantes.PROP_NUMB + i) != null) {
				BigDecimal nb = new BigDecimal(prop.getProperty(SPConstantes.PROP_NUMB
						+ i));
				arr.addObject((Number) nb);
				i++;
			}
		} // fin si pas erreur, sinon renvoit tableau vide

		return new NSArray(arr);
	}

	/**
	 * Methode de recupération d'un resultat numérique. Si une requete multiple a
	 * été envoyé au serveur, le nombre retourné est la somme des nombres reçus.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un Number
	 */

	public static Number getOneNumeric(Properties prop) {

		BigDecimal nb1 = new BigDecimal("1");
		if (getStatut(prop))
			if (prop.getProperty(SPConstantes.PROP_NUMB + "1") != null)
				nb1 = new BigDecimal(prop.getProperty(SPConstantes.PROP_NUMB + "1"));
		int i = 2;
		while (prop.getProperty(SPConstantes.PROP_NUMB + i) != null) {
			double n1 = nb1.doubleValue()
					+ new BigDecimal(prop.getProperty(SPConstantes.PROP_NUMB + i))
							.doubleValue();
			nb1 = new BigDecimal(n1);
			i++;
		}

		return (Number) nb1; // si pb renvoit 0
	}

	// ---------------------------------------
	// Pour Booleen

	// A TESTER : commence a 0 ou a 1 ??

	/**
	 * Methode de recupération de resultats booleens. Pour une requete simple, la
	 * methode retourne un NSArray contenant un seul Boolean. Pour une requete
	 * multiple, la methode retourne un NSArray de plusieurs Booleans.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un NSArray de Booleans.
	 */

	public static NSArray getBooleen(Properties prop) {

		NSMutableArray arr = new NSMutableArray();
		if (getStatut(prop)) {
			int i = 1;
			while (prop.getProperty(SPConstantes.PROP_BOOL + i) != null) {
				Boolean b = new Boolean(false);
				if (!prop.getProperty(SPConstantes.PROP_BOOL + i).equals(b.toString()))
					b = new Boolean(true);
				arr.addObject(b);
				i++;
			}
		} // fin si (pas erreur), sinon renvoit tableau vide

		return new NSArray(arr);
	}

	/**
	 * Methode de recupération d'un resultat booléen. Si une requete multiple a
	 * été envoyé au serveur, le booléen retourné est false si au moins un booléen
	 * reçu est false ou s'il y a eu une erreur, sinon true. Pour avoir chaque
	 * booléen séparément, utiliser 'resultatBooleen'.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un Boolean
	 */

	public static Boolean getOneBooleen(Properties prop) {
		int i = 1;
		Boolean b = new Boolean(false);
		Boolean bool = new Boolean(true);
		if (getStatut(prop))
			while (prop.getProperty(SPConstantes.PROP_BOOL + i) != null
					&& bool.booleanValue() == true) {
				if (prop.getProperty(SPConstantes.PROP_BOOL + i).equals(b.toString()))
					bool = new Boolean(false);
				i++;
			}
		return bool; // si pb renvoit false
	}

	/**
	 * Methode testant si l'appel au serveur s'est bien passé.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un boolean à true si aucun probleme.
	 */

	public static boolean getStatut(Properties prop) {

		boolean ok = true;
		if (prop.getProperty(SPConstantes.PROP_STATUT) == null)
			ok = false;
		else if (!(prop.getProperty(SPConstantes.PROP_STATUT).equals("1"))
				&& prop.getProperty(SPConstantes.PROP_ERREUR) != null)
			ok = false;
		return ok;
	}

	/**
	 * Methode renvoyant l'erreur survenue ou null si pas d'erreur.
	 * 
	 * @param prop
	 *          Properties reçu du serveur (par une methode connectionAuServeur);
	 * @return un String de l'erreur renvoyé par le serveur ou null.
	 */

	public static String getError(Properties prop) {
		String err = null;
		if (!getStatut(prop))
			err = prop.getProperty(SPConstantes.PROP_ERREUR);
		return err;
	}

	
	/** ************************************************************** */

	// methode de connection au serveur après traitement params en buffer
	protected static Properties connection(String nomMeth, StringBuffer buffer, String nomDA) {
		
		Properties prop = new Properties();
		WOResponse wores = new WOResponse();
		String url = ServeurPlanningURL();
		URL adr;
		try {
			adr = new URL(url+nomDA+"?");
			
			WOHTTPConnection htt = new WOHTTPConnection(adr.getHost(), adr.getPort());
			htt.setKeepAliveEnabled(false);
			htt.setSendTimeout(30000);
			htt.setReceiveTimeout(40000);
			// indique que connection doit etre fermée après envoit requete

			WORequest req = new WORequest("GET", adr.toString(), "HTTP/1.1", null, null, null);
			buffer.append(SPConstantes.PAR1_METHCLIENT+" = "+nomMeth+ "\n");
			req.setContent(buffer.toString()); // insertion params
			
			boolean bool = htt.sendRequest(req);
			if (bool) // requete bien envoyée
			{
				wores = htt.readResponse();
				String res = wores.contentString();
				prop = SPMethodes.stringToProperties(res);

			} else {
				if (prop.getProperty(SPConstantes.PROP_STATUT) == null) {
					prop.setProperty(SPConstantes.PROP_STATUT, "0");
					prop.setProperty(SPConstantes.PROP_ERREUR,
							"Probleme ServeurPlanning : ne repond pas ou trop lent.");
				}
			}
		} catch (MalformedURLException e) {
			prop.setProperty(SPConstantes.PROP_STATUT, "0");
			prop.setProperty(SPConstantes.PROP_ERREUR,
					"Probleme ServeurPlanning : url n'est pas accessible.");
		}
		return prop;
	}
	
	public static String ServeurPlanningURL()
	{
		return _SPUrl;
	}

	// ------ Methodes de traitement des parametres ---------

	private static StringBuffer paramsToBuffer(NSArray occups) {

		NSMutableArray keys = new NSMutableArray();
		NSMutableArray vals = new NSMutableArray();
		NSMutableArray debs = new NSMutableArray();
		NSMutableArray fins = new NSMutableArray();
		StringBuffer buffer = new StringBuffer();

		for (int i = 0; i < occups.count(); i++) {
			SPOccupation o1 = (SPOccupation) occups.objectAtIndex(i);
			keys.addObject(String.valueOf(o1.getIdKey().intValue()));
			vals.addObject(String.valueOf(o1.getIdVal().intValue()));
			debs.addObject(DateCtrl.dateToString(o1.getDateDebut(),				SPConstantes.DATE_FORMAT));
			fins.addObject(DateCtrl.dateToString(o1.getDateFin(),					SPConstantes.DATE_FORMAT));
		}

		buffer = paramsToBuffer(keys, vals, debs, fins);

		return buffer;
	}

	// ------------

	private static StringBuffer paramsToBuffer(NSArray idkeys, NSArray idvals,
			NSArray debuts, NSArray finis) {

		// pour passage params avec buffer dans CONTENT
		StringBuffer buffer = new StringBuffer();

		// params Object[] -> String
//		String idks = SPMethodes.numericsToString(idkeys).toString();
//		String idvs = SPMethodes.numericsToString(idvals).toString();
//		String debs = SPMethodes.datesToString(debuts).toString();
//		String fins = SPMethodes.datesToString(finis).toString();
//
//		buffer.append(SPConstantes.PAR2_IDKEYS + " = " + idks + "\n");
//		buffer.append(SPConstantes.PAR3_IDVALS + " = " + idvs + "\n");
//		buffer.append(SPConstantes.PAR4_DATESDEB + " = " + debs + "\n");
//		buffer.append(SPConstantes.PAR5_DATESFIN + " = " + fins + "\n");
		
		// new pour format Properties complet
		
		try {
			for (int i = 0; i<idkeys.count(); i++)
			{
				buffer.append(SPConstantes.PROP_OC_IDKEY+ i +" = " + idkeys.objectAtIndex(i).toString() + "\n");
				buffer.append(SPConstantes.PROP_OC_IDVAL+ i +" = " + idvals.objectAtIndex(i).toString() + "\n");
				buffer.append(SPConstantes.PROP_OC_DEB+ i +" = " + SPMethodes.dateToString(debuts.objectAtIndex(i)).toString() + "\n");
				buffer.append(SPConstantes.PROP_OC_FIN+ i +" = " + SPMethodes.dateToString(finis.objectAtIndex(i)).toString() + "\n");
			}
		}catch (ArrayIndexOutOfBoundsException arrEx)
		{}

		return buffer;
	}

}
