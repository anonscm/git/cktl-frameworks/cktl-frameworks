package fr.univlr.cri.planning;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.StringTokenizer;

import com.webobjects.appserver.WOHTTPConnection;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.cri.planning._imports.CktlLog;
import fr.univlr.cri.planning._imports.DateCtrl;


/**
 * <body> Cette classe contient les methodes nécessaires pour lire ou écrire dans 
 * un iCalendar (fichier .ics).<br>
 * <br>
 * <strong>On y trouve :</strong><br>
 * <br> _ 1 methode de lecture : readICalendar;
 * <br> _ 1 methode de création/écriture : createICalendar;
 * <br> _ 2 methodes pour la gestion des erreurs/exceptions apres une lecture : getStatut et getError;
 * <br> _ 1 methode de récupération des résultats retournés par la methode de lecture : getPlanning.<br>
 * <br>
 * <strong>Methode de lecture 'readICalendar' :</strong><br>
 * <br> Si le nom du calendrier est donné, on trouve l'url WebDav avec le noIndividu dans la table ICAL. 
 * Sinon, on prend en compte tous les calendriers inscris dans la table ICAL.<br>
	 * Cette methode fait appel au Serveur de planning pour récupérer et formater les informations.<br>
	 * Les informations trouvées sont retournées en Properties  (clé1 = valeur1, clé2 = valeur2...). 
	 * Toutes les dates sont au format SPConstantes.DATE_FORMAT (JJ/MM/AAAA hh:mm).
	 * On récupère un NSArray avec la méthode getPlanning.<br>
	 * <br>
	 * <strong>Methode de récupération des résultats 'getPlanning' :</strong><br><br>
	 * Elle retourne des informations données dans un NSArray contenant des SPOccupations. 
	 * Si le iCalendar traité contenait des évènements répétés (défini par des régles ou des dates),
	 * il est créé une SPOccupation pour chaque répétition.<br>
	 * <br>
	 * <strong>Methode de creation/ecriture 'createICalendar' : </strong><br> <br>
	 * Elle recoit en parametre le chemin ou placer le Calendrier. 
	 * Celui-ci doit etre un chemin local du type "C:\\Documents and Settings\\..." ou une URL vers un dossier WebDav,
	 * du type "http://ical.univ-lr.fr/login/".
	 * Le troisieme parametre doit etre un NSArray de SPOccupation, contenant les informations à insérer dans le iCalendar.
	 * Les valeurs prises en compte étant dateDebut, dateFin, typeTemps et detailsTemps. 
	 * Cette methode fait appel au Serveur de planning pour formater les informations.<br>
	 * 
	 * @see fr.univlr.cri.planning.SPOccupation
	 * 
 */


public class ICalendar {
	
	
	/**
	 * Methode pour lire dans des calendriers (fichier .ics) et formater les informations trouvées.<br>
	 * La récupération des résultats se fait grace aux methodes : getStatut, getError, getPlanning.
	 * @param noIndividu un Number identifiant de l'individu proprietaire du calendrier a lire.
	 * @param debut un NSTimestamp du debut de periode a lire ou null pour tout lire.
	 * @param fin un NSTimestamp de la fin de periode a lire ou null pour tout lire.
	 * @param nomCal un String du nom calendrier a lire ou NULL pour lire tous les calendriers de l'individu.
	 * @return Properties
	 */	
	public static Properties readICalendar(Number noIndividu, NSTimestamp debut, 
			NSTimestamp fin, String nomCal)
	{
		StringBuffer buffer = new StringBuffer();
		Properties prop = new Properties();
		if (noIndividu != null)
		{
			buffer.append("noIndividu = "+noIndividu+"\n");
			if (debut != null)
				buffer.append("debut = "+DateCtrl.dateToString(
						debut,SPConstantes.DATE_FORMAT)+"\n");
			if (fin != null)
				buffer.append("fin = "+DateCtrl.dateToString(
						fin,SPConstantes.DATE_FORMAT)+"\n");
			if (nomCal != null)
				buffer.append("calendarName = "+nomCal+"\n");
			
			prop = DemandePlanning.connection("readICalendar", buffer, "iCalendarPourPeriode");
		}
		else
		{
			prop.setProperty("0","statut");
			prop.setProperty("Probleme : noIndividu null.","erreur");
		}
		return prop;
	}
	
	/**
	 * Methode a utiliser apres 'readICalendar', retournant TRUE si aucun probleme n'est survenu.
	 * @param prop Properties recues après 'readICalendar'.
	 * @return boolean a FALSE si un probleme est apparu, sinon a TRUE.
	 */
	
	public static boolean getStatut(Properties prop) {
		return (DemandePlanning.getStatut(prop));
	}
	
	
	/**
	 * Methode a utiliser apres 'readICalendar', retournant le message d'erreur ou null.
	 * @param prop Properties recues après 'readICalendar'.
	 * @return message d'erreur ou null.
	 */
	
	public static String getError(Properties prop) {
		return DemandePlanning.getError(prop);
	}
	
	
	/**
	 * Methode a utiliser apres 'readICalendar', retournant les informations lues dans le ICalendar.
	 * Ces infos sont données dans un NSArray contenant une SPOccupation par Event trouvé, ou plus s'il y avait des répétitions de défini.
	 * Chaque SPOccupation contient des valeurs pour les clés : debut, fin, type, affichage et details.
	 * @param prop Properties recues après 'readICalendar'.
	 * @return NSArray contenant des SPOccupations (avec idkey et idval a null).
	 */
	
	public static NSArray getPlanning(Properties prop) {
		CktlLog.log("getPlanning()");
		NSMutableArray arr = new NSMutableArray();
		int i = 0;
		while (prop.getProperty("debut"+i) != null)
		{
			SPOccupation occ = new SPOccupation();
			occ.setDateDebutFromString(prop.getProperty("debut"+i));
			if (prop.getProperty("fin"+i) != null)
				occ.setDateFinFromString(prop.getProperty("fin"+i));
			if (prop.getProperty("type"+i) != null)
				occ.setTypeTemps(prop.getProperty("type"+i));
			if (prop.getProperty("affichage"+i) != null)
				occ.setAffichage(prop.getProperty("affichage"+i));
			if (prop.getProperty("details"+i) != null)
				occ.setDetailsTemps(prop.getProperty("details"+i));
			
			arr.addObject(occ);
			
			if (prop.getProperty("repeat"+i) != null)
			{
				long ecart = occ.getDateFin().getTime() - occ.getDateDebut().getTime();
				String repeat = prop.getProperty("repeat"+i);
				StringTokenizer st = new StringTokenizer(repeat, ",");
				while (st.hasMoreTokens()) {
					String dateDebut = st.nextToken();
					try{
					NSTimestamp d2 = DateCtrl.stringToDate(dateDebut, SPConstantes.DATE_FORMAT); // format a verifier
					NSTimestamp f2 = new NSTimestamp(d2.getTime()+ecart);
					SPOccupation occRepeat = new SPOccupation(d2, f2, null, null, occ.getTypeTemps(), occ.getDetailsTemps(), occ.getAffichage());
					arr.addObject(occRepeat);
					}catch (Exception e){}
				}		
			}
			i++;
		}
		return new NSArray(arr);
	}
	
	
	/**
	 * Methode pour créer un calendrier (fichier .ics) pour les informations reçues.
	 * Si le nom du fichier donné existe déjà dans le chemin donné, le fichier sera écrasé.
	 * @param cheminEtNomCal String du nom complet (avec chemin) pour le nouveau fichier .ics; 
	 * @param params NSArray de SPOccupation.
	 *  @see SPOccupation
	 */
	
	public static boolean createICalendar(String cheminEtNomCal, 
			NSArray params) 
	{
		StringBuffer buffer = new StringBuffer();
		
		if (cheminEtNomCal != null && !cheminEtNomCal.equals(""))
			buffer.append("cheminEtNom="+cheminEtNomCal+"*\n");
		try{
		for (int i=0;i<params.count();i++) {
			SPOccupation occ = (SPOccupation) params.objectAtIndex(i);
			buffer.append("debut"+i+" = "+DateCtrl.dateToString(
					occ.getDateDebut(),SPConstantes.DATE_FORMAT)+"\n");
			buffer.append("fin"+i+" = "+DateCtrl.dateToString(
					occ.getDateFin(),SPConstantes.DATE_FORMAT)+"\n");
			buffer.append("type"+i+" = "+occ.getTypeTemps()+"\n");
			if (occ.getAffichage().equals(SPConstantes.AFF_PRIVEE))
				buffer.append("affichage"+i+" = "+occ.getAffichage()+"\n");
			if (occ.getDetailsTemps() != null && !occ.getDetailsTemps().equals(""))
				buffer.append("details"+i+" = "+occ.getDetailsTemps()+"\n");
		}
		}catch (Exception e)
		{
//			LRLog.trace("le NSArray en Parametre doit contenir des " +
//					"SPOccupations (voir javadoc).");
		}
		
		boolean statut = false;
		
		if (buffer.toString() != null && buffer.toString() != "");
			statut = connection(buffer);

		return statut;
	}
	

	
	
	// methode de connection au serveur après traitement params en buffer
	// prend des infos en buffer et return le texte a insérer dans fichier ics
	private static boolean connection(StringBuffer buffer) {
		
		//Properties prop = new Properties();
		WOResponse wores = new WOResponse();
		String url = DemandePlanning.ServeurPlanningURL();
		URL adr;
		String res = null;
		boolean statut = false;
		try {
			adr = new URL(url+"createICalendar"+"?");

			WOHTTPConnection htt = new WOHTTPConnection(adr.getHost(), adr.getPort());
			htt.setKeepAliveEnabled(false);
			// indique que connection doit etre fermée après envoit requete

			WORequest req = new WORequest("GET", adr.toString(), "HTTP/1.1", null, null, null);
			req.setContent(buffer.toString()); // insertion params
			
			boolean bool = htt.sendRequest(req);
			if (bool) // requete bien envoyée
			{
				wores = htt.readResponse();
				//wores.setContentEncoding("ISO8859_1");
				res = wores.contentString();	
				Properties prop = SPMethodes.stringToProperties(res);
				if (prop.getProperty("statut") != null && 
						prop.getProperty("statut").toString().equals("true"))
					statut = true;
			} 
//			else 
//				LRLog.trace("Probleme ServeurPlanning : ne repond pas ou trop lent.");
//			
		} catch (MalformedURLException e) {
			//LRLog.trace("Probleme ServeurPlanning : url n'est pas accessible.");
		}
		return statut;
	}

}
