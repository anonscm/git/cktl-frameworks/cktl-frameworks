package fr.univlr.cri.planning;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.cri.planning._imports.CktlLog;
import fr.univlr.cri.planning._imports.DateCtrl;
import fr.univlr.cri.planning._imports.StringCtrl;

/**
 * Cette classe contient quelques methodes utiles pour modifier le type d'une
 * variable.
 */

public class SPMethodes {

	// -----------------------------------------------
	// Methodes toString() pour parametres
	// -----------------------------------------------

	/**
	 * Methode castant un Object numérique en String.
	 * 
	 * @param ob
	 *          Object numeric
	 * @return un String
	 */

	public static String numericToString(Object ob) {
		int res = 0;
		String rep = null;

		if (ob instanceof Integer) {
			res = ((Integer) ob).intValue();
			rep = String.valueOf(res);
		} else if (ob instanceof BigDecimal) {
			res = ((BigDecimal) ob).intValue();
			rep = String.valueOf(res);
		} else if (ob instanceof BigInteger) {
			res = ((BigInteger) ob).intValue();
			rep = String.valueOf(res);
		} else if (ob instanceof Number) // Number plus general
		{
			res = ((Number) ob).intValue();
			rep = String.valueOf(res);
		} else if (ob instanceof String)
			rep = (String) ob;
		else
			rep = "null";

		return rep;
		// si type non trouvé, retourne "null";
	}

	/**
	 * Methode castant un NSArray d'Object numérique en NSArray de String.
	 * 
	 * @param num
	 *          NSArray d'Object numerique
	 * @return un NSArray de String
	 */
	public static NSArray numericsToString(NSArray num) // plusieurs objets
	{
		NSMutableArray reponse = new NSMutableArray();
		for (int i = 0; i < num.count(); i++) {
			Object ob = num.objectAtIndex(i);
			String res = numericToString(ob);
			reponse.addObject(res);
		}
		return reponse;
	}

	/**
	 * Methode castant un Object date en String.
	 * 
	 * @param ob
	 *          Object date
	 * @return un String
	 */
	public static String dateToString(Object ob) // un seul objet
	{
		String res = null;

		if (ob instanceof NSTimestamp)
			res = DateCtrl.dateToString((NSTimestamp) ob, SPConstantes.DATE_FORMAT);
		else if (ob instanceof Date)
			res = ((Date) ob).toLocaleString();
		else if (ob instanceof String)
			res = (String) ob;
		else
			res = "null";

		return res;
		// si type non trouvé, retourne "null";
	}

	/**
	 * Methode castant un NSArray d'Object date en NSArray de String.
	 * 
	 * @param dat
	 *          NSArray d'Object date
	 * @return un NSArray de String
	 */
	public static NSArray datesToString(NSArray dat) // plusieurs objets
	{
		NSMutableArray reponse = new NSMutableArray();
		for (int i = 0; i < dat.count(); i++) {
			Object ob = dat.objectAtIndex(i);
			String res = dateToString(ob);
			reponse.addObject(res);
		}
		return reponse;
	}

	/**
	 * Methode castant un String en Properties.
	 * 
	 * @param chaine
	 *          String (format Properties : "clé1=val1 clé2=val2...")
	 * @return un Properties
	 */
	public static Properties stringToProperties(String chaine) {
		
		// TODO bidouille de la mort : les chaines arrivent en properties ... on retaille :x
		// passer tous les appel http via la classe SPHTTPConnection
		
		
		CktlLog.trace("stringToProperties() chaine="+chaine, true);
		
		if (chaine.startsWith("{")) {
			String prevChaine = chaine;
			// virer le '{' du debut
			chaine = prevChaine.substring(1);
			// virer le '}' de la fin
			chaine = chaine.substring(0,chaine.length()-1);
			// replace les ', ' par un '\n'
			chaine = StringCtrl.replace(chaine, ", ", "\n");
		}
		
		
		if (StringCtrl.containsIgnoreCase(chaine, SPConstantes.PROP_OC_DETAIL)) {
			// cas ou \n a l'interieur d'une valeur -> detail sur plusieurs lignes
			StringTokenizer a = new StringTokenizer(chaine, "\n");
			String prec = "";
			String res = "";
			while (a.hasMoreTokens()) {
				String s = a.nextToken();
				if (StringCtrl.containsIgnoreCase(s, "=")) {
					if (!res.equals("") && !prec.equals(""))
						res = res + "\n" + prec;
					else if (!prec.equals(""))
						res = prec;
					prec = s;
				} else
					// details en plusieurs lignes
					prec = prec + "\\n" + s;
			}
			chaine = res + "\n" + prec;
		}
		
		
		Properties prop = new Properties();
		try {
			//TODO corriger le forcage en iso
			byte[] b = chaine.getBytes("ISO-8859-1");
			//byte[] b = chaine.getBytes("UTF-8");
			//byte[] b = chaine.getBytes();
			if (b != null) {
				ByteArrayInputStream ba = new ByteArrayInputStream(b);
				prop.load(ba);
			}
		} catch (IOException io) {
			CktlLog.trace("Probleme IOException : WOResponse de format incorrect");
		} catch (NullPointerException nn) {
			CktlLog.trace("Probleme NullPointerException : WOResponse de format incorrect");
		}

		return prop;
	}

}
