/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlreport.server.metier.EOCategorie;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class CktlReportCategoriesTbv extends AFwkCktlReportGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	public static final String BDG_categoriesDg = "categoriesDg";
	private static final String CATEGORIE_KEY = "aCategorie";
	public static final String BDG_libelle = "libelle";

	public EOCategorie aCategorie;
	private NSArray<CktlAjaxTableViewColumn> colonnes;

	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

	static {
		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setOrderKeyPath(CATEGORIE_KEY + "." + EOCategorie.CAT_STR_ID_KEY);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(CATEGORIE_KEY + "." + EOCategorie.CAT_STR_ID_KEY, "emptyValue");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, CATEGORIE_KEY);
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.objectForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = new NSArray<String>(CATEGORIE_KEY);
		return keys;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (valueForBinding(BDG_libelle) != null) {
			((CktlAjaxTableViewColumn) _colonnesMap.valueForKey(CATEGORIE_KEY)).setLibelle(stringValueForBinding(BDG_libelle, "Categories"));
		}
		super.appendToResponse(response, context);
	}

	public CktlReportCategoriesTbv(WOContext arg0) {
		super(arg0);
	}

	public WODisplayGroup categoriesDg() {
		return (WODisplayGroup) valueForBinding(BDG_categoriesDg);
	}

}
