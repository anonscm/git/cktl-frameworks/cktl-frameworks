/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EORapport;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Gere l'affichage d'une liste de rapport prets a l'aide d'une tableView.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @binding appUser Obligatoire.
 * @binding rapportsPretsDg WODisplayGroup. Obligatoire
 * @binding isReadOnly
 * @binding rapport (quand non readOnly)
 */
public class RapportPretTbv extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public static final String BDG_rapportsPretsDg = "rapportsDg";
	private static final String RAPPORT_PRET_KEY = "unRapportPret";
	private static final String BINDING_isReadOnly = "isReadOnly";
	private static final String BINDING_rapport = "rapport";

	public EOPersRapportPret unRapportPret;

	private NSArray colonnes;

	public static final NSMutableDictionary _colonnesMap = new NSMutableDictionary();

	static {
		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Rapports sauvegardés");
		col1.setComponent(RapportPretItem.class.getName());
		//col1.setOrderKeyPath(RAPPORT_KEY+".");
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(RAPPORT_PRET_KEY, "emptyValue");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, RAPPORT_PRET_KEY);
	}

	public NSTimestampFormatter dateFormatter() {
		return new NSTimestampFormatter("%d/%m/%Y");
	}

	public NSArray getColonnes() {
		if (colonnes == null) {
			NSMutableArray res = new NSMutableArray();
			NSArray colkeys = getColonnesKeys();
			for (int i = 0; i < colkeys.count(); i++) {
				res.addObject(_colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
			}
			colonnes = res.immutableClone();
		}
		return colonnes;
	}

	public NSArray getColonnesKeys() {
		NSArray keys = new NSArray(RAPPORT_PRET_KEY);

		return keys;
	}

	public RapportPretTbv(WOContext arg0) {
		super(arg0);
	}

	public String containerCriteresId() {
		return getComponentId() + "_crits";
	}

	public String tbvContainerId() {
		return getComponentId() + "_liste";
	}

	public String jsUpdateAfterListUpdate() {
		if (updateContainerID() != null) {
			return "if ($('" + updateContainerID() + "')) {" + updateContainerID() + "Update();};";
		}
		return "";
	}

	public WODisplayGroup rapportsPretsDg() {
		return (WODisplayGroup) valueForBinding(BDG_rapportsPretsDg);
	}

	public String getRapportPretsContainerId() {
		return getComponentId() + "_rapportPrets";
	}

	//	public WOActionResults ajouter() {
	//		EOPersRapportPret persRapportPret = EOPersRapportPret.create(edc(), getAppUser().getPersonne(), getRapport(), null, null, getRapport().rapLibelle(), getAppUser().getCktlReportRapportCriteresGlobal());
	//		rapportsPretsDg().insertObjectAtIndex(persRapportPret, 0);
	//		return null;
	//	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_isReadOnly, Boolean.TRUE);
	}

	public EORapport getRapport() {
		return (EORapport) valueForBinding(BINDING_rapport);

	}

	public Boolean hasRapportsPrets() {
		return Boolean.valueOf(rapportsPretsDg() != null && rapportsPretsDg().displayedObjects().count() > 0);
	}

}
