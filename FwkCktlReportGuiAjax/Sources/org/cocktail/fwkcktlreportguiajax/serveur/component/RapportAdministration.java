/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Composant pour administrer un rapport
 * 
 * @binding rapport
 * @binding onAnnulerAction
 * @binding onEnregistrerAction
 * @binding windowId Si ce composant est affiché dans une fenêtre.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapportAdministration extends AFwkCktlReportGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	public static final String BINDING_rapport = "rapport";
	public static final String BDG_onAnnulerAction = "onAnnulerAction";
	public static final String BDG_onEnregistrerAction = "onEnregistrerAction";
	private static final String BINDING_windowId = "windowId";
	public EOTypeExport unTypeExport;

	private Boolean rapCategoriesReset = Boolean.FALSE;
	private EORapport _rapport;

	public RapportAdministration(WOContext arg0) {
		super(arg0);
	}

	public EORapport getRapport() {
		boolean hasChanged = false;
		EORapport tmp = (EORapport) valueForBinding(BINDING_rapport);
		if (tmp == null) {
			_rapport = tmp;
			hasChanged = true;
			//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
			//resetEdc();
		}
		else {
			if (_rapport == null || _rapport.globalID() == null ||
					!_rapport.globalID().equals(tmp.globalID())) {
				//On reinitialise le nested edc pour tenter de contourner un bug EOF (garbage collector non passé sur les nested).
				//resetEdc();
				if (tmp.editingContext() != null) {
					_rapport = tmp.localInstanceIn(edc());
				}
				else {
					_rapport = null;
				}
				hasChanged = true;
			}
		}
		//		if (hasChanged) {
		//			initialiseRepartTypeGroupeDisplayGroup(_groupe);
		//		}
		return _rapport;
	}

	public Boolean isRapportNotNull() {
		return Boolean.valueOf(getRapport() != null);
	}

	public NSArray getTypeExports() {
		return getRapport().getTypeExportsCache();
	}

	public String getClassTypeExport() {
		return "FwkCktlReportGuiAjax_export_" + unTypeExport.texStrId().toLowerCase() + "_big";
	}

	public WOActionResults onAnnuler() {
		try {
			if (edc().hasChanges()) {
				edc().revert();
			}
			if (hasBinding(BDG_onAnnulerAction)) {
				performParentAction(stringValueForBinding(BDG_onAnnulerAction, ""));
			}
			if (valueForBinding(BINDING_windowId) != null) {
				CktlAjaxWindow.close(context(), (String) valueForBinding(BINDING_windowId));
			}

		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		} finally {
			setRapCategoriesReset(Boolean.TRUE);
		}
		return null;
	}

	public WOActionResults onEnregistrer() {
		try {
			if (edc().hasChanges()) {
				edc().saveChanges();
				setInfoSaisieMessage("Modifications enregistrées");
			}
			if (hasBinding(BDG_onEnregistrerAction)) {
				performParentAction(stringValueForBinding(BDG_onEnregistrerAction, ""));
			}
			if (valueForBinding(BINDING_windowId) != null) {
				CktlAjaxWindow.close(context(), (String) valueForBinding(BINDING_windowId));
			}
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		} finally {
			setRapCategoriesReset(Boolean.TRUE);
		}
		return null;
	}

	public String getRappItemEtatId() {
		return getRapport().rapStrIdBasic() + "_etat";
	}

	public String getJsOnCompleteEnregistrer() {
		return "function(){ if ($('" + getRappItemEtatId() + "')) {" + getRappItemEtatId() + "Update();} " +
				"}";
	}

	public String getContainerActionsId() {
		return getComponentId() + "_actions";
	}

	public Boolean isEnregistrerDisabled() {
		return Boolean.valueOf(!edc().hasChanges());
	}

	public Boolean getRapCategoriesReset() {
		return rapCategoriesReset;
	}

	public void setRapCategoriesReset(Boolean rapCategoriesReset) {
		this.rapCategoriesReset = rapCategoriesReset;
	}

}
