/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureAbstract;
import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gere l'affichage d'un critere pour choix utilisateur
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @binding appUser Obligatoire.
 * @binding rapportCritere Le critere à afficher
 */
public class RapCrit extends AFwkCktlReportGuiAjaxComponent {
	private static final String LOLF_NOMENCLATURE_DEPENSE_STRID = "org.cocktail.cktlreport.critere.gfc.lolfcodedepense";
	private static final String LOLF_NOMENCLATURE_RECETTE_STRID = "org.cocktail.cktlreport.critere.gfc.lolfcoderecette";
	private static final NSArray<String> ORGAN_STRIDS = NSArray.componentsSeparatedByString("org.cocktail.cktlreport.critere.gfc.orgetab,org.cocktail.cktlreport.critere.gfc.orgub,org.cocktail.cktlreport.critere.gfc.orgcr,org.cocktail.cktlreport.critere.gfc.orgsouscr", ",");
	private static final NSArray<String> LOLF_NOMENCLATURE_STRIDS = NSArray.componentsSeparatedByString(LOLF_NOMENCLATURE_DEPENSE_STRID + "," + LOLF_NOMENCLATURE_RECETTE_STRID, ",");
	private static final String CANAL_STRID = "org.cocktail.cktlreport.critere.gfc.canal";
	private static final String FOURNIS_STRID = "org.cocktail.cktlreport.critere.gfc.foucode";

	private static final long serialVersionUID = 1L;
	public final static String BINDING_rapportCritere = "rapportCritere";

	//public final static String BINDING_criteresDico = "criteresDico";

	//private Object value;

	//private RapCritDelegatePopup popupDelegate;

	//	private NSDictionary unPopupItemSelection;

	public RapCrit(WOContext context) {
		super(context);
	}

	public EORapportCritere getRapportCritere() {
		return (EORapportCritere) valueForBinding(BINDING_rapportCritere);
	}

	public Boolean isComposant() {
		return getRapportCritere().toCritere().isComposant();
	}

	public Object convertParamValueToDisplayValue(Object value) {
		return value;
	}

	public Object convertDisplayValueToParamValueTo(Object value) {
		return value;
	}

	public Object getValue() {
		Object tmp = rapportCriteresCache().get(getCle());
		if (isDate()) {
			if (tmp == null) {
				setValue(new NSTimestamp(MyDateCtrl.getLastDayOfYear(DateCtrl.getCurrentYear())));
			}
		}
		return convertParamValueToDisplayValue(tmp);
	}

	public void setValue(Object value) {
		//this.value = value;
		//getCriteresDico().takeValueForKey(value, getCle());
		rapportCriteresCache().put(getCle(), value);
	}

	public String getCle() {
		return getRapportCritere().getCle();
	}

	public Boolean isObligatoire() {
		return getRapportCritere().isObligatoire();
	}

	public String getLibelleClass() {
		String res = "FwkCktlReportGuiAjax_RapExec_RapCrit_Libelle";
		if (isObligatoire().booleanValue()) {
			res = res + "_obligatoire";
		}
		return res;
	}

	public CktlReportRapportCriteres rapportCriteresCache() {
		//return getAppUser().getCktlReportCriteresDic().get(getRapportCritere().toRapport().rapStrId());
		return getAppUser().getCktlReportRapportCriteresGlobal();
	}

	public String getCompId() {
		return getComponentId() + "_" + StringCtrl.toBasicString(getRapportCritere().getCle(), null, '_');
	}

	public String getChampClass() {
		String res = "FwkCktlReportGuiAjax_RapExec_RapCrit_champ";
		if (isObligatoire().booleanValue()) {
			res = res + "_obligatoire";
		}
		return res;
	}

	public Boolean isDisabled() {
		Boolean res = Boolean.FALSE;
		return res;
	}

	public Boolean isOrganSelect() {
		return ORGAN_STRIDS.contains(getRapportCritere().toCritere().critStrId());
	}

	public Boolean isCanalSelect() {
		return CANAL_STRID.equals(getRapportCritere().toCritere().critStrId());
	}

	public Boolean isLolfNomenclatureSelect() {
		return LOLF_NOMENCLATURE_STRIDS.contains(getRapportCritere().toCritere().critStrId());
	}

	public Boolean isFournisSelect() {
		return FOURNIS_STRID.equals(getRapportCritere().toCritere().critStrId());
	}

	public Boolean isDate() {
		return getRapportCritere().toCritere().isValeurDate();
	}

	/**
	 * @return La requete sql contenue dans critMetadat si uen telle requete est detectee.
	 */
	public String getMetadataSql() {
		String sql = StringCtrl.normalize(getRapportCritere().toCritere().critMetadata());
		sql = sql.toUpperCase();
		if (sql.startsWith("SELECT ")) {
			return sql;
		}
		return null;
	}

	public String getLolNomenclatureMode() {
		if (LOLF_NOMENCLATURE_DEPENSE_STRID.equals(getRapportCritere().toCritere().critStrId())) {
			return EOLolfNomenclatureAbstract.MODE_DEPENSE;
		}
		else if (LOLF_NOMENCLATURE_RECETTE_STRID.equals(getRapportCritere().toCritere().critStrId())) {
			return EOLolfNomenclatureAbstract.MODE_RECETTE;
		}
		return null;
	}

	//	public IRapCritDelegate getDelegate() {
	//		//		if (getDelegatePopup() != null) {
	//		//			return getDelegatePopup();
	//		//		}
	//		if (getDelegateOrganSelect() != null) {
	//			return getDelegateOrganSelect();
	//		}
	//		if (getDelegateSelectDialog() != null) {
	//			return getDelegateSelectDialog();
	//		}
	//		return null;
	//	}

	//	public RapCritDelegatePopup getDelegatePopup() {
	//		if (getRapportCritere().toCritere().isPopup()) {
	//			if (popupDelegate == null) {
	//				popupDelegate = new RapCritDelegatePopup(this);
	//			}
	//		}
	//		else {
	//			popupDelegate = null;
	//		}
	//		return popupDelegate;
	//	}
	//
	//	public RapCritDelegateOrganSelect getDelegateOrganSelect() {
	//		if (isOrganSelect()) {
	//			if (delegateOrganSelect == null) {
	//				delegateOrganSelect = new RapCritDelegateOrganSelect(this);
	//			}
	//		}
	//		else {
	//			delegateOrganSelect = null;
	//		}
	//		return delegateOrganSelect;
	//	}
	//
	//	public RapCritDelegateSelectDialog getDelegateSelectDialog() {
	//		if ((getRapportCritere().toCritere().isDialog() || getRapportCritere().toCritere().isLibreOuLibreEtDialog())) {
	//			if (delegateSelectDialog == null) {
	//				delegateSelectDialog = new RapCritDelegateSelectDialog(this);
	//			}
	//		}
	//		else {
	//			delegateSelectDialog = null;
	//		}
	//		return delegateSelectDialog;
	//	}
}