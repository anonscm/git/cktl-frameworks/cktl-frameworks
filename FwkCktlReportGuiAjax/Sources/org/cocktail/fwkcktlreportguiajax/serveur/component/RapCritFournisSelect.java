/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOIndividuForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

public class RapCritFournisSelect extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;

	public final static String BINDING_rapportCritere = "rapportCritere";

	private IPersonne selectedInTree;
	public String tmpValue;

	public RapCritFournisSelect(WOContext arg0) {
		super(arg0);
	}

	public Object convertParamValueToDisplayValue(Object value) {
		return value;
	}

	public Object convertDisplayValueToParamValueTo(Object value) {
		return value;
	}

	public IPersonne getSelectedInTree() {
		return selectedInTree;
	}

	public void setSelectedInTree(IPersonne selectedInTree) {
		this.selectedInTree = selectedInTree;
		if (selectedInTree != null && selectedInTree.toFournis() != null) {
			this.rapportCriteresCache().put(getRapportCritere().getCle(), selectedInTree.toFournis().fouCode());
		}
		else {
			this.rapportCriteresCache().put(getRapportCritere().getCle(), null);

		}

	}

	public CktlReportRapportCriteres rapportCriteresCache() {
		//return getAppUser().getCktlReportCriteresDic().get(getRapportCritere().toRapport().rapStrId());
		return getAppUser().getCktlReportRapportCriteresGlobal();
	}

	public EORapportCritere getRapportCritere() {
		return (EORapportCritere) valueForBinding(BINDING_rapportCritere);
	}

	public EOQualifier getQualifierForStructures() {
		return EOStructureForFournisseurSpec.QUAL_STRUCTURES_IN_GROUPE_TYPE_FOURNISSEUR;
	}

	public EOQualifier getQualifierForIndividus() {
		return EOIndividuForFournisseurSpec.QUAL_INDIVIDUS_IN_GROUPE_TYPE_FOURNISSEUR;
	}

}
