/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import java.util.Iterator;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.CktlReportSqlCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Composant qui affiche un bouton de recherche et une fenêtre de recherche/selection. Une tableview est construite à partir d'une requete sql. Les
 * colonnes sont détectées en fonction des champs du select.
 * 
 * @binding title Titre de la fenêtre
 * @binding onSelectionner Action a executer lorsque la selection dans la tableView change
 * @binding selection Obligatoire Selection validée par l'utilisateur
 * @binding sql Obligatoire Requete sql qui produit les résultats
 * @binding displayGroup Obligatoire
 * @binding maxRowCount Nombre de ligne maximum à renvoyer
 * @binding keyForValue La selection renvoie un NSDictionary. Si vous specifiez la cle, value sera rempli avec la veleur correspondante de la
 *          selection.
 * @binding value
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportSelectDialog extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BDG_displayGroup = "displayGroup";
	private static final String BDG_title = "title";
	private static final String BDG_onSelectionner = "onSelectionner";
	private static final String BDG_selection = "selection";
	private static final String BDG_sql = "sql";
	private static final String BDG_maxRowCount = "maxRowCount";

	public static final Integer DEFAUT_maxRowCount = Integer.valueOf(100);
	private static final String BDG_keyForValue = "keyForValue";
	private static final String BDG_value = "value";

	public NSDictionary<String, Object> currentObject;
	private String filtre;
	private NSArray<CktlAjaxTableViewColumn> colonnes;
	private CktlReportSqlCtrl sqlCtrl;
	private String lastSql;
	private String infos;

	public CktlReportSelectDialog(WOContext context) {
		super(context);
	}

	public String modalWindowId() {
		return getComponentId() + "win";
	}

	public String getContainerWindowId() {
		return getComponentId() + "winContainer";
	}

	public String filtreButtonId() {
		return getComponentId() + "filtrebt";
	}

	public String tbvId() {
		return getComponentId() + "tbv";
	}

	public WODisplayGroup displayGroup() {
		return (WODisplayGroup) valueForBinding(BDG_displayGroup);
	}

	/**
	 * @return the filtre
	 */
	public String getFiltre() {
		return filtre;
	}

	/**
	 * @param filtre the filtre to set
	 */
	public void setFiltre(String filtre) {
		this.filtre = filtre;
	}

	public WOActionResults filtrer() {
		NSArray<NSDictionary<String, Object>> res = null;
		if (!StringCtrl.isEmpty(getFiltre())) {
			String filtre = getFiltre().trim();
			filtre = filtre.replaceAll("%", "");
			filtre = filtre.replaceAll("\\*", "");
			String whereClause1 = getSqlCtrl().buildWhereClauseFromFields(filtre, "=", "or", true);
			String whereClause2 = getSqlCtrl().buildWhereClauseFromFields(filtre + "%", "like", "or", true);
			String whereClause3 = getSqlCtrl().buildWhereClauseFromFields("%" + filtre + "%", "like", "or", true);

			NSMutableArray<NSDictionary<String, Object>> res1 = new NSMutableArray<NSDictionary<String, Object>>();
			String newSql = "select * from (select * from (" + getSqlCtrl().getSql() + " ) where " + whereClause1 + ") where rownum <=" + getMaxRowCount().intValue();
			CktlReportSqlCtrl _cktCktlReportSqlCtrl = new CktlReportSqlCtrl(newSql);
			res1.addObjectsFromArray(_cktCktlReportSqlCtrl.rawRowsForSQL(edc(), null, getAppUser().getCktlReportRapportCriteresGlobal()));
			if (res1.count() < getMaxRowCount().intValue()) {
				newSql = "select * from (select * from (" + getSqlCtrl().getSql() + " ) where " + whereClause2 + ") where rownum <=" + (getMaxRowCount().intValue() - res1.count());
				_cktCktlReportSqlCtrl.setSql(newSql);
				res1.addObjectsFromArray(_cktCktlReportSqlCtrl.rawRowsForSQL(edc(), null, getAppUser().getCktlReportRapportCriteresGlobal()));
				if (res1.count() < getMaxRowCount().intValue()) {
					newSql = "select * from (select * from (" + getSqlCtrl().getSql() + " ) where " + whereClause3 + ") where rownum <=" + (getMaxRowCount().intValue() - res1.count());
					_cktCktlReportSqlCtrl.setSql(newSql);
					res1.addObjectsFromArray(_cktCktlReportSqlCtrl.rawRowsForSQL(edc(), null, getAppUser().getCktlReportRapportCriteresGlobal()));
				}
			}
			res = NSArrayCtrl.getDistinctsOfNSArray(res1.immutableClone());
		}
		else {
			String newSql = "select * from (" + getSqlCtrl().getSql() + " )  where rownum <=" + getMaxRowCount().intValue();
			CktlReportSqlCtrl _cktCktlReportSqlCtrl = new CktlReportSqlCtrl(newSql);
			res = _cktCktlReportSqlCtrl.rawRowsForSQL(edc(), null, getAppUser().getCktlReportRapportCriteresGlobal());
		}
		if (res.count() >= getMaxRowCount().intValue() - 10) {
			setInfos("Seuls les " + res.count() + " premiers résultats sont affichés. Affinez votre recherche pour plus de pertinence dans les résultats");
		}
		else {
			setInfos(null);
		}
		displayGroup().setObjectArray(res);
		return null;
	}

	public WOActionResults afficherTous() {
		return null;
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
			NSArray<String> res = getSqlCtrl().getFields();
			Iterator<String> it = res.iterator();
			while (it.hasNext()) {
				String field = (String) it.next();

				// Colonne Libelle
				CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
				col.setLibelle(field);
				col.setOrderKeyPath(field);
				String keyPath = ERXQ.keyPath("currentObject", field.toUpperCase());
				CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(
						keyPath, "");
				col.setAssociations(ass);
				colTmp.add(col);
			}

			colonnes = colTmp.immutableClone();
		}
		return colonnes;
	}

	public String getGetTitle() {
		return stringValueForBinding(BDG_title, "Sélectionnez...");
	}

	public String getSelectionner() {
		return (String) valueForBinding(BDG_onSelectionner);
	}

	public String getSql() {
		return (String) valueForBinding(BDG_sql);
	}

	@SuppressWarnings("unchecked")
	public WOActionResults valider() {
		setValueForBinding(displayGroup().selectedObject(), BDG_selection);
		if (stringValueForBinding(BDG_keyForValue, null) != null) {
			setValueForBinding(((NSDictionary<String, Object>) displayGroup().selectedObject()).valueForKey(stringValueForBinding(BDG_keyForValue, null)), BDG_value);
		}
		CktlAjaxWindow.close(context(), modalWindowId());
		return null;
	}

	public CktlReportSqlCtrl getSqlCtrl() {
		if (sqlCtrl == null || lastSql == null || !lastSql.equals(getSql())) {
			sqlCtrl = new CktlReportSqlCtrl(getSql());
			lastSql = getSql();
		}
		return sqlCtrl;
	}

	public Integer getMaxRowCount() {
		if (hasBinding(BDG_maxRowCount) && valueForBinding(BDG_maxRowCount) != null) {
			return (Integer) valueForBinding(BDG_maxRowCount);
		}
		return DEFAUT_maxRowCount;
	}

	public String filtreId() {
		return getComponentId() + "filtre";
	}

	public Boolean isValiderDisabled() {
		return displayGroup().selectedObject() == null;
		//return Boolean.FALSE;
	}

	public String getContainerActionId() {
		return getComponentId() + "_actions";
	}

	/**
	 * @return the infos
	 */
	public String getInfos() {
		return infos;
	}

	/**
	 * @param infos the infos to set
	 */
	public void setInfos(String infos) {
		this.infos = infos;
	}

	public Boolean hasInfos() {
		return !MyStringCtrl.isEmpty(getInfos());
	}

}