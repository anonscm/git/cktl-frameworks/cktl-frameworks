/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.CktlReportHistoryManagement;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;
import org.cocktail.fwkcktlreportguiajax.serveur.CktlReportMailMessage;
import org.cocktail.fwkcktlreportguiajax.serveur.controleur.RapExecCtrl;
import org.cocktail.fwkcktlreportguiajax.serveur.exec.AExecFactory;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.ICktlReportingTaskListener;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

/**
 * Gere l'affichage de l'execution d'un rapport (inclue de composant de choix des criteres).
 * 
 * @binding appUser Obligatoire.
 * @binding rapport Rapport pour lequel aficher les critères
 * @binding updateContainerID
 * @binding goSauvegardeAction nom de l'action a executer lorsqu'on veut aller sur l'onglet des sauvegardes
 * @binding reset
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapExec extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	public final static String BINDING_rapportPret = "rapportPret";
	public final static String BINDING_rapport = "rapport";
	public final static String BINDING_criteresDico = "criteresDico";
	public final static String BINDING_reset = "reset";
	private static final String BINDING_goSauvegardeAction = "goSauvegardeAction";

	public EOTypeExport unTypeExport;
	public EOTypeExport unTypeExport2;
	public IRapportExt unRapportExt;
	private IRapportExt selectedRapportExt;
	private EOTypeExport selectedTypeExport;
	private RapExecCtrl ctrl;

	private Boolean emailChecked;
	private CktlReportMailMessage cktlMailMessage;
	private CktlAbstractReporter reporter;

	private EORapport lastRapport;

	//private Boolean showPlanification = Boolean.FALSE;

	public RapExec(WOContext context) {
		super(context);
		emailChecked = Boolean.FALSE;
		ctrl = new RapExecCtrl(this);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public void resetComp() {
		if (getReporter() != null && getReporter().getCurrentReportingTaskThread() != null) {
			getReporter().getCurrentReportingTaskThread().interrupt();
		}
		setReporter(null);
		//showPlanification = Boolean.FALSE;
		if (edc().hasChanges()) {
			edc().revert();
		}
	}

	public Boolean isRapportNotNull() {
		return Boolean.valueOf(getRapport() != null);
	}

	public WOActionResults onExecRapport() {
		try {
			if (edc().hasChanges()) {
				edc().revert();
			}
			reporter = null;
			setSelectedRapportExt(unRapportExt);
			setSelectedTypeExport(unTypeExport2);
			Integer rapHisto = CktlReportHistoryManagement.startHistory(edc(), getRapport(), getAppUser().getPersonne(), unTypeExport2);
			try {
				LocalReportingTaskListener localReportingTaskListener = new LocalReportingTaskListener(rapHisto);
				reporter = ctrl.lanceGenerationRapport(getSelectedRapportExt(), getSelectedTypeExport(), new NSArray<ICktlReportingTaskListener>(localReportingTaskListener));
				reporter.setResultMimeType(unTypeExport2.getMimeType());
				reporter.setResultFileName(AExecFactory.genererFileName(getRapport(), unTypeExport2));
			} catch (Throwable e) {
				try {
					CktlReportHistoryManagement.finishHistory(edc(), rapHisto, EOPersRapportHisto.STATUS_ERREUR, e.getLocalizedMessage() + (e.getCause() == null ? "" : " / " + e.getCause().getLocalizedMessage()));
				} catch (Exception e2) {
					setInfoSaisieMessage("Une erreur non bloquante est survenue lors de l'enregistrement de l'historique : " + e2.getLocalizedMessage());
				}
				if (e instanceof NullPointerException) {
					throw new Exception("NullPointerException, consultez le log.");
				}
				setErreurSaisieMessage(e.getLocalizedMessage());
			} finally {

			}
		} catch (Exception e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onRapportNoFavorite() {
		try {
			getRapport().deleteFromFavoris(getAppUser().getPersId());
			if (edc().hasChanges()) {
				edc().saveChanges();
				setInfoSaisieMessage("Le rapport a été supprimé de vos favoris");
			}
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onRapportFavorite() {
		try {
			getRapport().addToFavoris(getAppUser().getPersId());
			if (edc().hasChanges()) {
				edc().saveChanges();
				setInfoSaisieMessage("Le rapport a été ajouté à vos favoris");
			}
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onRapportSauverCriteres() {
		//showPlanification = !showPlanification;
		try {
			if (edc().hasChanges()) {
				edc().revert();
			}
			EOPersRapportPret persRapportPret = EOPersRapportPret.create(edc(), getAppUser().getPersonne(), getRapport(), getSelectedTypeExport(), (getSelectedRapportExt() != null ? getSelectedRapportExt().toTypeFormatRapport() : null), getRapport().rapLibelle(), getAppUser()
					.getCktlReportRapportCriteresGlobal());
			edc().saveChanges();
			setValueForBinding(persRapportPret, BINDING_rapportPret);
			setInfoSaisieMessage("Les critères du rapport ont été enregistrés.");
			performParentAction((String) valueForBinding(BINDING_goSauvegardeAction));
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public EORapport getRapport() {
		//return (EORapport) valueForBinding(BINDING_rapport);
		EORapport res = (EORapport) valueForBinding(BINDING_rapport);
		if (res != null && (lastRapport == null || !(lastRapport.equals(res)))) {
			lastRapport = res;
			resetComp();
		}
		return res;
	}

	public EOPersRapportPret getRapportPret() {
		return (EOPersRapportPret) valueForBinding(BINDING_rapportPret);
	}

	public String containerActionsId() {
		return getComponentId() + "_actions";
	}

	public String containerCriteresId() {
		return getComponentId() + "_criteres";
	}

	public String containerTypeExportId() {
		return getComponentId() + "_typeExport";
	}

	public String JSUpdateContainer() {
		return (updateContainerID() != null ? updateContainerID() + "Update();" : "");
	}

	public String getRappItemFavorisId() {
		return getRapport().rapStrIdBasic() + "_favoris";
	}

	/**
	 * @return Commande javascript pour rafraichir le flag favoris d'un eventuel item représentant le rapport dans une liste
	 */
	public String getJsOnCompleteFavorites() {
		return "function(){ if ($('" + getRappItemFavorisId() + "')) {" + getRappItemFavorisId() + "Update();}    " +
				"}";
	}

	public NSArray<EOTypeExport> getTypeExports() {
		return getRapport().getTypeExportsCache();
	}

	public String getClassTypeExport() {
		return "FwkCktlReportGuiAjax_export_" + unTypeExport.texStrId().toLowerCase() + "_big";
	}

	public Boolean showTypesExport() {
		return Boolean.valueOf(getTypeExports().count() > 1);
	}

	/**
	 * @return the selectedTypeExport
	 */
	public EOTypeExport getSelectedTypeExport() {
		return selectedTypeExport;
	}

	/**
	 * @param selectedTypeExport the selectedTypeExport to set
	 */
	public void setSelectedTypeExport(EOTypeExport selectedTypeExport) {
		this.selectedTypeExport = selectedTypeExport;
	}

	public String getGetClassForTypeExport2() {
		return "FwkCktlReportGuiAjax_export2_" + unTypeExport2.texStrId().toLowerCase() + "_big";
	}

	public String getGetExecActionType() {
		return unTypeExport2.texStrId().toLowerCase();
	}

	public String getGetExecActionText() {
		if (isEmailChecked())
			return "Envoyer";
		return "Générer";
	}

	public String getExecActionTitle() {
		return getGetExecActionText() + " le rapport " + "(" + unTypeExport2.texLibelle() + " via " + unRapportExt.toTypeFormatRapport().tfrLibelle() + ")";
	}

	public RapExecCtrl getCtrl() {
		return ctrl;
	}

	public void setCtrl(RapExecCtrl ctrl) {
		this.ctrl = ctrl;
	}

	public Boolean isEmailChecked() {
		return emailChecked;
	}

	public void setEmailChecked(Boolean emailChecked) {
		this.emailChecked = emailChecked;
		if (emailChecked) {
			cktlMailMessage = new CktlReportMailMessage();
			if (getAppUser().getEmails().count() > 0) {
				cktlMailMessage.addFroms((String) getAppUser().getEmails().objectAtIndex(0));
			}

			//FIXME ajouter en TO l'utilisateur (si la liste des destinataires est vide)

			//FIXME Mémoriser dans des preferences les destinataires fréquemment utilisés (attendre version future)

			cktlMailMessage.setSubject("Envoi d'un rapport : ");
			cktlMailMessage.setBody("Ce message a été généré automatiquement");
		}
	}

	public String getEmailCheckId() {
		return getComponentId() + "_emailCheck";
	}

	public String getJsToOpenSendReportWindow() {
		return "if ( " + getEmailCheckId() + ".checked) { openCAW_" + getSendReportWindowId() + "('Envoi du rapport par Email');} return true;";
	}

	public String getSendReportWindowId() {
		return getComponentId() + "_sendReportWindow";
	}

	public Boolean getEmailChecked() {
		return emailChecked;
	}

	public WOActionResults onSendRapportAnnuler() {
		return null;
	}

	public WOActionResults onSendRapportValider() {
		return null;
	}

	public CktlReportMailMessage cktlMailMessage() {
		return cktlMailMessage;
	}

	public IRapportExt getSelectedRapportExt() {
		return selectedRapportExt;
	}

	public void setSelectedRapportExt(IRapportExt selectedRapportExt) {
		this.selectedRapportExt = selectedRapportExt;
	}

	public String containerDestinataireId() {
		return getComponentId() + "_destContainer";
	}

	public CktlAbstractReporter getAbstractReporter() {
		return reporter;
	}

	public String getResultFileName() {
		return getAbstractReporter().getResultFileName();
	}

	public String getResultMimeType() {
		return getAbstractReporter().getResultMimeType();
	}

	public String getMonitorId() {
		return getComponentId() + "_monitor";
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public void setReporter(CktlAbstractReporter reporter) {
		this.reporter = reporter;
	}

	public String getExecResultContainerId() {
		return getComponentId() + "_execResult";
	}

	private final class LocalReportingTaskListener implements ICktlReportingTaskListener {
		private Integer _rapportHisto;

		public LocalReportingTaskListener(Integer rapportHisto) {
			this._rapportHisto = rapportHisto;
		}

		public void afterReportStarted() {
		}

		public void afterReportBuild(int pageTotalCount) {
		}

		public void afterPageExport(int pageNum, int pageCount) {
		}

		public synchronized void afterReportExport() {
		}

		public synchronized void afterReportFinish() {
			if (cktlMailMessage != null && isEmailChecked()) {
				try {
					ctrl.prepareAndSendMail(cktlMailMessage, getSelectedRapportExt());
				} catch (Throwable e1) {
					e1.printStackTrace();
					afterException(e1);
				}
			}
			try {
				CktlReportHistoryManagement.finishHistory(edc(), _rapportHisto, EOPersRapportHisto.STATUS_OK, null);
			} catch (Exception e2) {
				setInfoSaisieMessage("Une erreur non bloquante est survenue lors de l'enregistrement de l'historique : " + e2.getLocalizedMessage());
			}

		}

		public void afterDataSourceCreated() {
		}

		public void trace(String s) {
		}

		public void afterException(Throwable e) {
			try {
				CktlReportHistoryManagement.finishHistory(edc(), _rapportHisto, EOPersRapportHisto.STATUS_ERREUR, e.getLocalizedMessage());
			} catch (Exception e2) {
				setInfoSaisieMessage("Une erreur non bloquante est survenue lors de l'enregistrement de l'historique : " + e2.getLocalizedMessage());
			}
		}

		public void afterReportCanceled() {
			try {
				CktlReportHistoryManagement.finishHistory(edc(), _rapportHisto, EOPersRapportHisto.STATUS_ANNULE, "Annulé par l'utilisateur");
			} catch (Exception e2) {
				setInfoSaisieMessage("Une erreur non bloquante est survenue lors de l'enregistrement de l'historique : " + e2.getLocalizedMessage());
			}
		}

	}

	public Boolean getTabPlanification() {
		return (Boolean) valueForBinding("tabPlanification");
	}

	public Boolean getShowPlanification() {
		return (Boolean) valueForBinding("tabPlanification");
	}

	//	public void setShowPlanification(Boolean showPlanification) {
	//		this.showPlanification = showPlanification;
	//	}

	public WOActionResults onMonitorRefreshAction() {
		try {
			//permet d'enregistrer les messages prh_histo dans le thread du composant.
			if (edc().hasChanges()) {
				edc().saveChanges();
			}
		} catch (Exception e) {
			e.printStackTrace();
			setInfoSaisieMessage("Une erreur non bloquante est survenue lors de l'enregistrement de l'historique : " + e.getLocalizedMessage());
		}
		return null;
	}

	//	public WOActionResults supprimer() {
	//		EOPersRapportPret.supprimer(getRapportPret());
	//		// edc().saveChanges();
	//		return null;
	//	}

	public WOActionResults actionRefresh() {
		if (hasBinding("actionRefresh")) {
			return performParentAction(stringValueForBinding("actionRefresh", null));
		}
		return null;
	}

	public String getOnClickBeforeExec() {
		return "function(oc) {" + getMonitorId() + "Update(); return true;}";
	}

	public Boolean showRapportPret() {
		return Boolean.valueOf(getRapportPret() != null && getRapportPret().toRapport() != null && getRapportPret().toRapport().globalID().equals(getRapport().globalID()) && !getRapportPret().isDeletedEO());
	}

}