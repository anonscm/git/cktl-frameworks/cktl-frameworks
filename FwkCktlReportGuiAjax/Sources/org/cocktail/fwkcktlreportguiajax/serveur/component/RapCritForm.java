/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EOCritere;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Gestion de l'affectation d'un critere à un rapport.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class RapCritForm extends AFwkCktlReportGuiAjaxComponent {
	private static final long serialVersionUID = 1L;
	public final static String BINDING_rapportCritere = "rapportCritere";

	//public String rapportCritereRcrLibelle;

	public EOCritere item;

	public RapCritForm(WOContext arg0) {
		super(arg0);
	}

	public EORapportCritere getRapportCritere() {
		return (EORapportCritere) valueForBinding(BINDING_rapportCritere);
	}

	public String getLabelClass() {
		return "FwkCktlReportGuiAjax_RapCritForm_Label";
	}

	public String getFieldClass() {
		return "FwkCktlReportGuiAjax_RapCritForm_Field";
	}

	public String getFieldLibelleId() {
		return getComponentId() + "fLibelle";
	}

	public String getFieldTypeId() {
		return getComponentId() + "fType";
	}

	public String getFieldObligatoireId() {
		return getComponentId() + "fObligatoire";
	}

	public String getFieldCleId() {
		return getComponentId() + "fcle";
	}

	public String getFieldCommentaireId() {
		return getComponentId() + "fcommentaire";
	}

	public NSArray<EOCritere> getCriteresPossibles() {
		return EOCritere.fetchAll(edc(), new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EOCritere.SORT_CRIT_STR_ID
		}));
	}

	public WOActionResults onGoUp() {
		try {
			getRapportCritere().goUp();
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onGoDown() {
		try {
			getRapportCritere().goDown();
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onAdd() {
		try {
			getRapportCritere().toRapport().createToRapportCriteresRelationship();
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public WOActionResults onDelete() {
		try {
			getRapportCritere().toRapport().deleteToRapportCriteresRelationship(getRapportCritere());
		} catch (Exception e) {
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	public Boolean isGoUpDisabled() {
		return Boolean.valueOf(getRapportCritere().rcrPosition().intValue() <= 1);
	}

	public Boolean isGoDownDisabled() {
		return Boolean.valueOf(getRapportCritere().rcrPosition().intValue() >= getRapportCritere().toRapport().toRapportCriteres().count());
	}

	public Boolean isAddDisabled() {
		return Boolean.valueOf(getRapportCritere().rcrPosition().intValue() != getRapportCritere().toRapport().toRapportCriteres().count());
	}

	public Boolean isDeleteDisabled() {
		return Boolean.FALSE;
	}

}
