/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportguiajax.serveur.component;

import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;

/**
 * Gere l'affichage d'une liste de rapport prets a l'aide d'une tableView.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 * @binding appUser Obligatoire.
 * @binding rapportsPretsDg WODisplayGroup. Obligatoire
 * @binding isReadOnly
 * @binding rapport (quand non readOnly)
 * @binding actionRefresh
 */
public class RapportPlanificationTbv extends AFwkCktlReportGuiAjaxComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_isReadOnly = "isReadOnly";
	private static final String BINDING_rapport = "rapport";
	private static final String BINDING_rapportPret = "rapportPret";
	private static final String BINDING_ACTION_REFRESH = "actionRefresh";

	private EOPersRapportPret rapportPret;
	private IRapportExt selectedRapportExt;
	private EOTypeExport selectedTypeExport;
	public IRapportExt unRapportExt;
	public EOTypeExport unTypeExport;
	public EOPersRapportPretAbo unPretAbo;

	private WODisplayGroup rapportsAbosExecDg = new WODisplayGroup();

	public WODisplayGroup getRapportsAbosExecDg() {
		if (getRapportPret() != null) {
			rapportsAbosExecDg.setObjectArray(getRapportPret().toPersRapportPretAbos());
			return rapportsAbosExecDg;
		}
		return null;
	}

	public void setRapportsAbosExecDg(WODisplayGroup rapportsAbossExecDg) {
		this.rapportsAbosExecDg = rapportsAbossExecDg;
	}

	public RapportPlanificationTbv(WOContext arg0) {
		super(arg0);
	}

	public String getRapportPretsContainerId() {
		return getComponentId() + "_rapportPrets";
	}

	public WOActionResults ajouter() {
		EOPersRapportPretAbo persRapportPretAbo = EOPersRapportPretAbo.creerInstance(edc());
		persRapportPretAbo.setToPersRapportPretRelationship(getRapportPret());
		//rapportsAbosExecDg.insertObjectAtIndex(persRapportPretAbo, 0);
		// getRapportPret().toPersRapportPretAbos().add(persRapportPretAbo);
		// rapportPret = EOPersRapportPret.create(edc(), getAppUser().getPersonne(), getRapport(), null, null, getRapport().rapLibelle(), getAppUser().getCktlReportRapportCriteresGlobal());
		return null;
	}

	public WOActionResults supprimer() {
		EOPersRapportPret.supprimer(getRapportPret());
		edc().saveChanges();
		if (hasBinding("actionRefresh")) {
			performParentAction(stringValueForBinding("actionRefresh", null));
		}
		return null;
	}

	public Boolean isDisabled() {
		return isReadOnly();
	}

	public WOActionResults updateCriteresSauvegardes() {
		try {

			//	setSelectedTypeExport(unTypeExport);
			getRapportPret().deleteAllToPersRapportPretCritsRelationships();
			getRapportPret().addToToPersRapportPretCritsRelationship(getAppUser().getCktlReportRapportCriteresGlobal());

			edc().saveChanges();
			setInfoSaisieMessage("Les critères ont été mis à jour");

		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;
	}

	//	
	//	public WOActionResults valider() {
	//		try {
	//			
	//			// TODO : voir pourquoi !
	//			setSelectedTypeExport(unTypeExport);
	//			
	//			// On recup les criteres du rapport 
	//			CktlReportRapportCriteres criteres = CktlReportRapportCriteres.createFrom(null, super.getAppUser().getCktlReportRapportCriteresGlobal());
	//			
	//			// On reconstruit les criteres du rapport pret
	//			CktlReportRapportCriteres criteres_ori = new CktlReportRapportCriteres();
	//			for (Object element : getRapportPret().toPersRapportPretCrits().toArray()) {
	//				EOPersRapportPretCrit elt = (EOPersRapportPretCrit) element;
	//				String key = elt.toRapportCritere().toCritere().critCle();
	//				Object value = elt.toRapportCritere().toCritere().convertStringToValeur(elt.prpcCritValeur());
	//				if (key.equals("CA_EXER")) {
	//					if (!(value instanceof Integer)) {
	//						value = Integer.valueOf(value.toString());
	//					}
	//				}
	//				criteres_ori.put(key, value);
	//			}
	//			
	//			// Si le rapport a des criteres mais n'en avait pas, on cree tout
	//			if (!criteres.isEmpty() && criteres_ori.isEmpty()) {
	//				//On ne conserve que les criteres propres au rapport
	//				criteres.cleanCriteres(getRapportPret().toRapport());
	//				
	//				// On parcourt les criteres du rapport
	//				NSArray cles = criteres.allKeys();
	//				for (int i = 0; i < cles.count(); i++) {
	//					Object key = cles.get(i);
	//					Object object = criteres.objectForKey(key);
	//					
	//					// On rattache le bon EORapportCritere
	//					EOCritere eoCrit = EOCritere.fetchByKeyValue(edc(), "critCle", key.toString());
	//					
	//					NSDictionary keys = EOUtilities.primaryKeyForObject(edc(), getRapportPret().toRapport());
	//					Object rapId = keys.objectForKey("rapId");
	//					
	//					keys = EOUtilities.primaryKeyForObject(edc(), eoCrit);
	//					Object critId = keys.objectForKey("critId");
	//					
	//					NSMutableArray lesQualifiers = new NSMutableArray();
	//					lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("critId =%@", new NSArray(critId)));
	//					lesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("rapId =%@", new NSArray(rapId)));
	//					
	//					EORapportCritere rCrit = EORapportCritere.fetchByQualifier(edc(), new EOAndQualifier(lesQualifiers));
	//					
	//					// On cree le EOPersRapportPretCrit et on l'insert
	//					EOPersRapportPretCrit crits = EOPersRapportPretCrit.creerInstance(getRapportPret().editingContext());
	//					crits.setToRapportCritereRelationship(rCrit);
	//					crits.setPrpcCritValeur(object.toString());
	//					
	//					getRapportPret().addToToPersRapportPretCritsRelationship(crits);
	//				}
	//				edc().saveChanges();
	//				setInfoSaisieMessage("Le rapport a été préconfiguré");
	//			}
	//			else if (!criteres_ori.equals(criteres)) { // des criteres ont change ; on va les mettre a jour
	//				//On ne conserve que les criteres propres au rapport
	//				criteres.cleanCriteres(getRapportPret().toRapport());
	//				
	//				// On parcourt les criteres du rapport pret pour les mettre a jour
	//				for (Object element : getRapportPret().toPersRapportPretCrits()) {
	//					EOPersRapportPretCrit elt = (EOPersRapportPretCrit) element;
	//					String key = elt.toRapportCritere().toCritere().critCle();
	//					elt.setPrpcCritValeur(criteres.getValue(key).toString());
	//				}
	//				
	//				edc().saveChanges();
	//				setInfoSaisieMessage("Le rapport a été préconfiguré");
	//			}
	//			if (criteres_ori.equals(criteres) && edc().hasChanges()) {
	//				edc().saveChanges();
	//				setInfoSaisieMessage("Le rapport a été préconfiguré");
	//			}
	//		} catch (Exception e) {
	//			edc().revert();
	//			e.printStackTrace();
	//			setErreurSaisieMessage(e.getLocalizedMessage());
	//		}
	//		return null;
	//	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_isReadOnly, Boolean.TRUE);
	}

	public EORapport getRapport() {
		return (EORapport) valueForBinding(BINDING_rapport);
	}

	public String getClassTypeExport() {
		if (getRapportPret() != null && getRapportPret().toTypeExport() != null) {
			return "FwkCktlReportGuiAjax_export_" + getRapportPret().toTypeExport().texStrId().toLowerCase() + "_small";
		}
		return null;
	}

	/**
	 * @return the selectedRapportExt
	 */
	public IRapportExt getSelectedRapportExt() {
		if (selectedRapportExt == null && getRapportPret() != null && getRapportPret().toRapport().getRapportExts().count() > 0) {
			setSelectedRapportExt(getRapportPret().toRapport().getRapportExts().objectAtIndex(0));
		}
		return selectedRapportExt;
	}

	/**
	 * @param selectedRapportExt the selectedRapportExt to set
	 */
	public void setSelectedRapportExt(IRapportExt selectedRapportExt) {
		this.selectedRapportExt = selectedRapportExt;
		if (selectedRapportExt != null) {
			getRapportPret().setToTypeFormatRapportRelationship(selectedRapportExt.toTypeFormatRapport());
		}
		else {
			getRapportPret().setToTypeFormatRapportRelationship(null);
		}
	}

	/**
	 * @return the selectedTypeExport
	 */
	public EOTypeExport getSelectedTypeExport() {
		if (selectedTypeExport == null && getSelectedRapportExt() != null && getSelectedRapportExt().toTypeFormatRapport().toTypeFormatExports().count() > 0) {
			setSelectedTypeExport(((EOTypeFormatExport) getSelectedRapportExt().toTypeFormatRapport().toTypeFormatExports().objectAtIndex(0)).toTypeExport());
		}
		return selectedTypeExport;
	}

	/**
	 * @param selectedTypeExport the selectedTypeExport to set
	 */
	public void setSelectedTypeExport(EOTypeExport selectedTypeExport) {
		this.selectedTypeExport = selectedTypeExport;
		getRapportPret().setToTypeExportRelationship(selectedTypeExport);
	}

	public EOPersRapportPret getRapportPret() {
		if (rapportPret != null)
			return rapportPret;

		return (EOPersRapportPret) valueForBinding(BINDING_rapportPret);
	}

	public void setRapportPret(EOPersRapportPret rapportPret) {
		this.rapportPret = rapportPret;
	}

	public WOActionResults onChangeLibelle() {
		edc().saveChanges();
		if (hasBinding(BINDING_ACTION_REFRESH)) {
			performParentAction((String) valueForBinding(BINDING_ACTION_REFRESH));
		}
		return null;
	}

	public String getOnCompleteLibelle() {
		if (updateContainerID() != null) {
			return "function(oc){" + updateContainerID() + "Update();}";
		}
		return "";

	}

}
